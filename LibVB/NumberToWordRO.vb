﻿Namespace VBUtils
    Public Class NumberToWordRO

        Function SumaZodRO(ByVal suma2 As Decimal, ByVal CurrName As String, ByVal FracName As String) As String
            Dim Result As String
            Dim i As Integer
            Dim M_SUMA1 As Integer
            Dim M_SUMA2 As Decimal
            Dim T_SUMA As String
            Dim X As String
            M_SUMA1 = Int(suma2)
            M_SUMA2 = (suma2 - Int(suma2)) * 100
            T_SUMA = Space(12 - M_SUMA1.ToString().Length) & M_SUMA1.ToString().Trim()
            T_SUMA = T_SUMA.Replace(" ", "0")
            Result = ""

            i = 0

            While i < 12
                X = Trim(T_SUMA.Substring(i, 3))
                If Convert.ToInt32(X) <> 0 Then
                    Select Case i
                        Case 1
                            If Convert.ToInt32(X) > 1 Then
                                Result = Result & CTOL(Convert.ToInt32(X), "F")
                                Result = Result & " miliarde"
                            Else
                                Result = Result & CTOL(Convert.ToInt32(X), "M1")
                                Result = Result & " miliard"
                            End If
                        Case 4
                            If Convert.ToInt32(X) > 1 Then
                                Result = Result & CTOL(Convert.ToInt32(X), "F")
                                Result = Result & " milioane"
                            Else
                                Result = Result & CTOL(Convert.ToInt32(X), "M1")
                                Result = Result & " milion"
                            End If
                        Case 7
                            If Convert.ToInt32(X) > 1 Then
                                Result = Result & CTOL(Convert.ToInt32(X), "F")
                                Result = Result & " mii"
                            Else
                                Result = Result & CTOL(Convert.ToInt32(X), "M1")
                                Result = Result & " mie"
                            End If
                        Case Else
                            Result = Result & CTOL(Convert.ToInt32(X), "M") & " "
                    End Select
                End If
                i = i + 3
            End While

            Result = Result.Trim() + " " & CurrName & M_SUMA2.ToString() & " " & FracName

            SumaZodRO = Result
        End Function

        Function skaitmuoZodziu(ByVal sk As Long, ByVal G As String) As String
            skaitmuoZodziu = ""
            If sk < 20 Then
                Select Case sk
                    Case 1
                        If G = "F1" Then
                            skaitmuoZodziu = " o"
                        ElseIf G = "F" Then
                            skaitmuoZodziu = " unu"
                        ElseIf G = "M" Then
                            skaitmuoZodziu = " unu"
                        ElseIf G = "M1" Then
                            skaitmuoZodziu = " un"
                        End If
                    Case 2
                        If G <> "F" Then
                            skaitmuoZodziu = " doi"
                        Else
                            skaitmuoZodziu = " doua"
                        End If
                    Case 3
                        skaitmuoZodziu = " trei"
                    Case 4
                        skaitmuoZodziu = " patru"
                    Case 5
                        skaitmuoZodziu = " cinci"
                    Case 6
                        If G = "F2" Then
                            skaitmuoZodziu = " sai"
                        Else
                            skaitmuoZodziu = " sase"
                        End If
                    Case 7
                        skaitmuoZodziu = " sapte"
                    Case 8
                        skaitmuoZodziu = " opt"
                    Case 9
                        skaitmuoZodziu = " noua"
                    Case 10
                        skaitmuoZodziu = " zece"
                    Case 11
                        skaitmuoZodziu = " unsprezece"
                    Case 12
                        skaitmuoZodziu = " doisprezece"
                    Case 13
                        skaitmuoZodziu = " treisprezece"
                    Case 14
                        skaitmuoZodziu = " paisprezece"
                    Case 15
                        skaitmuoZodziu = " cincisprezece"
                    Case 16
                        skaitmuoZodziu = " saisprezece"
                    Case 17
                        skaitmuoZodziu = " saptesprezece"
                    Case 18
                        skaitmuoZodziu = " optsprezece"
                    Case 19
                        skaitmuoZodziu = " nouasprezece"
                End Select
            Else
                skaitmuoZodziu = skaitmuoZodziu(Int(sk / 100), "F2") & "zeci"
                If (sk Mod 10) <> 0 Then
                    skaitmuoZodziu = skaitmuoZodziu & " si" & skaitmuoZodziu(sk Mod 10, "M")
                End If
            End If
        End Function

        Function CTOL(ByVal CIFRA As Decimal, ByVal GEN As String) As String
            Dim Sute As Integer
            Dim x As Integer
            Dim Result As String
            Result = ""
            Sute = Int(CIFRA / 100)
            If Sute > 0 Then
                If Sute > 1 Then
                    Result = " unasuta"
                Else
                    Result = Result & skaitmuoZodziu(Sute, "F") & " sute"
                End If
            End If
            x = Int(CIFRA) Mod 100
            If x <> 0 Then
                Result = Result & skaitmuoZodziu(x, GEN)
            End If
            CTOL = Result
        End Function
    End Class
End Namespace