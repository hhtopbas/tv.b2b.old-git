﻿Namespace VBUtils
    Public Class NumberToWordEN
        Function SumaZodEng(ByVal suma2 As Decimal, ByVal CurrName As String, ByVal FracName As String) As String
            Dim eile As String
            Dim suma1 As Decimal
            Dim skaicius As Long
            Dim vidsk As Long
            Dim vPirmaRaide As Object

            skaicius = 1000000
            suma1 = suma2
            eile = ""
            While skaicius >= 1
                If skaicius <= suma1 Then
                    vidsk = Int(suma1 / skaicius)
                    eile = eile + skZodziai(vidsk)
                    eile = eile + tarpZodziai(skaicius, vidsk - Int(vidsk / 100) * 100) + " "
                End If
                suma1 = suma1 - Int(suma1 / skaicius) * skaicius
                skaicius = skaicius / 1000
            End While
            eile = eile + " " + CurrName + " " + Format(Int((suma2 - Int(suma2)) * 100 + 0.49), "0#")
            eile = eile + FracName
            vPirmaRaide = UCase(Left(eile, 1))
            SumaZodEng = vPirmaRaide + Right(eile, Len(eile) - 1)
        End Function
        Function skaitmuoZodziu(ByVal sk As Long) As String
            Select Case sk
                Case 1
                    skaitmuoZodziu = "one"
                Case 2
                    skaitmuoZodziu = "two"
                Case 3
                    skaitmuoZodziu = "three"
                Case 4
                    skaitmuoZodziu = "four"
                Case 5
                    skaitmuoZodziu = "five"
                Case 6
                    skaitmuoZodziu = "six"
                Case 7
                    skaitmuoZodziu = "seven"
                Case 8
                    skaitmuoZodziu = "eight"
                Case 9
                    skaitmuoZodziu = "nine"
                Case 10
                    skaitmuoZodziu = "ten"
                Case 11
                    skaitmuoZodziu = "eleven"
                Case 12
                    skaitmuoZodziu = "twelve"
                Case 13
                    skaitmuoZodziu = "thirteen"
                Case 14
                    skaitmuoZodziu = "fourteen"
                Case 15
                    skaitmuoZodziu = "fiveteen"
                Case 16
                    skaitmuoZodziu = "sixteen"
                Case 17
                    skaitmuoZodziu = "seventeen"
                Case 18
                    skaitmuoZodziu = "eighteen"
                Case 19
                    skaitmuoZodziu = "nineteen"
                Case 20
                    skaitmuoZodziu = "twenty"
                Case 30
                    skaitmuoZodziu = "thirty"
                Case 40
                    skaitmuoZodziu = "fourty"
                Case 50
                    skaitmuoZodziu = "fifty"
                Case 60
                    skaitmuoZodziu = "sixty"
                Case 70
                    skaitmuoZodziu = "seventy"
                Case 80
                    skaitmuoZodziu = "eighty"
                Case 90
                    skaitmuoZodziu = "ninety"
                Case 0
                    skaitmuoZodziu = ""
                Case Else
                    skaitmuoZodziu = ""
            End Select
        End Function
        Function skZodziai(ByVal sk1 As Long) As String
            Dim eile As String
            Dim sk As Long
            sk = sk1
            eile = ""
            If sk >= 100 Then
                eile = eile + skaitmuoZodziu(Int(sk / 100)) + " "
                If sk >= 200 Then
                    eile = eile + "hundred "
                Else
                    eile = eile + "hundred "
                End If
                sk = sk - Int(sk / 100) * 100
            End If
            If sk > 20 Then
                eile = eile + skaitmuoZodziu(Int(sk / 10) * 10) + " "
                sk = sk - Int(sk / 10) * 10
            End If
            eile = eile + skaitmuoZodziu(sk) + " "
            skZodziai = eile
        End Function
        Function tarpZodziai(ByVal nuliai As Long, ByVal sk As Long) As String
            Dim rakt As Long
            Dim retVal As String
            retVal = ""
            If (sk - Int(sk / 10) * 10 = 1) And (Not (sk = 11)) Then
                rakt = 1
            Else
                If (sk - Int(sk / 10) * 10 = 0) Or (sk > 10 And sk < 20) Then
                    rakt = 2
                Else
                    rakt = 3
                End If
            End If
            rakt = rakt + nuliai
            Select Case rakt
                Case 1000001
                    retVal = "milion"
                Case 1000002
                    retVal = "milions"
                Case 1000003
                    retVal = "milijonai"
                Case 1001
                    retVal = "thousand"
                Case 1002
                    retVal = "thousand"
                Case 1003
                    retVal = "thousand"
                Case 1
                    retVal = ""
                Case 2
                    retVal = ""
                Case 3
                    retVal = ""
                Case 4
                    retVal = ""
            End Select
            tarpZodziai = retVal
        End Function
    End Class
End Namespace