﻿Namespace VBUtils
    Public Class NumberToWordTR
        Function YaziileTL(ByVal Tutar, Optional ByVal Yazi_Tipi = 0) As String
            Dim SayiStr As String
            Dim Yuzler As String
            Dim Sonuc As String
            Dim Digit As String
            Dim Hane As Integer
            Dim Binlik As Integer
            Dim i As Integer
            SayiStr = Format(Int(Tutar), "#")
            Dim Kurus As Decimal

            Kurus = (Tutar - Int(Tutar)) * 100
            Yuzler = ""
            Sonuc = ""
            For i = Len(SayiStr) To 1 Step -1
                Digit = Mid(SayiStr, Len(SayiStr) - (i - 1), 1)
                Hane = (i - Int(i / 3) * 3)
                Hane = IIf(Hane = 0, 3, Hane)
                Binlik = Int(i / 3) + 1
                Select Case Hane
                    Case 1 : Select Case Digit
                            Case 0 : Yuzler = Yuzler + ""
                            Case 1
                                If Not Binlik = 2 Then
                                    Yuzler = Yuzler + "bir"
                                ElseIf Not Trim(Yuzler) = "" Then
                                    Yuzler = Yuzler + "Bir"
                                Else
                                    Yuzler = Yuzler + " "
                                End If
                            Case 2 : Yuzler = Yuzler + "iki "
                            Case 3 : Yuzler = Yuzler + "üç "
                            Case 4 : Yuzler = Yuzler + "dört "
                            Case 5 : Yuzler = Yuzler + "beş "
                            Case 6 : Yuzler = Yuzler + "alti "
                            Case 7 : Yuzler = Yuzler + "yedi "
                            Case 8 : Yuzler = Yuzler + "sekiz "
                            Case 9 : Yuzler = Yuzler + "dokuz "
                        End Select
                    Case 2 : Select Case Digit
                            Case 1 : Yuzler = Yuzler + "On"
                            Case 2 : Yuzler = Yuzler + "yirmi"
                            Case 3 : Yuzler = Yuzler + "otuz"
                            Case 4 : Yuzler = Yuzler + "kirk"
                            Case 5 : Yuzler = Yuzler + "elli"
                            Case 6 : Yuzler = Yuzler + "altmiş"
                            Case 7 : Yuzler = Yuzler + "yetmiş"
                            Case 8 : Yuzler = Yuzler + "seksen"
                            Case 9 : Yuzler = Yuzler + "doksan"
                        End Select
                    Case 3 : Select Case Digit
                            Case 1 : Yuzler = Yuzler + "yüz "
                            Case 2 : Yuzler = Yuzler + "ikiyüz "
                            Case 3 : Yuzler = Yuzler + "üçyüz "
                            Case 4 : Yuzler = Yuzler + "dörtyüz "
                            Case 5 : Yuzler = Yuzler + "beşyüz "
                            Case 6 : Yuzler = Yuzler + "altıyüz "
                            Case 7 : Yuzler = Yuzler + "yediyüz "
                            Case 8 : Yuzler = Yuzler + "sekizyüz "
                            Case 9 : Yuzler = Yuzler + "dokuzyüz "
                        End Select
                End Select
                If Hane = 1 Then
                    If Not Yuzler = "" Then
                        Select Case Binlik
                            Case 1 : Sonuc = Sonuc + Yuzler
                            Case 2 : Sonuc = Sonuc + Yuzler + "Bin,"
                            Case 3 : Sonuc = Sonuc + Yuzler + "Milyon,"
                            Case 4 : Sonuc = Sonuc + Yuzler + "Milyar,"
                            Case 5 : Sonuc = Sonuc + Yuzler + "Tirilyon,"
                            Case 6 : Sonuc = Sonuc + Yuzler + "-*E15-,"
                            Case 7 : Sonuc = Sonuc + Yuzler + "-*E18-,"
                            Case 8 : Sonuc = Sonuc + Yuzler + "-*E21-,"
                            Case Else : Sonuc = Sonuc + Yuzler + "-*E24-,"
                        End Select
                    End If
                    Yuzler = ""
                End If
            Next i
            Select Case Yazi_Tipi
                Case Is < 0 : Sonuc = LCase(Sonuc)
                Case Is = 0 : Sonuc = Sonuc
                Case Is > 0 : Sonuc = UCase(Sonuc)
            End Select

            YaziileTL = IIf(Trim(Sonuc) = "", "", Sonuc & ".TL") & " " & YaziileKurus(Kurus, 0)
        End Function
        Function YaziileKurus(ByVal Tutar, Optional ByVal Yazi_Tipi = 0) As String
            Dim SayiStr As String
            Dim Yuzler As String
            Dim Sonuc As String
            Dim Digit As String
            Dim Hane As Integer
            Dim Binlik As Integer
            Dim i As Integer
            SayiStr = Format(Tutar, "#")
            Yuzler = ""
            Sonuc = ""
            For i = Len(SayiStr) To 1 Step -1
                Digit = Mid(SayiStr, Len(SayiStr) - (i - 1), 1)
                Hane = (i - Int(i / 3) * 3)
                Hane = IIf(Hane = 0, 3, Hane)
                Binlik = Int(i / 3) + 1
                Select Case Hane
                    Case 1 : Select Case Digit
                            Case 0 : Yuzler = Yuzler + ""
                            Case 1
                                If Not Binlik = 2 Then
                                    Yuzler = Yuzler + "bir"
                                ElseIf Not Trim(Yuzler) = "" Then
                                    Yuzler = Yuzler + "Bir"
                                Else
                                    Yuzler = Yuzler + " "
                                End If
                            Case 2 : Yuzler = Yuzler + "iki "
                            Case 3 : Yuzler = Yuzler + "üç "
                            Case 4 : Yuzler = Yuzler + "dört "
                            Case 5 : Yuzler = Yuzler + "beş "
                            Case 6 : Yuzler = Yuzler + "alti "
                            Case 7 : Yuzler = Yuzler + "yedi "
                            Case 8 : Yuzler = Yuzler + "sekiz "
                            Case 9 : Yuzler = Yuzler + "dokuz "
                        End Select
                    Case 2 : Select Case Digit
                            Case 1 : Yuzler = Yuzler + "On"
                            Case 2 : Yuzler = Yuzler + "yirmi"
                            Case 3 : Yuzler = Yuzler + "otuz"
                            Case 4 : Yuzler = Yuzler + "kirk"
                            Case 5 : Yuzler = Yuzler + "elli"
                            Case 6 : Yuzler = Yuzler + "altmiş"
                            Case 7 : Yuzler = Yuzler + "yetmiş"
                            Case 8 : Yuzler = Yuzler + "seksen"
                            Case 9 : Yuzler = Yuzler + "doksan"
                        End Select
                    Case 3 : Select Case Digit
                            Case 1 : Yuzler = Yuzler + "yüz "
                            Case 2 : Yuzler = Yuzler + "ikiyüz "
                            Case 3 : Yuzler = Yuzler + "üçyüz "
                            Case 4 : Yuzler = Yuzler + "dörtyüz "
                            Case 5 : Yuzler = Yuzler + "beşyüz "
                            Case 6 : Yuzler = Yuzler + "altıyüz "
                            Case 7 : Yuzler = Yuzler + "yediyüz "
                            Case 8 : Yuzler = Yuzler + "sekizyüz "
                            Case 9 : Yuzler = Yuzler + "dokuzyüz "
                        End Select
                End Select
                If Hane = 1 Then
                    If Not Yuzler = "" Then
                        Select Case Binlik
                            Case 1 : Sonuc = Sonuc + Yuzler
                            Case 2 : Sonuc = Sonuc + Yuzler + "Bin,"
                            Case 3 : Sonuc = Sonuc + Yuzler + "Milyon,"
                            Case 4 : Sonuc = Sonuc + Yuzler + "Milyar,"
                            Case 5 : Sonuc = Sonuc + Yuzler + "Tirilyon,"
                            Case 6 : Sonuc = Sonuc + Yuzler + "-*E15-,"
                            Case 7 : Sonuc = Sonuc + Yuzler + "-*E18-,"
                            Case 8 : Sonuc = Sonuc + Yuzler + "-*E21-,"
                            Case Else : Sonuc = Sonuc + Yuzler + "-*E24-,"
                        End Select
                    End If
                    Yuzler = ""
                End If
            Next i
            Select Case Yazi_Tipi
                Case Is < 0 : Sonuc = LCase(Sonuc)
                Case Is = 0 : Sonuc = Sonuc
                Case Is > 0 : Sonuc = UCase(Sonuc)
            End Select

            YaziileKurus = IIf(Trim(Sonuc) = "", "", Sonuc & " .Kr.")
        End Function
    End Class
End Namespace