﻿Namespace VBUtils
    Public Class NumberToWordEE
        Function SumaZodEE(ByVal suma2 As Decimal, ByVal CurrName As String, ByVal FracName As String) As String
            Dim eile As String
            Dim suma1 As Decimal
            Dim skaicius As Long
            Dim vidsk As Long
            Dim vPirmaRaide As Object

            skaicius = 1000000
            suma1 = suma2
            eile = ""

            While skaicius >= 1
                If skaicius <= suma1 Then
                    vidsk = Int(suma1 / skaicius)
                    eile = eile + skZodziai(vidsk)
                    eile = eile + tarpZodziai(skaicius, vidsk - Int(vidsk / 100) * 100) + " "
                End If
                suma1 = suma1 - Int(suma1 / skaicius) * skaicius
                skaicius = skaicius / 1000
            End While
            eile = eile + " " + CurrName + " " + Format(Int((suma2 - Int(suma2)) * 100 + 0.49), "0#")
            eile = eile + " " + FracName

            vPirmaRaide = UCase(Left(eile, 1))
            SumaZodEE = vPirmaRaide + Right(eile, Len(eile) - 1)

        End Function
        Function skaitmuoZodziu(ByVal sk As Long) As String

            Select Case sk
                Case 1
                    skaitmuoZodziu = "üks"
                Case 2
                    skaitmuoZodziu = "kaks"
                Case 3
                    skaitmuoZodziu = "kolm"
                Case 4
                    skaitmuoZodziu = "neli"
                Case 5
                    skaitmuoZodziu = "viis"
                Case 6
                    skaitmuoZodziu = "kuus"
                Case 7
                    skaitmuoZodziu = "seitse"
                Case 8
                    skaitmuoZodziu = "kaheksa"
                Case 9
                    skaitmuoZodziu = "üheksa"
                Case 10
                    skaitmuoZodziu = "kümme"
                Case 11
                    skaitmuoZodziu = "üksteist"
                Case 12
                    skaitmuoZodziu = "kaksteist"
                Case 13
                    skaitmuoZodziu = "kolmteist"
                Case 14
                    skaitmuoZodziu = "neliteist"
                Case 15
                    skaitmuoZodziu = "viisteist"
                Case 16
                    skaitmuoZodziu = "kuusteist"
                Case 17
                    skaitmuoZodziu = "seitseteist"
                Case 18
                    skaitmuoZodziu = "kaheksateist"
                Case 19
                    skaitmuoZodziu = "üheksateist"
                Case 20
                    skaitmuoZodziu = "kakskümmend"
                Case 30
                    skaitmuoZodziu = "kolmkümmend"
                Case 40
                    skaitmuoZodziu = "nelikümmend"
                Case 50
                    skaitmuoZodziu = "viiskümmend"
                Case 60
                    skaitmuoZodziu = "kuuskümmend"
                Case 70
                    skaitmuoZodziu = "seitsekümmend"
                Case 80
                    skaitmuoZodziu = "kaheksakümmend"
                Case 90
                    skaitmuoZodziu = "üheksakümmend"
                Case 100
                    skaitmuoZodziu = "sada"
                Case 200
                    skaitmuoZodziu = "kakssada"
                Case 300
                    skaitmuoZodziu = "kolmsada"
                Case 400
                    skaitmuoZodziu = "nelisada"
                Case 500
                    skaitmuoZodziu = "viissada"
                Case 600
                    skaitmuoZodziu = "kuussada"
                Case 700
                    skaitmuoZodziu = "seitsesada"
                Case 800
                    skaitmuoZodziu = "kaheksasada"
                Case 900
                    skaitmuoZodziu = "üheksasada"
                Case 0
                    skaitmuoZodziu = ""
                Case Else
                    skaitmuoZodziu = ""
            End Select
        End Function
        Function skZodziai(ByVal sk1 As Long) As String
            Dim eile As String
            Dim sk As Long
            sk = sk1
            eile = ""
            If sk >= 100 Then
                eile = eile + skaitmuoZodziu(Int(sk / 100) * 100) + " "
                sk = sk - Int(sk / 100) * 100
            End If
            If sk > 20 Then
                eile = eile + skaitmuoZodziu(Int(sk / 10) * 10) + " "
                sk = sk - Int(sk / 10) * 10
            End If
            eile = eile + skaitmuoZodziu(sk) + " "
            skZodziai = eile
        End Function
        Function tarpZodziai(ByVal nuliai As Long, ByVal sk As Long) As String
            tarpZodziai = ""
            Dim rakt As Long
            If (sk - Int(sk / 10) * 10 = 1) And (Not (sk = 11)) Then
                rakt = 1
            Else
                If (sk - Int(sk / 10) * 10 = 0) Or (sk > 10 And sk < 20) Then
                    rakt = 2
                Else
                    rakt = 3
                End If
            End If

            rakt = rakt + nuliai

            Select Case rakt
                Case 1000001
                    tarpZodziai = "miljon"
                Case 1000002
                    tarpZodziai = "miljonit"
                Case 1000003
                    tarpZodziai = "miljonit"
                Case 1001
                    tarpZodziai = "tuhat"
                Case 1002
                    tarpZodziai = "tuhat"
                Case 1003
                    tarpZodziai = "tuhat"
                Case 1
                    tarpZodziai = " "
                Case 2
                    tarpZodziai = " "
                Case 3
                    tarpZodziai = " "
                Case 4
                    tarpZodziai = " "
            End Select
        End Function
    End Class
End Namespace