﻿Namespace VBUtils
    Public Class NumberToWordPL
        Function SumaZod(ByVal suma2 As Decimal, ByVal CurrName As String, ByVal FracName As String) As String
            Dim eile As String
            Dim suma1 As Decimal
            Dim skaicius As Long
            Dim vidsk As Long
            Dim vPirmaRaide As Object
            skaicius = 1000000
            suma1 = suma2
            eile = ""

            While skaicius >= 1
                If skaicius <= suma1 Then
                    vidsk = Int(suma1 / skaicius)
                    eile = eile + skZodziai(vidsk)
                    eile = eile + tarpZodziai(skaicius, vidsk - Int(vidsk / 100) * 100) + " "
                End If
                suma1 = suma1 - Int(suma1 / skaicius) * skaicius
                skaicius = skaicius / 1000
            End While
            eile = eile + " " + CurrName + " " + Format(Int((suma2 - Int(suma2)) * 100 + 0.49), "0#")
            eile = eile + " " + FracName
            vPirmaRaide = UCase(Left(eile, 1))
            SumaZod = vPirmaRaide + Right(eile, Len(eile) - 1)

        End Function
        Function skaitmuoZodziu(ByVal sk As Long) As String
            Select Case sk
                Case 1
                    skaitmuoZodziu = "jeden"
                Case 2
                    skaitmuoZodziu = "dwa"
                Case 3
                    skaitmuoZodziu = "trzy"
                Case 4
                    skaitmuoZodziu = "cztery"
                Case 5
                    skaitmuoZodziu = "pięć"
                Case 6
                    skaitmuoZodziu = "sześć"
                Case 7
                    skaitmuoZodziu = "siedem"
                Case 8
                    skaitmuoZodziu = "osiem"
                Case 9
                    skaitmuoZodziu = "dziewięć"
                Case 10
                    skaitmuoZodziu = "dziesięć"
                Case 11
                    skaitmuoZodziu = "jedenaście"
                Case 12
                    skaitmuoZodziu = "dwanaście"
                Case 13
                    skaitmuoZodziu = "trzynaście"
                Case 14
                    skaitmuoZodziu = "czternaście"
                Case 15
                    skaitmuoZodziu = "piętnaście"
                Case 16
                    skaitmuoZodziu = "szesnaście"
                Case 17
                    skaitmuoZodziu = "siedemnaście"
                Case 18
                    skaitmuoZodziu = "osiemnaście"
                Case 19
                    skaitmuoZodziu = "dziewiętnaście"
                Case 20
                    skaitmuoZodziu = "dwadzieścia"
                Case 30
                    skaitmuoZodziu = "trzydzieści"
                Case 40
                    skaitmuoZodziu = "czterdzieści"
                Case 50
                    skaitmuoZodziu = "pięćdziesiąt"
                Case 60
                    skaitmuoZodziu = "sześćdziesiąt"
                Case 70
                    skaitmuoZodziu = "siedemdziesiąt"
                Case 80
                    skaitmuoZodziu = "osiemdziesiąt"
                Case 90
                    skaitmuoZodziu = "dziewięćdziesiąt"
                Case 0
                    skaitmuoZodziu = "zero"
                Case Else
                    skaitmuoZodziu = ""
            End Select
        End Function
        Function skZodziai(ByVal sk1 As Long) As String
            Dim eile As String
            Dim sk As Long
            sk = sk1
            eile = ""
            If sk >= 100 Then
                eile = eile + skaitmuoZodziu(Int(sk / 100)) + " "
                If sk >= 200 Then
                    eile = eile + "sto "
                Else
                    eile = eile + "sto "
                End If
                sk = sk - Int(sk / 100) * 100
            End If
            If sk > 20 Then
                eile = eile + skaitmuoZodziu(Int(sk / 10) * 10) + " "
                sk = sk - Int(sk / 10) * 10
            End If
            eile = eile + skaitmuoZodziu(sk) + " "
            skZodziai = eile
        End Function
        Function tarpZodziai(ByVal nuliai As Long, ByVal sk As Long) As String
            tarpZodziai = ""
            Dim rakt As Long
            If (sk - Int(sk / 10) * 10 = 1) And (Not (sk = 11)) Then
                rakt = 1
            Else
                If (sk - Int(sk / 10) * 10 = 0) Or (sk > 10 And sk < 20) Then
                    rakt = 2
                Else
                    rakt = 3
                End If
            End If

            rakt = rakt + nuliai

            Select Case rakt
                Case 1000001
                    tarpZodziai = "milion"
                Case 1000002
                    tarpZodziai = "milion"
                Case 1000003
                    tarpZodziai = "milion"
                Case 1001
                    tarpZodziai = "tysiąc"
                Case 1002
                    tarpZodziai = "tysiąc"
                Case 1003
                    tarpZodziai = "tysiąc"
                Case 1
                    tarpZodziai = ""
                Case 2
                    tarpZodziai = ""
                Case 3
                    tarpZodziai = ""
                Case 4
                    tarpZodziai = ""
            End Select
        End Function
    End Class
End Namespace