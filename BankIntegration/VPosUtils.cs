﻿using _PosnetDotNetModule;
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml;

namespace BankIntegration
{
    public class VPosUtils
    {
        public static string GetPan(string host, string clientid, string ccno, string cv2, string ExpYear, string ExpMonth, string type, string installment)
        {
            string result = string.Empty;
            string pan = string.Empty;
            StreamWriter swr = null;
            StreamReader sr = null;
            try
            {
                string message = "clientid=" + clientid + "&pan=" + ccno + "&cv2=" + cv2 + "&Ecom_Payment_Card_ExpDate_Year=" + ExpYear + "&Ecom_Payment_Card_ExpDate_Month=" + ExpMonth + "&txtype=" + type + "&taksit=" + installment;
                WebRequest request = HttpWebRequest.Create(host);
                request.Method = "POST";
                request.ContentLength = message.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                swr = new StreamWriter(request.GetRequestStream());
                swr.Write(message);
                swr.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                sr = new StreamReader(response.GetResponseStream());
                result = sr.ReadToEnd();
                sr.Close();
                response.Close();
                request = null;
                int first = result.IndexOf('"', result.IndexOf("value", result.IndexOf("pan"))) + 1;
                int last = result.IndexOf('"', result.IndexOf('"', result.IndexOf("value", result.IndexOf("pan"))) + 1);
                pan = result.Substring(first, last - first);
            }

            catch
            {
                pan = String.Empty;
            }
            return pan;
        }

        public static string ConnectToHSBCPos(string host, string clientid, string name, string password, string currency,
                                                string cardnumber, string expmonth, string expyear, string cv2, string oid,
                                                string subTotal, ref string hataKodu, ref string posreferans, string taksit,
                                                string LeaderName, string LeaderPhone)
        {

            ePayment.cc5payment payment = new ePayment.cc5payment();
            payment.host = host;
            payment.clientid = clientid;  //POS magaza numarasi
            payment.name = name;  //POS kullanici adi (Buraya magaza yoneticisinin kullanici adi olmamali, Payment Clerk olmali)
            payment.password = password;  //POS sifre
            payment.bname = LeaderName;
            payment.phone = LeaderPhone;
            if (Convert.ToInt32(taksit) > 1)
            {
                payment.taksit = taksit;
            }

            string pan = GetPan(host, clientid, cardnumber, cv2, expyear, expmonth, "Auth", taksit);
            if (pan == "")
            {
                hataKodu = "HSBC kart hatası";
                //new VPosLog().writeVPosLog(ResNo, UserData.Rows[0]["UserID"].ToString(), name, subTotal, UserData.Rows[0]["IpNumber"].ToString(),
                //                           oid, "", "", "", "", "", hataKodu, "", "");
                return hataKodu;
            }
            payment.cardnumber = pan;
            payment.chargetype = "Auth";// "Auth";    //POS kullanim tipi, Satis icin Auth, degisik parametreler set edilerek,
            //rapor alinabilir, iade islemi yapilabilir.
            payment.currency = currency;   //YTL kodu
            payment.orderresult = 0;// 1;    //0 sa gercek islem , 1 ise test islemi(Garanti icin, diger bankalarda farkli olabilir)            
            payment.subtotal = subTotal.Replace(',', '.');

            try
            {
                payment.ip = HttpContext.Current.Request.UserHostAddress;
            }
            catch
            {
            }

            string str = payment.processorder();
            string rtn = "";

            if (str == "1")
            {
                if (payment.appr == "Approved")
                {
                    //new VPosLog().writeVPosLog(ResNo, UserData.Rows[0]["UserID"].ToString(), name, subTotal, UserData.Rows[0]["IpNumber"].ToString(),
                    //                       oid, "", "", "Approved", "", "", "", "", "");
                    rtn = "1";
                }
                else if (payment.appr == "Declined")
                {
                    rtn = "İşlem kabul edilmedi.Lütfen tekrar deneyiniz. ( ProcReturnCode=" + payment.procreturncode + ") ( Error Msg:" + payment.errmsg + ")";
                }
                else
                {
                    //rtn += " // " + str + "/";
                    //rtn += paymentCC.appr + "/";
                    //rtn += paymentCC.errmsg + "/";
                    rtn += payment.Extra("HOSTMSG") + " ( ProcReturnCode=" + payment.procreturncode + ") ( Error Msg:" + payment.errmsg + ")";
                }
            }
            else
            {
                rtn = "İşlem kabul edilmedi.Lütfen tekrar deneyiniz.";
            }

            posreferans = payment.transid;
            hataKodu = rtn;
            return hataKodu;
        }

        public static string ConnectToVPos(string host, string clientid, string name, string password, string currency,
                string cardnumber, string expmonth, string expyear, string cv2, string oid,
                string subTotal, ref string hataKodu, ref string posreferans, string taksit, string LeaderName, string LeaderPhone, bool test)
        {
            ePayment.cc5payment payment = new ePayment.cc5payment();
            payment.host = host;
            payment.clientid = clientid;  //POS magaza numarasi
            payment.name = name;  //POS kullanici adi (Buraya magaza yoneticisinin kullanici adi olmamali, Payment Clerk olmali)
            payment.password = password;  //POS sifre
            payment.chargetype = "Auth"; //POS kullanim tipi, Satis icin Auth, degisik parametreler set edilerek,
            //rapor alinabilir, iade islemi yapilabilir.
            payment.currency = currency;   //YTL kodu
            payment.orderresult = 0;// 1;    //0 sa gercek islem , 1 ise test islemi(Garanti icin, diger bankalarda farkli olabilir)
            payment.cardnumber = cardnumber;  //kredi karti no        
            payment.bname = LeaderName;
            payment.phone = LeaderPhone;

            if (Convert.ToInt32(taksit) > 1)
            {
                payment.taksit = taksit;
            }

            payment.expmonth = expmonth;    //kredi karti son kullanma tarihi AY
            payment.expyear = expyear;     //kredi karti son kullanma tarihi YIL
            payment.cv2 = cv2;              //kredi karti guvenlik kodu
            payment.oid = oid;        //oid; //order id, bu benzersiz olmali, eger ilk islemde hata doner,sonrasinda ayni order id ile islem yapmaya calisinca hata verir.        
            payment.subtotal = subTotal;

            try
            {
                payment.ip = HttpContext.Current.Request.UserHostAddress;
            }
            catch
            {
            }

            string str = payment.processorder();
            string rtn = "";

            if (str == "1")
            {
                if (payment.appr == "Approved")
                {
                    rtn = "1";
                }
                else if (payment.appr == "Declined")
                {
                    //rtn =  string.Format("İşlem kabul edilmedi.Lütfen tekrar deneyiniz. ( ProcReturnCode=" + payment.procreturncode + ") ( Error Msg:" + payment.errmsg + ")";
                    rtn = string.Format("Transaction was not accepted. Please try again. ( Error code= {0}) ( Error message: {1}", payment.procreturncode, payment.errmsg);
                }
                else
                {
                    //rtn += " // " + str + "/";
                    //rtn += paymentCC.appr + "/";
                    //rtn += paymentCC.errmsg + "/";
                    rtn += payment.Extra("HOSTMSG") + string.Format(" ( Error code= {0}) Error message: {1}", payment.procreturncode, payment.errmsg);
                }
            }
            else
            {
                rtn = "Transaction was not accepted. Please try again.";
            }

            posreferans = payment.transid;
            hataKodu = rtn;
            return hataKodu;
        }

        public static string ConnectToYapiKrediPos(string OwnIP, string mid, string tid, string cardnumber, string expmonth,
                        string expyear, string cv2, string oid, string subTotal, string currency, ref string hataKodu,
                        ref string posreferans, string taksit, string LeaderName, string LeaderPhone)
        {
            /*
            C_Posnet cs = new C_Posnet();
            //YKBPosnetActiveX.YKBPosnetClassClass cs = new YKBPosnetActiveX.YKBPosnetClassClass();
            cs.SetMid(mid);
            cs.SetTid(tid);
            cs.SetURL("https://www.posnet.ykb.com/PosnetWebService/XML");

            //cs.SetURL("193.254.228.100");	//üretim ortamı için 193.254.228.100 giriniz
            // cs.SetOwnIP(OwnIP);		//buraya kendi ip'nizi giriniz.
            //cs.SetPort("2222");
            expyear = expyear.Substring(expyear.Length - 2, 2);

            if (expmonth.Length == 1)
                expmonth = "0" + expmonth;
            oid = oid.Replace(",", "X").Replace(".", "X");
            while (oid.Length < 24)
                oid = "0" + oid;

            subTotal = subTotal.Replace(",", "X").Replace(".", "X");
            string[] totalList = subTotal.Split('X');
            string tamkisim = totalList[0];
            if (tamkisim.Length == 0) { hataKodu = "Tutar Hatalı"; return "Tutar Hatalı"; }
            string ondalikkisim = "00";
            if (totalList.Length > 1)
                ondalikkisim = totalList[1];
            if (ondalikkisim.Length > 2)
                ondalikkisim = ondalikkisim.Substring(0, 2);
            else
            {
                while (ondalikkisim.Length < 2)
                    ondalikkisim = ondalikkisim + "0";
            }
            if (taksit == "1") taksit = "00";
            if (taksit.Length == 1) taksit = "0" + taksit;

            bool ok = cs.DoSaleTran(
                cardnumber, 	//cc no
                expyear + expmonth,			//exp date
                cv2,			//cvc
                oid, 	//order id
                tamkisim + ondalikkisim,			//amount
                currency,			//currency code
                taksit, "00", "000000" 			//installment number
                );

            if (ok)
            {
                if (cs.GetApprovedCode() == "1" || cs.GetApprovedCode() == "2")
                {
                    hataKodu = "1";
                    posreferans = cs.GetHostlogkey();
                }
                else
                    hataKodu = "Error " + cs.GetResponseText();
            }
            else
                hataKodu = "Error Code " + cs.GetResponseText();
            return hataKodu.Replace('\'', ' ').Replace('"', ' ').Replace(':', ' ');
             */
            return "";
        }
    }
    public class Pos
    {
        #region Property
        private string SystemError = "Bankayla bağlantı kurulamadı ! Lütfen daha sonra tekrar deneyin.";

        public bool Result { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorCode { get; set; }
        public object BankResult { get; set; }

        // Vpos return values
        public string Code { get; set; }
        public string GroupId { get; set; }
        public string TransId { get; set; }
        public string RefNo { get; set; }
        #endregion
        #region Method
        public string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            return GetHexaDecimal(inputbytes);
        }
        public string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;
            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }
            return s.ToString();
        }
        public void GarantiBankasi(PosForm pf)
        {
            string log = string.Empty;
            string strOrderID = pf.OrderId;// "DENEME";

            try
            {
                string strMode = "PROD";
                string strVersion = "v0.01";
                string strTerminalID = pf.vPosInfo.ClientID; // "XXXXXXXX"; //8 Haneli TerminalID yazılmalı.
                string _strTerminalID = "0" + strTerminalID;
                string strProvUserID = "PROVAUT";
                string strProvisionPassword = pf.vPosInfo.ProvUserPass; // "XXXXXXXX"; //TerminalProvUserID şifresi
                string strUserID = pf.vPosInfo.UserID; // "XXXXXX";
                string strMerchantID = pf.vPosInfo.MerchantID; // "XXXXXXXX"; //Üye İşyeri Numarası
                string strIPAddress = HttpContext.Current.Request.IsLocal ? "192.168.1.1" : HttpContext.Current.Request.UserHostAddress; //Kullanıcının IP adresini alır
                string strEmailAddress = pf.CustomerEmail;
                string strNumber = pf.CardNumber==0?"": pf.CardNumber.ToString();
                string strExpireDate = pf.Month + pf.Year;
                string strCVV2 = pf.CardCvv2;
                string strAmount = pf.Amount.ToString("#.00").Replace(".", "").Replace(",", ""); //İşlem Tutarı 1.00 TL için 100 gönderilmeli
                string strType = "sales";
                string strCurrencyCode = pf.vPosInfo.BankCur;
                string strMotoInd = "N";
                string strInstallmentCount = pf.Instalment == 0 ? "" : pf.Instalment.ToString();
                string strHostAddress = pf.vPosInfo.Host; // "https ://sanalposprov.garanti.com.tr/VPServlet";

                string SecurityData = GetSHA1(strProvisionPassword + _strTerminalID).ToUpper();
                string HashData = GetSHA1(strOrderID + strTerminalID + strNumber + strAmount + SecurityData).ToUpper();

                string strXML = string.Empty;
                strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                         "<GVPSRequest>" +
                           "<Mode>" + strMode + "</Mode>" +
                           "<Version>" + strVersion + "</Version>" +
                           "<ChannelCode></ChannelCode>" +
                           "<Terminal>" +
                             "<ProvUserID>" + strProvUserID + "</ProvUserID>" +
                             "<HashData>" + HashData + "</HashData>" +
                             "<UserID>" + strUserID + "</UserID>" +
                             "<ID>" + strTerminalID + "</ID>" +
                             "<MerchantID>" + strMerchantID + "</MerchantID>" +
                           "</Terminal>" +
                           "<Customer>" +
                             "<IPAddress>" + strIPAddress + "</IPAddress>" +
                             "<EmailAddress>" + strEmailAddress + "</EmailAddress>" +
                           "</Customer>";
                if (pf.Secure3D != null)
                {
                    strXML += "<Card>" +
                                "<Number></Number>" +
                                "<ExpireDate></ExpireDate>" +
                                "<CVV2></CVV2>" +
                              "</Card>";
                }
                else
                {
                    strXML += "<Card>" +
                               "<Number>" + strNumber + "</Number>" +
                               "<ExpireDate>" + strExpireDate + "</ExpireDate>" +
                               "<CVV2>" + strCVV2 + "</CVV2>" +
                              "</Card>";
                }
                strXML += "<Order>" +
                             "<OrderID>" + strOrderID + "</OrderID>" +
                             "<GroupID></GroupID>" +
                             "<AddressList>" +
                               "<Address>" +
                               "<Type>" + (pf.Secure3D != null ? "B" : "S") + "</Type>" +
                                 "<Name></Name>" +
                                 "<LastName></LastName>" +
                                 "<Company></Company>" +
                                 "<Text></Text>" +
                                 "<District></District>" +
                                 "<City></City>" +
                                 "<PostalCode></PostalCode>" +
                                 "<Country></Country>" +
                                 "<PhoneNumber></PhoneNumber>" +
                               "</Address>" +
                             "</AddressList>" +
                           "</Order>" +
                           "<Transaction>" +
                             "<Type>" + strType + "</Type>" +
                             "<InstallmentCnt>" + strInstallmentCount + "</InstallmentCnt>" +
                             "<Amount>" + strAmount + "</Amount>" +
                             "<CurrencyCode>" + strCurrencyCode + "</CurrencyCode>" +
                             "<CardholderPresentCode>" + pf.CardholderPresentCode + "</CardholderPresentCode>" +
                             "<MotoInd>" + strMotoInd + "</MotoInd>";
                if (pf.Secure3D != null)
                {
                    strXML += "<Secure3D>" +
                            "<AuthenticationCode>" + pf.Secure3D.AuthenticationCode + "</AuthenticationCode>" +
                             "<SecurityLevel>" + pf.Secure3D.SecurityLevel + "</SecurityLevel>" +
                             "<TxnID>" + pf.Secure3D.TxnID + "</TxnID>" +
                             "<Md>" + pf.Secure3D.Md + "</Md>" +
                            "</Secure3D>";
                }
                strXML += "</Transaction>" +
                "</GVPSRequest>";
                var logs = new
                {
                    strMode = strMode,
                    strVersion = strVersion,
                    strTerminalID = strTerminalID,
                    strProvUserID = strProvUserID,
                    strProvisionPassword = strProvisionPassword,
                    strUserID = strUserID,
                    strMerchantID = strMerchantID,
                    strIPAddress = strIPAddress,
                    strEmailAddress = strEmailAddress,
                    strOrderID = strOrderID,
                    strNumber = strNumber,
                    strExpireDate = strExpireDate,
                    strCVV2 = strCVV2,
                    strAmount = strAmount,
                    strType = strType,
                    strCurrencyCode = strCurrencyCode,
                    strCardholderPresentCode = pf.CardholderPresentCode,
                    strMotoInd = strMotoInd,
                    strInstallmentCount = strInstallmentCount,
                    strHostAddress = strHostAddress,
                    SecurityData = SecurityData,
                    HashData = HashData
                };

                log += Newtonsoft.Json.JsonConvert.SerializeObject(logs) + "\r\n";


                string data = "data=" + strXML;

                WebRequest _WebRequest = WebRequest.Create(strHostAddress);
                _WebRequest.Method = "POST";

                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                _WebRequest.ContentType = "application/x-www-form-urlencoded";
                _WebRequest.ContentLength = byteArray.Length;

                Stream dataStream = _WebRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse _WebResponse = _WebRequest.GetResponse();

                //Console.WriteLine(((HttpWebResponse)_WebResponse).StatusDescription);

                dataStream = _WebResponse.GetResponseStream();

                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                var logs1 = new
                {
                    responseFromServer = responseFromServer
                };

                log += Newtonsoft.Json.JsonConvert.SerializeObject(logs1) + "\r\n";
                // Console.WriteLine(responseFromServer);

                //Müşteriye gösterilebilir ama Fraud riski açısından bu değerleri göstermeyelim.
                //responseFromServer

                //GVPSResponse XML'in değerlerini okuyoruz. İstediğiniz geri dönüş değerlerini gösterebilirsiniz.
                string XML = responseFromServer;
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(XML);

                //ReasonCode
                XmlElement xElement1 = xDoc.SelectSingleNode("//GVPSResponse/Transaction/Response/ReasonCode") as XmlElement;
                //lblResult2.Text = xElement1.InnerText;

                //Message
                XmlElement xElement2 = xDoc.SelectSingleNode("//GVPSResponse/Transaction/Response/Message") as XmlElement;
                //lblResult2.Text = xElement2.InnerText;
                //ErrorMsg
                XmlElement xElement3 = xDoc.SelectSingleNode("//GVPSResponse/Transaction/Response/ErrorMsg") as XmlElement;
                //00 ReasonCode döndüğünde işlem başarılıdır. Müşteriye başarılı veya başarısız şeklinde göstermeniz tavsiye edilir. (Fraud riski)
                XmlElement xElement4 = xDoc.SelectSingleNode("//GVPSResponse/Transaction/Response/SysErrMsg") as XmlElement;
                if (xElement1.InnerText == "00")
                {
                    this.Result = true;
                    this.Code = xElement1.InnerText;
                    var logs2 = new
                    {
                        Result = this.Result,
                        Code = this.Code
                    };

                    log += Newtonsoft.Json.JsonConvert.SerializeObject(logs2) + "\r\n";
                }
                else
                {
                    this.Result = false;
                    this.ErrorMessage =string.Format("{0} - {1}",xElement3.InnerText,xElement4.InnerText);
                    var logs3 = new
                    {
                        Result = this.Result,
                        ErrorMessage =this.ErrorMessage
                    };

                    log += Newtonsoft.Json.JsonConvert.SerializeObject(logs3) + "\r\n";
                }
                var logdir = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute("~/Log"));
                System.IO.File.WriteAllText(System.IO.Path.Combine(logdir, strOrderID) + ".txt", log);
            }
            catch (System.Exception)
            {
                this.Result = false;
                this.ErrorMessage = this.SystemError;
                var logs4 = new
                {
                    Result = this.Result,
                    SystemError = this.SystemError,
                    ErrorCode = this.ErrorCode,
                    ErrorMessage = this.ErrorMessage
                };

                log += Newtonsoft.Json.JsonConvert.SerializeObject(logs4) + "\r\n";

                var logdir = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute("~/Log"));
                System.IO.File.WriteAllText(System.IO.Path.Combine(logdir, strOrderID) + ".txt", log);
            }

        }
        public void YapiKredi(PosForm pf)
        {
            Boolean result = false;
            C_Posnet posnetObj = new C_Posnet();

            string log = string.Empty;

            string mid = pf.vPosInfo.ClientID;
            string tid = pf.vPosInfo.UserID;
            Random rnd = new Random();
            string orderid = "1234567890123456789" + rnd.Next(11111, 99999);
            try
            {
                //Random rnd = new Random();
                string ccno = pf.CardNumber.ToString();
                string expdateyear = pf.Year.ToString().Length == 4 ? pf.Year.ToString().Substring(2, 3) : pf.Year.ToString();
                string expdate = expdateyear + pf.Month;
                string cvc = string.Format("{0:000}", pf.CardCvv2);
                //string orderid = "1234567890123456789" + rnd.Next(11111, 99999);
                string amount = pf.Amount.ToString("#.00").Replace(".", "").Replace(",", "");
                string currencycode = pf.vPosInfo.BankCur;
                string instnumber = pf.Instalment.ToString();
                var logs = new
                {
                    ccno = ccno,
                    expdateyear = expdateyear,
                    expdate = expdate,
                    cvc = cvc,
                    amount = amount,
                    currencycode = currencycode,
                    instnumber = instnumber
                };

                log += Newtonsoft.Json.JsonConvert.SerializeObject(logs) + "\r\n";

                posnetObj.SetURL(pf.vPosInfo.Host);
                posnetObj.SetMid(mid);
                posnetObj.SetTid(tid);

                var logs1 = new
                {
                    Host = pf.vPosInfo.Host,
                    mid = mid,
                    tid = tid
                };

                log += Newtonsoft.Json.JsonConvert.SerializeObject(logs1) + "\r\n";

                result = posnetObj.DoSaleTran(ccno, expdate, cvc, orderid, amount, currencycode, instnumber, "", "");

                if (pf.Instalment > 0) { posnetObj.SetKOICode(pf.Instalment.ToString()); }

                if (posnetObj.GetApprovedCode() == "1")
                {
                    this.Result = true;
                    this.Code = posnetObj.GetAuthcode();
                    this.RefNo = posnetObj.GetHostlogkey();
                    this.BankResult = posnetObj;
                    var logs2 = new
                    {
                        Result = true,
                        Code = Code,
                        RefNo = RefNo
                    };

                    log += Newtonsoft.Json.JsonConvert.SerializeObject(logs2) + "\r\n";
                }
                else
                {
                    this.Result = false;
                    this.ErrorMessage = posnetObj.GetResponseText();
                    this.BankResult = posnetObj;
                    var logs3 = new
                    {
                        Result = true,
                        ErrorMessage = ErrorMessage
                    };

                    log += Newtonsoft.Json.JsonConvert.SerializeObject(logs3) + "\r\n";
                }
                var logdir = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute("~/Log"));
                System.IO.File.WriteAllText(System.IO.Path.Combine(logdir, orderid) + ".txt", log);
            }
            catch (Exception Ex)
            {
                this.Result = false;
                this.ErrorMessage = Ex.Message;
                //this.SystemError;
                this.BankResult = posnetObj;

                var logs = new { Result = false, ErrorMessage = Ex.Message, BankResult = posnetObj };
                log += Newtonsoft.Json.JsonConvert.SerializeObject(logs) + "\r\n";
                var logdir = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute("~/Log"));
                System.IO.File.WriteAllText(System.IO.Path.Combine(logdir, orderid) + ".txt", log);
            }
        }
        public void VakifBank(PosForm pf)
        {
            // Banka bilgileri.

            string kullanici = pf.vPosInfo.ClientID;
            string sifre = pf.vPosInfo.Password;
            string uyeno = pf.vPosInfo.UserID;
            string posno = pf.vPosInfo.PosVersion;
            string xcip = pf.vPosInfo.StoreKey;

            try
            {
                byte[] b = new byte[5000];
                string sonuc;
                System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("ISO-8859-9");

                String tutarcevir = pf.Amount.ToString();
                tutarcevir = tutarcevir.Replace(".", "");
                tutarcevir = tutarcevir.Replace(",", "");
                tutarcevir = String.Format("{0:0000000000.00}", Convert.ToInt32(tutarcevir)).Replace(",", "");

                string taksitcevir = "";

                if (pf.Amount == -1)
                {
                    taksitcevir = "00";
                }
                else
                {
                    taksitcevir = String.Format("{0:00}", pf.Instalment);
                }

                String yilcevir = pf.Year.ToString();
                yilcevir = yilcevir.Substring(2, 2);

                string aycevir = string.Format("{0:00}", pf.Month);

                string provizyonMesaji = "kullanici=" + kullanici + "&sifre=" + sifre + "&islem=PRO&uyeno=" + uyeno + "&posno=" + posno + "&kkno=" + pf.CardNumber + "&gectar=" + yilcevir + aycevir + "&cvc=" + string.Format("{0:000}", pf.CardCvv2) + "&tutar=" + tutarcevir + "&provno=000000&taksits=" + taksitcevir + "&islemyeri=I&uyeref=" + BITools.RandomNumber() + "&vbref=0&khip=" + BITools.GetIp() + "&xcip=" + xcip;

                b.Initialize();
                b = Encoding.ASCII.GetBytes(provizyonMesaji);

                //WebRequest h1 = (WebRequest)HttpWebRequest.Create("https ://subesiz.vakifbank.com.tr/vpos724v3/?" + provizyonMesaji);
                WebRequest h1 = (WebRequest)HttpWebRequest.Create(pf.vPosInfo.Host + provizyonMesaji);
                h1.Method = "GET";

                WebResponse wr = h1.GetResponse();
                Stream s2 = wr.GetResponseStream();

                byte[] buffer = new byte[10000];
                int len = 0, r = 1;
                while (r > 0)
                {
                    r = s2.Read(buffer, len, 10000 - len);
                    len += r;
                }
                s2.Close();
                sonuc = encoding.GetString(buffer, 0, len).Replace("\r", "").Replace("\n", "");

                String gelenonaykodu, gelenrefkodu;
                XmlNode node = null;
                XmlDocument _msgTemplate = new XmlDocument();
                _msgTemplate.LoadXml(sonuc);
                node = _msgTemplate.SelectSingleNode("//Cevap/Msg/Kod");
                gelenonaykodu = node.InnerText.ToString();

                if (gelenonaykodu == "00")
                {
                    node = _msgTemplate.SelectSingleNode("//Cevap/Msg/ProvNo");
                    gelenrefkodu = node.InnerText.ToString();
                    this.Result = true;
                    this.RefNo = gelenrefkodu;
                }
                else
                {
                    this.Result = false;
                    this.ErrorMessage = "";
                    this.ErrorCode = gelenonaykodu;
                }
            }
            catch (Exception e)
            {
                this.Result = false;
                this.ErrorMessage = this.SystemError;
            }
        }
        public void Akbank(PosForm pf)
        {
            try
            {
                ePayment.cc5payment mycc5pay = new ePayment.cc5payment();

                mycc5pay.host = pf.vPosInfo.Host; //"https ://finanstest.fbwebpos.com/servlet/cc5ApiServer";                
                mycc5pay.name = pf.vPosInfo.UserID;
                mycc5pay.password = pf.vPosInfo.Password;
                mycc5pay.clientid = pf.vPosInfo.ClientID;
                mycc5pay.orderresult = 0;
                mycc5pay.oid = BITools.RandomNumber();
                mycc5pay.currency = pf.vPosInfo.BankCur;
                mycc5pay.chargetype = "Auth";
                //gelenler
                mycc5pay.cardnumber = pf.CardNumber.ToString();
                mycc5pay.expmonth = pf.Month;
                mycc5pay.expyear = pf.Year;
                mycc5pay.cv2 = pf.CardCvv2.ToString();
                mycc5pay.subtotal = pf.Amount.ToString();
                if (pf.Instalment > 1)
                {
                    mycc5pay.taksit = pf.Instalment.ToString();
                }
                mycc5pay.bname = pf.CardHolder;
                mycc5pay.phone = BITools.GetIp();
                string x = mycc5pay.processorder();

                if (x == "1" & mycc5pay.appr == "Approved")
                {
                    this.Result = true;
                    this.GroupId = mycc5pay.groupid;
                    this.Code = mycc5pay.code;
                    this.TransId = mycc5pay.transid;
                    this.RefNo = mycc5pay.refno;
                }
                else
                {
                    this.ErrorMessage = this.SystemError;
                    this.ErrorCode = mycc5pay.errmsg;
                    this.Result = false;
                }
            }
            catch (System.Exception)
            {
                this.ErrorMessage = this.SystemError;
                this.Result = false;
            }
        }
        public void IsBankasi(PosForm pf)
        {
            try
            {
                ePayment.cc5payment mycc5pay = new ePayment.cc5payment();
                mycc5pay.host = pf.vPosInfo.Host; //"https ://finanstest.fbwebpos.com/servlet/cc5ApiServer";                
                mycc5pay.name = pf.vPosInfo.UserID;
                mycc5pay.password = pf.vPosInfo.Password;
                mycc5pay.clientid = pf.vPosInfo.ClientID;
                mycc5pay.orderresult = 0;
                mycc5pay.oid = BITools.RandomNumber();
                mycc5pay.cardnumber = pf.CardNumber.ToString();
                mycc5pay.expmonth = pf.Month.ToString();
                mycc5pay.expyear = pf.Year.ToString();
                mycc5pay.cv2 = pf.CardCvv2.ToString();
                mycc5pay.subtotal = pf.Amount.ToString();
                mycc5pay.currency = pf.vPosInfo.BankCur;
                mycc5pay.chargetype = "Auth";
                if (pf.Instalment > 1)
                {
                    mycc5pay.taksit = pf.Instalment.ToString();
                }
                //fatura bilgileri
                mycc5pay.bname = pf.CardHolder;
                mycc5pay.ip = BITools.GetIp();

                string x = mycc5pay.processorder();

                if (x == "1" & mycc5pay.appr == "Approved")
                {
                    //bankadan geri dönen
                    this.Result = true;
                    this.GroupId = mycc5pay.groupid;
                    this.TransId = mycc5pay.transid;
                    this.Code = mycc5pay.code;
                    this.RefNo = mycc5pay.refno;

                }
                else
                {
                    this.Result = false;
                    this.ErrorMessage = mycc5pay.errmsg;
                    this.ErrorCode = mycc5pay.errmsg;

                }
            }
            catch (Exception)
            {
                this.Result = false;
                this.ErrorMessage = this.SystemError;
            }
        }
        public void FinansBank(PosForm pf)
        {
            try
            {
                ePayment.cc5payment mycc5pay = new ePayment.cc5payment();
                mycc5pay.host = pf.vPosInfo.Host; //"https ://finanstest.fbwebpos.com/servlet/cc5ApiServer";                
                mycc5pay.name = pf.vPosInfo.UserID;
                mycc5pay.password = pf.vPosInfo.Password;
                mycc5pay.clientid = pf.vPosInfo.ClientID;
                mycc5pay.orderresult = 0;
                mycc5pay.oid = BITools.RandomNumber();
                mycc5pay.currency = pf.vPosInfo.BankCur;
                mycc5pay.chargetype = "Auth";
                //gelenler
                mycc5pay.cardnumber = pf.CardNumber.ToString();
                mycc5pay.expmonth = pf.Month;
                mycc5pay.expyear = pf.Year;
                mycc5pay.cv2 = pf.CardCvv2.ToString();
                mycc5pay.subtotal = pf.Amount.ToString();

                if (pf.Instalment > 1)
                {
                    mycc5pay.taksit = pf.Instalment.ToString();
                }

                //yedek bilgiler
                mycc5pay.bname = pf.CardHolder;
                mycc5pay.phone = BITools.GetIp();
                string x = mycc5pay.processorder();
                if (x == "1" & mycc5pay.appr == "Approved")
                {
                    //bankadan geri dönen
                    this.Result = true;
                    this.GroupId = mycc5pay.groupid;
                    this.RefNo = mycc5pay.refno;
                    this.TransId = mycc5pay.transid;
                    this.Code = mycc5pay.code;
                }
                else
                {
                    this.Result = false;
                    this.ErrorCode = mycc5pay.err;
                    this.ErrorMessage = mycc5pay.errmsg;
                }
            }
            catch (System.Exception)
            {
                this.Result = false;
                this.ErrorMessage = this.SystemError;
            }
        }
        public void DenizBank(PosForm pf)
        {
            try
            {
                ePayment.cc5payment mycc5pay = new ePayment.cc5payment();
                mycc5pay.host = pf.vPosInfo.Host; //"https ://finanstest.fbwebpos.com/servlet/cc5ApiServer";                
                mycc5pay.name = pf.vPosInfo.UserID;
                mycc5pay.password = pf.vPosInfo.Password;
                mycc5pay.clientid = pf.vPosInfo.ClientID;
                mycc5pay.orderresult = 0;
                mycc5pay.oid = BITools.RandomNumber();
                mycc5pay.currency = pf.vPosInfo.BankCur;
                mycc5pay.chargetype = "Auth";
                //gelenler
                mycc5pay.cardnumber = pf.CardNumber.ToString();
                mycc5pay.expmonth = pf.Month;
                mycc5pay.expyear = pf.Year;
                mycc5pay.cv2 = pf.CardCvv2.ToString();
                mycc5pay.subtotal = pf.Amount.ToString();

                if (pf.Instalment > 1)
                {
                    mycc5pay.taksit = pf.Instalment.ToString();
                }

                //yedek bilgiler
                mycc5pay.bname = pf.CardHolder;
                mycc5pay.phone = BITools.GetIp();
                string x = mycc5pay.processorder();
                if (x == "1" & mycc5pay.appr == "Approved")
                {
                    //bankadan geri dönen
                    this.Result = true;
                    this.GroupId = mycc5pay.groupid;
                    this.RefNo = mycc5pay.refno;
                    this.TransId = mycc5pay.transid;
                    this.Code = mycc5pay.code;
                }
                else
                {
                    this.Result = false;
                    this.ErrorCode = mycc5pay.err;
                    this.ErrorMessage = mycc5pay.errmsg;
                }
            }
            catch (System.Exception)
            {
                this.ErrorMessage = SystemError;
                this.Result = false;
            }
        }
        #endregion
    }
}
