﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankIntegration
{
    public class BITools
    {
        public static string GetIp()
        {
            if (string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.ServerVariables["remote_addr"]))
            {
                return "127.0.0.1";
            }
            else
            {
                return System.Web.HttpContext.Current.Request.ServerVariables["remote_addr"].ToString();
            }
        }

        public static bool IsNumeric(string value)
        {
            int result;
            return int.TryParse(value, out result);
        }

        public static bool IsBankTcKimlik(string pPIN)
        {
            bool returnValue = false;
            if (pPIN.Length == 11)
            {
                char[] charArray = pPIN.ToCharArray(0, 10);
                int num = 0;

                foreach (char item in charArray)
                {
                    num += int.Parse(item.ToString());
                }
                string numStr = num.ToString();

                if (numStr.Substring(numStr.Length - 1) == pPIN.Substring(10))
                {
                    returnValue = true;
                }
            }
            return returnValue;
        }

        public static string RandomNumber()
        {
            Random r = new Random();
            string strRNumber = r.Next(1, 10000000).ToString() + String.Format("{0:T}", DateTime.Now).Replace(":", string.Empty);
            return strRNumber;
        }
    }
}
