﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace BankIntegration
{
    [Serializable()]
    public class HowFindUs
    {
        public HowFindUs()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }
    }

    public class MNG
    {
        public List<HowFindUs> GetHowFindUs(string Market, string Operator, ref string errorMsg)
        {
            List<HowFindUs> records = new List<HowFindUs>();
            string tsql = string.Empty;
            tsql = @"  Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) 
                       From HowFindUs (NOLOCK)
                       Where 
                            Market=@Market 
                        And (Operator='' or Operator=@Operator)
	                    And Status=1
                       Order By Operator Desc,RecID  
                    ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", System.Data.DbType.AnsiString, Market);
                db.AddInParameter(dbCommand, "Operator", System.Data.DbType.AnsiString, Operator);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HowFindUs record = new HowFindUs();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }        
    }
}
