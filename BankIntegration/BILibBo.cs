﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankIntegration
{
    [Serializable()]
    public class BIKeyValue
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public BIKeyValue()
        {
        }
    }

    [Serializable()]
    public class BIHtmlCodeData
    {
        public BIHtmlCodeData()
        {
            _PdfPage = 1;
            _AfterInsertShow = false;
            _AfterInsertDisabled = false;
        }

        string _IdName;
        public string IdName
        {
            get { return _IdName; }
            set { _IdName = value; }
        }

        string _TagName;
        public string TagName
        {
            get { return _TagName; }
            set { _TagName = value; }
        }

        string _Data;
        public string Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        string _Value;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        int _PdfPage;
        public int PdfPage
        {
            get { return _PdfPage; }
            set { _PdfPage = value; }
        }

        bool _AfterInsertShow;
        public bool AfterInsertShow
        {
            get { return _AfterInsertShow; }
            set { _AfterInsertShow = value; }
        }

        bool _AfterInsertDisabled;
        public bool AfterInsertDisabled
        {
            get { return _AfterInsertDisabled; }
            set { _AfterInsertDisabled = value; }
        }
    }

    [Serializable()]
    public class BIWebConfig
    {
        public BIWebConfig()
        {
        }

        string _FORMNAME;
        public string FORMNAME
        {
            get { return _FORMNAME; }
            set { _FORMNAME = value; }
        }

        string _PARAMETERNAME;
        public string PARAMETERNAME
        {
            get { return _PARAMETERNAME; }
            set { _PARAMETERNAME = value; }
        }

        string _TYPES;
        public string TYPES
        {
            get { return _TYPES; }
            set { _TYPES = value; }
        }

        string _FIELDTYPE;
        public string FIELDTYPE
        {
            get { return _FIELDTYPE; }
            set { _FIELDTYPE = value; }
        }

        string _FIELDVALUE;
        public string FIELDVALUE
        {
            get { return _FIELDVALUE; }
            set { _FIELDVALUE = value; }
        }
    }

    [Serializable()]
    public class BIUser
    {
        public BIUser()
        {
        }
        public string UserID { get; set; }
        public string AgencyID { get; set; }
        public string OprOffice { get; set; }
        public string Operator { get; set; }
        public string Market { get; set; }
        public string MainOffice { get; set; }
        public Int16? AgencyType { get; set; }
        public string Cur { get; set; }
        public byte? WorkType { get; set; }
        public int? Country { get; set; }
        public int? Location { get; set; }
        public bool OwnAgency { get; set; }
        public string EMail { get; set; }
    }

    [Serializable()]
    public class BIPayTypeRecord
    {
        public BIPayTypeRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        Int16? _RateType;
        public Int16? RateType
        {
            get { return _RateType; }
            set { _RateType = value; }
        }

        Int16? _ExChgDate;
        public Int16? ExChgDate
        {
            get { return _ExChgDate; }
            set { _ExChgDate = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankName;
        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }

        string _CanReceiptPrt;
        public string CanReceiptPrt
        {
            get { return _CanReceiptPrt; }
            set { _CanReceiptPrt = value; }
        }

        Int16? _PayCat;
        public Int16? PayCat
        {
            get { return _PayCat; }
            set { _PayCat = value; }
        }

        string _PayCatName;
        public string PayCatName
        {
            get { return _PayCatName; }
            set { _PayCatName = value; }
        }

        int? _OprBankID;
        public int? OprBankID
        {
            get { return _OprBankID; }
            set { _OprBankID = value; }
        }

        DateTime? _PayBegDate;
        public DateTime? PayBegDate
        {
            get { return _PayBegDate; }
            set { _PayBegDate = value; }
        }

        DateTime? _PayEndDate;
        public DateTime? PayEndDate
        {
            get { return _PayEndDate; }
            set { _PayEndDate = value; }
        }

        DateTime? _ResBegDate;
        public DateTime? ResBegDate
        {
            get { return _ResBegDate; }
            set { _ResBegDate = value; }
        }

        DateTime? _ResEndDate;
        public DateTime? ResEndDate
        {
            get { return _ResEndDate; }
            set { _ResEndDate = value; }
        }

        Int16? _MinNight;
        public Int16? MinNight
        {
            get { return _MinNight; }
            set { _MinNight = value; }
        }

        Int16? _MaxNight;
        public Int16? MaxNight
        {
            get { return _MaxNight; }
            set { _MaxNight = value; }
        }

        bool? _HasInstalment;
        public bool? HasInstalment
        {
            get { return _HasInstalment; }
            set { _HasInstalment = value; }
        }

        string _CreditCard;
        public string CreditCard
        {
            get { return _CreditCard; }
            set { _CreditCard = value; }
        }

        Int16? _InstalNumFrom;
        public Int16? InstalNumFrom
        {
            get { return _InstalNumFrom; }
            set { _InstalNumFrom = value; }
        }

        Int16? _InstalNumTo;
        public Int16? InstalNumTo
        {
            get { return _InstalNumTo; }
            set { _InstalNumTo = value; }
        }

        decimal? _DiscountPer;
        public decimal? DiscountPer
        {
            get { return _DiscountPer; }
            set { _DiscountPer = value; }
        }

        decimal? _BankComPer;
        public decimal? BankComPer
        {
            get { return _BankComPer; }
            set { _BankComPer = value; }
        }

        Int16? _PosType;
        public Int16? PosType
        {
            get { return _PosType; }
            set { _PosType = value; }
        }

        DateTime? _EndofDay;
        public DateTime? EndofDay
        {
            get { return _EndofDay; }
            set { _EndofDay = value; }
        }

        Int16? _PeriodofInstalment;
        public Int16? PeriodofInstalment
        {
            get { return _PeriodofInstalment; }
            set { _PeriodofInstalment = value; }
        }

        string _SupDisCode;
        public string SupDisCode
        {
            get { return _SupDisCode; }
            set { _SupDisCode = value; }
        }

        Int16? _ValorDay;
        public Int16? ValorDay
        {
            get { return _ValorDay; }
            set { _ValorDay = value; }
        }

        string _PayCur;
        public string PayCur
        {
            get { return _PayCur; }
            set { _PayCur = value; }
        }

        decimal? _ResPayMinPer;
        public decimal? ResPayMinPer
        {
            get { return _ResPayMinPer; }
            set { _ResPayMinPer = value; }
        }

        string _SD;
        public string SD
        {
            get { return _SD; }
            set { _SD = value; }
        }

        bool? _AllowWIS;
        public bool? AllowWIS
        {
            get { return _AllowWIS; }
            set { _AllowWIS = value; }
        }

        decimal? _ResPayMinVal;
        public decimal? ResPayMinVal
        {
            get { return _ResPayMinVal; }
            set { _ResPayMinVal = value; }
        }

        string _PayPalAPIUserName;
        public string PayPalAPIUserName
        {
            get { return _PayPalAPIUserName; }
            set { _PayPalAPIUserName = value; }
        }

        string _PayPalAPIPwd;
        public string PayPalAPIPwd
        {
            get { return _PayPalAPIPwd; }
            set { _PayPalAPIPwd = value; }
        }

        string _PayPalAPISignature;
        public string PayPalAPISignature
        {
            get { return _PayPalAPISignature; }
            set { _PayPalAPISignature = value; }
        }

        string _PayPalSecureMerchantId;
        public string PayPalSecureMerchantId
        {
            get { return _PayPalSecureMerchantId; }
            set { _PayPalSecureMerchantId = value; }
        }

        string _PayPalMerchantName;
        public string PayPalMerchantName
        {
            get { return _PayPalMerchantName; }
            set { _PayPalMerchantName = value; }
        }

        Int16? _ValidRemDaytoCin;
        public Int16? ValidRemDaytoCin
        {
            get { return _ValidRemDaytoCin; }
            set { _ValidRemDaytoCin = value; }
        }

        bool? _ValidForB2B;
        public bool? ValidForB2B
        {
            get { return _ValidForB2B; }
            set { _ValidForB2B = value; }
        }

        bool? _ValidForB2C;
        public bool? ValidForB2C
        {
            get { return _ValidForB2C; }
            set { _ValidForB2C = value; }
        }

        decimal? _DiscountVal;
        public decimal? DiscountVal
        {
            get { return _DiscountVal; }
            set { _DiscountVal = value; }
        }

        bool? _Secure3d;
        public bool? Secure3d
        {
            get { return _Secure3d; }
            set { _Secure3d = value; }
        }
    }

    [Serializable()]
    public class BIPaymentPageDefination
    {
        public BIPaymentPageDefination()
        {
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _B2BUrl;
        public string B2BUrl
        {
            get { return _B2BUrl; }
            set { _B2BUrl = value; }
        }

        string _B2CUrl;
        public string B2CUrl
        {
            get { return _B2CUrl; }
            set { _B2CUrl = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

    }

    [Serializable()]
    public class BIResMainRecord
    {
        public BIResMainRecord()
        {
        }

        int _RefNo;
        public int RefNo
        {
            get { return _RefNo; }
            set { _RefNo = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _OperatorName;
        public string OperatorName
        {
            get { return _OperatorName; }
            set { _OperatorName = value; }
        }

        string _OperatorNameL;
        public string OperatorNameL
        {
            get { return _OperatorNameL; }
            set { _OperatorNameL = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _MarketName;
        public string MarketName
        {
            get { return _MarketName; }
            set { _MarketName = value; }
        }

        string _MarketNameL;
        public string MarketNameL
        {
            get { return _MarketNameL; }
            set { _MarketNameL = value; }
        }

        string _Office;
        public string Office
        {
            get { return _Office; }
            set { _Office = value; }
        }

        string _OfficeName;
        public string OfficeName
        {
            get { return _OfficeName; }
            set { _OfficeName = value; }
        }

        string _OfficeNameL;
        public string OfficeNameL
        {
            get { return _OfficeNameL; }
            set { _OfficeNameL = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        string _AgencyName;
        public string AgencyName
        {
            get { return _AgencyName; }
            set { _AgencyName = value; }
        }

        string _AgencyNameL;
        public string AgencyNameL
        {
            get { return _AgencyNameL; }
            set { _AgencyNameL = value; }
        }

        string _HolPack;
        public string HolPack
        {
            get { return _HolPack; }
            set { _HolPack = value; }
        }

        string _HolPackName;
        public string HolPackName
        {
            get { return _HolPackName; }
            set { _HolPackName = value; }
        }

        string _HolPackNameL;
        public string HolPackNameL
        {
            get { return _HolPackNameL; }
            set { _HolPackNameL = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _Days;
        public Int16? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }

        Int16? _PriceSource;
        public Int16? PriceSource
        {
            get { return _PriceSource; }
            set { _PriceSource = value; }
        }

        int? _PriceListNo;
        public int? PriceListNo
        {
            get { return _PriceListNo; }
            set { _PriceListNo = value; }
        }

        Int16? _SaleResource;
        public Int16? SaleResource
        {
            get { return _SaleResource; }
            set { _SaleResource = value; }
        }

        Int16? _ResLock;
        public Int16? ResLock
        {
            get { return _ResLock; }
            set { _ResLock = value; }
        }

        Int16? _ResStat;
        public Int16? ResStat
        {
            get { return _ResStat; }
            set { _ResStat = value; }
        }

        DateTime? _ResStatDate;
        public DateTime? ResStatDate
        {
            get { return _ResStatDate; }
            set { _ResStatDate = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        DateTime? _ConfStatDate;
        public DateTime? ConfStatDate
        {
            get { return _ConfStatDate; }
            set { _ConfStatDate = value; }
        }

        int? _CanReason;
        public int? CanReason
        {
            get { return _CanReason; }
            set { _CanReason = value; }
        }

        DateTime? _OptDate;
        public DateTime? OptDate
        {
            get { return _OptDate; }
            set { _OptDate = value; }
        }

        string _AgencyUser;
        public string AgencyUser
        {
            get { return _AgencyUser; }
            set { _AgencyUser = value; }
        }

        string _AgencyUserName;
        public string AgencyUserName
        {
            get { return _AgencyUserName; }
            set { _AgencyUserName = value; }
        }

        string _AgencyUserNameL;
        public string AgencyUserNameL
        {
            get { return _AgencyUserNameL; }
            set { _AgencyUserNameL = value; }
        }

        string _AuthorUser;
        public string AuthorUser
        {
            get { return _AuthorUser; }
            set { _AuthorUser = value; }
        }

        DateTime? _ResDate;
        public DateTime? ResDate
        {
            get { return _ResDate; }
            set { _ResDate = value; }
        }

        DateTime? _ChgDate;
        public DateTime? ChgDate
        {
            get { return _ChgDate; }
            set { _ChgDate = value; }
        }

        string _ChgUser;
        public string ChgUser
        {
            get { return _ChgUser; }
            set { _ChgUser = value; }
        }

        decimal? _NetPrice;
        public decimal? NetPrice
        {
            get { return _NetPrice; }
            set { _NetPrice = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _AgencyCom;
        public decimal? AgencyCom
        {
            get { return _AgencyCom; }
            set { _AgencyCom = value; }
        }

        decimal? _AgencyComPer;
        public decimal? AgencyComPer
        {
            get { return _AgencyComPer; }
            set { _AgencyComPer = value; }
        }

        decimal? _AgencyComSupPer;
        public decimal? AgencyComSupPer
        {
            get { return _AgencyComSupPer; }
            set { _AgencyComSupPer = value; }
        }

        string _Ballon;
        public string Ballon
        {
            get { return _Ballon; }
            set { _Ballon = value; }
        }

        DateTime? _ReceiveDate;
        public DateTime? ReceiveDate
        {
            get { return _ReceiveDate; }
            set { _ReceiveDate = value; }
        }

        DateTime? _RecCrtDate;
        public DateTime? RecCrtDate
        {
            get { return _RecCrtDate; }
            set { _RecCrtDate = value; }
        }

        string _RecCrtUser;
        public string RecCrtUser
        {
            get { return _RecCrtUser; }
            set { _RecCrtUser = value; }
        }

        DateTime? _RecChgDate;
        public DateTime? RecChgDate
        {
            get { return _RecChgDate; }
            set { _RecChgDate = value; }
        }

        string _RecChgUser;
        public string RecChgUser
        {
            get { return _RecChgUser; }
            set { _RecChgUser = value; }
        }

        string _ResNote;
        public string ResNote
        {
            get { return _ResNote; }
            set { _ResNote = value; }
        }

        string _IntNote;
        public string IntNote
        {
            get { return _IntNote; }
            set { _IntNote = value; }
        }

        string _Code1;
        public string Code1
        {
            get { return _Code1; }
            set { _Code1 = value; }
        }

        string _Code2;
        public string Code2
        {
            get { return _Code2; }
            set { _Code2 = value; }
        }

        string _Code3;
        public string Code3
        {
            get { return _Code3; }
            set { _Code3 = value; }
        }

        string _Code4;
        public string Code4
        {
            get { return _Code4; }
            set { _Code4 = value; }
        }

        Int16? _Adult;
        public Int16? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16? _Child;
        public Int16? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        string _ConfToAgency;
        public string ConfToAgency
        {
            get { return _ConfToAgency; }
            set { _ConfToAgency = value; }
        }

        DateTime? _ConfToAgencyDate;
        public DateTime? ConfToAgencyDate
        {
            get { return _ConfToAgencyDate; }
            set { _ConfToAgencyDate = value; }
        }

        decimal? _AgencyPayable;
        public decimal? AgencyPayable
        {
            get { return _AgencyPayable; }
            set { _AgencyPayable = value; }
        }

        DateTime? _PayLastDate;
        public DateTime? PayLastDate
        {
            get { return _PayLastDate; }
            set { _PayLastDate = value; }
        }

        decimal? _AgencyPayment;
        public decimal? AgencyPayment
        {
            get { return _AgencyPayment; }
            set { _AgencyPayment = value; }
        }

        string _PaymentStat;
        public string PaymentStat
        {
            get { return _PaymentStat; }
            set { _PaymentStat = value; }
        }

        decimal? _Discount;
        public decimal? Discount
        {
            get { return _Discount; }
            set { _Discount = value; }
        }

        string _AgencyComMan;
        public string AgencyComMan
        {
            get { return _AgencyComMan; }
            set { _AgencyComMan = value; }
        }

        string _SendInComing;
        public string SendInComing
        {
            get { return _SendInComing; }
            set { _SendInComing = value; }
        }

        DateTime? _SendInComingDate;
        public DateTime? SendInComingDate
        {
            get { return _SendInComingDate; }
            set { _SendInComingDate = value; }
        }

        string _AllDocPrint;
        public string AllDocPrint
        {
            get { return _AllDocPrint; }
            set { _AllDocPrint = value; }
        }

        string _SendAgency;
        public string SendAgency
        {
            get { return _SendAgency; }
            set { _SendAgency = value; }
        }

        DateTime? _SendAgencyDate;
        public DateTime? SendAgencyDate
        {
            get { return _SendAgencyDate; }
            set { _SendAgencyDate = value; }
        }

        string _EB;
        public string EB
        {
            get { return _EB; }
            set { _EB = value; }
        }

        int? _EBSupID;
        public int? EBSupID
        {
            get { return _EBSupID; }
            set { _EBSupID = value; }
        }

        decimal? _EBSupMaxPer;
        public decimal? EBSupMaxPer
        {
            get { return _EBSupMaxPer; }
            set { _EBSupMaxPer = value; }
        }

        int? _EBPasID;
        public int? EBPasID
        {
            get { return _EBPasID; }
            set { _EBPasID = value; }
        }

        decimal? _EBAgencyPer;
        public decimal? EBAgencyPer
        {
            get { return _EBAgencyPer; }
            set { _EBAgencyPer = value; }
        }

        decimal? _EBPasPer;
        public decimal? EBPasPer
        {
            get { return _EBPasPer; }
            set { _EBPasPer = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityNameL;
        public string DepCityNameL
        {
            get { return _DepCityNameL; }
            set { _DepCityNameL = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityNameL;
        public string ArrCityNameL
        {
            get { return _ArrCityNameL; }
            set { _ArrCityNameL = value; }
        }

        string _Credit;
        public string Credit
        {
            get { return _Credit; }
            set { _Credit = value; }
        }

        decimal? _CreditExpense;
        public decimal? CreditExpense
        {
            get { return _CreditExpense; }
            set { _CreditExpense = value; }
        }

        string _CreditExpenseCur;
        public string CreditExpenseCur
        {
            get { return _CreditExpenseCur; }
            set { _CreditExpenseCur = value; }
        }

        string _Broker;
        public string Broker
        {
            get { return _Broker; }
            set { _Broker = value; }
        }

        decimal? _BrokerCom;
        public decimal? BrokerCom
        {
            get { return _BrokerCom; }
            set { _BrokerCom = value; }
        }

        decimal? _BrokerComPer;
        public decimal? BrokerComPer
        {
            get { return _BrokerComPer; }
            set { _BrokerComPer = value; }
        }

        string _PLOperator;
        public string PLOperator
        {
            get { return _PLOperator; }
            set { _PLOperator = value; }
        }

        string _PLOperatorName;
        public string PLOperatorName
        {
            get { return _PLOperatorName; }
            set { _PLOperatorName = value; }
        }

        string _PLOperatorNameL;
        public string PLOperatorNameL
        {
            get { return _PLOperatorNameL; }
            set { _PLOperatorNameL = value; }
        }

        string _PLMarket;
        public string PLMarket
        {
            get { return _PLMarket; }
            set { _PLMarket = value; }
        }

        string _PLMarketName;
        public string PLMarketName
        {
            get { return _PLMarketName; }
            set { _PLMarketName = value; }
        }

        string _PLMarketNameL;
        public string PLMarketNameL
        {
            get { return _PLMarketNameL; }
            set { _PLMarketNameL = value; }
        }

        decimal? _AgencyBonus;
        public decimal? AgencyBonus
        {
            get { return _AgencyBonus; }
            set { _AgencyBonus = value; }
        }

        decimal? _AgencyBonusAmount;
        public decimal? AgencyBonusAmount
        {
            get { return _AgencyBonusAmount; }
            set { _AgencyBonusAmount = value; }
        }

        decimal? _UserBonus;
        public decimal? UserBonus
        {
            get { return _UserBonus; }
            set { _UserBonus = value; }
        }

        decimal? _UserBonusAmount;
        public decimal? UserBonusAmount
        {
            get { return _UserBonusAmount; }
            set { _UserBonusAmount = value; }
        }

        decimal? _PasBonus;
        public decimal? PasBonus
        {
            get { return _PasBonus; }
            set { _PasBonus = value; }
        }

        decimal? _PasBonusAmount;
        public decimal? PasBonusAmount
        {
            get { return _PasBonusAmount; }
            set { _PasBonusAmount = value; }
        }

        byte? _Complete;
        public byte? Complete
        {
            get { return _Complete; }
            set { _Complete = value; }
        }

        byte? _ReadByOpr;
        public byte? ReadByOpr
        {
            get { return _ReadByOpr; }
            set { _ReadByOpr = value; }
        }

        string _ReadByOprUser;
        public string ReadByOprUser
        {
            get { return _ReadByOprUser; }
            set { _ReadByOprUser = value; }
        }

        DateTime? _ReadByOprDate;
        public DateTime? ReadByOprDate
        {
            get { return _ReadByOprDate; }
            set { _ReadByOprDate = value; }
        }

        byte? _ReadByAgency;
        public byte? ReadByAgency
        {
            get { return _ReadByAgency; }
            set { _ReadByAgency = value; }
        }

        string _ReadByAgencyUser;
        public string ReadByAgencyUser
        {
            get { return _ReadByAgencyUser; }
            set { _ReadByAgencyUser = value; }
        }

        DateTime? _ReadByAgencyDate;
        public DateTime? ReadByAgencyDate
        {
            get { return _ReadByAgencyDate; }
            set { _ReadByAgencyDate = value; }
        }

        string _InvoiceIssued;
        public string InvoiceIssued
        {
            get { return _InvoiceIssued; }
            set { _InvoiceIssued = value; }
        }

        DateTime? _InvoiceDate;
        public DateTime? InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }

        decimal? _Balance;
        public decimal? Balance
        {
            get { return _Balance; }
            set { _Balance = value; }
        }

        decimal? _InvoicedAmount;
        public decimal? InvoicedAmount
        {
            get { return _InvoicedAmount; }
            set { _InvoicedAmount = value; }
        }

        decimal? _PasSupDis;
        public decimal? PasSupDis
        {
            get { return _PasSupDis; }
            set { _PasSupDis = value; }
        }

        decimal? _AgencySupDis;
        public decimal? AgencySupDis
        {
            get { return _AgencySupDis; }
            set { _AgencySupDis = value; }
        }

        decimal? _PasPayable;
        public decimal? PasPayable
        {
            get { return _PasPayable; }
            set { _PasPayable = value; }
        }

        decimal? _PasPayment;
        public decimal? PasPayment
        {
            get { return _PasPayment; }
            set { _PasPayment = value; }
        }

        decimal? _PasBalance;
        public decimal? PasBalance
        {
            get { return _PasBalance; }
            set { _PasBalance = value; }
        }

        decimal? _AgencyComSup;
        public decimal? AgencyComSup
        {
            get { return _AgencyComSup; }
            set { _AgencyComSup = value; }
        }

        decimal? _EBAgency;
        public decimal? EBAgency
        {
            get { return _EBAgency; }
            set { _EBAgency = value; }
        }

        decimal? _EBPas;
        public decimal? EBPas
        {
            get { return _EBPas; }
            set { _EBPas = value; }
        }

        decimal? _PasAmount;
        public decimal? PasAmount
        {
            get { return _PasAmount; }
            set { _PasAmount = value; }
        }

        decimal? _Tax;
        public decimal? Tax
        {
            get { return _Tax; }
            set { _Tax = value; }
        }

        decimal? _AgencyComInvAmount;
        public decimal? AgencyComInvAmount
        {
            get { return _AgencyComInvAmount; }
            set { _AgencyComInvAmount = value; }
        }

        string _SendToAcc;
        public string SendToAcc
        {
            get { return _SendToAcc; }
            set { _SendToAcc = value; }
        }

        DateTime? _SendToAccDate;
        public DateTime? SendToAccDate
        {
            get { return _SendToAccDate; }
            set { _SendToAccDate = value; }
        }

        string _SendToAccUser;
        public string SendToAccUser
        {
            get { return _SendToAccUser; }
            set { _SendToAccUser = value; }
        }

        string _AllowDocPrint;
        public string AllowDocPrint
        {
            get { return _AllowDocPrint; }
            set { _AllowDocPrint = value; }
        }

        string _CallFromResSer;
        public string CallFromResSer
        {
            get { return _CallFromResSer; }
            set { _CallFromResSer = value; }
        }

        decimal? _AgencyDisPasPer;
        public decimal? AgencyDisPasPer
        {
            get { return _AgencyDisPasPer; }
            set { _AgencyDisPasPer = value; }
        }

        decimal? _AgencyDisPasVal;
        public decimal? AgencyDisPasVal
        {
            get { return _AgencyDisPasVal; }
            set { _AgencyDisPasVal = value; }
        }

        string _LastUser;
        public string LastUser
        {
            get { return _LastUser; }
            set { _LastUser = value; }
        }

        string _AceCustomerCode;
        public string AceCustomerCode
        {
            get { return _AceCustomerCode; }
            set { _AceCustomerCode = value; }
        }

        string _AceTfNumber;
        public string AceTfNumber
        {
            get { return _AceTfNumber; }
            set { _AceTfNumber = value; }
        }

        string _SendToAccPL;
        public string SendToAccPL
        {
            get { return _SendToAccPL; }
            set { _SendToAccPL = value; }
        }

        byte? _EBPasChk;
        public byte? EBPasChk
        {
            get { return _EBPasChk; }
            set { _EBPasChk = value; }
        }

        byte? _EBAgencyChk;
        public byte? EBAgencyChk
        {
            get { return _EBAgencyChk; }
            set { _EBAgencyChk = value; }
        }

        string _WebIP;
        public string WebIP
        {
            get { return _WebIP; }
            set { _WebIP = value; }
        }

        int? _HowFindUs;
        public int? HowFindUs
        {
            get { return _HowFindUs; }
            set { _HowFindUs = value; }
        }

        DateTime? _EBPasDueDate;
        public DateTime? EBPasDueDate
        {
            get { return _EBPasDueDate; }
            set { _EBPasDueDate = value; }
        }

        decimal? _EBPasDuePer;
        public decimal? EBPasDuePer
        {
            get { return _EBPasDuePer; }
            set { _EBPasDuePer = value; }
        }

        DateTime? _EBPasDueDate2;
        public DateTime? EBPasDueDate2
        {
            get { return _EBPasDueDate2; }
            set { _EBPasDueDate2 = value; }
        }

        decimal? _EBPasDuePer2;
        public decimal? EBPasDuePer2
        {
            get { return _EBPasDuePer2; }
            set { _EBPasDuePer2 = value; }
        }

        byte? _PayPlanMode;
        public byte? PayPlanMode
        {
            get { return _PayPlanMode; }
            set { _PayPlanMode = value; }
        }

        decimal? _PasDiscBase;
        public decimal? PasDiscBase
        {
            get { return _PasDiscBase; }
            set { _PasDiscBase = value; }
        }

        decimal? _AgencyDiscBase;
        public decimal? AgencyDiscBase
        {
            get { return _AgencyDiscBase; }
            set { _AgencyDiscBase = value; }
        }

        byte? _InvoiceTo;
        public byte? InvoiceTo
        {
            get { return _InvoiceTo; }
            set { _InvoiceTo = value; }
        }

        decimal? _AgencyComPayAmount;
        public decimal? AgencyComPayAmount
        {
            get { return _AgencyComPayAmount; }
            set { _AgencyComPayAmount = value; }
        }

        byte? _AgencyEarnBonus;
        public byte? AgencyEarnBonus
        {
            get { return _AgencyEarnBonus; }
            set { _AgencyEarnBonus = value; }
        }

        byte? _UserEarnBonus;
        public byte? UserEarnBonus
        {
            get { return _UserEarnBonus; }
            set { _UserEarnBonus = value; }
        }

        byte? _PasEarnBonus;
        public byte? PasEarnBonus
        {
            get { return _PasEarnBonus; }
            set { _PasEarnBonus = value; }
        }

        bool? _AllowParent;
        public bool? AllowParent
        {
            get { return _AllowParent; }
            set { _AllowParent = value; }
        }

        string _PackType;
        public string PackType
        {
            get { return _PackType; }
            set { _PackType = value; }
        }

        Int16? _Pax;
        public Int16? Pax
        {
            get { return _Pax; }
            set { _Pax = value; }
        }

        byte? _PasPayRule;
        public byte? PasPayRule
        {
            get { return _PasPayRule; }
            set { _PasPayRule = value; }
        }

        bool? _HasDrReport;
        public bool? HasDrReport
        {
            get { return _HasDrReport; }
            set { _HasDrReport = value; }
        }

        decimal? _OldResPrice;
        public decimal? OldResPrice
        {
            get { return _OldResPrice; }
            set { _OldResPrice = value; }
        }

        bool? _HoneyMoonRes;
        public bool? HoneyMoonRes
        {
            get { return _HoneyMoonRes; }
            set { _HoneyMoonRes = value; }
        }

        bool? _SendToAce;
        public bool? SendToAce
        {
            get { return _SendToAce; }
            set { _SendToAce = value; }
        }

        DateTime? _SendToAceDate;
        public DateTime? SendToAceDate
        {
            get { return _SendToAceDate; }
            set { _SendToAceDate = value; }
        }

        string _MerlinxID;
        public string MerlinxID
        {
            get { return _MerlinxID; }
            set { _MerlinxID = value; }
        }

        string _SendTo3Pgm;
        public string SendTo3Pgm
        {
            get { return _SendTo3Pgm; }
            set { _SendTo3Pgm = value; }
        }

        DateTime? _SendTo3PgmDate;
        public DateTime? SendTo3PgmDate
        {
            get { return _SendTo3PgmDate; }
            set { _SendTo3PgmDate = value; }
        }

        DateTime? _ResTime;
        public DateTime? ResTime
        {
            get { return _ResTime; }
            set { _ResTime = value; }
        }

        string _PromoCode;
        public string PromoCode
        {
            get { return _PromoCode; }
            set { _PromoCode = value; }
        }

        decimal? _PromoAmount;
        public decimal? PromoAmount
        {
            get { return _PromoAmount; }
            set { _PromoAmount = value; }
        }

        bool? _ContractIssued;
        public bool? ContractIssued
        {
            get { return _ContractIssued; }
            set { _ContractIssued = value; }
        }

        DateTime? _ContractDate;
        public DateTime? ContractDate
        {
            get { return _ContractDate; }
            set { _ContractDate = value; }
        }

        Int16? _CalcErrNo;
        public Int16? CalcErrNo
        {
            get { return _CalcErrNo; }
            set { _CalcErrNo = value; }
        }

        bool? _SaleComp;
        public bool? SaleComp
        {
            get { return _SaleComp; }
            set { _SaleComp = value; }
        }

        decimal? _PLSpoVal;
        public decimal? PLSpoVal
        {
            get { return _PLSpoVal; }
            set { _PLSpoVal = value; }
        }

        byte? _PLSpoChk;
        public byte? PLSpoChk
        {
            get { return _PLSpoChk; }
            set { _PLSpoChk = value; }
        }

        decimal? _PackedPrice;
        public decimal? PackedPrice
        {
            get { return _PackedPrice; }
            set { _PackedPrice = value; }
        }

        decimal? _OutOfPackedPrice;
        public decimal? OutOfPackedPrice
        {
            get { return _OutOfPackedPrice; }
            set { _OutOfPackedPrice = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

    }

    [Serializable()]
    public class BIResCustRecord
    {
        public BIResCustRecord()
        {
            _Status = 0;
            _MemTable = false;
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        Int32? _CustNoT;
        public Int32? CustNoT
        {
            get { return _CustNoT; }
            set { _CustNoT = value; }
        }

        Int32 _CustNo;
        public Int32 CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _Title;
        public Int16? Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        string _TitleStr;
        public string TitleStr
        {
            get { return _TitleStr; }
            set { _TitleStr = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _SurnameL;
        public string SurnameL
        {
            get { return _SurnameL; }
            set { _SurnameL = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        DateTime? _Birtday;
        public DateTime? Birtday
        {
            get { return _Birtday; }
            set { _Birtday = value; }
        }

        Int16? _Age;
        public Int16? Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        string _PassSerie;
        public string PassSerie
        {
            get { return _PassSerie; }
            set { _PassSerie = value; }
        }

        string _PassNo;
        public string PassNo
        {
            get { return _PassNo; }
            set { _PassNo = value; }
        }

        DateTime? _PassIssueDate;
        public DateTime? PassIssueDate
        {
            get { return _PassIssueDate; }
            set { _PassIssueDate = value; }
        }

        DateTime? _PassExpDate;
        public DateTime? PassExpDate
        {
            get { return _PassExpDate; }
            set { _PassExpDate = value; }
        }

        string _PassGiven;
        public string PassGiven
        {
            get { return _PassGiven; }
            set { _PassGiven = value; }
        }

        string _IDNo;
        public string IDNo
        {
            get { return _IDNo; }
            set { _IDNo = value; }
        }

        int? _Nation;
        public int? Nation
        {
            get { return _Nation; }
            set { _Nation = value; }
        }

        string _NationName;
        public string NationName
        {
            get { return _NationName; }
            set { _NationName = value; }
        }

        string _NationNameL;
        public string NationNameL
        {
            get { return _NationNameL; }
            set { _NationNameL = value; }
        }

        string _Leader;
        public string Leader
        {
            get { return _Leader; }
            set { _Leader = value; }
        }

        byte? _DocVoucher;
        public byte? DocVoucher
        {
            get { return _DocVoucher; }
            set { _DocVoucher = value; }
        }

        byte? _DocTicket;
        public byte? DocTicket
        {
            get { return _DocTicket; }
            set { _DocTicket = value; }
        }

        byte? _DocInsur;
        public byte? DocInsur
        {
            get { return _DocInsur; }
            set { _DocInsur = value; }
        }

        decimal? _Bonus;
        public decimal? Bonus
        {
            get { return _Bonus; }
            set { _Bonus = value; }
        }

        decimal? _BonusAmount;
        public decimal? BonusAmount
        {
            get { return _BonusAmount; }
            set { _BonusAmount = value; }
        }

        bool? _HasPassport;
        public bool? HasPassport
        {
            get { return _HasPassport; }
            set { _HasPassport = value; }
        }

        byte? _Status;
        public byte? Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        decimal? _ppSalePrice;
        public decimal? ppSalePrice
        {
            get { return _ppSalePrice; }
            set { _ppSalePrice = value; }
        }

        decimal? _ppPromoAmount;
        public decimal? ppPromoAmount
        {
            get { return _ppPromoAmount; }
            set { _ppPromoAmount = value; }
        }

        decimal? _ppSupDisAmount;
        public decimal? ppSupDisAmount
        {
            get { return _ppSupDisAmount; }
            set { _ppSupDisAmount = value; }
        }

        decimal? _ppPasPayable;
        public decimal? ppPasPayable
        {
            get { return _ppPasPayable; }
            set { _ppPasPayable = value; }
        }

        decimal? _ppBaseCanAmount;
        public decimal? ppBaseCanAmount
        {
            get { return _ppBaseCanAmount; }
            set { _ppBaseCanAmount = value; }
        }

        decimal? _ppPasEB;
        public decimal? PpPasEB
        {
            get { return _ppPasEB; }
            set { _ppPasEB = value; }
        }

        decimal? _ppPLSpoVal;
        public decimal? PpPLSpoVal
        {
            get { return _ppPLSpoVal; }
            set { _ppPLSpoVal = value; }
        }

        int? _BookID;
        public int? BookID
        {
            get { return _BookID; }
            set { _BookID = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        int? _ParentID;
        public int? ParentID
        {
            get { return _ParentID; }
            set { _ParentID = value; }
        }

        string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

    }

    [Serializable()]
    public class BIResCustInfoRecord
    {
        public BIResCustInfoRecord()
        {
            _InvoiceAddr = "H";
            _ContactAddr = "H";
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _CTitle;
        public Int16? CTitle
        {
            get { return _CTitle; }
            set { _CTitle = value; }
        }

        string _CTitleName;
        public string CTitleName
        {
            get { return _CTitleName; }
            set { _CTitleName = value; }
        }

        string _CName;
        public string CName
        {
            get { return _CName; }
            set { _CName = value; }
        }

        string _CSurName;
        public string CSurName
        {
            get { return _CSurName; }
            set { _CSurName = value; }
        }

        string _AddrHome;
        public string AddrHome
        {
            get { return _AddrHome; }
            set { _AddrHome = value; }
        }

        string _AddrHomeCity;
        public string AddrHomeCity
        {
            get { return _AddrHomeCity; }
            set { _AddrHomeCity = value; }
        }

        string _AddrHomeZip;
        public string AddrHomeZip
        {
            get { return _AddrHomeZip; }
            set { _AddrHomeZip = value; }
        }

        string _AddrHomeTel;
        public string AddrHomeTel
        {
            get { return _AddrHomeTel; }
            set { _AddrHomeTel = value; }
        }

        string _AddrHomeFax;
        public string AddrHomeFax
        {
            get { return _AddrHomeFax; }
            set { _AddrHomeFax = value; }
        }

        string _AddrHomeEmail;
        public string AddrHomeEmail
        {
            get { return _AddrHomeEmail; }
            set { _AddrHomeEmail = value; }
        }

        string _AddrHomeCountry;
        public string AddrHomeCountry
        {
            get { return _AddrHomeCountry; }
            set { _AddrHomeCountry = value; }
        }

        string _AddrHomeCountryCode;
        public string AddrHomeCountryCode
        {
            get { return _AddrHomeCountryCode; }
            set { _AddrHomeCountryCode = value; }
        }

        string _HomeTaxOffice;
        public string HomeTaxOffice
        {
            get { return _HomeTaxOffice; }
            set { _HomeTaxOffice = value; }
        }

        string _HomeTaxAccNo;
        public string HomeTaxAccNo
        {
            get { return _HomeTaxAccNo; }
            set { _HomeTaxAccNo = value; }
        }

        string _AddrWork;
        public string AddrWork
        {
            get { return _AddrWork; }
            set { _AddrWork = value; }
        }

        string _AddrWorkCity;
        public string AddrWorkCity
        {
            get { return _AddrWorkCity; }
            set { _AddrWorkCity = value; }
        }

        string _AddrWorkZip;
        public string AddrWorkZip
        {
            get { return _AddrWorkZip; }
            set { _AddrWorkZip = value; }
        }

        string _AddrWorkTel;
        public string AddrWorkTel
        {
            get { return _AddrWorkTel; }
            set { _AddrWorkTel = value; }
        }

        string _AddrWorkFax;
        public string AddrWorkFax
        {
            get { return _AddrWorkFax; }
            set { _AddrWorkFax = value; }
        }

        string _AddrWorkEMail;
        public string AddrWorkEMail
        {
            get { return _AddrWorkEMail; }
            set { _AddrWorkEMail = value; }
        }

        string _AddrWorkCountry;
        public string AddrWorkCountry
        {
            get { return _AddrWorkCountry; }
            set { _AddrWorkCountry = value; }
        }

        string _AddrWorkCountryCode;
        public string AddrWorkCountryCode
        {
            get { return _AddrWorkCountryCode; }
            set { _AddrWorkCountryCode = value; }
        }

        string _WorkTaxOffice;
        public string WorkTaxOffice
        {
            get { return _WorkTaxOffice; }
            set { _WorkTaxOffice = value; }
        }

        string _WorkTaxAccNo;
        public string WorkTaxAccNo
        {
            get { return _WorkTaxAccNo; }
            set { _WorkTaxAccNo = value; }
        }

        string _ContactAddr;
        public string ContactAddr
        {
            get { return _ContactAddr; }
            set { _ContactAddr = value; }
        }

        string _MobTel;
        public string MobTel
        {
            get { return _MobTel; }
            set { _MobTel = value; }
        }

        string _Jobs;
        public string Jobs
        {
            get { return _Jobs; }
            set { _Jobs = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        string _InvoiceAddr;
        public string InvoiceAddr
        {
            get { return _InvoiceAddr; }
            set { _InvoiceAddr = value; }
        }

        string _WorkFirmName;
        public string WorkFirmName
        {
            get { return _WorkFirmName; }
            set { _WorkFirmName = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankAccNo;
        public string BankAccNo
        {
            get { return _BankAccNo; }
            set { _BankAccNo = value; }
        }

        string _BankIBAN;
        public string BankIBAN
        {
            get { return _BankIBAN; }
            set { _BankIBAN = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }
    }

    [Serializable()]
    public class BIBankVPosRecord
    {
        public BIBankVPosRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        string _Host;
        public string Host
        {
            get { return _Host; }
            set { _Host = value; }
        }

        string _ClientID;
        public string ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }

        string _UserID;
        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        string _Password;
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        string _BankCur;
        public string BankCur
        {
            get { return _BankCur; }
            set { _BankCur = value; }
        }

        string _Interface;
        public string Interface
        {
            get { return _Interface; }
            set { _Interface = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _ProvUserID;
        public string ProvUserID
        {
            get { return _ProvUserID; }
            set { _ProvUserID = value; }
        }

        string _ProvUserPass;
        public string ProvUserPass
        {
            get { return _ProvUserPass; }
            set { _ProvUserPass = value; }
        }

        string _MerchantID;
        public string MerchantID
        {
            get { return _MerchantID; }
            set { _MerchantID = value; }
        }

        string _PosVersion;
        public string PosVersion
        {
            get { return _PosVersion; }
            set { _PosVersion = value; }
        }

        string _SuccessUrlB2B;
        public string SuccessUrlB2B
        {
            get { return _SuccessUrlB2B; }
            set { _SuccessUrlB2B = value; }
        }

        string _SuccessUrlB2C;
        public string SuccessUrlB2C
        {
            get { return _SuccessUrlB2C; }
            set { _SuccessUrlB2C = value; }
        }

        string _ErrorUrlB2B;
        public string ErrorUrlB2B
        {
            get { return _ErrorUrlB2B; }
            set { _ErrorUrlB2B = value; }
        }

        string _ErrorUrlB2C;
        public string ErrorUrlB2C
        {
            get { return _ErrorUrlB2C; }
            set { _ErrorUrlB2C = value; }
        }

        string _StoreKey;
        public string StoreKey
        {
            get { return _StoreKey; }
            set { _StoreKey = value; }
        }
    }

    [Serializable()]
    public class BIResPayPlanRecord
    {
        public BIResPayPlanRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int16? _PayNo;
        public Int16? PayNo
        {
            get { return _PayNo; }
            set { _PayNo = value; }
        }

        DateTime? _DueDate;
        public DateTime? DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        bool? _isEB;
        public bool? IsEB
        {
            get { return _isEB; }
            set { _isEB = value; }
        }

        byte? _Status;
        public byte? Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        byte? _PlanType;
        public byte? PlanType
        {
            get { return _PlanType; }
            set { _PlanType = value; }
        }

        decimal? _Adult;
        public decimal? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        decimal? _Child;
        public decimal? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        decimal? _Infant;
        public decimal? Infant
        {
            get { return _Infant; }
            set { _Infant = value; }
        }
    }

    [Serializable()]
    public class BIPaymentInfo
    {
        public string ResNo { get; set; }
        public int? PriceListNo { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? ResAmount { get; set; }
        public decimal? PasAmount { get; set; }
        public decimal? SalePrice { get; set; }
        public DateTime? OptionTill { get; set; }
        public Int16? ConfStat { get; set; }
        public string SaleCur { get; set; }
        public int? CustNo { get; set; }
        public DateTime? ResBegDate { get; set; }
        public Int16? Night { get; set; }
        public decimal? PrePayment { get; set; }
        public decimal? PrePerVal { get; set; }
        public decimal? Remain { get; set; }
        public Int16? InstalNum { get; set; }
        public byte? PasPayRule { get; set; }
        public BIPaymentInfo()
        {
        }
    }

    [Serializable()]
    public class BIPaymentTypeData
    {
        public int? PayTypeID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int? PayCatID { get; set; }
        public Int16? PosType { get; set; }
        public string PayCatName { get; set; }
        public bool? HasInstalment { get; set; }
        public Int16 InstalNumFrom { get; set; }
        public Int16 InstalNumTo { get; set; }
        public int? CreditCardID { get; set; }
        public string Bank { get; set; }
        public string PayCur { get; set; }
        public string SupDisCode { get; set; }
        public decimal? DiscountPer { get; set; }
        public string CreditCardName { get; set; }
        public string CreditCardCode { get; set; }
        public decimal? ResPayMinPer { get; set; }
        public string WPosBank { get; set; }
        public bool isPicture { get; set; }
        public BIPaymentTypeData()
        {
        }
    }

    [Serializable()]
    public class BICreditCardAndDetail
    {
        public List<BICardDetail> CardDetailList { get; set; }
        public List<BICards> CardList { get; set; }
        public BICreditCardAndDetail()
        {
            this.CardDetailList = new List<BICardDetail>();
            this.CardList = new List<BICards>();
        }
    }

    [Serializable()]
    public class BICardDetail
    {
        public int? PayTypeID { get; set; }
        public int? PayCatID { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public int? CreditCardID { get; set; }
        public string Bank { get; set; }
        public int? InstalNum { get; set; }
        public decimal? DiscountPrice { get; set; }
        public string Currency { get; set; }
        public string SupDisCode { get; set; }
        public string WPosBank { get; set; }
        public decimal? MinPayPrice { get; set; }
        public int? PosType { get; set; }
        public bool Selected { get; set; }
        public decimal? PrePayment { get; set; }
        public BICardDetail()
        {
            this.Selected = false;
        }
    }

    [Serializable()]
    public class BICards
    {
        public int? CreditCardID { get; set; }
        public string CreditCardName { get; set; }
        public BICards()
        {
        }
    }

    [Serializable()]
    public class BIPaymentFormData
    {
        public bool isOK { get; set; }
        public bool Error { get; set; }
        public string ErrMessage { get; set; }
        public bool OwnAgency { get; set; }
        public bool DisabledPaidAmount { get; set; }
        public bool VisablePosType { get; set; }
        public int? PosType { get; set; }
        public bool UsePaymentRule { get; set; }
        public int? PaymentRule { get; set; }
        public IEnumerable AccountData { get; set; }
        public List<BIKeyValue> howFindUs { get; set; }
        public IEnumerable paymentTypeData { get; set; }
        public BICreditCardAndDetail paymentData { get; set; }
        public IEnumerable bankVPos { get; set; }
        public bool visablePerDesc { get; set; }
        public bool visableHowFindUs { get; set; }
        public decimal PaymentAmount { get; set; }
        public string PaymentAmountCurr { get; set; }
        public string PaymentTotalStr { get; set; }
        public string ResNo { get; set; }
        public BIPaymentFormData()
        {
            this.isOK = false;
            this.Error = false;
            this.OwnAgency = false;
            this.UsePaymentRule = false;
            this.AccountData = new List<BIKeyValue>();
            this.howFindUs = new List<BIKeyValue>();
            this.paymentTypeData = new List<BIPaymentTypeData>();
            this.paymentData = new BICreditCardAndDetail();
            this.bankVPos = new List<BIBankVPosRecord>();
            this.visablePerDesc = false;
            this.visableHowFindUs = true;
        }
    }

    [Serializable()]
    public class BIPaymentDetail
    {
        public string PaymentTypeCode { get; set; }
        public string PaymentType { get; set; }
        public string Total { get; set; }
        public string Paid { get; set; }
        public string Payable { get; set; }
        public string MinPayable { get; set; }
        public string PaidAmount { get; set; }
        public string PaidAmountCur { get; set; }
        public string RefeRans { get; set; }
        public BIPaymentDetail()
        {
        }
    }

    public class PosForm
    {
        public string OrderId { get; set; }
        public string CardHolder { get; set; }
        public string CardholderPresentCode { get; set; }
        public Secure3D Secure3D { get; set; }
        public long CardNumber { get; set; }
        public string CardCvv2 { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public decimal Amount { get; set; }
        public int Instalment { get; set; }
        public string CustomerEmail { get; set; }
        public BIBankVPosRecord vPosInfo { get; set; }
        public object BankResult { get; set; }
        public PosForm()
        {
            this.vPosInfo = new BIBankVPosRecord();
        }
    }
    public class Secure3D
    {
        public string AuthenticationCode { get; set; }
        public string SecurityLevel { get; set; }
        public string TxnID { get; set; }
        public string Md { get; set; }
    }
}
