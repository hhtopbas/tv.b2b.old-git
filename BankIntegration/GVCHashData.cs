﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;


public class GVCHashData
{
    //readOnly
    public string Type { get { return "sales"; } }
    public string ApiVersion { get { return "v0.01"; } }
    public string TerminalProvUserID { get { return "PROVOOS"; } } 
    public string Mode { get { return "PROD"; } }
    public string Rnd { get; set; }
    //
    public string OrderId { get; set; } //siparis numarasi (resNo)
    public string TerminalId { get; set; }// from db
    public string ProvisionPassword { get; set; } // from db 
    public string Amount { get; set; } //siparis tutari
    public string HashData { get; set; }
    public string SecurityData { get; set; }
    public string InstallmentCount { get; set; } //taksit. bos gelirse taksitsiz.
    public string StoreKey { get; set; } // from db
    public string TerminalMerchantId { get; set; }// from db //Üye İşyeri Numarası
    public string TerminalUserId { get; set; }// from db
    public string CustomerIpAddress { get; set; }
    public string CustomerEmailAddress { get; set; }// from db
    public string SuccessURL { get; set; }
    public string ErrorURL { get; set; }
    public string Lang { get; set; }
    public string MotoInd { get; set; }
    public string TxnTimeStamp { get; set; }
    public string RefreshTime { get; set; }

    public GVCHashData()
    {
        this.Rnd = DateTime.Now.ToString();
    }

    public GVCHashData(string orderId, string terminalId, string provisionPassword, string amount, string baseUrl, string errorUrl, 
                       string installmentCount, string storeKey, string customerIpAddress, string customerEmailAddress, string terminalMerchantId,
                       string terminalUserId, string lang, string motoInd, string txnTimeStamp, string refreshTime)
    {
        this.OrderId = orderId;        
        this.TerminalId = terminalId;
        this.ProvisionPassword = provisionPassword;
        this.Amount = amount; //amount 175,86 -> 17586 olmali. 86 kurus u ifade eder.  (iki kutu 175  ve 86 -> 17500+86 seklinde gonder)        
        this.InstallmentCount = installmentCount;
        this.StoreKey = storeKey;
        this.CustomerIpAddress = customerIpAddress;
        this.CustomerEmailAddress = customerEmailAddress;
        this.SuccessURL = baseUrl;
        this.ErrorURL = errorUrl;
        this.TerminalMerchantId = terminalMerchantId;
        this.TerminalUserId = terminalUserId;
        this.Lang = lang;
        this.MotoInd = motoInd;
        this.TxnTimeStamp = txnTimeStamp;
        this.RefreshTime = refreshTime;
        this.Rnd = DateTime.Now.ToString();
        string SecurityData = GetSHA1(ProvisionPassword + PrepareTerminalId(TerminalId)).ToUpper();
        this.HashData = GetSHA1(TerminalId + OrderId + Amount + SuccessURL + ErrorURL + Type + InstallmentCount + StoreKey + SecurityData).ToUpper();
    }

    public string PrepareTerminalId(string inTerminalId) // 9 digit e tamamlanir
    {
        var outTerminalId = inTerminalId;
        var diffCount = 9 - inTerminalId.Length;
        for (int i = 0; i < diffCount; i++)
        {
            outTerminalId = "0" + inTerminalId;
        }
        return outTerminalId;
    }

    public string GetSHA1(string SHA1Data)
    {
        SHA1 sha = new SHA1CryptoServiceProvider();
        string HashedPassword = SHA1Data;
        byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
        byte[] inputbytes = sha.ComputeHash(hashbytes);
        return GetHexaDecimal(inputbytes);
    }

    public string GetHexaDecimal(byte[] bytes)
    {
        StringBuilder s = new StringBuilder();
        int length = bytes.Length;
        for (int n = 0; n <= length - 1; n++)
        {
            s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
        }
        return s.ToString();
    }

    public int GetBankCurrency(string cur)
    {
        int rValue;
        switch (cur.ToUpper())
        {
            case "TRY":
                rValue = 949;
                break;
            case "USD":
                rValue = 840;
                break;
            case "EURO":
                rValue = 978;
                break;
            case "GBP":
                rValue = 826;
                break;
            case "JPY":
                rValue = 392;
                break;
            default:
                rValue = 949;
                break;
        }

        return rValue;
    }
}