﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TvTools;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Web;
using System.Globalization;
using System.Security.Cryptography;
using System.Configuration;

namespace BankIntegration
{
    public class BILib
    {
        public static CultureInfo getCultureInfo()
        {
            String[] userLang = HttpContext.Current.Request.UserLanguages;
            String[] CI = userLang[0].Split(';');
            string CultureStr = CI[0];
            if (Equals(CultureStr, "ar"))
            {
                string defaultCulture = Conversion.getStrOrNull(ConfigurationManager.AppSettings["DefaultCulture"]);
                CultureStr = string.IsNullOrEmpty(defaultCulture) ? "en-US" : (defaultCulture.IndexOf(CultureStr) > -1 ? defaultCulture : "en-US");
            }
            if (CultureStr.Length < 5)
            {
                CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                              where culinf.Name == CultureStr
                              select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
            }
            CultureInfo ci = new CultureInfo(CultureStr);
            return ci;
        }

        public object fieldValues(string FieldType, string FieldValue)
        {
            object retVal = null;
            switch (FieldType.ToLower())
            {
                case "string": retVal = FieldValue; break;
                case "bool": retVal = Convert.ToBoolean(FieldValue.ToLower()); break;
                case "short": retVal = Convert.ToInt16(FieldValue); break;
                case "int": retVal = Convert.ToInt32(FieldValue); break;
                case "long": retVal = Convert.ToInt64(FieldValue); break;
                case "double": retVal = Convert.ToDouble(FieldValue); break;
                case "decimal": retVal = Convert.ToDecimal(FieldValue); break;
                case "char": retVal = Convert.ToChar(FieldValue); break;
                case "byte": retVal = Convert.ToByte(FieldValue); break;
                case "datetime": retVal = Convert.ToDateTime(FieldValue); break;
            }
            return retVal;
        }

        public string getWebID()
        {
            string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\Setting.xml";
            System.Data.DataTable webID = new System.Data.DataTable();
            string retVal = string.Empty;

            try
            {
                webID.ReadXml(FilePath);
                retVal = webID.Rows[0]["CONFFOLDER"].ToString();
            }
            catch { }

            return retVal;
        }

        public List<BIWebConfig> getWebConfig()
        {
            List<BIWebConfig> webConfig = new List<BIWebConfig>();

            string xmlPath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + getWebID() + "\\WebConfig.xml";
            System.Data.DataTable webConfigData = new System.Data.DataTable();
            webConfigData.ReadXml(xmlPath);

            foreach (DataRow row in webConfigData.Rows)
            {
                BIWebConfig webConfigRecord = new BIWebConfig();

                webConfigRecord.FORMNAME = Conversion.getStrOrNull(row["FORMNAME"]);
                webConfigRecord.PARAMETERNAME = Conversion.getStrOrNull(row["PARAMETERNAME"]);
                webConfigRecord.TYPES = Conversion.getStrOrNull(row["TYPES"]);
                webConfigRecord.FIELDTYPE = Conversion.getStrOrNull(row["FIELDTYPE"]);
                webConfigRecord.FIELDVALUE = Conversion.getStrOrNull(row["FIELDVALUE"]);

                webConfig.Add(webConfigRecord);
            }

            return webConfig;
        }

        public object getFormConfigValue(string formName, string fieldName)
        {
            List<BIWebConfig> configData = getWebConfig();
            var query = from q in configData.AsEnumerable()
                        where q.FORMNAME == formName && q.PARAMETERNAME == fieldName
                        select new { type = q.FIELDTYPE, value = q.FIELDVALUE };
            if (query.Count() > 0)
                return fieldValues(query.FirstOrDefault().type, query.FirstOrDefault().value);
            else
                return null;
        }

        public decimal? Exchange(string Market, DateTime RateDate, string CurFrom, string CurTo, decimal? aValue, bool BuySale, ref string errorMsg)
        {
            if (aValue == null) return null;
            string tsql = @"usp_Exchange";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetStoredProcCommand(tsql);
            try
            {
                //db.AddOutParameter(dbCommand, "RateDate", DbType.Date, 8);
                db.AddOutParameter(dbCommand, "NewValue", DbType.Double, 30);
                decimal? newValue = 0;
                //db.AddParameter(dbCommand, "NewValue", DbType.Decimal, 18, ParameterDirection.Output, true, 18, 6, "NewValue", DataRowVersion.Current, newValue);
                db.AddOutParameter(dbCommand, "ErrCode", DbType.Int16, 8);

                //db.SetParameterValue(dbCommand, "RateDate", RateDate);

                db.AddInParameter(dbCommand, "RateDate", DbType.DateTime, RateDate);
                db.AddInParameter(dbCommand, "CurFrom", DbType.String, CurFrom);
                db.AddInParameter(dbCommand, "CurTo", DbType.String, CurTo);
                db.AddInParameter(dbCommand, "MainValue", DbType.Decimal, aValue);
                db.AddInParameter(dbCommand, "BuySale", DbType.Boolean, BuySale);
                db.AddInParameter(dbCommand, "RateType", DbType.Int16, -1);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                db.ExecuteNonQuery(dbCommand);

                Int16 errCode = (Int16)db.GetParameterValue(dbCommand, "ErrCode");
                newValue = Conversion.getDecimalOrNull(db.GetParameterValue(dbCommand, "NewValue"));

                if (errCode > 0)
                {
                    errorMsg = errCode.ToString();
                    return null;
                }
                else
                {
                    errorMsg = string.Empty;
                    return Math.Round(newValue.Value, 4);
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BIPaymentPageDefination> getPaymentPageDefination(string Market, ref string errorMsg)
        {
            try
            {
                object _ppd = getFormConfigValue("Payments", "PaymentPageDefination");
                List<BIPaymentPageDefination> ppd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIPaymentPageDefination>>(Conversion.getStrOrNull(_ppd));
                return ppd.Where(w => w.Market == Market).ToList<BIPaymentPageDefination>();
            }
            catch
            {
                return null;
            }
        }

        public BIUser getReservationCreateUserData(string ResNo, ref string errorMsg)
        {
            BIUser UserData = new BIUser();

            string tsql = @"
                            Select AgencyID=A.Code,UserID=U.Code,A.OprOffice,O.Operator,O.Market,
                               MainOffice=(Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End),
                               A.AgencyType,M.Cur,A.WorkType,M.Country,A.Location,OwnAgency=isnull(A.OwnAgency,0),
                               EMail=A.Email1
                            From ResMain (NOLOCK) RM
                            Left Join AgencyUser (NOLOCK) U ON U.Code=RM.AgencyUser And U.Agency=RM.Agency
                            Join Agency (NOLOCK) A ON A.Code=RM.Agency
                            Join Office O (NOLOCK) ON O.Code=A.OprOffice                            
                            Join Market M (NOLOCK) ON M.Code=O.Market
                            WHERE RM.ResNo=@ResNo
                            ";

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        return new BIUser
                        {
                            AgencyID = Conversion.getStrOrNull(R["AgencyID"]),
                            UserID = Conversion.getStrOrNull(R["UserID"]),
                            OprOffice = Conversion.getStrOrNull(R["OprOffice"]),
                            Operator = Conversion.getStrOrNull(R["Operator"]),
                            Market = Conversion.getStrOrNull(R["Market"]),
                            MainOffice = Conversion.getStrOrNull(R["MainOffice"]),
                            AgencyType = Conversion.getInt16OrNull(R["AgencyType"]),
                            Cur = Conversion.getStrOrNull(R["Cur"]),
                            WorkType = Conversion.getByteOrNull(R["WorkType"]),
                            Country = Conversion.getInt32OrNull(R["Country"]),
                            Location = Conversion.getInt32OrNull(R["Location"]),
                            OwnAgency = (bool)R["OwnAgency"],
                            EMail = Conversion.getStrOrNull(R["EMail"])
                        };
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public BIResMainRecord getResMain(BIUser UserData, string ResNo, ref string errorMsg)
        {
            BIResMainRecord record = new BIResMainRecord();
            string tsql = string.Empty;
            tsql = @"Select 
                          R.RefNo, R.ResNo, R.Operator, R.Market, R.BegDate, R.EndDate, R.Days, R.PriceSource, R.PriceListNo, R.SaleResource, R.ResLock, R.ResStat, R.ResStatDate,
                          R.Office, OfficeName=O.Name, OfficeNameL=isnull(dbo.FindLocalName(O.NameLID, @Market), isnull(O.Name, '')),
                          R.HolPack, HolPackName=isnull(H.Name, ''), HolPackNameL=isnull(dbo.FindLocalName(H.NameLID, @Market), isnull(H.Name, '')),
                          R.Agency, AgencyName=isnull(A.Name, ''), AgencyNameL=isnull(dbo.FindLocalName(A.NameLID, @Market), isnull(A.Name, '')),
                          R.ConfStat, R.ConfStatDate, R.CanReason, R.OptDate, R.AuthorUser, R.ResDate, R.ChgDate, R.ChgUser, R.NetPrice, R.NetCur, R.SalePrice, R.SaleCur, R.AgencyCom, R.AgencyComPer,
                          R.AgencyUser, AgencyUserName=isnull(AU.Name, ''), AgencyUserNameL=isnull(dbo.FindLocalName(AU.NameLID, @Market), isnull(AU.Name, '')),
                          R.AgencyComSupPer, R.Ballon, R.ReceiveDate, R.RecCrtDate, R.RecCrtUser, R.RecChgDate, R.RecChgUser, R.ResNote, R.IntNote, R.Code1, R.Code2, R.Code3, R.Code4, R.Adult, R.Child, R.ConfToAgency,
                          R.ConfToAgencyDate, R.AgencyPayable, R.PayLastDate, R.AgencyPayment, R.PaymentStat, R.Discount, R.AgencyComMan, R.SendInComing, R.SendInComingDate, R.AllDocPrint, R.SendAgency,
                          R.SendAgencyDate, R.EB, R.EBSupID, R.EBSupMaxPer, R.EBPasID, R.EBAgencyPer, R.EBPasPer, R.DepCity, R.ArrCity, R.Credit, R.CreditExpense, R.CreditExpenseCur, R.Broker, R.BrokerCom, R.BrokerComPer, 
                          R.PLOperator, R.PLMarket, R.AgencyBonus, R.AgencyBonusAmount, R.UserBonus, R.UserBonusAmount, R.PasBonus, R.PasBonusAmount, R.Complete, R.ReadByOpr, R.ReadByOprUser, R.ReadByOprDate,
                          R.ReadByAgency, R.ReadByAgencyUser, R.ReadByAgencyDate, R.InvoiceIssued, R.InvoiceDate, R.Balance, R.InvoicedAmount, R.PasSupDis, R.AgencySupDis, R.PasPayable, R.PasPayment, R.PasBalance,
                          R.AgencyComSup, R.EBAgency, R.EBPas, R.PasAmount, R.Tax, R.AgencyComInvAmount, R.SendToAcc, R.SendToAccDate, R.SendToAccUser, R.AllowDocPrint, R.CallFromResSer, R.AgencyDisPasPer,
                          R.AgencyDisPasVal, R.LastUser, R.AceCustomerCode, R.AceTfNumber, R.SendToAccPL, R.EBPasChk, R.EBAgencyChk, R.WebIP, R.HowFindUs, R.EBPasDueDate, R.EBPasDuePer, R.EBPasDueDate2,
                          R.EBPasDuePer2, R.PayPlanMode, R.PasDiscBase, R.AgencyDiscBase, R.InvoiceTo, R.AgencyComPayAmount, R.AgencyEarnBonus, R.UserEarnBonus, R.PasEarnBonus, R.AllowParent, R.PackType,
                          R.Pax, R.PasPayRule, R.HasDrReport, R.OldResPrice, R.HoneyMoonRes, R.SendToAce, R.SendToAceDate, R.MerlinxID, R.SendTo3Pgm, R.SendTo3PgmDate, R.ResTime, R.PromoCode, R.PromoAmount, 
                          R.ContractIssued, R.ContractDate, R.CalcErrNo, R.SaleComp, R.PLSpoVal, PackedPrice=R.PackedPrice,
                          MemTable=Cast(1 AS bit)
                        From ResMain R (NOLOCK)
                        Join Agency A (NOLOCK) ON A.Code=R.Agency
                        Join Office O (NOLOCK) ON O.Code=R.Office
                        Left Join AgencyUser AU (NOLOCK) ON AU.Code=R.AgencyUser And AU.Agency=R.Agency
                        Left Join HolPack H (NOLOCK) ON H.Code=R.HolPack
                        Where ResNo=@ResNo ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        record.RefNo = (int)rdr["RefNo"];
                        record.ResNo = Conversion.getStrOrNull(rdr["ResNo"]);
                        record.Operator = Conversion.getStrOrNull(rdr["Operator"]);
                        record.Market = Conversion.getStrOrNull(rdr["Market"]);
                        record.Office = Conversion.getStrOrNull(rdr["Office"]);
                        record.OfficeName = Conversion.getStrOrNull(rdr["OfficeName"]);
                        record.OfficeNameL = Conversion.getStrOrNull(rdr["OfficeNameL"]);
                        record.Agency = Conversion.getStrOrNull(rdr["Agency"]);
                        record.AgencyName = Conversion.getStrOrNull(rdr["AgencyName"]);
                        record.AgencyNameL = Conversion.getStrOrNull(rdr["AgencyNameL"]);
                        record.HolPack = Conversion.getStrOrNull(rdr["HolPack"]);
                        record.HolPackName = Conversion.getStrOrNull(rdr["HolPackName"]);
                        record.HolPackNameL = Conversion.getStrOrNull(rdr["HolPackNameL"]);
                        record.BegDate = Conversion.getDateTimeOrNull(rdr["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(rdr["EndDate"]);
                        record.Days = Conversion.getInt16OrNull(rdr["Days"]);
                        record.PriceSource = Conversion.getInt16OrNull(rdr["PriceSource"]);
                        record.PriceListNo = Conversion.getInt32OrNull(rdr["PriceListNo"]);
                        record.SaleResource = Conversion.getInt16OrNull(rdr["SaleResource"]);
                        record.ResLock = Conversion.getInt16OrNull(rdr["ResLock"]);
                        record.ResStat = Conversion.getInt16OrNull(rdr["ResStat"]);
                        record.ResStatDate = Conversion.getDateTimeOrNull(rdr["ResStatDate"]);
                        record.ConfStat = Conversion.getInt16OrNull(rdr["ConfStat"]);
                        record.ConfStatDate = Conversion.getDateTimeOrNull(rdr["ConfStatDate"]);
                        record.CanReason = Conversion.getInt32OrNull(rdr["CanReason"]);
                        record.OptDate = Conversion.getDateTimeOrNull(rdr["OptDate"]);
                        record.AgencyUser = Conversion.getStrOrNull(rdr["AgencyUser"]);
                        record.AgencyUserName = Conversion.getStrOrNull(rdr["AgencyUserName"]);
                        record.AgencyUserNameL = Conversion.getStrOrNull(rdr["AgencyUserNameL"]);
                        record.AuthorUser = Conversion.getStrOrNull(rdr["AuthorUser"]);
                        record.ResDate = Conversion.getDateTimeOrNull(rdr["ResDate"]);
                        record.ChgDate = Conversion.getDateTimeOrNull(rdr["ChgDate"]);
                        record.ChgUser = Conversion.getStrOrNull(rdr["ChgUser"]);
                        record.NetPrice = Conversion.getDecimalOrNull(rdr["NetPrice"]);
                        record.NetCur = Conversion.getStrOrNull(rdr["NetCur"]);
                        record.SalePrice = Conversion.getDecimalOrNull(rdr["SalePrice"]);
                        record.SaleCur = Conversion.getStrOrNull(rdr["SaleCur"]);
                        record.AgencyCom = Conversion.getDecimalOrNull(rdr["AgencyCom"]);
                        record.AgencyComPer = Conversion.getDecimalOrNull(rdr["AgencyComPer"]);
                        record.AgencyComSupPer = Conversion.getDecimalOrNull(rdr["AgencyComSupPer"]);
                        record.Ballon = Conversion.getStrOrNull(rdr["Ballon"]);
                        record.ReceiveDate = Conversion.getDateTimeOrNull(rdr["ReceiveDate"]);
                        record.RecCrtDate = Conversion.getDateTimeOrNull(rdr["RecCrtDate"]);
                        record.RecCrtUser = Conversion.getStrOrNull(rdr["RecCrtUser"]);
                        record.RecChgDate = Conversion.getDateTimeOrNull(rdr["RecChgDate"]);
                        record.RecChgUser = Conversion.getStrOrNull(rdr["RecChgUser"]);
                        record.ResNote = Conversion.getStrOrNull(rdr["ResNote"]);
                        record.IntNote = Conversion.getStrOrNull(rdr["IntNote"]);
                        record.Code1 = Conversion.getStrOrNull(rdr["Code1"]);
                        record.Code2 = Conversion.getStrOrNull(rdr["Code2"]);
                        record.Code3 = Conversion.getStrOrNull(rdr["Code3"]);
                        record.Code4 = Conversion.getStrOrNull(rdr["Code4"]);
                        record.Adult = Conversion.getInt16OrNull(rdr["Adult"]);
                        record.Child = Conversion.getInt16OrNull(rdr["Child"]);
                        record.ConfToAgency = Conversion.getStrOrNull(rdr["ConfToAgency"]);
                        record.ConfToAgencyDate = Conversion.getDateTimeOrNull(rdr["ConfToAgencyDate"]);
                        record.AgencyPayable = Conversion.getDecimalOrNull(rdr["AgencyPayable"]);
                        record.PayLastDate = Conversion.getDateTimeOrNull(rdr["PayLastDate"]);
                        record.AgencyPayment = Conversion.getDecimalOrNull(rdr["AgencyPayment"]);
                        record.PaymentStat = Conversion.getStrOrNull(rdr["PaymentStat"]);
                        record.Discount = Conversion.getDecimalOrNull(rdr["Discount"]);
                        record.AgencyComMan = Conversion.getStrOrNull(rdr["AgencyComMan"]);
                        record.SendInComing = Conversion.getStrOrNull(rdr["SendInComing"]);
                        record.SendInComingDate = Conversion.getDateTimeOrNull(rdr["SendInComingDate"]);
                        record.AllDocPrint = Conversion.getStrOrNull(rdr["AllDocPrint"]);
                        record.SendAgency = Conversion.getStrOrNull(rdr["SendAgency"]);
                        record.SendAgencyDate = Conversion.getDateTimeOrNull(rdr["SendAgencyDate"]);
                        record.EB = Conversion.getStrOrNull(rdr["EB"]);
                        record.EBSupID = Conversion.getInt32OrNull(rdr["EBSupID"]);
                        record.EBSupMaxPer = Conversion.getDecimalOrNull(rdr["EBSupMaxPer"]);
                        record.EBPasID = Conversion.getInt32OrNull(rdr["EBPasID"]);
                        record.EBAgencyPer = Conversion.getDecimalOrNull(rdr["EBAgencyPer"]);
                        record.EBPasPer = Conversion.getDecimalOrNull(rdr["EBPasPer"]);
                        record.DepCity = Conversion.getInt32OrNull(rdr["DepCity"]);
                        record.ArrCity = Conversion.getInt32OrNull(rdr["ArrCity"]);
                        record.Credit = Conversion.getStrOrNull(rdr["Credit"]);
                        record.CreditExpense = Conversion.getDecimalOrNull(rdr["CreditExpense"]);
                        record.CreditExpenseCur = Conversion.getStrOrNull(rdr["CreditExpenseCur"]);
                        record.Broker = Conversion.getStrOrNull(rdr["Broker"]);
                        record.BrokerCom = Conversion.getDecimalOrNull(rdr["BrokerCom"]);
                        record.BrokerComPer = Conversion.getDecimalOrNull(rdr["BrokerComPer"]);
                        record.PLOperator = Conversion.getStrOrNull(rdr["PLOperator"]);
                        record.PLMarket = Conversion.getStrOrNull(rdr["PLMarket"]);
                        record.AgencyBonus = Conversion.getDecimalOrNull(rdr["AgencyBonus"]);
                        record.AgencyBonusAmount = Conversion.getDecimalOrNull(rdr["AgencyBonusAmount"]);
                        record.UserBonus = Conversion.getDecimalOrNull(rdr["UserBonus"]);
                        record.UserBonusAmount = Conversion.getDecimalOrNull(rdr["UserBonusAmount"]);
                        record.PasBonus = Conversion.getDecimalOrNull(rdr["PasBonus"]);
                        record.PasBonusAmount = Conversion.getDecimalOrNull(rdr["PasBonusAmount"]);
                        record.Complete = Conversion.getByteOrNull(rdr["Complete"]);
                        record.ReadByOpr = Conversion.getByteOrNull(rdr["ReadByOpr"]);
                        record.ReadByOprUser = Conversion.getStrOrNull(rdr["ReadByOprUser"]);
                        record.ReadByOprDate = Conversion.getDateTimeOrNull(rdr["ReadByOprDate"]);
                        record.ReadByAgency = Conversion.getByteOrNull(rdr["ReadByAgency"]);
                        record.ReadByAgencyUser = Conversion.getStrOrNull(rdr["ReadByAgencyUser"]);
                        record.ReadByAgencyDate = Conversion.getDateTimeOrNull(rdr["ReadByAgencyDate"]);
                        record.InvoiceIssued = Conversion.getStrOrNull(rdr["InvoiceIssued"]);
                        record.InvoiceDate = Conversion.getDateTimeOrNull(rdr["InvoiceDate"]);
                        record.Balance = Conversion.getDecimalOrNull(rdr["Balance"]);
                        record.InvoicedAmount = Conversion.getDecimalOrNull(rdr["InvoicedAmount"]);
                        record.PasSupDis = Conversion.getDecimalOrNull(rdr["PasSupDis"]);
                        record.AgencySupDis = Conversion.getDecimalOrNull(rdr["AgencySupDis"]);
                        record.PasPayable = Conversion.getDecimalOrNull(rdr["PasPayable"]);
                        record.PasPayment = Conversion.getDecimalOrNull(rdr["PasPayment"]);
                        record.PasBalance = Conversion.getDecimalOrNull(rdr["PasBalance"]);
                        record.AgencyComSup = Conversion.getDecimalOrNull(rdr["AgencyComSup"]);
                        record.EBAgency = Conversion.getDecimalOrNull(rdr["EBAgency"]);
                        record.EBPas = Conversion.getDecimalOrNull(rdr["EBPas"]);
                        record.PasAmount = Conversion.getDecimalOrNull(rdr["PasAmount"]);
                        record.Tax = Conversion.getDecimalOrNull(rdr["Tax"]);
                        record.AgencyComInvAmount = Conversion.getDecimalOrNull(rdr["AgencyComInvAmount"]);
                        record.SendToAcc = Conversion.getStrOrNull(rdr["SendToAcc"]);
                        record.SendToAccDate = Conversion.getDateTimeOrNull(rdr["SendToAccDate"]);
                        record.SendToAccUser = Conversion.getStrOrNull(rdr["SendToAccUser"]);
                        record.AllowDocPrint = Conversion.getStrOrNull(rdr["AllowDocPrint"]);
                        record.CallFromResSer = Conversion.getStrOrNull(rdr["CallFromResSer"]);
                        record.AgencyDisPasPer = Conversion.getDecimalOrNull(rdr["AgencyDisPasPer"]);
                        record.AgencyDisPasVal = Conversion.getDecimalOrNull(rdr["AgencyDisPasVal"]);
                        record.LastUser = Conversion.getStrOrNull(rdr["LastUser"]);
                        record.AceCustomerCode = Conversion.getStrOrNull(rdr["AceCustomerCode"]);
                        record.AceTfNumber = Conversion.getStrOrNull(rdr["AceTfNumber"]);
                        record.SendToAccPL = Conversion.getStrOrNull(rdr["SendToAccPL"]);
                        record.EBPasChk = Conversion.getByteOrNull(rdr["EBPasChk"]);
                        record.EBAgencyChk = Conversion.getByteOrNull(rdr["EBAgencyChk"]);
                        record.WebIP = Conversion.getStrOrNull(rdr["WebIP"]);
                        record.HowFindUs = Conversion.getInt32OrNull(rdr["HowFindUs"]);
                        record.EBPasDueDate = Conversion.getDateTimeOrNull(rdr["EBPasDueDate"]);
                        record.EBPasDuePer = Conversion.getDecimalOrNull(rdr["EBPasDuePer"]);
                        record.EBPasDueDate2 = Conversion.getDateTimeOrNull(rdr["EBPasDueDate2"]);
                        record.EBPasDuePer2 = Conversion.getDecimalOrNull(rdr["EBPasDuePer2"]);
                        record.PayPlanMode = Conversion.getByteOrNull(rdr["PayPlanMode"]);
                        record.PasDiscBase = Conversion.getDecimalOrNull(rdr["PasDiscBase"]);
                        record.AgencyDiscBase = Conversion.getDecimalOrNull(rdr["AgencyDiscBase"]);
                        record.InvoiceTo = Conversion.getByteOrNull(rdr["InvoiceTo"]);
                        record.AgencyComPayAmount = Conversion.getDecimalOrNull(rdr["AgencyComPayAmount"]);
                        record.AgencyEarnBonus = Conversion.getByteOrNull(rdr["AgencyEarnBonus"]);
                        record.UserEarnBonus = Conversion.getByteOrNull(rdr["UserEarnBonus"]);
                        record.PasEarnBonus = Conversion.getByteOrNull(rdr["PasEarnBonus"]);
                        record.AllowParent = Conversion.getBoolOrNull(rdr["AllowParent"]);
                        record.PackType = Conversion.getStrOrNull(rdr["PackType"]);
                        record.Pax = Conversion.getInt16OrNull(rdr["Pax"]);
                        record.PasPayRule = Conversion.getByteOrNull(rdr["PasPayRule"]);
                        record.HasDrReport = Conversion.getBoolOrNull(rdr["HasDrReport"]);
                        record.OldResPrice = Conversion.getDecimalOrNull(rdr["OldResPrice"]);
                        record.HoneyMoonRes = Conversion.getBoolOrNull(rdr["HoneyMoonRes"]);
                        record.SendToAce = Conversion.getBoolOrNull(rdr["SendToAce"]);
                        record.SendToAceDate = Conversion.getDateTimeOrNull(rdr["SendToAceDate"]);
                        record.MerlinxID = Conversion.getStrOrNull(rdr["MerlinxID"]);
                        record.SendTo3Pgm = Conversion.getStrOrNull(rdr["SendTo3Pgm"]);
                        record.SendTo3PgmDate = Conversion.getDateTimeOrNull(rdr["SendTo3PgmDate"]);
                        record.ResTime = Conversion.getDateTimeOrNull(rdr["ResTime"]);
                        record.PromoCode = Conversion.getStrOrNull(rdr["PromoCode"]);
                        record.PromoAmount = Conversion.getDecimalOrNull(rdr["PromoAmount"]);
                        record.ContractIssued = Conversion.getBoolOrNull(rdr["ContractIssued"]);
                        record.ContractDate = Conversion.getDateTimeOrNull(rdr["ContractDate"]);
                        record.CalcErrNo = Conversion.getInt16OrNull(rdr["CalcErrNo"]);
                        record.SaleComp = Conversion.getBoolOrNull(rdr["SaleComp"]);
                        record.PLSpoVal = Conversion.getDecimalOrNull(rdr["PLSpoVal"]);
                        record.MemTable = true;
                    }
                }
                return record;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BIResCustRecord> getResCustList(string Market, string ResNo, ref string errorMsg)
        {
            List<BIResCustRecord> records = new List<BIResCustRecord>();

            string tsql = @"Select C.ResNo, C.SeqNo, C.CustNo, C.Title, T.Code AS TitleStr, C.Surname, C.Name, C.SurnameL, C.NameL, 
	                            C.Birtday, C.Age, C.PassSerie, C.PassNo, C.PassIssueDate, C.PassExpDate, C.PassGiven, C.IDNo, 
	                            C.Nation, NationName=(Select Name From Location (NOLOCK) Where RecID = C.Nation),
	                            NationNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market) ,Name) From Location (NOLOCK) Where RecID = C.Nation),
	                            C.Leader, C.DocVoucher, C.DocTicket, C.DocInsur, C.Bonus, C.BonusAmount, C.HasPassport, C.Status, 
                                C.ppSalePrice, C.ppPromoAmount, C.ppSupDisAmount, C.ppPasPayable, C.ppBaseCanAmount, C.ppPasEB, C.ppPLSpoVal
                            From ResCust C (NOLOCK) 
                            Join Title T (NOLOCK) ON T.TitleNo = C.Title 
                            Where C.ResNo = @ResNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        BIResCustRecord record = new BIResCustRecord();
                        record.ResNo = Conversion.getStrOrNull(rdr["ResNo"]);
                        record.SeqNo = Conversion.getInt16OrNull(rdr["SeqNo"]);
                        record.CustNo = (int)rdr["CustNo"];
                        record.CustNoT = record.CustNo;
                        record.Title = Conversion.getInt16OrNull(rdr["Title"]);
                        record.TitleStr = Conversion.getStrOrNull(rdr["TitleStr"]);
                        record.Surname = Conversion.getStrOrNull(rdr["Surname"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.SurnameL = Conversion.getStrOrNull(rdr["SurnameL"]);
                        record.NameL = Conversion.getStrOrNull(rdr["NameL"]);
                        record.Birtday = Conversion.getDateTimeOrNull(rdr["Birtday"]);
                        record.Age = Conversion.getInt16OrNull(rdr["Age"]);
                        record.PassSerie = Conversion.getStrOrNull(rdr["PassSerie"]);
                        record.PassNo = Conversion.getStrOrNull(rdr["PassNo"]);
                        record.PassIssueDate = Conversion.getDateTimeOrNull(rdr["PassIssueDate"]);
                        record.PassExpDate = Conversion.getDateTimeOrNull(rdr["PassExpDate"]);
                        record.PassGiven = Conversion.getStrOrNull(rdr["PassGiven"]);
                        record.IDNo = Conversion.getStrOrNull(rdr["IDNo"]);
                        record.Nation = Conversion.getInt32OrNull(rdr["Nation"]);
                        record.NationName = Conversion.getStrOrNull(rdr["NationName"]);
                        record.NationNameL = Conversion.getStrOrNull(rdr["NationNameL"]);
                        record.DocVoucher = Conversion.getByteOrNull(rdr["DocVoucher"].ToString());
                        record.DocTicket = Conversion.getByteOrNull(rdr["DocTicket"].ToString());
                        record.DocInsur = Conversion.getByteOrNull(rdr["DocInsur"].ToString());
                        record.Bonus = Conversion.getDecimalOrNull(rdr["Bonus"]);
                        record.BonusAmount = Conversion.getDecimalOrNull(rdr["BonusAmount"]);
                        record.HasPassport = Conversion.getBoolOrNull(rdr["HasPassport"]);
                        record.Status = Conversion.getByteOrNull(rdr["Status"]);
                        record.Leader = Conversion.getStrOrNull(rdr["Leader"]);
                        record.ppBaseCanAmount = Conversion.getDecimalOrNull(rdr["ppBaseCanAmount"]);
                        record.PpPasEB = Conversion.getDecimalOrNull(rdr["PpPasEB"]);
                        record.ppPasPayable = Conversion.getDecimalOrNull(rdr["ppPasPayable"]);
                        record.PpPLSpoVal = Conversion.getDecimalOrNull(rdr["PpPLSpoVal"]);
                        record.ppPromoAmount = Conversion.getDecimalOrNull(rdr["ppPromoAmount"]);
                        record.ppSalePrice = Conversion.getDecimalOrNull(rdr["ppSalePrice"]);
                        record.ppSupDisAmount = Conversion.getDecimalOrNull(rdr["ppSupDisAmount"]);
                        record.MemTable = true;
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public List<BIResCustInfoRecord> getResCustInfoList(string Market, string ResNo, ref string errorMsg)
        {
            List<BIResCustInfoRecord> list = new List<BIResCustInfoRecord>();

            string tsql = @"Select RCI.RecID, RCI.CustNo, RCI.CTitle, CTitleName=(Select Code From Title (NOLOCK) Where TitleNo=RCI.CTitle), RCI.CName, RCI.CSurName, 
                                RCI.AddrHome, RCI.AddrHomeCity, RCI.AddrHomeZip, RCI.AddrHomeTel, RCI.AddrHomeFax, RCI.AddrHomeEmail, RCI.AddrHomeCountry, RCI.AddrHomeCountryCode, RCI.HomeTaxOffice, RCI.HomeTaxAccNo,
                                RCI.AddrWork, RCI.AddrWorkCity, RCI.AddrWorkZip, RCI.AddrWorkTel, RCI.AddrWorkFax, RCI.AddrWorkEmail, RCI.AddrWorkCountry, RCI.AddrWorkCountryCode, RCI.WorkTaxOffice, RCI.WorkTaxAccNo,
                                RCI.ContactAddr, RCI.MobTel, RCI.Jobs, RCI.Note,  RCI.InvoiceAddr, RCI.WorkFirmName, RCI.Bank, RCI.BankAccNo, RCI.BankIBAN
                            From ResCustInfo RCI (NOLOCK)
                            Join ResCust RC (NOLOCK) ON RC.CustNo=RCI.CustNo
                            Where RC.ResNo = @ResNo";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        BIResCustInfoRecord record = new BIResCustInfoRecord();
                        record.RecID = (int)rdr["RecID"];
                        record.CustNo = (int)rdr["CustNo"];
                        record.CTitle = Conversion.getInt16OrNull(rdr["CTitle"]);
                        record.CTitleName = Conversion.getStrOrNull(rdr["CTitleName"]);
                        record.CName = Conversion.getStrOrNull(rdr["CName"]);
                        record.CSurName = Conversion.getStrOrNull(rdr["CSurName"]);
                        record.AddrHome = Conversion.getStrOrNull(rdr["AddrHome"]);
                        record.AddrHomeCity = Conversion.getStrOrNull(rdr["AddrHomeCity"]);
                        record.AddrHomeZip = Conversion.getStrOrNull(rdr["AddrHomeZip"]);
                        record.AddrHomeTel = Conversion.getStrOrNull(rdr["AddrHomeTel"]);
                        record.AddrHomeFax = Conversion.getStrOrNull(rdr["AddrHomeFax"]);
                        record.AddrHomeEmail = Conversion.getStrOrNull(rdr["AddrHomeEmail"]);
                        record.AddrHomeCountry = Conversion.getStrOrNull(rdr["AddrHomeCountry"]);
                        record.AddrHomeCountryCode = Conversion.getStrOrNull(rdr["AddrHomeCountryCode"]);
                        record.HomeTaxOffice = Conversion.getStrOrNull(rdr["HomeTaxOffice"]);
                        record.HomeTaxAccNo = Conversion.getStrOrNull(rdr["HomeTaxAccNo"]);
                        record.AddrWork = Conversion.getStrOrNull(rdr["AddrWork"]);
                        record.AddrWorkCity = Conversion.getStrOrNull(rdr["AddrWorkCity"]);
                        record.AddrWorkZip = Conversion.getStrOrNull(rdr["AddrWorkZip"]);
                        record.AddrWorkTel = Conversion.getStrOrNull(rdr["AddrWorkTel"]);
                        record.AddrWorkFax = Conversion.getStrOrNull(rdr["AddrWorkFax"]);
                        record.AddrWorkEMail = Conversion.getStrOrNull(rdr["AddrWorkEmail"]);
                        record.AddrWorkCountry = Conversion.getStrOrNull(rdr["AddrWorkCountry"]);
                        record.AddrWorkCountryCode = Conversion.getStrOrNull(rdr["AddrWorkCountryCode"]);
                        record.WorkTaxOffice = Conversion.getStrOrNull(rdr["WorkTaxOffice"]);
                        record.WorkTaxAccNo = Conversion.getStrOrNull(rdr["WorkTaxAccNo"]);
                        record.ContactAddr = Conversion.getStrOrNull(rdr["ContactAddr"]);
                        record.MobTel = Conversion.getStrOrNull(rdr["MobTel"]);
                        record.Jobs = Conversion.getStrOrNull(rdr["Jobs"]);
                        record.Note = Conversion.getStrOrNull(rdr["Note"]);
                        record.InvoiceAddr = Conversion.getStrOrNull(rdr["InvoiceAddr"]);
                        record.WorkFirmName = Conversion.getStrOrNull(rdr["WorkFirmName"]);
                        record.Bank = Conversion.getStrOrNull(rdr["Bank"]);
                        record.BankAccNo = Conversion.getStrOrNull(rdr["BankAccNo"]);
                        record.BankIBAN = Conversion.getStrOrNull(rdr["BankIBAN"]);
                        record.MemTable = true;
                        list.Add(record);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BIPayTypeRecord> getPayType(string Market, ref string errorMsg)
        {
            List<BIPayTypeRecord> records = new List<BIPayTypeRecord>();
            string tsql = string.Empty;

            tsql = @"Select PT.RecID, PT.Market, PT.Code, PT.Name, NameL=isnull(dbo.FindLocalName(PT.NameLID, @Market), PT.Name),
                           PT.RateType, PT.ExChgDate, PT.Bank, BankName=isnull(dbo.FindLocalName(B.NameLID, @Market),B.Name),
                           PT.CanReceiptPrt, PT.PayCat, PayCatName=isnull(dbo.FindLocalName(PTC.NameLID, @Market),PTC.Name),
                           PT.OprBankID,
                           PT.PayBegDate, PT.PayEndDate, PT.ResBegDate, PT.ResEndDate, PT.MinNight, PT.MaxNight, PT.HasInstalment, PT.CreditCard,
                           PT.InstalNumFrom, PT.InstalNumTo, PT.DiscountPer, PT.BankComPer, PT.PosType, PT.EndofDay, PT.PeriodofInstalment, 
                           PT.SupDisCode, PT.ValorDay, PT.PayCur, PT.ResPayMinPer, PT.SD, PT.AllowWIS, PT.ResPayMinVal, PT.PayPalAPIUserName,
                           PT.PayPalAPIPwd, PT.PayPalSecureMerchantId, PT.PayPalMerchantName, PT.ValidRemDaytoCin, PT.ValidForB2B, PT.ValidForB2C, PT.DiscountVal, PT.Secure3d
                        From PayType PT (NOLOCK)
                        Join PayTypeCat PTC (NOLOCK) ON PTC.Code=PT.PayCat
                        Left Join Bank B (NOLOCK) ON B.Code=PT.Bank 
                        ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        BIPayTypeRecord record = new BIPayTypeRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.RateType = Conversion.getInt16OrNull(R["RateType"]);
                        record.ExChgDate = Conversion.getInt16OrNull(R["ExChgDate"]);
                        record.Bank = Conversion.getStrOrNull(R["Bank"]);
                        record.BankName = Conversion.getStrOrNull(R["BankName"]);
                        record.CanReceiptPrt = Conversion.getStrOrNull(R["CanReceiptPrt"]);
                        record.PayCat = Conversion.getInt16OrNull(R["PayCat"]);
                        record.PayCatName = Conversion.getStrOrNull(R["PayCatName"]);
                        record.OprBankID = Conversion.getInt32OrNull(R["OprBankID"]);
                        record.PayBegDate = Conversion.getDateTimeOrNull(R["PayBegDate"]);
                        record.PayEndDate = Conversion.getDateTimeOrNull(R["PayEndDate"]);
                        record.ResBegDate = Conversion.getDateTimeOrNull(R["ResBegDate"]);
                        record.ResEndDate = Conversion.getDateTimeOrNull(R["ResEndDate"]);
                        record.MinNight = Conversion.getInt16OrNull(R["MinNight"]);
                        record.MaxNight = Conversion.getInt16OrNull(R["MaxNight"]);
                        record.HasInstalment = Conversion.getBoolOrNull(R["HasInstalment"]);
                        record.CreditCard = Conversion.getStrOrNull(R["CreditCard"]);
                        record.InstalNumFrom = Conversion.getInt16OrNull(R["InstalNumFrom"]);
                        record.InstalNumTo = Conversion.getInt16OrNull(R["InstalNumTo"]);
                        record.DiscountPer = Conversion.getDecimalOrNull(R["DiscountPer"]);
                        record.BankComPer = Conversion.getDecimalOrNull(R["BankComPer"]);
                        record.PosType = Conversion.getInt16OrNull(R["PosType"]);
                        record.EndofDay = Conversion.getDateTimeOrNull(R["EndofDay"]);
                        record.PeriodofInstalment = Conversion.getInt16OrNull(R["PeriodofInstalment"]);
                        record.SupDisCode = Conversion.getStrOrNull(R["SupDisCode"]);
                        record.ValorDay = Conversion.getInt16OrNull(R["ValorDay"]);
                        record.PayCur = Conversion.getStrOrNull(R["PayCur"]);
                        record.ResPayMinPer = Conversion.getDecimalOrNull(R["ResPayMinPer"]);
                        record.SD = Conversion.getStrOrNull(R["SD"]);
                        record.AllowWIS = Conversion.getBoolOrNull(R["AllowWIS"]);
                        record.ResPayMinVal = Conversion.getDecimalOrNull(R["ResPayMinVal"]);
                        record.PayPalAPIUserName = Conversion.getStrOrNull(R["PayPalAPIUserName"]);
                        record.PayPalAPIPwd = Conversion.getStrOrNull(R["PayPalAPIPwd"]);
                        record.PayPalSecureMerchantId = Conversion.getStrOrNull(R["PayPalSecureMerchantId"]);
                        record.PayPalMerchantName = Conversion.getStrOrNull(R["PayPalMerchantName"]);
                        record.ValidRemDaytoCin = Conversion.getInt16OrNull(R["ValidRemDaytoCin"]);
                        record.ValidForB2B = Conversion.getBoolOrNull(R["ValidForB2B"]);
                        record.ValidForB2C = Conversion.getBoolOrNull(R["ValidForB2C"]);
                        record.DiscountVal = Conversion.getDecimalOrNull(R["DiscountVal"]);
                        record.Secure3d = Conversion.getBoolOrNull(R["Secure3d"]);
                        records.Add(record);
                    }
                    if (records.Count > 0)
                        return records;
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BIResPayPlanRecord> getResPayPlan(string ResNo, ref string errorMsg)
        {
            List<BIResPayPlanRecord> records = new List<BIResPayPlanRecord>();
            string tsql = string.Empty;

            tsql = @"
                    Select RecID,ResNo,PayNo,DueDate,Amount,Cur,isEB,[Status],PlanType,Adult,Child,Infant
                    From ResPayPlan (NOLOCK) 
                    Where ResNo=@ResNo
                    Order By DueDate
                    ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        BIResPayPlanRecord record = new BIResPayPlanRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        record.PayNo = Conversion.getInt16OrNull(R["PayNo"]);
                        record.DueDate = Conversion.getDateTimeOrNull(R["DueDate"]);
                        record.Amount = Conversion.getDecimalOrNull(R["Amount"]);
                        record.Cur = Conversion.getStrOrNull(R["Cur"]);
                        record.IsEB = Conversion.getBoolOrNull(R["isEB"]);
                        record.Status = Conversion.getByteOrNull(R["Status"]);
                        record.PlanType = Conversion.getByteOrNull(R["PlanType"]);
                        record.Adult = Conversion.getDecimalOrNull(R["Adult"]);
                        record.Child = Conversion.getDecimalOrNull(R["Child"]);
                        record.Infant = Conversion.getDecimalOrNull(R["Infant"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public decimal? getResAmountForPay(string ResNo, ref string errorMsg)
        {
            string tsql = @"Select dbo.ufn_GetResAmountForPay(@ResNo) From ResMain (NOLOCK) Where ResNo=@ResNo";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                return (decimal?)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public decimal? getResAmount(string ResNo, Int16 AccountType, ref string errorMsg)
        {
            string tsql = @"Select ResAmount=dbo.ufn_GetResAmount(@ResNo, @AccountType, 1)";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "AccountType", DbType.Int16, AccountType);
                return (decimal?)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BIBankVPosRecord> getBankVPos(string BankCode, ref string errorMsg)
        {
            List<BIBankVPosRecord> records = new List<BIBankVPosRecord>();

            string tsql = @"Select RecID,Bank,Cur,Host,ClientID,UserID,[Password],BankCur,Interface,Operator,ProvUserID,ProvUserPass,MerchantID,PosVersion,
                              SuccessUrlB2B,SuccessUrlB2C,ErrorUrlB2B,ErrorUrlB2C,StoreKey
                            From BankVPos (NOLOCK)
                            ";
            if (!string.IsNullOrEmpty(BankCode))
                tsql += string.Format("Where Bank='{0}'", BankCode);

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        BIBankVPosRecord record = new BIBankVPosRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Bank = Conversion.getStrOrNull(R["Bank"]);
                        record.Cur = Conversion.getStrOrNull(R["Cur"]);
                        record.Host = Conversion.getStrOrNull(R["Host"]);
                        record.ClientID = Conversion.getStrOrNull(R["ClientID"]);
                        record.UserID = Conversion.getStrOrNull(R["UserID"]);
                        record.Password = Conversion.getStrOrNull(R["Password"]);
                        record.BankCur = Conversion.getStrOrNull(R["BankCur"]);
                        record.Interface = Conversion.getStrOrNull(R["Interface"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.ProvUserID = Conversion.getStrOrNull(R["ProvUserID"]);
                        record.ProvUserPass = Conversion.getStrOrNull(R["ProvUserPass"]);
                        record.MerchantID = Conversion.getStrOrNull(R["MerchantID"]);
                        record.PosVersion = Conversion.getStrOrNull(R["PosVersion"]);
                        record.SuccessUrlB2B = Conversion.getStrOrNull(R["SuccessUrlB2B"]);
                        record.SuccessUrlB2C = Conversion.getStrOrNull(R["SuccessUrlB2C"]);
                        record.ErrorUrlB2B = Conversion.getStrOrNull(R["ErrorUrlB2B"]);
                        record.ErrorUrlB2C = Conversion.getStrOrNull(R["ErrorUrlB2C"]);
                        record.StoreKey = Conversion.getStrOrNull(R["StoreKey"]);
                        records.Add(record);
                    }
                    return records;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool MakeResPayment(string AgencyID, Int16 AccountType, string AccountNo, string AccountName, DateTime? payDate, int? payTypeID, decimal? payAmount, string cur,
                             string ccNumber, int? ccTypeID, string ccBank, string ccHolder, DateTime? ccExpDate, Int16? ccSeqno, int? posType, int? instalNum,
                             string posReference, string resNo, string comment, ref string errorMsg)
        {
            string tsql = @"  exec usp_Make_ResPayment @Agency, @ReceiptType, @AccountType, @Account, @AccountName, @PayDate,
                                       @PayTypeID, @PayAmount, @PayCur, @CCNo, @CCTypeID, @CCBank, @CCHolder, @CCExpDate,
                                       @CCSecNo, @PosType, @InstalNum, @Reference, @Comment, @ResNo, @SaleCur, @ErrCode Output
                              Select @ErrCode  ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, AgencyID);
                db.AddInParameter(dbCommand, "ReceiptType", DbType.Int16, 0);
                db.AddInParameter(dbCommand, "AccountType", DbType.Int16, AccountType);
                db.AddInParameter(dbCommand, "Account", DbType.String, AccountNo);
                db.AddInParameter(dbCommand, "AccountName", DbType.String, AccountName);
                db.AddInParameter(dbCommand, "PayDate", DbType.DateTime, payDate);
                db.AddInParameter(dbCommand, "PayTypeID", DbType.Int32, payTypeID);
                db.AddInParameter(dbCommand, "PayAmount", DbType.Decimal, payAmount);
                db.AddInParameter(dbCommand, "PayCur", DbType.String, cur);
                db.AddInParameter(dbCommand, "CCNo", DbType.String, ccNumber);
                db.AddInParameter(dbCommand, "CCTypeID", DbType.Int32, ccTypeID.HasValue ? ccTypeID.Value : Convert.DBNull);
                db.AddInParameter(dbCommand, "CCBank", DbType.String, ccBank);
                db.AddInParameter(dbCommand, "CCHolder", DbType.String, ccHolder);
                db.AddInParameter(dbCommand, "CCExpDate", DbType.DateTime, ccExpDate);
                db.AddInParameter(dbCommand, "CCSecNo", DbType.String, ccSeqno);
                db.AddInParameter(dbCommand, "PosType", DbType.Int16, posType.HasValue ? posType : Convert.DBNull);
                db.AddInParameter(dbCommand, "InstalNum", DbType.Int16, instalNum);
                db.AddInParameter(dbCommand, "Reference", DbType.String, posReference);
                db.AddInParameter(dbCommand, "Comment", DbType.String, comment);
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNo);
                db.AddInParameter(dbCommand, "SaleCur", DbType.String, cur);
                db.AddInParameter(dbCommand, "ErrCode", DbType.Int32, null);

                int? retVal = Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
                if (retVal != null && retVal == 0)
                    return true;
                else
                {
                    errorMsg = "Payment transaction failed. ";
                    return false;
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool setReservationComplate(BIUser UserData, string ResNo, bool SaveDraft, ref string errorMsg)
        {
            string tsql = @" Update ResMain 
                             Set Complete=@Complete, ResStat=@ResStat 
                             Where ResNo=@ResNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Complete", DbType.Int16, SaveDraft ? 0 : 1);
                db.AddInParameter(dbCommand, "ResStat", DbType.Int16, SaveDraft ? 9 : 0);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool SetResOptDate(string ResNo, DateTime? optDate, int HowFindUs, ref string errorMsg)
        {
            string tsql = @"Update ResMain 
                            Set 
                                OptDate = @OptDate,
                                HowFindUs = isnull(@HowFindUs, HowFindUs) 
                            Where ResNo = @ResNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "OptDate", DbType.DateTime, optDate);
                db.AddInParameter(dbCommand, "HowFindUs", DbType.Int32, (HowFindUs > 0) ? HowFindUs : Convert.DBNull);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            return GetHexaDecimal(inputbytes);
        }

        public string GetSHA1X64(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            return Convert.ToBase64String(inputbytes);
        }

        public string GetPrice(decimal? price)
        {
            if (!price.HasValue) return "00";
            return Math.Floor(price.Value * 100).ToString("#.");
        }

        public static string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;
            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }
            return s.ToString();
        }

        public static string GetSHA256(string SHA256Data)
        {
            SHA256 sHA256 = SHA256Managed.Create();
            string HashedPassword = SHA256Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sHA256.ComputeHash(hashbytes);
            return GetHexaDecimal(inputbytes);
        }

        public BIPaymentInfo getPaymentInfoForReservation(string resNo, int? custNo, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (custNo.HasValue)
                tsql = @"   SELECT TOP 1
                                R.ResNo,R.PriceListNo,DiscountAmount=isnull(P.PasDiscBase,0), 
                                ResAmount=P.ResAmount,R.PasAmount,SalePrice=P.ResAmount,
                                R.OptDate OptionTill, R.Confstat,
                                R.SaleCur, C.CustNo, R.BegDate ResBegDate, R.Days Night, isnull(P.TotPayment, 0) PrePayment,
                                isnull(S.PerVal, 0) PrePerVal, Remain = P.Amount, 
                                J.InstalNum, PasPayRule = isnull(R.PasPayRule, 0)
                            FROM ResMain (NOLOCK) R 
                            JOIN ResCust (NOLOCK) C on R.ResNo = C.ResNo
                            Join dbo.GetPassAmountForPay(@ResNo, @CustNo) P ON P.CustNo = C.CustNo 
                            LEFT JOIN JournalCDet (NOLOCK) JCD on JCD.ResNo = R.ResNo                        
                            LEFT JOIN Journal (NOLOCK) J ON J.RecID = JCD.JournalID And J.Account = @CustNo
                            LEFT JOIN ResSupDis (NOLOCK) S on R.ResNo = S.ResNo and S.JournalID > 0 and S.CustNo = @CustNo
                            WHERE
                                R.ResNo = @ResNo
                            And C.CustNo = @CustNo
                            Order By J.PayDate ";
            else
                tsql = @"   SELECT TOP 1
                                R.ResNo, R.PriceListNo, dbo.ufn_Amount4Discount_Pas(R.ResNo) DiscountAmount, 
                                dbo.ufn_GetResAmount(R.ResNo, 4, 0) ResAmount, R.PasAmount, R.SalePrice,
                                R.OptDate OptionTill,R.Confstat, 
                                R.SaleCur, C.CustNo, R.BegDate ResBegDate, R.Days Night, R.AgencyPayment PrePayment,
                                S.PerVal PrePerVal, Remain = dbo.ufn_GetResAmount(R.ResNo, 3, 1), 
	                            J.InstalNum, PasPayRule = isnull(R.PasPayRule, 0)
                            FROM ResMain (NOLOCK) R 
                            JOIN ResCust (NOLOCK) C on R.ResNo = C.ResNo and C.Leader = 'Y'
                            LEFT JOIN JournalCDet (NOLOCK) JCD on JCD.ResNo = R.ResNo                        
                            LEFT JOIN Journal (NOLOCK) J ON J.RecID = JCD.JournalID
                            LEFT JOIN ResSupDis (NOLOCK) S on R.ResNo = S.ResNo and S.JournalID > 0 
                            WHERE
                                R.ResNo = @ResNo
                            Order By J.PayDate ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNo);
                db.AddInParameter(dbCommand, "CustNo", DbType.String, custNo);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        return new BIPaymentInfo
                        {
                            ResNo = Conversion.getStrOrNull(R["ResNo"]),
                            PriceListNo = Conversion.getInt32OrNull(R["PriceListNo"]),
                            DiscountAmount = Conversion.getDecimalOrNull(R["DiscountAmount"]),
                            ResAmount = Conversion.getDecimalOrNull(R["ResAmount"]),
                            PasAmount = Conversion.getDecimalOrNull(R["PasAmount"]),
                            SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]),
                            OptionTill = Conversion.getDateTimeOrNull(R["OptionTill"]),
                            ConfStat = Conversion.getInt16OrNull(R["ConfStat"]),
                            SaleCur = Conversion.getStrOrNull(R["SaleCur"]),
                            CustNo = Conversion.getInt32OrNull(R["CustNo"]),
                            ResBegDate = Conversion.getDateTimeOrNull(R["ResBegDate"]),
                            Night = Conversion.getInt16OrNull(R["Night"]),
                            PrePayment = Conversion.getDecimalOrNull(R["PrePayment"]),
                            PrePerVal = Conversion.getDecimalOrNull(R["PrePerVal"]),
                            Remain = Conversion.getDecimalOrNull(R["Remain"]),
                            InstalNum = Conversion.getInt16OrNull(R["InstalNum"]),
                            PasPayRule = Conversion.getByteOrNull(R["PasPayRule"])
                        };
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int? getLocationForCountry(int Location, ref string errorMsg)
        {
            string tsql =
@"
Select Country  
From Location (NOLOCK) 
Where RecID=(Select Country From Location (NOLOCK) Where RecID=@RecID) 
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "RecId", DbType.Int32, Location);

                return Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BIPaymentTypeData> getPaymentTypeData(string market, string plMarket, string Operator, string agency, string currency, int? country, int? catPackID, DateTime? payDate, DateTime? resBegDate, int? night, int? preperVal, bool onlyAgencyPayCategory, bool onlyCreditCards, ref string errorMsg)
        {
            List<BIPaymentTypeData> records = new List<BIPaymentTypeData>();
            string tsql = string.Empty;
            if (catPackID > 0)
            {
                tsql =
@"   
Select *
From ( 
    Select Distinct P.RecID PayTypeID, P.Code,Name = isnull(dbo.FindLocalName(P.NameLID, @Market), P.Name), 
        P.Paycat PayCatID, P.PosType,
        PayCatName = isnull(dbo.FindLocalName(PTC.NameLID, @Market), PTC.Name), 
        P.HasInstalment,
        isnull(P.InstalNumFrom, 0) InstalNumFrom, isnull(p.InstalNumTo, 0) InstalNumTo,
        B.RecID CreditCardID, P.Bank, P.PayCur, isnull(P.SupDiscode, '') SupDisCode,
        isnull(P.DiscountPer, 0) DiscountPer ,
        CreditCardName = isnull(dbo.FindLocalName(B.NameLID,@Market), B.Name),
        CreditCardCode = B.Code,
        isnull(P.ResPayMinPer, 100) ResPayMinPer,
        WPosBank = (Select Bank From BankCard K (NOLOCK) Where isnull(B.RedirectVPos, B.RecID) = K.RecID),
        isPicture = (CASE WHEN DATALENGTH(Picture) > 0 THEN Cast(1 AS bit) ELSE Cast(0 AS bit) END)
    From PayType P (NOLOCK)
    LEFT JOIN BankCard B (NOLOCK) ON P.CreditCard = B.Code and P.Bank = B.Bank and P.PayCat = 3
    LEFT JOIN OperatorBank O (NOLOCK) ON P.Bank = O.Bank And O.Operator = @Operator
    LEFT JOIN CatPackPayType CP (NOLOCK) ON CP.PayTypeID = P.RecID 
    JOIN PayTypeCat PTC (NOLOCK) ON PTC.Code = P.PayCat
    LEFT JOIN PayTypeDest (NOLOCK) PTD ON PTD.PayTypeID = P.RecID
    Where	Market=@plMarket 
        --And P.PayCur=@Currency 
        --And (@PrePerVal is null or ABS(P.DiscountPer)>=@PrePerVal)
        --And (@PrePerVal is null or isnull(p.InstalNumTo, 0)<=@PrePerVal)
        And (@PrePerVal is null or ((@PrePerVal between isnull(p.InstalNumFrom, 0) And isnull(p.InstalNumTo, 0))
							        OR (isnull(p.InstalNumTo, 0)<=@PrePerVal)))
        And @PayDate between IsNull(p.PayBegDate,@PayDate) 
        And IsNull(p.PayEndDate,@PayDate)
        And @ResBegDate between IsNull(p.ResBegDate,@ResBegDate) 
        And IsNull(p.ResEndDate,@ResBegDate)
        And @Night between IsNull(MinNight,@Night) 
        And IsNull(MaxNight,@Night)
        And (isnull(PTD.Country,0)=0 Or PTD.Country = @Country)
	    And (CP.CatPackID is null Or CP.CatPackID=@CatPackID)        
";
                if (onlyCreditCards)
                    tsql +=
    @" 
        And p.Paycat=3 
";
                if (onlyAgencyPayCategory)
                    tsql +=
    @"		
        And P.PayCat in (Select PayCat FROM AgencyPasPayCat WHERE Agency=@Agency)  
";
                tsql +=
    @"
    ) MultiTbl 
Order By PayCatID, CreditCardID, InstalNumFrom
";
            }
            else
            {
                tsql =
@"  
Select *
From (
    Select Distinct P.RecID PayTypeID, P.Code,Name = isnull(dbo.FindLocalName(P.NameLID, @Market), P.Name), 
        P.Paycat PayCatID, P.PosType,
        PayCatName = isnull(dbo.FindLocalName(PTC.NameLID, @Market), PTC.Name), 
        P.HasInstalment,
        isnull(P.InstalNumFrom, 0) InstalNumFrom, isnull(p.InstalNumTo, 0) InstalNumTo,
        B.RecID CreditCardID, P.Bank, P.PayCur, isnull(P.SupDiscode, '') SupDisCode,
        isnull(P.DiscountPer, 0) DiscountPer ,
        CreditCardName = isnull(dbo.FindLocalName(B.NameLID,@Market), B.Name),
        CreditCardCode = B.Code,
        isnull(P.ResPayMinPer, 100) ResPayMinPer,
        WPosBank = (Select Bank From BankCard K (NOLOCK) Where isnull(B.RedirectVPos, B.RecID) = K.RecID),
        isPicture = (CASE WHEN DATALENGTH(Picture) > 0 THEN Cast(1 AS bit) ELSE Cast(0 AS bit) END)
    From PayType P (NOLOCK) 
    LEFT JOIN BankCard B (NOLOCK) ON P.CreditCard = B.Code and P.Bank = B.Bank and P.PayCat = 3
    LEFT JOIN OperatorBank O (NOLOCK) ON P.Bank = O.Bank And O.Operator = @Operator
	LEFT JOIN CatPackPayType CP (NOLOCK) ON CP.PayTypeID = P.RecID
    JOIN PayTypeCat PTC (NOLOCK) ON PTC.Code = P.PayCat
    Left JOIN PayTypeDest (NOLOCK) PTD ON PTD.PayTypeID = P.RecID
    Where   Market=@plMarket 
        --And AllowWIS = 1
        --And P.PayCur=@Currency                                     
        And (@PrePerVal is null or isnull(p.InstalNumTo, 0)<=@PrePerVal)
        And @PayDate between IsNull(p.PayBegDate,@PayDate) 
        And IsNull(p.PayEndDate,@PayDate)
        And @ResBegDate between IsNull(p.ResBegDate,@ResBegDate) 
        And IsNull(p.ResEndDate,@ResBegDate)
        And @Night between IsNull(MinNight,@Night) 
        And IsNull(MaxNight,@Night) 
        And (isnull(PTD.Country,0)=0 Or PTD.Country = @Country) 
";
                if (onlyCreditCards)
                    tsql +=
@" 
        And p.Paycat=3 
";
                if (onlyAgencyPayCategory)
                    tsql +=
@"		
        --And P.PayCat in (Select PayCat FROM AgencyPasPayCat WHERE Agency=@Agency)  
";
                tsql +=
@"  ) MultiTbl 
Order By PayCatID, CreditCardID, InstalNumFrom
";
            }

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, market);
                db.AddInParameter(dbCommand, "plMarket", DbType.AnsiString, plMarket);
                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, Operator);
                db.AddInParameter(dbCommand, "Currency", DbType.AnsiString, currency);
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, catPackID);
                db.AddInParameter(dbCommand, "PayDate", DbType.DateTime, payDate);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.DateTime, resBegDate);
                db.AddInParameter(dbCommand, "Night", DbType.Int32, night);
                db.AddInParameter(dbCommand, "PrePerVal", DbType.Int32, (preperVal > -1) ? preperVal : Convert.DBNull);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, agency);
                db.AddInParameter(dbCommand, "Country", DbType.AnsiString, country);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        BIPaymentTypeData rec = new BIPaymentTypeData();

                        rec.PayTypeID = Conversion.getInt32OrNull(R["PayTypeID"]);
                        rec.Code = Conversion.getStrOrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.PayCatID = Conversion.getInt32OrNull(R["PayCatID"]);
                        rec.PosType = Conversion.getInt16OrNull(R["PosType"]);
                        rec.PayCatName = Conversion.getStrOrNull(R["PayCatName"]);
                        rec.HasInstalment = Conversion.getBoolOrNull(R["HasInstalment"]);
                        rec.InstalNumFrom = (Int16)R["InstalNumFrom"];
                        rec.InstalNumTo = (Int16)R["InstalNumTo"];
                        rec.CreditCardID = Conversion.getInt32OrNull(R["CreditCardID"]);
                        rec.Bank = Conversion.getStrOrNull(R["Bank"]);
                        rec.PayCur = Conversion.getStrOrNull(R["PayCur"]);
                        rec.SupDisCode = Conversion.getStrOrNull(R["SupDisCode"]);
                        rec.DiscountPer = Conversion.getDecimalOrNull(R["DiscountPer"]);
                        rec.CreditCardName = Conversion.getStrOrNull(R["CreditCardName"]);
                        rec.CreditCardCode = Conversion.getStrOrNull(R["CreditCardCode"]);
                        rec.ResPayMinPer = Conversion.getDecimalOrNull(R["ResPayMinPer"]);
                        rec.WPosBank = Conversion.getStrOrNull(R["WPosBank"]);
                        rec.isPicture = (bool)R["isPicture"];

                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public BICreditCardAndDetail prepareCardDetail(BIUser UserData, List<BIPaymentTypeData> paymentData, decimal? amount, string saleCur, decimal? PrePayment, decimal? disCountprice, int minPerVal, string prePerVal)
        {
            BICreditCardAndDetail retVal = new BICreditCardAndDetail();
            List<BICardDetail> cardDetail = new List<BICardDetail>();
            List<BICards> cardList = new List<BICards>();
            string errorMsg = string.Empty;
            decimal minpayper = 100;
            decimal disper = 0;
            string cardCode = string.Empty;
            foreach (BIPaymentTypeData row in paymentData.Where(w => w.PayCur == saleCur))
            //foreach (BIPaymentTypeData row in paymentData)
            {
                decimal? remain = amount;
                if (row.PayCur != saleCur)
                    remain = Exchange(UserData.Market, DateTime.Today, saleCur, row.PayCur, amount, true, ref errorMsg);

                if (row.PayCatID != 3)
                {
                    if (minPerVal < 0)
                    {
                        disper = prePerVal != "" ? (prePerVal == "0" ? row.DiscountPer.Value : 0) : row.DiscountPer.Value;
                        minpayper = prePerVal != "" ? (prePerVal == "0" ? row.ResPayMinPer.Value : 0) : row.ResPayMinPer.Value;
                    }
                    else
                    {
                        disper = 0;
                        minpayper = 0;
                    }
                    decimal Payment = Math.Round(remain.Value + (disCountprice.Value * disper / 100), 2);

                    BICardDetail card = new BICardDetail();
                    card.PayTypeID = row.PayTypeID;
                    card.Code = row.Code;
                    if (disper < 0)
                        card.FullName = row.PayCatName + " ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " discount )";
                    else
                        if (disper > 0)
                            card.FullName = row.PayCatName + " ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " supplement )";
                        else
                            card.FullName = row.PayCatName;

                    card.DiscountPrice = Payment;
                    card.Currency = row.PayCur;
                    card.Bank = row.Bank;
                    card.SupDisCode = row.SupDisCode;
                    card.InstalNum = 0;
                    card.MinPayPrice = Math.Round(Payment * minpayper / 100, 2);
                    card.PayCatID = row.PayCatID;
                    card.PrePayment = PrePayment;
                    cardDetail.Add(card);
                }
                else
                {
                    if (!string.Equals(cardCode, row.CreditCardID.ToString()))
                    {
                        BICards card = new BICards();
                        card.CreditCardID = row.CreditCardID;
                        card.CreditCardName = row.CreditCardName;
                        cardList.Add(card);
                    }
                    string taksitStr = "";
                    int possource = 1;
                    if (UserData.OwnAgency && row.PosType.HasValue)
                        possource = row.PosType.Value;

                    int forTo = minPerVal > 0 ? minPerVal : row.InstalNumTo;
                    for (int i = row.InstalNumFrom; i <= row.InstalNumTo; i++)
                    {
                        if (i > forTo) continue;
                        if (minPerVal < 0)
                        {
                            disper = prePerVal != "" ? (prePerVal == "0" ? row.DiscountPer.Value : 0) : row.DiscountPer.Value;
                            minpayper = prePerVal != "" ? (prePerVal == "0" ? row.ResPayMinPer.Value : 0) : row.ResPayMinPer.Value;
                        }
                        else
                        {
                            disper = 0;
                            minpayper = 0;
                        }
                        decimal Payment = Math.Round(remain.Value + (disCountprice.Value * disper / 100), 2);
                        if (i == 1)
                        {
                            taksitStr = " Tek Çekim ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " discount )";
                            if (disper < 0)
                                taksitStr = " Tek Çekim ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " discount )";
                            else
                                if (disper > 0)
                                    taksitStr = " Tek Çekim ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " supplement )";
                                else
                                    taksitStr = " Tek Çekim ";
                        }
                        else
                        {
                            if (disper < 0)
                                taksitStr = " " + i.ToString() + " Taksit ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " discount )";
                            else
                                if (disper > 0)
                                    taksitStr = " " + i.ToString() + " Taksit ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " supplement )";
                                else
                                    taksitStr = " " + i.ToString() + " Taksit ";
                        }
                        BICardDetail card = new BICardDetail();

                        card.PayTypeID = row.PayTypeID;
                        card.Code = row.Code;
                        card.FullName = row.CreditCardName.ToString() + taksitStr;
                        card.CreditCardID = row.CreditCardID;
                        card.DiscountPrice = Payment;
                        card.MinPayPrice = Math.Round(Payment * minpayper / 100, 2);
                        card.Currency = row.PayCur;
                        card.Bank = row.Bank;
                        card.SupDisCode = row.SupDisCode;
                        card.InstalNum = i;
                        card.PayCatID = row.PayCatID;
                        card.WPosBank = row.WPosBank;
                        card.PosType = possource;
                        card.PrePayment = PrePayment;
                        card.Selected = false;
                        cardDetail.Add(card);
                    }
                    cardCode = row.CreditCardID.ToString();
                }
            }
            retVal.CardDetailList = cardDetail;
            retVal.CardList = cardList;
            return retVal;
        }

        public BICreditCardAndDetail createCardDetail(BIUser UserData, List<BIPaymentTypeData> paymentData, BIPaymentInfo paymentInfoRes)
        {
            string errorMsg = string.Empty;
            decimal? price = paymentInfoRes.ResAmount;
            decimal? remain = paymentInfoRes.Remain;
            decimal? PrePayment = paymentInfoRes.PrePayment.HasValue ? paymentInfoRes.PrePayment.Value : 0;
            decimal? disCountprice = paymentInfoRes.DiscountAmount;
            int catPackID = paymentInfoRes.PriceListNo.HasValue ? paymentInfoRes.PriceListNo.Value : -1;
            int minPerVal = paymentInfoRes.InstalNum.HasValue ? paymentInfoRes.InstalNum.Value : -1;
            string prePerVal = paymentInfoRes.PrePerVal.ToString();
            return prepareCardDetail(UserData, paymentData, remain, paymentInfoRes.SaleCur, PrePayment, disCountprice, minPerVal, prePerVal);
        }
    }
}
