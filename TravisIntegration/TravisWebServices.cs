﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using TravisIntegration.Model;
using TravisIntegration.TravisServiceRef;
using TvBo;

namespace TravisIntegration
{
    public class TravisWebServices
    {
        public List<TISCountryInfo> getCountry()
        {
            SamanUsers sUser = TWSUser.getTWSUser();
            TravisServiceSoapClient travisWS = new TravisServiceSoapClient();
            TISCountryInfo[] countryList = travisWS.getCountries(sUser.Username, sUser.Password);
            return countryList.ToList();
        }

        public List<TISDurationOfStay> getDurationsOfStay()
        {
            SamanUsers sUser = TWSUser.getTWSUser();
            TravisServiceSoapClient travisWS = new TravisServiceSoapClient();
            TISDurationOfStay[] durationList = travisWS.getDurationsOfStay(sUser.Username, sUser.Password);
            return durationList.ToList();
        }

        public List<TISPlanInfo> getPlansWithDetail(long country, System.DateTime birthDate, int duration)
        {
            SamanUsers sUser = TWSUser.getTWSUser();
            TravisServiceSoapClient travisWS = new TravisServiceSoapClient();
            TISPlanInfo[] planDetail = travisWS.getPlansWithDetail(sUser.Username, sUser.Password, country, birthDate, duration);
            return planDetail.ToList();
        }

        public TISPriceInfo getPlanPriceDetail(long planCode, long counrty, System.DateTime birthDate, int duration)
        {
            SamanUsers sUser = TWSUser.getTWSUser();
            TravisServiceSoapClient travisWS = new TravisServiceSoapClient();

            TISPriceInfo planPriceDetail = travisWS.getPriceInquiry(sUser.Username, sUser.Password, counrty, birthDate, duration, planCode);
            return planPriceDetail;
        }
    }
}
