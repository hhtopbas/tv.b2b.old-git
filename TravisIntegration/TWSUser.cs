﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TravisIntegration.Model;
using TvBo;

namespace TravisIntegration
{
    public class TWSUser
    {
        public static SamanUsers getTWSUser() 
        { 
            List<SamanUsers> sUserL = new List<SamanUsers>();
            sUserL = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SamanUsers>>(new TvBo.Common().getFormConfigValue("Travis", "Users").ToString());
            if (sUserL != null && sUserL.Count > 0)
            {
                SamanUsers sUser = sUserL.FirstOrDefault();
                return sUser;
            }
            else
            { 
                return new SamanUsers();
            }
        }
    }
}
