﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using System.Collections;
using System.Data;

namespace TvTools
{
    public enum TNumberToTextOptions { ntoExplicitZero, ntoMinus, ntoPlus, ntoDigits, ntoNotReduceFrac }
    public enum TGender { genNeuter, genMasculine, genFeminine }
    // Род: нейтральный, мужской, женский          

    public struct TRusWord
    {
        public TGender Gender;
        public string Base;
        public string End1;
        public string End2;
        public string End5;

        public TRusWord(TGender _gender, string _base, string _end1, string _end2, string _end5)
        {
            Gender = _gender;
            Base = _base;
            End1 = _end1;
            End2 = _end2;
            End5 = _end5;
        }
    }

    public class NumberToWordRU
    {
        private TNumberAnalyser NumberAnalyser = new TNumberAnalyser();

        //Int16 MaxPrecision = 4; // до десятитысячных
        private int[] TenIn = new int[4] { 10, 100, 1000, 10000 };

        private TRusWord WD_EMPTY = new TRusWord(TGender.genMasculine, "", "", "", "");
        private TRusWord WD_THOUSEND = new TRusWord(TGender.genFeminine, "тысяч", "а", "и", "");
        private TRusWord WD_MILLION = new TRusWord(TGender.genMasculine, "миллион", "", "а", "ов");
        private TRusWord WD_MILLIARD = new TRusWord(TGender.genMasculine, "миллиард", "", "а", "ов");
        //{Дробная часть}
        private TRusWord WD_INT = new TRusWord(TGender.genFeminine, "цел", "ая", "ых", "ых");
        private TRusWord[] WD_FRAC = new TRusWord[4] 
        { 
            new TRusWord(TGender.genFeminine, "десят","ая","ых","ых"), 
            new TRusWord(TGender.genFeminine, "coт","ая","ых","ых"), 
            new TRusWord(TGender.genFeminine, "тысячн","ая","ых","ых"), 
            new TRusWord(TGender.genFeminine, "десятитысячн","ая","ых","ых") 
        };
        // {Рубли, копейки}
        //private TRusWord WD_RUBLE = new TRusWord(TGender.genMasculine, " рубл", "ь", "я", "ей");
        //private TRusWord WD_KOPECK = new TRusWord(TGender.genFeminine, " копе", "йка", "йки", "ек");

        private TRusWord WD_RUBLE = new TRusWord(TGender.genMasculine, "", "", "", "");
        private TRusWord WD_KOPECK = new TRusWord(TGender.genFeminine, "", "", "", "");


        public string CurrencyToText(decimal ASum, string CurrName, string FracName)
        {
            string result = string.Empty;
            Int64 RubSum;
            Int64 KopSum;

            RubSum = Convert.ToInt64(Math.Truncate(ASum));
            KopSum = Convert.ToInt64(Math.Round((ASum - RubSum) * 100));

            string resultRuble = CountOfUnits(WD_RUBLE, RubSum, new TNumberToTextOptions[] { TNumberToTextOptions.ntoExplicitZero, TNumberToTextOptions.ntoMinus });
            string resultKopeck = CountOfUnits(WD_KOPECK, KopSum, new TNumberToTextOptions[] { TNumberToTextOptions.ntoDigits }); // Копейки в цифрах
            result = resultRuble + " " + CurrName + (resultKopeck.Length > 0 ? " " + resultKopeck.ToString() + " " + FracName : "");
            if (result != "")
                result = result.Substring(0, 1).ToUpper() + result.Substring(1, result.Length - 1);

            return result;
        }

        public string CountOfUnits(TRusWord AUnit, Int64 N, TNumberToTextOptions[] Options)
        {
            int Mrd;
            int Mil;
            int Th;
            int Un;
            string result = string.Empty;
            if ((N == 0) && !(inTNumberToTextOptions(TNumberToTextOptions.ntoExplicitZero, Options)))
                return result;
            if (!inTNumberToTextOptions(TNumberToTextOptions.ntoDigits, Options))
            {
                if (N < 0 && inTNumberToTextOptions(TNumberToTextOptions.ntoDigits, Options))
                    result = "минус";
                else
                    if (N > 0 && inTNumberToTextOptions(TNumberToTextOptions.ntoPlus, Options))
                        result = "плюс";
                    else
                        if (N == 0)
                        {
                            result = "ноль " + AUnit.Base + AUnit.End5;
                            return result;
                        }
            }
            else
            {
                if (N < 0 && inTNumberToTextOptions(TNumberToTextOptions.ntoMinus, Options))
                    result = "-";
                else
                    if (N > 0 && inTNumberToTextOptions(TNumberToTextOptions.ntoPlus, Options))
                        result = "+";
            }

            N = Math.Abs(N);

            if (inTNumberToTextOptions(TNumberToTextOptions.ntoDigits, Options))
            {
                NumberAnalyser.Number = N;
                NumberAnalyser.UnitWord = AUnit;
                result += string.Format("{0} {1}", N.ToString(), NumberAnalyser.UnitWordInRightForm());
            }
            else
            {
                Mrd = Convert.ToInt32(Math.Truncate(N / 1000000000.0).ToString()) % 1000;
                Mil = Convert.ToInt32(Math.Truncate(N / 1000000.0).ToString()) % 1000;
                Th = Convert.ToInt32(Math.Truncate(N /  1000.0).ToString()) % 1000;
                Un = Convert.ToInt32(N) % 1000;
                result += NumberAnalyser.ConvertToText(WD_MILLIARD, Mrd)
                        + NumberAnalyser.ConvertToText(WD_MILLION, Mil)
                        + NumberAnalyser.ConvertToText(WD_THOUSEND, Th);

                if (Un > 0) result += NumberAnalyser.ConvertToText(AUnit, Un);
                else result += AUnit.Base + AUnit.End5;
            }

            return result;
        }

        private bool inTNumberToTextOptions(TNumberToTextOptions comp, TNumberToTextOptions[] Options)
        {
            foreach (TNumberToTextOptions q in Options)
                if (q == comp) return true;
            return false;
        }
    }

    public class TNumberAnalyser
    {        
        private TRusWord FUnitWord;
        private int[] Levels = new int[3];
        private Int64 FNumber;
        private int FFirstLevel;
        private int FSecondLevel;
        private int FThirdLevel;

        public int GetLevels(int i)
        {
            int retval;
            retval = FFirstLevel;
            switch (i)
            {
                case 1: retval = FFirstLevel; break;
                case 2: retval = FSecondLevel; break;
                case 3: retval = FThirdLevel; break;
                default: retval = FFirstLevel; break;
            }
            Levels[i - 1] = retval;
            return retval;
        }

        public TGender Gender
        {
            get { return GetGender(); }
        }

        //public int[] Levels
        //{
        //    get { return GetLevels(1); }
        //}
        
        public Int64 Number
        {
            get { return FNumber; }
            set { SetNumber(value); }
        }
        
        public TRusWord UnitWord
        {
            get { return this.FUnitWord; }
            set { this.FUnitWord = value; }
        }

        private void SetNumber(Int64 AValue)
        {
            if (FNumber != AValue)
            {
                FNumber = AValue;
                FFirstLevel = Convert.ToInt32((FNumber % 10).ToString());
                FSecondLevel = Convert.ToInt32(FNumber / 10) % 10;
                FThirdLevel = Convert.ToInt32(FNumber / 100) % 10;                
                if (FSecondLevel == 1)
                {
                    FFirstLevel = FFirstLevel + 10;
                    FSecondLevel = 0;
                }                
            }
        }

        public TGender GetGender()
        {
            return UnitWord.Gender;
        }

        public string GetNumberInWord(Int64 N, int Level)
        {
            string Result = string.Empty;
            if (Level == 1)
            {
                switch (N)
                {
                    case 0: Result = string.Empty; break;
                    case 1: if (Gender == TGender.genMasculine) Result = "один";
                        else if (Gender == TGender.genFeminine) Result = "одна";
                        else if (Gender == TGender.genNeuter) Result = "одно";
                        break;
                    case 2: if (Gender == TGender.genMasculine) Result = "два";
                        else if (Gender == TGender.genFeminine) Result = "две";
                        else if (Gender == TGender.genNeuter) Result = "два";
                        break;
                    case 3: Result = "три"; break;
                    case 4: Result = "четыре"; break;
                    case 5: Result = "пять"; break;
                    case 6: Result = "шесть"; break;
                    case 7: Result = "семь"; break;
                    case 8: Result = "восемь"; break;
                    case 9: Result = "девять"; break;
                    case 10: Result = "десять"; break;
                    case 11: Result = "одиннадцать"; break;
                    case 12: Result = "двенадцать"; break;
                    case 13: Result = "тринадцать"; break;
                    case 14: Result = "четырнадцать"; break;
                    case 15: Result = "пятнадцать"; break;
                    case 16: Result = "шестнадцать"; break;
                    case 17: Result = "семнадцать"; break;
                    case 18: Result = "восемнадцать"; break;
                    case 19: Result = "девятнадцать"; break;
                }
            }
            else
                if (Level == 2)
                {
                    switch (N)
                    {
                        case 0: Result = ""; break;
                        case 1: Result = "десять"; break;
                        case 2: Result = "двадцать"; break;
                        case 3: Result = "тридцать"; break;
                        case 4: Result = "сорок"; break;
                        case 5: Result = "пятьдесят"; break;
                        case 6: Result = "шестьдесят"; break;
                        case 7: Result = "семьдесят"; break;
                        case 8: Result = "восемьдесят"; break;
                        case 9: Result = "девяносто"; break;
                    }
                }
                else if (Level == 3)
                {
                    switch (N)
                    {
                        case 0: Result = ""; break;
                        case 1: Result = "сто"; break;
                        case 2: Result = "двести"; break;
                        case 3: Result = "триста"; break;
                        case 4: Result = "четыреста"; break;
                        case 5: Result = "пятьсот"; break;
                        case 6: Result = "шестьсот"; break;
                        case 7: Result = "семьсот"; break;
                        case 8: Result = "восемьсот"; break;
                        case 9: Result = "девятьсот"; break;
                    }
                }
            return Result;
        }

        public string UnitWordInRightForm()
        {
            string Result = UnitWord.Base;
            if (Levels[0] == 0 || (Levels[0] >= 5 && Levels[0] <= 9))
                Result += UnitWord.End5;
            else if (Levels[0] == 1)
                Result += UnitWord.End1;
            else if (Levels[0] >= 2 && Levels[0] <= 4)
                Result += UnitWord.End2;
            /*
            if (Convert.ToInt16(_Number.ToString().Substring(_Levels - 1, 1)) == 0 ||
                (Convert.ToInt16(_Number.ToString().Substring(_Levels - 1, 1)) >= 5 && Convert.ToInt16(_Number.ToString().Substring(_Levels - 1, 1)) <= 9))
                Result += _UnitWord.End5;
            else if (Convert.ToInt16(_Number.ToString().Substring(_Levels - 1, 1)) == 1)
                Result += _UnitWord.End1;
            else if (Convert.ToInt16(_Number.ToString().Substring(_Levels - 1, 1)) >= 2 && Convert.ToInt16(_Number.ToString().Substring(_Levels - 1, 1)) <= 4)
                Result += _UnitWord.End2;
             */
            return Result;
        }

        public string _Convert()
        {
            string s;
            string Result = string.Empty;
            string[] resultStr = new string[3] { "", "", "" };
            if (Number == 0)
                return Result;
            else
            {
                SetNumber(Number);
                for (int i = 3; i > 0; i--)
                {
                    //_Levels = i;
                    s = GetNumberInWord(GetLevels(i), i);
                    if (!string.IsNullOrEmpty(s))
                    {
                        Result += s + " ";
                    }
                }

                Result = Result + UnitWordInRightForm() + " ";
            }

            return Result;
        }

        public string ConvertToText(TRusWord AUnit, int ANumber)
        {
            UnitWord = AUnit;
            Number = ANumber;            
            return _Convert();
        }
    }


}
