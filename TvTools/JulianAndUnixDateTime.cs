using System;
namespace TvTools
{
	public static class DateTimeExtensions {
		public static double ToJulianDay(this DateTime dt) {
			return dt.ToOADate() + 2415018.5;
		}
		private static DateTime unixBase = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		public static double ToUnixTime(this DateTime dt) {
			return dt.Subtract(unixBase).TotalSeconds;
		}

		public static DateTime FromJulianDay(double julianDay) {
			return DateTime.FromOADate(julianDay - 2415018.5D);
		}

		public static DateTime FromUnixTime(double unixTime) {
			return unixBase.AddSeconds(unixTime);
		}        
	}
}