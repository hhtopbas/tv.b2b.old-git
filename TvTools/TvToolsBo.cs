﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TvTools
{
    public class objectList
    {
        public objectList()
        { 
        }

        string _TypeName;
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }

        object _Value;
        public object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
    }
}
