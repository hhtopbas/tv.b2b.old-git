﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Globalization;
using Newtonsoft.Json;

namespace TvTools
{
    public class UTCDateTimeConverter : Newtonsoft.Json.JsonConverter
    {
        private TimeZoneInfo pacificZone = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null) return null;
            var pacificTime = DateTime.Parse(reader.Value.ToString());
            return TimeZoneInfo.ConvertTimeToUtc(pacificTime, pacificZone);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(TimeZoneInfo.ConvertTimeFromUtc((DateTime)value, pacificZone));
        }
    }

    public static class DateTimeFunc
    {
        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        public static Int16? getAge(DateTime? birthdate, DateTime? refDate)
        {
            if (!birthdate.HasValue || !refDate.HasValue) return null;
            int years = refDate.Value.Year - birthdate.Value.Year;
            int month = refDate.Value.Month - birthdate.Value.Month;
            int day = refDate.Value.Day - birthdate.Value.Day;

            if (month < 0 || (month == 0 && day < 0))
                years--;
            return Convert.ToInt16(years);
        }

        public static bool CompareDate(DateTime? comp1, DateTime? comp2)
        {
            if (comp1.HasValue && comp2.HasValue) {
                return comp1.Value.Year == comp2.Value.Year && comp1.Value.Month == comp2.Value.Month && comp1.Value.Day == comp2.Value.Day;
            } else {
                return false;
            }
        }

    }

    public static class strFunc
    {
        public static string Trim(string input, char trimChar)
        {
            if (string.IsNullOrEmpty(input)) return input;
            string retInput = string.Empty;
            foreach (char r in input)
                if (r != trimChar) retInput += r;
            return retInput;
        }

        public static string ConvertTrToUtf(string pInput)
        {

            return pInput;
        }
    }

    public enum numericSeperator { Dot = 0, Comma = 1 }

    public static class Conversion
    {
        public static byte[] GetBytes(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }

        public static string GetString(byte[] bytes)
        {
            return System.Text.Encoding.UTF8.GetString(bytes);
        }

        public static string getStrOrNull(object InputVal)
        {
            string retVal = string.Empty;
            if (InputVal == null) return retVal;
            if (string.Equals(InputVal.GetType().Name, "String") && string.IsNullOrEmpty(InputVal.ToString())) return retVal;
            if (InputVal != DBNull.Value && InputVal != null)
                retVal = Convert.ToString(InputVal);
            return retVal;
        }

        public static Byte? getByteOrNull(object InputVal)
        {
            Byte? retVal = null;
            if (InputVal == null) return null;
            if (string.Equals(InputVal.GetType().Name, "String") && string.IsNullOrEmpty(InputVal.ToString())) return retVal;
            if (InputVal != DBNull.Value && InputVal != null)
                retVal = Convert.ToByte(InputVal);
            return retVal;
        }

        public static Int16? getInt16OrNull(object InputVal)
        {
            Int16? retVal = null;
            if (InputVal == null) return null;
            if (string.Equals(InputVal.GetType().Name, "String") && string.IsNullOrEmpty(InputVal.ToString())) return retVal;
            if (InputVal != DBNull.Value && InputVal != null)
            {
                try { retVal = Convert.ToInt16(InputVal); }
                catch { retVal = null; }
            }

            return retVal;
        }

        public static Int32? getInt32OrNull(object InputVal)
        {
            Int32? retVal = null;
            if (InputVal == null) return null;
            if (string.Equals(InputVal.GetType().Name, "String") && string.IsNullOrEmpty(InputVal.ToString())) return retVal;
            if (InputVal != DBNull.Value && InputVal != null)
            {
                try { retVal = Convert.ToInt32(InputVal); }
                catch { retVal = null; }
            }
            return retVal;
        }

        public static Int64? getInt64OrNull(object InputVal)
        {
            Int64? retVal = null;
            if (InputVal == null) return null;
            if (string.Equals(InputVal.GetType().Name, "String") && string.IsNullOrEmpty(InputVal.ToString())) return retVal;
            if (InputVal != DBNull.Value && InputVal != null)
                retVal = Convert.ToInt64(InputVal);

            return retVal;
        }

        public static DateTime? getDateTimeOrNull(object InputVal)
        {
            DateTime? retVal = null;
            if (InputVal == null) return null;
            if (string.Equals(InputVal.GetType().Name, "String") && string.IsNullOrEmpty(InputVal.ToString())) return retVal;
            if (InputVal != DBNull.Value && InputVal != null)
                try
                {
                    retVal = Convert.ToDateTime(InputVal);
                }
                catch
                {
                    retVal = null;
                }
            return retVal;
        }

        public static bool? getBoolOrNull(object InputVal)
        {
            bool? retVal = null;
            if (InputVal == null) return null;
            if (string.Equals(InputVal.GetType().Name, "String") && string.IsNullOrEmpty(InputVal.ToString())) return retVal;
            if (InputVal != DBNull.Value && InputVal != null)
                try { retVal = Convert.ToBoolean(InputVal); }
                catch { retVal = null; }
            return retVal;
        }

        public static decimal? getDecimalOrNull(object InputVal)
        {
            decimal? retVal = null;
            if (InputVal == null) return null;
            if (string.Equals(InputVal.GetType().Name, "String") && string.IsNullOrEmpty(InputVal.ToString())) return retVal;
            if (InputVal != DBNull.Value && InputVal != null)
                try { retVal = Convert.ToDecimal(InputVal); }
                catch { retVal = null; }
            return retVal;
        }

        public static char? getCharOrNull(object InputVal)
        {
            char? retVal = null;
            if (string.Equals(InputVal.GetType().Name, "String") && string.IsNullOrEmpty(InputVal.ToString())) return retVal;
            if (InputVal != DBNull.Value && InputVal != null)
                retVal = Convert.ToChar(InputVal);

            return retVal;
        }

        public static object getObject(object obj, string type)
        {
            object retVal = null;
            switch (type.ToLower())
            {
                case "int64": retVal = (Int64?)getInt64OrNull(obj); break;
                case "int32": retVal = (Int32?)getInt32OrNull(obj); break;
                case "int16": retVal = (Int16?)getInt16OrNull(obj); break;
                case "byte": retVal = (Int16?)getByteOrNull(obj); break;
                case "string": retVal = (string)getStrOrNull(obj); break;
                case "bool": retVal = (bool?)getBoolOrNull(obj); break;
                case "decimal": retVal = (decimal)getDecimalOrNull(obj); break;
                case "datetime": retVal = (DateTime)getDateTimeOrNull(obj); break;
            }
            return retVal;
        }

        public static string getObjectToString(object obj, Type type, string format)
        {
            string retVal = null;
            switch (type.Name.ToLower())
            {
                case "int64": retVal = getInt64OrNull(obj).HasValue ? getInt64OrNull(obj).Value.ToString(format) : ""; break;
                case "int32": retVal = getInt32OrNull(obj).HasValue ? getInt32OrNull(obj).Value.ToString(format) : ""; break;
                case "int16": retVal = getInt16OrNull(obj).HasValue ? getInt16OrNull(obj).Value.ToString(format) : ""; break;
                case "byte": retVal = getByteOrNull(obj).HasValue ? getByteOrNull(obj).Value.ToString(format) : ""; break;
                case "string": retVal = getStrOrNull(obj).ToString(); break;
                case "bool": retVal = getBoolOrNull(obj).ToString(); break;
                case "decimal": retVal = getDecimalOrNull(obj).HasValue ? getDecimalOrNull(obj).Value.ToString(format) : ""; break;
                case "datetime":
                    if (getDateTimeOrNull(obj).HasValue)
                    {
                        DateTime _date = getDateTimeOrNull(obj).Value;
                        retVal = string.Equals(format, "date") ? _date.ToShortDateString() : (string.Equals(format, "datetime") ? (_date.ToShortDateString() + " " + _date.ToShortTimeString()) : _date.ToString());
                    }
                    else retVal = "";
                    break;
            }
            return retVal;
        }

        public static string getObjectToString(object obj, Type type)
        {
            string retVal = null;
            switch (type.Name.ToLower())
            {
                case "int64": retVal = getInt64OrNull(obj).HasValue ? getInt64OrNull(obj).Value.ToString("#,###") : ""; break;
                case "int32": retVal = getInt32OrNull(obj).HasValue ? getInt32OrNull(obj).Value.ToString("#,###") : ""; break;
                case "int16": retVal = getInt16OrNull(obj).HasValue ? getInt16OrNull(obj).Value.ToString() : ""; break;
                case "byte": retVal = getByteOrNull(obj).HasValue ? getByteOrNull(obj).Value.ToString() : ""; break;
                case "string": retVal = getStrOrNull(obj).ToString(); break;
                case "bool": retVal = getBoolOrNull(obj).ToString(); break;
                case "decimal": retVal = getDecimalOrNull(obj).HasValue ? getDecimalOrNull(obj).Value.ToString("#,###.00") : ""; break;
                case "datetime":
                    if (getDateTimeOrNull(obj).HasValue)
                    {
                        DateTime _date = getDateTimeOrNull(obj).Value;
                        retVal = _date.ToShortDateString();
                        if ((_date - _date.Date).Ticks > 0)
                            retVal = retVal + " " + _date.ToShortTimeString();
                    }
                    else retVal = "";
                    break;
            }
            return retVal;
        }

        public static DateTime? convertDateTime(string date, string dateFormat)
        {
            char[] sperator = new char[] { '.', ',', '/', '-', '_', ' '};
            try
            {
                if (sperator.Contains(dateFormat[dateFormat.Length - 1]))
                    dateFormat = dateFormat.Substring(0, dateFormat.Length - 1);

                DateTime Date = DateTime.ParseExact(date, dateFormat.Replace('m', 'M'), CultureInfo.InvariantCulture);

                /*string seperator = string.Empty;
                if (dateFormat.ToLower().IndexOf('y') > 0)
                    seperator = dateFormat.Substring(dateFormat.ToLower().IndexOf('y') - 1, 1);
                else
                    if (dateFormat.ToLower().IndexOf('m') > 0)
                        seperator = dateFormat.Substring(dateFormat.ToLower().IndexOf('m') - 1, 1);
                    else
                        if (dateFormat.ToLower().IndexOf('d') > 0)
                            seperator = dateFormat.Substring(dateFormat.ToLower().IndexOf('d') - 1, 1);

                if (dateFormat[dateFormat.Length - 1].ToString() == seperator.ToString())
                    dateFormat = dateFormat.Substring(0, dateFormat.Length - 1);

                string[] dates = date.Split(seperator[0]);
                string[] dateFormats = dateFormat.Split(seperator[0]);
                int year = DateTime.Today.Year;
                int month = DateTime.Today.Month;
                int day = DateTime.Today.Day;
                for (int i = 0; i < dateFormats.Length; i++)
                {
                    switch (dateFormats[i].Substring(0, 1))
                    {
                        case "y":
                            year = Convert.ToInt32(dates[i]);
                            break;
                        case "m":
                        case "M":
                            month = Convert.ToInt32(dates[i]);
                            break;                                                    
                        case "d":
                            day = Convert.ToInt32(dates[i]);
                            break;
                    }
                }

                DateTime Date = new DateTime(year, month, day, 0, 0, 0, 0);*/
                return DateTime.SpecifyKind(Date, DateTimeKind.Utc);
            }
            catch
            {
                return null;
            }
        }

        public static long? ConvertToJulian(DateTime? dt)
        {
            if (!dt.HasValue) return null;
            int m = dt.Value.Month;
            int d = dt.Value.Day;
            int y = dt.Value.Year;
            if (m < 3)
            {
                m = m + 12;
                y = y - 1;
            }
            long jd = d + (153 * m - 457) / 5 + 365 * y + (y / 4) - (y / 100) + (y / 400) + 1721119;
            return jd;
        }

        public static DateTime? ConvertFromJulian(long? m_JulianDate)
        {
            if (!m_JulianDate.HasValue) return null;
            long L = m_JulianDate.Value + 68569;
            long N = (long)((4 * L) / 146097);
            L = L - ((long)((146097 * N + 3) / 4));
            long I = (long)((4000 * (L + 1) / 1461001));
            L = L - (long)((1461 * I) / 4) + 31;
            long J = (long)((80 * L) / 2447);
            int Day = (int)(L - (long)((2447 * J) / 80));
            L = (long)(J / 11);
            int Month = (int)(J + 2 - 12 * L);
            int Year = (int)(100 * (N - 49) + I + L);
            DateTime dt = new DateTime(Year, Month, Day, 0, 0, 0);
            return dt;
        }
        /// <summary>
        /// YYYY-MM-DD olarak alır
        /// </summary>
        /// <param name="pDate"></param>
        /// <returns></returns>
        public static DateTime? ConvertFromString(string pDate)
        {
            if (string.IsNullOrEmpty(pDate)) return null;
            string[] arrDates = pDate.Split('-');
            DateTime dt = new DateTime(Convert.ToInt32(arrDates[0]), Convert.ToInt32(arrDates[1]), Convert.ToInt32(arrDates[2]), 0, 0, 0);
            return dt;
        }

        public static DateTime? getJsDateTimeToDateTime(long? jsSeconds)
        {
            DateTime? dateTime = null;
            DateTime? unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
            if (jsSeconds.HasValue)
                dateTime = unixEpoch.Value.AddMilliseconds(jsSeconds.Value);
            return dateTime;
        }

        public static long? getDateTimeToJsDateTime(DateTime? date)
        {
            if (!date.HasValue) return null;
            return (date.Value.Ticks - 621355968000000000) / 1000;
        }

        public static string ConvertToDateString(DateTime date)
        {
            return string.Format("{0}-{1}-{2}", date.Year, date.Month, date.Day);
        }
    }

    public static class WebConfigLib
    {

    }

    public static class mathLib
    {
        public static object returnFormulaMinus(List<objectList> objects, ref Type _type)
        {
            object retVal = null;
            var query = from q in objects
                        group q by new { q.TypeName } into k
                        select new { k.Key.TypeName };
            bool _isDecimal = query.Where(w => string.Equals(w.TypeName, typeof(System.Decimal).Name)).Count() > 0;
            bool _isInt64 = query.Where(w => string.Equals(w.TypeName, typeof(System.Int64).Name)).Count() > 0;
            bool _isInt32 = query.Where(w => string.Equals(w.TypeName, typeof(System.Int32).Name)).Count() > 0;
            bool _isInt16 = query.Where(w => string.Equals(w.TypeName, typeof(System.Int16).Name)).Count() > 0;
            bool _isByte = query.Where(w => string.Equals(w.TypeName, typeof(System.Byte).Name)).Count() > 0;
            bool _isSingle = query.Where(w => string.Equals(w.TypeName, typeof(System.Single).Name)).Count() > 0;

            _type = _isDecimal ? typeof(System.Decimal) :
                                 (_isSingle ? typeof(System.Single) :
                                 (_isInt64 ? typeof(System.Int64) :
                                 (_isInt32 ? typeof(System.Int32) :
                                 (_isInt16 ? typeof(System.Int16) :
                                 (_isByte ? typeof(System.Byte) : typeof(System.String))))));
            if (objects.Count == 2)
            {
                switch (_type.Name)
                {
                    case "Decimal":
                        decimal firstDecimal = Conversion.getDecimalOrNull(objects.FirstOrDefault().Value.ToString()).HasValue ? Conversion.getDecimalOrNull(objects.FirstOrDefault().Value.ToString()).Value : Convert.ToDecimal(0);
                        decimal secondDecimal = Conversion.getDecimalOrNull(objects.LastOrDefault().Value.ToString()).HasValue ? Conversion.getDecimalOrNull(objects.LastOrDefault().Value.ToString()).Value : Convert.ToDecimal(0);
                        retVal = firstDecimal - secondDecimal;
                        break;
                    case "Single":
                        decimal firstSingle = Conversion.getDecimalOrNull(objects.FirstOrDefault().Value.ToString()).HasValue ? Conversion.getDecimalOrNull(objects.FirstOrDefault().Value.ToString()).Value : Convert.ToDecimal(0);
                        decimal secondSingle = Conversion.getDecimalOrNull(objects.LastOrDefault().Value.ToString()).HasValue ? Conversion.getDecimalOrNull(objects.LastOrDefault().Value.ToString()).Value : Convert.ToDecimal(0);
                        retVal = firstSingle - secondSingle;
                        break;
                    case "Int64":
                        Int64 firstInt64 = Conversion.getInt64OrNull(objects.FirstOrDefault().Value.ToString()).HasValue ? Conversion.getInt64OrNull(objects.FirstOrDefault().Value.ToString()).Value : Convert.ToInt64(0);
                        Int64 secondInt64 = Conversion.getInt64OrNull(objects.LastOrDefault().Value.ToString()).HasValue ? Conversion.getInt64OrNull(objects.LastOrDefault().Value.ToString()).Value : Convert.ToInt64(0);
                        retVal = firstInt64 - secondInt64;
                        break;
                    case "Int32":
                        Int32 firstInt32 = Conversion.getInt32OrNull(objects.FirstOrDefault().Value.ToString()).HasValue ? Conversion.getInt32OrNull(objects.FirstOrDefault().Value.ToString()).Value : Convert.ToInt32(0);
                        Int32 secondInt32 = Conversion.getInt32OrNull(objects.LastOrDefault().Value.ToString()).HasValue ? Conversion.getInt32OrNull(objects.LastOrDefault().Value.ToString()).Value : Convert.ToInt32(0);
                        retVal = firstInt32 - secondInt32;
                        break;
                    case "Int16":
                        Int16 firstInt16 = Conversion.getInt16OrNull(objects.FirstOrDefault().Value.ToString()).HasValue ? Conversion.getInt16OrNull(objects.FirstOrDefault().Value.ToString()).Value : Convert.ToInt16(0);
                        Int16 secondInt16 = Conversion.getInt16OrNull(objects.LastOrDefault().Value.ToString()).HasValue ? Conversion.getInt16OrNull(objects.LastOrDefault().Value.ToString()).Value : Convert.ToInt16(0);
                        retVal = firstInt16 - secondInt16;
                        break;
                    case "Byte":
                        byte firstByte = Conversion.getByteOrNull(objects.FirstOrDefault().Value.ToString()).HasValue ? Conversion.getByteOrNull(objects.FirstOrDefault().Value.ToString()).Value : Convert.ToByte(0);
                        byte secondByte = Conversion.getByteOrNull(objects.LastOrDefault().Value.ToString()).HasValue ? Conversion.getByteOrNull(objects.LastOrDefault().Value.ToString()).Value : Convert.ToByte(0);
                        retVal = firstByte - secondByte;
                        break;
                    default: retVal = string.Empty; break;
                }
            }
            return retVal;
        }

        public static object returnFormulaPlus(List<objectList> objects, ref Type _type)
        {
            object retVal = null;
            var query = from q in objects
                        group q by new { q.TypeName } into k
                        select new { k.Key.TypeName };
            bool _isDecimal = query.Where(w => string.Equals(w.TypeName, typeof(System.Decimal).Name)).Count() > 0;
            bool _isInt64 = query.Where(w => string.Equals(w.TypeName, typeof(System.Int64).Name)).Count() > 0;
            bool _isInt32 = query.Where(w => string.Equals(w.TypeName, typeof(System.Int32).Name)).Count() > 0;
            bool _isInt16 = query.Where(w => string.Equals(w.TypeName, typeof(System.Int16).Name)).Count() > 0;
            bool _isByte = query.Where(w => string.Equals(w.TypeName, typeof(System.Byte).Name)).Count() > 0;
            bool _isSingle = query.Where(w => string.Equals(w.TypeName, typeof(System.Single).Name)).Count() > 0;

            _type = _isDecimal ? typeof(System.Decimal) :
                                 (_isSingle ? typeof(System.Single) :
                                 (_isInt64 ? typeof(System.Int64) :
                                 (_isInt32 ? typeof(System.Int32) :
                                 (_isInt16 ? typeof(System.Int16) :
                                 (_isByte ? typeof(System.Byte) : typeof(System.String))))));

            switch (_type.Name)
            {
                case "Decimal":
                    var qDecimal = from q in objects
                                   select new { result = (Conversion.getDecimalOrNull(q.Value.ToString()).HasValue ? Conversion.getDecimalOrNull(q.Value.ToString()).Value : Convert.ToDecimal(0)) };
                    retVal = qDecimal.Sum(s => s.result);
                    break;
                case "Single":
                    var qSingle = from q in objects
                                  select new { result = (Conversion.getDecimalOrNull(q.Value.ToString()).HasValue ? Conversion.getDecimalOrNull(q.Value.ToString()).Value : Convert.ToDecimal(0)) };
                    retVal = qSingle.Sum(s => s.result);
                    break;
                case "Int64":
                    var qInt64 = from q in objects
                                 select new { result = (Conversion.getInt64OrNull(q.Value.ToString()).HasValue ? Conversion.getInt64OrNull(q.Value.ToString()).Value : Convert.ToDecimal(0)) };
                    retVal = qInt64.Sum(s => s.result);
                    break;
                case "Int32":
                    var qInt32 = from q in objects
                                 select new { result = (Conversion.getInt32OrNull(q.Value.ToString()).HasValue ? Conversion.getInt32OrNull(q.Value.ToString()).Value : Convert.ToDecimal(0)) };
                    retVal = qInt32.Sum(s => s.result);
                    break;
                case "Int16":
                    var qInt16 = from q in objects
                                 select new { result = (Conversion.getInt16OrNull(q.Value.ToString()).HasValue ? Conversion.getInt16OrNull(q.Value.ToString()).Value : Convert.ToDecimal(0)) };
                    retVal = qInt16.Sum(s => s.result);
                    break;
                case "Byte":
                    var qByte = from q in objects
                                select new { result = (Conversion.getByteOrNull(q.Value.ToString()).HasValue ? Conversion.getByteOrNull(q.Value.ToString()).Value : Convert.ToDecimal(0)) };
                    retVal = qByte.Sum(s => s.result);
                    break;
                default: retVal = string.Empty; break;
            }
            return retVal;
        }
    }

    public static class imgTools
    {
        public static string getTempImageUrl(byte[] img, string imgName, string tempDir, string tempDirUrl)
        {

            return "";
        }
    }

    public static class GZipCompres
    {
        public static string Compress(string text)
        {
            byte[] buffer = Encoding.Unicode.GetBytes(text);
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            using (System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, true))
            {
                zip.Write(buffer, 0, buffer.Length);
            }
            ms.Position = 0;
            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);
            byte[] gzBuffer = new byte[compressed.Length + 4];
            System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
            return Convert.ToBase64String(gzBuffer);
        }

        public static string Decompress(string compressedText)
        {
            byte[] gzBuffer = Convert.FromBase64String(compressedText);
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                int msgLength = BitConverter.ToInt32(gzBuffer, 0);
                ms.Write(gzBuffer, 4, gzBuffer.Length - 4);
                byte[] buffer = new byte[msgLength];
                ms.Position = 0;
                using (System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Decompress))
                {
                    zip.Read(buffer, 0, buffer.Length);
                }
                return Encoding.Unicode.GetString(buffer);
            }
        }
    }
}
