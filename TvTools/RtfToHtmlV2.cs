﻿using MarkupConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvTools
{
    public class RtfToHtmlV2
    {
        private IMarkupConverter markupConverter;
        public RtfToHtmlV2()
        {
            markupConverter = new MarkupConverter.MarkupConverter();
        }
        public string convertRtfToHtml(string rtfString)
        {
            return markupConverter.ConvertRtfToHtml(rtfString);
        }
    }
}
