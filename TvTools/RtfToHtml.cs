﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TvTools
{
    public class RTFtoHTML
    {
        private System.Windows.Forms.RichTextBox _rtfSource = new System.Windows.Forms.RichTextBox();

        public RTFtoHTML()
        {            
        }

        public string rtf
        {
            get { return _rtfSource.Rtf; }
            set { _rtfSource.Rtf = value; }
        }

        public string html()
        {            
            return GetHtml();
        }

        private string HtmlColorFromColor(System.Drawing.Color clr)
        {
            string strReturn = string.Empty;
            if (clr.IsNamedColor)
                strReturn = clr.Name.ToLower();
            else
            {
                strReturn = clr.Name;
                if (strReturn.Length > 6)
                    strReturn = strReturn.Substring(strReturn.Length - 6, 6);
                strReturn = "#" + strReturn;
            }
            return strReturn;
        }

        private string HtmlFontStyleFromFont(System.Drawing.Font fnt)
        {
            string strReturn = string.Empty;

            //  style 
            if (fnt.Italic)
                strReturn += "italic ";
            //else
            //    strReturn += "normal ";
            // variant
            //strReturn += "normal ";

            // weight
            if (fnt.Bold)
                strReturn += "bold ";
            //else
            //    strReturn += "normal ";

            // size
            strReturn += fnt.SizeInPoints.ToString() + "pt/normal ";

            // family
            strReturn += fnt.FontFamily.Name;

            return strReturn;
        }

        private string getHtmlStyle(System.Windows.Forms.HorizontalAlignment aling, System.Drawing.Font font, System.Drawing.Color backColor, System.Drawing.Color foreColor)
        {
            string htmlStyle = string.Empty;
            string htmlStyleFormat = "text-align:{0}; color:{1}; background-color:{2}; font-family:{3}; font-size:{4}; font-style:{5}; font-weight:{6}";
            htmlStyle = string.Format(htmlStyleFormat,
                                        aling.ToString().ToLower(),
                                        HtmlColorFromColor(foreColor),
                                        HtmlColorFromColor(backColor),
                                        font.OriginalFontName,
                                        font.SizeInPoints.ToString() + "pt",
                                        font.Italic ? "italic" : "normal",
                                        font.Bold ? "bold" : "normal");
            return htmlStyle;
        }

        private string getHtmlStyleNoAlign(System.Drawing.Font font, System.Drawing.Color backColor, System.Drawing.Color foreColor)
        {
            string htmlStyle = string.Empty;
            string htmlStyleFormat = "{0} color:{1}; background-color:{2}; font-family:{3}; font-size:{4}; font-style:{5}; font-weight:{6}";
            htmlStyle = string.Format(htmlStyleFormat,                                      
                                        "",
                                        HtmlColorFromColor(foreColor),
                                        HtmlColorFromColor(backColor),
                                        font.OriginalFontName,
                                        font.SizeInPoints.ToString() + "pt",
                                        font.Italic ? "italic" : "normal",
                                        font.Bold ? "bold" : "normal");
            return htmlStyle;
        }

        private string GetHtml()
        {
            string strReturn = "<div>";
            System.Drawing.Color clrForeColor = System.Drawing.Color.Black;
            System.Drawing.Color clrBackColor = System.Drawing.Color.Transparent;
            System.Drawing.Font fntCurrentFont = _rtfSource.Font;
            System.Windows.Forms.HorizontalAlignment altCurrent = System.Windows.Forms.HorizontalAlignment.Left;
            
            for (int intPos = 0; intPos < _rtfSource.Text.Length; intPos++)
            {
                _rtfSource.Select(intPos, 1);
                
                #region Alignment
                if (intPos == 0)
                {
                    strReturn += "<p style=\"text-align:" + _rtfSource.SelectionAlignment.ToString() + "\">";
                    altCurrent = _rtfSource.SelectionAlignment;
                }
                else
                {
                    if (_rtfSource.SelectionAlignment != altCurrent)
                    {
                        strReturn += "</p>";
                        strReturn += "<p style=\"text-align:" + _rtfSource.SelectionAlignment.ToString() + "\">";
                        altCurrent = _rtfSource.SelectionAlignment;
                    }
                }
                #endregion Alignment                
                if (intPos == 0)
                {
                    strReturn += "<span style=\"" + getHtmlStyleNoAlign(_rtfSource.SelectionFont, _rtfSource.SelectionBackColor, _rtfSource.SelectionColor) + "\">";
                    fntCurrentFont = _rtfSource.SelectionFont;
                    clrBackColor = _rtfSource.SelectionBackColor;
                    clrForeColor = _rtfSource.SelectionColor;
                }
                else
                {
                    if (_rtfSource.SelectionFont.GetHashCode() != fntCurrentFont.GetHashCode() || _rtfSource.SelectionColor != clrForeColor || _rtfSource.SelectionBackColor != clrBackColor)
                    {
                        strReturn += "</span>";
                        strReturn += "<span style=\"" + getHtmlStyleNoAlign(_rtfSource.SelectionFont, _rtfSource.SelectionBackColor, _rtfSource.SelectionColor) + "\">";
                        fntCurrentFont = _rtfSource.SelectionFont;
                        clrBackColor = _rtfSource.SelectionBackColor;
                        clrForeColor = _rtfSource.SelectionColor;
                    }
                }

                #region
                /*
                #region  Fore color
                if (intPos == 0)
                {
                    strReturn += "<span style=\"color:" + HtmlColorFromColor(_rtfSource.SelectionColor) + "\">";
                    clrForeColor = _rtfSource.SelectionColor;
                }
                else
                {
                    if (_rtfSource.SelectionColor != clrForeColor)
                    {
                        strReturn += "</span>";
                        strReturn += "<span style=\"color:" + HtmlColorFromColor(_rtfSource.SelectionColor) + "\">";
                        clrForeColor = _rtfSource.SelectionColor;
                    }
                }
                #endregion  Fore color

                #region Font Type
                if (intPos == 0)
                {
                    strReturn += "<span style=\"font:" + HtmlFontStyleFromFont(_rtfSource.SelectionFont) + "\">";
                    fntCurrentFont = _rtfSource.SelectionFont;
                }
                else
                {
                    if (_rtfSource.SelectionFont.GetHashCode() != fntCurrentFont.GetHashCode())
                    {
                        strReturn += "</span>";
                        strReturn += "<span style=\"font:" + HtmlFontStyleFromFont(_rtfSource.SelectionFont) + "\">";
                        fntCurrentFont = _rtfSource.SelectionFont;
                    }
                }
                #endregion Font Type

                #region Background color
                if (intPos == 0)
                {
                    strReturn += "<span style=\"background-color:" + HtmlColorFromColor(_rtfSource.SelectionBackColor) + "\">";
                    clrBackColor = _rtfSource.SelectionBackColor;
                }
                else
                {
                    if (_rtfSource.SelectionBackColor != clrBackColor)
                    {
                        strReturn += "</span>";
                        strReturn += "<span style=\"background-color:" + HtmlColorFromColor(_rtfSource.SelectionBackColor) + "\">";
                        clrBackColor = _rtfSource.SelectionBackColor;
                    }
                }
                #endregion Background color
                */
                #endregion

                strReturn += _rtfSource.Text.Substring(intPos, 1);
            }

            strReturn += "</span>";
            //strReturn += "</span>";
            strReturn += "</p>";
            strReturn += "</div>";

            strReturn = strReturn.Replace(Convert.ToChar(10).ToString(), "<br />").Replace("MS Sans Serif","sans-serif");;

            return strReturn;
        }
    }
}