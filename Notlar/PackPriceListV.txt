﻿USE [TVBS2015]
GO

/****** Object:  Index [I9_PackPriceList_B2B]    Script Date: 10.02.2016 18:25:36 ******/
CREATE NONCLUSTERED INDEX [I9_PackPriceList_B2B] ON [dbo].[PackPriceList]
(
	[CheckIn] ASC,
	[HolPackID] ASC,
	[DepCity] ASC,
	[ArrCity] ASC,
	[HotelID] ASC,
	[RoomID] ASC,
	[AccomID] ASC,
	[BoardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO