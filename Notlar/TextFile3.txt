﻿1:	  using System;
2:	  using System.IO;
3:	 
4:	  using iTextSharp.text;
5:	  using iTextSharp.text.pdf;
6:	 
7:	  namespace iTextSharp.tutorial.Chap13
8:	  {
9:	      /// <summary>
10:	      /// Chap13_form ժҪ˵
11:	      /// </summary>
12:	      public class Chap13_form
13:	      {
14:	          public Chap13_form()
15:	          {
16:	         Console.WriteLine("Chapter 13: a form with some text");
17:	 
18:	          // step 1: creation of a document-object
19:	          Document document = new Document(PageSize.A4, 50, 50, 50, 50);
20:	          try {
21:	              // step 2: we create a writer that listens to the document
22:	              PdfWriter writer = PdfWriter.GetInstance(document,  new FileStream("Chap13_form.pdf", FileMode.Create));
23:	              PdfAcroForm acroForm = writer.AcroForm;
24:	              // step 3: we open the document
25:	              document.Open();
26:	              // step 4: we add some content
27:	              BaseFont helv = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
28:	              float fontSize = 12;
29:	              acroForm.AddSingleLineTextField("name", "your name", helv, fontSize, 171, 800, 350, 820);
30:	              acroForm.AddMultiLineTextField("msg", "Hello iText!\nThis is a Test\nThere are multiple lines in this textfield", helv, fontSize, 171, 730, 350, 790);
31:	              acroForm.AddSingleLinePasswordField("password", "", helv, fontSize, 171, 700, 350, 720);
32:	              acroForm.AddHtmlPostButton("btn", "SUBMIT", "noValue", "Chap13.php", helv, fontSize, 355, 700, 420, 725);
33:	              acroForm.AddResetButton("reset", "RESET", null, helv, fontSize, 430, 700, 495, 725);
34:	              acroForm.AddCheckBox("CB1", "Food", true, 180, 685, 190, 695);
35:	              acroForm.AddCheckBox("CB2", "Drinks", false, 180, 645, 210, 675);
36:	              PdfFormField radiogroup = acroForm.GetRadioGroup("CreditCard", "Visa", true);
37:	              acroForm.AddRadioButton(radiogroup, "Visa", 180, 625, 195, 640);
38:	              acroForm.AddRadioButton(radiogroup, "MasterCard", 200, 625, 215, 640);
39:	              acroForm.AddRadioButton(radiogroup, "American", 220, 625, 235, 640);
40:	              acroForm.AddRadioGroup(radiogroup);
41:	              String[] colors = {"Red", "Green", "Blue"};
42:	              String[,] colorvalues = {{"#FF0000", "Red"}, {"#00FF00", "Green"}, {"#0000FF", "Blue"}};
43:	              acroForm.AddSelectList("list1", colors, "Green", helv, fontSize, 171, 550, 300, 600);
44:	              acroForm.AddSelectList("list2", colorvalues, "#0000FF", helv, fontSize, 315, 550, 450, 600);
45:	              acroForm.AddComboBox("combo1", colors, "Green", true, helv, fontSize, 171, 440, 300, 490);
46:	              acroForm.AddComboBox("combo2", colorvalues, "#0000FE", false, helv, fontSize, 315, 440, 450, 490);
47:	              PdfContentByte cb = new PdfContentByte(null);
48:	              cb.SetRGBColorStroke(0x00, 0x00, 0x00);
49:	              cb.SetRGBColorFill(0xFF, 0x00, 0x00);
50:	              cb.Arc(0, 0, 100, 100, 0, 360);
51:	              cb.FillStroke();
52:	              cb.SetRGBColorStroke(0x00, 0x00, 0x00);
53:	              cb.SetRGBColorFill(0xFF, 0xFF, 0xFF);
54:	              cb.Arc(20, 20, 80, 80, 0, 360);
55:	              cb.FillStroke();
56:	              cb.SetRGBColorStroke(0x00, 0x00, 0x00);
57:	              cb.SetRGBColorFill(0xFF, 0x00, 0x00);
58:	              cb.Arc(40, 40, 60, 60, 0, 360);
59:	              cb.FillStroke();
60:	              cb.SetRGBColorFill(0x00, 0x00, 0x00);
61:	              cb.Arc(48, 48, 52, 52, 0, 360);
62:	              cb.Fill();
63:	              acroForm.AddMap("map", null, "Chap13.php", cb, 171, 300, 271, 400);
64:	              acroForm.AddHiddenField("hidden", "secret");
65:	          }
66:	          catch(DocumentException de) 
67:	          {
68:	              Console.Error.WriteLine(de.Message);
69:	          }
70:	          catch(IOException ioe) 
71:	          {
72:	              Console.Error.WriteLine(ioe.Message);
73:	          }
74:	 
75:	          // step 5: close the document
76:	          document.Close();
77:	          }
78:	      }
79:	  }