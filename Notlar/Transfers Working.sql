--Direction
--Select Distinct Direction From [Transfer] (NOLOCK) Where B2BPub=1
--Service Area
--Select Distinct SerArea From [Transfer] (NOLOCK) Where B2BPub=1

--Select * From TransferPrice (NOLOCK)

--Select * From Transfer (NOLOCK) Where B2BPub=1

Declare @Market VarChar(10)
Select @Market='LTNOV'

if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
Into #Location
From Location (NOLOCK)                                

Select Distinct Country, L.Name, L.NameL,TrfFrom,TrfFromName=LFrom.Name,TrfFromNameL=LFrom.NameL, 
  TrfTo,TrfToName=LTo.Name,TrfToNameL=LTo.NameL,T.Direction,T.SerArea
From TransferPrice Tp (NOLOCK) 
JOIN [Transfer] T (NOLOCK) ON T.Code=Tp.[Transfer]
Join #Location L ON Tp.Country=L.RecID
Join #Location LFrom ON Tp.TrfFrom=LFrom.RecID
Join #Location LTo ON Tp.TrfTo=LTo.RecID
Where TrfFrom is not null and TrfTo is not null
  And Exists(Select RecID From [Transfer] T (NOLOCK) Where T.Code=Tp.Transfer And T.B2BPub=1)
  And Tp.EndDate>=dbo.DateOnly(GETDATE())