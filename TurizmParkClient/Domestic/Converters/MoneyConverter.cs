﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Domestic
{
    public class MoneyConverter
    {
        public static string Convert(decimal money, string newCurr = "", decimal rate = 1)
        {
            return (money * rate).ToString("0.00",System.Globalization.CultureInfo.GetCultureInfo("en-US")) + newCurr;
        }

        
        public static decimal ParseAmount(string money)
        {
            var regex = new System.Text.RegularExpressions.Regex(@"[0-9]+(,[0-9]{3})*(\.[0-9]{2})?");
            if (regex.IsMatch(money))
            {
                decimal amount = 0;
                if (decimal.TryParse(regex.Match(money).Value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.GetCultureInfo("en-US"), out amount))
                {
                    return amount;
                }
            }

            return 0M;
        }

        public static string ParseCurrency(string money)
        {
            var regex = new System.Text.RegularExpressions.Regex(@"[a-zA-Z]{3}");
            if (regex.IsMatch(money))
                return regex.Match(money).Value.ToString();
            else
                return default(string);
        }

       
    }
}
