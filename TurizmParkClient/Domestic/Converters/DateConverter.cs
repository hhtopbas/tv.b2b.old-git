﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Domestic
{
    public class DateConverter
    {
        public static DateTime ToDate(string text)
        {
            return new DateTime(int.Parse(text.Substring(4, 4)), int.Parse(text.Substring(2, 2)), int.Parse(text.Substring(0, 2)));
        }
        public static DateTime ToDateTime(string text)
        {
            return ToDate(text).AddHours(int.Parse(text.Substring(8,2))).AddMinutes(int.Parse(text.Substring(10,2)));
        }

        public static string ToDateString(DateTime? time)
        {
            return time == null ? "" : time.Value.ToString("ddMMyyyy",System.Globalization.CultureInfo.GetCultureInfo("tr-TR"));
        }
        public static string ToDateTimeString(DateTime time)
        {
            return time.ToString("ddMMyyyyHHmm", System.Globalization.CultureInfo.GetCultureInfo("tr-TR"));
        }

        public static string ToTimeString(TimeSpan time)
        {
            return time.Hours.ToString("00") + time.Minutes.ToString("00");
        }
    }
}
