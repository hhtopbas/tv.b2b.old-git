﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public abstract class BaseRequest
    {
        public string ClientName { get; set; }
        public string Password { get; set; }
    }
}
