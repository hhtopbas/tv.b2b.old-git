﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class GenericRequest<T> : BaseRequest
    {
        public T Data { get; set; }
    }
}
