﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class GenericResponse<T> : BaseResponse
    {
        public GenericResponse()
            : base()
        {
            Data = default(T);
        }
        public GenericResponse(T data) : base()
        {
            Data = data;
        }

        public GenericResponse(int errcode, string errmsg) : base(errcode,errmsg)
        {
            Data = default(T);
        }
        public T Data { get; set; }
    }
}
