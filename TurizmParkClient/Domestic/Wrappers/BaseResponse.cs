﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace TurizmParkClient.Services.Contracts.Domestic
{

    public abstract class BaseResponse
    {
        public BaseResponse()
        {
            ErrorCode = 0;
        }
        public BaseResponse(int err = 0, string msg = null)
        {
            ErrorCode = err;
            ErrorMessage = msg;
        }

        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
