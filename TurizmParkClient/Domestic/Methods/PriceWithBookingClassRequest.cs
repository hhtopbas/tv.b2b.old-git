﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class PriceWithBookingClassRequest : BaseRequest
    {
        public TurizmParkClient.Services.Contracts.Domestic.Recommendation[] Itinerary { get; set; }
        public TurizmParkClient.Services.Contracts.Domestic.PaxPlan PaxPlan { get; set; }
    }
}
