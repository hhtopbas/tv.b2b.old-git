﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class CreateReservationResponse : BaseResponse
    {
        public PassengerNameRecord Pnr { get; set; }
    }
}
