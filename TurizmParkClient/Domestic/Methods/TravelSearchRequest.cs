﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class TravelSearchRequest : BaseRequest
    {
        public TurizmParkClient.Services.Contracts.Domestic.TravelSearch SearchParameters { get; set; }
    }


}
