﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class SellFromReservationRequest : BaseRequest
    {
        public string tpLocator { get; set; }
        public CreditCard CardPayment { get; set; }
        public DiscountForm DiscountInfo { get; set; }
    }
}
