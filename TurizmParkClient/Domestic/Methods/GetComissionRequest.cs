﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class GetComissionRequest : BaseRequest
    {
        public TurizmParkClient.Services.Contracts.Domestic.Recommendation[] Itinerary { get; set; }
    }
}
