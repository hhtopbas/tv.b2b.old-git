﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class TravelSearchResponse : BaseResponse
    {
        public TurizmParkClient.Services.Contracts.Domestic.TravelSearchResults Results { get; set; }
    }
}
