﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class PriceWithBookingClassResponse : BaseResponse
    {
        public Recommendation[] Itinerary { get; set; }
        public FareProduct[] Fares { get; set; }
    }
}
