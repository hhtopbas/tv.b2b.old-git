﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class CreateReservationRequest : BaseRequest
    {
        public TurizmParkClient.Services.Contracts.Domestic.Recommendation[] Itinerary { get; set; }
        public TurizmParkClient.Services.Contracts.Domestic.Passenger[] Pax { get; set; }
        public TurizmParkClient.Services.Contracts.Domestic.BillingForm BillingInfo { get; set; }
        public TurizmParkClient.Services.Contracts.Domestic.ContactForm ContactInfo { get; set; }
        public TurizmParkClient.Services.Contracts.Domestic.RecievedFrom RecievedInfo { get; set; }
    }
}
