﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class TravelSearchResults
    {
        public TravelSearch SearchQuery { get; set; }

        public Recommendation[] FirstSegmentResults { get; set; }

        public Recommendation[] ReturnSegmentResults { get; set; }
    }
}
