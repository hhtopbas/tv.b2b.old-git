﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace TurizmParkClient.Services.Contracts.Domestic
{
   
    public class TravelSearch
    {
        public string Org { get; set; }
        public string Dest { get; set; }
        public string Date { get; set; }
        public string rDate { get; set; }
        public PaxPlan Pax { get; set; }
        public int ServiceType { get; set; }
        public string Currency { get; set; }    
    }

    public class TravelSearchInt
    {
        public string Org { get; set; }
        public string Dest { get; set; }
        public string Date { get; set; }
        public string rDate { get; set; }
        public PaxPlan Pax { get; set; }
        public string Cabin { get; set; }
    }
}