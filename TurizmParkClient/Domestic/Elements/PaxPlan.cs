﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class PaxPlan
    {
        public int ADT { get; set; }
        public int CHD { get; set; }
        public int INF { get; set; }
        public int CD { get; set; }
        public int ZS { get; set; }
        public int MM { get; set; }
    }
}