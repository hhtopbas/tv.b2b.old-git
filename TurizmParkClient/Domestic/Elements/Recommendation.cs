﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class Recommendation
    {
        public int IsSaleOk { get; set; }
        public int IsRezOk { get; set; }
        public Flight[] Flights { get; set; }
        public FareProduct[] Fares { get; set; }
        public string MarketingAirline { get; set; }       
    }    
}