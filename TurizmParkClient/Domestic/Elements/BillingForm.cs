﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class BillingForm
    {
        public int IsCompany { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string TaxNumber { get; set; }
        public string TaxOffice { get; set; }
        public string TCKimlikNo { get; set; }
        public string Note { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
    }
}