﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class Flight
    {
        public string OperatorAirline { get; set; }
        public string FlightCode { get; set; }
        public string Origin { get; set; }
        public string OriginName { get; set; }
        public string Destination { get; set; }
        public string DestinationName { get; set; }
        public string Departure { get; set; }        
        public string Arrival { get; set; }        
        public string SelectedClass { get; set; }

    }
}