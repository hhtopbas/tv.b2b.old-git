﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class Passenger
    {
        public int paxOrder { get; set; }
        public int pType { get; set; }
        public int isMale { get; set; }
        public string fName { get; set; }
        public string lName { get; set; }
        public string DOB { get; set; }
        public string TCNo { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string FrFlyer { get; set; }

        public class InfantPassenger
        {
            public int paxOrder { get; set; }
            public int isMale { get; set; }
            public string fName { get; set; }
            public string lName { get; set; }
            public string DOB { get; set; }
            public string Ticket { get; set; }
            public string TCNo { get; set; }
        }

        public string Ticket { get; set; }
        public InfantPassenger AssociatedInfant { get; set; }

        
    }
}