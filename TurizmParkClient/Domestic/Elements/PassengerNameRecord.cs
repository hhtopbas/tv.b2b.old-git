﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class PassengerNameRecord
    {
        public string ValidatingAirline { get; set; }
        public string PnrNumber { get; set; }
        public string tpLocator { get; set; }
        public string TimeOption { get; set; }

        public Flight[] AirItinerary { get; set; }
        public Passenger[] NameRecords { get; set; }
        public BillingForm Billing { get; set; }
        public ContactForm Contact { get; set; }
        public FareProduct[] Fares { get; set; }
        public CreditCard CardPayment { get; set; }
    }
}
