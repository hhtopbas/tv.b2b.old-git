﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class DiscountForm
    {
        public string Crr { get; set; }
        public string Amount { get; set; }
    }
}
