﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class RecievedFrom
    {
        public string AgencyName { get; set; }
        public string UserKey { get; set; }
        public string UserName { get; set; }
        public int? AcenteCariKart { get; set; }
        public int? AltAcenteCariKart { get; set; }
    }
}
