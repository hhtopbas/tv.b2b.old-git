﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class CreditCard
    {
        public string CardHolder { get; set; }
        public string CardNumber{ get; set; }
        /// <summary>
        /// MMyy
        /// </summary>
        public string ValidThrough { get; set; }
        public string Cvv { get; set; }

    }
}
