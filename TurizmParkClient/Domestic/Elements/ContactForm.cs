﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class ContactForm
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string cPhone { get; set; }
        public string Email { get; set; }
        public int UseSms { get; set; }
    }
}