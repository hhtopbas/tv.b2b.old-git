﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class FareProduct
    {
        public int[] PaxReferences { get; set; }
        public string Disc { get; set; }
        public string Crr { get; set; }
        public string Fare { get; set; }
        public string Taxes { get; set; }
        public string ServiceFee { get; set; }
        public int PaxCount { get; set; }
        public string Subtotal { get; set; }
    }
}
