﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.Contracts.Domestic
{
    public class CommisionTable
    {
        public class CommissionItem
        {
            public string AirlineCode { get; set; }
            public string ServiceFee { get; set; }
            public string Amount { get; set; }
            public int IsFixed { get; set; }
        }

        public CommissionItem[] Items { get; set; }
    }
}
