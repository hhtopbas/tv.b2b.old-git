﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International
{
    public abstract class BaseRequest
    {
        public string ClientName { get; set; }
        public string Password { get; set; }
    }
}
