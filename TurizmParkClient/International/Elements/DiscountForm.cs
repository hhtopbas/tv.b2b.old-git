﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements
{
    public class DiscountForm
    {
        public string Crr { get; set; }
        public string Amount { get; set; }
    }
}
