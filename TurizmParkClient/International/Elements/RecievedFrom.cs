﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements
{
    public class RecievedFrom
    {
        public int AgencyId { get; set; }
        public int SubAgencyId { get; set; }
    }
}
