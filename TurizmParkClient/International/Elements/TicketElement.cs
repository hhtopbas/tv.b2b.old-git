﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements
{
    public class TicketElement
    {
        public int PaxRef { get; set; }
        public string PassengerType { get; set; }
        public string PassengerName { get; set; }
        public string TicketNumber { get; set; }

        public string BaseFare { get; set; }
        public string EquivalentFare { get; set; }
        public string Taxes { get; set; }
        public string ServiceFee { get; set; }
        public string Total { get; set; }

        public string Rate { get; set; }

        public string BagLimits { get; set; }

        public string LastTktDate { get; set; }


    }
}
