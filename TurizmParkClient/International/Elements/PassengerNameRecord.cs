﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements
{
    public class PassengerNameRecord
    {
        public string ValidatingAirline { get; set; }
        public string PnrNumber { get; set; }
        public string tpLocator { get; set; }
        public string TimeOption { get; set; }
        public string Status { get; set; }
        public VendorLocator[] Locators { get; set; }
        public Flight[] AirItinerary { get; set; }
        public NameRecord[] Passengers { get; set; }
        public ContactForm Contact { get; set; }
        public TicketElement[] Ticket { get; set; }
    }
}
