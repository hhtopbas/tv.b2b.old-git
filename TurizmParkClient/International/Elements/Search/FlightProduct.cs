﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements.Search
{
    public class FlightProduct
    {
        public int fRef { get; set; }
        public string AirVendor { get; set; }
        public string FlightNumber { get; set; }
        public string Origin { get; set; }
        public string Departure { get; set; }
        public string Destination { get; set; }
        public string Arrival { get; set; }
        public int isDayChanged { get; set; }
        public string FlightTime { get; set; }
        public int isAirportChanged { get; set; }
        public string BookingCls { get; set; }
    }
}
