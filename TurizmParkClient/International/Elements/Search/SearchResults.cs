﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements.Search
{
    public class SearchResults
    {
        
        public Recommendation[] Recommendations { get; set; }
    }
}
