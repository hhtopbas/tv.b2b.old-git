﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements.Search
{
    public class TravelSearch
    {
        public string Org { get; set; }
        public string Dest { get; set; }
        public string Date { get; set; }
        public string rDate { get; set; }
        public PaxPlan Pax { get; set; }
        public string Cabin { get; set; }
    }
}
