﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements.Search
{
    public class Recommendation
    {
        public SegmentProduct[] Segments { get; set; }
        public PassengerFareProduct[] Fares { get; set; }

    }
}
