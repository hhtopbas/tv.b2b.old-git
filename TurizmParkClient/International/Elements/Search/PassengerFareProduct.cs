﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements.Search
{
    public class PassengerFareProduct
    {
        public string RequestedPassengerType { get; set; }
        public string ReturnedPassengerType { get; set; }
        public string LastTktDate { get; set; }
        public string BaseFare { get; set; }
        public string EquivalentFare { get; set; }
        public string Texes { get; set; }
        public string ServiceFee { get; set; }
        public string TotalFare { get; set; }
        public string Rate { get; set; }
    }
}
