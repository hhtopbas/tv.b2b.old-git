﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements.Search
{
    public class SegmentProduct
    {
        public int sRef { get; set; }
        public FlightProduct[] Flights { get; set; }
    }
}
