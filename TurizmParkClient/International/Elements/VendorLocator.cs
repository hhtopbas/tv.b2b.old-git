﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements
{
    public class VendorLocator
    {
        public string Vendor { get; set; }
        public string Locator { get; set; }
    }
}
