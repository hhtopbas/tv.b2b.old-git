﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements
{
    public class ContactForm
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
    }
}
