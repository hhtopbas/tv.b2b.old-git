﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Elements
{
    public class NameRecord
    {
        public int PaxRef { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PassengerType { get; set; }
        public string DOB { get; set; }
        public bool IsMale { get { return Title == "MR"; } }
        public string PassportCountry { get; set; }
        public string PassportNo { get; set; }
        public string PassportExpDate { get; set; }
    }
}
