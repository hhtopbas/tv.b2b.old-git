﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International
{
    public class MoneyConverter
    {
        public static string Convert(decimal money, string newCurr)
        {
            return (money).ToString("0.00", System.Globalization.CultureInfo.GetCultureInfo("en-US")) + newCurr;
        }

        public static decimal Parse(string money)
        {
            System.Text.RegularExpressions.Regex mregex = new System.Text.RegularExpressions.Regex(@"\d+\.\d*");

            if (mregex.IsMatch(money))
                return decimal.Parse(mregex.Match(money).Value, System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            else
                return default(decimal);
        }

      
    }
}
