﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International
{
    public class DateConverter
    {
        public static DateTime ToDate(string text)
        {
            return new DateTime(int.Parse(text.Substring(4, 4)), int.Parse(text.Substring(2, 2)), int.Parse(text.Substring(0, 2)));
        }
        public static string ToTimeString(DateTime time)
        {
            return time.ToString("ddMMyyyyHHmm");
        }
    }
}
