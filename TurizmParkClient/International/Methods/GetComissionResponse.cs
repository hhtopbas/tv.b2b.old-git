﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Methods
{
    public class GetComissionResponse : BaseResponse
    {
        public string Comission { get; set; }
    }
}
