﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Methods
{
    public class CreateReservationResponse : BaseResponse
    {
        public Elements.PassengerNameRecord Pnr { get; set; }
    }
}
