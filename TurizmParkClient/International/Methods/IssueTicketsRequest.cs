﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Services.International.Methods
{
    public class IssueTicketsRequest : BaseRequest
    {
        public string tpLocator { get; set; }
        public TurizmParkClient.Services.International.Elements.CreditCard CardPayment { get; set; }
        public TurizmParkClient.Services.International.Elements.DiscountForm DiscountInfo { get; set; }
    }
}
