﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurizmParkClient.Services.International.Elements;
using TurizmParkClient.Services.International.Elements.Search;

namespace TurizmParkClient.Services.International.Methods
{
    public class PriceWithBookingClassRequest : BaseRequest
    {
        public TravelSearch SearchQuery { get; set; }

        public Recommendation Itinerary { get; set; }

       
    }
}
