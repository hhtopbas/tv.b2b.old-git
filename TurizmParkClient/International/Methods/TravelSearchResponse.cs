﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurizmParkClient.Services.International.Elements.Search;

namespace TurizmParkClient.Services.International.Methods
{
    public class TravelSearchResponse : BaseResponse
    {
        public TravelSearch Query { get; set; }
        public SearchResults Results { get; set; }
    }
}
