﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurizmParkClient.Services.International.Elements;

namespace TurizmParkClient.Services.International.Methods
{
    public class ViewPnrResponse : BaseResponse
    {
        public PassengerNameRecord Pnr { get; set; }
    }
}
