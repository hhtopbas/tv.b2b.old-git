﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurizmParkClient.Services.International.Elements;
using TurizmParkClient.Services.International.Elements.Search;

namespace TurizmParkClient.Services.International.Methods
{
    public class PriceWithBookingClassResponse : BaseResponse
    {
        public Recommendation Itinerary { get; set; }
        public bool IsPassportRequired { get; set; }
        public NameRecord[] Names { get; set; }
    }
}
