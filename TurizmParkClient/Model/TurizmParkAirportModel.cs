﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurizmParkClient.Model
{
    public class mdlAirport
    {
        public string Airport { get; set; }
        public string Name { get; set; }
        public mdlAirport()
        {
        }
    }

    public class TurizmParkAirports
    {
        public string AirportCode { get; set; }
        public string AirportName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string StateCode { get; set; }
        public TurizmParkAirports()
        {
        }
    }

    public class TurizmParkAirlines
    {
        public string Code { get; set; }
        public string Code3 { get; set; }
        public string Name { get; set; }
        public Int16 Aktif { get; set; }

        public TurizmParkAirlines()
        {
        }
    }

    public class TurizmParkCountry
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string Continent { get; set; }
        public TurizmParkCountry()
        {
        }
    }

    public class TurizmParkCity
    {
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string StateCode { get; set; }
        public string CountryCode { get; set; }
        public TurizmParkCity()
        {
        }
    }
}
