﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurizmParkClient.Services.Contracts.Domestic;

namespace TurizmParkClient.Model
{
    public class TurizmParkFlightsModelDomestic
    {
        public int ID { get; set; }
        public string Airline { get; set; }
        public string AirlineLogo { get; set; }
        public string FlightNo { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }        
        public List<Flight> FlightDetail { get; set; }
        public List<FareProduct> PriceDetail { get; set; }
        public decimal? Price { get; set; }
        public string PriceStr { get; set; }
        public string Curr { get; set; }
        public Recommendation OrijinData { get; set; }

        public TurizmParkFlightsModelDomestic()
        {
        }
    }
}
