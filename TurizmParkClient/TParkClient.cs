﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel.Web;
using System.Runtime.Serialization.Json;

namespace TurizmParkClient
{
    public class TParkClient
    {
        public enum MessageFormat
        {
            Xml,
            Json
        }
        private static UTF8Encoding UTF8 = new UTF8Encoding(false);
        public Type[] IncludedTypes = new[] { typeof(object) };

        public string serializeToXml<T>(T data)
        {
            using (MemoryStream stream = new MemoryStream(500))
            {
                using (var xmlWriter =
                    XmlTextWriter.Create(stream,
                        new XmlWriterSettings()
                        {
                            OmitXmlDeclaration = true,
                            Encoding = UTF8,
                            Indent = true
                        }))
                {
                    new XmlSerializer(typeof(T), IncludedTypes)
                        .Serialize(xmlWriter, data);
                }
                string xml = UTF8.GetString(stream.ToArray());
                return xml;
            }
        }

        public string serializeToJson<T>(T data)
        {
            using (MemoryStream stream = new MemoryStream(500))
            {
                new DataContractJsonSerializer(typeof(T)).WriteObject(stream, data);
                string json = UTF8.GetString(stream.ToArray());
                return json;
            }
        }

        public T deserializeXml<T>(string xml)
        {
            return (T)(new XmlSerializer(typeof(T), IncludedTypes).Deserialize(new StringReader(xml)));
        }

        public T deserializeJson<T>(string json)
        {
            using (Stream s = new MemoryStream(UTF8.GetBytes(json)))
            {
                return (T)(new DataContractJsonSerializer(typeof(T)).ReadObject(s));
            }
        }

        public string Process(string url, string data, MessageFormat format)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Post;
            request.Timeout = 900000;
            switch (format)
            {
                case MessageFormat.Xml:
                default:
                    request.ContentType = "text/xml";
                    break;
                case MessageFormat.Json:
                    request.ContentType = "application/json";
                    break;
            }

            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(data);
            }


            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                return reader.ReadToEnd();
            }
        }        

        public T2 Process<T1, T2>(string url, T1 requestData, MessageFormat msgFormat)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Post;
            request.Timeout = 900000;
            switch (msgFormat)
            {
                case MessageFormat.Xml:
                default:
                    request.ContentType = "text/xml";
                    break;
                case MessageFormat.Json:
                    request.ContentType = "application/json";
                    break;
            }

            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                string requestText = "";
                switch (msgFormat)
                {
                    case MessageFormat.Xml:
                    default:
                        requestText = serializeToXml<T1>(requestData);
                        break;
                    case MessageFormat.Json:
                        requestText = serializeToJson<T1>(requestData);
                        break;
                }

                writer.Write(requestText);
                Console.WriteLine("Request sent");
                Console.WriteLine(requestText);
            }

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                Console.WriteLine("Response Recieved");
                string responseText = reader.ReadToEnd();
                Console.WriteLine(responseText);
                switch (msgFormat)
                {
                    case MessageFormat.Xml:
                    default:
                        return deserializeXml<T2>(responseText);
                    case MessageFormat.Json:
                        return deserializeJson<T2>(responseText);
                }
            }

        }        
    }
}
