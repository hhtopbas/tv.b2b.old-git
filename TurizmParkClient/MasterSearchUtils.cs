﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using TurizmParkClient.Model;
using TvTools;

namespace TurizmParkClient
{
    public class MasterSearchUtils
    {
        public static DateTime? parseDateTime(string dateTime)
        {
            try
            {
                int dd = Int32.Parse(dateTime.ToString().Substring(0, 2));
                int mm = Int32.Parse(dateTime.ToString().Substring(2, 2));
                int yyyy = Int32.Parse(dateTime.ToString().Substring(4, 4));
                int hour = Int32.Parse(dateTime.ToString().Substring(8, 2));
                int min = Int32.Parse(dateTime.ToString().Substring(10, 2));
                return new DateTime(yyyy, mm, dd, hour, min, 0);
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return null;
            }
        }

        public static decimal? parsePriceInternational(string _prices)
        {
            decimal? price = (decimal)0;
            
                string priceStr = _prices.Replace("TRY", "").Trim().Replace(",", "").Replace(".", "");
                decimal? _price = Conversion.getDecimalOrNull(priceStr);
                price += _price.HasValue ? _price.Value : (decimal)0;
            
            return price.HasValue ? (price.Value / (decimal)100) : price;
        }

        public static decimal? parsePrice(List<TurizmParkClient.Services.Contracts.Domestic.FareProduct> _prices)
        {
            decimal? price = (decimal)0;
            foreach (TurizmParkClient.Services.Contracts.Domestic.FareProduct row in _prices)
            {
                string priceStr = row.Subtotal.Replace(row.Crr, "").Trim().Replace(",", "").Replace(".", "");
                decimal? _price = Conversion.getDecimalOrNull(priceStr) * row.PaxCount;
                price += _price.HasValue ? _price.Value : (decimal)0;
            }
            return price.HasValue ? (price.Value / (decimal)100) : price;
        }

        public static string getServiceUrl(bool domestic)
        {            
            if (domestic)
                return System.Configuration.ConfigurationManager.AppSettings["TurizmParkDomesticServerUrl"].ToString();
            else return System.Configuration.ConfigurationManager.AppSettings["TurizmParkInternationalServerUrl"].ToString();
        }

        public static TurizmParkLoginUser getTurizmParkLoginUser()
        {
            string userName = System.Configuration.ConfigurationManager.AppSettings["TurizmParkLoginName"].ToString();
            //HttpContext.Current..Application["TurizmParkLoginName"] != null ? HttpContext.Current.Application["TurizmParkLoginName"].ToString() : string.Empty;
            string pass = System.Configuration.ConfigurationManager.AppSettings["TurizmParkPassword"].ToString();
            //HttpContext.Current.Application["TurizmParkPassword"] != null ? HttpContext.Current.Application["TurizmParkPassword"].ToString() : string.Empty;
            return new TurizmParkLoginUser(userName, pass);
        }

        public static List<TurizmParkCountry> getCountrys()
        {
            List<TurizmParkCountry> countries = new List<TurizmParkCountry>();
            string path = AppDomain.CurrentDomain.BaseDirectory + "3thParty\\TurizmParkPrj\\Data\\Countries.xml";
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlNodeList elemList = xDoc.GetElementsByTagName("Country");

            for (int i = 0; i < elemList.Count; i++)
            {
                string countryCode = elemList[i].SelectSingleNode("CountryCode").InnerXml;
                string countryName = elemList[i].SelectSingleNode("CountryName").InnerXml;
                string countryContinent = elemList[i].SelectSingleNode("Continent").InnerXml;
                if (countryCode == "IL")
                    continue;
                countries.Add(new TurizmParkCountry { CountryCode = countryCode, CountryName = countryName, Continent = countryContinent });
            }
            return countries;
        }

        public static List<TurizmParkCity> getCities()
        {
            List<TurizmParkCity> cities = new List<TurizmParkCity>();
            string path = AppDomain.CurrentDomain.BaseDirectory + "3thParty\\TurizmParkPrj\\Data\\Cities.xml";
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlNodeList elemList = xDoc.GetElementsByTagName("City");

            for (int i = 0; i < elemList.Count; i++)
            {
                string cityCode = elemList[i].SelectSingleNode("CityCode").InnerXml;
                string cityName = elemList[i].SelectSingleNode("CityName").InnerXml;
                //string stateCode = elemList[i].SelectSingleNode("StateCode").InnerXml;
                string countryCode = elemList[i].SelectSingleNode("CountryCode").InnerXml;
                if (countryCode == "IL")
                    continue;
                cities.Add(new TurizmParkCity { CityCode = cityCode, CityName = cityName, CountryCode = countryCode });
            }
            return cities;
        }

        public static List<TurizmParkAirports> getAirports()
        {
            List<TurizmParkCountry> country = TurizmParkClient.MasterSearchUtils.getCountrys();
            List<TurizmParkCity> city = TurizmParkClient.MasterSearchUtils.getCities();

            List<TurizmParkAirports> airports = new List<TurizmParkAirports>();
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "3thParty\\TurizmParkPrj\\Data\\Airports.xml";
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(path);
                XmlNodeList elemList = xDoc.GetElementsByTagName("Airport");

                for (int i = 0; i < elemList.Count; i++)
                {
                    string airportCode = elemList[i].SelectSingleNode("AirportCode").InnerXml;
                    string airportName = elemList[i].SelectSingleNode("AirportName").InnerXml;
                    string countryCode = elemList[i].SelectSingleNode("CountryCode").InnerXml;
                    if (countryCode == "IL")
                        continue;
                    string countryName = country.Find(f => f.CountryCode == countryCode).CountryName;
                    string cityCode = elemList[i].SelectSingleNode("CityCode").InnerXml;
                    string cityName = city.Find(f => f.CityCode == cityCode).CityName;
                    //string stateCode = elemList[i].SelectSingleNode("StateCode").InnerXml;

                    airports.Add(new TurizmParkAirports
                    {
                        AirportCode = airportCode,
                        AirportName = airportName,
                        CountryCode = countryCode,
                        CountryName = countryName,
                        CityCode = cityCode,
                        CityName = cityName
                    });
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return airports;
        }

        public static TurizmParkAirports getAirports(string airPort)
        {
            List<TurizmParkCountry> country = TurizmParkClient.MasterSearchUtils.getCountrys();
            List<TurizmParkCity> city = TurizmParkClient.MasterSearchUtils.getCities();

            List<TurizmParkAirports> airports = new List<TurizmParkAirports>();
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "3thParty\\TurizmParkPrj\\Data\\Airports.xml";
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(path);
                XmlNodeList elemList = xDoc.GetElementsByTagName("Airport");

                for (int i = 0; i < elemList.Count; i++)
                {
                    string airportCode = elemList[i].SelectSingleNode("AirportCode").InnerXml;
                    string airportName = elemList[i].SelectSingleNode("AirportName").InnerXml;
                    string countryCode = elemList[i].SelectSingleNode("CountryCode").InnerXml;
                    if (countryCode == "IL")
                        continue;                    
                    if (airportCode == airPort)
                    {
                        string countryName = country.Find(f => f.CountryCode == countryCode).CountryName;
                        string cityCode = elemList[i].SelectSingleNode("CityCode").InnerXml;
                        string cityName = city.Find(f => f.CityCode == cityCode).CityName;

                        return new TurizmParkAirports
                        {
                            AirportCode = airportCode,
                            AirportName = airportName,
                            CountryCode = countryCode,
                            CountryName = countryName,
                            CityCode = cityCode,
                            CityName = cityName
                        };
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return null;
            }
        }

        public static List<TurizmParkAirlines> getAirlines()
        {
            List<TurizmParkAirlines> airLines = new List<TurizmParkAirlines>();
            string FileName = AppDomain.CurrentDomain.BaseDirectory + "3thParty\\TurizmParkPrj\\Data\\Airlines.json";

            try
            {
                if (System.IO.File.Exists(FileName))
                {
                    System.IO.StreamReader reader = new System.IO.StreamReader(FileName);

                    try
                    {
                        string uncompressed = reader.ReadToEnd();
                        airLines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TurizmParkAirlines>>(uncompressed);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        reader.Close();
                    }
                }

                return airLines;
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return null;
            }
        }
    }

    public class TurizmParkLoginUser
    {
        public string ClientName { get; set; }
        public string Password { get; set; }

        public TurizmParkLoginUser()
        {
        }

        public TurizmParkLoginUser(string clientName, string password)
        {
            this.ClientName = clientName;
            this.Password = password;
        }
    }
}
