﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using TurizmParkClient.Model;
using TurizmParkClient.Services.Contracts;
using TvBo;
using TvTools;

namespace TurizmParkClient
{
    public class TurizmParkToTv
    {

        public ResDataRecord GenerateResMain(User UserData, ResDataRecord ResData, string resSaleCur, DateTime? BegDate, Int16? Night, Int16? Adult, Int16? Child, int? DepCity, int? ArrCity, ref string errorMsg)
        {
            errorMsg = string.Empty;

            ResMainRecord ResMain = new ResMainRecord(null);

            ResMain.PackType = "";

            Int16? SaleCurOpt = new Reservation().getIndSaleCurOption(UserData);
            string SaleCur = string.Empty;
            if (!SaleCurOpt.HasValue)
                SaleCur = UserData.SaleCur;
            else
                if (SaleCurOpt.Value == 0)
                    SaleCur = UserData.SaleCur;
                else
                    if (SaleCurOpt.Value == 1)
                        SaleCur = !string.IsNullOrEmpty(resSaleCur) ? resSaleCur : UserData.SaleCur;
                    else
                        SaleCur = !string.IsNullOrEmpty(resSaleCur) ? resSaleCur : UserData.SaleCur;

            try
            {
                ResMain.ResNo = UserData.SID.Substring(0, 10);
                ResMain.Operator = UserData.Operator;
                ResMain.Market = UserData.Market;
                ResMain.Office = UserData.OprOffice;
                ResMain.Agency = UserData.AgencyID;
                ResMain.AgencyName = UserData.AgencyName;
                ResMain.AgencyNameL = UserData.AgencyName;
                ResMain.HolPack = string.Empty;
                ResMain.HolPackName = string.Empty;
                ResMain.HolPackNameL = string.Empty;
                ResMain.BegDate = BegDate;
                ResMain.EndDate = BegDate.Value.AddDays(Night.Value);
                ResMain.Days = Night.Value;
                ResMain.PriceSource = 0;
                ResMain.PriceListNo = 0;
                ResMain.SaleResource = 3;
                ResMain.ResLock = 0;
                ResMain.ResStat = 9;
                ResMain.ConfStat = 0;
                ResMain.AgencyUser = UserData.UserID;
                ResMain.AgencyUserName = UserData.UserName;
                ResMain.AgencyUserNameL = UserData.UserName;
                ResMain.ResDate = DateTime.Today.Date;
                ResMain.ChgUser = "";
                ResMain.NetCur = SaleCur;
                ResMain.SaleCur = SaleCur;
                ResMain.Ballon = "N";
                ResMain.ReceiveDate = DateTime.Now;
                ResMain.RecCrtDate = DateTime.Now;
                ResMain.RecCrtUser = UserData.UserID;
                ResMain.RecChgUser = "";
                ResMain.Code1 = "";
                ResMain.Code2 = "";
                ResMain.Code3 = "";
                ResMain.Code4 = "";
                ResMain.Adult = Adult;
                ResMain.Child = Child;
                ResMain.ConfToAgency = "N";
                ResMain.PaymentStat = "U";
                ResMain.Discount = 0;
                ResMain.AgencyComMan = "N";
                ResMain.SendInComing = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ? "N" : "W";
                ResMain.AllDocPrint = "N";
                ResMain.SendAgency = "N";
                ResMain.EB = "N";
                ResMain.EBAgency = 0;
                ResMain.EBSupID = 0;
                ResMain.EBSupMaxPer = 0;
                ResMain.EBPasID = 0;
                ResMain.EBAgencyPer = 0;
                ResMain.EBPasPer = 0;
                ResMain.EBPas = 0;
                ResMain.DepCity = DepCity;
                ResMain.DepCityName = new Locations().getLocationName(UserData.Market, DepCity.Value, NameType.NormalName, ref errorMsg);
                ResMain.DepCityNameL = new Locations().getLocationName(UserData.Market, DepCity.Value, NameType.LocalName, ref errorMsg);
                ResMain.ArrCity = ArrCity;
                ResMain.ArrCityName = new Locations().getLocationName(UserData.Market, ArrCity.Value, NameType.NormalName, ref errorMsg);
                ResMain.ArrCityNameL = new Locations().getLocationName(UserData.Market, ArrCity.Value, NameType.LocalName, ref errorMsg);
                ResMain.Credit = "N";
                ResMain.CreditExpenseCur = "";
                ResMain.Broker = "";
                ResMain.BrokerCom = 0;
                ResMain.BrokerComPer = 0;
                ResMain.PLOperator = UserData.Operator;
                ResMain.PLMarket = UserData.Market;
                ResMain.AgencyBonus = 0;
                ResMain.AgencyBonusAmount = 0;
                ResMain.UserBonus = 0;
                ResMain.UserBonusAmount = 0;
                ResMain.PasBonus = 0;
                ResMain.PasBonusAmount = 0;
                ResMain.PasPayable = 0;
                ResMain.Complete = 0;
                ResMain.ResNote = "";
                ResMain.AgencyDisPasPer = 0;
                ResMain.InvoiceTo = UserData.AgencyRec.InvoiceTo;
                ResMain.PaymentFrom = UserData.AgencyRec.PaymentFrom;
                ResMain.WebIP = UserData.IpNumber;
                ResMain.PLSpoChk = 0;
                ResMain.MemTable = false;
                ResData.ResMain = ResMain;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
            }

            return ResData;
        }

        public List<TitleAgeRecord> AssignTitles(User UserData, TurizmParkClient.Services.Contracts.Domestic.PaxPlan paxs, Int32 BookID)
        {
            string errorMsg = string.Empty;

            List<TitleAgeRecord> titleAge = new List<TitleAgeRecord>();
            List<TitleRecord> dt = new TvBo.Common().getAdultTypes();

            int i;
            foreach (TitleRecord row in dt)
                titleAge.Add(new TitleAgeRecord
                {
                    Code = row.Code,
                    TitleNo = row.TitleNo,
                    Age1 = null,
                    Age2 = null
                });

            List<TitleRecord> titleChildAge = new TvBo.Common().getChildTypes();
            i = 0;
            for (int k = 0; k < paxs.CHD; k++, i++)
                titleAge.Add(new TitleAgeRecord
                {
                    Code = titleChildAge.Find(f => (f.TitleNo == 6 || f.TitleNo == 7)).Code,
                    TitleNo = titleChildAge.Find(f => (f.TitleNo == 6 || f.TitleNo == 7)).TitleNo,
                    Age1 = (decimal)2,
                    Age2 = UserData.TvParams.TvParamReser.MaxChdAge.HasValue ? UserData.TvParams.TvParamReser.MaxChdAge.Value : (decimal)12,
                    Adult = false
                });

            for (int k = 0; k < paxs.INF; k++, i++)
                titleAge.Add(new TitleAgeRecord
                {
                    Code = titleChildAge.Find(f => f.TitleNo >= 8).Code,
                    TitleNo = titleChildAge.Find(f => f.TitleNo >= 8).TitleNo,
                    Age1 = (decimal)0,
                    Age2 = (decimal)1.99,
                    Adult = false
                });

            return titleAge;
        }

        public ResCustRecord createChildRecord(string ResNo, ResCustRecord row, Int16 SeqNo, int BookID, List<ResCustRecord> dtCustomers, int defaultNation, bool ChdPassChk)
        {
            row.BookID = BookID;
            row.CustNo = dtCustomers.Count > 0 ? dtCustomers.Count + 1 : 1;
            row.CustNoT = row.CustNo;
            row.ResNo = ResNo;
            row.SeqNo = SeqNo;
            row.Surname = HttpContext.GetGlobalResourceObject("LibraryResource", "CustControlSURNAME").ToString();
            row.Name = HttpContext.GetGlobalResourceObject("LibraryResource", "CustControlNAME").ToString();
            row.SurnameL = "";
            row.NameL = "";
            row.PassSerie = "";
            row.PassNo = "";
            row.PassGiven = "";
            row.IDNo = "";
            row.Leader = "N";
            row.DocVoucher = 0;
            row.DocTicket = 0;
            row.DocInsur = 0;
            row.Bonus = 0;
            row.BonusAmount = 0;
            row.Nation = defaultNation;
            row.Status = 0;
            if (ChdPassChk) row.HasPassport = ChdPassChk;
            row.MemTable = false;

            return row;
        }

        public ResDataRecord GenerateTourists(User UserData, ResDataRecord ResData, TurizmParkClient.Services.Contracts.Domestic.PaxPlan paxs, ref string errorMsg)
        {
            errorMsg = string.Empty;
            string ResNo = ResData.ResMain.ResNo.Substring(0, ResData.ResMain.ResNo.Length > 10 ? 10 : ResData.ResMain.ResNo.Length);
            int defaultNation = UserData.Country != null ? UserData.Country.Value : 1;
            bool ChdPassChk = UserData.TvParams.TvParamReser.ChdPassChk;
            int AdultCount;
            int ChildCount;
            Int16 Adult = 0;
            Int16 Child = 0;

            Adult = Convert.ToInt16(paxs.ADT);
            Child = Convert.ToInt16(paxs.CHD + paxs.INF);

            List<TitleAgeRecord> T1 = new List<TitleAgeRecord>();
            //ResDataRecord ds = ResData;
            try
            {
                AdultCount = Adult;
                ChildCount = Child;

                int pax = (int)(AdultCount + ChildCount);
                int[][] titles = new int[][] { new int[pax], new int[pax] };
                List<ResCustRecord> dtCustomers = ResData.ResCust;

                List<TitleAgeRecord> Title = AssignTitles(UserData, paxs, 1);

                var query = from TD in Title
                            group TD by new { TitleNo = TD.TitleNo, Age1 = TD.Age1, Age2 = TD.Age2, Code = TD.Code } into rv
                            select new { TitleNo = rv.Key.TitleNo, Age1 = rv.Key.Age1, Age2 = rv.Key.Age2, Code = rv.Key.Code };

                foreach (var Q in query)
                    T1.Add(new TitleAgeRecord { Code = Q.Code, TitleNo = Q.TitleNo, Age1 = Q.Age1, Age2 = Q.Age2 });

                Int16 SeqNo = 0;
                if (dtCustomers.Count > 0)
                    SeqNo = (from cust in dtCustomers select cust.SeqNo).Last().Value;

                if (ResData.TitleCust == null)
                    ResData.TitleCust = new List<TitleAgeRecord>();


                #region Adult Turist

                int TitleCnt = 1;

                for (int i = 0; i < AdultCount; i++)
                {
                    try
                    {
                        SeqNo++;
                        ResCustRecord row = new ResCustRecord();
                        row.BookID = 1;
                        row.CustNo = dtCustomers.Count > 0 ? dtCustomers.Count + 1 : 1;
                        row.CustNoT = row.CustNo;
                        row.ResNo = ResNo;
                        row.SeqNo = SeqNo;
                        row.Title = Title[TitleCnt - 1].TitleNo;
                        TitleCnt++;
                        if (TitleCnt > 2)
                        {
                            TitleCnt = 1;
                        }
                        row.Surname = HttpContext.GetGlobalResourceObject("LibraryResource", "CustControlSURNAME").ToString();
                        row.Name = HttpContext.GetGlobalResourceObject("LibraryResource", "CustControlNAME").ToString();
                        row.SurnameL = "";
                        row.NameL = "";
                        row.PassSerie = "";
                        row.PassNo = "";
                        row.PassGiven = "";
                        row.IDNo = "";
                        row.Leader = "N";
                        row.DocVoucher = 0;
                        row.DocTicket = 0;
                        row.DocInsur = 0;
                        row.Bonus = 0;
                        row.BonusAmount = 0;
                        row.Nation = defaultNation;
                        row.Status = 0;
                        row.HasPassport = true;
                        row.MemTable = false;
                        dtCustomers.Add(row);
                    }
                    catch (Exception Ex)
                    {
                        dtCustomers.Clear();
                        errorMsg = Ex.Message;
                        return null;
                    }
                }
                #endregion Adult Turist

                #region Child Turist
                var titleChds = Title.Where(w => w.Adult == false).ToList<TitleAgeRecord>();
                TitleCnt = 0;

                #region Child
                if (paxs.CHD > 0)
                {
                    for (int i = 0; i < paxs.CHD; i++)
                    {
                        SeqNo++;
                        ResCustRecord row = new ResCustRecord();
                        row = createChildRecord(ResNo, row, SeqNo, 1, dtCustomers, defaultNation, ChdPassChk);
                        Int16 TitleNo = -1;
                        int Age = -1;

                        Age = Convert.ToUInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAge.HasValue ? UserData.TvParams.TvParamReser.MaxChdAge.Value : (decimal)12));
                        TitleNo = Convert.ToInt16((from q in query
                                                   where q.Age1 <= Age && q.Age2 >= Age && q.TitleNo > 5
                                                   select new { Title = q.TitleNo }).FirstOrDefault().Title.ToString());

                        if (TitleNo < 0)
                        {
                            TitleCnt++;
                            if (TitleCnt > 2)
                            {
                                TitleCnt = 1;
                            }
                        }
                        else row.Title = TitleNo; //Title.Rows[i]["TitleNo"]
                        row.Age = Convert.ToInt16(Age);
                        if (Age < 0)
                            if (Convert.ToInt16(Title[i].Age2) > 0)
                                row.Age = Convert.ToInt16(Title[i].Age2 - 1);
                        if (Age < 0)
                        {
                            if (Convert.ToInt16(Title[i].Age2) > 0)
                                row.Birtday = ResData.ResMain.EndDate.Value.AddDays(2).AddYears(-Convert.ToInt16(Title[i].Age2) + 1);
                        }
                        else
                            row.Birtday = ResData.ResMain.EndDate.Value.AddDays(2).AddYears(-Convert.ToInt16((Age + 1)));

                        if (row.Title > 5) row.ParentID = 1;
                        dtCustomers.Add(row);
                    }
                }
                #endregion
                #region Child Infant
                if (paxs.INF > 0)
                {
                    for (int i = 0; i < paxs.INF; i++)
                    {
                        SeqNo++;
                        ResCustRecord row = new ResCustRecord();
                        row = createChildRecord(ResNo, row, SeqNo, 1, dtCustomers, defaultNation, ChdPassChk);
                        Int16 TitleNo = -1;
                        int Age = -1;

                        Age = 1;
                        TitleNo = Convert.ToInt16((from q in query
                                                   where q.Age1 <= Age && q.Age2 >= Age && q.TitleNo > 5
                                                   select new { Title = q.TitleNo }).FirstOrDefault().Title.ToString());

                        if (TitleNo < 0)
                        {
                            TitleCnt++;
                            if (TitleCnt > 2)
                            {
                                TitleCnt = 1;
                            }
                        }
                        else row.Title = TitleNo; //Title.Rows[i]["TitleNo"]
                        row.Age = Convert.ToInt16(Age);
                        if (Age < 0)
                            if (Convert.ToInt16(Title[i].Age2) > 0)
                                row.Age = Convert.ToInt16(Title[i].Age2 - 1);
                        if (Age < 0)
                        {
                            if (Convert.ToInt16(Title[i].Age2) > 0)
                                row.Birtday = ResData.ResMain.EndDate.Value.AddDays(2).AddYears(-Convert.ToInt16(Title[i].Age2) + 1);
                        }
                        else
                            row.Birtday = ResData.ResMain.EndDate.Value.AddDays(2).AddYears(-Convert.ToInt16((Age + 1)));

                        if (row.Title > 5) row.ParentID = 1;
                        dtCustomers.Add(row);
                    }
                }
                #endregion
                #endregion

                dtCustomers.First().Leader = "Y";
            }

            

            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }

            foreach (TitleAgeRecord row in T1)
                ResData.TitleCust.Add(new TitleAgeRecord
                {
                    TitleNo = row.TitleNo,
                    Code = row.Code,
                    Age1 = row.Age1,
                    Age2 = row.Age2
                });

            List<TitleAgeRecord> tmpTitle = new List<TitleAgeRecord>();
            tmpTitle = (from q in ResData.TitleCust
                        group q by new { q.TitleNo, q.Code, q.Age1, q.Age2 } into k
                        select new TitleAgeRecord { TitleNo = k.Key.TitleNo, Code = k.Key.Code, Age1 = k.Key.Age1, Age2 = k.Key.Age2 }).ToList<TitleAgeRecord>();
            ResData.TitleCust = new List<TitleAgeRecord>();
            ResData.TitleCust = tmpTitle;
            return ResData;
        }

        public int? checkFlight(User userData, string flightNo, DateTime? depDate, DateTime? arrDate, TurizmParkAirports depAirport, TurizmParkAirports arrAirport, string airLine, string sClass, ref int? depCity, ref int? arrCity, bool Amedeus, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(flightNo) || !depDate.HasValue || !arrDate.HasValue)
                return 1;

            string tsql =
@"
Declare @Status bit,@DepCityID int,@ArrCityID int

Exec dbo.usp_Check_Flight_For_TurizmPark @FlightNo,@DepAirPort,@ArrAirPort,@DepAirPortName,@ArrAirPortName,@DepCity,@ArrCity,@DepCityName,@ArrCityName,@FlyDate,
	                                     @DepTime,@ArrTime,@DepCountry,@ArrCountry,@DepCountryName,@ArrCountryName,@ExtAirlineCode,@Operator,@IsAmadeus,
	                                     @Status OUTPUT,@DepCityID OUTPUT,@ArrCityID OUTPUT

Select Status=@Status,DepCityID=@DepCityID,ArrCityID=@ArrCityID
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.AnsiString, flightNo);
                db.AddInParameter(dbCommand, "DepAirPort", DbType.AnsiString, depAirport.AirportCode);
                db.AddInParameter(dbCommand, "ArrAirPort", DbType.AnsiString, arrAirport.AirportCode);
                db.AddInParameter(dbCommand, "DepAirPortName", DbType.String, depAirport.AirportName);
                db.AddInParameter(dbCommand, "ArrAirPortName", DbType.String, arrAirport.AirportName);
                db.AddInParameter(dbCommand, "DepCity", DbType.AnsiString, depAirport.CityCode);
                db.AddInParameter(dbCommand, "ArrCity", DbType.AnsiString, arrAirport.CityCode);
                db.AddInParameter(dbCommand, "DepCityName", DbType.String, depAirport.CityName);
                db.AddInParameter(dbCommand, "ArrCityName", DbType.String, arrAirport.CityName);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, depDate);
                db.AddInParameter(dbCommand, "DepTime", DbType.DateTime, depDate);
                db.AddInParameter(dbCommand, "ArrTime", DbType.DateTime, arrDate);
                db.AddInParameter(dbCommand, "DepCountry", DbType.AnsiString, depAirport.CountryCode);
                db.AddInParameter(dbCommand, "ArrCountry", DbType.AnsiString, arrAirport.CountryCode);
                db.AddInParameter(dbCommand, "DepCountryName", DbType.String, depAirport.CountryName);
                db.AddInParameter(dbCommand, "ArrCountryName", DbType.String, arrAirport.CountryName);
                db.AddInParameter(dbCommand, "ExtAirlineCode", DbType.AnsiString, airLine);
                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, userData.Operator);
                db.AddInParameter(dbCommand, "IsAmadeus", DbType.Boolean, Amedeus);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        Int16? status = Conversion.getInt16OrNull(oReader["Status"]);
                        if (status.HasValue && status.Value == 0)
                        {
                            depCity = Conversion.getInt32OrNull(oReader["DepCityID"]);
                            arrCity = Conversion.getInt32OrNull(oReader["ArrCityID"]);
                            return 0;
                        }
                        else return -1;
                    }
                    return -1;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return -1;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ResDataRecord createReservationDometic(User userData, TurizmParkClient.Services.Contracts.Domestic.PriceWithBookingClassResponse turizmParkServices, TurizmParkClient.Services.Contracts.Domestic.PaxPlan pax, DateTime BegDate, Int16? Night, Int16? Adult, Int16? Child, string airLine, ref string errorMsg)
        {
            try
            {
                DateTime EndDate = BegDate.AddDays(Night.HasValue ? Night.Value : 0);

                ResDataRecord ResData = new ResDataRecord();
                List<ResServiceRecord> resServices = ResData.ResService;
                List<TurizmParkAirports> airPorts = TurizmParkClient.MasterSearchUtils.getAirports();
                TurizmParkClient.Services.Contracts.Domestic.FareProduct[] fares = turizmParkServices.Fares;
                TurizmParkClient.Services.Contracts.Domestic.FareProduct adultFares = fares.Where(w => w.Disc == "ADT").FirstOrDefault();
                TurizmParkClient.Services.Contracts.Domestic.FareProduct childFares = fares.Where(w => w.Disc == "CHD").FirstOrDefault();
                TurizmParkClient.Services.Contracts.Domestic.FareProduct infantFares = fares.Where(w => w.Disc == "INF").FirstOrDefault();
                ResData = GenerateResMain(userData, ResData, turizmParkServices.Fares.FirstOrDefault().Crr, BegDate, Night, Adult, Child.Value, userData.Location, userData.Location, ref errorMsg);
                ResData = GenerateTourists(userData, ResData, pax, ref errorMsg);

                ResMainRecord ResMain = ResData.ResMain;
                List<ResConRecord> ResCon = ResData.ResCon;
                int serviceCount = turizmParkServices.Itinerary.Count();

                List<SelectCustRecord> customers = new List<SelectCustRecord>();
                foreach (ResCustRecord row in ResData.ResCust)
                {
                    SelectCustRecord selCust = new SelectCustRecord();
                    selCust.BookID = 1;
                    selCust.CustNo = row.CustNo;
                    selCust.Customers = row.Surname + " " + row.Name;
                    selCust.Selected = true;
                    customers.Add(selCust);
                }

                decimal adultPrice = adultFares != null ? (Conversion.getDecimalOrNull(adultFares.Subtotal.Replace(adultFares.Crr, "")).HasValue ? Conversion.getDecimalOrNull(adultFares.Subtotal.Replace(adultFares.Crr, "")).Value : 0) * adultFares.PaxCount : 0;
                decimal childPrice = childFares != null ? (Conversion.getDecimalOrNull(childFares.Subtotal.Replace(childFares.Crr, "")).HasValue ? Conversion.getDecimalOrNull(childFares.Subtotal.Replace(childFares.Crr, "")).Value : 0) * childFares.PaxCount : 0;
                decimal infantPrice = infantFares != null ? (Conversion.getDecimalOrNull(infantFares.Subtotal.Replace(adultFares.Crr, "")).HasValue ? Conversion.getDecimalOrNull(infantFares.Subtotal.Replace(infantFares.Crr, "")).Value : 0) * infantFares.PaxCount : 0;
                decimal totalPrice = adultPrice + childPrice + infantPrice;
                decimal totalCom = 10 * ResData.ResCust.Count;
                decimal servicePrice = (totalPrice + (totalCom * serviceCount)) / serviceCount;

                foreach (TurizmParkClient.Services.Contracts.Domestic.Recommendation row in turizmParkServices.Itinerary.ToList<TurizmParkClient.Services.Contracts.Domestic.Recommendation>())
                {
                    List<TurizmParkClient.Services.Contracts.Domestic.Flight> flights = row.Flights.ToList();
                    foreach (TurizmParkClient.Services.Contracts.Domestic.Flight fRow in flights)
                    {
                        string flight = fRow.FlightCode;
                        DateTime? depDate = MasterSearchUtils.parseDateTime(fRow.Departure);
                        DateTime? arrDate = MasterSearchUtils.parseDateTime(fRow.Arrival);
                        TurizmParkAirports depAirport = airPorts.Find(f => f.AirportCode == fRow.Origin);
                        TurizmParkAirports arrAirport = airPorts.Find(f => f.AirportCode == fRow.Destination);

                        if (depAirport == null || arrAirport == null) return null;
                        int? depCity = null;
                        int? arrCity = null;
                        if (checkFlight(userData, flight, depDate, arrDate, depAirport, arrAirport, airLine, fRow.SelectedClass, ref depCity, ref arrCity, false, ref errorMsg) == 0)
                        {
                            Int16 night = 1;
                            Int16 startDay = Convert.ToInt16(((depDate.HasValue ? depDate.Value : BegDate) - BegDate).Days.ToString());
                            int calcType = 0;
                            int priceType = 0;
                            string supplier = string.Empty;
                            ResData = new Reservation().AddService(userData, ResData, customers, 1, depDate.Value, arrDate.Value, "FLIGHT", fRow.FlightCode, string.Empty, string.Empty, string.Empty, fRow.SelectedClass, night, startDay,
                                                                   1, depCity, arrCity, string.Empty, string.Empty, calcType, priceType, null, null, string.Empty,
                                                                   "M", null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, supplier, null, ref errorMsg,
                                                                   null, string.Empty, null, null, null);
                            if (string.IsNullOrEmpty(errorMsg))
                            {
                                resServices.LastOrDefault().PriceSource = 2;
                                resServices.LastOrDefault().SaleCur = ResMain.SaleCur;
                                resServices.LastOrDefault().SalePrice = servicePrice;
                                resServices.LastOrDefault().NetCur = ResMain.SaleCur;
                                resServices.LastOrDefault().NetPrice = totalPrice / serviceCount;
                            }
                        }
                    }
                }

                #region Calculate ResCustPrice
                string ResNo = ResData.ResMain.ResNo;
                ResNo = ResNo.Length > 10 ? ResNo.Substring(0, 10) : ResNo;
                ResMainRecord resMain = ResData.ResMain;
                string agencyBak = resMain.Agency;
                if (!string.IsNullOrEmpty(ResData.ResMain.PromoCode) && string.Equals(userData.CustomRegID, TvBo.Common.crID_Kenba))
                {
                    ResData.OwnAgency = new Promos().getPromotionAgencyInfo(userData, ResData.ResMain.PromoCode, ref errorMsg);
                }
                if (!string.IsNullOrEmpty(ResData.ResMain.PromoCode) && string.Equals(userData.CustomRegID, TvBo.Common.crID_Kenba) && ResData.Resource == ResResource.B2CToB2B && !string.IsNullOrEmpty(ResData.OwnAgency))
                {
                    resMain.Agency = ResData.OwnAgency;
                }

                StringBuilder CalcStrSql = new Reservation().BuildCalcSqlString(ResData);
                if (!string.IsNullOrEmpty(ResData.ResMain.PromoCode) && string.Equals(userData.CustomRegID, TvBo.Common.crID_Kenba))
                {
                    CalcStrSql.AppendFormat("exec dbo.SetResPromoCode '{0}', 1 \n", ResNo);
                }
                CalcStrSql.Append("Declare @PassEB bit \n");
                CalcStrSql.Append("Declare @ErrCode SmallInt \n");
                CalcStrSql.Append("Declare @Supplier varchar(10) \n");
                CalcStrSql.AppendFormat("exec dbo.usp_Calc_Res_Price '{0}', Default, Default, @PassEB OutPut, @ErrCode OutPut, 1 \n", ResNo);
                CalcStrSql.Append("Select * From #ResMain \n");
                CalcStrSql.Append("Select * From #ResService \n");
                CalcStrSql.Append("Select * From #ResServiceExt \n");
                CalcStrSql.Append("Select * From #ResCust \n");
                CalcStrSql.Append("Select * From #ResCustPrice \n");
                CalcStrSql.Append("Select * From #ResSupDis \n");
                CalcStrSql.Append("Select ErrorCode=@ErrCode \n");
                CalcStrSql.Append("Select * From #ResPromo \n");
                CalcStrSql.AppendFormat("Exec dbo.GetPromoList '{0}', 1 \n", ResNo);
                CalcStrSql.AppendFormat("Select * From #CalcErrTbl \n");
                Database db = (Database)DatabaseFactory.CreateDatabase();
                DbCommand dbCommand = db.GetSqlStringCommand(CalcStrSql.ToString());
                DataSet ds;
                try
                {
                    ds = db.ExecuteDataSet(dbCommand);
                    if (ds != null)
                    {
                        ds.Tables[0].TableName = "ResMain";
                        ds.Tables[1].TableName = "ResService";
                        ds.Tables[2].TableName = "ResServiceExt";
                        ds.Tables[3].TableName = "ResCust";
                        ds.Tables[4].TableName = "ResCustPrice";
                        ds.Tables[5].TableName = "ResSupDis";
                        ds.Tables[6].TableName = "ErrorTable";
                        ds.Tables[7].TableName = "ResPromo";
                        ds.Tables[8].TableName = "PromoList";
                        ds.Tables[ds.Tables.Count - 1].TableName = "CalcErrTbl";
                    }
                    else
                    {
                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationPriceNotCalculated").ToString();
                        ds = null;
                        return ResData;
                    }
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    ds = null;
                    return ResData;
                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
                #endregion Calculate ResCustPrice

                int ErrorCode = Convert.ToInt32(ds.Tables["ErrorTable"].Rows[0]["ErrorCode"].ToString());
                if (ErrorCode != 0)
                {
                    errorMsg = new ResCalcError().calcError(ErrorCode, ResData);
                    return ResData;
                }

                ResData = new Reservation().SetCalcDataToResData(userData, ResData, ds, false, ref errorMsg);

                bool CreateChildAge = true;

                if (userData.WebService)
                    CreateChildAge = true;
                else
                {
                    object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
                    CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
                }

                string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "Version"));

                if (!string.Equals(version, "V2"))
                    if (!CreateChildAge)
                    {
                        foreach (ResCustRecord row in ResData.ResCust)
                        {
                            row.Age = null;
                            row.Birtday = null;
                        }
                    }

                List<PromoListRecord> PromoList = ResData.PromoList;
                PromoList = new Promos().getPromoListTemplate(userData.Market, PromoList, ref errorMsg);

                TvParameters MarketParams = new TvBo.Common().getTvParameters(ResData.ResMain.PLMarket, Conversion.getDecimalOrNull(userData.TvVersion), userData.WebVersion, ref errorMsg);
                resMain = ResData.ResMain;
                resMain.Agency = agencyBak;

                if (MarketParams.TvParamReser.ExtSerConfToResConf)
                {
                    var query = ResData.ResService.Where(w => w.StatConf.HasValue && w.StatConf.Value < 2).Select(s => s.StatConf).Union(ResData.ResServiceExt.Where(w => w.StatConf.HasValue && w.StatConf.Value < 2).Select(s => s.StatConf)).GroupBy(g => g).Select(k => k.Key);
                    if (query.Count() == 1)
                        resMain.ConfStat = query.First();
                    else if (query.Count() > 1)
                        resMain.ConfStat = 0;
                }
                else
                {
                    var query = from q in ResData.ResService
                                where q.StatConf.HasValue && q.StatConf.Value < 2
                                group q by new { q.StatConf } into k
                                select new { k.Key.StatConf };
                    if (query.Count() == 1)
                        resMain.ConfStat = query.First().StatConf;
                    else if (query.Count() > 1)
                        resMain.ConfStat = 0;
                }

                return ResData;
            }
            catch
            {
                return null;
            }
        }
    }
}
