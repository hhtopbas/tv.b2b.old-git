﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace TurizmParkClient
{
    public class MasterSearch
    {

        public static TurizmParkClient.Services.Contracts.Domestic.TravelSearchResponse OneWaySearchDomestic(DateTime date, string from, string to, TurizmParkClient.Services.Contracts.Domestic.PaxPlan pax, TurizmParkClient.TParkClient.MessageFormat format = TParkClient.MessageFormat.Json)
        {
            TurizmParkLoginUser user = TurizmParkClient.MasterSearchUtils.getTurizmParkLoginUser();
            TParkClient client = new TParkClient();

            TurizmParkClient.Services.Contracts.Domestic.TravelSearch param = new TurizmParkClient.Services.Contracts.Domestic.TravelSearch();
            param.Date = date.ToString("ddMMyyyy");
            param.Org = from;
            param.Dest = to;
            param.Pax = pax;
            param.ServiceType = 3;

            var response = client.Process<TurizmParkClient.Services.Contracts.Domestic.TravelSearchRequest, TurizmParkClient.Services.Contracts.Domestic.TravelSearchResponse>(
                MasterSearchUtils.getServiceUrl(true) + "/MasterSearch",
                new TurizmParkClient.Services.Contracts.Domestic.TravelSearchRequest
                {
                    SearchParameters = param,
                    ClientName = user.ClientName,
                    Password = user.Password,
                }, format);

            return response;
        }

        public static TurizmParkClient.Services.International.Methods.TravelSearchResponse OneWaySearchInternational(DateTime date, string from, string to, TurizmParkClient.Services.International.Elements.Search.PaxPlan pax, TurizmParkClient.TParkClient.MessageFormat format = TParkClient.MessageFormat.Json)
        {
            TurizmParkLoginUser user = TurizmParkClient.MasterSearchUtils.getTurizmParkLoginUser();
            TParkClient client = new TParkClient();

            TurizmParkClient.Services.International.Elements.Search.TravelSearch param = new Services.International.Elements.Search.TravelSearch();
            param.Date = date.ToString("ddMMyyyy");
            param.Org = from;
            param.Dest = to;
            TurizmParkClient.Services.International.Elements.Search.PaxPlan _pax = new Services.International.Elements.Search.PaxPlan();
            param.Pax = pax;
            param.Cabin = "";

            var response = client.Process<TurizmParkClient.Services.International.Methods.TravelSearchRequest, TurizmParkClient.Services.International.Methods.TravelSearchResponse>(
                MasterSearchUtils.getServiceUrl(true) + "/MasterSearch",
                new TurizmParkClient.Services.International.Methods.TravelSearchRequest
                {
                    query = param,
                    ClientName = user.ClientName,
                    Password = user.Password,
                }, format);

            return response;
        }

        public static TurizmParkClient.Services.Contracts.Domestic.TravelSearchResponse ReturnSearch(DateTime date, DateTime rDate, string from, string to, TurizmParkClient.Services.Contracts.Domestic.PaxPlan pax, TurizmParkClient.TParkClient.MessageFormat format = TParkClient.MessageFormat.Json)
        {
            TurizmParkLoginUser user = TurizmParkClient.MasterSearchUtils.getTurizmParkLoginUser();
            TParkClient client = new TParkClient();
            TurizmParkClient.Services.Contracts.Domestic.TravelSearch param = new TurizmParkClient.Services.Contracts.Domestic.TravelSearch();
            param.Date = date.ToString("ddMMyyyy");
            param.rDate = rDate.ToString("ddMMyyyy");
            param.Org = from;
            param.Dest = to;
            param.Pax = pax;
            param.ServiceType = 2;

            var response = client.Process<TurizmParkClient.Services.Contracts.Domestic.TravelSearchRequest, TurizmParkClient.Services.Contracts.Domestic.TravelSearchResponse>(
                MasterSearchUtils.getServiceUrl(true) + "/MasterSearch",
                new TurizmParkClient.Services.Contracts.Domestic.TravelSearchRequest
                {
                    SearchParameters = param,
                    ClientName = user.ClientName,
                    Password = user.Password,
                }, format);

            return response;
        }

        public static TurizmParkClient.Services.International.Methods.TravelSearchResponse ReturnSearchInternational(DateTime date, DateTime dateReturn, string from, string to, TurizmParkClient.Services.International.Elements.Search.PaxPlan pax, TurizmParkClient.TParkClient.MessageFormat format = TParkClient.MessageFormat.Json)
        {
            TurizmParkLoginUser user = TurizmParkClient.MasterSearchUtils.getTurizmParkLoginUser();
            TParkClient client = new TParkClient();

            TurizmParkClient.Services.International.Elements.Search.TravelSearch param = new Services.International.Elements.Search.TravelSearch();
            param.Date = date.ToString("ddMMyyyy");
            param.rDate = dateReturn.ToString("ddMMyyyy");
            param.Org = from;
            param.Dest = to;          
            TurizmParkClient.Services.International.Elements.Search.PaxPlan _pax = new Services.International.Elements.Search.PaxPlan();
            param.Pax = pax;
            param.Cabin = "";

            var response = client.Process<TurizmParkClient.Services.International.Methods.TravelSearchRequest, TurizmParkClient.Services.International.Methods.TravelSearchResponse>(
                MasterSearchUtils.getServiceUrl(false) + "/MasterSearch",
                new TurizmParkClient.Services.International.Methods.TravelSearchRequest
                {
                    query = param,
                    ClientName = user.ClientName,
                    Password = user.Password,
                }, format);

            return response;
        }

        public static TurizmParkClient.Services.Contracts.Domestic.PriceWithBookingClassResponse OneWayPricing(TurizmParkClient.Services.Contracts.Domestic.Recommendation dep, TurizmParkClient.Services.Contracts.Domestic.PaxPlan pax, TurizmParkClient.TParkClient.MessageFormat format)
        {
            TurizmParkLoginUser user = TurizmParkClient.MasterSearchUtils.getTurizmParkLoginUser();

            TParkClient client = new TParkClient();

            TurizmParkClient.Services.Contracts.Domestic.Recommendation[] itinerary = new TurizmParkClient.Services.Contracts.Domestic.Recommendation[1];
            itinerary[0] = dep;
            var response = client.Process<TurizmParkClient.Services.Contracts.Domestic.PriceWithBookingClassRequest, TurizmParkClient.Services.Contracts.Domestic.PriceWithBookingClassResponse>(
                MasterSearchUtils.getServiceUrl(true) + "/PriceWithBookingClass",
                new TurizmParkClient.Services.Contracts.Domestic.PriceWithBookingClassRequest
                {
                    ClientName = user.ClientName,
                    Password = user.Password,
                    Itinerary = itinerary,
                    PaxPlan = pax
                },
                msgFormat: format);

            return response;
        }

        public static TurizmParkClient.Services.Contracts.Domestic.PriceWithBookingClassResponse ReturnPricing(TurizmParkClient.Services.Contracts.Domestic.Recommendation dep, TurizmParkClient.Services.Contracts.Domestic.Recommendation ret, TurizmParkClient.Services.Contracts.Domestic.PaxPlan pax, TurizmParkClient.TParkClient.MessageFormat format)
        {
            TurizmParkLoginUser user = TurizmParkClient.MasterSearchUtils.getTurizmParkLoginUser();

            TParkClient client = new TParkClient();

            TurizmParkClient.Services.Contracts.Domestic.Recommendation[] itinerary = new TurizmParkClient.Services.Contracts.Domestic.Recommendation[ret == null ? 1 : 2];
            itinerary[0] = dep;
            if (ret != null)
                itinerary[1] = ret;
            var response = client.Process<TurizmParkClient.Services.Contracts.Domestic.PriceWithBookingClassRequest, TurizmParkClient.Services.Contracts.Domestic.PriceWithBookingClassResponse>(
                MasterSearchUtils.getServiceUrl(true) + "/PriceWithBookingClass",
                new TurizmParkClient.Services.Contracts.Domestic.PriceWithBookingClassRequest
                {
                    ClientName = user.ClientName,
                    Password = user.Password,
                    Itinerary = itinerary,
                    PaxPlan = pax
                },
                msgFormat: format);

            return response;
        }

        public static TurizmParkClient.Services.Contracts.Domestic.GetComissionTableResponse GetComissionTable()
        {
            TurizmParkLoginUser user = TurizmParkClient.MasterSearchUtils.getTurizmParkLoginUser();

            TParkClient client = new TParkClient();
            var response = client.Process<TurizmParkClient.Services.Contracts.Domestic.GetComissionTableRequest, TurizmParkClient.Services.Contracts.Domestic.GetComissionTableResponse>(
                MasterSearchUtils.getServiceUrl(true) + "/GetComissionTable",
                new TurizmParkClient.Services.Contracts.Domestic.GetComissionTableRequest
                {
                    ClientName = user.ClientName,
                    Password = user.Password
                },
                TParkClient.MessageFormat.Json);

            return response;
        }

        public static TurizmParkClient.Services.Contracts.Domestic.GetComissionResponse GetComissionReturn(TurizmParkClient.Services.Contracts.Domestic.Recommendation[] itinerary, TurizmParkClient.TParkClient.MessageFormat format)
        {
            TurizmParkLoginUser user = TurizmParkClient.MasterSearchUtils.getTurizmParkLoginUser();
            TParkClient client = new TParkClient();
            var response = client.Process<TurizmParkClient.Services.Contracts.Domestic.GetComissionRequest, TurizmParkClient.Services.Contracts.Domestic.GetComissionResponse>(
                MasterSearchUtils.getServiceUrl(true) + "/GetComission",
                new TurizmParkClient.Services.Contracts.Domestic.GetComissionRequest
                {
                    ClientName = user.ClientName,
                    Password = user.Password,
                    Itinerary = itinerary
                },
                msgFormat: format);

            return response;
        }
    }
}
