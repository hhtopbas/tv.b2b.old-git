using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using Telerik.Web;
using System.Text;

namespace TvLibrary
{
    class ItemTemplate : System.Web.UI.ITemplate
    {
        string text;
        string value;
        string objId;

        public ItemTemplate(string _text, string _value, string _objId)
        {
            text = _text;
            value = _value;
            objId = _objId;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            System.Web.UI.WebControls.CheckBox cb = new System.Web.UI.WebControls.CheckBox();
            cb.ID = "cb";
            cb.Style["margin-left"] = "7px";
            cb.DataBinding += new EventHandler(OnTextDataBinding);
            string javastring = "onCheckBoxClick_" + objId + "(this)";
            cb.Attributes.Add("onclick", javastring);
            container.Controls.Add(cb);

            System.Web.UI.WebControls.HiddenField value = new System.Web.UI.WebControls.HiddenField();
            value.ID = "value";
            value.DataBinding += new EventHandler(OnValueDataBinding);
            container.Controls.Add(value);
        }

        void OnTextDataBinding(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(text))
            {
                System.Web.UI.WebControls.CheckBox lbl = (System.Web.UI.WebControls.CheckBox)sender;
                Telerik.Web.UI.RadComboBoxItem iContainer = (Telerik.Web.UI.RadComboBoxItem)lbl.NamingContainer;
                DataRowView drv = ((DataRowView)iContainer.DataItem);
                lbl.Text = drv[text].ToString();
            }
        }

        void OnValueDataBinding(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(value))
            {
                System.Web.UI.WebControls.HiddenField lblValue = (System.Web.UI.WebControls.HiddenField)sender;
                Telerik.Web.UI.RadComboBoxItem iContainer = (Telerik.Web.UI.RadComboBoxItem)lblValue.NamingContainer;
                DataRowView drv = ((DataRowView)iContainer.DataItem);
                lblValue.Value = drv[value].ToString();
            }
        }

        /*
           <div style="margin-left:7px;" onclick="StopPropagation_San_ComboBox(event)">
                <asp:CheckBox runat="server" ID="San_ComboBox_cb" onclick="onCheckBoxClick_San_ComboBox(this)" />
                <asp:HiddenField runat="server" ID="San_ComboBox_value" Value='<%# Eval("value") %>' />
                <asp:Label runat="server" ID="San_ComboBox_cb_label" AssociatedControlID="San_ComboBox_cb"
                    Text='<%# Eval("name") %>' />
            </div>
         */
    }
}
