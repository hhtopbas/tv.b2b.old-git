﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace TvLibrary
{
    [DefaultProperty("Items"), System.Web.UI.ToolboxData("<{0}:SanCombo runat=server></{0}:SanCombo>")]
    public class SanMSCBSA : System.Web.UI.WebControls.ListControl        
    {        
        private Telerik.Web.UI.RadComboBox sanCombo;

        private string _iD;
        private System.Web.UI.WebControls.Unit _dropDownWidth = new System.Web.UI.WebControls.Unit("100px");
        private System.Web.UI.WebControls.Unit _width = new System.Web.UI.WebControls.Unit("100px");
        private System.Web.UI.WebControls.Unit _height = new System.Web.UI.WebControls.Unit("22px");
        private System.Web.UI.WebControls.Unit _maxDropDownHeight = new System.Web.UI.WebControls.Unit("100px");
        private bool _autoPostBack = false;
        private string _skin = "Simple";
        private string _selectAllText;
        private string _dataTextField;
        private string _dataValueField;
        private string _selectValue;

        public string SelectValue
        {
            get { return _selectValue; }
            set { _selectValue = value; }
        }

        public string SelectAllText
        {
            get { return _selectAllText; }
            set { _selectAllText = value; }
        }

        public Telerik.Web.UI.RadComboBoxItemCollection Items
        {
            get { return sanCombo.Items; }
            set
            {
                foreach (Telerik.Web.UI.RadComboBoxItem li in Items)
                    sanCombo.Items.Add(li);
            }
        }

        public object DataSource
        {
            get { return sanCombo.DataSource; }
            set { sanCombo.DataSource = value; }
        }

        public string DataValueField
        {
            get { return _dataValueField; }
            set { _dataValueField = value; }
        }

        public string DataTextField
        {
            get { return _dataTextField; }
            set { _dataTextField = value; }
        }

        public string Skin
        {
            get { return _skin; }
            set { _skin = value; }
        }

        public bool AutoPostBack
        {
            get { return _autoPostBack; }
            set { _autoPostBack = value; }
        }

        public string ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        public System.Web.UI.WebControls.Unit DropDownWidth
        {
            get { return _dropDownWidth; }
            set { _dropDownWidth = value; }
        }

        public System.Web.UI.WebControls.Unit Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public System.Web.UI.WebControls.Unit Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public System.Web.UI.WebControls.Unit maxDropDownHeight
        {
            get { return _maxDropDownHeight; }
            set { _maxDropDownHeight = value; }
        }

        public SanMSCBSA()
        { 
        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Label hdrlabel = (System.Web.UI.WebControls.Label)sanCombo.Header.FindControl("hdrLabel");
            if (string.IsNullOrEmpty(hdrlabel.Text))
                hdrlabel.Text = _selectAllText;
        }

        public void DataBind()
        {
            //base.DataBind();
            if (sanCombo != null && sanCombo.DataSource != null)
            {
                sanCombo.DataBind();
            }
        }

        protected void OnInit(EventArgs e)
        {
            //sanCombo = new RadComboBox();
            sanCombo.ID = _iD + "_SanCombo";
            sanCombo.Width = _width;
            sanCombo.Height = _height;
            sanCombo.AutoPostBack = false;
            sanCombo.Skin = _skin;
            sanCombo.DropDownWidth = _dropDownWidth;
            sanCombo.MaxHeight = _maxDropDownHeight;

            this.Controls.Add(sanCombo);

            sanCombo.HeaderTemplate = new HeaderTemplate(this.ID);

            sanCombo.ItemTemplate = new ItemTemplate(_dataTextField, _dataValueField, this.ID);

            sanCombo.OnClientLoad = "onClientLoad_" + this.ID;
            sanCombo.OnClientDropDownOpening = "onClientDropDownOpening_" + this.ID;
            sanCombo.OnClientDropDownClosing = "onDropDownClosing_" + this.ID;
            sanCombo.SelectedIndexChanged += new Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventHandler(onPostBack);

            base.OnInit(e);
        }

        /*
             <telerik:RadComboBox ID="San_ComboBox" runat="server" Width="150px" Skin="Simple"
                OnClientDropDownClosing="onDropDownClosing_San_ComboBox" OnClientDropDownOpening="onClientDropDownOpening_San_ComboBox"
                DropDownWidth="300px" MaxHeight="150px" AutoPostBack="True" OnClientLoad="onClientLoad_San_ComboBox"
                SortCaseSensitive="False" OnSelectedIndexChanged="San_ComboBox_SelectedIndexChanged">
        */

        protected void Render(System.Web.UI.HtmlTextWriter writer)
        {
            StringBuilder sb = new StringBuilder();

            System.IO.TextWriter tw = new System.IO.StringWriter(sb);

            System.Web.UI.HtmlTextWriter OriginalStream = new System.Web.UI.HtmlTextWriter(tw);

            base.Render(OriginalStream);

            string s = sb.ToString();

            StringBuilder jScript = new StringBuilder();
            jScript.Append("<script language=\"javascript\" type=\"text/javascript\">");
            jScript.Append("    var cancelDropDownClosing_" + this.ID + " = false;");

            #region function StopPropagation_
            jScript.Append("    function StopPropagation_" + this.ID + "(e) {");
            jScript.Append("        e.cancelBubble = true;");
            jScript.Append("        if (e.stopPropagation) {");
            jScript.Append("            e.stopPropagation();");
            jScript.Append("        }");
            jScript.Append("    }");
            #endregion

            #region function onClientDropDownOpening_
            jScript.Append("    function onClientDropDownOpening_" + this.ID + "(sender, eventArgs) {");
            jScript.Append("        cancelDropDownClosing_" + this.ID + " = true;");
            jScript.Append("    }");
            #endregion

            #region function onCheckBoxClick_
            jScript.Append("    function onCheckBoxClick_" + this.ID + "(chk) {");
            jScript.Append("        if (chk == null) return;");
            jScript.Append("        var comboName = '" + this.ClientID + "_" + this.ID + "'+'_SanCombo';");
            jScript.Append("        var combo = $find(comboName);");
            jScript.Append("        if (combo == null) return;");
            jScript.Append("        var comboCheckAll = document.getElementById(comboName + '_Header_cb_all');");
            jScript.Append("        if (comboCheckAll == null) return;");
            jScript.Append("        cancelDropDownClosing_" + this.ID + " = true;");
            jScript.Append("        var text = \"\";");
            jScript.Append("        var values = \"\";");
            jScript.Append("        var items = combo.get_items();");
            jScript.Append("        if (items == null || items.get_count() < 1) return;");
            jScript.Append("        var checkedAll = true;");
            jScript.Append("        var noCheckedAll = true;");
            jScript.Append("        for (var i = 0; i < items.get_count(); i++) {");
            jScript.Append("            var item = items.getItem(i);");
            jScript.Append("            var chk1 = document.getElementById(comboName + \"_i\" + i + \"_cb\");"); // $get(comboName + \"_i\" + i + \"_cb\");");
            jScript.Append("            if (chk1.checked) {");
            jScript.Append("                noCheckedAll = false;");
            jScript.Append("                comboCheckAll.checked = false;");
            jScript.Append("                if (text.length > 0) text += \",\";");
            jScript.Append("                text += chk1.parentNode.getElementsByTagName('label')[0].innerHTML;");//item.get_text();");                
            jScript.Append("                var svalue = document.getElementById(comboName + \"_i\" + i + \"_value\");");
            jScript.Append("                if (values.length > 0) values += \"|\";");
            jScript.Append("                values += svalue.value;");
            jScript.Append("            }");
            jScript.Append("            else checkedAll = false;");
            jScript.Append("        }");
            jScript.Append("        var selectedValues = document.getElementById(comboName + '_Header_SelectedValue');");
            jScript.Append("        selectedValues.value = values;");
            jScript.Append("        if (checkedAll) {");
            jScript.Append("            comboCheckAll.checked = true;");
            jScript.Append("            text = comboCheckAll.parentNode.getElementsByTagName('label')[0].innerHTML;");
            jScript.Append("        }");
            jScript.Append("        if (text.length > 0) {");
            jScript.Append("            combo.set_text(text);");
            jScript.Append("        }");
            jScript.Append("        else {");
            jScript.Append("            combo.set_text(\"\");");
            jScript.Append("        }");
            jScript.Append("        if (noCheckedAll) {");
            jScript.Append("            comboCheckAll.checked = true;");
            jScript.Append("            onCheckBoxAllClickCA(comboCheckAll);");
            jScript.Append("        }");
            jScript.Append("    }");
            #endregion

            #region function onSelectAllClick_
            jScript.Append("    function onSelectAllClick_" + this.ID + "(chk) {");
            jScript.Append("        if (chk == null) return;");
            jScript.Append("        var comboName = '" + this.ClientID + "_" + this.ID + "'+'_SanCombo';");
            jScript.Append("        var combo = $find(comboName);");
            jScript.Append("        cancelDropDownClosing_" + this.ID + " = true;");
            jScript.Append("        var text = \"\";");
            jScript.Append("        var values = \"\";");
            jScript.Append("        var items = combo.get_items();");
            jScript.Append("        if (items == null || items.get_count() < 1) return;");
            jScript.Append("        for (var i = 0; i < items.get_count(); i++) {");
            jScript.Append("            var item = items.getItem(i);");
            jScript.Append("            var chk1 = document.getElementById(comboName + \"_i\" + i + \"_cb\");");
            jScript.Append("            if (chk1 == null) return;");
            jScript.Append("            if (chk.checked) chk1.checked = true; else chk1.checked = false;");
            jScript.Append("            if (chk1.checked) {");
            jScript.Append("                if (text.length > 0) text += \",\";");
            jScript.Append("                text += chk1.parentNode.getElementsByTagName('label')[0].innerHTML;");
            jScript.Append("                var svalue = document.getElementById(comboName + \"_i\" + i + \"_value\");");
            jScript.Append("                if (values.length > 0) values += \"|\";");
            jScript.Append("                values += svalue.value;");
            jScript.Append("            }");
            jScript.Append("        }");
            jScript.Append("        var selectedValues = document.getElementById(comboName + '_Header_SelectedValue');");
            jScript.Append("        selectedValues.value = values;");
            jScript.Append("        if (chk.checked) text = chk.parentNode.getElementsByTagName('label')[0].innerHTML;");
            jScript.Append("        if (text.length > 0) {");
            jScript.Append("            combo.set_text(text);");
            jScript.Append("        }");
            jScript.Append("        else {");
            jScript.Append("            combo.set_text(\"\");");
            jScript.Append("        }");
            jScript.Append("    }");
            #endregion

            #region function onClientLoad_
            jScript.Append("    function onClientLoad_" + this.ID + "(sender) {");
            jScript.Append("        var combo = sender;");
            jScript.Append("        var comboName = '" + this.ClientID + "_" + this.ID + "'+'_SanCombo';");
            jScript.Append("        var text = \"\";");
            jScript.Append("        var values = \"\";");
            jScript.Append("        var items = combo.get_items();");
            jScript.Append("        if (items == null || items.get_count() < 1) return;");
            jScript.Append("        var checkedAll = true;");
            jScript.Append("        var noCheckedAll = true;");
            jScript.Append("        var comboCheckAll = document.getElementById(comboName + '_Header_cb_all');");
            jScript.Append("        for (var i = 0; i < items.get_count(); i++) {");
            jScript.Append("            var item = items.getItem(i);");
            jScript.Append("            var chk1 = document.getElementById(comboName + \"_i\" + i + \"_cb\");");
            jScript.Append("            if (chk1.checked) {");
            jScript.Append("                noCheckedAll = false;");
            jScript.Append("                comboCheckAll.checked = false;");
            jScript.Append("                if (text.length > 0) text += \",\";");
            jScript.Append("                text += chk1.parentNode.getElementsByTagName('label')[0].innerHTML;");
            jScript.Append("                var svalue = document.getElementById(comboName + \"_i\" + i + \"_value\");");
            jScript.Append("                if (values.length > 0) values += \"|\";");
            jScript.Append("                values += svalue.value;");
            jScript.Append("            }");
            jScript.Append("            else checkedAll = false;");
            jScript.Append("        }");
            jScript.Append("        var selectedValues = document.getElementById(comboName + '_Header_SelectedValue');");
            jScript.Append("        selectedValues.value = values;");
            jScript.Append("        if (checkedAll) {");
            jScript.Append("            comboCheckAll.checked = true;");
            jScript.Append("            text = comboCheckAll.parentNode.getElementsByTagName('label')[0].innerHTML;");
            jScript.Append("        }");
            jScript.Append("        if (text.length > 0) {");
            jScript.Append("            combo.set_text(text);");
            jScript.Append("        }");
            jScript.Append("        else {");
            jScript.Append("            combo.set_text(\"\");");
            jScript.Append("        }");
            jScript.Append("    }");
            #endregion

            #region function onDropDownClosing_
            jScript.Append("    function onDropDownClosing_" + this.ID + "(sender, args) {");
            jScript.Append("        cancelDropDownClosing_" + this.ID + " = false;");
            jScript.Append("        var comboName = \"" + this.ClientID + "_" + this.ID + "_SanCombo" + "\";");
            jScript.Append("        var selectedValues = document.getElementById(comboName + '_Header_SelectedValue');");
            jScript.Append("        var selectedOldValues = document.getElementById(comboName + '_Header_SelectedOldValue');");
            jScript.Append("        if (selectedValues.value != selectedOldValues.value) {");
            jScript.Append("            selectedOldValues.value = selectedValues.value;");
            if (_autoPostBack)
                jScript.Append("            __doPostBack(\'" + this.ClientID + "_" + this.ID + "_SanCombo" + "\',\'onPostBack\');");
            //jScript.Append("            window.setTimeout('__doPostBack(\'" + this.ClientID + "_" + this.ID + "_SanCombo" + "\',\'onPostBack\')', 0, 'JavaScript');");        
            //jScript.Append("            window.setTimeout('__doPostBack(\' + sender.get_id() + \',\'onPostBack\')', 0, 'JavaScript');");            
            jScript.Append("        }");
            jScript.Append("    }");
            #endregion

            jScript.Append("</script>");

            writer.Write(jScript.ToString() + s);
        }

        protected void onPostBack(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            System.Web.UI.WebControls.HiddenField selectedValues = (System.Web.UI.WebControls.HiddenField)sanCombo.Header.FindControl("SelectedValue");
            System.Web.UI.WebControls.HiddenField selectedOldValues = (System.Web.UI.WebControls.HiddenField)sanCombo.Header.FindControl("SelectedOldValue");
            OnChanged(new onChangeEventArgs(selectedValues.Value, selectedOldValues.Value));
        }        

        public event onChangeEventHandler onChanged;
        public delegate void onChangeEventHandler(Object sender, onChangeEventArgs e);

        protected virtual void OnChanged(onChangeEventArgs e)
        {
            onChanged(this, e);
        }

    }

    public class onChangeEventArgs : EventArgs
    {
        private string oldValue;
        private string newValue;

        public onChangeEventArgs(string oldValue, string newValue)
        {
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        public string NewValue
        {
            get { return newValue; }
        }

        public string OldValue
        {
            get { return oldValue; }
        }

    }

}
