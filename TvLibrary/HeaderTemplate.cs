using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using Telerik.Web;
using System.Text;

namespace TvLibrary
{
    class HeaderTemplate : System.Web.UI.ITemplate
    {
        string _objId;

        public HeaderTemplate(string ObjID)
        {
            _objId = ObjID;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            //HtmlTextWriterTag.Div

            System.Web.UI.WebControls.CheckBox cb_all = new System.Web.UI.WebControls.CheckBox();
            cb_all.ID = "cb_all";
            string javastring = "onSelectAllClick_" + _objId + "(this)";
            cb_all.Attributes.Add("onclick", javastring);
            container.Controls.Add(cb_all);
            System.Web.UI.WebControls.Label hdrLabel = new System.Web.UI.WebControls.Label();
            hdrLabel.ID = "hdrLabel";
            hdrLabel.AssociatedControlID = cb_all.ID;
            container.Controls.Add(hdrLabel);
            System.Web.UI.WebControls.HiddenField selectedValue = new System.Web.UI.WebControls.HiddenField();
            selectedValue.ID = "SelectedValue";
            container.Controls.Add(selectedValue);
            System.Web.UI.WebControls.HiddenField selectedOldValue = new System.Web.UI.WebControls.HiddenField();
            selectedOldValue.ID = "SelectedOldValue";
            container.Controls.Add(selectedOldValue);
        }
        /*
            <div>
                <asp:CheckBox runat="server" ID="San_ComboBox_cb_all" onclick="onCheckBoxAllClick_San_ComboBox(this)" />
                <asp:Label runat="server" ID="hdrLabel" AssociatedControlID="San_ComboBox_cb_all"
                    Text="Select All" />
                <asp:HiddenField runat="server" ID="San_ComboBox_SelectedField" />
                <asp:HiddenField runat="server" ID="San_ComboBox_SelectedOldField" />
            </div>
        */
    }
}
