﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TvLibrary
{
    public class RTFtoHTML
    {        
        private System.Windows.Forms.RichTextBox _rtfSource = new System.Windows.Forms.RichTextBox();

        public RTFtoHTML()
        {

        }

        public string rtf
        {
            get { return _rtfSource.Rtf; }
            set { _rtfSource.Rtf = value; }
        }

        public string html()
        {
            return GetHtml();
        }

        private string HtmlColorFromColor(System.Drawing.Color clr)
        {
            string strReturn = string.Empty;
            if (clr.IsNamedColor)
                strReturn = clr.Name.ToLower();
            else
            {
                strReturn = clr.Name;
                if (strReturn.Length > 6)
                    strReturn = strReturn.Substring(strReturn.Length - 6, 6);
                strReturn = "#" + strReturn;
            }
            return strReturn;
        }

        private string HtmlFontStyleFromFont(System.Drawing.Font fnt)
        {
            string strReturn = string.Empty;

            //  style 
            if (fnt.Italic)
                strReturn += "italic ";
            else
                strReturn += "normal ";
            // variant
            strReturn += "normal ";

            // weight
            if (fnt.Bold)
                strReturn += "bold ";
            else
                strReturn += "normal ";

            // size
            strReturn += fnt.SizeInPoints.ToString() + "pt/normal ";

            // family
            strReturn += fnt.FontFamily.Name;

            return strReturn;
        }

        private string GetHtml()
        {
            string strReturn = "<div>";
            System.Drawing.Color clrForeColor = System.Drawing.Color.Black;
            System.Drawing.Color clrBackColor = System.Drawing.Color.Black;
            System.Drawing.Font fntCurrentFont = _rtfSource.Font;
            System.Windows.Forms.HorizontalAlignment altCurrent = System.Windows.Forms.HorizontalAlignment.Left;
            for (int intPos = 0; intPos < _rtfSource.Text.Length - 1; intPos++)
            {
                _rtfSource.Select(intPos, 1);

                #region  Fore color
                if (intPos == 0)
                {
                    strReturn += "<span style=\"color:" + HtmlColorFromColor(_rtfSource.SelectionColor) + "\">";
                    clrForeColor = _rtfSource.SelectionColor;
                }
                else
                {
                    if (_rtfSource.SelectionColor == clrForeColor)
                    {

                    }
                    else
                    {
                        strReturn += "</span>";
                        strReturn += "<span style=\"color:" + HtmlColorFromColor(_rtfSource.SelectionColor) + "\">";
                        clrForeColor = _rtfSource.SelectionColor;
                    }
                }
                #endregion  Fore color

                #region Font Type
                if (intPos == 0)
                {
                    strReturn += "<span style=\"font:" + HtmlFontStyleFromFont(_rtfSource.SelectionFont) + "\">";
                    fntCurrentFont = _rtfSource.SelectionFont;
                }
                else
                {
                    if (_rtfSource.SelectionFont.GetHashCode() == fntCurrentFont.GetHashCode())
                    {

                    }
                    else
                    {
                        strReturn += "</span>";
                        strReturn += "<span style=\"font:" + HtmlFontStyleFromFont(_rtfSource.SelectionFont) + "\">";
                        fntCurrentFont = _rtfSource.SelectionFont;
                    }
                }
                #endregion Font Type

                #region Background color
                if (intPos == 0)
                {
                    strReturn += "<span style=\"background-color:" + HtmlColorFromColor(_rtfSource.SelectionBackColor) + "\">";
                    clrBackColor = _rtfSource.SelectionBackColor;
                }
                else
                {
                    if (_rtfSource.SelectionBackColor == clrBackColor)
                    {

                    }
                    else
                    {
                        strReturn += "</span>";
                        strReturn += "<span style=\"background-color:" + HtmlColorFromColor(_rtfSource.SelectionBackColor) + "\">";
                        clrBackColor = _rtfSource.SelectionBackColor;
                    }
                }
                #endregion Background color

                #region Alignment
                if (intPos == 0)
                {
                    strReturn += "<p style=\"text-align:" + _rtfSource.SelectionAlignment.ToString() + "\">";
                    altCurrent = _rtfSource.SelectionAlignment;
                }
                else
                {
                    if (_rtfSource.SelectionAlignment == altCurrent)
                    {

                    }
                    else
                    {
                        strReturn += "</p>";
                        strReturn += "<p style=\"text-align:" + _rtfSource.SelectionAlignment.ToString() + "\">";
                        altCurrent = _rtfSource.SelectionAlignment;
                    }
                }
                #endregion Alignment

                strReturn += _rtfSource.Text.Substring(intPos, 1);
            }

            strReturn += "</span>";
            strReturn += "</span>";
            strReturn += "</span>";
            strReturn += "</p>";
            strReturn += "</div>";

            strReturn = strReturn.Replace(Convert.ToChar(10).ToString(), "<br />");

            return strReturn;
        }
    }
}
