﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TvLibrary
{
    public class IntToWordRU
    {
        private string[] t_units_female = new string[10] { "ноль", "одна", "две", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять" };
        private string[] t_units_male = new string[10] { "ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять" };
        private string[] t_between10and20 = new string[] { "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать" };
        private string[] t_decades = new string[] { "ноль", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто" };
        private string[] t_hundrets = new string[] { "ноль", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот" };
        private string[][] t_razryad = new string[][] { new string[] { "", "", "", "" }, new string[] { "тысяча", "тысячи", "тысяч" }, new string[] { "миллион", "миллиона", "миллионов" }, new string[] { "миллиард", "миллиарда", "миллиардов" } };
        private bool[] t_razryad_isMale = new bool[] { false, false, true, true };
        private int num_of_razryad = 4;
        private int max_rub_kop_distance = 7;
        bool result;

        bool isNumeric(string value)
        {
            bool retVal = false;
            if (string.IsNullOrEmpty(value)) return false;
            try
            {
                Int16 decVal = Convert.ToInt16(value);
                retVal = true;
            }
            catch
            {
                retVal = false;
            }
            return retVal;
        }

        public int[] parse(string n)        
        {
            n = n.Replace(" ", "");
            string r = string.Empty;
            int processNo = 0;
            Int32 rubles = 0;
            Int32 kopeyki = 0;
            int rub_kop_distance = 0;
            for (int i = 0; i <= n.Length; i++)
            {

                if (i == n.Length)
                {
                    switch (processNo)
                    {
                        case 0: //ищем начало рублей
                            rubles = 0;
                            kopeyki = 0;
                            //throw new IllegalArgumentException("Нет цифр.");
                            break;
                        case 1: //рубли в процессе
                            result = Int32.TryParse(r, out rubles);
                            kopeyki = 0;
                            break;
                        case 2: //ищем копейки
                            kopeyki = 0;
                            break;
                        case 3: //первая цифра копеек найдена, 2я - нет
                            result = Int32.TryParse(r + "0", out kopeyki);
                            break;
                    }
                    break;
                }

                if (processNo == 0)
                {
                    if (isNumeric(n.Substring(i, 1)))
                    {
                        processNo++;
                        r += n.Substring(i, 1); //.Substring(i);
                        continue;
                    }
                }
                if (processNo == 1)
                {
                    if (isNumeric(n.Substring(i, 1)))
                    {
                        r += n.Substring(i, 1);
                    }
                    else
                    {
                        if (n.Substring(i, 1) != " ")
                        {
                            processNo++;
                            result = Int32.TryParse(r, out rubles);
                            r = string.Empty;
                            continue;
                        }
                    }
                }

                if (processNo == 2)
                {
                    if (isNumeric(n.Substring(i, 1)))
                    {
                        processNo++;
                        r += n.Substring(i, 1);
                        continue;
                    }

                    if (rub_kop_distance > max_rub_kop_distance)
                    {
                        kopeyki = 0;
                        break;
                    }
                    rub_kop_distance++;
                }

                if (processNo == 3)
                {
                    if (n.Substring(i, 1) != " ")
                        if (isNumeric(n.Substring(i)))
                        {
                            processNo++;
                            r += n.Substring(i);
                            //kopeyki = Integer.parseInt(r);
                            break;
                        }
                        else
                        {
                            result = Int32.TryParse(r + "0", out kopeyki);
                            break;
                        }
                }
            }


            return new int[] { rubles, kopeyki };
        }

        private string processRazryad(int n, bool isMale)
        {
            string r = string.Empty;
            int n1 = n % 10; n = n / 10;
            int n2 = n % 10; n = n / 10;
            int n3 = n % 10;
            if (n3 != 0) r += t_hundrets[n3] + " ";
            if (n2 > 1)
            {
                r += t_decades[n2] + " ";
                if (((n1 != 0))
                || ((n1 == 0) && (n2 == 0) && (n3 == 0)))
                    r += (isMale ? t_units_male[n1] : t_units_female[n1]);
            }
            else
                if (n2 == 1) r += t_between10and20[n1];
                else // if n2==0
                    if (((n1 != 0))
                    || ((n1 == 0) && (n2 == 0) && (n3 == 0)))
                        r += (isMale ? t_units_male[n1] : t_units_female[n1]);
            return r.Trim();
        }

        public string process(string n_rub_kop) //:throws IllegalArgumentException
        {
            string r = string.Empty;
            int[] rub_kop = parse(n_rub_kop);
            string[] finText = new string[3] { "рубль", "рубля", "рублей" };
            r += process(rub_kop[0], true, finText) + " ";

            r += (rub_kop[1] < 10 ? "0" + rub_kop[1].ToString() : rub_kop[1].ToString()) + " ";

            if ((rub_kop[1] % 10 == 1) && ((rub_kop[1] % 100 > 20) || (rub_kop[1] % 100 < 10)))
                r += "копейка";
            else
            {
                if (((rub_kop[1] % 10 == 2) || (rub_kop[1] % 10 == 3) || (rub_kop[1] % 10 == 4))
                && ((rub_kop[1] % 100 > 20) || (rub_kop[1] % 100 < 10)))
                    r += "копейки";
                else
                    r += "копеек";
            }

            return r.Trim();
        }

        public string process(int n, bool isMale)
        {
            return process(n, isMale, new String[] { "", "", "", });
        }

        public string process(int n, bool isMale, string[] finText)
        {
            String r = "";
            int[] razryad = new int[num_of_razryad];
            for (int i = 0; i < num_of_razryad; i++)
            {
                razryad[i] = n % 1000; n = n / 1000;
            }
            for (int i = num_of_razryad - 1; i > 0; i--)
            {
                if (razryad[i] != 0)
                {
                    if ((razryad[i] % 10 == 1)
                    && ((razryad[i] % 100 > 20) || (razryad[i] % 100 < 10)))
                        r += processRazryad(razryad[i], t_razryad_isMale[i]) + " " + t_razryad[i][0] + " ";
                    else
                    {
                        if (((razryad[i] % 10 == 2) || (razryad[i] % 10 == 3) || (razryad[i] % 10 == 4))
                        && ((razryad[i] % 100 > 20) || (razryad[i] % 100 < 10)))
                            r += processRazryad(razryad[i], t_razryad_isMale[i]) + " " + t_razryad[i][1] + " ";
                        else
                            r += processRazryad(razryad[i], t_razryad_isMale[i]) + " " + t_razryad[i][2] + " ";
                    }
                }
            }

            if ((razryad[0] != 0) || (r.Length == 0))
                r += processRazryad(razryad[0], isMale) + " ";

            if (razryad[0] != 0)
            {
                if ((razryad[0] % 10 == 1)
                && ((razryad[0] % 100 > 20) || (razryad[0] % 100 < 10)))
                    r += finText[0];
                else
                {
                    if (((razryad[0] % 10 == 2) || (razryad[0] % 10 == 3) || (razryad[0] % 10 == 4))
                    && ((razryad[0] % 100 > 20) || (razryad[0] % 100 < 10)))
                        r += finText[1];
                    else
                        r += finText[2];
                }
            }
            else
            {
                r += finText[2];
            }


            return r.Trim();
        }

    }
}

