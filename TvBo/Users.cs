﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Data.Common;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TvTools;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;

namespace TvBo
{
    public class Users
    {
        public User CreateUserData()
        {
            string TvVersion = string.Empty;
            string WebVersion = string.Empty;
            string CustomRegID = string.Empty;

            string tsql = @"Select Version, WVersion, CustomRegID From ParamSystem (NOLOCK) Where Market = ''";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        TvVersion = (string)oReader["Version"];
                        WebVersion = (string)oReader["WVersion"];
                        CustomRegID = (string)oReader["CustomRegID"];
                    }
                }
            }
            catch { }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

            #region ResMain Column
            User user = new User();

            user.GridColumnOptions = new List<GridOptions>();
            user.CustomRegID = CustomRegID;
            user.TvVersion = TvVersion;
            user.WebVersion = WebVersion;
            user.OwnAgency = false;
            user.ShowAllRes = false;
            user.MaxRoomCount = 3;
            user.ShowFlight = false;
            user.WebService = false;
            user.Aggregator = false;
            user.IsB2CAgency = false;
            #endregion ResMain Column

            return user;
        }

        public void setLoginUser(User UserData, ref string errorMsg)
        {
            string tsql = @"if Exists(Select SESSIONID From WEBUserLog Where SESSIONID=@SESSIONID And LOGOUT is null)
                            Begin
	                            Update WEBUserLog
                                Set LOGOUT=GetDate(), INUSE=Cast(0 AS bit)
                                Where SESSIONID=@SESSIONID And LOGOUT is null
                            End

                            INSERT INTO WEBUserLog (SESSIONID, USERID, AGENCYID, USERIP, BROWSERINFO, LOGIN, LOGOUT, INUSE)
                            VALUES (@SESSIONID, @USERID, @AGENCYID, @USERIP, @BROWSERINFO, GetDate(), null,Cast(1 AS bit))";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "SESSIONID", DbType.String, UserData.SID);
                db.AddInParameter(dbCommand, "USERID", DbType.String, UserData.UserID);
                db.AddInParameter(dbCommand, "AGENCYID", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "USERIP", DbType.String, UserData.IpNumber);
                db.AddInParameter(dbCommand, "BROWSERINFO", DbType.String, HttpContext.Current.Request.UserAgent);
                db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public void setLogoutUser(string SID, bool inUse, ref string errorMsg)
        {
            string tsql = @"Update WEBUserLog
                            Set LOGOUT = GetDate(), INUSE = @InUse
                            Where SESSIONID = @SESSIONID AND LOGOUT is null";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "SESSIONID", DbType.String, SID);
                db.AddInParameter(dbCommand, "InUse", DbType.Boolean, inUse);
                db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool editAgencyUser(string agency, string code, string name, string status, string pin, string phone, string mobile, string eMail,
                                   string hAddress, string showAllRes, string password, string expDate, string expedintID, ref string errorMsg)
        {
            string setValues = string.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("Name='{0}'", name);
            }
            if (!string.IsNullOrEmpty(phone))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("Phone='{0}'", phone);
            }
            if (!string.IsNullOrEmpty(pin))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("PIN='{0}'", pin);
            }
            if (!string.IsNullOrEmpty(mobile))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("Mobile='{0}'", mobile);
            }
            if (!string.IsNullOrEmpty(eMail))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("EMail='{0}'", eMail);
            }
            if (!string.IsNullOrEmpty(hAddress))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("HAddress='{0}'", hAddress);
            }

            if (!string.IsNullOrEmpty(password))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("Pass='{0}'", password);
            }
            if (!string.IsNullOrEmpty(expedintID))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("ExpedientID='{0}'", expedintID);
            }

            if (!string.IsNullOrEmpty(status))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("Status='{0}'", (Equals(status.ToLower(new CultureInfo("en-US", false)), "on") || Equals(status.ToLower(new CultureInfo("en-US", false)), "true") || Equals(status, "1")) ? "Y" : "N");
            }
            DateTime? _expDate = null;
            if (!string.IsNullOrEmpty(expDate))
            {
                string[] ydm = expDate.Split(':');
                if (!string.IsNullOrEmpty(ydm[0].ToString()) || !string.IsNullOrEmpty(ydm[1].ToString()) || !string.IsNullOrEmpty(ydm[2].ToString()))
                {
                    try
                    {
                        int y, m, d;
                        y = Convert.ToInt32(ydm[0].ToString());
                        m = Convert.ToInt32(ydm[1].ToString());
                        d = Convert.ToInt32(ydm[2].ToString());
                        if (setValues.Length > 0) setValues += ", ";
                        setValues += "ExpDate=@ExpDate ";
                        _expDate = new DateTime(y, m, d);
                    }
                    catch { }
                }
            }
            if (!string.IsNullOrEmpty(showAllRes))
            {
                if (setValues.Length > 0) setValues += ", ";
                setValues += string.Format("ShowAllRes='{0}'", (Equals(showAllRes.ToLower(new CultureInfo("en-US", false)), "on") || Equals(showAllRes.ToLower(new CultureInfo("en-US", false)), "true") || Equals(showAllRes, "1")) ? "Y" : "N");
            }

            string tsql = string.Empty;
            tsql = string.Format(@" Update AgencyUser
                                    Set {0}
                                    Where Agency=@AgencyID And Code=@Code ", setValues);
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "AgencyID", DbType.String, agency);
                db.AddInParameter(dbCommand, "Code", DbType.String, code);
                if (_expDate.HasValue) db.AddInParameter(dbCommand, "ExpDate", DbType.DateTime, _expDate.Value);
                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool createAgencyUser(string agency, string code, string name, string status, string pin, string phone, string mobile, string eMail,
                                     string hAddress, string showAllRes, string password, string expDate, string expedintID, ref string errorMsg)
        {
            string setValues = string.Empty;
            string setFields = string.Empty;
            if (string.IsNullOrEmpty(agency) || string.IsNullOrEmpty(code) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(password))
            {
                errorMsg = "Code, Name and Password fields is required.";
                return false;
            }
            if (!string.IsNullOrEmpty(agency))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", agency.ToUpper(new CultureInfo("en-US", false)));
                setFields += "Agency";
            }
            if (!string.IsNullOrEmpty(code))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", code.ToUpper(new CultureInfo("en-US", false)));
                setFields += "Code";
            }
            if (!string.IsNullOrEmpty(name))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", name);
                setFields += "Name";
            }
            if (!string.IsNullOrEmpty(phone))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", phone);
                setFields += "Phone";
            }
            if (!string.IsNullOrEmpty(pin))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", pin);
                setFields += "PIN";
            }
            if (!string.IsNullOrEmpty(mobile))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", mobile);
                setFields += "Mobile";
            }
            if (!string.IsNullOrEmpty(eMail))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", eMail);
                setFields += "EMail";
            }
            if (!string.IsNullOrEmpty(hAddress))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", hAddress);
                setFields += "HAddress";
            }
            if (!string.IsNullOrEmpty(password))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", password);
                setFields += "Pass";
            }
            if (!string.IsNullOrEmpty(expedintID))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", expedintID);
                setFields += "ExpedientID";
            }
            if (!string.IsNullOrEmpty(status))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", (Equals(status.ToLower(new CultureInfo("en-US", false)), "on") || Equals(status.ToLower(new CultureInfo("en-US", false)), "true") || Equals(status, "1")) ? "Y" : "N");
                setFields += "Status";
            }
            DateTime? _expDate = null;
            if (!string.IsNullOrEmpty(expDate))
            {
                string[] ydm = expDate.Split(':');
                if (!string.IsNullOrEmpty(ydm[0].ToString()) || !string.IsNullOrEmpty(ydm[1].ToString()) || !string.IsNullOrEmpty(ydm[2].ToString()))
                {
                    try
                    {
                        int y, m, d;
                        y = Convert.ToInt32(ydm[0].ToString());
                        m = Convert.ToInt32(ydm[1].ToString());
                        d = Convert.ToInt32(ydm[2].ToString());
                        if (setValues.Length > 0) setValues += ", ";
                        if (setFields.Length > 0) setFields += ", ";
                        setValues += "@ExpDate";
                        setFields += "ExpDate";
                        _expDate = DateTime.MinValue.AddYears(y).AddMonths(m).AddDays(d);

                    }
                    catch { }
                }
            }
            if (!string.IsNullOrEmpty(showAllRes))
            {
                if (setValues.Length > 0) setValues += ", ";
                if (setFields.Length > 0) setFields += ", ";
                setValues += string.Format("'{0}'", (Equals(showAllRes.ToLower(new CultureInfo("en-US", false)), "on") || Equals(showAllRes.ToLower(new CultureInfo("en-US", false)), "true") || Equals(showAllRes, "1")) ? "Y" : "N");
                setFields += "ShowAllRes";
            }
            setValues += ",0";
            setFields += ",CanEditOthRes";
            string tsql = string.Empty;
            tsql = string.Format(@" Insert Into AgencyUser ({0})
                                    Values ({1}) ", setFields, setValues);
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                if (_expDate.HasValue) db.AddInParameter(dbCommand, "ExpDate", DbType.DateTime, _expDate.Value);
                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public User getReservationCreateUserData(string ResNo, ref string errorMsg)
        {
            User UserData = new TvBo.Users().CreateUserData();

            string tsql = string.Empty;

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040039081"))
                tsql = @"SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
                                Operator, Market, ResSerie, MainOffice, 
                                MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
                                ShowAllRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
                                WorkType, Country, Location, AgencyCanDisCom, AgencyType,
                                IDNo, WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,
                                WDoc_TransVoucher,WDoc_ExcVoucher,WDoc_Confirmation,LangID,CanEditOthRes
                        FROM 
                        ( 
                                SELECT  U.Pass, U.ExpDate, Agency=A.Code, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
                                        U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
                                        A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
                                        (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
                                        (
                                            Case When 
                                                        A.MainOffice = '' 
                                                 Then 
                                                        A.Name 
                                                 Else 
                                                        (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
                                        ) AS MainOfficeName, 
                                        A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
                                        U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
                                        AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
                                                             Where Code = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                                  From Agency (NOLOCK)
                                                                  Where Code = RM.Agency)), 0), 
                                        ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                                             Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                                                  Agency = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                                            From Agency (NOLOCK)
                                                                            Where Code = RM.Agency)), 0),
                                        WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
                                        A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, OP.AgencyCanDisCom,
                                        IDNo=U.PIN,
                                        WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
                                        WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
                                        WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
                                        WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
                                        WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
                                        WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
                                        WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
                                        WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
                                        WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
                                        WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
                                        M.LangID,CanEditOthRes
                                From ResMain (NOLOCK) RM
                                Left Join AgencyUser (NOLOCK) U ON U.Code=RM.AgencyUser And U.Agency=RM.Agency
                                Join Agency (NOLOCK) A ON A.Code = RM.Agency
                                Left Join AgencyIP (NOLOCK) I ON I.Agency = RM.Agency 
                                Join Office O (NOLOCK) ON O.Code = A.OprOffice 
                                Join Operator OP (NOLOCK) ON OP.Code = O.Operator
                                Join Market M (NOLOCK) ON M.Code = O.Market                                
                                Left Join ParamPay P (NOLOCK) on O.Market = P.Market
                                WHERE RM.ResNo = @ResNo            
                        ) Q ";
            else
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040009030"))
                    tsql = @"SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
                                Operator, Market, ResSerie, MainOffice, 
                                MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
                                ShowAllRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
                                WorkType, Country, Location, AgencyCanDisCom, AgencyType,
                                IDNo, WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,LangID,CanEditOthRes
                        FROM 
                        ( 
                                SELECT  U.Pass, U.ExpDate, Agency=A.Code, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
                                        U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
                                        A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
                                        (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
                                        (
                                            Case When 
                                                        A.MainOffice = '' 
                                                 Then 
                                                        A.Name 
                                                 Else 
                                                        (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
                                        ) AS MainOfficeName, 
                                        A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
                                        U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
                                        AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
                                                             Where Code = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                                  From Agency (NOLOCK)
                                                                  Where Code = RM.Agency)), 0), 
                                        ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                                             Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                                                  Agency = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                                            From Agency (NOLOCK)
                                                                            Where Code = RM.Agency)), 0),
                                        WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
                                        A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, OP.AgencyCanDisCom,
                                        IDNo=U.PIN,
                                        WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
                                        WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
                                        WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
                                        WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
                                        WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
                                        WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
                                        WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
                                        M.LangID,CanEditOthRes
                                From ResMain (NOLOCK) RM
                                Left Join AgencyUser (NOLOCK) U ON U.Code=RM.AgencyUser And U.Agency=RM.Agency
                                Join Agency (NOLOCK) A ON A.Code = RM.Agency
                                Left Join AgencyIP (NOLOCK) I ON I.Agency = RM.Agency 
                                Join Office O (NOLOCK) ON O.Code = A.OprOffice 
                                Join Operator OP (NOLOCK) ON OP.Code = O.Operator
                                Join Market M (NOLOCK) ON M.Code = O.Market                                
                                Left Join ParamPay P (NOLOCK) on O.Market = P.Market
                                WHERE RM.ResNo = @ResNo            
                        ) Q ";
                else
                    tsql = @"SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
                                Operator, Market, ResSerie, MainOffice, 
                                MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
                                ShowAllRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
                                WorkType, Country, Location, AgencyCanDisCom, AgencyType,
                                IDNo, WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,LangID,CanEditOthRes
                        FROM 
                        ( 
                                SELECT  U.Pass, U.ExpDate, Agency=A.Code, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
                                        U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
                                        A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
                                        (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
                                        (
                                            Case When 
                                                        A.MainOffice = '' 
                                                 Then 
                                                        A.Name 
                                                 Else 
                                                        (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
                                        ) AS MainOfficeName, 
                                        A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
                                        U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
                                        AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
                                                             Where Code = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                                  From Agency (NOLOCK)
                                                                  Where Code = RM.Agency)), 0), 
                                        ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                                             Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                                                  Agency = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                                            From Agency (NOLOCK)
                                                                            Where Code = RM.Agency)), 0),
                                        WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
                                        A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, OP.AgencyCanDisCom,
                                        IDNo=U.PIN,
                                        WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
                                        WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
                                        WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
                                        WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
                                        WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
                                        WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
                                        WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
                                        M.LangID,CanEditOthRes=Cast('0' As bit)
                                From ResMain (NOLOCK) RM
                                Left Join AgencyUser (NOLOCK) U ON U.Code=RM.AgencyUser And U.Agency=RM.Agency
                                Join Agency (NOLOCK) A ON A.Code = RM.Agency
                                Left Join AgencyIP (NOLOCK) I ON I.Agency = RM.Agency 
                                Join Office O (NOLOCK) ON O.Code = A.OprOffice 
                                Join Operator OP (NOLOCK) ON OP.Code = O.Operator
                                Join Market M (NOLOCK) ON M.Code = O.Market                                
                                Left Join ParamPay P (NOLOCK) on O.Market = P.Market
                                WHERE RM.ResNo = @ResNo            
                        ) Q ";

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        DateTime today, expday;
                        today = DateTime.Now;
                        if (oReader["ExpDate"] == null || string.IsNullOrEmpty(oReader["ExpDate"].ToString()))
                            expday = today.AddDays(1);
                        else
                            expday = (DateTime)oReader["ExpDate"];

                        UserData.AgencyName = Conversion.getStrOrNull(oReader["AgencyName"]);
                        UserData.UserName = Conversion.getStrOrNull(oReader["UserName"]);
                        UserData.OprOffice = Conversion.getStrOrNull(oReader["OprOffice"]);
                        UserData.Operator = Conversion.getStrOrNull(oReader["Operator"]);
                        string Market = Conversion.getStrOrNull(oReader["Market"]);
                        UserData.Market = Market;
                        UserData.MainOffice = Conversion.getStrOrNull(oReader["MainOffice"]);
                        UserData.MainOfficeName = Conversion.getStrOrNull(oReader["MainOfficeName"]);
                        UserData.BlackList = Conversion.getStrOrNull(oReader["BlackList"]) == "Y" ? true : false;
                        UserData.AgencyType = Conversion.getInt16OrNull(oReader["AgencyType"]);
                        UserData.SID = HttpContext.Current.Session.SessionID;
                        UserData.Authenticated = "OK";
                        UserData.SaleCur = Conversion.getStrOrNull(oReader["Cur"]);
                        UserData.OwnAgency = (bool)oReader["OwnAgency"];
                        UserData.PayOptTime = (Int16)oReader["PayOptTime"];
                        UserData.IpNumber = "127.0.0.1";
                        UserData.ShowAllRes = Conversion.getStrOrNull(oReader["ShowAllRes"]) == "Y" ? true : false;
                        UserData.MainAgency = false;
                        UserData.MaxRoomCount = (byte)oReader["MaxRoomCount"];
                        UserData.WebVersion = (Convert.ToDecimal(Conversion.getStrOrNull(UserData.TvVersion)) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["WebVersion"]) : string.Empty;
                        UserData.CuratorUser = (Convert.ToDecimal(Conversion.getStrOrNull(UserData.TvVersion)) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["CuratorUser"]) : string.Empty;
                        UserData.MerlinxID = (Convert.ToDecimal(Conversion.getStrOrNull(UserData.TvVersion)) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["MerlinxID"]) : string.Empty;
                        UserData.Aggregator = (Convert.ToDecimal(Conversion.getStrOrNull(UserData.TvVersion)) > Convert.ToDecimal("030001000")) ? (bool)oReader["Aggregator"] : false;
                        UserData.Ci = getCultureInfo(string.Empty);
                        object workType = Conversion.getInt16OrNull(oReader["WorkType"]);
                        UserData.IsB2CAgency = workType != null && Convert.ToInt16(workType.ToString()) == 2 ? true : false;
                        UserData.Country = Conversion.getInt32OrNull(oReader["Country"]);
                        UserData.Location = Conversion.getInt32OrNull(oReader["Location"]);
                        UserData.TvParams = new TvBo.Common().getTvParameters(UserData.Market, Conversion.getDecimalOrNull(UserData.TvVersion), UserData.WebVersion, ref errorMsg);
                        UserData.AgencyCanDisCom = (bool)oReader["AgencyCanDisCom"];

                        UserData.AgencyID = oReader["Agency"].ToString().ToUpper(new CultureInfo("en-US", false));
                        UserData.UserID = oReader["Code"].ToString().ToUpper(new CultureInfo("en-US", false));

                        #region Show Flight
                        object _showFlight = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowFlight");
                        UserData.ShowFlight = (_showFlight != null) ? (bool)_showFlight : false;
                        #endregion

                        #region Payment Control
                        object _paymentControl = new TvBo.Common().getFormConfigValue("General", "PaymentControl");
                        if (_paymentControl != null && (bool)_paymentControl)
                        {
                            if (!(new Agency().getAgencyPasPaymentDefinition(UserData, ref errorMsg)))
                            {
                                errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AgencyPaymentTypesUndefined").ToString();
                                return null;
                            }
                        }
                        #endregion

                        #region Active contrat check
                        if ((bool)oReader["AgreeCheck"])
                        {
                            Int16? ContratCnt = Conversion.getInt16OrNull(oReader["ContratCnt"]);
                            if (ContratCnt.HasValue && ContratCnt.Value < 1)
                            {
                                errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AgencyNotAnyActiveContract").ToString();
                                return null;
                            }
                        }
                        #endregion

                        //UserData = setGridColumnsOptions(UserData);

                        UserData = getAgencyRecord(UserData);

                        #region Phone Mask
                        object _phoneMask = new TvBo.Common().getFormConfigValue("General", "PhoneMask");
                        if (!string.IsNullOrEmpty(Conversion.getStrOrNull(_phoneMask)))
                        {
                            List<PhoneMaskRecord> phoneMaskList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PhoneMaskRecord>>(Conversion.getStrOrNull(_phoneMask));
                            if (phoneMaskList != null && phoneMaskList.Count > 0)
                                UserData.PhoneMask = phoneMaskList.Find(f => f.Market == Market);
                        }
                        #endregion

                        #region Bonus
                        BonusRecord bonusInfo = new Users().getBonusInfoW(UserData.Market, UserData.AgencyID);
                        UserData.Bonus = bonusInfo;
                        #endregion

                        #region WDoc
                        WDocRecord wDoc = UserData.WDoc;
                        wDoc.WDoc_Voucher = (bool)oReader["WDoc_Voucher"];
                        wDoc.WDoc_Ticket = (bool)oReader["WDoc_Ticket"];
                        wDoc.WDoc_Agree = (bool)oReader["WDoc_Agree"];
                        wDoc.WDoc_InvoiceA = (bool)oReader["WDoc_InvoiceA"];
                        wDoc.WDoc_InvoiceP = (bool)oReader["WDoc_InvoiceP"];
                        wDoc.WDoc_Insurance = (bool)oReader["WDoc_Insurance"];
                        wDoc.WDoc_Receipt = (bool)oReader["WDoc_Receipt"];
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040039081"))
                        {
                            wDoc.WDoc_Confirmation = (bool)oReader["WDoc_Confirmation"];
                            wDoc.WDoc_ExcursionVoucher = (bool)oReader["WDoc_ExcVoucher"];
                            wDoc.WDoc_TransportVoucher = (bool)oReader["WDoc_TransVoucher"];
                        }
                        #endregion

                        UserData.MarketLang = Conversion.getInt32OrNull(oReader["LangID"]);
                        UserData.CanEditOthRes = (bool)oReader["CanEditOthRes"];

                        return UserData;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool UserControl(ref User UserData, string IpNumber, ref string errorMsg, string SessionID)
        {
            string tsql = string.Empty;
            if (VersionControl.getTableField("AgencyUser", "ShowAllOtherOffRes"))
            {
                #region Agency User Show Other Offices Reservation
                tsql =
@"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
    Operator, Market, ResSerie, MainOffice, 
    MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
    ShowAllRes,ShowAllOtherOffRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
    WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
    WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt, 
    WDoc_TransVoucher,WDoc_ExcVoucher,WDoc_Confirmation,LangID,CanEditOthRes,CanPayment,Pxm_Use,Nationality
FROM 
( 
    SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
            U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
            A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
            (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
            (
                Case When 
                            A.MainOffice = '' 
                        Then 
                            A.Name 
                        Else 
                            (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
            ) AS MainOfficeName, 
            A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
            U.ShowAllRes,U.ShowAllOtherOffRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
            AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                        Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                        From Agency (NOLOCK)
				                        Where Code=A.Code)), 0), 
            ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                    Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                        Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                            From Agency (NOLOCK)
	                                            Where Code=A.Code)), 0),
            WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
            A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, Country=(Select Top 1 Country From Location (NOLOCK) Where RecID=A.Location), A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
            IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
            WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
            WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
            WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
            WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
            WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
            WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
            WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
            WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
            WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
            WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
            M.LangID,CanEditOthRes,U.CanPayment,A.Pxm_Use,A.Nationality
    FROM AgencyUser (NOLOCK) U 
    Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
    Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
    Join Market M (NOLOCK) ON M.Code = O.Market                                
    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
    WHERE	A.Status='Y'
        And U.Status='Y'
";
                #endregion
            }
            else if (VersionControl.getTableField("Agency", "Nationality"))
                #region Agency Default Nationality
                tsql =
@"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
    Operator, Market, ResSerie, MainOffice, 
    MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
    ShowAllRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
    WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
    WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt, 
    WDoc_TransVoucher,WDoc_ExcVoucher,WDoc_Confirmation,LangID,CanEditOthRes,CanPayment,Pxm_Use,Nationality
FROM 
( 
    SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
            U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
            A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
            (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
            (
                Case When 
                            A.MainOffice = '' 
                        Then 
                            A.Name 
                        Else 
                            (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
            ) AS MainOfficeName, 
            A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
            U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
            AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                        Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                        From Agency (NOLOCK)
				                        Where Code=A.Code)), 0), 
            ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                    Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                        Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                            From Agency (NOLOCK)
	                                            Where Code=A.Code)), 0),
            WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
            A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, Country=(Select Top 1 Country From Location (NOLOCK) Where RecID=A.Location), A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
            IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
            WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
            WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
            WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
            WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
            WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
            WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
            WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
            WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
            WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
            WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
            M.LangID,CanEditOthRes,U.CanPayment,A.Pxm_Use,A.Nationality
    FROM AgencyUser (NOLOCK) U 
    Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
    Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
    Join Market M (NOLOCK) ON M.Code = O.Market                                
    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
    WHERE	A.Status='Y'
        And U.Status='Y'
";
                #endregion
            else
                if (VersionControl.getTableField("AgencyUser", "CanPayment"))
                    #region CanPayment
                    tsql =
    @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
    Operator, Market, ResSerie, MainOffice, 
    MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
    ShowAllRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
    WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
    WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt, 
    WDoc_TransVoucher,WDoc_ExcVoucher,WDoc_Confirmation,LangID,CanEditOthRes,CanPayment,Pxm_Use=Cast(0 as bit),Nationality=null
FROM 
( 
    SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
            U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
            A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
            (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
            (
                Case When 
                            A.MainOffice = '' 
                        Then 
                            A.Name 
                        Else 
                            (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
            ) AS MainOfficeName, 
            A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
            U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
            AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                        Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                        From Agency (NOLOCK)
				                        Where Code=A.Code)), 0), 
            ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                    Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                        Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                            From Agency (NOLOCK)
	                                            Where Code=A.Code)), 0),
            WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
            A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, Country=(Select Top 1 Country From Location (NOLOCK) Where RecID=A.Location), A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
            IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
            WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
            WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
            WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
            WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
            WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
            WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
            WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
            WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
            WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
            WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
            M.LangID,CanEditOthRes,U.CanPayment
    FROM AgencyUser (NOLOCK) U 
    Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
    Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
    Join Market M (NOLOCK) ON M.Code = O.Market                                
    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
    WHERE	U.Status='Y' 
";
                    #endregion
                else
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040039081"))
                        #region Add; WDoc_TransVoucher, WDoc_ExcVoucher, WDoc_Confirmation
                        tsql =
        @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
    Operator, Market, ResSerie, MainOffice, 
    MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
    ShowAllRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
    WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
    WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt, 
    WDoc_TransVoucher, WDoc_ExcVoucher, WDoc_Confirmation, LangID,CanEditOthRes,CanPayment=Cast(0 as bit),Pxm_Use=Cast(0 as bit),Nationality=null
FROM 
( 
    SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
            U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
            A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
            (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
            (
                Case When 
                            A.MainOffice = '' 
                        Then 
                            A.Name 
                        Else 
                            (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
            ) AS MainOfficeName, 
            A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
            U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
            AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                        Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                        From Agency (NOLOCK)
				                        Where Code=A.Code)), 0), 
            ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                    Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                        Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                            From Agency (NOLOCK)
	                                            Where Code=A.Code)), 0),
            WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
            A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, Country=(Select Top 1 Country From Location (NOLOCK) Where RecID=A.Location), A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
            IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
            WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
            WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
            WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
            WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
            WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
            WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
            WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
            WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
            WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
            WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
            M.LangID,CanEditOthRes
    FROM AgencyUser (NOLOCK) U 
    Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
    Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
    Join Market M (NOLOCK) ON M.Code = O.Market                                
    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
    WHERE	U.Status='Y' 
";
                        #endregion
                    else
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040009030"))
                            #region Second script
                            tsql =
            @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
    Operator, Market, ResSerie, MainOffice, 
    MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
    ShowAllRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
    WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
    WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,
    LangID,CanEditOthRes,CanPayment=Cast(0 as bit),Pxm_Use=Cast(0 as bit),Nationality=null
FROM 
( 
    SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
            U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
            A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
            (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
            (
                Case When 
                            A.MainOffice = '' 
                        Then 
                            A.Name 
                        Else 
                            (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
            ) AS MainOfficeName, 
            A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
            U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
            AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                        Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                        From Agency (NOLOCK)
				                        Where Code=A.Code)), 0), 
            ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                    Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                        Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                            From Agency (NOLOCK)
	                                            Where Code=A.Code)), 0),
            WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
            A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, Country=(Select Top 1 Country From Location (NOLOCK) Where RecID=A.Location), A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
            IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
            WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
            WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
            WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
            WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
            WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
            WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
            WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
            M.LangID,CanEditOthRes
    FROM AgencyUser (NOLOCK) U 
    Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
    Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
    Join Market M (NOLOCK) ON M.Code = O.Market                                
    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
    WHERE	U.Status='Y' 
";
                            #endregion
                        else
                            #region First script
                            tsql =
            @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
    Operator, Market, ResSerie, MainOffice, 
    MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
    ShowAllRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
    WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
    WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,
    LangID,CanEditOthRes,CanPayment=Cast(0 as bit),Pxm_Use=Cast(0 as bit),Nationality=null
FROM 
( 
    SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
            U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
            A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
            (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
            (
                Case When 
                            A.MainOffice = '' 
                        Then 
                            A.Name 
                        Else 
                            (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
            ) AS MainOfficeName, 
            A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
            U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
            AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                        Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                        From Agency (NOLOCK)
				                        Where Code=A.Code)), 0), 
            ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                    Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                        Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                            From Agency (NOLOCK)
	                                            Where Code=A.Code)), 0),
            WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
            A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, Country=(Select Top 1 Country From Location (NOLOCK) Where RecID=A.Location), A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
            IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
            WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
            WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
            WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
            WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
            WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
            WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
            WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
            M.LangID,CanEditOthRes=Cast('0' AS bit)
    FROM AgencyUser (NOLOCK) U 
    Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
    Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
    Join Market M (NOLOCK) ON M.Code = O.Market                                
    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
    WHERE U.Status='Y' 
";
                            #endregion

            if (string.Equals(UserData.CustomRegID, Common.crID_AtlasGlobal))
            {
                tsql +=
@"
      And U.LoginId=@LoginId
) Q 
";
            }
            else
            {
                if (string.IsNullOrEmpty(UserData.AgencyID) && !string.IsNullOrEmpty(UserData.EMail))
                    tsql +=
    @"
      And U.EMail=@EMail                                    
) Q 
";
                else
                    tsql +=
    @"
      And U.Agency=@Agency 
      And U.Code=@Code                                     
) Q 
";
            }
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                if (string.Equals(UserData.CustomRegID, Common.crID_AtlasGlobal))
                {
                    db.AddInParameter(dbCommand, "LoginId", DbType.AnsiString, UserData.LoginId);
                }
                else
                    if (string.IsNullOrEmpty(UserData.AgencyID) && !string.IsNullOrEmpty(UserData.EMail))
                        db.AddInParameter(dbCommand, "EMail", DbType.AnsiString, UserData.EMail);
                    else
                    {
                        db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                        db.AddInParameter(dbCommand, "Code", DbType.AnsiString, UserData.UserID);
                    }
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    var fieldNames = Enumerable.Range(0, oReader.FieldCount).Select(i => oReader.GetName(i)).ToArray();
                    if (oReader.Read())
                    {
                        if (!string.IsNullOrEmpty(UserData.Password) && (UserData.Password == Conversion.getStrOrNull(oReader["Pass"])))
                        {
                            DateTime today, expday;
                            today = DateTime.Now;
                            DateTime? ExpiryDate = Conversion.getDateTimeOrNull(oReader["ExpDate"]);
                            expday = ExpiryDate.HasValue ? ExpiryDate.Value : today.AddDays(1);
                            if (DateTime.Compare(today, expday) <= 0)
                            {
                                UserData.AgencyID = Conversion.getStrOrNull(oReader["Agency"]);
                                UserData.UserID = Conversion.getStrOrNull(oReader["Code"]);
                                UserData.AgencyName = Conversion.getStrOrNull(oReader["AgencyName"]);
                                UserData.AgencyNameL = UserData.AgencyName;
                                UserData.UserName = Conversion.getStrOrNull(oReader["UserName"]);
                                UserData.UserNameL = UserData.UserName;
                                UserData.OprOffice = Conversion.getStrOrNull(oReader["OprOffice"]);
                                UserData.Operator = Conversion.getStrOrNull(oReader["Operator"]);
                                string Market = Conversion.getStrOrNull(oReader["Market"]);
                                UserData.Market = Market;
                                UserData.MainOffice = Conversion.getStrOrNull(oReader["MainOffice"]);
                                UserData.MainOfficeName = Conversion.getStrOrNull(oReader["MainOfficeName"]);
                                UserData.BlackList = Equals(oReader["BlackList"], "Y");
                                UserData.AgencyType = Conversion.getInt16OrNull(oReader["AgencyType"]);
                                UserData.SID = SessionID;
                                UserData.Authenticated = "OK";
                                UserData.SaleCur = Conversion.getStrOrNull(oReader["Cur"]);
                                bool? ownAgency = Conversion.getBoolOrNull(oReader["OwnAgency"]);
                                UserData.OwnAgency = ownAgency.HasValue ? ownAgency.Value : false;
                                UserData.PayOptTime = (Int16)oReader["PayOptTime"];
                                UserData.IpNumber = IpNumber;
                                UserData.ShowAllRes = Equals(oReader["ShowAllRes"], "Y");
                                UserData.MainAgency = false;
                                byte? maxRoomCount = Conversion.getByteOrNull(oReader["MaxRoomCount"]);
                                UserData.MaxRoomCount = maxRoomCount.HasValue ? maxRoomCount.Value : Convert.ToByte(1);
                                UserData.WebVersion = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["WebVersion"]) : string.Empty;
                                UserData.CuratorUser = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["CuratorUser"]) : string.Empty;
                                UserData.MerlinxID = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["MerlinxID"]) : string.Empty;
                                UserData.Aggregator = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030001000")) ? (bool)oReader["Aggregator"] : false;
                                UserData.Ci = getCultureInfo(string.Empty);
                                Int16? workType = Conversion.getInt16OrNull(oReader["WorkType"]);
                                UserData.IsB2CAgency = (workType.HasValue && workType.Value == 2) ? true : false;
                                UserData.Country = Conversion.getInt32OrNull(oReader["Country"]);
                                UserData.Location = Conversion.getInt32OrNull(oReader["Location"]);
                                UserData.TvParams = new TvBo.Common().getTvParameters(UserData.Market, Conversion.getDecimalOrNull(UserData.TvVersion), UserData.WebVersion, ref errorMsg);
                                UserData.AgencyCanDisCom = (bool)oReader["AgencyCanDisCom"];
                                if (fieldNames.Contains("ShowAllOtherOffRes"))
                                {
                                    string ss = oReader["ShowAllOtherOffRes"].ToString();
                                    UserData.ShowAllOtherOffRes = ss.Equals("True") ? true : false;
                                }


                                #region Show Flight
                                object _showFlight = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowFlight");
                                UserData.ShowFlight = (_showFlight != null) ? (bool)_showFlight : false;
                                #endregion

                                #region Payment Control
                                object _paymentControl = new TvBo.Common().getFormConfigValue("General", "PaymentControl");
                                if (_paymentControl != null && (bool)_paymentControl)
                                {
                                    if (!(new Agency().getAgencyPasPaymentDefinition(UserData, ref errorMsg)))
                                    {
                                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AgencyPaymentTypesUndefined").ToString();
                                        return false;
                                    }
                                }
                                #endregion

                                #region Active contrat check
                                if ((bool)oReader["AgreeCheck"])
                                {
                                    Int16? ContratCnt = Conversion.getInt16OrNull(oReader["ContratCnt"]);
                                    if (ContratCnt.HasValue && ContratCnt.Value < 1)
                                    {
                                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AgencyNotAnyActiveContract").ToString();
                                        return false;
                                    }
                                }
                                #endregion

                                //UserData = setGridColumnsOptions(UserData);

                                UserData = getAgencyRecord(UserData);

                                #region Phone Mask
                                object _phoneMask = new TvBo.Common().getFormConfigValue("General", "PhoneMask");
                                if (!string.IsNullOrEmpty(Conversion.getStrOrNull(_phoneMask)))
                                {
                                    List<PhoneMaskRecord> phoneMaskList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PhoneMaskRecord>>(Conversion.getStrOrNull(_phoneMask));
                                    if (phoneMaskList != null && phoneMaskList.Count > 0)
                                        UserData.PhoneMask = phoneMaskList.Find(f => f.Market == Market);
                                }
                                #endregion

                                #region Bonus
                                BonusRecord bonusInfo = new Users().getBonusInfoW(UserData.Market, UserData.AgencyID);
                                UserData.Bonus = bonusInfo;
                                #endregion

                                #region WDoc
                                WDocRecord wDoc = UserData.WDoc;
                                wDoc.WDoc_Voucher = (bool)oReader["WDoc_Voucher"];
                                wDoc.WDoc_Ticket = (bool)oReader["WDoc_Ticket"];
                                wDoc.WDoc_Agree = (bool)oReader["WDoc_Agree"];
                                wDoc.WDoc_InvoiceA = (bool)oReader["WDoc_InvoiceA"];
                                wDoc.WDoc_InvoiceP = (bool)oReader["WDoc_InvoiceP"];
                                wDoc.WDoc_Insurance = (bool)oReader["WDoc_Insurance"];
                                wDoc.WDoc_Receipt = (bool)oReader["WDoc_Receipt"];
                                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040039081"))
                                {
                                    wDoc.WDoc_Confirmation = (bool)oReader["WDoc_Confirmation"];
                                    wDoc.WDoc_ExcursionVoucher = (bool)oReader["WDoc_ExcVoucher"];
                                    wDoc.WDoc_TransportVoucher = (bool)oReader["WDoc_TransVoucher"];
                                }
                                #endregion

                                UserData.PIN = Conversion.getStrOrNull(oReader["IDNo"]);
                                UserData.Phone = Conversion.getStrOrNull(oReader["Phone"]);
                                UserData.Mobile = Conversion.getStrOrNull(oReader["Mobile"]);
                                UserData.EMail = Conversion.getStrOrNull(oReader["EMail"]);
                                UserData.HAddress = Conversion.getStrOrNull(oReader["HAddress"]);
                                UserData.Bank = Conversion.getStrOrNull(oReader["Bank"]);
                                UserData.BankNo = Conversion.getStrOrNull(oReader["BankNo"]);
                                UserData.BankAccNo = Conversion.getStrOrNull(oReader["BankAccNo"]);

                                UserData.AgencyID = UserData.AgencyID.ToUpper(new CultureInfo("en-US", false));
                                UserData.UserID = UserData.UserID.ToUpper(new CultureInfo("en-US", false));

                                UserData.AgencyRating = new UICommon().getAgencyRating(UserData.Market, UserData.AgencyID, UserData.WebVersion, ref errorMsg);
                                UserData.MarketLang = Conversion.getInt32OrNull(oReader["LangID"]);
                                bool? canEditOthRes = Conversion.getBoolOrNull(oReader["CanEditOthRes"]);
                                UserData.CanEditOthRes = canEditOthRes.HasValue ? canEditOthRes.Value : false;
                                UserData.CanPayment = Conversion.getBoolOrNull(oReader["CanPayment"]);
                                UserData.Pxm_Use = (bool)oReader["Pxm_Use"];
                                #region Get Paximum setting
                                PaximumRecord paximumSetting = new TvSystem().getPaximumConfig(UserData, ref errorMsg);
                                if (paximumSetting.Pxm_Use.HasValue && paximumSetting.Pxm_Use.Value && !string.IsNullOrEmpty(paximumSetting.Pxm_AutMainCode))
                                {
                                    UserData.PaxSetting = new PaximumRecord();
                                    UserData.PaxSetting = paximumSetting;
                                }
                                else
                                {
                                    UserData.PaxSetting = null;
                                }
                                #endregion

                                UserData.PaxToken = getPaximumToken(UserData, ref errorMsg);
                                UserData.Nationality = Conversion.getStrOrNull(oReader["Nationality"]);

                                new Users().setLoginUser(UserData, ref errorMsg);

                                return true;
                            }
                            else return false;
                        }
                        else
                        {
                            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                                errorMsg = "Пароль введен неверно, пожалуйста, попробуйте еще раз";
                            return false;
                        }
                    }
                    else
                    {
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                            errorMsg = "Код агентства или логин введен неверно, пожалуйста, попробуйте еще раз";
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool getUser(ref User UserData, string IpNumber, string SessionID, ref string errorMsg)
        {
            bool webService = false;
            if (UserData.CustomRegID == TvBo.Common.crID_Calsedon)
                webService = true;

            string tsql = string.Empty;
            if (VersionControl.getTableField("AgencyUser", "ShowAllOtherOffRes"))
                #region ShowAllResOtherOff
                tsql =
                    @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
    Operator, Market, ResSerie, MainOffice, 
    MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
    ShowAllRes,ShowAllOtherOffRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
    WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
    WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,
    WDoc_TransVoucher,WDoc_ExcVoucher,WDoc_Confirmation,
    LangID,CanEditOthRes,CanPayment,Pxm_Use,Nationality,LoginId
FROM 
( 
    SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
            U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
            A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
            (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
            (
                Case When 
                            A.MainOffice = '' 
                        Then 
                            A.Name 
                        Else 
                            (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
            ) AS MainOfficeName, 
            A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
            U.ShowAllRes,U.ShowAllOtherOffRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
            AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                        Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                        From Agency (NOLOCK)
				                        Where Code=A.Code)), 0), 
            ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                    Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                        Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                            From Agency (NOLOCK)
	                                            Where Code=A.Code)), 0),
            WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
            A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
            IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
            WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
            WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
            WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
            WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
            WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
            WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
            WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
            WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
            WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
            WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
            M.LangID,CanEditOthRes,U.CanPayment,A.Pxm_Use,A.Nationality,U.LoginId
    FROM AgencyUser (NOLOCK) U 
    Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
    Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
    Join Market M (NOLOCK) ON M.Code = O.Market                                
    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
    WHERE	A.Status='Y'
        And U.Status='Y' 
        And U.Agency=@Agency 
        And U.Code=@Code                                     
) Q 
";
                #endregion

            else
                if (VersionControl.getTableField("Agency", "Nationality"))
                    #region Agency Default Nationality
                    tsql =
    @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
    Operator, Market, ResSerie, MainOffice, 
    MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
    ShowAllRes,0 as ShowAllOtherOffRes,MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
    WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
    WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,
    WDoc_TransVoucher,WDoc_ExcVoucher,WDoc_Confirmation,
    LangID,CanEditOthRes,CanPayment,Pxm_Use,Nationality,LoginId
FROM 
( 
    SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
            U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
            A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
            (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
            (
                Case When 
                            A.MainOffice = '' 
                        Then 
                            A.Name 
                        Else 
                            (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
            ) AS MainOfficeName, 
            A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
            U.ShowAllRes,MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
            AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                        Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                        From Agency (NOLOCK)
				                        Where Code=A.Code)), 0), 
            ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                    Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                        Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                            From Agency (NOLOCK)
	                                            Where Code=A.Code)), 0),
            WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
            A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
            IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
            WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
            WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
            WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
            WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
            WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
            WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
            WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
            WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
            WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
            WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
            M.LangID,CanEditOthRes,U.CanPayment,A.Pxm_Use,A.Nationality,U.LoginId
    FROM AgencyUser (NOLOCK) U 
    Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
    Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
    Join Market M (NOLOCK) ON M.Code = O.Market                                
    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
    WHERE	A.Status='Y'
        And U.Status='Y' 
        And U.Agency=@Agency 
        And U.Code=@Code                                     
) Q 
";
                    #endregion
                else
                    if (VersionControl.getTableField("AgencyUser", "CanPayment"))
                    {
                        #region Agency User Can Payment
                        tsql =
        @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
    Operator, Market, ResSerie, MainOffice, 
    MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
    ShowAllRes,0 as ShowAllOtherOffRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
    WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
    WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,
    WDoc_TransVoucher,WDoc_ExcVoucher,WDoc_Confirmation,
    LangID,CanEditOthRes,CanPayment,Pxm_Use=Cast(0 as bit),Nationality=null,LoginId
FROM 
( 
    SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
            U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
            A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
            (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
            (
                Case When 
                            A.MainOffice = '' 
                        Then 
                            A.Name 
                        Else 
                            (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
            ) AS MainOfficeName, 
            A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
            U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
            AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                        Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                        From Agency (NOLOCK)
				                        Where Code=A.Code)), 0), 
            ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                    Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                        Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                            From Agency (NOLOCK)
	                                            Where Code=A.Code)), 0),
            WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
            A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
            IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
            WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
            WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
            WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
            WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
            WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
            WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
            WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
            WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
            WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
            WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
            M.LangID,CanEditOthRes,U.CanPayment,U.LoginId
    FROM AgencyUser (NOLOCK) U 
    Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
    Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
    Join Market M (NOLOCK) ON M.Code = O.Market                                
    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
    WHERE	A.Status='Y'
        And U.Status='Y' 
        And U.Agency=@Agency 
        And U.Code=@Code                                     
) Q 
";
                        #endregion
                    }
                    else
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040039081"))
                            #region Version > 04.00.39.081
                            tsql =
            @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
        Operator, Market, ResSerie, MainOffice, 
        MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
        ShowAllRes, 0 as ShowAllOtherOffRes,MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
        WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
        WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,
        WDoc_TransVoucher,WDoc_ExcVoucher,WDoc_Confirmation,
        LangID,CanEditOthRes,CanPayment=Cast(0 as bit),Pxm_Use=Cast(0 as bit),Nationality=null,LoginId
FROM 
( 
        SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
                U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
                A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
                (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
                (
                    Case When 
                                A.MainOffice = '' 
                            Then 
                                A.Name 
                            Else 
                                (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
                ) AS MainOfficeName, 
                A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
                U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
                AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                            Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                            From Agency (NOLOCK)
				                            Where Code=A.Code)), 0), 
                ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                        Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                            Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                                From Agency (NOLOCK)
	                                                Where Code=A.Code)), 0),
                WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
                A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
                IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
                WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
                WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
                WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
                WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
                WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
                WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
                WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
                WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
                WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
                WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
                M.LangID,CanEditOthRes,U.LoginId
        FROM AgencyUser (NOLOCK) U 
        Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
        Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
        Join Office O (NOLOCK) ON O.Code = A.OprOffice 
        Join Operator OP (NOLOCK) ON OP.Code = O.Operator
        Join Market M (NOLOCK) ON M.Code = O.Market                                
        Left Join ParamPay P (NOLOCK) on O.Market = P.Market
        WHERE	A.Status='Y'
            And U.Status='Y' 
            And U.Agency=@Agency 
            And U.Code=@Code                                     
) Q 
";
                            #endregion
                        else
                            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040009030"))
                                #region Version > 04.00.09.030
                                tsql =
                @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
        Operator, Market, ResSerie, MainOffice, 
        MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
        ShowAllRes,0 as ShowAllOtherOffRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
        WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
        WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,
        LangID,CanEditOthRes,CanPayment=Cast(0 as bit),Pxm_Use=Cast(0 as bit),Nationality=null,LoginId
FROM 
( 
        SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
                U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
                A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
                (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
                (
                    Case When 
                                A.MainOffice = '' 
                            Then 
                                A.Name 
                            Else 
                                (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
                ) AS MainOfficeName, 
                A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
                U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),
                AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                            Where Code=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                            From Agency (NOLOCK)
				                            Where Code=A.Code)), 0), 
                ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                        Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                            Agency=(Select Case When AgencyType=2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                                From Agency (NOLOCK)
	                                                Where Code=A.Code)), 0),
                WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
                A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
                IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
                WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
                WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
                WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
                WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
                WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
                WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
                WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
                M.LangID,CanEditOthRes,U.LoginId
        FROM AgencyUser (NOLOCK) U 
        Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
        Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
        Join Office O (NOLOCK) ON O.Code = A.OprOffice 
        Join Operator OP (NOLOCK) ON OP.Code = O.Operator
        Join Market M (NOLOCK) ON M.Code = O.Market                                
        Left Join ParamPay P (NOLOCK) on O.Market = P.Market
        WHERE	A.Status='Y'
            And U.Status='Y' 
            And U.Agency=@Agency 
            And U.Code=@Code                                     
) Q
";
                                #endregion
                            else
                                #region First Query
                                tsql =
                @"
SELECT Pass, ExpDate, Agency, AgencyName, Code, UserName, IpNo, OprOffice, 
        Operator, Market, ResSerie, MainOffice, 
        MainOfficeName, BlackList, Cur, OwnAgency, PayOptTime,                                 
        ShowAllRes,0 as ShowAllOtherOffRes, MaxRoomCount, AgreeCheck, ContratCnt, WebVersion, CuratorUser, MerlinxID, Aggregator, 
        WorkType, Country, Location, AgencyCanDisCom, AgencyType, IDNo, Phone, Mobile, EMail, HAddress, Bank, BankNo, BankAccNo,
        WDoc_Voucher, WDoc_Ticket, WDoc_Agree, WDoc_InvoiceA, WDoc_InvoiceP, WDoc_Insurance, WDoc_Receipt,LangID,CanEditOthRes,
        CanPayment=Cast(0 as bit),Pxm_Use=Cast(0 as bit),Nationality=null,LoginId
FROM 
( 
        SELECT  U.Pass, U.ExpDate, U.Agency, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AgencyName, 
                U.Code, isnull(dbo.FindLocalName(U.NameLID, M.Code), U.Name) UserName, isnull(I.IpNo, '127.0.0.1') IpNo, 
                A.OprOffice, O.Operator, O.Market, isnull(OP.ResSerie, '') ResSerie, 
                (Case When A.MainOffice = '' Then U.Agency Else A.MainOffice End) MainOffice, 
                (
                    Case When 
                                A.MainOffice = '' 
                            Then 
                                A.Name 
                            Else 
                                (Select Name From Agency AA (NOLOCK) Where AA.Code = A.MainOffice) END
                ) AS MainOfficeName, 
                A.AgencyType, A.BlackList, M.Cur, isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime,
                U.ShowAllRes, MaxRoomCount=(Select pr_MaxRoomCnt From dbo.ufn_GetMarketParams (M.Code,'PR')),                
                AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
			                            Where Code = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
				                            From Agency (NOLOCK)
				                            Where Code = @Code)), 0), 
                ContratCnt = isnull((Select Count(RecID) From AgencyAgree 
                                        Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                            Agency = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
	                                                From Agency (NOLOCK)
	                                                Where Code = @Code)), 0),
                WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
                A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, AgencyCanDisCom=isnull(OP.AgencyCanDisCom, 0),
                IDNo=U.PIN, U.Phone, U.Mobile, U.EMail, U.HAddress, U.Bank, U.BankNo, U.BankAccNo,
                WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
                WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
                WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
                WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
                WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
                WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
                WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
                M.LangID,CanEditOthRes=Cast('0' as bit),
                U.LoginId=''
        FROM AgencyUser (NOLOCK) U 
        Join Agency (NOLOCK) A ON A.Code = U.Agency And A.Status = 'Y'
        Left Join AgencyIP (NOLOCK) I ON I.Agency = U.Agency 
        Join Office O (NOLOCK) ON O.Code = A.OprOffice 
        Join Operator OP (NOLOCK) ON OP.Code = O.Operator
        Join Market M (NOLOCK) ON M.Code = O.Market                                
        Left Join ParamPay P (NOLOCK) on O.Market = P.Market
        WHERE	U.Status='Y' 
            And U.Agency = @Agency 
            And U.Code= @Code                                     
) Q
";
                                #endregion
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Code", DbType.String, UserData.UserID);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    var fieldNames = Enumerable.Range(0, oReader.FieldCount).Select(i => oReader.GetName(i)).ToArray();
                    if (oReader.Read())
                    {
                        DateTime today, expday;
                        today = DateTime.Now;
                        DateTime? ExpiryDate = Conversion.getDateTimeOrNull(oReader["ExpDate"]);
                        expday = ExpiryDate.HasValue ? ExpiryDate.Value : today.AddDays(1);
                        if (DateTime.Compare(today, expday) <= 0)
                        {
                            UserData.AgencyID = Conversion.getStrOrNull(oReader["Agency"]);
                            UserData.UserID = Conversion.getStrOrNull(oReader["Code"]);
                            UserData.Password = Conversion.getStrOrNull(oReader["Pass"]);
                            UserData.AgencyName = Conversion.getStrOrNull(oReader["AgencyName"]);
                            UserData.UserName = Conversion.getStrOrNull(oReader["UserName"]);
                            UserData.OprOffice = Conversion.getStrOrNull(oReader["OprOffice"]);
                            UserData.Operator = Conversion.getStrOrNull(oReader["Operator"]);
                            string Market = Conversion.getStrOrNull(oReader["Market"]);
                            UserData.Market = Market;
                            UserData.MainOffice = Conversion.getStrOrNull(oReader["MainOffice"]);
                            UserData.MainOfficeName = Conversion.getStrOrNull(oReader["MainOfficeName"]);
                            UserData.BlackList = Equals(oReader["BlackList"], "Y");
                            UserData.AgencyType = Conversion.getInt16OrNull(oReader["AgencyType"]);
                            UserData.SID = SessionID;
                            UserData.Authenticated = "OK";
                            UserData.SaleCur = Conversion.getStrOrNull(oReader["Cur"]);
                            bool? ownAgency = Conversion.getBoolOrNull(oReader["OwnAgency"]);
                            UserData.OwnAgency = ownAgency.HasValue ? ownAgency.Value : false;
                            UserData.PayOptTime = (Int16)oReader["PayOptTime"];
                            UserData.IpNumber = IpNumber;
                            UserData.ShowAllRes = Equals(oReader["ShowAllRes"], "Y");
                            if (fieldNames.Contains("ShowAllOtherOffRes"))
                                UserData.ShowAllOtherOffRes = oReader["ShowAllOtherOffRes"].ToString() == "1" ? true : false;

                            UserData.MainAgency = false;
                            byte? maxRoomCount = Conversion.getByteOrNull(oReader["MaxRoomCount"]);
                            UserData.MaxRoomCount = maxRoomCount.HasValue ? maxRoomCount.Value : Convert.ToByte(1);
                            UserData.WebVersion = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["WebVersion"]) : string.Empty;
                            UserData.CuratorUser = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["CuratorUser"]) : string.Empty;
                            UserData.MerlinxID = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["MerlinxID"]) : string.Empty;
                            UserData.Aggregator = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030001000")) ? (bool)oReader["Aggregator"] : false;
                            UserData.Ci = getCultureInfo(webService ? "en-US" : string.Empty);
                            Int16? workType = Conversion.getInt16OrNull(oReader["WorkType"]);
                            UserData.IsB2CAgency = (workType.HasValue && workType.Value == 2) ? true : false;
                            UserData.Country = Conversion.getInt32OrNull(oReader["Country"]);
                            UserData.Location = Conversion.getInt32OrNull(oReader["Location"]);
                            UserData.TvParams = new TvBo.Common().getTvParameters(UserData.Market, Conversion.getDecimalOrNull(UserData.TvVersion), UserData.WebVersion, ref errorMsg);
                            UserData.AgencyCanDisCom = (bool)oReader["AgencyCanDisCom"];

                            #region Show Flight
                            if (!webService)
                            {
                                object _showFlight = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowFlight");
                                UserData.ShowFlight = (_showFlight != null) ? (bool)_showFlight : false;
                            }
                            else UserData.ShowFlight = false;

                            #endregion

                            #region Payment Control
                            if (!webService)
                            {
                                object _paymentControl = new TvBo.Common().getFormConfigValue("General", "PaymentControl");
                                if (_paymentControl != null && (bool)_paymentControl)
                                {
                                    if (!(new Agency().getAgencyPasPaymentDefinition(UserData, ref errorMsg)))
                                    {
                                        errorMsg = webService ? "Agency payment type undefined" : HttpContext.GetGlobalResourceObject("LibraryResource", "AgencyPaymentTypesUndefined").ToString();
                                        return false;
                                    }
                                }
                            }
                            #endregion

                            #region Active contrat check
                            if ((bool)oReader["AgreeCheck"])
                            {
                                Int16? ContratCnt = Conversion.getInt16OrNull(oReader["ContratCnt"]);
                                if (ContratCnt.HasValue && ContratCnt.Value < 1)
                                {
                                    errorMsg = webService ? "Agency not any active contract" : HttpContext.GetGlobalResourceObject("LibraryResource", "AgencyNotAnyActiveContract").ToString();
                                    return false;
                                }
                            }
                            #endregion

                            //UserData = setGridColumnsOptions(UserData);

                            UserData = getAgencyRecord(UserData);

                            #region Phone Mask
                            if (!webService)
                            {
                                object _phoneMask = new TvBo.Common().getFormConfigValue("General", "PhoneMask");
                                if (!string.IsNullOrEmpty(Conversion.getStrOrNull(_phoneMask)))
                                {
                                    List<PhoneMaskRecord> phoneMaskList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PhoneMaskRecord>>(Conversion.getStrOrNull(_phoneMask));
                                    if (phoneMaskList != null && phoneMaskList.Count > 0)
                                        UserData.PhoneMask = phoneMaskList.Find(f => f.Market == Market);
                                }
                            }
                            else
                                UserData.PhoneMask = new PhoneMaskRecord();

                            #endregion

                            #region Bonus
                            BonusRecord bonusInfo = new Users().getBonusInfoW(UserData.Market, UserData.AgencyID);
                            UserData.Bonus = bonusInfo;
                            #endregion

                            #region WDoc
                            WDocRecord wDoc = UserData.WDoc;
                            wDoc.WDoc_Voucher = (bool)oReader["WDoc_Voucher"];
                            wDoc.WDoc_Ticket = (bool)oReader["WDoc_Ticket"];
                            wDoc.WDoc_Agree = (bool)oReader["WDoc_Agree"];
                            wDoc.WDoc_InvoiceA = (bool)oReader["WDoc_InvoiceA"];
                            wDoc.WDoc_InvoiceP = (bool)oReader["WDoc_InvoiceP"];
                            wDoc.WDoc_Insurance = (bool)oReader["WDoc_Insurance"];
                            wDoc.WDoc_Receipt = (bool)oReader["WDoc_Receipt"];
                            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040039081"))
                            {
                                wDoc.WDoc_Confirmation = (bool)oReader["WDoc_Confirmation"];
                                wDoc.WDoc_ExcursionVoucher = (bool)oReader["WDoc_ExcVoucher"];
                                wDoc.WDoc_TransportVoucher = (bool)oReader["WDoc_TransVoucher"];
                            }
                            #endregion

                            UserData.PIN = Conversion.getStrOrNull(oReader["IDNo"]);
                            UserData.Phone = Conversion.getStrOrNull(oReader["Phone"]);
                            UserData.Mobile = Conversion.getStrOrNull(oReader["Mobile"]);
                            UserData.EMail = Conversion.getStrOrNull(oReader["EMail"]);
                            UserData.HAddress = Conversion.getStrOrNull(oReader["HAddress"]);
                            UserData.Bank = Conversion.getStrOrNull(oReader["Bank"]);
                            UserData.BankNo = Conversion.getStrOrNull(oReader["BankNo"]);
                            UserData.BankAccNo = Conversion.getStrOrNull(oReader["BankAccNo"]);

                            UserData.AgencyID = UserData.AgencyID.ToUpper(new CultureInfo("en-US", false));
                            UserData.UserID = UserData.UserID.ToUpper(new CultureInfo("en-US", false));
                            UserData.LoginId = Conversion.getStrOrNull(oReader["LoginId"]);

                            UserData.AgencyRating = new UICommon().getAgencyRating(UserData.Market, UserData.AgencyID, UserData.WebVersion, ref errorMsg);
                            UserData.MarketLang = Conversion.getInt32OrNull(oReader["LangID"]);
                            UserData.CanEditOthRes = (bool)oReader["CanEditOthRes"];
                            UserData.CanPayment = (bool)oReader["CanPayment"];
                            UserData.Pxm_Use = (bool)oReader["Pxm_Use"];
                            UserData.Nationality = Conversion.getStrOrNull(oReader["Nationality"]);

                            new Users().setLoginUser(UserData, ref errorMsg);

                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool AgencyControl(ref User UserData, string IpNumber, ref string errorMsg, string SessionID,bool checkAgencyActive)
        {
            string tsql = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040039081"))
                tsql = @"Select A.MasterPass AS Pass, A.Code, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AS AgencyName, A.OprOffice, 
                        A.MainOffice, MainOfficeName='', A.Status, IP.IpNo, O.Operator, O.Market, A.AgencyType, A.BlackList, M.Cur, 
                        isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime, 
                        MaxRoomCount=Cast('0' AS tinyint),
                        AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
                                                     Where Code = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                          From Agency (NOLOCK)
                                                          Where Code = @Agency)), Cast(0 as bit)), 
                        ContratCnt = isnull((Select Count(*) From AgencyAgree (NOLOCK)
                                            Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                              Agency = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                        From Agency 
                                                        Where Code = @Agency)), 0),
                        WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
                        A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, OP.AgencyCanDisCom,
                        IDNo='', Phone='', Mobile='', EMail='', HAddress='', Bank='', BankNo='', BankAccNo='',
                        WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
                        WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
                        WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
                        WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
                        WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
                        WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
                        WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
                        WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
                        WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
                        WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
                        M.LangID
                    From Agency A (NOLOCK) 
                    Left Join AgencyIP IP (NOLOCK) ON IP.Agency = A.Code 
                    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
                    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
                    Join Market M (NOLOCK)  ON M.Code = O.Market
                    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
                    Where 
                        (A.Status = 'Y' or @CheckAgencyActive=0)
                        AND (IP.IpNo is null or IP.IpNo = @IpNo) 
                        AND A.Code = @Agency ";

            else
                tsql = @"Select A.MasterPass AS Pass, A.Code, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AS AgencyName, A.OprOffice, 
                        A.MainOffice, MainOfficeName='', A.Status, IP.IpNo, O.Operator, O.Market, A.AgencyType, A.BlackList, M.Cur, 
                        isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime, 
                        MaxRoomCount=Cast('0' AS tinyint),
                        AgreeCheck = isnull((Select AgreeCheck From Agency (NOLOCK) 
                                                     Where Code = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                          From Agency (NOLOCK)
                                                          Where Code = @Agency)), Cast(0 as bit)), 
                        ContratCnt = isnull((Select Count(*) From AgencyAgree (NOLOCK)
                                            Where dbo.DateOnly(GetDate()) between BegDate and EndDate and isnull(Status,'N')= 'Y' and 
                                              Agency = (Select Case When AgencyType = 2 Then code Else dbo.IsEmpty(MainOffice, Code) End 
                                                        From Agency 
                                                        Where Code = @Agency)), 0),
                        WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
                        A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, OP.AgencyCanDisCom,
                        IDNo='', Phone='', Mobile='', EMail='', HAddress='', Bank='', BankNo='', BankAccNo='',
                        WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
                        WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
                        WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
                        WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
                        WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
                        WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
                        WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
                        M.LangID
                    From Agency A (NOLOCK) 
                    Left Join AgencyIP IP (NOLOCK) ON IP.Agency = A.Code 
                    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
                    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
                    Join Market M (NOLOCK)  ON M.Code = O.Market
                    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
                    Where 
                        (A.Status = 'Y' or @CheckAgencyActive=0)
                        AND (IP.IpNo is null or IP.IpNo = @IpNo) 
                        AND A.Code = @Agency ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "CheckAgencyActive", DbType.Boolean, checkAgencyActive);
                db.AddInParameter(dbCommand, "IpNo", DbType.String, IpNumber);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        if (!string.IsNullOrEmpty(UserData.Password) && (UserData.Password == (string)oReader["Pass"]))
                        {

                            UserData.AgencyName = (string)oReader["AgencyName"];
                            UserData.UserName = string.Empty;
                            UserData.OprOffice = (string)oReader["OprOffice"];
                            UserData.Operator = (string)oReader["Operator"];
                            UserData.Market = (string)oReader["Market"];
                            UserData.MainOffice = (string)oReader["MainOffice"];
                            UserData.MainOfficeName = (string)oReader["MainOfficeName"];
                            UserData.BlackList = (string)oReader["BlackList"] == "Y" ? true : false;
                            UserData.AgencyType = Conversion.getInt16OrNull(oReader["AgencyType"]);
                            UserData.SID = SessionID;
                            UserData.Authenticated = "OK";
                            UserData.SaleCur = (string)oReader["Cur"];
                            UserData.OwnAgency = (bool)oReader["OwnAgency"];
                            UserData.PayOptTime = (Int16)oReader["PayOptTime"];
                            UserData.IpNumber = IpNumber;
                            UserData.ShowAllRes = true;
                            UserData.ShowAllOtherOffRes = true;
                            UserData.MainAgency = true;
                            UserData.MaxRoomCount = (byte)oReader["MaxRoomCount"];
                            UserData.WebVersion = (Convert.ToDecimal((string)UserData.TvVersion) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["WebVersion"]) : string.Empty;
                            UserData.CuratorUser = (Convert.ToDecimal((string)UserData.TvVersion) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["CuratorUser"]) : string.Empty;
                            UserData.MerlinxID = (Convert.ToDecimal((string)UserData.TvVersion) > Convert.ToDecimal("030001000")) ? Conversion.getStrOrNull(oReader["MerlinxID"]) : string.Empty;
                            UserData.Aggregator = (Convert.ToDecimal((string)UserData.TvVersion) > Convert.ToDecimal("030001000")) ? (bool)oReader["Aggregator"] : false;
                            UserData.Ci = getCultureInfo(string.Empty);
                            object workType = Conversion.getInt16OrNull(oReader["WorkType"]);
                            UserData.IsB2CAgency = workType != null && Convert.ToInt16(workType.ToString()) == 2 ? true : false;
                            UserData.Country = Conversion.getInt32OrNull(oReader["Country"]);
                            UserData.Location = Conversion.getInt32OrNull(oReader["Location"]);
                            UserData.TvParams = new TvBo.Common().getTvParameters(UserData.Market, Conversion.getDecimalOrNull(UserData.TvVersion), UserData.WebVersion, ref errorMsg);
                            UserData.AgencyCanDisCom = (bool)oReader["AgencyCanDisCom"];

                            #region Show Flight
                            object _showFlight = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowFlight");
                            UserData.ShowFlight = (_showFlight != null) ? (bool)_showFlight : false;
                            #endregion

                            #region Payment Control
                            object _paymentControl = new TvBo.Common().getFormConfigValue("General", "PaymentControl");
                            if (_paymentControl != null && (bool)_paymentControl)
                            {
                                if (!(new Agency().getAgencyPasPaymentDefinition(UserData, ref errorMsg)))
                                {
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AgencyPaymentTypesUndefined").ToString();
                                    return false;
                                }
                            }
                            #endregion

                            #region Active contrat check
                            if ((bool)oReader["AgreeCheck"])
                            {
                                Int16? ContratCnt = Conversion.getInt16OrNull(oReader["ContratCnt"]);
                                if (!ContratCnt.HasValue && ContratCnt.Value < 1)
                                {
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AgencyNotAnyActiveContract").ToString();
                                    return false;
                                }
                            }
                            #endregion


                            UserData = getAgencyRecord(UserData);

                            #region Bonus
                            BonusRecord bonusInfo = new Users().getBonusInfoW(UserData.Market, UserData.AgencyID);
                            bonusInfo.BonusAgencySeeOwnW = true;
                            bonusInfo.BonusUserSeeAgencyW = true;
                            bonusInfo.BonusUserSeeOwnW = false;
                            bonusInfo.AgencyBonus = true;
                            bonusInfo.PassBonus = false;
                            bonusInfo.UserBonus = false;
                            UserData.Bonus = bonusInfo;
                            #endregion

                            #region WDoc
                            WDocRecord wDoc = UserData.WDoc;
                            wDoc.WDoc_Voucher = (bool)oReader["WDoc_Voucher"];
                            wDoc.WDoc_Ticket = (bool)oReader["WDoc_Ticket"];
                            wDoc.WDoc_Agree = (bool)oReader["WDoc_Agree"];
                            wDoc.WDoc_InvoiceA = (bool)oReader["WDoc_InvoiceA"];
                            wDoc.WDoc_InvoiceP = (bool)oReader["WDoc_InvoiceP"];
                            wDoc.WDoc_Insurance = (bool)oReader["WDoc_Insurance"];
                            wDoc.WDoc_Receipt = (bool)oReader["WDoc_Receipt"];
                            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040039081"))
                            {
                                wDoc.WDoc_Confirmation = (bool)oReader["WDoc_Confirmation"];
                                wDoc.WDoc_ExcursionVoucher = (bool)oReader["WDoc_ExcVoucher"];
                                wDoc.WDoc_TransportVoucher = (bool)oReader["WDoc_TransVoucher"];
                            }
                            #endregion

                            UserData.PIN = Conversion.getStrOrNull(oReader["IDNo"]);
                            UserData.Phone = Conversion.getStrOrNull(oReader["Phone"]);
                            UserData.Mobile = Conversion.getStrOrNull(oReader["Mobile"]);
                            UserData.EMail = Conversion.getStrOrNull(oReader["EMail"]);
                            UserData.HAddress = Conversion.getStrOrNull(oReader["HAddress"]);
                            UserData.Bank = Conversion.getStrOrNull(oReader["Bank"]);
                            UserData.BankNo = Conversion.getStrOrNull(oReader["BankNo"]);
                            UserData.BankAccNo = Conversion.getStrOrNull(oReader["BankAccNo"]);
                            UserData.AgencyRating = new UICommon().getAgencyRating(UserData.Market, UserData.AgencyID, UserData.WebVersion, ref errorMsg);
                            UserData.MarketLang = Conversion.getInt32OrNull(oReader["LangID"]);

                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
            }
            catch
            {
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool getAdminUser(ref User UserData, string IpNumber, ref string errorMsg, string SessionID)
        {
            string tsql = string.Empty;
            tsql = @"Select A.MasterPass AS Pass, A.Code, isnull(dbo.FindLocalName(A.NameLID, M.Code), A.Name) AS AgencyName, A.OprOffice, 
                        A.MainOffice, MainOfficeName='', A.Status, IP.IpNo, O.Operator, O.Market, A.AgencyType, A.BlackList, M.Cur, 
                        isnull(A.OwnAgency, 0) OwnAgency, isnull(P.PayOptTime, 0) PayOptTime, 
                        MaxRoomCount=Cast('0' AS tinyint),
                        AgreeCheck = Cast(0 as bit), 
                        ContratCnt = 1,
                        WebVersion = (Select WVersion From ParamSystem (NOLOCK) Where Market = ''), 
                        A.CuratorUser, A.MerlinxID, A.Aggregator, A.WorkType, M.Country, A.Location, OP.AgencyCanDisCom,
                        IDNo='', Phone='', Mobile='', EMail='', HAddress='', Bank='', BankNo='', BankAccNo='',
                        WDoc_Voucher=isnull(A.WDoc_Voucher, Cast('1' as bit)), 
                        WDoc_Ticket=isnull(A.WDoc_Ticket, Cast('1' as bit)), 
                        WDoc_Agree=isnull(A.WDoc_Agree, Cast('1' as bit)), 
                        WDoc_InvoiceA=isnull(A.WDoc_InvoiceA, Cast('1' as bit)), 
                        WDoc_InvoiceP=isnull(A.WDoc_InvoiceP, Cast('1' as bit)), 
                        WDoc_Insurance=isnull(A.WDoc_Insurance, Cast('1' as bit)), 
                        WDoc_Receipt=isnull(A.WDoc_Receipt, Cast('1' as bit)),
                        WDoc_TransVoucher=isnull(A.WDoc_TransVoucher, Cast('1' as bit)), 
                        WDoc_ExcVoucher=isnull(A.WDoc_ExcVoucher, Cast('1' as bit)), 
                        WDoc_Confirmation=isnull(A.WDoc_Confirmation, Cast('1' as bit)), 
                        M.LangID
                    From Agency A (NOLOCK) 
                    Left Join AgencyIP IP (NOLOCK) ON IP.Agency = A.Code 
                    Join Office O (NOLOCK) ON O.Code = A.OprOffice 
                    Join Operator OP (NOLOCK) ON OP.Code = O.Operator
                    Join Market M (NOLOCK)  ON M.Code = O.Market
                    Left Join ParamPay P (NOLOCK) on O.Market = P.Market
                    Where 
                        A.Status = 'Y'                         
                        AND A.Code = @Agency ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        UserData.AgencyName = (string)oReader["AgencyName"];
                        UserData.UserName = string.Empty;
                        UserData.OprOffice = (string)oReader["OprOffice"];
                        UserData.Operator = (string)oReader["Operator"];
                        UserData.Market = (string)oReader["Market"];
                        UserData.MainOffice = (string)oReader["MainOffice"];
                        UserData.MainOfficeName = (string)oReader["MainOfficeName"];
                        UserData.BlackList = (string)oReader["BlackList"] == "Y" ? true : false;
                        UserData.AgencyType = Conversion.getInt16OrNull(oReader["AgencyType"]);
                        UserData.SID = SessionID;
                        UserData.Authenticated = "OK";
                        UserData.SaleCur = (string)oReader["Cur"];
                        UserData.OwnAgency = (bool)oReader["OwnAgency"];
                        UserData.PayOptTime = (Int16)oReader["PayOptTime"];
                        UserData.IpNumber = IpNumber;
                        UserData.ShowAllRes = true;
                        UserData.MainAgency = true;
                        UserData.MaxRoomCount = (byte)oReader["MaxRoomCount"];
                        UserData.WebVersion = Conversion.getStrOrNull(oReader["WebVersion"]);
                        UserData.CuratorUser = Conversion.getStrOrNull(oReader["CuratorUser"]);
                        UserData.MerlinxID = Conversion.getStrOrNull(oReader["MerlinxID"]);
                        UserData.Aggregator = (bool)oReader["Aggregator"];
                        UserData.Ci = getCultureInfo("en-US");
                        object workType = Conversion.getInt16OrNull(oReader["WorkType"]);
                        UserData.IsB2CAgency = workType != null && Convert.ToInt16(workType.ToString()) == 2 ? true : false;
                        UserData.Country = Conversion.getInt32OrNull(oReader["Country"]);
                        UserData.Location = Conversion.getInt32OrNull(oReader["Location"]);
                        UserData.TvParams = new TvBo.Common().getTvParameters(UserData.Market, Conversion.getDecimalOrNull(UserData.TvVersion), UserData.WebVersion, ref errorMsg);
                        UserData.AgencyCanDisCom = (bool)oReader["AgencyCanDisCom"];

                        UserData.AgencyRec = new Agency().getAgency(UserData.AgencyID, true, ref errorMsg);

                        UserData.MarketLang = Conversion.getInt32OrNull(oReader["LangID"]);

                        return true;
                    }
                    else
                    {
                        errorMsg = "Agency not found.";
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public CultureInfo getCultureInfo(string culture)
        {
            if (!string.IsNullOrEmpty(culture))
                return new CultureInfo(culture);
            String[] userLang = HttpContext.Current.Request.UserLanguages;
            String[] CI = userLang[0].Split(';');
            string CultureStr = CI[0];
            CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
            if (allCulture.Where(w => Equals(w.Name, CultureStr)).Count() < 1)
            {
                CultureStr = "en-US";
            }
            if (Equals(CultureStr, "nb")) CultureStr = "nb-NO"; if (Equals(CultureStr, "sr-SC")) CultureStr = "en-US";
            if (Equals(CultureStr, "ar"))
            {
                string defaultCulture = Conversion.getStrOrNull(ConfigurationManager.AppSettings["DefaultCulture"]);
                CultureStr = string.IsNullOrEmpty(defaultCulture) ? "en-US" : (defaultCulture.IndexOf(CultureStr) > -1 ? defaultCulture : "en-US");
            }
            if (CultureStr.Length < 5)
            {
                CultureStr = (from culinf in CultureInfo.GetCultures(CultureTypes.AllCultures).AsEnumerable()
                              where culinf.Name == CultureStr
                              select new { CultureName = culinf.TextInfo.CultureName }).First().CultureName;
            }
            CultureInfo ci = new CultureInfo(CultureStr);
            return ci;
        }

        public User setGridColumnsOptions(User UserData)
        {
            string errorMsg = string.Empty;
            List<GridOptions> tmpGridColOpt = new UICommon().getWebGridConfig(UserData, ref errorMsg);
            if (tmpGridColOpt == null) return UserData;
            foreach (GridOptions row in tmpGridColOpt)
            {
                GridOptions gridOpt = new GridOptions();
                gridOpt.ID = UserData.GridColumnOptions.Count + 1;
                gridOpt.PageID = row.PageID;
                gridOpt.GridColumns = row.GridColumns;
                gridOpt.GridID = row.GridID;
                List<gridColumns> columns = new List<gridColumns>();
                columns = Newtonsoft.Json.JsonConvert.DeserializeObject<List<gridColumns>>(row.GridColumns);
                gridOpt.Columns = columns;
                gridOpt.Empty = false;
                UserData.GridColumnOptions.Add(gridOpt);
            }
            return UserData;
        }

        public User getAgencyRecord(User UserData)
        {
            string errorMsg = string.Empty;
            AgencyRecord agency = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
            UserData.AgencyRec = agency;
            return UserData;
        }

        public bool changePassword(User UserData, string Password)
        {
            string tsql = @"Update AgencyUser 
                            Set 
                                Pass = @Pass
                            Where
                                Code = @Code And Agency = @Agency ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Code", DbType.String, UserData.UserID);
                db.AddInParameter(dbCommand, "Pass", DbType.String, Password);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public BonusRecord getBonusInfoW(string market, string agency)
        {
            string tsql = @"Declare @UseSysParam VarChar(1), @BonusUserSeeAgencyW bit, @BonusAgencySeeOwnW bit, @BonusUserSeeOwnW bit,
                                @BonusPasUseW bit, @BonusAgencyUseW bit, @BonusUserUseW bit
                            Select @UseSysParam=UseSysParam From ParamReser (NOLOCK) Where Market=@Market     
                            --if (@UseSysParam = 'Y')
                            --  Select @BonusPasUseW=BonusPasUseW, @BonusUserUseW=BonusUserUseW, @BonusAgencyUseW=BonusAgencyUseW, @BonusUserSeeAgencyW=BonusUserSeeAgencyW, @BonusAgencySeeOwnW=BonusAgencySeeOwnW, @BonusUserSeeOwnW=BonusUserSeeOwnW From ParamReser (NOLOCK) Where Market = ''	
                            --Else 
                            Select @BonusPasUseW=BonusPasUseW, @BonusUserUseW=BonusUserUseW, @BonusAgencyUseW=BonusAgencyUseW, @BonusUserSeeAgencyW=BonusUserSeeAgencyW, @BonusAgencySeeOwnW=BonusAgencySeeOwnW, @BonusUserSeeOwnW=BonusUserSeeOwnW From ParamReser (NOLOCK) Where Market = @Market

                            if (Exists(Select RecID From Agency (NOLOCK) Where Code=(Select Case When isnull(MainOffice, '')='' Then @Agency Else MainOffice End From Agency (NOLOCK) Where Code=@Agency And BonusUseSysParam=0)))
                              Select @BonusUserSeeAgencyW=BonusUserSeeAgencyW, @BonusAgencySeeOwnW=BonusAgencySeeOwnW, @BonusUserSeeOwnW=BonusUserSeeOwnW	
                            From Agency (NOLOCK) Where Code=(Select Case When isnull(MainOffice, '')='' Then @Agency Else MainOffice End From Agency (NOLOCK) Where Code=@Agency)

                            Select 
	                            BonusPasUseW=isnull(@BonusPasUseW, 0), 
	                            BonusUserUseW=isnull(@BonusUserUseW, 0), 
	                            BonusAgencyUseW=isnull(@BonusAgencyUseW, 0), 	
	                            BonusUserSeeAgencyW=Case When isnull(@BonusUserSeeOwnW, 0)=1 Then isnull(@BonusUserSeeAgencyW, 0) Else Cast('0' as bit) End, 
	                            BonusAgencySeeOwnW=isnull(@BonusAgencySeeOwnW, 0), 
	                            BonusUserSeeOwnW=isnull(@BonusUserSeeOwnW, 0) ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, agency);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        BonusRecord record = new BonusRecord();
                        record.PassBonus = (bool)R["BonusPasUseW"];
                        record.UserBonus = (bool)R["BonusUserUseW"];
                        record.AgencyBonus = (bool)R["BonusAgencyUseW"];
                        record.BonusUserSeeAgencyW = (bool)R["BonusUserSeeAgencyW"];
                        record.BonusAgencySeeOwnW = (bool)R["BonusAgencySeeOwnW"];
                        record.BonusUserSeeOwnW = (bool)R["BonusUserSeeOwnW"];
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Dispose();
            }
        }

        public static bool checkCurrentUserOperator(string crID)
        {
            if (HttpContext.Current.Session["UserData"] == null) return false;
            return string.Equals(((User)HttpContext.Current.Session["UserData"]).CustomRegID, crID);
        }

        public PaximumUserToken getPaximumToken(User UserData, ref string errorMsg)
        {
            try
            {
                PaximumRecord paximumSetting = new TvSystem().getPaximumConfig(UserData, ref errorMsg);
                if (paximumSetting.Pxm_Use.HasValue && paximumSetting.Pxm_Use.Value && !string.IsNullOrEmpty(paximumSetting.Pxm_AutMainCode))
                {
                    string paximumApiUrl = paximumSetting.Pxm_APIAddr + (paximumSetting.Pxm_APIAddr.Substring(paximumSetting.Pxm_APIAddr.Length - 1, 1) == "/" ? "" : "/");
                    string paximumB2BUrl = paximumSetting.Pxm_RedictionUrl + (paximumSetting.Pxm_RedictionUrl.Substring(paximumSetting.Pxm_RedictionUrl.Length - 1, 1) == "/" ? "" : "/");
                    string paximumAccess_Token = paximumSetting.Pxm_AutMainCode;
                    string baseUrl = string.Format("{0}TourVisio/GenerateTokenForTourvisio", paximumApiUrl);

                    GenerateTokenForTourvisio pParams = new GenerateTokenForTourvisio();
                    pParams.AccountId = UserData.AgencyID;
                    pParams.TvCustomerNo = UserData.CustomRegID;
                    pParams.UserId = UserData.UserID;

                    WebClientWithTimeout client = new WebClientWithTimeout();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + paximumAccess_Token);

                    try
                    {
                        var response = client.UploadString(baseUrl, Newtonsoft.Json.JsonConvert.SerializeObject(pParams));
                        apiResponse b2bToken = Newtonsoft.Json.JsonConvert.DeserializeObject<apiResponse>(response,
                            new JsonSerializerSettings
                            {
                                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                                NullValueHandling = NullValueHandling.Ignore,
                                DefaultValueHandling = DefaultValueHandling.Ignore,
                                ObjectCreationHandling = ObjectCreationHandling.Auto
                            });
                        if (string.IsNullOrEmpty(b2bToken.message) && !string.IsNullOrEmpty(b2bToken.token))
                        {
                            PaximumUserToken paximumUserToken = new PaximumUserToken();
                            paximumUserToken.token = b2bToken.token;
                            paximumUserToken.paximumApiUrl = paximumApiUrl;
                            paximumUserToken.paximumB2BUrl = paximumB2BUrl;
                            return paximumUserToken;
                        }
                        else
                        {
                            errorMsg = "You do not have authorization For Paximum";
                            return null;
                        }

                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                {
                    errorMsg = "You do not have authorization For Paximum";
                    return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
        }
    }
}

