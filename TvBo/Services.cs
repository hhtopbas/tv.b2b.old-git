﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class Services
    {
        public List<CatPackPlanSerRecord> getCatPackPlanSer(int? catPackID, ref string errorMsg)
        {
            List<CatPackPlanSerRecord> records = new List<CatPackPlanSerRecord>();

            string tsql = @"Select HPS.RecID, CatPackID=CP.RecID, HPS.StepNo, HPS.[Service], HPS.ServiceItem, HPS.IncPack, HPS.ValAccomNights, HPS.DispNo, HPS.Pilot, HPS.Board, HPS.SClass, HPS.Room
                            From HolPackSer (NOLOCK) HPS
                            Join CatalogPack CP (NOLOCK) ON CP.HolPack=HPS.HolPack
                            Where CP.RecID=@CatPackID";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, catPackID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        CatPackPlanSerRecord record = new CatPackPlanSerRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.CatPackID = Conversion.getInt32OrNull(R["CatPackID"]);
                        record.StepNo = Conversion.getInt32OrNull(R["StepNo"]);
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.ServiceItem = Conversion.getStrOrNull(R["ServiceItem"]);
                        record.IncPack = Conversion.getStrOrNull(R["IncPack"]);
                        record.ValAccomNights = Conversion.getStrOrNull(R["ValAccomNights"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.Pilot = Conversion.getBoolOrNull(R["Pilot"]);
                        record.Board = Conversion.getStrOrNull(R["Board"]);
                        record.SClass = Conversion.getStrOrNull(R["SClass"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CatPackPlanRecord> getCatPackPlan(User UserData, int? catPackID, ref string errorMsg)
        {
            List<CatPackPlanRecord> records = new List<CatPackPlanRecord>();

            string tsql = @"Select RecID, CatPackID, HolPack, StepNo, [Service], StartDay, Duration, Departure, Arrival,
                                 IncPack, Priced, AccomNights, Explanation, DispOrd, Direction, PayCom, PayComEB, PayPasEB,
                                 CanDiscount, StepType, CanEdit, CanDelete
                            From CatPackPlan (NOLOCK)
                            Where CatPackID=@CatPackID";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, catPackID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        CatPackPlanRecord record = new CatPackPlanRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.CatPackID = Conversion.getInt32OrNull(R["CatPackID"]);
                        record.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        record.StepNo = Conversion.getInt32OrNull(R["StepNo"]);
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.StartDay = Conversion.getInt16OrNull(R["StartDay"]);
                        record.Duration = Conversion.getInt16OrNull(R["Duration"]);
                        record.Departure = Conversion.getInt32OrNull(R["Departure"]);
                        record.Arrival = Conversion.getInt32OrNull(R["Arrival"]);
                        record.IncPack = Conversion.getStrOrNull(R["IncPack"]);
                        record.Priced = Conversion.getStrOrNull(R["Priced"]);
                        record.AccomNights = Conversion.getStrOrNull(R["AccomNights"]);
                        record.Explanation = Conversion.getStrOrNull(R["Explanation"]);
                        record.DispOrd = Conversion.getInt16OrNull(R["DispOrd"]);
                        record.Direction = Conversion.getStrOrNull(R["Direction"]);
                        record.PayCom = Conversion.getStrOrNull(R["PayCom"]);
                        record.PayComEB = Conversion.getStrOrNull(R["PayComEB"]);
                        record.PayPasEB = Conversion.getBoolOrNull(R["PayPasEB"]);
                        record.CanDiscount = Conversion.getBoolOrNull(R["CanDiscount"]);
                        record.StepType = Conversion.getByteOrNull(R["StepType"]);
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030300999"))
                        {
                            record.CanEdit = Conversion.getBoolOrNull(R["CanEdit"]);
                            record.CanDelete = Conversion.getBoolOrNull(R["CanDelete"]);
                        }
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
