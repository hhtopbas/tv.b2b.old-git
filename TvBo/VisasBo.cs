﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    [Serializable()]
    public class VisaRecord
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string LocalName { get; set; }
        public string NameS { get; set; }
        public string VisaType { get; set; }
        public string VisaLocation { get; set; } //1:embassy, 2:port of entry, 3:foreign affairs
        public Int16? PrepareDay { get; set; }
        public Int16? PrepareHour { get; set; }
        public string EmbassAddr { get; set; }
        public string EmbassCity { get; set; }
        public string EmbassPhone { get; set; }
        public string EmbassFax { get; set; }
        public string VisaDoc { get; set; }
        public string VisaCondition { get; set; }
        public string Description { get; set; }
        public Int16 ConfStat { get; set; }
        public decimal? TaxPer { get; set; }
        public bool PayCom { get; set; }
        public bool PayComEB { get; set; }
        public bool PayPasEB { get; set; }
        public bool CanDiscount { get; set; }
        public bool? RestSingleSale { get; set; }
        public VisaRecord()
        {
        }

    }

    [Serializable()]
    public class VisaFormSex
    {
        public VisaFormSex() { }
        public string Sex { get; set; }
        public string SexName { get; set; }
        public string SexNameL { get; set; }
    }

    [Serializable()]
    public class VisaFormDataRecord
    {
        public VisaFormDataRecord() { }
        public string Name { get; set; }
        public object Value { get; set; }
        public string ValueType { get; set; }
    }

    [Serializable()]
    public class PassportTypeRecord
    {
        public PassportTypeRecord()
        { 
        }
        public Int16? Value { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
    }

    [Serializable()]
    public class PurposeTravelRecord
    {
        public PurposeTravelRecord()
        {
        }
        public Int16? Value { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
    }

    [Serializable()]
    public class MaritalRecord
    {
        public MaritalRecord()
        {
        }
        public Int16? Value { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
    }

    public class EntrancePortRecord
    {
        public int? RecID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public int? Location { get; set; }
        public string LocationName { get; set; }
        public string LocationNameL { get; set; }
        public int? TrfLocation { get; set; }
        public string TrfLocationName { get; set; }
        public string TrfLocationNameL { get; set; }
        public Int16? PortType { get; set; }
        
        public EntrancePortRecord()
        { 
        }
    }

    public class VisaRestricRecord
    {
        public int? RecID { get; set; }
        public string Nationality { get; set; }
        public VisaRestricRecord()
        {

        }
    }
}
