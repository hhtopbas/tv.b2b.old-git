﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TvTools;
using System.Text;
using System.Web;
using System.Threading;
using System.Globalization;

namespace TvBo
{
    public class Excursions
    {
        public List<ExcursionRecord> getAllExcursionInfo(User UserData, ref string errorMsg)
        {
            ExcursionRecord record = new ExcursionRecord();
            string tsql = string.Empty;
                tsql =
@"
Select Code,Name From Excursion (NOLOCK)
";
            List<ExcursionRecord> lstExcursion = new List<ExcursionRecord>();
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        lstExcursion.Add(record);
                    }
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
            return lstExcursion;
        }
        public ExcursionRecord getExcursion(User UserData, string Code, ref string errorMsg)
        {
            ExcursionRecord record = new ExcursionRecord();
            string tsql = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040014038"))
                tsql =
@"
Select Code,[Name],LocalName=isnull(dbo.FindLocalName(NameLID, @Market),[Name]),
	NameS,[Description],Note,ConfStat,TaxPer,PayCom,PayComEB,PayPasEB,CanDiscount,MinAgeRest
From Excursion (NOLOCK)
Where Code=@Code
";
            else
                tsql =
@"
Select Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market),[Name]),
	NameS, [Description], Note, ConfStat, TaxPer, PayCom, PayComEB, PayPasEB, CanDiscount
From Excursion (NOLOCK)
Where Code=@Code
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.Note = Conversion.getStrOrNull(R["Note"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040014038"))
                        {
                            //record.DepTime = Conversion.getDateTimeOrNull(R["DepTime"]);
                            record.MinAgeRest = Conversion.getInt16OrNull(R["MinAgeRest"]);
                        }
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ExcursionPriceRecord> getExcursionPrices(User UserData, string Code, int? Location, DateTime? Date, bool hasHotel, bool hasFlight, ref string errorMsg)
        {
            List<ExcursionPriceRecord> records = new List<ExcursionPriceRecord>();
            string tsql =
@"
Select P.Excursion,P.Market,P.Location, 
    LocationName=(Select [Name] From Location (NOLOCK) Where RecID=P.Location),
    LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=P.Location),
    BegDate, EndDate, NetCur, SaleType, SaleCur,
    FreeMaxAge, ChdG1MaxAge, ChdG2MaxAge, Supplier
From ExcursionPrice P (NOLOCK) 
Left Join GrpAgencyDet AG on AG.Groups=AgencyGrp
";

            string whereStr = string.Empty;
            if (!string.IsNullOrEmpty(Code))
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += string.Format(" Excursion='{0}' ", Code);
            }
            if (!Convert.IsDBNull(Location))
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += string.Format(" Location={0} ", Location.Value);
            }
            if (!Convert.IsDBNull(Date))
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += " @date between BegDate And EndDate ";
            }
            if (whereStr.Length > 0)
                whereStr += " And";
            whereStr += " ((P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null)) ";

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040011033"))
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += " IsNull(ValidHotel,0) in (@HasHotel,0) And IsNull(ValidFlight,0) in (@HasFlight,0) ";
            }
            if (whereStr.Length > 0)
                tsql += " Where " + whereStr;

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                if (!Convert.IsDBNull(Date))
                    db.AddInParameter(dbCommand, "date", DbType.DateTime, Date.Value);
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040011033"))
                {
                    db.AddInParameter(dbCommand, "HasHotel", DbType.Boolean, hasHotel);
                    db.AddInParameter(dbCommand, "HasFlight", DbType.Boolean, hasFlight);
                }
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ExcursionPriceRecord record = new ExcursionPriceRecord
                        {
                            Excursion = Conversion.getStrOrNull(R["Excursion"]),
                            Market = Conversion.getStrOrNull(R["Market"]),
                            Location = (int)(R["Location"]),
                            LocationName = Conversion.getStrOrNull(R["LocationName"]),
                            LocationLocalName =
                                                                  Conversion.getStrOrNull(R["LocationLocalName"]),
                            BegDate = (DateTime)R["BegDate"],
                            EndDate = (DateTime)R["EndDate"],
                            NetCur = Conversion.getStrOrNull(R["NetCur"]),
                            SaleType = Conversion.getStrOrNull(R["SaleType"]),
                            SaleCur = Conversion.getStrOrNull(R["SaleCur"]),
                            FreeMaxAge = Conversion.getDecimalOrNull(R["FreeMaxAge"]),
                            ChdG1MaxAge =
                                                                  Conversion.getDecimalOrNull(R["ChdG1MaxAge"]),
                            ChdG2MaxAge =
                                                                  Conversion.getDecimalOrNull(R["ChdG2MaxAge"]),
                            Supplier = Conversion.getStrOrNull(R["Supplier"])
                        };

                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public DataRow getExcursionDetail(User UserData, string Code, int? Location, DateTime? Date, bool hasHotel, bool hasFlight, ref string errorMsg)
        {
            ExcursionRecord excursion = getExcursion(UserData, Code, ref errorMsg);
            List<ExcursionPriceRecord> excursionPrice = getExcursionPrices(UserData, Code, Location, Date, hasHotel, hasFlight, ref errorMsg);
            var result = from q in excursionPrice.AsEnumerable()
                         where q.Excursion == excursion.Code
                         select new
                         {
                             excursion.Name,
                             excursion.LocalName,
                             excursion.NameS,
                             q.Location,
                             q.LocationName,
                             q.LocationLocalName,
                             excursion.CanDiscount,
                             q.Supplier
                         };
            return new TvBo.Common().LINQToDataRow(result);
        }

        public List<Location> getExcursionLocation(User UserData, string @plMarket, int? Country, int? City, ref string errorMsg)
        {
            List<int?> ExcurLoc = new List<int?>();
            List<Location> Location = new Locations().getLocationList(UserData.Market, LocationType.None, null, Country, null, null, ref errorMsg);

            string tsql = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040009029"))
                tsql =
@"
Select Distinct EP.Location
From ExcursionPrice EP (NOLOCK) 
Join Excursion E (NOLOCK) ON EP.Excursion = E.Code	
Left Join Location L (NOLOCK) ON EP.Location=L.RecID
Left Join GrpAgencyDet AG on AG.Groups=EP.AgencyGrp
Where EP.EndDate >= GetDate()-1
    And EP.Market=@plMarket
    And (L.City is null Or @ArrCity is null Or L.City=@ArrCity)
    And ((EP.Agency=@Agency or AG.Agency=@Agency) or (EP.Agency='' and AG.Agency is Null))
    And E.Type<>1
";
            else
                tsql =
@"
Select Distinct EP.Location
From ExcursionPrice EP (NOLOCK) 
Join Excursion E (NOLOCK) ON EP.Excursion = E.Code	
Left Join Location L (NOLOCK) ON EP.Location=L.RecID
Left Join GrpAgencyDet AG on AG.Groups=EP.AgencyGrp
Where EP.EndDate >= GetDate()-1
    And EP.Market=@plMarket
    And (L.City is null Or @ArrCity is null Or L.City=@ArrCity)
    And ((EP.Agency=@Agency or AG.Agency=@Agency) or (EP.Agency='' and AG.Agency is Null))
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, City);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        ExcurLoc.Add(Conversion.getInt32OrNull(oReader["Location"]));
                    }
                }
                List<Location> retVal = (from q1 in Location
                                         join q2 in ExcurLoc on q1.RecID equals (q2.HasValue ? q2.Value : -1)
                                         select q1).ToList<Location>();
                return retVal;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ExcursionRecord> getExcursionListForLocationAndDate(User UserData, string plMarket, int? Location, DateTime? ResBegDate, DateTime? ResEndDate, bool forPriceList, ref string errorMsg)
        {
            List<ExcursionRecord> list = new List<ExcursionRecord>();
            string tsql = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040009029"))
            {
                tsql =
@"
SET DATEFIRST 1
Select Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market),[Name]),
  NameS, [Description], Note, ConfStat, TaxPer, PayCom, PayComEB, PayPasEB, CanDiscount, Type=isnull(E.Type,0)
From (
   Select Distinct EP.Excursion
   From ExcursionPrice EP (NOLOCK)
   Left Join GrpAgencyDet AG on AG.Groups=EP.AgencyGrp
   Where EP.Location=@Location
      And EP.Market=@plMarket
      And (@ResBegDate is null or SubString(IsNull(EP.ValDays,'1111111'),DATEPART(dw,@ResBegDate),1)=1)
      And (@ResBegDate is null or @ResBegDate>=EP.BegDate)
      And (@ResEndDate is null or @ResEndDate<=EP.EndDate)      
      And ((EP.Agency=@Agency or AG.Agency=@Agency) or (EP.Agency='' and AG.Agency is Null))
";
                if (!forPriceList)
                {
                    tsql +=
@"
      And dbo.DateOnly(GetDate()) between EP.SaleBegDate and EP.SaleEndDate
";
                }
                tsql +=
@"
      And
	    (
		    not Exists(Select RecID From ExcursionDates Where Excursion=EP.Excursion and Market=EP.Market)
		    or
		    Exists(Select RecID From ExcursionDates 
			    Where Excursion=EP.Excursion and Market=EP.Market
			    and (Location Is Null or isnull(EP.Location,0)=Location)
			    and (@ResBegDate is null or SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@ResBegDate),1)=1))
	    ) 

) X
Join Excursion E (NOLOCK) ON E.Code=X.Excursion 
Where E.Type<>1
";
            }
            else
                tsql =
@"
Select Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market),[Name]),
  NameS, [Description], Note, ConfStat, TaxPer, PayCom, PayComEB, PayPasEB, CanDiscount
From (
   Select Distinct EP.Excursion
   From ExcursionPrice EP (NOLOCK)
   Left Join GrpAgencyDet AG on AG.Groups=EP.AgencyGrp
   Where EP.Location=@Location
      And EP.Market=@plMarket
      And (@ResBegDate is null or @ResBegDate>=EP.BegDate)
      And (@ResEndDate is null or @ResEndDate<=EP.EndDate)
      And ((EP.Agency=@Agency or AG.Agency=@Agency) or (EP.Agency='' and AG.Agency is Null))
) X
Join Excursion E (NOLOCK) ON E.Code=X.Excursion 
";
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200009") && !forPriceList)
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040009029"))
                {
                    if (UserData.WebService)
                        tsql += @" And E.B2CPub=1";
                    else
                        tsql += @" And E.B2BPub=1";
                }
                else
                {
                    if (UserData.WebService)
                        tsql += @"Where E.B2CPub=1";
                    else
                        tsql += @"Where E.B2BPub=1";
                }
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Location", DbType.Int32, Location);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.DateTime, ResBegDate);
                db.AddInParameter(dbCommand, "ResEndDate", DbType.DateTime, ResEndDate);
                db.AddInParameter(dbCommand, "plMarket", DbType.AnsiString, plMarket);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ExcursionRecord record = new ExcursionRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.Note = Conversion.getStrOrNull(R["Note"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040009029"))
                            record.Type = (ExcursionType)((Int16)R["Type"]);
                        else
                            record.Type = ExcursionType.Regular;

                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<calendarColor> getExcursionDates(User UserData, string plMarket, DateTime? resBegDate, DateTime? resEndDate, int? Location, string Excursion, ref string errorMsg)
        {
            List<calendarColor> dates = new List<calendarColor>();

            List<string> records = new List<string>();

            string tsql =
@"
Select BegDate,EndDate,DayofWeek 
From ExcursionDates (NOLOCK)
Where Excursion=@Excursion
    And Market=@plMarket
    And Location=@Location
    And BegDate<=@ResEndDate
    And EndDate>=@ResBegDate
Order By RecID Desc
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Excursion", DbType.String, Excursion);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Location", DbType.Int32, Location);
                db.AddInParameter(dbCommand, "ResEndDate", DbType.DateTime, resBegDate);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.DateTime, resEndDate);
                DataSet dbData = db.ExecuteDataSet(dbCommand);
                if (dbData != null && dbData.Tables[0] != null && dbData.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = dbData.Tables[0];
                    for (DateTime exdt = resBegDate.Value; exdt <= resEndDate.Value; exdt = exdt.AddDays(1))
                    {
                        int day = 0;
                        switch (exdt.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                day = 0;
                                break;
                            case DayOfWeek.Tuesday:
                                day = 1;
                                break;
                            case DayOfWeek.Wednesday:
                                day = 2;
                                break;
                            case DayOfWeek.Thursday:
                                day = 3;
                                break;
                            case DayOfWeek.Friday:
                                day = 4;
                                break;
                            case DayOfWeek.Saturday:
                                day = 5;
                                break;
                            case DayOfWeek.Sunday:
                                day = 6;
                                break;
                            default:
                                day = 0;
                                break;
                        }

                        if ((from d in dt.AsEnumerable()
                             where exdt >= d.Field<DateTime>("BegDate")
                             && exdt <= d.Field<DateTime>("EndDate")
                             && d.Field<string>("DayofWeek").ToString()[day] == '1'
                             select d).Count() > 0)
                            dates.Add(new calendarColor
                            {
                                Day = exdt.Day,
                                Month = exdt.Month,
                                Year = exdt.Year
                            });
                    }
                }
                return dates;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return dates;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ExcursionPack getExcursionPack(User UserData, int? packID, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 46, VersionControl.Equality.gt))
                tsql =
@"   
Select RecID,Name,NameL=ISNULL(dbo.FindLocalName(NameLID,@Market),Name),Market,MinSaleUnit
From ExcursionPack (NOLOCK)
Where RecID=@packID
";
            else
                tsql =
@"   
Select RecID,Name,NameL=ISNULL(dbo.FindLocalName(NameLID,@Market),Name),Market
From ExcursionPack (NOLOCK)
Where RecID=@packID
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "packID", DbType.Int32, packID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        return new ExcursionPack
                        {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            NameL = Conversion.getStrOrNull(R["NameL"]),
                            Market = Conversion.getStrOrNull(R["Market"]),
                            MinSaleUnit = (VersionControl.CheckWebVersion(UserData.WebVersion, 46, VersionControl.Equality.gt)) ? Conversion.getInt32OrNull(R["MinSaleUnit"]) : null
                        };
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ExcursionPack> getExcursionPackList(User UserData, ref string errorMsg)
        {
            List<ExcursionPack> records = new List<ExcursionPack>();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 46, VersionControl.Equality.gt))
                tsql =
@"   
Select RecID,Name,NameL=ISNULL(dbo.FindLocalName(NameLID,@Market),Name),Market,MinSaleUnit
From ExcursionPack (NOLOCK)
";
            else
                tsql =
@"   
Select RecID,Name,NameL=ISNULL(dbo.FindLocalName(NameLID,@Market),Name),Market
From ExcursionPack (NOLOCK)
";
            if (string.Equals(UserData.CustomRegID, Common.crID_CelexTravel))
                tsql += " where Market=@Market";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(new ExcursionPack
                        {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            NameL = Conversion.getStrOrNull(R["NameL"]),
                            Market = Conversion.getStrOrNull(R["Market"]),
                            MinSaleUnit = (VersionControl.CheckWebVersion(UserData.WebVersion, 46, VersionControl.Equality.gt)) ? Conversion.getInt32OrNull(R["MinSaleUnit"]) : null
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getExcursionMedia(User UserData, string code, DateTime? date, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"   
Select TextT,TextR,TextH 
From ExcursionText (NOLOCK)
Where Excursion=@code And Market=@market
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "code", DbType.AnsiString, code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        string TextT = Conversion.getStrOrNull(R["TextT"]);
                        string TextR = Conversion.getStrOrNull(R["TextR"]);
                        string TextH = Conversion.getStrOrNull(R["TextH"]);
                        if (!string.IsNullOrEmpty(TextH))
                            return TextH;
                        else
                            if (!string.IsNullOrEmpty(TextT))
                                return TextT;
                            else
                                if (!string.IsNullOrEmpty(TextR))
                                {
                                    SautinSoft.RtfToHtml rtfHtml = new SautinSoft.RtfToHtml();
                                    rtfHtml.Encoding = SautinSoft.eEncoding.UTF_8;
                                    rtfHtml.OutputFormat = SautinSoft.eOutputFormat.HTML_401;
                                    rtfHtml.HtmlParts = SautinSoft.eHtmlParts.Html_body;
                                    return rtfHtml.ConvertString(TextR);
                                }
                                else
                                    return string.Empty;
                    }
                    else
                        return string.Empty;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ExcursionTimeTable getExcursionTimes(User UserData, int? timeTableID, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"   
Select RecID,Excursion,Market,TransFromTime,TransToTime,FreeFromTime,FreeToTime,PriceCode,Explanation
From ExcursionDepTime1 (NOLOCK)
Where RecID=@timeTableID
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "timeTableID", DbType.Int32, timeTableID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        ExcursionTimeTable rec = new ExcursionTimeTable();
                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.Excursion = Conversion.getStrOrNull(R["Excursion"]);
                        rec.Market = Conversion.getStrOrNull(R["Market"]);
                        rec.TransFromTime = Conversion.getDateTimeOrNull(R["TransFromTime"]);
                        rec.TransToTime = Conversion.getDateTimeOrNull(R["TransToTime"]);
                        rec.FreeFromTime = Conversion.getInt32OrNull(R["FreeFromTime"]);
                        rec.FreeToTime = Conversion.getInt32OrNull(R["FreeToTime"]);
                        rec.PriceCode = Conversion.getStrOrNull(R["PriceCode"]);
                        rec.Explanation = Conversion.getStrOrNull(R["Explanation"]);

                        return rec;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ExcursionRecord> getExcursionPackDetail(User UserData, string plMarket, int? packID, ref string errorMsg)
        {
            List<ExcursionRecord> list = new List<ExcursionRecord>();
            string tsql = string.Empty;
            tsql =
@"
SET DATEFIRST 1
Select E.Code,E.[Name],LocalName=isnull(dbo.FindLocalName(E.NameLID,@Market),E.[Name]),
  E.NameS,E.[Description],E.Note,E.ConfStat,E.TaxPer,E.PayCom,E.PayComEB,E.PayPasEB,
  E.CanDiscount,Type=isnull(E.Type,0)
From ExcursionPack Ep (NOLOCK)
Join ExcursionPackDet Epd (NOLOCK) ON Ep.RecID=Epd.PackID
Join Excursion E (NOLOCK) ON Epd.Excursion=E.Code
Where Ep.Market=@plMarket
  And Ep.RecID=@packID 
  And E.B2BPub=1
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.AnsiString, plMarket);
                db.AddInParameter(dbCommand, "packID", DbType.Int32, packID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ExcursionRecord record = new ExcursionRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.Note = Conversion.getStrOrNull(R["Note"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040009029"))
                            record.Type = (ExcursionType)((Int16)R["Type"]);
                        else
                            record.Type = ExcursionType.Regular;

                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }

    public class ExcursionSearch
    {
        public List<ExcursionSearchFilterData> getExcursionFilterData(User UserData, DateTime? Date, ref string errorMsg)
        {
            List<ExcursionSearchFilterData> records = new List<ExcursionSearchFilterData>();

            string tsql =
@"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFIRST 1
Select Ep.RecID,Ep.Excursion,E.Name,NameL=isnull(dbo.FindLocalName(E.NameLID,@market),E.Name),
  Ep.Location,LocationName=L.Name,LocationNameL=isnull(dbo.FindLocalName(L.NameLID,@market),L.Name),
  IndSale,Ep.PriceCode,E.Category,CategoryName=Ec.Name,CategoryNameL=isnull(dbo.FindLocalName(Ec.NameLID,@market),Ec.Name),
  E.EstTime,E.Type
From ExcursionPrice Ep
join Location L ON L.RecID=Ep.Location
Left Join Excursion E ON E.Code=Ep.Excursion
Left Join ExcursionCat Ec ON Ec.Code=E.Category
Where 
    dbo.DateOnly(GetDate())<=Ep.EndDate
And (@Date is null or SubString(IsNull(Ep.ValDays,'1111111'),DATEPART(dw,@Date),1)=1)
And (Ep.SaleBegDate is null Or dbo.DateOnly(GetDate())>=Ep.SaleBegDate)
And (Ep.SaleEndDate is null Or dbo.DateOnly(GetDate())<=Ep.SaleEndDate)
And Ep.Market=@plMarket
And ((isnull(Ep.Agency,'')='' Or Ep.Agency=@AgencyId) And (isnull(Ep.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@AgencyId And Groups=Ep.AgencyGrp))) 
And isnull(E.B2BPub,0)=1
And
	(
		not Exists(Select RecID From ExcursionDates Where Excursion=Ep.Excursion and Market=Ep.Market)
		or
		Exists(Select RecID From ExcursionDates 
			Where Excursion=Ep.Excursion and Market=Ep.Market
			and (Location Is Null or isnull(Ep.Location,0)=Location)
			and (@Date is null or SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@Date),1)=1))
	) 
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "AgencyId", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Date", DbType.DateTime, Date);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ExcursionSearchFilterData rec = new ExcursionSearchFilterData();
                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.Excursion = Conversion.getStrOrNull(R["Excursion"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                        rec.Location = (int)R["Location"];
                        rec.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        rec.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        rec.IndSale = Conversion.getInt32OrNull(R["IndSale"]);
                        rec.PriceCode = Conversion.getStrOrNull(R["PriceCode"]);
                        rec.Category = Conversion.getStrOrNull(R["Category"]);
                        rec.CategoryName = Conversion.getStrOrNull(R["CategoryName"]);
                        rec.CategoryNameL = Conversion.getStrOrNull(R["CategoryNameL"]);
                        rec.EstTime = Conversion.getDateTimeOrNull(R["EstTime"]);
                        rec.Type = (ExcursionType)((Int16)R["Type"]);

                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ExcursionSearchFilterData> getExcursionPackFilterData(User UserData, DateTime? date, ref string errorMsg)
        {
            List<ExcursionSearchFilterData> records = new List<ExcursionSearchFilterData>();

            string tsql =
@"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFIRST 1
Select Distinct Name=EPck.Name,NameL=isnull(dbo.FindLocalName(EPck.NameLID,@market),EPck.Name),
  Ep.Location,LocationName=L.Name,LocationNameL=isnull(dbo.FindLocalName(L.NameLID,@market),L.Name),
  E.Category,CategoryName=Ec.Name,CategoryNameL=isnull(dbo.FindLocalName(Ec.NameLID,@market),Ec.Name)  
From ExcursionPrice Ep
Join ExcursionPack EPck ON Ep.Excursion=EP.Excursion
Join ExcursionPackDet EPckD ON EPck.RecID=EPckD.PackID
join Location L ON L.RecID=Ep.Location
Left Join Excursion E ON E.Code=Ep.Excursion
Left Join ExcursionCat Ec ON Ec.Code=E.Category
Where 
    dbo.DateOnly(GetDate())<=Ep.EndDate
and SubString(IsNull(Ep.ValDays,'1111111'),DATEPART(dw,@Date),1)=1
And (Ep.SaleBegDate is null Or dbo.DateOnly(GetDate())>=Ep.SaleBegDate)
And (Ep.SaleEndDate is null Or dbo.DateOnly(GetDate())<=Ep.SaleEndDate)
And Ep.ExcurPackID>0
And isnull(Ep.PriceCode,'')=''
And Ep.Market=@plMarket
And ((isnull(Ep.Agency,'')='' Or Ep.Agency=@AgencyId) And (isnull(Ep.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@AgencyId And Groups=Ep.AgencyGrp))) 
And isnull(E.B2BPub,0)=1
And
	(
		not Exists(Select RecID From ExcursionDates Where Excursion=Ep.Excursion and Market=Ep.Market)
		or
		Exists(Select RecID From ExcursionDates 
			Where Excursion=Ep.Excursion and Market=Ep.Market
			and (Location Is Null or isnull(Ep.Location,0)=Location)
			and SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@Date),1)=1)
	)   
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "AgencyId", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Date", DbType.DateTime, date);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ExcursionSearchFilterData rec = new ExcursionSearchFilterData();
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                        rec.Location = (int)R["Location"];
                        rec.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        rec.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        rec.Category = Conversion.getStrOrNull(R["Category"]);
                        rec.CategoryName = Conversion.getStrOrNull(R["CategoryName"]);
                        rec.CategoryNameL = Conversion.getStrOrNull(R["CategoryNameL"]);

                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ExcursionSearchResult> searchExcursionPackage(User UserData, ResDataRecord ResData, ExcursionSearchCriteria criteria, bool hasHotel, bool hasFlight, ref string errorMsg)
        {
            List<ExcursionSearchResult> records = new List<ExcursionSearchResult>();
            string tsql = string.Empty;

            if (criteria.Type == ExcursionType.Tour)
            {
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 36, VersionControl.Equality.gt))
                    tsql =
                    #region For Tour
 @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFIRST 1

if OBJECT_ID('TempDB.dbo.#ExcursionPackage') is not null Drop Table dbo.#ExcursionPackage
if OBJECT_ID('TempDB.dbo.#ExcursionPackageCount') is not null Drop Table dbo.#ExcursionPackageCount
if OBJECT_ID('TempDB.dbo.#ExcurPackCount') is not null Drop Table dbo.#ExcurPackCount
Declare @PackageServcieCount int

Select Exc.ExcDate,Exc.Excursion,PriceRecID=Exc.RecID,EP.RecID,EP.Name,NameL=isnull(dbo.FindLocalName(EP.NameLID,@Market),EP.Name)  
INTO #ExcursionPackage
From ExcursionPack EP 
Join ExcursionPackDet D ON D.PackID=EP.REcID
OUTER APPLY
(
   Select ExcDate=(Case When (D.DayOfWeek-DATEPART(dw, @BegDate))<0 
						then @BegDate+(7-Abs(D.DayOfWeek - DATEPART(dw, @BegDate))) 
						else @BegDate+(D.DayOfWeek-DATEPART(dw, @BegDate))
						end
				   ),
		P.RecID,P.Excursion
   From ExcursionPrice P
   Join Excursion E ON E.Code=P.Excursion
   Left Join GrpAgencyDet AG on AG.Groups=P.AgencyGrp
   Where P.Market=@Market
     And E.Code=D.Excursion
	 and P.ExcurPackID=EP.RecID
	 and (IsNull(P.Location,0)=@DepLocation or IsNull(P.Location,0)=0)
	 and @BegDate between P.BegDate and P.EndDate
	 and @ResDate between IsNull(P.SaleBegDate,@ResDate) and IsNull(P.SaleEndDate,@ResDate)
	 and @GrpPax between IsNull(P.FromPax,0) and IsNull(P.ToPax,999)	 
	 and ((P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null))    
	 and isnull(P.PriceCode,'')=''   
	 and P.SaleAdlPrice>0
	 and (E.Type=0 Or E.Type=2)	 
     and IsNull(ValidHotel,0)=@HasHotel
     and IsNull(ValidFlight,0)=@HasFlight
) Exc
Where EP.Market=@Market
  And Exc.ExcDate is not null
  And Exc.RecID is not null


Select EP.RecID,Count=Count(D.RecID)
INTO #ExcursionPackageCount
From ExcursionPack EP 
Join ExcursionPackDet D ON D.PackID=EP.RecID
Group By EP.RecID

Select RecID,Count=COUNT(PriceRecID)  
INTO #ExcurPackCount
From #ExcursionPackage 
Where 
	ExcDate between @BegDate and @EndDate
Group By RecID

if Exists(Select * From #ExcursionPackageCount C1 Join #ExcurPackCount C2 ON c1.RecID=C2.RecID Where C1.Count=C2.Count)
Begin
	Select Epk.ExcDate,Epk.RecID,Epk.Name,Epk.NameL,Epk.Excursion,Epk.PriceRecID
	From #ExcursionPackage Epk
	Join (Select C1.RecID From #ExcursionPackageCount C1 Join #ExcurPackCount C2 ON c1.RecID=C2.RecID Where C1.Count=C2.Count) Exc ON Epk.RecID=Exc.RecID
	Where 
		ExcDate between @BegDate and @EndDate
End
";
                    #endregion
                else
                    tsql =
                    #region For Tour
 @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFIRST 1
Select Distinct EP.RecID,EP.Name,NameL=isnull(dbo.FindLocalName(EP.NameLID,@Market),EP.Name)
From ExcursionPack EP 
Join ExcursionPackDet D ON D.PackID=EP.REcID
Where EP.Market=@Market
and Exists(
		   Select P.RecID 
		   From ExcursionPrice P
		   Join Excursion E ON E.Code=P.Excursion
		   Left Join GrpAgencyDet AG on AG.Groups=P.AgencyGrp
		   Where P.Market=@Market
             And E.Code=D.Excursion
			 and P.ExcurPackID is not null
			 and (IsNull(P.Location,0)=@DepLocation or IsNull(P.Location,0)=0)
			 and @BegDate between P.BegDate and P.EndDate
			 and @ResDate between IsNull(P.SaleBegDate,@ResDate) and IsNull(P.SaleEndDate,@ResDate)
			 and @GrpPax between IsNull(P.FromPax,0) and IsNull(P.ToPax,999)
			 and SubString(IsNull(P.ValDays,'1111111'),DATEPART(dw,@BegDate),1)=1
			 and ((P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null))    
			 and isnull(P.PriceCode,'')=''   
			 and P.SaleAdlPrice>0
			 and E.Type=0 
			 and
			   (
				 not Exists(Select RecID From ExcursionDates Where Excursion=E.Code and Market=P.Market)
				 or
				 Exists(Select RecID From ExcursionDates 
						Where Excursion=E.Code and Market=P.Market
						and (Location Is Null or isnull(P.Location,0)=Location)
						and SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@BegDate),1)=1)
			   )   
)
";
                    #endregion
            }

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "DepLocation", DbType.Int32, criteria.ArrCity);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, criteria.CheckIn);
                db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, ResData.ResMain.EndDate);
                db.AddInParameter(dbCommand, "ResDate", DbType.DateTime, DateTime.Today);
                db.AddInParameter(dbCommand, "GrpPax", DbType.Int32, criteria.RoomsInfo.FirstOrDefault().Adult + criteria.RoomsInfo.FirstOrDefault().Child);
                db.AddInParameter(dbCommand, "HasHotel", DbType.Boolean, hasHotel);
                db.AddInParameter(dbCommand, "HasFlight", DbType.Boolean, hasFlight);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    List<ExcursionPackService> list = new List<ExcursionPackService>();
                    int refNo = 0;
                    while (R.Read())
                    {
                        refNo++;
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, 36, VersionControl.Equality.gt))
                        {
                            ExcursionPackService rec = new ExcursionPackService();
                            rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                            rec.ExcDate = Conversion.getDateTimeOrNull(R["ExcDate"]);
                            rec.Name = Conversion.getStrOrNull(R["Name"]);
                            rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                            rec.PriceRecID = Conversion.getInt32OrNull(R["PriceRecID"]);
                            list.Add(rec);
                        }
                        else
                        {
                            ExcursionSearchResult rec = new ExcursionSearchResult();
                            rec.RefNo = Conversion.getInt32OrNull(R["RecID"]);
                            rec.CheckIn = criteria.CheckIn;
                            rec.PackageName = Conversion.getStrOrNull(R["Name"]);
                            rec.PackageNameL = Conversion.getStrOrNull(R["NameL"]);

                            rec.PackageDetails = getPackageDetail(UserData, criteria, rec.RefNo, hasHotel, hasFlight, ref errorMsg);
                            decimal? totalPrice = rec.PackageDetails.Count > 0 ? rec.PackageDetails.Sum(s => s.TotalPrice.HasValue ? s.TotalPrice.Value : 0) : 0;
                            rec.TotalPrice = totalPrice;
                            rec.SaleCur = rec.PackageDetails.Count > 0 ? rec.PackageDetails.FirstOrDefault().SaleCur : UserData.SaleCur;

                            if (rec.PackageDetails.Count > 0)
                            {
                                rec.Location = rec.PackageDetails.FirstOrDefault().Location;
                                rec.LocationName = rec.PackageDetails.FirstOrDefault().LocationName;
                                rec.LocationNameL = rec.PackageDetails.FirstOrDefault().LocationNameL;
                            }
                            records.Add(rec);
                        }

                    }
                    if (VersionControl.CheckWebVersion(UserData.WebVersion, 36, VersionControl.Equality.gt))
                    {
                        if (list.Count > 0)
                        {
                            var query = from q in list
                                        group q by new { q.RecID, q.Name, q.NameL } into k
                                        select new { k.Key.RecID, k.Key.Name, k.Key.NameL };

                            foreach (var row in query)
                            {
                                ExcursionSearchResult rec = new ExcursionSearchResult();
                                rec.RefNo = row.RecID;
                                rec.PackageName = row.Name;
                                rec.PackageNameL = row.NameL;
                                rec.CheckIn = criteria.CheckIn;

                                rec.PackageDetails = getPackageDetail(UserData, criteria, rec.RefNo, hasHotel, hasFlight, ref errorMsg);
                                decimal? totalPrice = rec.PackageDetails.Count > 0 ? rec.PackageDetails.Sum(s => s.TotalPrice.HasValue ? s.TotalPrice.Value : 0) : 0;
                                rec.TotalPrice = totalPrice;
                                rec.SaleCur = rec.PackageDetails.Count > 0 ? rec.PackageDetails.FirstOrDefault().SaleCur : UserData.SaleCur;
                                bool adding = true;
                                DateTime tmpDate = criteria.CheckIn;
                                foreach (var tmp in rec.PackageDetails.OrderBy(o => Common.getDayOfWeekTv(o.CheckIn.Value.DayOfWeek)))
                                {
                                    if (tmp.CheckIn.Value >= tmpDate)
                                    {
                                        adding = true;
                                        tmpDate = tmp.CheckIn.Value;
                                    }
                                    else
                                    {
                                        adding = false;
                                        break;
                                    }
                                }
                                if (adding)
                                    records.Add(rec);
                            }
                        }
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ExcursionSearchResult> getPackageDetail(User UserData, ExcursionSearchCriteria criteria, int? packID, bool hasHotel, bool hasFlight, ref string errorMsg)
        {
            List<ExcursionSearchResult> records = new List<ExcursionSearchResult>();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 36, VersionControl.Equality.gt))
                tsql =
@"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFIRST 1
Select E.Code,E.Name,NameL=isnull(dbo.FindLocalName(E.NameLID,@market),E.Name),
 P.Location,LocationName=L.Name,LocationNameL=isnull(dbo.FindLocalName(L.NameLID,@market),L.Name),
 SaleAdl=P.SaleAdlPrice,SaleChdG1=0,SaleChdG2=P.SaleChdG1Price,SaleChdG3=P.SaleChdG2Price,
 ChdG1MaxAge=FreeMaxAge,ChdG2MaxAge=ChdG1MaxAge,ChdG3MaxAge=ChdG2MaxAge,SaleCur=SaleCur,PriceCode='',
 TransFromTime=null,TransToTime=null,FreeFromTime=null,FreeToTime=null,P.ExcurPackID,
 ExcDate=(Case When (D.DayOfWeek-DATEPART(dw, @BegDate))<0 
						then @BegDate+(7-Abs(D.DayOfWeek - DATEPART(dw, @BegDate))) 
						else @BegDate+(D.DayOfWeek-DATEPART(dw, @BegDate))
						end
				   ),P.PriceType
From Excursion E
Join ExcursionPrice P On P.Excursion=E.Code
Join ExcursionPackDet D ON D.Excursion=E.Code
Left Join Location L On L.RecID=P.Location
Left Join GrpAgencyDet AG on AG.Groups=P.AgencyGrp
Where P.Market=@Market  
  and E.Code=D.Excursion
  and D.PackID=@PackID
  and P.ExcurPackID=D.PackID
  and (IsNull(P.Location,0)=@DepLocation or IsNull(P.Location,0)=0)
  and @BegDate between P.BegDate and P.EndDate
  and @ResDate between IsNull(P.SaleBegDate,@ResDate) and IsNull(P.SaleEndDate,@ResDate)
  and @GrpPax between IsNull(P.FromPax,0) and IsNull(P.ToPax,999)
  and SubString(IsNull(P.ValDays,'1111111'),DATEPART(dw,(Case When (D.DayOfWeek-DATEPART(dw, @BegDate))<=0 
						then @BegDate+(7-Abs(D.DayOfWeek - DATEPART(dw, @BegDate))) 
						else @BegDate+(D.DayOfWeek-DATEPART(dw, @BegDate))
						end
				   )),1)=1
  and ((P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null))    
  and isnull(P.PriceCode,'')=''   
  and P.SaleAdlPrice>0
  and E.Type=0 
  and
   (
     not Exists(Select RecID From ExcursionDates Where Excursion=E.Code and Market=P.Market)
     or
     Exists(Select RecID From ExcursionDates 
            Where Excursion=E.Code and Market=P.Market
            and (Location Is Null or isnull(P.Location,0)=Location)
            and SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,(Case When (D.DayOfWeek-DATEPART(dw, @BegDate))<=0 
						then @BegDate+(7-Abs(D.DayOfWeek - DATEPART(dw, @BegDate))) 
						else @BegDate+(D.DayOfWeek-DATEPART(dw, @BegDate))
						end
				   )),1)=1)
   )
  and IsNull(ValidHotel,0)=@HasHotel
  and IsNull(ValidFlight,0)=@HasFlight
";
            else
                tsql =
@"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFIRST 1
Select E.Code,E.Name,NameL=isnull(dbo.FindLocalName(E.NameLID,@market),E.Name),
 P.Location,LocationName=L.Name,LocationNameL=isnull(dbo.FindLocalName(L.NameLID,@market),L.Name),
 SaleAdl=P.SaleAdlPrice,SaleChdG1=0,SaleChdG2=P.SaleChdG1Price,SaleChdG3=P.SaleChdG2Price,
 ChdG1MaxAge=FreeMaxAge,ChdG2MaxAge=ChdG1MaxAge,ChdG3MaxAge=ChdG2MaxAge,SaleCur=SaleCur,PriceCode='',
 TransFromTime=null,TransToTime=null,FreeFromTime=null,FreeToTime=null,P.ExcurPackID,P.PriceType
From Excursion E
Join ExcursionPrice P On P.Excursion=E.Code
Join ExcursionPackDet D ON D.Excursion=E.Code
Left Join Location L On L.RecID=P.Location
Left Join GrpAgencyDet AG on AG.Groups=P.AgencyGrp
Where P.Market=@Market  
  and E.Code=D.Excursion
  and D.PackID=@PackID
  and P.ExcurPackID is not null
  and (IsNull(P.Location,0)=@DepLocation or IsNull(P.Location,0)=0)
  and @BegDate between P.BegDate and P.EndDate
  and @ResDate between IsNull(P.SaleBegDate,@ResDate) and IsNull(P.SaleEndDate,@ResDate)
  and @GrpPax between IsNull(P.FromPax,0) and IsNull(P.ToPax,999)
  and SubString(IsNull(P.ValDays,'1111111'),DATEPART(dw,@BegDate),1)=1
  and ((P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null))    
  and isnull(P.PriceCode,'')=''   
  and P.SaleAdlPrice>0
  and E.Type=0 
  and
   (
     not Exists(Select RecID From ExcursionDates Where Excursion=E.Code and Market=P.Market)
     or
     Exists(Select RecID From ExcursionDates 
            Where Excursion=E.Code and Market=P.Market
            and (Location Is Null or isnull(P.Location,0)=Location)
            and SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@BegDate),1)=1)
   )
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "DepLocation", DbType.Int32, criteria.ArrCity);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, criteria.CheckIn);
                db.AddInParameter(dbCommand, "ResDate", DbType.DateTime, DateTime.Today);
                db.AddInParameter(dbCommand, "GrpPax", DbType.Int32, criteria.RoomsInfo.FirstOrDefault().Adult + criteria.RoomsInfo.FirstOrDefault().Child);
                db.AddInParameter(dbCommand, "PackID", DbType.Int32, packID);
                db.AddInParameter(dbCommand, "HasHotel", DbType.Boolean, hasHotel);
                db.AddInParameter(dbCommand, "HasFlight", DbType.Boolean, hasFlight);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ExcursionSearchResult record = new ExcursionSearchResult();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, 36, VersionControl.Equality.gt))
                            record.CheckIn = Conversion.getDateTimeOrNull(R["ExcDate"]);
                        else
                            record.CheckIn = criteria.CheckIn;
                        record.Location = Conversion.getInt32OrNull(R["Location"]);
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        record.SaleAdl = Conversion.getDecimalOrNull(R["SaleAdl"]);
                        record.SaleChdG1 = Conversion.getDecimalOrNull(R["SaleChdG1"]);
                        record.SaleChdG2 = Conversion.getDecimalOrNull(R["SaleChdG2"]);
                        record.SaleChdG3 = Conversion.getDecimalOrNull(R["SaleChdG3"]);
                        record.ChdG1MaxAge = Conversion.getDecimalOrNull(R["ChdG1MaxAge"]);
                        record.ChdG2MaxAge = Conversion.getDecimalOrNull(R["ChdG2MaxAge"]);
                        record.ChdG3MaxAge = Conversion.getDecimalOrNull(R["ChdG3MaxAge"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.PriceCode = Conversion.getStrOrNull(R["PriceCode"]);
                        record.TransFromTime = Conversion.getDateTimeOrNull(R["TransFromTime"]);
                        record.TransToTime = Conversion.getDateTimeOrNull(R["TransToTime"]);
                        record.FreeFromTime = Conversion.getInt32OrNull(R["FreeFromTime"]);
                        record.FreeToTime = Conversion.getInt32OrNull(R["FreeToTime"]);
                        record.PriceType = Conversion.getStrOrNull(R["PriceType"]);

                        records.Add(calcSalePrice(UserData, record, criteria));
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ExcursionSearchResult> searchExcursion(User UserData, ExcursionSearchCriteria criteria, ref string errorMsg)
        {
            List<ExcursionSearchResult> records = new List<ExcursionSearchResult>();
            string tsql = string.Empty;
            if (criteria.Type == ExcursionType.Transit)
            {
                tsql =
                #region For Transit
 @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFIRST 1
Select E.Code,E.Name,NameL=isnull(dbo.FindLocalName(E.NameLID,@market),E.Name),
 P.Location,LocationName=L.Name,LocationNameL=isnull(dbo.FindLocalName(L.NameLID,@market),L.Name),
 SaleAdl=P.SaleAdlPrice,SaleChdG1=0,SaleChdG2=P.SaleChdG1Price,SaleChdG3=P.SaleChdG2Price,
 ChdG1MaxAge=FreeMaxAge,ChdG2MaxAge=ChdG1MaxAge,ChdG3MaxAge=ChdG2MaxAge,SaleCur=SaleCur,T.PriceCode,
 TransFromTime,TransToTime,FreeFromTime,FreeToTime,T.TimeTableID";
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050018155"))
                    tsql +=
@",P.VehicleCatID,VehicleCatName=Vc.Name,P.PriceType
";
                tsql +=
@"
From Excursion E
Join ExcursionPrice P On  P.Excursion=E.Code
Left Join Location L On L.RecID=P.Location
";
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050018155"))
                    tsql +=
@"
Left Join VehicleCategory Vc ON P.VehicleCatID=Vc.RecID
";
                tsql +=
@"
Outer Apply
(
 Select PriceCode,TransFromTime,TransToTime,FreeFromTime,FreeToTime,TimeTableID=RecID
 From ExcursionDepTime1 T1 
 Where T1.Excursion=E.Code and T1.Market=P.Market
       and dbo.Timeonly(@TransTime) between dbo.Timeonly(TransFromTime) and dbo.Timeonly(TransToTime)
       and ((@FreeTime between FreeFromTime And FreeToTime) or @FreeTime>=FreeFromTime)
) T
Left Join GrpAgencyDet AG on AG.Groups=P.AgencyGrp
Where P.Market=@Market
  and isnull(E.B2BPub,0)=1
  and (IsNull(P.Location,0)=@DepLocation or IsNull(P.Location,0)=0)
  and @BegDate between P.BegDate and P.EndDate
  and @ResDate between IsNull(P.SaleBegDate,@ResDate) and IsNull(P.SaleEndDate,@ResDate)
  and @GrpPax between IsNull(P.FromPax,0) and IsNull(P.ToPax,999)
  and SubString(IsNull(P.ValDays,'1111111'),DATEPART(dw,@BegDate),1)=1
  and ((P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null))  
  and P.PriceCode=T.PriceCode --for Transit tour
  and P.PriceCode<>'' --for transit
  and isnull(P.ExcurPackID,0)=0
  and E.EstTime <= @FreeTime
  --and Groups is null --for regular
  and P.SaleAdlPrice>0
  and E.Type=1 --1:regular    
  and IsNull(P.ValidHotel,0)=0
  and IsNull(P.ValidFlight,0)=0
  and
   (
     not Exists(Select RecID From ExcursionDates Where Excursion=E.Code and Market=P.Market)
     or
     Exists(Select RecID From ExcursionDates 
            Where Excursion=E.Code and Market=P.Market
            and (Location Is Null or isnull(P.Location,0)=Location)
            and SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@BegDate),1)=1)
   )   
";
                #endregion
            }
            else
                if (criteria.Type == ExcursionType.Regular)
                {
                    tsql =
                    #region For Regular
 @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFIRST 1
Select E.Code,E.Name,NameL=isnull(dbo.FindLocalName(E.NameLID,@market),E.Name),
 P.Location,LocationName=L.Name,LocationNameL=isnull(dbo.FindLocalName(L.NameLID,@market),L.Name),
 SaleAdl=P.SaleAdlPrice,SaleChdG1=0,SaleChdG2=P.SaleChdG1Price,SaleChdG3=P.SaleChdG2Price,
 ChdG1MaxAge=FreeMaxAge,ChdG2MaxAge=ChdG1MaxAge,ChdG3MaxAge=ChdG2MaxAge,SaleCur=SaleCur,PriceCode='',
 TransFromTime=null,TransToTime=null,FreeFromTime=null,FreeToTime=null,TimeTableID=null";
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050018155"))
                        tsql +=
    @",P.VehicleCatID,VehicleCatName=Vc.Name,P.PriceType
";
                    tsql +=
    @"
From Excursion E
Join ExcursionPrice P On  P.Excursion=E.Code
Left Join Location L On L.RecID=P.Location
Left Join GrpAgencyDet AG on AG.Groups=P.AgencyGrp
";
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050018155"))
                        tsql +=
    @"
Left Join VehicleCategory Vc ON P.VehicleCatID=Vc.RecID
";
                    tsql +=
    @"
Where P.Market=@Market
  and isnull(E.B2BPub,0)=1
  and (IsNull(P.Location,0)=@DepLocation or IsNull(P.Location,0)=0)
  and @BegDate between P.BegDate and P.EndDate
  and @ResDate between IsNull(P.SaleBegDate,@ResDate) and IsNull(P.SaleEndDate,@ResDate)
  and @GrpPax between IsNull(P.FromPax,0) and IsNull(P.ToPax,999)
  and SubString(IsNull(P.ValDays,'1111111'),DATEPART(dw,@BegDate),1)=1
  and ((P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null))    
  and isnull(P.PriceCode,'')='' 
  and isnull(P.ExcurPackID,0)=0
  --and Groups is null 
  and P.SaleAdlPrice>0
  and E.Type=0 
  and IsNull(P.ValidHotel,0)=0
  and IsNull(P.ValidFlight,0)=0
  and
   (
     not Exists(Select RecID From ExcursionDates Where Excursion=E.Code and Market=P.Market)
     or
     Exists(Select RecID From ExcursionDates 
            Where Excursion=E.Code and Market=P.Market
            and (Location Is Null or isnull(P.Location,0)=Location)
            and SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@BegDate),1)=1)
   )
";
                    #endregion
                }
                else
                    if (criteria.Type == ExcursionType.Private)
                    {
                        tsql =
                        #region For Private
 @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFIRST 1
Select E.Code,E.Name,NameL=isnull(dbo.FindLocalName(E.NameLID,@market),E.Name),
 P.Location,LocationName=L.Name,LocationNameL=isnull(dbo.FindLocalName(L.NameLID,@market),L.Name),
 SaleAdl=P.SaleAdlPrice,SaleChdG1=0,SaleChdG2=P.SaleChdG1Price,SaleChdG3=P.SaleChdG2Price,
 ChdG1MaxAge=FreeMaxAge,ChdG2MaxAge=ChdG1MaxAge,ChdG3MaxAge=ChdG2MaxAge,SaleCur=SaleCur,PriceCode='',
 TransFromTime=null,TransToTime=null,FreeFromTime=null,FreeToTime=null,TimeTableID=null";
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050018155"))
                            tsql +=
        @",P.VehicleCatID,VehicleCatName=Vc.Name,P.PriceType
";
                        tsql +=
        @"
From Excursion E
Join ExcursionPrice P On P.Excursion=E.Code
Left Join Location L On L.RecID=P.Location
Left Join GrpAgencyDet AG on AG.Groups=P.AgencyGrp
";
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050018155"))
                            tsql +=
        @"
Left Join VehicleCategory Vc ON P.VehicleCatID=Vc.RecID
";
                        tsql +=
        @"
Where P.Market=@Market
  and isnull(E.B2BPub,0)=1
  and (IsNull(P.Location,0)=@DepLocation or IsNull(P.Location,0)=0)
  and @BegDate between P.BegDate and P.EndDate
  and @ResDate between IsNull(P.SaleBegDate,@ResDate) and IsNull(P.SaleEndDate,@ResDate)
  and @GrpPax between IsNull(P.FromPax,0) and IsNull(P.ToPax,999)
  and SubString(IsNull(P.ValDays,'1111111'),DATEPART(dw,@BegDate),1)=1
  and ((P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null))    
  and isnull(P.PriceCode,'')='' 
  and isnull(P.ExcurPackID,0)=0
  --and Groups is null 
  and P.SaleAdlPrice>0
  and E.Type=2 
  and IsNull(P.ValidHotel,0)=0
  and IsNull(P.ValidFlight,0)=0
  and
   (
     not Exists(Select RecID From ExcursionDates Where Excursion=E.Code and Market=P.Market)
     or
     Exists(Select RecID From ExcursionDates 
            Where Excursion=E.Code and Market=P.Market
            and (Location Is Null or isnull(P.Location,0)=Location)
            and SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@BegDate),1)=1)
   )
";
                        #endregion
                    }
            if (!string.IsNullOrEmpty(criteria.Category))
                tsql +=
@"
  and (isnull(E.Category,'')='' Or E.Category=@category)
";
            tsql +=
@"
 Order By Location Desc,P.FromPax Desc,P.ToPax Desc,P.Agency Desc,P.AgencyGrp Desc,P.RecID Desc, NetAdlPrice
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "DepLocation", DbType.Int32, criteria.ArrCity);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, criteria.CheckIn);
                db.AddInParameter(dbCommand, "ResDate", DbType.DateTime, DateTime.Today);
                db.AddInParameter(dbCommand, "GrpPax", DbType.Int32, criteria.RoomsInfo.FirstOrDefault().Adult + criteria.RoomsInfo.FirstOrDefault().Child);
                if (criteria.Type == ExcursionType.Transit)
                {
                    db.AddInParameter(dbCommand, "TransTime", DbType.DateTime, criteria.Hour);
                    db.AddInParameter(dbCommand, "FreeTime", DbType.Int32, criteria.Duration);
                }
                if (!string.IsNullOrEmpty(criteria.Category))
                    db.AddInParameter(dbCommand, "category", DbType.String, criteria.Category);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    int refNo = 0;
                    while (R.Read())
                    {
                        refNo++;
                        ExcursionSearchResult rec = new ExcursionSearchResult();
                        rec.RefNo = refNo;
                        rec.Code = Conversion.getStrOrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                        rec.CheckIn = criteria.CheckIn;
                        rec.Location = Conversion.getInt32OrNull(R["Location"]);
                        rec.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        rec.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        rec.SaleAdl = Conversion.getDecimalOrNull(R["SaleAdl"]);
                        rec.SaleChdG1 = Conversion.getDecimalOrNull(R["SaleChdG1"]);
                        rec.SaleChdG2 = Conversion.getDecimalOrNull(R["SaleChdG2"]);
                        rec.SaleChdG3 = Conversion.getDecimalOrNull(R["SaleChdG3"]);
                        rec.ChdG1MaxAge = Conversion.getDecimalOrNull(R["ChdG1MaxAge"]);
                        rec.ChdG2MaxAge = Conversion.getDecimalOrNull(R["ChdG2MaxAge"]);
                        rec.ChdG3MaxAge = Conversion.getDecimalOrNull(R["ChdG3MaxAge"]);
                        rec.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        rec.PriceCode = Conversion.getStrOrNull(R["PriceCode"]);
                        rec.TransFromTime = Conversion.getDateTimeOrNull(R["TransFromTime"]);
                        rec.TransToTime = Conversion.getDateTimeOrNull(R["TransToTime"]);
                        rec.FreeFromTime = Conversion.getInt32OrNull(R["FreeFromTime"]);
                        rec.FreeToTime = Conversion.getInt32OrNull(R["FreeToTime"]);
                        rec.PriceType = Conversion.getStrOrNull(R["PriceType"]);
                        if (criteria.Type == ExcursionType.Transit)
                        {
                            rec.ExcTransTime = criteria.Hour;
                            rec.ExcFreeTime = criteria.Duration;
                            rec.ExcTimeID = Conversion.getInt32OrNull(R["TimeTableID"]);
                        }
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050018155"))
                        {
                            rec.VehicleCatID = Conversion.getInt32OrNull(R["VehicleCatID"]);
                            rec.VehicleCatName = Conversion.getStrOrNull(R["VehicleCatName"]);
                        }

                        records.Add(calcSalePrice(UserData, rec, criteria));
                    }
                    return records.OrderBy(o => o.TotalPrice).ToList<ExcursionSearchResult>();
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ExcursionSearchResult calcSalePrice(User UserData, ExcursionSearchResult rec, ExcursionSearchCriteria criteria)
        {
            decimal? totalPrice = (decimal)0;
            Int16 Adl = criteria.RoomsInfo.FirstOrDefault().Adult;
            Int16 Chd = criteria.RoomsInfo.FirstOrDefault().Child;
            decimal? chd1Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd1Age);
            decimal? chd2Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd2Age);
            decimal? chd3Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd3Age);
            decimal? chd4Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd4Age);

            int totalChd = 0;
            List<int> chdAgeList = new List<int>();
            int grp1 = 0;
            int grp2 = 0;
            int grp3 = 0;
            if (criteria.RoomsInfo.FirstOrDefault().Child != 0 && rec.PriceType!="S")
            {
                if (criteria.RoomsInfo.FirstOrDefault().Chd1Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value >= 0)
                    chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value);
                if (criteria.RoomsInfo.FirstOrDefault().Chd2Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value >= 0)
                    chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value);
                if (criteria.RoomsInfo.FirstOrDefault().Chd3Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value >= 0)
                    chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value);
                if (criteria.RoomsInfo.FirstOrDefault().Chd4Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value >= 0)
                    chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value);

                if (rec.ChdG1MaxAge.HasValue)
                {
                    grp1 += chdAgeList.Where(w => w <= rec.ChdG1MaxAge).Count();
                    totalPrice += ((rec.SaleChdG1.HasValue ? rec.SaleChdG1.Value : (decimal)0)) * grp1;
                    totalChd += grp1;
                }
                if (rec.ChdG1MaxAge.HasValue && rec.ChdG2MaxAge.HasValue)
                {
                    grp2 += chdAgeList.Where(w => w > rec.ChdG1MaxAge && w <= rec.ChdG2MaxAge.Value).Count();
                    totalPrice += ((rec.SaleChdG2.HasValue ? rec.SaleChdG2.Value : (decimal)0)) * grp2;
                    totalChd += grp2;
                }
                if (rec.ChdG2MaxAge.HasValue && rec.ChdG3MaxAge.HasValue)
                {
                    grp3 += chdAgeList.Where(w => w > rec.ChdG2MaxAge && w <= rec.ChdG3MaxAge.Value).Count();
                    totalPrice += ((rec.SaleChdG3.HasValue ? rec.SaleChdG3.Value : (decimal)0)) * grp3;
                    totalChd += grp3;
                }
            }
            totalPrice += rec.PriceType.Equals("P") ? rec.SaleAdl * Adl : rec.SaleAdl;
            if (rec.PriceType != "S")
            {
                if (totalChd < Chd)
                {
                    totalPrice += rec.SaleAdl * (Chd - totalChd);
                }
                else if (totalChd > Chd)
                {
                    totalPrice += rec.SaleAdl * (Chd - totalChd);
                }
            }
            rec.Calculated = true;
            string errorMsg = string.Empty;
            if (criteria.CurControl)
                if (!string.Equals(criteria.CurrentCur, rec.SaleCur))
                {
                    decimal? price = new TvBo.Common().Exchange(UserData.Market, criteria.CheckIn, rec.SaleCur, criteria.CurrentCur, totalPrice, true, ref errorMsg);
                    if (string.IsNullOrEmpty(errorMsg) && price.HasValue)
                    {
                        rec.SaleCur = criteria.CurrentCur;
                        totalPrice = price;
                    }
                }

            rec.TotalPrice = totalPrice;

            return rec;
        }

        public ResDataRecord getExcursionReservation(User UserData, ExcursionSearchCriteria criteria, ExcursionSearchResult bookExcursion, ref string errorMsg)
        {
            Int16 adult = Conversion.getInt16OrNull(criteria.RoomsInfo.FirstOrDefault().Adult).HasValue ? Conversion.getInt16OrNull(criteria.RoomsInfo.FirstOrDefault().Adult).Value : Convert.ToInt16(2);
            Int16 child = 0;
            Int16 infant = 0;

            #region calc child and infant count
            if (criteria.RoomsInfo.FirstOrDefault().Chd1Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value <= (bookExcursion.ChdG1MaxAge.HasValue ? bookExcursion.ChdG1MaxAge.Value : ((decimal)199 / 100)))
                infant++;
            else
                if (criteria.RoomsInfo.FirstOrDefault().Chd1Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value > (bookExcursion.ChdG1MaxAge.HasValue ? bookExcursion.ChdG1MaxAge.Value : ((decimal)199 / 100)))
                    child++;

            if (criteria.RoomsInfo.FirstOrDefault().Chd2Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value <= (bookExcursion.ChdG1MaxAge.HasValue ? bookExcursion.ChdG1MaxAge.Value : ((decimal)199 / 100)))
                infant++;
            else
                if (criteria.RoomsInfo.FirstOrDefault().Chd2Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value >= (bookExcursion.ChdG1MaxAge.HasValue ? bookExcursion.ChdG1MaxAge.Value : ((decimal)199 / 100)))
                    child++;

            if (criteria.RoomsInfo.FirstOrDefault().Chd3Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value <= (bookExcursion.ChdG1MaxAge.HasValue ? bookExcursion.ChdG1MaxAge.Value : ((decimal)199 / 100)))
                infant++;
            else
                if (criteria.RoomsInfo.FirstOrDefault().Chd3Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value >= (bookExcursion.ChdG1MaxAge.HasValue ? bookExcursion.ChdG1MaxAge.Value : ((decimal)199 / 100)))
                    child++;

            if (criteria.RoomsInfo.FirstOrDefault().Chd4Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value <= (bookExcursion.ChdG1MaxAge.HasValue ? bookExcursion.ChdG1MaxAge.Value : ((decimal)199 / 100)))
                infant++;
            else
                if (criteria.RoomsInfo.FirstOrDefault().Chd4Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value >= (bookExcursion.ChdG1MaxAge.HasValue ? bookExcursion.ChdG1MaxAge.Value : ((decimal)199 / 100)))
                    child++;
            #endregion

            bool CreateChildAge = true;
            if (UserData.WebService)
                CreateChildAge = true;
            else
            {
                object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
                CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
            }

            ResDataRecord ResData = new ResDataRecord();
            ResData.SelectBook = new ExcursionSearch().fillSelectBook(UserData, bookExcursion, criteria.RoomCount, criteria.RoomsInfo, adult, child, infant, ref errorMsg);
            ResData = new ExcursionSearch().bookExcurson(UserData, ResData, bookExcursion, CreateChildAge, bookExcursion.PriceCode, criteria.FlightNo, ref errorMsg);

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            {
                ResMainRecord resMain = ResData.ResMain;
                resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            }

            return ResData;
        }

        public List<SearchResult> fillSelectBook(User UserData, ExcursionSearchResult bookExcursion, int roomCount, List<SearchCriteriaRooms> roomInfo, Int16 Adult, Int16 Child, Int16 Infant, ref string errorMsg)
        {
            List<SearchResult> SelectBook = new List<SearchResult>();
            SearchResult row = new SearchResult();
            #region Age Group
            List<AgeGroup> ageGroups = new List<AgeGroup>();
            int roomCnt = 0;
            int refNo = 0;
            roomCnt++;
            refNo++;
            ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Adult, GroupNo = roomCnt, RefNo = refNo, Unit = Adult, Age = null, DateOfBirth = null });
            if (Infant > 0)
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Infant, GroupNo = roomCnt, RefNo = refNo, Unit = Infant, Age = Convert.ToDecimal(199.0 / 100), DateOfBirth = null });
            if (Child > 0)
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = Child, Age = Convert.ToDecimal(1299.0 / 100), DateOfBirth = null });
            #endregion
            row.CatPackID = 0;
            row.RefNo = 1;
            row.ARecNo = 0;
            row.PRecNo = 0;
            row.CheckIn = bookExcursion.CheckIn;
            row.CheckOut = bookExcursion.CheckIn;
            row.Night = 0;
            row.NetCur = bookExcursion.SaleCur;
            row.ChdG1Age1 = 0;
            row.ChdG1Age2 = bookExcursion.ChdG1MaxAge.HasValue ? bookExcursion.ChdG1MaxAge.Value : Convert.ToDecimal(199.0 / 100);
            row.ChdG2Age1 = Conversion.getInt32OrNull(row.ChdG1Age2);
            row.ChdG2Age2 = bookExcursion.ChdG2MaxAge.HasValue ? bookExcursion.ChdG2MaxAge.Value : Convert.ToDecimal(1299.0 / 100);
            row.ChdG3Age1 = bookExcursion.ChdG3MaxAge.HasValue ? Conversion.getInt32OrNull(row.ChdG2Age2) : 0;
            row.ChdG3Age2 = bookExcursion.ChdG3MaxAge.HasValue ? bookExcursion.ChdG2MaxAge.Value : 0;
            row.ChdG4Age1 = 0;
            row.ChdG4Age2 = 0;
            row.SaleCur = bookExcursion.SaleCur;
            row.HolPack = string.Empty;
            row.Operator = UserData.Operator;
            row.Market = UserData.Market;
            row.FlightClass = "";
            row.DepCity = bookExcursion.Location;
            row.ArrCity = bookExcursion.Location;
            row.HAdult = Adult;
            row.HChdAgeG1 = Infant;
            row.HChdAgeG2 = Child;
            row.HChdAgeG3 = 0;
            row.HChdAgeG4 = 0;
            if (Infant > 0)
                row.Child1Age = row.ChdG1Age2.HasValue ? Convert.ToInt16(Math.Truncate(row.ChdG1Age2.Value)) : Convert.ToInt16(1);
            if (Child > 0)
                row.Child2Age = row.ChdG2Age2.HasValue ? Convert.ToInt16(Math.Truncate(row.ChdG2Age2.Value)) : Convert.ToInt16(15);
            row.DepCityName = new Locations().getLocationName(UserData, bookExcursion.Location.Value, ref errorMsg);
            row.ArrCityName = new Locations().getLocationName(UserData, bookExcursion.Location.Value, ref errorMsg);
            row.DepSeat = 0;
            row.RetSeat = 0;
            row.StopSaleGuar = 0;
            row.StopSaleStd = 0;
            row.CurrentCur = UserData.SaleCur;
            row.AgeGroupList = ageGroups;
            row.PLCur = bookExcursion.SaleCur;
            SelectBook.Add(row);

            return SelectBook;
        }

        public ResDataRecord bookExcurson(User UserData, ResDataRecord _ResData, ExcursionSearchResult bookRow, bool CreateChildAge, string priceCode, string ResNote, ref string errorMsg)
        {

            ResDataRecord ResData = new ResDataRecord();
            ResData = new ResTables().copyData(_ResData);

            int BookID = 1;

            ResData.Title = new TvBo.Common().getTitle(ref errorMsg);

            #region Create ResMain
            ResData = new Reservation().GenerateResMain(UserData, ResData, SearchType.OnlyExcursion, string.Empty, ref errorMsg);
            ResMainRecord resMain = ResData.ResMain;
            resMain.ResNote = ResNote;
            resMain.SaleCur = bookRow.SaleCur;
            if (errorMsg != "")
                return null;
            #endregion

            #region Create ResCust
            ResData.ResCust = new List<ResCustRecord>();
            for (int i = 0; i < ResData.SelectBook.Count; i++)
            {
                //if (ResData.SelectBook[i].RefNo != null)
                BookID = ResData.SelectBook[i].RefNo;
                //else
                //    BookID = i + 1;
                ResData = new Reservation().GenerateTourists(UserData, ResData, BookID, ref errorMsg);
                if (errorMsg != "")
                    return null;
            }

            ResData.ResCust[0].Leader = "Y";
            #endregion Create ResCust


            ResData.ResService = new List<ResServiceRecord>();
            ResData.ResCon = new List<ResConRecord>();
            ResData.ResServiceExt = new List<ResServiceExtRecord>();
            ResData.ResConExt = new List<ResConExtRecord>();
            ResData.ResCustPrice = new List<ResCustPriceRecord>();
            ResData.ResCustInfo = new List<ResCustInfoRecord>();
            ResData.ResFlightDetail = new List<ResFlightDetailRecord>();
            ResData.ResSupDis = new List<ResSupDisRecord>();

            List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
            foreach (SelectCustRecord s in SelectCust)
                s.Selected = true;

            if (bookRow.PackageDetails != null && bookRow.PackageDetails.Count > 0)
            {
                foreach (ExcursionSearchResult row in bookRow.PackageDetails)
                {
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, bookRow.CheckIn.Value, bookRow.CheckIn.Value, "EXCURSION", row.Code,
                                    string.Empty, string.Empty, string.Empty, string.Empty, 1, 0, 1, row.Location, null, string.Empty, string.Empty, 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, bookRow.VehicleCatID, null);
                }
            }
            else
            {
                ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, bookRow.CheckIn.Value, bookRow.CheckIn.Value, "EXCURSION", bookRow.Code,
                                    string.Empty, string.Empty, string.Empty, string.Empty, 1, 0, 1, bookRow.Location, null, string.Empty, string.Empty, 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, bookRow.ExcTransTime, bookRow.ExcFreeTime, bookRow.ExcTimeID, bookRow.VehicleCatID, null);
            }
            List<ResServiceRecord> resServcies = ResData.ResService;
            ResServiceRecord service = resServcies.FirstOrDefault();
            service.PriceCode = priceCode;
            if (!string.IsNullOrEmpty(errorMsg))
            {
                return ResData;
            }

            List<ResServiceRecord> flights = ResData.ResService;
            int ServiceID = -1;// ResData.ResService.LastOrDefault() != null ? ResData.ResService.LastOrDefault().RecID : -1;
            //ResServiceRecord CompResServ = flights.Find(f => f.RecID == ServiceID);


            for (int i = 0; i < ResData.SelectBook.Count; i++)
            {
                BookID = ResData.SelectBook[i].RefNo;
                List<plResServiceRecord> CompService = new List<plResServiceRecord>();
                CompService = new Reservation().GetCompulsoryResService(UserData, ResData, BookID, ref errorMsg);

                if (CompService != null)
                {
                    foreach (plResServiceRecord row in CompService)
                    {
                        #region Create ResService
                        int RowCount = 0;
                        List<ResServiceRecord> compResServ = (from q in ResData.ResService
                                                              where q.Service == row.Service && q.ServiceType == row.ServiceType
                                                              select q).ToList<ResServiceRecord>();
                        List<ResServiceRecord> ResService = ResData.ResService;
                        List<ResConRecord> ResCon = ResData.ResCon;
                        ResServiceRecord resService = ResService.Find(f => f.ServiceType == row.ServiceType && f.Service == row.Service);
                        if (compResServ.Count() > 0)
                            RowCount = compResServ.Count();

                        ServiceID = -1;

                        if (RowCount > 0)
                        {
                            SelectCust = new List<SelectCustRecord>();
                            var Cst = from Rc in ResData.ResCust
                                      where Rc.BookID == BookID && Rc.Status == 0
                                      select Rc;
                            if (SelectCust == null)
                            {
                                SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);

                                for (int selectCustCount = 0; selectCustCount < SelectCust.Count; selectCustCount++)
                                    if (Cst.Where(w => w.CustNo == SelectCust[selectCustCount].CustNo).Count() > 0)
                                        SelectCust[selectCustCount].Selected = true;
                            }
                            else
                                if (SelectCust.Count < 1)
                                {
                                    SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);

                                    for (int selectCustCount = 0; selectCustCount < SelectCust.Count; selectCustCount++)
                                        if (Cst.Where(w => w.CustNo == SelectCust[selectCustCount].CustNo).Count() > 0)
                                            SelectCust[selectCustCount].Selected = true;
                                }
                            int Unit = 1;

                            var resControl = from q1 in ResData.ResService
                                             join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                             join q3 in SelectCust on q2.CustNo equals q3.CustNo
                                             where q3.Selected == true &&
                                                q1.ServiceType == row.ServiceType &&
                                                q1.Service == row.Service
                                             select q1;

                            if (resControl.Count() > 0)
                            {
                                //errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "DuplicateService").ToString();
                            }
                            else
                            {
                                Unit = Convert.ToInt16(Convert.ToInt16(Cst.Where(w => w.Title < 6 && w.BookID == BookID && w.Status == 0).Count()) + Cst.Where(w => w.Title > 5 && w.BookID == BookID && w.Status == 0).Count());
                                resService.Unit += Convert.ToInt16(Unit);
                                resService.Adult += Convert.ToInt16(Cst.Where(w => w.Title < 6 && w.BookID == BookID && w.Status == 0).Count());
                                resService.Child += Convert.ToInt16(Cst.Where(w => w.Title > 5 && w.BookID == BookID && w.Status == 0).Count());
                                ServiceID = resService.RecID;
                                ResCon = new Reservation().AddResCon(UserData, ResData, null, BookID, ServiceID, true, ref errorMsg);
                            }
                            if (errorMsg != "")
                            {
                                //errorMsg = GetGlobalResourceObject(UserData.Rows[0]["Lang"].ToString(), "ReservationMsg31").ToString() + " ( " + errorMsg + " )";
                                return ResData;
                            }
                        }
                        else
                        {
                            ResData.ResService = new Reservation().AddResService(UserData, ResData, row, BookID, SearchType.OnlyFlightSearch, true, false, false, null, ref errorMsg);
                            ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                            if (errorMsg != "")
                                return ResData;
                            ResData.ResCon = new Reservation().AddResCon(UserData, ResData, null, BookID, ServiceID, true, ref errorMsg);
                            if (errorMsg != "")
                                return ResData;
                        }
                        #endregion Create ResService

                        #region Create Compulsory And Include Extra Service

                        List<ResConExtRecord> resConExt = ResData.ResConExt;
                        bool seperate = false;
                        if (row.ResSeparate != null)
                            seperate = row.ResSeparate.Value;
                        ResData.ResServiceExt = new Reservation().AddResServiceExt(UserData, ResData, (seperate || RowCount == 0) ? ResData.ResService[ResData.ResService.Count - 1] : compResServ.FirstOrDefault(), ref resConExt, Convert.ToInt32(ResData.SelectBook[i].RefNo), -1, ServiceID, null, ref errorMsg);
                        //if (errorMsg != "") return ResData;
                        #endregion Create Compulsory And Include Extra Service
                    }
                }
            }

            List<ResServiceRecord> resServiceList = ResData.ResService;
            foreach (ResServiceRecord row in resServiceList)
                row.ExcludeService = false;
            List<ResServiceExtRecord> resServiceExtList = ResData.ResServiceExt;
            foreach (ResServiceExtRecord row in resServiceExtList)
                row.ExcludeService = false;

            #region Calculate ResCustPrice

            string ResNo = ResData.ResMain.ResNo;
            ResNo = ResNo.Length > 10 ? ResNo.Substring(0, 10) : ResNo;

            StringBuilder CalcStrSql = new Reservation().BuildCalcSqlString(ResData);

            CalcStrSql.Append("Declare  @PassEB bit \n");
            CalcStrSql.Append("Declare  @ErrCode SmallInt \n");
            CalcStrSql.Append("Declare @Supplier varchar(10) \n");
            CalcStrSql.Append("exec dbo.usp_Calc_Res_Price '" + ResNo + "', Default, Default, @PassEB OutPut, @ErrCode OutPut, 1 \n");
            CalcStrSql.Append("Select * From #ResMain \n");
            CalcStrSql.Append("Select * From #ResService \n");
            CalcStrSql.Append("Select * From #ResServiceExt \n");
            CalcStrSql.Append("Select * From #ResCust \n");
            CalcStrSql.Append("Select * From #ResCustPrice \n");
            CalcStrSql.Append("Select * From #ResSupDis \n");
            CalcStrSql.Append("Select ErrorCode=@ErrCode \n");
            CalcStrSql.Append("Select * From #CalcErrTbl \n");

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(CalcStrSql.ToString());
            DataSet ds;
            try
            {
                ds = db.ExecuteDataSet(dbCommand);
                if (ds != null)
                {
                    ds.Tables[0].TableName = "ResMain";
                    ds.Tables[1].TableName = "ResService";
                    ds.Tables[2].TableName = "ResServiceExt";
                    ds.Tables[3].TableName = "ResCust";
                    ds.Tables[4].TableName = "ResCustPrice";
                    ds.Tables[5].TableName = "ResSupDis";
                    ds.Tables[6].TableName = "ErrorTable";
                    ds.Tables[ds.Tables.Count - 1].TableName = "CalcErrTbl";
                }
                else
                {
                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationPriceNotCalculated").ToString();
                    ds = null;
                    return ResData;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                ds = null;
                return ResData;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
            #endregion Calculate ResCustPrice

            int ErrorCode = ds.Tables["ErrorTable"] != null ? Convert.ToInt32(ds.Tables["ErrorTable"].Rows[0]["ErrorCode"].ToString()) : 0;
            if (ErrorCode != 0)
            {
                errorMsg = new ResCalcError().calcError(ErrorCode, ResData);
                return ResData;
            }
            else
                if (ds.Tables["CalcErrTbl"] != null && ds.Tables["CalcErrTbl"].Rows.Count > 0)
                {
                    ErrorCode = ds.Tables["CalcErrTbl"] != null ? Convert.ToInt32(ds.Tables["CalcErrTbl"].Rows[0]["ErrCode"].ToString()) : 0;
                    if (ErrorCode != 0)
                    {
                        errorMsg = new ResCalcError().calcError(ErrorCode, ResData);
                        return ResData;
                    }
                }

            ResData = new Reservation().SetCalcDataToResData(UserData, ResData, ds, false, ref errorMsg);

            if (!CreateChildAge)
            {
                foreach (ResCustRecord row in ResData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
            }

            return ResData;
        }

        public List<calendarColor> getExcursionPackDates(User UserData, ResDataRecord ResData, int? Location, int? packID, ref string errorMsg)
        {
            List<calendarColor> dates = new List<calendarColor>();

            List<string> records = new List<string>();

            string tsql =
@"
Select D.BegDate,D.EndDate,DayofWeek 
From ExcursionDates (NOLOCK) D
Join ExcursionPackDet (NOLOCK) E ON D.Excursion=E.Excursion
Where E.PackID=@packID
    And D.Market=@plMarket
    And D.Location=@Location
    And D.BegDate<=@ResEndDate
    And D.EndDate>=@ResBegDate
Order By D.RecID Desc
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "packID", DbType.Int32, packID);
                db.AddInParameter(dbCommand, "plMarket", DbType.AnsiString, ResData.ResMain.PLMarket);
                db.AddInParameter(dbCommand, "Location", DbType.Int32, Location);
                db.AddInParameter(dbCommand, "ResEndDate", DbType.DateTime, ResData.ResMain.BegDate);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.DateTime, ResData.ResMain.EndDate);
                DataSet dbData = db.ExecuteDataSet(dbCommand);
                if (dbData != null && dbData.Tables[0] != null && dbData.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = dbData.Tables[0];
                    for (DateTime exdt = ResData.ResMain.BegDate.Value; exdt <= ResData.ResMain.EndDate.Value; exdt = exdt.AddDays(1))
                    {
                        int day = 0;
                        switch (exdt.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                day = 0;
                                break;
                            case DayOfWeek.Tuesday:
                                day = 1;
                                break;
                            case DayOfWeek.Wednesday:
                                day = 2;
                                break;
                            case DayOfWeek.Thursday:
                                day = 3;
                                break;
                            case DayOfWeek.Friday:
                                day = 4;
                                break;
                            case DayOfWeek.Saturday:
                                day = 5;
                                break;
                            case DayOfWeek.Sunday:
                                day = 6;
                                break;
                            default:
                                day = 0;
                                break;
                        }

                        if ((from d in dt.AsEnumerable()
                             where exdt >= d.Field<DateTime>("BegDate")
                             && exdt <= d.Field<DateTime>("EndDate")
                             && d.Field<string>("DayofWeek").ToString()[day] == '1'
                             select d).Count() > 0)
                            dates.Add(new calendarColor
                            {
                                Day = exdt.Day,
                                Month = exdt.Month,
                                Year = exdt.Year
                            });
                    }
                }
                return dates;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return dates;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }

}
