﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace TvBo
{
    [Serializable]
    public class WebAdminUser
    {
        public WebAdminUser()
        {
        }

        bool _isLogin;
        public bool IsLogin
        {
            get { return _isLogin; }
            set { _isLogin = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Password;
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        CultureInfo _ci;
        public CultureInfo Ci
        {
            get { return _ci; }
            set { _ci = value; }
        }
    }
}
