﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class TvSystem
    {
        public SupplierRecord getSupplier(string Market, string Code, ref string errorMsg)
        {
            SupplierRecord record = new SupplierRecord();
            string tsql = @"Select RecID, Code, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name), NameS, Status, Location, FirmName, 
	                            Address, AddrZip, AddrCity, AddrCountry,
	                            InvAddress, InvAddrZip, InvAddrCity, InvAddrCountry,
	                            TaxOffice, TaxAccNo, BlackList, MainOffice, 
	                            Bank1, Bank1AccNo, Bank1Curr, 
	                            Bank2, Bank2AccNo, Bank2Curr, 
	                            BossName, ContName, ACContacName, Phone1, Phone2, MobPhone, Fax1, Fax2,
	                            www, email1, email2, CreditLimit, Curr,  PayDay, PayMode, PayModeDay, PayType,
	                            Note, AllowSendRes, SendType, TAcCode, CIF, CapSocial, CapSocialCur, ConPas, 
	                            SendOptDate
                            From Supplier (NOLOCK)
                            Where Code=@Code";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);

                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        record.RecID = (int)rdr["RecID"];
                        record.Code = Conversion.getStrOrNull(rdr["Code"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.NameL = Conversion.getStrOrNull(rdr["NameL"]);
                        record.NameS = Conversion.getStrOrNull(rdr["NameS"]);
                        record.Status = Conversion.getStrOrNull(rdr["Status"]);
                        record.Location = Conversion.getInt32OrNull(rdr["Location"]);
                        record.FirmName = Conversion.getStrOrNull(rdr["FirmName"]);
                        record.Address = Conversion.getStrOrNull(rdr["Address"]);
                        record.AddrZip = Conversion.getStrOrNull(rdr["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(rdr["AddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(rdr["AddrCountry"]);
                        record.InvAddress = Conversion.getStrOrNull(rdr["InvAddress"]);
                        record.InvAddrZip = Conversion.getStrOrNull(rdr["InvAddrZip"]);
                        record.InvAddrCity = Conversion.getStrOrNull(rdr["InvAddrCity"]);
                        record.InvAddrCountry = Conversion.getStrOrNull(rdr["InvAddrCountry"]);
                        record.TaxOffice = Conversion.getStrOrNull(rdr["TaxOffice"]);
                        record.TaxAccNo = Conversion.getStrOrNull(rdr["TaxAccNo"]);
                        record.BlackList = Equals(rdr["BlackList"], "Y");
                        record.MainOffice = Conversion.getStrOrNull(rdr["MainOffice"]);
                        record.Bank1 = Conversion.getStrOrNull(rdr["Bank1"]);
                        record.Bank1AccNo = Conversion.getStrOrNull(rdr["Bank1AccNo"]);
                        record.Bank1Curr = Conversion.getStrOrNull(rdr["Bank1Curr"]);
                        record.Bank2 = Conversion.getStrOrNull(rdr["Bank2"]);
                        record.Bank2AccNo = Conversion.getStrOrNull(rdr["Bank2AccNo"]);
                        record.Bank2Curr = Conversion.getStrOrNull(rdr["Bank2Curr"]);
                        record.BossName = Conversion.getStrOrNull(rdr["BossName"]);
                        record.ContName = Conversion.getStrOrNull(rdr["ContName"]);
                        record.ACContacName = Conversion.getStrOrNull(rdr["ACContacName"]);
                        record.Phone1 = Conversion.getStrOrNull(rdr["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(rdr["Phone2"]);
                        record.MobPhone = Conversion.getStrOrNull(rdr["MobPhone"]);
                        record.Fax1 = Conversion.getStrOrNull(rdr["Fax1"]);
                        record.Fax2 = Conversion.getStrOrNull(rdr["Fax2"]);
                        record.www = Conversion.getStrOrNull(rdr["www"]);
                        record.Email1 = Conversion.getStrOrNull(rdr["Email1"]);
                        record.Email2 = Conversion.getStrOrNull(rdr["Email2"]);
                        record.CreditLimit = Conversion.getDecimalOrNull(rdr["CreditLimit"]);
                        record.Curr = Conversion.getStrOrNull(rdr["Curr"]);
                        record.PayDay = Conversion.getInt16OrNull(rdr["PayDay"]);
                        record.PayMode = Conversion.getInt16OrNull(rdr["PayMode"]);
                        record.PayModeDay = Conversion.getInt16OrNull(rdr["PayModeDay"]);
                        record.PayType = Conversion.getStrOrNull(rdr["PayType"]);
                        record.Note = Conversion.getStrOrNull(rdr["Note"]);
                        record.AllowSendRes = Conversion.getStrOrNull(rdr["AllowSendRes"]);
                        record.SendType = Conversion.getInt16OrNull(rdr["SendType"]);
                        record.TAcCode = Conversion.getStrOrNull(rdr["TAcCode"]);
                        record.CIF = Conversion.getStrOrNull(rdr["CIF"]);
                        record.CapSocial = Conversion.getDecimalOrNull(rdr["CapSocial"]);
                        record.CapSocialCur = Conversion.getStrOrNull(rdr["CapSocialCur"]);
                        record.ConPas = Conversion.getStrOrNull(rdr["ConPas"]);
                        record.SendOptDate = Conversion.getBoolOrNull(rdr["SendOptDate"]);
                    }
                }
                return record;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SupplierRecord> getSupplierList(string Market, ref string errorMsg)
        {
            List<SupplierRecord> records = new List<SupplierRecord>();
            string tsql = @"Select RecID, Code, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name), NameS, Status, Location, FirmName, 
	                            Address, AddrZip, AddrCity, AddrCountry,
	                            InvAddress, InvAddrZip, InvAddrCity, InvAddrCountry,
	                            TaxOffice, TaxAccNo, BlackList, MainOffice, 
	                            Bank1, Bank1AccNo, Bank1Curr, 
	                            Bank2, Bank2AccNo, Bank2Curr, 
	                            BossName, ContName, ACContacName, Phone1, Phone2, MobPhone, Fax1, Fax2,
	                            www, email1, email2, CreditLimit, Curr,  PayDay, PayMode, PayModeDay, PayType,
	                            Note, AllowSendRes, SendType, TAcCode, CIF, CapSocial, CapSocialCur, ConPas, 
	                            SendOptDate
                            From Supplier (NOLOCK) ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        SupplierRecord record = new SupplierRecord();
                        record.RecID = (int)rdr["RecID"];
                        record.Code = Conversion.getStrOrNull(rdr["Code"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.NameL = Conversion.getStrOrNull(rdr["NameL"]);
                        record.NameS = Conversion.getStrOrNull(rdr["NameS"]);
                        record.Status = Conversion.getStrOrNull(rdr["Status"]);
                        record.Location = Conversion.getInt32OrNull(rdr["Location"]);
                        record.FirmName = Conversion.getStrOrNull(rdr["FirmName"]);
                        record.Address = Conversion.getStrOrNull(rdr["Address"]);
                        record.AddrZip = Conversion.getStrOrNull(rdr["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(rdr["AddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(rdr["AddrCountry"]);
                        record.InvAddress = Conversion.getStrOrNull(rdr["InvAddress"]);
                        record.InvAddrZip = Conversion.getStrOrNull(rdr["InvAddrZip"]);
                        record.InvAddrCity = Conversion.getStrOrNull(rdr["InvAddrCity"]);
                        record.InvAddrCountry = Conversion.getStrOrNull(rdr["InvAddrCountry"]);
                        record.TaxOffice = Conversion.getStrOrNull(rdr["TaxOffice"]);
                        record.TaxAccNo = Conversion.getStrOrNull(rdr["TaxAccNo"]);
                        record.BlackList = Equals(rdr["BlackList"], "Y");
                        record.MainOffice = Conversion.getStrOrNull(rdr["MainOffice"]);
                        record.Bank1 = Conversion.getStrOrNull(rdr["Bank1"]);
                        record.Bank1AccNo = Conversion.getStrOrNull(rdr["Bank1AccNo"]);
                        record.Bank1Curr = Conversion.getStrOrNull(rdr["Bank1Curr"]);
                        record.Bank2 = Conversion.getStrOrNull(rdr["Bank2"]);
                        record.Bank2AccNo = Conversion.getStrOrNull(rdr["Bank2AccNo"]);
                        record.Bank2Curr = Conversion.getStrOrNull(rdr["Bank2Curr"]);
                        record.BossName = Conversion.getStrOrNull(rdr["BossName"]);
                        record.ContName = Conversion.getStrOrNull(rdr["ContName"]);
                        record.ACContacName = Conversion.getStrOrNull(rdr["ACContacName"]);
                        record.Phone1 = Conversion.getStrOrNull(rdr["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(rdr["Phone2"]);
                        record.MobPhone = Conversion.getStrOrNull(rdr["MobPhone"]);
                        record.Fax1 = Conversion.getStrOrNull(rdr["Fax1"]);
                        record.Fax2 = Conversion.getStrOrNull(rdr["Fax2"]);
                        record.www = Conversion.getStrOrNull(rdr["www"]);
                        record.Email1 = Conversion.getStrOrNull(rdr["Email1"]);
                        record.Email2 = Conversion.getStrOrNull(rdr["Email2"]);
                        record.CreditLimit = Conversion.getDecimalOrNull(rdr["CreditLimit"]);
                        record.Curr = Conversion.getStrOrNull(rdr["Curr"]);
                        record.PayDay = Conversion.getInt16OrNull(rdr["PayDay"]);
                        record.PayMode = Conversion.getInt16OrNull(rdr["PayMode"]);
                        record.PayModeDay = Conversion.getInt16OrNull(rdr["PayModeDay"]);
                        record.PayType = Conversion.getStrOrNull(rdr["PayType"]);
                        record.Note = Conversion.getStrOrNull(rdr["Note"]);
                        record.AllowSendRes = Conversion.getStrOrNull(rdr["AllowSendRes"]);
                        record.SendType = Conversion.getInt16OrNull(rdr["SendType"]);
                        record.TAcCode = Conversion.getStrOrNull(rdr["TAcCode"]);
                        record.CIF = Conversion.getStrOrNull(rdr["CIF"]);
                        record.CapSocial = Conversion.getDecimalOrNull(rdr["CapSocial"]);
                        record.CapSocialCur = Conversion.getStrOrNull(rdr["CapSocialCur"]);
                        record.ConPas = Conversion.getStrOrNull(rdr["ConPas"]);
                        record.SendOptDate = Conversion.getBoolOrNull(rdr["SendOptDate"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string GetRefNoForNordea(string resNo, ref string errorMsg)
        {
            string sql = "Select RefNo=dbo.DetBankPayRef(@ResNo)";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCmd = db.GetSqlStringCommand(sql);
            try
            {
                db.AddInParameter(dbCmd, "ResNo", DbType.String, resNo);

                return Conversion.getStrOrNull(db.ExecuteScalar(dbCmd));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCmd.Connection.Close();
            }
        }

        public PaximumRecord getPaximumConfig(User UserData, ref string errorMsg)
        {
            string tsql = string.Empty;

            if (VersionControl.getTableField("ParamSystem", "Pxm_RedictionUrl"))
            {
                if (VersionControl.getTableField("ParamSystem", "Pxm_DoNotShowOwnCntry"))
                    tsql =
@"
Select isnull(A.Pxm_Use,DPs.Pxm_Use) as Pxm_Use,DPs.Pxm_AutMainCode,DPs.Pxm_APIAddr,DPs.Pxm_RedictionUrl,
    Pxm_WorkType=isnull(Ps.Pxm_WorkType,CAST(1 As SmallInt)),
    Pxm_VoucherOpt=isnull(Ps.Pxm_VoucherOpt,CAST(0 As SmallInt)),
    Pxm_UseLimit=(Case When isnull(A.Pxm_UseLimit_UseMarOpt,1)=1 Then isnull(Ps.Pxm_UseLimit, 1) Else isnull(A.Pxm_UseLimit, 1) End),
    Pxm_UseLimit_UseMarOpt=isnull(A.Pxm_UseLimit_UseMarOpt,1),
    Pxm_CancelDeadLine=Case When isnull(A.Pxm_CancelDeadLine_UseMarOpt,1)=1 Then isnull(Ps.Pxm_CancelDeadLine, 1) Else isnull(A.Pxm_CancelDeadLine, 1) End,
    Pxm_CancelDeadLine_UseMarOpt=isnull(A.Pxm_CancelDeadLine_UseMarOpt,1),
    Pxm_DoNotShowOwnCntry=Case when isnull(A.Pxm_DoNotShowOwnCntry_UseMarOpt,1)=1 Then isnull(Ps.Pxm_DoNotShowOwnCntry, 1) Else isnull(A.Pxm_DoNotShowOwnCntry, 1) End
From ParamSystem DPs (NOLOCK), 
	 ParamSystem Ps (NOLOCK),
	 Agency A (NOLOCK)
Where DPs.Market=''
  And Ps.Market=@pMarket
  And A.Code=@pAgency
";
                else
                if (VersionControl.getTableField("ParamSystem", "Pxm_VoucherOpt"))
                {                    
                    if (VersionControl.getTableField("Agency", "Pxm_CancelDeadLine_UseMarOpt"))
                    {
                        tsql =
@"
Select DPs.Pxm_Use,DPs.Pxm_AutMainCode,DPs.Pxm_APIAddr,DPs.Pxm_RedictionUrl,
    Pxm_WorkType=isnull(Ps.Pxm_WorkType,CAST(1 As SmallInt)),
    Pxm_VoucherOpt=isnull(Ps.Pxm_VoucherOpt,CAST(0 As SmallInt)),
    Pxm_UseLimit=(Case When isnull(A.Pxm_UseLimit_UseMarOpt,1)=1 Then isnull(Ps.Pxm_UseLimit, 1) Else isnull(A.Pxm_UseLimit, 1) End),
    Pxm_UseLimit_UseMarOpt=isnull(A.Pxm_UseLimit_UseMarOpt,1),
    Pxm_CancelDeadLine=Case When isnull(A.Pxm_CancelDeadLine_UseMarOpt,1)=1 Then isnull(Ps.Pxm_CancelDeadLine, 1) Else isnull(A.Pxm_CancelDeadLine, 1) End,
    Pxm_CancelDeadLine_UseMarOpt=isnull(A.Pxm_CancelDeadLine_UseMarOpt,1),
    Pxm_DoNotShowOwnCntry=Cast(1 As Bit)
From ParamSystem DPs (NOLOCK), 
	 ParamSystem Ps (NOLOCK),
	 Agency A (NOLOCK)
Where DPs.Market=''
  And Ps.Market=@pMarket
  And A.Code=@pAgency
";
                    }
                    else
                    {
                        tsql =
@"
Select DPs.Pxm_Use,DPs.Pxm_AutMainCode,DPs.Pxm_APIAddr,DPs.Pxm_RedictionUrl,
    Pxm_WorkType=isnull(Ps.Pxm_WorkType,CAST(1 As SmallInt)),
    Pxm_VoucherOpt=isnull(Ps.Pxm_VoucherOpt,CAST(0 As SmallInt)),
    Pxm_UseLimit=CAST(0 As Bit),
    Pxm_UseLimit_UseMarOpt=CAST(0 As Bit),
    Pxm_CancelDeadLine=CAST(0 As Bit),
    Pxm_CancelDeadLine_UseMarOpt=CAST(0 As Bit),
    Pxm_DoNotShowOwnCntry=Cast(1 As Bit)
From ParamSystem DPs (NOLOCK), ParamSystem Ps (NOLOCK)
Where DPs.Market=''
  And Ps.Market=@pMarket
";
                    }
                }
                else
                    if (VersionControl.getTableField("ParamSystem", "Pxm_WorkType"))
                    {
                        tsql =
@"
Select DPs.Pxm_Use,DPs.Pxm_AutMainCode,DPs.Pxm_APIAddr,DPs.Pxm_RedictionUrl,
    Pxm_WorkType=isnull(Ps.Pxm_WorkType,CAST(1 As SmallInt)),
    Pxm_VoucherOpt=CAST(0 As SmallInt),
    Pxm_UseLimit=CAST(0 As Bit),
    Pxm_UseLimit_UseMarOpt=CAST(0 As Bit),
    Pxm_CancelDeadLine=CAST(0 As Bit),
    Pxm_CancelDeadLine_UseMarOpt=CAST(0 As Bit),
    Pxm_DoNotShowOwnCntry=Cast(1 As Bit)
From ParamSystem DPs (NOLOCK), ParamSystem Ps (NOLOCK)
Where DPs.Market=''
  And Ps.Market=@pMarket
";
                    }
                    else
                    {
                        tsql =
@"
Select Pxm_Use,Pxm_AutMainCode,Pxm_APIAddr,Pxm_RedictionUrl,
    Pxm_WorkType=Cast(1 As smallint),
    Pxm_VoucherOpt=CAST(0 As SmallInt),
    Pxm_UseLimit=CAST(0 As Bit),
    Pxm_UseLimit_UseMarOpt=CAST(0 As Bit),
    Pxm_CancelDeadLine=CAST(0 As Bit),
    Pxm_CancelDeadLine_UseMarOpt=CAST(0 As Bit),
    Pxm_DoNotShowOwnCntry=Cast(1 As Bit)
From ParamSystem (NOLOCK)
Where Market=''
";
                    }
                Database db = DatabaseFactory.CreateDatabase() as Database;
                DbCommand dbCommand = db.GetSqlStringCommand(tsql);

                try
                {
                    db.AddInParameter(dbCommand, "pMarket", DbType.AnsiString, UserData.Market);
                    db.AddInParameter(dbCommand, "pAgency", DbType.AnsiString, UserData.AgencyID);
                    using (IDataReader rdr = db.ExecuteReader(dbCommand))
                    {
                        if (rdr.Read())
                        {
                            PaximumRecord record = new PaximumRecord();
                            record.Pxm_Use = Conversion.getBoolOrNull(rdr["Pxm_Use"]);
                            record.Pxm_AutMainCode = Conversion.getStrOrNull(rdr["Pxm_AutMainCode"]);
                            record.Pxm_APIAddr = Conversion.getStrOrNull(rdr["Pxm_APIAddr"]);
                            record.Pxm_RedictionUrl = Conversion.getStrOrNull(rdr["Pxm_RedictionUrl"]);
                            record.Pxm_WorkType = (PxmWorkType)((Int16)(rdr["Pxm_WorkType"]));
                            record.Pxm_VoucherOpt = (PxmVoucherOption)((Int16)(rdr["Pxm_VoucherOpt"]));
                            record.Pxm_UseLimit = (bool)rdr["Pxm_UseLimit"];
                            record.Pxm_UseLimit_UseMarOpt = (bool)rdr["Pxm_UseLimit_UseMarOpt"];
                            record.Pxm_CancelDeadLine = (bool)rdr["Pxm_CancelDeadLine"];
                            record.Pxm_CancelDeadLine_UseMarOpt = (bool)rdr["Pxm_CancelDeadLine_UseMarOpt"];
                            record.Pxm_DoNotShowOwnCntry = (bool)rdr["Pxm_DoNotShowOwnCntry"];
                            return record;
                        }
                        else
                        {
                            return new PaximumRecord();
                        }
                    }
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return new PaximumRecord();
                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
            }
            else
            {
                return new PaximumRecord();
            }
        }
    }
}
