﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using TvTools;

namespace TvBo
{
    public class Parameters
    {
        public MarketRecord getMarket(string Market, ref string errorMsg)
        {
            string tsql = @"Select M.RecID, M.Code, M.Name, M.NameS, 
	                            NameL=isnull(dbo.FindLocalName(M.NameLID, M.Code), M.Name),
	                            M.Cur, M.PoolAllot, M.LangID, M.Country,
	                            CountryName=isnull(L.Name, ''), CountryNameL=isnull(dbo.FindLocalName(L.NameLID, M.Code), isnull(L.Name, ''))
                            From Market M (NOLOCK)
                            Left Join Location L (NOLOCK) ON L.RecID=M.Country
                            Where M.Code=@Market
                            ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        MarketRecord record = new MarketRecord();
                        record.RecID = (Int32)oReader["RecID"];
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.Cur = Conversion.getStrOrNull(oReader["Cur"]);
                        record.PoolAllot = Conversion.getInt32OrNull(oReader["PoolAllot"]);
                        record.LangID = Conversion.getInt32OrNull(oReader["LangID"]);
                        record.Country = Conversion.getInt32OrNull(oReader["Country"]);
                        record.CountryName = Conversion.getStrOrNull(oReader["CountryName"]);
                        record.CountryNameL = Conversion.getStrOrNull(oReader["CountryNameL"]);
                        return record;
                    }
                    else return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<MarketRecord> getMarkets(ref string errorMsg)
        {
            List<MarketRecord> records = new List<MarketRecord>();

            string tsql = @"Select M.RecID, M.Code, M.Name, M.NameS, 
	                            NameL=isnull(dbo.FindLocalName(M.NameLID, M.Code), M.Name),
	                            M.Cur, M.PoolAllot, M.LangID, M.Country,
	                            CountryName=isnull(L.Name, ''), CountryNameL=isnull(dbo.FindLocalName(L.NameLID, M.Code), isnull(L.Name, ''))
                            From Market M (NOLOCK)
                            Left Join Location L (NOLOCK) ON L.RecID=M.Country";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        MarketRecord record = new MarketRecord();
                        record.RecID = (Int32)oReader["RecID"];
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.Cur = Conversion.getStrOrNull(oReader["Cur"]);
                        record.PoolAllot = Conversion.getInt32OrNull(oReader["PoolAllot"]);
                        record.LangID = Conversion.getInt32OrNull(oReader["LangID"]);
                        record.Country = Conversion.getInt32OrNull(oReader["Country"]);
                        record.CountryName = Conversion.getStrOrNull(oReader["CountryName"]);
                        record.CountryNameL = Conversion.getStrOrNull(oReader["CountryNameL"]);                        
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OperatorRecord> getOperators(string Market, ref string errorMsg)
        {
            List<OperatorRecord> records = new List<OperatorRecord>();
            string tsql = @"Select O.RecID, O.Code, O.Name, O.NameS, NameL=isnull(dbo.FindLocalName(O.NameLID, @Market), O.Name),
	                            O.Status, O.MainOperator, O.Market, O.FirmName, 
	                            O.Location, LocationName=isnull(L.Name, ''), LocationNameL=isnull(dbo.FindLocalName(L.NameLID, @Market), isnull(L.Name, '')),
	                            O.Address, O.InvAddress, O.TaxOffice, O.TaxAccNo, O.BlackList, 
	                            O.Bank1, O.Bank1BankNo, O.Bank1AccNo, O.Bank1Curr, O.Bank1IBAN,
	                            O.Bank2, O.Bank2BankNo, O.Bank2AccNo, O.Bank2Curr, O.Bank2IBAN,
	                            O.BossName, O.ContName, O.ACContacName, O.Phone1, O.Phone2, O.MobPhone, O.Fax1, O.Fax2,
	                            O.www, O.email1, O.email2, O.Note, O.ResSerie, O.RegisterCode, 
	                            O.AddrZip, O.AddrCity, O.AddrCountry, O.InvAddrZip, O.InvAddrCity, O.InvAddrCountry,
	                            O.InsSupplierName, O.InsPolicyName, O.InsPolicyNo, O.InsIssuedDate, O.InsExpDate, 
	                            O.CompanyNo, O.Bank1AccId, O.Bank2AccId, O.CIF, O.CapSocial, O.CapSocialCur, 
	                            O.LicenseNo, O.AgencyCanDisCom
                            From Operator O (NOLOCK)
                            Left Join Location L (NOLOCK) ON L.RecID=O.Location";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        OperatorRecord record = new OperatorRecord();

                        record.RecID = (Int32)oReader["RecID"];
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.Status = Conversion.getStrOrNull(oReader["Status"]);
                        record.MainOperator = Conversion.getStrOrNull(oReader["MainOperator"]);
                        record.Market = Conversion.getStrOrNull(oReader["Market"]);
                        record.FirmName = Conversion.getStrOrNull(oReader["FirmName"]);
                        record.Location = Conversion.getInt32OrNull(oReader["Location"]);
                        record.LocationName = Conversion.getStrOrNull(oReader["LocationName"]);
                        record.Address = Conversion.getStrOrNull(oReader["Address"]);
                        record.InvAddress = Conversion.getStrOrNull(oReader["InvAddress"]);
                        record.TaxOffice = Conversion.getStrOrNull(oReader["TaxOffice"]);
                        record.TaxAccNo = Conversion.getStrOrNull(oReader["TaxAccNo"]);
                        record.BlackList = Conversion.getStrOrNull(oReader["BlackList"]);
                        record.Bank1 = Conversion.getStrOrNull(oReader["Bank1"]);
                        record.Bank1BankNo = Conversion.getStrOrNull(oReader["Bank1BankNo"]);
                        record.Bank1AccNo = Conversion.getStrOrNull(oReader["Bank1AccNo"]);
                        record.Bank1Curr = Conversion.getStrOrNull(oReader["Bank1Curr"]);
                        record.Bank1IBAN = Conversion.getStrOrNull(oReader["Bank1IBAN"]);
                        record.Bank2 = Conversion.getStrOrNull(oReader["Bank2"]);
                        record.Bank2BankNo = Conversion.getStrOrNull(oReader["Bank2BankNo"]);
                        record.Bank2AccNo = Conversion.getStrOrNull(oReader["Bank2AccNo"]);
                        record.Bank2Curr = Conversion.getStrOrNull(oReader["Bank2Curr"]);
                        record.Bank2IBAN = Conversion.getStrOrNull(oReader["Bank2IBAN"]);
                        record.BossName = Conversion.getStrOrNull(oReader["BossName"]);
                        record.ContName = Conversion.getStrOrNull(oReader["ContName"]);
                        record.ACContacName = Conversion.getStrOrNull(oReader["ACContacName"]);
                        record.Phone1 = Conversion.getStrOrNull(oReader["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(oReader["Phone2"]);
                        record.MobPhone = Conversion.getStrOrNull(oReader["MobPhone"]);
                        record.Fax1 = Conversion.getStrOrNull(oReader["Fax1"]);
                        record.Fax2 = Conversion.getStrOrNull(oReader["Fax2"]);
                        record.Www = Conversion.getStrOrNull(oReader["www"]);
                        record.Email1 = Conversion.getStrOrNull(oReader["email1"]);
                        record.Email2 = Conversion.getStrOrNull(oReader["email2"]);
                        record.Note = Conversion.getStrOrNull(oReader["Note"]);
                        record.ResSerie = Conversion.getStrOrNull(oReader["ResSerie"]);
                        record.RegisterCode = Conversion.getStrOrNull(oReader["RegisterCode"]);
                        record.AddrZip = Conversion.getStrOrNull(oReader["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(oReader["AddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(oReader["AddrCountry"]);
                        record.InvAddrZip = Conversion.getStrOrNull(oReader["InvAddrZip"]);
                        record.InvAddrCity = Conversion.getStrOrNull(oReader["InvAddrCity"]);
                        record.InvAddrCountry = Conversion.getStrOrNull(oReader["InvAddrCountry"]);
                        record.InsSupplierName = Conversion.getStrOrNull(oReader["InsSupplierName"]);
                        record.InsPolicyName = Conversion.getStrOrNull(oReader["InsPolicyName"]);
                        record.InsPolicyNo = Conversion.getStrOrNull(oReader["InsPolicyNo"]);
                        record.InsIssuedDate = Conversion.getDateTimeOrNull(oReader["InsIssuedDate"]);
                        record.InsExpDate = Conversion.getDateTimeOrNull(oReader["InsExpDate"]);
                        record.CompanyNo = Conversion.getStrOrNull(oReader["CompanyNo"]);
                        record.Bank1AccId = Conversion.getStrOrNull(oReader["Bank1AccId"]);
                        record.Bank2AccId = Conversion.getStrOrNull(oReader["Bank2AccId"]);
                        record.CIF = Conversion.getStrOrNull(oReader["CIF"]);
                        record.CapSocial = Conversion.getStrOrNull(oReader["CapSocial"]);
                        record.CapSocialCur = Conversion.getStrOrNull(oReader["CapSocialCur"]);
                        record.LicenseNo = Conversion.getStrOrNull(oReader["LicenseNo"]);
                        record.AgencyCanDisCom = (bool)oReader["AgencyCanDisCom"];

                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getOperatorName(string Market, string Operator, NameType nameType, ref string errorMsg)
        {
            string retVal = string.Empty;
            switch (nameType)
            {
                case NameType.NormalName:
                    retVal = CacheObjects.getOperators(Market).AsEnumerable().Where(w => w.Code == Operator).Select(s => s.Name).FirstOrDefault();
                    break;
                case NameType.LocalName:
                    retVal = CacheObjects.getOperators(Market).AsEnumerable().Where(w => w.Code == Operator).Select(s => s.NameL).FirstOrDefault();
                    break;
            }
            return retVal;
        }

        public string getMarketName(string Market, string _Market, NameType nameType, ref string errorMsg)
        {
            string retVal = string.Empty;
            switch (nameType)
            {
                case NameType.NormalName:
                    retVal = CacheObjects.getMarkets(Market).AsEnumerable().Where(w => w.Code == _Market).Select(s => s.Name).FirstOrDefault();
                    break;
                case NameType.LocalName:
                    retVal = CacheObjects.getMarkets(Market).AsEnumerable().Where(w => w.Code == _Market).Select(s => s.NameL).FirstOrDefault();
                    break;
            }
            return retVal;
        }
    }    
}
