﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TvTools;

namespace TvBo
{
    public class Search
    {
        public string getSearchText(string Market, ref string errorMsg)
        {
            string tsql = @"Select Note
                            From FixNotes (NOLOCK)
                            Where Market = @Market And Type = 0
                            And Section = 'PriceList' ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public string getSearchText(User UserData, ref string errorMsg)
        {
            string tsql = @"Select Note
                            From FixNotes (NOLOCK)
                            Where Market = @Market And Type = 0
                            And Section = 'PriceList' ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public object getSearchTextObj(User UserData, ref string errorMsg)
        {
            string tsql = @"Select Note
                            From FixNotes (NOLOCK)
                            Where Market=@Market And Type=0
                            And Section='PriceList' ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    return (object)db.ExecuteScalar(dbCommand);
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public CatalogPackRecord getCatalogPack(User UserData, int? CatPackID, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0023, VersionControl.Equality.gt))
                tsql = @"Select RecID, CatID, HolPack, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name), 
                            NameS, Operator, Market, TargetMarket, SaleSPONo, RevNo, RevDate, BegDate, EndDate, 
                            Ready, SaleBegDate, SaleEndDate, PubDate, WebPub, WebPubDate, CalcDate, [Description], FlightClass, 
                            AccomNight, SaleCur, CinDateSource, MinNight, MaxNight, BreakNoPriceChk, CheckInDateChk, OnlySpoServiceChk,
                            UseUFlightPriceChk, UFAgency, FBlockType, FPriceOpt, EBValid, CanCredit, DepCity, ArrCity, ExChgOpt, 
                            PasEBValid, PackType, B2CPub, B2CPubDate, SupDisCode, SupDisCodeB2C, MaxDiscountPer, RefHotel, Direction, 
                            Category, RefBoard, RefRoom, TANight, PLSPOExists, XMLExportDate, ChanceSale, EndTime, 
                            RoundType, RoundOpt, RoundVal1, RoundVal2, 
                            DepDepCity, DepArrCity, RetDepCity, RetArrCity, DepServiceType, RetServiceType
                        From CatalogPack (NOLOCK) 
                        Where RecID=@CatPackID
                        ";
            else
                tsql = @"Select RecID, CatID, HolPack, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name), 
                            NameS, Operator, Market, TargetMarket, SaleSPONo, RevNo, RevDate, BegDate, EndDate,
                            Ready, SaleBegDate, SaleEndDate, PubDate, WebPub, WebPubDate, CalcDate, [Description], FlightClass, 
                            AccomNight, SaleCur, CinDateSource, MinNight, MaxNight, BreakNoPriceChk, CheckInDateChk, OnlySpoServiceChk,
                            UseUFlightPriceChk, UFAgency, FBlockType, FPriceOpt, EBValid, CanCredit, DepCity, ArrCity, ExChgOpt, 
                            PasEBValid, PackType, B2CPub, B2CPubDate, SupDisCode, SupDisCodeB2C, MaxDiscountPer, RefHotel, Direction, 
                            Category, RefBoard, RefRoom, TANight, PLSPOExists, XMLExportDate, ChanceSale, EndTime, 
                            RoundType, RoundOpt, RoundVal1, RoundVal2,
                            DepDepCity=DepCity, DepArrCity=ArrCity, RetDepCity=ArrCity, RetArrCity=DepCity, DepServiceType='', RetServiceType=''
                        From CatalogPack (NOLOCK) 
                        Where RecID=@CatPackID
                        ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        CatalogPackRecord rec = new CatalogPackRecord();
                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.CatID = Conversion.getInt32OrNull(R["CatID"]);
                        rec.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                        rec.NameS = Conversion.getStrOrNull(R["NameS"]);
                        rec.Operator = Conversion.getStrOrNull(R["Operator"]);
                        rec.Market = Conversion.getStrOrNull(R["Market"]);
                        rec.TargetMarket = Conversion.getStrOrNull(R["TargetMarket"]);
                        rec.SaleSPONo = Conversion.getStrOrNull(R["SaleSPONo"]);
                        rec.RevNo = Conversion.getInt32OrNull(R["RevNo"]);
                        rec.RevDate = Conversion.getDateTimeOrNull(R["RevDate"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        rec.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        rec.Ready = Conversion.getStrOrNull(R["Ready"]);
                        rec.SaleBegDate = Conversion.getDateTimeOrNull(R["SaleBegDate"]);
                        rec.SaleEndDate = Conversion.getDateTimeOrNull(R["SaleEndDate"]);
                        rec.PubDate = Conversion.getDateTimeOrNull(R["PubDate"]);
                        rec.WebPub = Conversion.getStrOrNull(R["WebPub"]);
                        rec.WebPubDate = Conversion.getDateTimeOrNull(R["WebPubDate"]);
                        rec.CalcDate = Conversion.getDateTimeOrNull(R["CalcDate"]);
                        rec.Description = Conversion.getStrOrNull(R["Description"]);
                        rec.FlightClass = Conversion.getStrOrNull(R["FlightClass"]);
                        rec.AccomNight = Conversion.getStrOrNull(R["AccomNight"]);
                        rec.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        rec.CinDateSource = Conversion.getInt16OrNull(R["CinDateSource"]);
                        rec.MinNight = Conversion.getInt16OrNull(R["MinNight"]);
                        rec.MaxNight = Conversion.getInt16OrNull(R["MaxNight"]);
                        rec.BreakNoPriceChk = Conversion.getStrOrNull(R["BreakNoPriceChk"]);
                        rec.CheckInDateChk = Conversion.getStrOrNull(R["CheckInDateChk"]);
                        rec.OnlySpoServiceChk = Conversion.getStrOrNull(R["OnlySpoServiceChk"]);
                        rec.UseUFlightPriceChk = Conversion.getStrOrNull(R["UseUFlightPriceChk"]);
                        rec.UFAgency = Conversion.getStrOrNull(R["UFAgency"]);
                        rec.FBlockType = Conversion.getStrOrNull(R["FBlockType"]);
                        rec.FPriceOpt = Conversion.getStrOrNull(R["FPriceOpt"]);
                        rec.EBValid = Conversion.getStrOrNull(R["EBValid"]);
                        rec.CanCredit = Conversion.getStrOrNull(R["CanCredit"]);
                        rec.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        rec.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        rec.ExChgOpt = Conversion.getInt32OrNull(R["ExChgOpt"]);
                        rec.PasEBValid = Conversion.getStrOrNull(R["PasEBValid"]);
                        rec.PackType = Conversion.getStrOrNull(R["PackType"]);
                        rec.B2CPub = Conversion.getBoolOrNull(R["B2CPub"]);
                        rec.B2CPubDate = Conversion.getDateTimeOrNull(R["B2CPubDate"]);
                        rec.SupDisCode = Conversion.getStrOrNull(R["SupDisCode"]);
                        rec.SupDisCodeB2C = Conversion.getStrOrNull(R["SupDisCodeB2C"]);
                        rec.MaxDiscountPer = Conversion.getDecimalOrNull(R["MaxDiscountPer"]);
                        rec.RefHotel = Conversion.getStrOrNull(R["RefHotel"]);
                        rec.Direction = Conversion.getInt16OrNull(R["Direction"]);
                        rec.Category = Conversion.getInt16OrNull(R["Category"]);
                        rec.RefBoard = Conversion.getStrOrNull(R["RefBoard"]);
                        rec.RefRoom = Conversion.getStrOrNull(R["RefRoom"]);
                        rec.TANight = Conversion.getByteOrNull(R["TANight"]);
                        rec.PLSPOExists = Conversion.getBoolOrNull(R["PLSPOExists"]);
                        rec.XMLExportDate = Conversion.getDateTimeOrNull(R["XMLExportDate"]);
                        rec.ChanceSale = Conversion.getBoolOrNull(R["ChanceSale"]);
                        rec.EndTime = Conversion.getDateTimeOrNull(R["EndTime"]);
                        rec.RoundType = Conversion.getInt16OrNull(R["RoundType"]);
                        rec.RoundOpt = Conversion.getInt16OrNull(R["RoundOpt"]);
                        rec.RoundVal1 = Conversion.getDecimalOrNull(R["RoundVal1"]);
                        rec.DepDepCity = Conversion.getInt32OrNull(R["DepDepCity"]);
                        rec.DepArrCity = Conversion.getInt32OrNull(R["DepArrCity"]);
                        rec.RetDepCity = Conversion.getInt32OrNull(R["RetDepCity"]);
                        rec.RetArrCity = Conversion.getInt32OrNull(R["RetArrCity"]);
                        rec.DepServiceType = Conversion.getStrOrNull(R["DepServiceType"]);
                        rec.RetServiceType = Conversion.getStrOrNull(R["RetServiceType"]);
                        return rec;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CatalogPackRecord> getCatalogPackList(User UserData, ref string errorMsg)
        {
            List<CatalogPackRecord> records = new List<CatalogPackRecord>();

            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0023, VersionControl.Equality.gt))
                tsql = @"Select RecID, CatID, HolPack, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name), 
                            NameS, Operator, Market, TargetMarket, SaleSPONo, RevNo, RevDate, BegDate, EndDate,
                            Ready, SaleBegDate, SaleEndDate, PubDate, WebPub, WebPubDate, CalcDate, [Description], FlightClass, 
                            AccomNight, SaleCur, CinDateSource, MinNight, MaxNight, BreakNoPriceChk, CheckInDateChk, OnlySpoServiceChk,
                            UseUFlightPriceChk, UFAgency, FBlockType, FPriceOpt, EBValid, CanCredit, DepCity, ArrCity, ExChgOpt, 
                            PasEBValid, PackType, B2CPub, B2CPubDate, SupDisCode, SupDisCodeB2C, MaxDiscountPer, RefHotel, Direction, 
                            Category, RefBoard, RefRoom, TANight, PLSPOExists, XMLExportDate, ChanceSale, EndTime, 
                            RoundType, RoundOpt, RoundVal1, RoundVal2, 
                            DepDepCity, DepArrCity, RetDepCity, RetArrCity, DepServiceType, RetServiceType
                        From CatalogPack (NOLOCK) ";
            else
                tsql = @"Select RecID, CatID, HolPack, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name), 
                            NameS, Operator, Market, TargetMarket, SaleSPONo, RevNo, RevDate, BegDate, EndDate,
                            Ready, SaleBegDate, SaleEndDate, PubDate, WebPub, WebPubDate, CalcDate, [Description], FlightClass, 
                            AccomNight, SaleCur, CinDateSource, MinNight, MaxNight, BreakNoPriceChk, CheckInDateChk, OnlySpoServiceChk,
                            UseUFlightPriceChk, UFAgency, FBlockType, FPriceOpt, EBValid, CanCredit, DepCity, ArrCity, ExChgOpt, 
                            PasEBValid, PackType, B2CPub, B2CPubDate, SupDisCode, SupDisCodeB2C, MaxDiscountPer, RefHotel, Direction, 
                            Category, RefBoard, RefRoom, TANight, PLSPOExists, XMLExportDate, ChanceSale, EndTime, 
                            RoundType, RoundOpt, RoundVal1, RoundVal2,
                            DepDepCity=DepCity, DepArrCity=ArrCity, RetDepCity=ArrCity, RetArrCity=DepCity, DepServiceType='', RetServiceType=''
                        From CatalogPack (NOLOCK) ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        CatalogPackRecord rec = new CatalogPackRecord();
                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.CatID = Conversion.getInt32OrNull(R["CatID"]);
                        rec.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                        rec.NameS = Conversion.getStrOrNull(R["NameS"]);
                        rec.Operator = Conversion.getStrOrNull(R["Operator"]);
                        rec.Market = Conversion.getStrOrNull(R["Market"]);
                        rec.TargetMarket = Conversion.getStrOrNull(R["TargetMarket"]);
                        rec.SaleSPONo = Conversion.getStrOrNull(R["SaleSPONo"]);
                        rec.RevNo = Conversion.getInt32OrNull(R["RevNo"]);
                        rec.RevDate = Conversion.getDateTimeOrNull(R["RevDate"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        rec.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        rec.Ready = Conversion.getStrOrNull(R["Ready"]);
                        rec.SaleBegDate = Conversion.getDateTimeOrNull(R["SaleBegDate"]);
                        rec.SaleEndDate = Conversion.getDateTimeOrNull(R["SaleEndDate"]);
                        rec.PubDate = Conversion.getDateTimeOrNull(R["PubDate"]);
                        rec.WebPub = Conversion.getStrOrNull(R["WebPub"]);
                        rec.WebPubDate = Conversion.getDateTimeOrNull(R["WebPubDate"]);
                        rec.CalcDate = Conversion.getDateTimeOrNull(R["CalcDate"]);
                        rec.Description = Conversion.getStrOrNull(R["Description"]);
                        rec.FlightClass = Conversion.getStrOrNull(R["FlightClass"]);
                        rec.AccomNight = Conversion.getStrOrNull(R["AccomNight"]);
                        rec.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        rec.CinDateSource = Conversion.getInt16OrNull(R["CinDateSource"]);
                        rec.MinNight = Conversion.getInt16OrNull(R["MinNight"]);
                        rec.MaxNight = Conversion.getInt16OrNull(R["MaxNight"]);
                        rec.BreakNoPriceChk = Conversion.getStrOrNull(R["BreakNoPriceChk"]);
                        rec.CheckInDateChk = Conversion.getStrOrNull(R["CheckInDateChk"]);
                        rec.OnlySpoServiceChk = Conversion.getStrOrNull(R["OnlySpoServiceChk"]);
                        rec.UseUFlightPriceChk = Conversion.getStrOrNull(R["UseUFlightPriceChk"]);
                        rec.UFAgency = Conversion.getStrOrNull(R["UFAgency"]);
                        rec.FBlockType = Conversion.getStrOrNull(R["FBlockType"]);
                        rec.FPriceOpt = Conversion.getStrOrNull(R["FPriceOpt"]);
                        rec.EBValid = Conversion.getStrOrNull(R["EBValid"]);
                        rec.CanCredit = Conversion.getStrOrNull(R["CanCredit"]);
                        rec.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        rec.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        rec.ExChgOpt = Conversion.getInt32OrNull(R["ExChgOpt"]);
                        rec.PasEBValid = Conversion.getStrOrNull(R["PasEBValid"]);
                        rec.PackType = Conversion.getStrOrNull(R["PackType"]);
                        rec.B2CPub = Conversion.getBoolOrNull(R["B2CPub"]);
                        rec.B2CPubDate = Conversion.getDateTimeOrNull(R["B2CPubDate"]);
                        rec.SupDisCode = Conversion.getStrOrNull(R["SupDisCode"]);
                        rec.SupDisCodeB2C = Conversion.getStrOrNull(R["SupDisCodeB2C"]);
                        rec.MaxDiscountPer = Conversion.getDecimalOrNull(R["MaxDiscountPer"]);
                        rec.RefHotel = Conversion.getStrOrNull(R["RefHotel"]);
                        rec.Direction = Conversion.getInt16OrNull(R["Direction"]);
                        rec.Category = Conversion.getInt16OrNull(R["Category"]);
                        rec.RefBoard = Conversion.getStrOrNull(R["RefBoard"]);
                        rec.RefRoom = Conversion.getStrOrNull(R["RefRoom"]);
                        rec.TANight = Conversion.getByteOrNull(R["TANight"]);
                        rec.PLSPOExists = Conversion.getBoolOrNull(R["PLSPOExists"]);
                        rec.XMLExportDate = Conversion.getDateTimeOrNull(R["XMLExportDate"]);
                        rec.ChanceSale = Conversion.getBoolOrNull(R["ChanceSale"]);
                        rec.EndTime = Conversion.getDateTimeOrNull(R["EndTime"]);
                        rec.RoundType = Conversion.getInt16OrNull(R["RoundType"]);
                        rec.RoundOpt = Conversion.getInt16OrNull(R["RoundOpt"]);
                        rec.RoundVal1 = Conversion.getDecimalOrNull(R["RoundVal1"]);
                        rec.DepDepCity = Conversion.getInt32OrNull(R["DepDepCity"]);
                        rec.DepArrCity = Conversion.getInt32OrNull(R["DepArrCity"]);
                        rec.RetDepCity = Conversion.getInt32OrNull(R["RetDepCity"]);
                        rec.RetArrCity = Conversion.getInt32OrNull(R["RetArrCity"]);
                        rec.DepServiceType = Conversion.getStrOrNull(R["DepServiceType"]);
                        rec.RetServiceType = Conversion.getStrOrNull(R["RetServiceType"]);
                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public SearchPLData addPLLocations(DataSet plSearchData, SearchPLData baseData)
        {
            List<plLocationData> plData = new List<plLocationData>();
            DataTable Locations = plSearchData.Tables["Locations"];
            foreach (DataRow r in Locations.Rows)
            {
                try
                {
                    plLocationData plLocRecord = new plLocationData();
                    plLocRecord.DepCity = (int)r["DepCity"];
                    plLocRecord.DepCityName = r["DepCityName"].ToString();
                    plLocRecord.DepCityNameL = r["DepCityNameL"].ToString();
                    plLocRecord.ArrCity = (int)r["ArrCity"];
                    plLocRecord.ArrCityName = r["ArrCityName"].ToString();
                    plLocRecord.ArrCityNameL = r["ArrCityNameL"].ToString();
                    plLocRecord.ArrCountry = (int)r["ArrCountry"];
                    plLocRecord.ArrCountryName = r["ArrCountryName"].ToString();
                    plLocRecord.ArrCountryNameL = r["ArrCountryNameL"].ToString();
                    plLocRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                    plLocRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                    plLocRecord.HolPackCatName = Conversion.getStrOrNull(r["CategoryName"]);
                    plLocRecord.HolPackCatNameL = Conversion.getStrOrNull(r["CategoryNameL"]);
                    plData.Add(plLocRecord);
                }
                catch { }
            }
            baseData.PlLocations = plData;
            return baseData;
        }

        public srchFilterData addPLLocationsV2(DataSet plSearchData, srchFilterData baseData)
        {
            List<srchLocations> plData = new List<srchLocations>();
            DataTable Locations = plSearchData.Tables["Locations"];
            foreach (DataRow r in Locations.Rows)
            {
                try
                {
                    srchLocations plLocRecord = new srchLocations();
                    plLocRecord.DepCity = (int)r["DepCity"];
                    plLocRecord.DepCityName = r["DepCityName"].ToString();
                    plLocRecord.DepCityNameL = r["DepCityNameL"].ToString();
                    plLocRecord.ArrCity = (int)r["ArrCity"];
                    plLocRecord.ArrCityName = r["ArrCityName"].ToString();
                    plLocRecord.ArrCityNameL = r["ArrCityNameL"].ToString();
                    plLocRecord.ArrCountry = (int)r["ArrCountry"];
                    plLocRecord.ArrCountryName = r["ArrCountryName"].ToString();
                    plLocRecord.ArrCountryNameL = r["ArrCountryNameL"].ToString();
                    if (Locations.Columns.Contains("Direction"))
                        plLocRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                    if (Locations.Columns.Contains("Category"))
                        plLocRecord.Category = Conversion.getInt16OrNull(r["Category"]);
                    if (Locations.Columns.Contains("PackType"))
                        plLocRecord.PackType = Conversion.getStrOrNull(r["PackType"]);
                    plData.Add(plLocRecord);
                }
                catch { }
            }
            baseData.SrchLocation = plData;
            return baseData;
        }

        public SearchPLData addPLLocationsB2BMenuCat(DataSet plSearchData, SearchPLData baseData)
        {
            List<plLocationData> plData = new List<plLocationData>();
            DataTable Locations = plSearchData.Tables["Locations"];
            foreach (DataRow r in Locations.Rows)
            {
                try
                {
                    plLocationData plLocRecord = new plLocationData();
                    plLocRecord.DepCity = (int)r["DepCity"];
                    plLocRecord.DepCityName = r["DepCityName"].ToString();
                    plLocRecord.DepCityNameL = r["DepCityNameL"].ToString();
                    plLocRecord.ArrCity = (int)r["ArrCity"];
                    plLocRecord.ArrCityName = r["ArrCityName"].ToString();
                    plLocRecord.ArrCityNameL = r["ArrCityNameL"].ToString();
                    plLocRecord.ArrCountry = (int)r["ArrCountry"];
                    plLocRecord.ArrCountryName = r["ArrCountryName"].ToString();
                    plLocRecord.ArrCountryNameL = r["ArrCountryNameL"].ToString();
                    if (Locations.Columns.Contains("Direction"))
                        plLocRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                    if (Locations.Columns.Contains("Category"))
                        plLocRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                    if (Locations.Columns.Contains("CategoryName"))
                        plLocRecord.HolPackCatName = Conversion.getStrOrNull(r["CategoryName"]);
                    if (Locations.Columns.Contains("CategoryNameL"))
                        plLocRecord.HolPackCatNameL = Conversion.getStrOrNull(r["CategoryNameL"]);

                    plData.Add(plLocRecord);
                }
                catch { }
            }
            baseData.PlLocations = plData;
            return baseData;
        }

        public SearchPLData addPLCategorys(DataSet plSearchData, SearchPLData baseData)
        {
            List<plCategoryData> plData = new List<plCategoryData>();
            DataTable Categorys = plSearchData.Tables["Category"];
            foreach (DataRow r in Categorys.Rows)
            {
                plCategoryData plCatRecord = new plCategoryData();
                plCatRecord.Code = r["Code"].ToString();
                plCatRecord.Name = r["Name"].ToString();
                plCatRecord.NameL = r["NameL"].ToString();
                plCatRecord.Country = (int)r["Country"];
                plCatRecord.PLCountry = Conversion.getInt32OrNull(r["PL_Country"]);
                plCatRecord.City = (int)r["City"];
                plCatRecord.PLCity = Conversion.getInt32OrNull(r["PL_City"]);
                plCatRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plCatRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plCatRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                plCatRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plCatRecord);
            }
            baseData.plCategorys = plData;
            return baseData;
        }

        public srchFilterData addPLCategorysV2(DataSet plSearchData, srchFilterData baseData)
        {
            List<srchCategory> plData = new List<srchCategory>();
            DataTable Categorys = plSearchData.Tables["Category"];
            foreach (DataRow r in Categorys.Rows)
            {
                srchCategory plCatRecord = new srchCategory();
                plCatRecord.Code = r["Code"].ToString();
                plCatRecord.Name = r["Name"].ToString();
                plCatRecord.NameL = r["NameL"].ToString();
                plCatRecord.Country = (int)r["Country"];
                plCatRecord.PL_Country = Conversion.getInt32OrNull(r["PL_Country"]);
                plCatRecord.City = (int)r["City"];
                plCatRecord.PL_City = Conversion.getInt32OrNull(r["PL_City"]);
                plCatRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plCatRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                if (Categorys.Columns.Contains("Direction"))
                    plCatRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                if (Categorys.Columns.Contains("Category"))
                    plCatRecord.Category = Conversion.getInt16OrNull(r["Category"]);
                if (Categorys.Columns.Contains("PackType"))
                    plCatRecord.PackType = Conversion.getStrOrNull(r["PackType"]);
                plData.Add(plCatRecord);
            }
            baseData.SrchCategory = plData;
            return baseData;
        }

        public SearchPLData addPLCategorysB2BMenuCat(DataSet plSearchData, SearchPLData baseData)
        {
            List<plCategoryData> plData = new List<plCategoryData>();
            DataTable Categorys = plSearchData.Tables["Category"];
            foreach (DataRow r in Categorys.Rows)
            {
                plCategoryData plCatRecord = new plCategoryData();
                plCatRecord.Code = r["Code"].ToString();
                plCatRecord.Name = r["Name"].ToString();
                plCatRecord.NameL = r["NameL"].ToString();
                plCatRecord.Country = (int)r["Country"];
                plCatRecord.PLCountry = Conversion.getInt32OrNull(r["PL_Country"]);
                plCatRecord.City = (int)r["City"];
                plCatRecord.PLCity = Conversion.getInt32OrNull(r["PL_City"]);
                plCatRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plCatRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                if (Categorys.Columns.Contains("Direction"))
                    plCatRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                if (Categorys.Columns.Contains("Category"))
                    plCatRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plCatRecord);
            }
            baseData.plCategorys = plData;
            return baseData;
        }

        public SearchPLData addPLHolPacks(DataSet plSearchData, SearchPLData baseData)
        {
            List<plHolPackData> plData = new List<plHolPackData>();
            DataTable HolPacks = plSearchData.Tables["HolPack"];
            foreach (DataRow r in HolPacks.Rows)
            {
                plHolPackData plRecord = new plHolPackData();
                plRecord.HolPack = r["HolPack"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.DepCity = (int)r["DepCity"];
                plRecord.ArrCity = (int)r["ArrCity"];
                plRecord.Country = (int)r["Country"];
                plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.plHolPacks = plData;
            return baseData;
        }

        public srchFilterData addPLHolPacksV2(DataSet plSearchData, srchFilterData baseData)
        {
            List<srchHolPack> plData = new List<srchHolPack>();
            DataTable HolPacks = plSearchData.Tables["HolPack"];
            foreach (DataRow r in HolPacks.Rows)
            {
                srchHolPack plRecord = new srchHolPack();
                plRecord.HolPack = Conversion.getStrOrNull(r["HolPack"]);
                plRecord.Name = Conversion.getStrOrNull(r["Name"]);
                plRecord.NameL = Conversion.getStrOrNull(r["NameL"]);
                plRecord.DepCity = Conversion.getInt32OrNull(r["DepCity"]);
                plRecord.ArrCity = Conversion.getInt32OrNull(r["ArrCity"]);
                plRecord.Country = Conversion.getInt32OrNull(r["Country"]);
                if (HolPacks.Columns.Contains("Direction"))
                    plRecord.Direction = Conversion.getInt32OrNull(r["Direction"]);
                if (HolPacks.Columns.Contains("Category"))
                    plRecord.Category = Conversion.getInt32OrNull(r["Category"]);
                if (HolPacks.Columns.Contains("CatPackID"))
                    plRecord.CatPackID = Conversion.getInt32OrNull(r["CatPackID"]);
                if (HolPacks.Columns.Contains("PackType"))
                    plRecord.PackType = Conversion.getStrOrNull(r["PackType"]);
                plData.Add(plRecord);
            }
            baseData.SrchHolPack = plData;
            return baseData;
        }

        public SearchPLData addPLHolPacksB2BMenuCat(DataSet plSearchData, SearchPLData baseData)
        {
            List<plHolPackData> plData = new List<plHolPackData>();
            DataTable HolPacks = plSearchData.Tables["HolPack"];
            foreach (DataRow r in HolPacks.Rows)
            {
                plHolPackData plRecord = new plHolPackData();
                plRecord.HolPack = r["HolPack"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.DepCity = (int)r["DepCity"];
                plRecord.ArrCity = (int)r["ArrCity"];
                plRecord.Country = (int)r["Country"];
                if (HolPacks.Columns.Contains("Direction"))
                    plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                if (HolPacks.Columns.Contains("Category"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.plHolPacks = plData;
            return baseData;
        }

        public SearchPLData addPLBoards(DataSet plSearchData, SearchPLData baseData)
        {
            List<plBoardData> plData = new List<plBoardData>();
            DataTable Boards = plSearchData.Tables["Board"];
            foreach (DataRow r in Boards.Rows)
            {
                plBoardData plRecord = new plBoardData();
                plRecord.Code = r["Code"].ToString();
                plRecord.Hotel_Code = r["Hotel_Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.City = (int)r["City"];
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plRecord.PLDepCity = (int)r["PLDepCity"];
                plRecord.PLArrCity = (int)r["PLArrCity"];
                plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.plBoards = plData;
            return baseData;
        }

        public srchFilterData addPLBoardsV2(DataSet plSearchData, srchFilterData baseData)
        {
            List<srchBoards> plData = new List<srchBoards>();
            DataTable Boards = plSearchData.Tables["Board"];
            foreach (DataRow r in Boards.Rows)
            {
                srchBoards plRecord = new srchBoards();
                plRecord.Code = Conversion.getStrOrNull(r["Code"]);
                plRecord.Hotel_Code = Conversion.getStrOrNull(r["Hotel_Code"]);
                plRecord.Name = Conversion.getStrOrNull(r["Name"]);
                plRecord.NameL = Conversion.getStrOrNull(r["NameL"]);
                plRecord.City = Conversion.getInt32OrNull(r["City"]);
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plRecord.PLDepCity = Conversion.getInt32OrNull(r["PLDepCity"]);
                plRecord.PLArrCity = Conversion.getInt32OrNull(r["PLArrCity"]);
                if (Boards.Columns.Contains(""))
                    plRecord.Direction = Conversion.getInt32OrNull(r["Direction"]);
                if (Boards.Columns.Contains(""))
                    plRecord.Category = Conversion.getInt32OrNull(r["Category"]);
                if (Boards.Columns.Contains(""))
                    plRecord.PackType = Conversion.getStrOrNull(r["PackType"]);
                plData.Add(plRecord);
            }
            baseData.SrchBoards = plData;
            return baseData;
        }

        public SearchPLData addPLBoardsB2BMenuCat(DataSet plSearchData, SearchPLData baseData)
        {
            List<plBoardData> plData = new List<plBoardData>();
            DataTable Boards = plSearchData.Tables["Board"];
            foreach (DataRow r in Boards.Rows)
            {
                plBoardData plRecord = new plBoardData();
                plRecord.Code = r["Code"].ToString();
                plRecord.Hotel_Code = r["Hotel_Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.City = (int)r["City"];
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plRecord.PLDepCity = (int)r["PLDepCity"];
                plRecord.PLArrCity = (int)r["PLArrCity"];
                if (Boards.Columns.Contains("Direction"))
                    plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                if (Boards.Columns.Contains("Category"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.plBoards = plData;
            return baseData;
        }

        public SearchPLData addPLRooms(DataSet plSearchData, SearchPLData baseData)
        {
            List<plRoomData> plData = new List<plRoomData>();
            DataTable Rooms = plSearchData.Tables["Room"];
            foreach (DataRow r in Rooms.Rows)
            {
                plRoomData plRecord = new plRoomData();
                plRecord.Code = r["Code"].ToString();
                plRecord.Hotel_Code = r["Hotel_Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.City = (int)r["City"];
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plRecord.PLDepCity = (int)r["PLDepCity"];
                plRecord.PLArrCity = (int)r["PLArrCity"];
                plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.plRooms = plData;
            return baseData;
        }

        public srchFilterData addPLRoomsV2(DataSet plSearchData, srchFilterData baseData)
        {
            List<srchRooms> plData = new List<srchRooms>();
            DataTable Rooms = plSearchData.Tables["Room"];
            foreach (DataRow r in Rooms.Rows)
            {
                srchRooms plRecord = new srchRooms();
                plRecord.Code = Conversion.getStrOrNull(r["Code"]);
                plRecord.Hotel_Code = Conversion.getStrOrNull(r["Hotel_Code"]);
                plRecord.Name = Conversion.getStrOrNull(r["Name"]);
                plRecord.NameL = Conversion.getStrOrNull(r["NameL"]);
                plRecord.City = Conversion.getInt32OrNull(r["City"]);
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plRecord.PLDepCity = Conversion.getInt32OrNull(r["PLDepCity"]);
                plRecord.PLArrCity = Conversion.getInt32OrNull(r["PLArrCity"]);
                if (Rooms.Columns.Contains("Direction"))
                    plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                if (Rooms.Columns.Contains("Category"))
                    plRecord.Category = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.SrchRooms = plData;
            return baseData;
        }

        public SearchPLData addPLRoomsB2BMenuCat(DataSet plSearchData, SearchPLData baseData)
        {
            List<plRoomData> plData = new List<plRoomData>();
            DataTable Rooms = plSearchData.Tables["Room"];
            foreach (DataRow r in Rooms.Rows)
            {
                plRoomData plRecord = new plRoomData();
                plRecord.Code = r["Code"].ToString();
                plRecord.Hotel_Code = r["Hotel_Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.City = (int)r["City"];
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plRecord.PLDepCity = (int)r["PLDepCity"];
                plRecord.PLArrCity = (int)r["PLArrCity"];
                if (Rooms.Columns.Contains("Direction"))
                    plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                if (Rooms.Columns.Contains("Category"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.plRooms = plData;
            return baseData;
        }

        public SearchPLData addPLResorts(DataSet plSearchData, SearchPLData baseData)
        {
            List<plResortData> plData = new List<plResortData>();
            DataTable Resorts = plSearchData.Tables["Resort"];
            foreach (DataRow r in Resorts.Rows)
            {
                plResortData plRecord = new plResortData();
                plRecord.RecID = Conversion.getInt32OrNull(r["RecID"]);
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.Country = (int)r["Country"];
                plRecord.City = (int)r["City"];
                plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.plResorts = plData;
            return baseData;
        }

        public srchFilterData addPLResortsV2(DataSet plSearchData, srchFilterData baseData)
        {
            List<srchResort> plData = new List<srchResort>();
            DataTable Resorts = plSearchData.Tables["Resort"];
            foreach (DataRow r in Resorts.Rows)
            {
                srchResort plRecord = new srchResort();
                plRecord.RecID = Conversion.getInt32OrNull(r["RecID"]);
                plRecord.Name = Conversion.getStrOrNull(r["Name"]);
                plRecord.NameL = Conversion.getStrOrNull(r["NameL"]);
                plRecord.Country = Conversion.getInt32OrNull(r["Country"]);
                plRecord.City = Conversion.getInt32OrNull(r["City"]);
                if (Resorts.Columns.Contains("Direction"))
                    plRecord.Direction = Conversion.getInt32OrNull(r["Direction"]);
                if (Resorts.Columns.Contains("Category"))
                    plRecord.Category = Conversion.getInt32OrNull(r["Category"]);
                if (Resorts.Columns.Contains("PackType"))
                    plRecord.PackType = Conversion.getStrOrNull(r["PackType"]);
                plData.Add(plRecord);
            }
            baseData.SrchResort = plData;
            return baseData;
        }

        public SearchPLData addPLResortsB2BMenuCat(DataSet plSearchData, SearchPLData baseData)
        {
            List<plResortData> plData = new List<plResortData>();
            DataTable Resorts = plSearchData.Tables["Resort"];
            foreach (DataRow r in Resorts.Rows)
            {
                plResortData plRecord = new plResortData();
                plRecord.RecID = Conversion.getInt32OrNull(r["RecID"]);
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.Country = (int)r["Country"];
                plRecord.City = (int)r["City"];
                if (Resorts.Columns.Contains("Direction"))
                    plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                if (Resorts.Columns.Contains("Category"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.plResorts = plData;
            return baseData;
        }

        public SearchPLData addPLHotels(DataSet plSearchData, SearchPLData baseData, List<Location> _Locations)
        {
            List<plHotelData> plData = new List<plHotelData>();
            DataTable Hotels = plSearchData.Tables["Hotels"];
            foreach (DataRow r in Hotels.Rows)
            {
                plHotelData plRecord = new plHotelData();
                plRecord.Code = r["Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.Category = r["Category"].ToString();
                plRecord.Location = (int)r["Location"];
                plRecord.LocationName = _Locations.Find(f => f.RecID == plRecord.Location) != null ? _Locations.Find(f => f.RecID == plRecord.Location).Name : "";
                plRecord.LocationNameL = _Locations.Find(f => f.RecID == plRecord.Location) != null ? _Locations.Find(f => f.RecID == plRecord.Location).NameL : "";
                plRecord.ArrCity = (int)r["ArrCity"];
                plRecord.ArrCityName = _Locations.Find(f => f.RecID == plRecord.ArrCity) != null ? _Locations.Find(f => f.RecID == plRecord.ArrCity).Name : "";
                plRecord.ArrCityNameL = _Locations.Find(f => f.RecID == plRecord.ArrCity) != null ? _Locations.Find(f => f.RecID == plRecord.ArrCity).NameL : "";
                plRecord.Country = (int)r["Country"];
                plRecord.CountryName = _Locations.Find(f => f.RecID == plRecord.Country) != null ? _Locations.Find(f => f.RecID == plRecord.Country).Name : "";
                plRecord.CountryNameL = _Locations.Find(f => f.RecID == plRecord.Country) != null ? _Locations.Find(f => f.RecID == plRecord.Country).NameL : "";
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.TownName = _Locations.Find(f => f.RecID == plRecord.Town) != null ? _Locations.Find(f => f.RecID == plRecord.Town).Name : "";
                plRecord.TownNameL = _Locations.Find(f => f.RecID == plRecord.Town) != null ? _Locations.Find(f => f.RecID == plRecord.Town).NameL : "";
                plRecord.Holpack = Conversion.getStrOrNull(r["Holpack"]);
                plRecord.HolpackName = baseData.plHolPacks.Find(f => f.HolPack == plRecord.Holpack) != null ? baseData.plHolPacks.Find(f => f.HolPack == plRecord.Holpack).Name : "";
                plRecord.HolpackNameL = baseData.plHolPacks.Find(f => f.HolPack == plRecord.Holpack) != null ? baseData.plHolPacks.Find(f => f.HolPack == plRecord.Holpack).NameL : "";
                plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                plRecord.HolPackCat = Conversion.getInt16OrNull(r["HolPackCat"]);
                plData.Add(plRecord);
            }
            baseData.plHotels = plData;
            return baseData;
        }

        public srchFilterData addPLHotelsV2(DataSet plSearchData, srchFilterData baseData, List<Location> _Locations)
        {
            List<srchHotels> plData = new List<srchHotels>();
            DataTable Hotels = plSearchData.Tables["Hotels"];
            foreach (DataRow r in Hotels.Rows)
            {
                srchHotels plRecord = new srchHotels();
                plRecord.Code = r["Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.Category = r["Category"].ToString();
                plRecord.Location = (int)r["Location"];
                plRecord.LocationName = _Locations.Find(f => f.RecID == plRecord.Location) != null ? _Locations.Find(f => f.RecID == plRecord.Location).Name : "";
                plRecord.LocationNameL = _Locations.Find(f => f.RecID == plRecord.Location) != null ? _Locations.Find(f => f.RecID == plRecord.Location).NameL : "";
                plRecord.ArrCity = (int)r["ArrCity"];
                plRecord.ArrCityName = _Locations.Find(f => f.RecID == plRecord.ArrCity) != null ? _Locations.Find(f => f.RecID == plRecord.ArrCity).Name : "";
                plRecord.ArrCityNameL = _Locations.Find(f => f.RecID == plRecord.ArrCity) != null ? _Locations.Find(f => f.RecID == plRecord.ArrCity).NameL : "";
                plRecord.Country = (int)r["Country"];
                plRecord.CountryName = _Locations.Find(f => f.RecID == plRecord.Country) != null ? _Locations.Find(f => f.RecID == plRecord.Country).Name : "";
                plRecord.CountryNameL = _Locations.Find(f => f.RecID == plRecord.Country) != null ? _Locations.Find(f => f.RecID == plRecord.Country).NameL : "";
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.TownName = _Locations.Find(f => f.RecID == plRecord.Town) != null ? _Locations.Find(f => f.RecID == plRecord.Town).Name : "";
                plRecord.TownNameL = _Locations.Find(f => f.RecID == plRecord.Town) != null ? _Locations.Find(f => f.RecID == plRecord.Town).NameL : "";
                plRecord.Holpack = Conversion.getStrOrNull(r["Holpack"]);
                plRecord.HolpackName = baseData.SrchHolPack.Find(f => f.HolPack == plRecord.Holpack) != null ? baseData.SrchHolPack.Find(f => f.HolPack == plRecord.Holpack).Name : "";
                plRecord.HolpackNameL = baseData.SrchHolPack.Find(f => f.HolPack == plRecord.Holpack) != null ? baseData.SrchHolPack.Find(f => f.HolPack == plRecord.Holpack).NameL : "";
                if (Hotels.Columns.Contains("Direction"))
                    plRecord.Direction = Conversion.getInt32OrNull(r["Direction"]);
                if (Hotels.Columns.Contains("HolPackCat"))
                    plRecord.HolPackCat = Conversion.getInt32OrNull(r["HolPackCat"]);
                if (Hotels.Columns.Contains("PackType"))
                    plRecord.PackType = Conversion.getStrOrNull(r["PackType"]);
                plData.Add(plRecord);
            }
            baseData.SrchHotels = plData;
            return baseData;
        }

        public SearchPLData addPLHotelsB2BMenuCat(DataSet plSearchData, SearchPLData baseData, List<Location> _Locations)
        {
            List<plHotelData> plData = new List<plHotelData>();
            DataTable Hotels = plSearchData.Tables["Hotels"];
            foreach (DataRow r in Hotels.Rows)
            {
                plHotelData plRecord = new plHotelData();
                plRecord.Code = r["Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.Category = r["Category"].ToString();
                plRecord.Holpack = r["Holpack"].ToString();
                plRecord.Location = (int)r["Location"];
                plRecord.LocationName = _Locations.Find(f => f.RecID == plRecord.Location) != null ? _Locations.Find(f => f.RecID == plRecord.Location).Name : "";
                plRecord.LocationNameL = _Locations.Find(f => f.RecID == plRecord.Location) != null ? _Locations.Find(f => f.RecID == plRecord.Location).NameL : "";
                plRecord.ArrCity = (int)r["ArrCity"];
                plRecord.ArrCityName = _Locations.Find(f => f.RecID == plRecord.ArrCity) != null ? _Locations.Find(f => f.RecID == plRecord.ArrCity).Name : "";
                plRecord.ArrCityNameL = _Locations.Find(f => f.RecID == plRecord.ArrCity) != null ? _Locations.Find(f => f.RecID == plRecord.ArrCity).NameL : "";
                plRecord.Country = (int)r["Country"];
                plRecord.CountryName = _Locations.Find(f => f.RecID == plRecord.Country) != null ? _Locations.Find(f => f.RecID == plRecord.Country).Name : "";
                plRecord.CountryNameL = _Locations.Find(f => f.RecID == plRecord.Country) != null ? _Locations.Find(f => f.RecID == plRecord.Country).NameL : "";
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.TownName = _Locations.Find(f => f.RecID == plRecord.Town) != null ? _Locations.Find(f => f.RecID == plRecord.Town).Name : "";
                plRecord.TownNameL = _Locations.Find(f => f.RecID == plRecord.Town) != null ? _Locations.Find(f => f.RecID == plRecord.Town).NameL : "";
                plRecord.CatPackID = Conversion.getInt32OrNull(r["CatPackID"]);
                if (Hotels.Columns.Contains("Direction"))
                    plRecord.Direction = Conversion.getInt16OrNull(r["Direction"]);
                if (Hotels.Columns.Contains("HolPackCat"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["HolPackCat"]);
                plData.Add(plRecord);
            }
            baseData.plHotels = plData;
            return baseData;
        }

        public SearchPLData addPLHolPackCat(DataSet plSearchData, SearchPLData baseData)
        {
            List<HolPackCatRecord> plData = new List<HolPackCatRecord>();
            if (!plSearchData.Tables.Contains("HolPackCat"))
                return baseData;
            DataTable HolPackCat = plSearchData.Tables["HolPackCat"];
            foreach (DataRow R in HolPackCat.Rows)
            {
                HolPackCatRecord plRecord = new HolPackCatRecord();
                plRecord.RecID = Conversion.getInt32OrNull(R["RecID"]);
                plRecord.Direction = Conversion.getInt16OrNull(R["Direction"]);
                plRecord.Code = Conversion.getInt16OrNull(R["Code"]);
                plRecord.Name = Conversion.getStrOrNull(R["Name"]);
                plRecord.NameL = Conversion.getStrOrNull(R["NameL"]);
                plData.Add(plRecord);
            }
            baseData.PlHolPackCat = plData;
            return baseData;
        }

        public srchFilterData addPLHolPackCatV2(DataSet plSearchData, srchFilterData baseData)
        {
            List<srchHolPackCat> plData = new List<srchHolPackCat>();
            if (!plSearchData.Tables.Contains("HolPackCat"))
                return baseData;
            DataTable HolPackCat = plSearchData.Tables["HolPackCat"];
            foreach (DataRow R in HolPackCat.Rows)
            {
                srchHolPackCat plRecord = new srchHolPackCat();
                plRecord.RecID = Conversion.getInt32OrNull(R["RecID"]);
                plRecord.Direction = Conversion.getInt32OrNull(R["Direction"]);
                plRecord.Code = Conversion.getInt16OrNull(R["Code"]);
                plRecord.Name = Conversion.getStrOrNull(R["Name"]);
                plRecord.NameL = Conversion.getStrOrNull(R["NameL"]);
                plData.Add(plRecord);
            }
            baseData.SrchHolPackCat = plData;
            return baseData;
        }

        public SearchPLData addPLHolPackCatB2BMenuCat(DataSet plSearchData, SearchPLData baseData)
        {
            List<HolPackCatRecord> plData = new List<HolPackCatRecord>();
            if (!plSearchData.Tables.Contains("HolPackCat"))
                return baseData;
            DataTable HolPackCat = plSearchData.Tables["HolPackCat"];
            foreach (DataRow R in HolPackCat.Rows)
            {
                HolPackCatRecord plRecord = new HolPackCatRecord();
                plRecord.RecID = Conversion.getInt32OrNull(R["RecID"]);
                plRecord.Direction = Conversion.getInt16OrNull(R["Direction"]);
                plRecord.Code = Conversion.getInt16OrNull(R["Code"]);
                plRecord.Name = Conversion.getStrOrNull(R["Name"]);
                plRecord.NameL = Conversion.getStrOrNull(R["NameL"]);
                plData.Add(plRecord);
            }
            baseData.PlHolPackCat = plData;
            return baseData;
        }

        public SearchPLData addPLMaxPax(DataSet plSearchData, SearchPLData baseData)
        {
            plMaxPaxCounts plMaxPax = new plMaxPaxCounts();
            if (!plSearchData.Tables.Contains("MaxPaxCount"))
                return baseData;
            DataTable MaxPaxT = plSearchData.Tables["MaxPaxCount"];
            plMaxPax.maxAdult = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["Adult"]);
            plMaxPax.maxChdAgeG1 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG1"]);
            plMaxPax.maxChdAgeG2 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG2"]);
            plMaxPax.maxChdAgeG3 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG3"]);
            plMaxPax.maxChdAgeG4 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG4"]);
            List<int?> maxChd = new List<int?>();
            maxChd.Add(plMaxPax.maxChdAgeG1);
            maxChd.Add(plMaxPax.maxChdAgeG2);
            maxChd.Add(plMaxPax.maxChdAgeG3);
            maxChd.Add(plMaxPax.maxChdAgeG4);
            plMaxPax.maxChd = maxChd.OrderBy(o => (o.HasValue ? o.Value : 4)).LastOrDefault().Value;
            baseData.PlMaxPaxCount = plMaxPax;
            return baseData;
        }

        public SearchPLData addPLFacility(DataSet plSearchData, SearchPLData baseData)
        {
            List<plFacilityData> plData = new List<plFacilityData>();
            if (!plSearchData.Tables.Contains("Facility"))
                return baseData;
            DataTable Facility = plSearchData.Tables["Facility"];
            foreach (DataRow R in Facility.Rows)
            {
                plFacilityData plRecord = new plFacilityData();
                plRecord.Code = Conversion.getStrOrNull(R["Code"]);
                plRecord.Name = Conversion.getStrOrNull(R["Name"]);
                plRecord.NameL = Conversion.getStrOrNull(R["NameL"]);
                plRecord.City = Conversion.getInt32OrNull(R["City"]);
                plRecord.Direction = Conversion.getInt16OrNull(R["Direction"]);
                plRecord.HolPackCat = Conversion.getInt16OrNull(R["Category"]);
                plRecord.Hotel_Code = Conversion.getStrOrNull(R["Hotel_Code"]);
                plRecord.PLArrCity = Conversion.getInt32OrNull(R["PLArrCity"]);
                plRecord.PLDepCity = Conversion.getInt32OrNull(R["PLDepCity"]);
                plRecord.Town = Conversion.getInt32OrNull(R["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(R["Village"]);

                plData.Add(plRecord);
            }
            baseData.PlFacility = plData;
            return baseData;
        }

        public srchFilterData addPLMaxPaxV2(DataSet plSearchData, srchFilterData baseData)
        {
            List<srchMaxPaxCounts> plMaxPax = new List<srchMaxPaxCounts>();
            if (!plSearchData.Tables.Contains("MaxPaxCount"))
                return baseData;
            DataTable MaxPaxT = plSearchData.Tables["MaxPaxCount"];
            foreach (DataRow R in MaxPaxT.Rows)
            {
                srchMaxPaxCounts plRecord = new srchMaxPaxCounts();
                plRecord.Adult = Conversion.getInt32OrNull(R["Adult"]);
                plRecord.ChdAgeG1 = Conversion.getInt32OrNull(R["ChdAgeG1"]);
                plRecord.ChdAgeG2 = Conversion.getInt32OrNull(R["ChdAgeG2"]);
                plRecord.ChdAgeG3 = Conversion.getInt32OrNull(R["ChdAgeG3"]);
                plRecord.ChdAgeG4 = Conversion.getInt32OrNull(R["ChdAgeG4"]);
                if (MaxPaxT.Columns.Contains("PackType"))
                    plRecord.PackType = Conversion.getStrOrNull(R["PackType"]);
                plMaxPax.Add(plRecord);
            }
            baseData.SrchMaxPaxCounts = plMaxPax;
            return baseData;
        }

        public SearchPLData addPLMaxPaxB2BMenuCat(DataSet plSearchData, SearchPLData baseData)
        {
            plMaxPaxCounts plMaxPax = new plMaxPaxCounts();
            if (!plSearchData.Tables.Contains("MaxPaxCount"))
                return baseData;
            DataTable MaxPaxT = plSearchData.Tables["MaxPaxCount"];
            plMaxPax.maxAdult = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["Adult"]);
            plMaxPax.maxChdAgeG1 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG1"]);
            plMaxPax.maxChdAgeG2 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG2"]);
            plMaxPax.maxChdAgeG3 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG3"]);
            plMaxPax.maxChdAgeG4 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG4"]);
            List<int?> maxChd = new List<int?>();
            maxChd.Add(plMaxPax.maxChdAgeG1);
            maxChd.Add(plMaxPax.maxChdAgeG2);
            maxChd.Add(plMaxPax.maxChdAgeG3);
            maxChd.Add(plMaxPax.maxChdAgeG4);
            plMaxPax.maxChd = maxChd.OrderBy(o => (o.HasValue ? o.Value : 4)).LastOrDefault().Value;
            baseData.PlMaxPaxCount = plMaxPax;
            return baseData;
        }

        public List<HolPackCatRecord> PLHolPackCatList(User UserData, ref string errorMsg)
        {
            List<HolPackCatRecord> records = new List<HolPackCatRecord>();
            string tsql = @"Select Distinct HPC.RecID, HPC.Direction, HPC.Code, HPC.Name, NameL=isnull(dbo.FindLocalName(HPC.NameLID, @Market), HPC.Name)
                            From HolpackCat HPC (NOLOCK)
                            Join HolPack HP (NOLOCK) ON HP.Direction = HPC.Direction And HP.Category = HPC.Code
                            Join CatalogPack CP (NOLOCK) ON CP.HolPack = HP.Code
                            Where   CP.Ready = 'Y' 
                                And CP.WebPub = 'Y' 	
                                And CP.PackType = 'T'
                                And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate		                        
	                            And (not Exists(Select * from CatPackAgency (NOLOCK) Where CatPackID=CP.RecID) or
		                                 Exists(Select * from CatPackAgency (NOLOCK) Where CatPackID=CP.RecID and Agency=@Agency))
	                            And Exists(Select * From CatPackMarket (NOLOCK) Where CatPackID=CP.RecID AND Market=@Market  AND (Operator=@Operator OR Operator='')) ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HolPackCatRecord rec = new HolPackCatRecord();
                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.Direction = Conversion.getInt16OrNull(R["Direction"]);
                        rec.Code = Conversion.getInt16OrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HolPackCatRecord> getHolPackCatList(User UserData, ref string errorMsg)
        {
            List<HolPackCatRecord> records = new List<HolPackCatRecord>();
            string tsql = @"Select RecID, Direction, Code, Name,
	                            NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name)
                            From HolPackCat (NOLOCK)";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HolPackCatRecord rec = new HolPackCatRecord();
                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.Direction = Conversion.getInt16OrNull(R["Direction"]);
                        rec.Code = Conversion.getInt16OrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public StringBuilder getPricelistLocationDataForPackageSearch(string Operator, string Market, bool Groups, ref string errorMsg)
        {

            StringBuilder sql = new StringBuilder();
            sql.Append("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ");
            sql.Append("SET NOCOUNT ON ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CPM') is not null Drop Table dbo.#CPM ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Hotel') is not null Drop Table dbo.#Hotel ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatPriceA') is not null Drop Table dbo.#CatPriceA ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#HolPackCat') is not null Drop Table dbo.#HolPackCat \n");
            /*
                sql.Append("if OBJECT_ID('TempDB.dbo.#AgencyCP') is not null Drop Table dbo.#AgencyCP \n");
            
                sql.Append("Select Distinct RecID \n");
                sql.Append("Into #AgencyCP \n");
                sql.Append("From \n");
                sql.Append("( \n");
                sql.Append("    Select CP.RecID \n");
                sql.Append("    From CatalogPack CP (NOLOCK) \n");
                sql.Append("    Where \n");
                sql.Append("        Not Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID) or \n");
                sql.Append("        Exists(Select CA.RecID from CatPackAgency CA (NOLOCK) \n");
                sql.Append("               Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups=CA.AgencyGrp and AG.Agency=@Agency \n");
                sql.Append("               Where CA.CatPackID=CP.RecID and (AG.Agency=@Agency or CA.Agency=@Agency)) \n");
                sql.Append(") ACP \n");
            */
            //Aşağıdaki union Falcon TVB2B-2216
            sql.Append("Select * Into #CatalogPack from( ");
            sql.Append("Select CP.*,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name) ");
            sql.Append("From CatalogPack CP(NOLOCK) ");
            sql.Append("Inner Join CatPackAgency CPA(NOLOCK) On CP.RecId=CPA.CatPackId ");
            sql.Append("Where CP.ReadY='Y' ");
            sql.Append("And CP.WebPub='Y' And CP.WebPubDate<=GetDate() ");
            sql.Append("And CP.PackType='H' ");
            sql.Append("And (isnull(CP.B2BMenuCat,'')='' Or CP.B2BMenuCat='H') ");
            sql.Append("And EndDate > GetDate()-1 ");
            sql.Append("And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("And CPA.Agency=@Agency ");
            sql.Append("UNION ALL ");
            sql.Append("Select CP.*,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name) ");
            sql.Append("From CatalogPack CP(NOLOCK) ");
            sql.Append("Where CP.ReadY='Y' ");
            sql.Append("And CP.WebPub='Y' And CP.WebPubDate<=GetDate() ");
            sql.Append("And CP.PackType='H' ");
            sql.Append("And (isnull(CP.B2BMenuCat,'')='' Or CP.B2BMenuCat='H') ");
            sql.Append("And EndDate > GetDate()-1 ");
            sql.Append("And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            //TVB2B-6666
            //sql.Append("And NOT EXISTS(select null from CatPackAgency CPA(NOLOCK) where CPA.CatPackID=CP.RecID) ");
            sql.Append(") as ACP ");
            sql.Append("Select Direction,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) ");
            sql.Append("Into #HolPackCat ");
            sql.Append("From HolPackCat \n");
            sql.Append("Select Distinct CatPackID,Hotel ");
            sql.Append("Into #CatPriceA ");
            sql.Append("From CatPriceA A ");
            sql.Append("Join #CatalogPack CP on CP.RecID=A.CatPAckID ");
            sql.Append("Where A.Status=1 \n");
            sql.Append("Select CPM.RecID,CPM.CatPackID,CPM.Market,CPM.Operator ");
            sql.Append("Into #CPM ");
            sql.Append("From CatPackMarket CPM ");
            sql.Append("Join #CatalogPack CP ON CP.RecID=CPM.CatPackID ");
            sql.Append("Where CPM.Market=@Market ");
            sql.Append(" And (CPM.Operator=@Operator Or CPM.Operator='') \n");
            sql.Append("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] ");
            sql.Append("Into #Location ");
            sql.Append("From Location \n");
            sql.Append("Select [Name],NameL,Code,HotelType,Category,Location ");
            sql.Append("Into #Hotel ");
            sql.Append("From ");
            sql.Append("( ");
            sql.Append("    Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),H.HotelType,H.Category,H.Location ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
            sql.Append("    Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") H ");
            sql.Append("Group By [Name],NameL,Code,HotelType,Category,Location \n");
            sql.Append("--Locations \n");
            sql.Append("Select DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL ");
            sql.Append("From ( ");
            sql.Append("    SELECT CP.DepCity, ");
            sql.Append("        DepCityName=(Select Name From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        DepCityNameL=(Select NameL From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        ArrCountry=(Select Country From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCountryName=(Select Name From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        ArrCountryNameL=(Select NameL From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        CP.ArrCity, ");
            sql.Append("        ArrCityName=(Select Name From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCityNameL=(Select NameL From #Location L Where RecID=CP.ArrCity) ");
            sql.Append("    FROM #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON CPA.Hotel=H.Code ");
            sql.Append("    JOIN #Location L ON L.RecID=(Select Country From #Location Where RecID=H.Location) ");
            sql.Append("    LEFT JOIN #HolPackCat HPC ON HPC.Code=CP.Category And HPC.Direction=CP.Direction ");
            sql.Append("    WHERE 	dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("        And Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") Locations ");
            sql.Append("Group By DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityNameL,ArrCityName \n");
            sql.Append("--Category \n");
            sql.Append("SELECT Distinct HC.Code,HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID, @Market),HC.Name),L.Country, ");
            sql.Append("    PL_Country=(Select Country From #Location Where RecID=CP.ArrCity), ");
            sql.Append("    L.City,PL_City=CP.ArrCity,L.Town,L.Village ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("JOIN HotelCat HC  ON HC.Code=H.Category ");
            sql.Append("JOIN #Location L ON L.RecID=H.Location \n");
            sql.Append("--HolPack \n");
            sql.Append("Select [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID ");
            sql.Append("From ( ");
            sql.Append("    Select CP.HolPack, ");
            sql.Append("        Name=H.Name +  ");
            sql.Append("        ' ( ' + (Select Name From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select Name From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) +  ");
            sql.Append("        ' ( ' + (Select NameL From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select NameL From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        CP.DepCity , CP.ArrCity, L.Country, CatPackID=CP.RecID ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join HolPack H  ON H.Code=CP.HolPack ");
            sql.Append("    Join #Location L ON L.RecID=CP.ArrCity ");
            sql.Append("    Where  Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append("      And isnull(H.Ready,'N')='Y' ");
            sql.Append("      And isnull(H.WebPub,'N')='Y' ");
            sql.Append(") Holpacks ");
            sql.Append("Group By [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID \n");
            if (Groups)
            {
                #region with groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GBD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    JOIN GrpBoardDet  GBD ON GBD.Board=HB.Code ");
                sql.Append("    JOIN GrpBoard  GB ON GB.Code=GBD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GRD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR  ON HR.Hotel=H.Code ");
                sql.Append("    JOIN GrpRoomDet  GRD ON GRD.Room=HR.Code ");
                sql.Append("    JOIN GrpRoom  GB ON GB.Code=GRD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            else
            {
                #region without groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HB.Code,HB.Name,NameL=isnull(dbo.FindLocalName(HB.NameLID,@Market),HB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HR.Code,HR.Name,NameL=isnull(dbo.FindLocalName(HR.NameLID,@Market),HR.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR ON HR.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            sql.Append("--Resort \n");
            sql.Append("Select [Name],NameL,RecID,Country,City ");
            sql.Append("From ( ");
            sql.Append("    Select RecID=L.RecID,Name=(Select Name From #Location Where RecID=L.RecID), NameL=(Select NameL From #Location Where RecID=L.RecID),");
            sql.Append("         L.Country,City=CP.ArrCity ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append("    Where Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append(") Resort ");
            sql.Append("Group By [Name],NameL,RecID,Country,City \n");
            sql.Append("--Hotels \n");
            sql.Append("Select [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack ");
            sql.Append("From ( ");
            sql.Append("    Select H.Code,Name=H.Name,NameL=H.NameL,H.Category,H.Location,L.Country,L.City, ");
            sql.Append("		Town=isnull(L.Town,-1),CP.ArrCity,CatPackID=CP.RecID,CP.Holpack ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append(") Hotels ");
            sql.Append("Group By [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack \n");
            sql.Append("-- Max Pax Count \n");
            sql.Append("Select Adult=Max(isnull(Adult,6)),ChdAgeG1=Max(isnull(ChdAgeG1,4)),ChdAgeG2=Max(isnull(ChdAgeG2,4)),ChdAgeG3=Max(isnull(ChdAgeG3,4)),ChdAgeG4=Max(isnull(ChdAgeG4,4)) ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("Join CatPriceA CPA  ON CP.RecID=CPA.CatPackID And CPA.Status=1 ");
            sql.Append("Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("Join HotelAccomPax HAP  on HAP.Hotel=CPA.Hotel and HAP.Room=CPA.Room and HAP.Accom=CPA.Accom and IsNull(HAP.VisiblePL,'Y')='Y' ");
            return sql;
        }

        public StringBuilder getPricelistLocationDataForOnlyHotelSearch(string Operator, string Market, bool Groups, ref string errorMsg)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ");
            sql.Append("SET NOCOUNT ON ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CPM') is not null Drop Table dbo.#CPM ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Hotel') is not null Drop Table dbo.#Hotel ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatPriceA') is not null Drop Table dbo.#CatPriceA ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#HolPackCat') is not null Drop Table dbo.#HolPackCat \n");
            sql.Append("Select *,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name)  Into #CatalogPack ");
            sql.Append("From CatalogPack CP ");
            sql.Append("Where CP.ReadY='Y' ");
            sql.Append("  And CP.WebPub='Y' And CP.WebPubDate<=GetDate() ");
            sql.Append("  And CP.PackType='O' ");
            sql.Append("  And (isnull(CP.B2BMenuCat,'')='' Or CP.B2BMenuCat='O') ");
            sql.Append("  And EndDate > GetDate()-1 ");
            sql.Append("  And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("Select Direction,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) ");
            sql.Append("Into #HolPackCat ");
            sql.Append("From HolPackCat ");
            sql.Append("Select Distinct CatPackID,Hotel ");
            sql.Append("Into #CatPriceA ");
            sql.Append("From CatPriceA A ");
            sql.Append("Join #CatalogPack CP on CP.RecID=A.CatPAckID ");
            sql.Append("Where A.Status=1 ");
            sql.Append("Select CPM.RecID,CPM.CatPackID,CPM.Market,CPM.Operator ");
            sql.Append("Into #CPM ");
            sql.Append("From CatPackMarket CPM ");
            sql.Append("Join #CatalogPack CP ON CP.RecID=CPM.CatPackID ");
            sql.Append("Where CPM.Market=@Market ");
            sql.Append(" And (CPM.Operator=@Operator Or CPM.Operator='') ");
            sql.Append("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] ");
            sql.Append("Into #Location ");
            sql.Append("From Location ");
            sql.Append("Select [Name],NameL,Code,HotelType,Category,Location ");
            sql.Append("Into #Hotel ");
            sql.Append("From ");
            sql.Append("( ");
            sql.Append("    Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),H.HotelType,H.Category,H.Location ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
            sql.Append("    Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") H ");
            sql.Append("Group By [Name],NameL,Code,HotelType,Category,Location \n");
            sql.Append("--Locations \n");
            sql.Append("Select DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL ");
            sql.Append("From ( ");
            sql.Append("    SELECT CP.DepCity, ");
            sql.Append("        DepCityName=(Select Name From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        DepCityNameL=(Select NameL From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        ArrCountry=(Select Country From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCountryName=(Select Name From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        ArrCountryNameL=(Select NameL From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        CP.ArrCity, ");
            sql.Append("        ArrCityName=(Select Name From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCityNameL=(Select NameL From #Location L Where RecID=CP.ArrCity) ");
            sql.Append("    FROM #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON CPA.Hotel=H.Code ");
            sql.Append("    JOIN #Location L ON L.RecID=(Select Country From #Location Where RecID=H.Location) ");
            sql.Append("    LEFT JOIN #HolPackCat HPC ON HPC.Code=CP.Category And HPC.Direction=CP.Direction ");
            sql.Append("    WHERE 	dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("        And Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") Locations ");
            sql.Append("Group By DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL \n");
            sql.Append("--Category \n");
            sql.Append("SELECT Distinct HC.Code,HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID, @Market),HC.Name),L.Country, ");
            sql.Append("    PL_Country=(Select Country From #Location Where RecID=CP.ArrCity), ");
            sql.Append("    L.City, PL_City=CP.ArrCity, L.Town, L.Village ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("JOIN HotelCat HC  ON HC.Code=H.Category ");
            sql.Append("JOIN #Location L ON L.RecID=H.Location \n");
            sql.Append("--HolPack \n");
            sql.Append("Select [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID ");
            sql.Append("From ( ");
            sql.Append("    Select CP.HolPack, ");
            sql.Append("        Name=H.Name +  ");
            sql.Append("        ' ( ' + (Select Name From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select Name From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) +  ");
            sql.Append("        ' ( ' + (Select NameL From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select NameL From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        CP.DepCity , CP.ArrCity, L.Country, CatPackID=CP.RecID ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join HolPack H  ON H.Code=CP.HolPack ");
            sql.Append("    Join #Location L ON L.RecID=CP.ArrCity ");
            sql.Append("    Where  Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append("      And isnull(H.Ready,'N')='Y' ");
            sql.Append("      And isnull(H.WebPub,'N')='Y' ");
            sql.Append(") Holpacks ");
            sql.Append("Group By [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID \n");
            if (Groups)
            {
                #region with groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GBD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    JOIN GrpBoardDet  GBD ON GBD.Board=HB.Code ");
                sql.Append("    JOIN GrpBoard  GB ON GB.Code=GBD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GRD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR  ON HR.Hotel=H.Code ");
                sql.Append("    JOIN GrpRoomDet  GRD ON GRD.Room=HR.Code ");
                sql.Append("    JOIN GrpRoom  GB ON GB.Code=GRD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            else
            {
                #region without groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HB.Code,HB.Name,NameL=isnull(dbo.FindLocalName(HB.NameLID,@Market),HB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HR.Code,HR.Name,NameL=isnull(dbo.FindLocalName(HR.NameLID,@Market),HR.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR ON HR.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            sql.Append("--Resort \n");
            sql.Append("Select [Name],NameL,RecID,Country,City ");
            sql.Append("From ( ");
            sql.Append("    Select RecID=L.RecID,Name=(Select Name From #Location Where RecID=L.RecID),NameL=(Select NameL From #Location Where RecID=L.RecID), ");
            sql.Append("         L.Country,City=CP.ArrCity ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append("    Where Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append(") Resort ");
            sql.Append("Group By [Name],NameL,RecID,Country,City \n");
            sql.Append("--Hotels \n");
            sql.Append("Select [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack ");
            sql.Append("From ( ");
            sql.Append("    Select H.Code,Name=H.Name,NameL=H.NameL,H.Category,H.Location,L.Country,L.City, ");
            sql.Append("		Town=isnull(L.Town,-1),CP.ArrCity,CatPackID=CP.RecID,CP.Holpack ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append(") Hotels ");
            sql.Append("Group By [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack \n");
            sql.Append("-- Max Pax Count \n");
            sql.Append("Select Adult=Max(isnull(Adult,6)),ChdAgeG1=Max(isnull(ChdAgeG1,4)),ChdAgeG2=Max(isnull(ChdAgeG2,4)),ChdAgeG3=Max(isnull(ChdAgeG3,4)),ChdAgeG4=Max(isnull(ChdAgeG4,4)) ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("Join CatPriceA CPA  ON CP.RecID=CPA.CatPackID And CPA.Status=1 ");
            sql.Append("Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("Join HotelAccomPax HAP  on HAP.Hotel=CPA.Hotel and HAP.Room=CPA.Room and HAP.Accom=CPA.Accom and IsNull(HAP.VisiblePL,'Y')='Y' ");
            return sql;
        }

        public StringBuilder getPricelistLocationDataForCruiseSearch(string Operator, string Market, bool Groups, ref string errorMsg)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ");
            sql.Append("SET NOCOUNT ON ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CPM') is not null Drop Table dbo.#CPM ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Hotel') is not null Drop Table dbo.#Hotel ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatPriceA') is not null Drop Table dbo.#CatPriceA ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#HolPackCat') is not null Drop Table dbo.#HolPackCat \n");
            sql.Append("Select *,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name)  Into #CatalogPack ");
            sql.Append("From CatalogPack CP ");
            sql.Append("Where CP.ReadY='Y' ");
            sql.Append("  And CP.WebPub='Y' And CP.WebPubDate<=GetDate() ");
            sql.Append("  And CP.PackType='U' ");
            sql.Append("  And (isnull(CP.B2BMenuCat,'')='' Or CP.B2BMenuCat='U') ");
            sql.Append("  And EndDate > GetDate()-1 ");
            sql.Append("  And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("Select Direction,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) ");
            sql.Append("Into #HolPackCat ");
            sql.Append("From HolPackCat ");
            sql.Append("Select Distinct CatPackID,Hotel ");
            sql.Append("Into #CatPriceA ");
            sql.Append("From CatPriceA A ");
            sql.Append("Join #CatalogPack CP on CP.RecID=A.CatPAckID ");
            sql.Append("Where A.Status=1 ");
            sql.Append("Select CPM.RecID,CPM.CatPackID,CPM.Market,CPM.Operator ");
            sql.Append("Into #CPM ");
            sql.Append("From CatPackMarket CPM ");
            sql.Append("Join #CatalogPack CP ON CP.RecID=CPM.CatPackID ");
            sql.Append("Where CPM.Market=@Market ");
            sql.Append(" And (CPM.Operator=@Operator Or CPM.Operator='') ");
            sql.Append("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] ");
            sql.Append("Into #Location ");
            sql.Append("From Location ");
            sql.Append("Select [Name],NameL,Code,HotelType,Category,Location ");
            sql.Append("Into #Hotel ");
            sql.Append("From ");
            sql.Append("( ");
            sql.Append("    Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),H.HotelType,H.Category,H.Location ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
            sql.Append("    Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") H ");
            sql.Append("Group By [Name],NameL,Code,HotelType,Category,Location \n");
            sql.Append("--Locations \n");
            sql.Append("Select DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL ");
            sql.Append("From ( ");
            sql.Append("    SELECT CP.DepCity, ");
            sql.Append("        DepCityName=(Select Name From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        DepCityNameL=(Select NameL From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        ArrCountry=(Select Country From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCountryName=(Select Name From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        ArrCountryNameL=(Select NameL From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        CP.ArrCity, ");
            sql.Append("        ArrCityName=(Select Name From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCityNameL=(Select NameL From #Location L Where RecID=CP.ArrCity) ");
            sql.Append("    FROM #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON CPA.Hotel=H.Code ");
            sql.Append("    JOIN #Location L ON L.RecID=(Select Country From #Location Where RecID=H.Location) ");
            sql.Append("    LEFT JOIN #HolPackCat HPC ON HPC.Code=CP.Category And HPC.Direction=CP.Direction ");
            sql.Append("    WHERE 	dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("        And Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") Locations ");
            sql.Append("Group By DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL \n");
            sql.Append("--Category \n");
            sql.Append("SELECT Distinct HC.Code,HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID, @Market),HC.Name),L.Country, ");
            sql.Append("    PL_Country=(Select Country From #Location Where RecID=CP.ArrCity), ");
            sql.Append("    L.City, PL_City=CP.ArrCity, L.Town, L.Village ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("JOIN HotelCat HC  ON HC.Code=H.Category ");
            sql.Append("JOIN #Location L ON L.RecID=H.Location \n");
            sql.Append("--HolPack \n");
            sql.Append("Select [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID ");
            sql.Append("From ( ");
            sql.Append("    Select CP.HolPack, ");
            sql.Append("        Name=H.Name +  ");
            sql.Append("        ' ( ' + (Select Name From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select Name From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) +  ");
            sql.Append("        ' ( ' + (Select NameL From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select NameL From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        CP.DepCity , CP.ArrCity, L.Country, CatPackID=CP.RecID ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join HolPack H  ON H.Code=CP.HolPack ");
            sql.Append("    Join #Location L ON L.RecID=CP.ArrCity ");
            sql.Append("    Where  Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append("      And isnull(H.Ready,'N')='Y' ");
            sql.Append("      And isnull(H.WebPub,'N')='Y' ");
            sql.Append(") Holpacks ");
            sql.Append("Group By [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID \n");
            if (Groups)
            {
                #region with groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GBD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    JOIN GrpBoardDet  GBD ON GBD.Board=HB.Code ");
                sql.Append("    JOIN GrpBoard  GB ON GB.Code=GBD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GRD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR  ON HR.Hotel=H.Code ");
                sql.Append("    JOIN GrpRoomDet  GRD ON GRD.Room=HR.Code ");
                sql.Append("    JOIN GrpRoom  GB ON GB.Code=GRD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            else
            {
                #region without groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HB.Code,HB.Name,NameL=isnull(dbo.FindLocalName(HB.NameLID,@Market),HB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HR.Code,HR.Name,NameL=isnull(dbo.FindLocalName(HR.NameLID,@Market),HR.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR ON HR.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            sql.Append("--Resort \n");
            sql.Append("Select [Name],NameL,RecID,Country,City ");
            sql.Append("From ( ");
            sql.Append("    Select RecID=L.RecID,Name=(Select Name From #Location Where RecID=L.RecID),NameL=(Select NameL From #Location Where RecID=L.RecID), ");
            sql.Append("         L.Country,City=CP.ArrCity ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append("    Where Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append(") Resort ");
            sql.Append("Group By [Name],NameL,RecID,Country,City \n");
            sql.Append("--Hotels \n");
            sql.Append("Select [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack ");
            sql.Append("From ( ");
            sql.Append("    Select H.Code,Name=H.Name,NameL=H.NameL,H.Category,H.Location,L.Country,L.City, ");
            sql.Append("		Town=isnull(L.Town,-1),CP.ArrCity,CatPackID=CP.RecID,CP.Holpack ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append(") Hotels ");
            sql.Append("Group By [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack \n");
            sql.Append("-- Max Pax Count \n");
            sql.Append("Select Adult=Max(isnull(Adult,6)),ChdAgeG1=Max(isnull(ChdAgeG1,4)),ChdAgeG2=Max(isnull(ChdAgeG2,4)),ChdAgeG3=Max(isnull(ChdAgeG3,4)),ChdAgeG4=Max(isnull(ChdAgeG4,4)) ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("Join CatPriceA CPA  ON CP.RecID=CPA.CatPackID And CPA.Status=1 ");
            sql.Append("Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("Join HotelAccomPax HAP  on HAP.Hotel=CPA.Hotel and HAP.Room=CPA.Room and HAP.Accom=CPA.Accom and IsNull(HAP.VisiblePL,'Y')='Y' ");
            return sql;
        }

        public StringBuilder getPricelistLocationDataForOnlyHotelSearchV2(string Operator, string Market, bool Groups, ref string errorMsg)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ");
            sql.AppendLine("SET NOCOUNT ON ");

            sql.AppendLine("Declare @SaleDate DateTime");
            sql.AppendLine("Set @SaleDate=dbo.DateOnly(GetDate())");

            sql.AppendLine("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location ");
            sql.AppendLine("if OBJECT_ID('TempDB.dbo.#Hotel') is not null Drop Table dbo.#Hotel ");
            sql.AppendLine("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] ");
            sql.AppendLine("Into #Location ");
            sql.AppendLine("From Location ");
            sql.AppendLine("");
            sql.AppendLine("Select RecID,[Name],NameL,Code,HotelType,Category,Location ");
            sql.AppendLine("Into #Hotel ");
            sql.AppendLine("From ");
            sql.AppendLine("( ");
            sql.AppendLine("    Select H.RecID,H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),H.HotelType,H.Category,H.Location ");
            sql.AppendLine("    From OHotelSalePrice P    ");
            sql.AppendLine("    Join Hotel H ON H.RecID=P.HotelID");
            sql.AppendLine("    Where Exists(Select null From Market Where Code=@Market And RecID=P.MarketID)");
            sql.AppendLine("      And P.CheckIn>=@SaleDate");
            sql.AppendLine(") H ");
            sql.AppendLine("Group By RecID,[Name],NameL,Code,HotelType,Category,Location ");
            sql.AppendLine("");
            sql.AppendLine("--Locations ");
            sql.AppendLine("Select DepCity=-1,DepCityName='',DepCityNameL='',ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL ");
            sql.AppendLine("From ( ");
            sql.AppendLine("    SELECT         ");
            sql.AppendLine("        ArrCountry=Lc.RecID, ");
            sql.AppendLine("        ArrCountryName=Lc.Name, ");
            sql.AppendLine("        ArrCountryNameL=Lc.NameL, ");
            sql.AppendLine("        ArrCity=L.City, ");
            sql.AppendLine("        ArrCityName=Lcty.Name, ");
            sql.AppendLine("        ArrCityNameL=Lcty.NameL");
            sql.AppendLine("    From OHotelSalePrice P ");
            sql.AppendLine("    Join Hotel H ON H.RecID=P.HotelID");
            sql.AppendLine("    Join #Location L ON L.RecID=H.Location ");
            sql.AppendLine("    Left Join #Location Lc ON Lc.RecID=L.Country ");
            sql.AppendLine("    Left Join #Location Lcty ON Lcty.RecID=L.City ");
            sql.AppendLine("    WHERE Exists(Select null From Market Where Code=@Market And RecID=P.MarketID) ");
            sql.AppendLine("      And P.CheckIn>=@SaleDate ");
            sql.AppendLine(") Locations ");
            sql.AppendLine("Group By ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL ");
            sql.AppendLine("");
            sql.AppendLine("--Category ");
            sql.AppendLine("SELECT Distinct HC.Code,HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID, @Market),HC.Name),L.Country, ");
            sql.AppendLine("    PL_Country=L.Country,L.City,PL_City=L.City,L.Town,L.Village ");
            sql.AppendLine("From OHotelSalePrice P ");
            sql.AppendLine("JOIN #Hotel H ON H.RecID=P.HotelID ");
            sql.AppendLine("JOIN HotelCat HC  ON HC.Code=H.Category ");
            sql.AppendLine("JOIN #Location L ON L.RecID=H.Location ");
            sql.Append("--HolPack \n");
            sql.Append("Select Top 0 [Name]='',NameL='',HolPack='',DepCity=null,ArrCity=null,Country=null,CatPackID=null");

            if (Groups)
            {
                #region with groups
                sql.AppendLine("--Board ");
                sql.AppendLine("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.AppendLine("FROM ( ");
                sql.AppendLine("    SELECT Hotel_Code=H.Code,Code=GBD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.AppendLine("        PLArrCity=L.City,PLDepCity=-1 ");
                sql.AppendLine("    From OHotelSalePrice P ");
                sql.AppendLine("    JOIN #Hotel H ON H.RecID=P.HotelID");
                sql.AppendLine("    JOIN #Location L ON L.RecID=H.Location ");
                sql.AppendLine("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.AppendLine("    JOIN GrpBoardDet  GBD ON GBD.Board=HB.Code ");
                sql.AppendLine("    JOIN GrpBoard  GB ON GB.Code=GBD.Groups And GB.GrpType='P'     ");
                sql.AppendLine(") Boards ");
                sql.AppendLine("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.AppendLine("");
                sql.AppendLine("--Rooms ");
                sql.AppendLine("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.AppendLine("From ( ");
                sql.AppendLine("    SELECT Hotel_Code=H.Code,Code=GRD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.AppendLine("        PLArrCity=L.City,PLDepCity=-1 ");
                sql.AppendLine("    From OHotelSalePrice P ");
                sql.AppendLine("    JOIN #Hotel H ON H.RecID=P.HotelID ");
                sql.AppendLine("    JOIN #Location L ON L.RecID=H.Location ");
                sql.AppendLine("    JOIN HotelRoom HR  ON HR.Hotel=H.Code ");
                sql.AppendLine("    JOIN GrpRoomDet  GRD ON GRD.Room=HR.Code ");
                sql.AppendLine("    JOIN GrpRoom  GB ON GB.Code=GRD.Groups And GB.GrpType='P' ");
                sql.AppendLine(") Rooms ");
                sql.AppendLine("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                #endregion
            }
            else
            {
                #region without groups
                sql.AppendLine("--Board ");
                sql.AppendLine("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.AppendLine("FROM ( ");
                sql.AppendLine("    SELECT Hotel_Code=H.Code,HB.Code,HB.Name,NameL=isnull(dbo.FindLocalName(HB.NameLID,@Market),HB.Name),L.City,L.Town,L.Village, ");
                sql.AppendLine("        PLArrCity=L.City,PLDepCity=-1 ");
                sql.AppendLine("    From OHotelSalePrice P ");
                sql.AppendLine("    JOIN #Hotel H ON H.RecID=P.HotelID ");
                sql.AppendLine("    JOIN #Location L ON L.RecID=H.Location ");
                sql.AppendLine("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.AppendLine(") Boards ");
                sql.AppendLine("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.AppendLine("--Rooms ");
                sql.AppendLine("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.AppendLine("From ( ");
                sql.AppendLine("    SELECT Hotel_Code=H.Code,HR.Code,HR.Name,NameL=isnull(dbo.FindLocalName(HR.NameLID,@Market),HR.Name),L.City,L.Town,L.Village, ");
                sql.AppendLine("        PLArrCity=L.City,PLDepCity=-1 ");
                sql.AppendLine("    From OHotelSalePrice P ");
                sql.AppendLine("    JOIN #Hotel H ON H.RecID=P.HotelID ");
                sql.AppendLine("    JOIN #Location L ON L.RecID=H.Location ");
                sql.AppendLine("    JOIN HotelRoom HR ON HR.Hotel=H.Code ");
                sql.AppendLine(") Rooms ");
                sql.AppendLine("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                #endregion
            }
            sql.AppendLine("--Resort ");
            sql.AppendLine("Select [Name],NameL,RecID,Country,City ");
            sql.AppendLine("From ( ");
            sql.AppendLine("    Select RecID=L.RecID,Name=(Select Name From #Location Where RecID=L.RecID),NameL=(Select NameL From #Location Where RecID=L.RecID), ");
            sql.AppendLine("         L.Country,City=L.City ");
            sql.AppendLine("    From OHotelSalePrice P ");
            sql.AppendLine("    JOIN #Hotel H ON H.RecID=P.HotelID ");
            sql.AppendLine("    JOIN #Location L ON L.RecID=H.Location ");
            sql.AppendLine(") Resort ");
            sql.AppendLine("Group By [Name],NameL,RecID,Country,City ");
            sql.AppendLine("");
            sql.AppendLine("--Hotels ");
            sql.AppendLine("Select [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack ");
            sql.AppendLine("From ( ");
            sql.AppendLine("    Select H.Code,Name=H.Name,NameL=H.NameL,H.Category,H.Location,L.Country,L.City, ");
            sql.AppendLine("		Town=isnull(L.Town,-1),ArrCity=L.City,CatPackID=-1,Holpack='' ");
            sql.AppendLine("    From OHotelSalePrice P ");
            sql.AppendLine("    JOIN #Hotel H ON H.RecID=P.HotelID ");
            sql.AppendLine("    JOIN #Location L ON L.RecID=H.Location ");
            sql.AppendLine(") Hotels ");
            sql.AppendLine("Group By [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack ");
            sql.AppendLine("");
            sql.AppendLine("-- Max Pax Count ");
            sql.AppendLine("Select Adult=Max(isnull(Adult,6)),ChdAgeG1=Max(isnull(ChdAgeG1,4)),ChdAgeG2=Max(isnull(ChdAgeG2,4)),ChdAgeG3=Max(isnull(ChdAgeG3,4)),ChdAgeG4=Max(isnull(ChdAgeG4,4)) ");
            sql.AppendLine("From OHotelSalePrice P ");
            sql.AppendLine("JOIN #Hotel H ON H.RecID=P.HotelID ");
            sql.AppendLine("join HotelRoom R (nolock) on R.RecID = P.RoomID ");
            sql.AppendLine("join HotelAccom A (nolock) on A.RecID = P.AccomID ");
            sql.AppendLine("join HotelBoard B (nolock) on B.RecID = P.BoardID ");
            sql.AppendLine("join HotelAccomPax HAP (NOLOCK) on HAP.Hotel=H.Code and HAP.Room=R.Code and HAP.Accom=A.Code and IsNull(HAP.VisiblePL,'Y')='Y' ");
            return sql;
        }

        public StringBuilder getPricelistLocationDataForCultureTour(string Operator, string Market, bool Groups, ref string errorMsg)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ");
            sql.Append("SET NOCOUNT ON ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CPM') is not null Drop Table dbo.#CPM ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Hotel') is not null Drop Table dbo.#Hotel ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatPriceA') is not null Drop Table dbo.#CatPriceA ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#HolPackCat') is not null Drop Table dbo.#HolPackCat \n");
            sql.Append("Select *,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name)  Into #CatalogPack ");
            sql.Append("From CatalogPack CP ");
            sql.Append("Where CP.ReadY='Y' ");
            sql.Append("  And CP.WebPub='Y' And CP.WebPubDate<=GetDate() ");
            sql.Append("  And CP.PackType='T' ");
            sql.Append("  And (isnull(CP.B2BMenuCat,'')='' Or CP.B2BMenuCat='T') ");
            sql.Append("  And EndDate > GetDate()-1 ");
            sql.Append("  And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("Select Direction,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) ");
            sql.Append("Into #HolPackCat ");
            sql.Append("From HolPackCat ");
            sql.Append("Select Distinct CatPackID,Hotel ");
            sql.Append("Into #CatPriceA ");
            sql.Append("From CatPriceA A ");
            sql.Append("Join #CatalogPack CP on CP.RecID=A.CatPAckID ");
            sql.Append("Where A.Status=1 ");
            sql.Append("Select CPM.RecID,CPM.CatPackID,CPM.Market,CPM.Operator ");
            sql.Append("Into #CPM ");
            sql.Append("From CatPackMarket CPM ");
            sql.Append("Join #CatalogPack CP ON CP.RecID=CPM.CatPackID ");
            sql.Append("Where CPM.Market=@Market ");
            sql.Append(" And (CPM.Operator=@Operator Or CPM.Operator='') ");
            sql.Append("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] ");
            sql.Append("Into #Location ");
            sql.Append("From Location ");
            sql.Append("Select [Name],NameL,Code,HotelType,Category,Location ");
            sql.Append("Into #Hotel ");
            sql.Append("From ");
            sql.Append("( ");
            sql.Append("    Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),H.HotelType,H.Category,H.Location ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
            sql.Append("    Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") H ");
            sql.Append("Group By [Name],NameL,Code,HotelType,Category,Location \n");
            sql.Append("--Locations \n");
            sql.Append("Select DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL,Direction,Category,CategoryName,CategoryNameL ");
            sql.Append("From ( ");
            sql.Append("    SELECT CP.DepCity, ");
            sql.Append("        DepCityName=(Select Name From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        DepCityNameL=(Select NameL From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        ArrCountry=(Select Country From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCountryName=(Select Name From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        ArrCountryNameL=(Select NameL From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        CP.ArrCity, ");
            sql.Append("        ArrCityName=(Select Name From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCityNameL=(Select NameL From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        CP.Direction,CP.Category,CategoryName=HPC.Name,CategoryNameL=HPC.NameL ");
            sql.Append("    FROM #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON CPA.Hotel=H.Code ");
            sql.Append("    JOIN #Location L ON L.RecID=(Select Country From #Location Where RecID=H.Location) ");
            sql.Append("    LEFT JOIN #HolPackCat HPC ON HPC.Code=CP.Category And HPC.Direction=CP.Direction ");
            sql.Append("    WHERE 	dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("        And Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") Locations ");
            sql.Append("Group By DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL,Direction,Category,CategoryName,CategoryNameL \n");
            sql.Append("--Category \n");
            sql.Append("SELECT Distinct HC.Code,HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID, @Market),HC.Name),L.Country, ");
            sql.Append("    PL_Country=(Select Country From #Location Where RecID=CP.ArrCity), ");
            sql.Append("    L.City, PL_City=CP.ArrCity,L.Town,L.Village,CP.Direction,CP.Category ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("JOIN HotelCat HC  ON HC.Code=H.Category ");
            sql.Append("JOIN #Location L ON L.RecID=H.Location \n");
            sql.Append("--HolPack \n");
            sql.Append("Select [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID,Direction,Category ");
            sql.Append("From ( ");
            sql.Append("    Select CP.HolPack, ");
            sql.Append("        Name=H.Name +  ");
            sql.Append("        ' ( ' + (Select Name From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select Name From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) +  ");
            sql.Append("        ' ( ' + (Select NameL From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select NameL From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        CP.DepCity ,CP.ArrCity,L.Country,CatPackID=CP.RecID,CP.Direction,CP.Category ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join HolPack H ON H.Code=CP.HolPack ");
            sql.Append("    Join #Location L ON L.RecID=CP.ArrCity ");
            sql.Append("    Where  Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append("      And isnull(H.Ready,'N')='Y' ");
            sql.Append("      And isnull(H.WebPub,'N')='Y' ");
            sql.Append(") Holpacks ");
            sql.Append("Group By [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID,Direction,Category \n");
            if (Groups)
            {
                #region with groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GBD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,CP.Direction,CP.Category ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    JOIN GrpBoardDet  GBD ON GBD.Board=HB.Code ");
                sql.Append("    JOIN GrpBoard  GB ON GB.Code=GBD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category  ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GRD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,CP.Direction,CP.Category ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR  ON HR.Hotel=H.Code ");
                sql.Append("    JOIN GrpRoomDet  GRD ON GRD.Room=HR.Code ");
                sql.Append("    JOIN GrpRoom  GB ON GB.Code=GRD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category \n");
                #endregion
            }
            else
            {
                #region without groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HB.Code,HB.Name,NameL=isnull(dbo.FindLocalName(HB.NameLID,@Market),HB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,CP.Direction,CP.Category ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category  \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category  ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HR.Code,HR.Name,NameL=isnull(dbo.FindLocalName(HR.NameLID,@Market),HR.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,CP.Direction,CP.Category ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR ON HR.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category  \n");
                #endregion
            }
            sql.Append("--Resort \n");
            sql.Append("Select [Name],NameL,RecID,Country,City,Direction,Category ");
            sql.Append("From ( ");
            sql.Append("    Select RecID=L.RecID,Name=(Select Name From #Location Where RecID=L.RecID),NameL=(Select NameL From #Location Where RecID=L.RecID), ");
            sql.Append("         L.Country,City=CP.ArrCity,CP.Direction,CP.Category ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append("    Where Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append(") Resort ");
            sql.Append("Group By [Name],NameL,RecID,Country,City,Direction,Category \n");
            sql.Append("--Hotels \n");
            sql.Append("Select [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Direction,HolPackCat,Holpack ");
            sql.Append("From ( ");
            sql.Append("    Select H.Code,Name=H.Name,NameL=H.NameL,H.Category,H.Location,L.Country,L.City, ");
            sql.Append("		Town=isnull(L.Town,-1),CP.ArrCity,CatPackID=CP.RecID,CP.Holpack,CP.Direction,HolPackCat=CP.Category ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append(") Hotels ");
            sql.Append("Group By [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Direction,HolPackCat,Holpack \n");
            sql.Append("-- Culture Tour HolPackCat \n");
            sql.Append("Select Distinct HPC.RecID,HPC.Direction,HPC.Code,HPC.Name,NameL=isnull(dbo.FindLocalName(HPC.NameLID,@Market),HPC.Name)");
            sql.Append("From HolpackCat HPC (NOLOCK) ");
            sql.Append("Join HolPack HP (NOLOCK) ON HP.Direction=HPC.Direction And HP.Category=HPC.Code ");
            sql.Append("Join CatalogPack CP (NOLOCK) ON CP.HolPack=HP.Code ");
            sql.Append("Where CP.Ready='Y' ");
            sql.Append("  And CP.WebPub='Y' ");
            sql.Append("  And CP.PackType='T' ");
            sql.Append("  And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("  And Exists(Select * From CatPackMarket (NOLOCK) Where CatPackID=CP.RecID AND Market=@Market AND (Operator=@Operator OR Operator='')) \n");
            sql.Append("-- Max Pax Count \n");
            sql.Append("Select Adult=Max(isnull(Adult,6)),ChdAgeG1=Max(isnull(ChdAgeG1,4)),ChdAgeG2=Max(isnull(ChdAgeG2,4)),ChdAgeG3=Max(isnull(ChdAgeG3,4)),ChdAgeG4=Max(isnull(ChdAgeG4,4)) ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("Join CatPriceA CPA  ON CP.RecID=CPA.CatPackID And CPA.Status=1 ");
            sql.Append("Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("Join HotelAccomPax HAP  on HAP.Hotel=CPA.Hotel and HAP.Room=CPA.Room and HAP.Accom=CPA.Accom and IsNull(HAP.VisiblePL,'Y')='Y' ");
            return sql;
        }

        public StringBuilder getPricelistLocationDataForOther(string Operator, string Market, bool Groups, string B2BMenuCat, ref string errorMsg)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ");
            sql.Append("SET NOCOUNT ON ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CPM') is not null Drop Table dbo.#CPM ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Hotel') is not null Drop Table dbo.#Hotel ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatPriceA') is not null Drop Table dbo.#CatPriceA ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#HolPackCat') is not null Drop Table dbo.#HolPackCat \n");
            sql.Append("Select *,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name) Into #CatalogPack ");
            sql.Append("From CatalogPack CP ");
            sql.Append("Where CP.ReadY='Y' ");
            sql.Append("  And CP.WebPub='Y' And CP.WebPubDate<=GetDate() ");
            sql.Append("  And isnull(CP.B2BMenuCat,'')<>'' ");
            sql.Append("  And CP.B2BMenuCat=@B2BMenuCat ");
            sql.Append("  And EndDate > GetDate()-1 ");
            sql.Append("  And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("Select Direction,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) ");
            sql.Append("Into #HolPackCat ");
            sql.Append("From HolPackCat ");
            sql.Append("Select Distinct CatPackID,Hotel ");
            sql.Append("Into #CatPriceA ");
            sql.Append("From CatPriceA A ");
            sql.Append("Join #CatalogPack CP on CP.RecID=A.CatPAckID ");
            sql.Append("Where A.Status=1 ");
            sql.Append("Select CPM.RecID,CPM.CatPackID,CPM.Market,CPM.Operator ");
            sql.Append("Into #CPM ");
            sql.Append("From CatPackMarket CPM ");
            sql.Append("Join #CatalogPack CP ON CP.RecID=CPM.CatPackID ");
            sql.Append("Where CPM.Market=@Market ");
            sql.Append(" And (CPM.Operator=@Operator Or CPM.Operator='') ");
            sql.Append("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] ");
            sql.Append("Into #Location ");
            sql.Append("From Location ");
            sql.Append("Select [Name],NameL,Code,HotelType,Category,Location ");
            sql.Append("Into #Hotel ");
            sql.Append("From ");
            sql.Append("( ");
            sql.Append("    Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),H.HotelType,H.Category,H.Location ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
            sql.Append("    Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") H ");
            sql.Append("Group By [Name],NameL,Code,HotelType,Category,Location \n");
            sql.Append("--Locations \n");
            sql.Append("Select DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL ");
            sql.Append("From ( ");
            sql.Append("    SELECT CP.DepCity, ");
            sql.Append("        DepCityName=(Select Name From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        DepCityNameL=(Select NameL From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        ArrCountry=(Select Country From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCountryName=(Select Name From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        ArrCountryNameL=(Select NameL From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        CP.ArrCity, ");
            sql.Append("        ArrCityName=(Select Name From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCityNameL=(Select NameL From #Location L Where RecID=CP.ArrCity) ");
            sql.Append("    FROM #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON CPA.Hotel=H.Code ");
            sql.Append("    JOIN #Location L ON L.RecID=(Select Country From #Location Where RecID=H.Location) ");
            sql.Append("    LEFT JOIN #HolPackCat HPC ON HPC.Code=CP.Category And HPC.Direction=CP.Direction ");
            sql.Append("    WHERE 	dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("        And Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") Locations ");
            sql.Append("Group By DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL \n");
            sql.Append("--Category \n");
            sql.Append("SELECT Distinct HC.Code,HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID, @Market),HC.Name),L.Country, ");
            sql.Append("    PL_Country=(Select Country From #Location Where RecID=CP.ArrCity), ");
            sql.Append("    L.City, PL_City=CP.ArrCity, L.Town, L.Village ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("JOIN HotelCat HC  ON HC.Code=H.Category ");
            sql.Append("JOIN #Location L ON L.RecID=H.Location \n");
            sql.Append("--HolPack \n");
            sql.Append("Select [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID ");
            sql.Append("From ( ");
            sql.Append("    Select CP.HolPack, ");
            sql.Append("        Name=H.Name +  ");
            sql.Append("        ' ( ' + (Select Name From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select Name From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) +  ");
            sql.Append("        ' ( ' + (Select NameL From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select NameL From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        CP.DepCity , CP.ArrCity, L.Country, CatPackID=CP.RecID ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join HolPack H  ON H.Code=CP.HolPack ");
            sql.Append("    Join #Location L ON L.RecID=CP.ArrCity ");
            sql.Append("    Where  Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append("      And isnull(H.Ready,'N')='Y' ");
            sql.Append("      And isnull(H.WebPub,'N')='Y' ");
            sql.Append(") Holpacks ");
            sql.Append("Group By [Name],NameL, HolPack, DepCity, ArrCity, Country, CatPackID \n");
            if (Groups)
            {
                #region with groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GBD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    JOIN GrpBoardDet  GBD ON GBD.Board=HB.Code ");
                sql.Append("    JOIN GrpBoard  GB ON GB.Code=GBD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GRD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR  ON HR.Hotel=H.Code ");
                sql.Append("    JOIN GrpRoomDet  GRD ON GRD.Room=HR.Code ");
                sql.Append("    JOIN GrpRoom  GB ON GB.Code=GRD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            else
            {
                #region without groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HB.Code,HB.Name,NameL=isnull(dbo.FindLocalName(HB.NameLID,@Market),HB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HR.Code,HR.Name,NameL=isnull(dbo.FindLocalName(HR.NameLID,@Market),HR.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR ON HR.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            sql.Append("--Resort \n");
            sql.Append("Select [Name],NameL,RecID,Country,City ");
            sql.Append("From ( ");
            sql.Append("    Select RecID=L.RecID,Name=(Select Name From #Location Where RecID=L.RecID),NameL=(Select NameL From #Location Where RecID=L.RecID), ");
            sql.Append("         L.Country,City=CP.ArrCity ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append("    Where Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append(") Resort ");
            sql.Append("Group By [Name],NameL,RecID,Country,City \n");
            sql.Append("--Hotels \n");
            sql.Append("Select [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack ");
            sql.Append("From ( ");
            sql.Append("    Select H.Code,Name=H.Name,NameL=H.NameL,H.Category,H.Location,L.Country,L.City, ");
            sql.Append("		Town=isnull(L.Town,-1),CP.ArrCity,CatPackID=CP.RecID,CP.Holpack ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append(") Hotels ");
            sql.Append("Group By [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack \n");
            sql.Append("-- Max Pax Count \n");
            sql.Append("Select Adult=Max(isnull(Adult,6)),ChdAgeG1=Max(isnull(ChdAgeG1,4)),ChdAgeG2=Max(isnull(ChdAgeG2,4)),ChdAgeG3=Max(isnull(ChdAgeG3,4)),ChdAgeG4=Max(isnull(ChdAgeG4,4)) ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("Join CatPriceA CPA  ON CP.RecID=CPA.CatPackID And CPA.Status=1 ");
            sql.Append("Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("Join HotelAccomPax HAP  on HAP.Hotel=CPA.Hotel and HAP.Room=CPA.Room and HAP.Accom=CPA.Accom and IsNull(HAP.VisiblePL,'Y')='Y' ");
            return sql;
        }

        public StringBuilder getPricelistLocationDataSqlStr(string Operator, string Market, bool Groups, SearchType searchType, string B2BMenuCat, string OnlyHotelVersion, ref string errorMsg)
        {
            StringBuilder sql = new StringBuilder();
            switch (searchType)
            {
                case SearchType.PackageSearch:
                    sql = getPricelistLocationDataForPackageSearch(Operator, Market, Groups, ref errorMsg);
                    break;
                case SearchType.OnlyHotelSearch:
                    if (OnlyHotelVersion == "V2")
                        sql = getPricelistLocationDataForOnlyHotelSearchV2(Operator, Market, Groups, ref errorMsg);
                    else
                        sql = getPricelistLocationDataForOnlyHotelSearch(Operator, Market, Groups, ref errorMsg);
                    break;
                case SearchType.CruiseSearch:
                    sql = getPricelistLocationDataForCruiseSearch(Operator, Market, Groups, ref errorMsg);
                    break;
                case SearchType.TourPackageSearch:
                    sql = getPricelistLocationDataForCultureTour(Operator, Market, Groups, ref errorMsg);
                    break;
                case SearchType.OtherSearch:
                    sql = getPricelistLocationDataForOther(Operator, Market, Groups, B2BMenuCat, ref errorMsg);
                    break;
            }
            return sql;
        }

        public SearchPLData getPricelistLocationDataForB2bMenuCat(string Agency, string Operator, string Market, bool Groups, SearchType searchType, string B2BMenuCat, string OnlyHotelVersion, ref string errorMsg)
        {
            SearchPLData sPLData = new SearchPLData();
            List<Location> location = new Locations().getLocationList(Market, LocationType.None, null, null, null, null, ref errorMsg);
            StringBuilder tSqlStr = new StringBuilder();

            tSqlStr = getPricelistLocationDataSqlStr(Operator, Market, Groups, searchType, B2BMenuCat, OnlyHotelVersion, ref errorMsg);

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tSqlStr.ToString());

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);
                if (searchType == SearchType.OtherSearch)
                    db.AddInParameter(dbCommand, "B2BMenuCat", DbType.String, B2BMenuCat);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                ds.Tables[0].TableName = "Locations";
                ds.Tables[1].TableName = "Category";
                ds.Tables[2].TableName = "HolPack";
                ds.Tables[3].TableName = "Board";
                ds.Tables[4].TableName = "Room";
                ds.Tables[5].TableName = "Resort";
                ds.Tables[6].TableName = "Hotels";
                if (searchType == SearchType.TourPackageSearch)
                    ds.Tables[7].TableName = "HolPackCat";
                ds.Tables[searchType == SearchType.TourPackageSearch ? 8 : 7].TableName = "MaxPaxCount";

                sPLData = new Search().addPLLocationsB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLCategorysB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLHolPacksB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLBoardsB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLRoomsB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLResortsB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLHotelsB2BMenuCat(ds, sPLData, location);
                if (searchType == SearchType.TourPackageSearch)
                    sPLData = new Search().addPLHolPackCatB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLMaxPaxB2BMenuCat(ds, sPLData);

                return sPLData;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return sPLData;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public SearchPLData getPricelistLocationData(User UserData, string Operator, string Market, bool Groups, SearchType searchType, ref string errorMsg)
        {
            SearchPLData sPLData = new SearchPLData();
            List<Location> location = new Locations().getLocationList(Market, LocationType.None, null, null, null, null, ref errorMsg);
            StringBuilder tSqlStr = new StringBuilder();

            string tsql = string.Empty;

            string PackTypeSql = string.Empty;


            switch (searchType)
            {
                case SearchType.PackageSearch:
                    PackTypeSql = "  AND CP.PackType = 'H' ";
                    break;
                case SearchType.OnlyHotelSearch:
                    PackTypeSql = "  AND CP.PackType = 'O' ";
                    break;
                case SearchType.TourPackageSearch:
                    PackTypeSql = "  AND CP.PackType = 'T' ";
                    break;
            }
            #region sql
            tsql = @"   if OBJECT_ID('TempDB.dbo.#CPM') is not null Drop Table dbo.#CPM
                        if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack
                        if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
                        if OBJECT_ID('TempDB.dbo.#Hotel') is not null Drop Table dbo.#Hotel                        
                        if OBJECT_ID('TempDB.dbo.#CatPriceA') is not null Drop Table dbo.#CatPriceA
                        if OBJECT_ID('TempDB.dbo.#HolPackCat') is not null Drop Table dbo.#HolPackCat

                        SET NOCOUNT ON

                        Select *,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name) 
                        Into #CatalogPack 
                        From CatalogPack CP (NOLOCK)
                        Where CP.ReadY = 'Y' 
                          And CP.WebPub = 'Y' And CP.WebPubDate <= GetDate()  ";
            tsql += PackTypeSql;
            tsql += @"    And EndDate > GetDate()-1 
                          And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate                          

                        Select Direction,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name) 
                        Into #HolPackCat
                        From HolPackCat (NOLOCK)

                        Select Distinct CatPackID,Hotel
                        Into #CatPriceA
                        From CatPriceA A (NOLOCK)
                        Join #CatalogPack CP on CP.RecID=A.CatPAckID 
                        Where A.Status = 1";
            //            tsql+= @"
            //                        Select CPM.RecID, CPM.CatPackID, CPM.Market, CPM.Operator 
            //                        Into #CPM
            //						From HolPack (NOLOCK) H
            //						Left JOIN HolPackMarket (NOLOCK) HPM on hpm.HolPack=h.Code
            //                        Left JOIN CatPackMarket CPM (NOLOCK) on h.Code=HPM.HolPack
            //                        Join #CatalogPack CP ON CP.RecID = CPM.CatPackID
            //                        Where
            //						(hpm.Operator = @Operator Or IsNull(hpm.Operator,'')= '' )";
            // Novanın paylaşımlı satışları için yukarıdaki ekleme yapıldı
            tsql += @"
                        Select CPM.RecID, CPM.CatPackID, CPM.Market, CPM.Operator 
                        Into #CPM
                        From CatPackMarket CPM (NOLOCK) 
                        Join #CatalogPack CP ON CP.RecID = CPM.CatPackID
                        Where CPM.Market=@Market AND (CPM.Operator=@Operator OR CPM.Operator='')";
            tsql += @"
                        Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name), Country, City, Town, Village, Type 
                        Into #Location
                        From Location (NOLOCK)

                        Select [Name],NameL,Code,HotelType,Category,Location
                        Into #Hotel 
                        From 
                        (
                            Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),H.HotelType,H.Category,H.Location
                            From #CatalogPack CP
                            Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
                            Join Hotel H (NOLOCK) ON H.Code = CPA.Hotel
                            Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)        
                        ) H
                        Group By [Name],NameL,Code,HotelType,Category,Location

                        --Locations
                        Select DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,
                               ArrCity,ArrCityName,ArrCityNameL,Direction,Category,CategoryName,CategoryNameL
                        From (
                            SELECT CP.DepCity, 
                                DepCityName=(Select Name From #Location L Where RecID=CP.DepCity),
                                DepCityNameL=(Select NameL From #Location L Where RecID=CP.DepCity),
                                ArrCountry=(Select Country From #Location L Where RecID=CP.ArrCity),
                                ArrCountryName=(Select Name From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)),
                                ArrCountryNameL=(Select NameL From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)),
                                CP.ArrCity,
                                ArrCityName=(Select Name From #Location L Where RecID=CP.ArrCity),
                                ArrCityNameL=(Select NameL From #Location L Where RecID=CP.ArrCity),
                                CP.Direction, CP.Category, 
                                CategoryName=HPC.Name,
                                CategoryNameL=HPC.NameL
                            FROM #CatalogPack CP 
                            JOIN #CatPriceA CPA (NOLOCK) ON CPA.CatPackID=CP.RecID
                            JOIN #Hotel H ON CPA.Hotel=H.Code
                            JOIN #Location L ON L.RecID=(Select Country From #Location Where RecID=H.Location)                            
                            LEFT JOIN #HolPackCat HPC ON HPC.Code=CP.Category And HPC.Direction=CP.Direction
                            WHERE 	dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate
                                And Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
                        ) Locations
                        Group By DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,
                                 ArrCity,ArrCityName,ArrCityNameL,Direction,Category,CategoryName,CategoryNameL

                        --Category
                        SELECT Distinct HC.Code,HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID,@Market),HC.Name),
                            L.Country,PL_Country=(Select Country From #Location Where RecID=CP.ArrCity),
	                        L.City, PL_City=CP.ArrCity, L.Town, L.Village, CP.Direction, CP.Category
                        From #CatalogPack CP
                        JOIN #CatPriceA CPA (NOLOCK) ON CPA.CatPackID = CP.RecID    
                        JOIN #Hotel H ON H.Code = CPA.Hotel
                        JOIN HotelCat HC (NOLOCK) ON HC.Code = H.Category 
                        JOIN #Location L ON L.RecID = H.Location

                        --HolPack
                        Select [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID,Direction,Category
                        From (
	                        Select CP.HolPack,                     
                                Name=H.Name + 
		                        ' ( ' + (Select Name From #Location Where RecID = CP.DepCity) +
		                        ' , ' + (Select Name From #Location Where RecID = CP.ArrCity) + ' ) ', 
		                        NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) + 
		                        ' ( ' + (Select NameL From #Location Where RecID = CP.DepCity) +
		                        ' , ' + (Select NameL From #Location Where RecID = CP.ArrCity) + ' ) ', 
		                        CP.DepCity , CP.ArrCity, L.Country, CatPackID=CP.RecID,
                                CP.Direction, CP.Category
	                        From #CatalogPack CP
	                        Join HolPack H (NOLOCK) ON H.Code = CP.HolPack
	                        Join #Location L ON L.RecID = CP.ArrCity
	                        Where  Exists (Select * From #CPM Where CatPackID=CP.RecID)
                             And isnull(H.Ready,'N')='Y'
                             And isnull(H.WebPub,'N')='Y'
                        ) Holpacks
                        Group By [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID,Direction,Category  
                       ";

            if (Groups)
            {
                tsql += @"  --Board
                            SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category
	                        FROM (
                                SELECT Hotel_Code=H.Code,Code=GBD.Groups,
                                    GB.Name,
                                    NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name), 
                                    L.City,L.Town,L.Village,PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,
                                    CP.Direction,CP.Category
		                        From #CatalogPack CP
		                        Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
		                        Join #Hotel H ON H.Code = CPA.Hotel        
                                JOIN #Location L ON L.RecID = H.Location 
                                JOIN HotelBoard HB (NOLOCK) ON HB.Hotel =H.Code 
                                JOIN GrpBoardDet (NOLOCK) GBD ON GBD.Board = HB.Code
                                JOIN GrpBoard (NOLOCK) GB ON GB.Code = GBD.Groups And GB.GrpType = 'P'		
                                Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
                            ) Boards  
	                        Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category

                            --Rooms
                            Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category
	                        From (
		                        SELECT Hotel_Code=H.Code,Code=GRD.Groups, 
                                    GB.Name,
                                    NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name), 
                                    L.City,L.Town,L.Village,PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,
                                    CP.Direction, CP.Category
		                        From #CatalogPack CP
		                        Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
		                        Join #Hotel H ON H.Code = CPA.Hotel
		                        JOIN #Location L ON L.RecID = H.Location 
		                        JOIN HotelRoom HR (NOLOCK) ON HR.Hotel = H.Code 
		                        JOIN GrpRoomDet (NOLOCK) GRD ON GRD.Room = HR.Code
		                        JOIN GrpRoom (NOLOCK) GB ON GB.Code = GRD.Groups And GB.GrpType = 'P'
                                Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
	                        ) Rooms
	                        Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category
                            ";
            }
            else
            {
                tsql += @"  --Board
                            SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category
	                        FROM (
                                SELECT Hotel_Code=H.Code,HB.Code,
                                    HB.Name,
                                    NameL=isnull(dbo.FindLocalName(HB.NameLID,@Market),HB.Name),
                                    L.City,L.Town,L.Village,PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,CP.Direction,CP.Category
                                From #CatalogPack CP
		                        Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
		                        Join #Hotel H ON H.Code = CPA.Hotel
                                JOIN #Location L ON L.RecID = H.Location 
                                JOIN HotelBoard HB (NOLOCK) ON HB.Hotel = H.Code
                                Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
                            ) Boards 
	                        Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category

                            --Rooms
                            Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category
	                        From (
		                        SELECT Hotel_Code=H.Code,HR.Code,
                                    HR.Name,NameL=isnull(dbo.FindLocalName(HR.NameLID,@Market),HR.Name),
                                    L.City,L.Town,L.Village,PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,CP.Direction,CP.Category
		                        From #CatalogPack CP
		                        Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
		                        Join #Hotel H ON H.Code = CPA.Hotel
		                        JOIN #Location L  ON L.RecID = H.Location 
		                        JOIN HotelRoom HR (NOLOCK) ON HR.Hotel = H.Code 
                                Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
	                        ) Rooms
	                        Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category
                            ";
            }

            tsql += @"  --Resort
                        Select [Name],NameL,RecID,Country,City,Direction,Category,Direction,Category
                        From (
	                        Select RecID=L.RecID,
                                   Name=(Select Name From #Location Where RecID=L.RecID), 
                                   NameL=(Select NameL From #Location Where RecID=L.RecID), 
                                   L.Country,City=CP.ArrCity,CP.Direction,CP.Category
	                        From #CatalogPack CP
	                        JOIN #CatPriceA CPA (NOLOCK) ON CPA.CatPackID=CP.RecID
	                        JOIN #Hotel H ON H.Code=CPA.Hotel
	                        JOIN #Location L ON L.RecID=H.Location
	                        Where Exists (Select * From #CPM Where CatPackID=CP.RecID)
                        ) Resort
                        Group By [Name],NameL,RecID,Country,City,Direction,Category,Direction,Category

                        --Hotels
                        Select [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack,Direction,HolPackCat
                        From (
                            Select H.Code,Name=H.Name,NameL=H.NameL,
                                   H.Category,H.Location,L.Country,L.City,Town=isnull(L.Town,-1),CP.ArrCity,CatPackID=CP.RecID, 
                                   CP.Holpack,CP.Direction,HolPackCat=CP.Category
                            From #CatalogPack CP
                            JOIN #CatPriceA CPA (NOLOCK) ON CPA.CatPackID = CP.RecID    
                            JOIN #Hotel H ON H.Code = CPA.Hotel
                            JOIN #Location L ON L.RecID = H.Location  
                        ) Hotels
                        Group By [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack,Direction,HolPackCat
                    ";
            if (searchType == SearchType.TourPackageSearch)
                tsql += @"  -- Culture Tour HolPackCat
                            Select Distinct HPC.RecID,HPC.Direction,HPC.Code,HPC.Name,NameL=isnull(dbo.FindLocalName(HPC.NameLID,@Market),HPC.Name)
                            From HolpackCat HPC (NOLOCK)
                            Join HolPack HP (NOLOCK) ON HP.Direction=HPC.Direction And HP.Category=HPC.Code
                            Join CatalogPack CP (NOLOCK) ON CP.HolPack=HP.Code
                            Where CP.Ready='Y' 
                              And CP.WebPub='Y' 	
                              And CP.PackType='T'
                              And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate		                        	                          
	                          And Exists(Select * From CatPackMarket (NOLOCK) Where CatPackID=CP.RecID AND Market=@Market AND (Operator=@Operator OR Operator='')) 
                         ";

            tsql += @"  -- Max Pax Count
                        Select Adult=Max(isnull(Adult,6)), ChdAgeG1=Max(isnull(ChdAgeG1,4)), ChdAgeG2=Max(isnull(ChdAgeG2,4)), ChdAgeG3=Max(isnull(ChdAgeG3,4)), ChdAgeG4=Max(isnull(ChdAgeG4,4)) 
                        From #CatalogPack CP
                        Join CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID And CPA.Status=1
                        Join Hotel H (NOLOCK) ON H.Code = CPA.Hotel
                        Join HotelAccomPax HAP (NOLOCK) on HAP.Hotel=CPA.Hotel and HAP.Room=CPA.Room and HAP.Accom=CPA.Accom and IsNull(HAP.VisiblePL,'Y')='Y'";
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040043084"))
                tsql +=
@"
SELECT GF.Code,GF.Name,
    NameL=isnull(dbo.FindLocalName(GF.NameLID,@Market),GF.Name),Hotel_Code=H.Code,
    L.City,L.Town,L.Village,PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,
    CP.Direction, CP.Category
From #CatalogPack CP
Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
Join #Hotel H ON H.Code = CPA.Hotel
JOIN #Location L ON L.RecID = H.Location 
JOIN HotelFacility (NOLOCK) HF ON HF.Hotel=H.Code
JOIN GrpFacilityDet (NOLOCK) GFD ON GFD.Facility = HF.FacNo
JOIN GrpFacility (NOLOCK) GF ON GF.RecID = GFD.MasterID
Where @Market=GF.Market 
  And Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
Group By GF.Name,GF.NameLID,H.Code,GF.Code,L.City,L.Town,L.Village,CP.ArrCity,CP.DepCity,CP.Direction,CP.Category
";

            #endregion

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            dbCommand.CommandTimeout = 120;

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                ds.Tables[0].TableName = "Locations";
                ds.Tables[1].TableName = "Category";
                ds.Tables[2].TableName = "HolPack";
                ds.Tables[3].TableName = "Board";
                ds.Tables[4].TableName = "Room";
                ds.Tables[5].TableName = "Resort";
                ds.Tables[6].TableName = "Hotels";
                if (searchType == SearchType.TourPackageSearch)
                    ds.Tables[7].TableName = "HolPackCat";
                ds.Tables[searchType == SearchType.TourPackageSearch ? 8 : 7].TableName = "MaxPaxCount";
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040043084"))
                    ds.Tables[searchType == SearchType.TourPackageSearch ? 9 : 8].TableName = "Facility";

                sPLData = new Search().addPLLocations(ds, sPLData);
                sPLData = new Search().addPLCategorys(ds, sPLData);
                sPLData = new Search().addPLHolPacks(ds, sPLData);
                sPLData = new Search().addPLBoards(ds, sPLData);
                sPLData = new Search().addPLRooms(ds, sPLData);
                sPLData = new Search().addPLResorts(ds, sPLData);
                sPLData = new Search().addPLHotels(ds, sPLData, location);
                if (searchType == SearchType.TourPackageSearch)
                    sPLData = new Search().addPLHolPackCat(ds, sPLData);
                sPLData = new Search().addPLMaxPax(ds, sPLData);
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040043084"))
                    sPLData = new Search().addPLFacility(ds, sPLData);

                if (!(Equals(Operator, "LTNOV") || Equals(Operator, "LVNOV") || Equals(Operator, "EENOV") || Equals(Operator, "LTAIP")) && searchType != SearchType.TourPackageSearch)
                {
                    List<plBoardData> plBoard = sPLData.plBoards;
                    foreach (plBoardData row in plBoard)
                        row.HolPackCat = null;
                    List<plCategoryData> plCategory = sPLData.plCategorys;
                    foreach (plCategoryData row in plCategory)
                        row.HolPackCat = null;
                    List<plHolPackData> plHolPack = sPLData.plHolPacks;
                    foreach (plHolPackData row in plHolPack)
                        row.HolPackCat = null;
                    List<plHotelData> plHotels = sPLData.plHotels;
                    foreach (plHotelData row in plHotels)
                        row.HolPackCat = null;
                    List<plLocationData> plLocations = sPLData.PlLocations;
                    foreach (plLocationData row in plLocations)
                        row.HolPackCat = null;
                    List<plResortData> plResort = sPLData.plResorts;
                    foreach (plResortData row in plResort)
                        row.HolPackCat = null;
                    List<plRoomData> plRoom = sPLData.plRooms;
                    foreach (plRoomData row in plRoom)
                        row.HolPackCat = null;
                    List<plFacilityData> plFacility = sPLData.PlFacility;
                    foreach (plFacilityData row in plFacility)
                        row.HolPackCat = null;
                }
                return sPLData;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return sPLData;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static void setSearchFilterCache(bool useGroup, ref string errorMsg)
        {
            List<OperatorRecord> oprt = new TvBo.Common().getOperators(ref errorMsg);
            List<MarketRecord> market = new Parameters().getMarkets(ref errorMsg);
            foreach (MarketRecord mrkt in market)
                foreach (OperatorRecord opr in oprt)
                {
                    DateTime? cacheTime = TvTools.Conversion.getDateTimeOrNull(System.Web.HttpContext.Current.Application.Get(opr.Code + "_" + mrkt.Code));
                    if (!cacheTime.HasValue)
                        cacheTime = DateTime.Now;
                    bool getCache = (DateTime.Now.AddMinutes(-30).Ticks - cacheTime.Value.Ticks) <= 0;
                    if (getCache)
                    {
                        getSearchFilterDataV2(opr.Code, mrkt.Code, useGroup, ref errorMsg);
                        System.Web.HttpContext.Current.Application.Set(opr.Code + "_" + mrkt.Code, DateTime.Now);
                    }
                }
        }

        public srchFilterData getSearchFilterCache(string Operator, string Market)
        {
            string keyName = "SearchFilterData_" + Operator + "_" + Market;
            srchFilterData readData = new srchFilterData();
            if (System.Web.HttpContext.Current.Application.Get(keyName) != null)
                readData = (srchFilterData)System.Web.HttpContext.Current.Application.Get(keyName);
            return readData;
        }

        public static void getSearchFilterDataV2(string Operator, string Market, bool Groups, ref string errorMsg)
        {
            srchFilterData sPLData = new srchFilterData();
            List<Location> location = new Locations().getLocationList(Market, LocationType.None, null, null, null, null, ref errorMsg);
            string tsql = string.Empty;
            #region sql
            tsql = @"if OBJECT_ID('TempDB.dbo.#CPM') is not null Drop Table dbo.#CPM
                     if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack
                     if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
                     if OBJECT_ID('TempDB.dbo.#Hotel') is not null Drop Table dbo.#Hotel                        
                     if OBJECT_ID('TempDB.dbo.#CatPriceA') is not null Drop Table dbo.#CatPriceA
                     if OBJECT_ID('TempDB.dbo.#HolPackCat') is not null Drop Table dbo.#HolPackCat

                    SET NOCOUNT ON
                    Declare @toDay DateTime
                    Set @toDay=dbo.DateOnly(GetDate())     

                    Select RecID,HolPack,PackType,Operator,Market,SaleCur,
	                    BegDate,EndDate,SaleBegDate,SaleEndDate,MinNight,MaxNight,
	                    DepCity,ArrCity,
	                    EBValid,PasEBValid,
	                    Direction,Category,Allotment,Used
                    Into #CatalogPack 
                    From CatalogPack CP (NOLOCK)
                    Where CP.ReadY='Y' 
                      And CP.SaleEndDate>=@toDay
                      And CP.WebPub='Y' 
                      And CP.WebPubDate <= GetDate()    
                      --AND CP.PackType='H'     
                      And EndDate>=@toDay
                      And @toDay between CP.SaleBegDate And CP.SaleEndDate                          

                    Select Direction, Code, Name=isnull(dbo.FindLocalName(NameLID, @Market), Name) 
                    Into #HolPackCat
                    From HolPackCat (NOLOCK)

                    Select Distinct CatPackID,Hotel
                    Into #CatPriceA
                    From CatPriceA A (NOLOCK)
                    Join #CatalogPack CP on CP.RecID=A.CatPAckID 
                    Where A.Status=1

                    Select CPM.RecID,CPM.CatPackID,CPM.Market,CPM.Operator 
                    Into #CPM
                    From CatPackMarket CPM (NOLOCK) 
                    Join #CatalogPack CP ON CP.RecID = CPM.CatPackID
                    Where CPM.Market=@Market AND (CPM.Operator=@Operator OR CPM.Operator='')  

                    Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name),Country,City,Town,Village,[Type]
                    Into #Location
                    From Location (NOLOCK)

                    Select [Name],NameL,Code,HotelType,Category,Location
                    Into #Hotel 
                    From 
                    (
                        Select H.Code,Name,NameL=isnull(dbo.FindLocalName(H.NameLID, @Market),H.Name),H.HotelType,H.Category,H.Location
                        From #CatalogPack CP
                        Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
                        Join Hotel H (NOLOCK) ON H.Code = CPA.Hotel
                        Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
                    ) H
                    Group By [Name],NameL,Code,HotelType,Category,Location

                    --Locations
                    Select DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL,Direction,Category,CategoryName,PackType
                    From (
                        SELECT CP.DepCity,     
		                    DepCityName=(Select Name From #Location L Where RecID=CP.DepCity),
                            DepCityNameL=(Select NameL From #Location L Where RecID=CP.DepCity),
                            ArrCountry=(Select Country From #Location L Where RecID=CP.ArrCity),
                            ArrCountryName=(Select Name From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)),
                            ArrCountryNameL=(Select NameL From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)),
                            CP.ArrCity,
                            ArrCityName=(Select Name From #Location L Where RecID=CP.ArrCity),
                            ArrCityNameL=(Select NameL From #Location L Where RecID=CP.ArrCity),
                            CP.Direction,CP.Category,CategoryName=HPC.Name,
                            CP.PackType
                        FROM #CatalogPack CP 
                        JOIN #CatPriceA CPA (NOLOCK) ON CPA.CatPackID=CP.RecID
                        JOIN #Hotel H ON CPA.Hotel=H.Code
                        JOIN #Location L ON L.RecID=(Select Country From #Location Where RecID=H.Location)                            
                        LEFT JOIN #HolPackCat HPC ON HPC.Code=CP.Category And HPC.Direction=CP.Direction
                        WHERE @toDay between CP.SaleBegDate And CP.SaleEndDate
                          And Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
                    ) Locations
                    Group By DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL,Direction,Category,CategoryName,PackType

                    --Category
                    SELECT Distinct HC.Code,HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID,@Market),HC.Name),L.Country, 
                        PL_Country=(Select Country From #Location Where RecID=CP.ArrCity),
                        L.City,PL_City=CP.ArrCity,L.Town,L.Village,CP.Direction,CP.Category,CP.PackType
                    From #CatalogPack CP
                    JOIN #CatPriceA CPA (NOLOCK) ON CPA.CatPackID = CP.RecID    
                    JOIN #Hotel H ON H.Code = CPA.Hotel
                    JOIN HotelCat HC (NOLOCK) ON HC.Code = H.Category 
                    JOIN #Location L ON L.RecID = H.Location

                    --HolPack
                    Select [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID,Direction,Category,PackType
                    From (
                        Select CP.HolPack,                     
                            Name=H.Name + 
                            ' ( ' + (Select Name From #Location Where RecID=CP.DepCity) +
                            ' , ' + (Select Name From #Location Where RecID=CP.ArrCity) + ' ) ', 
                            NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name) + 
                            ' ( ' + (Select NameL From #Location Where RecID=CP.DepCity) +
                            ' , ' + (Select NameL From #Location Where RecID=CP.ArrCity) + ' ) ', 
                            CP.DepCity,CP.ArrCity,L.Country,CatPackID=CP.RecID,
                            CP.Direction,CP.Category,CP.PackType
                        From #CatalogPack CP
                        Join HolPack H (NOLOCK) ON H.Code=CP.HolPack
                        Join #Location L ON L.RecID=CP.ArrCity
                        Where  Exists (Select * From #CPM Where CatPackID=CP.RecID)
                    ) Holpacks
                    Group By [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID,Direction,Category,PackType
                    ";

            if (Groups)
            {
                tsql += @"--Board
                        SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category,PackType
                        FROM (
                            SELECT Hotel_Code=H.Code,Code=GBD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID, @Market),GB.Name),L.City,L.Town,L.Village,
                                PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,
                                CP.Direction,CP.Category,CP.PackType
                            From #CatalogPack CP
                            Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
                            Join #Hotel H ON H.Code = CPA.Hotel        
                            JOIN #Location L ON L.RecID = H.Location 
                            JOIN HotelBoard HB (NOLOCK) ON HB.Hotel =H.Code 
                            JOIN GrpBoardDet (NOLOCK) GBD ON GBD.Board = HB.Code
                            JOIN GrpBoard (NOLOCK) GB ON GB.Code = GBD.Groups And GB.GrpType = 'P'		
                            Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
                        ) Boards  
                        Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category,PackType

                        --Rooms
                        Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category,PackType
                        From (
                            SELECT Hotel_Code=H.Code,Code=GRD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID, @Market),GB.Name),L.City,L.Town,L.Village,
                                PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,
                                CP.Direction,CP.Category,CP.PackType
                            From #CatalogPack CP
                            Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
                            Join #Hotel H ON H.Code = CPA.Hotel
                            JOIN #Location L ON L.RecID = H.Location 
                            JOIN HotelRoom HR (NOLOCK) ON HR.Hotel = H.Code 
                            JOIN GrpRoomDet (NOLOCK) GRD ON GRD.Room = HR.Code
                            JOIN GrpRoom (NOLOCK) GB ON GB.Code = GRD.Groups And GB.GrpType = 'P'
                            Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
                        ) Rooms
                        Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category,PackType
                        ";
            }
            else
            {
                tsql += @"--Board
                        SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category,PackType
                            FROM (
                                SELECT Hotel_Code=H.Code,Code=GBD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID, @Market),GB.Name),L.City,L.Town,L.Village,
                                    PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,
                                    CP.Direction,CP.Category,CP.PackType
                                From #CatalogPack CP
		                        Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
		                        Join #Hotel H ON H.Code = CPA.Hotel
                                JOIN #Location L ON L.RecID = H.Location 
                                JOIN HotelBoard HB (NOLOCK) ON HB.Hotel = H.Code
                                Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
                            ) Boards 
	                    Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category,PackType

                        --Rooms
                        Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category,PackType
                        From (
                            SELECT Hotel_Code=H.Code,Code=GRD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID, @Market),GB.Name),L.City,L.Town,L.Village,
                                PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity,
                                CP.Direction,CP.Category,CP.PackType
		                        From #CatalogPack CP
		                        Join #CatPriceA CPA (NOLOCK) ON CP.RecID = CPA.CatPackID
		                        Join #Hotel H ON H.Code = CPA.Hotel
		                        JOIN #Location L  ON L.RecID = H.Location 
		                        JOIN HotelRoom HR (NOLOCK) ON HR.Hotel = H.Code 
                                Where Exists(Select RecID From #CPM Where CatPackID = CPA.CatPackID)
	                        ) Rooms
	                    Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity,Direction,Category,PackType
                        ";
            }

            tsql += @"--Resort
                    Select [Name],NameL,RecID,Country,City,Direction,Category,Direction,PackType
                    From (
                        Select RecID=L.RecID,Name=(Select Name From #Location Where RecID=L.RecID), 
		                    NameL=(Select NameL From #Location Where RecID=L.RecID), 
                            L.Country,City=CP.ArrCity,CP.Direction,CP.Category,PackType
                        From #CatalogPack CP
                        JOIN #CatPriceA CPA (NOLOCK) ON CPA.CatPackID=CP.RecID
                        JOIN #Hotel H ON H.Code=CPA.Hotel
                        JOIN #Location L ON L.RecID=H.Location
                        Where Exists (Select * From #CPM Where CatPackID=CP.RecID)
                    ) Resort
                    Group By [Name],NameL,RecID,Country,City,Direction,Category,Direction,PackType

                    --Hotels
                    Select [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack,Direction,HolPackCat,PackType
                    From (
                        Select H.Code,Name=H.Name,NameL=H.NameL,H.Category,H.Location,L.Country,L.City,Town=isnull(L.Town, -1),CP.ArrCity,CatPackID=CP.RecID, 
                               CP.Holpack,CP.Direction,HolPackCat=CP.Category,CP.PackType
                        From #CatalogPack CP
                        JOIN #CatPriceA CPA (NOLOCK) ON CPA.CatPackID = CP.RecID    
                        JOIN #Hotel H ON H.Code = CPA.Hotel
                        JOIN #Location L ON L.RecID = H.Location  
                    ) Hotels
                    Group By [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack,Direction,HolPackCat,PackType
                                
                    --Culture Tour HolPackCat
                    Select Distinct HPC.RecID,HPC.Direction,HPC.Code,HPC.Name,NameL=isnull(dbo.FindLocalName(HPC.NameLID, @Market), HPC.Name)
                    From HolpackCat HPC (NOLOCK)
                    Join HolPack HP (NOLOCK) ON HP.Direction = HPC.Direction And HP.Category = HPC.Code
                    Join CatalogPack CP (NOLOCK) ON CP.HolPack = HP.Code
                    Where CP.Ready='Y' 
                      And CP.WebPub='Y' 	
                      And CP.PackType='T'
                      And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate		                        	                          
                      And Exists(Select * From CatPackMarket (NOLOCK) Where CatPackID=CP.RecID AND Market=@Market  AND (Operator=@Operator OR Operator='')) 

                    -- Max Pax Count
                    Select Adult=Max(isnull(Adult,6)),
	                    ChdAgeG1=Max(isnull(ChdAgeG1,4)),
	                    ChdAgeG2=Max(isnull(ChdAgeG2,4)), 
	                    ChdAgeG3=Max(isnull(ChdAgeG3,4)),
	                    ChdAgeG4=Max(isnull(ChdAgeG4,4)),
	                    PackType
                    From #CatalogPack CP
                    Join CatPriceA CPA (NOLOCK) ON CP.RecID=CPA.CatPackID And CPA.Status=1
                    Join Hotel H (NOLOCK) ON H.Code=CPA.Hotel
                    Join HotelAccomPax HAP (NOLOCK) on HAP.Hotel=CPA.Hotel and HAP.Room=CPA.Room and HAP.Accom=CPA.Accom and IsNull(HAP.VisiblePL,'Y')='Y'
                    Group By Cp.PackType
                    ";
            #endregion

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                ds.Tables[0].TableName = "Locations";
                ds.Tables[1].TableName = "Category";
                ds.Tables[2].TableName = "HolPack";
                ds.Tables[3].TableName = "Board";
                ds.Tables[4].TableName = "Room";
                ds.Tables[5].TableName = "Resort";
                ds.Tables[6].TableName = "Hotels";
                ds.Tables[7].TableName = "HolPackCat";
                ds.Tables[8].TableName = "MaxPaxCount";

                sPLData = new Search().addPLLocationsV2(ds, sPLData);
                sPLData = new Search().addPLCategorysV2(ds, sPLData);
                sPLData = new Search().addPLHolPacksV2(ds, sPLData);
                sPLData = new Search().addPLBoardsV2(ds, sPLData);
                sPLData = new Search().addPLRoomsV2(ds, sPLData);
                sPLData = new Search().addPLResortsV2(ds, sPLData);
                sPLData = new Search().addPLHotelsV2(ds, sPLData, location);
                sPLData = new Search().addPLHolPackCatV2(ds, sPLData);
                sPLData = new Search().addPLMaxPaxV2(ds, sPLData);

                saveSearchFilterDataV2(Operator, Market, sPLData);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static void saveSearchFilterDataV2(string Operator, string Market, srchFilterData data)
        {
            string keyName = "SearchFilterData_" + Operator + "_" + Market;
            System.Web.HttpContext.Current.Application.Lock();
            if (System.Web.HttpContext.Current.Application.Get(keyName) != null)
                System.Web.HttpContext.Current.Application.Set(keyName, data);
            else
                System.Web.HttpContext.Current.Application.Add(keyName, data);
            System.Web.HttpContext.Current.Application.UnLock();
        }

        public List<LocationIDName> getSearchCountrys(bool useMarketLang, SearchPLData plSearchData, ref string errorMsg)
        {
            if (useMarketLang)
            {
                IEnumerable<LocationIDName> data = from q in plSearchData.PlLocations.AsEnumerable()
                                                   orderby q.ArrCountryNameL
                                                   group q by new { RecID = q.ArrCountry, Name = q.ArrCountryNameL } into k
                                                   select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 };
                return data.ToList<LocationIDName>();
            }
            else
            {
                IEnumerable<LocationIDName> data = from q in plSearchData.PlLocations.AsEnumerable()
                                                   orderby q.ArrCountryName
                                                   group q by new { RecID = q.ArrCountry, Name = q.ArrCountryName } into k
                                                   select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 };
                return data.ToList<LocationIDName>();
            }
        }

        public List<LocationIDName> getSearchDepCitys(bool useMarketLang, SearchPLData plSearchData, int? Country, ref string errorMsg)
        {

            Country = Country > 0 ? Country : null;
            if (useMarketLang)
                return (from q in plSearchData.PlLocations.AsEnumerable()
                        where (string.IsNullOrEmpty(Country.ToString()) || q.ArrCountry == Country)
                        orderby q.DepCityNameL
                        group q by new { RecID = q.DepCity, Name = q.DepCityNameL } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
            else
                return (from q in plSearchData.PlLocations.AsEnumerable()
                        where (string.IsNullOrEmpty(Country.ToString()) || q.ArrCountry == Country)
                        orderby q.DepCityName
                        group q by new { RecID = q.DepCity, Name = q.DepCityName } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();

        }

        public List<LocationIDName> getSearchDepCitys(bool useMarketLang, SearchPLData plSearchData, Int16? Direction, Int16? HolPackCat, ref string errorMsg)
        {
            if (useMarketLang)
                return (from q in plSearchData.PlLocations.AsEnumerable()
                        where q.Direction == Direction && q.HolPackCat == HolPackCat
                        orderby q.DepCityNameL
                        group q by new { RecID = q.DepCity, Name = q.DepCityNameL } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
            else
                return (from q in plSearchData.PlLocations.AsEnumerable()
                        where q.Direction == Direction && q.HolPackCat == HolPackCat
                        orderby q.DepCityName
                        group q by new { RecID = q.DepCity, Name = q.DepCityName } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
        }

        public List<plLocationData> getSearchArrCitys(bool useMarketLang, SearchPLData plSearchData, int? DepCity, ref string errorMsg)
        {
            DepCity = DepCity.HasValue && DepCity > 0 ? DepCity : null;

            if (plSearchData == null || plSearchData.PlLocations == null)
                return new List<plLocationData>();

            return (from q in plSearchData.PlLocations
                    where (string.IsNullOrEmpty(DepCity.ToString()) || q.DepCity == DepCity)
                    select q).ToList<plLocationData>();
        }

        public List<plLocationData> getSearchArrCitys(bool useMarketLang, SearchPLData plSearchData, int? DepCity, Int16? Direction, Int16? Category, ref string errorMsg)
        {
            DepCity = DepCity.HasValue && DepCity > 0 ? DepCity : null;
            return (from q in plSearchData.PlLocations
                    where (string.IsNullOrEmpty(DepCity.ToString()) || q.DepCity == DepCity) &&
                        q.Direction == Direction &&
                        q.HolPackCat == Category
                    select q).ToList<plLocationData>();
        }

        public List<LocationIDName> getSearchArrCitys(bool useMarketLang, SearchPLData plSearchData, int? Country, int? DepCity, ref string errorMsg)
        {
            Country = Country > 0 ? Country : null;
            DepCity = DepCity > 0 ? DepCity : null;
            if (useMarketLang)
                return (from q in plSearchData.PlLocations.AsEnumerable()
                        where (string.IsNullOrEmpty(DepCity.ToString()) || q.DepCity == DepCity)
                        orderby q.ArrCountryName, q.ArrCityNameL
                        group q by new { RecID = q.ArrCity, Name = q.ArrCountryNameL + " => " + q.ArrCityNameL } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
            else
                return (from q in plSearchData.PlLocations.AsEnumerable()
                        where (string.IsNullOrEmpty(DepCity.ToString()) || q.DepCity == DepCity)
                        orderby q.ArrCountryName, q.ArrCityName
                        group q by new { RecID = q.ArrCity, Name = q.ArrCountryName + " => " + q.ArrCityName } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
        }

        public List<LocationIDName> getSearchResorts(bool useMarketLang, SearchPLData plSearchData, int? Country, int? ArrCity, Int16? HolPackCat, ref string errorMsg)
        {
            Country = Country > 0 ? Country : null;
            ArrCity = ArrCity > 0 ? ArrCity : null;
            if (useMarketLang)
                return (from q in plSearchData.plResorts.AsEnumerable()
                        where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                        (string.IsNullOrEmpty(ArrCity.ToString()) || q.City == ArrCity) &&
                        ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                        !string.IsNullOrEmpty(q.Name)
                        orderby q.NameL
                        group q by new { RecID = q.RecID, Name = q.NameL } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
            else
                return (from q in plSearchData.plResorts.AsEnumerable()
                        where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                        (string.IsNullOrEmpty(ArrCity.ToString()) || q.City == ArrCity) &&
                        ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                        !string.IsNullOrEmpty(q.Name)
                        orderby q.Name
                        group q by new { RecID = q.RecID, Name = q.Name } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
        }

        public List<LocationIDName> getSearchResorts(bool useMarketLang, SearchPLData plSearchData, int? ArrCity, Int16? Direction, Int16? Category, ref string errorMsg)
        {
            ArrCity = ArrCity > 0 ? ArrCity : null;
            if (useMarketLang)
                return (from q in plSearchData.plResorts.AsEnumerable()
                        where (!ArrCity.HasValue || q.City == ArrCity) && !string.IsNullOrEmpty(q.Name) &&
                            (!Direction.HasValue || q.Direction == Direction) &&
                            (!Category.HasValue || q.HolPackCat == Category)
                        orderby q.NameL
                        group q by new { RecID = q.RecID, Name = q.NameL } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
            else
                return (from q in plSearchData.plResorts.AsEnumerable()
                        where (!ArrCity.HasValue || q.City == ArrCity) && !string.IsNullOrEmpty(q.Name) &&
                            (!Direction.HasValue || q.Direction == Direction) &&
                            (!Category.HasValue || q.HolPackCat == Category)
                        orderby q.Name
                        group q by new { RecID = q.RecID, Name = q.Name } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
        }

        public List<CodeName> getSearchCategorys(bool useMarketLang, SearchPLData plSearchData, int? Country, int? City, int? Resort, int? Village, ref string errorMsg)
        {
            Country = Country > 0 ? Country : null;
            City = City > 0 ? City : null;
            Resort = Resort.HasValue && Resort.Value > 0 ? Resort : null;
            if (Village.HasValue)
                return (from q in plSearchData.plCategorys.AsEnumerable()
                        where q.Village == Village.Value
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            else
                return (from q in plSearchData.plCategorys.AsEnumerable()
                        where (string.IsNullOrEmpty(Country.ToString()) || (q.Country == Country || q.PLCountry == Country)) &&
                              (string.IsNullOrEmpty(City.ToString()) || (q.City == City || q.PLCity == City)) &&
                              (string.IsNullOrEmpty(Resort.ToString()) || q.Town == Resort)
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchCategorysCulture(bool useMarketLang, SearchPLData plSearchData, int? City, int? Resort, int? Village, Int16? Direction, Int16? HolPackCat, ref string errorMsg)
        {
            City = City > 0 ? City : null;
            Resort = Resort.HasValue && Resort.Value > 0 ? Resort : null;
            if (Village.HasValue)
                return (from q in plSearchData.plCategorys.AsEnumerable()
                        where q.Village == Village.Value && q.Direction == Direction && q.HolPackCat == HolPackCat
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            else
                return (from q in plSearchData.plCategorys.AsEnumerable()
                        where (string.IsNullOrEmpty(City.ToString()) || q.City == City) &&
                              (string.IsNullOrEmpty(Resort.ToString()) || q.Town == Resort) &&
                              q.Direction == Direction &&
                              q.HolPackCat == HolPackCat
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchCategorys(bool useMarketLang, SearchPLData plSearchData, int? Country, int? City, string Resort, Int16? HolPackCat, ref string errorMsg)
        {
            Country = Country > 0 ? Country : null;
            City = City > 0 ? City : null;
            if (string.IsNullOrEmpty(Resort))
            {
                List<CodeName> query1 = (from q in plSearchData.plCategorys.AsEnumerable()
                                         where (string.IsNullOrEmpty(Country.ToString()) || q.PLCountry == Country) &&
                                               (string.IsNullOrEmpty(City.ToString()) || q.PLCity == City) &&
                                               ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) ||
                                               (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                                         group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                         select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                return query1;
            }
            else
            {
                var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                List<CodeName> query1 = (from q in plSearchData.plCategorys.AsEnumerable()
                                         join r in resort on (q.Village.HasValue ? q.Village.Value : q.Town) equals r
                                         where ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                                         group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                         select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                return query1;
            }
        }

        public List<CodeName> getSearchCategorysCulture(bool useMarketLang, SearchPLData plSearchData, int? City, string Resort, Int16? Direction, Int16? HolPackCat, ref string errorMsg)
        {
            City = City > 0 ? City : null;
            if (string.IsNullOrEmpty(Resort))
                return (from q in plSearchData.plCategorys.AsEnumerable()
                        where (string.IsNullOrEmpty(City.ToString()) || q.PLCity == City) &&
                            q.Direction == Direction &&
                            q.HolPackCat == HolPackCat
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            else
            {
                return (from q in plSearchData.plCategorys.AsEnumerable()
                        join r in Resort.Split('|') on q.Town equals Conversion.getInt32OrNull(r)
                        where (string.IsNullOrEmpty(City.ToString()) || q.PLCity == City) &&
                            q.Direction == Direction &&
                            q.HolPackCat == HolPackCat
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
        }

        public List<CodeHotelName> getSearchHotels(bool useMarketLang, SearchPLData plSearchData, int? Location, int? Country, int? City, string Resort, string Category, string Holpack, Int16? HolPackCat, ref string errorMsg)
        {
            if (Location != null)
            {
                return (from q in plSearchData.plHotels.AsEnumerable()
                        where q.Location == Location &&
                              (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                              ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                        group q by new
                        {
                            Code = q.Code,
                            Name = useMarketLang ? q.NameL : q.Name,
                            Category = q.Category,
                            HLocationName = q.LocationName,
                            HLocationNameL = q.LocationNameL
                        } into k
                        orderby k.Key.Name
                        select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
            }
            else
            {
                if (Resort.IndexOf('|') > -1)
                {
                    Country = Country > 0 ? Country : null;
                    City = City > 0 ? City : null;
                    return (from q in plSearchData.plHotels.AsEnumerable()
                            where (string.IsNullOrEmpty(Holpack) || q.Holpack == Holpack) &&
                                  (!(HolPackCat.HasValue && q.HolPackCat.HasValue) || (HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value)) &&
                                  (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                                  (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Location.ToString()))
                            group q by new
                            {
                                Code = q.Code,
                                Name = useMarketLang ? q.NameL : q.Name,
                                Category = q.Category,
                                HLocationName = q.LocationName,
                                HLocationNameL = q.LocationNameL
                            } into k
                            orderby k.Key.Name
                            select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
                }
                else
                {
                    Country = Country > 0 ? Country : null;
                    City = City > 0 ? City : null;
                    if (City.HasValue && !string.IsNullOrEmpty(Resort) && City.ToString() == Resort)
                    {
                        return (from q in plSearchData.plHotels.AsEnumerable()
                                where (!Country.HasValue || q.Country == Country) &&
                                      (!City.HasValue || q.ArrCity == City) &&
                                      (string.IsNullOrEmpty(Holpack) || q.Holpack == Holpack) &&
                                      (!(HolPackCat.HasValue && q.HolPackCat.HasValue) || (HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value)) &&
                                      (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                                      (q.Town.HasValue && q.Town.Value == -1)
                                group q by new
                                {
                                    Code = q.Code,
                                    Name = useMarketLang ? q.NameL : q.Name,
                                    Category = q.Category,
                                    HLocationName = q.LocationName,
                                    HLocationNameL = q.LocationNameL
                                } into k
                                orderby k.Key.Name
                                select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
                    }
                    else
                    {
                        return (from q in plSearchData.plHotels.AsEnumerable()
                                where (!Country.HasValue || q.Country == Country) &&
                                      (!City.HasValue || q.ArrCity == City) &&
                                      (string.IsNullOrEmpty(Holpack) || q.Holpack == Holpack) &&
                                      (!(HolPackCat.HasValue && q.HolPackCat.HasValue) || (HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value)) &&
                                      (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                                      (string.IsNullOrEmpty(Resort) || Resort == (q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new
                                {
                                    Code = q.Code,
                                    Name = useMarketLang ? q.NameL : q.Name,
                                    Category = q.Category,
                                    HLocationName = q.LocationName,
                                    HLocationNameL = q.LocationNameL
                                } into k
                                orderby k.Key.Name
                                select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
                    }
                }
            }
        }

        public List<CodeHotelName> getSearchHotelsCulture(bool useMarketLang, SearchPLData plSearchData, int? Location, int? City, string Resort, string Category, string Holpack, Int16? Direction, Int16? HolPackCat, ref string errorMsg)
        {
            if (Location != null)
            {

                return (from q in plSearchData.plHotels.AsEnumerable()
                        where q.Location == Location &&
                              (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                              (!Direction.HasValue || q.Direction == Direction) &&
                              (!HolPackCat.HasValue || q.HolPackCat == HolPackCat)
                        group q by new
                        {
                            Code = q.Code,
                            Name = useMarketLang ? q.NameL : q.Name,
                            Category = q.Category,
                            HLocationName = q.LocationName,
                            HLocationNameL = q.LocationNameL
                        } into k
                        select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
            }
            else
            {
                City = City > 0 ? City : null;
                return (from q in plSearchData.plHotels.AsEnumerable()
                        where (string.IsNullOrEmpty(City.ToString()) || q.ArrCity == City) &&
                              (string.IsNullOrEmpty(Holpack) || q.Holpack == Holpack) &&
                              (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                              (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : "")) &&
                              (!Direction.HasValue || q.Direction == Direction) &&
                              (!HolPackCat.HasValue || q.HolPackCat == HolPackCat)
                        group q by new
                        {
                            Code = q.Code,
                            Name = useMarketLang ? q.NameL : q.Name,
                            Category = q.Category,
                            HLocationName = q.LocationName,
                            HLocationNameL = q.LocationNameL
                        } into k
                        select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
            }
        }

        public List<CodeName> getSearchHotels(bool useMarketLang, SearchPLData plSearchData, int? Location, int? Country, int? City, int? Resort, string Category, string Holpack, ref string errorMsg)
        {
            if (Location != null)
                return (from q in plSearchData.plHotels.AsEnumerable()
                        where q.Location == Location
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            else
            {
                Country = Country > 0 ? Country : null;
                City = City > 0 ? City : null;
                Resort = Resort > 0 ? Resort : null;
                return (from q in plSearchData.plHotels.AsEnumerable()
                        where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                          (string.IsNullOrEmpty(City.ToString()) || q.ArrCity == City) &&
                          (string.IsNullOrEmpty(Resort.ToString()) || q.Town == Resort) &&
                          (string.IsNullOrEmpty(Category) || q.Category == Category) &&
                          (string.IsNullOrEmpty(Holpack) || q.Holpack == Holpack)
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
        }

        public List<CodeName> getSearchRooms(bool useMarketLang, SearchPLData plSearchData, int? DepCity, int? ArrCity, string Resort, int? Village, string Hotel_Code, Int16? HolPackCat, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                List<Location> locations = TvBo.CacheObjects.getLocationList(string.Empty);
                Location loc = locations.Find(f => f.RecID == ArrCity);
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (!string.IsNullOrEmpty(Resort) && Resort.IndexOf('|') > -1)
                {
                    var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                    return (from q in plSearchData.plRooms
                            join r in resort on (q.Village.HasValue ? q.Village.Value : q.Town) equals r
                            where ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                            group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
                else
                {
                    if (Village.HasValue)
                        return (from q in plSearchData.plRooms
                                where q.Village == Village.Value &&
                                    ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                                group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        if (loc != null && loc.Parent == loc.RecID)
                        return (from q in plSearchData.plRooms
                                join q1 in locations on q.PLArrCity equals q1.RecID
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q1.Country == ArrCity) &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        return (from q in plSearchData.plRooms
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
            }
            else
            {
                return (from q in plSearchData.plRooms
                        where Hotel_Code.Split('|').Contains(q.Hotel_Code) &&
                             ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
        }

        public List<CodeName> getSearchRoomsCulture(bool useMarketLang, SearchPLData plSearchData, int? DepCity, int? ArrCity, string Resort, int? Village, string Hotel_Code, Int16? Direction, Int16? HolPackCat, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (Village.HasValue)
                    return (from q in plSearchData.plRooms
                            where q.Village == Village.Value &&
                                q.Direction == Direction &&
                                q.HolPackCat == HolPackCat
                            group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                else
                    return (from q in plSearchData.plRooms
                            where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : "")) &&
                                q.Direction == Direction &&
                                q.HolPackCat == HolPackCat
                            group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
            else
            {
                return (from q in plSearchData.plRooms
                        where Hotel_Code.Split('|').Contains(q.Hotel_Code) &&
                              q.Direction == Direction &&
                              q.HolPackCat == HolPackCat
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
        }

        public List<CodeName> getSearchRooms(bool useMarketLang, SearchPLData plSearchData, int? DepCity, int? ArrCity, int? City, int? Resort, string Hotel_Code, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                Resort = Resort > 0 ? Resort : null;
                City = City > 0 ? City : null;
                return (from q in plSearchData.plRooms.AsEnumerable()
                        where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                        (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                        (string.IsNullOrEmpty(City.ToString()) || q.City == City) &&
                        (string.IsNullOrEmpty(Resort.ToString()) || q.Town == Resort) &&
                        (string.IsNullOrEmpty(Hotel_Code) || q.Hotel_Code == Hotel_Code)
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
            else
                return (from q in plSearchData.plRooms.AsEnumerable()
                        where q.Hotel_Code == Hotel_Code
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchBoards(bool useMarketLang, SearchPLData plSearchData, int? DepCity, int? ArrCity, string Resort, int? Village, string Hotel_Code, Int16? HolPackCat, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                List<Location> locations = TvBo.CacheObjects.getLocationList(string.Empty);
                Location loc = locations.Find(f => f.RecID == ArrCity);
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (!string.IsNullOrEmpty(Resort) && Resort.Split('|').Count() > 0)
                {
                    var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                    return (from q in plSearchData.plBoards.AsEnumerable()
                            join r in resort on (q.Village.HasValue ? q.Village.Value : q.Town) equals r
                            where ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                            group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
                else
                {
                    if (Village.HasValue)
                        return (from q in plSearchData.plBoards.AsEnumerable()
                                where q.Village == Village.Value &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                                group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        if (loc != null && loc.Parent == loc.RecID)
                        return (from q in plSearchData.plBoards.AsEnumerable()
                                join q1 in locations on q.PLArrCity equals q1.RecID
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q1.Country == ArrCity) &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        return (from q in plSearchData.plBoards.AsEnumerable()
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
            }
            else
                return (from q in plSearchData.plBoards.AsEnumerable()
                        where Hotel_Code.Split('|').Contains(q.Hotel_Code) &&
                        ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchBoardsCulture(bool useMarketLang, SearchPLData plSearchData, int? DepCity, int? ArrCity, string Resort, int? Village, string Hotel_Code, Int16? Direction, Int16? HolPackCat, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (Village.HasValue)
                    return (from q in plSearchData.plBoards.AsEnumerable()
                            where q.Village == Village.Value &&
                                  q.Direction == Direction &&
                                  q.HolPackCat == HolPackCat
                            group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                else
                    return (from q in plSearchData.plBoards.AsEnumerable()
                            where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                  (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                                  (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : "")) &&
                                  q.Direction == Direction &&
                                  q.HolPackCat == HolPackCat
                            group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
            else
                return (from q in plSearchData.plBoards.AsEnumerable()
                        where Hotel_Code.Split('|').Contains(q.Hotel_Code) &&
                              q.Direction == Direction &&
                              q.HolPackCat == HolPackCat
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchBoards(bool useMarketLang, SearchPLData plSearchData, int? DepCity, int? ArrCity, int? City, int? Resort, string Hotel_Code, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                Resort = Resort > 0 ? Resort : null;
                City = City > 0 ? City : null;
                return (from q in plSearchData.plBoards.AsEnumerable()
                        where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                        (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                        (string.IsNullOrEmpty(City.ToString()) || q.City == City) &&
                        (string.IsNullOrEmpty(Resort.ToString()) || q.Town == Resort) &&
                        (string.IsNullOrEmpty(Hotel_Code) || q.Hotel_Code == Hotel_Code)
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
            else
                return (from q in plSearchData.plBoards.AsEnumerable()
                        where q.Hotel_Code == Hotel_Code
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchHolPack(bool useMarketLang, SearchPLData plSearchData, int? DepCity, int? Country, int? City, Int16? HolPackCat, ref string errorMsg)
        {
            DepCity = DepCity > 0 ? DepCity : null;
            Country = Country > 0 ? Country : null;
            City = City > 0 ? City : null;
            return (from q in plSearchData.plHolPacks.AsEnumerable()
                    where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                    (string.IsNullOrEmpty(City.ToString()) || q.ArrCity == City) &&
                    (string.IsNullOrEmpty(DepCity.ToString()) || q.DepCity == DepCity) &&
                    ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                    group q by new { Code = q.HolPack, Name = useMarketLang ? q.NameL : q.Name } into k
                    select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchHolPack(bool useMarketLang, SearchPLData plSearchData, int? DepCity, int? City, Int16? Direction, Int16? Category, ref string errorMsg)
        {
            DepCity = DepCity > 0 ? DepCity : null;
            City = City > 0 ? City : null;
            return (from q in plSearchData.plHolPacks.AsEnumerable()
                    where (string.IsNullOrEmpty(City.ToString()) || q.ArrCity == City) &&
                          (string.IsNullOrEmpty(DepCity.ToString()) || q.DepCity == DepCity) &&
                          q.Direction == Direction &&
                          q.HolPackCat == Category
                    group q by new { Code = q.HolPack, Name = useMarketLang ? q.NameL : q.Name } into k
                    select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchHolPackCat(bool useMarketLang, SearchPLData plSearchData, Int16? Direction, ref string errorMsg)
        {
            return (from q in plSearchData.PlHolPackCat.AsEnumerable()
                    where q.Direction == Direction
                    group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                    select new CodeName { Code = k.Key.Code.ToString(), Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchWithOutCTHolPackCat(bool useMarketLang, SearchPLData plSearchData, ref string errorMsg)
        {
            return (from q in plSearchData.PlLocations.AsEnumerable()
                    where q.HolPackCat.HasValue
                    group q by new { Code = q.HolPackCat, Name = useMarketLang ? q.HolPackCatNameL : q.HolPackCatName } into k
                    select new CodeName { Code = k.Key.Code.ToString(), Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getSearchFacilitys(bool useMarketLang, SearchPLData plSearchData, int? DepCity, int? ArrCity, string Resort, int? Village, string Hotel_Code, Int16? HolPackCat, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                List<Location> locations = TvBo.CacheObjects.getLocationList(string.Empty);
                Location loc = locations.Find(f => f.RecID == ArrCity);
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (!string.IsNullOrEmpty(Resort) && Resort.Split('|').Count() > 0)
                {
                    var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                    return (from q in plSearchData.PlFacility.AsEnumerable()
                            join r in resort on (q.Village.HasValue ? q.Village.Value : q.Town) equals r
                            where ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                            group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
                else
                {
                    if (Village.HasValue)
                        return (from q in plSearchData.PlFacility.AsEnumerable()
                                where q.Village == Village.Value &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                                group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        if (loc != null && loc.Parent == loc.RecID)
                        return (from q in plSearchData.PlFacility.AsEnumerable()
                                join q1 in locations on q.PLArrCity equals q1.RecID
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q1.Country == ArrCity) &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        return (from q in plSearchData.PlFacility.AsEnumerable()
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
            }
            else
                return (from q in plSearchData.PlFacility.AsEnumerable()
                        where Hotel_Code.Split('|').Contains(q.Hotel_Code) &&
                        ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                        group q by new { Code = q.Code, Name = useMarketLang ? q.NameL : q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<FlyDate> getFlightDays(string Market, string Operator, ref string errorMsg)
        {
            string tsql;
            tsql = @"   Select Distinct P.FlyDate, D.DepCity, D.ArrCity
                         From FlightPrice P (NOLOCK)    
                         Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                         Where  
                            P.Buyer = '' 
                            AND P.FlyDate >= dbo.DateOnly(GetDate())
                            AND Exists(	Select Distinct CP.Operator From CatPackMarket CPM (NOLOCK)
                                           Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                                        Where 		
                                            CP.Ready = 'Y' 
                                            And CP.WebPub = 'Y' 
                                            And CP.WebPubDate <= GetDate()
                                            And CP.SaleEndDate >= dbo.DateOnly(GetDate())
                                            And CPM.Market = @Market
                                            AND CP.Operator = P.Operator
                                       )";
            List<FlyDate> flightDayList = new List<FlyDate>();
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                        flightDayList.Add(new FlyDate
                        {
                            Date = Convert.ToDateTime(oReader["FlyDate"]),
                            DepCity = Convert.ToInt32(oReader["DepCity"]),
                            ArrCity = Convert.ToInt32(oReader["ArrCity"])
                        });
                }
                return flightDayList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return flightDayList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> getEmptySearch(User UserData, SearchCriteria filter, bool checkAvailableRoom, SearchType sType, bool checkAvailableFlightSeat, Guid? logID, ref string errorMsg)
        {
            string resortWhereStr = string.Empty;
            string categoryWhereStr = string.Empty;
            string hotelWhereStr = string.Empty;
            string roomWhereStr = string.Empty;
            string boardWhereStr = string.Empty;
            List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

            if (!string.IsNullOrEmpty(filter.Resort))
            {
                if (filter.Resort.Split('|').Length > 0)
                {
                    if (filter.Resort.Split('|').Length == 1)
                    {
                        resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", filter.Resort.Split('|')[0]);
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                    else
                    {
                        foreach (string r in filter.Resort.Split('|'))
                        {
                            Location _location = locations.Find(f => f.RecID == Conversion.getInt32OrNull(r));
                            if (_location != null)
                            {
                                if (_location.Type == 3)
                                {
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                                    foreach (Location rL in locations.Where(w => w.Parent == _location.RecID))
                                        resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", rL.RecID);
                                }
                                else
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                            }
                        }
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                }
                else
                    resortWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Category))
            {
                foreach (string r in filter.Category.Split('|'))
                    categoryWhereStr += string.Format("INSERT INTO @Category(Code) VALUES('{0}') \n", r);
                if (categoryWhereStr.Length > 0)
                    categoryWhereStr = "DECLARE @Category TABLE (Code VarChar(10)) \n" + categoryWhereStr;
                else
                    categoryWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Hotel))
            {
                foreach (string r in filter.Hotel.Split('|'))
                    hotelWhereStr += string.Format("INSERT INTO @Hotel(Code) VALUES('{0}') \n", r);
                if (hotelWhereStr.Length > 0)
                    hotelWhereStr = "DECLARE @Hotel TABLE (Code VarChar(10)) \n" + hotelWhereStr;
                else
                    hotelWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Room))
            {
                foreach (string r in filter.Room.Split('|'))
                    roomWhereStr += string.Format("INSERT INTO @Room(Code) VALUES('{0}') \n", r);
                if (roomWhereStr.Length > 0)
                    roomWhereStr = "DECLARE @Room TABLE (Code VarChar(10) COLLATE Latin1_General_CI_AS) \n" + roomWhereStr;
                else
                    roomWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Board))
            {
                foreach (string r in filter.Board.Split('|'))
                    boardWhereStr += string.Format("INSERT INTO #tmpBoard(Code) VALUES('{0}') \n", r);
                if (boardWhereStr.Length > 0)
                    boardWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpBoard') is not null Drop Table dbo.#tmpBoard
Create Table #tmpBoard (Code VarChar(10) COLLATE Latin1_General_CI_AS) " + boardWhereStr;
                else
                    boardWhereStr = string.Empty;
            }
            string xmlAgeTable = string.Empty;
            if (filter.RoomCount > 0)
            {
                xmlAgeTable += @"
if OBJECT_ID('#tmpRoomTable') is not null Drop Table #tmpRoomTable 
Create Table #tmpRoomTable (RoomNr int, Adult int, Child int, ChdAge1 int, ChdAge2 int, ChdAge3 int, ChdAge4 int)";
                foreach (SearchCriteriaRooms row in filter.RoomsInfo)
                {
                    xmlAgeTable += string.Format("Insert Into #tmpRoomTable (RoomNr, Adult, Child, ChdAge1, ChdAge2, ChdAge3, ChdAge4) Values ({0}, {1}, {2}, {3}, {4}, {5}, {6})",
                        row.RoomNr,
                        row.Adult,
                        row.Child,
                        row.Chd1Age.HasValue ? row.Chd1Age.Value.ToString() : "-1",
                        row.Chd2Age.HasValue ? row.Chd2Age.Value.ToString() : "-1",
                        row.Chd3Age.HasValue ? row.Chd3Age.Value.ToString() : "-1",
                        row.Chd4Age.HasValue ? row.Chd4Age.Value.ToString() : "-1");
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("SET ARITHABORT ON \n");
            sb.Append("SET ANSI_WARNINGS OFF \n");
            sb.Append("SET DATEFIRST 1 \n");
            sb.AppendLine("Declare @Agency varChar(10),@Operator varChar(10),@Office varChar(10),@Market varChar(10),@CIn1 DateTime ");
            if (filter.ExpandDate > 0)
                sb.AppendLine("  ,@CIn2 DateTime");
            sb.AppendLine("  ,@Night1 smallInt");
            if (filter.NightFrom < filter.NightTo)
                sb.AppendLine("  ,@Night2 smallInt");

            sb.AppendLine("Select @Agency=@pAgency,@Operator=@pOperator,@Office=@pOffice,@Market=@pMarket,@CIn1=@pCIn1 ");
            if (filter.ExpandDate > 0)
                sb.AppendLine("  ,@CIn2=@pCIn2");
            sb.AppendLine("  ,@Night1=@pNight1");
            if (filter.NightFrom < filter.NightTo)
                sb.AppendLine("  ,@Night2=@pNight2");

            sb.Append(xmlAgeTable);
            sb.Append("Declare @totalSeat int \n");
            sb.Append("Select @totalSeat=(SUM(Adult)+Sum(Child)-Sum((Case When ChdAge1 < 2 and ChdAge1 > -1 then 1 else 0 end)+ \n");
            sb.Append("				                                (Case When ChdAge2 < 2 and ChdAge2 > -1 then 1 else 0 end)+ \n");
            sb.Append("				                                (Case When ChdAge3 < 2 and ChdAge3 > -1 then 1 else 0 end)+ \n");
            sb.Append("				                                (Case When ChdAge4 < 2 and ChdAge4 > -1 then 1 else 0 end))) \n");
            sb.Append("From #tmpRoomTable \n");

            sb.Append("if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#AgencyCP') is not null Drop Table dbo.#AgencyCP \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#PriceList') is not null Drop Table dbo.#PriceList \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#AvailableFlights') is not null Drop Table dbo.#AvailableFlights \n");

            sb.Append("CREATE TABLE #Location (RecID int,Name varChar(100),NameL nvarChar(300),Village int,Town int,City int,Country int) \n");

            sb.Append("Insert Into #Location (RecID,Name,NameL,Village,Town,City,Country) \n");
            sb.Append("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Village,Town,City,Country \n");
            sb.Append("From Location (NOLOCK) \n");
            sb.Append("Order By RecID \n");

            sb.Append("Select *  \n");
            sb.Append("Into #CatalogPack \n");
            sb.Append("From CatalogPack (NOLOCK) CP \n");
            sb.Append("Where	CP.Ready = 'Y'  \n");
            sb.Append("	And CP.EndDate > GetDate()-1  \n");
            sb.Append("	And CP.WebPub = 'Y'  \n");
            sb.Append("	And CP.WebPubDate < GetDate() \n");
            sb.Append("	And dbo.DateOnly(GETDATE()) BetWeen CP.SaleBegDate And CP.SaleEndDate \n");

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                if (filter.HolPackCat.HasValue)
                {
                    sb.AppendFormat(" And CP.Category = {0} \n", filter.HolPackCat.Value);
                    //if (filter.HolPackCat == 1)
                    //    filter.SType = SearchType.PackageSearch;

                }
                else
                    sb.Append(" And CP.Category is null \n");

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030300001"))
                sb.Append("	And (isnull(CP.Allotment,32000)-isnull(CP.Used,0))>0  \n");
            if (filter.SType != SearchType.OnlyHotelSearch)
            {
                if (filter.DepCity.HasValue)
                    sb.AppendFormat(" And CP.DepCity = {0} \n", filter.DepCity.ToString());
            }
            if (filter.ArrCity.HasValue)
            //if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                Location loc = locations.Find(f => f.RecID == filter.ArrCity.Value);
                if (loc != null && loc.Parent == loc.RecID)
                    sb.AppendFormat("  And CP.ArrCity in (Select City From Location (NOLOCK) Where Country={0}) \n", filter.ArrCity.Value.ToString());
                else
                    sb.AppendFormat("  And CP.ArrCity={0} \n", filter.ArrCity.Value.ToString());
            }
            //else sb.AppendFormat("  And CP.ArrCity={0} \n", filter.ArrCity.Value.ToString());

            switch (filter.SType)
            {
                case SearchType.PackageSearch:
                    if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && filter.HolPackCat.HasValue)
                            sb.Append("  And CP.PackType in ('H','O')  \n");
                    else
                        sb.Append("  And CP.PackType = 'H'  \n");
                    break;
                case SearchType.TourPackageSearch:
                    sb.Append("  And CP.PackType = 'T'  \n");
                    break;
                case SearchType.OnlyHotelSearch:
                    if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && filter.HolPackCat.HasValue)
                        sb.Append("  And CP.PackType in ('H','O')  \n");
                    else
                        sb.Append("  And CP.PackType = 'O'  \n");
                    break;
                default:
                    sb.Append("  And CP.PackType = 'none'  \n");
                    break;
            }
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0024, VersionControl.Equality.gt))
                if (!string.IsNullOrEmpty(filter.B2BMenuCat))
                    sb.AppendFormat("	And isnull(CP.B2BMenuCat,'')='{0}' \n", filter.B2BMenuCat.ToString());
                else
                    sb.Append("  And (isnull(CP.B2BMenuCat,'')='' Or CP.B2BMenuCat=CP.PackType) \n");

            sb.Append("Select Distinct RecID  \n");
            sb.Append("Into #AgencyCP \n");
            sb.Append("From  \n");
            sb.Append("( \n");
            sb.Append("	Select CP.RecID \n");
            sb.Append("	From #CatalogPack CP (NOLOCK)  \n");
            sb.Append("	Where   \n");
            sb.Append("	 Not Exists(Select RecID From CatPackAgency (NOLOCK) Where CatPackID=CP.RecID) or \n");
            sb.Append("  Exists(Select CA.RecID From CatPackAgency CA (NOLOCK) \n");
            sb.Append("	        Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups=CA.AgencyGrp and AG.Agency=@Agency --And AG.GrpType='P' \n");
            sb.Append("	        Where CA.CatPackID=CP.RecID and (AG.Agency=@Agency or CA.Agency=@Agency)) \n");
            sb.Append(") CP \n");
            sb.Append(" \n");
            sb.Append("Select CP.PackType,\n");
            sb.Append("	RT.RoomNr, PA.CatPackID, ARecNo = PA.RecNo, PRecNo = PP.RecNo, HAPRecID = HAP.RecID, PA.Hotel, PA.Room, PA.Board, PA.Accom, PP.Night, PP.HotNight, PP.CheckIn, \n");
            sb.Append(" CP.Operator, CP.FlightClass, CP.HolPack, CP.SaleCur, PLCur=CP.SaleCur, CP.DepCity, CP.ArrCity, flySeat=(HAP.Adult+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4),HAdult=HAP.Adult, \n");
            // #15276 Ticket geliştirmesi. 
            sb.Append(" Child = RT.Child,ChdAge1 = RT.ChdAge1,ChdAge2 = RT.ChdAge2,ChdAge3 = RT.ChdAge3,ChdAge4 = RT.ChdAge4,HChdAgeG1 = HAP.ChdAgeG1,HChdAgeG2 = HAP.ChdAgeG2,HChdAgeG3 = HAP.ChdAgeG3,HChdAgeG4 = HAP.ChdAgeG4, \n");
            sb.Append(" ChdG1Age1 = PP.ChdG1Age1,ChdG1Age2 = PP.ChdG1Age2,ChdG2Age1 = PP.ChdG2Age1,ChdG2Age2 = PP.ChdG2Age2,ChdG3Age1 = PP.ChdG3Age1,ChdG3Age2 = PP.ChdG3Age2,ChdG4Age1 = PP.ChdG4Age1,ChdG4Age2 = PP.ChdG4Age2, \n");
            // #15276 Ticket geliştirmesi.
            sb.Append(" DepCityName=(Select Top 1 Name From #Location WHERE RecID=CP.DepCity), \n");
            sb.Append(" ArrCityName=(Select Top 1 Name From #Location WHERE RecID=CP.ArrCity), \n");
            sb.Append(" DepCityNameL=(Select Top 1 NameL From #Location WHERE RecID=CP.DepCity), \n");
            sb.Append(" ArrCityNameL=(Select Top 1 NameL From #Location WHERE RecID=CP.ArrCity), \n");
            sb.Append("	RoomName=HR.Name, \n");
            sb.Append("	RoomNameL=(Select isnull(dbo.FindLocalName(HR.NameLID,@Market),isnull(dbo.FindLocalName(NameLID,@Market),HR.Name)) From Room (NOLOCK) Where Room.Code=PA.Room), \n");
            sb.Append("	AccomName=HA.Name, \n");
            sb.Append("	AccomNameL=(Select isnull(dbo.FindLocalName(HA.NameLID,@Market),isnull(dbo.FindLocalName(NameLID,@Market),HA.Name)) From RoomAccom (NOLOCK) Where Code=PA.Accom), \n");
            sb.Append("	BoardName=HB.Name, \n");
            sb.Append("	BoardNameL=(Select isnull(dbo.FindLocalName(HB.NameLID,@Market),isnull(dbo.FindLocalName(NameLID,@Market),HB.Name)) From Board (NOLOCK) Where Code=PA.Board), \n");
            sb.Append("	HotelName=H.Name, \n");
            sb.Append("	HotelNameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name), \n");
            sb.Append("	HotLocation=H.Location, HotCat=H.Category, \n");
            sb.Append("	HotLocationName=(Select Top 1 Name From #Location Where RecID=H.Location), \n");
            sb.Append("	HotLocationNameL=(Select Top 1 NameL From #Location Where RecID=H.Location), \n");

            sb.Append("	CalcSalePrice= \n");
            sb.Append("	    (Case When HAP.Adult>PA.Adl then PA.Adl else HAP.Adult end * PP.SaleAdl)+ \n");
            sb.Append("		 Case when HAP.Adult-PA.Adl>=1 then dbo.IfZero(PP.SaleExtBed1,PP.SaleAdl) else 0 end+ \n");
            sb.Append("		 Case when HAP.Adult-PA.Adl>=2 then dbo.IfZero(PP.SaleExtBed2,PP.SaleAdl) else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG1>0 then (PP.SaleChdG1*HAP.ChdAgeG1)+CHDP.ChdG1Price else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG2>0 then (PP.SaleChdG2*HAP.ChdAgeG2)+CHDP.ChdG2Price else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG3>0 then (PP.SaleChdG3*HAP.ChdAgeG3)+CHDP.ChdG3Price else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG4>0 then (PP.SaleChdG4*HAP.ChdAgeG4)+CHDP.ChdG4Price else 0 end, \n");
            sb.Append("	SaleChdG1= Case when HAP.ChdAgeG1>0 then PP.SaleChdG1+CHDP.ChdG1Price/dbo.IfZero(HAP.ChdAgeG1,1) else 0 end, \n");
            sb.Append("	SaleChdG2= Case when HAP.ChdAgeG2>0 then PP.SaleChdG2+CHDP.ChdG2Price/dbo.IfZero(HAP.ChdAgeG2,1) else 0 end, \n");
            sb.Append("	SaleChdG3= Case when HAP.ChdAgeG3>0 then PP.SaleChdG3+CHDP.ChdG3Price/dbo.IfZero(HAP.ChdAgeG3,1) else 0 end, \n");
            sb.Append("	SaleChdG4= Case when HAP.ChdAgeG4>0 then PP.SaleChdG4+CHDP.ChdG4Price/dbo.IfZero(HAP.ChdAgeG4,1) else 0 end, \n");
            sb.Append("	HotChdPrice= Case when HAP.ChdAgeG1>0 then CHDP.ChdG1Price else 0 end+ \n");
            sb.Append("				 Case when HAP.ChdAgeG2>0 then CHDP.ChdG2Price else 0 end+ \n");
            sb.Append("				 Case when HAP.ChdAgeG3>0 then CHDP.ChdG3Price else 0 end+ \n");
            sb.Append("				 Case when HAP.ChdAgeG4>0 then CHDP.ChdG4Price else 0 end, \n");
            sb.Append("	CP.Market, FreeRoom=null, DepSeat=null, RetSeat=Null, \n");
            sb.Append(" StopSaleGuar=dbo.ufn_StopSaleCnt(PA.Hotel, PA.Room, PA.Accom, PA.Board, @Market, @Agency, PA.CatPackID, H.Location, -2, (PP.CheckIn+(PP.Night-isnull(PP.HotNight,PP.Night))), PP.CheckIn+PP.Night, 0, 1), \n");
            sb.Append(" StopSaleStd=dbo.ufn_StopSaleCnt(PA.Hotel, PA.Room, PA.Accom, PA.Board, @Market, @Agency, PA.CatPackID, H.Location, 2, (PP.CheckIn+(PP.Night-isnull(PP.HotNight,PP.Night))), PP.CheckIn+PP.Night, 0, 1), \n");
            sb.Append(" HotelRecID=H.RecID, \n");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040018050"))
                sb.Append(" AutoStop=isnull(HR.AutoStop,0), \n");
            else
                sb.Append(" AutoStop=Cast(0 As bit), \n");
            sb.Append(" TransportType=Cast(0 As smallint), \n");
            sb.Append(" DepServiceType=isnull(CP.DepServiceType,''),RetServiceType=isnull(CP.RetServiceType,''), \n");

            if (UserData.ShowFlight && filter.SType == SearchType.PackageSearch)
            {
                sb.Append("    DepFlight='          ', \n");
                sb.Append("    RetFlight='          ', \n");
                sb.Append("    DFlight='          ', \n");
                sb.Append("    DepFlightTime=Cast(0 AS DateTime), \n");
                sb.Append("    RetFlightTime=Cast(0 AS DateTime) \n");


                //sb.Append("    DepFlight=isnull(FDD.FlightNo,'          '), RetFlight=isnull(FDD.NextFlight,'          '), \n");
                //sb.Append("    DFlight=FDD.FlightNo, \n");
                //sb.Append("    DepFlightTime=isnull(FDD.DepTime, Cast(0 AS DateTime)), \n");
                //sb.Append("    RetFlightTime=isnull(FDR.DepTime, Cast(0 AS DateTime)) \n");
            }
            else
            {
                sb.Append("    DepFlight='          ', RetFlight='          ', \n");
                sb.Append("    DFlight='          ', \n");
                sb.Append("    DepFlightTime=Cast(0 AS DateTime), RetFlightTime=Cast(0 AS DateTime) \n");
            }

            if (string.Equals(UserData.CustomRegID, Common.crID_UpJet) || string.Equals(UserData.CustomRegID, Common.crID_CelexTravel))
                sb.Append("    ,DispNo=isnull(Do.DispNo,9999) \n");
            sb.Append(@" 
                        ,DepFlightForSpo='          ',
	                    DepFlightMarket=Space(10)");

            sb.Append("Into #PriceList \n");
            sb.Append("From CatPriceA PA (NOLOCK) \n");
            sb.Append("Join CatPriceP PP (NOLOCK) on PP.CatPackID=PA.CatPackID and PP.PriceAID=PA.RecNo \n");
            if (filter.SType == SearchType.TourPackageSearch)
                sb.Append("Join #CatalogPack CP on CP.RecID=PA.CatPAckID  \n");
            else
                sb.Append("Join #CatalogPack CP on CP.RecID=PA.CatPAckID and CP.PackType<>'T' \n");

            sb.Append("join Hotel H  (NOLOCK) on H.Code=PA.Hotel \n");

            if (string.Equals(UserData.CustomRegID, Common.crID_UpJet) || string.Equals(UserData.CustomRegID, Common.crID_CelexTravel))
                sb.AppendLine("Left Join HotelDispOrder Do (NOLOCK) on H.Code=Do.Hotel And Do.Market=CP.Market and Do.Operator=CP.Operator ");

            sb.Append("join HotelAccomPax HAP (NOLOCK) on HAP.Hotel=PA.Hotel and HAP.Room=PA.Room and HAP.Accom=PA.Accom and IsNull(HAP.VisiblePL,'Y')='Y' \n");
            sb.Append("OUTER APPLY \n");
            sb.Append("( \n");
            sb.Append("   Select Max(Cast(isnull(Adult,0) as varchar(1)) + Cast(isnull(ChdAgeG1,0) as varchar(1)) + Cast(isnull(ChdAgeG2,0) as varchar(1)) + Cast(isnull(ChdAgeG3,0) as varchar(1)) + Cast(isnull(ChdAgeG4,0) as varchar(1))) AdlStr \n");
            sb.Append("   From CatHotelChd C (NOLOCK) \n");
            sb.Append("   where C.CatPackID=PP.CatPackID and C.RecNo=PP.HotelChdRecNo and \n");
            sb.Append("         dbo.IfZero(C.Adult,HAP.Adult)=HAP.Adult and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG1,HAP.ChdAgeG1)=HAP.ChdAgeG1 and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG2,HAP.ChdAgeG2)=HAP.ChdAgeG2 and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG3,HAP.ChdAgeG3)=HAP.ChdAgeG3 and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG4,HAP.ChdAgeG4)=HAP.ChdAgeG4 and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG1+C.ChdAgeG2+C.ChdAgeG3+C.ChdAgeG4,HAP.ChdAgeG1+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4)=HAP.ChdAgeG1+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4 \n");
            sb.Append(") CHDM \n");
            sb.Append("OUTER APPLY \n");
            sb.Append("( \n");
            sb.Append("Select Top 1 * From CatHotelChd C (NOLOCK) \n");
            sb.Append("Where C.CatPackID=PP.CatPackID and C.RecNo=PP.HotelChdRecNo and \n");
            sb.Append("      (Cast(isnull(Adult,0) as varchar(1)) + Cast(isnull(ChdAgeG1,0) as varchar(1)) + Cast(isnull(ChdAgeG2,0) as varchar(1)) + Cast(isnull(ChdAgeG3,0) as varchar(1)) + Cast(isnull(ChdAgeG4,0) as varchar(1))) = CHDM.AdlStr \n");
            sb.Append(") CHD \n");
            sb.Append("OUTER APPLY \n");
            sb.Append("( \n");
            sb.Append("Select * \n");
            sb.Append("From dbo.HotChdCalc(IsNull(CHD.ChdG1PriceN1,0),IsNull(CHD.ChdG1PriceN2,0),IsNull(CHD.ChdG1PriceN3,0),IsNull(CHD.ChdG1PriceN4,0), \n");
            sb.Append("                    IsNull(CHD.ChdG2PriceN1,0),IsNull(CHD.ChdG2PriceN2,0),IsNull(CHD.ChdG2PriceN3,0),IsNull(CHD.ChdG2PriceN4,0), \n");
            sb.Append("                    IsNull(CHD.ChdG3PriceN1,0),IsNull(CHD.ChdG3PriceN2,0),IsNull(CHD.ChdG3PriceN3,0),IsNull(CHD.ChdG3PriceN4,0), \n");
            sb.Append("                    IsNull(CHD.ChdG4PriceN1,0),IsNull(CHD.ChdG4PriceN2,0),IsNull(CHD.ChdG4PriceN3,0),IsNull(CHD.ChdG4PriceN4,0), \n");
            sb.Append("                    HAP.ChdAgeG1,HAP.ChdAgeG2,HAP.ChdAgeG3,HAP.ChdAgeG4,H.ChdCalcSale,IsNull(CHD.ChdG1Out,'N'), \n");
            sb.Append("                    PP.ChdG1Age1,PP.ChdG1Age2,PP.ChdG2Age1,PP.ChdG2Age2,PP.ChdG3Age1,PP.ChdG3Age2,PP.ChdG4Age1,PP.ChdG4Age2, \n");
            sb.Append("                    PA.CatPackID,0,0,0,0) \n");
            sb.Append(") CHDP \n");
            if (UserData.ShowFlight && filter.SType == SearchType.PackageSearch)
            {
                if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0023, VersionControl.Equality.gt))
                    sb.Append("OUTER APPLY (Select HolPack,StepNo From HolPackPlan (NOLOCK) Where HolPack=CP.HolPack And [Service]=isnull(CP.DepServiceType,'FLIGHT') And Departure=isnull(CP.DepDepCity,CP.DepCity) and Arrival=isnull(CP.DepArrCity,CP.ArrCity)) HPP \n");
                else
                    sb.Append("Left JOIN HolPackPlan HPP (NOLOCK) on HPP.HolPack = CP.HolPack and HPP.Service = 'FLIGHT' and HPP.Departure = CP.DepCity and HPP.Arrival = CP.ArrCity \n");
                //sb.Append("OUTER APPLY (Select ServiceItem From HolPackSer (NOLOCK) Where  HolPack=HPP.HolPack And StepNo=HPP.StepNo) HS \n");
                //sb.Append("OUTER APPLY (Select FlightNo, NextFlight,DepTime=isnull(TDepDate,FlyDate)+dbo.TimeOnly(isnull(TDepTime,DepTime)) From FlightDay (NOLOCK) Where FlightNo=HS.ServiceItem And FlyDate=PP.CheckIn) FDD \n");
                //sb.Append("OUTER APPLY (Select FlightNo, NextFlight,DepTime=isnull(TDepDate,FlyDate)+dbo.TimeOnly(isnull(TDepTime,DepTime)) From FlightDay (NOLOCK) Where FlightNo=FDD.NextFlight and FlyDate=(PP.CheckIn+PP.Night)) FDR \n");
            }
            sb.Append("Join #tmpRoomTable RT on RT.Adult=HAP.Adult And RT.Child=(HAP.ChdAgeG1+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4) \n");

            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0008, VersionControl.Equality.gt))
                sb.Append(@"join HotelRoom HR (NOLOCK) on HR.Hotel=PA.Hotel and HR.Code=PA.Room and IsNull((Select VisiblePL From HotelRoomMarOpt (NOLOCK) Where RoomID=HR.RecID and Market=CP.Market),IsNull(HR.VisiblePL,'Y'))='Y'
                            join HotelAccom HA (NOLOCK) on HA.Hotel=PA.Hotel and HA.Room=PA.Room and HA.Code=PA.Accom and IsNull((Select VisiblePL From HotelAccomMarOpt (NOLOCK) Where AccomID=HA.RecID and Market=CP.Market), IsNull(HA.VisiblePL,'Y'))='Y'
                            join HotelBoard HB (NOLOCK) on HB.Hotel=PA.Hotel and HB.Code=PA.Board and IsNull((Select VisiblePL From HotelBoardMarOpt (NOLOCK) Where BoardID=HB.RecID and Market=CP.Market),IsNull(HB.VisiblePL,'Y'))='Y'  ");
            else
                sb.Append(@"join HotelRoom HR (NOLOCK) on HR.Hotel=PA.Hotel and HR.Code=PA.Room and IsNull(HR.VisiblePL,'Y')='Y' 
                            join HotelAccom HA (NOLOCK) on HA.Hotel=PA.Hotel and HA.Room=PA.Room and HA.Code=PA.Accom and IsNull(HA.VisiblePL,'Y')='Y' 
                            join HotelBoard HB (NOLOCK) on HB.Hotel=PA.Hotel and HB.Code=PA.Board and IsNull(HB.VisiblePL,'Y')='Y' ");


            sb.Append("Where Exists(Select RecID From CatPackMarket (NOLOCK) Where CatPackID=PA.CatPackID and Market=@Market and (Operator=@Operator or Operator='')   )  \n");

            sb.Append("  And IsNull(PA.Status,1) = 1  \n");
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
            {
                if (filter.ExpandDate == 0)
                    sb.Append("  And (PP.CheckIn + (PP.Night - IsNull(PP.HotNight, PP.Night))) = @CIn1 \n");
                else
                    sb.Append("  And (PP.CheckIn + (PP.Night - IsNull(PP.HotNight, PP.Night))) Between @CIn1 AND @CIn2  \n");
            }
            else
            {
                if (filter.ExpandDate == 0)
                    sb.Append("  And PP.CheckIn = @CIn1 \n");
                else
                    sb.Append("  And PP.CheckIn Between @CIn1 AND @CIn2  \n");
            }
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                if (filter.NightFrom >= filter.NightTo)
                    sb.Append("  And IsNull(PP.HotNight,PP.Night) = @Night1 \n");
                else
                    sb.Append("  And IsNull(PP.HotNight,PP.Night) between @Night1 and @Night2 \n");
            }
            else
            {
                if (filter.NightFrom >= filter.NightTo)
                    sb.Append("  And PP.Night = @Night1 \n");
                else
                    sb.Append("  And PP.Night between @Night1 and @Night2 \n");
            }

            // #15276 Ticket geliştirmesi.
            //sb.Append("  And dbo.ChdAgeControl4Age(RT.Child, RT.ChdAge1, Rt.ChdAge2, Rt.ChdAge3, Rt.ChdAge4, HAP.ChdAgeG1, HAP.ChdAgeG2, HAP.ChdAgeG3, HAP.ChdAgeG4, PP.ChdG1Age1, PP.ChdG1Age2, PP.ChdG2Age1, PP.ChdG2Age2, PP.ChdG3Age1, PP.ChdG3Age2, PP.ChdG4Age1, PP.ChdG4Age2) = 1 \n");

            sb.Append("  And Exists(Select RecID From #AgencyCP Where RecID = PA.CatPackID)  \n");

            if (!string.IsNullOrEmpty(hotelWhereStr))
            {
                sb.Insert(0, hotelWhereStr);
                sb.Append(" And Exists(Select Code From @Hotel Where Code=H.Code) \n");
            }

            if (!string.IsNullOrEmpty(filter.Package))
                sb.AppendFormat("  And CP.HolPack = '{0}' \n", filter.Package);

            if (!string.IsNullOrEmpty(categoryWhereStr))
            {
                sb.Insert(0, categoryWhereStr);
                sb.Append("  And Exists(Select Code From @Category Where Code=H.Category)");
            }

            if (!string.IsNullOrEmpty(resortWhereStr))
            {

                sb.Insert(0, resortWhereStr);
                if (filter.Resort.Split('|').Length == 1)
                {
                    Location _locations = locations.Find(f => f.RecID == Conversion.getInt32OrNull(filter.Resort.Split('|')[0]));
                    if (_locations != null && _locations.Type == 3)
                        sb.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.Town={0} And L.RecID=H.Location) \n", _locations.RecID);
                    else
                        sb.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.RecID={0} And L.RecID=H.Location) \n", _locations.RecID);
                }
                else
                    sb.Append("  And Exists(Select R.RecID From #tmpResort R Where R.RecID=H.Location) \n");
            }

            if (!string.IsNullOrEmpty(roomWhereStr))
            {
                sb.Insert(0, roomWhereStr);
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Room From GrpRoomDet GRD (NOLOCK) Where Exists(Select Code From @Room Where Code=GRD.Groups) And Room=PA.Room) \n");
                else
                    sb.Append("  And Exists(Select Code From @Room Where Code=PA.Room) \n");
            }

            if (!string.IsNullOrEmpty(boardWhereStr))
            {
                sb.Insert(0, boardWhereStr);
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Board From GrpBoardDet GBD (NOLOCK) Where Exists(Select Code From #tmpBoard Where Code=GBD.Groups) And Board=PA.Board) \n");
                else
                    sb.Append("  And Exists(Select Code From #tmpBoard Where Code=PA.Board) \n");
            }
            sb.Append(" Declare @DepFlight varchar(10),@DepFlightTime DateTime, \n");
            sb.Append(" 	    @RetFlight varchar(10),@RetFlightTime DateTime, \n");
            sb.Append(" 	    @DepClass varchar(5), \n");
            sb.Append(" 	    @RetClass varchar(5), \n");
            sb.Append(" 	    @DepFree int, \n");
            sb.Append(" 	    @RetFree int, \n");
            sb.Append(" 	    @HolPack varChar(10), \n");
            sb.Append(" 	    @CheckIn dateTime,  \n");
            sb.Append(" 	    @Night int, \n");
            sb.Append(" 	    @HAdult int \n");
            sb.Append(" \n");
            sb.Append(" Update #PriceList Set DepFlight=DepFlightForSpo Where DepFlight=''");
            sb.Append(" Select Distinct X.HolPack,X.CheckIn,X.Night,HAdult=X.flySeat \n");
            sb.Append(" Into #AvailableFlights  \n");
            sb.Append(" From #PriceList X \n");
            sb.Append(" Where X.PackType <> 'O' \n");
            sb.Append(" \n");
            sb.Append(" Declare RCursor CURSOR LOCAL FAST_FORWARD FOR \n");
            sb.Append("     Select HolPack, CheckIn, Night, HAdult From #AvailableFlights \n");
            sb.Append("     Open RCursor \n");
            sb.Append("     Fetch Next FROM RCursor INTO @HolPack, @CheckIn, @Night, @HAdult \n");
            sb.Append("     While @@FETCH_STATUS = 0 \n");
            sb.Append("     begin   \n");
            sb.Append(" 			  \n");
            sb.Append("     Select @RetFlight = '', @DepFlight = '', @DepFree = 0, @RetFree = 0 \n");
            sb.Append("     if isnull(@DepFlight, '') = '' or isnull(@RetFlight, '') = '' \n");
            sb.Append(" 	    exec dbo.usp_GetAvailableFlightInPackage @HolPack, @CheckIn, @Night, @totalSeat, @DepFlight output, @RetFlight output, @DepClass output, @RetClass output, @DepFree output, @RetFree output \n");
            sb.Append("  \n");
            sb.Append("     if isnull(@RetFlight,'') = '' or @RetFlight = '%' \n");
            sb.Append(" 	    Select Top 1 @RetFlight = NextFlight From FlightDay (nolock) where FlightNo = @DepFlight and FlyDate = @CheckIn \n");
            sb.Append("  \n");
            sb.Append("     if (isnull(@DepFlight, '') <> '' And isnull(@DepFlight,'')<> '%') \n");
            sb.Append("     Begin \n");
            sb.Append("     Select @DepFlightTime=isnull(TDepDate,FlyDate)+dbo.TimeOnly(isnull(TDepTime,DepTime)) From FlightDay (NOLOCK) \n");
            sb.Append("         Where FlightNo = @DepFlight And FlyDate = @CheckIn \n");
            sb.Append("         \n");
            sb.Append("     End \n");
            sb.Append("      \n");
            sb.Append("     if (isnull(@RetFlight, '') <> '' And isnull(@RetFlight,'')<> '%') \n");
            sb.Append("     Begin \n");
            sb.Append("     Select @RetFlightTime=isnull(TDepDate,FlyDate)+dbo.TimeOnly(isnull(TDepTime,DepTime)) From FlightDay (NOLOCK) \n");
            sb.Append("         Where FlightNo = @RetFlight And FlyDate = @CheckIn + @Night \n");
            sb.Append("         \n");
            sb.Append("     End \n");
            sb.Append("     update #PriceList  \n");
            sb.Append("	    Set DepSeat = @DepFree,RetSeat = @RetFree, \n");
            sb.Append("     DepFlight=isnull(@DepFlight,''),RetFlight=isnull(@RetFlight,''),FlightClass=@DepClass,DepFlightTime=@DepFlightTime,RetFlightTime=@RetFlightTime,DepFlightForSPO = isnull(@DepFlight,'') \n");
            sb.Append("     where HolPack=@HolPack and CheckIn=@CheckIn and Night=@Night and HAdult<=@HAdult  \n");
            sb.Append(" 	and (isnull(@DepClass,'')<>'' or DepServiceType<>'FLIGHT' ) \n");
            if (string.Equals(UserData.CustomRegID, Common.crID_Detur))
            {
                sb.Append(@"
                            Update #PriceList  Set DepFlightMarket=O.Market --burası 2.nokta
	                         From FlightDay FD (NOLOCK)
	                        Join Operator O (NOLOCK) on O.Code=FD.FlgOwner
                            Where FlightNo = DepFlightForSpo and FlyDate = @CheckIn
            ");
            }
            sb.Append("  \n");
            sb.Append("     Fetch Next FROM RCursor INTO @HolPack, @CheckIn, @Night, @HAdult \n");
            sb.Append(" end \n");
            sb.Append(" close RCursor \n");
            sb.Append(" deallocate RCursor \n");

            sb.AppendLine("Declare @CatPackId int, ");
            sb.AppendLine("        @DepServiceType varChar(10), ");
            sb.AppendLine("        @DepService varChar(10),");
            sb.AppendLine("        @RetServiceType varChar(10),");
            sb.AppendLine("        @RetService varChar(10)");
            sb.AppendLine(" Declare SnCursor CURSOR LOCAL FAST_FORWARD FOR");
            sb.AppendLine(" Select Distinct X.CatPackID, X.CheckIn, X.Night, X.DepServiceType, X.RetServiceType");
            sb.AppendLine(" From #PriceList X ");
            sb.AppendLine(" Where X.DepFlight = '          '");
            sb.AppendLine("   And X.RetFlight = '          '");
            sb.AppendLine("   And X.DepServiceType <> X.RetServiceType");
            sb.AppendLine("   And X.PackType<>'O'");
            sb.AppendLine(" Open SnCursor");
            sb.AppendLine(" Fetch Next FROM SnCursor INTO @CatPackId, @CheckIn, @Night, @DepServiceType, @RetServiceType");
            sb.AppendLine(" While @@FETCH_STATUS = 0");
            sb.AppendLine(" Begin");
            sb.AppendLine("   if @DepServiceType <> 'FLIGHT' OR @DepServiceType<> @RetServiceType");
            sb.AppendLine("   Begin");
            sb.AppendLine("     Select @DepService = ServiceItem From CatPackPlanSer (NOLOCK)");
            sb.AppendLine("       Where StepNo = 1 ");
            sb.AppendLine("       And CatPackID = @CatPackId");
            sb.AppendLine("       Select @RetService = ServiceItem From CatPackPlanSer(NOLOCK)");
            sb.AppendLine("       Where StepNo = (Select Top 1 StepNo From CatPackPlanSer Where CatPackID = @CatPackId Order By StepNo Desc)");
            sb.AppendLine("	 And CatPackID = @CatPackId");
            sb.AppendLine("     Update #PriceList");
            sb.AppendLine("	 Set DepFlight = @DepService,");
            sb.AppendLine("	     RetFlight = @RetService");

            sb.AppendLine("      Where CatPackID = @CatPackId And CheckIn = @CheckIn And Night = @Night And DepServiceType = @DepServiceType And RetServiceType = @RetServiceType");
            sb.AppendLine("   End");
            sb.AppendLine("   Fetch Next FROM SnCursor INTO @CatPackId, @CheckIn, @Night, @DepServiceType, @RetServiceType");
            sb.AppendLine(" End");
            sb.AppendLine(" Close SnCursor");
            sb.AppendLine(" Deallocate SnCursor");
            if (string.Equals(UserData.CustomRegID, Common.crID_SunFun))
            {
                sb.AppendLine(" if(select count(*) from #PriceList where PackType<>'O')>0 ");
                sb.Append(" Delete From #PriceList where (isnull(DepSeat,0)<@totalSeat and DepServiceType='FLIGHT') or (isnull(RetSeat,0)<@totalSeat and RetServiceType='FLIGHT' ) ");
            }
            sb.Append("Delete From #PriceList \n");
            sb.Append("Where not Exists \n");
            sb.Append("    (Select * from (Select Max(CatPackID) as CatPackID, HolPack, Hotel, Room, Accom, Board, CheckIn, Night, StopSaleGuar, StopSaleStd, DepFlight From #PriceList \n");
            sb.Append("                    Group by Hotel, HolPack, Room, Accom, Board, CheckIn, Night, StopSaleGuar, StopSaleStd, DepFlight) as Newest \n");
            sb.Append("     Where Newest.CatPackID=#PriceList.CatPackID \n");
            sb.Append("           and Newest.Hotel=#PriceList.Hotel \n");
            sb.Append("           and Newest.HolPack=#PriceList.HolPack \n");
            sb.Append("           and Newest.Room=#PriceList.Room \n");
            sb.Append("           and Newest.Accom=#PriceList.Accom \n");
            sb.Append("           and Newest.Board=#PriceList.Board \n");
            sb.Append("           and Newest.CheckIn=#PriceList.CheckIn \n");
            sb.Append("           and Newest.Night=#PriceList.Night \n");
            sb.Append("           and Newest.StopSaleGuar=#PriceList.StopSaleGuar \n");
            sb.Append("           and Newest.StopSaleStd=#PriceList.StopSaleStd \n");
            sb.Append("           and Newest.DepFlight=#PriceList.DepFlight \n");
            sb.Append("    ) \n");

            sb.AppendLine("Delete From #PriceList ");
            sb.AppendLine("Where not dbo.ChdAgeControl4Age(Child, ChdAge1, ChdAge2, ChdAge3, ChdAge4, HChdAgeG1, HChdAgeG2, HChdAgeG3, HChdAgeG4, ChdG1Age1, ChdG1Age2, ChdG2Age1, ChdG2Age2, ChdG3Age1, ChdG3Age2, ChdG4Age1, ChdG4Age2) = 1 ");

            sb.Append("Delete From #PriceList \n");
            sb.Append("Where not Exists \n");
            sb.Append("	( Select *  \n");
            sb.Append("	  From (Select *  \n");
            sb.Append("			  From ( \n");
            sb.Append("					Select Hotel, CheckIn, Night, cnt=Count(Hotel) \n");
            sb.Append("					From ( \n");
            sb.Append("						Select pl.Hotel, pl.CheckIn, pl.Night, pl.RoomNr \n");
            sb.Append("						From #PriceList pl \n");
            sb.Append("						Group By pl.Hotel, pl.CheckIn, pl.Night, pl.RoomNr \n");
            sb.Append("					) X \n");
            sb.Append("					Group By Hotel, CheckIn, Night \n");
            sb.Append("			  ) Y \n");
            sb.Append("			  Where cnt >= (Select Count(RoomNr) From #tmpRoomTable) \n");
            sb.Append("	 ) as Newest \n");
            sb.Append("	Where Newest.Hotel = #PriceList.Hotel \n");
            sb.Append("	  And Newest.CheckIn = #PriceList.CheckIn \n");
            sb.Append("	  And Newest.Night= #PriceList.Night) \n");

            #region Only Hotel Flight Or Transport Type
            sb.Append("  Update #PriceList \n");
            sb.Append("  Set TransportType=Case when HP.Service='FLIGHT' Then 1 \n");
            sb.Append("                         when HP.Service='TRANSPORT' Then 2 Else 0 End \n");
            sb.Append("  From HolPackPlan HP (NOLOCK) \n");
            sb.Append("  Where HP.HolPack=#PriceList.HolPack and HP.StepNo=1 \n");

            sb.Append("  Update #PriceList \n");
            sb.Append("  Set TransportType=2,DepServiceType='TRANSPORT',RetServiceType='TRANSPORT' \n");
            sb.Append("  From HolPackPlan HP (NOLOCK) \n");
            sb.Append("  Where #PriceList.TransportType=0 And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack=#PriceList.HolPack And Service='TRANSPORT') Between 1 And 2 \n");
            sb.Append("   And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack=#PriceList.HolPack And Service='FLIGHT')=0 \n");
            #endregion
            //            sb.Append(@" update P set
            // DepSeat=CASE P.DepServiceType 
            //						WHEN 'FLIGHT'
            //						THEN dbo.ufn_Get_DepRet_FlightSeatWeb(P.Operator, P.Market,  '          ', P.CheckIn, P.Night, P.FlightClass,null, P.HolPack,  0,null)
            //						WHEN 'TRANSPORT'
            //						THEN dbo.ufn_CheckTransportAlllotForSearch(CheckIn,DepCity,ArrCity,Operator,Holpack)
            //						ELSE 0
            //						END,
            //						RetSeat=CASE P.RetServiceType
            //						WHEN 'FLIGHT'
            //						THEN
            //						CASE P.DepServiceType 
            //							WHEN 'FLIGHT' 
            //							THEN dbo.ufn_Get_DepRet_FlightSeatWeb(P.Operator, P.Market,  '          ', P.CheckIn, P.Night, P.FlightClass,null, P.HolPack,  1,null)
            //							WHEN 'TRANSPORT'
            //							THEN dbo.ufn_Get_DepRet_FlightSeatWeb(P.Operator, P.Market,  '          ', DATEADD(DAY,P.Night,P.CheckIn) , 0, P.FlightClass,null, P.HolPack,  0,null) 
            //							ELSE 0 
            //						END
            //						WHEN 'TRANSPORT'
            //						THEN dbo.ufn_CheckTransportAlllotForSearch(DATEADD(DAY, P.Night, P.CheckIn), ArrCity,DepCity,Operator,P.HolPack)
            //						ELSE 0
            //						END
            //						from #PriceList P ");
            string checkAllotStr = string.Empty;
            if (checkAvailableRoom && !filter.ExtAllotControl)
            {
                if (checkAllotStr.Length > 0)
                    checkAllotStr += ", ";
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050005129"))
                {
                    if (string.Equals(UserData.CustomRegID, Common.crID_Detur))
                        checkAllotStr += " FreeRoom=dbo.ufn_HotelFreeAmount(IsNull(NullIf(DepFlightMarket,Space(10)),Market), @Office, @Agency, Hotel, Room, Accom, -1, (CheckIn+(Night-isnull(HotNight,Night))), isnull(HotNight,Night), 11)";
                    else
                        checkAllotStr += " FreeRoom=dbo.ufn_HotelFreeAmount(Market, @Office, @Agency, Hotel, Room, Accom, -1, (CheckIn+(Night-isnull(HotNight,Night))), isnull(HotNight,Night), 11) ";
                }
                else
                {
                    checkAllotStr += " FreeRoom=dbo.ufn_HotelFreeAmount(Market, @Office, @Agency, Hotel, Room, Accom, -1, (CheckIn+(Night-isnull(HotNight,Night))), isnull(HotNight,Night), 1) ";
                }
            }

            List<dbInformationShema> spShema = VersionControl.getSpShema("ufn_CheckTransportAlllotForSearch", ref errorMsg);

            if (checkAvailableFlightSeat)
            {
                if (checkAllotStr.Length > 0)
                    checkAllotStr += ", ";
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 37, VersionControl.Equality.gt))
                {
                    //if (VersionControl.) {
                    //    checkAllotStr += @" DepSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(CheckIn,DepCity,ArrCity,Operator,Holpack)                                              
                    //                                 When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 0, null) 
                    //                                 Else null 
                    //                                 End
                    //                  ";
                    //} else {
                    if (spShema.Where(w => w.Name == "@HolPack").Count() > 0)
                        checkAllotStr += @" DepSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(CheckIn,DepCity,ArrCity,Operator,Holpack)                                              
                                                     --When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 0, null) 
                                                     When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, null, null, HolPack, 0, null) 
                                                     Else null 
                                                     End
                                      ";
                    else
                        checkAllotStr += @" DepSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(CheckIn,DepCity,ArrCity,Operator)                                              
                                                     --When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 0, null) 
                                                     When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, null, null, HolPack, 0, null) 
                                                     Else null 
                                                     End
                                      ";
                    // }
                }
                else
                    if (sType != SearchType.OnlyHotelSearch)
                {
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200003"))
                        //checkAllotStr += " DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 0, null) ";
                        checkAllotStr += " DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, null, null, HolPack, 0, null) ";
                    else
                        //checkAllotStr += " DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, CatPackID, PRecNo, DepFlight, CheckIn, Night, FlightClass, HolPack, DepCity, ArrCity, 0, null) ";
                        checkAllotStr += " DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, CatPackID, PRecNo, DepFlight, CheckIn, Night, null, HolPack, DepCity, ArrCity, 0, null) ";
                }
                if (checkAllotStr.Length > 0)
                    checkAllotStr += ", ";
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 37, VersionControl.Equality.gt))
                {
                    if (spShema.Where(w => w.Name == "@HolPack").Count() > 0)
                        checkAllotStr += @" RetSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(CheckIn+Night,ArrCity,DepCity,Operator,HolPack)
                                                     --When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 1, null)
                                                     When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, null, null, HolPack, 1, null)
                                                     Else null 
                                                     End                                            
                                      ";
                    else
                        checkAllotStr += @" RetSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(CheckIn+Night,ArrCity,DepCity,Operator)
                                                     --When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 1, null)
                                                     When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, null, null, HolPack, 1, null)
                                                     Else null 
                                                     End                                            
                                      ";
                }
                else
                    if (sType != SearchType.OnlyHotelSearch)
                {
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200003"))
                        //checkAllotStr += " RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 1, null) ";
                        checkAllotStr += " RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, null, null, HolPack, 1, null) ";
                    else
                        //checkAllotStr += " RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, CatPackID, PRecNo, DepFlight, CheckIn, Night, FlightClass, HolPack, DepCity, ArrCity, 1, null) ";
                        checkAllotStr += " RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, CatPackID, PRecNo, DepFlight, CheckIn, Night, null, HolPack, DepCity, ArrCity, 1, null) ";
                }
            }

            if (checkAllotStr.Length > 0)
            {
                sb.Append(" Update #PriceList ");
                sb.Append(" Set ");
                sb.Append(checkAllotStr);
            }

            sb.Append("Select P.*, \n");
            sb.Append(" OldSalePrice=CalcSalePrice \n");
            sb.Append("From #PriceList P \n");

            string whereStr = string.Empty;
            if (checkAvailableRoom && !filter.ExtAllotControl)
            {
                if (whereStr.Length > 0)
                    whereStr += " AND ";
                whereStr += " FreeRoom>0 ";
            }
            if (checkAvailableFlightSeat)
            {
                if (whereStr.Length > 0)
                    whereStr += " AND ";
                whereStr += " (DepSeat is null Or DepSeat>=@totalSeat) And (RetSeat is null Or RetSeat>=@totalSeat) ";
            }

            if ((filter.ShowStopSaleHotels && !filter.ExtAllotControl) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                if (whereStr.Length > 0)
                    whereStr += " AND ";
                if (checkAvailableRoom)
                    whereStr += " Not ((StopSaleStd=2 And StopSaleGuar=2) Or (FreeRoom<=0 And AutoStop=1))";
                else
                    whereStr += " Not (StopSaleStd=2 And StopSaleGuar=2)";
            }

            if (whereStr.Length > 0)
            {
                sb.Append(" Where ");
                sb.Append(whereStr);
            }
            sb.Append(" Order by P.CalcSalePrice ");

            List<SearchResult> result = new List<SearchResult>();
            #region Age Group
            List<AgeGroup> ageGroups = new List<AgeGroup>();
            int roomCnt = 0;
            int refNo = 0;
            foreach (SearchCriteriaRooms row in filter.RoomsInfo)
            {
                roomCnt++;
                refNo++;
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Adult, GroupNo = roomCnt, RefNo = refNo, Unit = row.Adult, Age = null, DateOfBirth = null });
                if (row.Chd1Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Infant, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd1Age.Value, DateOfBirth = null });
                if (row.Chd2Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd2Age.Value, DateOfBirth = null });
                if (row.Chd3Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd3Age.Value, DateOfBirth = null });
                if (row.Chd4Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd4Age.Value, DateOfBirth = null });
            }

            #endregion
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sb.ToString());
            dbCommand.CommandTimeout = 120;
            try
            {
                int i = 0;
                db.AddInParameter(dbCommand, "pAgency", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "pOperator", DbType.AnsiString, UserData.Operator);
                db.AddInParameter(dbCommand, "pOffice", DbType.AnsiString, UserData.OprOffice);
                db.AddInParameter(dbCommand, "pMarket", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "pCIn1", DbType.Date, filter.CheckIn.Date);
                if (filter.ExpandDate > 0)
                    db.AddInParameter(dbCommand, "pCIn2", DbType.Date, filter.CheckIn.Date.AddDays(filter.ExpandDate));
                db.AddInParameter(dbCommand, "pNight1", DbType.Int16, filter.NightFrom);
                if (filter.NightFrom < filter.NightTo)
                    db.AddInParameter(dbCommand, "pNight2", DbType.Int16, filter.NightTo);
                Stopwatch sw = new Stopwatch();
                sw.Start();
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        ++i;
                        SearchResult r = new SearchResult();
                        r.RefNo = i;
                        r.RoomNr = (int)oReader["RoomNr"];
                        r.Calculated = false;
                        r.CatPackID = Conversion.getInt32OrNull(oReader["CatPackID"]);
                        r.ARecNo = Conversion.getInt32OrNull(oReader["ARecNo"]);
                        r.PRecNo = Conversion.getInt32OrNull(oReader["PRecNo"]);
                        r.HAPRecId = Conversion.getInt32OrNull(oReader["HAPRecID"]);
                        r.Market = Conversion.getStrOrNull(oReader["Market"]);
                        r.HolPack = Conversion.getStrOrNull(oReader["HolPack"]);
                        r.CalcSalePrice = Conversion.getDecimalOrNull(oReader["CalcSalePrice"]);
                        r.OldSalePrice = Conversion.getDecimalOrNull(oReader["OldSalePrice"]);
                        r.SaleChdG1 = Conversion.getDecimalOrNull(oReader["SaleChdG1"]);
                        r.SaleChdG2 = Conversion.getDecimalOrNull(oReader["SaleChdG2"]);
                        r.SaleChdG3 = Conversion.getDecimalOrNull(oReader["SaleChdG3"]);
                        r.SaleChdG4 = Conversion.getDecimalOrNull(oReader["SaleChdG4"]);
                        r.HotChdPrice = Conversion.getDecimalOrNull(oReader["HotChdPrice"]);
                        r.Hotel = Conversion.getStrOrNull(oReader["Hotel"]);
                        r.HotelName = Conversion.getStrOrNull(oReader["HotelName"]);
                        r.HotelNameL = Conversion.getStrOrNull(oReader["HotelNameL"]);
                        r.HotelLocation = Conversion.getInt32OrNull(oReader["HotLocation"]);
                        r.HotLocationName = Conversion.getStrOrNull(oReader["HotLocationName"]);
                        r.HotLocationNameL = Conversion.getStrOrNull(oReader["HotLocationNameL"]);
                        r.HotCat = Conversion.getStrOrNull(oReader["HotCat"]);
                        r.Room = Conversion.getStrOrNull(oReader["Room"]);
                        r.RoomName = Conversion.getStrOrNull(oReader["RoomName"]);
                        r.RoomNameL = Conversion.getStrOrNull(oReader["RoomNameL"]);
                        r.Board = Conversion.getStrOrNull(oReader["Board"]);
                        r.BoardName = Conversion.getStrOrNull(oReader["BoardName"]);
                        r.BoardNameL = Conversion.getStrOrNull(oReader["BoardNameL"]);
                        r.Accom = Conversion.getStrOrNull(oReader["Accom"]);
                        r.AccomName = Conversion.getStrOrNull(oReader["AccomName"]);
                        r.AccomNameL = Conversion.getStrOrNull(oReader["AccomNameL"]);
                        r.Night = Conversion.getInt16OrNull(oReader["Night"]);
                        r.HotNight = Conversion.getInt16OrNull(oReader["HotNight"]);
                        r.CheckIn = Conversion.getDateTimeOrNull(oReader["CheckIn"]);
                        r.CheckOut = r.CheckIn.Value.AddDays(r.Night.Value);
                        r.SaleCur = Conversion.getStrOrNull(oReader["SaleCur"]);
                        r.PLCur = Conversion.getStrOrNull(oReader["PLCur"]);
                        r.CurrentCur = filter.CurControl ? filter.CurrentCur : string.Empty;
                        r.DepCity = Conversion.getInt32OrNull(oReader["DepCity"]);
                        r.DepCityName = Conversion.getStrOrNull(oReader["DepCityName"]);
                        r.DepCityNameL = Conversion.getStrOrNull(oReader["DepCityNameL"]);
                        r.ArrCity = Conversion.getInt32OrNull(oReader["ArrCity"]);
                        r.ArrCityName = Conversion.getStrOrNull(oReader["ArrCityName"]);
                        r.ArrCityNameL = Conversion.getStrOrNull(oReader["ArrCityNameL"]);
                        r.DepFlight = Conversion.getStrOrNull(oReader["DepFlight"]);

                        r.RetFlight = Conversion.getStrOrNull(oReader["RetFlight"]);
                        r.DEPFlightTime = Conversion.getDateTimeOrNull(oReader["DEPFlightTime"]);
                        r.RETFlightTime = Conversion.getDateTimeOrNull(oReader["RETFlightTime"]);
                        if (checkAvailableRoom && !filter.ExtAllotControl)
                            r.FreeRoom = Conversion.getInt32OrNull(oReader["FreeRoom"]);
                        #region OnlyHotelHolPaCat Transport Sead Problem
                        if (checkAvailableFlightSeat && sType != SearchType.OnlyHotelSearch)
                            if (checkAvailableRoom && !filter.ExtAllotControl)
                            {
                                r.DepSeat = Conversion.getInt32OrNull(oReader["DepSeat"]);
                                r.RetSeat = Conversion.getInt32OrNull(oReader["RetSeat"]);
                            }
                        #endregion
                        //r.DepSeat = Conversion.getInt32OrNull(oReader["DepSeat"]);
                        //r.RetSeat = Conversion.getInt32OrNull(oReader["RetSeat"]);

                        r.StopSaleGuar = Conversion.getInt16OrNull(oReader["StopSaleGuar"]);
                        r.StopSaleStd = Conversion.getInt16OrNull(oReader["StopSaleStd"]);
                        r.AutoStop = Conversion.getBoolOrNull(oReader["AutoStop"]);
                        r.DFlight = Conversion.getStrOrNull(oReader["DFlight"]);
                        r.DepServiceType = Conversion.getStrOrNull(oReader["DepServiceType"]);
                        r.RetServiceType = Conversion.getStrOrNull(oReader["RetServiceType"]);
                        r.TransportType = Conversion.getInt16OrNull(oReader["TransportType"]);
                        r.HotelRecID = Conversion.getInt32OrNull(oReader["HotelRecID"]);
                        r.AgeGroupList = ageGroups;
                        r.LogID = logID;
                        if (string.Equals(UserData.CustomRegID, Common.crID_UpJet) || string.Equals(UserData.CustomRegID, Common.crID_CelexTravel))
                            r.OrderHotel = Conversion.getInt32OrNull(oReader["DispNo"]);
                        else
                            r.OrderHotel = (int)999;
                        result.Add(r);
                    }
                }
                sw.Stop();
                try
                {
                    PriceSearchRequest preq = new PriceSearchRequest();
                    preq.Adult = filter.RoomsInfo.Sum(s => s.Adult);
                    preq.ArrCity = filter.ArrCity;
                    preq.Board = filter.Board;
                    preq.CatName = filter.Category;
                    preq.CheckInFrom = filter.CheckIn;
                    preq.Child = filter.RoomsInfo.Sum(s => s.Child);
                    preq.Country = filter.Country;
                    preq.Currency = filter.CurrentCur;
                    preq.DepCity = filter.DepCity;
                    preq.Direction = !string.IsNullOrEmpty(filter.Direction) ? Convert.ToInt32(filter.Direction) : 0;
                    preq.FlightClass = filter.SClass;
                    preq.HolPack = filter.Package;
                    preq.HotelCode = filter.Hotel;
                    preq.IsB2CAgency = false;
                    preq.NightFrom = filter.NightFrom;
                    preq.NightTo = filter.NightTo;
                    preq.Room = filter.Room;
                    preq.UserData = UserData;
                    preq.PriceSearchTime = (int)sw.ElapsedMilliseconds;
                    TourVisio.WebService.Adapter.Enums.enmProductType prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Hotel;
                    switch (filter.SType)
                    {
                        case SearchType.CruiseSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Cruise; break;
                        case SearchType.DynamicFlightPackageSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Dynamic; break;
                        case SearchType.HotelSale: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Hotel; break;
                        case SearchType.OnlyExcursion: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Excursion; break;
                        case SearchType.OnlyFlightSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Flight; break;
                        case SearchType.OnlyHotelSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Hotel; break;
                        case SearchType.OnlyTransfer: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Transfer; break;
                        case SearchType.PackageSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.HolidayPackage; break;
                        case SearchType.PackPriceSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.HolidayPackage; break;
                        case SearchType.TourPackageSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Tour; break;
                    }
                    preq.ProductType = prodType;
                    Task.Factory.StartNew(() => SanBI.TrySend(preq, result.Count));
                }
                catch
                {

                }
                return result;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return result;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> getEmptySearchT(User UserData, SearchCriteria filter, bool checkAvailableRoom, SearchType sType, bool checkAvailableFlightSeat, Guid? logID, ref string errorMsg)
        {
            string resortWhereStr = string.Empty;
            string categoryWhereStr = string.Empty;
            string hotelWhereStr = string.Empty;
            string roomWhereStr = string.Empty;
            string boardWhereStr = string.Empty;
            List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);
            bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
            if (!string.IsNullOrEmpty(filter.Resort))
            {
                if (filter.Resort.Split('|').Length > 0)
                {
                    if (filter.Resort.Split('|').Length == 1)
                    {
                        resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", filter.Resort.Split('|')[0]);
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                    else
                    {
                        foreach (string r in filter.Resort.Split('|'))
                        {
                            Location _location = locations.Find(f => f.RecID == Conversion.getInt32OrNull(r));
                            if (_location != null)
                            {
                                if (_location.Type == 3)
                                {
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                                    foreach (Location rL in locations.Where(w => w.Parent == _location.RecID))
                                        resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", rL.RecID);
                                }
                                else
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                            }
                        }
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                }
                else
                    resortWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Category))
            {
                foreach (string r in filter.Category.Split('|'))
                    categoryWhereStr += string.Format("INSERT INTO @Category(Code) VALUES('{0}') \n", r);
                if (categoryWhereStr.Length > 0)
                    categoryWhereStr = "DECLARE @Category TABLE (Code VarChar(10)) \n" + categoryWhereStr;
                else
                    categoryWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Hotel))
            {
                foreach (string r in filter.Hotel.Split('|'))
                    hotelWhereStr += string.Format("INSERT INTO @Hotel(Code) VALUES('{0}') \n", r);
                if (hotelWhereStr.Length > 0)
                    hotelWhereStr = "DECLARE @Hotel TABLE (Code VarChar(10)) \n" + hotelWhereStr;
                else
                    hotelWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Room))
            {
                foreach (string r in filter.Room.Split('|'))
                    roomWhereStr += string.Format("INSERT INTO @Room(Code) VALUES('{0}') \n", r);
                if (roomWhereStr.Length > 0)
                    roomWhereStr = "DECLARE @Room TABLE (Code VarChar(10)) \n" + roomWhereStr;
                else
                    roomWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Board))
            {
                foreach (string r in filter.Board.Split('|'))
                    boardWhereStr += string.Format("INSERT INTO #tmpBoard(Code) VALUES('{0}') \n", r);
                if (boardWhereStr.Length > 0)
                    boardWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpBoard') is not null Drop Table dbo.#tmpBoard
Create Table #tmpBoard (Code VarChar(10) COLLATE Latin1_General_CI_AS) " + boardWhereStr;
                else
                    boardWhereStr = string.Empty;
            }
            string xmlAgeTable = string.Empty;
            if (filter.RoomCount > 0)
            {
                xmlAgeTable += @"
if OBJECT_ID('TempDB.dbo.#tmpRoomTable') is not null Drop Table dbo.#tmpRoomTable 
Create Table #tmpRoomTable (RoomNr int, Adult int, Child int, ChdAge1 int, ChdAge2 int, ChdAge3 int, ChdAge4 int) ";
                foreach (SearchCriteriaRooms row in filter.RoomsInfo)
                {
                    //SearchCriteriaRooms row = filter.RoomsInfo.FirstOrDefault();
                    xmlAgeTable += string.Format("Insert Into #tmpRoomTable (RoomNr, Adult, Child, ChdAge1, ChdAge2, ChdAge3, ChdAge4) Values ({0}, {1}, {2}, {3}, {4}, {5}, {6}) \n",
                        row.RoomNr,
                        row.Adult,
                        row.Child,
                        row.Chd1Age.HasValue ? row.Chd1Age.Value.ToString() : "-1",
                        row.Chd2Age.HasValue ? row.Chd2Age.Value.ToString() : "-1",
                        row.Chd3Age.HasValue ? row.Chd3Age.Value.ToString() : "-1",
                        row.Chd4Age.HasValue ? row.Chd4Age.Value.ToString() : "-1");
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("SET DATEFIRST 1 \n");
            sb.Append(xmlAgeTable);
            sb.Append("Declare @totalSeat int \n");
            sb.Append("Select @totalSeat=(SUM(Adult)+Sum(Child)-Sum((Case When ChdAge1 < 2 and ChdAge1 > -1 then 1 else 0 end)+ \n");
            sb.Append("				                                (Case When ChdAge2 < 2 and ChdAge2 > -1 then 1 else 0 end)+ \n");
            sb.Append("				                                (Case When ChdAge3 < 2 and ChdAge3 > -1 then 1 else 0 end)+ \n");
            sb.Append("				                                (Case When ChdAge4 < 2 and ChdAge4 > -1 then 1 else 0 end))) \n");
            sb.Append("From #tmpRoomTable \n");

            sb.Append("if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#AgencyCP') is not null Drop Table dbo.#AgencyCP \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#PriceList') is not null Drop Table dbo.#PriceList \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location \n");

            sb.Append("Select RecID, Name=isnull(dbo.FindLocalName(NameLID, @Market), Name), Village, Town, City, Country \n");
            sb.Append("Into #Location \n");
            sb.Append("From Location (NOLOCK) \n");

            sb.Append("Select *  \n");
            sb.Append("Into #CatalogPack \n");
            sb.Append("From CatalogPack (NOLOCK) CP \n");
            sb.Append("Where	CP.Ready = 'Y'  \n");
            sb.Append("	And CP.EndDate > GetDate()-1  \n");
            sb.Append("	And CP.WebPub = 'Y'  \n");
            sb.Append("	And CP.WebPubDate < GetDate() \n");
            sb.Append("	And dbo.DateOnly(GETDATE()) BetWeen CP.SaleBegDate And CP.SaleEndDate \n");

            // 15-August-2011 Holiday package category control
            if (filter.HolPackCat.HasValue)
                sb.AppendFormat(" And CP.Category = {0} \n", filter.HolPackCat.Value);
            else
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                sb.Append(" And CP.Category is null \n");

            // 12-August-2011 new Pricelist allotment control
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030300001"))
                sb.Append("	And (isnull(CP.Allotment,32000)-isnull(CP.Used,0))>0  \n");
            if (filter.SType != SearchType.OnlyHotelSearch)
            {
                if (filter.DepCity.HasValue)
                    sb.AppendFormat(" And CP.DepCity = {0} \n", filter.DepCity.ToString());
            }
            if (filter.ArrCity.HasValue)
                sb.AppendFormat(" And CP.ArrCity = {0} \n", filter.ArrCity.ToString());
            switch (filter.SType)
            {
                case SearchType.PackageSearch:
                    sb.Append("	And CP.PackType = 'H'  \n");
                    break;
                case SearchType.TourPackageSearch:
                    sb.Append("	And CP.PackType = 'T'  \n");
                    break;
                case SearchType.OnlyHotelSearch:
                    sb.Append("	And CP.PackType = 'O'  \n");
                    break;
                default:
                    sb.Append("	And CP.PackType = 'none'  \n");
                    break;
            }
            sb.Append("Select Distinct RecID  \n");
            sb.Append("Into #AgencyCP \n");
            sb.Append("From  \n");
            sb.Append("( \n");
            sb.Append("	Select CP.RecID \n");
            sb.Append("	From #CatalogPack CP (NOLOCK)  \n");
            sb.Append("	Where   \n");
            sb.Append("	 Not Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID) or \n");
            sb.Append("  Exists(Select CA.RecID from CatPackAgency CA (NOLOCK) \n");
            sb.Append("	        Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups=CA.AgencyGrp and AG.Agency=@Agency --And AG.GrpType='P' \n");
            sb.Append("	        Where CA.CatPackID=CP.RecID and (AG.Agency=@Agency or CA.Agency=@Agency)) \n");
            //sb.Append("	Union All  \n");
            //sb.Append("	Select CP.RecID  \n");
            //sb.Append("	From #CatalogPack CP (NOLOCK)  \n");
            //sb.Append("	Where  \n");
            //sb.Append("		Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=@Agency) \n");
            sb.Append(") CP \n");
            sb.Append(" \n");
            sb.Append("Select \n");
            //if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            //    sb.Append(" TOP 2000 \n");
            sb.Append("	RT.RoomNr,PA.CatPackID,ARecNo=PA.RecNo,PRecNo=PP.RecNo,HAPRecID=HAP.RecID,PA.Hotel,PA.Room,PA.Board,PA.Accom, PP.Night, PP.HotNight, PP.CheckIn, \n");
            sb.Append(" CP.Operator,CP.FlightClass,CP.HolPack,PP.NetCur,CP.SaleCur,PLCur=CP.SaleCur,CP.DepCity,CP.ArrCity,flySeat=(HAP.Adult+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4), \n");
            sb.Append(" HAdult=HAP.Adult, HChdAgeG1=HAP.ChdAgeG1, HChdAgeG2=HAP.ChdAgeG2, HChdAgeG3=HAP.ChdAgeG3, HChdAgeG4=HAP.ChdAgeG4, \n");
            sb.Append(" PP.ChdG1Age1, PP.ChdG1Age2, PP.ChdG2Age1, PP.ChdG2Age2, PP.ChdG3Age1, PP.ChdG3Age2, PP.ChdG4Age1, PP.ChdG4Age2, \n");
            sb.Append(" DepCityName=(Select Top 1 Name From #Location WHERE RecID=CP.DepCity), \n");
            sb.Append(" ArrCityName=(Select Top 1 Name From #Location WHERE RecID=CP.ArrCity), \n");
            sb.Append(" CP.SaleEndDate, \n");
            sb.Append("	RoomName=(Select HR.Name From Room (NOLOCK) Where Code=PA.Room), \n");
            sb.Append("	RoomNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), HR.Name) From Room (NOLOCK) Where Code=PA.Room), \n");
            sb.Append("	AccomName=(Select HA.Name From RoomAccom (NOLOCK) Where Code=PA.Accom), \n");
            sb.Append("	AccomNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), HA.Name) From RoomAccom (NOLOCK) Where Code=PA.Accom), \n");
            sb.Append("	BoardName=(Select HB.Name From Board (NOLOCK) Where Code=PA.Board), \n");
            sb.Append("	BoardNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), HB.Name) From Board (NOLOCK) Where Code=PA.Board), \n");
            sb.Append("	HotelName=H.Name, HotelNameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name), HotLocation=H.Location, HotCat=H.Category, \n");
            sb.Append("	HotLocationName=(Select Top 1 Name From #Location Where RecID=H.Location), \n");
            sb.Append("	OldSalePrice=Cast(0 As decimal), \n");
            sb.Append("	LastPrice=Cast(0 As decimal), \n");
            sb.Append("	AccomFullName= \n");
            sb.Append("	  case when HAP.Adult>0 then Str(HAP.Adult,1)+' '+Cast(IsNull(PRM.PLAdlCap,'Adl') as varchar(100)) else '' end+ \n");
            sb.Append("	  case when HAP.ChdAgeG1>0 and PP.ChdG1Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG1)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG1Age1)+'-'+Convert(VarChar(5),PP.ChdG1Age2)+')' else '' end+ \n");
            sb.Append("	  case when HAP.ChdAgeG2>0 and PP.ChdG2Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG2)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG2Age1)+'-'+Convert(VarChar(5),PP.ChdG2Age2)+')' else '' end+ \n");
            sb.Append("	  case when HAP.ChdAgeG3>0 and PP.ChdG3Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG3)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG3Age1)+'-'+Convert(VarChar(5),PP.ChdG3Age2)+')' else '' end+ \n");
            sb.Append("	  case when HAP.ChdAgeG4>0 and PP.ChdG4Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG4)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG4Age1)+'-'+Convert(VarChar(5),PP.ChdG4Age2)+')' else '' end, \n");
            sb.Append("	CalcSalePrice=PP.SalePrice+ \n");
            sb.Append("		 Case when HAP.Adult-PA.Adl>=1 then PP.SaleExtBed1 else 0 end+ \n");
            sb.Append("		 Case when HAP.Adult-PA.Adl>=2 then PP.SaleExtBed2 else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG1>0 then (PP.SaleChdG1*HAP.ChdAgeG1)+CHDP.ChdG1Price else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG2>0 then (PP.SaleChdG2*HAP.ChdAgeG2)+CHDP.ChdG2Price else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG3>0 then (PP.SaleChdG3*HAP.ChdAgeG3)+CHDP.ChdG3Price else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG4>0 then (PP.SaleChdG4*HAP.ChdAgeG4)+CHDP.ChdG4Price else 0 end, \n");
            sb.Append("	SaleChdG1= Case when HAP.ChdAgeG1>0 then PP.SaleChdG1+CHDP.ChdG1Price/dbo.IfZero(HAP.ChdAgeG1,1) else 0 end, \n");
            sb.Append("	SaleChdG2= Case when HAP.ChdAgeG2>0 then PP.SaleChdG2+CHDP.ChdG2Price/dbo.IfZero(HAP.ChdAgeG2,1) else 0 end, \n");
            sb.Append("	SaleChdG3= Case when HAP.ChdAgeG3>0 then PP.SaleChdG3+CHDP.ChdG3Price/dbo.IfZero(HAP.ChdAgeG3,1) else 0 end, \n");
            sb.Append("	SaleChdG4= Case when HAP.ChdAgeG4>0 then PP.SaleChdG4+CHDP.ChdG4Price/dbo.IfZero(HAP.ChdAgeG4,1) else 0 end, \n");
            sb.Append("	HotChdPrice= Case when HAP.ChdAgeG1>0 then CHDP.ChdG1Price else 0 end+ \n");
            sb.Append("				 Case when HAP.ChdAgeG2>0 then CHDP.ChdG2Price else 0 end+ \n");
            sb.Append("				 Case when HAP.ChdAgeG3>0 then CHDP.ChdG3Price else 0 end+ \n");
            sb.Append("				 Case when HAP.ChdAgeG4>0 then CHDP.ChdG4Price else 0 end, \n");
            sb.Append("	Pax=HAP.Adult+ \n");
            sb.Append("	    Case When TP.ChdG1Age2<PP.ChdG1Age2 then HAP.ChdAgeG1 else 0 end+ \n");
            sb.Append("	    Case When TP.ChdG2Age2<PP.ChdG2Age2 then HAP.ChdAgeG2 else 0 end+ \n");
            sb.Append("	    Case When TP.ChdG3Age2<PP.ChdG3Age2 then HAP.ChdAgeG3 else 0 end+ \n");
            sb.Append("	    Case When TP.ChdG3Age2<PP.ChdG4Age2 then HAP.ChdAgeG4 else 0 end, \n");
            sb.Append("	CP.Market, DepSeat=null, RetSeat=Null, HotelRecID=H.RecID, \n");
            sb.Append(" StopSaleGuar=dbo.ufn_StopSaleCnt(PA.Hotel, PA.Room, PA.Accom, PA.Board, @Market, @Agency, PA.CatPackID, H.Location, -2, (PP.CheckIn+(PP.Night-isnull(PP.HotNight,PP.Night))), PP.CheckIn+PP.Night, 0, 1), \n");
            sb.Append(" StopSaleStd=dbo.ufn_StopSaleCnt(PA.Hotel, PA.Room, PA.Accom, PA.Board, @Market, @Agency, PA.CatPackID, H.Location, 2, (PP.CheckIn+(PP.Night-isnull(PP.HotNight,PP.Night))), PP.CheckIn+PP.Night, 0, 1), \n");
            sb.Append(" PasEBPer=Case When CP.PasEBValid='Y' then \n");
            sb.Append("             (Select Top 1 PasEBPer \n");
            sb.Append("              From PasEB EB (NOLOCK) \n");
            sb.Append("              Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID \n");
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0034, VersionControl.Equality.gt))
                sb.Append("              Left Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=PA.Hotel And (EBH.Room='' or EBH.Room=PA.Room) And (EBH.Board='' or EBH.Board=PA.Board) \n");
            else
                sb.Append("              Left Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=PA.Hotel \n");
            sb.Append("              Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P' \n");
            sb.Append("              Where OwnerOperator=CP.Operator And OwnerMarket=CP.Market And Operator=@Operator \n");
            sb.Append("                And Market=@Market And CP.PasEBValid='Y' \n");
            sb.Append("                And (not Exists(Select null from PasEBHot Where PasEBID=EB.RecID) or \n");
            sb.Append("                    (Exists(Select null from PasEBHot Where PasEBHot.PasEBID=EB.RecID and PasEBHot.Hotel=PA.Hotel))) \n");
            sb.Append("                And ((EB.HolPack=CP.HolPack or PG.HolPack=CP.HolPack) or (ISNULL(EB.HolPack,'')='')) \n");
            sb.Append("                And dbo.DateOnly(GetDate()) between SaleBegDate and SaleEndDate and PP.CheckIn between ResBegDate and ResEndDate \n");
            sb.Append("                And (PP.Night between IsNull(MinDay,0) and IsNull(MaxDay,999)) \n");
            sb.Append("                And SubString(DoW,DATEPART(dw,PP.CheckIn),1)=1 \n");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040014038"))
                sb.Append("                And DateDIFF(day,dbo.DateOnly(GetDate()),PP.CheckIn) between IsNull(EBP.RemFromDay,0) and IsNull(EBP.RemToDay,999)  \n");

            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0034, VersionControl.Equality.gt))
                sb.Append("              Order by EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc,EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc,SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc) \n");
            else
                sb.Append("              Order by EBH.Hotel Desc, EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc,SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc) \n");
            sb.Append("          Else 0 End, \n");
            sb.Append(" FreeRoom=dbo.ufn_TourFreeAmount(CP.Holpack, PP.CheckIn, dbo.DateOnly(GetDate())), \n");
            sb.Append(" EBAmount=Case When CP.PasEBValid='Y' then \n");
            sb.Append("             dbo.ufn_GetEBAmountPL(CP.RecID,PA.RecNo,PP.RecNo,HAP.RecID,PA.Adl,PP.ChdG1Age1,PP.ChdG1Age2,PP.ChdG2Age1,PP.ChdG2Age2,PP.ChdG3Age1,PP.ChdG3Age2,0,0,PP.HotelChdRecNo,H.ChdCalcSale,HAP.Adult,HAP.ChdAgeG1,HAP.ChdAgeG2,HAP.ChdAgeG3,0,0,@CustomRegID,(Case when HAP.ChdAgeG1>0 then CHDP.ChdG1Price else 0 end+Case when HAP.ChdAgeG2>0 then CHDP.ChdG2Price else 0 end+Case when HAP.ChdAgeG3>0 then CHDP.ChdG3Price else 0 end+Case when HAP.ChdAgeG4>0 then CHDP.ChdG4Price else 0 end)) \n");
            sb.Append("          else 0 end, \n");
            sb.Append(" Description=CP.Description,OprText=(Case When isnull(Hp.OprText,'')='' then O.NameS else Hp.OprText end),  \n");
            sb.Append(" CPName=CP.Name,CPNameL=isnull(dbo.FindLocalName(CP.NameLID,@Market),CP.Name), \n");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040018050"))
                sb.Append("    AutoStop=isnull(HR.AutoStop,0), \n");
            else
                sb.Append("    AutoStop=Cast(0 As bit), \n");
            sb.Append(" TransportType=Cast(0 As smallint), \n");
            sb.Append(" DepServiceType=isnull(CP.DepServiceType,''),RetServiceType=isnull(CP.RetServiceType,''), \n");
            if (string.Equals(UserData.CustomRegID, Common.crID_FilipTravel))
            {
                sb.Append(" DepFlight=isnull(FDD.FlightNo,'          '), RetFlight=isnull(FDD.NextFlight,'          '), \n");
                sb.Append(" DFlight=FDD.FlightNo, \n");
                sb.Append(" DepFlightTime=isnull(FDD.DepTime, Cast(0 AS DateTime)),RetFlightTime=isnull(FDR.DepTime, Cast(0 AS DateTime)) \n");
            }
            else
            {
                sb.Append(" DepFlight='          ', RetFlight='          ', \n");
                sb.Append(" DFlight='', \n");
                sb.Append(" DepFlightTime=Cast(0 AS DateTime), RetFlightTime=Cast(0 AS DateTime) \n");
            }
            sb.Append("Into #PriceList \n");
            sb.Append("From CatPriceA PA (NOLOCK) \n");
            sb.Append("Join CatPriceP PP (NOLOCK) on PP.CatPackID=PA.CatPackID and PP.PriceAID=PA.RecNo \n");
            sb.Append("Join #CatalogPack CP (NOLOCK) on CP.RecID=PA.CatPAckID and CP.PackType='T' \n");
            sb.Append("join Holpack Hp (NOLOCK) on Hp.Code=CP.HolPack \n");
            sb.Append("join Hotel H  (NOLOCK) on H.Code=PA.Hotel \n");
            sb.Append("join HotelAccomPax HAP  (NOLOCK) on HAP.Hotel=PA.Hotel and HAP.Room=PA.Room and HAP.Accom=PA.Accom and IsNull(HAP.VisiblePL,'Y')='Y' \n");
            sb.Append("Left Join CatTourChd CHD (NOLOCK) on CHD.CatPackID=PP.CatPackID and CHD.RecNo=PP.HotelChdRecNo \n");
            sb.Append("Join CatPackTourPrice TP (NOLOCK) on TP.CatPackID=CP.RecID and TP.HolPack=CP.HolPack and TP.TourDate=PP.CheckIn \n");
            sb.Append("Left Join ParamPrice PRM (NOLOCK) on PRM.Market=CP.Market \n");
            sb.Append("Join Operator O (NOLOCK) ON O.Code = CP.Operator \n");

            if (string.Equals(UserData.CustomRegID, Common.crID_FilipTravel))
            {
                sb.Append("OUTER APPLY (Select HolPack,StepNo From HolPackPlan (NOLOCK) Where HolPack=CP.HolPack And [Service]=isnull(CP.DepServiceType,'FLIGHT') And Departure=isnull(CP.DepDepCity,CP.DepCity) and Arrival=isnull(CP.DepArrCity,CP.ArrCity)) HPP \n");
                sb.Append("OUTER APPLY (Select ServiceItem From HolPackSer (NOLOCK) Where  HolPack=HPP.HolPack And StepNo=HPP.StepNo) HS \n");
                sb.Append("OUTER APPLY (Select FlightNo, NextFlight,DepTime=isnull(TDepDate,FlyDate)+dbo.TimeOnly(isnull(TDepTime,DepTime)) From FlightDay (NOLOCK) Where FlightNo=HS.ServiceItem And FlyDate=PP.CheckIn) FDD \n");
                sb.Append("OUTER APPLY (Select FlightNo, NextFlight,DepTime=isnull(TDepDate,FlyDate)+dbo.TimeOnly(isnull(TDepTime,DepTime)) From FlightDay (NOLOCK) Where FlightNo=FDD.NextFlight and FlyDate=(PP.CheckIn+PP.Night)) FDR \n");
            }

            sb.Append("OUTER APPLY \n");
            sb.Append("( \n");
            sb.Append("Select * \n");
            sb.Append("From dbo.HotChdCalc(IsNull(CHD.ChdG1PriceN1,0),IsNull(CHD.ChdG1PriceN2,0),IsNull(CHD.ChdG1PriceN3,0),IsNull(CHD.ChdG1PriceN4,0), \n");
            sb.Append("                    IsNull(CHD.ChdG2PriceN1,0),IsNull(CHD.ChdG2PriceN2,0),IsNull(CHD.ChdG2PriceN3,0),IsNull(CHD.ChdG2PriceN4,0), \n");
            sb.Append("                    IsNull(CHD.ChdG3PriceN1,0),IsNull(CHD.ChdG3PriceN2,0),IsNull(CHD.ChdG3PriceN3,0),IsNull(CHD.ChdG3PriceN4,0), \n");
            sb.Append("                    IsNull(CHD.ChdG4PriceN1,0),IsNull(CHD.ChdG4PriceN2,0),IsNull(CHD.ChdG4PriceN3,0),IsNull(CHD.ChdG4PriceN4,0), \n");
            sb.Append("                    HAP.ChdAgeG1,HAP.ChdAgeG2,HAP.ChdAgeG3,HAP.ChdAgeG4,H.ChdCalcSale,IsNull(CHD.ChdG1Out,'N'), \n");
            sb.Append("                    PP.ChdG1Age1,PP.ChdG1Age2,PP.ChdG2Age1,PP.ChdG2Age2,PP.ChdG3Age1,PP.ChdG3Age2,PP.ChdG4Age1,PP.ChdG4Age2, \n");
            sb.Append("                    PA.CatPackID,0,0,0,0) \n");
            sb.Append(") CHDP \n");

            sb.Append("Join #tmpRoomTable RT on RT.Adult=HAP.Adult And RT.Child=(HAP.ChdAgeG1+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4) \n");

            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0008, VersionControl.Equality.gt))
                sb.Append(@"join HotelRoom HR (NOLOCK) on HR.Hotel=PA.Hotel and HR.Code=PA.Room and IsNull((Select VisiblePL From HotelRoomMarOpt Where RoomID=HR.RecID and Market=CP.Market),IsNull(HR.VisiblePL,'Y'))='Y'
                            join HotelAccom HA (NOLOCK) on HA.Hotel=PA.Hotel and HA.Room=PA.Room and HA.Code=PA.Accom and IsNull((Select VisiblePL From HotelAccomMarOpt Where AccomID=HA.RecID and Market=CP.Market), IsNull(HA.VisiblePL,'Y'))='Y'
                            join HotelBoard HB (NOLOCK) on HB.Hotel=PA.Hotel and HB.Code=PA.Board and IsNull((Select VisiblePL From HotelBoardMarOpt Where BoardID=HB.RecID and Market=CP.Market),IsNull(HB.VisiblePL,'Y'))='Y'  ");
            else
                sb.Append(@"join HotelRoom HR (NOLOCK) on HR.Hotel=PA.Hotel and HR.Code=PA.Room and IsNull(HR.VisiblePL,'Y')='Y' 
                            join HotelAccom HA (NOLOCK) on HA.Hotel=PA.Hotel and HA.Room=PA.Room and HA.Code=PA.Accom and IsNull(HA.VisiblePL,'Y')='Y' 
                            join HotelBoard HB (NOLOCK) on HB.Hotel=PA.Hotel and HB.Code=PA.Board and IsNull(HB.VisiblePL,'Y')='Y' ");


            sb.Append("Where Exists(Select RecID From CatPackMarket Where CatPackID=PA.CatPackID and Market=@Market and (Operator=@Operator or Operator='')   )  \n");

            //sb.Append("  And IsNull(PA.Status,1) = 1  \n");

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
            {
                if (filter.ExpandDate == 0)
                    sb.Append("  And (PP.CheckIn + (PP.Night - IsNull(PP.HotNight, PP.Night))) = @CIn1 \n");
                else
                    sb.Append("  And (PP.CheckIn + (PP.Night - IsNull(PP.HotNight, PP.Night))) Between @CIn1 AND @CIn2  \n");
            }
            else
            {
                if (filter.ExpandDate == 0)
                    sb.Append("  And PP.CheckIn = @CIn1 \n");
                else
                    sb.Append("  And PP.CheckIn Between @CIn1 AND @CIn2  \n");
            }
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                if (filter.NightFrom >= filter.NightTo)
                    sb.Append("  And IsNull(PP.HotNight,PP.Night) = @Night1 \n");
                else
                    sb.Append("  And IsNull(PP.HotNight,PP.Night) between @Night1 and @Night2 \n");
            }
            else
            {
                if (filter.NightFrom >= filter.NightTo)
                    sb.Append("  And PP.Night = @Night1 \n");
                else
                    sb.Append("  And PP.Night between @Night1 and @Night2 \n");
            }

            sb.Append("  And dbo.ChdAgeControl4Age(RT.Child, RT.ChdAge1, Rt.ChdAge2, Rt.ChdAge3, Rt.ChdAge4, HAP.ChdAgeG1, HAP.ChdAgeG2, HAP.ChdAgeG3, HAP.ChdAgeG4, PP.ChdG1Age1, PP.ChdG1Age2, PP.ChdG2Age1, PP.ChdG2Age2, PP.ChdG3Age1, PP.ChdG3Age2, PP.ChdG4Age1, PP.ChdG4Age2) = 1 \n");

            sb.Append("  And Exists(Select RecID From #AgencyCP Where RecID = PA.CatPackID)  \n");

            if (!string.IsNullOrEmpty(hotelWhereStr))
            {
                sb.Insert(0, hotelWhereStr);
                sb.Append(" And Exists(Select Code From @Hotel Where Code=H.Code) \n");
            }

            if (!string.IsNullOrEmpty(filter.Package))
                sb.AppendFormat("  And CP.HolPack = '{0}' \n", filter.Package);

            if (!string.IsNullOrEmpty(categoryWhereStr))
            {
                sb.Insert(0, categoryWhereStr);
                sb.Append("  And Exists(Select Code From @Category Where Code=H.Category)");
            }

            if (!string.IsNullOrEmpty(resortWhereStr))
            {

                sb.Insert(0, resortWhereStr);
                if (filter.Resort.Split('|').Length == 1)
                {
                    Location _locations = locations.Find(f => f.RecID == Conversion.getInt32OrNull(filter.Resort.Split('|')[0]));
                    if (_locations != null && _locations.Type == 3)
                        sb.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.Town={0} And L.RecID=H.Location) \n", _locations.RecID);
                    else
                        sb.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.RecID={0} And L.RecID=H.Location) \n", _locations.RecID);
                }
                else
                    sb.Append("  And Exists(Select R.RecID From #tmpResort R Where R.RecID=H.Location) \n");
            }

            if (!string.IsNullOrEmpty(roomWhereStr))
            {
                sb.Insert(0, roomWhereStr);
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Room From GrpRoomDet GRD (NOLOCK) Where Exists(Select Code From @Room Where Code=GRD.Groups) And Room=PA.Room) \n");
                else
                    sb.Append("  And (Select Code From @Room Where Code=PA.Room) \n");
            }

            if (!string.IsNullOrEmpty(boardWhereStr))
            {
                sb.Insert(0, boardWhereStr);
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Board From GrpBoardDet GBD (NOLOCK) Where Exists(Select Code From @Board Where Code=GBD.Groups) And Board=PA.Board) \n");
                else
                    sb.Append("  And (Select Code From @Board Where Code=PA.Board) \n");
            }

            sb.Append("Delete From #PriceList \n");
            sb.Append("Where not Exists \n");
            sb.Append("    (Select * from (Select Max(CatPackID) as CatPackID, HolPack, Hotel, Room, Accom, Board, CheckIn, Night From #PriceList \n");
            sb.Append("                    Group by Hotel, HolPack, Room, Accom, Board, CheckIn, Night) as Newest \n");
            sb.Append("     Where Newest.CatPackID=#PriceList.CatPackID \n");
            sb.Append("           and Newest.Hotel=#PriceList.Hotel \n");
            sb.Append("           and Newest.HolPack=#PriceList.HolPack \n");
            sb.Append("           and Newest.Room=#PriceList.Room \n");
            sb.Append("           and Newest.Accom=#PriceList.Accom \n");
            sb.Append("           and Newest.Board=#PriceList.Board \n");
            sb.Append("           and Newest.CheckIn=#PriceList.CheckIn \n");
            sb.Append("           and Newest.Night=#PriceList.Night \n");
            sb.Append("    ) \n");

            #region Only Hotel Flight Or Transport Type
            sb.Append("  Update #PriceList \n");
            sb.Append("  Set TransportType=Case when HP.Service='FLIGHT' Then 1 \n");
            sb.Append("                         when HP.Service='TRANSPORT' Then 2 Else 0 End \n");
            sb.Append("  From HolPackPlan HP \n");
            sb.Append("  Where HP.HolPack=#PriceList.HolPack and HP.StepNo=1 \n");

            sb.Append("  Update #PriceList \n");
            sb.Append("  Set TransportType = 2 \n");
            sb.Append("  From HolPackPlan HP \n");
            sb.Append("  Where #PriceList.TransportType=0 And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack=#PriceList.HolPack And Service='TRANSPORT') Between 1 And 2 \n");
            sb.Append("   And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack=#PriceList.HolPack And Service='FLIGHT')=0 \n");
            #endregion

            string checkAllotStr = string.Empty;
            List<dbInformationShema> spShema = VersionControl.getSpShema("ufn_CheckTransportAlllotForSearch", ref errorMsg);

            if (string.Equals(UserData.CustomRegID, Common.crID_FilipTravel) /*checkAvailableFlightSeat*/)
            {
                if (checkAllotStr.Length > 0)
                    checkAllotStr += ", ";
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 37, VersionControl.Equality.gt))
                {
                    if (spShema.Where(w => w.Name == "@HolPack").Count() > 0)
                        checkAllotStr += @" DepSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(CheckIn,DepCity,ArrCity,Operator,Holpack)                                              
                                                     When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 0, null) 
                                                     Else null 
                                                     End
                                      ";
                    else
                        checkAllotStr += @" DepSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(CheckIn,DepCity,ArrCity,Operator)                                              
                                                     When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 0, null) 
                                                     Else null 
                                                     End
                                      ";
                }
                else
                    if (sType != SearchType.OnlyHotelSearch)
                {
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200003"))
                        checkAllotStr += " DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 0, null) ";
                    else
                        checkAllotStr += " DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, CatPackID, PRecNo, DepFlight, CheckIn, Night, FlightClass, HolPack, DepCity, ArrCity, 0, null) ";
                }
                if (checkAllotStr.Length > 0)
                    checkAllotStr += ", ";

                if (VersionControl.CheckWebVersion(UserData.WebVersion, 37, VersionControl.Equality.gt))
                {
                    if (spShema.Where(w => w.Name == "@HolPack").Count() > 0)
                        checkAllotStr += @" RetSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(CheckIn+Night,ArrCity,DepCity,Operator,HolPack)
                                                     When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 1, null)
                                                     Else null 
                                                     End                                            
                                      ";
                    else
                        checkAllotStr += @" RetSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(CheckIn+Night,ArrCity,DepCity,Operator)
                                                     When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 1, null)
                                                     Else null 
                                                     End                                            
                                      ";
                }
                else
                    if (sType != SearchType.OnlyHotelSearch)
                {
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200003"))
                        checkAllotStr += " RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 1, null) ";
                    else
                        checkAllotStr += " RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, CatPackID, PRecNo, DepFlight, CheckIn, Night, FlightClass, HolPack, DepCity, ArrCity, 1, null) ";
                }
            }

            if (checkAllotStr.Length > 0)
            {
                sb.Append(" Update #PriceList ");
                sb.Append(" Set ");
                sb.Append(checkAllotStr);
            }

            sb.Append("Update #PriceList \n");
            sb.Append("Set OldSalePrice=CalcSalePrice \n");

            sb.Append("Update #PriceList \n");
            sb.Append("Set CalcSalePrice=CalcSalePrice-((EBAmount*IsNull(PasEBPer,0))/100) \n");

            sb.Append("Update #PriceList \n");
            sb.Append("Set LastPrice=dbo.ufn_Exchange(dbo.DateOnly(GetDate()),PLCur, isnull(@Cur,PLCur),CalcSalePrice,1,Market), SaleCur=isnull(@Cur,PLCur) \n");

            sb.Append("Select P.* \n");
            sb.Append("From #PriceList P \n");

            string whereStr = string.Empty;
            if (/*checkAvailableRoom && */!filter.ExtAllotControl)
            {
                if (whereStr.Length > 0)
                    whereStr += " AND ";
                whereStr += " FreeRoom>=@totalSeat ";
            }

            if ((filter.ShowStopSaleHotels && !filter.ExtAllotControl) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                if (whereStr.Length > 0)
                    whereStr += " AND ";
                whereStr += " Not (StopSaleStd=2 And StopSaleGuar=2)";
            }

            if (whereStr.Length > 0)
            {
                sb.Append(" Where ");
                sb.Append(whereStr);
            }
            sb.Append(" Order by P.LastPrice ");

            List<SearchResult> result = new List<SearchResult>();
            #region Age Group
            List<AgeGroup> ageGroups = new List<AgeGroup>();
            int roomCnt = 0;
            int refNo = 0;
            foreach (SearchCriteriaRooms row in filter.RoomsInfo)
            {
                roomCnt++;
                refNo++;
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Adult, GroupNo = roomCnt, RefNo = refNo, Unit = row.Adult, Age = null, DateOfBirth = null });
                if (row.Chd1Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Infant, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd1Age.Value, DateOfBirth = null });
                if (row.Chd2Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd2Age.Value, DateOfBirth = null });
                if (row.Chd3Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd3Age.Value, DateOfBirth = null });
                if (row.Chd4Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd4Age.Value, DateOfBirth = null });
            }

            #endregion
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sb.ToString());
            dbCommand.CommandTimeout = 120;
            try
            {
                int i = 0;
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "CustomRegID", DbType.String, UserData.CustomRegID);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Office", DbType.String, UserData.OprOffice);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "CIn1", DbType.Date, filter.CheckIn.Date);
                if (filter.ExpandDate > 0)
                    db.AddInParameter(dbCommand, "CIn2", DbType.Date, filter.CheckIn.Date.AddDays(filter.ExpandDate));
                db.AddInParameter(dbCommand, "Night1", DbType.String, filter.NightFrom);
                if (filter.NightFrom < filter.NightTo)
                    db.AddInParameter(dbCommand, "Night2", DbType.String, filter.NightTo);
                db.AddInParameter(dbCommand, "Cur", DbType.String, filter.CurControl ? filter.CurrentCur : null);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        ++i;
                        SearchResult r = new SearchResult();
                        r.RefNo = i;
                        r.RoomNr = (int)oReader["RoomNr"];
                        r.Calculated = true;
                        r.CatPackID = Conversion.getInt32OrNull(oReader["CatPackID"]);
                        r.ARecNo = Conversion.getInt32OrNull(oReader["ARecNo"]);
                        r.PRecNo = Conversion.getInt32OrNull(oReader["PRecNo"]);
                        r.HAPRecId = Conversion.getInt32OrNull(oReader["HAPRecID"]);
                        r.Market = Conversion.getStrOrNull(oReader["Market"]);
                        r.Operator = Conversion.getStrOrNull(oReader["Operator"]);
                        r.HolPack = Conversion.getStrOrNull(oReader["HolPack"]);
                        r.Night = (Int16)oReader["Night"];
                        r.CheckIn = (DateTime)oReader["CheckIn"];
                        r.CheckOut = r.CheckIn.Value.AddDays(r.Night.Value);
                        r.AccomFullName = Conversion.getStrOrNull(oReader["AccomFullName"]);

                        r.NetCur = Conversion.getStrOrNull(oReader["NetCur"]);
                        //r.EBValidDate = Conversion.getDateTimeOrNull(oReader["EBValidDate"]);
                        r.EBAmount = Conversion.getDecimalOrNull(oReader["EBAmount"]);
                        r.Description = Conversion.getStrOrNull(oReader["Description"]);
                        if (string.IsNullOrEmpty(r.Description))
                            r.Description = useLocalName ? Conversion.getStrOrNull(oReader["CPNameL"]) : Conversion.getStrOrNull(oReader["CPName"]);

                        r.OprText = Conversion.getStrOrNull(oReader["OprText"]);
                        r.SaleEndDate = Conversion.getDateTimeOrNull(oReader["SaleEndDate"]);
                        r.HAdult = (Int16)oReader["HAdult"];
                        r.HChdAgeG1 = (Int16)oReader["HChdAgeG1"];
                        r.HChdAgeG2 = (Int16)oReader["HChdAgeG2"];
                        r.HChdAgeG3 = (Int16)oReader["HChdAgeG3"];
                        r.HChdAgeG4 = (Int16)oReader["HChdAgeG4"];
                        r.ChdG1Age1 = Conversion.getDecimalOrNull(oReader["ChdG1Age1"]);
                        r.ChdG1Age2 = Conversion.getDecimalOrNull(oReader["ChdG1Age2"]);
                        r.ChdG2Age1 = Conversion.getDecimalOrNull(oReader["ChdG2Age1"]);
                        r.ChdG2Age2 = Conversion.getDecimalOrNull(oReader["ChdG2Age2"]);
                        r.ChdG3Age1 = Conversion.getDecimalOrNull(oReader["ChdG3Age1"]);
                        r.ChdG3Age2 = Conversion.getDecimalOrNull(oReader["ChdG3Age2"]);
                        r.ChdG4Age1 = Conversion.getDecimalOrNull(oReader["ChdG4Age1"]);
                        r.ChdG4Age2 = Conversion.getDecimalOrNull(oReader["ChdG4Age2"]);

                        r.LastPrice = Conversion.getDecimalOrNull(oReader["LastPrice"]);
                        r.CalcSalePrice = Conversion.getDecimalOrNull(oReader["CalcSalePrice"]);
                        r.OldSalePrice = Conversion.getDecimalOrNull(oReader["OldSalePrice"]);
                        r.SaleChdG1 = Conversion.getDecimalOrNull(oReader["SaleChdG1"]);
                        r.SaleChdG2 = Conversion.getDecimalOrNull(oReader["SaleChdG2"]);
                        r.SaleChdG3 = Conversion.getDecimalOrNull(oReader["SaleChdG3"]);
                        r.SaleChdG4 = Conversion.getDecimalOrNull(oReader["SaleChdG4"]);
                        r.HotChdPrice = Conversion.getDecimalOrNull(oReader["HotChdPrice"]);
                        r.Hotel = Conversion.getStrOrNull(oReader["Hotel"]);
                        r.HotelName = Conversion.getStrOrNull(oReader["HotelName"]);
                        r.HotelNameL = Conversion.getStrOrNull(oReader["HotelNameL"]);
                        r.HotelLocation = Conversion.getInt32OrNull(oReader["HotLocation"]);
                        r.HotLocationName = Conversion.getStrOrNull(oReader["HotLocationName"]);
                        r.HotelRecID = Conversion.getInt32OrNull(oReader["HotelRecID"]);
                        r.HotCat = Conversion.getStrOrNull(oReader["HotCat"]);
                        r.Room = Conversion.getStrOrNull(oReader["Room"]);
                        r.RoomName = Conversion.getStrOrNull(oReader["RoomName"]);
                        r.RoomNameL = Conversion.getStrOrNull(oReader["RoomNameL"]);
                        r.Board = Conversion.getStrOrNull(oReader["Board"]);
                        r.BoardName = Conversion.getStrOrNull(oReader["BoardName"]);
                        r.BoardNameL = Conversion.getStrOrNull(oReader["BoardNameL"]);
                        r.Accom = Conversion.getStrOrNull(oReader["Accom"]);
                        r.AccomName = Conversion.getStrOrNull(oReader["AccomName"]);
                        r.AccomNameL = Conversion.getStrOrNull(oReader["AccomNameL"]);
                        r.Night = Conversion.getInt16OrNull(oReader["Night"]);
                        r.HotNight = Conversion.getInt16OrNull(oReader["HotNight"]);
                        r.CheckIn = Conversion.getDateTimeOrNull(oReader["CheckIn"]);
                        r.CheckOut = r.CheckIn.Value.AddDays(r.Night.Value);
                        r.SaleCur = Conversion.getStrOrNull(oReader["SaleCur"]);
                        r.PLCur = Conversion.getStrOrNull(oReader["PLCur"]);
                        r.CurrentCur = filter.CurControl ? filter.CurrentCur : string.Empty;
                        r.DepCity = Conversion.getInt32OrNull(oReader["DepCity"]);
                        r.DepCityName = Conversion.getStrOrNull(oReader["DepCityName"]);
                        r.ArrCity = Conversion.getInt32OrNull(oReader["ArrCity"]);
                        r.ArrCityName = Conversion.getStrOrNull(oReader["ArrCityName"]);
                        r.DepFlight = Conversion.getStrOrNull(oReader["DepFlight"]);
                        r.RetFlight = Conversion.getStrOrNull(oReader["RetFlight"]);
                        r.DEPFlightTime = Conversion.getDateTimeOrNull(oReader["DEPFlightTime"]);
                        r.RETFlightTime = Conversion.getDateTimeOrNull(oReader["RETFlightTime"]);
                        r.FreeRoom = Conversion.getInt32OrNull(oReader["FreeRoom"]);
                        r.StopSaleGuar = Conversion.getInt16OrNull(oReader["StopSaleGuar"]);
                        r.StopSaleStd = Conversion.getInt16OrNull(oReader["StopSaleStd"]);
                        r.AutoStop = Conversion.getBoolOrNull(oReader["AutoStop"]);
                        if (string.Equals(UserData.CustomRegID, Common.crID_FilipTravel))
                        {
                            r.DepSeat = Conversion.getInt32OrNull(oReader["DepSeat"]);
                            r.RetSeat = Conversion.getInt32OrNull(oReader["RetSeat"]);
                        }

                        r.LogID = logID;
                        result.Add(r);

                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return result;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<MultiRoomSelection> getEmptySearchGroups(User UserData, List<SearchResult> data, SearchCriteria filter, ref string errorMsg)
        {
            int i = 0;
            List<SearchCriteriaRooms> roomDetail = filter.RoomsInfo;
            int TotalAdult = roomDetail.Sum(s => s.Adult);
            int TotalChild = roomDetail.Sum(s => s.Child);

            List<MultiRoomSelection> list = new List<MultiRoomSelection>();
            List<hotelGroups> tmpdata = new List<hotelGroups>();
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_PliusTravel) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_FamilyTour) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_FilipTravel) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_KidyTour) ||
                //string.Equals(UserData.CustomRegID, TvBo.Common.crID_Calypso) ||
                string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun))
            {
                var q1 = from q in data.OrderBy(o => o.OrderHotel)
                         group q by new { q.Hotel, q.CheckIn, q.Night } into k
                         select new { k.Key.Hotel, k.Key.CheckIn, k.Key.Night };
                foreach (var r1 in q1)
                {
                    tmpdata.Add(new hotelGroups
                    {
                        Hotel = r1.Hotel,
                        CheckIn = r1.CheckIn,
                        Night = r1.Night,
                        MinPrice = data.Where(w => w.Hotel == r1.Hotel && w.Night == r1.Night && w.CheckIn.Value == r1.CheckIn.Value).Min(m => m.LastPrice),
                        roomCount = data.Where(w => w.Hotel == r1.Hotel && w.Night == r1.Night && w.CheckIn.Value == r1.CheckIn.Value).GroupBy(g => g.RoomNr).Count()
                    });
                }
            }
            else
            {
                var q1 = from q in data.OrderBy(o => o.OrderHotel)
                         group q by new { q.Hotel, q.CheckIn, q.Night, q.UseDynamicFlights } into k
                         select new { k.Key.Hotel, k.Key.CheckIn, k.Key.Night, k.Key.UseDynamicFlights };
                foreach (var r1 in q1)
                {
                    tmpdata.Add(new hotelGroups
                    {
                        Hotel = r1.Hotel,
                        CheckIn = r1.CheckIn,
                        Night = r1.Night,
                        MinPrice = data.Where(w => w.Hotel == r1.Hotel && w.Night == r1.Night && w.CheckIn.Value == r1.CheckIn.Value).Min(m => m.CalcSalePrice),
                        roomCount = data.Where(w => w.Hotel == r1.Hotel && w.Night == r1.Night && w.CheckIn.Value == r1.CheckIn.Value).GroupBy(g => g.RoomNr).Count(),
                        useDynamicFlights = r1.UseDynamicFlights,
                        OrderHotel = data.Where(w => w.Hotel == r1.Hotel && w.Night == r1.Night && w.CheckIn.Value == r1.CheckIn.Value).FirstOrDefault().OrderHotel
                    });
                }
            }

            foreach (var r in tmpdata.Where(w => w.useDynamicFlights ? (w.roomCount >= filter.RoomCount) : (w.roomCount == filter.RoomCount)).OrderBy(o => o.MinPrice).OrderBy(o => o.OrderHotel))
            {
                List<SearchResult> addTmp = (from q in data
                                             orderby q.LastPrice
                                             where q.Hotel == r.Hotel && q.CheckIn == r.CheckIn && q.Night == r.Night
                                             select q).ToList<SearchResult>();
                MultiRoomSelection roomsGroup = new MultiRoomSelection();
                ++i;
                roomsGroup.RoomGroupRefNo = i;
                roomsGroup.Hotel = r.Hotel;
                roomsGroup.Night = r.Night.Value;
                roomsGroup.CheckIn = r.CheckIn.Value;
                roomsGroup.MinPrice = r.MinPrice;
                roomsGroup.OrderDisplay = r.OrderHotel;
                foreach (SearchResult row in addTmp)
                    roomsGroup.RoomPriceList.Add(row.RefNo);
                list.Add(roomsGroup);
            }

            return list;
        }

        public List<MultiRoomSelection> getEmptySearchGroupsOH(User UserData, List<SearchResultOH> data, SearchCriteria filter, ref string errorMsg)
        {
            int i = 0;
            List<SearchCriteriaRooms> roomDetail = filter.RoomsInfo;
            int TotalAdult = roomDetail.Sum(s => s.Adult);
            int TotalChild = roomDetail.Sum(s => s.Child);

            List<MultiRoomSelection> list = new List<MultiRoomSelection>();
            List<hotelGroups> tmpdata = new List<hotelGroups>();
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                var q1 = from q in data
                         group q by new { q.Hotel, q.CheckIn, q.Night } into k
                         select new { k.Key.Hotel, k.Key.CheckIn, k.Key.Night };
                foreach (var r1 in q1)
                {
                    tmpdata.Add(new hotelGroups
                    {
                        Hotel = r1.Hotel,
                        CheckIn = r1.CheckIn,
                        Night = r1.Night,
                        MinPrice = data.Where(w => w.Hotel == r1.Hotel && w.Night == r1.Night && w.CheckIn.Value == r1.CheckIn.Value).Min(m => m.SalePrice),
                        roomCount = data.Where(w => w.Hotel == r1.Hotel && w.Night == r1.Night && w.CheckIn.Value == r1.CheckIn.Value).GroupBy(g => g.RoomNr).Count()
                    });
                }
            }
            else
            {
                var q1 = from q in data
                         group q by new { q.Hotel, q.CheckIn, q.Night } into k
                         select new { k.Key.Hotel, k.Key.CheckIn, k.Key.Night };
                foreach (var r1 in q1)
                {
                    tmpdata.Add(new hotelGroups
                    {
                        Hotel = r1.Hotel,
                        CheckIn = r1.CheckIn,
                        Night = r1.Night,
                        MinPrice = data.Where(w => w.Hotel == r1.Hotel && w.Night == r1.Night && w.CheckIn.Value == r1.CheckIn.Value).Min(m => m.SalePrice),
                        roomCount = data.Where(w => w.Hotel == r1.Hotel && w.Night == r1.Night && w.CheckIn.Value == r1.CheckIn.Value).GroupBy(g => g.RoomNr).Count(),
                        useDynamicFlights = false
                    });
                }
            }

            foreach (var r in tmpdata.Where(w => w.useDynamicFlights ? (w.roomCount >= filter.RoomCount) : (w.roomCount == filter.RoomCount)).OrderBy(o => o.MinPrice))
            {
                List<SearchResultOH> addTmp = (from q in data
                                               orderby q.SalePrice
                                               where q.Hotel == r.Hotel && q.CheckIn == r.CheckIn && q.Night == r.Night
                                               select q).ToList<SearchResultOH>();
                MultiRoomSelection roomsGroup = new MultiRoomSelection();
                ++i;
                roomsGroup.RoomGroupRefNo = i;
                roomsGroup.Hotel = r.Hotel;
                roomsGroup.Night = r.Night.Value;
                roomsGroup.CheckIn = r.CheckIn.Value;
                roomsGroup.MinPrice = r.MinPrice;
                foreach (SearchResultOH row in addTmp)
                    roomsGroup.RoomPriceList.Add(row.RefNo);
                list.Add(roomsGroup);
            }
            return list.OrderBy(o => o.MinPrice).ToList<MultiRoomSelection>();
        }

        public List<SearchResult> getEmptySearchOld(User UserData, SearchCriteria filter, ref string errorMsg)
        {
            string resortWhereStr = string.Empty;
            string categoryWhereStr = string.Empty;
            string hotelWhereStr = string.Empty;
            string roomWhereStr = string.Empty;
            string boardWhereStr = string.Empty;

            if (!string.IsNullOrEmpty(filter.Resort))
            {
                foreach (string r in filter.Resort.Split('|'))
                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n", r);
                if (resortWhereStr.Length > 0)
                    resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                else
                    resortWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Category))
            {
                foreach (string r in filter.Category.Split('|'))
                    categoryWhereStr += string.Format("INSERT INTO @Category(Code) VALUES('{0}') \n", r);
                if (categoryWhereStr.Length > 0)
                    categoryWhereStr = "DECLARE @Category TABLE (Code VarChar(10)) \n" + categoryWhereStr;
                else
                    categoryWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Hotel))
            {
                foreach (string r in filter.Hotel.Split('|'))
                    hotelWhereStr += string.Format("INSERT INTO @Hotel(Code) VALUES('{0}') \n", r);
                if (hotelWhereStr.Length > 0)
                    hotelWhereStr = "DECLARE @Hotel TABLE (Code VarChar(10)) \n" + hotelWhereStr;
                else
                    hotelWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Room))
            {
                foreach (string r in filter.Room.Split('|'))
                    roomWhereStr += string.Format("INSERT INTO @Room(Code) VALUES('{0}') \n", r);
                if (roomWhereStr.Length > 0)
                    roomWhereStr = "DECLARE @Room TABLE (Code VarChar(10)) \n" + roomWhereStr;
                else
                    roomWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Board))
            {
                foreach (string r in filter.Board.Split('|'))
                    boardWhereStr += string.Format("INSERT INTO #tmpBoard(Code) VALUES('{0}') \n", r);
                if (boardWhereStr.Length > 0)
                    boardWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpBoard') is not null Drop Table dbo.#tmpBoard
Create Table #tmpBoard (Code VarChar(10) COLLATE Latin1_General_CI_AS) " + boardWhereStr;
                else
                    boardWhereStr = string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#AgencyCP') is not null Drop Table dbo.#AgencyCP \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#PriceList') is not null Drop Table dbo.#PriceList \n");
            sb.Append("Select *  \n");
            sb.Append("Into #CatalogPack \n");
            sb.Append("From CatalogPack (NOLOCK) CP \n");
            sb.Append("Where	CP.Ready = 'Y'  \n");
            sb.Append("	And CP.EndDate > GetDate()-1  \n");
            sb.Append("	And CP.WebPub = 'Y'  \n");
            sb.Append("	And CP.WebPubDate < GetDate() \n");
            sb.Append("	And dbo.DateOnly(GETDATE()) BetWeen CP.SaleBegDate And CP.SaleEndDate \n");
            sb.AppendFormat(" And CP.DepCity = {0} \n", filter.DepCity.ToString());
            sb.AppendFormat(" And CP.ArrCity = {0} \n", filter.ArrCity.ToString());
            switch (filter.SType)
            {
                case SearchType.PackageSearch:
                    sb.Append("	And CP.PackType = 'H'  \n");
                    break;
                case SearchType.OnlyHotelSearch:
                    sb.Append("	And CP.PackType = 'O'  \n");
                    break;
                default:
                    sb.Append("	And CP.PackType = 'none'  \n");
                    break;
            }
            sb.Append("Select Distinct RecID  \n");
            sb.Append("Into #AgencyCP \n");
            sb.Append("From  \n");
            sb.Append("( \n");
            sb.Append("	Select CP.RecID \n");
            sb.Append("	From #CatalogPack CP (NOLOCK)  \n");
            sb.Append("	Where   \n");
            sb.Append("	 Not Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID) or \n");
            sb.Append("  Exists(Select CA.RecID from CatPackAgency CA (NOLOCK) \n");
            sb.Append("	        Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups=CA.AgencyGrp and AG.Agency=@Agency --And AG.GrpType='P' \n");
            sb.Append("	        Where CA.CatPackID=CP.RecID and (AG.Agency=@Agency or CA.Agency=@Agency)) \n");
            //sb.Append("	Union All  \n");
            //sb.Append("	Select CP.RecID  \n");
            //sb.Append("	From #CatalogPack CP (NOLOCK)  \n");
            //sb.Append("	Where  \n");
            //sb.Append("		Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=@Agency) \n");
            sb.Append(") CP \n");
            sb.Append(" \n");
            sb.Append("Select \n");
            sb.Append("	PA.CatPackID, ARecNo = PA.RecNo, PRecNo = PP.RecNo, HAPRecID = HAP.RecID, PA.Hotel, PA.Room, PA.Board, PA.Accom, PP.Night, PP.CheckIn, \n");
            sb.Append(" CP.HolPack, CP.SaleCur,	 \n");
            sb.Append("	CalcSalePrice=PP.SalePrice+ \n");
            sb.Append("		 Case when HAP.Adult-PA.Adl>=1 then PP.SaleExtBed1 else 0 end+ \n");
            sb.Append("		 Case when HAP.Adult-PA.Adl>=2 then PP.SaleExtBed2 else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG1>0 then (PP.SaleChdG1*HAP.ChdAgeG1)+CHDP.ChdG1Price else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG2>0 then (PP.SaleChdG2*HAP.ChdAgeG2)+CHDP.ChdG2Price else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG3>0 then (PP.SaleChdG3*HAP.ChdAgeG3)+CHDP.ChdG3Price else 0 end+ \n");
            sb.Append("		 Case when HAP.ChdAgeG4>0 then (PP.SaleChdG4*HAP.ChdAgeG4)+CHDP.ChdG4Price else 0 end, \n");
            sb.Append("	SaleChdG1= Case when HAP.ChdAgeG1>0 then PP.SaleChdG1+CHDP.ChdG1Price/dbo.IfZero(HAP.ChdAgeG1,1) else 0 end, \n");
            sb.Append("	SaleChdG2= Case when HAP.ChdAgeG2>0 then PP.SaleChdG2+CHDP.ChdG2Price/dbo.IfZero(HAP.ChdAgeG2,1) else 0 end, \n");
            sb.Append("	SaleChdG3= Case when HAP.ChdAgeG3>0 then PP.SaleChdG3+CHDP.ChdG3Price/dbo.IfZero(HAP.ChdAgeG3,1) else 0 end, \n");
            sb.Append("	SaleChdG4= Case when HAP.ChdAgeG4>0 then PP.SaleChdG4+CHDP.ChdG4Price/dbo.IfZero(HAP.ChdAgeG4,1) else 0 end, \n");
            sb.Append("	HotChdPrice= Case when HAP.ChdAgeG1>0 then CHDP.ChdG1Price else 0 end+ \n");
            sb.Append("				 Case when HAP.ChdAgeG2>0 then CHDP.ChdG2Price else 0 end+ \n");
            sb.Append("				 Case when HAP.ChdAgeG3>0 then CHDP.ChdG3Price else 0 end+ \n");
            sb.Append("				 Case when HAP.ChdAgeG4>0 then CHDP.ChdG4Price else 0 end, \n");
            if (UserData.ShowFlight && filter.SType == SearchType.PackageSearch)
            {
                sb.Append("    DepFlight=FDD.FlightNo, RetFlight=FDD.NextFlight, \n");
                sb.Append("    DepFlightTime=isnull(FDD.DepTime, Cast(0 AS DateTime)), RetFlightTime=isnull(FDR.DepTime, Cast(0 AS DateTime)) \n");
            }
            else
            {
                sb.Append("    DepFlight='          ', RetFlight='          ', \n");
                sb.Append("    DepFlightTime=Cast(0 AS DateTime), RetFlightTime=Cast(0 AS DateTime) \n");
            }
            sb.Append("Into #PriceList \n");
            sb.Append("From CatPriceA PA (NOLOCK) \n");
            sb.Append("Join CatPriceP PP (NOLOCK) on PP.CatPackID=PA.CatPackID and PP.PriceAID=PA.RecNo \n");

            sb.Append("Join #CatalogPack CP (NOLOCK) on CP.RecID=PA.CatPAckID and CP.PackType<>'T' \n");
            sb.Append("join Hotel H  (NOLOCK) on H.Code=PA.Hotel \n");
            sb.Append("join HotelAccomPax HAP  (NOLOCK) on HAP.Hotel=PA.Hotel and HAP.Room=PA.Room and HAP.Accom=PA.Accom and IsNull(HAP.VisiblePL,'Y')='Y' \n");
            sb.Append("OUTER APPLY \n");
            sb.Append("( \n");
            sb.Append("   Select Max(Cast(isnull(Adult,0) as varchar(1)) + Cast(isnull(ChdAgeG1,0) as varchar(1)) + Cast(isnull(ChdAgeG2,0) as varchar(1)) + Cast(isnull(ChdAgeG3,0) as varchar(1)) + Cast(isnull(ChdAgeG4,0) as varchar(1))) AdlStr \n");
            sb.Append("   From CatHotelChd C \n");
            sb.Append("   where C.CatPackID=PP.CatPackID and C.RecNo=PP.HotelChdRecNo and \n");
            sb.Append("         dbo.IfZero(C.Adult,HAP.Adult)=HAP.Adult and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG1,HAP.ChdAgeG1)=HAP.ChdAgeG1 and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG2,HAP.ChdAgeG2)=HAP.ChdAgeG2 and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG3,HAP.ChdAgeG3)=HAP.ChdAgeG3 and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG4,HAP.ChdAgeG4)=HAP.ChdAgeG4 and \n");
            sb.Append("         dbo.IfZero(C.ChdAgeG1+C.ChdAgeG2+C.ChdAgeG3+C.ChdAgeG4,HAP.ChdAgeG1+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4)=HAP.ChdAgeG1+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4 \n");
            sb.Append(") CHDM \n");
            sb.Append("OUTER APPLY \n");
            sb.Append("( \n");
            sb.Append("Select Top 1 * From CatHotelChd C (NOLOCK) \n");
            sb.Append("Where C.CatPackID=PP.CatPackID and C.RecNo=PP.HotelChdRecNo and \n");
            sb.Append("      (Cast(isnull(Adult,0) as varchar(1)) + Cast(isnull(ChdAgeG1,0) as varchar(1)) + Cast(isnull(ChdAgeG2,0) as varchar(1)) + Cast(isnull(ChdAgeG3,0) as varchar(1)) + Cast(isnull(ChdAgeG4,0) as varchar(1))) = CHDM.AdlStr \n");
            sb.Append(") CHD \n");
            sb.Append("OUTER APPLY \n");
            sb.Append("( \n");
            sb.Append("Select * \n");
            sb.Append("From dbo.HotChdCalc(IsNull(CHD.ChdG1PriceN1,0),IsNull(CHD.ChdG1PriceN2,0),IsNull(CHD.ChdG1PriceN3,0),IsNull(CHD.ChdG1PriceN4,0), \n");
            sb.Append("                    IsNull(CHD.ChdG2PriceN1,0),IsNull(CHD.ChdG2PriceN2,0),IsNull(CHD.ChdG2PriceN3,0),IsNull(CHD.ChdG2PriceN4,0), \n");
            sb.Append("                    IsNull(CHD.ChdG3PriceN1,0),IsNull(CHD.ChdG3PriceN2,0),IsNull(CHD.ChdG3PriceN3,0),IsNull(CHD.ChdG3PriceN4,0), \n");
            sb.Append("                    IsNull(CHD.ChdG4PriceN1,0),IsNull(CHD.ChdG4PriceN2,0),IsNull(CHD.ChdG4PriceN3,0),IsNull(CHD.ChdG4PriceN4,0), \n");
            sb.Append("                    HAP.ChdAgeG1,HAP.ChdAgeG2,HAP.ChdAgeG3,HAP.ChdAgeG4,H.ChdCalcSale,IsNull(CHD.ChdG1Out,'N'), \n");
            sb.Append("                    PP.ChdG1Age1,PP.ChdG1Age2,PP.ChdG2Age1,PP.ChdG2Age2,PP.ChdG3Age1,PP.ChdG3Age2,PP.ChdG4Age1,PP.ChdG4Age2, \n");
            sb.Append("                    PA.CatPackID,0,0,0,0) \n");
            sb.Append(") CHDP \n");
            if (UserData.ShowFlight && filter.SType == SearchType.PackageSearch)
            {
                sb.Append("Left JOIN HolPackPlan HPP (NOLOCK) on HPP.HolPack = CP.HolPack and HPP.Service = 'FLIGHT' and HPP.Departure = CP.DepCity and HPP.Arrival = CP.ArrCity \n");
                sb.Append("Left JOIN HolPackSer HS (NOLOCK) on HS.HolPack = HPP.HolPack and HS.StepNo = HPP.StepNo \n");
                sb.Append("Left JOIN FlightDay FDD (NOLOCK) on FDD.FlightNo = HS.ServiceItem and FDD.FlyDate=PP.CheckIn \n");
                sb.Append("Left JOIN FlightDay FDR (NOLOCK) on FDR.FlightNo = FDD.NextFlight and FDR.FlyDate=(PP.CheckIn + PP.Night) \n");
            }

            sb.Append("Where Exists(Select RecID From CatPackMarket Where CatPackID=PA.CatPackID and Market=@Market and (Operator=@Operator or Operator='')   )  \n");

            sb.Append("  And Exists(Select Code From HotelRoom HR (NOLOCK) Where HR.Hotel=PA.Hotel and HR.Code=PA.Room and IsNull(HR.VisiblePL,'Y')='Y') \n");
            sb.Append("  And Exists(Select Code From HotelAccom HA (NOLOCK) Where HA.Hotel=PA.Hotel and HA.Room=PA.Room and HA.Code=PA.Accom and IsNull(HA.VisiblePL,'Y')='Y') \n");
            sb.Append("  And Exists(Select Code From HotelBoard HB (NOLOCK) Where HB.Hotel=PA.Hotel and HB.Code=PA.Board and IsNull(HB.VisiblePL,'Y')='Y') \n");

            sb.Append("  And IsNull(PA.Status,1) = 1  \n");
            if (string.Equals(UserData.CustomRegID, Common.crID_Mng_Tr))
            {
                if (filter.ExpandDate == 0)
                    sb.Append("  And (PP.CheckIn + (PP.Night - IsNull(PP.HotNight, PP.Night))) = @CIn1 \n");
                else
                    sb.Append("  And (PP.CheckIn + (PP.Night - IsNull(PP.HotNight, PP.Night))) Between @CIn1 AND @CIn2  \n");
            }
            else
            {
                if (filter.ExpandDate == 0)
                    sb.Append("  And PP.CheckIn = @CIn1 \n");
                else
                    sb.Append("  And PP.CheckIn Between @CIn1 AND @CIn2  \n");
            }
            if (string.Equals(UserData.CustomRegID, Common.crID_Mng_Tr))
            {
                if (filter.NightFrom >= filter.NightTo)
                    sb.Append("  And IsNull(PP.HotNight,PP.Night) = @Night1 \n");
                else
                    sb.Append("  And IsNull(PP.HotNight,PP.Night) between @Night1 and @Night2 \n");
            }
            else
            {
                if (filter.NightFrom >= filter.NightTo)
                    sb.Append("  And PP.Night = @Night1 \n");
                else
                    sb.Append("  And PP.Night between @Night1 and @Night2 \n");
            }
            /*
            sb.AppendFormat("  And HAP.Adult = {0} \n", filter.Adult.ToString());
            sb.AppendFormat("  And HAP.ChdAgeG1 + HAP.ChdAgeG2 + HAP.ChdAgeG3 + HAP.ChdAgeG4 = {0} \n", filter.Child.ToString());
            if (filter.Child > 0)
                sb.AppendFormat("  And dbo.ChdAgeControl4Age({0}, {1}, {2}, {3}, {4}, HAP.ChdAgeG1, HAP.ChdAgeG2, HAP.ChdAgeG3, HAP.ChdAgeG4, PP.ChdG1Age1, PP.ChdG1Age2, PP.ChdG2Age1, PP.ChdG2Age2, PP.ChdG3Age1, PP.ChdG3Age2, PP.ChdG4Age1, PP.ChdG4Age2) = 1 \n",
                                        filter.Child,
                                        filter.Chd1Age == null ? "null" : filter.Chd1Age.ToString(),
                                        filter.Chd2Age == null ? "null" : filter.Chd2Age.ToString(),
                                        filter.Chd3Age == null ? "null" : filter.Chd3Age.ToString(),
                                        filter.Chd4Age == null ? "null" : filter.Chd4Age.ToString());
            */
            sb.Append("  And Exists(Select RecID From #AgencyCP Where RecID=PA.CatPAckID)  \n");

            if (!string.IsNullOrEmpty(hotelWhereStr))
            {
                sb.Insert(0, hotelWhereStr);
                sb.Append(" And Exists(Select Code From @Hotel Where Code=H.Code) \n");
            }
            //if (!string.IsNullOrEmpty(filter.Hotel))
            //    sb.AppendFormat("  And H.Code = '{0}' \n", filter.Hotel);

            if (!string.IsNullOrEmpty(filter.Package))
                sb.AppendFormat("  And CP.HolPack = '{0}' \n", filter.Package);

            if (!string.IsNullOrEmpty(categoryWhereStr))
            {
                sb.Insert(0, categoryWhereStr);
                sb.Append("  And Exists(Select Code @Category Where Code=H.Category)");
            }
            //if (!string.IsNullOrEmpty(filter.Category))
            //    sb.AppendFormat("  And H.Category = '{0}' \n", filter.Category);

            if (!string.IsNullOrEmpty(resortWhereStr))
            {
                sb.Insert(0, resortWhereStr);
                sb.Append("  And Exists(Select R.RecID From #tmpResort R Where Exists(Select RecID From Location L (NOLOCK) Where L.Town=R.RecID AND L.RecID=H.Location)) \n");
            }
            //if (filter.Resort != null)
            //    sb.AppendFormat("  And Exists(Select RecId Town From Location (NOLOCK) Where RecID=H.Location And Town = {0}) \n", filter.Resort);

            if (!string.IsNullOrEmpty(roomWhereStr))
            {
                sb.Insert(0, roomWhereStr);
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Room From GrpRoomDet GRD (NOLOCK) Where Exists(Select Code From @Room Where Code=GRD.Groups) And Room=PA.Room) \n");
                else
                    sb.Append("  And (Select Code From @Room Where Code=PA.Room) \n");
            }
            //if (!string.IsNullOrEmpty(filter.Room))
            //    if (filter.UseGroup)
            //And Exits(Select Room From GrpRoomDet GRD (NOLOCK) Where Exists(Select Room From @Room Where Code=GRD.Groups) And Room = PA.Room)
            //        sb.AppendFormat("  And Exists(Select Room From GrpRoomDet (NOLOCK) Where Groups = '{0}' And Room = PA.Room) \n", filter.Room);
            //    else
            //        sb.AppendFormat("  And PA.Room = '{0}' \n", filter.Room);

            if (!string.IsNullOrEmpty(boardWhereStr))
            {
                sb.Insert(0, boardWhereStr);
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Board From GrpBoardDet GBD (NOLOCK) Where Exists(Select Code From @Board Where Code=GBD.Groups) And Board=PA.Board) \n");
                else
                    sb.Append("  And (Select Code From @Board Where Code=PA.Board) \n");
            }
            //if (!string.IsNullOrEmpty(filter.Board))
            //    if (filter.UseGroup)
            //        sb.AppendFormat("      And Exists(Select Board From GrpBoardDet (NOLOCK) Where Groups = '{0}' And Board = PA.Board) \n", filter.Board);
            //    else
            //        sb.AppendFormat("      And PA.Board = '{0}' \n", filter.Board);

            sb.Append("Delete From #PriceList \n");
            sb.Append("Where not Exists \n");
            sb.Append("    (Select * from (Select Max(CatPackID) as CatPackID, HolPack, Hotel, Room, Accom, Board, CheckIn, Night, DepFlight From #PriceList \n");
            sb.Append("                    Group by Hotel, HolPack, Room, Accom, Board, CheckIn, Night, DepFlight) as Newest \n");
            sb.Append("     Where Newest.CatPackID=#PriceList.CatPackID \n");
            sb.Append("           and Newest.Hotel=#PriceList.Hotel \n");
            sb.Append("           and Newest.HolPack=#PriceList.HolPack \n");
            sb.Append("           and Newest.Room=#PriceList.Room \n");
            sb.Append("           and Newest.Accom=#PriceList.Accom \n");
            sb.Append("           and Newest.Board=#PriceList.Board \n");
            sb.Append("           and Newest.CheckIn=#PriceList.CheckIn \n");
            sb.Append("           and Newest.Night=#PriceList.Night \n");
            sb.Append("           and Newest.DepFlight=#PriceList.DepFlight \n");
            sb.Append("    ) \n");
            sb.Append("Select * From #PriceList \n");

            List<SearchResult> result = new List<SearchResult>();
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(sb.ToString());

            try
            {
                int i = 0;
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "CIn1", DbType.Date, filter.CheckIn.Date);
                if (filter.ExpandDate > 0)
                    db.AddInParameter(dbCommand, "CIn2", DbType.Date, filter.CheckIn.Date.AddDays(filter.ExpandDate));
                db.AddInParameter(dbCommand, "Night1", DbType.String, filter.NightFrom);
                if (filter.NightFrom < filter.NightTo)
                    db.AddInParameter(dbCommand, "Night2", DbType.String, filter.NightTo);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        ++i;
                        SearchResult r = new SearchResult();
                        r.RefNo = i;
                        r.Calculated = false;
                        r.CatPackID = (int)oReader["CatPackID"];
                        r.ARecNo = (int)oReader["ARecNo"];
                        r.PRecNo = (int)oReader["PRecNo"];
                        r.HAPRecId = (int)oReader["HAPRecID"];
                        r.CalcSalePrice = Conversion.getDecimalOrNull(oReader["CalcSalePrice"]);
                        r.SaleChdG1 = Conversion.getDecimalOrNull(oReader["SaleChdG1"]);
                        r.SaleChdG2 = Conversion.getDecimalOrNull(oReader["SaleChdG2"]);
                        r.SaleChdG3 = Conversion.getDecimalOrNull(oReader["SaleChdG3"]);
                        r.SaleChdG4 = Conversion.getDecimalOrNull(oReader["SaleChdG4"]);
                        r.HotChdPrice = Conversion.getDecimalOrNull(oReader["HotChdPrice"]);
                        r.Hotel = (string)oReader["Hotel"];
                        r.Room = (string)oReader["Room"];
                        r.Board = (string)oReader["Board"];
                        r.Accom = (string)oReader["Accom"];
                        r.Night = (Int16)oReader["Night"];
                        r.CheckIn = (DateTime)oReader["CheckIn"];
                        r.CheckOut = r.CheckIn.Value.AddDays(r.Night.Value);
                        r.SaleCur = (string)oReader["SaleCur"];
                        r.CurrentCur = filter.CurrentCur;

                        result.Add(r);
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return result;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> getFilteredSearchData(User UserData, List<SearchResult> data, string FilterRecords, int From, int To, bool checkAvailableRoom, bool checkAvailableFlightSeat, bool extAllotControl, SearchType sType, string Currency, ref string errorMsg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SET ARITHABORT ON \n");
            sb.Append("SET DATEFIRST 1 \n");
            sb.Append(@"if OBJECT_ID('TempDB.dbo.#Temp1') is not null Drop Table dbo.#Temp1
                        if OBJECT_ID('TempDB.dbo.#Temp2') is not null Drop Table dbo.#Temp2
                        if OBJECT_ID('TempDB.dbo.#Temp3') is not null Drop Table dbo.#Temp3
                        if OBJECT_ID('TempDB.dbo.#Temp4') is not null Drop Table dbo.#Temp4
                        if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
                        if OBJECT_ID('TempDB.dbo.#EBPas') is not null Drop Table dbo.#EBPas
                        if OBJECT_ID('TempDB.dbo.#EBPas1') is not null Drop Table dbo.#EBPas1
                        if OBJECT_ID('TempDB.dbo.#AgencyEB') is not null Drop Table dbo.#AgencyEB
                        if OBJECT_ID('TempDB.dbo.#AgencyEB1') is not null Drop Table dbo.#AgencyEB1
                        if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack
                        if OBJECT_ID('TempDB.dbo.#AgencyCP') is not null Drop Table dbo.#AgencyCP
                        if OBJECT_ID('TempDB.dbo.#AvailableFlights') is not null Drop Table dbo.#AvailableFlights
                       ");

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050025168"))
                sb.Append(@"
Declare @EBCalcType smallint
--F.B. 08.03.2016 Last EB işi parametreye bağlandı.
Select @EBCalcType = isnull(EBCalcType,0) From ParamPrice (NOLOCK) Where Market=''

Declare @GetEBLastRecID SmallInt, @GetEBPLastRecID SmallInt

Set @GetEBLastRecID = @EBCalcType
Set @GetEBPLastRecID = @EBCalcType
");
            else
                sb.Append(@"
Declare @GetEBLastRecID SmallInt, @GetEBPLastRecID SmallInt
Set @GetEBLastRecID=0
Set @GetEBPLastRecID=0                        
if @CustomRegID in ('0970801','1210601','0969801','1323201','0855201') 
begin
    Set @GetEBLastRecID=1
    Set @GetEBPLastRecID=1
end 
");

            sb.Append(@"Declare @SaleBegDate DateTime
                        Set @SaleBegDate = GetDate() ");

            sb.Append(@"Select * Into #CatalogPack 
                        From CatalogPack (NOLOCK) CP
                        Where   CP.Ready = 'Y' 
       	                    And CP.EndDate > GetDate()-1
       	                    And CP.WebPub = 'Y' 
       	                    And CP.WebPubDate < GetDate() 
       	                    And dbo.DateOnly(GETDATE()) BetWeen CP.SaleBegDate And CP.SaleEndDate ");
            if (sType == SearchType.TourPackageSearch)
                sb.AppendLine("   And CP.PackType='T' ");
            else
                sb.AppendLine("   And CP.PackType<>'T' ");

            sb.Append(@"Select Distinct RecID 
                        Into #AgencyCP 
                        From 
                        ( 
            	            Select CP.RecID 
            	            From #CatalogPack CP (NOLOCK) 
            	            Where 
            		            Not Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID) or
                                Exists(Select CA.RecID from CatPackAgency CA (NOLOCK)
                                       Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups=CA.AgencyGrp and AG.Agency=@Agency --And AG.GrpType='P'
                                       Where CA.CatPackID=CP.RecID and (AG.Agency=@Agency or CA.Agency=@Agency)) 
            	            --Union All 
            	            --Select CP.RecID 
            	            --From #CatalogPack CP (NOLOCK) 
            	            --Where 
            		        --    Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=@Agency) 
                        ) ACP ");

            sb.Append(@"Select RecID, Name=isnull(dbo.FindLocalName(NameLID, @Market), Name), Village, Town, City, Country 
                        Into #Location 
                        From Location (NOLOCK) ");

            sb.Append(@"Select UKey=Cast(T.CatPackID AS VarChar)+'-'+Cast(PP.RecNo AS VarChar)+'-'+Cast(PA.RecNo AS VarChar)+'-'+Cast(HAP.RecID AS VarChar), T.RefNo, 
            	            ARecNo=PA.RecNo, PA.Hotel, PA.Room, PA.Accom, PA.Board, 
            	            PRecNo=PP.RecNo, PP.CheckIn, PP.Night, PP.HotNight, PA.Adl, PP.ChdG1Age1, PP.ChdG1Age2, PP.ChdG2Age1, PP.ChdG2Age2, 
            	            PP.ChdG3Age1, PP.ChdG3Age2, PP.ChdG4Age1, PP.ChdG4Age2, PP.HotelChdRecNo,                             
            	            CatPackID=CP.RecID, CP.BegDate, CP.EndDate, CP.HolPack, CP.Operator, CP.Market, CP.EBValid, CP.PasEBValid, CP.FlightClass, 
            	            CP.DepCity, CP.ArrCity, CPName=isnull(dbo.FindLocalName(CP.NameLID, @Market), CP.Name), CP.SaleCur, PP.NetCur, PLSpoExists=IsNull(CP.PLSpoExists,0), CPDescription=CP.Description, CP.SaleSPONo, CP.SaleEndDate, 
            	            H.ChdCalcSale, HotLocation=H.Location, HotCat=H.Category,
            	            HAPRecID=HAP.RecID, HAdult=HAP.Adult, HChdAgeG1=HAP.ChdAgeG1, HChdAgeG2=HAP.ChdAgeG2, HChdAgeG3=HAP.ChdAgeG3, HChdAgeG4=HAP.ChdAgeG4,
                            T.CalcSalePrice, T.SaleChdG1, T.SaleChdG2, T.SaleChdG3, T.SaleChdG4, T.HotChdPrice, T.DepFlight, T.RetFlight,
                            PP.SaleAdl, PP.SaleExtBed1, PP.SaleExtBed2,DepServiceType=isnull(CP.DepServiceType,''),RetServiceType=isnull(CP.RetServiceType,''),
                            AccomFullName=
            	                case when HAP.Adult>0 then Str(HAP.Adult,1)+' '+Cast(IsNull(PRM.PLAdlCap,'Adl') as varchar(100)) else '' end+ 
            	                case when HAP.ChdAgeG1>0 and PP.ChdG1Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG1)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG1Age1)+'-'+Convert(VarChar(5),PP.ChdG1Age2)+')' else '' end+
            	                case when HAP.ChdAgeG2>0 and PP.ChdG2Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG2)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG2Age1)+'-'+Convert(VarChar(5),PP.ChdG2Age2)+')' else '' end+
            	                case when HAP.ChdAgeG3>0 and PP.ChdG3Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG3)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG3Age1)+'-'+Convert(VarChar(5),PP.ChdG3Age2)+')' else '' end+
            	                case when HAP.ChdAgeG4>0 and PP.ChdG4Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG4)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG4Age1)+'-'+Convert(VarChar(5),PP.ChdG4Age2)+')' else '' end, ");
            sb.Append(@"    
                            InsurPrice=Cast(0.0 as dec(18,2)),
                            CompExtSerPrice=Cast(0.0 as dec(18,2)), 
                            GrandSalePrice=Cast(0.0 as dec(18,2)),
                            

                       ");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030300003"))
                sb.Append(@"    SpoPer=Cast(0.0 as dec(18,2)),
                                SpoAdlVal=Cast(0.0 as dec(18,2)),
                                SpoExtB1Val=Cast(0.0 as dec(18,2)),
                                SpoExtB2Val=Cast(0.0 as dec(18,2)),
                                SpoChdGrp1Val=Cast(0.0 as dec(18,2)),
                                SpoChdGrp2Val=Cast(0.0 as dec(18,2)),
                                SpoChdGrp3Val=Cast(0.0 as dec(18,2)),
                                SpoChdGrp4Val=Cast(0.0 as dec(18,2)),
                                SpoFlightTot=Cast(0.0 as dec(18,2)),
                                SpoHotelTot=Cast(0.0 as dec(18,2)),
                                PasEBValidFlight = 1, PasEBValidHotel = 1,
                                EBFlightValid = 1, EBHotelValid = 1,
                                FlightValidDate=Cast(73048 AS DateTime),
                                HotelValidDate=Cast(73048 AS DateTime),
                                DepFlightForSpo='          ',
                                TransportType=Cast(0 As smallint),CP.PEBResource,
                                RetArrCity=CP.RetArrCity,
								RetDepCity=CP.RetDepCity,
                                DepFlightMarket=Space(10)
                                
");
            else
                sb.Append(@"    SpoAdlVal=Cast(0.0 as dec(18,2)), 
                                SpoExtB1Val=Cast(0.0 as dec(18,2)),
                                SpoExtB2Val=Cast(0.0 as dec(18,2)),
                                SpoChdGrp1Val=Cast(0.0 as dec(18,2)),
                                SpoChdGrp2Val=Cast(0.0 as dec(18,2)),
                                SpoChdGrp3Val=Cast(0.0 as dec(18,2)),
                                SpoChdGrp4Val=Cast(0.0 as dec(18,2)),
                                SpoFlightTot=Cast(0.0 as dec(18,2)),
                                SpoHotelTot=Cast(0.0 as dec(18,2)),
                                PasEBValidFlight=1, PasEBValidHotel=1,
                                EBFlightValid=1, EBHotelValid=1,
                                FlightValidDate=Cast(73048 AS DateTime),
                                HotelValidDate=Cast(73048 AS DateTime),
                                DepFlightForSpo='          ' 
");

            if (sType == SearchType.TourPackageSearch)
                sb.Append(@"Into #Temp1 
                        From CatPriceA PA (NOLOCK) 
                        Join CatPriceP PP (NOLOCK) on PP.CatPackID=PA.CatPackID and PP.PriceAID=PA.RecNo 
                        Join #CatalogPack CP (NOLOCK) on CP.RecID=PA.CatPAckID
                        join Hotel H (NOLOCK) on H.Code=PA.Hotel 
                        ");
            else
                sb.Append(@"Into #Temp1 
                        From CatPriceA PA (NOLOCK) 
                        Join CatPriceP PP (NOLOCK) on PP.CatPackID=PA.CatPackID and PP.PriceAID=PA.RecNo 
                        Join #CatalogPack CP (NOLOCK) on CP.RecID=PA.CatPAckID
                        join Hotel H (NOLOCK) on H.Code=PA.Hotel 
                        ");

            sb.Append(@"join HotelRoom HR (NOLOCK) on HR.Hotel=PA.Hotel and HR.Code=PA.Room 
                        join HotelAccom HA (NOLOCK) on HA.Hotel=PA.Hotel and HA.Room=PA.Room and HA.Code=PA.Accom 
                        join HotelBoard HB (NOLOCK) on HB.Hotel=PA.Hotel and HB.Code=PA.Board ");

            //if (Convert.ToDecimal(UserData.WebVersion) > Convert.ToDecimal("0008"))
            //                sb.Append(@"join HotelRoom HR (NOLOCK) on HR.Hotel=PA.Hotel and HR.Code=PA.Room and IsNull((Select VisiblePL From HotelRoomMarOpt Where RoomID=HR.RecID and Market=CP.Market),IsNull(HR.VisiblePL,'Y'))='Y'
            //                            join HotelAccom HA (NOLOCK) on HA.Hotel=PA.Hotel and HA.Room=PA.Room and HA.Code=PA.Accom and IsNull(HA.VisiblePL,'Y')='Y' IsNull((Select VisiblePL From HotelAccomMarOpt Where AccomID=HA.RecID and Market=CP.Market),IsNull(HA.VisiblePL,'Y')='Y'
            //                            join HotelBoard HB (NOLOCK) on HB.Hotel=PA.Hotel and HB.Code=PA.Board and IsNull((Select VisiblePL From HotelBoardMarOpt Where BoardID=HB.RecID and Market=CP.Market),IsNull(HB.VisiblePL,'Y')='Y' ");
            //            else
            //                sb.Append(@"join HotelRoom HR (NOLOCK) on HR.Hotel=PA.Hotel and HR.Code=PA.Room and IsNull(HR.VisiblePL,'Y')='Y' 
            //                            join HotelAccom HA (NOLOCK) on HA.Hotel=PA.Hotel and HA.Room=PA.Room and HA.Code=PA.Accom and IsNull(HA.VisiblePL,'Y')='Y' 
            //                            join HotelBoard HB (NOLOCK) on HB.Hotel=PA.Hotel and HB.Code=PA.Board and IsNull(HB.VisiblePL,'Y')='Y' ");

            sb.Append(@"join HotelAccomPax HAP  (NOLOCK) on HAP.Hotel=PA.Hotel and HAP.Room=PA.Room and HAP.Accom=PA.Accom and IsNull(HAP.VisiblePL,'Y')='Y'
                        Join #tmpCalcTable T On T.CatPackID = CP.RecID And T.ARecNo = PA.RecNo And T.PRecNo = PP.RecNo And T.HAPRecNo = HAP.RecID
                        join HotelCat HC (NOLOCK) on HC.Code=H.Category 
                        Left Join ParamPrice PRM (NOLOCK) on PRM.Market=CP.Market ");

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200000"))
            {
                if (extAllotControl)
                    sb.Append(@"Select P.*,             	              
            	              0 AS FreeAllot, EBPerc = Cast(0 AS decimal(18, 5)), EBValidDate = Cast(73048 AS DateTime), 0 AS AgencyEB 
                            Into #Temp2 
                            From #Temp1 P ");
                else
                    sb.Append(@"Select P.*,             	              
            	              0 AS FreeAllot, EBPerc = Cast(0 AS decimal(18, 5)), EBValidDate = Cast(73048 AS DateTime), 0 AS AgencyEB 
                            Into #Temp2 
                            From #Temp1 P ");
            }
            else
                sb.Append(@"Select P.*,             	         
            	              0 AS FreeAllot, EBPerc = Cast(0 AS decimal(18, 5)), EBValidDate = Cast(73048 AS DateTime), 0 AS AgencyEB 
                            Into #Temp2 
                            From #Temp1 P ");

            //object showAgencyEB = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowAgencyEB");
            //if (showAgencyEB != null && (bool)showAgencyEB)
            //{
            sb.Append(@"--Eğer AgencyEB Yazılacak İse 
                            Select Distinct Hotel, EBValid, HolPack, CheckIn, Night, Operator, Market 
                            Into #AgencyEB1 
                            From #Temp2 

                            Select *, AgencyEB=Case When EBValid='Y' Then ( 
                			            Select Top 1 EBPer  From AgencyComEB EB (NOLOCK) 
                			            Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups=EB.AgencyGrp and AG.GrpType='P' 
                			            Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups=EB.HolPackGrp and PG.GrpType='P' 
                			            Where	OwnerOperator=x.Operator 
                				            And OwnerMarket=x.Market 
                				            And Operator=@Operator 
                				            And Market=@Market 
                				            And ((EB.Agency=(Select Case WHEN isnull(MainOffice, '')='' Then Code Else MainOffice End From Agency Where Code=@Agency) 
                				               Or AG.Agency=(Select Case WHEN isnull(MainOffice, '')='' Then Code Else MainOffice End From Agency Where Code=@Agency)) Or (EB.Agency='' And AG.Agency is Null)) 
                				            And ((EB.HolPackGrp='' And (EB.HolPack=x.HolPack Or EB.HolPack='')) Or PG.HolPack=x.HolPack) 
                				            And dbo.DateOnly(GETDATE()) Between SaleBegDate And SaleEndDate 
                				            And X.CheckIn Between ResBegDate And ResEndDate 
                			            Order By EB.Agency Desc, AgencyGrp Desc, EB.HolPack Desc, HolPackGrp Desc, SaleBegDate Desc, ResBegDate Desc 
                		            ) Else 0 End 
                            INTO #AgencyEB 
                            From #AgencyEB1 X ");

            sb.Append(@"Update x  
                            Set AgencyEB=Case When y.AgencyEB is null then 0 Else y.AgencyEB End  
                            From #Temp2 x ,#AgencyEB y  
                            where	x.Hotel=y.Hotel  
                	            And x.EBValid=y.EBValid  
                	            And x.Holpack=y.Holpack  
                	            And x.CheckIn=y.CheckIn  
                	            And x.Night=y.Night ");
            //}

            sb.Append(@"Select X.* 
                        Into #Temp3 
                        From ( 
                            Select R.*, 
                                PEB_Amount=Cast(0.0 as dec(18,2)),
                                PasEBPer=IsNull(PEB.PasEBPer,0),
                                PEB_AdlVal=IsNull(PEB.AdlVal,0),
                                PEB_EBedVal=IsNull(PEB.EBedVal,0),
                                PEB_ChdGrp1Val=IsNull(PEB.ChdGrp1Val,0),
                                PEB_ChdGrp2Val=IsNull(PEB.ChdGrp2Val,0),
                                PEB_ChdGrp3Val=IsNull(PEB.ChdGrp3Val,0),
                                PEB_ChdGrp4Val=IsNull(PEB.ChdGrp4Val,0),

                                HKB_Exists=Case When HKB.PriceType is null then 0 else 1 end,  
                                HKB_PriceType=HKB.PriceType,  
                                HKB_CalcType=HKB.CalcType, 
                                HKB_AdlPrice=HKB.AdlPrice, 
                                HKB_SupPer=HKB.SupPer, 
                                HKB_EBOpt=HKB.EBOpt, 
                                HKB_Cur=HKB.Cur,  
                                HKB_Amount=Cast(0 as dec(18,2)), 
                                SPWithEB=R.CalcSalePrice,

                                PasEBPer2=Cast(0.0 as dec(5,2)),
                                PEB_RecID=PEB.EBRecID,PEB_ResBegDate=PEB.ResBegDate,PEB_CinAccom=PEB.CinAccom,
                                PEB_ResEndDate=case when PEB.CinAccom=0 then R.CheckIn+R.Night-1
                                                    when PEB.ResEndDate<R.CheckIn+R.Night-1 then PEB.ResEndDate 
                                                    else R.CheckIn+R.Night-1 
                                               end,

                                SpoDesc=Cast('' as varChar(255)),
                                EBAmount=Case When R.PasEBValid='Y' And (PEB.PasEBPer is not null or PEB.AdlVal is not null) Then
					                                dbo.ufn_GetEBAmountPL(R.CatPackID,R.ARecNo,R.PRecNo,R.HapRecID,R.Adl,R.ChdG1Age1,R.ChdG1Age2,R.ChdG2Age1,R.ChdG2Age2,R.ChdG3Age1,R.ChdG3Age2,R.ChdG4Age1,R.ChdG4Age2,
										                                  R.HotelChdRecNo,R.ChdCalcSale,R.HAdult,R.HChdAgeG1,R.HChdAgeG2,R.HChdAgeG3,R.HChdAgeG4,0,@CustomRegID,R.HotChdPrice)
				                              Else 0 End,
                                PEB_SaleEndDate=PEB.SaleEndDate

                            From #Temp2 R 
                            -- 2010-02-11 KickBack 
                            OUTER APPLY  
                            ( 
                                  Select Top 1 PriceType,CalcType,AdlPrice,SupPer,EBOpt,Cur 
                                  From HotelFeed (NOLOCK) HF 
                                  Where Hotel=R.Hotel and Status=1 and ForSale=1 and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate 
                                  and R.CheckIn between BegDate and EndDate 
                                  and R.Night between IsNull(MinDay,0) and IsNull(MaxDay,999) 
                                  and SubString(AccomDays,DATEPART(dw,R.CheckIn),1)=1 
                                  and (Room='' or Room=R.Room) 
                                  and (Board='' or Board=R.Board) 
                                  Order by Room Desc, Board Desc,CrtDate Desc 
                            ) HKB  
                            OUTER APPLY
                            ( 
                       ");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040014038"))
                sb.Append(@"    Select Top 1 PasEBPer,AdlVal,EBedVal=IsNull(EBedVal,AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val,
                                    EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,EBP.SaleEndDate,
									LastEBRecID = EB.RecID * @GetEBLastRecID, LastEBPRecID = EBP.RecID * @GetEBPLastRecID	                            
	                            From PasEB EB (NOLOCK)
	                            Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID
	                            Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=R.Hotel and dbo.IsEmpty(EBH.Room,R.Room)=R.Room and dbo.IsEmpty(EBH.Board,R.Board)=R.Board
	                            Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P'
	                            Where R.PasEBValid='Y' and OwnerOperator=R.Operator and OwnerMarket=R.Market and Operator=@Operator and Market=@Market and R.PasEBValid='Y' and EB.SaleType=0		                              
		                              and ( (EB.HolPack=R.HolPack or PG.HolPack=R.HolPack) or (EB.HolPack='' and PG.HolPack is Null) )
		                              and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate and R.CheckIn between ResBegDate and ResEndDate
		                              and (R.Night between IsNull(MinDay,0) and IsNull(MaxDay,999))
		                              and SubString(DoW,DATEPART(dw,R.CheckIn),1)=1
                                      and DateDIFF(day,dbo.DateOnly(GetDate()),R.CheckIn) between IsNull(EBP.RemFromDay,0) and IsNull(EBP.RemToDay,999)
	                            Order by LastEBRecID desc,EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc,EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc,LastEBPRecID Desc,SaleBegDate Desc,ResBegDate Desc, EBP.RecID Desc

                            ) PEB
                            -- 2010-02-11 KickBack 
                        ) X 
                        ");
            else
                if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0034, VersionControl.Equality.gt))
                sb.Append(@"Select Top 1 PasEBPer,AdlVal,EBedVal=IsNull(EBedVal,AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val,
                                    EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,EBP.SaleEndDate,
									LastEBRecID = EB.RecID * @GetEBLastRecID, LastEBPRecID = EBP.RecID * @GetEBPLastRecID	                            
	                            From PasEB EB (NOLOCK)
	                            Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID
	                            Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=R.Hotel and (EBH.Room='' or EBH.Room=R.Room) and (EBH.Board='' or EBH.Board=R.Board)
	                            Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P'
	                            Where R.PasEBValid='Y' and OwnerOperator=R.Operator and OwnerMarket=R.Market and Operator=@Operator and Market=@Market and R.PasEBValid='Y' and EB.SaleType=0		                              
		                              and ( (EB.HolPack=R.HolPack or PG.HolPack=R.HolPack) or (EB.HolPack='' and PG.HolPack is Null) )
		                              and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate and R.CheckIn between ResBegDate and ResEndDate
		                              and (R.Night between IsNull(MinDay,0) and IsNull(MaxDay,999))
		                              and SubString(DoW,DATEPART(dw,R.CheckIn),1)=1
	                            Order by LastEBRecID desc,EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc,EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc,LastEBPRecID Desc,SaleBegDate Desc,ResBegDate Desc, EBP.RecID Desc

                            ) PEB
                            -- 2010-02-11 KickBack 
                        ) X 
                        ");
            else
                sb.Append(@"Select Top 1 PasEBPer,AdlVal,EBedVal=IsNull(EBedVal,AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val,
                                    EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,EBP.SaleEndDate,
									LastEBRecID = EB.RecID * @GetEBLastRecID, LastEBPRecID = EBP.RecID * @GetEBPLastRecID	                            
	                            From PasEB EB (NOLOCK)
	                            Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID
	                            Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=R.Hotel and (EBH.Room='' or EBH.Room=R.Room)
	                            Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P'
	                            Where R.PasEBValid='Y' and OwnerOperator=R.Operator and OwnerMarket=R.Market and Operator=@Operator and Market=@Market and R.PasEBValid='Y'
		                              --and (not Exists(Select * from PasEBHot Where PasEBID=EB.RecID) or (Exists(Select * from PasEBHot Where PasEBHot.PasEBID=EB.RecID and PasEBHot.Hotel=V.Hotel and (EBH.Room='' or EBH.Room=V.Room))) )
		                              and ( (EB.HolPack=R.HolPack or PG.HolPack=R.HolPack) or (EB.HolPack='' and PG.HolPack is Null) )
		                              and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate and R.CheckIn between ResBegDate and ResEndDate
		                              and (R.Night between IsNull(MinDay,0) and IsNull(MaxDay,999))
		                              and SubString(DoW,DATEPART(dw,R.CheckIn),1)=1
	                            Order by LastEBRecID desc,EBH.Hotel Desc,EBH.Room Desc,EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc,LastEBPRecID Desc,SaleBegDate Desc,ResBegDate Desc, EBP.RecID Desc

                            ) PEB
                            -- 2010-02-11 KickBack 
                        ) X 
                        ");

            sb.Append(@"
                        if (Select Count(*) from #Temp3 where PlSpoExists=1) > 0
                        Begin                          
                        declare @totalSeat int
                        Select @totalSeat=sum(HAdult)+sum(HChdAgeG1)+sum(HChdAgeG2)+sum(HChdAgeG3)+sum(HChdAgeG4) From #Temp2 where RefNo=(select top 1 RefNo from #tmpCalcTable) group by Hotel
                       ");

            sb.Append(@"
                        Declare @DepFlight varchar(10),
	                           @RetFlight varchar(10),
	                           @DepClass varchar(5),
	                           @RetClass varchar(5),
	                           @DepFree int,
	                           @RetFree int,
	                           @HolPack varChar(10),
	                           @CheckIn dateTime, 
	                           @Night int,
	                           @HAdult int

                        Select Distinct X.HolPack,X.CheckIn,X.Night,X.HAdult
                        Into #AvailableFlights 
                        From #Temp2 X

                        Declare RCursor CURSOR LOCAL FAST_FORWARD FOR
                           Select HolPack, CheckIn, Night, HAdult From #AvailableFlights
                           Open RCursor
                           Fetch Next FROM RCursor INTO @HolPack, @CheckIn, @Night, @HAdult
                           While @@FETCH_STATUS = 0
                           begin  
			 
                           Select @RetFlight = '', @DepFlight = '', @DepFree = 0, @RetFree = 0
                           if isnull(@DepFlight, '') = '' or isnull(@RetFlight, '') = ''
	                           exec dbo.usp_GetAvailableFlightInPackage @HolPack, @CheckIn, @Night,@totalSeat , @DepFlight output, @RetFlight output, @DepClass output, @RetClass output, @DepFree output, @RetFree output

                           if isnull(@RetFlight,'') = '' or @RetFlight = '%'
	                           Select Top 1 @RetFlight = NextFlight From FlightDay (nolock) where FlightNo = @DepFlight and FlyDate = @CheckIn

                           update #Temp3 
                           Set DepFlight=@DepFlight,RetFlight=@RetFlight,FlightClass=@DepClass,
                            DepFlightForSPO = @DepFlight    
                           --Case When isnull(@DepClass,'')='' Then FlightClass Else @DepClass End
                           where HolPack=@HolPack and CheckIn=@CheckIn and Night=@Night and HAdult=@HAdult 
						     and isnull(@DepClass,'')<>''

                           Fetch Next FROM RCursor INTO @HolPack, @CheckIn, @Night, @HAdult
                        end
                        close RCursor
                        deallocate RCursor                        
        

-- Declare @CatPackId int,
--         @DepServiceType varChar(10),
--		 @DepService varChar(10),
--		 @RetServiceType varChar(10),
--		 @RetService varChar(10)
--
-- Declare SnCursor CURSOR LOCAL FAST_FORWARD FOR 
-- 
-- Select Distinct X.CatPackID,X.CheckIn,X.Night,X.DepServiceType,X.RetServiceType
-- From #Temp3 X 
-- Where X.DepFlight='          '
--   And X.RetFlight='          '
--
-- Open SnCursor 
-- Fetch Next FROM SnCursor INTO @CatPackId, @CheckIn, @Night, @DepServiceType, @RetServiceType
-- While @@FETCH_STATUS = 0 
-- Begin   
--   if @DepServiceType<>'FLIGHT' OR @DepServiceType<>@RetServiceType
--   Begin
--     
--     Select @DepService=ServiceItem From CatPackPlanSer (NOLOCK)
--	 Where StepNo=1
--	 And CatPackID=@CatPackId
--
--	 Select @RetService=ServiceItem From CatPackPlanSer (NOLOCK)
--	 Where StepNo=(Select Top 1 StepNo From CatPackPlanSer Where CatPackID=@CatPackId Order By StepNo Desc)
--	 And CatPackID=@CatPackId
--
--	 Update #PriceList
--	 Set DepFlight=@DepService,
--	     RetFlight=@RetService
--      Where CatPackID=@CatPackId And CheckIn=@CheckIn And Night=@Night And DepServiceType=@DepServiceType And RetServiceType=@RetServiceType
--
--   End
--   Fetch Next FROM SnCursor INTO @CatPackId, @CheckIn, @Night, @DepServiceType, @RetServiceType
-- End
-- Close SnCursor 
-- Deallocate SnCursor 

                       ");
            //            sb.Append(@"
            //                        if (Select Count(*) from #Temp3 where PlSpoExists=1) > 0
            //                        
            //                          Update #Temp3 
            //                          Set DepFlightForSpo=X.Service
            //                          From
            //                          (
            //                               Select S.CatPackID,X.CatPricePID,Service=S.Service,DA,S.SClass From CatService S (NOLOCK)
            //                               Join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo
            //                               Where S.ServiceType='FLIGHT' and DA=0
            //                          ) X 
            //                          Where X.CatPackID=#Temp3.CatPackID and X.CatPricePID=#Temp3.PRecNo and LTrim(DepFlightForSpo)='' and #Temp3.PlSpoExists=1     
            //                    ");
            if (string.Equals(UserData.CustomRegID, Common.crID_Detur))
            {
                sb.Append(@"
    Update #Temp3 Set DepFlight=DepFlightForSpo Where DepFlight=''
    --Ucağın Marketini bulma
    Update #Temp3 Set DepFlightMarket=O.Market --burası 2.nokta
    From FlightDay FD (NOLOCK)
    Join Operator O (NOLOCK) on O.Code=FD.FlgOwner
    Where FlightNo = DepFlightForSpo and FlyDate = CheckIn

                ");
            }

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030300003"))
            {
                sb.Append(@"Update #Temp3
                            Set
                              SPOPer=Y.SPOPer,
	                          SpoAdlVal=Y.AdlVal, 
                              SpoExtB1Val=Y.ExtB1Val, 
                              SpoExtB2Val=Y.ExtB2Val,
	                          SpoChdGrp1Val=case when X.SaleChdG1>0 then Y.ChdGrp1Val else 0 end,
				              SpoChdGrp2Val=case when X.SaleChdG2>0 then Y.ChdGrp2Val else 0 end, 
				              SpoChdGrp3Val=case when X.SaleChdG3>0 then Y.ChdGrp3Val else 0 end, 
				              SpoChdGrp4Val=case when X.SaleChdG4>0 then Y.ChdGrp4Val else 0 end,
	                          SpoFlightTot=Y.FlightTot, 
                              SpoHotelTot=Y.HotelTot,
	                          PasEBValidFlight=Y.PEBValidForFlight,
	                          PasEBValidHotel=Y.PEBValidForHotel,
                              FlightValidDate=Case When Y.FlightValidDate is null Then Cast(73048 AS DateTime) Else Y.FlightValidDate End,
                              HotelValidDate=Case When Y.HotelValidDate is null Then Cast(73048 AS DateTime) Else Y.HotelValidDate End,
	                          EBFlightValid=isnull((Select Top 1 IsNull(PayPasEB,0) From CatPackPlan (NOLOCK) where CatPackID = X.CatPackID and Service = 'FLIGHT'),0),
	                          EBHotelValid=isnull((Select Top 1 IsNull(PayPasEB,0) From CatPackPlan (NOLOCK) where CatPackID = X.CatPackID and Service = 'HOTEL'),0),
                            ");
                List<dbInformationShema> plSpoValsTable = VersionControl.getTableFunctionShema("PLSpoVals", ref errorMsg);
                if (plSpoValsTable.Find(f => f.Name == "Descriptions") != null)
                {
                    sb.Append(@"  SpoDesc =isnull(Y.Descriptions,'') ");
                }
                else
                {
                    sb.Append(@"  SpoDesc =isnull(SD.Description,'') ");
                }

                sb.Append(@"
                          from #Temp3 X
                          outer apply
                          (
                            select * from dbo.PLSpoVals(0,X.CatPackID,@SaleBegDate,X.CheckIn,X.Night,X.Hotel,X.Room,X.Accom,X.Board,X.Adl,X.HAdult,X.HChdAgeG1,X.HChdAgeG2,X.HChdAgeG3,X.HChdAgeG4,X.HotLocation,X.HotCat,0,X.DepFlight,@Agency,@CustomRegID)
                          ) Y
                          outer apply
                          (
                            Select CPS.Description From CatPriceSPO CPS (NOLOCK) 
                            Left Join CatPriceSPOSer CPSS (NOLOCK) ON CPS.RecID=CPSS.SpoID 
                            Left Join CatPriceSPOVal CPSV (NOLOCK) ON CPS.RecID=CPSV.SpoID And CPSS.ValID=CPSV.RecID
                            Where  CPS.RecID=Y.FlgSpoID
                              And (isnull(CPS.PriceListNo,0)=0 Or CPS.PriceListNo=X.CatPackID)
                              And (isnull(CPS.HolPack,'')='' Or CPS.HolPack=X.HolPack)
                              And (isnull(CPSS.Service,'')='' Or (CPSS.Service=X.Hotel Or CPSS.ST=1))
                              And X.CheckIn between isnull(CPSV.CheckIn1,0) And isnull(CPSV.CheckIn2,GetDate()+999)
                              And X.HotNight between isnull(CPSV.MinNight,0) And isnull(CPSV.MaxNight, 999)
                              And (isnull(CPSV.Room,'')='' Or X.Room=CPSV.Room)
                              And (isnull(CPSV.Accom,'')='' Or X.Accom=CPSV.Accom)
                              And (isnull(CPSV.Board,'')='' Or X.Board=CPSV.Board)
                          ) SD
                          Where UKey = X.UKey and PlSpoExists=1

                          --if @CustomRegID = '0970301'
                          --    Update #Temp3 
                          --      Set PlSpoExists=0
                          --    Where IsNull(SpoPer,0)=0 and IsNull(SpoAdlVal,0)=0 and IsNull(SpoExtB1Val,0)=0 and IsNull(SpoExtB2Val,0)=0 and IsNull(SpoChdGrp1Val,0)=0 and 
                          --          IsNull(SpoChdGrp2Val,0)=0 and IsNull(SpoChdGrp3Val,0)=0  and IsNull(SpoChdGrp4Val,0)=0                          
                          
                         Update #Temp3
                          Set SaleAdl=SaleAdl+IsNull(SpoAdlVal,0),
                              SaleExtBed1=Case when SaleExtBed1<>0 or IsNull(SpoExtB1Val,0) > 0 then SaleExtBed1+IsNull(SpoExtB1Val,0) else 0 end,
                              SaleExtBed2=Case when SaleExtBed2<>0 or IsNull(SpoExtB2Val,0) > 0 then SaleExtBed2+IsNull(SpoExtB2Val,0) else 0 end,
                              SaleChdG1=Case when SaleChdG1<>0 or IsNull(SpoChdGrp1Val,0) > 0 then SaleChdG1+IsNull(SpoChdGrp1Val,0) else 0 end,
                              SaleChdG2=Case when SaleChdG2<>0 or IsNull(SpoChdGrp2Val,0) > 0 then SaleChdG2+IsNull(SpoChdGrp2Val,0) else 0 end,
                              SaleChdG3=Case when SaleChdG3<>0 or IsNull(SpoChdGrp3Val,0) > 0 then SaleChdG3+IsNull(SpoChdGrp3Val,0) else 0 end,
                              SaleChdG4=Case when SaleChdG4<>0 or IsNull(SpoChdGrp4Val,0) > 0 then SaleChdG4+IsNull(SpoChdGrp4Val,0) else 0 end
                          Where PlSpoExists=1 and (isnull(SpoPer, 0)=0 or SpoHotelTot<>0)

                         Update #Temp3
                          Set SaleAdl=SaleAdl * (1 + IsNull(SpoPer,0)/100),
                              SaleExtBed1=Case when SaleExtBed1<>0 then SaleExtBed1 * (1 + IsNull(SpoPer,0)/100) else 0 end,
                              SaleExtBed2=Case when SaleExtBed2<>0 then SaleExtBed2 * (1 + IsNull(SpoPer,0)/100) else 0 end,
                              SaleChdG1=Case when SaleChdG1<>0 then SaleChdG1 * (1 + IsNull(SpoPer,0)/100) else 0 end,
                              SaleChdG2=Case when SaleChdG2<>0 then SaleChdG2 * (1 + IsNull(SpoPer,0)/100) else 0 end,
                              SaleChdG3=Case when SaleChdG3<>0 then SaleChdG3 * (1 + IsNull(SpoPer,0)/100) else 0 end,
                              SaleChdG4=Case when SaleChdG4<>0 then SaleChdG4 * (1 + IsNull(SpoPer,0)/100) else 0 end,
                              SpoFlightTot = case 
                                               when SpoPer = 0 then SpoFlightTot
                                               else (Case When HAdult>Adl then Adl else HAdult end * SaleAdl * IsNull(SpoPer,0)/100+
                                                     Case when HAdult-Adl>=1 then SaleExtBed1 * IsNull(SpoPer,0)/100 else 0 end+
                                                     Case when HAdult-Adl>=2 then SaleExtBed2 * IsNull(SpoPer,0)/100 else 0 end+
                                                     Case when HChdAgeG1>0 then SaleChdG1 * HChdAgeG1 * IsNull(SpoPer,0)/100 else 0 end+
                                                     Case when HChdAgeG2>0 then SaleChdG2 * HChdAgeG2 * IsNull(SpoPer,0)/100 else 0 end+
                                                     Case when HChdAgeG3>0 then SaleChdG3 * HChdAgeG3 * IsNull(SpoPer,0)/100 else 0 end+
                                                     Case when HChdAgeG4>0 then SaleChdG4 * HChdAgeG4 * IsNull(SpoPer,0)/100 else 0 end)
                                             end
                          Where PlSpoExists=1 and isnull(SpoPer, 0) <> 0

                          
                          
                          Update #Temp3
                          Set
                            SPWithEB=--SaleAdl*Adl+
                               (Case When HAdult>Adl then Adl else HAdult end * SaleAdl)+
                               Case when HAdult-Adl>=1 then SaleExtBed1 else 0 end+
                               Case when HAdult-Adl>=2 then SaleExtBed2 else 0 end+
                               Case when HChdAgeG1>0 then SaleChdG1 * HChdAgeG1 else 0 end+
                               Case when HChdAgeG2>0 then SaleChdG2 * HChdAgeG2 else 0 end+
                               Case when HChdAgeG3>0 then SaleChdG3 * HChdAgeG3 else 0 end+
                               Case when HChdAgeG4>0 then SaleChdG4 * HChdAgeG4 else 0 end,
                              
                              PlSpoExists=Case When IsNull(SpoPer,0)<>0 or
                                                    IsNull(SpoAdlVal,0)<>0 or
                                                    (HAdult-Adl>=1 and IsNull(SpoExtB1Val,0)<>0 ) or
                                                    (HAdult-Adl>=2 and IsNull(SpoExtB2Val,0)<>0 ) or
                                                    (HChdAgeG1>0 and IsNull(SpoChdGrp1Val,0)<>0 ) or
                                                    (HChdAgeG2>0 and IsNull(SpoChdGrp2Val,0)<>0 ) or
                                                    (HChdAgeG3>0 and IsNull(SpoChdGrp3Val,0)<>0 ) or
                                                    (HChdAgeG4>0 and IsNull(SpoChdGrp4Val,0)<>0 ) then 1 else 0 end,                            
                             
                              EBAmount=Case 
											When PEBResource = 1 then 
											  case 
												when SpoPer = 0 then (EBAmount+(SpoHotelTot * isnull(EBHotelValid,0))) * PasEBValidHotel * PasEBValidFlight
												  			    else ((EBAmount + (SpoHotelTot * isnull(EBHotelValid,0)))) * PasEBValidHotel * PasEBValidFlight
												end
											Else
											  case 
												when SpoPer = 0 then (EBAmount + (SpoFlightTot * isnull(EBFlightValid,0)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel)
												                else ((EBAmount + (EBAmount * SpoPer * isnull(EBFlightValid,0) / 100)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel)
												end
											End,
                              PasEBPer = Case 
										   When PEBResource = 1 Then PasEBPer * PasEBValidHotel
																Else PasEBPer * PasEBValidFlight * PasEBValidHotel
										   End,
                              PEB_AdlVal=Case 
										   When PEBResource = 1 Then PEB_AdlVal * PasEBValidHotel
																Else PEB_AdlVal * PasEBValidFlight * PasEBValidHotel
										   End                          
                              From #Temp3 x
                              Where PlSpoExists=1
                        End ");
            }
            else
                sb.Append(@"Update #Temp3
                            Set
	                          SpoAdlVal=Y.AdlVal, 
                              SpoExtB1Val=Y.ExtB1Val, 
                              SpoExtB2Val=Y.ExtB2Val,
                              SpoChdGrp1Val=case when X.SaleChdG1>0 then Y.ChdGrp1Val else 0 end,
				              SpoChdGrp2Val=case when X.SaleChdG2>0 then Y.ChdGrp2Val else 0 end, 
				              SpoChdGrp3Val=case when X.SaleChdG3>0 then Y.ChdGrp3Val else 0 end, 
				              SpoChdGrp4Val=case when X.SaleChdG4>0 then Y.ChdGrp4Val else 0 end,
                              SpoFlightTot=Y.FlightTot, 
                              SpoHotelTot=Y.HotelTot,
	                          PasEBValidFlight=Y.PEBValidForFlight,
	                          PasEBValidHotel=Y.PEBValidForHotel,
                              FlightValidDate=Case When Y.FlightValidDate is null Then Cast(73048 AS DateTime) Else Y.FlightValidDate End,
                              HotelValidDate=Case When Y.HotelValidDate is null Then Cast(73048 AS DateTime) Else Y.HotelValidDate End,
	                          EBFlightValid=isnull((Select Top 1 IsNull(PayPasEB,0) From CatPackPlan where CatPackID = X.CatPackID and Service = 'FLIGHT'),0),
	                          EBHotelValid=isnull((Select Top 1 IsNull(PayPasEB,0) From CatPackPlan where CatPackID = X.CatPackID and Service = 'HOTEL'),0)
                          from #Temp3 X
                          outer apply
                          (
                            select * from dbo.PLSpoVals(0,X.CatPackID,@SaleBegDate,X.CheckIn,X.Night,X.Hotel,X.Room,X.Accom,X.Board,X.Adl,X.HAdult,X.HChdAgeG1,X.HChdAgeG2,X.HChdAgeG3,X.HChdAgeG4,X.HotLocation,X.HotCat,0,X.DepFlightForSpo,@Agency,@CustomRegID)
                          ) Y
                          Where UKey = X.UKey and PlSpoExists=1


                          Update #Temp3 Set PlSpoExists=0
                          Where IsNull(SpoAdlVal,0)=0 and IsNull(SpoExtB1Val,0)=0 and IsNull(SpoExtB2Val,0)=0 and IsNull(SpoChdGrp1Val,0)=0 and 
                                IsNull(SpoChdGrp2Val,0)=0 and IsNull(SpoChdGrp3Val,0)=0  and IsNull(SpoChdGrp4Val,0)=0 and #Temp3.PlSpoExists=1
                          
                          Update #Temp3
                          Set SaleAdl=SaleAdl+IsNull(SpoAdlVal,0),
                              SaleExtBed1=Case when SaleExtBed1<>0 or IsNull(SpoExtB1Val,0)>0 then SaleExtBed1+IsNull(SpoExtB1Val,0) else 0 end,
                              SaleExtBed2=Case when SaleExtBed2<>0 or IsNull(SpoExtB2Val,0)>0 then SaleExtBed2+IsNull(SpoExtB2Val,0) else 0 end,
                              SaleChdG1=Case when SaleChdG1<>0 or IsNull(SpoChdGrp1Val,0)>0 then SaleChdG1+IsNull(SpoChdGrp1Val,0) else 0 end,
                              SaleChdG2=Case when SaleChdG2<>0 or IsNull(SpoChdGrp2Val,0)>0 then SaleChdG2+IsNull(SpoChdGrp2Val,0) else 0 end,
                              SaleChdG3=Case when SaleChdG3<>0 or IsNull(SpoChdGrp3Val,0)>0 then SaleChdG3+IsNull(SpoChdGrp3Val,0) else 0 end,
                              SaleChdG4=Case when SaleChdG4<>0 or IsNull(SpoChdGrp4Val,0)>0 then SaleChdG4+IsNull(SpoChdGrp4Val,0) else 0 end
                          Where PlSpoExists=1
                          
                          Update #Temp3
                          Set
                           SPWithEB=SaleAdl*Adl+
	                           Case when HAdult-Adl>=1 then SaleExtBed1 else 0 end+
	                           Case when HAdult-Adl>=2 then SaleExtBed2 else 0 end+
	                           Case when HChdAgeG1>0 then SaleChdG1*HChdAgeG1 else 0 end+
	                           Case when HChdAgeG2>0 then SaleChdG2*HChdAgeG2 else 0 end+
	                           Case when HChdAgeG3>0 then SaleChdG3*HChdAgeG3 else 0 end+
	                           Case when HChdAgeG4>0 then SaleChdG4*HChdAgeG4 else 0 end, 
                           PlSpoExists=Case When IsNull(SpoAdlVal,0)<>0 or
                                                (HAdult-Adl>=1 and IsNull(SpoExtB1Val,0)<>0 ) or
                                                (HAdult-Adl>=2 and IsNull(SpoExtB2Val,0)<>0 ) or
                                                (HChdAgeG1>0 and IsNull(SpoChdGrp1Val,0)<>0 ) or
                                                (HChdAgeG2>0 and IsNull(SpoChdGrp2Val,0)<>0 ) or
                                                (HChdAgeG3>0 and IsNull(SpoChdGrp3Val,0)<>0 ) or
                                                (HChdAgeG4>0 and IsNull(SpoChdGrp4Val,0)<>0 ) then 1 else 0 end,

                              EBAmount = (EBAmount + (SpoFlightTot * isnull(EBFlightValid,0)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel),
                              PasEBPer = PasEBPer * PasEBValidFlight * PasEBValidHotel,
                              PEB_AdlVal=PEB_AdlVal * PasEBValidFlight * PasEBValidHotel

                          From #Temp3 x
                          Where PlSpoExists=1
                        End ");

            sb.Append(@"Update #Temp3 Set HKB_Exists=0 Where HKB_Exists=1 and (PasEBPer>0 or PEB_AdlVal<>0) and HKB_EBOpt=1

                        Update #Temp3 Set HKB_Amount=dbo.Calc_HotKickBack(0,HKB_PriceType,HKB_CalcType,HKB_AdlPrice*dbo.ufn_Exchange(dbo.DateOnly(@SaleBegDate),HKB_Cur,SaleCur,1,1,Market),
                                                                        HKB_SupPer,HKB_Cur,Night,Adl,
                                                                        Case When HAdult-Adl>=1 then 1 else 0 end,Case When HAdult-Adl>=2 then 1 else 0 end,CatPackID,PRecNo,HotChdPrice,SaleCur,'', SpoHotelTot)
                        Where HKB_Exists=1

                        Update #Temp3 Set SPWithEB=SPWithEB-HKB_Amount, EBAmount = case when IsNull(EBAmount,0) > 0 then EBAmount - HKB_Amount else 0 end Where HKB_Exists=1                        

                        --PasEbPer2 - 2.period ********************
                        Update #Temp3
                        Set PasEbPer2=IsNUll(PEB2.PasEBPer,0)
                        From #Temp3 V
                        OUTER APPLY
                        ( ");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040014038"))
                sb.Append(@" Select Top 1 PasEBPer=IsNull(PasEBPer,0),AdlVal,EBedVal=IsNull(EBedVal,AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val,EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,
		                           LastEBRecID=EB.RecID*@GetEBLastRecID,LastEBPRecID=EBP.RecID*@GetEBPLastRecID
	                        From PasEB EB (NOLOCK)
	                        Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID
	                        Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=V.Hotel and (EBH.Room='' or EBH.Room=V.Room)and (EBH.Board='' or EBH.Board=V.Board)
	                        Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P'
	                        Where EBP.RecID<>V.PEB_RecID and V.PasEBPer<>0 and
	                        V.PasEBValid='Y' and OwnerOperator=V.Operator and OwnerMarket=V.Market and Operator=@Operator and Market=@Market and V.PasEBValid='Y' and EB.SaleType=0 and 
		                         ( (EB.HolPack=V.HolPack or PG.HolPack=V.HolPack) or (EB.HolPack='' and PG.HolPack is Null) )
		                          and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate 
		                          and V.PEB_ResEndDate+1 between ResBegDate and ResEndDate and CinAccom=1
		                          and (V.Night between IsNull(MinDay,0) and IsNull(MaxDay,999))
		                          and SubString(DoW,DATEPART(dw,V.CheckIn),1)=1	        
                                  and DateDIFF(day,dbo.DateOnly(GetDate()),V.CheckIn) between IsNull(EBP.RemFromDay,0) and IsNull(EBP.RemToDay,999)
	                        Order by LastEBRecID desc, EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc,EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc, LastEBPRecID Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc
                          ");
            else
                if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0034, VersionControl.Equality.gt))
                sb.Append(@" Select Top 1 PasEBPer=IsNull(PasEBPer,0),AdlVal,EBedVal=IsNull(EBedVal,AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val,EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,
		                           LastEBRecID=EB.RecID*@GetEBLastRecID,LastEBPRecID=EBP.RecID*@GetEBPLastRecID
	                        From PasEB EB (NOLOCK)
	                        Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID
	                        Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=V.Hotel and (EBH.Room='' or EBH.Room=V.Room)and (EBH.Room='' or EBH.Board=V.Board)
	                        Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P'
	                        Where EBP.RecID<>V.PEB_RecID and V.PasEBPer<>0 and
	                        V.PasEBValid='Y' and OwnerOperator=V.Operator and OwnerMarket=V.Market and Operator=@Operator and Market=@Market and V.PasEBValid='Y' and EB.SaleType=0 and 
		                         ( (EB.HolPack=V.HolPack or PG.HolPack=V.HolPack) or (EB.HolPack='' and PG.HolPack is Null) )
		                          and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate 
		                          and V.PEB_ResEndDate+1 between ResBegDate and ResEndDate and CinAccom=1
		                          and (V.Night between IsNull(MinDay,0) and IsNull(MaxDay,999))
		                          and SubString(DoW,DATEPART(dw,V.CheckIn),1)=1	                        
	                        Order by LastEBRecID desc, EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc,EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc, LastEBPRecID Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc
                          ");
            else
                sb.Append(@" Select Top 1 PasEBPer=IsNull(PasEBPer,0),AdlVal,EBedVal=IsNull(EBedVal,AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val,EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,
		                           LastEBRecID=EB.RecID*@GetEBLastRecID,LastEBPRecID=EBP.RecID*@GetEBPLastRecID
	                        From PasEB EB (NOLOCK)
	                        Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID
	                        Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=V.Hotel and (EBH.Room='' or EBH.Room=V.Room)
	                        Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P'
	                        Where EBP.RecID<>V.PEB_RecID and V.PasEBPer<>0 and
	                        V.PasEBValid='Y' and OwnerOperator=V.Operator and OwnerMarket=V.Market and Operator=@Operator and Market=@Market and V.PasEBValid='Y' and EB.SaleType=0 and 
		                         ( (EB.HolPack=V.HolPack or PG.HolPack=V.HolPack) or (EB.HolPack='' and PG.HolPack is Null) )
		                          and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate 
		                          and V.PEB_ResEndDate+1 between ResBegDate and ResEndDate and CinAccom=1
		                          and (V.Night between IsNull(MinDay,0) and IsNull(MaxDay,999))
		                          and SubString(DoW,DATEPART(dw,V.CheckIn),1)=1	                        
	                        Order by LastEBRecID desc, EBH.Hotel Desc,EBH.Room Desc, EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc, LastEBPRecID Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc
                          ");

            sb.Append(@" ) PEB2
                        Where V.PEB_CinAccom=1 and PEB_ResEndDate<V.CheckIn+Night-1
                        --PasEbPer2 - 2.period ***************

                        --Update #Temp3 Set SPWithEB=SPWithEB-(IsNull(EBAmount,0)*PasEBPer / 100) where IsNull(EBAmount,0) > 0 and PasEBPer<>0

                        Update #Temp3
                        Set SPWithEB=SPWithEB-(IsNull(EBAmount,0)*PasEBPer* ((DATEDIFF(day,CheckIn,PEB_ResEndDate)+1)/ cast(Night as dec(18,6))) / 100)
                                             -(IsNull(EBAmount,0)*PasEBPer2* ((DATEDIFF(day,PEB_ResEndDate,CheckIn+Night-1))/ cast(Night as dec(18,6))) / 100)


                        Update #Temp3
                        Set PEB_Amount=  Case When HAdult>0 and PEB_AdlVal<>0 then (Case When HAdult>Adl then Adl else HAdult end) * PEB_AdlVal else 0 end+
                                         Case When HAdult-Adl>=1 then PEB_EBedVal else 0 end+
                                         Case When HAdult-Adl>=2 then PEB_EBedVal else 0 end+
                                         Case When HChdAgeG1>0 and PEB_ChdGrp1Val<>0 then HChdAgeG1*PEB_ChdGrp1Val else 0 end+
                                         Case When HChdAgeG2>0 and PEB_ChdGrp2Val<>0 then HChdAgeG2*PEB_ChdGrp2Val else 0 end+
                                         Case When HChdAgeG3>0 and PEB_ChdGrp3Val<>0 then HChdAgeG3*PEB_ChdGrp3Val else 0 end+
                                         Case When HChdAgeG4>0 and PEB_ChdGrp4Val<>0 then HChdAgeG4*PEB_ChdGrp4Val else 0 end,
                             SPWithEB=SPWithEB-
		                        (
		                          Case When HAdult>0 and PEB_AdlVal<>0 then Case When HAdult>Adl then Adl else HAdult end * PEB_AdlVal else 0 end+
		                          Case When HAdult-Adl>=1 then PEB_EBedVal else 0 end+
		                          Case When HAdult-Adl>=2 then PEB_EBedVal else 0 end+
		                          Case When HChdAgeG1>0 and PEB_ChdGrp1Val<>0 then HChdAgeG1*PEB_ChdGrp1Val else 0 end+
		                          Case When HChdAgeG2>0 and PEB_ChdGrp2Val<>0 then HChdAgeG2*PEB_ChdGrp2Val else 0 end+
		                          Case When HChdAgeG3>0 and PEB_ChdGrp3Val<>0 then HChdAgeG3*PEB_ChdGrp3Val else 0 end+
		                          Case When HChdAgeG4>0 and PEB_ChdGrp4Val<>0 then HChdAgeG4*PEB_ChdGrp4Val else 0 end
		                        )
                        Where PEB_AdlVal<>0

                        Update #Temp3
                        Set 
                            PEB_EBedVal=0,
                            PEB_ChdGrp1Val=0,
                            PEB_ChdGrp2Val=0,
                            PEB_ChdGrp3Val=0,
                            PEB_ChdGrp4Val=0
                        Where PEB_AdlVal=0 ");


            #region Only Hotel Flight Or Transport Type
            sb.Append("  Update #Temp3 Set TransportType = Case when HP.Service = 'FLIGHT' Then 1 when HP.Service = 'TRANSPORT' Then 2 Else 0 End \n");
            sb.Append("  From HolPackPlan HP \n");
            sb.Append("  Where HP.HolPack = #Temp3.HolPack and HP.StepNo = 1 \n");

            sb.Append("  Update #Temp3 Set TransportType = 2,DepServiceType='TRANSPORT',RetServiceType='TRANSPORT'  \n");
            sb.Append("  From HolPackPlan HP \n");
            sb.Append("  Where #Temp3.TransportType = 0 And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack = #Temp3.HolPack And Service = 'TRANSPORT') Between 1 And 2 \n");
            sb.Append("  And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack = #Temp3.HolPack And Service = 'FLIGHT') = 0 \n");
            #endregion


            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030300003"))
                sb.Append(@"
                        Update #Temp3 Set PEB_Amount=PEB_Amount*-1
                        Where PEB_Amount<>0

                        --Set @hasPLAllotment=case when Exists(Select * From #Temp3 Where PLFreeAmount<1000) then 1 else 0 end

                        Update #Temp3 
                        Set 
                            SPWithEB=dbo.ufn_Exchange(dbo.DateOnly(@SaleBegDate), SaleCur, isnull(@Cur,SaleCur),SPWithEB,1,Market),
                            CalcSalePrice=dbo.ufn_Exchange(dbo.DateOnly(@SaleBegDate), SaleCur, isnull(@Cur,SaleCur),CalcSalePrice,1,Market)
                        ");
            else
                sb.Append(@"                        
                        Update #Temp3 
                        Set 
                            SPWithEB=dbo.ufn_Exchange(dbo.DateOnly(@SaleBegDate), SaleCur, isnull(@Cur,SaleCur),SPWithEB,1,Market),
                            CalcSalePrice=dbo.ufn_Exchange(dbo.DateOnly(@SaleBegDate), SaleCur, isnull(@Cur,SaleCur),CalcSalePrice,1,Market)
                           ");

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030300003"))
                sb.Append(@" Select R.*, PLSPO_Amount=IsNull(SpoFlightTot,0)+IsNull(SpoHotelTot,0), 
                       ");
            else
                sb.Append(@"Select R.*, 
                       ");
            if (string.Equals(UserData.CustomRegID, Common.crID_Detur))
            {
                sb.Append(" FreeRoom = dbo.ufn_HotelFreeAmount(IsNull(NullIf(R.DepFlightMarket,Space(10)),R.Market), @Office, @Agency, R.Hotel, R.Room, R.Accom, -1, R.CheckIn, R.Night, 11), \n");
                sb.Append(" StopSaleGuar=dbo.ufn_StopSaleCnt(Hotel, Room, Accom, Board, IsNull(NullIf(R.DepFlightMarket,Space(10)),R.Market), @Agency, CatPackID, HotLocation, -2, (CheckIn+(Night-isnull(HotNight,Night))), CheckIn+Night, 0, 1), \n");
                sb.Append(" StopSaleStd=dbo.ufn_StopSaleCnt(Hotel, Room, Accom, Board, IsNull(NullIf(R.DepFlightMarket,Space(10)),R.Market), @Agency, CatPackID, HotLocation, 2, (CheckIn+(Night-isnull(HotNight,Night))), CheckIn+Night, 0, 1), \n");
            }
            if (!checkAvailableRoom && !extAllotControl)
            {
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200000"))
                {
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050005129"))
                        if (string.Equals(UserData.CustomRegID, Common.crID_Detur))
                        {
                            //sb.Append(" FreeRoom = dbo.ufn_HotelFreeAmount(IsNull(NullIf(R.DepFlightMarket,Space(10)),R.Market), @Office, @Agency, R.Hotel, R.Room, R.Accom, -1, R.CheckIn, R.Night, 11), \n");
                            //sb.Append(" StopSaleGuar=dbo.ufn_StopSaleCnt(Hotel, Room, Accom, Board, IsNull(NullIf(R.DepFlightMarket,Space(10)),R.Market), @Agency, CatPackID, HotLocation, -2, (CheckIn+(Night-isnull(HotNight,Night))), CheckIn+Night, 0, 1), \n");
                            //sb.Append(" StopSaleStd=dbo.ufn_StopSaleCnt(Hotel, Room, Accom, Board, IsNull(NullIf(R.DepFlightMarket,Space(10)),R.Market), @Agency, CatPackID, HotLocation, 2, (CheckIn+(Night-isnull(HotNight,Night))), CheckIn+Night, 0, 1), \n");
                        }
                        else
                            sb.Append(" FreeRoom = dbo.ufn_HotelFreeAmount(R.Market, @Office, @Agency, R.Hotel, R.Room, R.Accom, -1, R.CheckIn, R.Night, 11), \n");

                    else
                        sb.Append(" FreeRoom = dbo.ufn_HotelFreeAmount(R.Market, @Office, @Agency, R.Hotel, R.Room, R.Accom, -1, R.CheckIn, R.Night, 1), \n");
                }
                else
                    sb.Append(" FreeRoom = dbo.ufn_HotelFreeAmount(R.Market, @Office, @Agency, R.Hotel, R.Room, R.Accom, -1, R.CheckIn, R.Night, 'Y'), \n");
            }

            List<dbInformationShema> spShema = VersionControl.getSpShema("ufn_CheckTransportAlllotForSearch", ref errorMsg);

            if (!checkAvailableFlightSeat && !extAllotControl)
                if (!checkAvailableFlightSeat)
                {
                    if (spShema.Where(w => w.Name == "@HolPack").Count() > 0)
                    {
                        sb.Append(" DepSeat = Case When DepServiceType = 'TRANSPORT' Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn, R.DepCity, R.ArrCity, R.Operator, R.HolPack)  \n");
                        //sb.Append("                When DepServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, 0, null)  \n");
                        sb.Append("                When DepServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, 0, null)  \n");
                        sb.Append("                When DepServiceType = '' Then  (Case When TransportType = 1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, null, null, R.HolPack, 0, null) \n");
                        sb.Append("                                                     When TransportType = 2 Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn, R.DepCity, R.ArrCity, R.Operator, R.HolPack) \n");
                        sb.Append("                                                     End) \n");
                        sb.Append("                End,   \n");

                        sb.Append(" RetSeat = Case When DepServiceType = 'FLIGHT' Then  \n");
                        sb.Append("                Case When RetServiceType = 'TRANSPORT' Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn + R.Night, R.RetDepCity, R.RetArrCity, R.Operator, R.HolPack)  \n");
                        //sb.Append("                     When RetServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.RetFlight, R.CheckIn + R.Night, 0, R.FlightClass, null, R.HolPack, 0, null)  \n");
                        sb.Append("                     When RetServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, 1, null)  \n");

                        sb.Append("                End  \n");
                        sb.Append("                When DepServiceType = 'TRANSPORT' Then  \n");
                        sb.Append("                Case When RetServiceType = 'TRANSPORT' Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn + R.Night,R.RetDepCity, R.RetArrCity, R.Operator, R.HolPack)  \n");
                        //sb.Append("                     When RetServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.RetFlight, R.CheckIn + R.Night, 0, R.FlightClass, null, R.HolPack, 0, null)  \n");
                        sb.Append("                     When RetServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.RetFlight, R.CheckIn + R.Night, 0, null, null, R.HolPack, 0, null)  \n");
                        sb.Append("                End  \n");
                        sb.Append("           End,   \n");

                        //sb.Append(" DepSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn,R.DepCity,R.ArrCity,R.Operator,R.HolPack) \n");
                        //sb.Append("              When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator,R.Market,R.DepFlight,R.CheckIn,R.Night,R.FlightClass,null,R.HolPack,0,null)  \n");
                        //sb.Append("              Else null  \n");
                        //sb.Append("              End, \n");

                        //sb.Append(" RetSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn+R.Night,R.ArrCity,R.DepCity,R.Operator,R.HolPack) \n");
                        //sb.Append("              When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator,R.Market,R.DepFlight,R.CheckIn,R.Night,R.FlightClass,null,R.HolPack,1,null)  \n");
                        //sb.Append("              Else null  \n");
                        //sb.Append("              End, \n");

                    }
                    else
                    {
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, 37, VersionControl.Equality.gt))
                        {

                            sb.Append(" DepSeat = Case When DepServiceType = 'TRANSPORT' Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn, R.DepCity, R.ArrCity, R.Operator, R.HolPack)  \n");
                            //sb.Append("                When DepServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, 0, null)  \n");
                            sb.Append("                When DepServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, null, null, R.HolPack, 0, null)  \n");
                            sb.Append("                Else null  \n");
                            sb.Append("                End,   \n");

                            sb.Append(" RetSeat = Case When DepServiceType = 'FLIGHT' Then  \n");
                            sb.Append("                Case When RetServiceType = 'TRANSPORT' Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn + R.Night, R.RetDepCity, R.RetArrCity, R.Operator, R.HolPack)  \n");
                            //sb.Append("                     When RetServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.RetFlight, R.CheckIn + R.Night, 0, R.FlightClass, null, R.HolPack, 0, null)  \n");
                            sb.Append("                     When RetServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.RetFlight, R.CheckIn, 0, null, null, R.HolPack, 1, null)  \n");
                            sb.Append("                End  \n");
                            sb.Append("                When DepServiceType = 'TRANSPORT' Then  \n");
                            sb.Append("                Case When RetServiceType = 'TRANSPORT' Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn + R.Night,R.RetDepCity, R.RetArrCity, R.Operator, R.HolPack)  \n");
                            //sb.Append("                     When RetServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.RetFlight, R.CheckIn + R.Night, 0, R.FlightClass, null, R.HolPack, 0, null)  \n");
                            sb.Append("                     When RetServiceType = 'FLIGHT' Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.RetFlight, R.CheckIn + R.Night, 0, null, null, R.HolPack, 0, null)  \n");
                            sb.Append("                End  \n");
                            sb.Append("           End,   \n");

                            //sb.Append(" DepSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn,R.DepCity,R.ArrCity,R.Operator) \n");
                            //sb.Append("              When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator,R.Market,R.DepFlight,R.CheckIn,R.Night,R.FlightClass,null,R.HolPack,0,null)  \n");
                            //sb.Append("              Else null  \n");
                            //sb.Append("              End, \n");

                            //sb.Append(" RetSeat=Case When TransportType=2 Then dbo.ufn_CheckTransportAlllotForSearch(R.CheckIn+R.Night,R.ArrCity,R.DepCity,R.Operator) \n");
                            //sb.Append("              When TransportType=1 Then dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator,R.Market,R.DepFlight,R.CheckIn,R.Night,R.FlightClass,null,R.HolPack,1,null)  \n");
                            //sb.Append("              Else null  \n");
                            //sb.Append("              End, \n");

                        }
                        else
                            if (sType != SearchType.OnlyHotelSearch)
                        {
                            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200003"))
                            {
                                //sb.Append(" DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, 0, null), ");
                                //sb.Append(" RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, 1, null), ");
                            }
                            else
                            {
                                sb.Append(" DepSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.CatPackID, R.PRecNo, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, R.HolPack, R.DepCity, R.ArrCity, 0, null), \n");
                                sb.Append(" RetSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.CatPackID, R.PRecNo, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, R.HolPack, R.DepCity, R.ArrCity, 1, null), \n");
                            }
                        }
                    }
                }

            sb.Append("       Cur=@Cur,  \n");
            sb.Append("       CurrentCur=isnull(@Cur, R.SaleCur), \n");
            sb.Append("       Description = R.CPDescription, OprText = (Case When isnull(H.OprText, '') = '' then O.NameS else H.OprText end) \n");
            sb.Append("Into #Temp4 \n");
            sb.Append("From #Temp3 R \n");
            sb.Append("Join HolPack H (NOLOCK) ON H.Code = R.HolPack \n");
            sb.Append("Join Operator O (NOLOCK) ON O.Code = R.Operator \n");

            if (string.Equals(UserData.CustomRegID, Common.crID_SunFun) && Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050017149"))
            {
                sb.Append(@"
                            Update #Temp4
                            Set InsurPrice=Cast(SPWithEB*SalePerOfRes/100 as Dec(18,2))
                            From HolPack (NOLOCK) H
                            Join Insurance I (NOLOCK) on I.Code=H.SaleDepInsurance and I.SalePriceDependRes=1
                            Where H.Code=#Temp4.HolPack

                            Update #Temp4
                            Set CompExtSerPrice=dbo.ufn_GetFlightExtraService1(Market,'',DepFlight,CheckIn,CheckIn,@SaleBegDate,Night,HAdult,HChdAgeG1,HChdAgeG2+HChdAgeG3+HChdAgeG4,'','',0,1,0,1)
                            Where DepFlight<>''

                            Update #Temp4
                            Set CalcSalePrice=CalcSalePrice+InsurPrice+IsNull(CompExtSerPrice,0),
                                SPWithEB=SPWithEB+InsurPrice+IsNull(CompExtSerPrice,0)
                            Where InsurPrice>0
                           ");
            }

            //            sb.Append(@"
            //    update P set
            // DepSeat=CASE P.DepServiceType 
            //						WHEN 'FLIGHT'
            //						THEN dbo.ufn_Get_DepRet_FlightSeatWeb(P.Operator, P.Market,  '          ', P.CheckIn, P.Night, P.FlightClass,null, P.HolPack,  0,null)
            //						WHEN 'TRANSPORT'
            //						THEN dbo.ufn_CheckTransportAlllotForSearch(CheckIn,DepCity,ArrCity,Operator,Holpack)
            //						ELSE 0
            //						END,
            //						RetSeat=CASE P.RetServiceType
            //						WHEN 'FLIGHT'
            //						THEN
            //						CASE P.DepServiceType 
            //							WHEN 'FLIGHT' 
            //							THEN dbo.ufn_Get_DepRet_FlightSeatWeb(P.Operator, P.Market,  '          ', P.CheckIn, P.Night, P.FlightClass,null, P.HolPack,  1,null)
            //							WHEN 'TRANSPORT'
            //							THEN dbo.ufn_Get_DepRet_FlightSeatWeb(P.Operator, P.Market,  '          ', DATEADD(DAY,P.Night,P.CheckIn) , 0, P.FlightClass,null, P.HolPack,  0,null) 
            //							ELSE 0 
            //						END
            //						WHEN 'TRANSPORT'
            //						THEN dbo.ufn_CheckTransportAlllotForSearch(DATEADD(DAY, P.Night, P.CheckIn), ArrCity,DepCity,Operator,P.HolPack)
            //						ELSE 0
            //						END
            //						from #Temp4 P
            //                    Select * From #Temp4 
            //                        Order By SPWithEB
            //                      ");

            if (string.Equals(UserData.CustomRegID, Common.crID_Detur))
            {
                sb.Append(@" Select *,DepFlightPNLName=isnull(fdd.PNLName,tmp.DepFlight),RetFlightPNLName=isnull(fdr.PNLName,tmp.RetFlight) From #Temp4 (nolock) tmp 
                            left join FlightDay (nolock) fdd on tmp.DepFlight=fdd.FlightNo and fdd.FlyDate=tmp.CheckIn 
                            left join FlightDay (nolock) fdr on tmp.RetFlight=fdr.FlightNo and fdr.FlyDate=dateadd(dd,tmp.Night,tmp.CheckIn)
                            where StopSaleGuar<>2 and StopSaleStd<>2
                        Order By SPWithEB");
            }
            else
            {
                sb.Append(@"Select * From #Temp4 
                        Order By SPWithEB");
            }
            string dateFormat = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').ToString().Replace("dd", "d").Replace("yy", "y").Replace("MM", "m").Replace(UserData.Ci.DateTimeFormat.DateSeparator, "");
            string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator[0].ToString();

            List<SearchResult> result = new List<SearchResult>();
            Database db = DatabaseFactory.CreateDatabase() as Database;
            string dbParamaters = @"
declare 
@CustomRegID varchar(10)='1188101',
@Operator varchar(10)='NTRAVEL',
@Market varchar(10)='AZMARKET',
@Agency varchar(10)='ERAS',
@Office varchar(10)='BAKUMAIN',
@DateFormat varchar(10)='dmyy',
@Seperator varchar(10)='.',
@Cur varchar(10)='USD'
                ";
            DbCommand dbCommand = db.GetSqlStringCommand(FilterRecords + sb.ToString());

            try
            {
                dbCommand.CommandTimeout = 180;
                dbCommand.CommandType = CommandType.Text;
                db.AddInParameter(dbCommand, "CustomRegID", DbType.String, UserData.CustomRegID);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Office", DbType.String, UserData.OprOffice);
                db.AddInParameter(dbCommand, "DateFormat", DbType.String, dateFormat);
                db.AddInParameter(dbCommand, "Seperator", DbType.String, dateSeperator);
                db.AddInParameter(dbCommand, "Cur", DbType.String, string.IsNullOrEmpty(Currency) ? null : Currency);


                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        string uKey = Conversion.getStrOrNull(oReader["UKey"]);
                        string[] uKeyDim = uKey.Split('-');
                        int? tempRefNo = Conversion.getInt32OrNull(oReader["RefNo"]);
                        SearchResult r = data.Find(f => f.RefNo == tempRefNo);
                        r.Calculated = true;
                        r.CatPackID = (int)oReader["CatPackID"];
                        r.ARecNo = (int)oReader["ARecNo"];
                        r.PRecNo = (int)oReader["PRecNo"];
                        r.HAPRecId = (int)oReader["HAPRecID"];
                        r.AccomFullName = Conversion.getStrOrNull(oReader["AccomFullName"]);
                        r.Night = (Int16)oReader["Night"];
                        r.CheckIn = (DateTime)oReader["CheckIn"];
                        r.CheckOut = r.CheckIn.Value.AddDays(r.Night.Value);
                        r.SaleCur = Conversion.getStrOrNull(oReader["CurrentCur"]);
                        r.NetCur = Conversion.getStrOrNull(oReader["NetCur"]);
                        r.DepFlight = Conversion.getStrOrNull(oReader["DepFlight"]);
                        r.RetFlight = Conversion.getStrOrNull(oReader["RetFlight"]);
                        r.EBValidDate = Conversion.getDateTimeOrNull(oReader["EBValidDate"]);
                        r.AgencyEB = Conversion.getDecimalOrNull(oReader["AgencyEB"]);
                        r.EBAmount = Conversion.getDecimalOrNull(oReader["EBAmount"]);
                        r.PasEBPer = Conversion.getDecimalOrNull(oReader["PasEBPer"]);
                        r.PEB_AdlVal = Conversion.getDecimalOrNull(oReader["PEB_AdlVal"]);
                        r.PEB_SaleEndDate = Conversion.getDateTimeOrNull(oReader["PEB_SaleEndDate"]);
                        r.PEB_ResEndDate = Conversion.getDateTimeOrNull(oReader["PEB_ResEndDate"]);
                        r.LastPrice = Conversion.getDecimalOrNull(oReader["SPWithEB"]);
                        r.CalcSalePrice = Conversion.getDecimalOrNull(oReader["CalcSalePrice"]);
                        if (!checkAvailableRoom && !extAllotControl)
                            r.FreeRoom = (int)oReader["FreeRoom"];
                        if (!checkAvailableFlightSeat && /*sType != SearchType.OnlyHotelSearch &&*/ !extAllotControl)
                            if (!checkAvailableFlightSeat)
                            {
                                r.DepSeat = Conversion.getInt32OrNull(oReader["DepSeat"]);
                                r.RetSeat = Conversion.getInt32OrNull(oReader["RetSeat"]);
                            }
                        r.CurrentCur = Conversion.getStrOrNull(oReader["CurrentCur"]);
                        r.Description = Conversion.getStrOrNull(oReader["Description"]);
                        r.SpoDesc = Conversion.getStrOrNull(oReader["SpoDesc"]);
                        r.OprText = Conversion.getStrOrNull(oReader["OprText"]);
                        r.SaleSPONo = Conversion.getInt32OrNull(oReader["SaleSPONo"]);
                        r.EBPerc = Conversion.getDecimalOrNull(oReader["EBPerc"]);
                        r.SaleEndDate = Conversion.getDateTimeOrNull(oReader["SaleEndDate"]);
                        r.HolPack = Conversion.getStrOrNull(oReader["HolPack"]);
                        r.HAdult = (Int16)oReader["HAdult"];
                        r.HChdAgeG1 = (Int16)oReader["HChdAgeG1"];
                        r.HChdAgeG2 = (Int16)oReader["HChdAgeG2"];
                        r.HChdAgeG3 = (Int16)oReader["HChdAgeG3"];
                        r.HChdAgeG4 = (Int16)oReader["HChdAgeG4"];
                        r.ChdG1Age1 = Conversion.getDecimalOrNull(oReader["ChdG1Age1"]);
                        r.ChdG1Age2 = Conversion.getDecimalOrNull(oReader["ChdG1Age2"]);
                        r.ChdG2Age1 = Conversion.getDecimalOrNull(oReader["ChdG2Age1"]);
                        r.ChdG2Age2 = Conversion.getDecimalOrNull(oReader["ChdG2Age2"]);
                        r.ChdG3Age1 = Conversion.getDecimalOrNull(oReader["ChdG3Age1"]);
                        r.ChdG3Age2 = Conversion.getDecimalOrNull(oReader["ChdG3Age2"]);
                        r.ChdG4Age1 = Conversion.getDecimalOrNull(oReader["ChdG4Age1"]);
                        r.ChdG4Age2 = Conversion.getDecimalOrNull(oReader["ChdG4Age2"]);
                        r.Operator = Conversion.getStrOrNull(oReader["Operator"]);
                        r.Market = Conversion.getStrOrNull(oReader["Market"]);
                        r.FlightClass = Conversion.getStrOrNull(oReader["FlightClass"]);
                        r.plSpoExists = Conversion.getBoolOrNull(oReader["PLSpoExists"]);
                        r.FlightValidDate = Conversion.getDateTimeOrNull(oReader["FlightValidDate"]);
                        r.HotelValidDate = Conversion.getDateTimeOrNull(oReader["HotelValidDate"]);
                        r.TransportType = Conversion.getInt16OrNull(oReader["TransportType"]);
                        if (string.Equals(UserData.CustomRegID, Common.crID_Detur))
                        {
                            r.StopSaleGuar = Conversion.getInt16OrNull(oReader["StopSaleGuar"]);
                            r.StopSaleStd = Conversion.getInt16OrNull(oReader["StopSaleStd"]);
                            r.DepFlightMarket = Conversion.getStrOrNull(oReader["DepFlightMarket"]);
                            r.DepFlightPNLName = Conversion.getStrOrNull(oReader["DepFlightPNLName"]);
                            r.RetFlightPNLName = Conversion.getStrOrNull(oReader["RetFlightPNLName"]);
                        }
                        r.SearchTime = DateTime.Now;
                    }
                }
                return data;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return data;
            }
            finally
            {
                if (dbCommand.Connection.State == ConnectionState.Open)
                    dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string createCalcDataSqlString(List<SearchResult> data, int From, int To)
        {
            StringBuilder createTable = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            string createSQLHeader = @"
if OBJECT_ID('TempDB.dbo.#tmpCalcTable') is not null Drop Table dbo.#tmpCalcTable
Create Table #tmpCalcTable (RefNo int, CatPackID int, ARecNo int, PRecNo int, HAPRecNo int, CalcSalePrice decimal(18,5), SaleChdG1 decimal(18,5), SaleChdG2 decimal(18,5), SaleChdG3 decimal(18,5), SaleChdG4 decimal(18,5), HotChdPrice decimal(18,5), DepFlight VarChar(10) COLLATE Latin1_General_CI_AS, RetFlight VarChar(10) COLLATE Latin1_General_CI_AS, TransportType smallint)";
            List<SearchResult> query = (from q in data.AsEnumerable()
                                        where q.RefNo >= From && q.RefNo <= To && !q.Calculated
                                        select q).ToList<SearchResult>();
            if (query.Count() == 0)
                return "";
            foreach (SearchResult row in query)
            {
                createTable.Append("Insert Into #tmpCalcTable (RefNo, CatPackID, ARecNo, PRecNo, HAPRecNo, CalcSalePrice, SaleChdG1, SaleChdG2, SaleChdG3, SaleChdG4, HotChdPrice, DepFlight, RetFlight) \n");
                createTable.AppendFormat("Values ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, '{11}', '{12}', {13}) \n",
                        row.RefNo, row.CatPackID, row.ARecNo, row.PRecNo, row.HAPRecId, row.CalcSalePrice.Value.ToString().Replace(',', '.'), row.SaleChdG1.Value.ToString().Replace(',', '.'), row.SaleChdG2.Value.ToString().Replace(',', '.'), row.SaleChdG3.Value.ToString().Replace(',', '.'), row.SaleChdG4.Value.ToString().Replace(',', '.'), row.HotChdPrice.Value.ToString().Replace(',', '.'), row.DepFlight, row.RetFlight, row.TransportType);
            }
            if (createTable.Length > 0)
                createSQLHeader += createTable.ToString();
            return createSQLHeader;
        }

        public string createCalcDataMultiRoomFullDataSqlString(List<SearchResult> data, ref bool isEmpty)
        {
            isEmpty = true;
            StringBuilder createTable = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            string createSQLHeader = @"
if OBJECT_ID('TempDB.dbo.#tmpCalcTable') is not null Drop Table dbo.#tmpCalcTable
Create Table #tmpCalcTable (RefNo int, CatPackID int, ARecNo int, PRecNo int, HAPRecNo int, CalcSalePrice decimal(18,5), SaleChdG1 decimal(18,5), SaleChdG2 decimal(18,5), SaleChdG3 decimal(18,5), SaleChdG4 decimal(18,5), HotChdPrice decimal(18,5), DepFlight VarChar(10) COLLATE Latin1_General_CI_AS, RetFlight VarChar(10) COLLATE Latin1_General_CI_AS, TransportType smallint) ";
            //foreach (SearchResult filterRow in data)
            //{
            List<SearchResult> query = (from q1 in data
                                        where !q1.Calculated
                                        select q1).ToList<SearchResult>();
            if (query.Count() == 0)
                return createSQLHeader;
            foreach (SearchResult row in query)
            {
                createTable.Append("Insert Into #tmpCalcTable (RefNo, CatPackID, ARecNo, PRecNo, HAPRecNo, CalcSalePrice, SaleChdG1, SaleChdG2, SaleChdG3, SaleChdG4, HotChdPrice, DepFlight, RetFlight, TransportType) \n");
                createTable.AppendFormat("Values ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, '{11}', '{12}', {13}) \n",
                                            row.RefNo,
                                            row.CatPackID,
                                            row.ARecNo,
                                            row.PRecNo,
                                            row.HAPRecId,
                                            row.CalcSalePrice.HasValue ? row.CalcSalePrice.Value.ToString().Replace(',', '.') : "null",
                                            row.SaleChdG1.HasValue ? row.SaleChdG1.Value.ToString().Replace(',', '.') : "null",
                                            row.SaleChdG2.HasValue ? row.SaleChdG2.Value.ToString().Replace(',', '.') : "null",
                                            row.SaleChdG3.HasValue ? row.SaleChdG3.Value.ToString().Replace(',', '.') : "null",
                                            row.SaleChdG4.HasValue ? row.SaleChdG4.Value.ToString().Replace(',', '.') : "null",
                                            row.HotChdPrice.HasValue ? row.HotChdPrice.Value.ToString().Replace(',', '.') : "null",
                                            row.DepFlight,
                                            row.RetFlight,
                                            row.TransportType.HasValue ? row.TransportType.Value.ToString() : "null");
            }
            //}
            if (createTable.Length > 0)
            {
                createSQLHeader += createTable.ToString();
                isEmpty = false;
            }
            return createSQLHeader;
        }

        public string createCalcDataMultiRoomSqlString(List<SearchResult> data, List<MultiRoomSelection> filterData, ref bool isEmpty)
        {
            isEmpty = true;
            StringBuilder createTable = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            string createSQLHeader = @"
if OBJECT_ID('TempDB.dbo.#tmpCalcTable') is not null Drop Table dbo.#tmpCalcTable 
Create Table #tmpCalcTable (RefNo int, CatPackID int, ARecNo int, PRecNo int, HAPRecNo int, CalcSalePrice decimal(18,5), SaleChdG1 decimal(18,5), SaleChdG2 decimal(18,5), SaleChdG3 decimal(18,5), SaleChdG4 decimal(18,5), HotChdPrice decimal(18,5), DepFlight VarChar(10) COLLATE Latin1_General_CI_AS, RetFlight VarChar(10) COLLATE Latin1_General_CI_AS) ";
            foreach (MultiRoomSelection filterRow in filterData)
            {
                List<SearchResult> query = (from q in filterRow.RoomPriceList
                                            join q1 in data on q equals q1.RefNo
                                            where !q1.Calculated
                                            select q1).ToList<SearchResult>();
                if (query.Count() == 0)
                    return createSQLHeader;
                foreach (SearchResult row in query)
                {
                    try
                    {
                        createTable.Append("Insert Into #tmpCalcTable (RefNo, CatPackID, ARecNo, PRecNo, HAPRecNo, CalcSalePrice, SaleChdG1, SaleChdG2, SaleChdG3, SaleChdG4, HotChdPrice, DepFlight, RetFlight) \n");
                        createTable.AppendFormat("Values ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, '{11}', '{12}') \n",
                                                    row.RefNo,
                                                    row.CatPackID,
                                                    row.ARecNo,
                                                    row.PRecNo,
                                                    row.HAPRecId,
                                                    row.CalcSalePrice.HasValue ? row.CalcSalePrice.Value.ToString().Replace(',', '.') : "null",
                                                    row.SaleChdG1.HasValue ? row.SaleChdG1.Value.ToString().Replace(',', '.') : "null",
                                                    row.SaleChdG2.HasValue ? row.SaleChdG2.Value.ToString().Replace(',', '.') : "null",
                                                    row.SaleChdG3.HasValue ? row.SaleChdG3.Value.ToString().Replace(',', '.') : "null",
                                                    row.SaleChdG4.HasValue ? row.SaleChdG4.Value.ToString().Replace(',', '.') : "null",
                                                    row.HotChdPrice.HasValue ? row.HotChdPrice.Value.ToString().Replace(',', '.') : "null",
                                                    row.DepFlight, row.RetFlight);
                    }
                    catch
                    {
                        int i = 1;
                        i = 0;
                    }
                }
            }
            if (createTable.Length > 0)
            {
                createSQLHeader += createTable.ToString();
                isEmpty = false;
            }
            return createSQLHeader;
        }

        public List<HotelMarOptRecord> getHotelMarOptList(User UserData, List<SearchResult> data, ref string errorMsg)
        {
            List<HotelMarOptRecord> records = new List<HotelMarOptRecord>();
            var query = from q in data
                        group q by new { q.Hotel } into k
                        select new { Hotel = k.Key.Hotel };
            string tmpQueryStr = @"
if OBJECT_ID('TempDB.dbo.#tmpHotelTable') is not null Drop Table dbo.#tmpHotelTable 
Create TABLE #tmpHotelTable (Hotel VarChar(10) COLLATE Latin1_General_CI_AS) ";
            foreach (var row in query)
                tmpQueryStr += string.Format(" Insert Into #tmpHotelTable (Hotel) Values ('{0}') ", row.Hotel);
            tmpQueryStr += @"Select H.RecID, Hotel=HT.Hotel, Market=@Market, InfoWeb=isnull(H.InfoWeb, ''), H.MaxChdAge
                                 From #tmpHotelTable HT
                                 Left Join HotelMarOpt (NOLOCK) H ON H.Hotel=HT.Hotel
                                 Where H.Market=@Market";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tmpQueryStr);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        HotelMarOptRecord record = new HotelMarOptRecord();
                        record.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        record.Hotel = Conversion.getStrOrNull(oReader["Hotel"]);
                        record.Market = Conversion.getStrOrNull(oReader["Market"]);
                        record.InfoWeb = Conversion.getStrOrNull(oReader["InfoWeb"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Dispose();
            }
        }

        public List<FlightDetailRecord> getFlightDetail(User UserData, List<SearchResult> data, ref string errorMsg)
        {
            List<FlightDetailRecord> records = new List<FlightDetailRecord>();

            var query = from q in data
                        group q by new { DepFlight = (q.UseDynamicFlights ? q.DynamicFlights.DFlightNo : q.DepFlight), q.CheckIn } into k
                        select new { FlightNo = k.Key.DepFlight, FlyDate = k.Key.CheckIn };
            string tmpQueryStr = @"Set DateFormat DMY
                                   if OBJECT_ID('TempDB.dbo.#tmpFlightTable') is not null Drop Table dbo.#tmpFlightTable
                                   Create Table #tmpFlightTable (FlightNo VarChar(10)  COLLATE Latin1_General_CI_AS, FlyDate DateTime) ";
            foreach (var row in query)
                tmpQueryStr += string.Format(" Insert Into #tmpFlightTable (FlightNo, FlyDate) Values ('{0}', '{1}') ", row.FlightNo, row.FlyDate.Value.ToString("dd/MM/yyyy"));
            var query1 = from q in data
                         group q by new { FlightNo = q.UseDynamicFlights ? q.DynamicFlights.RFlightNo : q.RetFlight, FlyDate = q.CheckIn.Value.AddDays(q.Night.Value) } into k
                         select new { FlightNo = k.Key.FlightNo, FlyDate = k.Key.FlyDate };
            foreach (var row in query1)
                tmpQueryStr += string.Format(" Insert Into #tmpFlightTable (FlightNo, FlyDate) Values ('{0}', '{1}') ", row.FlightNo, row.FlyDate.ToString("dd/MM/yyyy"));

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                tmpQueryStr += @"Select FD.FlightNo, FD.FlyDate, DepTime, TDepTime, DepAirport,
	                            DepAirportName=(Select Name From Airport (NOLOCK) Where Code=FD.DepAirport),
	                            DepAirportNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Airport (NOLOCK) Where Code=FD.DepAirport),
	                            DepCity,
	                            DepCityName=(Select Name From Location (NOLOCK) Where RecID=FD.DepCity),
	                            DepCityNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=FD.DepCity),
	                            ArrTime, TArrTime, 
	                            ArrAirport, 
	                            ArrAirportName=(Select Name From Airport (NOLOCK) Where Code=FD.ArrAirport), 
	                            ArrAirportNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Airport (NOLOCK) Where Code=FD.ArrAirport), 
	                            ArrCity,
	                            ArrCityName=(Select Name From Location (NOLOCK) Where RecID=FD.ArrCity),
	                            ArrCityNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=FD.ArrCity),	
	                            Airline,
	                            AirlineName=(Select Name From Airline (NOLOCK) Where Code=FD.Airline),
	                            AirlineNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Airline (NOLOCK) Where Code=FD.Airline),
	                            MaxInfAge, MaxChdAge, MaxTeenAge, BagWeight
                            From FlightDay (NOLOCK) FD
                            Join #tmpFlightTable FT ON FT.FlightNo=FD.FlightNo And FT.FlyDate=FD.FlyDate ";
            else
                tmpQueryStr += @"Select FD.FlightNo, FD.FlyDate, DepTime, DepAirport,
	                            DepAirportName=(Select Name From Airport (NOLOCK) Where Code=FD.DepAirport),
	                            DepAirportNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Airport (NOLOCK) Where Code=FD.DepAirport),
	                            DepCity,
	                            DepCityName=(Select Name From Location (NOLOCK) Where RecID=FD.DepCity),
	                            DepCityNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=FD.DepCity),
	                            ArrTime, 
	                            ArrAirport, 
	                            ArrAirportName=(Select Name From Airport (NOLOCK) Where Code=FD.ArrAirport), 
	                            ArrAirportNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Airport (NOLOCK) Where Code=FD.ArrAirport), 
	                            ArrCity,
	                            ArrCityName=(Select Name From Location (NOLOCK) Where RecID=FD.ArrCity),
	                            ArrCityNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=FD.ArrCity),	
	                            Airline,
	                            AirlineName=(Select Name From Airline (NOLOCK) Where Code=FD.Airline),
	                            AirlineNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Airline (NOLOCK) Where Code=FD.Airline),
	                            MaxInfAge, MaxChdAge, MaxTeenAge, BagWeight
                            From FlightDay (NOLOCK) FD
                            Join #tmpFlightTable FT ON FT.FlightNo=FD.FlightNo And FT.FlyDate=FD.FlyDate ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tmpQueryStr);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(new FlightDetailRecord
                        {
                            FlightNo = Conversion.getStrOrNull(R["FlightNo"]),
                            FlyDate = Conversion.getDateTimeOrNull(R["FlyDate"]),
                            DepTime = Conversion.getDateTimeOrNull(R["DepTime"]),
                            DepAirport = Conversion.getStrOrNull(R["DepAirport"]),
                            DepAirportName = Conversion.getStrOrNull(R["DepAirportName"]),
                            DepAirportNameL = Conversion.getStrOrNull(R["DepAirportNameL"]),
                            DepCity = Conversion.getInt32OrNull(R["DepCity"]),
                            DepCityName = Conversion.getStrOrNull(R["DepCityName"]),
                            DepCityNameL = Conversion.getStrOrNull(R["DepCityNameL"]),
                            ArrTime = Conversion.getDateTimeOrNull(R["ArrTime"]),
                            ArrAirport = Conversion.getStrOrNull(R["ArrAirport"]),
                            ArrAirportName = Conversion.getStrOrNull(R["DepAirportName"]),
                            ArrAirportNameL = Conversion.getStrOrNull(R["DepAirportNameL"]),
                            ArrCity = Conversion.getInt32OrNull(R["ArrCity"]),
                            ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]),
                            ArrCityNameL = Conversion.getStrOrNull(R["ArrCityNameL"]),
                            Airline = Conversion.getStrOrNull(R["Airline"]),
                            AirlineName = Conversion.getStrOrNull(R["AirlineName"]),
                            AirlineNameL = Conversion.getStrOrNull(R["AirlineNameL"]),
                            MaxInfAge = Conversion.getDecimalOrNull(R["MaxInfAge"]),
                            MaxChdAge = Conversion.getDecimalOrNull(R["MaxChdAge"]),
                            MaxTeenAge = Conversion.getDecimalOrNull(R["MaxTeenAge"]),
                            BagWeight = Conversion.getDecimalOrNull(R["BagWeight"]),
                            TDepTime = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003")) ? Conversion.getDateTimeOrNull(R["TDepTime"]) : null,
                            TArrTime = (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003")) ? Conversion.getDateTimeOrNull(R["TArrTime"]) : null
                        });
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Dispose();
            }
        }

        public List<SearchResultOH> setEmptyOnlyHotelSearchDataMultiRoomFullData(User UserData, List<SearchResultOH> data, bool checkAvailableRoom, bool extAllotControl, string Currency, ref string errorMsg)
        {
            //string createSQLHeader = string.Empty;
            //bool isEmpty = false;
            //createSQLHeader += createCalcDataMultiRoomFullDataSqlString(data, ref isEmpty);
            //if (!isEmpty)
            //    data = new Search().getFilteredSearchData(UserData, data, createSQLHeader, -1, -1, checkAvailableRoom, checkAvailableFlightSeat, extAllotControl, sType, Currency, ref errorMsg);
            return data;
        }

        public List<SearchResult> setEmptySearchDataMultiRoomFullData(User UserData, List<SearchResult> data, bool checkAvailableRoom, bool checkAvailableFlightSeat, bool extAllotControl, SearchType sType, string Currency, ref string errorMsg)
        {
            string createSQLHeader = string.Empty;
            bool isEmpty = false;
            createSQLHeader += createCalcDataMultiRoomFullDataSqlString(data, ref isEmpty);
            if (!isEmpty)
                data = new Search().getFilteredSearchData(UserData, data, createSQLHeader, -1, -1, checkAvailableRoom, checkAvailableFlightSeat, extAllotControl, sType, Currency, ref errorMsg);
            return data;
        }

        public List<SearchResult> setEmptySearchDataMultiRoom(User UserData, List<SearchResult> data, List<MultiRoomSelection> filterData, bool checkAvailableRoom, bool checkAvailableFlightSeat, bool extAllotControl, SearchType sType, string Currency, ref string errorMsg)
        {
            string createSQLHeader = string.Empty;
            bool isEmpty = false;
            createSQLHeader += createCalcDataMultiRoomSqlString(data, filterData, ref isEmpty);
            if (!isEmpty)
            {
                data = new Search().getFilteredSearchData(UserData, data, createSQLHeader, -1, -1, checkAvailableRoom, checkAvailableFlightSeat, extAllotControl, sType, Currency, ref errorMsg);
            }
            return data;
        }

        public List<SearchResult> setEmptySearchData(User UserData, List<SearchResult> data, int _from, int _to, bool checkAvailableRoom, bool checkAvailableFlightSeat, bool extAllotControl, SearchType sType, string Currency, ref string errorMsg)
        {
            string createSQLHeader = createCalcDataSqlString(data, _from, _to);
            int From = _from;
            int To = _to;
            data = new Search().getFilteredSearchData(UserData, data, createSQLHeader, From, To, checkAvailableRoom, checkAvailableFlightSeat, extAllotControl, sType, Currency, ref errorMsg);
            List<SearchResult> tmpData = data.Where(w => w.RefNo >= From && w.RefNo <= To && !w.Calculated).Select(s => s).ToList<SearchResult>();
            int calcReturnCount = data.Where(w => w.RefNo >= From && w.RefNo <= To && w.Calculated).Count();
            foreach (SearchResult row in tmpData)
                data.Remove(row);

            while (19 > calcReturnCount && data.Count > 10)
            {
                int tmpFrom = To + 1;
                int tmpTo = To + tmpFrom - calcReturnCount - 1;
                createSQLHeader = createCalcDataSqlString(data, tmpFrom, tmpTo);
                data = new Search().getFilteredSearchData(UserData, data, createSQLHeader, tmpFrom, tmpTo, checkAvailableRoom, checkAvailableFlightSeat, extAllotControl, sType, Currency, ref errorMsg);
                From = tmpFrom;
                To = tmpTo;
                calcReturnCount = data.Where(w => w.RefNo >= _from && w.RefNo <= To && w.Calculated).Count();
                tmpData = data.Where(w => w.RefNo >= From && w.RefNo <= To && !w.Calculated).Select(s => s).ToList<SearchResult>();
                foreach (SearchResult row in tmpData)
                    data.Remove(row);
            }
            int i = 0;
            foreach (SearchResult row in data)
            {
                ++i;
                row.RefNo = i;
            }
            return data;
        }

        public string[] getHotelInfoHotels(User UserData, List<SearchResult> data, int From, int To)
        {
            var query = from q in data.AsEnumerable()
                        where q.RefNo >= From && q.RefNo <= To
                        group q by new { Hotel = q.Hotel } into k
                        select new { Hotel = k.Key.Hotel };
            string result = string.Empty;
            foreach (var q in query)
            {
                if (result.Length > 0)
                    result += ":";
                result += q.Hotel;
            }
            return result.Split(':');
        }

        public DataRow getSelectBook(User UserData, SearchType sType, int CatPackID, int ARecNo, int PRecNo, int HAPRecID, ref string errorMsg)
        {
            DataTable dt = new DataTable();
            string tsql = @"Select PL.*, DepFlight=FDD.FlightNo, RetFlight=FDD.NextFlight
                            From PriceListExtV PL
                            Left JOIN HolPackPlan HPP (NOLOCK) on HPP.HolPack = PL.HolPack and HPP.Service = 'FLIGHT' And HPP.Departure = PL.DepCity And HPP.Arrival = PL.ArrCity 
                            Left JOIN HolPackSer HS (NOLOCK) on HS.HolPack = HPP.HolPack and HS.StepNo = HPP.StepNo
                            Left JOIN FlightDay FDD (NOLOCK) on FDD.FlightNo = HS.ServiceItem and FDD.FlyDate=PL.CheckIn
                            Left JOIN FlightDay FDR (NOLOCK) on FDR.FlightNo = FDD.NextFlight and FDR.FlyDate=(PL.CheckIn + PL.Night)
                            Where PL.CatPackID = @CatPackID
                              And PL.ARecNo = @ARecNo
                              And PL.PRecNo = @PRecNo
                              And PL.HAPRecID = @HAPRecID ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                db.AddInParameter(dbCommand, "ARecNo", DbType.Int32, ARecNo);
                db.AddInParameter(dbCommand, "PRecNo", DbType.Int32, PRecNo);
                db.AddInParameter(dbCommand, "HAPRecID", DbType.Int32, HAPRecID);

                IDataReader oReader = db.ExecuteReader(dbCommand);
                dt.Load(oReader);

                return dt.Rows[0];
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public SearchResult getB2CToB2BPriceSearch(User UserData, int RoomNr, int CatPackID, int ARecNo, int PRecNo, int HAPRecNo, string Operator, string Market, string Cur, string Agency, string DepFlight, string RetFlight, bool TourPack, ref string errorMsg)
        {
            StringBuilder sb = new StringBuilder();
            DepFlight = DepFlight != "" ? DepFlight : "          ";
            RetFlight = RetFlight != "" ? RetFlight : "          ";
            sb.Append("SET ARITHABORT ON \n");
            sb.Append(@"if OBJECT_ID('TempDB.dbo.#Temp1') is not null Drop Table dbo.#Temp1
                        if OBJECT_ID('TempDB.dbo.#Temp2') is not null Drop Table dbo.#Temp2
                        if OBJECT_ID('TempDB.dbo.#Temp3') is not null Drop Table dbo.#Temp3
                        if OBJECT_ID('TempDB.dbo.#Temp4') is not null Drop Table dbo.#Temp4
                        if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
                        if OBJECT_ID('TempDB.dbo.#EBPas') is not null Drop Table dbo.#EBPas
                        if OBJECT_ID('TempDB.dbo.#EBPas1') is not null Drop Table dbo.#EBPas1
                        if OBJECT_ID('TempDB.dbo.#AgencyEB') is not null Drop Table dbo.#AgencyEB
                        if OBJECT_ID('TempDB.dbo.#AgencyEB1') is not null Drop Table dbo.#AgencyEB1
                        if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack
                        if OBJECT_ID('TempDB.dbo.#AgencyCP') is not null Drop Table dbo.#AgencyCP ");

            sb.Append(@"Declare @SaleBegDate DateTime
                        Set @SaleBegDate = GetDate() ");

            sb.Append(@"Select * Into #CatalogPack 
                        From CatalogPack (NOLOCK) CP
                        Where	CP.Ready = 'Y' 
       	                    And CP.EndDate > GetDate()-1
       	                    And CP.WebPub = 'Y' 
       	                    And CP.WebPubDate < GetDate() 
       	                    And dbo.DateOnly(GETDATE()) BetWeen CP.SaleBegDate And CP.SaleEndDate 
                            And CP.RecID=@CatPackID ");

            sb.Append(@"Select Distinct RecID 
                        Into #AgencyCP 
                        From 
                        ( 
            	            Select CP.RecID 
            	            From #CatalogPack CP (NOLOCK) 
            	            Where 
            		            Not Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID) or
                                Exists(Select CA.RecID from CatPackAgency CA (NOLOCK) 
                                       Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups=CA.AgencyGrp and AG.Agency=@Agency --And AG.GrpType='P'
                                       Where CA.CatPackID=CP.RecID and (AG.Agency=@Agency or CA.Agency=@Agency)) 
            	            --Union All 
            	            --Select CP.RecID 
            	            --From #CatalogPack CP (NOLOCK) 
            	            --Where 
            		        --    Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=@Agency) 
                        ) ACP 
                        ");

            sb.Append(@"Select RecID, Name=isnull(dbo.FindLocalName(NameLID, @Market), Name), Village, Town, City, Country 
                        Into #Location 
                        From Location (NOLOCK) ");

            sb.Append(@"Select UKey=Cast(CP.RecID AS VarChar)+'-'+Cast(PP.RecNo AS VarChar)+'-'+Cast(PA.RecNo AS VarChar)+'-'+Cast(HAP.RecID AS VarChar),
            	            ARecNo=PA.RecNo, PA.Hotel, PA.Room, PA.Accom, PA.Board, 
            	            PRecNo=PP.RecNo, PP.CheckIn, PP.Night, PP.HotNight, PA.Adl, PP.ChdG1Age1, PP.ChdG1Age2, PP.ChdG2Age1, PP.ChdG2Age2, 
            	            PP.ChdG3Age1, PP.ChdG3Age2, PP.ChdG4Age1, PP.ChdG4Age2, PP.HotelChdRecNo,                             
            	            CatPackID=CP.RecID, CP.BegDate, CP.EndDate, CP.HolPack, CP.Operator, CP.Market, CP.EBValid, CP.PasEBValid, CP.FlightClass, 
            	            CP.DepCity, CP.ArrCity, CPName=isnull(dbo.FindLocalName(CP.NameLID, @Market), CP.Name), CP.SaleCur, PP.NetCur, PLCur=CP.SaleCur,
                            PLSpoExists=0, CPDescription=CP.Description, CP.SaleSPONo, CP.SaleEndDate, 
            	            H.ChdCalcSale, HotelName=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name), HotLocation=H.Location, HotCat=H.Category, 
            	            RoomName=isnull(dbo.FindLocalName(HR.NameLID, @Market), HR.Name),
            	            AccomName=isnull(dbo.FindLocalName(HA.NameLID, @Market), HA.Name),
            	            BoardName=isnull(dbo.FindLocalName(HB.NameLID, @Market), HB.Name),
            	            HAPRecID=HAP.RecID, HAdult=HAP.Adult, HChdAgeG1=HAP.ChdAgeG1, HChdAgeG2=HAP.ChdAgeG2, HChdAgeG3=HAP.ChdAgeG3, HChdAgeG4=HAP.ChdAgeG4,
                            CalcSalePrice=PP.SalePrice+ 
                                Case when HAP.Adult-PA.Adl>=1 then PP.SaleExtBed1 else 0 end+ 
                                Case when HAP.Adult-PA.Adl>=2 then PP.SaleExtBed2 else 0 end+
                                Case when HAP.ChdAgeG1>0 then (PP.SaleChdG1*HAP.ChdAgeG1)+CHDP.ChdG1Price else 0 end+
                                Case when HAP.ChdAgeG2>0 then (PP.SaleChdG2*HAP.ChdAgeG2)+CHDP.ChdG2Price else 0 end+
                                Case when HAP.ChdAgeG3>0 then (PP.SaleChdG3*HAP.ChdAgeG3)+CHDP.ChdG3Price else 0 end+
                                Case when HAP.ChdAgeG4>0 then (PP.SaleChdG4*HAP.ChdAgeG4)+CHDP.ChdG4Price else 0 end,
                                HotChdPrice=Case when HAP.ChdAgeG1>0 then CHDP.ChdG1Price else 0 end+ 
                                            Case when HAP.ChdAgeG2>0 then CHDP.ChdG2Price else 0 end+ 
                                            Case when HAP.ChdAgeG3>0 then CHDP.ChdG3Price else 0 end+ 
                                            Case when HAP.ChdAgeG4>0 then CHDP.ChdG4Price else 0 end, 
                            SaleChdG1= Case when HAP.ChdAgeG1>0 then PP.SaleChdG1+CHDP.ChdG1Price/dbo.IfZero(HAP.ChdAgeG1,1) else 0 end,
                            SaleChdG2= Case when HAP.ChdAgeG2>0 then PP.SaleChdG2+CHDP.ChdG2Price/dbo.IfZero(HAP.ChdAgeG2,1) else 0 end, 
                            SaleChdG3= Case when HAP.ChdAgeG3>0 then PP.SaleChdG3+CHDP.ChdG3Price/dbo.IfZero(HAP.ChdAgeG3,1) else 0 end, 
                            SaleChdG4= Case when HAP.ChdAgeG4>0 then PP.SaleChdG4+CHDP.ChdG4Price/dbo.IfZero(HAP.ChdAgeG4,1) else 0 end, 
                            DepFlight=@DepFlight, RetFlight=@RetFlight,
                            PP.SaleAdl, PP.SaleExtBed1, PP.SaleExtBed2,
                            AccomFullName=
            	                case when HAP.Adult>0 then Str(HAP.Adult,1)+' '+Cast(IsNull(PRM.PLAdlCap,'Adl') as varchar(100)) else '' end+ 
            	                case when HAP.ChdAgeG1>0 and PP.ChdG1Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG1)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG1Age1)+'-'+Convert(VarChar(5),PP.ChdG1Age2)+')' else '' end+
            	                case when HAP.ChdAgeG2>0 and PP.ChdG2Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG2)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG2Age1)+'-'+Convert(VarChar(5),PP.ChdG2Age2)+')' else '' end+
            	                case when HAP.ChdAgeG3>0 and PP.ChdG3Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG3)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG3Age1)+'-'+Convert(VarChar(5),PP.ChdG3Age2)+')' else '' end+
            	                case when HAP.ChdAgeG4>0 and PP.ChdG4Age2>0 then '+'+Convert(VarChar(5),HAP.ChdAgeG4)+' '+Cast(IsNull(PRM.PLChdCap,'Chd') as varchar(100))+'('+Convert(VarChar(5),PP.ChdG4Age1)+'-'+Convert(VarChar(5),PP.ChdG4Age2)+')' else '' end,
	                        SpoAdlVal=Cast(0.0 as dec(18,2)), 
                            SpoExtB1Val=Cast(0.0 as dec(18,2)),
                            SpoExtB2Val=Cast(0.0 as dec(18,2)),
                            SpoChdGrp1Val=Cast(0.0 as dec(18,2)),
                            SpoChdGrp2Val=Cast(0.0 as dec(18,2)),
                            SpoChdGrp3Val=Cast(0.0 as dec(18,2)),
                            SpoChdGrp4Val=Cast(0.0 as dec(18,2)),
                            SpoFlightTot=Cast(0.0 as dec(18,2)),
                            SpoHotelTot=Cast(0.0 as dec(18,2)),
                            PasEBValidFlight = 1, PasEBValidHotel = 1,
                            EBFlightValid = 1, EBHotelValid = 1,
                            DepFlightForSpo='          '
            Into #Temp1 
                        From CatPriceA PA (NOLOCK) 
                        Join CatPriceP PP (NOLOCK) on PP.CatPackID=PA.CatPackID and PP.PriceAID=PA.RecNo             
                        Join #CatalogPack CP (NOLOCK) on CP.RecID=PA.CatPAckID --and CP.PackType<>'T' 
                        join Hotel H (NOLOCK) on H.Code=PA.Hotel 
                        join HotelRoom HR (NOLOCK) on HR.Hotel=PA.Hotel and HR.Code=PA.Room and IsNull(HR.VisiblePL,'Y')='Y' 
                        join HotelAccom HA (NOLOCK) on HA.Hotel=PA.Hotel and HA.Room=PA.Room and HA.Code=PA.Accom and IsNull(HA.VisiblePL,'Y')='Y' 
                        join HotelBoard HB (NOLOCK) on HB.Hotel=PA.Hotel and HB.Code=PA.Board and IsNull(HB.VisiblePL,'Y')='Y'
                        join HotelAccomPax HAP  (NOLOCK) on HAP.Hotel=PA.Hotel and HAP.Room=PA.Room and HAP.Accom=PA.Accom and IsNull(HAP.VisiblePL,'Y')='Y'                        
                        OUTER APPLY 
                        ( 
                           Select Max(Cast(isnull(Adult,0) as varchar(1)) + Cast(isnull(ChdAgeG1,0) as varchar(1)) + Cast(isnull(ChdAgeG2,0) as varchar(1)) + Cast(isnull(ChdAgeG3,0) as varchar(1)) + Cast(isnull(ChdAgeG4,0) as varchar(1))) AdlStr 
                           From CatHotelChd C 
                           where C.CatPackID=PP.CatPackID and C.RecNo=PP.HotelChdRecNo and
                                 dbo.IfZero(C.Adult,HAP.Adult)=HAP.Adult and
                                 dbo.IfZero(C.ChdAgeG1,HAP.ChdAgeG1)=HAP.ChdAgeG1 and
                                 dbo.IfZero(C.ChdAgeG2,HAP.ChdAgeG2)=HAP.ChdAgeG2 and
                                 dbo.IfZero(C.ChdAgeG3,HAP.ChdAgeG3)=HAP.ChdAgeG3 and
                                 dbo.IfZero(C.ChdAgeG4,HAP.ChdAgeG4)=HAP.ChdAgeG4 and
                                 dbo.IfZero(C.ChdAgeG1+C.ChdAgeG2+C.ChdAgeG3+C.ChdAgeG4,HAP.ChdAgeG1+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4)=HAP.ChdAgeG1+HAP.ChdAgeG2+HAP.ChdAgeG3+HAP.ChdAgeG4
                        ) CHDM
                        OUTER APPLY
                        (
                        Select Top 1 * From CatHotelChd C (NOLOCK) 
                        Where C.CatPackID=PP.CatPackID and C.RecNo=PP.HotelChdRecNo and
                              (Cast(isnull(Adult,0) as varchar(1)) + Cast(isnull(ChdAgeG1,0) as varchar(1)) + Cast(isnull(ChdAgeG2,0) as varchar(1)) + Cast(isnull(ChdAgeG3,0) as varchar(1)) + Cast(isnull(ChdAgeG4,0) as varchar(1))) = CHDM.AdlStr
                        ) CHD
                        OUTER APPLY
                        (
                        Select *
                        From dbo.HotChdCalc(IsNull(CHD.ChdG1PriceN1,0),IsNull(CHD.ChdG1PriceN2,0),IsNull(CHD.ChdG1PriceN3,0),IsNull(CHD.ChdG1PriceN4,0),
                                            IsNull(CHD.ChdG2PriceN1,0),IsNull(CHD.ChdG2PriceN2,0),IsNull(CHD.ChdG2PriceN3,0),IsNull(CHD.ChdG2PriceN4,0),
                                            IsNull(CHD.ChdG3PriceN1,0),IsNull(CHD.ChdG3PriceN2,0),IsNull(CHD.ChdG3PriceN3,0),IsNull(CHD.ChdG3PriceN4,0),
                                            IsNull(CHD.ChdG4PriceN1,0),IsNull(CHD.ChdG4PriceN2,0),IsNull(CHD.ChdG4PriceN3,0),IsNull(CHD.ChdG4PriceN4,0),
                                            HAP.ChdAgeG1,HAP.ChdAgeG2,HAP.ChdAgeG3,HAP.ChdAgeG4,H.ChdCalcSale,IsNull(CHD.ChdG1Out,'N'),
                                            PP.ChdG1Age1,PP.ChdG1Age2,PP.ChdG2Age1,PP.ChdG2Age2,PP.ChdG3Age1,PP.ChdG3Age2,PP.ChdG4Age1,PP.ChdG4Age2,
                                            PA.CatPackID,0,0,0,0)
                        ) CHDP
                        join HotelCat HC (NOLOCK) on HC.Code=H.Category
                        Left Join ParamPrice PRM (NOLOCK) on PRM.Market=CP.Market
                        Where Exists(Select RecID From CatPackMarket Where CatPackID=PA.CatPackID and Market=@Market and (Operator=@Operator or Operator='')) And
                              CP.RecID=@CatPackID And
                              PA.RecNo=@ARecNo And
                              PP.RecNO=@PRecNo And
                              HAP.RecID=@HAPRecNo And
                              Exists(Select RecID From #AgencyCP Where RecID=PA.CatPackID)
                        ");

            sb.Append(@"Select P.*, 
        	              StopSaleGuar=dbo.ufn_StopSaleCnt(P.Hotel, P.Room, P.Accom, P.Board, @Market, @Agency, P.CatPackID, P.HotLocation, -2, P.CheckIn, P.CheckIn+P.Night, 0, 1), 
                          StopSaleStd=dbo.ufn_StopSaleCnt(P.Hotel, P.Room, P.Accom, P.Board, @Market, @Agency, P.CatPackID, P.HotLocation, 2, P.CheckIn, P.CheckIn+P.Night, 0, 1), 
        	              DepCityName=(Select Top 1 Name From #Location WHERE RecID=P.DepCity), 
        	              ArrCityName=(Select Top 1 Name From #Location WHERE RecID=P.ArrCity), 
        	              HotLocationName=(Select Top 1 Name From #Location WHERE RecID=P.HotLocation), 
                          EBAmount= Case When p.PasEBValid='Y' Then
				                        dbo.ufn_GetEBAmountPL(P.CatPackID,P.ARecNo,P.PRecNo,P.HapRecID,P.Adl,P.ChdG1Age1,P.ChdG1Age2,P.ChdG2Age1,P.ChdG2Age2,P.ChdG3Age1,P.ChdG3Age2,P.ChdG4Age1,P.ChdG4Age2,
									                          P.HotelChdRecNo,P.ChdCalcSale,P.HAdult,P.HChdAgeG1,P.HChdAgeG2,P.HChdAgeG3,P.HChdAgeG4,0,@CustomRegID,P.HotChdPrice)
			                        Else 0 End,
        	              0 AS FreeAllot, EBPerc = Cast(0 AS decimal(18, 5)), EBValidDate = Cast(73048 AS DateTime), 0 AS AgencyEB 
                        Into #Temp2 
                        From #Temp1 P ");

            sb.Append(@"Select Distinct Hotel, Room, Board, PasEBValid, HolPack, CheckIn, Night, Operator, Market 
                        Into #EBPas1 
                        From #Temp2 ");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040014038"))
                sb.Append(@"Select *, EBPer=Case When PasEBValid='Y' Then 
        	                   (Select Top 1 Cast(Cast(FLOOR(isnull(PasEBPer,0)*100) As int) as VarChar(5))+ ';' + Cast(Convert(int ,SaleEndDate) As VarChar(12)) 
        		                From PasEB EB With (NOLOCK) 
        		                Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID 
        		                Left Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=X.Hotel and (EBH.Room='' or EBH.Room=X.Room) And (EBH.Board='' or EBH.Board=X.Board)
        		                Left Join GrpHolPackDet PG on PG.Groups = EB.HolPackGrp and PG.GrpType='P' 
        		                Where Operator=@Operator and Market=@Market And OwnerOperator = X.Operator And OwnerMarket=X.Market 
        			                and X.PasEBValid = 'Y' 
        			                And Exists(Select Distinct RecID From 
        						                ( 
        							                Select EB1.RecID 
        							                From PasEB EB1 (NOLOCK) 
        							                Where 
        								                Not Exists(Select RecID From PasEBHot Where PasEBID=EB1.RecID) 
        							                AND EB1.RecID = EB.RecID 
        							                Union All 
        							                Select EB1.RecID 
        							                From PasEB EB1 (NOLOCK) 
        							                Where 
        								                Exists(Select RecID From PasEBHot Where PasEBID=EB1.RecID and PasEBHot.Hotel=X.Hotel)  
        							                AND EB1.RecID = EB.RecID 
        						                ) XX1 
        					                 ) 
        			                and ( (EB.HolPack=X.HolPack or PG.HolPack=X.HolPack) or (EB.HolPack='' and PG.HolPack is Null) ) 
        			                and dbo.DateOnly(GETDATE()) between SaleBegDate and SaleEndDate and X.CheckIn between ResBegDate and ResEndDate 
        			                and (X.Night between IsNull(MinDay,0) and IsNull(MaxDay,999)) 
        			                and SubString(DoW,DATEPART(dw,X.CheckIn),1)=1 
                                    and DateDIFF(day,dbo.DateOnly(GetDate()),X.CheckIn) between IsNull(EBP.RemFromDay,0) and IsNull(EBP.RemToDay,999)
        		                Order by EBH.Hotel Desc,EBH.Room desc, EBH.Board desc, EB.HolPack Desc,HolPackGrp Desc, EB.CrtDate Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc) 
                                Else '0;' + '73048' End 
                        INTO #EBPas 
                        From #EBPas1 X ");
            else
                if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0034, VersionControl.Equality.gt))
                sb.Append(@"Select *, EBPer=Case When PasEBValid='Y' Then 
        	                   (Select Top 1 Cast(Cast(FLOOR(isnull(PasEBPer,0)*100) As int) as VarChar(5))+ ';' + Cast(Convert(int ,SaleEndDate) As VarChar(12)) 
        		                From PasEB EB With (NOLOCK) 
        		                Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID 
        		                Left Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=X.Hotel and (EBH.Room='' or EBH.Room=X.Room) And (EBH.Board='' or EBH.Board=X.Board)
        		                Left Join GrpHolPackDet PG on PG.Groups = EB.HolPackGrp and PG.GrpType='P' 
        		                Where Operator=@Operator and Market=@Market And OwnerOperator = X.Operator And OwnerMarket=X.Market 
        			                and X.PasEBValid = 'Y' 
        			                And Exists(Select Distinct RecID From 
        						                ( 
        							                Select EB1.RecID 
        							                From PasEB EB1 (NOLOCK) 
        							                Where 
        								                Not Exists(Select RecID From PasEBHot Where PasEBID=EB1.RecID) 
        							                AND EB1.RecID = EB.RecID 
        							                Union All 
        							                Select EB1.RecID 
        							                From PasEB EB1 (NOLOCK) 
        							                Where 
        								                Exists(Select RecID From PasEBHot Where PasEBID=EB1.RecID and PasEBHot.Hotel=X.Hotel)  
        							                AND EB1.RecID = EB.RecID 
        						                ) XX1 
        					                 ) 
        			                and ( (EB.HolPack=X.HolPack or PG.HolPack=X.HolPack) or (EB.HolPack='' and PG.HolPack is Null) ) 
        			                and dbo.DateOnly(GETDATE()) between SaleBegDate and SaleEndDate and X.CheckIn between ResBegDate and ResEndDate 
        			                and (X.Night between IsNull(MinDay,0) and IsNull(MaxDay,999)) 
        			                and SubString(DoW,DATEPART(dw,X.CheckIn),1)=1 
        		                Order by EBH.Hotel Desc,EBH.Room desc, EBH.Board desc, EB.HolPack Desc,HolPackGrp Desc, EB.CrtDate Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc) 
                                Else '0;' + '73048' End 
                        INTO #EBPas 
                        From #EBPas1 X ");
            else
                sb.Append(@"Select *, EBPer=Case When PasEBValid='Y' Then 
        	                   (Select Top 1 Cast(Cast(FLOOR(isnull(PasEBPer,0)*100) As int) as VarChar(5))+ ';' + Cast(Convert(int ,SaleEndDate) As VarChar(12)) 
        		                From PasEB EB With (NOLOCK) 
        		                Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID 
        		                Left Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=X.Hotel 
        		                Left Join GrpHolPackDet PG on PG.Groups = EB.HolPackGrp and PG.GrpType='P' 
        		                Where Operator=@Operator and Market=@Market And OwnerOperator = X.Operator And OwnerMarket=X.Market 
        			                and X.PasEBValid = 'Y' 
        			                And Exists(Select Distinct RecID From 
        						                ( 
        							                Select EB1.RecID 
        							                From PasEB EB1 (NOLOCK) 
        							                Where 
        								                Not Exists(Select RecID From PasEBHot Where PasEBID=EB1.RecID) 
        							                AND EB1.RecID = EB.RecID 
        							                Union All 
        							                Select EB1.RecID 
        							                From PasEB EB1 (NOLOCK) 
        							                Where 
        								                Exists(Select RecID From PasEBHot Where PasEBID=EB1.RecID and PasEBHot.Hotel=X.Hotel)  
        							                AND EB1.RecID = EB.RecID 
        						                ) XX1 
        					                 ) 
        			                and ( (EB.HolPack=X.HolPack or PG.HolPack=X.HolPack) or (EB.HolPack='' and PG.HolPack is Null) ) 
        			                and dbo.DateOnly(GETDATE()) between SaleBegDate and SaleEndDate and X.CheckIn between ResBegDate and ResEndDate 
        			                and (X.Night between IsNull(MinDay,0) and IsNull(MaxDay,999)) 
        			                and SubString(DoW,DATEPART(dw,X.CheckIn),1)=1 
        		                Order by EBH.Hotel Desc, EB.HolPack Desc,HolPackGrp Desc, EB.CrtDate Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc) 
                                Else '0;' + '73048' End 
                        INTO #EBPas 
                        From #EBPas1 X ");

            sb.Append(@"Update x 
                        Set EBPerc = Case when y.EBPer is null Then 0 Else Cast(Substring(y.EBPer, 1, CharIndex(';', y.EBPer, 1) - 1) As Int) / 100 End, 
                            EBValidDate = Case when y.EBPer is null then Cast(73048 AS DateTime) Else Cast(Convert(int, Substring(y.EBPer, CharIndex(';', y.EBPer, 1) + 1, 12)) AS DateTime) End 
                        From #Temp2 x ,#EBPas y 
                        where	x.Hotel=y.Hotel 
            	            And x.PasEBValid=y.PasEBValid 
            	            And x.Holpack=y.Holpack 
            	            And x.CheckIn=y.CheckIn 
            	            And x.Night=y.Night ");

            //object showAgencyEB = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowAgencyEB");
            //if (showAgencyEB != null && (bool)showAgencyEB)
            //{
            sb.Append(@"--Eğer AgencyEB Yazılacak İse 
                            Select Distinct Hotel, EBValid, HolPack, CheckIn, Night, Operator, Market 
                            Into #AgencyEB1 
                            From #Temp2 

                            Select *, AgencyEB=Case When EBValid='Y' Then ( 
                			            Select Top 1 EBPer  From AgencyComEB EB (NOLOCK) 
                			            Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups=EB.AgencyGrp and AG.GrpType='P' 
                			            Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups=EB.HolPackGrp and PG.GrpType='P' 
                			            Where	OwnerOperator=x.Operator 
                				            And OwnerMarket=x.Market 
                				            And Operator=@Operator 
                				            And Market=@Market 
                				            And ((EB.Agency=(Select Case WHEN isnull(MainOffice, '')='' Then Code Else MainOffice End From Agency Where Code=@Agency) 
                				               Or AG.Agency=(Select Case WHEN isnull(MainOffice, '')='' Then Code Else MainOffice End From Agency Where Code=@Agency)) Or (EB.Agency='' And AG.Agency is Null)) 
                				            And ((EB.HolPackGrp='' And (EB.HolPack=x.HolPack Or EB.HolPack='')) Or PG.HolPack=x.HolPack) 
                				            And dbo.DateOnly(GETDATE()) Between SaleBegDate And SaleEndDate 
                				            And X.CheckIn Between ResBegDate And ResEndDate 
                			            Order By EB.Agency Desc, AgencyGrp Desc, EB.HolPack Desc, HolPackGrp Desc, SaleBegDate Desc, ResBegDate Desc 
                		            ) Else 0 End 
                            INTO #AgencyEB 
                            From #AgencyEB1 X ");

            sb.Append(@"Update x  
                            Set AgencyEB=Case When y.AgencyEB is null then 0 Else y.AgencyEB End  
                            From #Temp2 x ,#AgencyEB y  
                            where	x.Hotel=y.Hotel  
                	            And x.EBValid=y.EBValid  
                	            And x.Holpack=y.Holpack  
                	            And x.CheckIn=y.CheckIn  
                	            And x.Night=y.Night ");
            //}

            sb.Append(@"Select X.* 
                        Into #Temp3 
                        From ( 
                            Select R.*, 
                                HKB_Exists=Case When HKB.PriceType is null then 0 else 1 end,  
                                HKB_PriceType=HKB.PriceType,  
                                HKB_CalcType=HKB.CalcType, 
                                HKB_AdlPrice=HKB.AdlPrice, 
                                HKB_SupPer=HKB.SupPer, 
                                HKB_EBOpt=HKB.EBOpt, 
                                HKB_Cur=HKB.Cur,  
                                HKB_Amount=Cast(0 as dec(18,2)), 
                                SPWithEB=R.CalcSalePrice
                            From #Temp2 R 
                            -- 2010-02-11 KickBack 
                            OUTER APPLY  
                            ( 
                                  Select Top 1 PriceType,CalcType,AdlPrice,SupPer,EBOpt,Cur 
                                  From HotelFeed (NOLOCK) HF 
                                  Where Hotel=R.Hotel and Status=1 and ForSale=1 and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate 
                                  and R.CheckIn between BegDate and EndDate 
                                  and R.Night between IsNull(MinDay,0) and IsNull(MaxDay,999) 
                                  and SubString(AccomDays,DATEPART(dw,R.CheckIn),1)=1 
                                  and (Room='' or Room=R.Room) 
                                  and (Board='' or Board=R.Board) 
                                  Order by Room Desc, Board Desc,CrtDate Desc 
                            ) HKB  
                            -- 2010-02-11 KickBack 
                        ) X                                                 
                        ");

            sb.Append(@"
                        Select R.*, 
                       ");
            //if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            //{
            //    sb.Append("       DepSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.CatPackID, R.PRecNo, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, R.DepCity, R.ArrCity, 0, null),  \n");
            //    sb.Append("       RetSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.CatPackID, R.PRecNo, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, R.DepCity, R.ArrCity, 1, null), \n");
            //}
            //else
            //{
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200003"))
            {
                sb.Append("       DepSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, 0, null), \n");
                sb.Append("       RetSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, null, R.HolPack, 1, null), \n");
            }
            else
            {
                sb.Append("       DepSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.CatPackID, R.PRecNo, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, R.HolPack, R.DepCity, R.ArrCity, 0, null),  \n");
                sb.Append("       RetSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.CatPackID, R.PRecNo, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, R.HolPack, R.DepCity, R.ArrCity, 1, null), \n");
            }

            //sb.Append("       DepSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.CatPackID, R.PRecNo, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, R.HolPack, R.DepCity, R.ArrCity, 0, null),  \n");
            //sb.Append("       RetSeat = dbo.ufn_Get_DepRet_FlightSeatWeb(R.Operator, R.Market, R.CatPackID, R.PRecNo, R.DepFlight, R.CheckIn, R.Night, R.FlightClass, R.HolPack, R.DepCity, R.ArrCity, 1, null), \n");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050005129"))
                sb.Append("       FreeRoom = dbo.ufn_HotelFreeAmount(R.Market, @Office, @Agency, R.Hotel, R.Room, R.Accom, -1, R.CheckIn, R.Night, 11), \n");
            else
                sb.Append("       FreeRoom = dbo.ufn_HotelFreeAmount(R.Market, @Office, @Agency, R.Hotel, R.Room, R.Accom, -1, R.CheckIn, R.Night, 1), \n");
            sb.Append("       Cur=@Cur,  \n");
            sb.Append("       CurrentCur=isnull(@Cur, R.SaleCur), \n");
            sb.Append("       Description = R.CPDescription, OprText = (Case When isnull(H.OprText, '') = '' then O.NameS else H.OprText end) \n");
            sb.Append("Into #Temp4 \n");
            sb.Append("From #Temp3 R \n");
            sb.Append("Join HolPack H (NOLOCK) ON H.Code = R.HolPack \n");
            sb.Append("Join Operator O (NOLOCK) ON O.Code = R.Operator \n");

            sb.Append("Select * From #Temp4 \n");

            string dateFormat = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').ToString().Replace("dd", "d").Replace("yy", "y").Replace("MM", "m").Replace(UserData.Ci.DateTimeFormat.DateSeparator, "");
            string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator[0].ToString();

            List<SearchResult> result = new List<SearchResult>();
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sb.ToString());

            try
            {
                db.AddInParameter(dbCommand, "CustomRegID", DbType.String, UserData.CustomRegID);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Office", DbType.String, UserData.OprOffice);
                db.AddInParameter(dbCommand, "DateFormat", DbType.String, dateFormat);
                db.AddInParameter(dbCommand, "Seperator", DbType.String, dateSeperator);
                db.AddInParameter(dbCommand, "Cur", DbType.String, Cur);
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                db.AddInParameter(dbCommand, "ARecNo", DbType.Int32, ARecNo);
                db.AddInParameter(dbCommand, "PRecNo", DbType.Int32, PRecNo);
                db.AddInParameter(dbCommand, "HAPRecNo", DbType.Int32, HAPRecNo);
                db.AddInParameter(dbCommand, "DepFlight", DbType.String, DepFlight);
                db.AddInParameter(dbCommand, "RetFlight", DbType.String, RetFlight);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        SearchResult r = new SearchResult();
                        r.RefNo = RoomNr;
                        r.RoomNr = RoomNr;
                        r.Calculated = true;
                        r.CatPackID = CatPackID;
                        r.ARecNo = ARecNo;
                        r.PRecNo = PRecNo;
                        r.HAPRecId = HAPRecNo;
                        r.Hotel = Conversion.getStrOrNull(oReader["Hotel"]);
                        r.HotelName = Conversion.getStrOrNull(oReader["HotelName"]);
                        r.Room = Conversion.getStrOrNull(oReader["Room"]);
                        r.RoomName = Conversion.getStrOrNull(oReader["RoomName"]);
                        r.Board = Conversion.getStrOrNull(oReader["Board"]);
                        r.BoardName = Conversion.getStrOrNull(oReader["BoardName"]);
                        r.Accom = Conversion.getStrOrNull(oReader["Accom"]);
                        r.AccomName = Conversion.getStrOrNull(oReader["AccomName"]);
                        r.AccomFullName = Conversion.getStrOrNull(oReader["AccomFullName"]);
                        r.Night = (Int16)oReader["Night"];
                        r.CheckIn = (DateTime)oReader["CheckIn"];
                        r.CheckOut = r.CheckIn.Value.AddDays(r.Night.Value);
                        r.SaleCur = Conversion.getStrOrNull(oReader["CurrentCur"]);
                        r.NetCur = Conversion.getStrOrNull(oReader["NetCur"]);
                        r.StopSaleGuar = Conversion.getInt16OrNull(oReader["StopSaleGuar"]);
                        r.StopSaleStd = Conversion.getInt16OrNull(oReader["StopSaleStd"]);
                        r.HotLocationName = Conversion.getStrOrNull(oReader["HotLocationName"]);
                        r.HotCat = Conversion.getStrOrNull(oReader["HotCat"]);
                        r.DepCity = Conversion.getInt32OrNull(oReader["DepCity"]);
                        r.DepCityName = Conversion.getStrOrNull(oReader["DepCityName"]);
                        r.ArrCity = Conversion.getInt32OrNull(oReader["ArrCity"]);
                        r.ArrCityName = Conversion.getStrOrNull(oReader["ArrCityName"]);
                        r.DepFlight = Conversion.getStrOrNull(oReader["DepFlight"]);
                        r.RetFlight = Conversion.getStrOrNull(oReader["RetFlight"]);
                        r.EBValidDate = Conversion.getDateTimeOrNull(oReader["EBValidDate"]);
                        r.AgencyEB = Conversion.getDecimalOrNull(oReader["AgencyEB"]);
                        r.LastPrice = Conversion.getDecimalOrNull(oReader["SPWithEB"]);
                        r.DepSeat = Conversion.getInt32OrNull(oReader["DepSeat"]);
                        r.RetSeat = (int)oReader["RetSeat"];
                        r.FreeRoom = (int)oReader["FreeRoom"];
                        r.CurrentCur = Conversion.getStrOrNull(oReader["CurrentCur"]);
                        r.PLCur = Conversion.getStrOrNull(oReader["PLCur"]);
                        r.Description = Conversion.getStrOrNull(oReader["Description"]);
                        r.OprText = Conversion.getStrOrNull(oReader["OprText"]);
                        r.SaleSPONo = Conversion.getInt32OrNull(oReader["SaleSPONo"]);
                        r.EBPerc = Conversion.getDecimalOrNull(oReader["EBPerc"]);
                        r.SaleEndDate = Conversion.getDateTimeOrNull(oReader["SaleEndDate"]);
                        r.HolPack = Conversion.getStrOrNull(oReader["HolPack"]);
                        r.HAdult = (Int16)oReader["HAdult"];
                        r.HChdAgeG1 = (Int16)oReader["HChdAgeG1"];
                        r.HChdAgeG2 = (Int16)oReader["HChdAgeG2"];
                        r.HChdAgeG3 = (Int16)oReader["HChdAgeG3"];
                        r.HChdAgeG4 = (Int16)oReader["HChdAgeG4"];
                        r.ChdG1Age1 = Conversion.getDecimalOrNull(oReader["ChdG1Age1"]);
                        r.ChdG1Age2 = Conversion.getDecimalOrNull(oReader["ChdG1Age2"]);
                        r.ChdG2Age1 = Conversion.getDecimalOrNull(oReader["ChdG2Age1"]);
                        r.ChdG2Age2 = Conversion.getDecimalOrNull(oReader["ChdG2Age2"]);
                        r.ChdG3Age1 = Conversion.getDecimalOrNull(oReader["ChdG3Age1"]);
                        r.ChdG3Age2 = Conversion.getDecimalOrNull(oReader["ChdG3Age2"]);
                        r.ChdG4Age1 = Conversion.getDecimalOrNull(oReader["ChdG4Age1"]);
                        r.ChdG4Age2 = Conversion.getDecimalOrNull(oReader["ChdG4Age2"]);
                        r.Operator = Conversion.getStrOrNull(oReader["Operator"]);
                        r.Market = Conversion.getStrOrNull(oReader["Market"]);
                        r.FlightClass = Conversion.getStrOrNull(oReader["FlightClass"]);
                        r.plSpoExists = Conversion.getBoolOrNull(oReader["PLSpoExists"]);

                        return r;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CalendarDays> getPLCheckin(User UserData, Int16? Category, int? arrCity, ref string errorMsg)
        {
            List<CalendarDays> records = new List<CalendarDays>();
            string tsql = @"Select Distinct Cpp.CheckIn From CatalogPack Cp (NOLOCK)
                            Join CatPriceP Cpp (NOLOCK) ON Cp.RecID=Cpp.CatPackID
                            Left Join CatPackMarket Cpm (NOLOCK) ON Cpm.CatPackID=Cp.RecID 
                            Where Category=@Category
                              And Cp.Ready='Y'
                              And CP.WebPub = 'Y' And CP.WebPubDate <= GetDate() 
                              And dbo.DateOnly(GetDate()) between Cp.SaleBegDate And Cp.SaleEndDate 
                              And Cpp.CheckIn>=GetDate()-1
                              And Cpm.Market=@Market 
                              And (Cpm.Operator=@Operator OR Cpm.Operator='')                            
                              ";
            if (arrCity.HasValue)
                tsql += string.Format("And Cp.ArrCity={0} ", arrCity.Value);

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Category", DbType.Int16, Category);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        CalendarDays record = new CalendarDays();
                        DateTime CheckIn = (DateTime)R["CheckIn"];
                        record.Year = CheckIn.Year;
                        record.Month = CheckIn.Month;
                        record.Day = CheckIn.Day;
                        record.Text = "";
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> getPackPriceSearchV2(User UserData, SearchCriteria filter, bool checkAvailableRoom, SearchType sType, bool checkAvailableFlightSeat, bool isG7, Guid? logID, ref string errorMsg)
        {

            string resortWhereStr = string.Empty;
            string categoryWhereStr = string.Empty;
            string hotelWhereStr = string.Empty;
            string roomWhereStr = string.Empty;
            string boardWhereStr = string.Empty;
            string facilityWhereStr = string.Empty;
            List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);


            if (!string.IsNullOrEmpty(filter.Resort))
            {
                #region filter Resort
                if (filter.Resort.Split('|').Length > 0)
                {
                    if (filter.Resort.Split('|').Length == 1)
                    {
                        resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", filter.Resort.Split('|')[0]);
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                    else
                    {
                        foreach (string r in filter.Resort.Split('|'))
                        {
                            Location _location = locations.Find(f => f.RecID == Conversion.getInt32OrNull(r));
                            if (_location != null)
                            {
                                if (_location.Type == 3)
                                {
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                                    foreach (Location rL in locations.Where(w => w.Parent == _location.RecID))
                                        resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", rL.RecID);
                                }
                                else
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                            }
                        }
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                }
                else
                    resortWhereStr = string.Empty;
                #endregion
            }

            if (!string.IsNullOrEmpty(filter.Category))
            {
                #region filter Category
                foreach (string r in filter.Category.Split('|'))
                    categoryWhereStr += string.Format("INSERT INTO @Category(Code) VALUES('{0}') \n", r);
                if (categoryWhereStr.Length > 0)
                    categoryWhereStr = "DECLARE @Category TABLE (Code VarChar(10)) \n" + categoryWhereStr;
                else
                    categoryWhereStr = string.Empty;
                #endregion
            }

            if (!string.IsNullOrEmpty(filter.Hotel))
            {
                #region filter Hotel
                foreach (string r in filter.Hotel.Split('|'))
                    hotelWhereStr += string.Format("INSERT INTO @Hotel(Code) VALUES('{0}') \n", r);
                if (hotelWhereStr.Length > 0)
                    hotelWhereStr = "DECLARE @Hotel TABLE (Code VarChar(10)) \n" + hotelWhereStr;
                else
                    hotelWhereStr = string.Empty;
                #endregion
            }

            if (!string.IsNullOrEmpty(filter.Room))
            {
                #region filter Room
                foreach (string r in filter.Room.Split('|'))
                    roomWhereStr += string.Format("INSERT INTO @Room(Code) VALUES('{0}') \n", r);
                if (roomWhereStr.Length > 0)
                    roomWhereStr = "DECLARE @Room TABLE (Code VarChar(10)) \n" + roomWhereStr;
                else
                    roomWhereStr = string.Empty;
                #endregion
            }

            if (!string.IsNullOrEmpty(filter.Board))
            {
                #region filter Board
                foreach (string r in filter.Board.Split('|'))
                    boardWhereStr += string.Format("INSERT INTO #tmpBoard(Code) VALUES('{0}') \n", r);
                if (boardWhereStr.Length > 0)
                    boardWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpBoard') is not null Drop Table dbo.#tmpBoard 
Create Table #tmpBoard (Code VarChar(10) COLLATE Latin1_General_CI_AS) " + boardWhereStr;
                else
                    boardWhereStr = string.Empty;
                #endregion
            }

            if (!string.IsNullOrEmpty(filter.Facility))
            {
                #region filter Facility
                foreach (string r in filter.Facility.Split('|'))
                    facilityWhereStr += string.Format("INSERT INTO @Facility(Code) VALUES('{0}') \n", r);
                if (facilityWhereStr.Length > 0)
                    facilityWhereStr = "DECLARE @Facility TABLE (Code varChar(10)) \n" + facilityWhereStr;
                else
                    facilityWhereStr = string.Empty;
                #endregion
            }

            StringBuilder q = new StringBuilder();

            #region Query Header
            q.Append("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED \n");
            q.Append("SET NOCOUNT ON \n");
            q.Append("SET DATEFIRST 1 \n");
            q.Append("Declare @CustomRegId VarChar(20), @SaleBegDate DateTime \n");
            q.Append("Set @SaleBegDate=GetDate() \n");

            if (!string.IsNullOrEmpty(resortWhereStr))
                q.Append(resortWhereStr + "\n");
            if (!string.IsNullOrEmpty(categoryWhereStr))
                q.Append(categoryWhereStr + "\n");
            if (!string.IsNullOrEmpty(hotelWhereStr))
                q.Append(hotelWhereStr + "\n");
            if (!string.IsNullOrEmpty(roomWhereStr))
                q.Append(roomWhereStr + "\n");
            if (!string.IsNullOrEmpty(boardWhereStr))
                q.Append(boardWhereStr + "\n");
            if (!string.IsNullOrEmpty(facilityWhereStr))
                q.AppendLine(facilityWhereStr);

            q.Append("if OBJECT_ID('TempDB.dbo.#PriceList') is not null Drop Table dbo.#PriceList \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#AgencyEB1') is not null Drop Table dbo.#AgencyEB1 \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#AgencyEB') is not null Drop Table dbo.#AgencyEB \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#PriceListEB') is not null Drop Table dbo.#PriceListEB \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#PriceListStopSale') is not null Drop Table dbo.#PriceListStopSale \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location \n");

            q.Append("Declare @GetEBLastRecID SmallInt, @GetEBPLastRecID SmallInt \n");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050025168"))
            {
                q.Append(@"
Declare @EBCalcType smallint
--F.B. 08.03.2016 Last EB işi parametreye bağlandı.
Select @EBCalcType = isnull(EBCalcType,0) From ParamPrice (NOLOCK) Where Market=''

Set @GetEBLastRecID = @EBCalcType
Set @GetEBPLastRecID = @EBCalcType
");
            }
            else
            {
                q.Append("Set @GetEBLastRecID=0 \n");
                q.Append("Set @GetEBPLastRecID=0 \n");
                q.Append("if @CustomRegID in ('0970801','1210601','0969801','1323201','0855201')  \n");
                q.Append("begin \n");
                q.Append("    Set @GetEBLastRecID=1 \n");
                q.Append("    Set @GetEBPLastRecID=1 \n");
                q.Append("end  \n");
            }
            q.Append("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Village,Town,City,Country \n");
            q.Append("Into #Location \n");
            q.Append("From Location (NOLOCK) \n");

            q.Append("Select @CustomRegID=CustomRegID From ParamSystem (NOLOCK) Where isnull(Market, '')='' \n");
            #endregion

            #region Main Search Query
            q.Append("Select P.CatPackID,P.ARecNo,P.Hotel,P.Room,P.Accom,P.Board, \n");
            q.Append("  P.Adl,P.Chd,P.PriceAID,P.PRecNo,P.CheckIn,CheckOut=(P.CheckIn+P.Night),P.Night,P.HotCheckIn,P.HotNight, \n");
            q.Append("	P.SaleAdl,P.SaleExtBed1,P.SaleExtBed2,P.ChdG1Age1,P.ChdG1Age2,P.SaleChdG1,P.ChdG2Age1,P.ChdG2Age2,P.SaleChdG2, \n");
            q.Append("  P.ChdG3Age1,P.ChdG3Age2,P.SaleChdG3,P.ChdG4Age1,P.ChdG4Age2,P.SaleChdG4, \n");
            q.Append("  P.OSalePrice,PLCur=P.SaleCur,P.SaleCur,P.SaleBegDate,P.SaleEndDate,CPName,CPNameLID,[Description]=CPDescription, \n");
            q.Append("  P.HolPack,P.Operator,P.Market,P.SaleSPONo,P.PubDate,P.CWebPub,P.WebPubDate,P.EBValid, \n");
            q.Append("  P.DepCity,P.ArrCity,P.FlightClass,P.HotelChdRecNo,P.HAPRecID,P.HAdult,P.HChdAgeG1, \n");
            q.Append("  P.HChdAgeG2,P.HChdAgeG3,P.HChdAgeG4,P.HotelName,P.HotCat,HotelLocation=P.HotLocation,P.RoomName,P.AccomName,P.BoardName, \n");
            q.Append("  P.AccomFullName,P.CalcSalePrice,P.ChdCalcSale,P.PasEBValid, \n");
            q.Append("  OldSalePrice=P.CalcSalePrice,HotChdPrice,P.NetCur,P.Direction,P.Category, \n");
            q.Append("  StopSaleGuar=dbo.ufn_StopSaleCnt(P.Hotel,P.Room,P.Accom,P.Board,@Market,@Agency,P.CatPackID,P.HotLocation,-2,P.HotCheckIn,P.HotCheckIn+P.HotNight,0,1), \n");
            q.Append("  StopSaleStd=dbo.ufn_StopSaleCnt(P.Hotel,P.Room,P.Accom,P.Board,@Market,@Agency,P.CatPackID,P.HotLocation,2,P.HotCheckIn,P.HotCheckIn+P.HotNight,0,1), \n");
            q.Append("  SpoPer=Cast(0.0 as dec(18,2)), SpoAdlVal=Cast(0.0 as dec(18,2)), \n");
            q.Append("  SpoExtB1Val=Cast(0.0 as dec(18,2)), \n");
            q.Append("  SpoExtB2Val=Cast(0.0 as dec(18,2)), \n");
            q.Append("  SpoChdGrp1Val=Cast(0.0 as dec(18,2)), \n");
            q.Append("  SpoChdGrp2Val=Cast(0.0 as dec(18,2)), \n");
            q.Append("  SpoChdGrp3Val=Cast(0.0 as dec(18,2)), \n");
            q.Append("  SpoChdGrp4Val=Cast(0.0 as dec(18,2)), \n");
            q.Append("  SpoFlightTot=Cast(0.0 as dec(18,2)), \n");
            q.Append("  SpoHotelTot=Cast(0.0 as dec(18,2)), \n");
            q.Append("  PasEBValidFlight=1, \n");
            q.Append("  PasEBValidHotel=1, \n");
            q.Append("  EBFlightValid=1, \n");
            q.Append("  EBHotelValid=1, \n");
            q.Append("  P.PlSpoExists, \n");
            q.Append("  TransportType=Cast(0 As smallint), \n");
            q.Append("  DepFlightForSpo='          ', \n");
            if (UserData.ShowFlight && filter.SType == SearchType.PackageSearch)
            {
                q.Append("  DepFlight=isnull(FDD.FlightNo, '          '), \n");
                q.Append("  RetFlight=isnull(FDD.NextFlight, '          '), \n");
                q.Append("  DFlight=isnull(FDD.FlightNo, ''), \n");
                q.Append("  DepFlightTime=isnull(FDD.DepTime, Cast(0 AS DateTime)), \n");
                q.Append("  RetFlightTime=isnull(FDR.DepTime, Cast(0 AS DateTime)), \n");
            }
            else
            {
                q.Append("  DepFlight='          ', \n");
                q.Append("  RetFlight='          ', \n");
                q.Append("  DFlight='', \n");
                q.Append("  DepFlightTime=Cast(0 AS DateTime), \n");
                q.Append("  RetFlightTime=Cast(0 AS DateTime), \n");
            }

            q.Append("    AgencyEB=0, \n");
            q.Append("    DepSeat=null, \n");
            q.Append("    RetSeat=null, \n");
            q.Append("    FreeAllot=Cast(0 As int), \n");
            q.Append("    EBPerc=0, \n");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040018050"))
                q.Append("    AutoStop=isnull(P.AutoStop,0), \n");
            else
                q.Append("    AutoStop=Cast(0 As bit), \n");

            q.Append("    FlightValidDate=Cast(73048 AS DateTime), \n");
            q.Append("    HotelValidDate=Cast(73048 AS DateTime), \n");
            q.Append("    DepServiceType=isnull(CP.DepServiceType,''), RetServiceType=isnull(CP.RetServiceType,''), \n");
            q.Append("    HNameLID,HRNameLID,HANameLID,HBNameLID \n");
            q.Append("Into #PriceList \n");
            q.Append("From PriceListExtV P (Nolock) \n");
            q.Append("Join CatalogPack CP (Nolock) ON P.CatPackID=CP.RecID \n");
            if (UserData.ShowFlight && filter.SType == SearchType.PackageSearch)
            {
                q.Append("Left JOIN HolPackPlan H (NOLOCK) on H.HolPack=P.HolPack and H.Service='FLIGHT' and H.Departure=P.DepCity and H.Arrival = P.ArrCity \n");
                q.Append("Left JOIN HolPackSer HS (NOLOCK) on HS.HolPack=H.HolPack and HS.StepNo=H.StepNo \n");
                q.Append("Left JOIN FlightDay FDD (NOLOCK) on FDD.FlightNo=HS.ServiceItem and FDD.FlyDate=P.CheckIn \n");
                q.Append("Left JOIN FlightDay FDR (NOLOCK) on FDR.FlightNo=FDD.NextFlight and FDR.FlyDate=(P.CheckIn+P.Night) \n");
            }
            #endregion Main Search Query

            #region Where

            q.Append("Where P.SaleEndDate>=dbo.DateOnly(@SaleBegDate) \n");
            q.Append("  And P.Ready='Y' \n");
            q.Append("  And P.HotStatus=1 \n");
            q.Append("  And P.CWebPub='Y' \n");
            q.Append("  And P.WebPubDate<=@SaleBegDate \n");
            q.Append("  And dbo.DateOnly(@SaleBegDate) BetWeen P.SaleBegDate And P.SaleEndDate \n");
            q.Append("  And Exists(Select * From CatPackMarket Where CatPackID=P.CatPackID and Market=@Market and (Operator=@Operator or Operator='')) \n");
            q.Append("  And isnull(P.PLFreeAmount,32000)>0 \n");
            if (UserData.ShowFlight && filter.SType == SearchType.PackageSearch)
                q.Append("  And ( HS.ServiceItem is null or FDD.FlightNo is not null) \n ");

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
            {
                if (filter.ExpandDate == 0)
                    q.Append("  And ((P.Checkin+(P.Night-P.HotNight))=@CIn1) \n");
                else
                    q.Append("  And ((P.Checkin+(P.Night-P.HotNight)) Between @CIn1 AND @CIn2) \n");
            }
            else
            {
                if (filter.ExpandDate == 0)
                    q.Append("  And P.CheckIn=@CIn1 \n");
                else
                    q.Append("  And (P.CheckIn Between @CIn1 AND @CIn2) \n");
            }
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
            {
                q.Append("  And (IsNull(P.HotNight, P.Night) between @Night1 and @Night2) \n");
            }
            else
            {
                if (filter.NightFrom != filter.NightTo && filter.NightTo >= filter.NightFrom)
                    q.Append("  And (P.Night between @Night1 and @Night2) \n");
                else
                    q.Append("  And P.Night=@Night1 \n");
            }
            if (filter.DepCity.HasValue)
                q.AppendFormat("  And P.DepCity={0} \n", filter.DepCity.Value.ToString());
            if (filter.ArrCity.HasValue)
            {
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                {
                    Location loc = locations.Find(f => f.RecID == filter.ArrCity.Value);
                    if (loc != null && loc.Parent == loc.RecID)
                        q.AppendFormat("  And P.ArrCity in (Select City From Location (NOLOCK) Where Country={0}) \n", filter.ArrCity.Value.ToString());
                    else
                        q.AppendFormat("  And P.ArrCity={0} \n", filter.ArrCity.Value.ToString());
                }
                else
                    q.AppendFormat("  And P.ArrCity={0} \n", filter.ArrCity.Value.ToString());
            }
            if (!isG7)
            {
                q.AppendFormat("  And P.HAdult={0} \n", filter.RoomsInfo.FirstOrDefault().Adult);
                q.AppendFormat("  And P.HChdAgeG1+P.HChdAgeG2+P.HChdAgeG3+P.HChdAgeG4={0} \n", filter.RoomsInfo.FirstOrDefault().Child);
            }
            else
            {
                q.AppendFormat("  And P.HAdult<={0} \n", filter.RoomsInfo.FirstOrDefault().Adult);
                q.AppendFormat("  And P.HChdAgeG1+P.HChdAgeG2+P.HChdAgeG3+P.HChdAgeG4<={0} \n", filter.RoomsInfo.FirstOrDefault().Child);
            }
            if (filter.RoomsInfo.FirstOrDefault().Child > 0)
                q.AppendFormat("  And dbo.ChdAgeControl4AgeV2({0},{1},{2},{3},{4},P.HChdAgeG1,P.HChdAgeG2,P.HChdAgeG3,P.HChdAgeG4,P.ChdG1Age1,P.ChdG1Age2,P.ChdG2Age1,P.ChdG2Age2,P.ChdG3Age1,P.ChdG3Age2,P.ChdG4Age1,P.ChdG4Age2)=1 \n",
                                        filter.RoomsInfo.FirstOrDefault().Child,
                                        filter.RoomsInfo.FirstOrDefault().Chd1Age.HasValue ? filter.RoomsInfo.FirstOrDefault().Chd1Age.ToString() : "null",
                                        filter.RoomsInfo.FirstOrDefault().Chd2Age.HasValue ? filter.RoomsInfo.FirstOrDefault().Chd2Age.ToString() : "null",
                                        filter.RoomsInfo.FirstOrDefault().Chd3Age.HasValue ? filter.RoomsInfo.FirstOrDefault().Chd3Age.ToString() : "null",
                                        filter.RoomsInfo.FirstOrDefault().Chd4Age.HasValue ? filter.RoomsInfo.FirstOrDefault().Chd4Age.ToString() : "null");

            q.AppendFormat("  And Exists(Select Distinct RecID From	( \n");
            q.AppendFormat("  	Select CP.RecID  \n");
            q.AppendFormat("  	From CatalogPack CP (NOLOCK) \n");
            q.AppendFormat("    	Where  \n");
            q.AppendFormat("    		Not Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID)  \n");
            q.AppendFormat("    	AND CP.RecID = P.CatPackID \n");
            q.AppendFormat("    	union all \n");
            q.AppendFormat("    	Select CP.RecID  \n");
            q.AppendFormat("    	From CatalogPack CP (NOLOCK) \n");
            q.AppendFormat("    	Where  \n");
            q.AppendFormat("    		Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=@Agency)  \n");
            q.AppendFormat("    	AND CP.RecID = P.CatPackID \n");
            q.AppendFormat("    	union all \n");
            q.AppendFormat("    	Select CP.RecID  \n");
            q.AppendFormat("    	From CatalogPack CP (NOLOCK) \n");
            q.AppendFormat("    	Where  \n");
            q.AppendFormat("    		Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=(Select MainOffice From Agency (NOLOCK) Where Code=@Agency))  \n");
            q.AppendFormat("    	AND CP.RecID = P.CatPackID \n");
            q.AppendFormat("    ) CPA \n");
            q.AppendFormat("  )	 \n");

            switch (filter.SType)
            {
                case SearchType.PackageSearch:
                    q.Append("	And P.PackType = 'H'  \n");
                    break;
                case SearchType.TourPackageSearch:
                    q.Append("	And P.PackType = 'T'  \n");
                    break;
                case SearchType.OnlyHotelSearch:
                    q.Append("	And P.PackType = 'O'  \n");
                    break;
                default:
                    q.Append("	And P.PackType = ''  \n");
                    break;
            }

            if (!string.IsNullOrEmpty(hotelWhereStr))
                q.Append(" And Exists(Select Code From @Hotel Where Code=P.Hotel) \n");
            if (!string.IsNullOrEmpty(filter.Package))
                q.AppendFormat("  And P.HolPack='{0}' \n", filter.Package);
            if (!string.IsNullOrEmpty(categoryWhereStr))
                q.Append("  And Exists(Select Code From @Category Where Code=P.HotCat)");
            if (!string.IsNullOrEmpty(resortWhereStr))
            {
                if (filter.Resort.Split('|').Length == 1)
                {
                    Location _locations = locations.Find(f => f.RecID == Conversion.getInt32OrNull(filter.Resort.Split('|')[0]));
                    if (_locations != null && _locations.Type == 3)
                        q.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.Town={0} And L.RecID=P.HotLocation) \n", _locations.RecID);
                    else
                        q.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.RecID={0} And L.RecID=P.HotLocation) \n", _locations.RecID);
                }
                else
                    q.Append("  And Exists(Select R.RecID From #tmpResort R Where R.RecID=P.HotLocation) \n");
            }
            if (!string.IsNullOrEmpty(roomWhereStr))
            {
                if (filter.UseGroup)
                    q.Append("  And Exists(Select Room From GrpRoomDet GRD (NOLOCK) Where Exists(Select Code From @Room Where Code=GRD.Groups) And Room=P.Room) \n");
                else
                    q.Append("  And Exists(Select Code From @Room Where Code=P.Room) \n");
            }
            if (!string.IsNullOrEmpty(boardWhereStr))
            {
                if (filter.UseGroup)
                    q.Append("  And Exists(Select Board From GrpBoardDet GBD (NOLOCK) Where Exists(Select Code From #tmpBoard Where Code=GBD.Groups) And Board=P.Board) \n");
                else
                    q.Append("  And Exists(Select Code From #tmpBoard Where Code=P.Board) \n");
            }
            if (!string.IsNullOrEmpty(facilityWhereStr))
            {
                if (filter.UseGroup)
                    q.AppendLine(@" And Exists(Select null From GrpFacilityDet GFD (NOLOCK)
                                                Join GrpFacility GF (NOLOCK) ON GF.RecID=GFD.MasterID
                                                Join @Facility F ON F.Code=GF.Code
                                                Where Exists(Select null From GrpFacility GF (NOLOCK) Where GF.RecID=GFD.MasterID)
                                                  And Exists(Select null From HotelFacility HF (NOLOCK) Where HF.FacNo=GFD.Facility And HF.Hotel=P.Hotel))");
            }
            #endregion where

            #region Order
            q.Append(" Order By P.CalcSalePrice, P.CatPackID Desc ");
            #endregion

            #region Duplicate Record Delete
            q.Append("Delete From #PriceList \n");
            q.Append("Where not Exists \n");
            q.Append(" (Select * From (Select Max(CatPackID) as CatPackID, HolPack, Hotel, Room, Accom, Board, CheckIn, Night, StopSaleGuar, StopSaleStd, DepFlight From #PriceList \n");
            q.Append("                 Group by Hotel, HolPack, Room, Accom, Board, CheckIn, Night, StopSaleGuar, StopSaleStd, DepFlight) as Newest \n");
            q.Append("  Where Newest.CatPackID=#PriceList.CatPackID \n");
            q.Append("    and Newest.Hotel=#PriceList.Hotel \n");
            q.Append("    and Newest.HolPack=#PriceList.HolPack \n");
            q.Append("    and Newest.Room=#PriceList.Room \n");
            q.Append("    and Newest.Accom=#PriceList.Accom \n");
            q.Append("    and Newest.Board=#PriceList.Board \n");
            q.Append("    and Newest.CheckIn=#PriceList.CheckIn \n");
            q.Append("    and Newest.Night=#PriceList.Night \n");
            q.Append("    and Newest.StopSaleGuar=#PriceList.StopSaleGuar \n");
            q.Append("    and Newest.StopSaleStd=#PriceList.StopSaleStd \n");
            q.Append("    and Newest.DepFlight=#PriceList.DepFlight \n");
            q.Append("  ) \n");
            #endregion

            #region Flight SPO


            q.Append("Declare @CurCatPackID int \n");
            q.Append("Declare MyCursor CURSOR LOCAL FAST_FORWARD FOR \n");
            q.Append("Select Distinct CatPackID from #PriceList \n");
            q.Append("Open MyCursor \n");
            q.Append("Fetch Next FROM MyCursor INTO @CurCatPackID \n");
            q.Append("While @@FETCH_STATUS = 0 \n");
            q.Append("Begin \n");
            q.Append("  Update #PriceList  \n");
            q.Append("  Set DepFlightForSpo=X.Service \n");
            q.Append("  From ( \n");
            q.Append("    Select S.CatPackID,X.CatPricePID,Service=S.Service,DA,S.SClass  \n");
            q.Append("    From CatService S (NOLOCK) \n");
            q.Append("    Join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo \n");
            q.Append("    Where X.CatPackID=@CurCatPackID  \n");
            q.Append("      and S.ServiceType='FLIGHT'  \n");
            q.Append("      and DA=0 \n");
            q.Append("  ) X  \n");
            q.Append("  Where X.CatPackID=@CurCatPackID  \n");
            q.Append("    and X.CatPricePID=#PriceList.PRecNo  \n");
            q.Append("    and LTrim(DepFlightForSpo)='' \n");
            q.Append("  Fetch Next FROM MyCursor INTO @CurCatPackID \n");
            q.Append("End \n");
            q.Append("Close MyCursor \n");
            q.Append("DeAllocate MyCursor \n");

            #endregion

            #region Flight Allotment

            if (isG7)
            {
                if (filter.SType == SearchType.PackageSearch && checkAvailableFlightSeat)
                {
                    q.AppendLine(@"Select *,
				                    DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(p.Operator, p.Market,  DepFlight, p.CheckIn, p.Night, p.FlightClass,null, p.HolPack,  0,null),
				                    RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(p.Operator, p.Market,  DepFlight, p.CheckIn, p.Night, p.FlightClass,null, p.HolPack,  1,null) INTO #FlightSeat
				                    From
				                    (
				                        Select Distinct CatPackID, PRecNo, CheckIn, Night, FlightClass, HolPack, DepCity, ArrCity, Operator, Market, DepFlight, RetFlight from #PriceList
				                    ) P
				                     Update x 
					                    Set DepSeat=z.DepSeat, 
            			                    RetSeat=z.RetSeat 
					                    From #PriceList x, #FlightSeat z 
					                    Where	x.CatPackID=z.CatPackID 
            			                    And x.CheckIn=z.CheckIn 
            			                    And x.Night=z.Night 
            			                    And x.FlightClass=z.FlightClass 
            			                    And x.HolPack=z.HolPack 
            			                    And x.DepCity=z.DepCity 
            			                    And x.ArrCity=z.ArrCity 
            			                    And x.DepFlight=z.DepFlight 
            			                    And x.RetFlight=z.RetFlight ");
                }
                else
                    q.AppendLine(" Update #PriceList SET DepSeat=100,RetSeat=100 ");
            }
            #endregion


            if (filter.ShowAgencyEB)
            {
                #region Calc AgencyEB
                q.Append("Select Distinct Hotel, EBValid, HolPack, CheckIn, Night, Operator, Market \n");
                q.Append("Into #AgencyEB1 \n");
                q.Append("From #PriceList \n");

                q.Append("Select *, \n");
                q.Append("	AgencyEB=Case When EBValid='Y' Then ( \n");
                q.Append("	Select Top 1 EBPer  From AgencyComEB EB (NOLOCK) \n");
                q.Append("	Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups=EB.AgencyGrp and AG.GrpType='P' \n");
                q.Append("	Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups=EB.HolPackGrp and PG.GrpType='P' \n");
                q.Append("	Where	OwnerOperator=x.Operator \n");
                q.Append("		 And OwnerMarket=x.Market \n");
                q.Append("		 And Operator=@Operator \n");
                q.Append("		 And Market=@Market \n");
                q.Append("		 And ((EB.Agency=(Select Case WHEN isnull(MainOffice, '')='' Then Code Else MainOffice End From Agency (NOLOCK) Where Code = @Agency) \n");
                q.Append("		    Or AG.Agency=(Select Case WHEN isnull(MainOffice, '')='' Then Code Else MainOffice End From Agency (NOLOCK) Where Code = @Agency)) Or (EB.Agency='' And AG.Agency is Null)) \n");
                q.Append("		 And ((EB.HolPackGrp='' And (EB.HolPack=x.HolPack Or EB.HolPack='')) Or PG.HolPack=x.HolPack) \n");
                q.Append("		 And dbo.DateOnly(@SaleBegDate) Between SaleBegDate And SaleEndDate \n");
                q.Append(" 	  	 And X.CheckIn Between ResBegDate And ResEndDate \n");
                q.Append("	 Order By EB.Agency Desc, AgencyGrp Desc, EB.HolPack Desc, HolPackGrp Desc, SaleBegDate Desc, ResBegDate Desc \n");
                q.Append("	) Else 0 End \n");
                q.Append("INTO #AgencyEB \n");
                q.Append("From #AgencyEB1 X \n");

                q.Append("Update x \n");
                q.Append("Set AgencyEB=Case When y.AgencyEB is null then 0 Else y.AgencyEB End \n");
                q.Append("From #PriceList x ,#AgencyEB y \n");
                q.Append("where	x.Hotel=y.Hotel \n");
                q.Append("	And x.EBValid=y.EBValid \n");
                q.Append("	And x.Holpack=y.Holpack \n");
                q.Append("	And x.CheckIn=y.CheckIn \n");
                q.Append("	And x.Night=y.Night \n");
                #endregion
            }

            #region Passanger EB
            q.Append("Select X.* \n");
            q.Append("Into #PriceListEB \n");
            q.Append("From ( \n");
            q.Append("    Select R.*, \n");
            q.Append("        PEB_Amount=Cast(0.0 as dec(18,2)), \n");
            q.Append("        PasEBPer=IsNull(PEB.PasEBPer,0), \n");
            q.Append("        PEB_AdlVal=IsNull(PEB.AdlVal,0), \n");
            q.Append("        PEB_EBedVal=IsNull(PEB.EBedVal,0), \n");
            q.Append("        PEB_ChdGrp1Val=IsNull(PEB.ChdGrp1Val,0), \n");
            q.Append("        PEB_ChdGrp2Val=IsNull(PEB.ChdGrp2Val,0), \n");
            q.Append("        PEB_ChdGrp3Val=IsNull(PEB.ChdGrp3Val,0), \n");
            q.Append("        PEB_ChdGrp4Val=IsNull(PEB.ChdGrp4Val,0), \n");
            q.Append("        EBValidDate=Case When PEB.PasEBPer is null Then Cast(73048 AS DateTime) Else PEB.SaleEndDate End, \n");
            q.Append("        HKB_Exists=Case When HKB.PriceType is null then 0 else 1 end, \n");
            q.Append("        HKB_PriceType=HKB.PriceType, \n");
            q.Append("        HKB_CalcType=HKB.CalcType, \n");
            q.Append("        HKB_AdlPrice=HKB.AdlPrice, \n");
            q.Append("        HKB_SupPer=HKB.SupPer, \n");
            q.Append("        HKB_EBOpt=HKB.EBOpt, \n");
            q.Append("        HKB_Cur=HKB.Cur, \n");
            q.Append("        HKB_Amount=Cast(0 as dec(18,2)), \n");
            q.Append("        SPWithEB=R.CalcSalePrice, \n");
            q.Append("        PasEBPer2=Cast(0.0 as dec(5,2)), \n");
            q.Append("        PEB_RecID=PEB.EBRecID,PEB_ResBegDate=PEB.ResBegDate,PEB_CinAccom=PEB.CinAccom, \n");
            q.Append("        PEB_ResEndDate=case when PEB.CinAccom=0 then R.CheckIn+R.Night-1 \n");
            q.Append("                            when PEB.ResEndDate<R.CheckIn+R.Night-1 then PEB.ResEndDate \n");
            q.Append("                            else R.CheckIn+R.Night-1 \n");
            q.Append("                       end, \n");
            q.Append("        EBAmount=Case When R.PasEBValid='Y' And (PEB.PasEBPer is not null or PEB.AdlVal is not null) then \n");
            q.Append("                            dbo.ufn_GetEBAmountPL(R.CatPackID,R.ARecNo,R.PRecNo,HapRecID,R.Adl,R.ChdG1Age1,R.ChdG1Age2,R.ChdG2Age1,R.ChdG2Age2,R.ChdG3Age1,R.ChdG3Age2,R.ChdG4Age1,R.ChdG4Age2,R.HotelChdRecNo,R.ChdCalcSale,R.HAdult,HChdAgeG1,HChdAgeG2,HChdAgeG3,HChdAgeG4,0,@CustomRegID,HotChdPrice) \n");
            q.Append("                      else 0 end, \n");
            q.Append("        PEB_SaleEndDate=PEB.SaleEndDate \n");
            q.Append("    From #PriceList R \n");
            q.Append("    -- 2010-02-11 KickBack \n");
            q.Append("    OUTER APPLY \n");
            q.Append("    ( \n");
            q.Append("          Select Top 1 PriceType,CalcType,AdlPrice,SupPer,EBOpt,Cur \n");
            q.Append("          From HotelFeed (NOLOCK) HF \n");
            q.Append("          Where Hotel=R.Hotel and Status=1 and ForSale=1 and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate \n");
            q.Append("          and R.CheckIn between BegDate and EndDate \n");
            q.Append("          and R.Night between IsNull(MinDay,0) and IsNull(MaxDay,999) \n");
            q.Append("          and SubString(AccomDays,DATEPART(dw,R.CheckIn),1)=1 \n");
            q.Append("          and (Room='' or Room=R.Room) \n");
            q.Append("          and (Board='' or Board=R.Board) \n");
            q.Append("          and exists(Select * from HotelSeason (NOLOCK) Where Hotel = HF.Hotel and RecID = HF.SeasonID and Market=R.Market) \n");
            q.Append("          and exists(Select * from HotelNetPrice (NOLOCK) Where SeasonID = HF.SeasonID and Hotel = HF.Hotel and Room = R.Room and Accom = R.Accom and Board = R.Board and R.CheckIn between BegDate and EndDate) \n");
            q.Append("          Order by Room Desc, Board Desc,CrtDate Desc \n");
            q.Append("    ) HKB \n");
            q.Append("    OUTER APPLY \n");
            q.Append("    ( \n");
            q.Append("        Select Top 1 PasEBPer,AdlVal,EBedVal=IsNull(EBedVal, AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val, \n");
            q.Append("          EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,EBP.SaleEndDate, \n");
            q.Append("          LastEBRecID = EB.RecID * @GetEBLastRecID, LastEBPRecID = EBP.RecID * @GetEBPLastRecID \n");
            q.Append("        From PasEB EB (NOLOCK) \n");
            q.Append("        Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID \n");

            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0034, VersionControl.Equality.gt))
                q.Append("        Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=R.Hotel and dbo.IsEmpty(EBH.Room,R.Room)=R.Room and dbo.IsEmpty(EBH.Board,R.Board)=R.Board \n");
            else
                q.Append("        Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=R.Hotel and (EBH.Room='' or EBH.Room=R.Room) \n");
            q.Append("        Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P' \n");
            q.Append("        Where R.PasEBValid='Y' and OwnerOperator=R.Operator and OwnerMarket=R.Market and Operator=@Operator and Market=@Market and R.PasEBValid='Y' \n");
            q.Append("              and ( (EB.HolPack=R.HolPack or PG.HolPack=R.HolPack) or (EB.HolPack='' and PG.HolPack is Null) ) \n");
            q.Append("              and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate and R.CheckIn between ResBegDate and ResEndDate \n");
            q.Append("              and (R.Night between IsNull(MinDay,0) and IsNull(MaxDay,999)) \n");
            q.Append("              and SubString(DoW,DATEPART(dw,R.CheckIn),1)=1 \n");

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040014038"))
                q.Append("              and DateDIFF(day,dbo.DateOnly(GetDate()),R.CheckIn) between IsNull(EBP.RemFromDay,0) and IsNull(EBP.RemToDay,999) \n");
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0034, VersionControl.Equality.gt))
                q.Append("        Order by LastEBRecID desc,EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc,EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc,LastEBPRecID Desc,SaleBegDate Desc,ResBegDate Desc, EBP.RecID Desc \n");
            else
                q.Append("        Order by EBH.Hotel Desc,EBH.Room Desc, EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc,SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc \n");
            q.Append("    ) PEB \n");
            q.Append("    -- 2010-02-11 KickBack \n");
            q.Append(") X \n");

            q.Append("Update #PriceListEB \n");
            q.Append("Set \n");
            q.Append(" SpoPer=Y.SpoPer,  \n");
            q.Append(" SpoAdlVal=Y.AdlVal,  \n");
            q.Append(" SpoExtB1Val=Y.ExtB1Val,  \n");
            q.Append(" SpoExtB2Val=Y.ExtB2Val, \n");
            q.Append(" SpoChdGrp1Val =case when X.SaleChdG1 > 0 then Y.ChdGrp1Val else 0 end, \n");
            q.Append(" SpoChdGrp2Val =case when X.SaleChdG2 > 0 then Y.ChdGrp2Val else 0 end, \n");
            q.Append(" SpoChdGrp3Val =case when X.SaleChdG3 > 0 then Y.ChdGrp3Val else 0 end, \n");
            q.Append(" SpoChdGrp4Val =case when X.SaleChdG4 > 0 then Y.ChdGrp4Val else 0 end, \n");
            q.Append(" SpoFlightTot=Y.FlightTot,  \n");
            q.Append(" SpoHotelTot=Y.HotelTot, \n");
            q.Append(" PasEBValidFlight=Y.PEBValidForFlight, \n");
            q.Append(" PasEBValidHotel=Y.PEBValidForHotel, \n");
            q.Append(" EBFlightValid = isnull((Select Top 1 IsNull(PayPasEB,0) From CatPackPlan Where CatPackID=X.CatPackID and Service='FLIGHT'),0), \n");
            q.Append(" EBHotelValid = isnull((Select Top 1 IsNull(PayPasEB,0) From CatPackPlan Where CatPackID=X.CatPackID and Service='HOTEL'),0), \n");
            q.Append(" FlightValidDate=Y.FlightValidDate, \n");
            q.Append(" HotelValidDate=Y.HotelValidDate \n");
            q.Append("  \n");
            q.Append("From #PriceListEB X \n");
            q.Append("Outer Apply \n");
            q.Append("( \n");
            q.Append("  Select * From dbo.PLSpoVals(0,X.CatPackID,@SaleBegDate,X.CheckIn,X.Night,X.Hotel,X.Room,X.Accom,X.Board,X.Adl,X.HAdult,X.HChdAgeG1,X.HChdAgeG2,X.HChdAgeG3,X.HChdAgeG4,X.HotelLocation,X.HotCat,0,X.DepFlightForSpo,@Agency,@CustomRegID) \n");
            q.Append(") Y \n");
            q.Append("Where CatPackID=X.CatPackID  \n");
            q.Append("  and ARecNo=X.ARecNo  \n");
            q.Append("  and PRecNo=X.PRecNo  \n");
            q.Append("  and HAPRecID=X.HAPRecID \n");
            q.Append("  and PlSpoExists=1 \n");
            q.Append(" \n");
            q.Append("Update #PriceListEB \n");
            q.Append("Set SaleAdl=SaleAdl+IsNull(SpoAdlVal,0), \n");
            q.Append("  SaleExtBed1=Case when SaleExtBed1<>0 or IsNull(SpoExtB1Val,0)>0 then SaleExtBed1+IsNull(SpoExtB1Val,0) else 0 end, \n");
            q.Append("  SaleExtBed2=Case when SaleExtBed2<>0 or IsNull(SpoExtB2Val,0)>0 then SaleExtBed2+IsNull(SpoExtB2Val,0) else 0 end, \n");
            q.Append("  SaleChdG1=Case when SaleChdG1<>0 or IsNull(SpoChdGrp1Val,0)>0 then SaleChdG1+IsNull(SpoChdGrp1Val,0) else 0 end, \n");
            q.Append("  SaleChdG2=Case when SaleChdG2<>0 or IsNull(SpoChdGrp2Val,0)>0 then SaleChdG2+IsNull(SpoChdGrp2Val,0) else 0 end, \n");
            q.Append("  SaleChdG3=Case when SaleChdG3<>0 or IsNull(SpoChdGrp3Val,0)>0 then SaleChdG3+IsNull(SpoChdGrp3Val,0) else 0 end, \n");
            q.Append("  SaleChdG4=Case when SaleChdG4<>0 or IsNull(SpoChdGrp4Val,0)>0 then SaleChdG4+IsNull(SpoChdGrp4Val,0) else 0 end \n");
            q.Append("Where PlSpoExists=1 and (isnull(SpoPer,0)=0 or SpoHotelTot <> 0)   \n");
            q.Append("   \n");
            q.Append("Update #PriceListEB    \n");
            q.Append("Set SaleAdl=SaleAdl*(1 + IsNull(SpoPer,0)/100), \n");
            q.Append("    SaleExtBed1=Case when SaleExtBed1<>0 then SaleExtBed1*(1+IsNull(SpoPer,0)/100) else 0 end, \n");
            q.Append("    SaleExtBed2=Case when SaleExtBed2<>0 then SaleExtBed2*(1+IsNull(SpoPer,0)/100) else 0 end, \n");
            q.Append("    SaleChdG1=Case when SaleChdG1<>0 then SaleChdG1*(1+IsNull(SpoPer,0)/100) else 0 end, \n");
            q.Append("    SaleChdG2=Case when SaleChdG2<>0 then SaleChdG2*(1+IsNull(SpoPer,0)/100) else 0 end, \n");
            q.Append("    SaleChdG3=Case when SaleChdG3<>0 then SaleChdG3*(1+IsNull(SpoPer,0)/100) else 0 end, \n");
            q.Append("    SaleChdG4=Case when SaleChdG4<>0 then SaleChdG4*(1+IsNull(SpoPer,0)/100) else 0 end, \n");
            q.Append("    SpoFlightTot=Case When SpoPer=0 then SpoFlightTot \n");
            q.Append("                      Else (((Case When HAdult>Adl Then Adl Else HAdult End)*SaleAdl*IsNull(SpoPer,0)/100)+ \n");
            q.Append("                            Case When HAdult-Adl>=1 then SaleExtBed1 * IsNull(SpoPer,0)/100 else 0 end+ \n");
            q.Append("                            Case When HAdult-Adl>=2 then SaleExtBed2 * IsNull(SpoPer,0)/100 else 0 end+ \n");
            q.Append("                            Case When HChdAgeG1>0 then SaleChdG1 * HChdAgeG1 * IsNull(SpoPer,0)/100 else 0 end+ \n");
            q.Append("                            Case When HChdAgeG2>0 then SaleChdG2 * HChdAgeG2 * IsNull(SpoPer,0)/100 else 0 end+ \n");
            q.Append("                            Case When HChdAgeG3>0 then SaleChdG3 * HChdAgeG3 * IsNull(SpoPer,0)/100 else 0 end+ \n");
            q.Append("                            Case When HChdAgeG4>0 then SaleChdG4 * HChdAgeG4 * IsNull(SpoPer,0)/100 else 0 end) \n");
            q.Append("                      End \n");
            q.Append("Where PlSpoExists=1 and isnull(SpoPer,0)<>0 \n");
            q.Append(" \n");
            q.Append("  Update #PriceListEB \n");
            q.Append("  Set \n");
            q.Append("  CalcSalePrice=((Case When HAdult>Adl then Adl else HAdult end) * SaleAdl)+ \n");
            q.Append("				   Case when HAdult-Adl>=1 then SaleExtBed1 else 0 end+ \n");
            q.Append("				   Case when HAdult-Adl>=2 then SaleExtBed2 else 0 end+ \n");
            q.Append("				   Case when HChdAgeG1>0 then SaleChdG1 * HChdAgeG1 else 0 end+ \n");
            q.Append("				   Case when HChdAgeG2>0 then SaleChdG2 * HChdAgeG2 else 0 end+ \n");
            q.Append("				   Case when HChdAgeG3>0 then SaleChdG3 * HChdAgeG3 else 0 end+ \n");
            q.Append("				   Case when HChdAgeG4>0 then SaleChdG4 * HChdAgeG4 else 0 end, \n");
            q.Append("   \n");
            q.Append("  PlSpoExists=Case When IsNull(SpoPer,0)<>0 or \n");
            q.Append("                        IsNull(SpoAdlVal,0)<>0 or \n");
            q.Append("                        (HAdult-Adl>=1 and IsNull(SpoExtB1Val,0)<>0 ) or \n");
            q.Append("                        (HAdult-Adl>=2 and IsNull(SpoExtB2Val,0)<>0 ) or \n");
            q.Append("                        (HChdAgeG1>0 and IsNull(SpoChdGrp1Val,0)<>0 ) or \n");
            q.Append("                        (HChdAgeG2>0 and IsNull(SpoChdGrp2Val,0)<>0 ) or \n");
            q.Append("                        (HChdAgeG3>0 and IsNull(SpoChdGrp3Val,0)<>0 ) or \n");
            q.Append("                        (HChdAgeG4>0 and IsNull(SpoChdGrp4Val,0)<>0 ) then 1 else 0 end, \n");
            q.Append("  EBAmount = ((EBAmount + (EBAmount * SpoPer * isnull(EBFlightValid,0) / 100)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel), \n /*case  \n");
            q.Append("               when SpoPer = 0 and SpoFlightTot>0 then (EBAmount + (SpoFlightTot * isnull(EBFlightValid,0)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel) \n");
            q.Append("               else ((EBAmount + (EBAmount * SpoPer * isnull(EBFlightValid,0) / 100)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel) \n");
            q.Append("             end,*/  \n");
            q.Append("  PasEBPer = PasEBPer * PasEBValidFlight * PasEBValidHotel, \n");
            q.Append("  PEB_AdlVal=PEB_AdlVal * PasEBValidFlight * PasEBValidHotel \n");
            q.Append("  From #PriceListEB X \n");
            q.Append("  Where PlSpoExists=1 \n");
            q.Append("   \n");
            q.Append("Update #PriceListEB  \n");
            q.Append("Set HKB_Exists=0  \n");
            q.Append("Where HKB_Exists=1 and (PasEBPer>0 or PEB_AdlVal<>0)  and HKB_EBOpt=1 \n");
            q.Append(" \n");
            q.Append("Update #PriceListEB  \n");
            q.Append("Set HKB_Amount=dbo.Calc_HotKickBack(0, \n");
            q.Append("                                    HKB_PriceType, \n");
            q.Append("                                    HKB_CalcType, \n");
            q.Append("                                    HKB_AdlPrice*dbo.ufn_Exchange(dbo.DateOnly(@SaleBegDate),HKB_Cur,SaleCur,1,1,Market), \n");
            q.Append("                                    HKB_SupPer, \n");
            q.Append("                                    HKB_Cur, \n");
            q.Append("                                    Night, \n");
            q.Append("                                    Adl, \n");
            q.Append("                                    Case When HAdult-Adl>=1 Then 1 Else 0 End, \n");
            q.Append("                                    Case When HAdult-Adl>=2 Then 1 else 0 End, \n");
            q.Append("                                    CatPackID, \n");
            q.Append("                                    PRecNo, \n");
            q.Append("                                    HotChdPrice, \n");
            q.Append("                                    SaleCur, \n");
            q.Append("                                    '',  \n");
            q.Append("                                    SpoHotelTot) \n");
            q.Append("Where HKB_Exists=1 \n");
            q.Append(" \n");
            q.Append("Update #PriceListEB  \n");
            q.Append("Set CalcSalePrice=CalcSalePrice-HKB_Amount,  \n");
            q.Append("	EBAmount=Case When IsNull(EBAmount,0)>0 Then EBAmount-HKB_Amount Else 0 End  \n");
            q.Append("Where HKB_Exists=1 \n");
            q.Append(" \n");
            #endregion

            #region Only Hotel Flight Or Transport Type
            q.Append("  Update #PriceList Set TransportType = Case when HP.Service = 'FLIGHT' Then 1 when HP.Service = 'TRANSPORT' Then 2 Else 0 End \n");
            q.Append("  From HolPackPlan HP \n");
            q.Append("  Where HP.HolPack = #PriceList.HolPack and HP.StepNo = 1 \n");

            q.Append("  Update #PriceList Set TransportType = 2 \n");
            q.Append("  From HolPackPlan HP \n");
            q.Append("  Where #PriceList.TransportType = 0 And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack = #PriceList.HolPack And Service = 'TRANSPORT') Between 1 And 2 \n");
            q.Append("  And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack = #PriceList.HolPack And Service = 'FLIGHT') = 0 \n");
            #endregion

            #region Last Calculation
            q.Append("--PasEbPer2 - 2.period ******************** \n");
            q.Append("Update #PriceListEB  \n");
            q.Append("Set PasEbPer2=IsNull(PEB2.PasEBPer,0)  \n");
            q.Append("From #PriceListEB V  \n");
            q.Append("OUTER APPLY  \n");
            q.Append("(Select Top 1 PasEBPer=IsNull(PasEBPer,0),AdlVal,EBedVal=IsNull(EBedVal,AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val,EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,  \n");
            q.Append("		LastEBRecID=EB.RecID*@GetEBLastRecID,LastEBPRecID=EBP.RecID*@GetEBPLastRecID  \n");
            q.Append(" From PasEB EB (NOLOCK)  \n");
            q.Append(" Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID  \n");
            q.Append(" Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=V.Hotel and (EBH.Room='' or EBH.Room=V.Room)and (EBH.Board='' or EBH.Board=V.Board)  \n");
            q.Append(" Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P' \n");
            q.Append(" Where EBP.RecID<>V.PEB_RecID and \n");
            q.Append("   V.PasEBPer<>0 and  \n");
            q.Append("   V.PasEBValid='Y' and OwnerOperator=V.Operator and OwnerMarket=V.Market and Operator=@Operator and Market=@Market and V.PasEBValid='Y' and EB.SaleType=0 and  \n");
            q.Append("		( (EB.HolPack=V.HolPack or PG.HolPack=V.HolPack) or (EB.HolPack='' and PG.HolPack is Null) )  \n");
            q.Append("		and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate   \n");
            q.Append("		and V.PEB_ResEndDate+1 between ResBegDate and ResEndDate and CinAccom=1  \n");
            q.Append("		and (V.Night between IsNull(MinDay,0) and IsNull(MaxDay,999))  \n");
            q.Append("		and SubString(DoW,DATEPART(dw,V.CheckIn),1)=1  \n");
            q.Append("        and DateDIFF(day,dbo.DateOnly(GetDate()),V.CheckIn) between IsNull(EBP.RemFromDay,0) and IsNull(EBP.RemToDay,999)  \n");
            q.Append(" Order by LastEBRecID desc, EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc,EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc, LastEBPRecID Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc  \n");
            q.Append(") PEB2  \n");
            q.Append("Where V.PEB_CinAccom=1 and PEB_ResEndDate<V.CheckIn+Night-1  \n");
            q.Append(" \n");
            q.Append(" \n");
            q.Append("Update #PriceListEB  \n");
            q.Append("Set CalcSalePrice=CalcSalePrice-(IsNull(EBAmount,0)*PasEBPer* ((DATEDIFF(day,CheckIn,PEB_ResEndDate)+1)/ cast(Night as dec(18,6))) / 100) \n");
            q.Append("                               -(IsNull(EBAmount,0)*PasEBPer2* ((DATEDIFF(day,PEB_ResEndDate,CheckIn+Night-1))/ cast(Night as dec(18,6))) / 100)   \n");
            q.Append(" \n");
            q.Append("Update #PriceListEB  \n");
            q.Append("Set PEB_Amount=  Case When HAdult>0 and PEB_AdlVal<>0 then (Case When HAdult>Adl then Adl else HAdult end) * PEB_AdlVal else 0 end+ \n");
            q.Append("                 Case When HAdult-Adl>=1 then PEB_EBedVal else 0 end+ \n");
            q.Append("                 Case When HAdult-Adl>=2 then PEB_EBedVal else 0 end+ \n");
            q.Append("                 Case When HChdAgeG1>0 and PEB_ChdGrp1Val<>0 then HChdAgeG1*PEB_ChdGrp1Val else 0 end+ \n");
            q.Append("                 Case When HChdAgeG2>0 and PEB_ChdGrp2Val<>0 then HChdAgeG2*PEB_ChdGrp2Val else 0 end+ \n");
            q.Append("                 Case When HChdAgeG3>0 and PEB_ChdGrp3Val<>0 then HChdAgeG3*PEB_ChdGrp3Val else 0 end+ \n");
            q.Append("                 Case When HChdAgeG4>0 and PEB_ChdGrp4Val<>0 then HChdAgeG4*PEB_ChdGrp4Val else 0 end, \n");
            q.Append(" \n");
            q.Append("CalcSalePrice=CalcSalePrice- \n");
            q.Append("( \n");
            q.Append("  Case When HAdult>0 and PEB_AdlVal<>0 then Case When HAdult>Adl then Adl else HAdult end * PEB_AdlVal else 0 end+ \n");
            q.Append("  Case When HAdult-Adl>=1 then PEB_EBedVal else 0 end+ \n");
            q.Append("  Case When HAdult-Adl>=2 then PEB_EBedVal else 0 end+ \n");
            q.Append("  Case When HChdAgeG1>0 and PEB_ChdGrp1Val<>0 then HChdAgeG1*PEB_ChdGrp1Val else 0 end+ \n");
            q.Append("  Case When HChdAgeG2>0 and PEB_ChdGrp2Val<>0 then HChdAgeG2*PEB_ChdGrp2Val else 0 end+ \n");
            q.Append("  Case When HChdAgeG3>0 and PEB_ChdGrp3Val<>0 then HChdAgeG3*PEB_ChdGrp3Val else 0 end+ \n");
            q.Append("  Case When HChdAgeG4>0 and PEB_ChdGrp4Val<>0 then HChdAgeG4*PEB_ChdGrp4Val else 0 end \n");
            q.Append(") \n");
            q.Append("Where PEB_AdlVal<>0 \n");
            q.Append(" \n");
            q.Append("Update #PriceListEB  \n");
            q.Append("Set PEB_EBedVal=0, \n");
            q.Append("	PEB_ChdGrp1Val=0, \n");
            q.Append("	PEB_ChdGrp2Val=0, \n");
            q.Append("	PEB_ChdGrp3Val=0, \n");
            q.Append("	PEB_ChdGrp4Val=0 \n");
            q.Append("Where PEB_AdlVal=0 \n");
            q.Append(" \n");
            q.Append("Update #PriceListEB  \n");
            q.Append("Set PEB_Amount=PEB_Amount*-1 \n");
            q.Append("Where PEB_Amount<>0 \n");
            q.Append(" \n");
            q.Append("Update #PriceListEB  \n");
            q.Append("Set CalcSalePrice=dbo.ufn_Exchange(@SaleBegDate,SaleCur,isnull(@Cur,SaleCur),CalcSalePrice,1,Market),  \n");
            q.Append("	OldSalePrice=dbo.ufn_Exchange(@SaleBegDate,SaleCur,isnull(@Cur,SaleCur),OldSalePrice,1,Market), \n");
            q.Append("	SaleCur=isnull(@Cur,SaleCur)  \n");

            q.Append("SET NOCOUNT OFF \n");
            #endregion

            #region Select Query
            q.Append("Select \n");
            q.Append("  CatPackID,ARecNo,PRecNo,HAPRecId,HAdult,HChdAgeG1,HChdAgeG2,HChdAgeG3,HChdAgeG4,ChdG1Age1,ChdG1Age2,ChdG2Age1,ChdG2Age2,ChdG3Age1,ChdG3Age2,ChdG4Age1,ChdG4Age2,PL.Hotel,HotelName, \n");
            q.Append("  HotelLocation,HotLocationName=L.Name,HotLocationNameL=L.NameL, \n");
            q.Append("  HotCat,CheckIn,CheckOut,PL.Night,HotCheckIn,HotNight,Room,RoomName,Board,BoardName,Accom,AccomName,AccomFullName, \n");
            q.Append("  CalcSalePrice,SaleChdG1,SaleChdG2,SaleChdG3,SaleChdG4,HotChdPrice,OldSalePrice,NetCur,PL.SaleCur,AgencyEB,PL.DepCity,DepFlight,DepFlightTime,PL.ArrCity,RetFlight,RetFlightTime,EBValidDate, \n");
            q.Append("  PL.Description,SaleSPONo,EBPerc,EBAmount,PasEBPer,SaleEndDate,HolPack,PL.Operator,PL.Market,FlightClass,plSpoExists,FlightValidDate,HotelValidDate, \n");
            q.Append("  SaleAdl,LastPrice=CalcSalePrice,PL.Direction,PL.Category, \n");
            q.Append("  HotelNameL=isnull(dbo.FindLocalName(HNameLID,@Market),HotelName), \n");
            q.Append("  HotelRecID=Htl.RecID, \n");
            q.Append("  RoomNameL=isnull(dbo.FindLocalName(HRNameLID,@Market),RoomName), \n");
            q.Append("  AccomNameL=isnull(dbo.FindLocalName(HANameLID,@Market),AccomName), \n");
            q.Append("  BoardNameL=isnull(dbo.FindLocalName(HBNameLID,@Market),BoardName), \n");
            q.Append("  HolPackName=H.Name,HolPackNameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name), \n");
            q.Append("  DepCityName=L1.Name, DepCityNameL=L1.NameL, \n");
            q.Append("  ArrCityName=L2.Name, ArrCityNameL=L2.NameL, \n");
            q.Append("  Cur=@Cur,PL.PLCur, \n");
            q.Append("  CurrentCur=isnull(@Cur,PL.SaleCur), \n");
            q.Append("  InfoWeb=isnull(Hmo.InfoWeb, ''), Hmo.MaxChdAge,OprText = (Case When isnull(H.OprText, '') = '' then O.NameS else H.OprText end), \n");
            q.Append("  PEB_SaleEndDate,PEB_ResEndDate,StopSaleGuar,StopSaleStd,DFlight,DepServiceType,RetServiceType,AutoStop,TransportType, \n");
            q.Append("  PLSPO_Amount=IsNull(SpoFlightTot,0)+IsNull(SpoHotelTot,0), \n");
            q.Append("  DepSeat,RetSeat,FreeAllot, \n");
            q.Append("  HotelOrder=isnull(HtlOrd.DispNo ,9999) \n");
            q.Append("From #PriceListEB PL \n");
            q.Append("Join #Location L ON L.RecID=PL.HotelLocation \n");
            q.Append("Join #Location L1 ON L1.RecID=PL.DepCity \n");
            q.Append("Join #Location L2 ON L2.RecID=PL.ArrCity \n");
            q.Append("Join HolPack H (NOLOCK) ON H.Code=PL.HolPack \n");
            q.Append("Join Hotel Htl (NOLOCK) ON Htl.Code=PL.Hotel \n");
            q.Append("Join Operator O (NOLOCK) ON O.Code=PL.Operator \n");
            q.Append("Left Join HotelMarOpt (NOLOCK) Hmo ON Hmo.Hotel=PL.Hotel And Hmo.Market=@Market \n");
            q.Append("Left Join HotelDispOrder HtlOrd (NOLOCK) ON HtlOrd.Hotel=PL.Hotel And HtlOrd.Market=@Market \n");
            if ((filter.ShowStopSaleHotels && !filter.ExtAllotControl) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                q.Append("Where Not (StopSaleStd=2 And StopSaleGuar=2) \n");
            #endregion

            #region Last Where Query
            string lastWhere = string.Empty;
            if (filter.PriceFrom.HasValue)
            {
                if (lastWhere.Length > 0)
                    lastWhere += " and ";
                lastWhere += " CalcSalePrice>=@Price1 \n";
            }
            if (filter.PriceTo.HasValue)
            {
                if (lastWhere.Length > 0)
                    lastWhere += " and ";
                lastWhere += " CalcSalePrice<=@Price2 \n";
            }
            if (lastWhere.Length > 0)
            {
                q.Append(" and ");
                q.Append(lastWhere);
            }
            #endregion

            q.Append("Order By HotelOrder,CalcSalePrice \n");

            int InfCount = 0;
            if (filter.RoomsInfo.FirstOrDefault().Chd1Age.HasValue && (filter.RoomsInfo.FirstOrDefault().Chd1Age.Value > 0 && filter.RoomsInfo.FirstOrDefault().Chd1Age.Value <= 1.99))
                InfCount++;
            else
                if (filter.RoomsInfo.FirstOrDefault().Chd2Age.HasValue && (filter.RoomsInfo.FirstOrDefault().Chd2Age.Value > 0 && filter.RoomsInfo.FirstOrDefault().Chd2Age.Value <= 1.99))
                InfCount++;
            else
                    if (filter.RoomsInfo.FirstOrDefault().Chd3Age.HasValue && (filter.RoomsInfo.FirstOrDefault().Chd3Age.Value > 0 && filter.RoomsInfo.FirstOrDefault().Chd3Age.Value <= 1.99))
                InfCount++;
            else
                        if (filter.RoomsInfo.FirstOrDefault().Chd4Age.HasValue && (filter.RoomsInfo.FirstOrDefault().Chd4Age.Value > 0 && filter.RoomsInfo.FirstOrDefault().Chd4Age.Value <= 1.99))
                InfCount++;

            int CurrentSeat = 0;
            CurrentSeat = (filter.RoomsInfo.FirstOrDefault().Adult + filter.RoomsInfo.FirstOrDefault().Child) - InfCount;
            string dateFormat = UserData.Ci.DateTimeFormat.ShortDatePattern.ToString().Replace("dd", "d").Replace("yy", "y").Replace("MM", "m").Replace(UserData.Ci.DateTimeFormat.DateSeparator, "");
            string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator.ToString();
            List<SearchResult> result = new List<SearchResult>();

            #region Age Group
            List<AgeGroup> ageGroups = new List<AgeGroup>();
            int roomCnt = 0;
            int refNo = 0;
            foreach (SearchCriteriaRooms row in filter.RoomsInfo)
            {
                roomCnt++;
                refNo++;
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Adult, GroupNo = roomCnt, RefNo = refNo, Unit = row.Adult, Age = null, DateOfBirth = null });
                if (row.Chd1Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Infant, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd1Age.Value, DateOfBirth = null });
                if (row.Chd2Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd2Age.Value, DateOfBirth = null });
                if (row.Chd3Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd3Age.Value, DateOfBirth = null });
                if (row.Chd4Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd4Age.Value, DateOfBirth = null });
            }

            #endregion

            Database db = DatabaseFactory.CreateDatabase();

            DbCommand dbCommand = db.GetSqlStringCommand(q.ToString());
            dbCommand.CommandTimeout = 180;
            try
            {
                int i = 0;

                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, UserData.Operator);
                db.AddInParameter(dbCommand, "Office", DbType.AnsiString, UserData.OprOffice);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "CIn1", DbType.Date, filter.CheckIn.Date);
                if (filter.ExpandDate > 0)
                    db.AddInParameter(dbCommand, "CIn2", DbType.Date, filter.CheckIn.Date.AddDays(filter.ExpandDate));
                db.AddInParameter(dbCommand, "Night1", DbType.Int16, filter.NightFrom);
                if (filter.NightFrom < filter.NightTo)
                    db.AddInParameter(dbCommand, "Night2", DbType.Int16, filter.NightTo);
                db.AddInParameter(dbCommand, "Cur", DbType.AnsiString, !filter.CurControl || string.IsNullOrEmpty(filter.CurrentCur) ? null : filter.CurrentCur);
                if (filter.PriceFrom.HasValue)
                    db.AddInParameter(dbCommand, "Price1", DbType.Decimal, filter.PriceFrom);
                if (filter.PriceTo.HasValue)
                    db.AddInParameter(dbCommand, "Price2", DbType.Decimal, filter.PriceTo);
                Stopwatch sw = new Stopwatch();
                sw.Start();
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ++i;
                        SearchResult r = new SearchResult();
                        r.RefNo = i;
                        r.RoomNr = i;
                        r.Calculated = false;
                        r.CatPackID = Conversion.getInt32OrNull(R["CatPackID"]);
                        r.ARecNo = Conversion.getInt32OrNull(R["ARecNo"]);
                        r.PRecNo = Conversion.getInt32OrNull(R["PRecNo"]);
                        r.HAPRecId = Conversion.getInt32OrNull(R["HAPRecId"]);
                        r.HAdult = (Int16)R["HAdult"];
                        r.HChdAgeG1 = (Int16)R["HChdAgeG1"];
                        r.HChdAgeG2 = (Int16)R["HChdAgeG2"];
                        r.HChdAgeG3 = (Int16)R["HChdAgeG3"];
                        r.HChdAgeG4 = (Int16)R["HChdAgeG4"];
                        r.ChdG1Age1 = Conversion.getDecimalOrNull(R["ChdG1Age1"]);
                        r.ChdG1Age2 = Conversion.getDecimalOrNull(R["ChdG1Age2"]);
                        r.ChdG2Age1 = Conversion.getDecimalOrNull(R["ChdG2Age1"]);
                        r.ChdG2Age2 = Conversion.getDecimalOrNull(R["ChdG2Age2"]);
                        r.ChdG3Age1 = Conversion.getDecimalOrNull(R["ChdG3Age1"]);
                        r.ChdG3Age2 = Conversion.getDecimalOrNull(R["ChdG3Age2"]);
                        r.ChdG4Age1 = Conversion.getDecimalOrNull(R["ChdG4Age1"]);
                        r.ChdG4Age2 = Conversion.getDecimalOrNull(R["ChdG4Age2"]);
                        r.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        r.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        r.HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]);
                        r.HotelRecID = Conversion.getInt32OrNull(R["HotelRecID"]);
                        r.HotelLocation = Conversion.getInt32OrNull(R["HotelLocation"]);
                        r.HotLocationName = Conversion.getStrOrNull(R["HotLocationName"]);
                        r.HotLocationNameL = Conversion.getStrOrNull(R["HotLocationNameL"]);
                        r.HotCat = Conversion.getStrOrNull(R["HotCat"]);
                        r.CheckIn = Conversion.getDateTimeOrNull(R["CheckIn"]);
                        r.CheckOut = Conversion.getDateTimeOrNull(R["CheckOut"]);
                        r.Night = Conversion.getInt16OrNull(R["Night"]);
                        r.HotCheckIn = Conversion.getDateTimeOrNull(R["HotCheckIn"]);
                        r.HotNight = Conversion.getInt16OrNull(R["HotNight"]);
                        r.Room = Conversion.getStrOrNull(R["Room"]);
                        r.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        r.RoomNameL = Conversion.getStrOrNull(R["RoomNameL"]);
                        r.Board = Conversion.getStrOrNull(R["Board"]);
                        r.BoardName = Conversion.getStrOrNull(R["BoardName"]);
                        r.BoardNameL = Conversion.getStrOrNull(R["BoardNameL"]);
                        r.Accom = Conversion.getStrOrNull(R["Accom"]);
                        r.AccomName = Conversion.getStrOrNull(R["AccomName"]);
                        r.AccomNameL = Conversion.getStrOrNull(R["AccomNameL"]);
                        r.AccomFullName = Conversion.getStrOrNull(R["AccomFullName"]);
                        r.CalcSalePrice = Conversion.getDecimalOrNull(R["CalcSalePrice"]);
                        r.SaleChdG1 = Conversion.getDecimalOrNull(R["SaleChdG1"]);
                        r.SaleChdG2 = Conversion.getDecimalOrNull(R["SaleChdG2"]);
                        r.SaleChdG3 = Conversion.getDecimalOrNull(R["SaleChdG3"]);
                        r.SaleChdG4 = Conversion.getDecimalOrNull(R["SaleChdG4"]);
                        r.HotChdPrice = Conversion.getDecimalOrNull(R["HotChdPrice"]);
                        r.OldSalePrice = Conversion.getDecimalOrNull(R["OldSalePrice"]);
                        r.NetCur = Conversion.getStrOrNull(R["NetCur"]);
                        r.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        r.PLCur = Conversion.getStrOrNull(R["PLCur"]);
                        r.AgencyEB = Conversion.getDecimalOrNull(R["AgencyEB"]);
                        r.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        r.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        r.DepCityNameL = Conversion.getStrOrNull(R["DepCityNameL"]);
                        r.DepFlight = Conversion.getStrOrNull(R["DepFlight"]);
                        r.DEPFlightTime = Conversion.getDateTimeOrNull(R["DepFlightTime"]);
                        r.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        r.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        r.ArrCityNameL = Conversion.getStrOrNull(R["ArrCityNameL"]);
                        r.RetFlight = Conversion.getStrOrNull(R["RetFlight"]);
                        r.RETFlightTime = Conversion.getDateTimeOrNull(R["RetFlightTime"]);
                        r.EBValidDate = Conversion.getDateTimeOrNull(R["EBValidDate"]);
                        r.Description = Conversion.getStrOrNull(R["Description"]);
                        r.SaleSPONo = Conversion.getInt32OrNull(R["SaleSPONo"]);
                        r.EBPerc = Conversion.getDecimalOrNull(R["EBPerc"]);
                        r.EBAmount = Conversion.getDecimalOrNull(R["EBAmount"]);
                        r.PasEBPer = Conversion.getDecimalOrNull(R["PasEBPer"]);
                        r.SaleEndDate = Conversion.getDateTimeOrNull(R["SaleEndDate"]);
                        r.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        r.Operator = Conversion.getStrOrNull(R["Operator"]);
                        r.Market = Conversion.getStrOrNull(R["Market"]);
                        r.FlightClass = Conversion.getStrOrNull(R["FlightClass"]);
                        r.plSpoExists = Conversion.getBoolOrNull(R["plSpoExists"]);
                        r.FlightValidDate = Conversion.getDateTimeOrNull(R["FlightValidDate"]);
                        r.HotelValidDate = Conversion.getDateTimeOrNull(R["HotelValidDate"]);
                        r.PEB_SaleEndDate = Conversion.getDateTimeOrNull(R["PEB_SaleEndDate"]);
                        r.PEB_ResEndDate = Conversion.getDateTimeOrNull(R["PEB_ResEndDate"]);
                        r.SaleAdl = Conversion.getDecimalOrNull(R["SaleAdl"]);
                        r.LastPrice = Conversion.getDecimalOrNull(R["LastPrice"]);
                        r.Direction = Conversion.getInt32OrNull(R["Direction"]);
                        r.Category = Conversion.getInt32OrNull(R["Category"]);
                        r.HolPackName = Conversion.getStrOrNull(R["HolPackName"]);
                        r.HolPackNameL = Conversion.getStrOrNull(R["HolPackNameL"]);
                        r.CurrentCur = Conversion.getStrOrNull(R["CurrentCur"]);
                        r.InfoWeb = Conversion.getStrOrNull(R["InfoWeb"]);
                        r.MaxChdAge = Conversion.getDecimalOrNull(R["MaxChdAge"]);
                        r.OprText = Conversion.getStrOrNull(R["OprText"]);
                        r.StopSaleGuar = Conversion.getInt32OrNull(R["StopSaleGuar"]);
                        r.StopSaleStd = Conversion.getInt32OrNull(R["StopSaleStd"]);
                        r.DFlight = Conversion.getStrOrNull(R["DFlight"]);
                        r.DepServiceType = Conversion.getStrOrNull(R["DepServiceType"]);
                        r.RetServiceType = Conversion.getStrOrNull(R["RetServiceType"]);

                        //2015-02-05 Vidas geri almamızı istedi.
                        //if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt))
                        //{
                        //    if (filter.ShowStopSaleHotels)
                        r.AutoStop = Conversion.getBoolOrNull(R["AutoStop"]);
                        //    else
                        //        r.AutoStop = false;
                        //}
                        //else
                        //{
                        //    r.AutoStop = Conversion.getBoolOrNull(R["AutoStop"]);
                        //}

                        r.TransportType = Conversion.getInt16OrNull(R["TransportType"]);
                        r.DepSeat = Conversion.getInt32OrNull(R["DepSeat"]);
                        r.RetSeat = Conversion.getInt32OrNull(R["RetSeat"]);
                        r.HotelFreeAllot = Conversion.getInt16OrNull(R["FreeAllot"]);
                        r.AgeGroupList = ageGroups;
                        r.OrderHotel = Conversion.getInt32OrNull(R["HotelOrder"]);
                        r.LogID = logID;
                        r.SearchTime = DateTime.Now;
                        result.Add(r);
                    }
                }
                sw.Stop();
                PriceSearchRequest preq = new PriceSearchRequest();
                preq.Adult = filter.RoomsInfo.Sum(s => s.Adult);
                preq.ArrCity = filter.ArrCity;
                preq.Board = filter.Board;
                preq.CatName = filter.Category;
                preq.CheckInFrom = filter.CheckIn;
                preq.Child = filter.RoomsInfo.Sum(s => s.Child);
                preq.Country = filter.Country;
                preq.Currency = filter.CurrentCur;
                preq.DepCity = filter.DepCity;
                preq.Direction = !string.IsNullOrEmpty(filter.Direction) ? Convert.ToInt32(filter.Direction) : 0;
                preq.FlightClass = filter.SClass;
                preq.HolPack = filter.Package;
                preq.HotelCode = filter.Hotel;
                preq.IsB2CAgency = false;
                preq.NightFrom = filter.NightFrom;
                preq.NightTo = filter.NightTo;
                preq.Room = filter.Room;
                preq.UserData = UserData;
                preq.PriceSearchTime = (int)sw.ElapsedMilliseconds;
                TourVisio.WebService.Adapter.Enums.enmProductType prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Hotel;
                switch (filter.SType)
                {
                    case SearchType.CruiseSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Cruise; break;
                    case SearchType.DynamicFlightPackageSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Dynamic; break;
                    case SearchType.HotelSale: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Hotel; break;
                    case SearchType.OnlyExcursion: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Excursion; break;
                    case SearchType.OnlyFlightSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Flight; break;
                    case SearchType.OnlyHotelSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Hotel; break;
                    case SearchType.OnlyTransfer: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Transfer; break;
                    case SearchType.PackageSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.HolidayPackage; break;
                    case SearchType.PackPriceSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.HolidayPackage; break;
                    case SearchType.TourPackageSearch: prodType = TourVisio.WebService.Adapter.Enums.enmProductType.Tour; break;
                }
                preq.ProductType = prodType;
                Task.Factory.StartNew(() => SanBI.TrySend(preq, result.Count));
                return result;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return result;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> checkStopSale(User UserData, List<SearchResult> checkedData, ref string errorMsg)
        {
            if (checkedData == null)
                return null;
            var query = from q in checkedData
                        where !q.StopSaleGuar.HasValue || !q.StopSaleStd.HasValue
                        group q by new
                        {
                            q.Hotel,
                            q.Room,
                            q.Accom,
                            q.Board,
                            q.Market,
                            q.CatPackID,
                            q.HotelLocation,
                            CheckIn = q.HotCheckIn.HasValue ? q.HotCheckIn.Value : q.CheckIn,
                            Night = q.HotCheckIn.HasValue ? q.HotNight : q.Night
                        } into k
                        select new
                        {
                            k.Key.Hotel,
                            k.Key.Room,
                            k.Key.Accom,
                            k.Key.Board,
                            k.Key.Market,
                            k.Key.CatPackID,
                            k.Key.HotelLocation,
                            k.Key.CheckIn,
                            k.Key.Night
                        };
            StringBuilder tSql = new StringBuilder();
            tSql.Append("Set DATEFORMAT DMY \n");
            tSql.Append("Declare @CheckTable TABLE (Hotel VarChar(10), Room VarChar(10), Accom VarChar(10), Board VarChar(10), Market VarChar(10), CatpackID int, HotLocation int, CheckIn DateTime, Night int) \n");
            foreach (var row in query)
            {
                tSql.Append("Insert Into @CheckTable (Hotel,Room,Accom,Board,Market,CatPackID,HotLocation,CheckIn,Night) \n");
                tSql.AppendFormat("Values ('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}',{8}) \n",
                                   row.Hotel,
                                   row.Room,
                                   row.Accom,
                                   row.Board,
                                   row.Market,
                                   row.CatPackID,
                                   row.HotelLocation,
                                   row.CheckIn.Value.ToString("dd-MM-yyyy"),
                                   row.Night);
            }
            tSql.Append("Select Hotel,Room,Accom,Board,Market,CatPackID,HotLocation,CheckIn,Night, \n");
            tSql.Append(" StopSaleGuar=dbo.ufn_StopSaleCnt(Hotel, Room, Accom, Board, @Market, @Agency, CatPackID, HotLocation, -2, CheckIn, CheckIn+Night, 0, 1), \n");
            tSql.Append(" StopSaleStd=dbo.ufn_StopSaleCnt(Hotel, Room, Accom, Board, @Market, @Agency, CatPackID, HotLocation, 2, CheckIn, CheckIn+Night, 0, 1) \n");
            tSql.Append("From @CheckTable ");
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tSql.ToString());
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {

                        List<SearchResult> rows = checkedData.Where(w =>
                                                        w.Hotel == Conversion.getStrOrNull(R["Hotel"]) &&
                                                        w.Room == Conversion.getStrOrNull(R["Room"]) &&
                                                        w.Accom == Conversion.getStrOrNull(R["Accom"]) &&
                                                        w.Board == Conversion.getStrOrNull(R["Board"]) &&
                                                        w.Market == Conversion.getStrOrNull(R["Market"]) &&
                                                        w.CatPackID == Conversion.getInt32OrNull(R["CatPackID"]) &&
                                                        w.HotelLocation == Conversion.getInt32OrNull(R["HotLocation"]) &&
                                                        w.CheckIn == Conversion.getDateTimeOrNull(R["CheckIn"]) &&
                                                        w.Night == Conversion.getInt16OrNull(R["Night"])
                                                  ).ToList<SearchResult>();
                        if (rows != null && rows.Count > 0)
                            foreach (SearchResult row in rows)
                            {
                                row.StopSaleStd = Conversion.getInt32OrNull(R["StopSaleStd"]);
                                row.StopSaleGuar = Conversion.getInt32OrNull(R["StopSaleGuar"]);
                            }
                    }
                    return checkedData;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> checkFreeSeatWV038(User UserData, List<SearchResult> checkedData, ref string errorMsg)
        {
            List<dbInformationShema> spShema = VersionControl.getSpShema("ufn_CheckTransportAlllotForSearch", ref errorMsg);

            if (checkedData == null)
                return null;
            var query = from q in checkedData
                        where !q.DepSeat.HasValue || !q.RetSeat.HasValue
                        group q by new
                        {
                            q.Operator,
                            q.Market,
                            q.DepFlight,
                            q.RetFlight,
                            q.CheckIn,
                            q.Night,
                            q.FlightClass,
                            q.HolPack,
                            q.DepCity,
                            q.DepServiceType,
                            q.ArrCity,
                            q.RetServiceType,
                            q.TransportType
                        } into k
                        select new
                        {
                            k.Key.Operator,
                            k.Key.Market,
                            k.Key.DepFlight,
                            k.Key.RetFlight,
                            k.Key.CheckIn,
                            k.Key.Night,
                            k.Key.FlightClass,
                            k.Key.HolPack,
                            k.Key.DepCity,
                            k.Key.DepServiceType,
                            k.Key.ArrCity,
                            k.Key.RetServiceType,
                            k.Key.TransportType
                        };
            StringBuilder tSql = new StringBuilder();
            tSql.Append("Set DATEFORMAT DMY \n");
            tSql.Append("Declare @CheckTable TABLE (Operator VarChar(10),Market VarChar(10),DepFlight VarChar(10),RetFlight VarChar(10),CheckIn DateTime,Night int,FlightClass VarChar(5),HolPack VarChar(10),DepCity int,DepServiceType VarChar(10),ArrCity int,RetServiceType VarChar(10),TransportType smallint) \n");
            foreach (var row in query)
            {
                tSql.Append("Insert Into @CheckTable (Operator,Market,DepFlight,RetFlight,CheckIn,Night,FlightClass,HolPack,DepCity,DepServiceType,ArrCity,RetServiceType,TransportType) \n");
                tSql.AppendFormat("Values ('{0}','{1}','{2}','{3}','{4}',{5},'{6}','{7}',{8},'{9}',{10},'{11}',{12}) \n",
                                   row.Operator,
                                   row.Market,
                                   row.DepFlight,
                                   row.RetFlight,
                                   row.CheckIn.Value.ToString("dd-MM-yyyy"),
                                   row.Night,
                                   row.FlightClass,
                                   row.HolPack,
                                   row.DepCity,
                                   row.DepServiceType,
                                   row.ArrCity,
                                   row.RetServiceType,
                                   row.TransportType);
            }
            tSql.Append("Select Operator,Market,DepFlight,RetFlight,CheckIn,Night,FlightClass,HolPack,DepServiceType,RetServiceType, \n");
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 37, VersionControl.Equality.gt))
            {
                tSql.Append(" DepSeat=Case When TransportType=2 Then \n");
                if (spShema.Where(w => w.Name == "@HolPack").Count() > 0)
                    tSql.Append("   dbo.ufn_CheckTransportAlllotForSearch(CheckIn,DepCity,ArrCity,Operator,Holpack)\n");
                else
                    tSql.Append("   dbo.ufn_CheckTransportAlllotForSearch(CheckIn,DepCity,ArrCity,Operator)\n");

                tSql.Append(" Else \n");
                tSql.Append("   dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 0, null)  \n");
                tSql.Append(" End, \n");
                tSql.Append(" RetSeat=Case When TransportType=2 Then \n");
                if (spShema.Where(w => w.Name == "@HolPack").Count() > 0)
                    tSql.Append("   dbo.ufn_CheckTransportAlllotForSearch(CheckIn+Night,ArrCity,DepCity,Operator,Holpack) \n");
                else
                    tSql.Append("   dbo.ufn_CheckTransportAlllotForSearch(CheckIn+Night,ArrCity,DepCity,Operator) \n");
                tSql.Append(" Else \n");
                tSql.Append("   dbo.ufn_Get_DepRet_FlightSeatWeb(Operator, Market, DepFlight, CheckIn, Night, FlightClass, null, HolPack, 1, null) \n");
                tSql.Append(" End \n");
            }
            else
            {
                tSql.Append(" DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator,Market,DepFlight,CheckIn,Night,FlightClass,null,HolPack,0,null), \n");
                tSql.Append(" RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator,Market,DepFlight,CheckIn,Night,FlightClass,null,HolPack,1,null) \n");
            }
            tSql.Append("From @CheckTable ");
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tSql.ToString());
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        List<SearchResult> rows = checkedData.Where(w =>
                                                    w.Operator == Conversion.getStrOrNull(R["Operator"]) &&
                                                    w.Market == Conversion.getStrOrNull(R["Market"]) &&
                                                    w.DepFlight == Conversion.getStrOrNull(R["DepFlight"]) &&
                                                    w.RetFlight == Conversion.getStrOrNull(R["RetFlight"]) &&
                                                    w.CheckIn == Conversion.getDateTimeOrNull(R["CheckIn"]) &&
                                                    w.Night == Conversion.getInt16OrNull(R["Night"]) &&
                                                    w.FlightClass == Conversion.getStrOrNull(R["FlightClass"]) &&
                                                    w.HolPack == Conversion.getStrOrNull(R["HolPack"]) &&
                                                    w.DepServiceType == Conversion.getStrOrNull(R["DepServiceType"]) &&
                                                    w.RetServiceType == Conversion.getStrOrNull(R["RetServiceType"])
                                                ).ToList<SearchResult>();
                        if (rows != null && rows.Count > 0)
                            foreach (SearchResult row in rows)
                            {
                                row.DepSeat = Conversion.getInt32OrNull(R["DepSeat"]);
                                row.RetSeat = Conversion.getInt32OrNull(R["RetSeat"]);
                            }
                    }
                    return checkedData;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> checkFreeSeat(User UserData, List<SearchResult> checkedData, ref string errorMsg)
        {
            if (checkedData == null)
                return null;
            var query = from q in checkedData
                        where !q.DepSeat.HasValue || !q.RetSeat.HasValue
                        group q by new
                        {
                            q.Operator,
                            q.Market,
                            q.DepFlight,
                            q.CheckIn,
                            q.Night,
                            q.FlightClass,
                            q.HolPack
                        } into k
                        select new
                        {
                            k.Key.Operator,
                            k.Key.Market,
                            k.Key.DepFlight,
                            k.Key.CheckIn,
                            k.Key.Night,
                            k.Key.FlightClass,
                            k.Key.HolPack
                        };
            StringBuilder tSql = new StringBuilder();
            tSql.Append("Set DATEFORMAT DMY \n");
            tSql.Append("Declare @CheckTable TABLE (Operator VarChar(10), Market VarChar(10), DepFlight VarChar(10), CheckIn DateTime, Night int, FlightClass VarChar(5), HolPack VarChar(10)) \n");
            foreach (var row in query)
            {
                tSql.Append("Insert Into @CheckTable (Operator,Market,DepFlight,CheckIn,Night,FlightClass,HolPack) \n");
                tSql.AppendFormat("Values ('{0}','{1}','{2}','{3}',{4},'{5}','{6}') \n",
                                   row.Operator,
                                   row.Market,
                                   row.DepFlight,
                                   row.CheckIn.Value.ToString("dd-MM-yyyy"),
                                   row.Night,
                                   row.FlightClass,
                                   row.HolPack);
            }
            tSql.Append("Select Operator,Market,DepFlight,CheckIn,Night,FlightClass,HolPack, \n");
            tSql.Append(" DepSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator,Market,DepFlight,CheckIn,Night,FlightClass,null,HolPack,0,null), \n");
            tSql.Append(" RetSeat=dbo.ufn_Get_DepRet_FlightSeatWeb(Operator,Market,DepFlight,CheckIn,Night,FlightClass,null,HolPack,1,null) \n");
            tSql.Append("From @CheckTable ");
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tSql.ToString());
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {

                        List<SearchResult> rows = checkedData.Where(w =>
                                                    w.Operator == Conversion.getStrOrNull(R["Operator"]) &&
                                                    w.Market == Conversion.getStrOrNull(R["Market"]) &&
                                                    w.DepFlight == Conversion.getStrOrNull(R["DepFlight"]) &&
                                                    w.CheckIn == Conversion.getDateTimeOrNull(R["CheckIn"]) &&
                                                    w.Night == Conversion.getInt16OrNull(R["Night"]) &&
                                                    w.FlightClass == Conversion.getStrOrNull(R["FlightClass"]) &&
                                                    w.HolPack == Conversion.getStrOrNull(R["HolPack"])
                                                ).ToList<SearchResult>();
                        if (rows != null && rows.Count > 0)
                            foreach (SearchResult row in rows)
                            {
                                row.DepSeat = Conversion.getInt32OrNull(R["DepSeat"]);
                                row.RetSeat = Conversion.getInt32OrNull(R["RetSeat"]);
                            }
                    }
                    return checkedData;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> checkFreeRoom(User UserData, List<SearchResult> checkedData, ref string errorMsg)
        {
            //FreeRoom = dbo.ufn_HotelFreeAmount(R.Market, @Office, @Agency, R.Hotel, R.Room, R.Accom, -1, R.CheckIn, R.Night, 1)
            var query = from q in checkedData
                        where !q.FreeRoom.HasValue
                        group q by new
                        {
                            q.Market,
                            q.Hotel,
                            q.Room,
                            q.Accom,
                            CheckIn = q.HotCheckIn.HasValue ? q.HotCheckIn.Value : q.CheckIn,
                            Night = q.HotNight
                        } into k
                        select new
                        {
                            k.Key.Market,
                            k.Key.Hotel,
                            k.Key.Room,
                            k.Key.Accom,
                            k.Key.CheckIn,
                            k.Key.Night
                        };
            StringBuilder tSql = new StringBuilder();
            tSql.Append("Set DATEFORMAT DMY \n");
            tSql.Append("Declare @CheckTable TABLE (Market VarChar(10), Hotel VarChar(10), Room VarChar(10), Accom VarChar(10), CheckIn DateTime, Night int) \n");
            foreach (var row in query)
            {
                tSql.Append("Insert Into @CheckTable (Market,Hotel,Room,Accom,CheckIn,Night) \n");
                tSql.AppendFormat("Values ('{0}','{1}','{2}','{3}','{4}',{5}) \n",
                                   row.Market,
                                   row.Hotel,
                                   row.Room,
                                   row.Accom,
                                   row.CheckIn.Value.ToString("dd-MM-yyyy"),
                                   row.Night);
            }
            tSql.Append("Select Ct.Market,Ct.Hotel,Ct.Room,Ct.Accom,Ct.CheckIn,Ct.Night,  \n");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050005129"))
                tSql.Append("  FreeRoom=dbo.ufn_HotelFreeAmount(Ct.Market,@Office,@Agency,Ct.Hotel,Ct.Room,Ct.Accom,-1,Ct.CheckIn,Ct.Night,11), \n");
            else
                tSql.Append("  FreeRoom=dbo.ufn_HotelFreeAmount(Ct.Market,@Office,@Agency,Ct.Hotel,Ct.Room,Ct.Accom,-1,Ct.CheckIn,Ct.Night,1), \n");
            tSql.Append("  H.UseSysParamAllot,H.HotAllotChk,H.HotAllotNotAcceptFull,H.HotAllotWarnFull,H.HotAllotNotFullGA \n");
            tSql.Append("From @CheckTable Ct \n");
            tSql.Append("Join Hotel (NOLOCK) H ON H.Code=Ct.Hotel \n");

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tSql.ToString());
            dbCommand.CommandTimeout = 120;
            try
            {
                db.AddInParameter(dbCommand, "Office", DbType.String, UserData.OprOffice);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        string Market = Conversion.getStrOrNull(R["Market"]);
                        string Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        string Room = Conversion.getStrOrNull(R["Room"]);
                        string Accom = Conversion.getStrOrNull(R["Accom"]);
                        bool useSysParamAllot = Equals(Conversion.getStrOrNull(R["UseSysParamAllot"]), "Y");
                        bool hotAllotChk = useSysParamAllot ? UserData.TvParams.TvParamReser.HotAllotChk : Equals(Conversion.getStrOrNull(R["HotAllotChk"]), "Y");
                        bool hotAllotNotAcceptFull = useSysParamAllot ? UserData.TvParams.TvParamReser.HotAllotNotAcceptFull : Equals(Conversion.getStrOrNull(R["HotAllotNotAcceptFull"]), "Y");
                        bool hotAllotWarnFull = useSysParamAllot ? UserData.TvParams.TvParamReser.HotAllotWarnFull : Equals(Conversion.getStrOrNull(R["HotAllotWarnFull"]), "Y");
                        bool hotAllotNotFullGA = useSysParamAllot ? UserData.TvParams.TvParamReser.HotAllotNotFullGA : Equals(Conversion.getStrOrNull(R["HotAllotNotFullGA"]), "Y");

                        DateTime? CheckIn = Conversion.getDateTimeOrNull(R["CheckIn"]);
                        Int16? Night = Conversion.getInt16OrNull(R["Night"]);
                        List<SearchResult> rows = checkedData.Where(w =>
                                                        w.Market == Market &&
                                                        w.Hotel == Hotel &&
                                                        w.Room == Room &&
                                                        w.Accom == Accom &&
                                                        (w.HotCheckIn.HasValue ? w.HotCheckIn.Value : w.CheckIn) == CheckIn &&
                                                        w.HotNight == Night
                                                    ).ToList<SearchResult>();
                        if (rows != null && rows.Count > 0)
                            foreach (SearchResult row in rows)
                            {
                                row.FreeRoom = Conversion.getInt32OrNull(R["FreeRoom"]);
                                row.HotAllotChk = hotAllotChk ? "Y" : "N";
                                row.HotAllotNotAcceptFull = hotAllotNotAcceptFull ? "Y" : "N";
                                row.HotAllotWarnFull = hotAllotWarnFull ? "Y" : "N";
                                row.HotAllotNotFullGA = hotAllotNotFullGA ? "Y" : "N";
                            }

                    }
                    return checkedData;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> getRoomSeatStopSaleControl(User UserData, List<SearchResult> checkedData, ref string errorMsg)
        {
            //checkedData = checkStopSale(UserData, checkedData, ref errorMsg);
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 37, VersionControl.Equality.gt))
                checkedData = checkFreeSeatWV038(UserData, checkedData, ref errorMsg);
            else
                checkedData = checkFreeSeat(UserData, checkedData, ref errorMsg);

            checkedData = checkFreeRoom(UserData, checkedData, ref errorMsg);

            return checkedData;
        }

        public List<B2BMenuCat> getB2BMenuCatList(User UserData, ref string errorMsg)
        {
            List<B2BMenuCat> records = new List<B2BMenuCat>();
            string tsql = @"Select RecID,Code,Name,[Status] 
                            From B2BMenuCat (NOLOCK)
                            ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(new B2BMenuCat
                        {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            Code = Conversion.getStrOrNull(R["Code"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            Status = Conversion.getBoolOrNull(R["Status"])
                        });

                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResult> getDynamicFlightSearch(User UserData, SearchCriteria filter, bool checkAvailableRoom, SearchType sType, bool checkAvailableFlightSeat, Guid? logID, ref string errorMsg)
        {
            string resortWhereStr = string.Empty;
            string categoryWhereStr = string.Empty;
            string hotelWhereStr = string.Empty;
            string roomWhereStr = string.Empty;
            string boardWhereStr = string.Empty;
            List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

            if (!string.IsNullOrEmpty(filter.Resort))
            {
                if (filter.Resort.Split('|').Length > 0)
                {
                    if (filter.Resort.Split('|').Length == 1)
                    {
                        resortWhereStr += string.Format("INSERT INTO #Resort(RecID) VALUES({0}) \n ", filter.Resort.Split('|')[0]);
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                    else
                    {
                        foreach (string r in filter.Resort.Split('|'))
                        {
                            Location _location = locations.Find(f => f.RecID == Conversion.getInt32OrNull(r));
                            if (_location != null)
                            {
                                if (_location.Type == 3)
                                {
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                                    foreach (Location rL in locations.Where(w => w.Parent == _location.RecID))
                                        resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", rL.RecID);
                                }
                                else
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                            }
                        }
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                }
                else
                    resortWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Category))
            {
                foreach (string r in filter.Category.Split('|'))
                    categoryWhereStr += string.Format("INSERT INTO @Category(Code) VALUES('{0}') \n", r);
                if (categoryWhereStr.Length > 0)
                    categoryWhereStr = "DECLARE @Category TABLE (Code VarChar(10)) \n" + categoryWhereStr;
                else
                    categoryWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Hotel))
            {
                foreach (string r in filter.Hotel.Split('|'))
                    hotelWhereStr += string.Format("INSERT INTO @Hotel(Code) VALUES('{0}') \n", r);
                if (hotelWhereStr.Length > 0)
                    hotelWhereStr = "DECLARE @Hotel TABLE (Code VarChar(10)) \n" + hotelWhereStr;
                else
                    hotelWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Room))
            {
                foreach (string r in filter.Room.Split('|'))
                    roomWhereStr += string.Format("INSERT INTO @Room(Code) VALUES('{0}') \n", r);
                if (roomWhereStr.Length > 0)
                    roomWhereStr = "DECLARE @Room TABLE (Code VarChar(10)) \n" + roomWhereStr;
                else
                    roomWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Board))
            {
                foreach (string r in filter.Board.Split('|'))
                    boardWhereStr += string.Format("INSERT INTO #tmpBoard(Code) VALUES('{0}') \n", r);
                if (boardWhereStr.Length > 0)
                    boardWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpBoard') is not null Drop Table dbo.#tmpBoard 
Create Table #tmpBoard (Code VarChar(10) COLLATE Latin1_General_CI_AS) " + boardWhereStr;
                else
                    boardWhereStr = string.Empty;
            }
            string xmlAgeTable = string.Empty;
            if (filter.RoomCount > 0)
            {
                xmlAgeTable += @"
if OBJECT_ID('TempDB.dbo.#tmpRoomTable') is not null Drop Table dbo.#tmpRoomTable 
Create Table #tmpRoomTable (RoomNr int, Adult int, Child int, ChdAge1 int, ChdAge2 int, ChdAge3 int, ChdAge4 int) ";
                foreach (SearchCriteriaRooms row in filter.RoomsInfo)
                {
                    xmlAgeTable += string.Format("Insert Into #tmpRoomTable (RoomNr, Adult, Child, ChdAge1, ChdAge2, ChdAge3, ChdAge4) Values ({0}, {1}, {2}, {3}, {4}, {5}, {6}) \n",
                        row.RoomNr,
                        row.Adult,
                        row.Child,
                        row.Chd1Age.HasValue ? row.Chd1Age.Value.ToString() : "-1",
                        row.Chd2Age.HasValue ? row.Chd2Age.Value.ToString() : "-1",
                        row.Chd3Age.HasValue ? row.Chd3Age.Value.ToString() : "-1",
                        row.Chd4Age.HasValue ? row.Chd4Age.Value.ToString() : "-1");
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("SET DATEFIRST 1 \n");
            sb.Append(xmlAgeTable);
            sb.Append("Declare @totalSeat int \n");
            sb.Append("Select @totalSeat=(SUM(Adult)+Sum(Child)-Sum((Case When ChdAge1 < 2 and ChdAge1 > -1 then 1 else 0 end)+ \n");
            sb.Append("				                                (Case When ChdAge2 < 2 and ChdAge2 > -1 then 1 else 0 end)+ \n");
            sb.Append("				                                (Case When ChdAge3 < 2 and ChdAge3 > -1 then 1 else 0 end)+ \n");
            sb.Append("				                                (Case When ChdAge4 < 2 and ChdAge4 > -1 then 1 else 0 end))) \n");
            sb.Append("From #tmpRoomTable \n");

            if (!string.IsNullOrEmpty(resortWhereStr))
                sb.AppendLine(resortWhereStr);
            if (!string.IsNullOrEmpty(categoryWhereStr))
                sb.AppendLine(categoryWhereStr);
            if (!string.IsNullOrEmpty(hotelWhereStr))
                sb.AppendLine(hotelWhereStr);
            if (!string.IsNullOrEmpty(roomWhereStr))
                sb.AppendLine(roomWhereStr);
            if (!string.IsNullOrEmpty(boardWhereStr))
                sb.AppendLine(boardWhereStr);

            sb.Append("Declare @CustomRegId VarChar(20), @SaleBegDate DateTime \n");
            sb.Append("Set @SaleBegDate=dbo.DateOnly(GetDate()) \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#PriceList') is not null Drop Table dbo.#PriceList \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#AgencyEB1') is not null Drop Table dbo.#AgencyEB1 \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#AgencyEB') is not null Drop Table dbo.#AgencyEB \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#PriceListEB') is not null Drop Table dbo.#PriceListEB \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#PriceListStopSale') is not null Drop Table dbo.#PriceListStopSale \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location \n");
            sb.Append("CREATE TABLE TempDB.dbo.#Location \n");
            sb.Append("( \n");
            sb.Append("	RecID int, \n");
            sb.Append("	Name varChar(30), \n");
            sb.Append("	NameL nvarChar(300), \n");
            sb.Append("	Village int, \n");
            sb.Append("	Town int, \n");
            sb.Append("	City int, \n");
            sb.Append("	Country int \n");
            sb.Append(") \n");
            sb.Append(" \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#FlightTbl') is not null Drop Table dbo.#FlightTbl \n");
            sb.Append("CREATE TABLE dbo.#FlightTbl \n");
            sb.Append("( \n");
            sb.Append("  CatPackID Int, \n");
            sb.Append("  FlightNo Varchar(10), \n");
            sb.Append("  StepType SmallInt \n");
            sb.Append(") \n");
            sb.Append(" \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#PriceTbl') is not null Drop Table dbo.#PriceTbl \n");
            sb.Append("CREATE TABLE dbo.#PriceTbl \n");
            sb.Append("( \n");
            sb.Append("  RoomNr int, \n");
            sb.Append("  HolPack Varchar(10), \n");
            sb.Append("  FlyDate Datetime, \n");
            sb.Append("  DFlightNo Varchar(10), \n");
            sb.Append("  DFClass1 Varchar(2), \n");
            sb.Append("  DFUnit1 Smallint, \n");
            sb.Append("  DFClass2 Varchar(2), \n");
            sb.Append("  DFUnit2 Smallint, \n");
            sb.Append("  DFClass3 Varchar(2), \n");
            sb.Append("  DFUnit3 Smallint, \n");
            sb.Append("  DSupplier Varchar(10), \n");
            sb.Append("  RFlightNo Varchar(10), \n");
            sb.Append("  RFClass1 Varchar(2), \n");
            sb.Append("  RFUnit1 Smallint, \n");
            sb.Append("  RFClass2 Varchar(2), \n");
            sb.Append("  RFUnit2 Smallint, \n");
            sb.Append("  RFClass3 Varchar(2), \n");
            sb.Append("  RFUnit3 Smallint, \n");
            sb.Append("  RSupplier Varchar(10), \n");
            sb.Append("  Airline Varchar(10), \n");
            sb.Append("  OldPrice Decimal(18,2), \n");
            sb.Append("  Price Decimal(18, 2), \n");
            sb.Append("  AdlPrice Decimal(18, 2), \n");
            sb.Append("  Currency Varchar(5), \n");
            sb.Append("  Departure Int, \n");
            sb.Append("  CatPackId Int, \n");
            sb.Append("  ARecNo Int, \n");
            sb.Append("  PRecNo Int, \n");
            sb.Append("  HapRecId Int, \n");
            sb.Append("  Night smallint, \n");
            sb.Append("  SPOStr Varchar(10), \n");
            sb.Append("  AdlSeat Smallint, \n");
            sb.Append("  ChdSeat Smallint, \n");
            sb.Append("  Hotel Varchar(10), \n");
            sb.Append("  Room Varchar(10), \n");
            sb.Append("  Accom Varchar(10), \n");
            sb.Append("  Board Varchar(10) \n");
            sb.Append(") \n");
            sb.Append(" \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#HolpackFlightTbl') is not null Drop Table dbo.#HolpackFlightTbl \n");
            sb.Append("CREATE TABLE dbo.#HolpackFlightTbl \n");
            sb.Append("( \n");
            sb.Append("  Holpack varchar(10), \n");
            sb.Append("  FlightNo varchar(10), \n");
            sb.Append("  CheckIn datetime, \n");
            sb.Append("  Night smallint, \n");
            sb.Append("  DepSeat int, \n");
            sb.Append("  RetSeat int \n");
            sb.Append(") \n");
            sb.Append(" \n");
            sb.Append("Declare @GetEBLastRecID SmallInt, @GetEBPLastRecID SmallInt,@SaleDate DateTime \n");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050025168"))
            {
                sb.Append(@"
Declare @EBCalcType smallint
--F.B. 08.03.2016 Last EB işi parametreye bağlandı.
Select @EBCalcType = isnull(EBCalcType,0) From ParamPrice (NOLOCK) Where Market=''

Set @GetEBLastRecID = @EBCalcType
Set @GetEBPLastRecID = @EBCalcType
");
            }
            else
            {
                sb.Append("Set @GetEBLastRecID=0 \n");
                sb.Append("Set @GetEBPLastRecID=0 \n");
                sb.Append("Set @SaleDate=dbo.DateOnly(GETDATE()) \n");
                sb.Append(" \n");
                sb.Append("if @CustomRegID in ('0970801','1210601','0969801','1323201','0855201') \n");
                sb.Append("begin \n");
                sb.Append("    Set @GetEBLastRecID=1 \n");
                sb.Append("    Set @GetEBPLastRecID=1 \n");
                sb.Append("end \n");
            }
            sb.Append("  \n");
            sb.Append("Insert Into #Location (RecID,Name,NameL,Village,Town,City,Country) \n");
            sb.Append("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Village,Town,City,Country \n");
            sb.Append("From Location \n");
            sb.Append("Order By RecID \n");
            sb.Append(" \n");
            sb.Append("Select @CustomRegID=CustomRegID From ParamSystem Where isnull(Market, '')='' \n");

            sb.Append("Select RT.RoomNr,P.CatPackID,P.ARecNo,P.Hotel,P.Room,P.Accom,P.Board, \n");
            sb.Append("  P.Adl,P.Chd,P.PriceAID,P.PRecNo,P.CheckIn,CheckOut=(P.CheckIn+P.Night),P.Night,P.HotCheckIn,P.HotNight, \n");
            sb.Append("  P.SaleAdl,P.SaleExtBed1,P.SaleExtBed2,P.ChdG1Age1,P.ChdG1Age2,P.SaleChdG1,P.ChdG2Age1,P.ChdG2Age2,P.SaleChdG2, \n");
            sb.Append("  P.ChdG3Age1,P.ChdG3Age2,P.SaleChdG3,P.ChdG4Age1,P.ChdG4Age2,P.SaleChdG4, \n");
            sb.Append("  P.OSalePrice,PLCur=P.SaleCur,P.SaleCur,P.SaleBegDate,P.SaleEndDate,CPName,CPNameLID,[Description]=CPDescription, \n");
            sb.Append("  P.HolPack,P.Operator,P.Market,P.SaleSPONo,P.PubDate,P.CWebPub,P.WebPubDate,P.EBValid, \n");
            sb.Append("  P.DepCity,P.ArrCity,P.FlightClass,P.HotelChdRecNo,P.HAPRecID,P.HAdult,P.HChdAgeG1, \n");
            sb.Append("  P.HChdAgeG2,P.HChdAgeG3,P.HChdAgeG4,P.HotelName,P.HotCat,HotelLocation=P.HotLocation,P.RoomName,P.AccomName,P.BoardName, \n");
            sb.Append("  P.AccomFullName,P.CalcSalePrice,P.ChdCalcSale,P.PasEBValid, \n");
            sb.Append("  OldSalePrice=P.CalcSalePrice,HotChdPrice,P.NetCur,P.Direction,P.Category, \n");
            sb.Append("  StopSaleGuar=dbo.ufn_StopSaleCnt(P.Hotel,P.Room,P.Accom,P.Board,@Market,@Agency,P.CatPackID,P.HotLocation,-2,P.HotCheckIn,P.HotCheckIn+P.HotNight,0,1), \n");
            sb.Append("  StopSaleStd=dbo.ufn_StopSaleCnt(P.Hotel,P.Room,P.Accom,P.Board,@Market,@Agency,P.CatPackID,P.HotLocation,2,P.HotCheckIn,P.HotCheckIn+P.HotNight,0,1), \n");
            sb.Append("   \n");
            sb.Append("  SpoPer=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  SpoAdlVal=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  SpoExtB1Val=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  SpoExtB2Val=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  SpoChdGrp1Val=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  SpoChdGrp2Val=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  SpoChdGrp3Val=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  SpoChdGrp4Val=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  SpoFlightTot=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  SpoHotelTot=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  PasEBValidFlight=1, \n");
            sb.Append("  PasEBValidHotel=1, \n");
            sb.Append("  EBFlightValid=1, \n");
            sb.Append("  EBHotelValid=1, \n");
            sb.Append("  P.PlSpoExists, \n");
            sb.Append("  TransportType=Cast(0 As smallint), \n");
            sb.Append("  DepFlightForSpo='          ', \n");
            sb.Append("  DepFlight='          ', \n");
            sb.Append("  RetFlight='          ', \n");
            sb.Append("  DFlight='', \n");
            sb.Append("  DepFlightTime=Cast(0 AS DateTime), \n");
            sb.Append("  RetFlightTime=Cast(0 AS DateTime), \n");
            sb.Append("  AgencyEB=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FreeAllot=Cast(0 as int), \n");
            sb.Append("  EBPerc=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  AutoStop=isnull(P.AutoStop,0), \n");
            sb.Append("  FlightValidDate=Cast(73048 AS DateTime), \n");
            sb.Append("  HotelValidDate=Cast(73048 AS DateTime), \n");
            sb.Append("  DepServiceType=isnull(CP.DepServiceType,''), \n");
            sb.Append("  RetServiceType=isnull(CP.RetServiceType,''), \n");
            sb.Append("  HNameLID,HRNameLID,HANameLID,HBNameLID, \n");
            sb.Append("  TotPLFlightPrice=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  TotAdlPLFlightPrice=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FDSaleAdl=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FDSaleExtBed1=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FDSaleExtBed2=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FDSaleChdG1=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FDSaleChdG2=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FDSaleChdG3=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FDSaleChdG4=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FRSaleAdl=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FRSaleExtBed1=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FRSaleExtBed2=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FRSaleChdG1=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FRSaleChdG2=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FRSaleChdG3=Cast(0.0 as dec(18,2)), \n");
            sb.Append("  FRSaleChdG4=Cast(0.0 as dec(18,2)) \n");
            sb.Append("Into #PriceList  \n");
            sb.Append("From PriceListExtV P  \n");
            sb.Append("Join CatalogPack CP ON P.CatPackID=CP.RecID \n");
            sb.Append("Join #tmpRoomTable RT on RT.Adult=P.HAdult And RT.Child=(P.HChdAgeG1+P.HChdAgeG2+P.HChdAgeG3+P.HChdAgeG4) \n");
            //sb.Append("Left JOIN HolPackPlan H on H.HolPack=P.HolPack and H.Service='FLIGHT' and H.Departure=P.DepCity and H.Arrival=P.ArrCity \n");
            //sb.Append("Left JOIN HolPackSer HS on HS.HolPack=H.HolPack and HS.StepNo=H.StepNo \n");
            //sb.Append("Left JOIN FlightDay FDD on FDD.FlightNo=HS.ServiceItem and FDD.FlyDate=P.CheckIn \n");
            //sb.Append("Left JOIN FlightDay FDR on FDR.FlightNo=FDD.NextFlight and FDR.FlyDate=(P.CheckIn+P.Night) \n");
            sb.Append("Where P.SaleEndDate>=@SaleBegDate \n");
            sb.Append("  And P.Ready='Y' \n");
            sb.Append("  And P.HotStatus=1 \n");
            sb.Append("  And P.CWebPub='Y' \n");
            sb.Append("  And P.WebPubDate<=GetDate() \n");
            sb.Append("  And @SaleBegDate BetWeen P.SaleBegDate And P.SaleEndDate \n");
            sb.Append("  And Exists(Select * From CatPackMarket Where CatPackID=P.CatPackID and Market=@Market and (Operator=@Operator or Operator='')) \n");
            sb.Append("  And isnull(P.PLFreeAmount,32000)>0 \n");
            // sb.Append("  And ( HS.ServiceItem is null or FDD.FlightNo is not null) \n");

            //if (filter.ExpandDate == 0)
            //    sb.Append("  And P.CheckIn=@CIn1 \n");
            //else 
            sb.Append("  And (P.CheckIn Between @CIn1 AND @CIn2) \n");

            if (filter.NightFrom != filter.NightTo && filter.NightTo >= filter.NightFrom)
                sb.Append("  And (P.Night between @Night1 and @Night2) \n");
            else
                sb.Append("  And P.Night=@Night1 \n");

            if (filter.DepCity.HasValue)
                sb.AppendFormat("  And P.DepCity={0} \n", filter.DepCity.Value.ToString());
            if (filter.ArrCity.HasValue)
            {
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                {
                    Location loc = locations.Find(f => f.RecID == filter.ArrCity.Value);
                    if (loc != null && loc.Parent == loc.RecID)
                        sb.AppendFormat("  And P.ArrCity in (Select City From Location (NOLOCK) Where Country={0}) \n", filter.ArrCity.Value.ToString());
                    else
                        sb.AppendFormat("  And P.ArrCity={0} \n", filter.ArrCity.Value.ToString());
                }
                else
                    sb.AppendFormat("  And P.ArrCity={0} \n", filter.ArrCity.Value.ToString());
            }

            sb.Append("And Exists(Select Distinct RecID From ( \n");
            sb.Append("  	Select CP.RecID  \n");
            sb.Append("  	From CatalogPack CP (NOLOCK) \n");
            sb.Append("    	Where \n");
            sb.Append("    		Not Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID) \n");
            sb.Append("    	AND CP.RecID = P.CatPackID \n");
            sb.Append("    	union all \n");
            sb.Append("    	Select CP.RecID \n");
            sb.Append("    	From CatalogPack CP (NOLOCK) \n");
            sb.Append("    	Where \n");
            sb.Append("    		Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=@Agency) \n");
            sb.Append("    	AND CP.RecID = P.CatPackID \n");
            sb.Append("    	union all \n");
            sb.Append("    	Select CP.RecID \n");
            sb.Append("    	From CatalogPack CP (NOLOCK) \n");
            sb.Append("    	Where \n");
            sb.Append("    		Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=(Select MainOffice From Agency (NOLOCK) Where Code=@Agency)) \n");
            sb.Append("    	AND CP.RecID = P.CatPackID \n");
            sb.Append("    ) CPA \n");
            sb.Append("  ) \n");
            sb.Append("  and P.PackType='H' \n");
            sb.Append("  and dbo.ChdAgeControl4Age(RT.Child,RT.ChdAge1,RT.ChdAge2,RT.ChdAge3,RT.ChdAge4,HChdAgeG1,HChdAgeG2,HChdAgeG3,HChdAgeG4,ChdG1Age1,ChdG1Age2,ChdG2Age1,ChdG2Age2,ChdG3Age1,ChdG3Age2,ChdG4Age1,ChdG4Age2)=1 \n");

            if (!string.IsNullOrEmpty(hotelWhereStr))
                sb.Append(" And Exists(Select Code From @Hotel Where Code=P.Hotel) \n");
            if (!string.IsNullOrEmpty(filter.Package))
                sb.AppendFormat("  And P.HolPack='{0}' \n", filter.Package);
            if (!string.IsNullOrEmpty(categoryWhereStr))
                sb.Append("  And Exists(Select Code From @Category Where Code=P.HotCat)");
            if (!string.IsNullOrEmpty(resortWhereStr))
            {
                if (filter.Resort.Split('|').Length == 1)
                {
                    Location _locations = locations.Find(f => f.RecID == Conversion.getInt32OrNull(filter.Resort.Split('|')[0]));
                    if (_locations != null && _locations.Type == 3)
                        sb.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.Town={0} And L.RecID=P.HotLocation) \n", _locations.RecID);
                    else
                        sb.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.RecID={0} And L.RecID=P.HotLocation) \n", _locations.RecID);
                }
                else
                    sb.Append("  And Exists(Select R.RecID From #tmpResort R Where R.RecID=P.HotLocation) \n");
            }
            if (!string.IsNullOrEmpty(roomWhereStr))
            {
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Room From GrpRoomDet GRD (NOLOCK) Where Exists(Select Code From @Room Where Code=GRD.Groups) And Room=P.Room) \n");
                else
                    sb.Append("  And Exists(Select Code From @Room Where Code=P.Room) \n");
            }
            if (!string.IsNullOrEmpty(boardWhereStr))
            {
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Board From GrpBoardDet GBD (NOLOCK) Where Exists(Select Code From @Board Where Code=GBD.Groups) And Board=P.Board) \n");
                else
                    sb.Append("  And Exists(Select Code From @Board Where Code=P.Board) \n");
            }

            sb.Append("Delete From #PriceList  \n");
            sb.Append("Where StopSaleGuar=2 and StopSaleStd=2  \n");
            sb.Append(" \n");
            sb.Append("Delete From #PriceList  \n");
            sb.Append("Where not Exists  \n");
            sb.Append(" (Select * From (Select Max(CatPackID) as CatPackID, HolPack, Hotel, Room, Accom, Board, CheckIn, Night, StopSaleGuar, StopSaleStd, DepFlight From #PriceList  \n");
            sb.Append("                 Group by Hotel, HolPack, Room, Accom, Board, CheckIn, Night, StopSaleGuar, StopSaleStd, DepFlight) as Newest  \n");
            sb.Append("  Where Newest.CatPackID=#PriceList.CatPackID  \n");
            sb.Append("    and Newest.Hotel=#PriceList.Hotel  \n");
            sb.Append("    and Newest.HolPack=#PriceList.HolPack  \n");
            sb.Append("    and Newest.Room=#PriceList.Room  \n");
            sb.Append("    and Newest.Accom=#PriceList.Accom  \n");
            sb.Append("    and Newest.Board=#PriceList.Board  \n");
            sb.Append("    and Newest.CheckIn=#PriceList.CheckIn  \n");
            sb.Append("    and Newest.Night=#PriceList.Night  \n");
            sb.Append("    and Newest.StopSaleGuar=#PriceList.StopSaleGuar  \n");
            sb.Append("    and Newest.StopSaleStd=#PriceList.StopSaleStd  \n");
            sb.Append("    and Newest.DepFlight=#PriceList.DepFlight  \n");
            sb.Append("  ) \n");
            sb.Append("  \n");
            sb.Append("Delete From #PriceList  \n");
            sb.Append("Where not Exists  \n");
            sb.Append("	( Select *  \n");
            sb.Append("	  From (Select *  \n");
            sb.Append("			  From (  \n");
            sb.Append("					Select Hotel, CheckIn, Night, cnt=Count(Hotel)  \n");
            sb.Append("					From (  \n");
            sb.Append("						Select pl.Hotel, pl.CheckIn, pl.Night, pl.RoomNr  \n");
            sb.Append("						From #PriceList pl  \n");
            sb.Append("						Group By pl.Hotel, pl.CheckIn, pl.Night, pl.RoomNr  \n");
            sb.Append("					) X  \n");
            sb.Append("					Group By Hotel, CheckIn, Night  \n");
            sb.Append("			  ) Y  \n");
            sb.Append("			  Where cnt >= (Select Count(RoomNr) From #tmpRoomTable)  \n");
            sb.Append("	 ) as Newest  \n");
            sb.Append("	Where Newest.Hotel = #PriceList.Hotel  \n");
            sb.Append("	  And Newest.CheckIn = #PriceList.CheckIn  \n");
            sb.Append("	  And Newest.Night= #PriceList.Night)  \n");
            sb.Append("  \n");
            sb.Append("Declare @CurrentCatPackID int  \n");
            sb.Append("Declare cursorFlights CURSOR LOCAL FAST_FORWARD FOR \n");
            sb.Append("Select Distinct CatPackID from #PriceList \n");
            sb.Append("Open cursorFlights \n");
            sb.Append("Fetch Next FROM cursorFlights INTO @CurrentCatPackID \n");
            sb.Append("While @@FETCH_STATUS = 0 \n");
            sb.Append("Begin \n");
            sb.Append("  Update #PriceList \n");
            sb.Append("  Set FRSaleAdl=X.SaleAdl,  \n");
            sb.Append("      FRSaleExtBed1=X.SaleExtBed1, \n");
            sb.Append("	  FRSaleExtBed2=X.SaleExtBed2, \n");
            sb.Append("	  FRSaleChdG1=X.SaleChdG1, \n");
            sb.Append("	  FRSaleChdG2=X.SaleChdG2, \n");
            sb.Append("	  FRSaleChdG3=X.SaleChdG3, \n");
            sb.Append("	  FRSaleChdG4=X.SaleChdG4  \n");
            sb.Append("  From (  \n");
            sb.Append("      Select S.CatPackID,N.CatPricePID,Service=S.Service,DA,S.SClass,S.SaleAdl,S.SaleExtBed1,S.SaleExtBed2,S.SaleChdG1,S.SaleChdG2,S.SaleChdG3,S.SaleChdG4 \n");
            sb.Append("      From CatService S (NOLOCK)  \n");
            sb.Append("      Join CatServicEndx N (NOLOCK) on N.CatPackID=S.CatPackID and N.CatServiceID=S.RecNo  \n");
            sb.Append("      Where N.CatPackID=@CurrentCatPackID \n");
            sb.Append("        and S.ServiceType='FLIGHT' \n");
            sb.Append("        and DA<>0  \n");
            sb.Append("  ) X \n");
            sb.Append("  Where #PriceList.CatPackID=@CurrentCatPackID \n");
            sb.Append("      and #PriceList.PRecNo=X.CatPricePID \n");
            sb.Append("      and LTrim(DepFlightForSpo)='' \n");
            sb.Append("  \n");
            sb.Append("  Update #PriceList \n");
            sb.Append("  Set DepFlightForSpo=X.Service, \n");
            sb.Append("	  FDSaleAdl=X.SaleAdl, \n");
            sb.Append("	  FDSaleExtBed1=X.SaleExtBed1, \n");
            sb.Append("	  FDSaleExtBed2=X.SaleExtBed2, \n");
            sb.Append("	  FDSaleChdG1=X.SaleChdG1, \n");
            sb.Append("	  FDSaleChdG2=X.SaleChdG2, \n");
            sb.Append("	  FDSaleChdG3=X.SaleChdG3, \n");
            sb.Append("	  FDSaleChdG4=X.SaleChdG4  \n");
            sb.Append("  From (  \n");
            sb.Append("	  Select S.CatPackID,N.CatPricePID,Service=S.Service,DA,S.SClass,S.SaleAdl,S.SaleExtBed1,S.SaleExtBed2,S.SaleChdG1,S.SaleChdG2,S.SaleChdG3,S.SaleChdG4 \n");
            sb.Append("      From CatService S (NOLOCK)  \n");
            sb.Append("      Join CatServicEndx N (NOLOCK) on N.CatPackID=S.CatPackID and N.CatServiceID=S.RecNo  \n");
            sb.Append("      Where N.CatPackID=@CurrentCatPackID \n");
            sb.Append("        and S.ServiceType='FLIGHT' \n");
            sb.Append("        and DA=0  \n");
            sb.Append("  ) X  \n");
            sb.Append("  Where #PriceList.CatPackID=@CurrentCatPackID \n");
            sb.Append("    and #PriceList.PRecNo=X.CatPricePID \n");
            sb.Append("    and LTrim(DepFlightForSpo)=''  \n");
            sb.Append(" \n");
            sb.Append("  Fetch Next FROM cursorFlights INTO @CurrentCatPackID \n");
            sb.Append("End \n");
            sb.Append("Close cursorFlights \n");
            sb.Append("DeAllocate cursorFlights \n");
            sb.Append("  \n");
            sb.Append("--Total adult flight price & total flight price  \n");
            sb.Append("Update #PriceList \n");
            sb.Append("Set TotAdlPLFlightPrice=FDSaleAdl+FRSaleAdl,  \n");
            sb.Append("    TotPLFlightPrice=  \n");
            sb.Append("     ((Case When HAdult>Adl then Adl else HAdult end)*(FDSaleAdl+FRSaleAdl))+  \n");
            sb.Append("     Case when HAdult-Adl>=1 then (FDSaleExtBed1+FRSaleExtBed1) else 0 end+  \n");
            sb.Append("     Case when HAdult-Adl>=2 then (FDSaleExtBed2+FRSaleExtBed2) else 0 end+  \n");
            sb.Append("     Case when HChdAgeG1>0 then (FDSaleChdG1+FRSaleChdG1)*HChdAgeG1 else 0 end+  \n");
            sb.Append("     Case when HChdAgeG2>0 then (FDSaleChdG2+FRSaleChdG2)*HChdAgeG2 else 0 end+  \n");
            sb.Append("     Case when HChdAgeG3>0 then (FDSaleChdG3+FRSaleChdG3)*HChdAgeG3 else 0 end+  \n");
            sb.Append("     Case when HChdAgeG4>0 then (FDSaleChdG4+FRSaleChdG4)*HChdAgeG4 else 0 End  \n");
            sb.Append("--Pricelist total adult price  \n");
            sb.Append("  \n");
            sb.Append("Update #PriceList  \n");
            sb.Append("Set  \n");
            sb.Append(" SpoPer=Y.SpoPer,  \n");
            sb.Append(" SpoAdlVal=Y.AdlVal,  \n");
            sb.Append(" SpoExtB1Val=Y.ExtB1Val,  \n");
            sb.Append(" SpoExtB2Val=Y.ExtB2Val,  \n");
            sb.Append(" SpoChdGrp1Val =case when X.SaleChdG1 > 0 then Y.ChdGrp1Val else 0 end, \n");
            sb.Append(" SpoChdGrp2Val =case when X.SaleChdG2 > 0 then Y.ChdGrp2Val else 0 end, \n");
            sb.Append(" SpoChdGrp3Val =case when X.SaleChdG3 > 0 then Y.ChdGrp3Val else 0 end, \n");
            sb.Append(" SpoChdGrp4Val =case when X.SaleChdG4 > 0 then Y.ChdGrp4Val else 0 end, \n");
            sb.Append(" SpoFlightTot=Y.FlightTot,  \n");
            sb.Append(" SpoHotelTot=Y.HotelTot,  \n");
            sb.Append(" PasEBValidFlight=Y.PEBValidForFlight,  \n");
            sb.Append(" PasEBValidHotel=Y.PEBValidForHotel,  \n");
            sb.Append(" EBFlightValid = isnull((Select Top 1 IsNull(PayPasEB,0) From CatPackPlan Where CatPackID=X.CatPackID and Service='FLIGHT'),0),  \n");
            sb.Append(" EBHotelValid = isnull((Select Top 1 IsNull(PayPasEB,0) From CatPackPlan Where CatPackID=X.CatPackID and Service='HOTEL'),0)  \n");
            sb.Append("From #PriceList X \n");
            sb.Append("Outer Apply  \n");
            sb.Append("(  \n");
            sb.Append("  Select * From dbo.PLSpoVals(0,X.CatPackID,@SaleBegDate,X.CheckIn,X.Night,X.Hotel,X.Room,X.Accom,X.Board,X.Adl,X.HAdult,X.HChdAgeG1,X.HChdAgeG2,X.HChdAgeG3,X.HChdAgeG4,X.HotelLocation,X.HotCat,0,X.DepFlightForSpo,@Agency,@CustomRegID) \n");
            sb.Append(") Y \n");
            sb.Append("Where CatPackID=X.CatPackID  \n");
            sb.Append("  and ARecNo=X.ARecNo  \n");
            sb.Append("  and PRecNo=X.PRecNo  \n");
            sb.Append("  and HAPRecID=X.HAPRecID  \n");
            sb.Append("  and PlSpoExists=1  \n");
            sb.Append(" \n");
            sb.Append("Update #PriceList  \n");
            sb.Append("Set  \n");
            sb.Append("  SaleAdl=SaleAdl*(1+IsNull(SpoPer,0)/100),  \n");
            sb.Append("  SaleExtBed1=Case when SaleExtBed1<>0 then SaleExtBed1*(1+IsNull(SpoPer,0)/100) else 0 end,  \n");
            sb.Append("  SaleExtBed2=Case when SaleExtBed2<>0 then SaleExtBed2*(1+IsNull(SpoPer,0)/100) else 0 end,  \n");
            sb.Append("  SaleChdG1=Case when SaleChdG1<>0 then SaleChdG1*(1+IsNull(SpoPer,0)/100) else 0 end,  \n");
            sb.Append("  SaleChdG2=Case when SaleChdG2<>0 then SaleChdG2*(1+IsNull(SpoPer,0)/100) else 0 end,  \n");
            sb.Append("  SaleChdG3=Case when SaleChdG3<>0 then SaleChdG3*(1+IsNull(SpoPer,0)/100) else 0 end,  \n");
            sb.Append("  SaleChdG4=Case when SaleChdG4<>0 then SaleChdG4*(1+IsNull(SpoPer,0)/100) else 0 end,  \n");
            sb.Append("  SpoFlightTot=Case When SpoPer=0 Then SpoFlightTot \n");
            sb.Append("                    else (Case When HAdult>Adl then Adl else HAdult end * SaleAdl * IsNull(SpoPer,0)/100+  \n");
            sb.Append("                          Case when HAdult-Adl>=1 then SaleExtBed1 * IsNull(SpoPer,0)/100 else 0 end+  \n");
            sb.Append("                          Case when HAdult-Adl>=2 then SaleExtBed2 * IsNull(SpoPer,0)/100 else 0 end+  \n");
            sb.Append("                          Case when HChdAgeG1>0 then SaleChdG1 * HChdAgeG1 * IsNull(SpoPer,0)/100 else 0 end+  \n");
            sb.Append("                          Case when HChdAgeG2>0 then SaleChdG2 * HChdAgeG2 * IsNull(SpoPer,0)/100 else 0 end+  \n");
            sb.Append("                          Case when HChdAgeG3>0 then SaleChdG3 * HChdAgeG3 * IsNull(SpoPer,0)/100 else 0 end+  \n");
            sb.Append("                          Case when HChdAgeG4>0 then SaleChdG4 * HChdAgeG4 * IsNull(SpoPer,0)/100 else 0 end)  \n");
            sb.Append("               End \n");
            sb.Append("Where PlSpoExists=1 and isnull(SpoPer, 0) <> 0 \n");
            sb.Append(" \n");
            sb.Append("Update #PriceList  \n");
            sb.Append("Set SaleAdl=SaleAdl+IsNull(SpoAdlVal,0), \n");
            sb.Append("  SaleExtBed1=Case when SaleExtBed1<>0 or IsNull(SpoExtB1Val,0)>0 then SaleExtBed1+IsNull(SpoExtB1Val,0) else 0 end,  \n");
            sb.Append("  SaleExtBed2=Case when SaleExtBed2<>0 or IsNull(SpoExtB2Val,0)>0 then SaleExtBed2+IsNull(SpoExtB2Val,0) else 0 end,  \n");
            sb.Append("  SaleChdG1=Case when SaleChdG1<>0 or IsNull(SpoChdGrp1Val,0)>0 then SaleChdG1+IsNull(SpoChdGrp1Val,0) else 0 end,  \n");
            sb.Append("  SaleChdG2=Case when SaleChdG2<>0 or IsNull(SpoChdGrp2Val,0)>0 then SaleChdG2+IsNull(SpoChdGrp2Val,0) else 0 end,  \n");
            sb.Append("  SaleChdG3=Case when SaleChdG3<>0 or IsNull(SpoChdGrp3Val,0)>0 then SaleChdG3+IsNull(SpoChdGrp3Val,0) else 0 end,  \n");
            sb.Append("  SaleChdG4=Case when SaleChdG4<>0 or IsNull(SpoChdGrp4Val,0)>0 then SaleChdG4+IsNull(SpoChdGrp4Val,0) else 0 end \n");
            sb.Append("Where PlSpoExists=1 and  (isnull(SpoPer, 0) = 0 or SpoHotelTot <> 0) \n");
            sb.Append(" \n");
            sb.Append("Select X.* \n");
            sb.Append("Into #PriceListEB  \n");
            sb.Append("From ( \n");
            sb.Append("    Select R.*,  \n");
            sb.Append("        PEB_Amount=Cast(0.0 as dec(18,2)), \n");
            sb.Append("        PasEBPer=IsNull(PEB.PasEBPer,0), \n");
            sb.Append("        PEB_AdlVal=IsNull(PEB.AdlVal,0), \n");
            sb.Append("        PEB_EBedVal=IsNull(PEB.EBedVal,0), \n");
            sb.Append("        PEB_ChdGrp1Val=IsNull(PEB.ChdGrp1Val,0), \n");
            sb.Append("        PEB_ChdGrp2Val=IsNull(PEB.ChdGrp2Val,0), \n");
            sb.Append("        PEB_ChdGrp3Val=IsNull(PEB.ChdGrp3Val,0), \n");
            sb.Append("        PEB_ChdGrp4Val=IsNull(PEB.ChdGrp4Val,0), \n");
            sb.Append("        EBValidDate=Case When PEB.PasEBPer is null Then Cast(73048 AS DateTime) Else PEB.SaleEndDate End,  \n");
            sb.Append("        HKB_Exists=Case When HKB.PriceType is null then 0 else 1 end,  \n");
            sb.Append("        HKB_PriceType=HKB.PriceType, \n");
            sb.Append("        HKB_CalcType=HKB.CalcType, \n");
            sb.Append("        HKB_AdlPrice=HKB.AdlPrice, \n");
            sb.Append("        HKB_SupPer=HKB.SupPer, \n");
            sb.Append("        HKB_EBOpt=HKB.EBOpt, \n");
            sb.Append("        HKB_Cur=HKB.Cur, \n");
            sb.Append("        HKB_Amount=Cast(0 as dec(18,2)), \n");
            sb.Append("        SPWithEB=R.CalcSalePrice,  \n");
            sb.Append("        PasEBPer2=Cast(0.0 as dec(5,2)), \n");
            sb.Append("        PEB_RecID=PEB.EBRecID,PEB_ResBegDate=PEB.ResBegDate,PEB_CinAccom=PEB.CinAccom, \n");
            sb.Append("        PEB_ResEndDate=case when PEB.CinAccom=0 then R.CheckIn+R.Night-1 \n");
            sb.Append("                            when PEB.ResEndDate<R.CheckIn+R.Night-1 then PEB.ResEndDate  \n");
            sb.Append("                            else R.CheckIn+R.Night-1 \n");
            sb.Append("                       end,  \n");
            sb.Append("        EBAmount=Case When R.PasEBValid='Y' And (PEB.PasEBPer is not null or PEB.AdlVal is not null) then  \n");
            sb.Append("                            dbo.ufn_GetEBAmountPL(R.CatPackID,R.ARecNo,R.PRecNo,HapRecID,R.Adl,R.ChdG1Age1,R.ChdG1Age2,R.ChdG2Age1,R.ChdG2Age2,R.ChdG3Age1,R.ChdG3Age2,R.ChdG4Age1,R.ChdG4Age2,R.HotelChdRecNo,R.ChdCalcSale,R.HAdult,HChdAgeG1,HChdAgeG2,HChdAgeG3,HChdAgeG4,0,@CustomRegID,HotChdPrice) \n");
            sb.Append("                      else 0 end,  \n");
            sb.Append("        PEB_SaleEndDate=PEB.SaleEndDate, \n");
            sb.Append("        AdlEBAmount=Cast(0 as dec(18,2)) \n");
            sb.Append("    From #PriceList R  \n");
            sb.Append("    -- 2010-02-11 KickBack \n");
            sb.Append("    OUTER APPLY  \n");
            sb.Append("    (  \n");
            sb.Append("          Select Top 1 PriceType,CalcType,AdlPrice,SupPer,EBOpt,Cur \n");
            sb.Append("          From HotelFeed (NOLOCK) HF \n");
            sb.Append("          Where Hotel=R.Hotel and Status=1 and ForSale=1 and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate \n");
            sb.Append("          and R.CheckIn between BegDate and EndDate \n");
            sb.Append("          and R.Night between IsNull(MinDay,0) and IsNull(MaxDay,999) \n");
            sb.Append("          and SubString(AccomDays,DATEPART(dw,R.CheckIn),1)=1 \n");
            sb.Append("          and (Room='' or Room=R.Room)  \n");
            sb.Append("          and (Board='' or Board=R.Board) \n");
            sb.Append("          and exists(Select * from HotelSeason (NOLOCK) Where Hotel = HF.Hotel and RecID = HF.SeasonID and Market=R.Market) \n");
            sb.Append("          and exists(Select * from HotelNetPrice (NOLOCK) Where SeasonID = HF.SeasonID and Hotel = HF.Hotel and Room = R.Room and Accom = R.Accom and Board = R.Board and R.CheckIn between BegDate and EndDate) \n");
            sb.Append("          Order by Room Desc, Board Desc,CrtDate Desc \n");
            sb.Append("    ) HKB \n");
            sb.Append("    OUTER APPLY \n");
            sb.Append("    ( \n");
            sb.Append("        Select Top 1 PasEBPer,AdlVal,EBedVal=IsNull(EBedVal, AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val, \n");
            sb.Append("          EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,EBP.SaleEndDate, \n");
            sb.Append("          LastEBRecID = EB.RecID * @GetEBLastRecID, LastEBPRecID = EBP.RecID * @GetEBPLastRecID \n");
            sb.Append("        From PasEB EB (NOLOCK)  \n");
            sb.Append("        Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID  \n");
            sb.Append("        Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=R.Hotel and (EBH.Room='' or EBH.Room=R.Room) and (EBH.Board='' or EBH.Board=R.Board) \n");
            sb.Append("        Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P' \n");
            sb.Append("        Where R.PasEBValid='Y' and OwnerOperator=R.Operator and OwnerMarket=R.Market and Operator=@Operator and Market=@Market and R.PasEBValid='Y' \n");
            sb.Append("              and ( (EB.HolPack=R.HolPack or PG.HolPack=R.HolPack) or (EB.HolPack='' and PG.HolPack is Null) )  \n");
            sb.Append("              and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate and R.CheckIn between ResBegDate and ResEndDate  \n");
            sb.Append("              and (R.Night between IsNull(MinDay,0) and IsNull(MaxDay,999)) \n");
            sb.Append("              and SubString(DoW,DATEPART(dw,R.CheckIn),1)=1 \n");
            sb.Append("              and DateDIFF(day,dbo.DateOnly(GetDate()),R.CheckIn) between IsNull(EBP.RemFromDay,0) and IsNull(EBP.RemToDay,999) \n");
            sb.Append("        Order by EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc, EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc,SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc  \n");
            sb.Append("    ) PEB \n");
            sb.Append("    -- 2010-02-11 KickBack \n");
            sb.Append(") X \n");
            sb.Append(" \n");
            sb.Append("Update #PriceListEB  \n");
            sb.Append("Set  \n");
            sb.Append("  CalcSalePrice= \n");
            sb.Append("   ((Case When HAdult>Adl then Adl else HAdult end) * SaleAdl)+  \n");
            sb.Append("	Case when HAdult-Adl>=1 then SaleExtBed1 else 0 end+  \n");
            sb.Append("	Case when HAdult-Adl>=2 then SaleExtBed2 else 0 end+  \n");
            sb.Append("	Case when HChdAgeG1>0 then SaleChdG1 * HChdAgeG1 else 0 end+  \n");
            sb.Append("	Case when HChdAgeG2>0 then SaleChdG2 * HChdAgeG2 else 0 end+  \n");
            sb.Append("	Case when HChdAgeG3>0 then SaleChdG3 * HChdAgeG3 else 0 end+  \n");
            sb.Append("	Case when HChdAgeG4>0 then SaleChdG4 * HChdAgeG4 else 0 end,  \n");
            sb.Append("	PlSpoExists=Case When IsNull(SpoPer,0)<>0 or  \n");
            sb.Append("						IsNull(SpoAdlVal,0)<>0 or \n");
            sb.Append("						(HAdult-Adl>=1 and IsNull(SpoExtB1Val,0)<>0 ) or \n");
            sb.Append("						(HAdult-Adl>=2 and IsNull(SpoExtB2Val,0)<>0 ) or \n");
            sb.Append("						(HChdAgeG1>0 and IsNull(SpoChdGrp1Val,0)<>0 ) or \n");
            sb.Append("						(HChdAgeG2>0 and IsNull(SpoChdGrp2Val,0)<>0 ) or \n");
            sb.Append("						(HChdAgeG3>0 and IsNull(SpoChdGrp3Val,0)<>0 ) or \n");
            sb.Append("						(HChdAgeG4>0 and IsNull(SpoChdGrp4Val,0)<>0 ) then 1 else 0 end,  \n");
            sb.Append("	EBAmount = case \n");
            sb.Append("			   when SpoPer = 0 then (EBAmount + (SpoFlightTot * isnull(EBFlightValid,0)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel)  \n");
            sb.Append("			   else ((EBAmount + (EBAmount * SpoPer * isnull(EBFlightValid,0) / 100)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel) \n");
            sb.Append("			 end,  \n");
            sb.Append("	AdlEBAmount=case \n");
            sb.Append("				  when SpoPer = 0 then (AdlEBAmount + (SpoFlightTot * isnull(EBFlightValid,0)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel)  \n");
            sb.Append("				  else ((AdlEBAmount + (AdlEBAmount * SpoPer * isnull(EBFlightValid,0) / 100)) + (SpoHotelTot * isnull(EBHotelValid,0))) * (PasEBValidFlight * PasEBValidHotel)  \n");
            sb.Append("				end,  \n");
            sb.Append("	PasEBPer = PasEBPer * PasEBValidFlight * PasEBValidHotel,  \n");
            sb.Append("	PEB_AdlVal=PEB_AdlVal * PasEBValidFlight * PasEBValidHotel \n");
            sb.Append("From #PriceListEB X \n");
            sb.Append("Where PlSpoExists=1 \n");
            sb.Append("  \n");
            sb.Append("Update #PriceListEB \n");
            sb.Append("Set HKB_Exists=0  \n");
            sb.Append("Where HKB_Exists=1 and (PasEBPer>0 or PEB_AdlVal<>0)  and HKB_EBOpt=1 \n");
            sb.Append(" \n");
            sb.Append("Update #PriceListEB  \n");
            sb.Append("Set HKB_Amount=dbo.Calc_HotKickBack(0, \n");
            sb.Append("                                    HKB_PriceType, \n");
            sb.Append("                                    HKB_CalcType, \n");
            sb.Append("                                    HKB_AdlPrice*dbo.ufn_Exchange(dbo.DateOnly(@SaleBegDate),HKB_Cur,SaleCur,1,1,Market), \n");
            sb.Append("                                    HKB_SupPer, \n");
            sb.Append("                                    HKB_Cur, \n");
            sb.Append("                                    Night, \n");
            sb.Append("                                    Adl, \n");
            sb.Append("                                    Case When HAdult-Adl>=1 Then 1 Else 0 End, \n");
            sb.Append("                                    Case When HAdult-Adl>=2 Then 1 else 0 End, \n");
            sb.Append("                                    CatPackID, \n");
            sb.Append("                                    PRecNo,  \n");
            sb.Append("                                    HotChdPrice, \n");
            sb.Append("                                    SaleCur, \n");
            sb.Append("                                    '',  \n");
            sb.Append("                                    SpoHotelTot) \n");
            sb.Append("Where HKB_Exists=1 \n");
            sb.Append(" \n");
            sb.Append("Update #PriceListEB  \n");
            sb.Append("Set CalcSalePrice=CalcSalePrice-HKB_Amount,  \n");
            sb.Append("	EBAmount=Case When IsNull(EBAmount,0)>0 Then EBAmount-HKB_Amount Else 0 End \n");
            sb.Append("Where HKB_Exists=1 \n");
            sb.Append(" \n");
            sb.Append("  Update #PriceList Set TransportType = Case when HP.Service = 'FLIGHT' Then 1 when HP.Service = 'TRANSPORT' Then 2 Else 0 End \n");
            sb.Append("  From HolPackPlan HP \n");
            sb.Append("  Where HP.HolPack = #PriceList.HolPack and HP.StepNo = 1 \n");
            sb.Append("  Update #PriceList Set TransportType = 2 \n");
            sb.Append("  From HolPackPlan HP \n");
            sb.Append("  Where #PriceList.TransportType = 0 And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack = #PriceList.HolPack And Service = 'TRANSPORT') Between 1 And 2 \n");
            sb.Append("  And (Select Count(HolPack) From HolPackPlan (Nolock) Where HolPack = #PriceList.HolPack And Service = 'FLIGHT') = 0 \n");
            sb.Append("  \n");
            sb.Append("--PasEbPer2 - 2.period ******************** \n");
            sb.Append("Update #PriceListEB \n");
            sb.Append("Set PasEbPer2=IsNUll(PEB2.PasEBPer,0)  \n");
            sb.Append("From #PriceListEB V  \n");
            sb.Append("OUTER APPLY  \n");
            sb.Append("(Select Top 1 PasEBPer=IsNull(PasEBPer,0),AdlVal,EBedVal=IsNull(EBedVal,AdlVal),ChdGrp1Val,ChdGrp2Val,ChdGrp3Val,ChdGrp4Val,EBRecID=EBP.RecID,ResBegDate,ResEndDate,CinAccom,  \n");
            sb.Append("		LastEBRecID=EB.RecID*@GetEBLastRecID,LastEBPRecID=EBP.RecID*@GetEBPLastRecID  \n");
            sb.Append(" From PasEB EB (NOLOCK)  \n");
            sb.Append(" Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID  \n");
            sb.Append(" Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=V.Hotel and (EBH.Room='' or EBH.Room=V.Room)and (EBH.Room='' or EBH.Board=V.Board)  \n");
            sb.Append(" Left Join GrpHolPackDet PG (NOLOCK) on PG.Groups = EB.HolPackGrp and PG.GrpType='P' \n");
            sb.Append(" Where EBP.RecID<>V.PEB_RecID and V.PasEBPer<>0 and  \n");
            sb.Append("   V.PasEBValid='Y' and OwnerOperator=V.Operator and OwnerMarket=V.Market and Operator=@Operator and Market=@Market and V.PasEBValid='Y' and EB.SaleType=0 and  \n");
            sb.Append("		( (EB.HolPack=V.HolPack or PG.HolPack=V.HolPack) or (EB.HolPack='' and PG.HolPack is Null) )  \n");
            sb.Append("		and dbo.DateOnly(@SaleBegDate) between SaleBegDate and SaleEndDate \n");
            sb.Append("		and V.PEB_ResEndDate+1 between ResBegDate and ResEndDate and CinAccom=1  \n");
            sb.Append("		and (V.Night between IsNull(MinDay,0) and IsNull(MaxDay,999))  \n");
            sb.Append("		and SubString(DoW,DATEPART(dw,V.CheckIn),1)=1  \n");
            sb.Append("        and DateDIFF(day,dbo.DateOnly(GetDate()),V.CheckIn) between IsNull(EBP.RemFromDay,0) and IsNull(EBP.RemToDay,999)  \n");
            sb.Append(" Order by LastEBRecID desc, EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc,EB.HolPack Desc,HolPackGrp Desc,EB.CrtDate Desc, LastEBPRecID Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc  \n");
            sb.Append(") PEB2  \n");
            sb.Append("Where V.PEB_CinAccom=1 and PEB_ResEndDate<V.CheckIn+Night-1  \n");
            sb.Append(" \n");
            sb.Append(" \n");
            sb.Append("Update #PriceListEB  \n");
            sb.Append("Set CalcSalePrice=CalcSalePrice-(IsNull(EBAmount,0)*PasEBPer* ((DATEDIFF(day,CheckIn,PEB_ResEndDate)+1)/ cast(Night as dec(18,6))) / 100) \n");
            sb.Append("                               -(IsNull(EBAmount,0)*PasEBPer2* ((DATEDIFF(day,PEB_ResEndDate,CheckIn+Night-1))/ cast(Night as dec(18,6))) / 100) \n");
            sb.Append("Where IsNull(EBAmount,0) > 0 and PasEBPer<>0 \n");
            sb.Append(" \n");
            sb.Append("Update #PriceListEB  \n");
            sb.Append("Set PEB_Amount=  Case When HAdult>0 and PEB_AdlVal<>0 then Case When HAdult>Adl then Adl else HAdult end * PEB_AdlVal else 0 end+ \n");
            sb.Append("                 Case When HAdult-Adl>=1 then PEB_EBedVal else 0 end+ \n");
            sb.Append("                 Case When HAdult-Adl>=2 then PEB_EBedVal else 0 end+ \n");
            sb.Append("                 Case When HChdAgeG1>0 and PEB_ChdGrp1Val<>0 then HChdAgeG1*PEB_ChdGrp1Val else 0 end+  \n");
            sb.Append("                 Case When HChdAgeG2>0 and PEB_ChdGrp2Val<>0 then HChdAgeG2*PEB_ChdGrp2Val else 0 end+  \n");
            sb.Append("                 Case When HChdAgeG3>0 and PEB_ChdGrp3Val<>0 then HChdAgeG3*PEB_ChdGrp3Val else 0 end+  \n");
            sb.Append("                 Case When HChdAgeG4>0 and PEB_ChdGrp4Val<>0 then HChdAgeG4*PEB_ChdGrp4Val else 0 end,  \n");
            sb.Append(" \n");
            sb.Append("CalcSalePrice=CalcSalePrice- \n");
            sb.Append("(  \n");
            sb.Append("  Case When HAdult>0 and PEB_AdlVal<>0 then Case When HAdult>Adl then Adl else HAdult end * PEB_AdlVal else 0 end+ \n");
            sb.Append("  Case When HAdult-Adl>=1 then PEB_EBedVal else 0 end+ \n");
            sb.Append("  Case When HAdult-Adl>=2 then PEB_EBedVal else 0 end+ \n");
            sb.Append("  Case When HChdAgeG1>0 and PEB_ChdGrp1Val<>0 then HChdAgeG1*PEB_ChdGrp1Val else 0 end+  \n");
            sb.Append("  Case When HChdAgeG2>0 and PEB_ChdGrp2Val<>0 then HChdAgeG2*PEB_ChdGrp2Val else 0 end+  \n");
            sb.Append("  Case When HChdAgeG3>0 and PEB_ChdGrp3Val<>0 then HChdAgeG3*PEB_ChdGrp3Val else 0 end+  \n");
            sb.Append("  Case When HChdAgeG4>0 and PEB_ChdGrp4Val<>0 then HChdAgeG4*PEB_ChdGrp4Val else 0 end \n");
            sb.Append(")  \n");
            sb.Append("Where PEB_AdlVal<>0  \n");
            sb.Append(" \n");
            sb.Append("Update #PriceListEB  \n");
            sb.Append("Set PEB_EBedVal=0, \n");
            sb.Append("	PEB_ChdGrp1Val=0, \n");
            sb.Append("	PEB_ChdGrp2Val=0, \n");
            sb.Append("	PEB_ChdGrp3Val=0, \n");
            sb.Append("	PEB_ChdGrp4Val=0  \n");
            sb.Append("Where PEB_AdlVal=0 \n");
            sb.Append(" \n");
            sb.Append("Update #PriceListEB  \n");
            sb.Append("Set PEB_Amount=PEB_Amount*-1 \n");
            sb.Append("Where PEB_Amount<>0  \n");
            sb.Append(" \n");
            sb.Append("Declare @catPackID int,@holPack varChar(10)  \n");
            sb.Append(" \n");
            sb.Append("-- Dynamic Flight start  \n");
            sb.Append("Declare cursorFlights Cursor Local Forward_Only Static For \n");
            sb.Append("Select Distinct CatPackID From #PriceListEB  \n");
            sb.Append("Open cursorFlights \n");
            sb.Append("Fetch Next From cursorFlights Into @catPackID  \n");
            sb.Append("While @@FETCH_STATUS=0 \n");
            sb.Append("Begin  \n");
            sb.Append("  Select @holPack=HolPack From CatalogPack Where RecID=@catPackID  \n");
            sb.Append(" \n");
            sb.Append("  Insert Into #FlightTbl (CatPackID,FlightNo,StepType) \n");
            sb.Append("  Select @catPackID,H.ServiceItem,P.StepType \n");
            sb.Append("  From HolPackSer H  \n");
            sb.Append("  Join HolPackPlan P On H.HolPack=P.HolPack and H.StepNo=P.StepNo  \n");
            sb.Append("  Where H.HolPack=@holPack \n");
            sb.Append("    and H.[Service]='FLIGHT' \n");
            sb.Append("    and P.StepType=1 \n");
            sb.Append(" \n");
            sb.Append("  Insert Into #FlightTbl (CatPackID,FlightNo,StepType) \n");
            sb.Append("  Select @catPackID,H.ServiceItem,P.StepType \n");
            sb.Append("  From HolPackSer H  \n");
            sb.Append("  Join HolPAckPlan P On H.HolPack=P.HolPack and H.StepNo=P.StepNo  \n");
            sb.Append("  Where H.HolPack=@holPack \n");
            sb.Append("    and H.[Service]='FLIGHT' \n");
            sb.Append("    and P.StepType=2 \n");
            sb.Append(" \n");
            sb.Append("  Fetch Next FROM cursorFlights INTO @catPackID  \n");
            sb.Append("End  \n");
            sb.Append("Close cursorFlights  \n");
            sb.Append("DeAllocate cursorFlights \n");
            sb.Append(" \n");
            sb.Append("Declare @pCatPackID int,@pCheckIn DateTime,@pNight smallint,@pHolPack varChar(10),@pDepCity int,@pcHotel varChar(10),@pcRoom varChar(10),@pcAccom varChar(10),@pcBoard varChar(10),@pOldPrice decimal(18,2),  \n");
            sb.Append("		@pOwnMarket varChar(10),@pOwnOperator varChar(10),  \n");
            sb.Append("		@pDepFlightCatPackID int,@pFlightNo varChar(10),  \n");
            sb.Append("		@pDepFree int,@pRetFree int,  \n");
            sb.Append("		@pNeedSeat int,@pPreSeat int, \n");
            sb.Append("		@pNeedSeatTmp int,@pPreSeatTmp int, \n");
            sb.Append("		@pAirLine varChar(10),  \n");
            sb.Append("		@pRoomNr int  \n");
            sb.Append(" \n");
            sb.Append("Select @pNeedSeat=@totalSeat,  \n");
            sb.Append("       @pNeedSeatTmp=0,  \n");
            sb.Append("       @pPreSeat=0,  \n");
            sb.Append("       @pPreSeatTmp=0  \n");
            sb.Append(" \n");
            sb.Append("Declare cursorCreatePriceTable Cursor Local Forward_Only Static For  \n");
            sb.Append("Select Distinct RoomNr,CatPackID,CheckIn,Night,HolPack,DepCity,Hotel,Room,Accom,Board,OldSalePrice \n");
            sb.Append("From #PriceListEB  \n");
            sb.Append("Order By CatPackID,CheckIn,Night,Hotel,RoomNr  \n");
            sb.Append("Open cursorCreatePriceTable  \n");
            sb.Append("Fetch Next From cursorCreatePriceTable Into @pRoomNr,@pCatPackID,@pCheckIn,@pNight,@pHolPack,@pDepCity,@pcHotel,@pcRoom,@pcAccom,@pcBoard,@pOldPrice  \n");
            sb.Append("While @@FETCH_STATUS=0  \n");
            sb.Append("Begin  \n");
            sb.Append("  Select @pOwnMarket=Market From HolPack Where Code=@pHolPack  \n");
            sb.Append("  Select @pOwnOperator=Code From Operator Where Market=@pOwnMarket \n");
            sb.Append("  \n");
            sb.Append("  Declare cursorDepFlight Cursor Local Forward_Only Static For  \n");
            sb.Append("  Select Distinct CatPackID,FlightNo \n");
            sb.Append("  From #FlightTbl  \n");
            sb.Append("  Where CatPackID=@pCatPackID \n");
            sb.Append("    and StepType=1  \n");
            sb.Append("  Open cursorDepFlight  \n");
            sb.Append("  Fetch Next From cursorDepFlight Into @pDepFlightCatPackID,@pFlightNo  \n");
            sb.Append("  While @@FETCH_STATUS=0  \n");
            sb.Append("  Begin  \n");
            sb.Append("	If Not Exists (Select null From #HolpackFlightTbl Where Holpack=@pHolPack and FlightNo=@pFlightNo and CheckIn=@pCheckIn and Night=@pNight)  \n");
            sb.Append("	Begin \n");
            sb.Append("      Select @pDepFree = dbo.ufn_Get_DepRet_FlightSeatWeb(@pOwnOperator,@pOwnMarket,@pFlightNo,@pCheckIn,@pNight,null,null,@pHolPack,0,null) \n");
            sb.Append("	  If @pDepFree>=@pNeedSeat+@pPreSeat  \n");
            sb.Append("	  Begin \n");
            sb.Append("	    Select @pRetFree=dbo.ufn_Get_DepRet_FlightSeatWeb(@pOwnOperator,@pOwnMarket,@pFlightNo,@pCheckIn,@pNight,null,null,@pHolPack,1,null)  \n");
            sb.Append("	  End \n");
            sb.Append("	  \n");
            sb.Append("	  Insert Into #HolpackFlightTbl(Holpack,FlightNo,CheckIn,DepSeat,RetSeat) \n");
            sb.Append("	  Values (@pHolpack,@pFlightNo,@pCheckIn,@pDepFree,@pRetFree) \n");
            sb.Append("	End \n");
            sb.Append("	Else  \n");
            sb.Append("	Begin \n");
            sb.Append("	  Select @pDepFree=DepSeat,@pRetFree=RetSeat From #HolpackFlightTbl WHERE Holpack=@pHolPack and FlightNo=@pFlightNo and CheckIn=@pCheckIn and Night=@pNight  \n");
            sb.Append("    End  \n");
            sb.Append("  \n");
            sb.Append("    If @pDepFree>=@pNeedSeat+@pPreSeat and @pRetFree>=@pNeedSeat+@pPreSeat \n");
            sb.Append("    Begin  \n");
            sb.Append("	  Select @pAirline=Airline From FlightDay \n");
            sb.Append("	  Where FlightNo=@pFlightNo \n");
            sb.Append("	  and FlyDate=@pCheckIn \n");
            sb.Append("	  \n");
            sb.Append("	  Insert Into #PriceTbl (RoomNr,HolPack,FlyDate,DFlightNo,DFClass1,DFUnit1,DFClass2,DFUnit2,DFClass3,DFUnit3,RFlightNo,RFClass1,RFUnit1,RFClass2,RFUnit2,RFClass3,RFUnit3,Airline,OldPrice,Price,AdlPrice, Currency, Departure,SPOStr,Hotel,Room,Accom,Board) \n");
            sb.Append("	  Values (@pRoomNr,@pHolPack,@pCheckIn,@pFlightNo,'',null,'',null,'',null,null,'',null,'',null,'',null,@pAirLine,@pOldPrice,null,null,'',@pDepCity,'',@pcHotel,@pcRoom,@pcAccom,@pcBoard)  \n");
            sb.Append("	End  \n");
            sb.Append("  \n");
            sb.Append("    Fetch Next From cursorDepFlight Into @pDepFlightCatPackID, @pFlightNo  \n");
            sb.Append("  End  \n");
            sb.Append("  Close cursorDepFlight  \n");
            sb.Append("  DeAllocate cursorDepFlight  \n");
            sb.Append("  \n");
            sb.Append("  Fetch Next From cursorCreatePriceTable Into @pRoomNr,@pCatPackID,@pCheckIn,@pNight,@pHolPack,@pDepCity,@pcHotel,@pcRoom,@pcAccom,@pcBoard,@pOldPrice  \n");
            sb.Append("End  \n");
            sb.Append("Close cursorCreatePriceTable  \n");
            sb.Append("DeAllocate cursorCreatePriceTable \n");
            sb.Append("  \n");
            sb.Append("-- Departure flight select class  \n");
            sb.Append("Declare @pIsOk bit,@pCurFlightNo varChar(10),@pCurFlyDate DateTime,@pFlyDate DateTime,  \n");
            sb.Append("		@pFClass varChar(5),@pSupplier varChar(10),@pRowCount int  \n");
            sb.Append("  \n");
            sb.Append("Set @pIsOk=0  \n");
            sb.Append("Set @pCurFlightNo=''  \n");
            sb.Append("Set @pCurFlyDate=Null \n");
            sb.Append("Set @pNeedSeatTmp=@pNeedSeat  \n");
            sb.Append("Set @pPreSeatTmp=@pPreSeat  \n");
            sb.Append("  \n");
            sb.Append("if OBJECT_ID('TempDB.dbo.#depFlightClass') is not null Drop Table dbo.#depFlightClass  \n");
            sb.Append("Select Distinct Pt.DFlightNo,F.FlyDate,F.SClass,B.Supplier,Pt.HolPack,B.SalePriority,P.[Priority]  \n");
            sb.Append("Into #depFlightClass  \n");
            sb.Append("From FlightPrice F  \n");
            sb.Append("Join #FlightTbl Ft On Ft.FlightNo=F.FlightNo  \n");
            sb.Append("Join #PriceTbl Pt On Pt.DFlightNo=F.FlightNo and Pt.FlyDate=F.FlyDate  \n");
            sb.Append("Left Join FlightClassPriority P On P.Class=F.SClass  \n");
            sb.Append("Outer Apply  \n");
            sb.Append("(  \n");
            sb.Append("  Select Top 1 SalePriority=isnull(SalePriority,0),Supplier From FlightBlock  \n");
            sb.Append("  Where FlightNo = F.FlightNo and RecID = F.BlockID  \n");
            sb.Append("  Order By SalePriority  \n");
            sb.Append(") B  \n");
            sb.Append("Where F.FlyDate Between @CIn1 And @CIn2  \n");
            sb.Append("  \n");
            sb.Append("Declare cursorDepFlightClass Cursor Local Forward_Only Static For  \n");
            sb.Append("Select DFlightNo,FlyDate,SClass,Supplier,HolPack  \n");
            sb.Append("From #depFlightClass  \n");
            sb.Append("Order By DFlightNo,FlyDate,SalePriority,[Priority],SClass  \n");
            sb.Append("Open cursorDepFlightClass  \n");
            sb.Append("Fetch Next FROM cursorDepFlightClass Into @pFlightNo,@pFlyDate,@pFClass,@pSupplier,@pHolPack  \n");
            sb.Append("While @@FETCH_STATUS = 0  \n");
            sb.Append("Begin  \n");
            sb.Append("    Select @pOwnMarket = Market From HolPAck (Nolock) Where Code = @pHolPack \n");
            sb.Append("    Select @pOwnOperator = Code From Operator (Nolock) Where Market = @pOwnMarket  \n");
            sb.Append(" \n");
            sb.Append("    If @pCurFlightNo=@pFlightNo and @pCurFlyDate=@pFlyDate and @pIsOk = 1  \n");
            sb.Append("    Begin  \n");
            sb.Append("      Select @pNeedSeatTmp=@pNeedSeat,@pPreSeatTmp=@pPreSeat \n");
            sb.Append(" \n");
            sb.Append("      Fetch Next From cursorDepFlightClass Into @pFlightNo,@pFlyDate,@pFClass,@pSupplier,@pHolPack \n");
            sb.Append("      continue \n");
            sb.Append("    End  \n");
            sb.Append(" \n");
            sb.Append("    Set @pIsOk = 0 \n");
            sb.Append(" \n");
            sb.Append("    Select @pDepFree=dbo.ufn_Get_DepRet_FlightSeatWeb(@pOwnOperator,@pOwnMarket,@pFlightNo,@pFlyDate,@Night1,@pFClass,null,@pHolPack,0,null)  \n");
            sb.Append("  \n");
            sb.Append("    If @pDepFree<@pPreSeatTmp \n");
            sb.Append("    Begin  \n");
            sb.Append("      Set @pPreSeatTmp=@pPreSeatTmp-@pDepFree  \n");
            sb.Append("      Fetch Next FROM cursorDepFlightClass Into @pFlightNo,@pFlyDate,@pFClass,@pSupplier,@pHolPack  \n");
            sb.Append("      Continue \n");
            sb.Append("    End  \n");
            sb.Append(" \n");
            sb.Append("    Set @pDepFree=@pDepFree-@pPreSeatTmp  \n");
            sb.Append("  \n");
            sb.Append("    If @pDepFree>0  \n");
            sb.Append("      If @pDepFree>=@pNeedSeatTmp  \n");
            sb.Append("      Begin  \n");
            sb.Append("        Update #PriceTbl \n");
            sb.Append("        Set DFClass1=@pFClass, \n");
            sb.Append("            DFUnit1=@pDepFree,--@pNeedSeatTmp,  \n");
            sb.Append("            DSupplier=@pSupplier \n");
            sb.Append("        Where DFlightNo=@pFlightNo \n");
            sb.Append("          and FlyDate=@pFlyDate  \n");
            sb.Append("          and isnull(DFClass1,'')='' \n");
            sb.Append(" \n");
            sb.Append("        Set @pRowCount=@@ROWCOUNT  \n");
            sb.Append(" \n");
            sb.Append("        If @pRowCount=0  \n");
            sb.Append("          Update #PriceTbl \n");
            sb.Append("          Set DFClass2=@pFClass, \n");
            sb.Append("              DFUnit2=@pDepFree,--@pNeedSeatTmp,  \n");
            sb.Append("              DSupplier=@pSupplier \n");
            sb.Append("          Where DFlightNo=@pFlightNo \n");
            sb.Append("            and FlyDate=@pFlyDate  \n");
            sb.Append("            and isnull(DFClass2,'')='' \n");
            sb.Append(" \n");
            sb.Append("        If @pRowCount=0 and @@ROWCOUNT=0 \n");
            sb.Append("          Update #PriceTbl \n");
            sb.Append("          Set DFClass3=@pFClass, \n");
            sb.Append("			  DFUnit3=@pDepFree,--@pNeedSeatTmp,   \n");
            sb.Append("			  DSupplier=@pSupplier  \n");
            sb.Append("		  Where DFlightNo=@pFlightNo  \n");
            sb.Append("		    and FlyDate=@pFlyDate \n");
            sb.Append("		    and isnull(DFClass3,'')=''  \n");
            sb.Append("		  \n");
            sb.Append("        Select @pIsOk=1, @pNeedSeatTmp=@pNeedSeat,@pPreSeatTmp=@pPreSeat  \n");
            sb.Append("      End  \n");
            sb.Append("      Else \n");
            sb.Append("      Begin  \n");
            sb.Append("        Update #PriceTbl \n");
            sb.Append("        Set DFClass1=@pFClass,  \n");
            sb.Append("            DFUnit1=@pNeedSeatTmp-@pDepFree, \n");
            sb.Append("            DSupplier=@pSupplier \n");
            sb.Append("        Where DFlightNo=@pFlightNo \n");
            sb.Append("          and FlyDate=@pFlyDate  \n");
            sb.Append("          and isnull(DFClass1,'')='' \n");
            sb.Append(" \n");
            sb.Append("        Set @pRowCount=@@ROWCOUNT  \n");
            sb.Append(" \n");
            sb.Append("        If @pRowCount=0  \n");
            sb.Append("          Update #PriceTbl \n");
            sb.Append("          Set DFClass2=@pFClass, \n");
            sb.Append("              DFUnit2=@pNeedSeatTmp-@pDepFree, \n");
            sb.Append("              DSupplier=@pSupplier \n");
            sb.Append("          Where DFlightNo=@pFlightNo \n");
            sb.Append("            and FlyDate=@pFlyDate  \n");
            sb.Append("            and isnull(DFClass2,'')='' \n");
            sb.Append(" \n");
            sb.Append("        If @pRowCount=0 and @@ROWCOUNT=0 \n");
            sb.Append("          Update #PriceTbl \n");
            sb.Append("          Set DFClass3=@pFClass, \n");
            sb.Append("              DFUnit3=@pNeedSeatTmp-@pDepFree, \n");
            sb.Append("              DSupplier=@pSupplier \n");
            sb.Append("          Where DFlightNo=@pFlightNo \n");
            sb.Append("            and FlyDate=@pFlyDate  \n");
            sb.Append("            and isnull(DFClass3,'')='' \n");
            sb.Append(" \n");
            sb.Append("        Select @pNeedSeatTmp=@pNeedSeatTmp-@pDepFree \n");
            sb.Append(" \n");
            sb.Append("      End  \n");
            sb.Append(" \n");
            sb.Append("      If @pCurFlightNo<>@pFlightNo Or @pCurFlyDate<>@pFlyDate  \n");
            sb.Append("        Select @pCurFlightNo=@pFlightNo,@pCurFlyDate=@pFlyDate  \n");
            sb.Append("  \n");
            sb.Append("    Fetch Next FROM cursorDepFlightClass Into @pFlightNo,@pFlyDate,@pFClass,@pSupplier,@pHolPack  \n");
            sb.Append("End  \n");
            sb.Append("Close cursorDepFlightClass  \n");
            sb.Append("DeAllocate cursorDepFlightClass  \n");
            sb.Append("-- Departure flight select class  \n");
            sb.Append("  \n");
            sb.Append("-- Return flight select class  \n");
            sb.Append("Set @pIsOk=0  \n");
            sb.Append("Set @pCurFlightNo=''  \n");
            sb.Append("Set @pCurFlyDate=null  \n");
            sb.Append("Set @pNeedSeatTmp=@pNeedSeat  \n");
            sb.Append("Set @pPreSeatTmp=@pPreSeat  \n");
            sb.Append("  \n");
            sb.Append("Declare @pDFlightNo varChar(10),@pDFlyDate DateTime,@pStat smallint,  \n");
            sb.Append("		@pIsNext bit,@pSameDest bit  \n");
            sb.Append("  \n");
            sb.Append("Declare depFlightCursor Cursor Local Forward_Only Static For  \n");
            sb.Append("Select Distinct DFlightNo,FlyDate From #PriceTbl  \n");
            sb.Append("Open depFlightCursor  \n");
            sb.Append("Fetch Next From depFlightCursor Into @pDFlightNo,@pDFlyDate \n");
            sb.Append("While @@FETCH_STATUS=0  \n");
            sb.Append("Begin \n");
            sb.Append("  \n");
            sb.Append("    Select @pNeedSeatTmp=@pNeedSeat,@pPreSeatTmp=@pPreSeat  \n");
            sb.Append("  \n");
            sb.Append("    Declare cursorRetFlightClass Cursor Local Forward_Only Static For \n");
            sb.Append("  \n");
            sb.Append("    Select F.FlightNo,F.FlyDate,F.SClass,B.Supplier,  \n");
            sb.Append("      IsNext=Case When F.FlightNo=NF.NextFlight Then 1 Else 0 End,  \n");
            sb.Append("      SameDest=Case When FD.DepCity=SD.ArrCity and FD.ArrCity=SD.DepCity Then 1 Else 0 End \n");
            sb.Append("    From FlightPrice F \n");
            sb.Append("    Join FlightDay Fd On FD.FlightNo=F.FlightNo and FD.FlyDate=F.FlyDate \n");
            sb.Append("    Join #FlightTbl T On T.FlightNo=F.FlightNo and T.StepType=2  \n");
            sb.Append("    Left Join FlightClassPriority P On P.Class=F.SClass  \n");
            sb.Append("    Outer Apply  \n");
            sb.Append("    (  \n");
            sb.Append("      Select Top 1 SalePriority=isnull(SalePriority,0),Supplier  \n");
            sb.Append("      From FlightBlock \n");
            sb.Append("      Where FlightNo=F.FlightNo  \n");
            sb.Append("        and RecID=F.BlockID  \n");
            sb.Append("      Order By SalePriority  \n");
            sb.Append("    ) B  \n");
            sb.Append("    Outer Apply  \n");
            sb.Append("    (  \n");
            sb.Append("      Select Top 1 FD2.NextFlight  \n");
            sb.Append("      From #FlightTbl D  \n");
            sb.Append("      Join FlightDay FD2 On FD2.FlightNo=D.FlightNo  \n");
            sb.Append("      Where D.StepType=1 \n");
            sb.Append("        and D.FlightNo=@pDFlightNo \n");
            sb.Append("        and FD2.FlyDate=F.FlyDate-@pNight  \n");
            sb.Append("        and FD2.NextFlight=F.FlightNo  \n");
            sb.Append("    ) NF \n");
            sb.Append("    Outer Apply  \n");
            sb.Append("    (  \n");
            sb.Append("      Select Top 1 FD2.DepCity,FD2.ArrCity \n");
            sb.Append("      From #FlightTbl D  \n");
            sb.Append("      Join FlightDay FD2 (Nolock) On FD2.FlightNo=D.FlightNo \n");
            sb.Append("      Where D.StepType=1 \n");
            sb.Append("        and FD2.FlyDate=F.FlyDate-@pNight  \n");
            sb.Append("        and FD2.DepCity=FD.ArrCity \n");
            sb.Append("        and FD2.ArrCity=FD.ArrCity \n");
            sb.Append("    ) SD \n");
            sb.Append("    Where F.FlyDate=@pDFlyDate+@Night1 \n");
            sb.Append("    Order By IsNext desc,SameDest desc,B.SalePriority,P.Priority,F.FlightNo,F.FlyDate,F.SClass  \n");
            sb.Append("  \n");
            sb.Append("    Open cursorRetFlightClass  \n");
            sb.Append("    Fetch Next From cursorRetFlightClass Into @pFlightNo,@pFlyDate,@pFClass,@pSupplier,@pIsNext,@pSameDest  \n");
            sb.Append("    While @@FETCH_STATUS = 0  \n");
            sb.Append("    Begin  \n");
            sb.Append("      If @pCurFlightNo=@pFlightNo and @pCurFlyDate=@pFlyDate and @pIsOk=1  \n");
            sb.Append("      Begin  \n");
            sb.Append("        Select @pNeedSeatTmp=@pNeedSeat,@pPreSeatTmp=@pPreSeat  \n");
            sb.Append("        Fetch Next From cursorRetFlightClass Into @pFlightNo,@pFlyDate,@pFClass,@pSupplier,@pIsNext,@pSameDest  \n");
            sb.Append("        continue  \n");
            sb.Append("      End  \n");
            sb.Append(" \n");
            sb.Append("      Set @pIsOk = 0 \n");
            sb.Append("      Select Top 1 @pSupplier=Supplier From FlightPrice  \n");
            sb.Append("      Where BS='B' \n");
            sb.Append("        and FlightNo=@pFlightNo \n");
            sb.Append("        and FlyDate=@pFlyDate \n");
            sb.Append("        and Operator=@pOwnOperator  \n");
            sb.Append("  \n");
            sb.Append("      Exec dbo.sp_CheckFlightAlllot @pFlightNo,@pFlyDate,@pFClass,@pOwnOperator,@pSupplier,'','H',1,@SaleDate, @pStat OutPut, @pRetFree OutPut \n");
            sb.Append(" \n");
            sb.Append("      If @pRetFree<@pPreSeatTmp  \n");
            sb.Append("      Begin  \n");
            sb.Append("        Set @pPreSeatTmp=@pPreSeatTmp-@pRetFree  \n");
            sb.Append("        Fetch Next From cursorRetFlightClass Into @pFlightNo,@pFlyDate,@pFClass,@pSupplier,@pIsNext,@pSameDest \n");
            sb.Append("        Continue \n");
            sb.Append("      End  \n");
            sb.Append(" \n");
            sb.Append("      Set @pRetFree=@pRetFree-@pPreSeatTmp \n");
            sb.Append(" \n");
            sb.Append("      If @pRetFree>0 \n");
            sb.Append("        If @pRetFree>=@pNeedSeatTmp  \n");
            sb.Append("        Begin  \n");
            sb.Append("          Update #PriceTbl \n");
            sb.Append("          Set RFlightNo=@pFlightNo,  \n");
            sb.Append("              RFClass1=@pFClass, \n");
            sb.Append("              RFUnit1=@pRetFree,--@pNeedSeatTmp,  \n");
            sb.Append("              RSupplier=@pSupplier \n");
            sb.Append("          Where DFlightNo=@pDFlightNo  \n");
            sb.Append("            and FlyDate=@pDFlyDate --And isnull(RFCl-@LNighass1,'') = '' \n");
            sb.Append(" \n");
            sb.Append("          Set @pRowCount=@@ROWCOUNT  \n");
            sb.Append("          If @pRowCount=0  \n");
            sb.Append("            Update #PriceTbl \n");
            sb.Append("            Set RFClass2=@pFClass, \n");
            sb.Append("                RFUnit2=@pRetFree,--@pNeedSeatTmp,  \n");
            sb.Append("                RSupplier=@pSupplier \n");
            sb.Append("            Where DFlightNo=@pDFlightNo  \n");
            sb.Append("              and FlyDate=@pDFlyDate --And isnull(RFClass2,'') = ''  \n");
            sb.Append(" \n");
            sb.Append("          If @pRowCount=0 and @@ROWCOUNT=0 \n");
            sb.Append("            Update #PriceTbl \n");
            sb.Append("            Set RFClass3=@pFClass, \n");
            sb.Append("                RFUnit3=@pRetFree,--@pNeedSeatTmp,  \n");
            sb.Append("                RSupplier=@pSupplier \n");
            sb.Append("            Where DFlightNo=@pDFlightNo  \n");
            sb.Append("              and FlyDate=@pDFlyDate --And isnull(RFClass3,'') = ''  \n");
            sb.Append(" \n");
            sb.Append("          Select @pIsOk=1,@pNeedSeatTmp=@pNeedSeat,@pPreSeatTmp=@pPreSeat  \n");
            sb.Append("          break  \n");
            sb.Append("        End  \n");
            sb.Append("        Else \n");
            sb.Append("        Begin  \n");
            sb.Append("          Update #PriceTbl \n");
            sb.Append("          Set RFlightNo=@pFlightNo,  \n");
            sb.Append("              RFClass1=@pFClass, \n");
            sb.Append("              RFUnit1=@pNeedSeatTmp-@pRetFree, \n");
            sb.Append("              RSupplier=@pSupplier \n");
            sb.Append("          Where DFlightNo=@pDFlightNo  \n");
            sb.Append("            and FlyDate=@pDFlyDate --And isnull(RFClass1,'') = ''  \n");
            sb.Append(" \n");
            sb.Append("          Set @pRowCount=@@ROWCOUNT  \n");
            sb.Append("          If @pRowCount=0  \n");
            sb.Append("            Update #PriceTbl \n");
            sb.Append("            Set RFClass2=@pFClass, \n");
            sb.Append("                RFUnit2=@pNeedSeatTmp-@pRetFree, \n");
            sb.Append("                RSupplier=@pSupplier \n");
            sb.Append("            Where DFlightNo=@pDFlightNo  \n");
            sb.Append("              and FlyDate=@pDFlyDate --And isnull(RFClass2,'') = ''  \n");
            sb.Append(" \n");
            sb.Append("          If @pRowCount=0 and @@ROWCOUNT=0 \n");
            sb.Append("            Update #PriceTbl \n");
            sb.Append("            Set RFClass3=@pFClass, \n");
            sb.Append("                RFUnit3=@pNeedSeatTmp-@pRetFree, \n");
            sb.Append("                RSupplier=@pSupplier \n");
            sb.Append("            Where DFlightNo=@pDFlightNo  \n");
            sb.Append("              and FlyDate=@pDFlyDate --And isnull(RFClass3,'') = ''  \n");
            sb.Append(" \n");
            sb.Append("          Set @pNeedSeatTmp=@pNeedSeatTmp-@pRetFree  \n");
            sb.Append("        End  \n");
            sb.Append(" \n");
            sb.Append("      If @pCurFlightNo<>@pFlightNo Or @pCurFlyDate<>@pFlyDate  \n");
            sb.Append("      Select @pCurFlightNo=@pFlightNo,@pCurFlyDate=@pFlyDate \n");
            sb.Append(" \n");
            sb.Append("      Fetch Next From cursorRetFlightClass Into @pFlightNo,@pFlyDate,@pFClass,@pSupplier,@pIsNext,@pSameDest \n");
            sb.Append("    End  \n");
            sb.Append("    Close cursorRetFlightClass \n");
            sb.Append("    DEALLOCATE cursorRetFlightClass  \n");
            sb.Append(" \n");
            sb.Append("    Fetch Next FROM depFlightCursor Into @pDFlightNo,@pDFlyDate  \n");
            sb.Append("  End  \n");
            sb.Append("  Close depFlightCursor  \n");
            sb.Append("  DEALLOCATE depFlightCursor \n");
            sb.Append(" \n");
            sb.Append("-- Return flight select class  \n");
            sb.Append(" \n");
            sb.Append("Delete From #PriceTbl  \n");
            sb.Append("Where RFlightNo Is Null Or Isnull(DFClass1,'')='' Or Isnull(RFClass1, '')='' \n");
            sb.Append(" \n");
            sb.Append("-- (Pricelist price - Flight price)  \n");
            sb.Append(" \n");
            sb.Append("Update #PriceTbl \n");
            sb.Append("Set Price=X.CalcSalePrice-TotPLFlightPrice,  \n");
            sb.Append("    AdlPrice=X.SaleAdl-TotAdlPLFlightPrice,  \n");
            sb.Append("    Currency=SaleCur,  \n");
            sb.Append("    AdlSeat=X.HAdult,  \n");
            sb.Append("    ChdSeat=X.HChdAgeG2+X.HChdAgeG3+X.HChdAgeG4, \n");
            sb.Append("	CatPackId=X.CatPackId,  \n");
            sb.Append("	ArecNo=X.ArecNo,  \n");
            sb.Append("	PRecNo=X.PRecNo,  \n");
            sb.Append("	HapRecId=X.HapRecID,  \n");
            sb.Append("	Night=X.Night,  \n");
            sb.Append("	SPOStr=Case When PEB_Amount<>0 Then 'E' Else '' End + \n");
            sb.Append("           Case when SpoHotelTot<>0 Then 'H' Else '' End + \n");
            sb.Append("           Case when SpoFlightTot<>0 Then 'F' Else '' End +  \n");
            sb.Append("           Case when HKB_Amount<>0 Then 'K' Else '' End  \n");
            sb.Append("From #PriceTbl P \n");
            sb.Append("Join #PriceListEB X On X.CheckIn=P.FlyDate and X.HolPack=P.HolPack and X.Hotel=P.Hotel and X.Room=P.Room and X.Accom=P.Accom and X.Board=P.Board \n");
            sb.Append(" \n");
            sb.Append("-- (Pricelist price - Flight price)  \n");
            sb.Append(" \n");
            sb.Append("-- (Pricelist price + Calculate departure flight prices) \n");
            sb.Append(" \n");
            sb.Append("Declare @pDFClass1 varChar(5),@pDFClass2 varChar(5),@pDFClass3 varChar(5),@pDFUnit1 smallint,@pDFUnit2 smallint,@pDFUnit3 smallint,  \n");
            sb.Append("        @pRFlightNo varChar(10),@pRFClass1 varChar(5),@pRFClass2 varChar(5),@pRFClass3 varChar(5),@pRFUnit1 smallint,@pRFUnit2 smallint,@pRFUnit3 smallint,  \n");
            sb.Append("        @pRSupplier varChar(10),@pAdlSeat smallint,@pChdSeat smallint, \n");
            sb.Append("		@pGenPer decimal(18,2),@pGenVal decimal(18,2),  \n");
            sb.Append("		@pAdlPer decimal(18,2),@pAdlVal decimal(18,2),@pChdG1Per decimal(18,2), \n");
            sb.Append("		@pChdG1Val decimal(18,2),@pChdG2Per decimal(18,2),@pChdG2Val decimal(18,2),@pChdG3Per decimal(18,2),@pChdG3Val decimal(18,2), \n");
            sb.Append("		@pPackCom decimal(18,2),@pValCur varChar(5),@pIErrCode int, \n");
            sb.Append("		@pNetCur varChar(10),@pNetPrice decimal(18,2),@pNetChd decimal(18,2),@pNetInf decimal(18,2),  \n");
            sb.Append("		@pFSalePrice1 decimal(18,2),@pFSalePrice2 decimal(18,2),@pFSalePrice3 decimal(18,2),  \n");
            sb.Append("		@pFCSalePrice1 decimal(18,2),@pFCSalePrice2 decimal(18,2),@pFCSalePrice3 decimal(18,2), \n");
            sb.Append("		@pRate decimal(18,2), \n");
            sb.Append("		@pSaleCalcType Smallint,  \n");
            sb.Append("		@pProcAdlSeat Smallint,@pProcChdSeat Smallint,  \n");
            sb.Append("        @pA1 Smallint,@pA2 Smallint,@pA3 Smallint,@pC1 Smallint,@pC2 Smallint,@pC3 Smallint, \n");
            sb.Append("        @pFTotPrice Decimal(18,2)  \n");
            sb.Append("		  \n");
            sb.Append("		  \n");
            sb.Append(" \n");
            //18-Nisan-2014 profit hesaplama düzeltmesi.

            sb.Append("if Exists(Select RecID From ParamPrice (NOLOCK) Where Market=@Market And UseSysParam='Y') \n");
            sb.Append("  Select @pSaleCalcType=SaleCalcType From ParamPrice (NOLOCK) Where Market='' \n");
            sb.Append("Else \n");
            sb.Append("  Select @pSaleCalcType=SaleCalcType From ParamPrice (NOLOCK) Where Market=@Market \n");

            sb.Append(" \n");
            sb.Append("Declare depFlightPriceCursor Cursor Local Forward_Only Static For  \n");
            sb.Append("Select Distinct HolPack,FlyDate,DFlightNo,DFClass1,DFClass2,DFClass3,DFUnit1,DFUnit2,DFUnit3,DSupplier,RFlightNo,RFClass1,RFClass2,RFClass3,RFUnit1,RFUnit2,RFUnit3,RSupplier,AdlSeat,ChdSeat \n");
            sb.Append("From #PriceTbl  \n");
            sb.Append("Open depFlightPriceCursor  \n");
            sb.Append("Fetch Next From depFlightPriceCursor Into @pHolPack,@pFlyDate,@pFlightNo,@pDFClass1,@pDFClass2,@pDFClass3,@pDFUnit1,@pDFUnit2,@pDFUnit3,@pSupplier,@pRFlightNo,@pRFClass1,@pRFClass2,@pRFClass3,@pRFUnit1,@pRFUnit2,@pRFUnit3,@pRSupplier,@pAdlSeat,@pChdSeat  \n");
            sb.Append("While @@FETCH_STATUS=0  \n");
            sb.Append("Begin  \n");
            sb.Append("  \n");
            sb.Append("  Select @pOwnMarket=Market From HolPack Where Code=@pHolPack  \n");
            sb.Append("  Select @pOwnOperator=Code From Operator Where Market=@pOwnMarket  \n");
            sb.Append("  \n");
            sb.Append("  -- Departure Flight  \n");
            sb.Append("  \n");
            //sb.Append("  Select @pFSalePrice1=0,  \n");
            //sb.Append("		 @pFSalePrice2=0, \n");
            //sb.Append("		 @pFSalePrice3=0, \n");
            //sb.Append("		 @pFCSalePrice1=0,  \n");
            //sb.Append("		 @pFCSalePrice2=0,  \n");
            //sb.Append("		 @pFCSalePrice3=0 \n");
            //sb.Append(" \n");
            //sb.Append("  Select @pFTotPrice=0,  \n");
            //sb.Append("         @pProcAdlSeat=@pAdlSeat,  \n");
            //sb.Append("         @pProcChdSeat=@pChdSeat,  \n");
            //sb.Append("         @pA1=0, \n");
            //sb.Append("         @pA2=0, \n");
            //sb.Append("         @pA3=0, \n");
            //sb.Append("         @pC1=0, \n");
            //sb.Append("         @pC2=0, \n");
            //sb.Append("         @pC3=0  \n");
            //sb.Append(" \n");
            //sb.Append("  Select top 1 \n");
            //sb.Append("     @pNetCur=P.NetCur,  \n");
            //sb.Append("	 @pNetPrice=P.NetPrice, \n");
            //sb.Append("	 @pNetChd=P.NetChd, \n");
            //sb.Append("	 @pNetInf=P.NetInf  \n");
            //sb.Append("  From FlightPrice P (NOLOCK)  \n");
            //sb.Append("  Join FlightBlock B On B.RecID=P.BlockID and B.BS='B'  \n");
            //sb.Append("  Where P.FlyDate=@pFlyDate \n");
            //sb.Append("    and P.FlightNo=@pFlightNo \n");
            //sb.Append("    and P.SClass=@pDFClass1 \n");
            //sb.Append("    and P.Supplier=@pSupplier \n");
            //sb.Append("    and P.Operator=@pOwnOperator  \n");
            //sb.Append("    and P.BlockType='H' \n");
            //sb.Append("    and P.BS='B'  \n");
            //sb.Append("  Order by B.SalePriority, P.SalePrice, P.NetPrice  \n");
            //sb.Append("  \n");
            sb.Append("  Select @pGenPer=GenPer,  \n");
            sb.Append("		 @pGenVal=GenVal, \n");
            sb.Append("		 @pAdlPer=AdlPer, \n");
            sb.Append("		 @pAdlVal=AdlVal, \n");
            sb.Append("		 @pChdG1Per=ChdG1Per, \n");
            sb.Append("		 @pChdG1Val=ChdG1Val, \n");
            sb.Append("		 @pChdG2Per=ChdG2Per, \n");
            sb.Append("		 @pChdG2Val=ChdG2Val, \n");
            sb.Append("		 @pChdG3Per=ChdG3Per, \n");
            sb.Append("		 @pChdG3Val=ChdG3Val, \n");
            sb.Append("		 @pValCur=ValCur, \n");
            sb.Append("		 @pPackCom=PackCom  \n");
            sb.Append("  From dbo.ufn_FindProfit ('FLIGHT',@pFlightNo,@pFlyDate,@pHolPack,@pOwnMarket,null,@Night1,null,null,null,null,null)  \n");
            sb.Append("  \n");
            sb.Append("  if @@ROWCOUNT>0  \n");
            sb.Append("    if (IsNull(@pValCur,'')<>'') and \n");
            sb.Append("	   ( (IsNull(@pGenVal,0)<>0) or \n");
            sb.Append("	     (IsNull(@pAdlVal,0)<>0) or \n");
            sb.Append("	     (IsNull(@pChdG1Val,0)<>0) or \n");
            sb.Append("	     (IsNull(@pChdG2Val,0)<>0) or \n");
            sb.Append("	     (IsNull(@pChdG3Val,0)<>0) )  \n");
            sb.Append("    Begin  \n");
            sb.Append("	  Exec usp_Exchange @SaleDate,@pValCur,@pNetCur,1,1,@pRate OutPut,@pIErrCode OutPut,-1,@pOwnMarket  \n");
            sb.Append("	  if @pIErrCode>0 \n");
            sb.Append("	  Begin \n");
            sb.Append("	    Delete From #PriceTbl \n");
            sb.Append("	    Where HolPack=@pHolPack \n");
            sb.Append("          and FlyDate=@pFlyDate --And Hotel = @LcHotel  \n");
            sb.Append("	    Fetch Next From depFlightPriceCursor Into @pHolPack,@pFlyDate,@pFlightNo,@pDFClass1,@pDFClass2,@pDFClass3,@pDFUnit1,@pDFUnit2,@pDFUnit3,@pSupplier,@pRFlightNo,@pRFClass1,@pRFClass2,@pRFClass3,@pRFUnit1,@pRFUnit2,@pRFUnit3,@pRSupplier,@pAdlSeat,@pChdSeat  \n");
            sb.Append("	    Continue \n");
            sb.Append("	  End--Exchange Problem  \n");
            sb.Append("	  \n");
            sb.Append("	  Select @pGenVal=@pGenVal*@pRate, \n");
            sb.Append("			 @pAdlVal=@pAdlVal*@pRate, \n");
            sb.Append("			 @pChdG1Val=@pChdG1Val*@pRate, \n");
            sb.Append("			 @pChdG2Val=@pChdG2Val*@pRate, \n");
            sb.Append("			 @pChdG3Val=@pChdG3Val*@pRate  \n");
            sb.Append("	 \n");
            sb.Append("    End \n");
            sb.Append("  Select @pFSalePrice1=0,  \n");
            sb.Append("		 @pFSalePrice2=0, \n");
            sb.Append("		 @pFSalePrice3=0, \n");
            sb.Append("		 @pFCSalePrice1=0,  \n");
            sb.Append("		 @pFCSalePrice2=0,  \n");
            sb.Append("		 @pFCSalePrice3=0 \n");
            sb.Append("  \n");
            sb.Append("  Select top 1 \n");
            sb.Append("     @pNetCur=P.NetCur,  \n");
            sb.Append("	 @pNetPrice=P.NetPrice, \n");
            sb.Append("	 @pNetChd=P.NetChd, \n");
            sb.Append("	 @pNetInf=P.NetInf  \n");
            sb.Append("  From FlightPrice P (NOLOCK)  \n");
            sb.Append("  Join FlightBlock B On B.RecID=P.BlockID and B.BS='B'  \n");
            sb.Append("  Where P.FlyDate=@pFlyDate \n");
            sb.Append("    and P.FlightNo=@pFlightNo \n");
            sb.Append("    and P.SClass=@pDFClass1 \n");
            sb.Append("    and P.Supplier=@pSupplier \n");
            sb.Append("    and P.Operator=@pOwnOperator  \n");
            sb.Append("    and P.BlockType='H' \n");
            sb.Append("    and P.BS='B'  \n");
            sb.Append("  Order by B.SalePriority, P.SalePrice, P.NetPrice  \n");
            sb.Append("  \n");
            sb.Append("  Set @pFSalePrice1=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,1,@pSaleCalcType) \n");
            sb.Append("  Set @pFCSalePrice1=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,3,@pSaleCalcType)  \n");
            sb.Append("  \n");
            sb.Append("  If @pDFClass2<>''  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select Top 1 \n");
            sb.Append("      @pNetCur=P.NetCur,  \n");
            sb.Append("      @pNetPrice=P.NetPrice,  \n");
            sb.Append("      @pNetChd=P.NetChd,  \n");
            sb.Append("      @pNetInf=P.NetInf \n");
            sb.Append("    From FlightPrice P  \n");
            sb.Append("    Join FlightBlock B On B.RecID=P.BlockID and B.BS='B'  \n");
            sb.Append("    Where P.FlyDate=@pFlyDate  \n");
            sb.Append("      and P.FlightNo=@pFlightNo  \n");
            sb.Append("      and P.SClass=@pDFClass2  \n");
            sb.Append("      and P.Supplier=@pSupplier  \n");
            sb.Append("      and P.Operator=@pOwnOperator \n");
            sb.Append("      and P.BlockType='H'  \n");
            sb.Append("      and P.BS='B' \n");
            sb.Append("    Order by B.SalePriority,P.SalePrice,P.NetPrice  \n");
            sb.Append("  \n");
            sb.Append("    Set @pFSalePrice2=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,1,@pSaleCalcType)  \n");
            sb.Append("    Set @pFCSalePrice2=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,3,@pSaleCalcType)  \n");
            sb.Append("  End  \n");
            sb.Append("  \n");
            sb.Append("  If @pDFClass3<>''  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select Top 1 \n");
            sb.Append("      @pNetCur=P.NetCur,  \n");
            sb.Append("      @pNetPrice=P.NetPrice,  \n");
            sb.Append("      @pNetChd=P.NetChd,  \n");
            sb.Append("      @pNetInf=P.NetInf \n");
            sb.Append("    From FlightPrice P  \n");
            sb.Append("    Join FlightBlock B On B.RecID=P.BlockID and B.BS='B'  \n");
            sb.Append("    Where P.FlyDate=@pFlyDate  \n");
            sb.Append("      and P.FlightNo=@pFlightNo  \n");
            sb.Append("      and P.SClass=@pDFClass3  \n");
            sb.Append("      and P.Supplier=@pSupplier  \n");
            sb.Append("      and P.Operator=@pOwnOperator \n");
            sb.Append("      and P.BlockType='H'  \n");
            sb.Append("      and P.BS='B' \n");
            sb.Append("    Order by B.SalePriority,P.SalePrice,P.NetPrice \n");
            sb.Append(" \n");
            sb.Append("    Set @pFSalePrice3=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,1,@pSaleCalcType)  \n");
            sb.Append("    Set @pFCSalePrice3=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,3,@pSaleCalcType)  \n");
            sb.Append("  End  \n");
            sb.Append(" \n");
            sb.Append("  Select @pFTotPrice=0,  \n");
            sb.Append("         @pProcAdlSeat=@pAdlSeat,  \n");
            sb.Append("         @pProcChdSeat=@pChdSeat,  \n");
            sb.Append("         @pA1=0, \n");
            sb.Append("         @pA2=0, \n");
            sb.Append("         @pA3=0, \n");
            sb.Append("         @pC1=0, \n");
            sb.Append("         @pC2=0, \n");
            sb.Append("         @pC3=0  \n");
            sb.Append(" \n");
            sb.Append("  If @pDFUnit1>=@pProcAdlSeat  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select @pA1=@pProcAdlSeat,@pProcAdlSeat=0  \n");
            sb.Append("    If @pDFUnit1-@pA1>=@pProcChdSeat  \n");
            sb.Append("       Select @pC1=@pProcChdSeat,@pProcChdSeat=0  \n");
            sb.Append("    Else  \n");
            sb.Append("       Select @pC1=@pProcChdSeat-(@pDFUnit1-@pA1),@pProcChdSeat=@pProcChdSeat-@pC1  \n");
            sb.Append("  End  \n");
            sb.Append("  Else  \n");
            sb.Append("    Select @pA1=isnull(@pDFUnit1,0),@pProcAdlSeat=@pProcAdlSeat-isnull(@pDFUnit1,0) \n");
            sb.Append("  \n");
            sb.Append("  If @pDFUnit2>=@pProcAdlSeat  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select @pA2=@pProcAdlSeat,@pProcAdlSeat=0  \n");
            sb.Append("    If @pDFUnit2-@pA2>=@pProcChdSeat \n");
            sb.Append("       Select @pC2=@pProcChdSeat,@pProcChdSeat=0 \n");
            sb.Append("    Else \n");
            sb.Append("       Select @pC2=@pProcChdSeat-(@pDFUnit2-@pA2),@pProcChdSeat=@pProcChdSeat-@pC2 \n");
            sb.Append("  End  \n");
            sb.Append("  Else \n");
            sb.Append("    Select @pA2=isnull(@pDFUnit2,0),@pProcAdlSeat=@pProcAdlSeat-isnull(@pDFUnit2,0)  \n");
            sb.Append(" \n");
            sb.Append("  If @pDFUnit3>=@pProcAdlSeat  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select @pA3=@pProcAdlSeat,@pProcAdlSeat=0  \n");
            sb.Append("    If @pDFUnit3-@pA3>=@pProcChdSeat \n");
            sb.Append("       Select @pC3=@pProcChdSeat,@pProcChdSeat=0 \n");
            sb.Append("    Else \n");
            sb.Append("       Select @pC3=@pProcChdSeat-(@pDFUnit3-@pA3),@pProcChdSeat=@pProcChdSeat-@pC3 \n");
            sb.Append("  End  \n");
            sb.Append("  Else \n");
            sb.Append("    Select @pA3=isnull(@pDFUnit3,0),@pProcAdlSeat=@pProcAdlSeat-isnull(@pDFUnit3,0)  \n");
            sb.Append(" \n");
            sb.Append("  Update #PriceTbl \n");
            sb.Append("  Set Price=Price+(@pFSalePrice1*@pA1)+(@pFCSalePrice1*@pC1)+  \n");
            sb.Append("                  (@pFSalePrice2*@pA2)+(@pFCSalePrice2*@pC2)+  \n");
            sb.Append("                  (@pFSalePrice3*@pA3)+(@pFCSalePrice3*@pC3),  \n");
            sb.Append("      AdlPrice=AdlPrice+(Case When @pA1=0  \n");
            sb.Append("							 Then 0 \n");
            sb.Append("                             Else (Case When @pA2=0  \n");
            sb.Append("									   Then @pFSalePrice1 \n");
            sb.Append("                                       Else (Case When @pA3=0  \n");
            sb.Append("                                                 Then (Case When @pFSalePrice2>=@pFSalePrice1 Then @pFSalePrice1 Else @pFSalePrice2 End) \n");
            sb.Append("                                                 Else (Case When @pFSalePrice3>=@pFSalePrice2 and @pFSalePrice2>=@pFSalePrice1 Then @pFSalePrice1  \n");
            sb.Append("                                                           When @pFSalePrice3>=@pFSalePrice2 and @pFSalePrice2<@pFSalePrice1 Then @pFSalePrice2  \n");
            sb.Append("                                                           when @pFSalePrice2>=@pFSalePrice3 and @pFSalePrice3>=@pFSalePrice1 Then @pFSalePrice1 \n");
            sb.Append("                                                           when @pFSalePrice2>=@pFSalePrice3 and @pFSalePrice3<@pFSalePrice1 Then @pFSalePrice3  \n");
            sb.Append("                                                           when @pFSalePrice1>=@pFSalePrice3 and @pFSalePrice3>=@pFSalePrice2 Then @pFSalePrice2 \n");
            sb.Append("                                                           when @pFSalePrice1>=@pFSalePrice3 and @pFSalePrice3<@pFSalePrice2 Then @pFSalePrice3  \n");
            sb.Append("                                                       End)  \n");
            sb.Append("                                            End) \n");
            sb.Append("                                  End) \n");
            sb.Append("						End)  \n");
            sb.Append("  Where HolPack=@pHolPack  \n");
            sb.Append("    and FlyDate=@pFlyDate  \n");
            sb.Append("    and DFlightNo=@pFlightNo --and Hotel = @LcHotel  \n");
            sb.Append(" \n");
            sb.Append("  Select @pGenPer=GenPer,  \n");
            sb.Append("         @pGenVal=GenVal,  \n");
            sb.Append("         @pAdlPer=AdlPer,  \n");
            sb.Append("         @pAdlVal=AdlVal,  \n");
            sb.Append("         @pChdG1Per=ChdG1Per,  \n");
            sb.Append("         @pChdG1Val=ChdG1Val,  \n");
            sb.Append("         @pChdG2Per=ChdG2Per,  \n");
            sb.Append("         @pChdG2Val=ChdG2Val,  \n");
            sb.Append("         @pChdG3Per=ChdG3Per,  \n");
            sb.Append("         @pChdG3Val=ChdG3Val,  \n");
            sb.Append("         @pValCur=ValCur,  \n");
            sb.Append("         @pPackCom=PackCom \n");
            sb.Append("    From dbo.ufn_FindProfit('FLIGHT',@pRFlightNo,@pFlyDate+@Night1,@pHolPack,@pOwnMarket,null,@Night1,null,null,null,null,null)  \n");
            sb.Append(" \n");
            sb.Append("    If @@ROWCOUNT>0  \n");
            sb.Append("      If (IsNull(@pValCur,'')<>'') and \n");
            sb.Append("         ( (IsNull(@pGenVal,0)<>0) or  \n");
            sb.Append("           (IsNull(@pAdlVal,0)<>0) or  \n");
            sb.Append("           (IsNull(@pChdG1Val,0)<>0) or  \n");
            sb.Append("           (IsNull(@pChdG2Val,0)<>0) or  \n");
            sb.Append("           (IsNull(@pChdG3Val,0)<>0) ) \n");
            sb.Append("      Begin  \n");
            sb.Append("        Exec usp_Exchange @SaleDate,@pValCur,@pNetCur,1,1,@pRate OutPut,@pIErrCode OutPut,-1,@pOwnMarket \n");
            sb.Append("        If @pIErrCode>0  \n");
            sb.Append("        Begin  \n");
            sb.Append("          Delete From #PriceTbl  \n");
            sb.Append("          Where HolPack=@pHolPack  \n");
            sb.Append("            and FlyDate=@pFlyDate --And Hotel = @LcHotel \n");
            sb.Append(" \n");
            sb.Append("          Fetch Next From depFlightPriceCursor INTO @pHolPack,@pFlyDate,@pFlightNo,@pDFClass1,@pDFClass2,@pDFClass3,@pDFUnit1,@pDFUnit2,@pDFUnit3,@pSupplier,@pRFlightNo,@pRFClass1,@pRFClass2,@pRFClass3,@pRFUnit1,@pRFUnit2,@pRFUnit3,@pRSupplier,@pAdlSeat,@pChdSeat  \n");
            sb.Append("          Continue \n");
            sb.Append("        end--Exchange Problem  \n");
            sb.Append("  \n");
            sb.Append("        Select @pGenVal=@pGenVal*@pRate,  \n");
            sb.Append("               @pAdlVal=@pAdlVal*@pRate,  \n");
            sb.Append("               @pChdG1Val=@pChdG1Val*@pRate,  \n");
            sb.Append("               @pChdG2Val=@pChdG2Val*@pRate,  \n");
            sb.Append("               @pChdG3Val=@pChdG3Val*@pRate \n");
            sb.Append("      End \n");
            sb.Append("  \n");
            sb.Append("  -- Departure Flight \n");
            sb.Append("  \n");
            sb.Append("  -- Return Flight  \n");
            //sb.Append("  Select @pFSalePrice1=0, \n");
            //sb.Append("		 @pFSalePrice2=0,  \n");
            //sb.Append("		 @pFSalePrice3=0,  \n");
            //sb.Append("		 @pFCSalePrice1=0, \n");
            //sb.Append("		 @pFCSalePrice2=0, \n");
            //sb.Append("		 @pFCSalePrice3=0  \n");
            //sb.Append("  \n");
            sb.Append("  \n");
            sb.Append("  Select Top 1  \n");
            sb.Append("    @pNetCur=P.NetCur,  \n");
            sb.Append("    @pNetPrice=P.NetPrice,  \n");
            sb.Append("    @pNetChd=P.NetChd,  \n");
            sb.Append("    @pNetInf=P.NetInf \n");
            sb.Append("  From FlightPrice P  \n");
            sb.Append("  Join FlightBlock B On B.RecID=P.BlockID and B.BS='B'  \n");
            sb.Append("  Where P.FlyDate=@pFlyDate++@Night1  \n");
            sb.Append("    and P.FlightNo=@pRFlightNo  \n");
            sb.Append("    and P.SClass=@pRFClass1  \n");
            sb.Append("    and P.Supplier=@pSupplier  \n");
            sb.Append("    and P.Operator=@pOwnOperator \n");
            sb.Append("    and P.BlockType='H'  \n");
            sb.Append("    and P.BS='B' \n");
            sb.Append("  Order by B.SalePriority,P.SalePrice,P.NetPrice  \n");
            sb.Append("  \n");
            sb.Append("  Set @pFSalePrice1=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,1,@pSaleCalcType)  \n");
            sb.Append("  Set @pFCSalePrice1=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,3,@pSaleCalcType) \n");
            sb.Append(" \n");
            sb.Append("  If @pDFClass2<>''  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select Top 1 \n");
            sb.Append("      @pNetCur=P.NetCur, \n");
            sb.Append("      @pNetPrice=P.NetPrice, \n");
            sb.Append("      @pNetChd=P.NetChd, \n");
            sb.Append("      @pNetInf=P.NetInf  \n");
            sb.Append("    From FlightPrice P \n");
            sb.Append("    Join FlightBlock B On B.RecID=P.BlockID and B.BS='B' \n");
            sb.Append("    Where P.FlyDate=@pFlyDate+@Night1  \n");
            sb.Append("      and P.FlightNo=@pRFlightNo  \n");
            sb.Append("      and P.SClass=@pRFClass2  \n");
            sb.Append("      and P.Supplier=@pSupplier  \n");
            sb.Append("      and P.Operator=@pOwnOperator \n");
            sb.Append("      and P.BlockType='H'  \n");
            sb.Append("      and P.BS='B' \n");
            sb.Append("    Order by B.SalePriority,P.SalePrice,P.NetPrice \n");
            sb.Append(" \n");
            sb.Append("    Set @pFSalePrice2=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,1,@pSaleCalcType)  \n");
            sb.Append("    Set @pFCSalePrice2=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,3,@pSaleCalcType) \n");
            sb.Append("  End  \n");
            sb.Append(" \n");
            sb.Append("  If @pDFClass3<>''  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select Top 1 \n");
            sb.Append("      @pNetCur=P.NetCur, \n");
            sb.Append("      @pNetPrice=P.NetPrice, \n");
            sb.Append("      @pNetChd=P.NetChd, \n");
            sb.Append("      @pNetInf=P.NetInf  \n");
            sb.Append("    From FlightPrice P \n");
            sb.Append("    Join FlightBlock B On B.RecID=P.BlockID and B.BS='B' \n");
            sb.Append("    Where P.FlyDate=@pFlyDate+@Night1  \n");
            sb.Append("      and P.FlightNo=@pRFlightNo  \n");
            sb.Append("      and P.SClass=@pRFClass3  \n");
            sb.Append("      and P.Supplier=@pSupplier  \n");
            sb.Append("      and P.Operator=@pOwnOperator \n");
            sb.Append("      and P.BlockType='H'  \n");
            sb.Append("      and P.BS='B' \n");
            sb.Append("    Order by B.SalePriority,P.SalePrice,P.NetPrice \n");
            sb.Append(" \n");
            sb.Append("    Set @pFSalePrice3=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,1,@pSaleCalcType)  \n");
            sb.Append("    Set @pFCSalePrice3=dbo.usf_Calc_Price(@pNetPrice,@pGenPer,@pGenVal,@pAdlPer,@pAdlVal,@pChdG1Per,@pChdG1Val,@pChdG2Per,@pChdG2Val,@pChdG3Per,@pChdG3Val,0,0,@pPackCom,3,@pSaleCalcType) \n");
            sb.Append("  End  \n");
            sb.Append(" \n");
            sb.Append("  Select @pFTotPrice=0,  \n");
            sb.Append("         @pProcAdlSeat=@pAdlSeat,  \n");
            sb.Append("         @pProcChdSeat=@pChdSeat,  \n");
            sb.Append("         @pA1=0, \n");
            sb.Append("         @pA2=0, \n");
            sb.Append("         @pA3=0, \n");
            sb.Append("         @pC1=0, \n");
            sb.Append("         @pC2=0, \n");
            sb.Append("         @pC3=0  \n");
            sb.Append(" \n");
            sb.Append("  If @pRFUnit1>=@pProcAdlSeat  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select @pA1=@pProcAdlSeat,@pProcAdlSeat=0  \n");
            sb.Append(" \n");
            sb.Append("    If @pRFUnit1-@pA1>=@pProcChdSeat \n");
            sb.Append("      Select @pC1=@pProcChdSeat,@pProcChdSeat=0  \n");
            sb.Append("    Else \n");
            sb.Append("      Select @pC1=@pProcChdSeat-(@pRFUnit1-@pA1),@pProcChdSeat=@pProcChdSeat-@pC1  \n");
            sb.Append("    End  \n");
            sb.Append("    Else \n");
            sb.Append("      Select @pA1=isnull(@pRFUnit1,0),@pProcAdlSeat=@pProcAdlSeat-isnull(@pRFUnit1,0)  \n");
            sb.Append(" \n");
            sb.Append("  If @pRFUnit2>=@pProcAdlSeat  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select @pA2=@pProcAdlSeat,@pProcAdlSeat=0  \n");
            sb.Append(" \n");
            sb.Append("    If @pRFUnit2-@pA2>=@pProcChdSeat \n");
            sb.Append("      Select @pC2=@pProcChdSeat,@pProcChdSeat=0  \n");
            sb.Append("    Else \n");
            sb.Append("      Select @pC2=@pProcChdSeat-(@pRFUnit2-@pA2),@pProcChdSeat=@pProcChdSeat-@pC2  \n");
            sb.Append("    End  \n");
            sb.Append("    Else \n");
            sb.Append("      Select @pA2=isnull(@pRFUnit2,0),@pProcAdlSeat=@pProcAdlSeat-isnull(@pRFUnit2,0)  \n");
            sb.Append(" \n");
            sb.Append("  If @pRFUnit3>=@pProcAdlSeat  \n");
            sb.Append("  Begin  \n");
            sb.Append("    Select @pA3=@pProcAdlSeat,@pProcAdlSeat=0  \n");
            sb.Append(" \n");
            sb.Append("    If @pRFUnit3-@pA3>=@pProcChdSeat \n");
            sb.Append("      Select @pC3=@pProcChdSeat,@pProcChdSeat=0  \n");
            sb.Append("    Else \n");
            sb.Append("      Select @pC3=@pProcChdSeat-(@pRFUnit3-@pA3),@pProcChdSeat=@pProcChdSeat-@pC3  \n");
            sb.Append("    End  \n");
            sb.Append("    Else \n");
            sb.Append("      Select @pA3=isnull(@pRFUnit3,0),@pProcAdlSeat=@pProcAdlSeat-isnull(@pRFUnit3,0)  \n");
            sb.Append(" \n");
            sb.Append(" \n");
            sb.Append("  Update #PriceTbl \n");
            sb.Append("  Set Price=Price+(@pFSalePrice1*@pA1)+(@pFCSalePrice1*@pC1)+  \n");
            sb.Append("                  (@pFSalePrice2*@pA2)+(@pFCSalePrice2*@pC2)+  \n");
            sb.Append("                  (@pFSalePrice3*@pA3)+(@pFCSalePrice3*@pC3),  \n");
            sb.Append("      AdlPrice=AdlPrice+(Case When @pA1=0  \n");
            sb.Append("							 Then 0 \n");
            sb.Append("                             Else (Case When @pA2=0  \n");
            sb.Append("									   Then @pFSalePrice1 \n");
            sb.Append("                                       Else (Case When @pA3=0  \n");
            sb.Append("                                                 Then (Case When @pFSalePrice2>=@pFSalePrice1 Then @pFSalePrice1 Else @pFSalePrice2 End) \n");
            sb.Append("                                                 Else (Case When @pFSalePrice3>=@pFSalePrice2 and @pFSalePrice2>=@pFSalePrice1 Then @pFSalePrice1  \n");
            sb.Append("                                                           When @pFSalePrice3>=@pFSalePrice2 and @pFSalePrice2<@pFSalePrice1 Then @pFSalePrice2  \n");
            sb.Append("                                                           when @pFSalePrice2>=@pFSalePrice3 and @pFSalePrice3>=@pFSalePrice1 Then @pFSalePrice1 \n");
            sb.Append("                                                           when @pFSalePrice2>=@pFSalePrice3 and @pFSalePrice3<@pFSalePrice1 Then @pFSalePrice3  \n");
            sb.Append("                                                           when @pFSalePrice1>=@pFSalePrice3 and @pFSalePrice3>=@pFSalePrice2 Then @pFSalePrice2 \n");
            sb.Append("                                                           when @pFSalePrice1>=@pFSalePrice3 and @pFSalePrice3<@pFSalePrice2 Then @pFSalePrice3  \n");
            sb.Append("                                                       End)  \n");
            sb.Append("                                            End) \n");
            sb.Append("                                  End) \n");
            sb.Append("						End)  \n");
            sb.Append("  Where HolPack=@pHolPack  \n");
            sb.Append("    and FlyDate=@pFlyDate  \n");
            sb.Append("    and DFlightNo=@pFlightNo --and Hotel = @LcHotel  \n");
            sb.Append("  -- Return Flight \n");
            sb.Append(" \n");
            sb.Append("  Fetch Next From depFlightPriceCursor INTO @pHolPack,@pFlyDate,@pFlightNo,@pDFClass1,@pDFClass2,@pDFClass3,@pDFUnit1,@pDFUnit2,@pDFUnit3,@pSupplier,@pRFlightNo,@pRFClass1,@pRFClass2,@pRFClass3,@pRFUnit1,@pRFUnit2,@pRFUnit3,@pRSupplier,@pAdlSeat,@pChdSeat  \n");
            sb.Append("End  \n");
            sb.Append("Close depFlightPriceCursor \n");
            sb.Append("DeAllocate depFlightPriceCursor  \n");
            sb.Append(" \n");
            sb.Append("-- (Pricelist price + Calculate departure flight prices) \n");
            sb.Append(" \n");
            sb.Append("Delete Pt  \n");
            sb.Append("From #PriceTbl Pt  \n");
            sb.Append("INNER JOIN FlightDay Fd ON Fd.FlyDate=Pt.FlyDate and Fd.FlightNo=Pt.DFlightNo  \n");
            sb.Append("Where Pt.FlyDate=dbo.DateOnly(GetDate()) \n");
            sb.Append(" and ((Fd.FlyDate+isnull(Fd.DepTime,0))-isnull(Fd.SaleRelease,0)-isnull(Fd.CountCloseTime,0))>=GetDate() \n");
            sb.Append(" \n");
            sb.Append("-- Dynamic Flight start  \n");
            sb.Append(" \n");
            sb.Append("Update #PriceListEB  \n");
            sb.Append("Set CalcSalePrice=dbo.ufn_Exchange(@SaleBegDate,SaleCur,isnull(@Cur,SaleCur),CalcSalePrice,1,Market),  \n");
            sb.Append("	OldSalePrice=dbo.ufn_Exchange(@SaleBegDate,SaleCur,isnull(@Cur,SaleCur),OldSalePrice,1,Market), \n");
            sb.Append("	SaleCur=isnull(@Cur,SaleCur) \n");
            sb.Append("	 \n");
            sb.Append("--Select * From #PriceTbl \n");


            sb.Append("SET NOCOUNT OFF \n");
            sb.Append(" \n");
            sb.Append("-- Select Query \n");
            sb.Append("Select PL.RoomNr, \n");
            sb.Append("  PL.CatPackID,PL.ARecNo,PL.PRecNo,PL.HAPRecId,HAdult,HChdAgeG1,HChdAgeG2,HChdAgeG3,HChdAgeG4,ChdG1Age1,ChdG1Age2,ChdG2Age1,ChdG2Age2,ChdG3Age1,ChdG3Age2,ChdG4Age1,ChdG4Age2,PL.Hotel,HotelName, \n");
            sb.Append("  HotelLocation,HotLocationName=L.Name,HotLocationNameL=L.NameL, \n");
            sb.Append("  HotCat,CheckIn,CheckOut,PL.Night,HotCheckIn,HotNight,PL.Room,RoomName,PL.Board,BoardName,PL.Accom,AccomName,AccomFullName, \n");
            sb.Append("  CalcSalePrice,SaleChdG1,SaleChdG2,SaleChdG3,SaleChdG4,HotChdPrice,OldSalePrice,NetCur,PL.SaleCur,AgencyEB,PL.DepCity,DepFlight,DepFlightTime,PL.ArrCity,RetFlight,RetFlightTime,EBValidDate, \n");
            sb.Append("  PL.Description,SaleSPONo,EBPerc,EBAmount,PasEBPer,SaleEndDate,PL.HolPack,Operator,PL.Market,FlightClass,plSpoExists,FlightValidDate,HotelValidDate, \n");
            sb.Append("  LastPrice=CalcSalePrice,PL.Direction,PL.Category, \n");
            sb.Append("  HotelNameL=isnull(dbo.FindLocalName(HNameLID,@Market),HotelName), \n");
            sb.Append("  HotelRecID=Htl.RecID, \n");
            sb.Append("  RoomNameL=isnull(dbo.FindLocalName(HRNameLID,@Market),RoomName), \n");
            sb.Append("  AccomNameL=isnull(dbo.FindLocalName(HANameLID,@Market),AccomName), \n");
            sb.Append("  BoardNameL=isnull(dbo.FindLocalName(HBNameLID,@Market),BoardName), \n");
            sb.Append("  HolPackName=H.Name,HolPackNameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name), \n");
            sb.Append("  DepCityName=L1.Name, DepCityNameL=L1.NameL, \n");
            sb.Append("  ArrCityName=L2.Name, ArrCityNameL=L2.NameL, \n");
            sb.Append("  Cur=@Cur,PL.PLCur, \n");
            sb.Append("  CurrentCur=isnull(@Cur,PL.SaleCur), \n");
            sb.Append("  InfoWeb=isnull(Hmo.InfoWeb, ''), Hmo.MaxChdAge,OprText = (Case When isnull(H.OprText, '') = '' then O.NameS else H.OprText end), \n");
            sb.Append("  PEB_SaleEndDate,StopSaleGuar,StopSaleStd,DFlight,DepServiceType,RetServiceType,AutoStop,TransportType, \n");
            sb.Append("  PLSPO_Amount=IsNull(SpoFlightTot,0)+IsNull(SpoHotelTot,0), \n");
            sb.Append("  P.DFlightNo,P.DFClass1,P.DFUnit1,P.DFClass2,P.DFUnit2,P.DFClass3,P.DFUnit3, \n");
            sb.Append("  P.RFlightNo,P.RFClass1,P.RFUnit1,P.RFClass2,P.RFUnit2,P.RFClass3,P.RFUnit3, \n");
            sb.Append("  P.OldPrice,P.Price \n");
            sb.Append("From #PriceListEB PL \n");
            sb.Append("Join #PriceTbl P ON PL.CatPackID=P.CatPackId and PL.ARecNo=P.ARecNo and PL.PRecNo=P.PRecNo And PL.HAPRecID=P.HapRecId and PL.RoomNr=P.RoomNr \n");
            sb.Append("Join #Location L ON L.RecID=PL.HotelLocation \n");
            sb.Append("Join #Location L1 ON L1.RecID=PL.DepCity \n");
            sb.Append("Join #Location L2 ON L2.RecID=PL.ArrCity \n");
            sb.Append("Join HolPack H (NOLOCK) ON H.Code=PL.HolPack \n");
            sb.Append("Join Hotel Htl (NOLOCK) ON Htl.Code=PL.Hotel \n");
            sb.Append("Join Operator O (NOLOCK) ON O.Code=PL.Operator \n");
            sb.Append("Left Join HotelMarOpt (NOLOCK) Hmo ON Hmo.Hotel=PL.Hotel And Hmo.Market=@Market \n");
            //sb.Append("Where Not (StopSaleStd=2 And StopSaleGuar=2) \n");

            int InfCount = 0;
            if (filter.RoomsInfo.FirstOrDefault().Chd1Age.HasValue && (filter.RoomsInfo.FirstOrDefault().Chd1Age.Value > 0 && filter.RoomsInfo.FirstOrDefault().Chd1Age.Value <= 1.99))
                InfCount++;
            else
                if (filter.RoomsInfo.FirstOrDefault().Chd2Age.HasValue && (filter.RoomsInfo.FirstOrDefault().Chd2Age.Value > 0 && filter.RoomsInfo.FirstOrDefault().Chd2Age.Value <= 1.99))
                InfCount++;
            else
                    if (filter.RoomsInfo.FirstOrDefault().Chd3Age.HasValue && (filter.RoomsInfo.FirstOrDefault().Chd3Age.Value > 0 && filter.RoomsInfo.FirstOrDefault().Chd3Age.Value <= 1.99))
                InfCount++;
            else
                        if (filter.RoomsInfo.FirstOrDefault().Chd4Age.HasValue && (filter.RoomsInfo.FirstOrDefault().Chd4Age.Value > 0 && filter.RoomsInfo.FirstOrDefault().Chd4Age.Value <= 1.99))
                InfCount++;

            int CurrentSeat = 0;
            CurrentSeat = (filter.RoomsInfo.FirstOrDefault().Adult + filter.RoomsInfo.FirstOrDefault().Child) - InfCount;
            string dateFormat = UserData.Ci.DateTimeFormat.ShortDatePattern.ToString().Replace("dd", "d").Replace("yy", "y").Replace("MM", "m").Replace(UserData.Ci.DateTimeFormat.DateSeparator, "");
            string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator.ToString();
            List<SearchResult> result = new List<SearchResult>();

            #region Age Group
            List<AgeGroup> ageGroups = new List<AgeGroup>();
            int roomCnt = 0;
            int refNo = 0;
            foreach (SearchCriteriaRooms row in filter.RoomsInfo)
            {
                roomCnt++;
                refNo++;
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Adult, GroupNo = roomCnt, RefNo = refNo, Unit = row.Adult, Age = null, DateOfBirth = null });
                if (row.Chd1Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Infant, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd1Age.Value, DateOfBirth = null });
                if (row.Chd2Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd2Age.Value, DateOfBirth = null });
                if (row.Chd3Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd3Age.Value, DateOfBirth = null });
                if (row.Chd4Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd4Age.Value, DateOfBirth = null });
            }

            #endregion

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sb.ToString());
            dbCommand.CommandTimeout = 120;
            try
            {
                int i = 0;

                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, UserData.Operator);
                db.AddInParameter(dbCommand, "Office", DbType.AnsiString, UserData.OprOffice);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "CIn1", DbType.Date, filter.CheckIn.Date);
                //if (filter.ExpandDate > 0) 
                db.AddInParameter(dbCommand, "CIn2", DbType.Date, filter.CheckIn.Date.AddDays(filter.ExpandDate));
                db.AddInParameter(dbCommand, "Night1", DbType.Int16, filter.NightFrom);
                if (filter.NightFrom < filter.NightTo)
                    db.AddInParameter(dbCommand, "Night2", DbType.Int16, filter.NightTo);
                db.AddInParameter(dbCommand, "Cur", DbType.AnsiString, !filter.CurControl || string.IsNullOrEmpty(filter.CurrentCur) ? null : filter.CurrentCur);
                if (filter.PriceFrom.HasValue)
                    db.AddInParameter(dbCommand, "Price1", DbType.Decimal, filter.PriceFrom);
                if (filter.PriceTo.HasValue)
                    db.AddInParameter(dbCommand, "Price2", DbType.Decimal, filter.PriceTo);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ++i;
                        SearchResult r = new SearchResult();
                        r.RefNo = i;
                        r.RoomNr = (int)R["RoomNr"];
                        r.Calculated = true;
                        r.CatPackID = Conversion.getInt32OrNull(R["CatPackID"]);
                        r.ARecNo = Conversion.getInt32OrNull(R["ARecNo"]);
                        r.PRecNo = Conversion.getInt32OrNull(R["PRecNo"]);
                        r.HAPRecId = Conversion.getInt32OrNull(R["HAPRecId"]);
                        r.HAdult = (Int16)R["HAdult"];
                        r.HChdAgeG1 = (Int16)R["HChdAgeG1"];
                        r.HChdAgeG2 = (Int16)R["HChdAgeG2"];
                        r.HChdAgeG3 = (Int16)R["HChdAgeG3"];
                        r.HChdAgeG4 = (Int16)R["HChdAgeG4"];
                        r.ChdG1Age1 = Conversion.getDecimalOrNull(R["ChdG1Age1"]);
                        r.ChdG1Age2 = Conversion.getDecimalOrNull(R["ChdG1Age2"]);
                        r.ChdG2Age1 = Conversion.getDecimalOrNull(R["ChdG2Age1"]);
                        r.ChdG2Age2 = Conversion.getDecimalOrNull(R["ChdG2Age2"]);
                        r.ChdG3Age1 = Conversion.getDecimalOrNull(R["ChdG3Age1"]);
                        r.ChdG3Age2 = Conversion.getDecimalOrNull(R["ChdG3Age2"]);
                        r.ChdG4Age1 = Conversion.getDecimalOrNull(R["ChdG4Age1"]);
                        r.ChdG4Age2 = Conversion.getDecimalOrNull(R["ChdG4Age2"]);
                        r.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        r.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        r.HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]);
                        r.HotelRecID = Conversion.getInt32OrNull(R["HotelRecID"]);
                        r.HotelLocation = Conversion.getInt32OrNull(R["HotelLocation"]);
                        r.HotLocationName = Conversion.getStrOrNull(R["HotLocationName"]);
                        r.HotLocationNameL = Conversion.getStrOrNull(R["HotLocationNameL"]);
                        r.HotCat = Conversion.getStrOrNull(R["HotCat"]);
                        r.CheckIn = Conversion.getDateTimeOrNull(R["CheckIn"]);
                        r.CheckOut = Conversion.getDateTimeOrNull(R["CheckOut"]);
                        r.Night = Conversion.getInt16OrNull(R["Night"]);
                        r.HotCheckIn = Conversion.getDateTimeOrNull(R["HotCheckIn"]);
                        r.HotNight = Conversion.getInt16OrNull(R["HotNight"]);
                        r.Room = Conversion.getStrOrNull(R["Room"]);
                        r.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        r.RoomNameL = Conversion.getStrOrNull(R["RoomNameL"]);
                        r.Board = Conversion.getStrOrNull(R["Board"]);
                        r.BoardName = Conversion.getStrOrNull(R["BoardName"]);
                        r.BoardNameL = Conversion.getStrOrNull(R["BoardNameL"]);
                        r.Accom = Conversion.getStrOrNull(R["Accom"]);
                        r.AccomName = Conversion.getStrOrNull(R["AccomName"]);
                        r.AccomNameL = Conversion.getStrOrNull(R["AccomNameL"]);
                        r.AccomFullName = Conversion.getStrOrNull(R["AccomFullName"]);
                        r.CalcSalePrice = Conversion.getDecimalOrNull(R["CalcSalePrice"]);
                        r.SaleChdG1 = Conversion.getDecimalOrNull(R["SaleChdG1"]);
                        r.SaleChdG2 = Conversion.getDecimalOrNull(R["SaleChdG2"]);
                        r.SaleChdG3 = Conversion.getDecimalOrNull(R["SaleChdG3"]);
                        r.SaleChdG4 = Conversion.getDecimalOrNull(R["SaleChdG4"]);
                        r.HotChdPrice = Conversion.getDecimalOrNull(R["HotChdPrice"]);
                        r.OldSalePrice = Conversion.getDecimalOrNull(R["OldSalePrice"]);
                        r.NetCur = Conversion.getStrOrNull(R["NetCur"]);
                        r.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        r.PLCur = Conversion.getStrOrNull(R["PLCur"]);
                        r.AgencyEB = Conversion.getDecimalOrNull(R["AgencyEB"]);
                        r.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        r.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        r.DepCityNameL = Conversion.getStrOrNull(R["DepCityNameL"]);
                        r.DepFlight = Conversion.getStrOrNull(R["DepFlight"]);
                        r.DEPFlightTime = Conversion.getDateTimeOrNull(R["DepFlightTime"]);
                        r.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        r.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        r.ArrCityNameL = Conversion.getStrOrNull(R["ArrCityNameL"]);
                        r.RetFlight = Conversion.getStrOrNull(R["RetFlight"]);
                        r.RETFlightTime = Conversion.getDateTimeOrNull(R["RetFlightTime"]);
                        r.EBValidDate = Conversion.getDateTimeOrNull(R["EBValidDate"]);
                        r.Description = Conversion.getStrOrNull(R["Description"]);
                        r.SaleSPONo = Conversion.getInt32OrNull(R["SaleSPONo"]);
                        r.EBPerc = Conversion.getDecimalOrNull(R["EBPerc"]);
                        r.EBAmount = Conversion.getDecimalOrNull(R["EBAmount"]);
                        r.PasEBPer = Conversion.getDecimalOrNull(R["PasEBPer"]);
                        r.SaleEndDate = Conversion.getDateTimeOrNull(R["SaleEndDate"]);
                        r.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        r.Operator = Conversion.getStrOrNull(R["Operator"]);
                        r.Market = Conversion.getStrOrNull(R["Market"]);
                        r.FlightClass = Conversion.getStrOrNull(R["FlightClass"]);
                        r.plSpoExists = Conversion.getBoolOrNull(R["plSpoExists"]);
                        r.FlightValidDate = Conversion.getDateTimeOrNull(R["FlightValidDate"]);
                        r.HotelValidDate = Conversion.getDateTimeOrNull(R["HotelValidDate"]);
                        r.PEB_SaleEndDate = Conversion.getDateTimeOrNull(R["PEB_SaleEndDate"]);
                        r.LastPrice = Conversion.getDecimalOrNull(R["LastPrice"]);
                        r.Direction = Conversion.getInt32OrNull(R["Direction"]);
                        r.Category = Conversion.getInt32OrNull(R["Category"]);
                        r.HolPackName = Conversion.getStrOrNull(R["HolPackName"]);
                        r.HolPackNameL = Conversion.getStrOrNull(R["HolPackNameL"]);
                        r.CurrentCur = Conversion.getStrOrNull(R["CurrentCur"]);
                        r.InfoWeb = Conversion.getStrOrNull(R["InfoWeb"]);
                        r.MaxChdAge = Conversion.getDecimalOrNull(R["MaxChdAge"]);
                        r.OprText = Conversion.getStrOrNull(R["OprText"]);
                        r.StopSaleGuar = Conversion.getInt32OrNull(R["StopSaleGuar"]);
                        r.StopSaleStd = Conversion.getInt32OrNull(R["StopSaleStd"]);
                        r.DFlight = Conversion.getStrOrNull(R["DFlight"]);
                        r.DepServiceType = Conversion.getStrOrNull(R["DepServiceType"]);
                        r.RetServiceType = Conversion.getStrOrNull(R["RetServiceType"]);
                        r.AutoStop = Conversion.getBoolOrNull(R["AutoStop"]);
                        r.TransportType = Conversion.getInt16OrNull(R["TransportType"]);
                        r.AgeGroupList = ageGroups;

                        r.UseDynamicFlights = true;
                        r.DynamicFlights.DFlightNo = Conversion.getStrOrNull(R["DFlightNo"]);
                        r.DynamicFlights.DFClass1 = Conversion.getStrOrNull(R["DFClass1"]);
                        r.DynamicFlights.DFUnit1 = Conversion.getInt16OrNull(R["DFUnit1"]);
                        r.DynamicFlights.DFClass2 = Conversion.getStrOrNull(R["DFClass2"]);
                        r.DynamicFlights.DFUnit2 = Conversion.getInt16OrNull(R["DFUnit2"]);
                        r.DynamicFlights.DFClass3 = Conversion.getStrOrNull(R["DFClass3"]);
                        r.DynamicFlights.DFUnit3 = Conversion.getInt16OrNull(R["DFUnit3"]);
                        r.DynamicFlights.RFlightNo = Conversion.getStrOrNull(R["RFlightNo"]);
                        r.DynamicFlights.RFClass1 = Conversion.getStrOrNull(R["RFClass1"]);
                        r.DynamicFlights.RFUnit1 = Conversion.getInt16OrNull(R["RFUnit1"]);
                        r.DynamicFlights.RFClass2 = Conversion.getStrOrNull(R["RFClass2"]);
                        r.DynamicFlights.RFUnit2 = Conversion.getInt16OrNull(R["RFUnit2"]);
                        r.DynamicFlights.RFClass3 = Conversion.getStrOrNull(R["RFClass3"]);
                        r.DynamicFlights.RFUnit3 = Conversion.getInt16OrNull(R["RFUnit3"]);
                        r.DynamicFlights.OldPrice = Conversion.getDecimalOrNull(R["OldPrice"]);
                        r.DynamicFlights.Price = Conversion.getDecimalOrNull(R["Price"]);

                        r.LogID = logID;

                        r.SearchTime = DateTime.Now;
                        result.Add(r);
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return result;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SearchResultOH> getOnlyHotelSearch(User UserData, SearchCriteria filter, bool checkAvailableRoom, Guid? logID, ref string errorMsg)
        {
            List<SearchResultOH> result = new List<SearchResultOH>();
            string resortWhereStr = string.Empty;
            string categoryWhereStr = string.Empty;
            string hotelWhereStr = string.Empty;
            string roomWhereStr = string.Empty;
            string boardWhereStr = string.Empty;

            List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

            if (!string.IsNullOrEmpty(filter.Resort))
            {
                if (filter.Resort.Split('|').Length > 0)
                {
                    if (filter.Resort.Split('|').Length == 1)
                    {
                        resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", filter.Resort.Split('|')[0]);
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                    else
                    {
                        foreach (string r in filter.Resort.Split('|'))
                        {
                            Location _location = locations.Find(f => f.RecID == Conversion.getInt32OrNull(r));
                            if (_location != null)
                            {
                                if (_location.Type == 3)
                                {
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                                    foreach (Location rL in locations.Where(w => w.Parent == _location.RecID))
                                        resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", rL.RecID);
                                }
                                else
                                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n ", r);
                            }
                        }
                        resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                    }
                }
                else
                    resortWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Category))
            {
                foreach (string r in filter.Category.Split('|'))
                    categoryWhereStr += string.Format("INSERT INTO @Category(Code) VALUES('{0}') \n", r);
                if (categoryWhereStr.Length > 0)
                    categoryWhereStr = "DECLARE @Category TABLE (Code VarChar(10)) \n" + categoryWhereStr;
                else
                    categoryWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Hotel))
            {
                foreach (string r in filter.Hotel.Split('|'))
                    hotelWhereStr += string.Format("INSERT INTO @Hotel(Code) VALUES('{0}') \n", r);
                if (hotelWhereStr.Length > 0)
                    hotelWhereStr = "DECLARE @Hotel TABLE (Code VarChar(10)) \n" + hotelWhereStr;
                else
                    hotelWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Room))
            {
                foreach (string r in filter.Room.Split('|'))
                    roomWhereStr += string.Format("INSERT INTO @Room(Code) VALUES('{0}') \n", r);
                if (roomWhereStr.Length > 0)
                    roomWhereStr = "DECLARE @Room TABLE (Code VarChar(10)) \n" + roomWhereStr;
                else
                    roomWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Board))
            {
                foreach (string r in filter.Board.Split('|'))
                    boardWhereStr += string.Format("INSERT INTO #tmpBoard(Code) VALUES('{0}') \n", r);
                if (boardWhereStr.Length > 0)
                    boardWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpBoard') is not null Drop Table dbo.#tmpBoard 
Create Table #tmpBoard (Code VarChar(10) COLLATE Latin1_General_CI_AS) " + boardWhereStr;
                else
                    boardWhereStr = string.Empty;
            }
            string xmlAgeTable = string.Empty;
            if (filter.RoomCount > 0)
            {
                xmlAgeTable += @"
if OBJECT_ID('TempDB.dbo.#tmpRoomTable') is not null Drop Table dbo.#tmpRoomTable 
Create Table #tmpRoomTable (RoomNr int, Adult int, Child int, ChdAge1 int, ChdAge2 int, ChdAge3 int, ChdAge4 int) ";
                foreach (SearchCriteriaRooms row in filter.RoomsInfo)
                {
                    xmlAgeTable += string.Format("Insert Into #tmpRoomTable (RoomNr, Adult, Child, ChdAge1, ChdAge2, ChdAge3, ChdAge4) Values ({0}, {1}, {2}, {3}, {4}, {5}, {6}) \n",
                        row.RoomNr,
                        row.Adult,
                        row.Child,
                        row.Chd1Age.HasValue ? row.Chd1Age.Value.ToString() : "-1",
                        row.Chd2Age.HasValue ? row.Chd2Age.Value.ToString() : "-1",
                        row.Chd3Age.HasValue ? row.Chd3Age.Value.ToString() : "-1",
                        row.Chd4Age.HasValue ? row.Chd4Age.Value.ToString() : "-1");
                }
            }

            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(resortWhereStr))
                sb.AppendLine(resortWhereStr);
            if (!string.IsNullOrEmpty(categoryWhereStr))
                sb.AppendLine(categoryWhereStr);
            if (!string.IsNullOrEmpty(hotelWhereStr))
                sb.AppendLine(hotelWhereStr);
            if (!string.IsNullOrEmpty(roomWhereStr))
                sb.AppendLine(roomWhereStr);
            if (!string.IsNullOrEmpty(boardWhereStr))
                sb.AppendLine(boardWhereStr);

            sb.AppendLine(xmlAgeTable);

            sb.AppendLine("if @Cur = '' Set @Cur = null ");

            sb.AppendLine("Select RT.RoomNr,");
            sb.AppendLine("  P.PriceID,P.PartNo,P.Hotel,P.Room,P.Accom,P.Board,P.CheckIn,P.Night,P.SaleCur,");
            sb.AppendLine("  AccomFullName=");
            sb.AppendLine("	  case when P.HAPAdult>0 then LTrim(Str(P.HAPAdult,2))+' Adl' else '' end+");
            sb.AppendLine("	  case when P.HAPChdAgeG1>0 and P.ChdG1Age2>0 then '+'+Convert(VarChar(5),P.HAPChdAgeG1)+' Chd'+'('+Convert(VarChar(5),P.ChdG1Age1)+'-'+Convert(VarChar(5),P.ChdG1Age2)+')' else '' end+");
            sb.AppendLine("	  case when P.HAPChdAgeG2>0 and P.ChdG2Age2>0 then '+'+Convert(VarChar(5),P.HAPChdAgeG2)+' Chd'+'('+Convert(VarChar(5),P.ChdG2Age1)+'-'+Convert(VarChar(5),P.ChdG2Age2)+')' else '' end+");
            sb.AppendLine("	  case when P.HAPChdAgeG3>0 and P.ChdG3Age2>0 then '+'+Convert(VarChar(5),P.HAPChdAgeG3)+' Chd'+'('+Convert(VarChar(5),P.ChdG3Age1)+'-'+Convert(VarChar(5),P.ChdG3Age2)+')' else '' end+");
            sb.AppendLine("	  case when P.HAPChdAgeG4>0 and P.ChdG4Age2>0 then '+'+Convert(VarChar(5),P.HAPChdAgeG4)+' Chs'+'('+Convert(VarChar(5),P.ChdG4Age1)+'-'+Convert(VarChar(5),P.ChdG4Age2)+')' else '' end,");
            sb.AppendLine("  AccomFullNameL=P.AccomFullName,");
            sb.AppendLine("  P.ContractPrice,P.SalePrice,P.AdlPrice,P.EBed1Price,P.ChdG1Price,P.ChdG2Price,P.ChdG3Price,P.ChdG4Price,P.CAdlPrice,P.CEBed1Price,");
            sb.AppendLine("  P.CChdG1Price, P.CChdG2Price, P.CChdG3Price, P.CChdG4Price, ");
            sb.AppendLine("  P.ChdG1Age1, P.ChdG1Age2, P.ChdG2Age1, P.ChdG2Age2, P.ChdG3Age1, P.ChdG3Age2, P.ChdG4Age1, P.ChdG4Age2,");
            sb.AppendLine("  P.HAPRecID, P.HAPAdult, P.HAPChdAgeG1, P.HAPChdAgeG2, P.HAPChdAgeG3, P.HAPChdAgeG4,");
            sb.AppendLine("  P.AdlCount, P.ChdCount,P.Market,");
            sb.AppendLine("  P.HotelName, HotelNameL=isnull(dbo.FindLocalName(P.HNameLID,@Market), P.HotelName),");
            sb.AppendLine("  P.HotCat, HotCatName=P.CatName, HotCatNameL=isnull(dbo.FindLocalName(P.HCNameLID,@Market), P.CatName),");
            sb.AppendLine("  P.HotLocation, HotLocationName=L.Name, HotLocationNameL=isnull(dbo.FindLocalName(L.NameLID,@Market),L.Name),");
            sb.AppendLine("  P.RoomName,RoomNameL=isnull(dbo.FindLocalName(P.HRNameLID,@Market),P.RoomName),");
            sb.AppendLine("  P.AccomName,AccomNameL=isnull(dbo.FindLocalName(P.HANameLID,@Market),P.AccomName),");
            sb.AppendLine("  P.BoardName,BoardNameL=isnull(dbo.FindLocalName(P.HBNameLID,@Market),P.BoardName),");
            sb.AppendLine("  ArrCity=L.City,ArrCityName=Lc.Name,ArrCityNameL=isnull(dbo.FindLocalName(Lc.NameLID,@Market),Lc.Name)");
            sb.AppendLine("From HotelSaleExtV P  ");
            sb.AppendLine("Join Location (NOLOCK) L ON L.RecID=P.Location");
            sb.AppendLine("Left Join Location (NOLOCK) Lc ON Lc.RecID=L.City");
            sb.AppendLine("Join #tmpRoomTable RT on RT.Adult=HAPAdult And RT.Child=(HAPChdAgeG1+HAPChdAgeG2+HAPChdAgeG3+HAPChdAgeG4)");
            sb.AppendLine("Where  Market=@Market ");
            sb.AppendLine(" And (P.CheckIn Between @CIn1 AND @CIn2) ");
            sb.AppendLine(" And L.City=" + filter.ArrCity.ToString());
            if (filter.NightFrom != filter.NightTo && filter.NightTo >= filter.NightFrom)
                sb.Append("  And (P.Night between @Night1 and @Night2) ");
            else
                sb.Append("  And P.Night=@Night1 ");
            if (filter.RoomsInfo.Where(w => w.Chd1Age > -1 || w.Chd3Age > -1 || w.Chd3Age > -1 || w.Chd4Age > -1).Count() > 0)
                sb.AppendLine(" And dbo.ChdAgeControl4Age(RT.Child,RT.ChdAge1,RT.ChdAge2,RT.ChdAge3,RT.ChdAge4,HChdAgeG1,HChdAgeG2,HChdAgeG3,HChdAgeG4,ChdG1Age1,ChdG1Age2,ChdG2Age1,ChdG2Age2,ChdG3Age1,ChdG3Age2,ChdG4Age1,ChdG4Age2)=1");

            if (!string.IsNullOrEmpty(hotelWhereStr))
                sb.Append(" And Exists(Select Code From @Hotel Where Code=P.Hotel) \n");
            if (!string.IsNullOrEmpty(categoryWhereStr))
                sb.Append("  And Exists(Select Code From @Category Where Code=P.HotCat)");
            if (!string.IsNullOrEmpty(resortWhereStr))
            {
                if (filter.Resort.Split('|').Length == 1)
                {
                    Location _locations = locations.Find(f => f.RecID == Conversion.getInt32OrNull(filter.Resort.Split('|')[0]));
                    if (_locations != null && _locations.Type == 3)
                        sb.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.Town={0} And L.RecID=P.HotLocation) \n", _locations.RecID);
                    else
                        sb.AppendFormat("  And Exists(Select RecID From Location L (NOLOCK) Where L.RecID={0} And L.RecID=P.HotLocation) \n", _locations.RecID);
                }
                else
                    sb.Append("  And Exists(Select R.RecID From #tmpResort R Where R.RecID=P.HotLocation) \n");
            }
            if (!string.IsNullOrEmpty(roomWhereStr))
            {
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Room From GrpRoomDet GRD (NOLOCK) Where Exists(Select Code From @Room Where Code=GRD.Groups) And Room=P.Room) \n");
                else
                    sb.Append("  And Exists(Select Code From @Room Where Code=P.Room) \n");
            }
            if (!string.IsNullOrEmpty(boardWhereStr))
            {
                if (filter.UseGroup)
                    sb.Append("  And Exists(Select Board From GrpBoardDet GBD (NOLOCK) Where Exists(Select Code From @Board Where Code=GBD.Groups) And Board=P.Board) \n");
                else
                    sb.Append("  And Exists(Select Code From @Board Where Code=P.Board) \n");
            }
            #region Age Group
            List<AgeGroup> ageGroups = new List<AgeGroup>();
            int roomCnt = 0;
            int refNo = 0;
            foreach (SearchCriteriaRooms row in filter.RoomsInfo)
            {
                roomCnt++;
                refNo++;
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Adult, GroupNo = roomCnt, RefNo = refNo, Unit = row.Adult, Age = null, DateOfBirth = null });
                if (row.Chd1Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Infant, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd1Age.Value, DateOfBirth = null });
                if (row.Chd2Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd2Age.Value, DateOfBirth = null });
                if (row.Chd3Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd3Age.Value, DateOfBirth = null });
                if (row.Chd4Age.HasValue)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = 1, Age = row.Chd4Age.Value, DateOfBirth = null });
            }

            #endregion

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sb.ToString());
            dbCommand.CommandTimeout = 120;
            try
            {
                int i = 0;

                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "CIn1", DbType.Date, filter.CheckIn.Date);
                //if (filter.ExpandDate > 0) 
                db.AddInParameter(dbCommand, "CIn2", DbType.Date, filter.CheckIn.Date.AddDays(filter.ExpandDate));
                db.AddInParameter(dbCommand, "Night1", DbType.Int16, filter.NightFrom);
                if (filter.NightFrom < filter.NightTo)
                    db.AddInParameter(dbCommand, "Night2", DbType.Int16, filter.NightTo);
                db.AddInParameter(dbCommand, "Cur", DbType.AnsiString, !filter.CurControl || string.IsNullOrEmpty(filter.CurrentCur) ? null : filter.CurrentCur);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ++i;
                        SearchResultOH r = new SearchResultOH();
                        //r.Calculated = true;
                        r.RefNo = i;
                        r.RoomNr = (int)R["RoomNr"];
                        r.Accom = Conversion.getStrOrNull(R["Accom"]);
                        //r.AdlCount = Conversion.getInt16OrNull(R["AdlCount"]);
                        //r.AdlPrice = Conversion.getDecimalOrNull(R["AdlPrice"]);
                        //r.AccomFullName = Conversion.getStrOrNull(R["AccomFullName"]);
                        r.AccomName = Conversion.getStrOrNull(R["AccomName"]);
                        r.AccomNameL = Conversion.getStrOrNull(R["AccomNameL"]);
                        //r.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        //r.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        //r.ArrCityNameL = Conversion.getStrOrNull(R["ArrCityNameL"]);
                        r.Board = Conversion.getStrOrNull(R["Board"]);
                        r.BoardName = Conversion.getStrOrNull(R["BoardName"]);
                        r.BoardNameL = Conversion.getStrOrNull(R["BoardNameL"]);
                        //r.ChdCount = Conversion.getInt16OrNull(R["ChdCount"]);
                        r.ChdG1Age1 = Conversion.getDecimalOrNull(R["ChdG1Age1"]);
                        r.ChdG1Age2 = Conversion.getDecimalOrNull(R["ChdG1Age2"]);
                        r.ChdG2Age1 = Conversion.getDecimalOrNull(R["ChdG2Age1"]);
                        r.ChdG2Age2 = Conversion.getDecimalOrNull(R["ChdG2Age2"]);
                        r.ChdG3Age1 = Conversion.getDecimalOrNull(R["ChdG3Age1"]);
                        r.ChdG3Age2 = Conversion.getDecimalOrNull(R["ChdG3Age2"]);
                        r.ChdG4Age1 = Conversion.getDecimalOrNull(R["ChdG4Age1"]);
                        r.ChdG4Age2 = Conversion.getDecimalOrNull(R["ChdG4Age2"]);
                        r.CheckIn = Conversion.getDateTimeOrNull(R["CheckIn"]);
                        r.Night = Conversion.getInt16OrNull(R["Night"]);
                        ////r.CheckOut = r.CheckIn.Value.AddDays(r.Night.Value);
                        ////r.CalcSalePrice = Conversion.getDecimalOrNull(R["ContractPrice"]);
                        r.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        //r.EBed1Price = Conversion.getDecimalOrNull(R["EBed1Price"]);
                        //r.HAdult = (Int16)R["HAPAdult"];
                        //r.HChdAgeG1 = (Int16)R["HAPChdAgeG1"];
                        //r.HChdAgeG2 = (Int16)R["HAPChdAgeG2"];
                        //r.HChdAgeG3 = (Int16)R["HAPChdAgeG3"];
                        //r.HChdAgeG4 = (Int16)R["HAPChdAgeG4"];
                        //r.HAPRecId = Conversion.getInt32OrNull(R["HAPRecID"]);
                        r.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        r.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        r.HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]);
                        r.HotCat = Conversion.getStrOrNull(R["HotCat"]);
                        //r.HotCatName = Conversion.getStrOrNull(R["HotCatName"]);
                        //r.HotCatNameL = Conversion.getStrOrNull(R["HotCatNameL"]);
                        //r.HotLocation = Conversion.getInt32OrNull(R["HotLocation"]);
                        r.HotLocationName = Conversion.getStrOrNull(R["HotLocationName"]);
                        r.HotLocationNameL = Conversion.getStrOrNull(R["HotLocationNameL"]);
                        r.Market = Conversion.getStrOrNull(R["Market"]);
                        r.PartNo = Conversion.getInt32OrNull(R["PartNo"]);
                        r.PriceID = Conversion.getInt32OrNull(R["PriceID"]);
                        r.Room = Conversion.getStrOrNull(R["Room"]);
                        r.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        r.RoomNameL = Conversion.getStrOrNull(R["RoomNameL"]);
                        // r.LastPrice = Conversion.getDecimalOrNull(R["SalePrice"]);

                        result.Add(r);
                    }
                    return result;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightDays> getCheckIn(string Market, string Operator, int? ArrCity, string packType, string b2bMenuCat, string holPack, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"
Select Distinct CheckIn 
From CatPriceP P (NOLOCK)
Join CatalogPack C (NOLOCK) ON C.RecID=P.CatPackID
Where P.CheckIn>=GetDate()
  And C.Operator=@operator
  And C.Market=@market   
";

            if (!string.IsNullOrEmpty(b2bMenuCat))
                tsql += string.Format("  And C.B2BMenuCat='{0}' ", b2bMenuCat);
            if (!string.IsNullOrEmpty(packType))
                tsql += string.Format("  And C.PackType='{0}' ", packType);
            if (ArrCity.HasValue)
                tsql += string.Format("  And C.ArrCity={0} ", ArrCity.Value.ToString());
            if (!string.IsNullOrEmpty(holPack))
                tsql += string.Format("  And C.HolPack='{0}' ", holPack);

            List<FlightDays> flightDayList = new List<FlightDays>();

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "operator", DbType.AnsiString, Operator);
                db.AddInParameter(dbCommand, "market", DbType.AnsiString, Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightDays row = new FlightDays
                        {
                            FlyDate = (DateTime)oReader["CheckIn"]
                        };
                        flightDayList.Add(row);
                    }
                }
                return flightDayList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return flightDayList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }

    public class OnlyHotelSearch
    {
        public List<HotelMarOptRecord> getHotelMarOptList(User UserData, List<SearchResultOH> data, ref string errorMsg)
        {
            List<HotelMarOptRecord> records = new List<HotelMarOptRecord>();
            var query = from q in data
                        group q by new { q.Hotel } into k
                        select new { Hotel = k.Key.Hotel };
            string tmpQueryStr = @"
                if OBJECT_ID('TempDB.dbo.#tmpHotelTable') is not null Drop Table dbo.#tmpHotelTable 
                Create TABLE #tmpHotelTable (Hotel VarChar(10) COLLATE Latin1_General_CI_AS) ";
            foreach (var row in query)
                tmpQueryStr += string.Format(" Insert Into #tmpHotelTable (Hotel) Values ('{0}') ", row.Hotel);
            tmpQueryStr += @"Select H.RecID, Hotel=HT.Hotel, Market=@Market, InfoWeb=isnull(H.InfoWeb, ''), H.MaxChdAge
                                 From #tmpHotelTable HT
                                 Left Join HotelMarOpt (NOLOCK) H ON H.Hotel=HT.Hotel
                                 Where H.Market=@Market";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tmpQueryStr);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        HotelMarOptRecord record = new HotelMarOptRecord();
                        record.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        record.Hotel = Conversion.getStrOrNull(oReader["Hotel"]);
                        record.Market = Conversion.getStrOrNull(oReader["Market"]);
                        record.InfoWeb = Conversion.getStrOrNull(oReader["InfoWeb"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Dispose();
            }
        }

        public List<OnlyHotelSearchFilterData> getOnlyHotelSearchFilterData(User UserData, bool Groups, ref string errorMsg)
        {
            List<OnlyHotelSearchFilterData> records = new List<OnlyHotelSearchFilterData>();
            string tsql =
@"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
SET ARITHABORT ON
Declare @SaleDate Date
Select @SaleDate=GETDATE()

if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location

Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] 
Into #Location
From Location(NOLOCK)
if @Groups=0
Begin
    Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),
      Room=R.Code,RoomName=R.Name,RoomNameL=isnull(dbo.FindLocalName(R.NameLID,@Market),R.Name),  
      Board=B.Code,BoardName=B.Name,BoardNameL=isnull(dbo.FindLocalName(B.NameLID,@Market),B.Name),
      H.Category,CategoryName=HC.Name,CategoryNameL=isnull(dbo.FindLocalName(HC.NameLID,@Market),HC.Name),
      H.Location,LocationName=L.Name,LocationNameL=L.NameL,
      L.City,CityName=Lc.Name,CityNameL=Lc.NameL,
      L.Country,CountryName=Lu.Name,CountryNameL=Lu.NameL
    From OHotelSalePrice (NOLOCK) P 
    Join Hotel (NOLOCK) H ON P.HotelID=H.RecID
    Join HotelCat (NOLOCK) HC ON HC.Code=H.Category	
    Join HotelRoom (NOLOCK) R ON R.RecID=P.RoomID
    Join HotelBoard (NOLOCK) B ON B.RecID=P.BoardID
    Join #Location (NOLOCK) L ON H.Location=L.RecID
    Join #Location (NOLOCK) Lc ON L.City=Lc.RecID
    Join #Location (NOLOCK) Lu ON L.Country=Lu.RecID
    Where P.CheckIn>=@SaleDate
    Group By H.Code,H.Name,H.NameLID,R.Code,R.Name,R.NameLID,B.Code,B.Name,B.NameLID,
		     H.Category,HC.Name,HC.NameLID,H.Location,L.Name,L.NameL,L.City,Lc.Name,Lc.NameL,L.Country,Lu.Name,Lu.NameL
End
Else
Begin
    Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),
      Room=RG.Code,RoomName=RG.Name,RoomNameL=isnull(dbo.FindLocalName(RG.NameLID,@Market),RG.Name),  
      Board=BG.Code,BoardName=BG.Name,BoardNameL=isnull(dbo.FindLocalName(BG.NameLID,@Market),BG.Name),
      H.Category,CategoryName=HC.Name,CategoryNameL=isnull(dbo.FindLocalName(HC.NameLID,@Market),HC.Name),
      H.Location,LocationName=L.Name,LocationNameL=L.NameL,
      L.City,CityName=Lc.Name,CityNameL=Lc.NameL,
      L.Country,CountryName=Lu.Name,CountryNameL=Lu.NameL
    From OHotelSalePrice (NOLOCK) P 
    Join Hotel (NOLOCK) H ON P.HotelID=H.RecID
    Join HotelCat (NOLOCK) HC ON HC.Code=H.Category	
    Join HotelRoom (NOLOCK) R ON R.RecID=P.RoomID
    Join HotelBoard (NOLOCK) B ON B.RecID=P.BoardID
    Join #Location (NOLOCK) L ON H.Location=L.RecID
    Join #Location (NOLOCK) Lc ON L.City=Lc.RecID
    Join #Location (NOLOCK) Lu ON L.Country=Lu.RecID
    OUTER APPLY
    (
      Select GB.Code,GB.Name,GB.NameLID From GrpBoard (NOLOCK) GB 
      Join GrpBoardDet (NOLOCK) GBD ON GBD.Groups=GB.Code 
      Where GBD.Board=B.Code
    ) BG
    OUTER APPLY
    (
      Select RM.Code,RM.Name,RM.NameLID From GrpRoom (NOLOCK) RM
      Join GrpRoomDet (NOLOCK) RMD ON RMD.Groups=RM.Code
      Where RMD.Room=R.Code
    ) RG
    Where P.CheckIn>=@SaleDate
    Group By H.Code,H.Name,H.NameLID,RG.Code,RG.Name,RG.NameLID,BG.Code,BG.Name,BG.NameLID,
		     H.Category,HC.Name,HC.NameLID,H.Location,L.Name,L.NameL,L.City,Lc.Name,Lc.NameL,L.Country,Lu.Name,Lu.NameL	
End
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            dbCommand.CommandTimeout = 240;
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Groups", DbType.Boolean, Groups);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        OnlyHotelSearchFilterData record = new OnlyHotelSearchFilterData();
                        record.Hotel = Conversion.getStrOrNull(R["Code"]);
                        record.HotelName = Conversion.getStrOrNull(R["Name"]);
                        record.HotelNameL = Conversion.getStrOrNull(R["NameL"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        record.RoomNameL = Conversion.getStrOrNull(R["RoomNameL"]);
                        record.Board = Conversion.getStrOrNull(R["Board"]);
                        record.BoardName = Conversion.getStrOrNull(R["BoardName"]);
                        record.BoardNameL = Conversion.getStrOrNull(R["BoardNameL"]);
                        record.Category = Conversion.getStrOrNull(R["Category"]);
                        record.CategoryName = Conversion.getStrOrNull(R["CategoryName"]);
                        record.CategoryNameL = Conversion.getStrOrNull(R["CategoryNameL"]);
                        record.Location = Conversion.getInt32OrNull(R["Location"]);
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        record.City = Conversion.getInt32OrNull(R["City"]);
                        record.CityName = Conversion.getStrOrNull(R["CityName"]);
                        record.CityNameL = Conversion.getStrOrNull(R["CityNameL"]);
                        record.Country = Conversion.getInt32OrNull(R["Country"]);
                        record.CountryName = Conversion.getStrOrNull(R["CountryName"]);
                        record.CountryNameL = Conversion.getStrOrNull(R["CountryNameL"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeHotelName> getOnlyHotelSearchFilterHotel(bool useMarketLang, List<OnlyHotelSearchFilterData> searchData, int? Location, int? Country, int? City, string Resort, string Category, ref string errorMsg)
        {
            if (Location != null)
                return (from q in searchData
                        where q.Location == Location
                        group q by new { Code = q.Hotel, Name = useMarketLang ? q.HotelNameL : q.HotelName, Category = q.Category, LocationName = useMarketLang ? q.LocationNameL : q.LocationName } into k
                        select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.LocationName }).ToList<CodeHotelName>();
            else
            {
                Country = Country > 0 ? Country : null;
                City = City > 0 ? City : null;
                if (!string.IsNullOrEmpty(Resort) && Resort.Split('|').Count() > 0)
                {
                    var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                    return (from q in searchData
                            join r in resort on q.Location equals r
                            where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                              (string.IsNullOrEmpty(City.ToString()) || q.City == City) &&
                              (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Location.HasValue ? q.Location.Value.ToString() : "")) &&
                              (string.IsNullOrEmpty(Category) || q.Category == Category)
                            group q by new { Code = q.Hotel, Name = useMarketLang ? q.HotelNameL : q.HotelName, Category = q.Category, LocationName = useMarketLang ? q.LocationNameL : q.LocationName } into k
                            select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.LocationName }).ToList<CodeHotelName>();
                }
                else
                {
                    return (from q in searchData
                            where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                              (string.IsNullOrEmpty(City.ToString()) || q.City == City) &&
                              (string.IsNullOrEmpty(Category) || q.Category == Category)
                            group q by new { Code = q.Hotel, Name = useMarketLang ? q.HotelNameL : q.HotelName, Category = q.Category, LocationName = useMarketLang ? q.LocationNameL : q.LocationName } into k
                            select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.LocationName }).ToList<CodeHotelName>();
                }
            }
        }

        public List<CodeName> getOnlyHotelSearchFilterRoom(bool useMarketLang, List<OnlyHotelSearchFilterData> searchData, int? ArrCity, string Resort, string Hotel_Code, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                List<Location> locations = TvBo.CacheObjects.getLocationList(string.Empty);
                Location loc = locations.Find(f => f.RecID == ArrCity);
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (!string.IsNullOrEmpty(Resort) && Resort.IndexOf('|') > -1)
                {
                    var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                    return (from q in searchData
                            join r in resort on q.Location.Value equals r
                            group q by new { Code = q.Room, Name = useMarketLang ? q.RoomNameL : q.RoomName } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
                else
                {
                    if (loc != null && loc.Parent == loc.RecID)
                        return (from q in searchData
                                join q1 in locations on q.City equals q1.RecID
                                where (string.IsNullOrEmpty(ArrCity.ToString()) || q1.Country == ArrCity) &&
                                  (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Location.HasValue ? q.Location.Value.ToString() : ""))
                                group q by new { Code = q.Room, Name = useMarketLang ? q.RoomNameL : q.RoomName } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        return (from q in searchData
                                where (string.IsNullOrEmpty(ArrCity.ToString()) || q.City == ArrCity) &&
                                  (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Location.HasValue ? q.Location.Value.ToString() : ""))
                                group q by new { Code = q.Room, Name = useMarketLang ? q.RoomNameL : q.RoomName } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
            }
            else
            {
                return (from q in searchData
                        where Hotel_Code.Split('|').Contains(q.Hotel)
                        group q by new { Code = q.Room, Name = useMarketLang ? q.RoomNameL : q.RoomName } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
        }

        public List<CodeName> getOnlyHotelSearchFilterBoard(bool useMarketLang, List<OnlyHotelSearchFilterData> searchData, int? ArrCity, string Resort, string Hotel_Code, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                List<Location> locations = TvBo.CacheObjects.getLocationList(string.Empty);
                Location loc = locations.Find(f => f.RecID == ArrCity);
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (!string.IsNullOrEmpty(Resort) && Resort.Split('|').Count() > 0)
                {
                    var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                    return (from q in searchData
                            join r in resort on q.Location equals r
                            group q by new { Code = q.Board, Name = useMarketLang ? q.BoardNameL : q.BoardName } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
                else
                {
                    if (loc != null && loc.Parent == loc.RecID)
                        return (from q in searchData
                                join q1 in locations on q.City equals q1.RecID
                                where (string.IsNullOrEmpty(ArrCity.ToString()) || q1.Country == ArrCity) &&
                                  (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Location.HasValue ? q.Location.Value.ToString() : ""))
                                group q by new { Code = q.Board, Name = useMarketLang ? q.BoardNameL : q.BoardName } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        return (from q in searchData
                                where (string.IsNullOrEmpty(ArrCity.ToString()) || q.City == ArrCity) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Location.HasValue ? q.Location.Value.ToString() : ""))
                                group q by new { Code = q.Board, Name = useMarketLang ? q.BoardNameL : q.BoardName } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
            }
            else
                return (from q in searchData
                        where Hotel_Code.Split('|').Contains(q.Hotel)
                        group q by new { Code = q.Board, Name = useMarketLang ? q.BoardNameL : q.BoardName } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<CodeName> getOnlyHotelSearchFilterCategory(bool useMarketLang, List<OnlyHotelSearchFilterData> searchData, int? Country, int? City, string Resort, ref string errorMsg)
        {
            Country = Country > 0 ? Country : null;
            City = City > 0 ? City : null;
            if (!string.IsNullOrEmpty(Resort) && Resort.Split('|').Count() > 0)
            {
                var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                return (from q in searchData
                        join r in resort on q.Location equals r
                        where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                              (string.IsNullOrEmpty(City.ToString()) || q.City == City) &&
                              (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Location.HasValue ? q.Location.Value.ToString() : ""))
                        group q by new { Code = q.Category, Name = useMarketLang ? q.CategoryNameL : q.CategoryName } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
            else
            {
                return (from q in searchData
                        where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                              (string.IsNullOrEmpty(City.ToString()) || q.City == City)
                        group q by new { Code = q.Category, Name = useMarketLang ? q.CategoryNameL : q.CategoryName } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
        }

        public List<LocationIDName> getOnlyHotelSearchFilterResort(bool useMarketLang, List<OnlyHotelSearchFilterData> searchData, int? Country, int? ArrCity, ref string errorMsg)
        {
            Country = Country > 0 ? Country : null;
            ArrCity = ArrCity > 0 ? ArrCity : null;
            if (useMarketLang)
                return (from q in searchData
                        where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                          (string.IsNullOrEmpty(ArrCity.ToString()) || q.City == ArrCity) &&
                          !string.IsNullOrEmpty(q.LocationName)
                        orderby (useMarketLang ? q.LocationNameL : q.LocationName)
                        group q by new { RecID = q.Location, Name = useMarketLang ? q.LocationNameL : q.LocationName } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
            else
                return (from q in searchData
                        where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                          (string.IsNullOrEmpty(ArrCity.ToString()) || q.City == ArrCity) &&
                          !string.IsNullOrEmpty(q.LocationName)
                        orderby (useMarketLang ? q.LocationNameL : q.LocationName)
                        group q by new { RecID = q.Location, Name = useMarketLang ? q.LocationNameL : q.LocationName } into k
                        select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
        }

        public List<OnlyHotelSearchFilterCity> getOnlyHotelSearchFilterCity(bool useMarketLang, List<OnlyHotelSearchFilterData> searchData, int? Country, ref string errorMsg)
        {
            Country = Country > 0 ? Country : null;

            return (from q in searchData
                    where !Country.HasValue || (q.Country.HasValue && q.Country == Country.Value)
                    orderby (useMarketLang ? q.CountryNameL : q.CountryName), (useMarketLang ? q.CityNameL : q.CityName)
                    group q by new { Country = q.Country, CountryName = useMarketLang ? q.CountryNameL : q.CountryName, City = q.City, CityName = useMarketLang ? q.CityNameL : q.CityName } into k
                    select new OnlyHotelSearchFilterCity { Country = k.Key.Country, CountryName = k.Key.CountryName, City = k.Key.City, CityName = k.Key.CityName }).ToList<OnlyHotelSearchFilterCity>();
        }

        public List<LocationIDName> getOnlyHotelSearchFilterCountry(bool useMarketLang, List<OnlyHotelSearchFilterData> searchData, ref string errorMsg)
        {
            return (from q in searchData
                    orderby (useMarketLang ? q.CountryNameL : q.CountryName)
                    group q by new { RecID = q.Country, Name = (useMarketLang ? q.CountryNameL : q.CountryName) } into k
                    select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
        }

        public List<SearchResultOH> getOnlyHotelPriceSearch(User UserData, SearchCriteria filter, Guid? logID, ref string errorMsg)
        {
            List<SearchResultOH> records = new List<SearchResultOH>();

            List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

            StringBuilder sq = new StringBuilder();
            string param = string.Empty;

            param += "<ROOT>";
            #region How many rooms searching
            foreach (SearchCriteriaRooms row in filter.RoomsInfo)
            {
                param += string.Format("<SEARCH RoomNr=\"{0}\" Adult=\"{1}\" Child=\"{2}\" ", row.RoomNr, row.Adult, row.Child);
                if (row.Child > 0)
                    param += string.Format("ChdAge1=\"{0}\" ", row.Chd1Age);
                if (row.Child > 1)
                    param += string.Format("ChdAge2=\"{0}\" ", row.Chd2Age);
                if (row.Child > 2)
                    param += string.Format("ChdAge3=\"{0}\" ", row.Chd3Age);
                if (row.Child > 3)
                    param += string.Format("ChdAge4=\"{0}\" ", row.Chd4Age);
                param += "/>";
            }
            #endregion

            #region How many search location
            if (!string.IsNullOrEmpty(filter.Resort))
            {
                if (filter.Resort.Split('|').Length > 0)
                {
                    if (filter.Resort.Split('|').Length == 1)
                    {
                        param += string.Format("<LOCATIONS Location=\"{0}\" /> ", filter.Resort.Split('|')[0]);
                    }
                    else
                    {
                        foreach (string r in filter.Resort.Split('|'))
                        {
                            Location _location = locations.Find(f => f.RecID == Conversion.getInt32OrNull(r));
                            if (_location != null)
                            {
                                if (_location.Type == 3)
                                {
                                    foreach (Location rL in locations.Where(w => w.Parent == _location.RecID))
                                        param += string.Format("<LOCATIONS Location=\"{0}\" /> ", rL.RecID);
                                }
                                else
                                    param += string.Format("<LOCATIONS Location=\"{0}\" /> ", r);
                            }
                        }
                    }
                }
            }
            #endregion

            #region How many search hotel
            if (!string.IsNullOrEmpty(filter.Hotel))
            {
                foreach (string r in filter.Hotel.Split('|'))
                    param += string.Format("<HOTELS Hotel=\"{0}\" />", r);
            }
            #endregion

            #region How many search hotel category
            if (!string.IsNullOrEmpty(filter.Category))
            {
                foreach (string r in filter.Category.Split('|'))
                    param += string.Format("<HOTCATS Category=\"{0}\" />", r);
            }
            #endregion

            #region How many search hotel room
            if (!string.IsNullOrEmpty(filter.Room))
            {
                foreach (string r in filter.Room.Split('|'))
                    param += string.Format("<ROOMS Room=\"{0}\" />", r);
            }
            #endregion

            #region How many search hotel board
            if (!string.IsNullOrEmpty(filter.Board))
            {
                foreach (string r in filter.Board.Split('|'))
                    param += string.Format("<BOARDS Board=\"{0}\" />", r);
            }
            #endregion
            param += "</ROOT>";

            XmlDocument paramsXml = new XmlDocument();
            paramsXml.LoadXml(param);

            sq.AppendLine("Exec dbo.usp_HotelSaleMultiSearch @Market,@FirstCheckIn,@LastCheckIn,@FirstNight,@LastNight,@Cur,@Params,@RoomGroupSearch,@BoardGroupSearch");

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(sq.ToString());
            try
            {
                int i = 0;
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "FirstCheckIn", DbType.DateTime, filter.CheckIn);
                db.AddInParameter(dbCommand, "LastCheckIn", DbType.DateTime, filter.CheckIn.AddDays(filter.ExpandDate));
                db.AddInParameter(dbCommand, "FirstNight", DbType.Int16, filter.NightFrom);
                db.AddInParameter(dbCommand, "LastNight", DbType.Int16, filter.NightTo);
                db.AddInParameter(dbCommand, "Cur", DbType.AnsiString, !filter.CurControl || string.IsNullOrEmpty(filter.CurrentCur) ? null : filter.CurrentCur);
                db.AddInParameter(dbCommand, "Params", DbType.Xml, paramsXml.InnerXml);
                db.AddInParameter(dbCommand, "RoomGroupSearch", DbType.Boolean, filter.UseGroup);
                db.AddInParameter(dbCommand, "BoardGroupSearch", DbType.Boolean, filter.UseGroup);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        i++;
                        SearchResultOH record = new SearchResultOH();
                        record.AccomID = Conversion.getInt32OrNull(R["AccomID"]);
                        record.Accom = Conversion.getStrOrNull(R["Accom"]);
                        record.AccomName = Conversion.getStrOrNull(R["AccomName"]);
                        record.AccomNameL = Conversion.getStrOrNull(R["AccomNameL"]);
                        record.AdlPrice = Conversion.getDecimalOrNull(R["AdlPrice"]);
                        record.AdultCount = Conversion.getInt32OrNull(R["AdultCount"]);
                        record.Allotment = Conversion.getInt32OrNull(R["Allotment"]);
                        record.BoardID = Conversion.getInt32OrNull(R["BoardID"]);
                        record.Board = Conversion.getStrOrNull(R["Board"]);
                        record.BoardName = Conversion.getStrOrNull(R["BoardName"]);
                        record.BoardNameL = Conversion.getStrOrNull(R["BoardNameL"]);
                        record.CAdlPrice = Conversion.getDecimalOrNull(R["CAdlPrice"]);
                        record.CatName = Conversion.getStrOrNull(R["CatName"]);
                        record.CatNameL = Conversion.getStrOrNull(R["CatNameL"]);
                        record.CChdG1Price = Conversion.getDecimalOrNull(R["CChdG1Price"]);
                        record.CChdG2Price = Conversion.getDecimalOrNull(R["CChdG2Price"]);
                        record.CChdG3Price = Conversion.getDecimalOrNull(R["CChdG3Price"]);
                        record.CChdG4Price = Conversion.getDecimalOrNull(R["CChdG4Price"]);
                        record.CEBed1Price = Conversion.getDecimalOrNull(R["CEBed1Price"]);
                        record.ChdAge1 = Conversion.getInt32OrNull(R["ChdAge1"]);
                        record.ChdAge2 = Conversion.getInt32OrNull(R["ChdAge2"]);
                        record.ChdAge3 = Conversion.getInt32OrNull(R["ChdAge3"]);
                        record.ChdAge4 = Conversion.getInt32OrNull(R["ChdAge4"]);
                        record.ChdG1Age1 = Conversion.getDecimalOrNull(R["ChdG1Age1"]);
                        record.ChdG1Age2 = Conversion.getDecimalOrNull(R["ChdG1Age2"]);
                        record.ChdG1Price = Conversion.getDecimalOrNull(R["ChdG1Price"]);
                        record.ChdG2Age1 = Conversion.getDecimalOrNull(R["ChdG2Age1"]);
                        record.ChdG2Age2 = Conversion.getDecimalOrNull(R["ChdG2Age2"]);
                        record.ChdG2Price = Conversion.getDecimalOrNull(R["ChdG2Price"]);
                        record.ChdG3Age1 = Conversion.getDecimalOrNull(R["ChdG3Age1"]);
                        record.ChdG3Age2 = Conversion.getDecimalOrNull(R["ChdG3Age2"]);
                        record.ChdG3Price = Conversion.getDecimalOrNull(R["ChdG3Price"]);
                        record.ChdG4Age1 = Conversion.getDecimalOrNull(R["ChdG4Age1"]);
                        record.ChdG4Age2 = Conversion.getDecimalOrNull(R["ChdG4Age2"]);
                        record.ChdG4Price = Conversion.getDecimalOrNull(R["ChdG4Price"]);
                        record.CheckIn = Conversion.getDateTimeOrNull(R["CheckIn"]);
                        record.ChildCount = Conversion.getInt32OrNull(R["ChildCount"]);
                        record.ContractPrice = Conversion.getDecimalOrNull(R["ContractPrice"]);
                        record.EBed1Price = Conversion.getDecimalOrNull(R["EBed1Price"]);
                        record.HotCat = Conversion.getStrOrNull(R["HotCat"]);
                        record.HotelID = Conversion.getInt32OrNull(R["HotelID"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        record.HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]);
                        record.HotLocation = Conversion.getInt32OrNull(R["HotLocation"]);
                        record.HotLocationName = Conversion.getStrOrNull(R["HotLocationName"]);
                        record.HotLocationNameL = Conversion.getStrOrNull(R["HotLocationNameL"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Night = Conversion.getInt16OrNull(R["Night"]);
                        record.OrgCur = Conversion.getStrOrNull(R["OrgCur"]);
                        record.PartNo = Conversion.getInt32OrNull(R["PartNo"]);
                        record.PriceID = Conversion.getInt32OrNull(R["PriceID"]);
                        record.RefNo = i;
                        record.RoomID = Conversion.getInt32OrNull(R["RoomID"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        record.RoomNameL = Conversion.getStrOrNull(R["RoomNameL"]);
                        record.RoomNr = Conversion.getInt32OrNull(R["SearchNr"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.StdAdult = Conversion.getInt32OrNull(R["StdAdult"]);
                        record.StopSaleGuar = Conversion.getInt16OrNull(R["StopSaleGuar"]);
                        record.StopSaleStd = Conversion.getInt16OrNull(R["StopSaleStd"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }


        }

        public List<SearchResultOH> getHotelSaleSearch(User UserData, MixSearchCriteria filter, Guid? logID, ref string errorMsg)
        {
            List<SearchResultOH> records = new List<SearchResultOH>();

            List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

            StringBuilder sq = new StringBuilder();
            string param = string.Empty;

            param += "<ROOT>";
            #region How many rooms searching
            foreach (MixSearchCriteriaRoom row in filter.RoomInfoList)
            {
                param += string.Format("<SEARCH RoomNr=\"{0}\" Adult=\"{1}\" Child=\"{2}\" ", row.RoomNr, row.AdultCount, row.ChildCount);
                if (row.ChildAge.Count > 0)
                    param += string.Format("ChdAge1=\"{0}\" ", row.ChildAge[0]);
                if (row.ChildAge.Count > 1)
                    param += string.Format("ChdAge2=\"{0}\" ", row.ChildAge[1]);
                if (row.ChildAge.Count > 2)
                    param += string.Format("ChdAge3=\"{0}\" ", row.ChildAge[2]);
                if (row.ChildAge.Count > 3)
                    param += string.Format("ChdAge4=\"{0}\" ", row.ChildAge[3]);
                param += "/>";
            }
            #endregion

            #region How many search location
            if (!string.IsNullOrEmpty(filter.Resort))
            {
                if (filter.Resort.Split('|').Length > 0)
                {
                    if (filter.Resort.Split('|').Length == 1)
                    {
                        param += string.Format("<LOCATIONS Location=\"{0}\" /> ", filter.Resort.Split('|')[0]);
                    }
                    else
                    {
                        foreach (string r in filter.Resort.Split('|'))
                        {
                            Location _location = locations.Find(f => f.RecID == Conversion.getInt32OrNull(r));
                            if (_location != null)
                            {
                                if (_location.Type > 2)
                                {
                                    //foreach (Location rL in locations.Where(w => w.Parent == _location.RecID))
                                    param += string.Format("<LOCATIONS Location=\"{0}\" /> ", _location.RecID);
                                }
                                else
                                    param += string.Format("<LOCATIONS Location=\"{0}\" /> ", r);
                            }
                        }
                    }
                }
            }
            else
                if (filter.ArrCity.HasValue)
            {
                param += string.Format("<LOCATIONS Location=\"{0}\" /> ", filter.ArrCity);
            }
            else if (filter.Country.HasValue)
            {
                IEnumerable<Location> clocations = locations.Where(w => w.Country == filter.Country.Value && w.Type == 2);
                foreach (var cl in clocations)
                {
                    param += string.Format("<LOCATIONS Location=\"{0}\" /> ", cl.RecID);
                }
            }
            #endregion

            #region How many search hotel
            if (!string.IsNullOrEmpty(filter.Hotel))
            {
                foreach (string r in filter.Hotel.Split('|'))
                    param += string.Format("<HOTELS Hotel=\"{0}\" />", r);
            }
            #endregion

            #region How many search hotel category
            if (!string.IsNullOrEmpty(filter.Category))
            {
                foreach (string r in filter.Category.Split('|'))
                    param += string.Format("<HOTCATS Category=\"{0}\" />", r);
            }
            #endregion

            #region How many search hotel room
            if (!string.IsNullOrEmpty(filter.Room))
            {
                foreach (string r in filter.Room.Split('|'))
                    param += string.Format("<ROOMS Room=\"{0}\" />", r);
            }
            #endregion

            #region How many search hotel roomview
            if (!string.IsNullOrEmpty(filter.RoomView))
            {
                foreach (string r in filter.RoomView.Split('|'))
                    param += string.Format("<ROOMVIEWS RoomView=\"{0}\" />", r);
            }
            #endregion

            #region How many search hotel board
            if (!string.IsNullOrEmpty(filter.Board))
            {
                foreach (string r in filter.Board.Split('|'))
                    param += string.Format("<BOARDS Board=\"{0}\" />", r);
            }
            #endregion
            param += "</ROOT>";

            XmlDocument paramsXml = new XmlDocument();
            paramsXml.LoadXml(param);
            //thsT6AWVSOgfzoNA
            sq.AppendFormat(
@"
Declare @pMarket varChar(10),@pAgency varChar(10)=@Agency,@pFirstCheckIn DateTime,@pLastCheckIn DateTime,@pFirstNight SmallInt,@pLastNight SmallInt,@pCur varChar(5),@pParams xml,@pRoomGroupSearch bit,@pBoardGroupSearch bit
Select @pMarket=@Market,@pFirstCheckIn=@FirstCheckIn,@pLastCheckIn=@LastCheckIn,@pFirstNight=@FirstNight,@pLastNight=@LastNight,@pCur=@Cur,@pParams=@Params,@pRoomGroupSearch=@RoomGroupSearch,@pBoardGroupSearch=@BoardGroupSearch
");
            sq.AppendLine(
@"
if @pAgency<>''
Exec dbo.usp_HotelSaleMultiSearch @pMarket,@pAgency,@pFirstCheckIn,@pLastCheckIn,@pFirstNight,@pLastNight,@pCur,@pParams,@pRoomGroupSearch,@pBoardGroupSearch
else 
Exec dbo.usp_HotelSaleMultiSearch @pMarket,null,@pFirstCheckIn,@pLastCheckIn,@pFirstNight,@pLastNight,@pCur,@pParams,@pRoomGroupSearch,@pBoardGroupSearch
");

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(sq.ToString());
            dbCommand.CommandTimeout = 120;
            try
            {
                int i = 0;
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                //if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel))
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                //else
                //    db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, "");
                db.AddInParameter(dbCommand, "FirstCheckIn", DbType.DateTime, filter.CheckIn);
                db.AddInParameter(dbCommand, "LastCheckIn", DbType.DateTime, filter.CheckIn.AddDays(filter.ExpandDay));
                db.AddInParameter(dbCommand, "FirstNight", DbType.Int16, filter.NightFrom);
                db.AddInParameter(dbCommand, "LastNight", DbType.Int16, filter.NightTo);
                db.AddInParameter(dbCommand, "Cur", DbType.AnsiString, !filter.CurControl || string.IsNullOrEmpty(filter.CurrentCur) ? null : filter.CurrentCur);
                db.AddInParameter(dbCommand, "Params", DbType.Xml, paramsXml.InnerXml);
                db.AddInParameter(dbCommand, "RoomGroupSearch", DbType.Boolean, filter.UseRoomGroup);
                db.AddInParameter(dbCommand, "BoardGroupSearch", DbType.Boolean, filter.UseBoardGroup);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        i++;
                        SearchResultOH record = new SearchResultOH();
                        record.AccomID = Conversion.getInt32OrNull(R["AccomID"]);
                        record.Accom = Conversion.getStrOrNull(R["Accom"]);
                        record.AccomName = Conversion.getStrOrNull(R["AccomName"]);
                        record.AccomNameL = Conversion.getStrOrNull(R["AccomNameL"]);
                        record.AdlPrice = Conversion.getDecimalOrNull(R["AdlPrice"]);
                        record.AdultCount = Conversion.getInt32OrNull(R["AdultCount"]);
                        record.Allotment = Conversion.getInt32OrNull(R["Allotment"]);
                        record.BoardID = Conversion.getInt32OrNull(R["BoardID"]);
                        record.Board = Conversion.getStrOrNull(R["Board"]);
                        record.BoardName = Conversion.getStrOrNull(R["BoardName"]);
                        record.BoardNameL = Conversion.getStrOrNull(R["BoardNameL"]);
                        record.CAdlPrice = Conversion.getDecimalOrNull(R["CAdlPrice"]);
                        record.CatName = Conversion.getStrOrNull(R["CatName"]);
                        record.CatNameL = Conversion.getStrOrNull(R["CatNameL"]);
                        record.CChdG1Price = Conversion.getDecimalOrNull(R["CChdG1Price"]);
                        record.CChdG2Price = Conversion.getDecimalOrNull(R["CChdG2Price"]);
                        record.CChdG3Price = Conversion.getDecimalOrNull(R["CChdG3Price"]);
                        record.CChdG4Price = Conversion.getDecimalOrNull(R["CChdG4Price"]);
                        record.CEBed1Price = Conversion.getDecimalOrNull(R["CEBed1Price"]);
                        record.ChdAge1 = Conversion.getInt32OrNull(R["ChdAge1"]);
                        record.ChdAge2 = Conversion.getInt32OrNull(R["ChdAge2"]);
                        record.ChdAge3 = Conversion.getInt32OrNull(R["ChdAge3"]);
                        record.ChdAge4 = Conversion.getInt32OrNull(R["ChdAge4"]);
                        record.ChdG1Age1 = Conversion.getDecimalOrNull(R["ChdG1Age1"]);
                        record.ChdG1Age2 = Conversion.getDecimalOrNull(R["ChdG1Age2"]);
                        record.ChdG1Price = Conversion.getDecimalOrNull(R["ChdG1Price"]);
                        record.ChdG2Age1 = Conversion.getDecimalOrNull(R["ChdG2Age1"]);
                        record.ChdG2Age2 = Conversion.getDecimalOrNull(R["ChdG2Age2"]);
                        record.ChdG2Price = Conversion.getDecimalOrNull(R["ChdG2Price"]);
                        record.ChdG3Age1 = Conversion.getDecimalOrNull(R["ChdG3Age1"]);
                        record.ChdG3Age2 = Conversion.getDecimalOrNull(R["ChdG3Age2"]);
                        record.ChdG3Price = Conversion.getDecimalOrNull(R["ChdG3Price"]);
                        record.ChdG4Age1 = Conversion.getDecimalOrNull(R["ChdG4Age1"]);
                        record.ChdG4Age2 = Conversion.getDecimalOrNull(R["ChdG4Age2"]);
                        record.ChdG4Price = Conversion.getDecimalOrNull(R["ChdG4Price"]);
                        record.CheckIn = Conversion.getDateTimeOrNull(R["CheckIn"]);
                        record.ChildCount = Conversion.getInt32OrNull(R["ChildCount"]);
                        record.ContractPrice = Conversion.getDecimalOrNull(R["ContractPrice"]);
                        record.EBed1Price = Conversion.getDecimalOrNull(R["EBed1Price"]);
                        record.HotCat = Conversion.getStrOrNull(R["HotCat"]);
                        record.HotelID = Conversion.getInt32OrNull(R["HotelID"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        record.HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]);
                        record.HotLocation = Conversion.getInt32OrNull(R["HotLocation"]);
                        record.HotLocationName = Conversion.getStrOrNull(R["HotLocationName"]);
                        record.HotLocationNameL = Conversion.getStrOrNull(R["HotLocationNameL"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Night = Conversion.getInt16OrNull(R["Night"]);
                        record.OrgCur = Conversion.getStrOrNull(R["OrgCur"]);
                        record.PartNo = Conversion.getInt32OrNull(R["PartNo"]);
                        record.PriceID = Conversion.getInt64OrNull(R["PriceID"]);
                        record.RefNo = i;
                        record.RoomID = Conversion.getInt32OrNull(R["RoomID"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        record.RoomNameL = Conversion.getStrOrNull(R["RoomNameL"]);
                        record.RoomNr = Conversion.getInt32OrNull(R["SearchNr"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.StdAdult = Conversion.getInt32OrNull(R["StdAdult"]);
                        record.StopSaleGuar = Conversion.getInt16OrNull(R["StopSaleGuar"]);
                        record.StopSaleStd = Conversion.getInt16OrNull(R["StopSaleStd"]);
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050022163"))
                        {
                            record.HotSPO = Conversion.getStrOrNull(R["HotSPO"]);
                        }
                        if (!filter.ShowStopSaleHotel || !(filter.ShowStopSaleHotel && record.StopSaleGuar == 2 && record.StopSaleStd == 2))
                            records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelGroup> getOnlyHotelPriceGroup(User UserData, SearchCriteria filter, List<SearchResultOH> priceData, bool useLocalName, bool showHotelImage, ref string errorMsg)
        {
            List<HotelMarOptRecord> hotelInfoUrlList = getHotelMarOptList(UserData, priceData, ref errorMsg);
            HotelImageUrl hotelImg = null;
            if (showHotelImage)
            {
                string hotelImagesUrl = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "HotelImagesUrl"));
                List<HotelImageUrl> hotelImgUrl = string.IsNullOrEmpty(hotelImagesUrl) ? new List<HotelImageUrl>() : Newtonsoft.Json.JsonConvert.DeserializeObject<List<HotelImageUrl>>(hotelImagesUrl);
                hotelImg = hotelImgUrl.Find(f => f.Market == UserData.Market);
            }
            List<HotelGroup> result = new List<HotelGroup>();
            var query = from q in priceData
                        group q by new { q.CheckIn, q.Night, q.Hotel } into k
                        select new { k.Key.CheckIn, k.Key.Night, k.Key.Hotel };
            int grpCount = 0;
            foreach (var row in query)
            {
                HotelGroup hGrp = new HotelGroup();
                List<SearchResultOH> groups = priceData.Where(w => w.CheckIn == row.CheckIn && w.Night == row.Night && w.Hotel == row.Hotel).ToList<SearchResultOH>();
                string imageUrl = hotelImg != null && !string.IsNullOrEmpty(hotelImg.Url) ? hotelImg + "/" + groups.FirstOrDefault().HotelID.ToString() + "/0_T.jpg" : string.Empty;

                HotelMarOptRecord hotelUrl = hotelInfoUrlList.Find(f => f.Hotel == row.Hotel);
                string _hotelUrl = (hotelUrl != null && !string.IsNullOrEmpty(hotelUrl.InfoWeb)) ? hotelUrl.InfoWeb : string.Empty;

                int grpCnt = (from q in groups
                              group q by new { q.RoomNr } into k
                              select new { k.Key.RoomNr }).Count();
                if (grpCnt > 0 && grpCnt == filter.RoomCount)
                {
                    SearchResultOH first = groups.FirstOrDefault();

                    grpCount++;
                    hGrp.HotelGroupID = grpCount;
                    hGrp.CheckIn = row.CheckIn.Value.ToShortDateString();
                    hGrp.CheckOut = row.CheckIn.Value.AddDays(row.Night.Value).ToShortDateString();
                    hGrp.Night = row.Night.Value.ToString();
                    hGrp.HotelName = useLocalName ? first.HotelNameL : first.HotelName;
                    hGrp.HotelLocationName = useLocalName ? first.HotLocationNameL : first.HotLocationName;
                    hGrp.showHotelImage = showHotelImage;
                    hGrp.hotelImageUrl = showHotelImage && !string.IsNullOrEmpty(imageUrl) ? imageUrl : string.Empty;
                    hGrp.hotelInfoUrl = _hotelUrl;
                    hGrp.Parameter1 = !((int)(grpCount / 2.0) == (grpCount / 2.0));
                    List<PriceDetail> pdetail = new List<PriceDetail>();
                    var q1 = from q in groups
                             group q by new { q.RoomNr } into k
                             select new { k.Key.RoomNr };
                    decimal selectPrice = (decimal)0;
                    foreach (var r1 in q1)
                    {
                        SearchCriteriaRooms room = filter.RoomsInfo.Find(f => f.RoomNr == r1.RoomNr);
                        PriceDetail pd = new PriceDetail();
                        pd.labelAccomName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom").ToString();
                        pd.labelBoardName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblBoardName").ToString();
                        pd.labelFreeRoom = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFreeRoom").ToString();
                        pd.labelPrice = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblPrice").ToString();
                        pd.labelRoomName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoomName").ToString();
                        pd.labelSelected = string.Empty;
                        pd.PriceDetailID = grpCount;
                        if (room != null)
                        {
                            pd.AccomInfo = room.Adult.ToString() + "x" + (string.IsNullOrEmpty(UserData.TvParams.TvParamPrice.PLAdlCap) ? "Adl" : UserData.TvParams.TvParamPrice.PLAdlCap);
                            if (room.Child > 0)
                                pd.AccomInfo += room.Child.ToString() + "x" + (string.IsNullOrEmpty(UserData.TvParams.TvParamPrice.PLChdCap) ? "Chd" : UserData.TvParams.TvParamPrice.PLChdCap);
                        }
                        List<AccomPrice> ap = new List<AccomPrice>();
                        List<SearchResultOH> apList = groups.Where(w => w.RoomNr == r1.RoomNr.Value).ToList<SearchResultOH>();
                        foreach (SearchResultOH accomP in apList)
                        {
                            Int16 StopSaleValue = 0;
                            string stopSaleMessage = string.Empty;
                            AccomPrice p = new AccomPrice();
                            p.RefNo = accomP.RefNo;
                            p.RoomNr = accomP.RoomNr.HasValue ? accomP.RoomNr.Value : 1;
                            p.AccomName = useLocalName ? accomP.AccomNameL : accomP.AccomName;
                            p.BoardName = useLocalName ? accomP.BoardNameL : accomP.BoardName;
                            p.FreeRoom = accomP.Allotment.HasValue ? (accomP.Allotment.Value > 3 ? "3+" : (accomP.Allotment.Value < 0 ? "0" : accomP.Allotment.Value.ToString())) : "";
                            p.PriceID = accomP.PriceID.HasValue ? accomP.PriceID.Value : -1;
                            p.RoomName = useLocalName ? accomP.RoomNameL : accomP.RoomName;
                            p.SaleCur = accomP.SaleCur;
                            p.SalePrice = accomP.SalePrice.HasValue ? accomP.SalePrice.Value.ToString("#.00") : "";
                            p.PriceNoComma = accomP.SalePrice.HasValue ? accomP.SalePrice.Value.ToString("#.00") : "";
                            p.Price = accomP.SalePrice;
                            if ((accomP.StopSaleGuar == 2 && accomP.StopSaleStd == 2) /*&& (accomP.Allotment > 0 || (accomP.AutoStop.HasValue && !row.AutoStop.Value))*/)
                            {
                                StopSaleValue = 2;
                            }
                            else
                                if (accomP.StopSaleGuar == 1 && accomP.StopSaleStd == 1)
                            {
                                string msg = new Reservation().getStopSaleMessage(UserData.Market, accomP.Hotel, accomP.Room, accomP.Accom, accomP.Board, null, UserData.AgencyID, accomP.CheckIn, accomP.Night, true, ref errorMsg);
                                stopSaleMessage = string.Format(HttpContext.GetGlobalResourceObject("PackageSearchResult", "StopSaleWarningMessage").ToString(),
                                                                    string.IsNullOrEmpty(msg) ? "" : msg + ", ");
                                StopSaleValue = 1;
                            }
                            else
                                p.StopSale = StopSaleValue.ToString();
                            p.StopSaleMessage = stopSaleMessage;
                            p.Discount = accomP.SalePrice < accomP.ContractPrice;
                            ap.Add(p);
                        }
                        List<AccomPrice> pList = ap.OrderBy(o => o.Price).ToList<AccomPrice>();
                        pList[0].Selected = true;
                        pd.accomPrice = pList;
                        pdetail.Add(pd);
                    }
                    bool pDetailPriceValid = pdetail.Where(w => (w.accomPrice.Where(w1 => w1.Selected == true && w1.Price.HasValue).Count() > 0)).Count() > 0;
                    selectPrice += pDetailPriceValid ? pdetail.Sum(s => s.accomPrice.Where(w => w.Selected == true && w.Price.HasValue).First().Price.Value) : 0;
                    hGrp.SelectedPrice = selectPrice.ToString("#,###.00");
                    hGrp.priceDetail = pdetail;
                    hGrp.SelectedPriceCur = hGrp.priceDetail.First().accomPrice.First().SaleCur;
                    hGrp.SortingID = -1;
                    result.Add(hGrp);
                }
            }
            return result;
        }

        public List<HotelGroup> getHotelSalePriceGroup(User UserData, MixSearchCriteria filter, List<SearchResultOH> priceData, bool useLocalName, bool showHotelImage, ref string errorMsg)
        {
            List<HotelMarOptRecord> hotelInfoUrlList = getHotelMarOptList(UserData, priceData, ref errorMsg);
            HotelImageUrl hotelImg = null;
            if (showHotelImage)
            {
                string hotelImagesUrl = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "HotelImagesUrl"));
                List<HotelImageUrl> hotelImgUrl = string.IsNullOrEmpty(hotelImagesUrl) ? new List<HotelImageUrl>() : Newtonsoft.Json.JsonConvert.DeserializeObject<List<HotelImageUrl>>(hotelImagesUrl);
                hotelImg = hotelImgUrl.Find(f => f.Market == UserData.Market);
            }
            List<HotelGroup> result = new List<HotelGroup>();
            var query = from q in priceData
                        group q by new { q.CheckIn, q.Night, q.Hotel, q.OrderHotel } into k
                        select new { k.Key.CheckIn, k.Key.Night, k.Key.Hotel, k.Key.OrderHotel };
            int grpCount = 0;
            foreach (var row in query)
            {
                HotelGroup hGrp = new HotelGroup();
                List<SearchResultOH> groups = priceData.Where(w => w.CheckIn == row.CheckIn && w.Night == row.Night && w.Hotel == row.Hotel).ToList<SearchResultOH>();
                string imageUrl = hotelImg != null && !string.IsNullOrEmpty(hotelImg.Url) ? hotelImg + "/" + groups.FirstOrDefault().HotelID.ToString() + "/0_T.jpg" : string.Empty;

                HotelMarOptRecord hotelUrl = hotelInfoUrlList.Find(f => f.Hotel == row.Hotel);
                string _hotelUrl = (hotelUrl != null && !string.IsNullOrEmpty(hotelUrl.InfoWeb)) ? hotelUrl.InfoWeb : string.Empty;

                int grpCnt = (from q in groups
                              group q by new { q.RoomNr } into k
                              select new { k.Key.RoomNr }).Count();
                if (grpCnt > 0 && grpCnt == filter.SearchRoomCount)
                {
                    SearchResultOH first = groups.FirstOrDefault();

                    grpCount++;
                    hGrp.HotelGroupID = grpCount;
                    hGrp.CheckIn = row.CheckIn.Value.ToShortDateString();
                    hGrp.CheckOut = row.CheckIn.Value.AddDays(row.Night.Value).ToShortDateString();
                    hGrp.Night = row.Night.Value.ToString();
                    hGrp.HotelName = useLocalName ? first.HotelNameL : first.HotelName;
                    hGrp.HotelCatName = useLocalName ? first.CatNameL : first.CatName;
                    hGrp.HotelLocationName = useLocalName ? first.HotLocationNameL : first.HotLocationName;
                    hGrp.showHotelImage = showHotelImage;
                    hGrp.hotelImageUrl = showHotelImage && !string.IsNullOrEmpty(imageUrl) ? imageUrl : string.Empty;
                    hGrp.hotelInfoUrl = _hotelUrl;
                    hGrp.Parameter1 = grpCount % 2 != 0;
                    hGrp.OrderHotel = first.OrderHotel;
                    List<PriceDetail> pdetail = new List<PriceDetail>();
                    var q1 = from q in groups
                             group q by new { q.RoomNr } into k
                             select new { k.Key.RoomNr };
                    decimal selectPrice = (decimal)0;
                    foreach (var r1 in q1)
                    {
                        #region Room Groups
                        MixSearchCriteriaRoom room = filter.RoomInfoList.Find(f => f.RoomNr == r1.RoomNr);
                        PriceDetail pd = new PriceDetail();
                        pd.labelAccomName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom").ToString();
                        pd.labelBoardName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblBoardName").ToString();
                        pd.labelFreeRoom = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFreeRoom").ToString();
                        pd.labelPrice = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblPrice").ToString();
                        pd.labelRoomName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoomName").ToString();
                        pd.labelSelected = string.Empty;
                        pd.labelOffer = string.Empty;
                        pd.PriceDetailID = grpCount;
                        if (room != null)
                        {
                            pd.AccomInfo = room.AdultCount.ToString() + "x" + (string.IsNullOrEmpty(UserData.TvParams.TvParamPrice.PLAdlCap) ? "Adl" : UserData.TvParams.TvParamPrice.PLAdlCap);
                            if (room.ChildCount > 0)
                                pd.AccomInfo += room.ChildCount.ToString() + "x" + (string.IsNullOrEmpty(UserData.TvParams.TvParamPrice.PLChdCap) ? "Chd" : UserData.TvParams.TvParamPrice.PLChdCap);
                        }
                        List<AccomPrice> ap = new List<AccomPrice>();
                        List<SearchResultOH> apList = groups.Where(w => w.RoomNr == r1.RoomNr.Value).ToList<SearchResultOH>();
                        int pOE = 0;
                        foreach (SearchResultOH accomP in apList)
                        {
                            pOE++;
                            Int16 StopSaleValue = 0;
                            string stopSaleMessage = string.Empty;
                            AccomPrice p = new AccomPrice();
                            p.RefNo = accomP.RefNo;
                            p.RoomNr = accomP.RoomNr.HasValue ? accomP.RoomNr.Value : 1;
                            p.AccomName = useLocalName ? accomP.AccomNameL : accomP.AccomName;
                            p.BoardName = useLocalName ? accomP.BoardNameL : accomP.BoardName;
                            p.FreeRoom = accomP.Allotment.HasValue ? (accomP.Allotment.Value > 3 ? "3+" : (accomP.Allotment.Value < 0 ? "0" : accomP.Allotment.Value.ToString())) : "";
                            p.PriceID = accomP.PriceID.HasValue ? accomP.PriceID.Value : -1;
                            p.RoomName = useLocalName ? accomP.RoomNameL : accomP.RoomName;
                            p.SaleCur = accomP.SaleCur;
                            p.SalePrice = accomP.SalePrice.HasValue ? accomP.SalePrice.Value.ToString("#.00") : "";
                            p.PriceNoComma = accomP.SalePrice.HasValue ? (accomP.SalePrice.Value * 100).ToString("#.") : "";
                            p.Price = accomP.SalePrice;
                            if ((accomP.StopSaleGuar == 2 && accomP.StopSaleStd == 2))
                            {
                                StopSaleValue = 2;
                            }
                            else if (accomP.StopSaleGuar == 1 && accomP.StopSaleStd == 1)
                            {
                                string msg = new Reservation().getStopSaleMessage(UserData.Market, accomP.Hotel, accomP.Room, accomP.Accom, accomP.Board, null, UserData.AgencyID, accomP.CheckIn, accomP.Night, true, ref errorMsg);
                                stopSaleMessage = string.Format(HttpContext.GetGlobalResourceObject("PackageSearchResult", "StopSaleWarningMessage").ToString(),
                                                                    string.IsNullOrEmpty(msg) ? "" : msg + ", ");
                                StopSaleValue = 1;
                            }
                            p.StopSale = StopSaleValue.ToString();
                            p.showStopSale = StopSaleValue > 0;
                            p.StopSaleMessage = stopSaleMessage;
                            p.Discount = accomP.SalePrice < accomP.ContractPrice;
                            string discountMsg = string.Empty;
                            /*
                            S: Price SPO
                            E: EarlyBooking
                            T : Turbo Early Booking
                            D : Long Stay
                            L: Long Stay
                            U: Upgrade Aksiyonu
                            */
                            switch (accomP.HotSPO)
                            {
                                case "S":
                                    discountMsg = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "HotelSaleDiscountMsgS").ToString();
                                    break;
                                case "E":
                                    discountMsg = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "HotelSaleDiscountMsgE").ToString();
                                    break;
                                case "T":
                                    discountMsg = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "HotelSaleDiscountMsgT").ToString();
                                    break;
                                case "D":
                                    discountMsg = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "HotelSaleDiscountMsgD").ToString();
                                    break;
                                case "L":
                                    discountMsg = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "HotelSaleDiscountMsgL").ToString();
                                    break;
                                case "U":
                                    discountMsg = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "HotelSaleDiscountMsgU").ToString();
                                    break;
                                default:
                                    discountMsg = string.Empty;
                                    break;
                            }
                            p.discountMsg = discountMsg;
                            p.priceOddEven = pOE % 2 != 0;
                            ap.Add(p);
                        }
                        List<AccomPrice> pList = ap.OrderBy(o => o.Price).ToList<AccomPrice>();
                        pList[0].Selected = true;
                        pd.accomPrice = pList;
                        pdetail.Add(pd);
                        #endregion Room Groups
                    }
                    bool pDetailPriceValid = pdetail.Where(w => (w.accomPrice.Where(w1 => w1.Selected == true && w1.Price.HasValue).Count() > 0)).Count() > 0;
                    selectPrice += pDetailPriceValid ? pdetail.Sum(s => s.accomPrice.Where(w => w.Selected == true && w.Price.HasValue).First().Price.Value) : 0;
                    hGrp.MinPrice = selectPrice;
                    hGrp.SelectedPrice = selectPrice.ToString("#,###.00");
                    hGrp.priceDetail = pdetail;
                    hGrp.SelectedPriceCur = hGrp.priceDetail.First().accomPrice.First().SaleCur;
                    hGrp.SortingID = -1;
                    hGrp.buttonStopSale = pdetail.Where(w => (w.accomPrice.Where(w1 => w1.Selected == true && w1.Price.HasValue && w1.showStopSale).Count() > 0)).Count() > 0;
                    result.Add(hGrp);
                }
            }
            return result;
        }
    }

    public class LongSearch
    {
        public string getBaseQueryForHotelPackage(User UserData, SearchCriteria filter, ref string errorMsg)
        {
            string resortWhereStr = string.Empty;
            string categoryWhereStr = string.Empty;
            string hotelWhereStr = string.Empty;
            string roomWhereStr = string.Empty;
            string boardWhereStr = string.Empty;

            if (!string.IsNullOrEmpty(filter.Resort))
            {
                foreach (string r in filter.Resort.Split('|'))
                    resortWhereStr += string.Format("INSERT INTO #tmpResort(RecID) VALUES({0}) \n", r);
                if (resortWhereStr.Length > 0)
                    resortWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpResort') is not null Drop Table dbo.#tmpResort 
Create Table #tmpResort (RecID int) " + resortWhereStr;
                else
                    resortWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Category))
            {
                foreach (string r in filter.Category.Split('|'))
                    categoryWhereStr += string.Format("INSERT INTO @Category(Code) VALUES('{0}') \n", r);
                if (categoryWhereStr.Length > 0)
                    categoryWhereStr = "DECLARE @Category TABLE (Code VarChar(10)) \n" + categoryWhereStr;
                else
                    categoryWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Hotel))
            {
                foreach (string r in filter.Hotel.Split('|'))
                    hotelWhereStr += string.Format("INSERT INTO @Hotel(Code) VALUES('{0}') \n", r);
                if (hotelWhereStr.Length > 0)
                    hotelWhereStr = "DECLARE @Hotel TABLE (Code VarChar(10)) \n" + hotelWhereStr;
                else
                    hotelWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Room))
            {
                foreach (string r in filter.Room.Split('|'))
                    roomWhereStr += string.Format("INSERT INTO @Room(Code) VALUES('{0}') \n", r);
                if (roomWhereStr.Length > 0)
                    roomWhereStr = "DECLARE @Room TABLE (Code VarChar(10)) \n" + roomWhereStr;
                else
                    roomWhereStr = string.Empty;
            }

            if (!string.IsNullOrEmpty(filter.Board))
            {
                foreach (string r in filter.Board.Split('|'))
                    boardWhereStr += string.Format("INSERT INTO #tmpBoard(Code) VALUES('{0}') \n", r);
                if (boardWhereStr.Length > 0)
                    boardWhereStr = @"
if OBJECT_ID('TempDB.dbo.#tmpBoard') is not null Drop Table dbo.#tmpBoard 
Create Table #tmpBoard (Code VarChar(10) COLLATE Latin1_General_CI_AS) " + boardWhereStr;
                else
                    boardWhereStr = string.Empty;
            }

            StringBuilder q = new StringBuilder();
            q.Append("Set DateFormat DMY \n");
            q.Append("Declare @CustomRegId VarChar(20) \n");
            q.Append("Select @CustomRegID = CustomRegID From ParamSystem (NOLOCK) Where isnull(Market, '')='' \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#PriceList') is not null Drop Table dbo.#PriceList \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#Result') is not null Drop Table dbo.#Result \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#GrpEB') is not null Drop Table dbo.#GrpEB \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#PasEBPer') is not null Drop Table dbo.#PasEBPer \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#PasEBHot') is not null Drop Table dbo.#PasEBHot \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#EB') is not null Drop Table dbo.#EB \n");
            q.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location \n");
            q.Append("Select RecID, Country, City, Town, Village, [Name]=isnull(dbo.FindLocalName(NameLID, @Market), Name) \n");
            q.Append("INTO #Location \n");
            q.Append("From Location (NOLOCK) \n");
            q.Append("Select *, EBPerc=0, EBValidDate=Cast(73048 AS DateTime) \n");
            q.Append("Into #PriceList \n");
            q.Append("From PriceListExtV P \n");
            q.Append("Join #Location L On L.RecID = P.HotLocation \n");
            q.Append("Where \n");
            q.Append("     P.Ready = 'Y' And P.CWebPub = 'Y' AND P.WebPubDate <= GetDate() And P.HotStatus = 1 \n");
            q.Append(" And dbo.DateOnly(GetDate()) between SaleBegDate And SaleEndDate \n");
            q.Append(" And P.CheckIn between @BegDate And @EndDate \n");

            if (filter.Country.HasValue)
                q.AppendFormat(" And L.Country={0} \n", filter.Country.Value);

            if (filter.DepCity.HasValue)
                q.AppendFormat(" And P.DepCity={0} \n", filter.DepCity.Value);

            if (filter.ArrCity.HasValue)
                q.AppendFormat(" And P.ArrCity={0} \n", filter.ArrCity.Value);

            if (!string.IsNullOrEmpty(hotelWhereStr))
            {
                q.Insert(0, hotelWhereStr);
                q.Append(" And Exists(Select Code From @Hotel Where Code=H.Code) \n");
            }

            if (!string.IsNullOrEmpty(filter.Package))
                q.AppendFormat("  And CP.HolPack = '{0}' \n", filter.Package);

            if (!string.IsNullOrEmpty(categoryWhereStr))
            {
                q.Insert(0, categoryWhereStr);
                q.Append("  And Exists(Select Code From @Category Where Code=H.Category)");
            }

            if (!string.IsNullOrEmpty(resortWhereStr))
            {
                q.Insert(0, resortWhereStr);
                q.Append("  And Exists(Select R.RecID From #tmpResort R Where Exists(Select RecID From Location L (NOLOCK) Where L.Town=R.RecID AND L.RecID=H.Location)) \n");
            }

            if (!string.IsNullOrEmpty(roomWhereStr))
            {
                q.Insert(0, roomWhereStr);
                if (filter.UseGroup)
                    q.Append("  And Exists(Select Room From GrpRoomDet GRD (NOLOCK) Where Exists(Select Code From @Room Where Code=GRD.Groups) And Room=PA.Room) \n");
                else
                    q.Append("  And (Select Code From @Room Where Code=PA.Room) \n");
            }

            if (!string.IsNullOrEmpty(boardWhereStr))
            {
                q.Insert(0, boardWhereStr);
                if (filter.UseGroup)
                    q.Append("  And Exists(Select Board From GrpBoardDet GBD (NOLOCK) Where Exists(Select Code From @Board Where Code=GBD.Groups) And Board=PA.Board) \n");
                else
                    q.Append("  And (Select Code From @Board Where Code=PA.Board) \n");
            }

            if (filter.NightFrom >= filter.NightTo)
                q.Append("  And PP.Night = @Night1 \n");
            else
                q.Append("  And PP.Night between @Night1 and @Night2 \n");

            q.AppendFormat(" And P.HAdult={0} \n", 2);
            q.AppendFormat(" And P.HChdAgeG1+P.HChdAgeG2+P.HChdAgeG3+P.HChdAgeG4={0} \n", 0);
            q.Append(" And Exists(Select * From CatPackMarket Where CatPackID=P.CatPackID and Market=@Market and (Operator=@Operator or Operator='')) \n");
            q.Append(" And Exists(Select * From CatalogPack (NOLOCK) Where RecID = P.CatPackID And (PackType = 'H' or PackType = 'C' or PackType = 'U')) \n");
            q.Append(" And (not Exists(Select * from CatPackAgency Where CatPackID=P.CatPackID) or \n");
            q.Append("          Exists(Select * from CatPackAgency Where CatPackID=P.CatPackID and Agency=@Agency)) \n");
            q.Append(" Select Distinct Hotel, Room, Board, PasEBValid, HolPack, CheckIn, Night, Operator, Market \n");
            q.Append(" Into #GrpEB \n");
            q.Append(" From #PriceList \n");
            q.Append(" Select *, EBPer = Case When PasEBValid='Y' then (Select Top 1 Cast(Cast(FLOOR(isnull(PasEBPer,0)*100) as int) as varchar(5))+ ';' + Convert(varchar(10),SaleEndDate,103) \n");
            q.Append("                                                  From PasEB EB With (NOLOCK) \n");
            q.Append("                                                  Join PasEBPer EBP (NOLOCK) on EBP.PasEBID=EB.RecID \n");
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0034, VersionControl.Equality.gt))
                q.Append("                                                  Left Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=X.Hotel and (EBH.Room='' or EBH.Room=X.Room) and (EBH.Board='' or EBH.Board=X.Board) \n");
            else
                q.Append("                                                  Left Join PasEBHot EBH (NOLOCK) on EBH.PasEBID=EB.RecID and EBH.Hotel=X.Hotel and (EBH.Room='' or EBH.Room=X.Room) \n");
            q.Append("                                                  Left Join GrpHolPackDet PG on PG.Groups = EB.HolPackGrp and PG.GrpType='P' \n");
            q.Append("                                                  Where Operator=@Operator and Market=@Market And OwnerOperator = X.Operator And OwnerMarket=X.Market \n");
            q.Append("                                                    and X.PasEBValid = 'Y' \n");
            q.Append("                                                    and (   not Exists(Select * from PasEBHot Where PasEBID=EB.RecID) \n");
            q.Append("                                                            or (Exists(Select * from PasEBHot Where PasEBHot.PasEBID=EB.RecID and PasEBHot.Hotel=X.Hotel)) \n");
            q.Append("                                                         ) \n");
            q.Append("                                                    and ( (EB.HolPack=X.HolPack or PG.HolPack=X.HolPack) or (EB.HolPack='' and PG.HolPack is Null) ) \n");
            q.Append("                                                    and dbo.DateOnly(GetDate()) between SaleBegDate and SaleEndDate and X.CheckIn between ResBegDate and ResEndDate \n");
            q.Append("                                                    and (X.Night between IsNull(MinDay,0) and IsNull(MaxDay,999)) \n");
            q.Append("                                                    and SubString(DoW,DATEPART(dw,X.CheckIn),1)=1 \n");
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040014038"))
                q.Append("              and DateDIFF(day,dbo.DateOnly(GetDate()),X.CheckIn) between IsNull(EBP.RemFromDay,0) and IsNull(EBP.RemToDay,999) \n");
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0034, VersionControl.Equality.gt))
                q.Append("                                                  Order by EBH.Hotel Desc,EBH.Room Desc,EB.HolPack Desc,HolPackGrp Desc, EB.CrtDate Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc) \n");
            else
                q.Append("                                                  Order by EBH.Hotel Desc,EBH.Room Desc,EBH.Board Desc,EB.HolPack Desc,HolPackGrp Desc, EB.CrtDate Desc, SaleBegDate Desc, ResBegDate Desc, EBP.RecID Desc) \n");
            q.Append("           Else '0;' + Convert(varchar(10),Cast(73048 AS DateTime),103) End \n");
            q.Append(" INTO #EB \n");
            q.Append(" From #GrpEB X \n");
            q.Append(" Update x \n");
            q.Append(" Set EBPerc=case when y.EBPer is null then 0 \n");
            q.Append("                 else cast(substring(y.EBPer, 1, CharIndex(';',y.EBPer,1)-1) as Int)/100 \n");
            q.Append("    end, \n");
            q.Append("    EBValidDate =	case when y.EBPer is null then Cast(73048 AS DateTime) \n");
            q.Append("                  else substring(y.EBPer, CharIndex(';',y.EBPer,1)+1,10) \n");
            q.Append("                     end \n");
            q.Append(" From #PriceList X ,#EB Y \n");
            q.Append(" Where X.Hotel=Y.Hotel And X.PasEBValid=Y.PasEBValid And X.Holpack=Y.Holpack And X.CheckIn=Y.CheckIn And X.Night=Y.Night \n");
            q.Append(" Select *, \n");
            q.Append("     EBAmountPL = CASE WHEN  EBPerc > 0 THEN \n");
            q.Append("                     (Case When @Cur <> SaleCur then dbo.ufn_Exchange(dbo.DateOnly(GetDate()), SaleCur, isnull(@Cur, SaleCur), isnull(Case When Pl.PasEBValid='Y' then dbo.ufn_GetEBAmountPL(Pl.CatPackID, Pl.ARecNo, Pl.PRecNo, Pl.HAPRecID, Pl.Adl, Pl.ChdG1Age1, Pl.ChdG1Age2, Pl.ChdG2Age1, Pl.ChdG2Age2, Pl.ChdG3Age1, Pl.ChdG3Age2, Pl.ChdG4Age1, Pl.ChdG4Age2, Pl.HotelChdRecNo, Pl.ChdCalcSale, Pl.HAdult, Pl.HChdAgeG1, Pl.HChdAgeG2, Pl.HChdAgeG3, Pl.HChdAgeG4, 0, @CustomRegID, Pl.HotChdPrice) Else 0 End, 0), 1, Market) \n");
            q.Append("                          Else isnull(Case When Pl.PasEBValid='Y' then dbo.ufn_GetEBAmountPL(Pl.CatPackID, Pl.ARecNo, Pl.PRecNo, Pl.HAPRecID, Pl.Adl, Pl.ChdG1Age1, Pl.ChdG1Age2, Pl.ChdG2Age1, Pl.ChdG2Age2, Pl.ChdG3Age1, Pl.ChdG3Age2, Pl.ChdG4Age1, Pl.ChdG4Age2, Pl.HotelChdRecNo, Pl.ChdCalcSale, Pl.HAdult, Pl.HChdAgeG1, Pl.HChdAgeG2, Pl.HChdAgeG3, Pl.HChdAgeG4, 0, @CustomRegID, Pl.HotChdPrice) Else 0 End, 0) End) \n");
            q.Append("                  ELSE 0.0 END, \n");
            q.Append("     ExcSalePrice = (Case When @Cur <> SaleCur then dbo.ufn_Exchange(dbo.DateOnly(GetDate()), SaleCur, isnull(@Cur, SaleCur), isnull(Pl.CalcSalePrice, 0), 1, Market) Else isnull(Pl.CalcSalePrice, 0) End) \n");
            q.Append(" Into #Result \n");
            q.Append(" From #PriceList Pl \n");


            return q.ToString();
            ;
        }

        public bool? getTotalMinMaxPrice(User UserData, SearchCriteria filter, ref string errorMsg)
        {
            string tsql = string.Empty;

            int searchNight1;
            int searchNight2;
            searchNight1 = filter.NightFrom;
            searchNight2 = filter.NightTo;
            tsql = getBaseQueryForHotelPackage(UserData, filter, ref errorMsg);
            tsql += " Select DepCity, DepCityName, ArrCity, ArrCityName, SaleCur";
            for (int i = searchNight1; i <= searchNight2; i++)
            {
                tsql += ", [" + i.ToString() + "]";
            }
            tsql += @"  From 
                        (   Select P.Night, P.DepCity, DepCityName=L1.Name, P.ArrCity, ArrCityName=L2.Name, SaleCur=@Cur, 
                                PriceFinall = ExcSalePrice - (EBAmountPL * EBPerc / 100)
                            From #Result P
                            Join #Location L On L.RecID = P.HotLocation
                            Join #Location L1 On L1.RecID = P.DepCity        
                            Join #Location L2 On L2.RecID = P.ArrCity
                            Where   (@RoomPrice1 is null or @RoomPrice1 <= (ExcSalePrice - (EBAmountPL * EBPerc / 100))) 
                                And (@RoomPrice2 is null or @RoomPrice2 >= (ExcSalePrice - (EBAmountPL * EBPerc / 100)))
                        ) As PTable PIVOT (    
					    Min(PriceFinall)    
					    FOR Night in  ( ";
            string days = string.Empty;
            for (int i = filter.NightFrom; i <= filter.NightTo; i++)
            {
                days += ",[" + i.ToString() + "]";
            }
            days = days.Remove(0, 1);
            tsql += days;
            tsql += @")) As Pvt 
                       Order By DepCityName, ArrCityName, SaleCur ";
            List<longTotalMinMaxPrice> result = new List<longTotalMinMaxPrice>();
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "BegDate", DbType.Date, filter.CheckIn);
                db.AddInParameter(dbCommand, "EndDate", DbType.Date, filter.CheckIn.AddDays(filter.ExpandDate));
                db.AddInParameter(dbCommand, "Cur", DbType.String, filter.CurrentCur);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        longTotalMinMaxPrice r = new longTotalMinMaxPrice();
                        r.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        r.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        r.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        r.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        r.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        result.Add(r);
                    }
                }
                return null;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }

    public class PytonG7Ent
    {
        public SearchResult getB2CToB2BPriceSearch(User userData, DateTime checkIn, Int16 night, string holPack, string hotel, string room, string board, string accom, ref string errorMsg)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Select CatPackID,ARecNo,PRecNo,CheckIn,Night,DepCity,ArrCity,SaleCur,Operator,Market,FlightClass,PackType ");
            sb.AppendLine("From PriceListV ");
            sb.AppendLine("Where Ready='Y' ");
            sb.AppendLine("  And CWebPub='Y' ");
            sb.AppendLine("  And GetDate()>=WebPubDate ");
            sb.AppendLine("  And @CheckIn between CPBegDate and CPEndDate ");
            sb.AppendLine("  And dbo.DateOnly(GetDate()) between SaleBegDate And SaleEndDate ");
            sb.AppendLine("  And HolPack=@holPack ");
            sb.AppendLine("  And Hotel=@hotel ");
            sb.AppendLine("  And Room=@room ");
            sb.AppendLine("  And (@accom is null or Accom=@accom) ");
            sb.AppendLine("  And (@board is null or Board=@board) ");
            sb.AppendLine("  And Night=@night ");
            sb.AppendLine("  And CheckIn=@checkIn ");
            sb.AppendLine("  And Exists(Select * From CatPackMarket Where CatPackID=PriceListV.CatPackID and Market=@Market and (Operator=@Operator or Operator='')) ");
            sb.AppendLine("  And isnull(PLFreeAmount,32000)>0 ");
            sb.AppendLine("  And Exists(Select Distinct RecID From	( ");
            sb.AppendLine("  	Select CP.RecID ");
            sb.AppendLine("  	From CatalogPack CP (NOLOCK) ");
            sb.AppendLine("    	Where ");
            sb.AppendLine("    		Not Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID) ");
            sb.AppendLine("    	AND CP.RecID = PriceListV.CatPackID ");
            sb.AppendLine("    	union all ");
            sb.AppendLine("    	Select CP.RecID ");
            sb.AppendLine("    	From CatalogPack CP (NOLOCK) ");
            sb.AppendLine("    	Where ");
            sb.AppendLine("    		Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=@Agency) ");
            sb.AppendLine("    	AND CP.RecID = PriceListV.CatPackID ");
            sb.AppendLine("    	union all ");
            sb.AppendLine("    	Select CP.RecID ");
            sb.AppendLine("    	From CatalogPack CP (NOLOCK) ");
            sb.AppendLine("    	Where ");
            sb.AppendLine("    		Exists(Select RecID From CatPackAgency Where CatPackID=CP.RecID and Agency=(Select MainOffice From Agency (NOLOCK) Where Code=@Agency)) ");
            sb.AppendLine("    	AND CP.RecID=PriceListV.CatPackID  ");
            sb.AppendLine("    ) CPA ");
            sb.AppendLine("  )");
            sb.AppendLine("Order By CatPackID Desc ");

            string dateFormat = strFunc.Trim(userData.Ci.DateTimeFormat.ShortDatePattern, ' ').ToString().Replace("dd", "d").Replace("yy", "y").Replace("MM", "m").Replace(userData.Ci.DateTimeFormat.DateSeparator, "");
            string dateSeperator = userData.Ci.DateTimeFormat.DateSeparator[0].ToString();

            List<SearchResult> result = new List<SearchResult>();
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sb.ToString());

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, userData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, userData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, userData.AgencyID);
                db.AddInParameter(dbCommand, "checkIn", DbType.DateTime, checkIn);
                db.AddInParameter(dbCommand, "night", DbType.Int16, night);
                db.AddInParameter(dbCommand, "holPack", DbType.AnsiString, holPack);
                db.AddInParameter(dbCommand, "hotel", DbType.AnsiString, hotel);
                db.AddInParameter(dbCommand, "room", DbType.AnsiString, room);
                db.AddInParameter(dbCommand, "accom", DbType.AnsiString, !String.IsNullOrEmpty(accom) ? accom : Convert.DBNull);
                db.AddInParameter(dbCommand, "board", DbType.AnsiString, !String.IsNullOrEmpty(board) ? board : Convert.DBNull);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        //CatPackID,ARecNo,PRecNo,DepCity,ArrCity,SaleCur,Operator,Market,FlightClass
                        SearchResult r = new SearchResult();
                        r.RefNo = 1;
                        r.RoomNr = 1;
                        r.Calculated = false;
                        r.CatPackID = Conversion.getInt32OrNull(oReader["CatPackID"]);
                        r.ARecNo = Conversion.getInt32OrNull(oReader["ARecNo"]);
                        r.PRecNo = Conversion.getInt32OrNull(oReader["PRecNo"]);
                        r.Night = (Int16)oReader["Night"];
                        r.CheckIn = (DateTime)oReader["CheckIn"];
                        r.CheckOut = r.CheckIn.Value.AddDays(r.Night.Value);
                        r.SaleCur = Conversion.getStrOrNull(oReader["SaleCur"]);
                        r.DepCity = Conversion.getInt32OrNull(oReader["DepCity"]);
                        r.ArrCity = Conversion.getInt32OrNull(oReader["ArrCity"]);
                        r.Operator = Conversion.getStrOrNull(oReader["Operator"]);
                        r.Market = Conversion.getStrOrNull(oReader["Market"]);
                        r.FlightClass = Conversion.getStrOrNull(oReader["FlightClass"]);
                        return r;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}