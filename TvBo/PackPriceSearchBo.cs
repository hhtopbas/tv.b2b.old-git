﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class PPAccomPrice
    {
        public int RefNo { get; set; }
        public int RoomNr { get; set; }
        public int PriceID { get; set; }
        public string DepFlight { get; set; }
        public string DepFlightSeat { get; set; }
        public string RetFlight { get; set; }
        public string RetFlightSeat { get; set; }
        public string FreeRoom { get; set; }
        public string RoomName { get; set; }
        public string BoardName { get; set; }
        public string AccomName { get; set; }
        public string SalePrice { get; set; }
        public decimal? Price { get; set; }
        public string StopSaleMessage { get; set; }
        public string StopSale { get; set; }
        public string SaleCur { get; set; }        
        public string PriceNoComma { get; set; }
        public bool Selected { get; set; }
        public bool Discount { get; set; }
        public bool priceOddEven { get; set; }
        public PPAccomPrice()
        {
            this.Selected = false;
            this.Discount = false;
            this.priceOddEven = false;
        }
    }
    public class PPPriceDetail
    {
        public int PriceDetailID { get; set; }
        public string labelDepFlight { get; set; }
        public string labelRetFlight { get; set; }
        public string AccomInfo { get; set; }
        public string labelFreeRoom { get; set; }
        public string labelRoomName { get; set; }
        public string labelBoardName { get; set; }
        public string labelAccomName { get; set; }
        public string labelPrice { get; set; }
        public string labelSelected { get; set; }
        public string labelOffer { get; set; }
        public List<PPAccomPrice> accomPrice { get; set; }
        public decimal selectedPrice { get; set; }
        public string priceCurr { get; set; }

        public PPPriceDetail()
        {
            this.accomPrice = new List<PPAccomPrice>();
            this.selectedPrice = (decimal)0;
        }
    }
    public class PPHotelGroup
    {
        public int HotelGroupID { get; set; }
        public int? SortingID { get; set; }
        public bool showHotelImage { get; set; }
        public string hotelImageUrl { get; set; }
        public string hotelInfoUrl { get; set; }
        public string HotelName { get; set; }
        public string HotelLocationName { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public string Night { get; set; }
        public decimal MinPrice { get; set; }
        public string SelectedPrice { get; set; }
        public string SelectedPriceCur { get; set; }
        public List<PPPriceDetail> priceDetail { get; set; }
        public bool? Parameter1 { get; set; }
        public string Parameter2 { get; set; }
        public int? Parameter3 { get; set; }
        public PPHotelGroup()
        {
            this.priceDetail = new List<PPPriceDetail>();
            this.MinPrice = 0;
        }
    }
    [Serializable()]
    public class PackPriceSearchResult
    {
        public int? BookID { get; set; }
        public int? SearchNr { get; set; }
        public int? PriceID { get; set; }
        public int? PartNo { get; set; }
        public long? HSRecID { get; set; }
        public int? HSPartNo { get; set; }
        public int? MarketID { get; set; }
        public int? HolPackID { get; set; }
        public int? HotelID { get; set; }
        public int? RoomID { get; set; }
        public int? AccomID { get; set; }
        public int? BoardID { get; set; }
        public int? DepCity { get; set; }
        public int? ArrCity { get; set; }
        public DateTime? CheckIn { get; set; }
        public Int16? Night { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? AdlPrice { get; set; }
        public decimal? EBed1Price { get; set; }
        public decimal? EBed2Price { get; set; }
        public decimal? ChdG1Price { get; set; }
        public decimal? ChdG2Price { get; set; }
        public decimal? ChdG3Price { get; set; }
        public decimal? ChdG4Price { get; set; }
        public decimal? Chd1Price { get; set; }
        public decimal? Chd2Price { get; set; }
        public decimal? Chd3Price { get; set; }
        public decimal? Chd4Price { get; set; }
        public Int16? Adl { get; set; }
        public Int16? ExtBed { get; set; }
        public Int16? Chd { get; set; }
        public Int16? ChdAgeG1 { get; set; }
        public Int16? ChdAgeG2 { get; set; }
        public Int16? ChdAgeG3 { get; set; }
        public Int16? ChdAgeG4 { get; set; }
        public decimal? ChdG1Age1 { get; set; }
        public decimal? ChdG1Age2 { get; set; }
        public decimal? ChdG2Age1 { get; set; }
        public decimal? ChdG2Age2 { get; set; }
        public decimal? ChdG3Age1 { get; set; }
        public decimal? ChdG3Age2 { get; set; }
        public decimal? ChdG4Age1 { get; set; }
        public decimal? ChdG4Age2 { get; set; }
        public Int16? SaleType { get; set; }
        public string Services { get; set; }
        public string PackDepFlight { get; set; }
        public int? CurID { get; set; }
        public string Market { get; set; }
        public string Operator { get; set; }
        public string HolPack { get; set; }
        public string DepName { get; set; }
        public string DepNameL { get; set; }
        public string ArrName { get; set; }
        public string ArrNameL { get; set; }
        public string Hotel { get; set; }
        public string HotelName { get; set; }
        public string HotelNameL { get; set; }
        public string GiataCode { get; set; }
        public string Room { get; set; }
        public string RoomName { get; set; }
        public string RoomNameL { get; set; }
        public string Accom { get; set; }
        public string AccomName { get; set; }
        public string AccomNameL { get; set; }
        public string Board { get; set; }
        public string BoardName { get; set; }
        public string BoardNameL { get; set; }
        public string HotCategory { get; set; }
        public string CatName { get; set; }
        public string CatNameL { get; set; }
        public int? HotLocation { get; set; }
        public string HotLocationName { get; set; }
        public string HotLocationNameL { get; set; }
        public bool? ChdCalcSale { get; set; }
        public string FlightClass { get; set; }
        public string SaleCur { get; set; }
        public string CurrentCur { get; set; }
        public Int16? StdAdl { get; set; }
        public string AccomFullName { get; set; }
        public Int16? StopSaleGuar { get; set; }
        public Int16? StopSaleStd { get; set; }
        public Int16? HotelAllot { get; set; }
        public Int16? DepSeat { get; set; }
        public Int16? RetSeat { get; set; }
        public string DepFlight { get; set; }
        public string RetFlight { get; set; }
        public int? PackSPOID { get; set; }
        public int? PackSPOValID { get; set; }
        public int? PackChildSPOID { get; set; }
        public Int16? DiscountType { get; set; }
        public decimal? SpoPer { get; set; }
        public decimal? SpoAdlVal { get; set; }
        public decimal? SpoExtB1Val { get; set; }
        public decimal? SpoExtB2Val { get; set; }
        public decimal? SpoChdGrp1Val { get; set; }
        public decimal? SpoChdGrp2Val { get; set; }
        public decimal? SpoChdGrp3Val { get; set; }
        public decimal? SpoChdGrp4Val { get; set; }
        public decimal? CHDSpoPer { get; set; }
        public decimal? CHDSpoVal { get; set; }
        public bool? HotChd1Free { get; set; }
        public bool? HotChd2Free { get; set; }
        public bool? HotChd3Free { get; set; }
        public bool? HotChd4Free { get; set; }
        public decimal? ChildBasePrice { get; set; }
        public string ChdSpSType { get; set; }
        public decimal? ChdSpPer { get; set; }
        public decimal? CHDSpVal { get; set; }
        public int? Chd1GrpNo { get; set; }
        public int? Chd2GrpNo { get; set; }
        public int? Chd3GrpNo { get; set; }
        public int? Chd4GrpNo { get; set; }
        public int? ChildSPOChdNo { get; set; }
        public string ServiceDetail { get; set; }
        public string CustomFixNote { get; set; }
        public bool? AutoStop { get; set; }
        public string WebPub { get; set; }
        public bool? B2BActive { get; set; }
        public bool? B2CActive { get; set; }
        public PackPriceSearchResult()
        {
            this.BookID = 1;
        }
    }
}
