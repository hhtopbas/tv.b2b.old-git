﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class PromoRecord
    {
        public PromoRecord()
        { 
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        Int16? _PromoType;
        public Int16? PromoType
        {
            get { return _PromoType; }
            set { _PromoType = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        string _PayCom;
        public string PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        string _PayComEB;
        public string PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool? _PayPasEB;
        public bool? PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool? _CanDiscount;
        public bool? CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        bool? _PasEBValid;
        public bool? PasEBValid
        {
            get { return _PasEBValid; }
            set { _PasEBValid = value; }
        }

        bool? _SpoValid;
        public bool? SpoValid
        {
            get { return _SpoValid; }
            set { _SpoValid = value; }
        }

        bool? _ValidWithPromo;
        public bool? ValidWithPromo
        {
            get { return _ValidWithPromo; }
            set { _ValidWithPromo = value; }
        }

    }

    [Serializable()]
    public class ResPromoRecord
    {
        public ResPromoRecord()
        {
            _MemTable = false;
            _RecStatus = RecordStatus.New;
        }        
        
        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        int? _PromoID;
        public int? PromoID
        {
            get { return _PromoID; }
            set { _PromoID = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        bool? _Confirmed;
        public bool? Confirmed
        {
            get { return _Confirmed; }
            set { _Confirmed = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        string _Remarks;
        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }

        string _PayCom;
        public string PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        string _PayComEB;
        public string PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool? _PayPasEB;
        public bool? PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool? _Status;
        public bool? Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        int? _PromoPriceID;
        public int? PromoPriceID
        {
            get { return _PromoPriceID; }
            set { _PromoPriceID = value; }
        }

        Int16? _PromoType;
        public Int16? PromoType
        {
            get { return _PromoType; }
            set { _PromoType = value; }
        }

        decimal? _NetAmount;
        public decimal? NetAmount
        {
            get { return _NetAmount; }
            set { _NetAmount = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable]
    public class PromoListRecord
    {
        public PromoListRecord()
        {
            _Sel = false;
            _Exist = false;
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int16? _PromoType;
        public Int16? PromoType
        {
            get { return _PromoType; }
            set { _PromoType = value; }
        }

        int? _PromoID;
        public int? PromoID
        {
            get { return _PromoID; }
            set { _PromoID = value; }
        }

        string _PromoName;
        public string PromoName
        {
            get { return _PromoName; }
            set { _PromoName = value; }
        }

        string _PromoNameL;
        public string PromoNameL
        {
            get { return _PromoNameL; }
            set { _PromoNameL = value; }
        }

        int? _PromoPriceID;
        public int? PromoPriceID
        {
            get { return _PromoPriceID; }
            set { _PromoPriceID = value; }
        }

        bool? _ConfReq;
        public bool? ConfReq
        {
            get { return _ConfReq; }
            set { _ConfReq = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _ResCur;
        public string ResCur
        {
            get { return _ResCur; }
            set { _ResCur = value; }
        }

        string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        Int16? _CalcType;
        public Int16? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        decimal? _ChdG1MaxAge;
        public decimal? ChdG1MaxAge
        {
            get { return _ChdG1MaxAge; }
            set { _ChdG1MaxAge = value; }
        }

        decimal? _SaleChdG1Price;
        public decimal? SaleChdG1Price
        {
            get { return _SaleChdG1Price; }
            set { _SaleChdG1Price = value; }
        }

        decimal? _ChdG2MaxAge;
        public decimal? ChdG2MaxAge
        {
            get { return _ChdG2MaxAge; }
            set { _ChdG2MaxAge = value; }
        }

        decimal? _SaleChdG2Price;
        public decimal? SaleChdG2Price
        {
            get { return _SaleChdG2Price; }
            set { _SaleChdG2Price = value; }
        }

        decimal? _ChdG3MaxAge;
        public decimal? ChdG3MaxAge
        {
            get { return _ChdG3MaxAge; }
            set { _ChdG3MaxAge = value; }
        }

        decimal? _SaleChdG3Price;
        public decimal? SaleChdG3Price
        {
            get { return _SaleChdG3Price; }
            set { _SaleChdG3Price = value; }
        }

        bool _Sel;
        public bool Sel
        {
            get { return _Sel; }
            set { _Sel = value; }
        }

        bool? _PasEBValid;
        public bool? PasEBValid
        {
            get { return _PasEBValid; }
            set { _PasEBValid = value; }
        }

        bool? _SpoValid;
        public bool? SpoValid
        {
            get { return _SpoValid; }
            set { _SpoValid = value; }
        }

        bool? _ValidWithPromo;
        public bool? ValidWithPromo
        {
            get { return _ValidWithPromo; }
            set { _ValidWithPromo = value; }
        }

        bool? _Exist;
        public bool? Exist
        {
            get { return _Exist; }
            set { _Exist = value; }
        }
    }    
}
