﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class Rentings
    {
        public RentingRecord getRenting(string Market, string Code, ref string errorMsg)
        {
            RentingRecord record = new RentingRecord();

            string tsql = @"Select Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market), [Name]),
	                            NameS, Category, [Description],
	                            ConfStat, TaxPer, PayCom, PayComEB, PayPasEB, CanDiscount
                            From Renting (NOLOCK) ";
            if (!string.IsNullOrEmpty(Code)) tsql += string.Format("Where Code= '{0}' ", Code);

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Category = Conversion.getStrOrNull(R["Category"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<RentingPriceRecord> getRentingPrices(string Market, string plMarket, string Code, int? Location, DateTime? BegDate, DateTime? EndDate, ref string errorMsg)
        {
            List<RentingPriceRecord> records = new List<RentingPriceRecord>();
            string tsql = @"Select Renting, Market, Location, 
	                            LocationName=(Select [Name] From Location (NOLOCK) Where RecID=RentingPrice.Location),
	                            LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=RentingPrice.Location),
	                            BegDate, EndDate, NetCur, SaleType, SaleCur, Supplier
                            From RentingPrice (NOLOCK) 
                            Where Market=@plMarket 
                            ";

            string whereStr = " ";
            if (!string.IsNullOrEmpty(Code))
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += string.Format(" Renting='{0}' ", Code);
            }

            if (Location.HasValue)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += string.Format(" Location={0} ", Location.Value);
            }

            if (BegDate.HasValue)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += " @BegDate >= BegDate ";
            }

            if (EndDate.HasValue)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += " @EndDate <= EndDate ";
            }

            if (whereStr.Length > 1) tsql += whereStr;

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                if (BegDate.HasValue) 
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate.Value.Date);
                if (BegDate.HasValue)
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate.Value.Date);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {                                
                        RentingPriceRecord record = new RentingPriceRecord();
                        record.Renting = Conversion.getStrOrNull(R["Renting"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Location = (int)R["Location"];
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationLocalName = Conversion.getStrOrNull(R["LocationLocalName"]);
                        record.BegDate = (DateTime)R["BegDate"];
                        record.EndDate = (DateTime)R["EndDate"];
                        record.NetCur = Conversion.getStrOrNull(R["NetCur"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.SaleType = Conversion.getStrOrNull(R["SaleType"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);                        
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public DataRow getRentingDetail(string Market, string plMarket, string Code, int Location, DateTime? BegDate, DateTime? EndDate, ref string errorMsg)
        {            
            RentingRecord renting = getRenting(Market, Code, ref errorMsg);
            List<RentingPriceRecord> rentingPrice = getRentingPrices(Market, plMarket, Code, Location, BegDate, EndDate, ref errorMsg);
            var query = from q in rentingPrice.AsEnumerable()
                        where q.Renting == renting.Code
                        select new
                        {
                           renting.Name,
                           renting.NameS,
                           renting.LocalName,
                           renting.Category,
                           q.Location,
                           q.LocationName,
                           q.LocationLocalName,
                           renting.CanDiscount
                        };

            return new TvBo.Common().LINQToDataRow(query);
        }

        public List<RentingRecord> getRentingList(string Market, string plMarket, int? Location, DateTime? BegDate, DateTime? EndDate, ref string errorMsg)
        {
            List<RentingRecord> records = new List<RentingRecord>();
            string whereStr = " ";            

            if (Location.HasValue)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += string.Format(" Location={0} ", Location.Value);
            }

            if (BegDate.HasValue)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += " @BegDate >= BegDate ";
            }

            if (EndDate.HasValue)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += " @EndDate <= EndDate ";
            }
            string tsql = string.Format(@"Select R.Code, R.[Name], LocalName=isnull(dbo.FindLocalName(R.NameLID, @Market), R.[Name]),
	                                            R.NameS, R.Category, R.[Description],
	                                            R.ConfStat, R.TaxPer, R.PayCom, R.PayComEB, R.PayPasEB, R.CanDiscount
                                            From (
	                                            Select Distinct Renting
	                                            From RentingPrice (NOLOCK) 
	                                            Where Market=@plMarket
	                                             {0} 
                                            ) X
                                            Join Renting R (NOLOCK) ON R.Code=X.Renting ", whereStr);

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                if (BegDate.HasValue)
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate.Value.Date);
                if (BegDate.HasValue)
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate.Value.Date);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        RentingRecord record = new RentingRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Category = Conversion.getStrOrNull(R["Category"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
