﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using TvBo;
using TvTools;

namespace TvBo
{
    public class CacheObjects
    {
        public static string getOperatorLogoUrl(string Operator)
        {
            List<OperatorLogo> obj;
            if (!CacheHelper.Get("OprLogo", out obj))
            {
                obj = new UICommon().getOperatorLogo();
                CacheHelper.Add(obj, "OprLogo", 24);
                HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.Url.ToString(), false);
            }
            return obj.Find(f => f.Operator == Operator).OperatorLogoPath;
        }
        public static List<HotelHandicapRecord> GetAllHotelHandikaps(string pMarket)
        {
            string key = string.Format("HotelHandikap_{0}", pMarket);
            List<HotelHandicapRecord> obj;
            if (!CacheHelper.Get(key, out obj))
            {
                obj = new Hotels().getAllHotelHandicaps(pMarket, pMarket);
                CacheHelper.Add(obj, key, 1);
            }
            return obj;
        }
        public static string getAgencyLogoUrl(string Agency)
        {
            List<OperatorLogo> obj;
            if (!CacheHelper.Get("AgencyLogo", out obj))
            {
                obj = new UICommon().getAgencyLogo();
                CacheHelper.Add(obj, "AgencyLogo", 24);
                HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.Url.ToString(), false);
            }
            return obj.Find(f => f.Operator == Agency).OperatorLogoPath;
        }

        public static SearchPLData getPackageSearchData(string Operator, string Market, bool GroupSearch, SearchType sType, Int16? holPackCat, string b2bMenuCat)
        {
            string errorMsg = string.Empty;
            if (HttpContext.Current.Session["UserData"] == null)
                return new SearchPLData();
            User UserData = (User)HttpContext.Current.Session["UserData"];
            if (!string.IsNullOrEmpty(b2bMenuCat))
                sType = SearchType.OtherSearch;
            string onlyHotelVersion = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "OnlyHotelVersion"));

            string key = string.Empty;
            switch (sType)
            {
                case SearchType.PackageSearch:
                    key = "Package" + "_" + Operator + "_" + Market;
                    break;
                case SearchType.OnlyHotelSearch:
                    key = "OnlyHotel" + "_" + Operator + "_" + Market;
                    break;
                case SearchType.CruiseSearch:
                    key = "Cruise" + "_" + Operator + "_" + Market;
                    break;
                case SearchType.OtherSearch:
                    if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0024, VersionControl.Equality.gt))
                        key = "Other_" + b2bMenuCat + "_" + Operator + "_" + Market;
                    else
                        key = holPackCat.ToString() + "OnlyHotel" + "_" + Operator + "_" + Market;
                    break;
                case SearchType.TourPackageSearch:
                    key = "CultureTour" + "_" + Operator + "_" + Market;
                    break;
                case SearchType.NoSearch:
                    key = "None" + "_" + Operator + "_" + Market;
                    break;
                case SearchType.PackPriceSearch:
                    key = "PackPrice" + "_" + Operator + "_" + Market;
                    break;
            }
            SearchPLData obj;
            if (!CacheHelper.Get(key, out obj))
            {
                if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0024, VersionControl.Equality.gt))
                {
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                        obj = new Search().getPricelistLocationData(UserData, Operator, Market, GroupSearch, sType, ref errorMsg);
                    else
                    {
                        if (sType == SearchType.PackPriceSearch)
                            obj = new PackPriceSearchs().getPackPriceSearchFilterCacheData(Operator, Market, GroupSearch, ref errorMsg);
                        else
                            obj = new Search().getPricelistLocationDataForB2bMenuCat(UserData.AgencyID, Operator, Market, GroupSearch, sType, b2bMenuCat, onlyHotelVersion, ref errorMsg);
                    }
                }
                else
                    obj = new Search().getPricelistLocationData(UserData, Operator, Market, GroupSearch, sType, ref errorMsg);
                CacheHelper.Add(obj, key);
            }
            return obj;
        }

        public static List<FlightDays> getFlightDaysDepCityArrCity(string Market)
        {
            string errorMsg = string.Empty;
            string key = "FlightDays" + "_" + Market;
            List<FlightDays> obj;
            if (!CacheHelper.Get(key, out obj))
            {
                obj = new Flights().getFlightDays(Market, ref errorMsg);
                CacheHelper.Add(obj, key);
                //HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.Url.ToString());
            }
            return obj;
        }

        public static List<WebConfig> getWebConfigData()
        {
            List<WebConfig> obj;
            if (!CacheHelper.Get("WebConfig", out obj))
            {
                obj = new UICommon().getWebConfig();
                CacheHelper.Add(obj, "WebConfig");
            }
            return obj;
        }

        public static List<MainMenu> getMainMenuData(User UserData, ref string errorMsg)
        {
            List<MainMenu> obj = new List<MainMenu>();
            if (UserData == null)
            {
                errorMsg = "User data not created.";
                return obj;
            }
            if (UserData.Ci == null)
            {
                errorMsg = "User culture info not created.";
                CultureInfo[] allCulture = CultureInfo.GetCultures(CultureTypes.AllCultures);
                if (allCulture.Where(w => Equals(w.Name, HttpContext.Current.Session["Culture"])).Count() < 1)
                {
                    HttpContext.Current.Session["Culture"] = "en-US";
                }
                //if (Equals(HttpContext.Current.Session["Culture"], "ar") || Equals(HttpContext.Current.Session["Culture"], "ar-SA"))
                //    HttpContext.Current.Session["Culture"] = "en-US";

                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo(((System.Globalization.CultureInfo)HttpContext.Current.Session["Culture"]).Name, false);
                HttpContext.Current.Session["Culture"] = culture;
                UserData.Ci = culture;
            }

            System.Threading.Thread.CurrentThread.CurrentCulture = UserData.Ci;
            System.Threading.Thread.CurrentThread.CurrentUICulture = UserData.Ci;

            obj = new UICommon().getMainMenu();
            foreach (TvBo.MainMenu row in obj)
            {
                #region Paximum settings correction
                if (UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value)
                {
                    if ((UserData.PaxSetting.Pxm_WorkType == PxmWorkType.All || UserData.PaxSetting.Pxm_WorkType == PxmWorkType.UsePaximumInline) && string.Equals(row.Url, "OnlyHotelSearch"))
                    {
                        row.Url = "OnlyHotelMixSearch";
                    }
                }
                #endregion
            }
            obj = new UICommon().setMainMenuLanguage(UserData, obj);
            return obj;
        }

        public static List<ResViewMenu> getResViewMenuData()
        {
            List<ResViewMenu> obj;
            if (!CacheHelper.Get("ResViewMenu", out obj))
            {
                obj = new UICommon().getResViewMenu();
                CacheHelper.Add(obj, "ResViewMenu");
            }
            return obj;
        }

        public static List<TvBo.MakeResMenu> getMakeResMenuData()
        {
            List<TvBo.MakeResMenu> obj;
            if (!CacheHelper.Get("MakeResMenu", out obj))
            {
                obj = new UICommon().getMakeResMenu();
                CacheHelper.Add(obj, "MakeResMenu");
            }
            return obj;
        }

        public static List<TvBo.Location> getLocationList(string Market)
        {
            string errorMsg = string.Empty;
            List<TvBo.Location> obj;
            if (!CacheHelper.Get("Location_" + Market, out obj))
            {
                obj = new Locations().getLocationList(Market, LocationType.None, null, null, null, null, ref errorMsg);
                CacheHelper.Add(obj, "Location_" + Market);
            }
            return obj;
        }

        public static List<TvBo.MarketRecord> getMarkets(string Market)
        {
            string errorMsg = string.Empty;
            List<TvBo.MarketRecord> obj;
            if (!CacheHelper.Get("Market_" + Market, out obj))
            {
                obj = new Parameters().getMarkets(ref errorMsg);
                CacheHelper.Add(obj, "Market_" + Market);
            }
            return obj;
        }

        public static List<TvBo.OperatorRecord> getOperators(string Market)
        {
            string errorMsg = string.Empty;
            List<TvBo.OperatorRecord> obj;
            if (!CacheHelper.Get("Operator_" + Market, out obj))
            {
                obj = new Parameters().getOperators(Market, ref errorMsg);
                CacheHelper.Add(obj, "Operator_" + Market);
            }
            return obj;
        }

        public static List<TvBo.DDLData> getNations(string Market)
        {
            string errorMsg = string.Empty;
            List<TvBo.DDLData> obj;
            if (!CacheHelper.Get("Nation_" + Market, out obj))
            {
                obj = new UICommon().getNations(Market, ref errorMsg);
                CacheHelper.Add(obj, "Nation_" + Market);
            }
            return obj;
        }

        public static List<TvBo.Nationality> getNationality(User UserData)
        {
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
            {
                string errorMsg = string.Empty;
                List<TvBo.Nationality> obj;
                if (!CacheHelper.Get("Nationality", out obj))
                {
                    obj = new Locations().getNationalityList(ref errorMsg);
                    CacheHelper.Add(obj, "Nationality");
                }
                return obj;
            }
            else
            {
                return null;
            }
        }

        public static object getPaxLocationForCountry(User UserData, string Country, GetDestinationListResponse CountryLocation)
        {
            string errorMsg = string.Empty;
            object obj;
            if (!CacheHelper.Get("PaxLocation_" + Country, out obj))
            {
                CacheHelper.Add(CountryLocation, "PaxLocation_" + Country, 24);
            }
            return obj;
        }
        public static string getMarketPriceListFixNote(string Market)
        {
            string obj = string.Empty;
            if (!CacheHelper.Get("MarketPriceListFixNote_" + Market, out obj))
            {
                try
                {
                    string errorMsg = string.Empty;
                    string searchTextRTF = new Search().getSearchText(Market, ref errorMsg);
                    StringBuilder sb = new StringBuilder();
                    if (!string.IsNullOrEmpty(searchTextRTF))
                    {
                        TvTools.RtfToHtmlV2 rtfV2 = new RtfToHtmlV2();
                        obj = rtfV2.convertRtfToHtml(searchTextRTF);
                        //TvTools.RTFtoHTML rtf = new RTFtoHTML();
                        //searchTextRTF = Regex.Replace(searchTextRTF, @"\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?", "");
                        //rtf.rtf = searchTextRTF;
                        //obj = rtf.html();
                    }
                    CacheHelper.Add(obj, "MarketPriceListFixNote_" + Market, 2);
                }
                catch
                {

                }

            }
            return obj;
        }
    }
}
