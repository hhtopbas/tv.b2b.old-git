﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public enum LocationType { Country, City, Resort, Village, None };

    [Serializable()]
    public class Location
    {
        public Location()
        {

        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int _Parent;
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        Int16 _Type;
        public Int16 Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        int? _Village;
        public int? Village
        {
            get { return _Village; }
            set { _Village = value; }
        }

        int? _Town;
        public int? Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        int? _City;
        public int? City
        {
            get { return _City; }
            set { _City = value; }
        }

        int _Country;
        public int Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        string _CountryCode;
        public string CountryCode
        {
            get { return _CountryCode; }
            set { _CountryCode = value; }
        }

        string _VisaReq;
        public string VisaReq
        {
            get { return _VisaReq; }
            set { _VisaReq = value; }
        }

        bool? _EUMember;
        public bool? EUMember
        {
            get { return _EUMember; }
            set { _EUMember = value; }
        }
    }

    [Serializable()]
    public class LocationIDName
    {
        public LocationIDName()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        Int16 _Type;
        public Int16 Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
    }

    [Serializable()]
    public class Nationality
    {
        public int? RecID { get; set; }
        public string Code3 { get; set; }
        public string Code2 { get; set; }
        public Int16? CodeNum { get; set; }
        public string Name { get; set; }
        public int OrderNo { get; set; }
        public Nationality()
        { 
        }
    }
}
