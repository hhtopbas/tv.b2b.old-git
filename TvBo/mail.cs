﻿using System.Net;
using System.Net.Mail;
//using System.Windows.Forms;
using System.Configuration;
using System;
using System.Data;
using System.Web.Configuration;

namespace TvBo
{
    public class Mail
    {
        static string mailAccountEmail = ConfigurationManager.AppSettings["MailAccountEmail"].ToString();
        static string mailAccountHost = ConfigurationManager.AppSettings["MailAccountHost"].ToString();
        static int mailAccountPort = Convert.ToInt32(ConfigurationManager.AppSettings["MailAccountPort"]);
        static string mailAccountUser = ConfigurationManager.AppSettings["MailAccountUser"].ToString();
        static string mailAccountPass = ConfigurationManager.AppSettings["MailAccountPass"].ToString();

        public static bool SendMail(string toAddress, string fileName, string subject, string body)
        {
            bool isComplete = false;
            MailMessage message = new MailMessage();
            message.From = new MailAddress(mailAccountEmail);
            message.To.Add(toAddress);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            if (!string.IsNullOrEmpty(fileName))
            {
                Attachment attach = new Attachment(fileName);
                message.Attachments.Add(attach);
            }

            SmtpClient smtp = new SmtpClient(mailAccountHost);
            if (!string.IsNullOrEmpty(mailAccountPass))
                smtp.Credentials = new NetworkCredential(mailAccountEmail, mailAccountPass);

            smtp.Port = mailAccountPort;

            try
            {
                smtp.Send(message);
                isComplete = true;
            }
            catch (Exception)
            {
                isComplete = false;
            }
            finally
            {
                message.Dispose();
            }
            return isComplete;
        }
        public static bool SendMail(string fromAddress, string fromHost, string fromPass, string toAddress, string fileName, string subject, string body)
        {
            bool isComplete = false;
            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromAddress);
            message.To.Add(toAddress);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            if (!string.IsNullOrEmpty(fileName))
            {
                Attachment attach = new Attachment(fileName);
                message.Attachments.Add(attach);
            }

            SmtpClient smtp = new SmtpClient(fromHost);
            if (!string.IsNullOrEmpty(fromPass))
                smtp.Credentials = new NetworkCredential(fromAddress, fromPass);

            smtp.Port = mailAccountPort;
            try
            {
                smtp.Send(message);
                isComplete = true;
            }
            catch (Exception Ex)
            {
                isComplete = false;
            }
            finally
            {
                message.Dispose();
            }
            return isComplete;
        }
        public static bool SendMail(string from, string[] toMails, string subject, string body)
        {
            bool isComplete = false;
            MailMessage message = new MailMessage();
            message.From = new MailAddress(from);
            for (int i = 0; i < toMails.Length; i++)
            {
                message.To.Add(toMails[i]);
            }
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;

            SmtpClient smtp = new SmtpClient(mailAccountHost);
            if (!string.IsNullOrEmpty(mailAccountPass))
                smtp.Credentials = new NetworkCredential(mailAccountEmail, mailAccountPass);

            smtp.Port = mailAccountPort;

            try
            {
                smtp.Send(message);
                isComplete = true;
            }
            catch (Exception)
            {
                isComplete = false;
            }
            finally
            {
                message.Dispose();
            }
            return isComplete;
        }
        public static string MailReplaced(string token, string mailTemplate, string MailTemplateType, string UserAccountEmail, string ResNumber, int? CultureId, int? DefaultCultureId)
        {
            string errMsg = " ";
            decimal balance = 0;
            decimal pasPayable = 0;
            decimal remainingAmount = 0;
            Booking book = new Booking();
            try
            {
                switch (MailTemplateType)
                {
                    case "DsLostPassword":
                        #region LostPassword
                        SiteMember userAccount = SiteMemberManagerCustom.GetByEmail(UserAccountEmail);
                        mailTemplate = mailTemplate.Replace("{UserName}", string.Concat(userAccount.SiteMemberFirstName, " ", userAccount.SiteMemberLastName));
                        mailTemplate = mailTemplate.Replace("{Password}", userAccount.SiteMemberPwd);
                        mailTemplate = mailTemplate.Replace("{Email}", userAccount.SiteMemberMail);
                        #endregion
                        break;
                    case "DsNewReservation":
                        #region NewReservation
                        DataSet dsReservation = new DataSet();
                        dsReservation.Tables.Clear();
                        DataTable dtResPayPlan = TourVisioDB.GetResPayPlan(ResNumber).Tables[0].Copy();
                        DataTable dtResBalance = TourVisioDB.GetResPayPlan(ResNumber).Tables[1].Copy();
                        DataTable dtReservation = book.GetReservationTotalInfo(token, ResNumber, ref errMsg).Tables[0].Copy();
                        DataTable dtReservationCustInfo = book.GetReservationTotalInfo(token, ResNumber, ref errMsg).Tables[1].Copy();
                        dsReservation.Tables.Add(dtResPayPlan);  //GetReservationPayPlan
                        dsReservation.Tables.Add(dtResBalance); //GetReservationBalance
                        dsReservation.Tables.Add(dtReservation); //GetReservationInfo
                        dsReservation.Tables.Add(dtReservationCustInfo); //GetReservationCustomerInfo
                        if (dsReservation.Tables["Table1"].Rows.Count > 0 && dsReservation.Tables["ResMain"].Rows.Count > 0)
                        {
                            #region BalanceAmount
                            balance = decimal.Parse(dsReservation.Tables["Table1"].Rows[0]["Balance"].ToString());
                            #endregion
                        }
                        for (int i = 0; i < dsReservation.Tables.Count; i++)
                        {
                            for (int j = 0; j < dsReservation.Tables[i].Columns.Count; j++)
                            {
                                if (dsReservation.Tables[i].Rows.Count > 0)
                                    mailTemplate = mailTemplate.Replace("{" + dsReservation.Tables[i].Columns[j].Caption + "}", dsReservation.Tables[i].Rows[0][j].ToString()).Replace("{ReservationRemainingAmount}", balance.ToString());
                            }
                        }
                        #endregion
                        break;
                }
            }
            catch
            {
                mailTemplate = "";
            }
            return mailTemplate;
        }
        public Mail()
        {

        }
    }
}
