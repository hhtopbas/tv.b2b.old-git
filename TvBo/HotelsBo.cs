﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class HotelRecord
    {
        public HotelRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _HotelType;
        public string HotelType
        {
            get { return _HotelType; }
            set { _HotelType = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        int? _AirportDist;
        public int? AirportDist
        {
            get { return _AirportDist; }
            set { _AirportDist = value; }
        }

        int? _CenterDist;
        public int? CenterDist
        {
            get { return _CenterDist; }
            set { _CenterDist = value; }
        }

        int? _SeaDist;
        public int? SeaDist
        {
            get { return _SeaDist; }
            set { _SeaDist = value; }
        }

        DateTime? _CheckInTime;
        public DateTime? CheckInTime
        {
            get { return _CheckInTime; }
            set { _CheckInTime = value; }
        }

        DateTime? _CheckOutTime;
        public DateTime? CheckOutTime
        {
            get { return _CheckOutTime; }
            set { _CheckOutTime = value; }
        }

        string _PostAddress;
        public string PostAddress
        {
            get { return _PostAddress; }
            set { _PostAddress = value; }
        }

        int _Location;
        public int Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _LocationLocalName;
        public string LocationLocalName
        {
            get { return _LocationLocalName; }
            set { _LocationLocalName = value; }
        }

        int _TrfLocation;
        public int TrfLocation
        {
            get { return _TrfLocation; }
            set { _TrfLocation = value; }
        }

        string _TrfLocationName;
        public string TrfLocationName
        {
            get { return _TrfLocationName; }
            set { _TrfLocationName = value; }
        }

        string _TrfLocationLocalName;
        public string TrfLocationLocalName
        {
            get { return _TrfLocationLocalName; }
            set { _TrfLocationLocalName = value; }
        }

        string _PostZip;
        public string PostZip
        {
            get { return _PostZip; }
            set { _PostZip = value; }
        }

        string _InvAddress;
        public string InvAddress
        {
            get { return _InvAddress; }
            set { _InvAddress = value; }
        }

        string _Phone1;
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        string _Phone2;
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }

        string _Fax;
        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }

        string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        string _homepage;
        public string Homepage
        {
            get { return _homepage; }
            set { _homepage = value; }
        }

        string _ManagerName;
        public string ManagerName
        {
            get { return _ManagerName; }
            set { _ManagerName = value; }
        }

        Int16? _RoomCount;
        public Int16? RoomCount
        {
            get { return _RoomCount; }
            set { _RoomCount = value; }
        }

        string _BossName;
        public string BossName
        {
            get { return _BossName; }
            set { _BossName = value; }
        }

        string _ContactName;
        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }

        string _TaxOffice;
        public string TaxOffice
        {
            get { return _TaxOffice; }
            set { _TaxOffice = value; }
        }

        string _TaxAccNo;
        public string TaxAccNo
        {
            get { return _TaxAccNo; }
            set { _TaxAccNo = value; }
        }

        string _AirPort1;
        public string AirPort1
        {
            get { return _AirPort1; }
            set { _AirPort1 = value; }
        }

        Int16? _ChdAgeGrpCnt;
        public Int16? ChdAgeGrpCnt
        {
            get { return _ChdAgeGrpCnt; }
            set { _ChdAgeGrpCnt = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool _UseSysParamAllot;
        public bool UseSysParamAllot
        {
            get { return _UseSysParamAllot; }
            set { _UseSysParamAllot = value; }
        }

        bool _HotAllotChk;
        public bool HotAllotChk
        {
            get { return _HotAllotChk; }
            set { _HotAllotChk = value; }
        }

        bool _HotAllotWarnFull;
        public bool HotAllotWarnFull
        {
            get { return _HotAllotWarnFull; }
            set { _HotAllotWarnFull = value; }
        }

        bool _HotAllotNotAcceptFull;
        public bool HotAllotNotAcceptFull
        {
            get { return _HotAllotNotAcceptFull; }
            set { _HotAllotNotAcceptFull = value; }
        }

        bool _HotAllotNotFullGA;
        public bool HotAllotNotFullGA
        {
            get { return _HotAllotNotFullGA; }
            set { _HotAllotNotFullGA = value; }
        }

        bool _PayCom;
        public bool PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        bool _PayComEB;
        public bool PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        string _DefSupplier;
        public string DefSupplier
        {
            get { return _DefSupplier; }
            set { _DefSupplier = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        decimal? _MaxChdAge;
        public decimal? MaxChdAge
        {
            get { return _MaxChdAge; }
            set { _MaxChdAge = value; }
        }

        decimal? _Lat;
        public decimal? Lat
        {
            get { return _Lat; }
            set { _Lat = value; }
        }

        decimal? _Lon;
        public decimal? Lon
        {
            get { return _Lon; }
            set { _Lon = value; }
        }

        string _OfCategory;
        public string OfCategory
        {
            get { return _OfCategory; }
            set { _OfCategory = value; }
        }

        bool _WithoutLady;
        public bool WithoutLady
        {
            get { return _WithoutLady; }
            set { _WithoutLady = value; }
        }

        bool? _Chk1;
        public bool? Chk1
        {
            get { return _Chk1; }
            set { _Chk1 = value; }
        }
    }

    public class HotelAccomRecord
    {
        public HotelAccomRecord()
        {
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _Name2;
        public string Name2
        {
            get { return _Name2; }
            set { _Name2 = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        Int16? _StdAdl;
        public Int16? StdAdl
        {
            get { return _StdAdl; }
            set { _StdAdl = value; }
        }

        Int16? _MinAdl;
        public Int16? MinAdl
        {
            get { return _MinAdl; }
            set { _MinAdl = value; }
        }

        Int16? _MaxAdl;
        public Int16? MaxAdl
        {
            get { return _MaxAdl; }
            set { _MaxAdl = value; }
        }

        Int16? _MaxChd;
        public Int16? MaxChd
        {
            get { return _MaxChd; }
            set { _MaxChd = value; }
        }

        Int16? _MaxPax;
        public Int16? MaxPax
        {
            get { return _MaxPax; }
            set { _MaxPax = value; }
        }

        Int16? _DispNo;
        public Int16? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

    }

    public class ufnHotelAllotCheckRecord
    {
        public ufnHotelAllotCheckRecord()
        {
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        Int16? _AllotType;
        public Int16? AllotType
        {
            get { return _AllotType; }
            set { _AllotType = value; }
        }

        DateTime? _AllotDate;
        public DateTime? AllotDate
        {
            get { return _AllotDate; }
            set { _AllotDate = value; }
        }

        Int16? _Avail;
        public Int16? Avail
        {
            get { return _Avail; }
            set { _Avail = value; }
        }

        Int16? aAllot;
        public Int16? AAllot
        {
            get { return aAllot; }
            set { aAllot = value; }
        }

        Int16? aUse;
        public Int16? AUse
        {
            get { return aUse; }
            set { aUse = value; }
        }

        Int16? _Release;
        public Int16? Release
        {
            get { return _Release; }
            set { _Release = value; }
        }

        string _Type;
        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        Int16? _ErrorCode;
        public Int16? ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }

        bool? _CanMakeRes;
        public bool? CanMakeRes
        {
            get { return _CanMakeRes; }
            set { _CanMakeRes = value; }
        }

    }

    public class RoomRecord
    {
        public int RecID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameS { get; set; }
        public string NameL { get; set; }
        public int? DispNo { get; set; }
        public string Explanation { get; set; }
        public int? RoomViewID { get; set; }
        public RoomRecord()
        {
        }
    }

    public class HotelRoomRecord
    {
        public int RecID { get; set; }
        public string Hotel { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameS { get; set; }
        public string NameL { get; set; }
        public int? DispNo { get; set; }
        public string BaseRoom { get; set; }
        public int? RoomViewID { get; set; }
        public HotelRoomRecord()
        {
        }
    }

    public class BoardRecord
    {
        public BoardRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        int? _DispNo;
        public int? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

    }

    public class HotelBoardRecord
    {
        public HotelBoardRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        int? _DispNo;
        public int? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

    }

    public class HotelAccomPaxRecord
    {
        public HotelAccomPaxRecord()
        {
        }

        Int32? _RecID;
        public Int32? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _RoomName;
        public string RoomName
        {
            get { return _RoomName; }
            set { _RoomName = value; }
        }

        string _Accom;
        public string Accom
        {
            get { return _Accom; }
            set { _Accom = value; }
        }

        string _AccomName;
        public string AccomName
        {
            get { return _AccomName; }
            set { _AccomName = value; }
        }

        Int16? _Adult;
        public Int16? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16? _ChdAgeG1;
        public Int16? ChdAgeG1
        {
            get { return _ChdAgeG1; }
            set { _ChdAgeG1 = value; }
        }

        Int16? _ChdAgeG2;
        public Int16? ChdAgeG2
        {
            get { return _ChdAgeG2; }
            set { _ChdAgeG2 = value; }
        }

        Int16? _ChdAgeG3;
        public Int16? ChdAgeG3
        {
            get { return _ChdAgeG3; }
            set { _ChdAgeG3 = value; }
        }

        Int16? _ChdAgeG4;
        public Int16? ChdAgeG4
        {
            get { return _ChdAgeG4; }
            set { _ChdAgeG4 = value; }
        }

        Int16? _DispNo;
        public Int16? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

        string _VisiblePL;
        public string VisiblePL
        {
            get { return _VisiblePL; }
            set { _VisiblePL = value; }
        }

    }

    public class HotelMarOptRecord
    {
        public int? RecID { get; set; }
        public string Hotel { get; set; }
        public string Market { get; set; }
        public string InfoWeb { get; set; }
        public decimal? MaxChdAge { get; set; }
        public decimal? MinAccomAge { get; set; }
        public HotelMarOptRecord()
        {
        }
    }

    public class HotelHandicapRecord
    {
        public HotelHandicapRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        bool? _PrtVoucher;
        public bool? PrtVoucher
        {
            get { return _PrtVoucher; }
            set { _PrtVoucher = value; }
        }

    }

    public class HotelPressRecord
    {
        public HotelPressRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _TextType;
        public string TextType
        {
            get { return _TextType; }
            set { _TextType = value; }
        }

        string _TextCat;
        public string TextCat
        {
            get { return _TextCat; }
            set { _TextCat = value; }
        }

        string _TextCatName;
        public string TextCatName
        {
            get { return _TextCatName; }
            set { _TextCatName = value; }
        }

        string _TextCatNameL;
        public string TextCatNameL
        {
            get { return _TextCatNameL; }
            set { _TextCatNameL = value; }
        }

        string _PresText;
        public string PresText
        {
            get { return _PresText; }
            set { _PresText = value; }
        }

    }

    public class FacilityCatRecord
    {
        public FacilityCatRecord()
        {
        }

        int? _RecId;
        public int? RecId
        {
            get { return _RecId; }
            set { _RecId = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        int? _DispNo;
        public int? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }
    }

    public class FacilityRecord
    {
        public FacilityRecord()
        {
        }

        int? _RecId;
        public int? RecId
        {
            get { return _RecId; }
            set { _RecId = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        Int16? _DispNo;
        public Int16? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

        bool? _WebDisp;
        public bool? WebDisp
        {
            get { return _WebDisp; }
            set { _WebDisp = value; }
        }

        int? _FacCatID;
        public int? FacCatID
        {
            get { return _FacCatID; }
            set { _FacCatID = value; }
        }

        bool? _UseinWebFilter;
        public bool? UseinWebFilter
        {
            get { return _UseinWebFilter; }
            set { _UseinWebFilter = value; }
        }

    }

    public class HotelFacilityRecord
    {
        public HotelFacilityRecord()
        {
        }

        int? _RecId;
        public int? RecId
        {
            get { return _RecId; }
            set { _RecId = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        int? _FacNo;
        public int? FacNo
        {
            get { return _FacNo; }
            set { _FacNo = value; }
        }

        string _FacilityName;
        public string FacilityName
        {
            get { return _FacilityName; }
            set { _FacilityName = value; }
        }

        string _FacilityNameL;
        public string FacilityNameL
        {
            get { return _FacilityNameL; }
            set { _FacilityNameL = value; }
        }

        int? _Unit;
        public int? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        string _Priced;
        public string Priced
        {
            get { return _Priced; }
            set { _Priced = value; }
        }

        int? _DispNo;
        public int? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        string _NoteLocalName;
        public string NoteLocalName
        {
            get { return _NoteLocalName; }
            set { _NoteLocalName = value; }
        }

        bool? _UseinWebFilter;
        public bool? UseinWebFilter
        {
            get { return _UseinWebFilter; }
            set { _UseinWebFilter = value; }
        }
    }

    public class RoomFacilityRecord
    {
        public RoomFacilityRecord()
        {
        }

        int? _RecId;
        public int? RecId { get { return _RecId; } set { _RecId = value; } }

        string _Name;
        public string Name { get { return _Name; } set { _Name = value; } }

        string _NameL;
        public string NameL { get { return _NameL; } set { _NameL = value; } }

        string _NameS;
        public string NameS { get { return _NameS; } set { _NameS = value; } }

        Int16? _DispNo;
        public Int16? DispNo { get { return _DispNo; } set { _DispNo = value; } }

        bool? _WebDisp;
        public bool? WebDisp { get { return _WebDisp; } set { _WebDisp = value; } }

        int? _FacCatID;
        public int? FacCatID { get { return _FacCatID; } set { _FacCatID = value; } }
    }

    public class HotelRoomFacRec
    {
        public HotelRoomFacRec()
        {
        }

        int? _RecID;
        public int? RecID { get { return _RecID; } set { _RecID = value; } }

        string _Hotel;
        public string Hotel { get { return _Hotel; } set { _Hotel = value; } }

        string _Room;
        public string Room { get { return _Room; } set { _Room = value; } }

        string _RoomName;
        public string RoomName { get { return _RoomName; } set { _RoomName = value; } }

        string _RoomNameL;
        public string RoomNameL { get { return _RoomNameL; } set { _RoomNameL = value; } }

        int? _FacNo;
        public int? FacNo { get { return _FacNo; } set { _FacNo = value; } }

        string _FacilityName;
        public string FacilityName { get { return _FacilityName; } set { _FacilityName = value; } }

        string _FacilityNameL;
        public string FacilityNameL { get { return _FacilityNameL; } set { _FacilityNameL = value; } }

        int? _Unit;
        public int? Unit { get { return _Unit; } set { _Unit = value; } }

        string _Priced;
        public string Priced { get { return _Priced; } set { _Priced = value; } }

        int? _DispNo;
        public int? DispNo { get { return _DispNo; } set { _DispNo = value; } }

        string _Note;
        public string Note { get { return _Note; } set { _Note = value; } }

        string _NoteLocalName;
        public string NoteLocalName { get { return _NoteLocalName; } set { _NoteLocalName = value; } }
    }

    public class HotelProperty
    {
        public HotelProperty()
        {
            _HotelFacility = new List<HotelFacilityRecord>();
            _HotelRoom = new List<HotelRoomRecord>();
            _HotelBoard = new List<HotelBoardRecord>();
            _HotelAccom = new List<HotelAccomRecord>();
            _HotelRoomFacility = new List<HotelRoomFacRec>();
        }

        List<HotelFacilityRecord> _HotelFacility;
        public List<HotelFacilityRecord> HotelFacility
        {
            get { return _HotelFacility; }
            set { _HotelFacility = value; }
        }

        List<HotelRoomRecord> _HotelRoom;
        public List<HotelRoomRecord> HotelRoom
        {
            get { return _HotelRoom; }
            set { _HotelRoom = value; }
        }

        List<HotelBoardRecord> _HotelBoard;
        public List<HotelBoardRecord> HotelBoard
        {
            get { return _HotelBoard; }
            set { _HotelBoard = value; }
        }

        List<HotelAccomRecord> _HotelAccom;
        public List<HotelAccomRecord> HotelAccom
        {
            get { return _HotelAccom; }
            set { _HotelAccom = value; }
        }

        List<HotelRoomFacRec> _HotelRoomFacility;
        public List<HotelRoomFacRec> HotelRoomFacility
        {
            get { return _HotelRoomFacility; }
            set { _HotelRoomFacility = value; }
        }
    }

    public class HotelTextCat
    {
        public HotelTextCat()
        {
        }

        int? _RecId;
        public int? RecId { get { return _RecId; } set { _RecId = value; } }

        string _Code;
        public string Code { get { return _Code; } set { _Code = value; } }

        string _Name;
        public string Name { get { return _Name; } set { _Name = value; } }

        string _NameL;
        public string NameL { get { return _NameL; } set { _NameL = value; } }
    }

    public class HotelPictTextRecord
    {
        public HotelPictTextRecord()
        {
        }
        int? _RecID;
        public int? RecID { get { return _RecID; } set { _RecID = value; } }
        int? _PicID;
        public int? PicID { get { return _PicID; } set { _PicID = value; } }
        string _Hotel;
        public string Hotel { get { return _Hotel; } set { _Hotel = value; } }
        string _Market;
        public string Market { get { return _Market; } set { _Market = value; } }
        string _PicText;
        public string PicText { get { return _PicText; } set { _PicText = value; } }
        string _TextCat;
        public string TextCat { get { return _TextCat; } set { _TextCat = value; } }
        string _TextCatName;
        public string TextCatName { get { return _TextCatName; } set { _TextCatName = value; } }
        string _TextCatNameL;
        public string TextCatNameL { get { return _TextCatNameL; } set { _TextCatNameL = value; } }
        string _Status;
        public string Status { get { return _Status; } set { _Status = value; } }
        string _TextType;
        public string TextType { get { return _TextType; } set { _TextType = value; } }
    }

    public class HotelPictRecord
    {
        public HotelPictRecord()
        {
        }
        int? _RecId;
        public int? RecId { get { return _RecId; } set { _RecId = value; } }
        string _Hotel;
        public string Hotel { get { return _Hotel; } set { _Hotel = value; } }
        Int16? _DispNo;
        public Int16? DispNo { get { return _DispNo; } set { _DispNo = value; } }
        string _Status;
        public string Status { get { return _Status; } set { _Status = value; } }
        DateTime? _CrtDate;
        public DateTime? CrtDate { get { return _CrtDate; } set { _CrtDate = value; } }
        long? _CrtDateJ;
        public long? CrtDateJ { get { return _CrtDateJ; } set { _CrtDateJ = value; } }
        string _CrtUser;
        public string CrtUser { get { return _CrtUser; } set { _CrtUser = value; } }
        DateTime? _ChgDate;
        public DateTime? ChgDate { get { return _ChgDate; } set { _ChgDate = value; } }
        long? _ChgDateJ;
        public long? ChgDateJ { get { return _ChgDateJ; } set { _ChgDateJ = value; } }
        string _ChgUser;
        public string ChgUser { get { return _ChgUser; } set { _ChgUser = value; } }
        string _imgPath;
        public string imgPath { get { return _imgPath; } set { _imgPath = value; } }
    }

    public class HotelAirportRecord
    {
        public HotelAirportRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _AirPort;
        public string AirPort
        {
            get { return _AirPort; }
            set { _AirPort = value; }
        }

        int? _Distance;
        public int? Distance
        {
            get { return _Distance; }
            set { _Distance = value; }
        }
    }

    public class HotelImageUrl
    {
        public string Market { get; set; }
        public string Url { get; set; }

        public HotelImageUrl()
        {
        }
    }

    public class HotelPresTextWidthCategory
    {
        public string Hotel { get; set; }
        public string TextCatName { get; set; }
        public string TextCatLocalName { get; set; }
        public string PresText { get; set; }

        public HotelPresTextWidthCategory()
        {
        }
    }

    public class RoomViewRecord
    {
        public int? RecID { get; set; }
        public string Name { get; set; }
        public string Explanation { get; set; }
        public RoomViewRecord()
        {

        }
    }

    public class HotelDispOrderRecord
    {
        public int? RecID { get; set; }
        public string Operator { get; set; }
        public string Market { get; set; }
        public string Hotel { get; set; }
        public Int16? DispNo { get; set; }
        public HotelDispOrderRecord()
        {
        }
    }

    public class HotelCatRecord
    {
        public int? RecId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public Int16? DispOrderWeb { get; set; }
        public Int16? StarCount { get; set; }
        public bool? PxmRec { get; set; }
        public HotelCatRecord()
        {

        }
    }
}
