﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;
using TvTools;
using System.ComponentModel;
using System.Configuration;
using System.Security.Cryptography;

namespace TvBo
{
    public class Common
    {
        #region International Country List
        public static string IntCountryList =
 @"[{'CountryName':'Afghanistan','IntCode':'AF','CountryPhoneCode':'93','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Albania','IntCode':'AL','CountryPhoneCode':'355','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Algeria','IntCode':'DZ','CountryPhoneCode':'213','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'American Samoa','IntCode':'AS','CountryPhoneCode':'1 684','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Andorra','IntCode':'AD','CountryPhoneCode':'376','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Angola','IntCode':'AO','CountryPhoneCode':'244','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Anguilla','IntCode':'AI','CountryPhoneCode':'1 264','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Antarctica','IntCode':'AQ','CountryPhoneCode':'672','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Antigua & Barbuda','IntCode':'AG','CountryPhoneCode':'1 268','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Antilles, Netherlands','IntCode':'AN','CountryPhoneCode':'599','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Arabia, Saudi','IntCode':'SA','CountryPhoneCode':'966','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Argentina','IntCode':'AR','CountryPhoneCode':'54','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Armenia','IntCode':'AM','CountryPhoneCode':'374','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Aruba','IntCode':'AW','CountryPhoneCode':'297','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Australia','IntCode':'AU','CountryPhoneCode':'61','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Austria','IntCode':'AT','CountryPhoneCode':'43','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Azerbaijan','IntCode':'AZ','CountryPhoneCode':'994','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Bahamas, The','IntCode':'BS','CountryPhoneCode':'1 242','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Bahrain','IntCode':'BH','CountryPhoneCode':'973','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Bangladesh','IntCode':'BD','CountryPhoneCode':'880','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Barbados','IntCode':'BB','CountryPhoneCode':'1 246','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Belarus','IntCode':'BY','CountryPhoneCode':'375','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Belgium','IntCode':'BE','CountryPhoneCode':'32','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Belize','IntCode':'BZ','CountryPhoneCode':'501','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Benin','IntCode':'BJ','CountryPhoneCode':'229','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Bermuda','IntCode':'BM','CountryPhoneCode':'1 441','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Bhutan','IntCode':'BT','CountryPhoneCode':'975','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Bolivia','IntCode':'BO','CountryPhoneCode':'591','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Bosnia and Herzegovina','IntCode':'BA','CountryPhoneCode':'387','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Botswana','IntCode':'BW','CountryPhoneCode':'267','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Brazil','IntCode':'BR','CountryPhoneCode':'55','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'British Virgin Islands','IntCode':'VG','CountryPhoneCode':'1 284','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Brunei Darussalam','IntCode':'BN','CountryPhoneCode':'673','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Bulgaria','IntCode':'BG','CountryPhoneCode':'359','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Burkina Faso','IntCode':'BF','CountryPhoneCode':'226','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Burundi','IntCode':'BI','CountryPhoneCode':'257','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Cambodia','IntCode':'KH','CountryPhoneCode':'855','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Cameroon','IntCode':'CM','CountryPhoneCode':'237','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Canada','IntCode':'CA','CountryPhoneCode':'1','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Cape Verde','IntCode':'CV','CountryPhoneCode':'238','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Cayman Islands','IntCode':'KY','CountryPhoneCode':'1 345','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Central African Republic','IntCode':'CF','CountryPhoneCode':'236','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Chad','IntCode':'TD','CountryPhoneCode':'235','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Chile','IntCode':'CL','CountryPhoneCode':'56','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'China','IntCode':'CN','CountryPhoneCode':'86','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Christmas Island','IntCode':'CX','CountryPhoneCode':'61','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Cocos (Keeling) Islands','IntCode':'CC','CountryPhoneCode':'61','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Colombia','IntCode':'CO','CountryPhoneCode':'57','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Comoros','IntCode':'KM','CountryPhoneCode':'269','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Congo','IntCode':'CG','CountryPhoneCode':'242','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Congo, Dem. Rep. of the','IntCode':'CD','CountryPhoneCode':'243','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Cook Islands','IntCode':'CK','CountryPhoneCode':'682','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Costa Rica','IntCode':'CR','CountryPhoneCode':'506','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Cote D Ivoire','IntCode':'CI','CountryPhoneCode':'225','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Croatia','IntCode':'HR','CountryPhoneCode':'385','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Cuba','IntCode':'CU','CountryPhoneCode':'53','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Cyprus','IntCode':'CY','CountryPhoneCode':'357','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Czech Republic','IntCode':'CZ','CountryPhoneCode':'420','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Denmark','IntCode':'DK','CountryPhoneCode':'45','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Djibouti','IntCode':'DJ','CountryPhoneCode':'253','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Dominica','IntCode':'DM','CountryPhoneCode':'1 767','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Dominican Republic','IntCode':'DO','CountryPhoneCode':'1 809','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Ecuador','IntCode':'EC','CountryPhoneCode':'593','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Egypt','IntCode':'EG','CountryPhoneCode':'20','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'El Salvador','IntCode':'SV','CountryPhoneCode':'503','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Equatorial Guinea','IntCode':'GQ','CountryPhoneCode':'240','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Eritrea','IntCode':'ER','CountryPhoneCode':'291','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Estonia','IntCode':'EE','CountryPhoneCode':'372','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Ethiopia','IntCode':'ET','CountryPhoneCode':'251','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Falkland Islands (Malvinas)','IntCode':'FK','CountryPhoneCode':'500','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Faroe Islands','IntCode':'FO','CountryPhoneCode':'298','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Fiji','IntCode':'FJ','CountryPhoneCode':'679','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Finland','IntCode':'FI','CountryPhoneCode':'358','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'France','IntCode':'FR','CountryPhoneCode':'33','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'French Polynesia','IntCode':'PF','CountryPhoneCode':'689','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Gabon','IntCode':'GA','CountryPhoneCode':'241','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Gambia, the','IntCode':'GM','CountryPhoneCode':'220','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Georgia','IntCode':'GE','CountryPhoneCode':'995','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Germany','IntCode':'DE','CountryPhoneCode':'49','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Ghana','IntCode':'GH','CountryPhoneCode':'233','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Gibraltar','IntCode':'GI','CountryPhoneCode':'350','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Greece','IntCode':'GR','CountryPhoneCode':'30','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Greenland','IntCode':'GL','CountryPhoneCode':'299','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Grenada','IntCode':'GD','CountryPhoneCode':'1 473','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Guam','IntCode':'GU','CountryPhoneCode':'1 671','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Guatemala','IntCode':'GT','CountryPhoneCode':'502','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Guinea','IntCode':'GN','CountryPhoneCode':'224','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Guinea-Bissau','IntCode':'GW','CountryPhoneCode':'245','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Guyana','IntCode':'GY','CountryPhoneCode':'592','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Haiti','IntCode':'HT','CountryPhoneCode':'509','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Holy See (Vatican)','IntCode':'VA','CountryPhoneCode':'39','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Honduras','IntCode':'HN','CountryPhoneCode':'504','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Hong Kong, (China)','IntCode':'HK','CountryPhoneCode':'852','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Hungary','IntCode':'HU','CountryPhoneCode':'36','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Iceland','IntCode':'IS','CountryPhoneCode':'354','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'India','IntCode':'IN','CountryPhoneCode':'91','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Indonesia','IntCode':'ID','CountryPhoneCode':'62','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Iran, Islamic Republic of','IntCode':'IR','CountryPhoneCode':'98','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Iraq','IntCode':'IQ','CountryPhoneCode':'964','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Ireland','IntCode':'IE','CountryPhoneCode':'353','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Israel','IntCode':'IL','CountryPhoneCode':'972','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Italy','IntCode':'IT','CountryPhoneCode':'39','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Ivory Coast (Cote d Ivoire)','IntCode':'CI','CountryPhoneCode':'225','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Jamaica','IntCode':'JM','CountryPhoneCode':'1 876','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Japan','IntCode':'JP','CountryPhoneCode':'81','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Jordan','IntCode':'JO','CountryPhoneCode':'962','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Kazakhstan','IntCode':'KZ','CountryPhoneCode':'7','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Kenya','IntCode':'KE','CountryPhoneCode':'254','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Kiribati','IntCode':'KI','CountryPhoneCode':'686','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Korea Dem. People s Rep.','IntCode':'KP','CountryPhoneCode':'850','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Korea, (South) Republic of','IntCode':'KR','CountryPhoneCode':'82','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Kosovo','IntCode':'KV','CountryPhoneCode':'381','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Kuwait','IntCode':'KW','CountryPhoneCode':'965','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Kyrgyzstan','IntCode':'KG','CountryPhoneCode':'996','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Lao People s Democ. Rep.','IntCode':'LA','CountryPhoneCode':'856','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Latvia','IntCode':'LV','CountryPhoneCode':'371','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Lebanon','IntCode':'LB','CountryPhoneCode':'961','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Lesotho','IntCode':'LS','CountryPhoneCode':'266','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Liberia','IntCode':'LR','CountryPhoneCode':'231','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Libyan Arab Jamahiriya','IntCode':'LY','CountryPhoneCode':'218','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Liechtenstein','IntCode':'LI','CountryPhoneCode':'423','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Lithuania','IntCode':'LT','CountryPhoneCode':'370','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Luxembourg','IntCode':'LU','CountryPhoneCode':'352','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Macao, (China)','IntCode':'MO','CountryPhoneCode':'853','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Macedonia, TFYR','IntCode':'MK','CountryPhoneCode':'389','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Madagascar','IntCode':'MG','CountryPhoneCode':'261','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Malawi','IntCode':'MW','CountryPhoneCode':'265','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Malaysia','IntCode':'MY','CountryPhoneCode':'60','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Maldives','IntCode':'MV','CountryPhoneCode':'960','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Mali','IntCode':'ML','CountryPhoneCode':'223','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Malta','IntCode':'MT','CountryPhoneCode':'356','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Man, Isle of','IntCode':'IM','CountryPhoneCode':'44','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Marshall Islands','IntCode':'MH','CountryPhoneCode':'692','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Mauritania','IntCode':'MR','CountryPhoneCode':'222','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Mauritius','IntCode':'MU','CountryPhoneCode':'230','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Mayotte (FR)','IntCode':'YT','CountryPhoneCode':'262','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Mexico','IntCode':'MX','CountryPhoneCode':'52','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Micronesia, Fed. States of','IntCode':'FM','CountryPhoneCode':'691','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Moldova, Republic of','IntCode':'MD','CountryPhoneCode':'373','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Monaco','IntCode':'MC','CountryPhoneCode':'377','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Mongolia','IntCode':'MN','CountryPhoneCode':'976','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Montserrat','IntCode':'MS','CountryPhoneCode':'1 664','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Morocco','IntCode':'MA','CountryPhoneCode':'212','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Mozambique','IntCode':'MZ','CountryPhoneCode':'258','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Myanmar (ex-Burma)','IntCode':'MM','CountryPhoneCode':'95','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Namibia','IntCode':'NA','CountryPhoneCode':'264','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Nauru','IntCode':'NR','CountryPhoneCode':'674','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Nepal','IntCode':'NP','CountryPhoneCode':'977','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Netherlands','IntCode':'NL','CountryPhoneCode':'31','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Netherlands Antilles','IntCode':'AN','CountryPhoneCode':'599','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'New Caledonia','IntCode':'NC','CountryPhoneCode':'687','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'New Zealand','IntCode':'NZ','CountryPhoneCode':'64','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Nicaragua','IntCode':'NI','CountryPhoneCode':'505','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Niger','IntCode':'NE','CountryPhoneCode':'227','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Nigeria','IntCode':'NG','CountryPhoneCode':'234','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Niue','IntCode':'NU','CountryPhoneCode':'683','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Northern Mariana Islands','IntCode':'MP','CountryPhoneCode':'1 670','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Norway','IntCode':'NO','CountryPhoneCode':'47','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Oman','IntCode':'OM','CountryPhoneCode':'968','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Pakistan','IntCode':'PK','CountryPhoneCode':'92','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Palau','IntCode':'PW','CountryPhoneCode':'680','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Panama','IntCode':'PA','CountryPhoneCode':'507','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Papua New Guinea','IntCode':'PG','CountryPhoneCode':'675','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Paraguay','IntCode':'PY','CountryPhoneCode':'595','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Peru','IntCode':'PE','CountryPhoneCode':'51','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Philippines','IntCode':'PH','CountryPhoneCode':'63','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Pitcairn Island','IntCode':'PN','CountryPhoneCode':'870','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Poland','IntCode':'PL','CountryPhoneCode':'48','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Portugal','IntCode':'PT','CountryPhoneCode':'351','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Puerto Rico','IntCode':'PR','CountryPhoneCode':'1','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Qatar','IntCode':'QA','CountryPhoneCode':'974','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Romania','IntCode':'RO','CountryPhoneCode':'40','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Russia (Russian Fed.)','IntCode':'RU','CountryPhoneCode':'7','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Rwanda','IntCode':'RW','CountryPhoneCode':'250','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Saint Barthelemy (FR)','IntCode':'BL','CountryPhoneCode':'590','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Saint Helena (UK)','IntCode':'SH','CountryPhoneCode':'290','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Saint Kitts and Nevis','IntCode':'KN','CountryPhoneCode':'1 869','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Saint Lucia','IntCode':'LC','CountryPhoneCode':'1 758','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Saint Martin (FR)','IntCode':'MF','CountryPhoneCode':'1 599','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'S Pierre & Miquelon (FR)','IntCode':'PM','CountryPhoneCode':'508','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'S Vincent & Grenadines','IntCode':'VC','CountryPhoneCode':'1 784','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Samoa','IntCode':'WS','CountryPhoneCode':'685','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'San Marino','IntCode':'SM','CountryPhoneCode':'378','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Sao Tome and Principe','IntCode':'ST','CountryPhoneCode':'239','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Saudi Arabia','IntCode':'SA','CountryPhoneCode':'966','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Senegal','IntCode':'SN','CountryPhoneCode':'221','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Serbia','IntCode':'RS','CountryPhoneCode':'381','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Seychelles','IntCode':'SC','CountryPhoneCode':'248','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Sierra Leone','IntCode':'SL','CountryPhoneCode':'232','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Singapore','IntCode':'SG','CountryPhoneCode':'65','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Slovakia','IntCode':'SK','CountryPhoneCode':'421','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Slovenia','IntCode':'SI','CountryPhoneCode':'386','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Solomon Islands','IntCode':'SB','CountryPhoneCode':'677','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Somalia','IntCode':'SO','CountryPhoneCode':'252','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'South Africa','IntCode':'ZA','CountryPhoneCode':'27','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Spain','IntCode':'ES','CountryPhoneCode':'34','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Sri Lanka (ex-Ceilan)','IntCode':'LK','CountryPhoneCode':'94','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Sudan','IntCode':'SD','CountryPhoneCode':'249','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Suriname','IntCode':'SR','CountryPhoneCode':'597','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Swaziland','IntCode':'SZ','CountryPhoneCode':'268','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Sweden','IntCode':'SE','CountryPhoneCode':'46','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Switzerland','IntCode':'CH','CountryPhoneCode':'41','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Syrian Arab Republic','IntCode':'SY','CountryPhoneCode':'963','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Taiwan','IntCode':'TW','CountryPhoneCode':'886','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Tajikistan','IntCode':'TJ','CountryPhoneCode':'992','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Tanzania, United Rep. of','IntCode':'TZ','CountryPhoneCode':'255','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Thailand','IntCode':'TH','CountryPhoneCode':'66','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Timor-Leste (East Timor)','IntCode':'TL','CountryPhoneCode':'670','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Togo','IntCode':'TG','CountryPhoneCode':'228','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Tokelau','IntCode':'TK','CountryPhoneCode':'690','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Tonga','IntCode':'TO','CountryPhoneCode':'676','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Trinidad & Tobago','IntCode':'TT','CountryPhoneCode':'1 868','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Tunisia','IntCode':'TN','CountryPhoneCode':'216','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Turkey','IntCode':'TR','CountryPhoneCode':'90','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Turkmenistan','IntCode':'TM','CountryPhoneCode':'993','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Turks and Caicos Islands','IntCode':'TC','CountryPhoneCode':'1 649','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Tuvalu','IntCode':'TV','CountryPhoneCode':'688','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Uganda','IntCode':'UG','CountryPhoneCode':'256','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Ukraine','IntCode':'UA','CountryPhoneCode':'380','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'United Arab Emirates','IntCode':'AE','CountryPhoneCode':'971','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'United Kingdom','IntCode':'UK','CountryPhoneCode':'44','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'United States','IntCode':'US','CountryPhoneCode':'1','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Uruguay','IntCode':'UY','CountryPhoneCode':'598','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Uzbekistan','IntCode':'UZ','CountryPhoneCode':'998','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Vanuatu','IntCode':'VU','CountryPhoneCode':'678','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Vatican (Holy See)','IntCode':'VA','CountryPhoneCode':'39','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Venezuela','IntCode':'VE','CountryPhoneCode':'58','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Viet Nam','IntCode':'VN','CountryPhoneCode':'84','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Virgin Islands, British','IntCode':'VG','CountryPhoneCode':'1 284','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Virgin Islands, U.S.','IntCode':'VI','CountryPhoneCode':'1 340','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Wallis and Futuna','IntCode':'WF','CountryPhoneCode':'681','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Yemen','IntCode':'YE','CountryPhoneCode':'967','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Zambia','IntCode':'ZM','CountryPhoneCode':'260','PhoneMask':'','PhoneMaskMob':''},
{'CountryName':'Zimbabwe','IntCode':'ZW','CountryPhoneCode':'263','PhoneMask':'','PhoneMaskMob':''}]";
        #endregion

        public static string crID_DemoAtila = "1555901";
        public static string crID_Paximum = "0962301";
        public static string crID_Qasswa = "1550501";
        public static string crID_Kusadasi_Ro = "0962301";
        public static string crID_Fibula_Ro = "0855201";
        public static string crID_Novaturas_Lt = "0855101";
        public static string crID_Mng_Tr = "0860901";
        public static string crID_Detur = "0970301";
        public static string crID_Safiran = "0969201";
        public static string crID_Jolly = "0973001";
        public static string crID_Sunrise = "0969801";
        public static string crID_TopTours_EE = "1075001";
        public static string crID_Anex = "0970801";
        public static string crID_Orex = "1210601";
        public static string crID_Aqua_Bg = "1076901";
        public static string crID_Vas_Al = "0970201";
        public static string crID_Karma_EG = "1081001";
        public static string crID_NTravel = "1188101"; // tourinfo son olarak era oldu, yeni mumara yetid lazım "0962301" ama bu kuşadası numarası.
        public static string crID_SummerTour = "1188101"; // Yukarıdakinin aynısı başvuru karışmaması amacıyla eklendi
        public static string crID_FitTurizm = "1076601";
        public static string crID_Magellan = "0852601";
        public static string crID_Azur = "1196901";
        public static string crID_Rezeda = "1198101";
        public static string crID_Qualitum = "1195901";
        public static string crID_BigBlue = "1190101";
        public static string crID_Mahbal = "1198801";
        public static string crID_Marine = "1210401";
        public static string crID_Milana = "1220701";
        public static string crID_Karnak = "0302501";
        public static string crID_Jazz = "1434701";//"0849401"; 2017 Nisan itibariyle farklı durumda
        public static string crID_Elsenal = "1444101"; //"0313501"; 2014 Haziranda değişmiş
        public static string crID_Quheilan = "1212801";
        public static string crID_Emerald = "1192601";
        public static string crID_Go2Holiday = "1220301";
        public static string crID_Go2HolidayBg = "1194201";
        public static string crID_Calsedon = "1324401";
        public static string crID_Kenba = "1327801";
        public static string crID_UpJet = "0525501";
        public static string crID_FamilyTour = "1331001";
        public static string crID_AliBabaTour = "1221201";
        public static string crID_PliusTravel = "1438701";
        public static string crID_Karavan = "0748201";
        public static string crID_HolidayPlus = "1327701";
        public static string crID_TourPlus = "0521301";
        public static string crID_YekTravel = "1442001";
        public static string crID_CelexTravel = "1194601";
        public static string crID_FilipTravel = "1444501";
        public static string crID_Doris = "1210301";
        public static string crID_HouseTravel = "1444401";
        public static string crID_ZemExpert = "1433301";
        public static string crID_Apollo = "0972601";
        public static string crID_Dallas = "1545501";
        public static string crID_RoyalPersia = "1550201";
        public static string crID_Calypso = "1323201";
        public static string crID_Magidos = "1435601";
        public static string crID_AtlasGlobal = "1547001";
        public static string crID_SunFun = "1552401";
        public static string crID_SunOrient = "1553501";
        public static string crID_ViyaTurizm = "1551901";
        public static string crID_Enka = "1554101";
        public static string crID_MbnTour_Ir = "1550701";
        public static string crID_MbnTour_Tr = "1433901";
        public static string crID_Laluz = "0313301";
        public static string crID_Kartaca = "0417201";
        public static string crID_KidyTour = "1555501";
        public static string crID_SeaTravel = "0966301";
        public static string crID_TransTeo = "1660301";
        public static string crID_DreamHolidays = "1548101";
        public static string crID_TahaVoyages = "1440901";
        public static string crID_BalkanHolidays = "1553701";
        public static string ctID_TivronaTours = "1552901";
        public static string ctID_BTBTour = "1663801";
        public static string ctID_Falcon = "1772101";
        public static string ctID_TandemMayak = "1774501";
        public static string ctID_NeedTour = "1776501";
        public static string ctID_Zershan = "1770701";
        public static string ctID_DuaTour = "1770001";
        public static string ctID_BelPreGo = "1883801";
        public static string ctID_VoyageLtd = "1992501";


        public List<AgencyRoles> getAgencyRoles(string agencyID, ref string errorMsg)
        {
            List<AgencyRoles> records = new List<AgencyRoles>();
            string tsql = "Exec dbo.usp_GetB2BRole @AgencyID";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "AgencyID", DbType.AnsiString, agencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(new AgencyRoles
                        {
                            RoleID = Conversion.getInt32OrNull(R["RoleID"]),
                            Description = Conversion.getStrOrNull(R["Description"]),
                            Category = Conversion.getStrOrNull(R["Category"]),
                            Authority = (bool)R["Authority"],
                            OrderNo = Conversion.getInt16OrNull(R["OrderNo"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Int16> getB2BRoles(ref string errorMsg)
        {
            List<Int16> records = new List<Int16>();
            string tsql =
@"
Select RoleID From B2BRoles (NOLOCK)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add((Int16)R["RoleID"]);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool updateRole(Int16? pRoleID, string pDesc, bool pDefaultValue, string pCategory, int? pOrderNo, ref string errorMsg)
        {
            string tsql =
@"
if Exists(Select null From B2BRoles (NOLOCK) Where RoleID=@pRoleID)
Begin
  Update B2BRoles
  Set  Description=@pDesc,
       DefaultValue=@pDefaultValue,
       Category=@pCategory,
       OrderNo=@pOrderNo
  Where RoleID=@pRoleID
End
Else
Begin
  Insert Into B2BRoles (RoleID,[Description],DefaultValue,Category,OrderNo)
  Values (@pRoleID,@pDesc,@pDefaultValue,@pCategory,@pOrderNo)

--  if (@pDefaultValue = 1)
--  Begin
--   if (not Exists(Select null From B2BAgencyRoles (NOLOCK) Where RoleID=@pRoleID))
--   Begin
--    Insert Into B2BAgencyRoles (RoleID)
--	VALUES (@pRoleID) 
--   End
--
--   Insert Into B2BAgencyRoles (AgencyID,RoleID)
--	Select RecID,@pRoleID From Agency (NOLOCK)
--	Where not Exists(Select null From B2BAgencyRoles (NOLOCK) Where AgencyID=RecID)	  
--  End
End
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pRoleID", DbType.Int16, pRoleID);
                db.AddInParameter(dbCommand, "pDesc", DbType.String, pDesc);
                db.AddInParameter(dbCommand, "pDefaultValue", DbType.Boolean, pDefaultValue);
                db.AddInParameter(dbCommand, "pCategory", DbType.AnsiString, pCategory);
                db.AddInParameter(dbCommand, "pOrderNo", DbType.Int32, pOrderNo);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool eraseRules(Int16 role, ref string errorMsg)
        {
            string tsql =
@"
Delete B2BRoles
Where RoleID=@pRoleID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pRoleID", DbType.Int16, (Int16)role);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool updateRoles()
        {
            string errorMsg = string.Empty;

            List<activeRules> activeB2BRole = Newtonsoft.Json.JsonConvert.DeserializeObject<List<activeRules>>(Conversion.getStrOrNull(getFormConfigValue("AgencyRoles", "ActiveRoles")));
            if (activeB2BRole == null)
                return true;
            List<Int16> oldRules = getB2BRoles(ref errorMsg);

            List<dbInformationShema> b2bRolesTable = VersionControl.getTableShema("B2BRoles", ref errorMsg);
            if (b2bRolesTable == null || b2bRolesTable.Count == 0)
                return true;
            List<B2BRoles> roles = new List<B2BRoles>();
            foreach (B2BRoles role in Enum.GetValues(typeof(B2BRoles)))
            {
                FieldInfo fieldInfo = role.GetType().GetField(role.ToString());
                B2BRoleAttribute attribs = (B2BRoleAttribute)fieldInfo.GetCustomAttributes(typeof(B2BRoleAttribute), false).FirstOrDefault();
                if (attribs != null)
                {
                    string attr = attribs.Description;
                    bool? defaultVal = Conversion.getBoolOrNull(attribs.Default);
                    FieldInfo catFieldInfo = attribs.Category.GetType().GetField(role.ToString());
                    string category = attribs.Category.GetEnumDescription();
                    int? orderNo = Conversion.getInt32OrNull(attribs.Order);
                    short? rolesId = Conversion.getInt16OrNull(role.GetHashCode());
                    activeRules roleIsActive = activeB2BRole.Find(f => f.RoleID == (rolesId.HasValue ? rolesId.Value : -1));
                    if (roleIsActive != null)
                    {
                        if (!updateRole(rolesId.Value, attr, roleIsActive != null ? roleIsActive.Default : defaultVal.Value, category, orderNo, ref errorMsg))
                        {
                            return false;
                        }
                        roles.Add((B2BRoles)rolesId.Value);
                    }
                }
            }

            foreach (var row in oldRules)
            {
                if (!roles.Contains((B2BRoles)row))
                {
                    eraseRules((Int16)row, ref errorMsg);
                }
            }

            return true;
        }

        public static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }
            string filenName = AppDomain.CurrentDomain.BaseDirectory + "Log\\ErrLog.txt";
            FileStream f = new FileStream(filenName, FileMode.OpenOrCreate, FileAccess.Write);
            try
            {
                StreamWriter writer = new StreamWriter(f);
                writer.Write(stringBuilder.ToString());
                writer.Close();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                f.Close();
            }
            return stringBuilder.ToString();
        }

        public static string getDBOwner()
        {
            string tsql = @"Select CustomRegID From ParamSystem (NOLOCK) Where Market=''";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static string getTvVersion(ref string errorMsg)
        {
            string tsql = @"Select [Version] From ParamSystem (NOLOCK) Where Market=''";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static string getWebVersion(ref string errorMsg)
        {
            string tsql = @"Select WVersion From ParamSystem (NOLOCK) Where Market=''";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static Int16? getAge(DateTime? _bDay, DateTime? _tDay)
        {
            if (!_bDay.HasValue || !_tDay.HasValue)
                return null;

            int bday = _bDay.Value.Day;
            int bmo = _bDay.Value.Month;
            int byr = _bDay.Value.Year;

            int age;

            int tday = _tDay.Value.Day;
            int tmo = _tDay.Value.Month;
            int tyr = _tDay.Value.Year;

            age = tyr - byr;
            if (tmo < bmo || (tmo == bmo && tday < bday))
                age -= 1;
            return Convert.ToInt16(age.ToString());
        }

        public string getDateFormatRegion(CultureInfo Ci)
        {
            List<regionRecord> lang = new List<regionRecord>();
            lang.Add(new regionRecord { Region = "tr", Year = "y", Month = "a", Day = "g" });
            lang.Add(new regionRecord { Region = "en", Year = "y", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "sq", Year = "v", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "hy", Year = "տ", Month = "ա", Day = "օ" });
            lang.Add(new regionRecord { Region = "az", Year = "i", Month = "a", Day = "g" });
            lang.Add(new regionRecord { Region = "eu", Year = "u", Month = "h", Day = "e" });
            lang.Add(new regionRecord { Region = "be", Year = "г", Month = "м", Day = "д" });
            lang.Add(new regionRecord { Region = "bg", Year = "г", Month = "м", Day = "д" });
            lang.Add(new regionRecord { Region = "ca", Year = "a", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "cs", Year = "l", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "da", Year = "å", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "nl", Year = "j", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "et", Year = "a", Month = "k", Day = "p" });
            lang.Add(new regionRecord { Region = "fi", Year = "v", Month = "k", Day = "p" });
            lang.Add(new regionRecord { Region = "fr", Year = "a", Month = "m", Day = "j" });
            lang.Add(new regionRecord { Region = "gl", Year = "a", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "ka", Year = "წ", Month = "თ", Day = "დ" });
            lang.Add(new regionRecord { Region = "de", Year = "j", Month = "m", Day = "t" });
            lang.Add(new regionRecord { Region = "el", Year = "έ", Month = "μ", Day = "η" });
            lang.Add(new regionRecord { Region = "he", Year = "ש", Month = "ח", Day = "י" });
            lang.Add(new regionRecord { Region = "hi", Year = "व", Month = "मा", Day = "दि" });
            lang.Add(new regionRecord { Region = "hu", Year = "é", Month = "h", Day = "n" });
            lang.Add(new regionRecord { Region = "is", Year = "á", Month = "h", Day = "n" });
            lang.Add(new regionRecord { Region = "id", Year = "t", Month = "b", Day = "h" });
            lang.Add(new regionRecord { Region = "ga", Year = "b", Month = "m", Day = "l" });
            lang.Add(new regionRecord { Region = "it", Year = "a", Month = "m", Day = "g" });
            lang.Add(new regionRecord { Region = "lv", Year = "g", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "lt", Year = "y", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "mk", Year = "г", Month = "м", Day = "д" });
            lang.Add(new regionRecord { Region = "ms", Year = "t", Month = "b", Day = "h" });
            lang.Add(new regionRecord { Region = "mt", Year = "s", Month = "x", Day = "f" });
            lang.Add(new regionRecord { Region = "no", Year = "å", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "nb", Year = "å", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "pl", Year = "l", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "pt", Year = "a", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "ro", Year = "a", Month = "l", Day = "z" });
            lang.Add(new regionRecord { Region = "ru", Year = "г", Month = "м", Day = "д" });
            lang.Add(new regionRecord { Region = "sr", Year = "г", Month = "м", Day = "д" });
            lang.Add(new regionRecord { Region = "sk", Year = "r", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "sl", Year = "l", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "es", Year = "a", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "sv", Year = "å", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "uk", Year = "р", Month = "м", Day = "д" });
            lang.Add(new regionRecord { Region = "vi", Year = "n", Month = "t", Day = "n" });
            lang.Add(new regionRecord { Region = "cy", Year = "b", Month = "m", Day = "d" });
            lang.Add(new regionRecord { Region = "ar", Year = "y", Month = "m", Day = "d" });

            string dateFormat = new TvBo.Common().getDateFormat(Ci);
            regionRecord eng = lang.Find(f => f.Region == "en");
            regionRecord current = lang.Find(f => f.Region == Ci.Name.Substring(0, Ci.Name.LastIndexOf('-')));

            return current != null ? getDateFormatStrRegion(Ci, current) : getDateFormatStrRegion(Ci, eng);
        }

        public string getDateFormatStrRegion(CultureInfo Ci, regionRecord region)
        {
            string shortDatePattern = strFunc.Trim(Ci.DateTimeFormat.ShortDatePattern, ' ');
            string dateSeperator = Ci.DateTimeFormat.DateSeparator[0].ToString();
            if (Ci.Name.ToLower() == "lt-lt")
                dateSeperator = ".";
            string[] _format = shortDatePattern.Split(Convert.ToChar(dateSeperator));

            string _dateFormat = string.Empty;
            for (int i = 0; i < _format.Length; i++)
            {
                if (!string.IsNullOrEmpty(_format[i].ToString()))
                    switch (_format[i].ToString().ToLower()[0])
                    {
                        case 'd':
                            _dateFormat += string.Format("/{0}{0}", region.Day);
                            break;
                        case 'm':
                            _dateFormat += string.Format("/{0}{0}", region.Month);
                            break;
                        case 'y':
                            _dateFormat += string.Format("/{0}{0}{0}{0}", region.Year);
                            break;
                        default:
                            break;
                    }
            }
            _dateFormat = _dateFormat.Remove(0, 1);
            return _dateFormat;
        }

        public static object DeepClone(object obj)
        {
            object objResult = null;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);

                ms.Position = 0;
                objResult = bf.Deserialize(ms);
            }
            return objResult;
        }

        public string getDateFormat(CultureInfo Ci)
        {
            string shortDatePattern = strFunc.Trim(Ci.DateTimeFormat.ShortDatePattern, ' ');
            string dateSeperator = Ci.DateTimeFormat.DateSeparator[0].ToString();
            if (Ci.Name.ToLower() == "lt-lt")
                dateSeperator = ".";
            string[] _format = shortDatePattern.Split(Convert.ToChar(dateSeperator));

            string _dateFormat = string.Empty;
            for (int i = 0; i < _format.Length; i++)
            {
                if (!string.IsNullOrEmpty(_format[i].ToString()))
                {
                    switch (_format[i].ToString().ToLower()[0])
                    {
                        case 'd':
                            _dateFormat += "/dd";
                            break;
                        case 'm':
                            _dateFormat += "/MM";
                            break;
                        case 'y':
                            _dateFormat += "/yyyy";
                            break;
                        default:
                            break;
                    }
                }
            }
            _dateFormat = _dateFormat.Remove(0, 1);
            return _dateFormat;
        }

        public string getDateFormatOrg(CultureInfo Ci)
        {
            string shortDatePattern = strFunc.Trim(Ci.DateTimeFormat.ShortDatePattern, ' ');
            string dateSeperator = Ci.DateTimeFormat.DateSeparator[0].ToString();
            if (Ci.Name.ToLower() == "lt-lt")
                dateSeperator = ".";
            string[] _format = shortDatePattern.Split(Convert.ToChar(dateSeperator));

            string _dateFormat = string.Empty;
            for (int i = 0; i < _format.Length; i++)
            {
                if (!string.IsNullOrEmpty(_format[i].ToString()))
                {
                    switch (_format[i].ToString().ToLower()[0])
                    {
                        case 'd':
                            _dateFormat += Ci.DateTimeFormat.DateSeparator[0] + "dd";
                            break;
                        case 'm':
                            _dateFormat += Ci.DateTimeFormat.DateSeparator[0] + "MM";
                            break;
                        case 'y':
                            _dateFormat += Ci.DateTimeFormat.DateSeparator[0] + "yyyy";
                            break;
                        default:
                            break;
                    }
                }
            }
            _dateFormat = _dateFormat.Remove(0, 1);
            return _dateFormat;
        }

        public string getDateMask(CultureInfo Ci)
        {
            string shortDatePattern = strFunc.Trim(Ci.DateTimeFormat.ShortDatePattern, ' ');

            string[] _format = shortDatePattern.Split(Ci.DateTimeFormat.DateSeparator[0]);

            string _dateFormat = string.Empty;
            for (int i = 0; i < _format.Length; i++)
            {
                switch (_format[i].ToString().ToLower()[0])
                {
                    case 'd':
                        _dateFormat += "/99";
                        break;
                    case 'm':
                        _dateFormat += "/99";
                        break;
                    case 'y':
                        _dateFormat += "/9999";
                        break;
                }
            }
            _dateFormat = _dateFormat.Remove(0, 1);
            return _dateFormat;
        }

        public static string getSubString(string values, int lenght)
        {
            string retVal = string.Empty;
            if (!string.IsNullOrEmpty(values))
                retVal = values.Length > lenght ? values.Substring(0, lenght) : values;
            return retVal;
        }

        public static string EncodeJsString(string s)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\'':
                        sb.Append("\\'");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            return sb.ToString();
        }

        public string logUserCreate(User UserData)
        {
            string tsql = string.Empty;
            tsql += "if OBJECT_ID('TempDB.dbo.#TVUser') is not null Drop Table dbo.#TVUser ";
            tsql += "CREATE TABLE dbo.#TVUser (Agency varchar(10), UserID varchar(10)) ";
            tsql += string.Format("INSERT INTO dbo.#TVUser (Agency, UserID) VALUES ('{0}','{1}') ", UserData.AgencyID, UserData.UserID);
            return tsql;
        }

        public object fieldValues(string FieldType, string FieldValue)
        {
            object retVal = null;
            switch (FieldType.ToLower())
            {
                case "string":
                    retVal = FieldValue;
                    break;
                case "bool":
                    retVal = Convert.ToBoolean(FieldValue.ToLower());
                    break;
                case "short":
                    retVal = Convert.ToInt16(FieldValue);
                    break;
                case "int":
                    retVal = Convert.ToInt32(FieldValue);
                    break;
                case "long":
                    retVal = Convert.ToInt64(FieldValue);
                    break;
                case "double":
                    retVal = Convert.ToDouble(FieldValue);
                    break;
                case "decimal":
                    retVal = Convert.ToDecimal(FieldValue);
                    break;
                case "char":
                    retVal = Convert.ToChar(FieldValue);
                    break;
                case "byte":
                    retVal = Convert.ToByte(FieldValue);
                    break;
                case "datetime":
                    retVal = Convert.ToDateTime(FieldValue);
                    break;
                case "json":
                    retVal = FieldValue.ToString();
                    break;
                default:
                    retVal = FieldValue.ToString();
                    break;
            }
            return retVal;
        }

        public string createLogUser(User UserData)
        {
            string tsql = string.Empty;
            tsql += "if OBJECT_ID('TempDB.dbo.#TVUser') is not null Drop Table dbo.#TVUser \n";
            tsql += "CREATE TABLE dbo.#TVUser (Agency varchar(10), UserID varchar(10)) \n";
            tsql += "Insert Into dbo.#TVUser(Agency, UserID) \n";
            tsql += string.Format(" Values('{0}','{1}') \n", UserData.AgencyID, UserData.UserID);
            return tsql;
        }

        public DataRow LINQToDataRow<T>(IEnumerable<T> source, DataRow target)
        {
            PropertyInfo[] oProps = null;
            if (target == null)
                return null;
            if (source == null)
                return null;
            DataTable dt = target.Table;
            dt.Rows.Clear();
            target = dt.NewRow();
            try
            {
                foreach (T rec in source)
                {
                    // Use reflection to get property names, to create table, Only first time, others follow;
                    if (oProps == null)
                    {
                        oProps = rec.GetType().GetProperties();
                        foreach (PropertyInfo pi in oProps)
                        {
                            Type colType = pi.PropertyType;
                            if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                                colType = colType.GetGenericArguments()[0];
                            if (dt.Columns.Contains(pi.Name))
                                target[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
                        }
                    }
                    else
                    {
                        foreach (PropertyInfo pi in oProps)
                            if (dt.Columns.Contains(pi.Name))
                                target[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
                    }
                }
                dt.Rows.Add(target);
                return dt.Rows[0];
            }
            catch
            {
                return null;
            }
        }

        public DataRow LINQToDataRow<T>(IEnumerable<T> varlist)
        {
            using (DataTable dtReturn = new DataTable())
            {
                // column names 
                PropertyInfo[] oProps = null;
                if (varlist == null)
                    return null;
                foreach (T rec in varlist)
                {
                    // Use reflection to get property names, to create table, Only first time, others follow;
                    if (oProps == null)
                    {
                        oProps = rec.GetType().GetProperties();
                        foreach (PropertyInfo pi in oProps)
                        {
                            Type colType = pi.PropertyType;
                            if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                                colType = colType.GetGenericArguments()[0];
                            dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                        }
                    }
                    DataRow dr = dtReturn.NewRow();
                    foreach (PropertyInfo pi in oProps)
                        dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
                    dtReturn.Rows.Add(dr);
                }
                if (dtReturn != null && dtReturn.Rows.Count > 0)
                    return dtReturn.Rows[0];
                else
                    return null;
            }
        }

        public DataTable LINQToDataTable<T>(IEnumerable<T> varlist)
        {
            using (DataTable dtReturn = new DataTable())
            {
                // column names 
                PropertyInfo[] oProps = null;
                if (varlist == null)
                    return dtReturn;
                foreach (T rec in varlist)
                {
                    // Use reflection to get property names, to create table, Only first time, others follow;
                    if (oProps == null)
                    {
                        oProps = rec.GetType().GetProperties();
                        foreach (PropertyInfo pi in oProps)
                        {
                            Type colType = pi.PropertyType;
                            if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                                colType = colType.GetGenericArguments()[0];
                            dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                        }
                    }
                    DataRow dr = dtReturn.NewRow();
                    foreach (PropertyInfo pi in oProps)
                        dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
                    dtReturn.Rows.Add(dr);
                }
                return dtReturn;
            }
        }

        public DataTable ReadXmlDataTable(string FileName, ref string errorMsg)
        {
            try
            {
                DataTable DataTable1 = new DataTable();
                DataTable1.ReadXml(FileName);
                DataTable1.AcceptChanges();
                return DataTable1;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
        }

        public DataSet ReadXmlDataSet(string FileName, ref string errorMsg)
        {
            try
            {
                DataSet DataSet1 = new DataSet();
                DataSet1.ReadXml(FileName);
                DataSet1.AcceptChanges();
                return DataSet1;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
        }

        public DataSet getTvParametersDataSet(string Market, decimal? TvVersion, string WebVersion, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql = @"   Declare @Version VarChar(9), @WVersion VarChar(8)
                        Select @Version=Version, @WVersion=WVersion From ParamSystem (NOLOCK) Where  Market = '' ";

            #region ParamSystem
            if (VersionControl.CheckWebVersion(WebVersion, VersionControl.WebWersion0013, VersionControl.Equality.gt))
                tsql += @" if (Exists(Select RecID From ParamSystem Where UseSysParam = 'Y' And Market = @Market))
	                            Select RecID, Market, UseSysParam, BaseCur, BaseCurDec, LocalLangID, SysLangID, AutoChgKBLayout, BaseBank, BuyRate, 
		                            SaleRate, PaymentRate, RateDateOpt, ExChgOpt, DefPrinter, DefFaxPrinter, FirmLogoBig, FirmLogoMed, LogLevel, 
		                            LogKeepDay, KeyCase, [Version]=@Version, Bonus_AgencyMaxPerc, Bonus_UserMaxPerc, Bonus_PassMaxPerc, Bonus_Version, Bonus_UseSysParam,
		                            VC_CheckType, VC_SharePoint, VC_FtpUser, VC_FtpPass, VC_ProxyName, VC_ProxyUser, VC_ProxyPass, VC_BackUpdate, VC_ShowDlg, VC_CheckStartup,
		                            CustomRegID, SMTPServer, SMTPPort, SMTPAccount, SMTPPass,SMTPUseSSL, PrtTicketDayOpt, PrtVoucherDayOpt, PrtInsuranceDayOpt, PrtContractDayOpt, 
		                            PrtTicketDayOptW, PrtVoucherDayOptW, PrtInsuranceDayOptW, PrtContractDayOptW, TvCCServer, SMTPauth, Bonus_EarnForUse, WVersion=@WVersion, 
		                            Bonus_AgencyMinLimit, Bonus_UserMinLimit, Bonus_PassMinLimit, SystemAdminMails, AutoGetRate, ImpRateSaveBank, ImpRateChgBuyPer, 
		                            ImpRateChgSalePer, BirthdayMsgSend, BirtdayMsgSubject, SejourServerName, SejourDBName,TVCC_OptionDate 
	                            From ParamSystem (NOLOCK) Where  Market=''
                            Else
	                            Select RecID, Market, UseSysParam, BaseCur, BaseCurDec, LocalLangID, SysLangID, AutoChgKBLayout, BaseBank, BuyRate, 
		                            SaleRate, PaymentRate, RateDateOpt, ExChgOpt, DefPrinter, DefFaxPrinter, FirmLogoBig, FirmLogoMed, LogLevel, 
		                            LogKeepDay, KeyCase, [Version]=@Version, Bonus_AgencyMaxPerc, Bonus_UserMaxPerc, Bonus_PassMaxPerc, Bonus_Version, Bonus_UseSysParam,
		                            VC_CheckType, VC_SharePoint, VC_FtpUser, VC_FtpPass, VC_ProxyName, VC_ProxyUser, VC_ProxyPass, VC_BackUpdate, VC_ShowDlg, VC_CheckStartup,
		                            CustomRegID, SMTPServer, SMTPPort, SMTPAccount, SMTPPass,SMTPUseSSL, PrtTicketDayOpt, PrtVoucherDayOpt, PrtInsuranceDayOpt, PrtContractDayOpt, 
		                            PrtTicketDayOptW, PrtVoucherDayOptW, PrtInsuranceDayOptW, PrtContractDayOptW, TvCCServer, SMTPauth, Bonus_EarnForUse, WVersion=@WVersion, 
		                            Bonus_AgencyMinLimit, Bonus_UserMinLimit, Bonus_PassMinLimit, SystemAdminMails, AutoGetRate, ImpRateSaveBank, ImpRateChgBuyPer, 
		                            ImpRateChgSalePer, BirthdayMsgSend, BirtdayMsgSubject, SejourServerName, SejourDBName,TVCC_OptionDate 
	                            From ParamSystem (NOLOCK) Where  Market=@Market ";
            else
                tsql += @" if (Exists(Select RecID From ParamSystem Where UseSysParam = 'Y' And Market = @Market))
	                            Select RecID, Market, UseSysParam, BaseCur, BaseCurDec, LocalLangID, SysLangID, AutoChgKBLayout, BaseBank, BuyRate, 
		                            SaleRate, PaymentRate, RateDateOpt, ExChgOpt, DefPrinter, DefFaxPrinter, FirmLogoBig, FirmLogoMed, LogLevel, 
		                            LogKeepDay, KeyCase, [Version]=@Version, Bonus_AgencyMaxPerc, Bonus_UserMaxPerc, Bonus_PassMaxPerc, Bonus_Version, Bonus_UseSysParam,
		                            VC_CheckType, VC_SharePoint, VC_FtpUser, VC_FtpPass, VC_ProxyName, VC_ProxyUser, VC_ProxyPass, VC_BackUpdate, VC_ShowDlg, VC_CheckStartup,
		                            CustomRegID, SMTPServer, SMTPPort, SMTPAccount, SMTPPass,SMTPUseSSL, PrtTicketDayOpt, PrtVoucherDayOpt, PrtInsuranceDayOpt, PrtContractDayOpt, 
		                            PrtTicketDayOptW, PrtVoucherDayOptW, PrtInsuranceDayOptW, PrtContractDayOptW, TvCCServer, SMTPauth, Bonus_EarnForUse, WVersion=@WVersion, 
		                            Bonus_AgencyMinLimit, Bonus_UserMinLimit, Bonus_PassMinLimit, SystemAdminMails, AutoGetRate, ImpRateSaveBank, ImpRateChgBuyPer, 
		                            ImpRateChgSalePer, BirthdayMsgSend, BirtdayMsgSubject, SejourServerName='', SejourDBName='',TVCC_OptionDate
	                            From ParamSystem (NOLOCK) Where  Market=''
                            Else
	                            Select RecID, Market, UseSysParam, BaseCur, BaseCurDec, LocalLangID, SysLangID, AutoChgKBLayout, BaseBank, BuyRate, 
		                            SaleRate, PaymentRate, RateDateOpt, ExChgOpt, DefPrinter, DefFaxPrinter, FirmLogoBig, FirmLogoMed, LogLevel, 
		                            LogKeepDay, KeyCase, [Version]=@Version, Bonus_AgencyMaxPerc, Bonus_UserMaxPerc, Bonus_PassMaxPerc, Bonus_Version, Bonus_UseSysParam,
		                            VC_CheckType, VC_SharePoint, VC_FtpUser, VC_FtpPass, VC_ProxyName, VC_ProxyUser, VC_ProxyPass, VC_BackUpdate, VC_ShowDlg, VC_CheckStartup,
		                            CustomRegID, SMTPServer, SMTPPort, SMTPAccount, SMTPPass,SMTPUseSSL, PrtTicketDayOpt, PrtVoucherDayOpt, PrtInsuranceDayOpt, PrtContractDayOpt, 
		                            PrtTicketDayOptW, PrtVoucherDayOptW, PrtInsuranceDayOptW, PrtContractDayOptW, TvCCServer, SMTPauth, Bonus_EarnForUse, WVersion=@WVersion, 
		                            Bonus_AgencyMinLimit, Bonus_UserMinLimit, Bonus_PassMinLimit, SystemAdminMails, AutoGetRate, ImpRateSaveBank, ImpRateChgBuyPer, 
		                            ImpRateChgSalePer, BirthdayMsgSend, BirtdayMsgSubject, SejourServerName='', SejourDBName='',TVCC_OptionDate
	                            From ParamSystem (NOLOCK) Where  Market=@Market ";

            #endregion

            #region Param Reser
            if (TvVersion >= Convert.ToDecimal("050022163"))
                tsql += @"  if (Exists(Select null From ParamReser Where UseSysParam='Y' And Market=@Market))
                                Select  PrS.RecID,PrS.Market,PrS.UseSysParam,Pr.NameRules,Pr.AdultBirtDayChk,Pr.ChildBirtDayChk,Pr.MinAdultAge,Pr.MaxInfAge,Pr.MaxChdAge,
                                        Pr.PassReqRes,Pr.AllowDupResCtrl,Pr.DupResSurName,Pr.DupResName,Pr.DupResBirthDay,Pr.DupResPass,Pr.DupResIdNo,Pr.DupResBetPer,Pr.DupResBetPerDay,
                                        Pr.HotAllotChk,Pr.HotAllotWarnFull,Pr.HotAllotNotAcceptFull,Pr.HotAllotNotFullGA,Pr.WarnMaxDay,PrS.AutoLockChk,PrS.AutoLockMode, 
                                        PrS.AutoLockDay,PrS.AutoLockDayAfBef,PrS.AgencyMailResChg,PrS.AgencyMailResChgConf,PrS.AgencyMailFlgChg,PrS.AgencyMailFlgChgConf, 
                                        PrS.AgencyMailCommentChg,PrS.AgencyMailCommentChgConf,Pr.PayRuleOfCancel,Pr.MaxRoomCnt,Pr.AllowBlackPasRes,PrS.SendOptForPrint, 
                                        PrS.SendOptForExcel,Pr.AllowDupOTicCtrl,Pr.ExtSerConfToResConf,Pr.AllowMinAge1,Pr.AllowMinAge2,Pr.ChdPassChk,PrS.B2B_NeedAddress,PrS.B2C_NeedAddress, 
                                        Pr.NROnCX_Visa,Pr.NROnCX_CanIns,Pr.NROnCX_StdIns,PrS.BonusPasUseW,PrS.BonusAgencyUseW,PrS.BonusUserUseW,PrS.BonusAgencySeeOwnW,PrS.BonusUserSeeOwnW,PrS.BonusUserSeeAgencyW,
                                        Pr.MinChgCountForFee,Pr.MaxPaxCnt,Pr.PassIssueReqRes,Pr.ChkResChgforFee,PrS.LeaderEMail,Pr.AgeCalcType,PrS.MaxChdAgeB2Bsrc,PrS.SendNotMailResCancelB2B,PrS.HoneymoonChkW,
                                        Pr.NeedPhone, Pr.NeedPIN,Pr.AdultBirtDayWebChk,PrS.AllowNoNameTV,PrS.AllowNoNameB2B,PrS.AllowNoNameB2C
                                From ParamReser Pr (NOLOCK), ParamReser PrS (NOLOCK)
                                Where Pr.Market='' And PrS.Market=@Market
                            Else 
                                Select	RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
                                        PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
                                        HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
                                        AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
                                        AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
                                        SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
                                        NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
                                        MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType, MaxChdAgeB2Bsrc,SendNotMailResCancelB2B, HoneymoonChkW, NeedPhone, NeedPIN,AdultBirtDayWebChk,
                                        AllowNoNameTV,AllowNoNameB2B,AllowNoNameB2C
                                From ParamReser (NOLOCK) Where  Market = @Market ";
            else if (TvVersion > Convert.ToDecimal("040073123"))
                tsql += @"  if (Exists(Select RecID From ParamReser Where UseSysParam = 'Y' And Market = @Market))
                                Select  PrS.RecID,PrS.Market,PrS.UseSysParam,Pr.NameRules,Pr.AdultBirtDayChk,Pr.ChildBirtDayChk,Pr.MinAdultAge,Pr.MaxInfAge,Pr.MaxChdAge,
                                        Pr.PassReqRes,Pr.AllowDupResCtrl,Pr.DupResSurName,Pr.DupResName,Pr.DupResBirthDay,Pr.DupResPass,Pr.DupResIdNo,Pr.DupResBetPer,Pr.DupResBetPerDay,
                                        Pr.HotAllotChk,Pr.HotAllotWarnFull,Pr.HotAllotNotAcceptFull,Pr.HotAllotNotFullGA,Pr.WarnMaxDay,PrS.AutoLockChk,PrS.AutoLockMode, 
                                        PrS.AutoLockDay,PrS.AutoLockDayAfBef,PrS.AgencyMailResChg,PrS.AgencyMailResChgConf,PrS.AgencyMailFlgChg,PrS.AgencyMailFlgChgConf, 
                                        PrS.AgencyMailCommentChg,PrS.AgencyMailCommentChgConf,Pr.PayRuleOfCancel,Pr.MaxRoomCnt,Pr.AllowBlackPasRes,PrS.SendOptForPrint, 
                                        PrS.SendOptForExcel,Pr.AllowDupOTicCtrl,Pr.ExtSerConfToResConf,Pr.AllowMinAge1,Pr.AllowMinAge2,Pr.ChdPassChk,PrS.B2B_NeedAddress,PrS.B2C_NeedAddress, 
                                        Pr.NROnCX_Visa,Pr.NROnCX_CanIns,Pr.NROnCX_StdIns,PrS.BonusPasUseW,PrS.BonusAgencyUseW,PrS.BonusUserUseW,PrS.BonusAgencySeeOwnW,PrS.BonusUserSeeOwnW,PrS.BonusUserSeeAgencyW,
                                        Pr.MinChgCountForFee,Pr.MaxPaxCnt,Pr.PassIssueReqRes,Pr.ChkResChgforFee,PrS.LeaderEMail,Pr.AgeCalcType,PrS.MaxChdAgeB2Bsrc,PrS.SendNotMailResCancelB2B,PrS.HoneymoonChkW,
                                        Pr.NeedPIN,Pr.AdultBirtDayWebChk
                                From ParamReser Pr (NOLOCK), ParamReser PrS (NOLOCK)
                                Where Pr.Market='' And PrS.Market=@Market
                            Else 
                                Select	RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
                                        PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
                                        HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
                                        AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
                                        AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
                                        SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
                                        NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
                                        MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType, MaxChdAgeB2Bsrc,SendNotMailResCancelB2B, HoneymoonChkW, NeedPIN,AdultBirtDayWebChk
                                From ParamReser (NOLOCK) Where  Market = @Market ";
            else if (TvVersion > Convert.ToDecimal("040016043"))
                tsql += @"  if (Exists(Select RecID From ParamReser Where UseSysParam = 'Y' And Market = @Market))
                                Select  PrS.RecID,PrS.Market,PrS.UseSysParam,Pr.NameRules,Pr.AdultBirtDayChk,Pr.ChildBirtDayChk,Pr.MinAdultAge,Pr.MaxInfAge,Pr.MaxChdAge,
                                        Pr.PassReqRes,Pr.AllowDupResCtrl,Pr.DupResSurName,Pr.DupResName,Pr.DupResBirthDay,Pr.DupResPass,Pr.DupResIdNo,Pr.DupResBetPer,Pr.DupResBetPerDay,
                                        Pr.HotAllotChk,Pr.HotAllotWarnFull,Pr.HotAllotNotAcceptFull,Pr.HotAllotNotFullGA,Pr.WarnMaxDay,PrS.AutoLockChk,PrS.AutoLockMode, 
                                        PrS.AutoLockDay,PrS.AutoLockDayAfBef,PrS.AgencyMailResChg,PrS.AgencyMailResChgConf,PrS.AgencyMailFlgChg,PrS.AgencyMailFlgChgConf, 
                                        PrS.AgencyMailCommentChg,PrS.AgencyMailCommentChgConf,Pr.PayRuleOfCancel,Pr.MaxRoomCnt,Pr.AllowBlackPasRes,PrS.SendOptForPrint, 
                                        PrS.SendOptForExcel,Pr.AllowDupOTicCtrl,Pr.ExtSerConfToResConf,Pr.AllowMinAge1,Pr.AllowMinAge2,Pr.ChdPassChk,PrS.B2B_NeedAddress,PrS.B2C_NeedAddress, 
                                        Pr.NROnCX_Visa,Pr.NROnCX_CanIns,Pr.NROnCX_StdIns,PrS.BonusPasUseW,PrS.BonusAgencyUseW,PrS.BonusUserUseW,PrS.BonusAgencySeeOwnW,PrS.BonusUserSeeOwnW,PrS.BonusUserSeeAgencyW,
                                        Pr.MinChgCountForFee,Pr.MaxPaxCnt,Pr.PassIssueReqRes,Pr.ChkResChgforFee,PrS.LeaderEMail,Pr.AgeCalcType,PrS.MaxChdAgeB2Bsrc,PrS.SendNotMailResCancelB2B,PrS.HoneymoonChkW,Pr.NeedPIN
                                From ParamReser Pr (NOLOCK), ParamReser PrS (NOLOCK)
                                Where Pr.Market='' And PrS.Market=@Market
                            Else 
                                Select	RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
                                        PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
                                        HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
                                        AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
                                        AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
                                        SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
                                        NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
                                        MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType, MaxChdAgeB2Bsrc,SendNotMailResCancelB2B, HoneymoonChkW, NeedPIN
                                From ParamReser (NOLOCK) Where  Market = @Market ";
            else if (VersionControl.CheckWebVersion(WebVersion, VersionControl.WebWersion0030, VersionControl.Equality.gt))
                tsql += @"  if (Exists(Select RecID From ParamReser Where UseSysParam = 'Y' And Market = @Market))
                                Select  RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
	                                    PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
	                                    HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
	                                    AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
	                                    AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
	                                    SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
	                                    NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
	                                    MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType, MaxChdAgeB2Bsrc,SendNotMailResCancelB2B, HoneymoonChkW
                                From ParamReser (NOLOCK) Where  Market = ''
                            Else 
                                Select	RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
	                                    PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
	                                    HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
	                                    AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
	                                    AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
	                                    SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
	                                    NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
	                                    MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType, MaxChdAgeB2Bsrc,SendNotMailResCancelB2B, HoneymoonChkW
                                From ParamReser (NOLOCK) Where  Market = @Market ";
            else if (VersionControl.CheckWebVersion(WebVersion, VersionControl.WebWersion0023, VersionControl.Equality.gt))
                tsql += @"  if (Exists(Select RecID From ParamReser Where UseSysParam = 'Y' And Market = @Market))
                                Select  RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
	                                    PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
	                                    HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
	                                    AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
	                                    AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
	                                    SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
	                                    NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
	                                    MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType, MaxChdAgeB2Bsrc,SendNotMailResCancelB2B
                                From ParamReser (NOLOCK) Where  Market = ''
                            Else 
                                Select	RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
	                                    PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
	                                    HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
	                                    AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
	                                    AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
	                                    SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
	                                    NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
	                                    MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType, MaxChdAgeB2Bsrc,SendNotMailResCancelB2B
                                From ParamReser (NOLOCK) Where  Market = @Market ";
            else if (VersionControl.CheckWebVersion(WebVersion, VersionControl.WebWersion0021, VersionControl.Equality.gt))
                tsql += @"  if (Exists(Select RecID From ParamReser Where UseSysParam = 'Y' And Market = @Market))
                                Select  RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
	                                    PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
	                                    HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
	                                    AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
	                                    AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
	                                    SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
	                                    NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
	                                    MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType, MaxChdAgeB2Bsrc
                                From ParamReser (NOLOCK) Where  Market = ''
                            Else 
                                Select	RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
	                                    PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
	                                    HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
	                                    AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
	                                    AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
	                                    SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
	                                    NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
	                                    MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType, MaxChdAgeB2Bsrc
                                From ParamReser (NOLOCK) Where  Market = @Market ";
            else
                tsql += @"  if (Exists(Select RecID From ParamReser Where UseSysParam = 'Y' And Market = @Market))
                                Select  RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
	                                    PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
	                                    HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
	                                    AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
	                                    AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
	                                    SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
	                                    NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
	                                    MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType
                                From ParamReser (NOLOCK) Where  Market = ''
                            Else 
                                Select	RecID, Market, UseSysParam, NameRules, AdultBirtDayChk, ChildBirtDayChk, MinAdultAge, MaxInfAge, MaxChdAge,
	                                    PassReqRes, AllowDupResCtrl, DupResSurName, DupResName, DupResBirthDay, DupResPass, DupResIdNo, DupResBetPer, DupResBetPerDay,
	                                    HotAllotChk, HotAllotWarnFull, HotAllotNotAcceptFull, HotAllotNotFullGA, WarnMaxDay, AutoLockChk, AutoLockMode, 
	                                    AutoLockDay, AutoLockDayAfBef, AgencyMailResChg, AgencyMailResChgConf, AgencyMailFlgChg, AgencyMailFlgChgConf, 
	                                    AgencyMailCommentChg, AgencyMailCommentChgConf, PayRuleOfCancel, MaxRoomCnt, AllowBlackPasRes, SendOptForPrint, 
	                                    SendOptForExcel, AllowDupOTicCtrl, ExtSerConfToResConf, AllowMinAge1, AllowMinAge2, ChdPassChk, B2B_NeedAddress, B2C_NeedAddress, 
	                                    NROnCX_Visa, NROnCX_CanIns, NROnCX_StdIns, BonusPasUseW, BonusAgencyUseW, BonusUserUseW, BonusAgencySeeOwnW, BonusUserSeeOwnW, BonusUserSeeAgencyW,
	                                    MinChgCountForFee, MaxPaxCnt, PassIssueReqRes, ChkResChgforFee, LeaderEMail, AgeCalcType
                                From ParamReser (NOLOCK) Where  Market = @Market ";
            #endregion

            if (VersionControl.CheckWebVersion(WebVersion, VersionControl.WebWersion0016, VersionControl.Equality.gt))
                tsql += @"  if (Exists(Select RecID From ParamFlight Where UseSysParam = 'Y' And Market = @Market))
                                Select	UseSysParam, PrintOWasRT, VRTStat, OpenNVAday, ShowFlyDateOpt, AllotChk, AllotWarnFull, AllotDoNotOver, 
			                            UseOptForAllot, OptAllotDoNotOver, OTicSaleReqTel, RetFlightOpt, DispMinFlightAllot, DispMinFlightAllotW, MaxPaxonTicket,UseTicPriceForOTic
	                            From ParamFlight (NOLOCK) Where  Market = ''
                            Else 
                                Select	UseSysParam, PrintOWasRT, VRTStat, OpenNVAday, ShowFlyDateOpt, AllotChk, AllotWarnFull, AllotDoNotOver, 
			                            UseOptForAllot, OptAllotDoNotOver, OTicSaleReqTel, RetFlightOpt, DispMinFlightAllot, DispMinFlightAllotW, MaxPaxonTicket,UseTicPriceForOTic
	                            From ParamFlight (NOLOCK) Where  Market = @Market ";
            else
                tsql += @"  if (Exists(Select RecID From ParamFlight Where UseSysParam = 'Y' And Market = @Market))
                                Select	UseSysParam, PrintOWasRT, VRTStat, OpenNVAday, ShowFlyDateOpt, AllotChk, AllotWarnFull, AllotDoNotOver, 
			                            UseOptForAllot, OptAllotDoNotOver, OTicSaleReqTel, RetFlightOpt, DispMinFlightAllot, DispMinFlightAllotW,UseTicPriceForOTic
	                            From ParamFlight (NOLOCK) Where  Market = ''
                            Else 
                                Select	UseSysParam, PrintOWasRT, VRTStat, OpenNVAday, ShowFlyDateOpt, AllotChk, AllotWarnFull, AllotDoNotOver, 
			                            UseOptForAllot, OptAllotDoNotOver, OTicSaleReqTel, RetFlightOpt, DispMinFlightAllot, DispMinFlightAllotW,UseTicPriceForOTic
	                            From ParamFlight (NOLOCK) Where  Market = @Market ";

            tsql += @"  Select RoundPayment, PayMode, PayModeDay, AfBef, PayType, ResPayOver, AgencyResPayRule, PayOptTime, ColWithInvoice, PayPlanDateOpt 
                        From ParamPay (NOLOCK)
                        Where Market = @Market ";

            tsql += @"  if (Exists(Select RecID From ParamPrice Where UseSysParam = 'Y' And Market = @Market))
                            Select Pp.UseSysParam,Pp.RoundBuyPrice,Pp.RoundSalePrice,Pp.ChdGrp1Name,Pp.ChdGrp2Name,Pp.ChdGrp3Name,Pp.ChdGrp4Name, 
                                   Pp.ChdGrp1SName,Pp.ChdGrp2SName,Pp.ChdGrp3SName,Pp.ChdGrp4SName,Pp.SaleCalcType,Pp.LastPriceListOpt, 
                                   Pp.InvPrtNoPay,Pp.ResLockonInvoice,Pp.RoundCom,Pp.InvSumType,Pp.ResSalePriceOpt, 
                                   Pp.CreditMode,Pp.AllowMakeRes,PpS.PLAdlCap,PpS.PLChdCap,Pp.SaleCurOpt,Pp.CanInsIncStd,Pp.IndSaleCurOpt,Pp.PLSPOCalcType, 
                                   Pp.RoundType,Pp.RoundOpt,Pp.RndOpt1,Pp.RndOpt2
                            From ParamPrice Pp (NOLOCK), ParamPrice PpS (NOLOCK)
                            Where PP.Market='' And PpS.Market=@Market
                        Else
                            Select UseSysParam, RoundBuyPrice, RoundSalePrice, ChdGrp1Name, ChdGrp2Name, ChdGrp3Name, ChdGrp4Name, 
                                   ChdGrp1SName, ChdGrp2SName, ChdGrp3SName, ChdGrp4SName, SaleCalcType, LastPriceListOpt, 
                                   InvPrtNoPay, ResLockonInvoice, RoundCom, InvSumType, ResSalePriceOpt, 
                                   CreditMode, AllowMakeRes, PLAdlCap, PLChdCap, SaleCurOpt, CanInsIncStd, IndSaleCurOpt, PLSPOCalcType, 
                                   RoundType, RoundOpt, RndOpt1, RndOpt2 
                            From ParamPrice (NOLOCK)
                            Where Market = @Market ";

            tsql += @"  if (Exists(Select RecID From ParamTransport Where UseSysParam = 'Y' And Market = @Market))
	                        Select UseSysParam, AllotChk, AllotWarnFull, AllotDoNotOver 
	                        From ParamTransport (NOLOCK)
	                        Where Market = ''
                        Else
	                        Select UseSysParam, AllotChk, AllotWarnFull, AllotDoNotOver 
	                        From ParamTransport (NOLOCK)
	                        Where Market = @Market ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                ds.Tables[0].TableName = "ParamSystem";
                ds.Tables[1].TableName = "ParamReser";
                ds.Tables[2].TableName = "ParamFlight";
                ds.Tables[3].TableName = "ParamPay";
                ds.Tables[4].TableName = "ParamPrice";
                ds.Tables[5].TableName = "ParamTransport";
                return ds;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public TvParameters getTvParameters(string Market, decimal? TvVersion, string WebVersion, ref string errorMsg)
        {
            DataSet parameters = getTvParametersDataSet(Market, TvVersion, WebVersion, ref errorMsg);
            if (parameters != null)
            {
                TvParameters param = new TvParameters();
                DataRow rowParamSystem = parameters.Tables["ParamSystem"].Rows.Count > 0 ? parameters.Tables["ParamSystem"].Rows[0] : null;
                DataRow rowParamReser = parameters.Tables["ParamReser"].Rows.Count > 0 ? parameters.Tables["ParamReser"].Rows[0] : null;
                DataRow rowParamFlight = parameters.Tables["ParamFlight"].Rows.Count > 0 ? parameters.Tables["ParamFlight"].Rows[0] : null;
                DataRow rowParamPay = parameters.Tables["ParamPay"].Rows.Count > 0 ? parameters.Tables["ParamPay"].Rows[0] : null;
                DataRow rowParamPrice = parameters.Tables["ParamPrice"].Rows.Count > 0 ? parameters.Tables["ParamPrice"].Rows[0] : null;
                DataRow rowParamTransport = parameters.Tables["ParamTransport"].Rows.Count > 0 ? parameters.Tables["ParamTransport"].Rows[0] : null;

                #region ParamSystem
                if (rowParamSystem != null)
                    param.TvParamSystem = new ParamSystem
                    {
                        RecID = Conversion.getInt32OrNull(rowParamSystem["RecID"]),
                        Market = Conversion.getStrOrNull(rowParamSystem["Market"]),
                        UseSysParam = Conversion.getStrOrNull(rowParamSystem["UseSysParam"]),
                        BaseCur = Conversion.getStrOrNull(rowParamSystem["BaseCur"]),
                        BaseCurDec = Conversion.getInt16OrNull(rowParamSystem["BaseCurDec"]),
                        LocalLangID = Conversion.getInt32OrNull(rowParamSystem["LocalLangID"]),
                        SysLangID = Conversion.getInt32OrNull(rowParamSystem["SysLangID"]),
                        AutoChgKBLayout = Conversion.getStrOrNull(rowParamSystem["AutoChgKBLayout"]),
                        BaseBank = Conversion.getStrOrNull(rowParamSystem["BaseBank"]),
                        BuyRate = Conversion.getInt16OrNull(rowParamSystem["BuyRate"]),
                        SaleRate = Conversion.getInt16OrNull(rowParamSystem["SaleRate"]),
                        PaymentRate = Conversion.getInt16OrNull(rowParamSystem["PaymentRate"]),
                        RateDateOpt = Conversion.getInt16OrNull(rowParamSystem["RateDateOpt"]),
                        ExChgOpt = Conversion.getInt16OrNull(rowParamSystem["ExChgOpt"]),
                        DefPrinter = Conversion.getStrOrNull(rowParamSystem["DefPrinter"]),
                        DefFaxPrinter = Conversion.getStrOrNull(rowParamSystem["DefFaxPrinter"]),
                        LogLevel = Conversion.getInt16OrNull(rowParamSystem["LogLevel"]),
                        LogKeepDay = Conversion.getInt16OrNull(rowParamSystem["LogKeepDay"]),
                        KeyCase = Conversion.getInt16OrNull(rowParamSystem["KeyCase"]),
                        Version = Conversion.getStrOrNull(rowParamSystem["Version"]),
                        Bonus_AgencyMaxPerc = Conversion.getInt16OrNull(rowParamSystem["Bonus_AgencyMaxPerc"]),
                        Bonus_UserMaxPerc = Conversion.getInt16OrNull(rowParamSystem["Bonus_UserMaxPerc"]),
                        Bonus_PassMaxPerc = Conversion.getInt16OrNull(rowParamSystem["Bonus_PassMaxPerc"]),
                        Bonus_Version = Conversion.getStrOrNull(rowParamSystem["Bonus_Version"]),
                        Bonus_UseSysParam = Conversion.getStrOrNull(rowParamSystem["Bonus_UseSysParam"]),
                        VC_CheckType = Conversion.getByteOrNull(rowParamSystem["VC_CheckType"]),
                        VC_SharePoint = Conversion.getStrOrNull(rowParamSystem["VC_SharePoint"]),
                        VC_FtpUser = Conversion.getStrOrNull(rowParamSystem["VC_FtpUser"]),
                        VC_FtpPass = Conversion.getStrOrNull(rowParamSystem["VC_FtpPass"]),
                        VC_ProxyName = Conversion.getStrOrNull(rowParamSystem["VC_ProxyName"]),
                        VC_ProxyUser = Conversion.getStrOrNull(rowParamSystem["VC_ProxyUser"]),
                        VC_ProxyPass = Conversion.getStrOrNull(rowParamSystem["VC_ProxyPass"]),
                        VC_BackUpdate = Conversion.getStrOrNull(rowParamSystem["VC_BackUpdate"]),
                        VC_ShowDlg = Conversion.getStrOrNull(rowParamSystem["VC_ShowDlg"]),
                        VC_CheckStartup = Conversion.getStrOrNull(rowParamSystem["VC_CheckStartup"]),
                        CustomRegID = Conversion.getStrOrNull(rowParamSystem["CustomRegID"]),
                        SMTPServer = Conversion.getStrOrNull(rowParamSystem["SMTPServer"]),
                        SMTPPort = Conversion.getInt32OrNull(rowParamSystem["SMTPPort"]),
                        SMTPAccount = Conversion.getStrOrNull(rowParamSystem["SMTPAccount"]),
                        SMTPPass = Conversion.getStrOrNull(rowParamSystem["SMTPPass"]),
                        SMTPUseSSL = Conversion.getInt16OrNull(rowParamSystem["SMTPUseSSL"]),
                        PrtTicketDayOpt = Conversion.getInt16OrNull(rowParamSystem["PrtTicketDayOpt"]),
                        PrtVoucherDayOpt = Conversion.getInt16OrNull(rowParamSystem["PrtVoucherDayOpt"]),
                        PrtInsuranceDayOpt = Conversion.getInt16OrNull(rowParamSystem["PrtInsuranceDayOpt"]),
                        PrtContractDayOpt = Conversion.getInt16OrNull(rowParamSystem["PrtContractDayOpt"]),
                        PrtTicketDayOptW = Conversion.getInt16OrNull(rowParamSystem["PrtTicketDayOptW"]),
                        PrtVoucherDayOptW = Conversion.getInt16OrNull(rowParamSystem["PrtVoucherDayOptW"]),
                        PrtInsuranceDayOptW = Conversion.getInt16OrNull(rowParamSystem["PrtInsuranceDayOptW"]),
                        PrtContractDayOptW = Conversion.getInt16OrNull(rowParamSystem["PrtContractDayOptW"]),
                        TvCCServer = Conversion.getStrOrNull(rowParamSystem["TvCCServer"]),
                        SMTPauth = Conversion.getBoolOrNull(rowParamSystem["SMTPauth"]),
                        Bonus_EarnForUse = Conversion.getBoolOrNull(rowParamSystem["Bonus_EarnForUse"]),
                        WVersion = Conversion.getStrOrNull(rowParamSystem["WVersion"]),
                        Bonus_AgencyMinLimit = Conversion.getDecimalOrNull(rowParamSystem["Bonus_AgencyMinLimit"]),
                        Bonus_UserMinLimit = Conversion.getDecimalOrNull(rowParamSystem["Bonus_UserMinLimit"]),
                        Bonus_PassMinLimit = Conversion.getDecimalOrNull(rowParamSystem["Bonus_PassMinLimit"]),
                        SystemAdminMails = Conversion.getStrOrNull(rowParamSystem["SystemAdminMails"]),
                        AutoGetRate = Conversion.getBoolOrNull(rowParamSystem["AutoGetRate"]),
                        ImpRateSaveBank = Conversion.getStrOrNull(rowParamSystem["ImpRateSaveBank"]),
                        ImpRateChgBuyPer = Conversion.getDecimalOrNull(rowParamSystem["ImpRateChgBuyPer"]),
                        ImpRateChgSalePer = Conversion.getDecimalOrNull(rowParamSystem["ImpRateChgSalePer"]),
                        BirthdayMsgSend = Conversion.getBoolOrNull(rowParamSystem["BirthdayMsgSend"]),
                        BirtdayMsgSubject = Conversion.getStrOrNull(rowParamSystem["BirtdayMsgSubject"]),
                        SejourServerName = Conversion.getStrOrNull(rowParamSystem["SejourServerName"]),
                        SejourDBName = Conversion.getStrOrNull(rowParamSystem["SejourDBName"]),
                        TVCCOptionDate=Conversion.getBoolOrNull(rowParamSystem["TVCC_OptionDate"])
                    };

                #endregion ParamSystem

                #region ParamReser
                if (rowParamReser != null)
                    param.TvParamReser = new ParamReser
                    {
                        RecID = Conversion.getInt32OrNull(rowParamReser["RecID"]),
                        Market = Conversion.getStrOrNull(rowParamReser["Market"]),
                        UseSysParam = Equals(rowParamReser["UseSysParam"], "Y"),
                        NameRules = Conversion.getInt16OrNull(rowParamReser["NameRules"]),
                        AdultBirtDayChk = Equals(rowParamReser["AdultBirtDayChk"], "Y"),
                        AdultBirtDayWebChk = (TvVersion > Convert.ToDecimal("040073123")) ? (!string.IsNullOrEmpty(rowParamReser["AdultBirtDayWebChk"].ToString()) ? string.Equals(rowParamReser["AdultBirtDayWebChk"], "Y") : false) : false,
                        ChildBirtDayChk = Equals(rowParamReser["ChildBirtDayChk"], "Y"),
                        MinAdultAge = Conversion.getInt16OrNull(rowParamReser["MinAdultAge"]),
                        MaxInfAge = Conversion.getDecimalOrNull(rowParamReser["MaxInfAge"]),
                        MaxChdAge = Conversion.getDecimalOrNull(rowParamReser["MaxChdAge"]),
                        PassReqRes = Equals(rowParamReser["PassReqRes"], "Y"),
                        AllowDupResCtrl = Equals(rowParamReser["AllowDupResCtrl"], "Y"),
                        DupResSurName = Equals(rowParamReser["DupResSurName"], "Y"),
                        DupResName = Equals(rowParamReser["DupResName"], "Y"),
                        DupResBirthDay = Equals(rowParamReser["DupResBirthDay"], "Y"),
                        DupResPass = Equals(rowParamReser["DupResPass"], "Y"),
                        DupResIdNo = Equals(rowParamReser["DupResIdNo"], "Y"),
                        DupResBetPer = Equals(rowParamReser["DupResBetPer"], "Y"),
                        DupResBetPerDay = Conversion.getInt16OrNull(rowParamReser["DupResBetPerDay"]),
                        HotAllotChk = Equals(rowParamReser["HotAllotChk"], "Y"),
                        HotAllotWarnFull = Equals(rowParamReser["HotAllotWarnFull"], "Y"),
                        HotAllotNotAcceptFull = Equals(rowParamReser["HotAllotNotAcceptFull"], "Y"),
                        HotAllotNotFullGA = Equals(rowParamReser["HotAllotNotFullGA"], "Y"),
                        WarnMaxDay = Conversion.getInt16OrNull(rowParamReser["WarnMaxDay"]),
                        AutoLockChk = Equals(rowParamReser["AutoLockChk"], "Y"),
                        AutoLockMode = Conversion.getInt16OrNull(rowParamReser["AutoLockMode"]),
                        AutoLockDay = Conversion.getInt16OrNull(rowParamReser["AutoLockDay"]),
                        AutoLockDayAfBef = Conversion.getStrOrNull(rowParamReser["AutoLockDayAfBef"]),
                        AgencyMailResChg = Equals(rowParamReser["AgencyMailResChg"], "Y"),
                        AgencyMailResChgConf = Equals(rowParamReser["AgencyMailResChgConf"], "Y"),
                        AgencyMailFlgChg = Equals(rowParamReser["AgencyMailFlgChg"], "Y"),
                        AgencyMailFlgChgConf = Equals(rowParamReser["AgencyMailFlgChgConf"], "Y"),
                        AgencyMailCommentChg = Equals(rowParamReser["AgencyMailCommentChg"], "Y"),
                        AgencyMailCommentChgConf = Equals(rowParamReser["AgencyMailCommentChgConf"], "Y"),
                        PayRuleOfCancel = Conversion.getByteOrNull(rowParamReser["PayRuleOfCancel"]),
                        MaxRoomCnt = Conversion.getByteOrNull(rowParamReser["MaxRoomCnt"]),
                        AllowBlackPasRes = Conversion.getBoolOrNull(rowParamReser["AllowBlackPasRes"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["AllowBlackPasRes"]).Value : false,
                        SendOptForPrint = Conversion.getBoolOrNull(rowParamReser["SendOptForPrint"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["SendOptForPrint"]).Value : false,
                        SendOptForExcel = Conversion.getBoolOrNull(rowParamReser["SendOptForExcel"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["SendOptForExcel"]).Value : false,
                        AllowDupOTicCtrl = Conversion.getBoolOrNull(rowParamReser["AllowDupOTicCtrl"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["AllowDupOTicCtrl"]).Value : false,
                        ExtSerConfToResConf = Conversion.getBoolOrNull(rowParamReser["ExtSerConfToResConf"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["ExtSerConfToResConf"]).Value : false,
                        AllowMinAge1 = Conversion.getByteOrNull(rowParamReser["AllowMinAge1"]),
                        AllowMinAge2 = Conversion.getByteOrNull(rowParamReser["AllowMinAge2"]),
                        ChdPassChk = Conversion.getBoolOrNull(rowParamReser["ChdPassChk"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["ChdPassChk"]).Value : false,
                        B2B_NeedAddress = Conversion.getBoolOrNull(rowParamReser["B2B_NeedAddress"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["B2B_NeedAddress"]).Value : false,
                        B2C_NeedAddress = Conversion.getBoolOrNull(rowParamReser["B2C_NeedAddress"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["B2C_NeedAddress"]).Value : false,
                        NROnCX_Visa = Conversion.getBoolOrNull(rowParamReser["NROnCX_Visa"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["NROnCX_Visa"]).Value : false,
                        NROnCX_CanIns = Conversion.getBoolOrNull(rowParamReser["NROnCX_CanIns"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["NROnCX_CanIns"]).Value : false,
                        NROnCX_StdIns = Conversion.getBoolOrNull(rowParamReser["NROnCX_StdIns"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["NROnCX_StdIns"]).Value : false,
                        BonusPasUseW = Conversion.getBoolOrNull(rowParamReser["BonusPasUseW"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["BonusPasUseW"]).Value : false,
                        BonusAgencyUseW = Conversion.getBoolOrNull(rowParamReser["BonusAgencyUseW"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["BonusAgencyUseW"]).Value : false,
                        BonusUserUseW = Conversion.getBoolOrNull(rowParamReser["BonusUserUseW"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["BonusUserUseW"]).Value : false,
                        BonusAgencySeeOwnW = Conversion.getBoolOrNull(rowParamReser["BonusAgencySeeOwnW"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["BonusAgencySeeOwnW"]).Value : false,
                        BonusUserSeeOwnW = Conversion.getBoolOrNull(rowParamReser["BonusUserSeeOwnW"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["BonusUserSeeOwnW"]).Value : false,
                        BonusUserSeeAgencyW = Conversion.getBoolOrNull(rowParamReser["BonusUserSeeAgencyW"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["BonusUserSeeAgencyW"]).Value : false,
                        MinChgCountForFee = Conversion.getInt16OrNull(rowParamReser["MinChgCountForFee"]),
                        MaxPaxCnt = Conversion.getByteOrNull(rowParamReser["MaxPaxCnt"]),
                        PassIssueReqRes = Conversion.getBoolOrNull(rowParamReser["PassIssueReqRes"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["PassIssueReqRes"]).Value : false,
                        ChkResChgforFee = Conversion.getBoolOrNull(rowParamReser["ChkResChgforFee"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["ChkResChgforFee"]).Value : false,
                        LeaderEMail = Conversion.getBoolOrNull(rowParamReser["LeaderEMail"]).HasValue ? Conversion.getBoolOrNull(rowParamReser["LeaderEMail"]).Value : false,
                        AgeCalcType = Conversion.getByteOrNull(rowParamReser["AgeCalcType"]),
                        MaxChdAgeB2Bsrc = rowParamReser.Table.Columns.Contains("MaxChdAgeB2Bsrc") ? Conversion.getDecimalOrNull(rowParamReser["MaxChdAgeB2Bsrc"]) : null,
                        SendNotMailResCancelB2B = rowParamReser.Table.Columns.Contains("SendNotMailResCancelB2B") ? Conversion.getBoolOrNull(rowParamReser["SendNotMailResCancelB2B"]) : null,
                        HoneymoonChkW = rowParamReser.Table.Columns.Contains("HoneymoonChkW") ? Conversion.getBoolOrNull(rowParamReser["HoneymoonChkW"]) : null,
                        NeedPhone = rowParamReser.Table.Columns.Contains("NeedPhone") ? Conversion.getBoolOrNull(rowParamReser["NeedPhone"]) : null,
                        NeedPIN = rowParamReser.Table.Columns.Contains("NeedPIN") ? Conversion.getBoolOrNull(rowParamReser["NeedPIN"]) : null,
                        AllowNoNameTV = TvVersion >= Convert.ToDecimal("050022163") ? Conversion.getBoolOrNull(rowParamReser["AllowNoNameTV"]) : false,
                        AllowNoNameB2B = TvVersion >= Convert.ToDecimal("050022163") ? Conversion.getBoolOrNull(rowParamReser["AllowNoNameB2B"]) : false,
                        AllowNoNameB2C = TvVersion >= Convert.ToDecimal("050022163") ? Conversion.getBoolOrNull(rowParamReser["AllowNoNameB2C"]) : false
                    };

                #endregion

                #region ParamFlight
                if (rowParamFlight != null)
                    param.TvParamFlight = new ParamFlight
                    {
                        UseSysParam = Equals(rowParamFlight["UseSysParam"], "Y"),
                        PrintOWasRT = Equals(rowParamFlight["PrintOWasRT"], "Y"),
                        VRTStat = Conversion.getStrOrNull(rowParamFlight["VRTStat"]),
                        OpenNVAday = Conversion.getInt16OrNull(rowParamFlight["OpenNVAday"]),
                        ShowFlyDateOpt = Equals(rowParamFlight["ShowFlyDateOpt"], "Y"),
                        AllotChk = Equals(rowParamFlight["AllotChk"], "Y"),
                        AllotWarnFull = Equals(rowParamFlight["AllotWarnFull"], "Y"),
                        AllotDoNotOver = Equals(rowParamFlight["AllotDoNotOver"], "Y"),
                        UseOptForAllot = Equals(rowParamFlight["UseOptForAllot"], "Y"),
                        OptAllotDoNotOver = Equals(rowParamFlight["OptAllotDoNotOver"], "Y"),
                        OTicSaleReqTel = Conversion.getBoolOrNull(rowParamFlight["OTicSaleReqTel"]).HasValue ? (bool)rowParamFlight["OTicSaleReqTel"] : false,
                        RetFlightOpt = Conversion.getInt16OrNull(rowParamFlight["RetFlightOpt"]),
                        DispMinFlightAllot = Conversion.getInt16OrNull(rowParamFlight["DispMinFlightAllot"]),
                        DispMinFlightAllotW = Conversion.getInt16OrNull(rowParamFlight["DispMinFlightAllotW"]),
                        MaxPaxonTicket = rowParamFlight.Table.Columns.Contains("MaxPaxonTicket") ? Conversion.getInt16OrNull(rowParamFlight["MaxPaxonTicket"]) : Convert.ToInt16(99),
                        UseTicPriceForOTic = Conversion.getBoolOrNull(rowParamFlight["MaxPaxonTicket"])
                    };

                #endregion

                #region ParamPay
                if (rowParamPay != null)
                    param.TvParamPay = new ParamPay
                    {
                        RoundPayment = Conversion.getInt16OrNull(rowParamPay["RoundPayment"]),
                        PayMode = Conversion.getInt16OrNull(rowParamPay["PayMode"]),
                        PayModeDay = Conversion.getInt16OrNull(rowParamPay["PayModeDay"]),
                        AfBef = Conversion.getStrOrNull(rowParamPay["AfBef"]),
                        PayType = Conversion.getStrOrNull(rowParamPay["PayType"]),
                        ResPayOver = Conversion.getInt16OrNull(rowParamPay["ResPayOver"]),
                        AgencyResPayRule = Conversion.getInt16OrNull(rowParamPay["AgencyResPayRule"]),
                        PayOptTime = Conversion.getInt16OrNull(rowParamPay["PayOptTime"]),
                        ColWithInvoice = Conversion.getBoolOrNull(rowParamPay["ColWithInvoice"]).HasValue ? (bool)rowParamPay["ColWithInvoice"] : false,
                        PayPlanDateOpt = Conversion.getInt16OrNull(rowParamPay["PayPlanDateOpt"])
                    };

                #endregion

                #region ParamPrice
                if (rowParamPrice != null)
                    param.TvParamPrice = new ParamPrice
                    {
                        UseSysParam = Equals(rowParamPrice["UseSysParam"], "Y"),
                        RoundBuyPrice = Conversion.getInt16OrNull(rowParamPrice["RoundBuyPrice"]),
                        RoundSalePrice = Conversion.getInt16OrNull(rowParamPrice["RoundSalePrice"]),
                        ChdGrp1Name = Conversion.getStrOrNull(rowParamPrice["ChdGrp1Name"]),
                        ChdGrp2Name = Conversion.getStrOrNull(rowParamPrice["ChdGrp2Name"]),
                        ChdGrp3Name = Conversion.getStrOrNull(rowParamPrice["ChdGrp3Name"]),
                        ChdGrp4Name = Conversion.getStrOrNull(rowParamPrice["ChdGrp4Name"]),
                        ChdGrp1SName = Conversion.getStrOrNull(rowParamPrice["ChdGrp1SName"]),
                        ChdGrp2SName = Conversion.getStrOrNull(rowParamPrice["ChdGrp2SName"]),
                        ChdGrp3SName = Conversion.getStrOrNull(rowParamPrice["ChdGrp3SName"]),
                        ChdGrp4SName = Conversion.getStrOrNull(rowParamPrice["ChdGrp4SName"]),
                        SaleCalcType = Conversion.getInt16OrNull(rowParamPrice["SaleCalcType"]),
                        LastPriceListOpt = Equals(rowParamPrice["LastPriceListOpt"], "Y"),
                        InvPrtNoPay = Equals(rowParamPrice["InvPrtNoPay"], "Y"),
                        ResLockonInvoice = Equals(rowParamPrice["ResLockonInvoice"], "Y"),
                        RoundCom = Conversion.getInt16OrNull(rowParamPrice["RoundCom"]),
                        InvSumType = Conversion.getInt16OrNull(rowParamPrice["InvSumType"]),
                        ResSalePriceOpt = Conversion.getInt16OrNull(rowParamPrice["ResSalePriceOpt"]),
                        CreditMode = Conversion.getInt16OrNull(rowParamPrice["CreditMode"]),
                        AllowMakeRes = Equals(rowParamPrice["AllowMakeRes"], "Y"),
                        PLAdlCap = Conversion.getStrOrNull(rowParamPrice["PLAdlCap"]),
                        PLChdCap = Conversion.getStrOrNull(rowParamPrice["PLChdCap"]),
                        SaleCurOpt = Conversion.getInt16OrNull(rowParamPrice["SaleCurOpt"]),
                        CanInsIncStd = Conversion.getBoolOrNull(rowParamPrice["CanInsIncStd"]),
                        IndSaleCurOpt = Conversion.getByteOrNull(rowParamPrice["IndSaleCurOpt"]),
                        PLSPOCalcType = Conversion.getByteOrNull(rowParamPrice["PLSPOCalcType"]),
                        RoundType = Conversion.getInt16OrNull(rowParamPrice["RoundType"]),
                        RndOpt1 = Conversion.getInt16OrNull(rowParamPrice["RndOpt1"]),
                        RndOpt2 = Conversion.getInt16OrNull(rowParamPrice["RndOpt2"])
                    };

                #endregion

                #region ParamTransport
                if (rowParamTransport != null)
                    param.TvParamTransport = new ParamTransport
                    {
                        UseSysParam = Equals(rowParamTransport["UseSysParam"], "Y"),
                        AllotChk = Equals(rowParamTransport["AllotChk"], "Y"),
                        AllotWarnFull = Equals(rowParamTransport["AllotWarnFull"], "Y"),
                        AllotDoNotOver = Equals(rowParamTransport["AllotDoNotOver"], "Y")
                    };

                #endregion

                return param;
            }
            else
            {
                if (string.IsNullOrEmpty(errorMsg))
                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "TvParametersLoadError").ToString();
                return null;
            }
        }

        public object getTvParameter(User UserData, string Market, ParametersSection section, ref string errorMsg)
        {

            return null;

        }

        public decimal? Exchange(string Market, DateTime RateDate, string CurFrom, string CurTo, decimal? aValue, bool BuySale,ref string errorMsg, Int16? pRateType = null)
        {
            if (aValue == null)
                return null;
            string tsql = @"usp_Exchange";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetStoredProcCommand(tsql);
            try
            {
                //db.AddOutParameter(dbCommand, "RateDate", DbType.Date, 8);
                db.AddOutParameter(dbCommand, "NewValue", DbType.Double, 30);
                decimal? newValue = 0;
                //db.AddParameter(dbCommand, "NewValue", DbType.Decimal, 18, ParameterDirection.Output, true, 18, 6, "NewValue", DataRowVersion.Current, newValue);
                db.AddOutParameter(dbCommand, "ErrCode", DbType.Int16, 8);

                //db.SetParameterValue(dbCommand, "RateDate", RateDate);

                db.AddInParameter(dbCommand, "RateDate", DbType.DateTime, RateDate);
                db.AddInParameter(dbCommand, "CurFrom", DbType.String, CurFrom);
                db.AddInParameter(dbCommand, "CurTo", DbType.String, CurTo);
                db.AddInParameter(dbCommand, "MainValue", DbType.Decimal, aValue);
                db.AddInParameter(dbCommand, "BuySale", DbType.Boolean, BuySale);
                db.AddInParameter(dbCommand, "RateType", DbType.Int16, pRateType.HasValue?pRateType.Value:-1);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                db.ExecuteNonQuery(dbCommand);

                Int16 errCode = (Int16)db.GetParameterValue(dbCommand, "ErrCode");
                newValue = Conversion.getDecimalOrNull(db.GetParameterValue(dbCommand, "NewValue"));

                if (errCode > 0)
                {
                    errorMsg = errCode.ToString();
                    return null;
                }
                else
                {
                    errorMsg = string.Empty;
                    return Math.Round(newValue.Value, 4);
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AgencyIPList> AllowIps(string AgencyCode, ref string errorMsg)
        {
            List<AgencyIPList> ipList = new List<AgencyIPList>();

            string tsql = string.Empty;
            tsql += "Select IpNo, ServiceControl = isnull(ServiceControl, 0) ";
            tsql += "From AgencyIp (NOLOCK) ";
            tsql += string.Format("Where Agency = '{0}' ", AgencyCode);
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        AgencyIPList record = new AgencyIPList
                        {
                            IpNo = (string)oReader["IpNo"],
                            ServiceControl = (bool)oReader["ServiceControl"]
                        };
                        ipList.Add(record);
                    }
                }
                return ipList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return ipList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public object getFormConfigValue(string formName, string fieldName)
        {
            List<WebConfig> configData = CacheObjects.getWebConfigData();
            var query = from q in configData.AsEnumerable()
                        where q.FORMNAME == formName && q.PARAMETERNAME == fieldName
                        select new { type = q.FIELDTYPE, value = q.FIELDVALUE };
            if (query.Count() > 0)
                return fieldValues(query.FirstOrDefault().type, query.FirstOrDefault().value);
            else
                return null;
        }

        public List<TitleRecord> getAdultTypes()
        {
            List<TitleRecord> records = new List<TitleRecord>();

            string tsql = @"Select TitleNo, Code, KeyNo, Sex, [Enable], Explanation, IntCode
                            From Title (NOLOCK)
                            Where isnull(Enable,'N')='Y' and TitleNo<6";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        records.Add(new TitleRecord
                        {
                            Code = Conversion.getStrOrNull(rdr["Code"]),
                            Enable = Equals(rdr["Enable"], "Y"),
                            Explanation = Conversion.getStrOrNull(rdr["Explanation"]),
                            IntCode = Conversion.getStrOrNull(rdr["IntCode"]),
                            KeyNo = (Int16)rdr["KeyNo"],
                            Sex = Conversion.getStrOrNull(rdr["Sex"]),
                            TitleNo = (Int16)rdr["TitleNo"]
                        });
                    }
                }
                return records;
            }
            catch
            {
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public List<TitleRecord> getChildTypes()
        {
            List<TitleRecord> records = new List<TitleRecord>();

            string tsql = @"Select TitleNo, Code, KeyNo, Sex, [Enable], Explanation, IntCode
                            From Title (NOLOCK)
                            Where isnull(Enable,'N')='Y' And TitleNo>=6";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        records.Add(new TitleRecord
                        {
                            Code = Conversion.getStrOrNull(rdr["Code"]),
                            Enable = Equals(rdr["Enable"], "Y"),
                            Explanation = Conversion.getStrOrNull(rdr["Explanation"]),
                            IntCode = Conversion.getStrOrNull(rdr["IntCode"]),
                            KeyNo = (Int16)rdr["KeyNo"],
                            Sex = Conversion.getStrOrNull(rdr["Sex"]),
                            TitleNo = (Int16)rdr["TitleNo"]
                        });
                    }
                }
                return records;
            }
            catch
            {
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public DataTable getAdultType()
        {
            string tsql = @"Select TitleNo, Code, Sex Gender 
                            From Title (NOLOCK) 
                            Where isnull(Enable,'N')='Y' and Sex in ('M','F') ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch
            {
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public DataTable getChildTitleTypes(ref string errorMsg)
        {
            string tsql = @"Select TitleNo, Code, Sex Gender, 
                            (CASE TitleNo WHEN 6 THEN 2 
			                              WHEN 7 THEN 2
			                              WHEN 8 THEN 0
			                              WHEN 9 THEN 0	
		                            END			  	
                            ) Age1,
                            (CASE TitleNo WHEN 6 THEN 17.99 
			                              WHEN 7 THEN 17.99
			                              WHEN 8 THEN 1.99
			                              WHEN 9 THEN 1.99
		                            END			  	
                            ) Age2
                            From Title (NOLOCK) 
                            Where isnull(Enable,'N')='Y' and Sex ='N' Order By TitleNo Desc ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static int Pos(string str1, string str2)
        {
            int position = -1;
            for (int i = 0; i < str1.Length; i++)
            {
                if (str1[i] == str2[0])
                {
                    position = i;
                    break;
                }
            }
            return position;
        }

        public List<TitleRecord> getTitle(ref string errorMsg)
        {
            List<TitleRecord> records = new List<TitleRecord>();
            string tsql = @"Select TitleNo, Code, KeyNo, Sex, [Enable], Explanation, IntCode 
                            From Title (NOLOCK) ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TitleRecord rec = new TitleRecord
                        {
                            TitleNo = (Int16)R["TitleNo"],
                            Code = Conversion.getStrOrNull(R["Code"]),
                            KeyNo = (Int16)R["KeyNo"],
                            Sex = Conversion.getStrOrNull(R["Sex"]),
                            Enable = Equals(R["Enable"], "Y"),
                            Explanation = Conversion.getStrOrNull(R["Explanation"]),
                            IntCode = Conversion.getStrOrNull(R["IntCode"])
                        };
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public object getValueForQuery(string ResNo, string query, ref string errorMsg)
        {
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            try
            {
                return db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<listInteger> getDataListInteger(string data)
        {
            List<listInteger> _data = new List<listInteger>();
            _data = JsonConvert.DeserializeObject<List<listInteger>>(data);
            return _data;
        }

        public string sp_Calc_Errors(User UserData, Int16 ErrCode, ResDataRecord ResData)
        {
            bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
            if (ErrCode > 0 && ErrCode < 100)
            {
                return HttpContext.GetGlobalResourceObject("SpCalcErrors", "CalcResPrice" + ErrCode.ToString()).ToString();
            }
            else
                //10000 - 19999 
                if (ErrCode >= 10000 && ErrCode <= 19999)
            {
                int Service_SeqNo = (ErrCode - 10000) / 100;
                int Service_ErrNo = (ErrCode - 10000) - (Service_SeqNo * 100);
                ResServiceRecord row = ResData.ResService.Find(f => f.SeqNo == Service_SeqNo);
                string errorMsg = string.Format("Service : {0} <br /> Error : {1}",
                    row != null ? (useLocalName ? row.ServiceNameL : row.ServiceName) : "",
                    HttpContext.GetGlobalResourceObject("SpCalcErrors", "CalcService" + Service_ErrNo.ToString()).ToString());
                return errorMsg;
            }
            else
                    if (ErrCode >= 20000 && ErrCode <= 29999)
            {
                int Service_SeqNo = (ErrCode - 20000) / 100;
                int Service_ErrNo = (ErrCode - 20000) - (Service_SeqNo * 100);
                ResServiceExtRecord row = ResData.ResServiceExt.Find(f => f.SeqNo == Service_SeqNo);
                string errorMsg = string.Format("Service : {0} <br /> Error : {1}",
                    row != null ? (useLocalName ? row.ServiceTypeNameL : row.ServiceTypeName) : "",
                    HttpContext.GetGlobalResourceObject("SpCalcErrors", "CalcEXtService" + Service_ErrNo.ToString()).ToString());
                return errorMsg;
            }
            return "";
        }

        public bool getResServiceSeperate(string service)
        {
            string tsql = @"Select ResSeparate=isnull(ResSeparate, Cast('0' as bit) From Service (NOLOCK) Where Code = @Service";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Service", DbType.String, service);
                return (bool)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Int16> getPLNights(string Market, SearchType sType, ref string errorMsg)
        {
            List<Int16> records = new List<short>();
            string tsql = @"Select Distinct Night=isnull(HotNight,Night)
                            From CatPriceP CPP (NOLOCK)
                            Join CatalogPack CP (NOLOCK) ON CP.RecID = CPP.CatPackID
                            Where	CP.Ready = 'Y'
                                And CP.EndDate > dbo.DateOnly(GetDate())
                                And CP.WebPub = 'Y'
                                And CP.WebPubDate < GetDate()
                                And dbo.DateOnly(GetDate()) BetWeen CP.SaleBegDate And CP.SaleEndDate
                                And CP.Market=@Market";
            if (sType == SearchType.OnlyHotelSearch)
                tsql += "  And CP.PackType='O' ";
            else if (sType == SearchType.TourPackageSearch)
                tsql += "  And CP.PackType='T' ";
            else if (sType == SearchType.PackageSearch)
                tsql += "  And (CP.PackType<>'O' And CP.PackType<>'T') ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(Convert.ToInt16(R["Night"]));
                    }
                }
                records = records.OrderBy(o => o).ToList<Int16>();
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Int16> getPLNightsOH(string Market, ref string errorMsg)
        {
            List<Int16> records = new List<short>();
            string tsql =
@"
Select P.Night From OHotelSalePrice P (NOLOCK)
Where Exists (Select RecID From Market (NOLOCK) Where Code=@Market And RecID=P.MarketID)
Group By P.Night
Order By P.Night
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(Convert.ToInt16(R["Night"]));
                    }
                }
                records = records.OrderBy(o => o).ToList<Int16>();
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ServiceRecord getMainService(string Market, string ServiceCode)
        {
            ServiceRecord record = new ServiceRecord();
            string tsql = @"Select Code, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name), 
	                            NameS, DispOrd=isnull(DispOrd, 999), Type, ResSeparate=isnull(ResSeparate, Cast('0' AS bit))
                            From [Service] (NOLOCK) 
                            Where Code=@Code ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, ServiceCode);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.DispOrd = Conversion.getInt16OrNull(R["DispOrd"]);
                        record.Type = Conversion.getStrOrNull(R["Type"]);
                        record.ResSeparate = Conversion.getBoolOrNull(R["ResSeparate"]);
                    }
                    return record;
                }

            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ServiceRecord> getMainServices(string Market)
        {
            List<ServiceRecord> records = new List<ServiceRecord>();
            string tsql = @"Select Code, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name), 
	                            NameS, DispOrd=isnull(DispOrd, 999), Type, ResSeparate=isnull(ResSeparate, Cast('0' AS bit))
                            From [Service] (NOLOCK) 
                            ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ServiceRecord record = new ServiceRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.DispOrd = Conversion.getInt16OrNull(R["DispOrd"]);
                        record.Type = Conversion.getStrOrNull(R["Type"]);
                        record.ResSeparate = Conversion.getBoolOrNull(R["ResSeparate"]);
                        records.Add(record);
                    }
                    return records;
                }

            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HandicapsRecord> getHandicaps(User UserData, DateTime? BegDate, DateTime? EndDate, string Types, string holpack, HandicapTypes handicapTypes, ref string errorMsg)
        {
            List<HandicapsRecord> records = new List<HandicapsRecord>();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 38, VersionControl.Equality.gt))
                tsql =
@"
Select RecID,Type,Code,Market,BegDate,EndDate, 
   Name,NameL=isnull(dbo.FindLocalNameEx(NameLID, @Market), ''),PrtVoucher,ForPackage=isnull(ForPackage,1),ForOnlyFlight=isnull(ForOnlyFlight,1)
From Handicaps (NOLOCK)
Where Market=@Market --And PrtVoucher=1
";
            else
                tsql =
    @"
Select RecID,Type,Code,Market,BegDate,EndDate, 
   Name,NameL=isnull(dbo.FindLocalNameEx(NameLID, @Market), ''),PrtVoucher,,ForPackage=Cast(1 As Bit),ForOnlyFlight==Cast(1 As Bit)
From Handicaps (NOLOCK)
Where Market=@Market --And PrtVoucher=1
";
            if (BegDate.HasValue && EndDate.HasValue)
                tsql += " And BegDate <= @EndDate And EndDate >= @BegDate ";
            if (BegDate.HasValue && !EndDate.HasValue)
                tsql += " And @BegDate between BegDate And EndDate ";
            if (!string.IsNullOrEmpty(Types))
                tsql += string.Format(" And Type='{0}'", Types);
            if (!string.IsNullOrEmpty(holpack))
                tsql += string.Format(" And Code='{0}'", holpack);
            if (handicapTypes == HandicapTypes.Package)
                tsql += " And ForPackage=1";
            if (handicapTypes == HandicapTypes.OnlyTicket)
                tsql += " And ForOnlyFlight=1";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                if (BegDate.HasValue && EndDate.HasValue)
                {
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate);
                }
                if (BegDate.HasValue && !EndDate.HasValue)
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HandicapsRecord rec = new HandicapsRecord
                        {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            Type = Conversion.getStrOrNull(R["Type"]),
                            Code = Conversion.getStrOrNull(R["Code"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            NameL = Conversion.getStrOrNull(R["NameL"]),
                            BegDate = Conversion.getDateTimeOrNull(R["BegDate"]),
                            EndDate = Conversion.getDateTimeOrNull(R["EndDate"]),
                            PrtVoucher = Conversion.getBoolOrNull(R["PrtVoucher"]),
                            ForPackage = Conversion.getBoolOrNull(R["ForPackage"]),
                            ForOnlyFlight = Conversion.getBoolOrNull(R["ForOnlyFlight"])
                        };
                        records.Add(rec);
                    }
                    if (records.Count > 0)
                        return records;
                    else
                        return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<PayTypeRecord> getPayType(string Market, ref string errorMsg)
        {
            bool useSpecCode = VersionControl.getTableField("PayType", "SpecCode");

            List<PayTypeRecord> records = new List<PayTypeRecord>();
            string tsql = string.Empty;
            if (VersionControl.getTableField("PayType", "SpecCode"))
            {
                tsql =
@"
Select PT.RecID,PT.Market,PT.Code,PT.Name,NameL=isnull(dbo.FindLocalName(PT.NameLID,@Market),PT.Name),
    PT.RateType,PT.ExChgDate,PT.Bank,BankName=isnull(dbo.FindLocalName(B.NameLID,@Market),B.Name),
    PT.CanReceiptPrt,PT.PayCat,PayCatName=isnull(dbo.FindLocalName(PTC.NameLID,@Market),PTC.Name),
    PT.OprBankID,
    PT.PayBegDate,PT.PayEndDate,PT.ResBegDate,PT.ResEndDate,PT.MinNight,PT.MaxNight,PT.HasInstalment,PT.CreditCard,
    PT.InstalNumFrom,PT.InstalNumTo,PT.DiscountPer,PT.BankComPer,PT.PosType,PT.EndofDay,PT.PeriodofInstalment, 
    PT.SupDisCode,PT.ValorDay,PT.PayCur,PT.ResPayMinPer,PT.SD,PT.AllowWIS,PT.ResPayMinVal,PT.PayPalAPIUserName,
    PT.PayPalAPIPwd,PT.PayPalSecureMerchantId,PT.PayPalMerchantName,PT.ValidRemDaytoCin,PT.ValidForB2B,PT.ValidForB2C,PT.DiscountVal,
    PT.SpecCode
From PayType PT (NOLOCK)
Join PayTypeCat PTC (NOLOCK) ON PTC.Code=PT.PayCat
Left Join Bank B (NOLOCK) ON B.Code=PT.Bank 
Where PT.Market=@Market
  And isnull(PT.ValidForB2B,1)=1
";
            }
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        PayTypeRecord record = new PayTypeRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.RateType = Conversion.getInt16OrNull(R["RateType"]);
                        record.ExChgDate = Conversion.getInt16OrNull(R["ExChgDate"]);
                        record.Bank = Conversion.getStrOrNull(R["Bank"]);
                        record.BankName = Conversion.getStrOrNull(R["BankName"]);
                        record.CanReceiptPrt = Conversion.getStrOrNull(R["CanReceiptPrt"]);
                        record.PayCat = Conversion.getInt16OrNull(R["PayCat"]);
                        record.PayCatName = Conversion.getStrOrNull(R["PayCatName"]);
                        record.OprBankID = Conversion.getInt32OrNull(R["OprBankID"]);
                        record.PayBegDate = Conversion.getDateTimeOrNull(R["PayBegDate"]);
                        record.PayEndDate = Conversion.getDateTimeOrNull(R["PayEndDate"]);
                        record.ResBegDate = Conversion.getDateTimeOrNull(R["ResBegDate"]);
                        record.ResEndDate = Conversion.getDateTimeOrNull(R["ResEndDate"]);
                        record.MinNight = Conversion.getInt16OrNull(R["MinNight"]);
                        record.MaxNight = Conversion.getInt16OrNull(R["MaxNight"]);
                        record.HasInstalment = Conversion.getBoolOrNull(R["HasInstalment"]);
                        record.CreditCard = Conversion.getStrOrNull(R["CreditCard"]);
                        record.InstalNumFrom = Conversion.getInt16OrNull(R["InstalNumFrom"]);
                        record.InstalNumTo = Conversion.getInt16OrNull(R["InstalNumTo"]);
                        record.DiscountPer = Conversion.getDecimalOrNull(R["DiscountPer"]);
                        record.BankComPer = Conversion.getDecimalOrNull(R["BankComPer"]);
                        record.PosType = Conversion.getInt16OrNull(R["PosType"]);
                        record.EndofDay = Conversion.getDateTimeOrNull(R["EndofDay"]);
                        record.PeriodofInstalment = Conversion.getInt16OrNull(R["PeriodofInstalment"]);
                        record.SupDisCode = Conversion.getStrOrNull(R["SupDisCode"]);
                        record.ValorDay = Conversion.getInt16OrNull(R["ValorDay"]);
                        record.PayCur = Conversion.getStrOrNull(R["PayCur"]);
                        record.ResPayMinPer = Conversion.getDecimalOrNull(R["ResPayMinPer"]);
                        record.SD = Conversion.getStrOrNull(R["SD"]);
                        record.AllowWIS = Conversion.getBoolOrNull(R["AllowWIS"]);
                        record.ResPayMinVal = Conversion.getDecimalOrNull(R["ResPayMinVal"]);
                        record.PayPalAPIUserName = Conversion.getStrOrNull(R["PayPalAPIUserName"]);
                        record.PayPalAPIPwd = Conversion.getStrOrNull(R["PayPalAPIPwd"]);
                        record.PayPalSecureMerchantId = Conversion.getStrOrNull(R["PayPalSecureMerchantId"]);
                        record.PayPalMerchantName = Conversion.getStrOrNull(R["PayPalMerchantName"]);
                        record.ValidRemDaytoCin = Conversion.getInt16OrNull(R["ValidRemDaytoCin"]);
                        record.ValidForB2B = Conversion.getBoolOrNull(R["ValidForB2B"]);
                        record.ValidForB2C = Conversion.getBoolOrNull(R["ValidForB2C"]);
                        record.DiscountVal = Conversion.getDecimalOrNull(R["DiscountVal"]);
                        record.SpecCode = Conversion.getStrOrNull(R["SpecCode"]);

                        records.Add(record);
                    }
                    if (records.Count > 0)
                        return records;
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<PayTypeRecord> getPayType(User UserData, string Market, ref string errorMsg)
        {
            bool useSpecCode = VersionControl.getTableField("PayType", "SpecCode");

            List<PayTypeRecord> records = new List<PayTypeRecord>();
            string tsql = string.Empty;
            if (VersionControl.getTableField("PayType", "SpecCode"))
            {
                tsql =
@"
Select PT.RecID,PT.Market,PT.Code,PT.Name,NameL=isnull(dbo.FindLocalName(PT.NameLID,@Market),PT.Name),
    PT.RateType,PT.ExChgDate,PT.Bank,BankName=isnull(dbo.FindLocalName(B.NameLID,@Market),B.Name),
    PT.CanReceiptPrt,PT.PayCat,PayCatName=isnull(dbo.FindLocalName(PTC.NameLID,@Market),PTC.Name),
    PT.OprBankID,
    PT.PayBegDate,PT.PayEndDate,PT.ResBegDate,PT.ResEndDate,PT.MinNight,PT.MaxNight,PT.HasInstalment,PT.CreditCard,
    PT.InstalNumFrom,PT.InstalNumTo,PT.DiscountPer,PT.BankComPer,PT.PosType,PT.EndofDay,PT.PeriodofInstalment, 
    PT.SupDisCode,PT.ValorDay,PT.PayCur,PT.ResPayMinPer,PT.SD,PT.AllowWIS,PT.ResPayMinVal,PT.PayPalAPIUserName,
    PT.PayPalAPIPwd,PT.PayPalSecureMerchantId,PT.PayPalMerchantName,PT.ValidRemDaytoCin,PT.ValidForB2B,PT.ValidForB2C,PT.DiscountVal,
    PT.SpecCode
From PayType PT (NOLOCK)
Join PayTypeCat PTC (NOLOCK) ON PTC.Code=PT.PayCat
Left Join Bank B (NOLOCK) ON B.Code=PT.Bank 
Where PT.Market=@Market
  And isnull(PT.ValidForB2B,1)=1
";
            }
            else
                if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0016, VersionControl.Equality.gt))
                tsql = @"Select PT.RecID, PT.Market, PT.Code, PT.Name, NameL=isnull(dbo.FindLocalName(PT.NameLID, @Market), PT.Name),
                           PT.RateType, PT.ExChgDate, PT.Bank, BankName=isnull(dbo.FindLocalName(B.NameLID, @Market),B.Name),
                           PT.CanReceiptPrt, PT.PayCat, PayCatName=isnull(dbo.FindLocalName(PTC.NameLID, @Market),PTC.Name),
                           PT.OprBankID,
                           PT.PayBegDate, PT.PayEndDate, PT.ResBegDate, PT.ResEndDate, PT.MinNight, PT.MaxNight, PT.HasInstalment, PT.CreditCard,
                           PT.InstalNumFrom, PT.InstalNumTo, PT.DiscountPer, PT.BankComPer, PT.PosType, PT.EndofDay, PT.PeriodofInstalment, 
                           PT.SupDisCode, PT.ValorDay, PT.PayCur, PT.ResPayMinPer, PT.SD, PT.AllowWIS, PT.ResPayMinVal, PT.PayPalAPIUserName,
                           PT.PayPalAPIPwd, PT.PayPalSecureMerchantId, PT.PayPalMerchantName, PT.ValidRemDaytoCin, PT.ValidForB2B, PT.ValidForB2C, PT.DiscountVal,
                           SpecCode=''
                        From PayType PT (NOLOCK)
                        Join PayTypeCat PTC (NOLOCK) ON PTC.Code=PT.PayCat
                        Left Join Bank B (NOLOCK) ON B.Code=PT.Bank 
                        Where PT.Market=@Market 
                          And isnull(PT.ValidForB2B,1)=1
                        ";
            else
                tsql = @"Select PT.RecID, PT.Market, PT.Code, PT.Name, NameL=isnull(dbo.FindLocalName(PT.NameLID, @Market), PT.Name),
                           PT.RateType, PT.ExChgDate, PT.Bank, BankName=isnull(dbo.FindLocalName(B.NameLID, @Market),B.Name),
                           PT.CanReceiptPrt, PT.PayCat, PayCatName=isnull(dbo.FindLocalName(PTC.NameLID, @Market),PTC.Name),
                           PT.OprBankID,
                           PT.PayBegDate, PT.PayEndDate, PT.ResBegDate, PT.ResEndDate, PT.MinNight, PT.MaxNight, PT.HasInstalment, PT.CreditCard,
                           PT.InstalNumFrom, PT.InstalNumTo, PT.DiscountPer, PT.BankComPer, PT.PosType, PT.EndofDay, PT.PeriodofInstalment, 
                           PT.SupDisCode, PT.ValorDay, PT.PayCur, PT.ResPayMinPer, PT.SD, PT.AllowWIS, PT.ResPayMinVal, PT.PayPalAPIUserName,
                           PT.PayPalAPIPwd, PT.PayPalSecureMerchantId, PT.PayPalMerchantName, PT.ValidRemDaytoCin, PT.ValidForB2B, PT.ValidForB2C,
                           SpecCode=''
                        From PayType PT (NOLOCK)
                        Join PayTypeCat PTC (NOLOCK) ON PTC.Code=PT.PayCat
                        Left Join Bank B (NOLOCK) ON B.Code=PT.Bank                         
                        ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        PayTypeRecord record = new PayTypeRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.RateType = Conversion.getInt16OrNull(R["RateType"]);
                        record.ExChgDate = Conversion.getInt16OrNull(R["ExChgDate"]);
                        record.Bank = Conversion.getStrOrNull(R["Bank"]);
                        record.BankName = Conversion.getStrOrNull(R["BankName"]);
                        record.CanReceiptPrt = Conversion.getStrOrNull(R["CanReceiptPrt"]);
                        record.PayCat = Conversion.getInt16OrNull(R["PayCat"]);
                        record.PayCatName = Conversion.getStrOrNull(R["PayCatName"]);
                        record.OprBankID = Conversion.getInt32OrNull(R["OprBankID"]);
                        record.PayBegDate = Conversion.getDateTimeOrNull(R["PayBegDate"]);
                        record.PayEndDate = Conversion.getDateTimeOrNull(R["PayEndDate"]);
                        record.ResBegDate = Conversion.getDateTimeOrNull(R["ResBegDate"]);
                        record.ResEndDate = Conversion.getDateTimeOrNull(R["ResEndDate"]);
                        record.MinNight = Conversion.getInt16OrNull(R["MinNight"]);
                        record.MaxNight = Conversion.getInt16OrNull(R["MaxNight"]);
                        record.HasInstalment = Conversion.getBoolOrNull(R["HasInstalment"]);
                        record.CreditCard = Conversion.getStrOrNull(R["CreditCard"]);
                        record.InstalNumFrom = Conversion.getInt16OrNull(R["InstalNumFrom"]);
                        record.InstalNumTo = Conversion.getInt16OrNull(R["InstalNumTo"]);
                        record.DiscountPer = Conversion.getDecimalOrNull(R["DiscountPer"]);
                        record.BankComPer = Conversion.getDecimalOrNull(R["BankComPer"]);
                        record.PosType = Conversion.getInt16OrNull(R["PosType"]);
                        record.EndofDay = Conversion.getDateTimeOrNull(R["EndofDay"]);
                        record.PeriodofInstalment = Conversion.getInt16OrNull(R["PeriodofInstalment"]);
                        record.SupDisCode = Conversion.getStrOrNull(R["SupDisCode"]);
                        record.ValorDay = Conversion.getInt16OrNull(R["ValorDay"]);
                        record.PayCur = Conversion.getStrOrNull(R["PayCur"]);
                        record.ResPayMinPer = Conversion.getDecimalOrNull(R["ResPayMinPer"]);
                        record.SD = Conversion.getStrOrNull(R["SD"]);
                        record.AllowWIS = Conversion.getBoolOrNull(R["AllowWIS"]);
                        record.ResPayMinVal = Conversion.getDecimalOrNull(R["ResPayMinVal"]);
                        record.PayPalAPIUserName = Conversion.getStrOrNull(R["PayPalAPIUserName"]);
                        record.PayPalAPIPwd = Conversion.getStrOrNull(R["PayPalAPIPwd"]);
                        record.PayPalSecureMerchantId = Conversion.getStrOrNull(R["PayPalSecureMerchantId"]);
                        record.PayPalMerchantName = Conversion.getStrOrNull(R["PayPalMerchantName"]);
                        record.ValidRemDaytoCin = Conversion.getInt16OrNull(R["ValidRemDaytoCin"]);
                        record.ValidForB2B = Conversion.getBoolOrNull(R["ValidForB2B"]);
                        record.ValidForB2C = Conversion.getBoolOrNull(R["ValidForB2C"]);
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0016, VersionControl.Equality.gt))
                            record.DiscountVal = Conversion.getDecimalOrNull(R["DiscountVal"]);
                        record.SpecCode = Conversion.getStrOrNull(R["SpecCode"]);

                        records.Add(record);
                    }
                    if (records.Count > 0)
                        return records;
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ReadyCurrencyRecord> getReadyCurrency(string Market, string BaseCur, ref string errorMsg)
        {
            List<ReadyCurrencyRecord> records = new List<ReadyCurrencyRecord>();
            string tsql = @"Select Code, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name), 
	                            RateIsReady=dbo.ufn_Exchange(@Date, Code, @BaseCur, 1, 1, @Market)
                            From Currency (NOLOCK) ";

            object configMarketKur = new TvBo.Common().getFormConfigValue("SearchPanel", "SearchCurrency");
            string confKurStr = Conversion.getStrOrNull(configMarketKur);
            List<MarketConfigCurr> confKurList = new List<MarketConfigCurr>();
            try
            {
                if (!string.IsNullOrEmpty(confKurStr))
                    confKurList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MarketConfigCurr>>(confKurStr).Where(w => w.Market == Market).Select(s => s).ToList<MarketConfigCurr>();
            }
            catch { confKurList = null; }

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "BaseCur", DbType.String, BaseCur);
                db.AddInParameter(dbCommand, "Date", DbType.Date, DateTime.Today.Date);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ReadyCurrencyRecord rec = new ReadyCurrencyRecord
                        {
                            Code = Conversion.getStrOrNull(R["Code"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            NameL = Conversion.getStrOrNull(R["NameL"]),
                            RateIsReady = Conversion.getDecimalOrNull(R["RateIsReady"])
                        };
                        records.Add(rec);
                    }
                    if (records.Count > 0)
                    {
                        List<ReadyCurrencyRecord> list;
                        if (confKurList != null && confKurList.Count > 0)
                            list = (from q1 in records
                                    join q2 in confKurList on q1.Code equals q2.Currency
                                    where (q1.RateIsReady.HasValue && q1.RateIsReady.Value > 0)
                                    select q1).ToList<ReadyCurrencyRecord>();
                        else
                            list = (from q1 in records
                                    where (q1.RateIsReady.HasValue && q1.RateIsReady.Value > 0) && q1.Code == BaseCur
                                    select q1).ToList<ReadyCurrencyRecord>();
                        return list;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public OfficeRecord getOffice(string Market, string Code, ref string errorMsg)
        {
            string tsql = @"Select O.RecID, O.Code, O.Name, NameL=isnull(dbo.FindLocalName(O.NameLID, @Market), O.Name),
                                O.NameS, O.Location, LocationName=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                O.Operator, O.Market, O.[Description], O.ResEntry,
                                O.ResNoOpt, O.BegResNo, O.EndResNo, O.DefPrinter, O.DefFaxPrinter, O.InsurPrinter, O.TicketPrinter,
                                O.VoucherPrinter, O.UseOprMarket, O.ResSerie, O.InvoicePrinter, O.AutoInvoiceNo,    
                                O.[Address], O.AddrZip, O.AddrCity, O.AddrCountry, O.ContName, O.Phone1, O.Phone2, O.MobPhone, O.Fax1,
                                O.Fax2, O.www, O.email1, O.email2, O.ReceiptPrinter
                            From Office O (NOLOCK)
                            Left Join Location L (NOLOCK) ON L.RecID=O.Location
                            Where O.Code=@Code ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        OfficeRecord record = new OfficeRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Location = Conversion.getInt32OrNull(R["Location"]);
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.ResEntry = Conversion.getStrOrNull(R["ResEntry"]);
                        record.ResNoOpt = Conversion.getInt16OrNull(R["ResNoOpt"]);
                        record.BegResNo = Conversion.getInt32OrNull(R["BegResNo"]);
                        record.EndResNo = Conversion.getInt32OrNull(R["EndResNo"]);
                        record.DefPrinter = Conversion.getStrOrNull(R["DefPrinter"]);
                        record.DefFaxPrinter = Conversion.getStrOrNull(R["DefFaxPrinter"]);
                        record.InsurPrinter = Conversion.getStrOrNull(R["InsurPrinter"]);
                        record.TicketPrinter = Conversion.getStrOrNull(R["TicketPrinter"]);
                        record.VoucherPrinter = Conversion.getStrOrNull(R["VoucherPrinter"]);
                        record.UseOprMarket = Conversion.getStrOrNull(R["UseOprMarket"]);
                        record.ResSerie = Conversion.getStrOrNull(R["ResSerie"]);
                        record.InvoicePrinter = Conversion.getStrOrNull(R["InvoicePrinter"]);
                        record.AutoInvoiceNo = Conversion.getStrOrNull(R["AutoInvoiceNo"]);
                        //record.InvoiceSerial = Conversion.getStrOrNull(R["InvoiceSerial"]);
                        //record.InvoiceNoFrom = Conversion.getInt32OrNull(R["InvoiceNoFrom"]);
                        //record.InvoiceNoTo = Conversion.getInt32OrNull(R["InvoiceNoTo"]);
                        //record.AutoReceiptNo = Conversion.getStrOrNull(R["AutoReceiptNo"]);
                        //record.ReceiptSerial = Conversion.getStrOrNull(R["ReceiptSerial"]);
                        //record.ReceiptNoFrom = Conversion.getInt32OrNull(R["ReceiptNoFrom"]);
                        //record.ReceiptNoTo = Conversion.getInt32OrNull(R["ReceiptNoTo"]);
                        //O.InvoiceSerial, O.InvoiceNoFrom, O.InvoiceNoTo, O.AutoReceiptNo, O.ReceiptSerial, O.ReceiptNoFrom, O.ReceiptNoTo,
                        record.Address = Conversion.getStrOrNull(R["Address"]);
                        record.AddrZip = Conversion.getStrOrNull(R["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(R["AddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(R["AddrCountry"]);
                        record.ContName = Conversion.getStrOrNull(R["ContName"]);
                        record.Phone1 = Conversion.getStrOrNull(R["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(R["Phone2"]);
                        record.MobPhone = Conversion.getStrOrNull(R["MobPhone"]);
                        record.Fax1 = Conversion.getStrOrNull(R["Fax1"]);
                        record.Fax2 = Conversion.getStrOrNull(R["Fax2"]);
                        record.www = Conversion.getStrOrNull(R["www"]);
                        record.email1 = Conversion.getStrOrNull(R["email1"]);
                        record.email2 = Conversion.getStrOrNull(R["email2"]);
                        record.ReceiptPrinter = Conversion.getStrOrNull(R["ReceiptPrinter"]);
                        return record;
                    }
                    else
                        return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public OperatorRecord getOperator(string Code, ref string errorMsg)
        {
            string tsql = @"Select O.RecID, O.Code, O.Name, NameL=ISNULL(dbo.FindLocalName(O.NameLID, O.Market), O.Name),
	                            O.NameS, O.Status, O.MainOperator, O.Market, O.Location, 
	                            LocationName=L.Name, LocationNameL=ISNULL(dbo.FindLocalName(L.NameLID, O.Market), L.Name),
	                            O.FirmName, O.Address, O.AddrZip, O.AddrCity, O.AddrCountry, O.InvAddress, O.InvAddrZip, O.InvAddrCity,
	                            O.InvAddrCountry, O.TaxOffice, O.TaxAccNo, O.BlackList, O.Bank1, O.Bank1BankNo, O.Bank1AccNo,
	                            O.Bank1Curr, O.Bank1IBAN, O.Bank2, O.Bank2BankNo, O.Bank2AccNo, O.Bank2Curr, O.Bank2IBAN,
	                            O.BossName, O.ContName, O.ACContacName, O.Phone1, O.Phone2, O.MobPhone, O.Fax1, O.Fax2, O.www,
	                            O.email1, O.email2, O.Note, O.ResSerie, O.RegisterCode, O.InsSupplierName, O.InsPolicyName, 
	                            O.InsPolicyNo, O.InsIssuedDate, O.InsExpDate,
	                            O.CompanyNo, O.Bank1AccId, O.Bank2AccId, O.CIF, O.CapSocial, O.CapSocialCur, O.LicenseNo, O.AgencyCanDisCom
                            From Operator O (NOLOCK)
                            Left Join Location L (NOLOCK) ON L.RecID = O.Location
                            Where O.Code=@Code";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        OperatorRecord record = new OperatorRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        record.MainOperator = Conversion.getStrOrNull(R["MainOperator"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Location = Conversion.getInt32OrNull(R["Location"]);
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        record.FirmName = Conversion.getStrOrNull(R["FirmName"]);
                        record.Address = Conversion.getStrOrNull(R["Address"]);
                        record.AddrZip = Conversion.getStrOrNull(R["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(R["AddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(R["AddrCountry"]);
                        record.InvAddress = Conversion.getStrOrNull(R["InvAddress"]);
                        record.InvAddrZip = Conversion.getStrOrNull(R["InvAddrZip"]);
                        record.InvAddrCity = Conversion.getStrOrNull(R["InvAddrCity"]);
                        record.InvAddrCountry = Conversion.getStrOrNull(R["InvAddrCountry"]);
                        record.TaxOffice = Conversion.getStrOrNull(R["TaxOffice"]);
                        record.TaxAccNo = Conversion.getStrOrNull(R["TaxAccNo"]);
                        record.BlackList = Conversion.getStrOrNull(R["BlackList"]);
                        record.Bank1 = Conversion.getStrOrNull(R["Bank1"]);
                        record.Bank1BankNo = Conversion.getStrOrNull(R["Bank1BankNo"]);
                        record.Bank1AccNo = Conversion.getStrOrNull(R["Bank1AccNo"]);
                        record.Bank1Curr = Conversion.getStrOrNull(R["Bank1Curr"]);
                        record.Bank1IBAN = Conversion.getStrOrNull(R["Bank1IBAN"]);
                        record.Bank2 = Conversion.getStrOrNull(R["Bank2"]);
                        record.Bank2BankNo = Conversion.getStrOrNull(R["Bank2BankNo"]);
                        record.Bank2AccNo = Conversion.getStrOrNull(R["Bank2AccNo"]);
                        record.Bank2Curr = Conversion.getStrOrNull(R["Bank2Curr"]);
                        record.Bank2IBAN = Conversion.getStrOrNull(R["Bank2IBAN"]);
                        record.BossName = Conversion.getStrOrNull(R["BossName"]);
                        record.ContName = Conversion.getStrOrNull(R["ContName"]);
                        record.ACContacName = Conversion.getStrOrNull(R["ACContacName"]);
                        record.Phone1 = Conversion.getStrOrNull(R["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(R["Phone2"]);
                        record.MobPhone = Conversion.getStrOrNull(R["MobPhone"]);
                        record.Fax1 = Conversion.getStrOrNull(R["Fax1"]);
                        record.Fax2 = Conversion.getStrOrNull(R["Fax2"]);
                        record.Www = Conversion.getStrOrNull(R["Www"]);
                        record.Email1 = Conversion.getStrOrNull(R["Email1"]);
                        record.Email2 = Conversion.getStrOrNull(R["Email2"]);
                        record.Note = Conversion.getStrOrNull(R["Note"]);
                        record.ResSerie = Conversion.getStrOrNull(R["ResSerie"]);
                        record.RegisterCode = Conversion.getStrOrNull(R["RegisterCode"]);
                        record.InsSupplierName = Conversion.getStrOrNull(R["InsSupplierName"]);
                        record.InsPolicyName = Conversion.getStrOrNull(R["InsPolicyName"]);
                        record.InsPolicyNo = Conversion.getStrOrNull(R["InsPolicyNo"]);
                        record.InsIssuedDate = Conversion.getDateTimeOrNull(R["InsIssuedDate"]);
                        record.InsExpDate = Conversion.getDateTimeOrNull(R["InsExpDate"]);
                        record.CompanyNo = Conversion.getStrOrNull(R["CompanyNo"]);
                        record.Bank1AccId = Conversion.getStrOrNull(R["Bank1AccId"]);
                        record.Bank2AccId = Conversion.getStrOrNull(R["Bank2AccId"]);
                        record.CIF = Conversion.getStrOrNull(R["CIF"]);
                        record.CapSocial = Conversion.getStrOrNull(R["CapSocial"]);
                        record.CapSocialCur = Conversion.getStrOrNull(R["CapSocialCur"]);
                        record.LicenseNo = Conversion.getStrOrNull(R["LicenseNo"]);
                        record.AgencyCanDisCom = Conversion.getBoolOrNull(R["AgencyCanDisCom"]);
                        return record;
                    }
                    else
                        return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OperatorRecord> getOperators(ref string errorMsg)
        {
            List<OperatorRecord> records = new List<OperatorRecord>();
            string tsql = @"Select O.RecID, O.Code, O.Name, NameL=ISNULL(dbo.FindLocalName(O.NameLID, O.Market), O.Name),
	                            O.NameS, O.Status, O.MainOperator, O.Market, O.Location, 
	                            LocationName=L.Name, LocationNameL=ISNULL(dbo.FindLocalName(L.NameLID, O.Market), L.Name),
	                            O.FirmName, O.Address, O.AddrZip, O.AddrCity, O.AddrCountry, O.InvAddress, O.InvAddrZip, O.InvAddrCity,
	                            O.InvAddrCountry, O.TaxOffice, O.TaxAccNo, O.BlackList, O.Bank1, O.Bank1BankNo, O.Bank1AccNo,
	                            O.Bank1Curr, O.Bank1IBAN, O.Bank2, O.Bank2BankNo, O.Bank2AccNo, O.Bank2Curr, O.Bank2IBAN,
	                            O.BossName, O.ContName, O.ACContacName, O.Phone1, O.Phone2, O.MobPhone, O.Fax1, O.Fax2, O.www,
	                            O.email1, O.email2, O.Note, O.ResSerie, O.RegisterCode, O.InsSupplierName, O.InsPolicyName, 
	                            O.InsPolicyNo, O.InsIssuedDate, O.InsExpDate,
	                            O.CompanyNo, O.Bank1AccId, O.Bank2AccId, O.CIF, O.CapSocial, O.CapSocialCur, O.LicenseNo, O.AgencyCanDisCom
                            From Operator O (NOLOCK)
                            Left Join Location L (NOLOCK) ON L.RecID = O.Location
                            ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        OperatorRecord record = new OperatorRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        record.MainOperator = Conversion.getStrOrNull(R["MainOperator"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Location = Conversion.getInt32OrNull(R["Location"]);
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        record.FirmName = Conversion.getStrOrNull(R["FirmName"]);
                        record.Address = Conversion.getStrOrNull(R["Address"]);
                        record.AddrZip = Conversion.getStrOrNull(R["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(R["AddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(R["AddrCountry"]);
                        record.InvAddress = Conversion.getStrOrNull(R["InvAddress"]);
                        record.InvAddrZip = Conversion.getStrOrNull(R["InvAddrZip"]);
                        record.InvAddrCity = Conversion.getStrOrNull(R["InvAddrCity"]);
                        record.InvAddrCountry = Conversion.getStrOrNull(R["InvAddrCountry"]);
                        record.TaxOffice = Conversion.getStrOrNull(R["TaxOffice"]);
                        record.TaxAccNo = Conversion.getStrOrNull(R["TaxAccNo"]);
                        record.BlackList = Conversion.getStrOrNull(R["BlackList"]);
                        record.Bank1 = Conversion.getStrOrNull(R["Bank1"]);
                        record.Bank1BankNo = Conversion.getStrOrNull(R["Bank1BankNo"]);
                        record.Bank1AccNo = Conversion.getStrOrNull(R["Bank1AccNo"]);
                        record.Bank1Curr = Conversion.getStrOrNull(R["Bank1Curr"]);
                        record.Bank1IBAN = Conversion.getStrOrNull(R["Bank1IBAN"]);
                        record.Bank2 = Conversion.getStrOrNull(R["Bank2"]);
                        record.Bank2BankNo = Conversion.getStrOrNull(R["Bank2BankNo"]);
                        record.Bank2AccNo = Conversion.getStrOrNull(R["Bank2AccNo"]);
                        record.Bank2Curr = Conversion.getStrOrNull(R["Bank2Curr"]);
                        record.Bank2IBAN = Conversion.getStrOrNull(R["Bank2IBAN"]);
                        record.BossName = Conversion.getStrOrNull(R["BossName"]);
                        record.ContName = Conversion.getStrOrNull(R["ContName"]);
                        record.ACContacName = Conversion.getStrOrNull(R["ACContacName"]);
                        record.Phone1 = Conversion.getStrOrNull(R["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(R["Phone2"]);
                        record.MobPhone = Conversion.getStrOrNull(R["MobPhone"]);
                        record.Fax1 = Conversion.getStrOrNull(R["Fax1"]);
                        record.Fax2 = Conversion.getStrOrNull(R["Fax2"]);
                        record.Www = Conversion.getStrOrNull(R["Www"]);
                        record.Email1 = Conversion.getStrOrNull(R["Email1"]);
                        record.Email2 = Conversion.getStrOrNull(R["Email2"]);
                        record.Note = Conversion.getStrOrNull(R["Note"]);
                        record.ResSerie = Conversion.getStrOrNull(R["ResSerie"]);
                        record.RegisterCode = Conversion.getStrOrNull(R["RegisterCode"]);
                        record.InsSupplierName = Conversion.getStrOrNull(R["InsSupplierName"]);
                        record.InsPolicyName = Conversion.getStrOrNull(R["InsPolicyName"]);
                        record.InsPolicyNo = Conversion.getStrOrNull(R["InsPolicyNo"]);
                        record.InsIssuedDate = Conversion.getDateTimeOrNull(R["InsIssuedDate"]);
                        record.InsExpDate = Conversion.getDateTimeOrNull(R["InsExpDate"]);
                        record.CompanyNo = Conversion.getStrOrNull(R["CompanyNo"]);
                        record.Bank1AccId = Conversion.getStrOrNull(R["Bank1AccId"]);
                        record.Bank2AccId = Conversion.getStrOrNull(R["Bank2AccId"]);
                        record.CIF = Conversion.getStrOrNull(R["CIF"]);
                        record.CapSocial = Conversion.getStrOrNull(R["CapSocial"]);
                        record.CapSocialCur = Conversion.getStrOrNull(R["CapSocialCur"]);
                        record.LicenseNo = Conversion.getStrOrNull(R["LicenseNo"]);
                        record.AgencyCanDisCom = Conversion.getBoolOrNull(R["AgencyCanDisCom"]);
                        records.Add(record);
                    }
                    return records;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        //        public List<AgencyCommisionListRecord> getAgencyCommisionList(string AgencyID, string selectedAgency, DateTime? BeginDate, DateTime? EndDate, bool RegisterDate, string currency, ref string errorMsg)
        //        {
        //            List<AgencyCommisionListRecord> records = new List<AgencyCommisionListRecord>();
        //            string tsql = string.Format(
        //@"Select {0}, Cur=isnull(@Curr,M.Cur),	
        //	Sum(case when isnull(@Curr,M.Cur)=M.Cur then TotSale else dbo.ufn_Exchange(GetDate(), M.Cur,@Curr, isnull(TotSale,0),1,DB.Market) end) as Debit,	
        //	Sum(case when isnull(@Curr,M.Cur)=M.Cur then TotCom else dbo.ufn_Exchange(GetDate(), M.Cur,@Curr, isnull(TotCom,0),1,DB.Market) end) as Commission, 	
        //	Sum(case when isnull(@Curr,M.Cur)=M.Cur then TotPayment else dbo.ufn_Exchange(GetDate(), M.Cur,@Curr, isnull(TotPayment,0),1,DB.Market) end) as Payment, 	
        //	Sum(case when isnull(@Curr,M.Cur)=M.Cur then TotBalance else dbo.ufn_Exchange(GetDate(), M.Cur,@Curr, isnull(TotBalance,0),1,DB.Market) end) as Balance,
        //    Sum(TotAdl) as Adult, 
        //    Sum(TotChd) as Child, 
        //    Sum(TotPax) as Pax
        //From AgencyDayBook DB (NOLOCK)
        //Join Agency A (NOLOCK) ON A.Code=Agency
        //Left Join Market M (NOLOCK) on M.Code=DB.Market
        //", !RegisterDate ? "SaleDate" : "ResBegDate");
        //            if (string.IsNullOrEmpty(selectedAgency))
        //                tsql += "Where (DB.Agency=@currentAgency Or A.MainOffice=@currentAgency) ";
        //            else
        //                tsql += "Where DB.Agency=@Code ";
        //            if (BeginDate.HasValue)
        //                tsql += string.Format("  And {0}>=@BeginDate ", !RegisterDate ? "SaleDate" : "ResBegDate");
        //            if (EndDate.HasValue)
        //                tsql += string.Format("  And {0}<=@EndDate ", !RegisterDate ? "SaleDate" : "ResBegDate");
        //            tsql += string.Format("Group by isnull(@Curr,M.Cur),{0} ", !RegisterDate ? "SaleDate" : "ResBegDate");
        //            tsql += string.Format("Order by {0},isnull(@Curr,M.Cur) ", !RegisterDate ? "SaleDate" : "ResBegDate");

        //            Database db = DatabaseFactory.CreateDatabase() as Database;
        //            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
        //            try
        //            {
        //                if (string.IsNullOrEmpty(selectedAgency))
        //                    db.AddInParameter(dbCommand, "currentAgency", DbType.AnsiString, AgencyID);
        //                else
        //                    db.AddInParameter(dbCommand, "Code", DbType.AnsiString, selectedAgency);
        //                if (BeginDate.HasValue)
        //                    db.AddInParameter(dbCommand, "BeginDate", DbType.DateTime, BeginDate.Value);
        //                if (EndDate.HasValue)
        //                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate.Value);
        //                db.AddInParameter(dbCommand, "Curr", DbType.AnsiString, currency);
        //                using (IDataReader R = db.ExecuteReader(dbCommand))
        //                {
        //                    while (R.Read())
        //                    {
        //                        AgencyCommisionListRecord record = new AgencyCommisionListRecord();
        //                        record.SaleDate = !RegisterDate ? Conversion.getDateTimeOrNull(R["SaleDate"]) : Conversion.getDateTimeOrNull(R["ResBegDate"]);
        //                        record.Cur = Conversion.getStrOrNull(R["Cur"]);
        //                        record.Debit = Conversion.getDecimalOrNull(R["Debit"]);
        //                        record.Commission = Conversion.getDecimalOrNull(R["Commission"]);
        //                        record.Payment = Conversion.getDecimalOrNull(R["Payment"]);
        //                        record.Balance = Conversion.getDecimalOrNull(R["Balance"]);
        //                        record.Adult = Conversion.getInt32OrNull(R["Adult"]);
        //                        record.Child = Conversion.getInt32OrNull(R["Child"]);
        //                        record.Pax = Conversion.getInt32OrNull(R["Pax"]);
        //                        records.Add(record);
        //                    }
        //                    return records;
        //                }
        //            }
        //            catch (Exception Ex)
        //            {
        //                errorMsg = Ex.Message;
        //                return null;
        //            }
        //            finally
        //            {
        //                dbCommand.Connection.Close();
        //                dbCommand.Dispose();
        //            }
        //        }
        public List<AgencyCommisionListRecord> getAgencyCommisionList(string AgencyID, string selectedAgency, DateTime? BeginDate, DateTime? EndDate, bool RegisterDate, string currency, ref string errorMsg)
        {
            List<AgencyCommisionListRecord> records = new List<AgencyCommisionListRecord>();
            string tsql = string.Format(
@"Select {0}, Cur=isnull(@Curr,M.Cur),	
	Sum(case when isnull(@Curr,RM.SaleCur)=M.Cur then SalePrice else dbo.ufn_Exchange(GetDate(), RM.SaleCur,@Curr, isnull(SalePrice,0),1,RM.Market) end) as SalePrice,	
	Sum(case when isnull(@Curr,RM.SaleCur)=M.Cur then isnull(AgencyCom,0) else dbo.ufn_Exchange(GetDate(), RM.SaleCur,@Curr, isnull(AgencyCom,0),1,RM.Market) end) as Commission, 	
	Sum(case when isnull(@Curr,RM.SaleCur)=M.Cur then isnull(RM.AgencyPayment,0) else dbo.ufn_Exchange(GetDate(), RM.SaleCur,@Curr, isnull(RM.AgencyPayment,0),1,RM.Market) end) as Payment, 	
	Sum(case when isnull(@Curr,RM.SaleCur)=M.Cur then isnull(Balance,0) else dbo.ufn_Exchange(GetDate(), RM.SaleCur,@Curr, isnull(Balance,0),1,RM.Market) end) as Balance,
	Sum(case when isnull(@Curr,RM.SaleCur)=M.Cur then isnull(RM.BrokerCom,0) else dbo.ufn_Exchange(GetDate(), RM.SaleCur,@Curr, isnull(RM.BrokerCom,0),1,RM.Market) end) as BrokerCom,
    Sum(RM.Adult) as Adult, 
    Sum(RM.Child) as Child, 
    Sum(RM.Pax) as Pax,
	RM.Agency as Agency
From ResMain RM (NOLOCK)
Join Agency A (NOLOCK) ON A.Code=RM.Agency
Left Join Market M (NOLOCK) on M.Code=RM.Market
", !RegisterDate ? "ResDate" : "BegDate");
            if (string.IsNullOrEmpty(selectedAgency))
                tsql += "Where (RM.Agency=@currentAgency Or A.MainOffice=@currentAgency) ";
            else
                tsql += "Where RM.Agency=@Code ";
            if (BeginDate.HasValue)
                tsql += string.Format("  And {0}>=@BeginDate ", !RegisterDate ? "ResDate" : "BegDate");
            if (EndDate.HasValue)
                tsql += string.Format("  And {0}<=@EndDate ", !RegisterDate ? "ResDate" : "BegDate");

            tsql += string.Format("Group by Agency,isnull(@Curr,M.Cur),{0} ", !RegisterDate ? "ResDate" : "BegDate");
            tsql += string.Format("Order by {0},isnull(@Curr,M.Cur) ", !RegisterDate ? "ResDate" : "BegDate");

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                if (string.IsNullOrEmpty(selectedAgency))
                    db.AddInParameter(dbCommand, "currentAgency", DbType.AnsiString, AgencyID);
                else
                    db.AddInParameter(dbCommand, "Code", DbType.AnsiString, selectedAgency);
                if (BeginDate.HasValue)
                    db.AddInParameter(dbCommand, "BeginDate", DbType.DateTime, BeginDate.Value);
                if (EndDate.HasValue)
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate.Value);
                db.AddInParameter(dbCommand, "Curr", DbType.AnsiString, currency);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AgencyCommisionListRecord record = new AgencyCommisionListRecord();
                        record.AgencyCode = Conversion.getStrOrNull(R["Agency"]);
                        record.SaleDate = !RegisterDate ? Conversion.getDateTimeOrNull(R["ResDate"]) : Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.Cur = Conversion.getStrOrNull(R["Cur"]);
                        record.Debit = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.Commission = Conversion.getDecimalOrNull(R["Commission"]);
                        record.BrokerCommission = Conversion.getDecimalOrNull(R["BrokerCom"]);
                        record.Payment = Conversion.getDecimalOrNull(R["Payment"]);
                        record.Balance = Conversion.getDecimalOrNull(R["Balance"]);
                        record.Adult = Conversion.getInt32OrNull(R["Adult"]);
                        record.Child = Conversion.getInt32OrNull(R["Child"]);
                        record.Pax = Conversion.getInt32OrNull(R["Pax"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<CodeName> getAgencyCommisionReportAgencyList(string AgencyID, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = @"if Exists(Select RecID From Agency Where Code=@Code And ISNULL(MainOffice, '') = '')
                            Begin
	                            Select A.Code, Name=isnull(dbo.FindLocalName(A.NameLID, O.Market), A.Name)
	                            From Agency A (NOLOCK)
	                            Join Office O (NOLOCK) ON O.Code=A.OprOffice
	                            Where (Case when ISNULL(A.MainOffice, '') = '' then A.Code Else A.MainOffice End)=@Code
                            End
                            Else
                            Begin
                            Select A.Code, Name=isnull(dbo.FindLocalName(A.NameLID, O.Market), A.Name)
	                            From Agency A (NOLOCK)
	                            Join Office O (NOLOCK) ON O.Code=A.OprOffice
	                            Where A.Code=@Code
                            End";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, AgencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        CodeName record = new CodeName();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<IntCountryListRecord> getCountryList(User UserData, ref string errorMsg)
        {
            bool? notIncPaximumLocation = Conversion.getBoolOrNull(getFormConfigValue("General", "notUseIncludePaximumLocation"));
            string tsql = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
            {
                tsql =
@"
Select 
    CountryName=L.Name,
    CountryNameL=isnull(dbo.FindLocalName(L.NameLID,@Market),L.Name), 
	Nationality=N.Code3,
	NationalityName=N.Name,
	IntCode=N.Code2,
	CountryPhoneCode=L.IntPhoneCode,
	L.PhoneMask,L.PhoneMaskMob
From Location L (NOLOCK)
Left Join Nationality N (NOLOCK) ON L.CountryCode=N.Code3
Where L.Type=1
";
                if (notIncPaximumLocation.HasValue && notIncPaximumLocation.Value)
                    tsql += " and L.PxmRec is null";
            }
            else
                tsql =
@"
Select 
    CountryName=Name,
    CountryNameL=isnull(dbo.FindLocalName(NameLID, @Market), Name), 
	IntCode=CountryCode,
	CountryPhoneCode=IntPhoneCode,
	PhoneMask, PhoneMaskMob
From Location
Where Type=1
";

            List<IntCountryListRecord> records = new List<IntCountryListRecord>();
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        IntCountryListRecord record = new IntCountryListRecord();
                        record.CountryName = Conversion.getStrOrNull(R["CountryName"]);
                        record.CountryNameL = Conversion.getStrOrNull(R["CountryNameL"]);
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                        {
                            record.Nationality = Conversion.getStrOrNull(R["Nationality"]);
                            record.NationalityName = Conversion.getStrOrNull(R["NationalityName"]);
                        }
                        record.IntCode = Conversion.getStrOrNull(R["IntCode"]);
                        record.CountryPhoneCode = Conversion.getStrOrNull(R["CountryPhoneCode"]);
                        record.PhoneMask = Conversion.getStrOrNull(R["PhoneMask"]);
                        record.PhoneMaskMob = Conversion.getStrOrNull(R["PhoneMaskMob"]);
                        records.Add(record);
                    }
                    if (records.Count > 0)
                        return records;
                    else
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<List<IntCountryListRecord>>(Common.IntCountryList);
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public IntCountryListRecord getCountryIntCode(string Market, int? country, ref string errorMsg)
        {
            string tsql = @"Select CountryName=isnull(dbo.FindLocalName(NameLID, @Market), Name), 
	                            IntCode=CountryCode,
	                            CountryPhoneCode=IntPhoneCode,
	                            PhoneMask, PhoneMaskMob
                            From Location
                            Where Type=1 And RecID=@Country";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, country);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        IntCountryListRecord record = new IntCountryListRecord();
                        record.CountryName = Conversion.getStrOrNull(R["CountryName"]);
                        record.IntCode = Conversion.getStrOrNull(R["IntCode"]);
                        record.CountryPhoneCode = Conversion.getStrOrNull(R["CountryPhoneCode"]);
                        record.PhoneMask = Conversion.getStrOrNull(R["PhoneMask"]);
                        record.PhoneMaskMob = Conversion.getStrOrNull(R["PhoneMaskMob"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FixNotesRecord getFixNotes(string Market, string Section, ref string errorMsg)
        {
            string tsql = @"Select RecID, Market, Section, [Type], Note
                            From FixNotes (NOLOCK)
                            Where Market=@Market And Section=@Section";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Section", DbType.String, Section);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        FixNotesRecord record = new FixNotesRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Section = Conversion.getStrOrNull(R["Section"]);
                        record.Type = Conversion.getByteOrNull(R["Type"]);
                        record.Note = Conversion.getStrOrNull(R["Note"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public decimal? getTax(User UserData, DateTime? ResDate, int? TaxLocation, ref string errorMsg)
        {
            string tsql = @"Declare @Tax decimal(18,6)
						    Declare @CountryId int

						    SELECT @CountryId = Country FROM Location WHERE RecID = @RecId

                            Select Top 1 @Tax=isnull(R.Rate, 0)
	                        From TaxRate R (NOLOCK) 
                            Join Tax T (NOLOCK) on T.RecID=R.TaxID 
                            Where Market=@Market and Country = @CountryId and @ResDate between R.BegDate and R.EndDate                                                 

                            if @Tax = 0
                            Begin
                                Select @Tax = isnull(TaxPer, 0) 
                                From Location 
                                Where RecID = @CountryId	                        
                            End
                            Select Tax=@Tax ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "ResDate", DbType.DateTime, ResDate);
                db.AddInParameter(dbCommand, "RecID", DbType.Int32, TaxLocation);

                return Conversion.getDecimalOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ContractTextRecord getContractText(string Holpack, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (VersionControl.getTableField("HolPack", "ContractDocType"))
                tsql = @"Select Code,UseContText,ContractText,ContractPict,ContractHtml,ContractDocType,ChgDate From HolPack (NOLOCK) Where Code=@Code";
            else
                tsql = @"Select Code,UseContText,ContractText,ContractPict,ContractHtml,ContractDocType='',ChgDate From HolPack (NOLOCK) Where Code=@Code";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, Holpack);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        ContractTextRecord record = new ContractTextRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.UseContText = Conversion.getStrOrNull(R["UseContText"]);
                        record.ContractText = Conversion.getStrOrNull(R["ContractText"]);
                        record.ContractPict = R["ContractPict"];
                        record.ContractHtml = Conversion.getStrOrNull(R["ContractHtml"]);
                        record.ContractDocType = Conversion.getStrOrNull(R["ContractDocType"]);
                        record.ChgDate = Conversion.getDateTimeOrNull(R["ChgDate"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public HolPackRecord getHolPack(string Market, string Code, ref string errorMsg)
        {
            string tsql = @"Select RecID, Code, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name),
	                            NameS, DepCity, BegDate, EndDate, [Status], Ready, webpub, [Description], 
	                            SaleCur, UseDates, AccomNights, PackType, FlightSeat, CalcType, FixCinType, FixCinDay, AgencyCom,
	                            EBValid, ArrCity, Market, AccountNo1, AccountNo2, AccountNo3, TaxPer, PasEBValid, PNight, RefHotel,
	                            RefBoard, RefRoom, Direction, Category, TANight, OprText
                            From HolPack (NOLOCK)
                            Where Code=@Code";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        HolPackRecord record = new HolPackRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        record.Ready = Conversion.getStrOrNull(R["Ready"]);
                        record.webpub = Conversion.getStrOrNull(R["webpub"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.UseDates = Conversion.getInt16OrNull(R["UseDates"]);
                        record.AccomNights = Conversion.getStrOrNull(R["AccomNights"]);
                        record.PackType = Conversion.getStrOrNull(R["PackType"]);
                        record.FlightSeat = Conversion.getStrOrNull(R["FlightSeat"]);
                        record.CalcType = Conversion.getStrOrNull(R["CalcType"]);
                        record.FixCinType = Conversion.getInt16OrNull(R["FixCinType"]);
                        record.FixCinDay = Conversion.getStrOrNull(R["FixCinDay"]);
                        record.AgencyCom = Conversion.getDecimalOrNull(R["AgencyCom"]);
                        record.EBValid = Conversion.getStrOrNull(R["EBValid"]);
                        record.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.AccountNo1 = Conversion.getStrOrNull(R["AccountNo1"]);
                        record.AccountNo2 = Conversion.getStrOrNull(R["AccountNo2"]);
                        record.AccountNo3 = Conversion.getStrOrNull(R["AccountNo3"]);
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PasEBValid = Conversion.getStrOrNull(R["PasEBValid"]);
                        record.PNight = Conversion.getByteOrNull(R["PNight"]);
                        record.RefHotel = Conversion.getStrOrNull(R["RefHotel"]);
                        record.RefBoard = Conversion.getStrOrNull(R["RefBoard"]);
                        record.RefRoom = Conversion.getStrOrNull(R["RefRoom"]);
                        record.Direction = Conversion.getInt16OrNull(R["Direction"]);
                        record.Category = Conversion.getInt16OrNull(R["Category"]);
                        record.TANight = Conversion.getByteOrNull(R["TANight"]);
                        record.OprText = Conversion.getStrOrNull(R["OprText"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HolPackRecord> getHolPackList(string Market, ref string errorMsg)
        {
            List<HolPackRecord> records = new List<HolPackRecord>();
            string tsql =
@"
Select RecID, Code, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name),
  NameS, DepCity, BegDate, EndDate, [Status], Ready, webpub, [Description], 
  SaleCur, UseDates, AccomNights, PackType, FlightSeat, CalcType, FixCinType, FixCinDay, AgencyCom,
  EBValid, ArrCity, Market, AccountNo1, AccountNo2, AccountNo3, TaxPer, PasEBValid, PNight, RefHotel,
  RefBoard, RefRoom, Direction, Category, TANight, OprText
From HolPack (NOLOCK)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HolPackRecord record = new HolPackRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        record.Ready = Conversion.getStrOrNull(R["Ready"]);
                        record.webpub = Conversion.getStrOrNull(R["webpub"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.UseDates = Conversion.getInt16OrNull(R["UseDates"]);
                        record.AccomNights = Conversion.getStrOrNull(R["AccomNights"]);
                        record.PackType = Conversion.getStrOrNull(R["PackType"]);
                        record.FlightSeat = Conversion.getStrOrNull(R["FlightSeat"]);
                        record.CalcType = Conversion.getStrOrNull(R["CalcType"]);
                        record.FixCinType = Conversion.getInt16OrNull(R["FixCinType"]);
                        record.FixCinDay = Conversion.getStrOrNull(R["FixCinDay"]);
                        record.AgencyCom = Conversion.getDecimalOrNull(R["AgencyCom"]);
                        record.EBValid = Conversion.getStrOrNull(R["EBValid"]);
                        record.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.AccountNo1 = Conversion.getStrOrNull(R["AccountNo1"]);
                        record.AccountNo2 = Conversion.getStrOrNull(R["AccountNo2"]);
                        record.AccountNo3 = Conversion.getStrOrNull(R["AccountNo3"]);
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PasEBValid = Conversion.getStrOrNull(R["PasEBValid"]);
                        record.PNight = Conversion.getByteOrNull(R["PNight"]);
                        record.RefHotel = Conversion.getStrOrNull(R["RefHotel"]);
                        record.RefBoard = Conversion.getStrOrNull(R["RefBoard"]);
                        record.RefRoom = Conversion.getStrOrNull(R["RefRoom"]);
                        record.Direction = Conversion.getInt16OrNull(R["Direction"]);
                        record.Category = Conversion.getInt16OrNull(R["Category"]);
                        record.TANight = Conversion.getByteOrNull(R["TANight"]);
                        record.OprText = Conversion.getStrOrNull(R["OprText"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<holPackBrochureReady> getholPackBrochureReady(string Market, DateTime? BegDate, DateTime? EndDate, ref string errorMsg)
        {
            List<holPackBrochureReady> records = new List<holPackBrochureReady>();
            string tsql = @"Select HB.HolPack, HolPackName=H.Name, 
                                HolPackNameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name), 
                                brochureReady=Case when HB.FileImage is null then Cast(0 AS bit) else Cast(1 As bit) end
                            From HolPackBrochure HB (NOLOCK)
                            Join HolPack H (NOLOCK) ON H.Code=HB.HolPack
                            Where
	                            HB.Market=@Market And
	                            @BegDate<=HB.EndDate And 
                                @Enddate>=HB.BegDate	
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        holPackBrochureReady record = new holPackBrochureReady();
                        record.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        record.HolPackName = Conversion.getStrOrNull(R["HolPackName"]);
                        record.HolPackNameL = Conversion.getStrOrNull(R["HolPackNameL"]);
                        record.brochureReady = (bool)R["brochureReady"];
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public HolPackBrochureRecord getHolPackBrochure(string Market, string holPack, DateTime? BegDate, DateTime? EndDate, ref string errorMsg)
        {
            string tsql = @"Select Top 1 RecID,HolPack,Market,BegDate,EndDate,FileType,FileImage,Description
                            From HolPackBrochure (NOLOCK)
                            Where
	                            Market=@Market And
	                            HolPack=@HolPack And
	                            @BegDate<=EndDate And
                                @EndDate>=BegDate
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "HolPack", DbType.String, holPack);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        HolPackBrochureRecord record = new HolPackBrochureRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        ;
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.FileType = Conversion.getStrOrNull(R["FileType"]);
                        record.FileImage = R["FileImage"];
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public DocumentNoRecord getNewDocumentNo(User UserData, int? DocType, ref string errorMsg)
        {
            string tsql = @"Declare @NewSerialNo VarChar(5), @NewNo int
                            Select Top 1 @NewSerialNo=Serial From 
                            (
	                            Select Distinct Serial From OfficeDocNo D (NOLOCK)
	                            Join OfficeDocNoTemp T (NOLOCK) on T.DocNoID=D.RecID
	                            Where D.Office=@Office And D.Status=1 and DocType=1
                            ) X
                            Exec dbo.sp_Get_NewDocNo @Office, @DocType, @NewSerialNo, @NewNo OUTPUT
                            Select Serial=@NewSerialNo, No=@NewNo
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Office", DbType.String, UserData.OprOffice);
                db.AddInParameter(dbCommand, "DocType", DbType.Int32, DocType);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        DocumentNoRecord record = new DocumentNoRecord();
                        record.Serial = Conversion.getStrOrNull(R["Serial"]);
                        record.No = Conversion.getInt32OrNull(R["No"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool setReceiptSerialNo(User UserData, int? RecID, string RSerial, int RNo, ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string tsql = @"
                            Update Journal
                            Set ReceiptSerial=@Serial,
                                ReceiptNo=@No,
                                ReceiptDate=@Date,
                                ReceiptPrt='Y'
                            Where RecID=@RecID
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "RecID", DbType.Int32, RecID);
                db.AddInParameter(dbCommand, "Serial", DbType.String, RSerial);
                db.AddInParameter(dbCommand, "No", DbType.Int32, RNo);
                db.AddInParameter(dbCommand, "Date", DbType.DateTime, DateTime.Today.Date);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getCompulsoryMael(string plMarket, ref string errorMsg)
        {
            string retVal = string.Empty;
            string jSonCompulsoryMael = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "CompulsoryMael"));
            if (string.IsNullOrEmpty(jSonCompulsoryMael))
                return retVal;
            else
            {
                try
                {
                    List<CompulsoryMealRecord> compulsoryMaelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CompulsoryMealRecord>>(jSonCompulsoryMael);
                    CompulsoryMealRecord compulsoryMael = compulsoryMaelList.Find(f => Equals(f.Market, plMarket));
                    if (compulsoryMael != null)
                        retVal = compulsoryMael.CompulsoryMeal;
                }
                catch
                {
                    retVal = string.Empty;
                }
                return retVal;
            }
        }

        public string getCompulsorymainService(string plMarket, ref string errorMsg)
        {
            string retVal = string.Empty;
            string jSonCompulsoryMael = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "CompulsoryMael"));
            if (string.IsNullOrEmpty(jSonCompulsoryMael))
                return retVal;
            else
            {
                try
                {
                    List<CompulsoryMealRecord> compulsoryMaelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CompulsoryMealRecord>>(jSonCompulsoryMael);
                    CompulsoryMealRecord compulsoryMael = compulsoryMaelList.Find(f => Equals(f.Market, plMarket));
                    if (compulsoryMael != null)
                        retVal = compulsoryMael.CompulsoryMeal;
                }
                catch
                {
                    retVal = string.Empty;
                }
                return retVal;
            }
        }
        public ReservationCustomCompulsoryCheck getReservationCustomCompulsoryCheck(string plMarket, ref string errorMsg)
        {
            string jSonCompulsoryMael = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ReservationCustomCompulsoryCheck"));
            if (string.IsNullOrEmpty(jSonCompulsoryMael))
                return null;
            else
            {
                try
                {
                    List<ReservationCustomCompulsoryCheck> compulsoryMaelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ReservationCustomCompulsoryCheck>>(jSonCompulsoryMael);
                    ReservationCustomCompulsoryCheck compulsoryMael = compulsoryMaelList.Find(f => Equals(f.Market, plMarket));
                    if (compulsoryMael != null)
                        return compulsoryMael;
                }
                catch
                {
                    return null;
                }
                return null;
            }
        }
        public ReservationCustomCompulsoryCheck getReservationCustomNotCompulsoryCheck(string plMarket, ref string errorMsg)
        {
            string jSonCompulsoryMael = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ReservationCustomNotCompulsoryCheck"));
            if (string.IsNullOrEmpty(jSonCompulsoryMael))
                return null;
            else
            {
                try
                {
                    List<ReservationCustomCompulsoryCheck> compulsoryMaelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ReservationCustomCompulsoryCheck>>(jSonCompulsoryMael);
                    ReservationCustomCompulsoryCheck compulsoryMael = compulsoryMaelList.Find(f => Equals(f.Market, plMarket));
                    if (compulsoryMael != null)
                        return compulsoryMael;
                }
                catch
                {
                    return null;
                }
                return null;
            }
        }
        public static short getDayOfWeekTv(DayOfWeek doW)
        {
            short dayOfW = 1;
            switch (doW)
            {
                case DayOfWeek.Monday:
                    dayOfW = 1;
                    break;
                case DayOfWeek.Tuesday:
                    dayOfW = 2;
                    break;
                case DayOfWeek.Wednesday:
                    dayOfW = 3;
                    break;
                case DayOfWeek.Thursday:
                    dayOfW = 4;
                    break;
                case DayOfWeek.Friday:
                    dayOfW = 5;
                    break;
                case DayOfWeek.Saturday:
                    dayOfW = 6;
                    break;
                case DayOfWeek.Sunday:
                    dayOfW = 7;
                    break;

            }
            return dayOfW;
        }
        /// <summary>
        /// NewReservation parameters 0:ResNo
        /// </summary>
        /// <param name="pType"></param>
        /// <param name="pMarket"></param>
        /// <returns></returns>
        public static string GetSmsText(SmsBo.SmsType pType, string pMarket)
        {
            string retVal = string.Empty;
            object _smsTexts = new TvBo.Common().getFormConfigValue("MakeRes", "SMSTexts");
            if (!string.IsNullOrEmpty(Conversion.getStrOrNull(_smsTexts)))
            {
                List<SmsBo> smsTextList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SmsBo>>(Conversion.getStrOrNull(_smsTexts));
                if (smsTextList != null && smsTextList.Count > 0)
                    retVal = smsTextList.Find(f => f.Market == pMarket && f.Type == pType).Text;
            }
            return retVal;
        }
    }
}

public static class SANCyrpto
{
    private const string saltValue = "S@Nt5G";
    private const string hashAlgorithm = "SHA1";
    private const int passwordIterations = 2;
    private const string initVector = "@1B2c3Dq@5F6x7H8";
    private const int keySize = 256;

    public static string Encyrpt(string passPhrase, string plainText)
    {
        byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
        byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        PasswordDeriveBytes password =
            new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

        byte[] keyBytes = password.GetBytes(keySize / 8);

        RijndaelManaged symmetricKey = new RijndaelManaged();

        symmetricKey.Mode = CipherMode.CBC;

        ICryptoTransform encryptor =
            symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);

        MemoryStream memoryStream = new MemoryStream();

        CryptoStream cryptoStream =
            new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

        cryptoStream.FlushFinalBlock();

        byte[] cipherTextBytes = memoryStream.ToArray();

        memoryStream.Close();
        cryptoStream.Close();

        string cipherText = Convert.ToBase64String(cipherTextBytes);

        return cipherText;
    }

    public static string Encyrpt(string passPhrase, string plainText, bool URLEncode)
    {
        if (URLEncode)
            return HttpContext.Current.Server.UrlEncode(Encyrpt(passPhrase, plainText));
        else
            return Encyrpt(passPhrase, plainText);
    }

    public static string Decyrpt(string passPhrase, string cipherText)
    {
        byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
        byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

        byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

        PasswordDeriveBytes password =
            new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

        byte[] keyBytes = password.GetBytes(keySize / 8);

        RijndaelManaged symmetricKey = new RijndaelManaged();

        symmetricKey.Mode = CipherMode.CBC;

        ICryptoTransform decryptor =
            symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);

        MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

        CryptoStream cryptoStream =
            new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

        byte[] plainTextBytes = new byte[cipherTextBytes.Length];

        int decryptedByteCount =
            cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

        memoryStream.Close();
        cryptoStream.Close();

        string plainText =
            Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);

        return plainText;
    }

    public static string Decrypt(string passPhrase, string cipherText, bool URLDecode)
    {
        if (URLDecode)
            return Decyrpt(passPhrase, HttpContext.Current.Server.UrlDecode(cipherText));
        else
            return Decyrpt(passPhrase, cipherText);
    }
}
