﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;
using System.Web;

namespace TvBo
{
    public class AdServices
    {
        public AdServiceRecord getAdService(string Market, string Service, string Code, ref string errorMsg)
        {
            AdServiceRecord record = new AdServiceRecord();
            string tsql = @"Select [Service], Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market), [Name]),
	                          NameS, [Description], [Status], 
	                          ConfStat, TaxPer, PayCom, PayComEB, PayPasEB, CanDiscount
                            From AdService (NOLOCK)
                            Where [Service]=@Service
                              And Code=@Code
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Service", DbType.String, Service);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.Status = Equals(R["Status"], "Y");
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AdServicePriceRecord> getAdServicePrices(User UserData, ResDataRecord ResData, string Service, string Code, int? Country, int? Location, DateTime? BegDate, DateTime? EndDate, bool Compulsory, bool IncPack, ref string errorMsg)
        {
            List<AdServicePriceRecord> records = new List<AdServicePriceRecord>();
            string tsql = @"Declare @UserCountry VarChar(10)
                            Select @UserCountry=Country From Market (NOLOCK) Where Code=@Market

                            Select S.[Service], S.AdService, S.Market, S.Supplier, 
                                S.Country, CountryName=(Select [Name] From Location (NOLOCK) Where RecID=S.Country),
                                CountryLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=S.Country),
                                S.BegDate, S.EndDate, 
                                S.Location, LocationName=(Select [Name] From Location (NOLOCK) Where RecID=S.Location),
                                LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=S.Location),
                                S.PriceType, S.CalcType, S.NetCur, S.SaleCur, S.ChdG1MaxAge, S.ChdG2MaxAge, S.ChdG3MaxAge, 
                                S.SaleBegDate, S.SaleEndDate, S.FromPax, S.ToPax, S.FromDay, S.ToDay, S.Compulsory, S.UseInPL, 
                                S.ChdG1UseAllot, S.ChdG2UseAllot, S.ChdG3UseAllot
                            From AdServicePrice (NOLOCK) S
                            Left Join ServiceMarOpt SMP (NOLOCK) ON SMP.Service = S.Service And SMP.Market = @Market
                            Where dbo.DateOnly(GetDate()) between S.SaleBegDate And S.SaleEndDate 
                              And S.Market=@plMarket
                              And ((isnull(S.Agency, '')='' Or S.Agency=@AgencyID) 
                              And (isnull(S.AgencyGrp, '')='' Or Exists(Select RecID From GrpAgencyDet Where GrpType='P' And Agency=@AgencyID And Groups=S.AgencyGrp))) 
                            ";
            if (!(Compulsory || IncPack))
                tsql += " And isnull(SMP.WebSale, 'N') = 'Y' ";
            string whereStr = " ";
            if (!string.IsNullOrEmpty(Service))
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += string.Format(" S.[Service]='{0}' ", Service);
            }

            if (!string.IsNullOrEmpty(Code))
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += string.Format(" AdService='{0}' ", Code);
            }

            if (Country != null)
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += string.Format(" (Country is null or Country={0} Or Country=@UserCountry) ", new Locations().getLocationForCountry(Country.HasValue ? Country.Value : -1, ref errorMsg));
            }
            else
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += string.Format(" Country=@UserCountry ", new Locations().getLocationForCountry(Country.HasValue ? Country.Value : -1, ref errorMsg));
            }

            if (Location != null && Location != -999)
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += string.Format(" Location is null or Location={0} ", Location);
            }
            if (BegDate.HasValue)
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += " @BegDate >= BegDate ";
            }
            if (EndDate.HasValue)
            {
                if (whereStr.Length > 0)
                    whereStr += " And";
                whereStr += " @EndDate <= EndDate ";
            }

            if (whereStr.Length > 1)
                tsql = tsql + whereStr;

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, ResData.ResMain.PLMarket);
                db.AddInParameter(dbCommand, "AgencyID", DbType.String, UserData.AgencyID);

                if (BegDate.HasValue)
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate.Value);
                if (EndDate.HasValue)
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate.Value);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        int? _Location = Conversion.getInt32OrNull(R["Location"]);
                        string _LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        string _LocationLocalName = Conversion.getStrOrNull(R["LocationLocalName"]);
                        AdServicePriceRecord record = new AdServicePriceRecord
                                                          {
                                                              Service = Conversion.getStrOrNull(R["Service"]),
                                                              AdService = Conversion.getStrOrNull(R["AdService"]),
                                                              Market = Conversion.getStrOrNull(R["Market"]),
                                                              Supplier = Conversion.getStrOrNull(R["Supplier"]),
                                                              Country = Conversion.getInt32OrNull(R["Country"]),
                                                              CountryName = Conversion.getStrOrNull(R["CountryName"]),
                                                              CountryLocalName =
                                                                  Conversion.getStrOrNull(R["CountryLocalName"]),
                                                              BegDate = (DateTime)R["BegDate"],
                                                              EndDate = (DateTime)R["EndDate"],
                                                              Location = _Location.HasValue ? _Location : -999,
                                                              LocationName = string.IsNullOrEmpty(_LocationName) ? (ResData.Resource != ResResource.PytonG7 ? HttpContext.GetGlobalResourceObject("Controls", "comboAllLocation").ToString() : "Alle lokaties") : _LocationName,
                                                              LocationLocalName = string.IsNullOrEmpty(_LocationLocalName) ? (ResData.Resource != ResResource.PytonG7 ? HttpContext.GetGlobalResourceObject("LibraryResource", "AllLocation").ToString() : "Alle lokaties") : _LocationLocalName,
                                                              PriceType = (Int16)R["PriceType"],
                                                              CalcType = (Int16)R["CalcType"],
                                                              NetCur = Conversion.getStrOrNull(R["NetCur"]),
                                                              SaleCur = Conversion.getStrOrNull(R["SaleCur"]),
                                                              ChdG1MaxAge =
                                                                  Conversion.getDecimalOrNull(R["ChdG1MaxAge"]),
                                                              ChdG2MaxAge =
                                                                  Conversion.getDecimalOrNull(R["ChdG2MaxAge"]),
                                                              ChdG3MaxAge =
                                                                  Conversion.getDecimalOrNull(R["ChdG3MaxAge"]),
                                                              SaleBegDate = (DateTime)R["SaleBegDate"],
                                                              SaleEndDate = (DateTime)R["SaleEndDate"],
                                                              FromPax = Conversion.getInt16OrNull(R["FromPax"]),
                                                              ToPax = Conversion.getInt16OrNull(R["ToPax"]),
                                                              FromDay = Conversion.getInt16OrNull(R["FromDay"]),
                                                              ToDay = Conversion.getInt16OrNull(R["ToDay"]),
                                                              Compulsory = (bool)R["Compulsory"],
                                                              UseInPL = (bool)R["UseInPL"],
                                                              ChdG1UseAllot = (bool)R["ChdG1UseAllot"],
                                                              ChdG2UseAllot = (bool)R["ChdG2UseAllot"],
                                                              ChdG3UseAllot = (bool)R["ChdG3UseAllot"]
                                                          };

                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public DataRow getAdServiceDetail(User UserData, ResDataRecord ResData, string Service, string Code, int? Country, int? Location, DateTime? BegDate, DateTime? EndDate, bool Compulsory, bool IncPack, ref string errorMsg)
        {
            AdServiceRecord adService = getAdService(UserData.Market, Service, Code, ref errorMsg);
            List<AdServicePriceRecord> adServicePrice = getAdServicePrices(UserData, ResData, Service, Code, Country, Location, BegDate, EndDate, Compulsory, IncPack, ref errorMsg);
            var query = from q in adServicePrice.AsEnumerable()
                        where q.Service == adService.Service && q.AdService == adService.Code
                        select new
                        {
                            adService.Service,
                            adService.Code,
                            adService.Name,
                            adService.LocalName,
                            q.Country,
                            q.CountryName,
                            q.CountryLocalName,
                            q.Location,
                            q.LocationName,
                            q.LocationLocalName,
                            q.BegDate,
                            q.EndDate,
                            adService.CanDiscount
                        };
            return new TvBo.Common().LINQToDataRow(query);
        }

        public List<AdServiceRecord> getAdServiceLocations(string Market, string plMarket, DateTime? ResBegDate, DateTime? ResEndDate, int? Country, int? Location, ref string errorMsg)
        {
            List<AdServiceRecord> list = new List<AdServiceRecord>();
            string tsql = @"Select  S.[Service], S.Code, S.[Name], LocalName=isnull(dbo.FindLocalName(S.NameLID, @Market), S.[Name]),
                                S.NameS, S.[Description], S.[Status], 
                                S.ConfStat, S.TaxPer, S.PayCom, S.PayComEB, S.PayPasEB, S.CanDiscount 
                            From (
	                            Select Distinct Service, AdService
	                            From AdServicePrice (NOLOCK)
	                            Where Market=@plMarket
	                             And (Country is null or Country=@Country)
	                             And (Location is null or Location=@Location)
                                 And (@ResBegDate is null Or BegDate<=@ResBegDate)
                                 And (@ResEndDate is null Or EndDate>=@ResEndDate)
                            ) X
                            Join AdService S (NOLOCK) on S.Code=X.AdService And S.Service=X.Service 
                            Join ServiceMarOpt SMP (NOLOCK) ON SMP.Service=S.Service And SMP.Market=@Market And SMP.WebSale='Y' 
                            Where S.ShowInResB2B=1
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, Country);
                db.AddInParameter(dbCommand, "Location", DbType.Int32, Location == -999 ? null : Location);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.DateTime, ResBegDate);
                db.AddInParameter(dbCommand, "ResEndDate", DbType.DateTime, ResEndDate);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AdServiceRecord record = new AdServiceRecord();
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.Status = Equals(R["Status"], "Y");
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}