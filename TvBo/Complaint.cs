﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using TvTools;

namespace TvBo
{
    public class Complaints
    {
        public ComplaintRecord getComplaint(User UserData, int? RecID, ref string errorMsg)
        {
            string tsql =
@"
Select RecID,ResNo,PasTrfID,GroupNo,GuestName,Agency,TrfFrom,TrfTo,[Status],Category,ComplaintDate,ComplaintSource,ComplaintExplain,
  Investigation,DepartmentAction,ManagementAction,Active,DepartmentID,ReporterName,Supplier,Hotel,[Type],LastInformToGuest,
  GuestRight,CompExplain,CompAmount,CompAmountCur,CompTo
From Complaint C (NOLOCK) 
Where Exists(Select null From ResMain RM (NOLOCK) 
             Join Agency A (NOLOCK) ON RM.Agency=A.Code
             Where ResNo=C.ResNo 
               And @Agency=(Case When A.MainOffice='' Then A.Code Else A.MainOffice End))
  And C.RecID=@RecID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "RecID", DbType.Int32, RecID);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    if (R.Read()) {
                        return new ComplaintRecord {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            ResNo = Conversion.getStrOrNull(R["ResNo"]),
                            PasTrfID = Conversion.getInt32OrNull(R["PasTrfID"]),
                            GroupNo = Conversion.getInt32OrNull(R["GroupNo"]),
                            GuestName = Conversion.getStrOrNull(R["GuestName"]),
                            Agency = Conversion.getInt32OrNull(R["Agency"]),
                            TrfFrom = Conversion.getInt32OrNull(R["TrfFrom"]),
                            TrfTo = Conversion.getInt32OrNull(R["TrfTo"]),
                            Status = Conversion.getInt16OrNull(R["Status"]),
                            Category = Conversion.getInt32OrNull(R["Category"]),
                            ComplaintDate = Conversion.getDateTimeOrNull(R["ComplaintDate"]),
                            ComplaintSource = Conversion.getInt16OrNull(R["ComplaintSource"]),
                            ComplaintExplain = Conversion.getStrOrNull(R["ComplaintExplain"]),
                            Investigation = Conversion.getStrOrNull(R["Investigation"]),
                            DepartmentAction = Conversion.getStrOrNull(R["DepartmentAction"]),
                            ManagementAction = Conversion.getStrOrNull(R["ManagementAction"]),
                            Active = Conversion.getBoolOrNull(R["Active"]),
                            DepartmentID = Conversion.getInt32OrNull(R["DepartmentID"]),
                            ReporterName = Conversion.getStrOrNull(R["ReporterName"]),
                            Supplier = Conversion.getInt32OrNull(R["Supplier"]),
                            Hotel = Conversion.getInt32OrNull(R["Hotel"]),
                            Type = Conversion.getInt16OrNull(R["Type"]),
                            LastInformToGuest = Conversion.getDateTimeOrNull(R["LastInformToGuest"]),
                            GuestRight = Conversion.getBoolOrNull(R["GuestRight"]),
                            CompExplain = Conversion.getStrOrNull(R["CompExplain"]),
                            CompAmount = Conversion.getDecimalOrNull(R["CompAmount"]),
                            CompAmountCur = Conversion.getStrOrNull(R["CompAmountCur"]),
                            CompTo = Conversion.getInt16OrNull(R["CompTo"])
                        };
                    } else {
                        return null;
                    }
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<ComplaintList> getComplaintList(User UserData, DateTime? BegDate, DateTime? EndDate, Int16? Type, bool? Active, int? Department, string Hotel, ref string errorMsg)
        {
            List<ComplaintList> records = new List<ComplaintList>();

            string tsql =
@"
Select C.RecID,haveAttach=case when Exists(Select RecID from ComplaintPict where CompId=C.RecId) then Cast(1 AS Bit) else Cast(0 AS Bit) end,  
    DepartmentID,DepartmentName=CD.Name,DepartmentNameL=isnull(dbo.FindLocalName(CD.NameLID,@Market),CD.Name),
    HotelCode=H.Code,HotelName=H.Name,HotelNameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),
    ComplaintSource,ComplaintSourceName=R.Name,ComplaintSourceNameL=isnull(dbo.FindLocalName(R.NameLID,@Market),R.Name),
    C.[Status],StatusName=S.Name,StatusNameL=isnull(dbo.FindLocalName(S.NameLID,@Market),S.Name),
    C.Category,CategoryName=CG.Name,CategoryNameL=isnull(dbo.FindLocalName(CG.NameLID,@Market),CG.Name),
    ReceivedDate=C.ComplaintDate,Active,[Type]
From Complaint C (NOLOCK) 
Left Join CompDepartment CD (NOLOCK) ON C.DepartmentID=CD.RecID
Left Join Hotel H (NOLOCK) ON C.Hotel=H.RecID
Left Join CompReporter R (NOLOCK) ON C.ComplaintSource=R.RecID
Left Join CompStatus S (NOLOCK) ON C.[Status]=S.RecID
Left Join CompCategory CG (NOLOCK) ON C.Category=CG.RecID
Where Exists(Select null From ResMain RM (NOLOCK) 
             Join Agency A (NOLOCK) ON RM.Agency=A.Code
             Where ResNo=C.ResNo 
               And @Agency=(Case When A.MainOffice='' Then A.Code Else A.MainOffice End))
";
            if (BegDate.HasValue && EndDate.HasValue) {
                tsql +=
@"
  And C.ComplaintDate between @BegDate and @EndDate
";
            } else if (BegDate.HasValue || EndDate.HasValue) {
                if (BegDate.HasValue)
                    tsql +=
@"
  And C.ComplaintDate>=@BegDate
";
                else
                    tsql +=
@"
  And C.ComplaintDate<=@EndDate
";
            }
            if (Type.HasValue) {
                tsql += string.Format(@"
  And [Type]={0}
", Type);
            }
            if (Active.HasValue) {
                tsql += string.Format(@"
  And Active={0}
", Active.Value ? 1 : 0);
            }
            if (Department.HasValue) {
                tsql += string.Format(@"
  And DepartmentID={0}
", Department);
            }
            if (!string.IsNullOrEmpty(Hotel)) {
                tsql += string.Format(@"
  And H.Code='{0}'
", Hotel);
            }
            tsql +=
@"
Order by C.RecID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                if (BegDate.HasValue)
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                if (EndDate.HasValue)
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        records.Add(new ComplaintList {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            HaveAttach = (bool)R["HaveAttach"],
                            DepartmentID = Conversion.getInt32OrNull(R["DepartmentID"]),
                            DepartmentName = Conversion.getStrOrNull(R["DepartmentName"]),
                            DepartmentNameL = Conversion.getStrOrNull(R["DepartmentNameL"]),
                            HotelCode = Conversion.getStrOrNull(R["HotelCode"]),
                            HotelName = Conversion.getStrOrNull(R["HotelName"]),
                            HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]),
                            ComplaintSource = Conversion.getInt32OrNull(R["ComplaintSource"]),
                            ComplaintSourceName = Conversion.getStrOrNull(R["ComplaintSourceName"]),
                            ComplaintSourceNameL = Conversion.getStrOrNull(R["ComplaintSourceNameL"]),
                            Status = Conversion.getInt32OrNull(R["Status"]),
                            StatusName = Conversion.getStrOrNull(R["StatusName"]),
                            StatusNameL = Conversion.getStrOrNull(R["StatusNameL"]),
                            Category = Conversion.getInt32OrNull(R["Category"]),
                            CategoryName = Conversion.getStrOrNull(R["CategoryName"]),
                            CategoryNameL = Conversion.getStrOrNull(R["CategoryNameL"]),
                            ReceivedDate = Conversion.getDateTimeOrNull(R["ReceivedDate"]),
                            Active = Conversion.getBoolOrNull(R["Active"]),
                            ComplaintType = Conversion.getInt16OrNull(R["Type"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<CodeName> getComplaintHotelList(User UserData, bool UseLocalName, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();

            string tsql =
@"
Select Distinct Code=H.Code,Name=H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name)
From Complaint C (NOLOCK) 
Join Hotel H (NOLOCK) ON C.Hotel=H.RecID
Where Exists(Select null From ResMain RM (NOLOCK) 
             Join Agency A (NOLOCK) ON RM.Agency=A.Code
             Where ResNo=C.ResNo 
               And @Agency=(Case When A.MainOffice='' Then A.Code Else A.MainOffice End))
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        records.Add(new CodeName {
                            Code = Conversion.getStrOrNull(R["Code"]),
                            Name = UseLocalName ? Conversion.getStrOrNull(R["NameL"]) : Conversion.getStrOrNull(R["Name"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<CompDepartment> getComplaintDepartment(User UserData, ref string errorMsg)
        {
            List<CompDepartment> records = new List<CompDepartment>();

            string tsql =
@"
Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) 
From CompDepartment (NOLOCK)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        records.Add(new CompDepartment {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            NameL = Conversion.getStrOrNull(R["NameL"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<CompCategory> getComplaintCategory(User UserData, ref string errorMsg)
        {
            List<CompCategory> records = new List<CompCategory>();

            string tsql =
@"
Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),CatType 
From CompCategory (NOLOCK)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        records.Add(new CompCategory {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            NameL = Conversion.getStrOrNull(R["NameL"]),
                            CatType = Conversion.getInt32OrNull(R["CatType"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<CompReporter> getComplaintReporter(User UserData, ref string errorMsg)
        {
            List<CompReporter> records = new List<CompReporter>();

            string tsql =
@"
Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) 
From CompReporter (NOLOCK)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        records.Add(new CompReporter {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            NameL = Conversion.getStrOrNull(R["NameL"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<CompStatus> getComplaintStatus(User UserData, ref string errorMsg)
        {
            List<CompStatus> records = new List<CompStatus>();

            string tsql =
@"
Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) 
From CompStatus (NOLOCK)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        records.Add(new CompStatus {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            NameL = Conversion.getStrOrNull(R["NameL"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public bool updateComplaint(User UserData, ComplaintRecord ComplaintRec, ref string errorMsg)
        {
            return true;
        }
        public bool insertComplaint(User UserData, ComplaintRecord ComplaintRec, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO Complaint (ResNo,GroupNo,GuestName,Agency,TrfFrom,TrfTo,[Status],Category,ComplaintDate,
					   ComplaintSource,ComplaintExplain,DepartmentID,ReporterName,Hotel,[Type])
			   VALUES (@ResNo,@GroupNo,@GuestName,@Agency,@TrfFrom,@TrfTo,@Status,@Category,@ComplaintDate,
					   @ComplaintSource,@ComplaintExplain,@DepartmentID,@ReporterName,@Hotel,@Type)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "ResNo", DbType.AnsiString, ComplaintRec.ResNo);
                db.AddInParameter(dbCommand, "GroupNo", DbType.Int32, ComplaintRec.GroupNo);
                db.AddInParameter(dbCommand, "GuestName", DbType.String, ComplaintRec.GuestName);
                db.AddInParameter(dbCommand, "Agency", DbType.Int32, ComplaintRec.Agency);
                db.AddInParameter(dbCommand, "TrfFrom", DbType.Int32, ComplaintRec.TrfFrom);
                db.AddInParameter(dbCommand, "TrfTo", DbType.Int32, ComplaintRec.TrfTo);
                db.AddInParameter(dbCommand, "Status", DbType.Int16, ComplaintRec.Status);
                db.AddInParameter(dbCommand, "Category", DbType.Int32, ComplaintRec.Category);
                db.AddInParameter(dbCommand, "ComplaintDate", DbType.DateTime, ComplaintRec.ComplaintDate);
                db.AddInParameter(dbCommand, "ComplaintSource", DbType.Int16, ComplaintRec.ComplaintSource);
                db.AddInParameter(dbCommand, "ComplaintExplain", DbType.String, ComplaintRec.ComplaintExplain);
                db.AddInParameter(dbCommand, "DepartmentID", DbType.Int32, ComplaintRec.DepartmentID);
                db.AddInParameter(dbCommand, "ReporterName", DbType.String, ComplaintRec.ReporterName);
                db.AddInParameter(dbCommand, "Hotel", DbType.Int32, ComplaintRec.Hotel);
                db.AddInParameter(dbCommand, "Type", DbType.Int16, ComplaintRec.Type);

                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return false;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }            
        }
        public bool saveComplaint(User UserData, bool? Edit, ComplaintRecord ComplaintRec, ref string errorMsg)
        {
            if (Edit.HasValue && Edit.Value) {
               return updateComplaint(UserData, ComplaintRec, ref errorMsg);
            } else {
               return insertComplaint(UserData, ComplaintRec, ref errorMsg);
            }            
        }
        public List<object> getImageList(User UserData, int? CompId, ref string errorMsg)
        {
            List<object> records = new List<object>();

            string tsql =
@"
Select Picture
From ComplaintPict (NOLOCK)
Where CompId=@CompId
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "CompId", DbType.Int32, CompId);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        records.Add(R["Picture"]);
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public bool saveComplaintPicture(User UserData, string filePath, int? CompId, ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string tsql=
@"
Declare @picCount int
Select @picCount=Count(CompId) From ComplaintPict (NOLOCK) Where CompId=@CompId
if @picCount<3
Begin
  INSERT INTO ComplaintPict(CompId,Picture)
         VALUES (@CompId,@Picture)           
End
";
            if (File.Exists(filePath) && CompId.HasValue) {
                byte[] imageObj = File.ReadAllBytes(filePath);

                Database db = (Database)DatabaseFactory.CreateDatabase();
                DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
                try {
                    db.AddInParameter(dbCommand, "CompId", DbType.Int32, CompId);
                    db.AddInParameter(dbCommand, "Picture", DbType.Binary, imageObj);
                    db.ExecuteNonQuery(dbCommand);
                    return true;
                }
                catch (Exception Ex) {
                    errorMsg = Ex.Message;
                    return false;
                }
                finally {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
            } else {
                return false;
            }
        }
    }
}
