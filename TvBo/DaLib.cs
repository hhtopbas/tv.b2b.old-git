﻿using System;
using System.Collections.Generic;
using System.Data;

namespace TvBo
{
    public static class DaLib
    {
        public static IEnumerable<Object[]> DataRecord(this IDataReader inRecord, Object toRecord)
        {
            if (inRecord== null)
                throw new ArgumentNullException("inRecord");

              
            while (inRecord.Read())
            {
                Object[] row = new Object[inRecord.FieldCount];                
                inRecord.GetValues(row);
                yield return row;
            }
        }
    }
    
}
