﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using TvTools;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace TvBo
{
    public class StopSales
    {
        public List<StopSaleCityRecord> GetDepCity(User UserData, ref string errorMsg)
        {
            List<StopSaleCityRecord> list = new List<StopSaleCityRecord>();
            string tsql = @"Select RecID=DepCity, Name=dbo.GetLocationNameL(DepCity, @Market)
                            From (
                                    Select Distinct CP.DepCity
                                    From CatalogPack CP (NOLOCK)
                                    JOIN CatPriceA CPA (NOLOCK) ON CP.RecID=CPA.CatPackID
                                    JOIN (
                                            Select  Distinct HotelCode=Hotel
                                            From StopSale (NOLOCK)
                                            Where (Market=@Market Or isnull(Market, '')='')
                                                And	Status='Y'
                                                And ForB2B=1
                                                And EndDate>=GetDate() 
                                        ) X ON CPA.Hotel=X.HotelCode
                                    Where	CP.Ready='Y'
                                        And CP.WebPub='Y'
                                        And CPA.Status=1
                                        And CP.WebPubDate<=GetDate()	
                                        And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate 
                                ) X";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "@Market", System.Data.DbType.String, UserData.Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        StopSaleCityRecord rec = new StopSaleCityRecord();
                        rec.RecID = (int)Conversion.getInt32OrNull(oReader["RecID"]);
                        rec.Name = Conversion.getStrOrNull(oReader["Name"]);
                        list.Add(rec);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
            }
        }

        public List<CodeName> getStopSaleListHotels(User UserData, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = @"Select * From 
                        (
                            Select  Distinct HotelCode = Hotel, 
                                    Hotel= isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) + ' (' + (Select isnull(dbo.FindLocalName(NameLID, @Market),Name) From Location (NOLOCK) Where RecID = Hotel.Location)  + ')'
                                                   From Hotel (NOLOCK) Where Code = StopSale.Hotel), '')
                            From StopSale (NOLOCK)
                            Where (Market = @Market OR isnull(Market, '') = '')
                                And	Status = 'Y'
                                And ForB2B = 1
                                AND EndDate >= dbo.DateOnly(GetDate())
                        ) X
                        Order By Hotel ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "@Market", System.Data.DbType.String, UserData.Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        CodeName rec = new CodeName();
                        rec.Code = Conversion.getStrOrNull(oReader["HotelCode"]);
                        rec.Name = Conversion.getStrOrNull(oReader["Hotel"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
            }
        }

        public List<CodeName> getStopSaleArrCityHotels(User UserData, DateTime? begDate, DateTime? endDate, int? arrCity, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            if (arrCity.HasValue)
                tsql = @"Select Distinct HotelCode, Hotel From 
                        (
                            Select  Distinct HotelCode=Hotel, 
                                    Hotel=isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) + ' (' + (Select isnull(dbo.FindLocalName(NameLID, @Market),Name) From Location (NOLOCK) Where RecID = Hotel.Location)  + ' )'
                                                  From Hotel (NOLOCK) Where Code = StopSale.Hotel), '')
                            From StopSale (NOLOCK)
                            Where Market=@Market 
                                AND	Status='Y'
                                AND ForB2B=1
                                AND EndDate>=GetDate() 
                                AND (@BegDate is null or EndDate>=@BegDate)
                                AND (@EndDate is null or BegDate<=@EndDate)
                                AND Exists(Select * From Location (NOLOCK) 
		                                   Where RecID in (Select Location From Hotel (NOLOCK) Where Code=StopSale.Hotel)
				                             And City=@ArrCity)
                        ) X
                        Order By Hotel";
            else
                tsql = @"Select Distinct HotelCode, Hotel From 
                        (
                            Select  Distinct HotelCode=Hotel, 
                                    Hotel=isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) + ' (' + (Select isnull(dbo.FindLocalName(NameLID, @Market),Name) From Location (NOLOCK) Where RecID = Hotel.Location)  + ' )'
                                                  From Hotel (NOLOCK) Where Code = StopSale.Hotel), '')
                            From StopSale (NOLOCK)
                            Where Market = @Market 
                                AND	Status = 'Y'
                                AND ForB2B=1
                                AND EndDate >= GetDate() 
                                AND (@BegDate is null or EndDate >= @BegDate)
	                            AND (@EndDate is null or BegDate <= @EndDate)		                        
                        ) X
                        Order By Hotel";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, begDate);
                db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, endDate);
                if (arrCity.HasValue)
                    db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, arrCity.Value);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        CodeName rec = new CodeName();
                        rec.Code = Conversion.getStrOrNull(oReader["HotelCode"]);
                        rec.Name = Conversion.getStrOrNull(oReader["Hotel"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
            }
        }

        public List<StopSaleResultRecord> getStopSaleStandartResult(User UserData, DateTime? begDate, DateTime? endDate, string Hotel, int? depCity, int? arrCity, ref string error)
        {
            List<StopSaleResultRecord> list = new List<StopSaleResultRecord>();
            string tsql = string.Format(@"Select HotelCode, (Hotel + Case WHEN HotelCode='_ALL' THEN ' (' + Location + ')' ELSE '' END) Hotel, 
                                        Category, Room, Accom, Board, BegDate, EndDate, PriceList, AppType, Location, City, AllotType
                                        From (
                                        Select  
                                            HotelCode=CASE WHEN Hotel='' THEN '_ALL' ELSE Hotel END, 
                                            Hotel=isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) + ' (' + (Select isnull(dbo.FindLocalName(NameLID, @Market),Name) From Location (NOLOCK) Where RecID=Hotel.Location)  + ' )' + ' (* ' + Category + ')'
                                                               From Hotel (NOLOCK) Where Code=StopSale.Hotel), '_ALL'), 
                                            Category=isnull((Select Category From Hotel (NOLOCK) Where Code=StopSale.Hotel), '_ALL'),
                                            Room=isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Room (NOLOCK) Where Code=StopSale.Room), '_ALL'), 
                                            Accom=(CASE WHEN isnull(Accom, '')='' THEN '_ALL' ELSE Accom END),
                                            Board=isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Board (NOLOCK) Where Code=StopSale.Board), '_ALL'), 
                                            BegDate, EndDate, 
                                            PriceList=isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From CatalogPack (NOLOCK) Where RecID=StopSale.PriceListNo), '_ALL'), 
                                            AppType,        
                                            Location=isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=StopSale.Location), '_ALL'), 
                                            City=isnull((Select City From Location (NOLOCK) Where RecID=StopSale.Location), 0),
                                            isnull(AllotType, 9) AllotType
                                        From StopSale (NOLOCK)
                                        Where	(Market=@Market Or isnull(Market, '')='')
                                            AND Status='Y'
                                            AND ForB2B=1
                                            AND EndDate>=dbo.DateOnly(GetDate())
                                            AND (@BegDate is null or EndDate >= @BegDate)
                                            AND (@EndDate is null or BegDate <= @EndDate)                                
                                            {0}
                                        ) X ", arrCity.HasValue ? "AND @ArrCity in (Select City From Location (NOLOCK) Where (RecID=(Select Location From Hotel (NOLOCK) Where Code=StopSale.Hotel)) OR RecID=StopSale.Location )" : "");
            if (Hotel != "")
                tsql += " Where HotelCode = '" + Hotel + "' ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "@BegDate", DbType.DateTime, begDate);
                db.AddInParameter(dbCommand, "@EndDate", DbType.DateTime, endDate);
                db.AddInParameter(dbCommand, "@Market", DbType.String, UserData.Market);
                if (arrCity.HasValue)
                    db.AddInParameter(dbCommand, "@ArrCity", DbType.Int32, arrCity.Value);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        StopSaleResultRecord rec = new StopSaleResultRecord();
                        rec.Accom = Conversion.getStrOrNull(oReader["Accom"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(oReader["BegDate"]);
                        rec.EndDate = Conversion.getDateTimeOrNull(oReader["EndDate"]);
                        rec.Board = Conversion.getStrOrNull(oReader["Board"]);
                        rec.AllotType = Conversion.getByteOrNull(oReader["AllotType"]);
                        rec.AppType = Conversion.getStrOrNull(oReader["AppType"]);
                        rec.Category = Conversion.getStrOrNull(oReader["Category"]);
                        rec.City = Conversion.getStrOrNull(oReader["City"]);
                        rec.Hotel = Conversion.getStrOrNull(oReader["Hotel"]);
                        rec.HotelCode = Conversion.getStrOrNull(oReader["HotelCode"]);
                        rec.Location = Conversion.getInt32OrNull(oReader["Location"]);
                        rec.PriceList = Conversion.getStrOrNull(oReader["PriceList"]);
                        rec.Room = Conversion.getStrOrNull(oReader["Room"]);
                        list.Add(rec);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                error = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<StopSaleFlightSeatOpt> getStopSaleFlightSeatWithOptResult(User UserData, DateTime? begDate, DateTime? endDate, int? depCity, int? arrCity, string fClass, ref string error)
        {
            List<StopSaleFlightSeatOpt> list = new List<StopSaleFlightSeatOpt>();
            string tsql = @"Declare @Date1 DateTime, @Date2 DateTime, @FlgClass varchar(2), @DepLocation int, @ArrLocation int
                            Set @Date1 = @BegDate
                            Set @Date2 = @EndDate
                            Set @FlgClass = @FClass
                            Set @DepLocation = @DepCity
                            Set @ArrLocation = @ArrCity
                            if OBJECT_ID('TempDB.dbo.#OverSeats') is not null Drop Table dbo.#OverSeats
                            Select * Into #OverSeats From 
                            (
                            Select S.BegDate, M.Days, Count(Unit) OverSeat
                            from ResService S
                            join ResCust P (NOLOCK) on P.ResNo = S.ResNo and P.CustNo in (Select CustNo From ResCon where ResNo = S.ResNo and ServiceID = S.RecID)
                            join ResCon C (NOLOCK) on C.ResNo = S.ResNo and C.CustNo = P.CustNo and C.ServiceID = S.RecID
                            join ResMain M on S.ResNo = M.ResNo
                            where M.ResStat in (0,1) and M.ConfStat <> 2 and M.PLOperator = @Operator and
                                  S.ServiceType = 'FLIGHT' and S.FlgClass = @FlgClass and
                                  S.StatSer in (0,1) and S.StatConf <> 2 and
                                  S.DepLocation = M.DepCity and S.ArrLocation = M.ArrCity and 
                                  S.BegDate between @Date1 and @Date2 and S.DepLocation = @DepLocation and S.ArrLocation = @ArrLocation and
                                  S.AllotType = 'O' and exists(	Select * from ResService SS 
	                        join ResCust PP (NOLOCK) on PP.ResNo = SS.ResNo and PP.CustNo in (Select CustNo From ResCon where ResNo = SS.ResNo and ServiceID = SS.RecID)
	                        join ResCon CC (NOLOCK) on CC.ResNo = S.ResNo and CC.CustNo = PP.CustNo and CC.ServiceID = SS.RecID and CC.CustNo = C.CustNo
	                        where SS.ResNo = S.ResNo and 
		                        SS.ServiceType = 'FLIGHT' and SS.StatSer in (0,1) and SS.StatConf <> 2 and
                                SS.FlgClass = S.FlgClass and SS.DepLocation = S.ArrLocation and SS.ArrLocation = S.DepLocation)
	                        Group By S.BegDate, M.Days
	                        union All
	                        Select S.BegDate, 0 Days, Count(Unit) OverSeat from ResService S
	                        join ResCust P (NOLOCK) on P.ResNo = S.ResNo and P.CustNo in (Select CustNo From ResCon where ResNo = S.ResNo and ServiceID = S.RecID)
	                        join ResCon C (NOLOCK) on C.ResNo = S.ResNo and C.CustNo = P.CustNo and C.ServiceID = S.RecID
	                        join ResMain M on S.ResNo = M.ResNo
	                        where M.ResStat in (0,1) and M.ConfStat <> 2 and M.PLOperator = @Operator and
		                        S.ServiceType = 'FLIGHT' and S.FlgClass = @FlgClass and S.StatSer in (0,1) and S.StatConf <> 2 and
		                        S.DepLocation = M.DepCity and S.ArrLocation = M.ArrCity and S.BegDate between @Date1 and @Date2 and 
		                        S.DepLocation = @DepLocation and S.ArrLocation = @ArrLocation and 
		                        S.AllotType = 'O' and not exists(Select * from ResService SS    
										                         join ResCust PP (NOLOCK) on PP.ResNo = SS.ResNo and PP.CustNo in (Select CustNo From ResCon where ResNo = SS.ResNo and ServiceID = SS.RecID)
										                         join ResCon CC (NOLOCK) on CC.ResNo = SS.ResNo and CC.CustNo = PP.CustNo and CC.ServiceID = SS.RecID and CC.CustNo = C.CustNo
										                         where SS.ResNo = S.ResNo and SS.ServiceType = 'FLIGHT' and SS.StatSer in (0,1) and SS.StatConf <> 2 and
											                        SS.FlgClass = S.FlgClass and SS.DepLocation = S.ArrLocation and SS.ArrLocation = S.DepLocation)
	                        Group By S.BegDate, M.Days) X

                            Select FlyDate=AllotDate, Night=isnull(M.Night,0), Seat=isnull(FlightSeat,0) - isnull(UsedFlightSeat,0) + isnull(OS.OverSeat,0)
                            From FlightOpt M (NOLOCK) 
                            Left Join #OverSeats OS on OS.BegDate = M.AllotDate And OS.Days = M.Night
                            Where 
                                Operator = @Operator
                            And AllotDate between @BegDate And @EndDate
                            And DepCity = @DepCity 
                            And ArrCity = @ArrCity
                            And Class = @FClass
                            And Night > 0
                            Order By AllotDate, Night";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "@DepCity", DbType.Int32, depCity);
                db.AddInParameter(dbCommand, "@ArrCity", DbType.Int32, arrCity);
                db.AddInParameter(dbCommand, "@BegDate", DbType.DateTime, begDate);
                db.AddInParameter(dbCommand, "@EndDate", DbType.DateTime, endDate);
                db.AddInParameter(dbCommand, "@FClass", DbType.String, fClass);
                db.AddInParameter(dbCommand, "@Operator", DbType.String, UserData.Operator);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        StopSaleFlightSeatOpt rec = new StopSaleFlightSeatOpt();
                        rec.FlyDate = (DateTime)oReader["FlyDate"];
                        rec.Night = Conversion.getInt32OrNull(oReader["Night"]);
                        rec.Seat = Conversion.getInt32OrNull(oReader["Seat"]);
                        list.Add(rec);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                error = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
            }
        }

        public List<StopSaleCityRecord> GetArrCityByDep(User UserData, int? DepCity, ref string errorMsg)
        {
            List<StopSaleCityRecord> list = new List<StopSaleCityRecord>();
            string tsql = @"Select RecID=ArrCity, Name=dbo.GetLocationNameL(ArrCity, @Market)
                            From (
                                    Select Distinct ArrCity From PriceListV PL
                                    Join (
                                            Select  Distinct HotelCode = Hotel
                                            From StopSale (NOLOCK)
                                            Where (Market = @Market Or isnull(Market, '') = '')
                                                And	Status = 'Y'
                                                And ForB2B=1
                                                And EndDate >= GetDate() 
                                        ) P ON PL.Hotel = P.HotelCode
                                    Where	PL.Ready = 'Y'
                                        And PL.CWebPub = 'Y'
                                        And PL.HotStatus = 1
                                        And PL.WebPubDate <= GetDate()
                                        And (@DepCity is null or PL.DepCity = @DepCity)
                                        And dbo.DateOnly(GetDate()) between PL.SaleBegDate And PL.SaleEndDate 
                                        And PL.Category is null
                                 ) X";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "@Market", System.Data.DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "@DepCity", System.Data.DbType.Int32, DepCity);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        StopSaleCityRecord rec = new StopSaleCityRecord();
                        rec.RecID = (int)Conversion.getInt32OrNull(oReader["RecID"]);
                        rec.Name = Conversion.getStrOrNull(oReader["Name"]);
                        list.Add(rec);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
            }

        }

        public List<CodeName> GetFlightClass(User UserData, ref string errorMsg)
        {
            List<CodeName> list = new List<CodeName>();
            string tsql = @"Select Code=Class, Name=isnull(dbo.FindLocalName(NameLID, @Market), Name) 
                            From FlightClass (NOLOCK)
                            Where B2B = 1";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "@Market", System.Data.DbType.String, UserData.Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        CodeName rec = new CodeName();
                        rec.Code = Conversion.getStrOrNull(oReader["Code"]);
                        rec.Name = Conversion.getStrOrNull(oReader["Name"]);
                        list.Add(rec);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
            }

        }

    }
}
