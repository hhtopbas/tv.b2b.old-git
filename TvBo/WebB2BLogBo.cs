﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class WEBSearchLog
    {        
        public Guid RECID { get; set; }
        public string SESSIONID { get; set; }
        public string USERID { get; set; }
        public string AGENCYID { get; set; }
        public DateTime CREATEDATE { get; set; }
        public Int16 SALERESOURCE { get; set; }
        public DateTime CHECKIN { get; set; }
        public Int16? EXPENDDATE { get; set; }
        public Int16? NIGHTFROM { get; set; }
        public Int16? NIGHTTO { get; set; }
        public int? DEPCITY { get; set; }
        public int? ARRCITY { get; set; }
        public decimal? PRICEFROM { get; set; }
        public decimal? PRICETO { get; set; }
        public string PACKAGE { get; set; }
        public bool? ONLYTICKETOW { get; set; }
        public string PACKTYPE { get; set; }

        public WEBSearchLog()
        {
        }
    }

    public class WEBRoomInfoLog
    {
        public Guid RECID { get; set; }
        public Guid PARENTID { get; set; }
        public Int16 ADULT { get; set; }
        public Int16 CHILD { get; set; }

        public WEBRoomInfoLog()
        {
            this.ADULT = 0;
            this.CHILD = 0;
        }
    }

    public class WEBLocationLog
    {
        public Guid RECID { get; set; }
        public Guid PARENTID { get; set; }
        public int LOCATION { get; set; }

        public WEBLocationLog()
        {
            this.LOCATION = 0;
        }
    }

    public class WEBHotelLog
    {
        public Guid RECID { get; set; }
        public Guid PARENTID { get; set; }
        public string HOTEL { get; set; }

        public WEBHotelLog()
        {
        }
    }

    public class WEBRoomLog
    {
        public Guid RECID { get; set; }
        public Guid PARENTID { get; set; }
        public string ROOM { get; set; }

        public WEBRoomLog()
        {
        }
    }

    public class WEBBoardLog
    {
        public Guid RECID { get; set; }
        public Guid PARENTID { get; set; }
        public string BOARD { get; set; }

        public WEBBoardLog()
        {
        }
    }

    public class WEBCategoryLog
    {
        public Guid RECID { get; set; }
        public Guid PARENTID { get; set; }
        public string CATEGORY { get; set; }

        public WEBCategoryLog()
        {
        }
    }

    public class WEBBookLog
    {
        public Guid RECID { get; set; }
        public Guid PARENTID { get; set; }
        public DateTime BEGINBOOK { get; set; }
        public DateTime ENDBOOK { get; set; }

        public WEBBookLog()
        {
        }
    }
}
