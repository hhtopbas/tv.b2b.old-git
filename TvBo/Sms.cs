﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using TvTools;

namespace TvBo
{
    public class Sms
    {
        private List<SmsBo> getSmsTextList(string pMarket)
        {
            List<SmsBo> records = new List<SmsBo>();
            string tsql =
@"
Select Market,Type,FixTxt,ValB2B,ValB2C,ValTV From SMSText (NOLOCK) where Market=@Market      
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, pMarket);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        SmsBo record = new SmsBo();
                        if (Enum.IsDefined(typeof(SmsBo.SmsType),Convert.ToInt32(R["Type"])))
                        {
                            record.Type = (SmsBo.SmsType)Convert.ToInt32(R["Type"]);
                            record.Text = Conversion.getStrOrNull(R["FixTxt"]);
                            record.ValB2B = Conversion.getBoolOrNull(R["ValB2B"]);
                            records.Add(record);
                        }
                    }
                    return records;
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        private string GetSmsText(string pMarket,SmsBo.SmsType pSmsType)
        {
            List<SmsBo> smsTexts = getSmsTextList(pMarket);
            if (smsTexts != null && smsTexts.Any())
                return smsTexts.Where(w => w.Type == pSmsType).FirstOrDefault().Text;
            else
                return null;
        }

        private string SmsTextWithParams(string pMarket, ResDataRecord ResData,SmsBo.SmsType pSmsType,ref string errorMsg)
        {
            try
            {
                string smsText = GetSmsText(pMarket, pSmsType);
                switch (pSmsType)
                {
                    case SmsBo.SmsType.NewReservation:
                        smsText = smsText.Replace("%01", ResData.ResMain.ResNo);
                        smsText = smsText.Replace("%02", ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Name + "," + ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Surname);
                        smsText = smsText.Replace("%03", ResData.ResMain.DepCityNameL);
                        smsText = smsText.Replace("%07", ResData.ResMain.ArrCityNameL);
                        break;
                    case SmsBo.SmsType.PaymentReceived:
                        smsText = smsText.Replace("%01", ResData.ResMain.ResNo);
                        smsText = smsText.Replace("%02",string.Format("{0} {1}", ResData.ResMain.PasAmount.Value.ToString(),ResData.ResMain.SaleCur));
                        smsText = smsText.Replace("%04", ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Name + "," + ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Surname);
                        smsText = smsText.Replace("%4", ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Name + "," + ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Surname);
                        break;
                }
                return smsText;
            }
            catch(Exception ex)
            {
                errorMsg = ex.Message;
            }
            return null;
        }

        public bool AddSmsQueue(string pMarket,ResDataRecord ResData, SmsBo.SmsType pSmsType, string pUser, string pTo,ref string errorMsg)
        {
            string smsText = SmsTextWithParams(pMarket, ResData, pSmsType,ref errorMsg);

            string tsql =
@"
exec dbo.usp_SendSMS @From,@To,@Msg,@CrtUser
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "From", DbType.AnsiString, DBNull.Value);
                db.AddInParameter(dbCommand, "To", DbType.AnsiString, pTo);
                db.AddInParameter(dbCommand, "Msg", DbType.AnsiString, smsText);
                db.AddInParameter(dbCommand, "CrtUser", DbType.AnsiString, pUser);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
