﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class HandFeeRecord
    {
        public HandFeeRecord()
        {         
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        Int16 _ConfStat;
        public Int16 ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool _PayCom;
        public bool PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        bool _PayComEB;
        public bool PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

    }

    public class HandFeePriceRecord
    {
        public HandFeePriceRecord()
        { 
        }

        string _HandFee;
        public string HandFee
        {
            get { return _HandFee; }
            set { _HandFee = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        int _Country;
        public int Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; }
        }

        string _CountryLocalName;
        public string CountryLocalName
        {
            get { return _CountryLocalName; }
            set { _CountryLocalName = value; }
        }

        int _Location;
        public int Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _LocationLocalName;
        public string LocationLocalName
        {
            get { return _LocationLocalName; }
            set { _LocationLocalName = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _HotelName;
        public string HotelName
        {
            get { return _HotelName; }
            set { _HotelName = value; }
        }

        string _HotelLocalName;
        public string HotelLocalName
        {
            get { return _HotelLocalName; }
            set { _HotelLocalName = value; }
        }

        string _HotelGrp;
        public string HotelGrp
        {
            get { return _HotelGrp; }
            set { _HotelGrp = value; }
        }

        string _HotelCat;
        public string HotelCat
        {
            get { return _HotelCat; }
            set { _HotelCat = value; }
        }

        DateTime _BegDate;
        public DateTime BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime _EndDate;
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _CalcType;
        public Int16? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }        

        decimal? _ChdG1MaxAge;
        public decimal? ChdG1MaxAge
        {
            get { return _ChdG1MaxAge; }
            set { _ChdG1MaxAge = value; }
        }

        decimal? _ChdG2MaxAge;
        public decimal? ChdG2MaxAge
        {
            get { return _ChdG2MaxAge; }
            set { _ChdG2MaxAge = value; }
        }

        decimal? _ChdG3MaxAge;
        public decimal? ChdG3MaxAge
        {
            get { return _ChdG3MaxAge; }
            set { _ChdG3MaxAge = value; }
        }

    }
}
