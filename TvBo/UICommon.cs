﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.Security;
using TvTools;


namespace TvBo
{
    public static class WebRoot
    {
        //        private static string basePageRoot;
        public static string BasePageRoot
        {
            get { return getBasePageRoot(); }
        }

        //        private static string basePage;
        public static string BasePage
        {
            get { return getBasePage(); }
        }

        public static string getBasePageRoot()
        {
            string http = "http://";
            if (HttpContext.Current != null)
            {
                http = HttpContext.Current.Request.IsSecureConnection ? "https://" : "http://";
                http = http + HttpContext.Current.Request.Url.Host;
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.ApplicationPath))
                    http = http + HttpContext.Current.Request.ApplicationPath;
            }
            return http + "/";
            //return Conversion.getStrOrNull(HttpContext.Current.Session["BasePageRoot"]);//absolutePath;
        }

        public static string getBasePage()
        {
            return Conversion.getStrOrNull(HttpContext.Current.Session["BasePage"]);//absolutePath + "Default.aspx";
        }
    }

    public class UICommon
    {
        private static List<string> iFramePage;

        public static List<string> IFramePage
        {
            get { return UICommon.iFramePage; }
            set { UICommon.iFramePage = value; }
        }

        public UICommon()
        {
            iFramePage = new List<string>();
            iFramePage.Add("Announce.aspx");
            iFramePage.Add("ChangePassword.aspx");
            iFramePage.Add("NewAnnounce.aspx");
            iFramePage.Add("CustomerInfo.aspx");
            iFramePage.Add("Promotion.aspx");
            iFramePage.Add("MakeReservation.aspx");
            iFramePage.Add("CustomerDetail.aspx");
            iFramePage.Add("CustomerAddress.aspx");
            iFramePage.Add("BookTicket.aspx");
            iFramePage.Add("AddRoomAndAccom.aspx");
            iFramePage.Add("CancelReservation.aspx");
            iFramePage.Add("ChangeResDate.aspx");
            iFramePage.Add("ExtraService_Add.aspx");
            iFramePage.Add("ReservationAddTourist.aspx");
            iFramePage.Add("RSAdd_Excursion.aspx");
            iFramePage.Add("RSAdd_Flight.aspx");
            iFramePage.Add("RSAdd_Handfee.aspx");
            iFramePage.Add("RSAdd_Hotel.aspx");
            iFramePage.Add("RSAdd_Insurance.aspx");
            iFramePage.Add("RSAdd_Other.aspx");
            iFramePage.Add("RSAdd_Renting.aspx");
            iFramePage.Add("RSAdd_Transfer.aspx");
            iFramePage.Add("RSAdd_Transport.aspx");
            iFramePage.Add("RSAdd_Visa.aspx");
            iFramePage.Add("RSEdit_Excursion.aspx");
            iFramePage.Add("RSEdit_Flight.aspx");
            iFramePage.Add("RSEdit_Handfee.aspx");
            iFramePage.Add("RSEdit_Hotel.aspx");
            iFramePage.Add("RSEdit_Insurance.aspx");
            iFramePage.Add("RSEdit_Other.aspx");
            iFramePage.Add("RSEdit_Renting.aspx");
            iFramePage.Add("RSEdit_Transfer.aspx");
            iFramePage.Add("RSEdit_Transport.aspx");
            iFramePage.Add("RSEdit_Visa.aspx");
            iFramePage.Add("RSView_Excursion.aspx");
            iFramePage.Add("RSView_Flight.aspx");
            iFramePage.Add("RSView_Handfee.aspx");
            iFramePage.Add("RSView_Hotel.aspx");
            iFramePage.Add("RSView_Insurance.aspx");
            iFramePage.Add("RSView_Other.aspx");
            iFramePage.Add("RSView_Renting.aspx");
            iFramePage.Add("RSView_Transfer.aspx");
            iFramePage.Add("RSView_Transport.aspx");
            iFramePage.Add("RSView_Visa.aspx");
            iFramePage.Add("ServiceChangeRoomAndAccom.aspx");
            iFramePage.Add("Ssrc.aspx");
            #region Novaturas Document
            iFramePage.Add("AgreementNext.aspx");
            iFramePage.Add("BusContrat.aspx");
            iFramePage.Add("ClientProforma.aspx");
            iFramePage.Add("ClientProformaEn.aspx");
            iFramePage.Add("Contrat.aspx");
            iFramePage.Add("FlyTicket.aspx");
            iFramePage.Add("Proforma.aspx");
            iFramePage.Add("ProformaEn.aspx");
            iFramePage.Add("Voucher.aspx");
            #endregion Novaturas Document

        }

        public string getWebID()
        {
            string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\Setting.xml";
            System.Data.DataTable webID = new System.Data.DataTable();
            string retVal = string.Empty;

            try {
                webID.ReadXml(FilePath);
                retVal = webID.Rows[0]["CONFFOLDER"].ToString();
            }
            catch { }

            return retVal;
        }

        public byte[] getPictures(string Code, string Tables, string Fields, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql = "Select Pic=" + Fields + " From " + Tables + " (NOLOCK) Where Code = '" + Code + "' ";

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                return (byte[])db.ExecuteScalar(dbCommand);
            }
            catch (Exception ex) {
                errorMsg = ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static string EncodeJsString(string s)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("'");
            foreach (char c in s) {
                switch (c) {
                    case '\"': sb.Append("\\\""); break;
                    case '\'': sb.Append("\\'"); break;
                    case '\\': sb.Append("\\\\"); break;
                    case '\b': sb.Append("\\b"); break;
                    case '\f': sb.Append("\\f"); break;
                    case '\n': sb.Append("\\n"); break;
                    case '\r': sb.Append("\\r"); break;
                    case '\t': sb.Append("\\t"); break;
                    default:
                        int i = (int)c;
                        if (i < 32) {
                            sb.AppendFormat("\\u{0:X04}", i);
                        } else {
                            sb.Append(c);
                        }
                        break;
                }
            }
            sb.Append("\'");
            return sb.ToString();
        }

        public List<WebConfig> getWebConfig()
        {
            List<WebConfig> webConfig = new List<WebConfig>();

            string xmlPath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + getWebID() + "\\WebConfig.xml";
            System.Data.DataTable webConfigData = new System.Data.DataTable();
            webConfigData.ReadXml(xmlPath);

            foreach (DataRow row in webConfigData.Rows) {
                WebConfig webConfigRecord = new WebConfig();

                webConfigRecord.FORMNAME = Conversion.getStrOrNull(row["FORMNAME"]);
                webConfigRecord.PARAMETERNAME = Conversion.getStrOrNull(row["PARAMETERNAME"]);
                webConfigRecord.TYPES = Conversion.getStrOrNull(row["TYPES"]);
                webConfigRecord.FIELDTYPE = Conversion.getStrOrNull(row["FIELDTYPE"]);
                webConfigRecord.FIELDVALUE = Conversion.getStrOrNull(row["FIELDVALUE"]);

                webConfig.Add(webConfigRecord);
            }

            return webConfig;
        }

        public List<MainMenu> getMainMenu()
        {
            List<MainMenu> mainMenu = new List<MainMenu>();

            string xmlPath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + getWebID() + "\\MainMenu.xml";
            System.Data.DataTable mnData = new System.Data.DataTable();
            mnData.ReadXml(xmlPath);
            int idNo = 0;
            foreach (DataRow row in mnData.Rows) {
                MainMenu menuRecord = new MainMenu();
                menuRecord.IDNo = idNo;
                menuRecord.SubMenu = (bool)row["SubMenu"];
                menuRecord.MenuItem = Conversion.getStrOrNull(row["MenuItem"]);
                menuRecord.SubMenuItem = Conversion.getStrOrNull(row["SubMenuItem"]);
                menuRecord.ResourceName = Conversion.getStrOrNull(row["ResourceName"]);
                menuRecord.Disable = (bool)row["Disable"];
                menuRecord.Url = Conversion.getStrOrNull(row["Url"]);
                menuRecord.ResNo = (bool)row["ResNo"];
                menuRecord.NoShow = Conversion.getStrOrNull(row["noShow"]);
                menuRecord.Selected = false;
                if (mnData.Columns.Contains("ShowAllUser"))
                    menuRecord.ShowAllUser = (bool)row["ShowAllUser"];
                else menuRecord.ShowAllUser = true;
                if (mnData.Columns.Contains("OnlyMainAgency"))
                    menuRecord.OnlyMainAgency = (bool)row["OnlyMainAgency"];
                if (mnData.Columns.Contains("HolPackCategory"))
                    menuRecord.HolPackCategory = Conversion.getInt32OrNull(row["HolPackCategory"]);
                if (mnData.Columns.Contains("PackageType"))
                    menuRecord.PackageType = Conversion.getStrOrNull(row["PackageType"]);
                if (mnData.Columns.Contains("B2BMenuCat"))
                    menuRecord.B2BMenuCat = Conversion.getStrOrNull(row["B2BMenuCat"]);
                if (mnData.Columns.Contains("PageExt"))
                    menuRecord.PageExt = Conversion.getStrOrNull(row["PageExt"]);
                else menuRecord.PageExt = string.Empty;
                if (mnData.Columns.Contains("QueryStr"))
                    menuRecord.QueryStr = Conversion.getStrOrNull(row["QueryStr"]);
                else menuRecord.QueryStr = string.Empty;
                if (mnData.Columns.Contains("External"))
                    menuRecord.External = (bool)row["External"];
                if (mnData.Columns.Contains("NewWindow"))
                    menuRecord.NewWindow = (bool)row["NewWindow"];
                if (mnData.Columns.Contains("Style"))
                    menuRecord.Style = Conversion.getStrOrNull(row["Style"]);
                if (mnData.Columns.Contains("AuthorityID"))
                    menuRecord.AuthorityID = Conversion.getInt16OrNull(row["AuthorityID"]);
                if (mnData.Columns.Contains("ServiceType"))
                    menuRecord.ServiceType = Conversion.getStrOrNull(row["ServiceType"]);
                if (mnData.Columns.Contains("ShowMarket"))
                    menuRecord.ShowMarket = Conversion.getStrOrNull(row["ShowMarket"]);
                mainMenu.Add(menuRecord);
                ++idNo;
            }
            return mainMenu;
        }

        public List<ResViewMenu> getResViewMenu()
        {
            List<ResViewMenu> menu = new List<ResViewMenu>();

            string xmlPath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + getWebID() + "\\ResViewMenu.xml";
            System.Data.DataTable mnData = new System.Data.DataTable();
            mnData.ReadXml(xmlPath);

            foreach (DataRow row in mnData.Rows) {
                ResViewMenu menuRecord = new ResViewMenu();

                menuRecord.Code = Conversion.getStrOrNull(row["Code"]);
                menuRecord.ResourceName = Conversion.getStrOrNull(row["ResourceName"]);
                menuRecord.Disable = Conversion.getBoolOrNull(row["Disable"]).HasValue ? Conversion.getBoolOrNull(row["Disable"]).Value : true;
                menuRecord.Url = Conversion.getStrOrNull(row["Url"]);
                menuRecord.Type = Conversion.getStrOrNull(row["Type"]);
                menuRecord.Service = Conversion.getStrOrNull(row["Service"]);
                menuRecord.SubFolder = Conversion.getBoolOrNull(row["SubFolder"]).HasValue ? Conversion.getBoolOrNull(row["SubFolder"]).Value : true;
                menuRecord.PaymentControl = Conversion.getBoolOrNull(row["PaymentControl"]).HasValue ? Conversion.getBoolOrNull(row["PaymentControl"]).Value : false;
                menuRecord.NoShowMarket = Conversion.getStrOrNull(row["noShowMarket"]);
                if (mnData.Columns.Contains("ResBusyControl"))
                    menuRecord.ResBusyControl = Conversion.getBoolOrNull(row["ResBusyControl"]).HasValue ? Conversion.getBoolOrNull(row["ResBusyControl"]).Value : false;
                if (mnData.Columns.Contains("ResStatusControl"))
                    menuRecord.ResStatusControl = Conversion.getBoolOrNull(row["ResStatusControl"]).HasValue ? Conversion.getBoolOrNull(row["ResStatusControl"]).Value : true;
                if (mnData.Columns.Contains("afterCheckout"))
                    menuRecord.afterCheckout = Conversion.getBoolOrNull(row["afterCheckout"]).HasValue ? Conversion.getBoolOrNull(row["afterCheckout"]).Value : false;
                if (mnData.Columns.Contains("MultiLang"))
                    menuRecord.MultiLang = Conversion.getBoolOrNull(row["MultiLang"]).HasValue ? Conversion.getBoolOrNull(row["MultiLang"]).Value : false;
                if (mnData.Columns.Contains("ShowType"))
                    menuRecord.ShowType = Conversion.getInt16OrNull(row["ShowType"]);
                if (mnData.Columns.Contains("External"))
                    menuRecord.External = Conversion.getBoolOrNull(row["External"]).HasValue ? Conversion.getBoolOrNull(row["External"]).Value : false;
                if (mnData.Columns.Contains("FormID"))
                    menuRecord.FormID = Conversion.getInt32OrNull(row["FormID"]);
                if (mnData.Columns.Contains("MarketFormID"))
                    menuRecord.MarketFormID = Conversion.getStrOrNull(row["MarketFormID"]);
                menu.Add(menuRecord);
            }
            return menu;
        }

        public List<ExternalServiceMenu> getExternalServiceMenu()
        {
            List<ExternalServiceMenu> menu = new List<ExternalServiceMenu>();

            string xmlPath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + getWebID() + "\\ExternalServiceMenu.xml";
            if (File.Exists(xmlPath)) {
                System.Data.DataTable mnData = new System.Data.DataTable();
                mnData.ReadXml(xmlPath);
                foreach (DataRow row in mnData.Rows) {
                    ExternalServiceMenu menuRecord = new ExternalServiceMenu();
                    menuRecord.Code = Conversion.getStrOrNull(row["Code"]);
                    menuRecord.ResourceName = Conversion.getStrOrNull(row["ResourceName"]);
                    menuRecord.Disable = Conversion.getBoolOrNull(row["Disable"]).HasValue ? Conversion.getBoolOrNull(row["Disable"]).Value : true;
                    menuRecord.Url = Conversion.getStrOrNull(row["Url"]);
                    menuRecord.Params = Conversion.getStrOrNull(row["Params"]);
                    menuRecord.ExternalWindow = Conversion.getBoolOrNull(row["ExternalWindow"]).HasValue ? Conversion.getBoolOrNull(row["ExternalWindow"]).Value : false;
                    menu.Add(menuRecord);
                }
            }
            return menu;
        }

        public List<MakeResMenu> getMakeResMenu()
        {
            List<MakeResMenu> menu = new List<MakeResMenu>();

            string xmlPath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + getWebID() + "\\MakeResMenu.xml";
            System.Data.DataTable mnData = new System.Data.DataTable();
            mnData.ReadXml(xmlPath);

            foreach (DataRow row in mnData.Rows) {
                MakeResMenu menuRecord = new MakeResMenu();

                menuRecord.Code = (string)row["Code"];
                menuRecord.Disable = (bool)row["Disable"];

                menu.Add(menuRecord);
            }

            return menu;
        }

        public HeaderData getHeaderData(User UserData, ref string errorMsg)
        {
            HeaderData hd = new HeaderData();
            string tsql = string.Empty;
            if (UserData.MainAgency)
                tsql = @"Select UserName='ADMIN',UserNameL='ADMIN',                             
                           AgencyName=A.Name,
                           AgencyNameL=isnull(dbo.FindLocalName(A.NameLID,O.Market),A.Name),
                           MainAgency=(Select [Name] From Agency (NOLOCK) Where Code=A.MainOffice),
                           MainAgencyL=(Select isnull(dbo.FindLocalName(NameLID,O.Market),[Name]) From Agency (NOLOCK) Where Code=A.MainOffice),
                           Office=O.Name,
	                       OfficeL=isnull(dbo.FindLocalName(O.NameLID,O.Market),O.Name),    
                           Operator=Op.Name,
                           OperatorL=isnull(dbo.FindLocalName(Op.NameLID,O.Market),Op.Name),
                           Market=M.Name,
                           MarketL=isnull(dbo.FindLocalName(M.NameLID,O.Market),M.Name),
                           OprAddress=O.Address+' '+O.AddrCity+' '+O.AddrCountry
                         From Agency A (NOLOCK) 
                           Join Office O (NOLOCK) on O.Code=A.OprOffice
                           Join Operator Op (NOLOCK) on OP.Code=O.Operator
                           Join Market M (NOLOCK) on M.Code=O.Market
                         Where A.Code=@Agency
                        ";
            else
                tsql = @"Select UserName=U.Name,
                           UserNameL=isnull(dbo.FindLocalName(U.NameLID,O.Market),U.Name),
                           AgencyName=A.Name,
                           AgencyNameL=isnull(dbo.FindLocalName(A.NameLID,O.Market),A.Name),
                           MainAgency=(Select [Name] From Agency (NOLOCK) Where Code=A.MainOffice),
                           MainAgencyL=(Select isnull(dbo.FindLocalName(NameLID,O.Market),[Name]) From Agency (NOLOCK) Where Code=A.MainOffice),
                           Office=O.Name,
	                       OfficeL=isnull(dbo.FindLocalName(O.NameLID,O.Market),O.Name),    
                           Operator=Op.Name,
                           OperatorL=isnull(dbo.FindLocalName(Op.NameLID,O.Market),Op.Name),
                           Market=M.Name,
                           MarketL=isnull(dbo.FindLocalName(M.NameLID,O.Market),M.Name),
                           OprAddress=O.Address+' '+O.AddrCity+' '+O.AddrCountry
                         From AgencyUser U (NOLOCK)
	                       Join Agency A (NOLOCK) on A.Code=U.Agency
	                       Join Office O (NOLOCK) on O.Code=A.OprOffice
                           Join Operator Op (NOLOCK) on OP.Code=O.Operator
                           Join Market M (NOLOCK) on M.Code=O.Market
                         Where A.Code=@Agency And U.Code=@User
                        ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "User", DbType.String, UserData.UserID);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    while (oReader.Read()) {
                        hd.AgencyName = Conversion.getStrOrNull(oReader["AgencyName"]);
                        hd.AgencyNameL = Conversion.getStrOrNull(oReader["AgencyNameL"]);
                        hd.MainAgency = Conversion.getStrOrNull(oReader["MainAgency"]);
                        hd.MainAgencyL = Conversion.getStrOrNull(oReader["MainAgencyL"]);
                        hd.Market = Conversion.getStrOrNull(oReader["Market"]);
                        hd.MarketL = Conversion.getStrOrNull(oReader["MarketL"]);
                        hd.Office = Conversion.getStrOrNull(oReader["Office"]);
                        hd.OfficeL = Conversion.getStrOrNull(oReader["OfficeL"]);
                        hd.Operator = Conversion.getStrOrNull(oReader["Operator"]);
                        hd.OperatorL = Conversion.getStrOrNull(oReader["OperatorL"]);
                        hd.UserName = Conversion.getStrOrNull(oReader["UserName"]);
                        hd.UserNameL = Conversion.getStrOrNull(oReader["UserNameL"]);
                        hd.OprAddress = Conversion.getStrOrNull(oReader["OprAddress"]);
                    }
                }
                return hd;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return hd;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OperatorLogo> getOperatorLogo()
        {
            List<OperatorLogo> oLogo = new List<OperatorLogo>();
            string tsql = string.Empty;
            tsql = @"Select Code,Logo From Operator (NOLOCK) ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    while (oReader.Read()) {
                        OperatorLogo record = new OperatorLogo();
                        record.Operator = Conversion.getStrOrNull(oReader["Code"]);
                        object logo = oReader["Logo"];
                        record.OperatorLogoPath = WriteBinaryImage(logo, record.Operator, "", "", "");
                        oLogo.Add(record);
                    }
                }
                return oLogo;
            }
            catch {
                return oLogo;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OperatorLogo> getAgencyLogo()
        {
            List<OperatorLogo> aLogo = new List<OperatorLogo>();
            string tsql = string.Empty;
            tsql = @"Select Code,LogoSmall,LogoUrl From Agency (NOLOCK) ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    while (oReader.Read()) {
                        OperatorLogo record = new OperatorLogo();
                        record.Operator = Conversion.getStrOrNull(oReader["Code"]);
                        object logo = oReader["LogoSmall"];
                        if (logo != null) {
                            record.OperatorLogoPath = WriteBinaryImage(logo, record.Operator, "AG_", "", "");
                            aLogo.Add(record);
                        }
                    }
                }
                return aLogo;
            }
            catch {
                return aLogo;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            try {
                if (byteArrayIn.Length > 0) {
                    using (MemoryStream ms = new MemoryStream(byteArrayIn)) {
                        using (Bitmap Origninal = new Bitmap(ms)) {
                            Bitmap returnBmp = new Bitmap(Origninal.Width, Origninal.Height);
                            Graphics g = Graphics.FromImage(returnBmp);
                            g.DrawImage(Origninal, 0, 0, Origninal.Width, Origninal.Height);

                            g.Dispose();
                            ms.Close();
                            return (System.Drawing.Image)returnBmp;
                        }
                    }
                }
                return null;
            }
            catch {
                return null;
            }
        }

        public static string WriteBinaryImage(object _image, string Operator, string firstExt, string H, string W)
        {
            string contentType = HttpContext.Current.Response.ContentType;
            if (_image != null && _image != Convert.DBNull) {
                byte[] image = (byte[])_image;
                HttpContext.Current.Response.ContentType = "image/jpeg";
                using (MemoryStream ms = new MemoryStream(image)) {
                    Bitmap bmp;
                    bmp = (Bitmap)Bitmap.FromStream(ms);
                    try {
                        if (H != "" && W != "") {
                            bmp = ImageResize.FixedSize(bmp, Convert.ToInt32(H), Convert.ToInt32(H));
                        } else
                            if (W == "" && H != "") {
                            bmp = ImageResize.ConstrainProportions(bmp, Convert.ToInt32(H), Dimensions.Height);
                        } else
                                if (H == "" && W != "") {
                            bmp = ImageResize.ConstrainProportions(bmp, Convert.ToInt32(W), Dimensions.Width);
                        }

                    }
                    catch {
                        HttpContext.Current.Response.ContentType = contentType;
                    }

                    try {
                        if (!File.Exists(HttpContext.Current.Server.MapPath("LogoCache") + "\\" + firstExt + Operator + ".jpg")) {
                            bmp.Save(AppDomain.CurrentDomain.BaseDirectory + "LogoCache\\" + firstExt + Operator + ".jpg", ImageFormat.Jpeg);
                        }
                        HttpContext.Current.Response.ContentType = contentType;
                        return VirtualPathUtility.ToAbsolute("~/") + "LogoCache/" + firstExt + Operator + ".jpg";
                    }
                    catch (Exception Ex) {
                        HttpContext.Current.Response.ContentType = contentType;
                        string errorMsg = Ex.Message;
                        return "";
                    }
                }
            } else {
                HttpContext.Current.Response.ContentType = contentType;
                return "";
            }

        }
        public static string WriteBinaryImageCache(object _file, string fileName, bool isDimensional, int? width, int? height)
        {
            string contentType = HttpContext.Current.Response.ContentType;
            string errorMsg = string.Empty;
            try {
                if (_file != null && _file != Convert.DBNull) {
                    #region image is not null
                    if (isDimensional) {
                        #region image is Small size
                        byte[] image = (byte[])_file;
                        HttpContext.Current.Response.ContentType = "image/jpeg";
                        using (MemoryStream ms = new MemoryStream(image)) {
                            Bitmap bmp;
                            bmp = (Bitmap)Bitmap.FromStream(ms);
                            try {
                                bmp = ImageResize.ConstrainProportions(bmp, 65, Dimensions.Height);
                                if (height.HasValue && width.HasValue) {
                                    bmp = ImageResize.FixedSize(bmp, height.Value, height.Value);
                                } else if (!height.HasValue && width.HasValue) {
                                    bmp = ImageResize.ConstrainProportions(bmp, width.Value, Dimensions.Width);
                                } else if (height.HasValue && !width.HasValue) {
                                    bmp = ImageResize.ConstrainProportions(bmp, height.Value, Dimensions.Height);
                                }
                            }
                            catch {
                                HttpContext.Current.Response.ContentType = contentType;
                            }

                            try {
                                if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "Cache\\" + fileName)) {
                                    bmp.Save(AppDomain.CurrentDomain.BaseDirectory + "Cache\\" + fileName, ImageFormat.Jpeg);
                                }
                                HttpContext.Current.Response.ContentType = contentType;
                                return fileName;
                            }
                            catch (Exception Ex) {
                                HttpContext.Current.Response.ContentType = contentType;
                                errorMsg = Ex.Message;
                                return "";
                            }
                        }
                        #endregion
                    } else {
                        #region image is Normal size
                        if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "Cache\\" + fileName))
                            File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + "Cache\\" + fileName, (byte[])_file);
                        return fileName;
                        #endregion
                    }
                    #endregion
                } else {
                    HttpContext.Current.Response.ContentType = contentType;
                    return "";
                }
            }
            catch (Exception _Exception) {
                errorMsg = _Exception.Message;
                HttpContext.Current.Response.ContentType = contentType;
                return "";
            }
        }
        public static string WriteBinaryFile(object _file, string fileName, string firstExt, string ext)
        {
            string errorMsg = string.Empty;
            try {
                if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "ACE\\" + firstExt + fileName + "." + ext))
                    File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + "ACE\\" + firstExt + fileName + "." + ext, (byte[])_file);
                return firstExt + fileName + "." + ext;
            }
            catch (Exception _Exception) {
                errorMsg = _Exception.Message;
                return "";
            }
        }

        public object readCustPhotoFile(string fileName)
        {
            string errorMsg = string.Empty;
            try {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "CustPicCache\\" + fileName))
                    return File.ReadAllBytes(AppDomain.CurrentDomain.BaseDirectory + "CustPicCache\\" + fileName);
                else return null;
            }
            catch (Exception _Exception) {
                errorMsg = _Exception.Message;
                return null;
            }
        }

        public string GetMenuGlobalResourceObject(User UserData, string className, string resourceKey)
        {
            CultureInfo ci = UserData.Ci;

            string webID = getWebID();
            string lang = ci.Parent.Name == "en" ? "" : ci.Parent.Name;

            string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + webID + "\\MenuLang." + lang + ".xml";
            DataTable WebConfigData = new DataTable();
            try {
                if (File.Exists(FilePath))
                    WebConfigData.ReadXml(FilePath);
                else {
                    FilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + webID + "\\MenuLang.xml";
                    if (File.Exists(FilePath)) {
                        WebConfigData.ReadXml(FilePath);
                    } else
                        return HttpContext.GetGlobalResourceObject("mainMenu", resourceKey).ToString();
                }

                var retVal = WebConfigData.AsEnumerable().Where(w => w.Field<string>("MSG_KEY") == resourceKey);
                if (retVal.Count() > 0)
                    return retVal.CopyToDataTable().Rows[0]["MESSAGE"].ToString();
                else {
                    FilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + webID + "\\MenuLang.xml";
                    WebConfigData.ReadXml(FilePath);
                    retVal = WebConfigData.AsEnumerable().Where(w => w.Field<string>("MSG_KEY") == resourceKey);
                    if (retVal.Count() > 0)
                        return retVal.CopyToDataTable().Rows[0]["MESSAGE"].ToString();
                    else
                        return HttpContext.GetGlobalResourceObject("mainMenu", resourceKey).ToString();
                }
            }
            catch (Exception Ex) {
                string err = Ex.Message;
                return HttpContext.GetGlobalResourceObject("mainMenu", resourceKey).ToString();
            }
        }

        public string GetSubMenuGlobalResourceObject(User UserData, string resourceKey)
        {

            CultureInfo ci = UserData.Ci;
            string webID = getWebID();

            string lang = ci.Parent.Name == "en" ? "" : ci.Parent.Name;

            string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\SubMenuLang." + lang + ".xml";
            DataTable WebConfigData = new DataTable();
            try {
                if (File.Exists(FilePath))
                    WebConfigData.ReadXml(FilePath);
                else {
                    FilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\SubMenuLang.xml";
                    if (File.Exists(FilePath))
                        WebConfigData.ReadXml(FilePath);
                    else
                        return resourceKey;
                }

                var retVal = WebConfigData.AsEnumerable().Where(w => w.Field<string>("MSG_KEY") == resourceKey);
                if (retVal.Count() > 0)
                    return retVal.FirstOrDefault().Field<string>("MESSAGE").ToString();
                else {
                    FilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\SubMenuLang.xml";
                    WebConfigData.ReadXml(FilePath);
                    retVal = WebConfigData.AsEnumerable().Where(w => w.Field<string>("MSG_KEY") == resourceKey);
                    if (retVal.Count() > 0)
                        return retVal.FirstOrDefault().Field<string>("MESSAGE").ToString();
                    else
                        return resourceKey;
                }
            }
            catch (Exception Ex) {
                string err = Ex.Message;
                return resourceKey;
            }
        }

        public string GetExternalServiceMenuGlobalResourceObject(User UserData, string resourceKey)
        {

            CultureInfo ci = UserData.Ci;
            string webID = getWebID();

            string lang = ci.Parent.Name == "en" ? "" : ci.Parent.Name;

            string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + getWebID() + "\\ExternalServiceMenuLang.xml";
            DataTable WebConfigData = new DataTable();
            try {
                if (File.Exists(FilePath))
                    WebConfigData.ReadXml(FilePath);
                else return string.Empty;

                var retVal = WebConfigData.AsEnumerable().Where(w => w.Field<string>("MSG_KEY") == resourceKey);
                if (retVal.Count() > 0)
                    return retVal.FirstOrDefault().Field<string>("MESSAGE").ToString();
                else return string.Empty;

            }
            catch (Exception Ex) {
                string err = Ex.Message;
                return string.Empty;
            }
        }

        public List<CodeName> getListData(int From, int To, string addNamePer, string addNameEnd)
        {
            List<CodeName> ddlData = new List<CodeName>();
            for (int i = From; i <= To; i++) {
                CodeName row = new CodeName();
                row.Code = i.ToString();
                row.Name = addNamePer + i.ToString() + addNameEnd;
                ddlData.Add(row);
            }
            return ddlData;
        }

        public static void fillDropDownList(object dataSource, System.Web.UI.WebControls.DropDownList Combo, string ValueField, string TextField, string selectValue)
        {
            bool emtyData = (Combo.DataSource == null) || Combo.Items.Count < 1;
            Combo.Items.Clear();
            Combo.DataSource = dataSource;
            Combo.DataValueField = ValueField;
            Combo.DataTextField = TextField;
            Combo.DataBind();
            if (!string.IsNullOrEmpty(selectValue))
                if (Combo.SelectedItem == null)
                    Combo.Items.FindByValue(selectValue).Selected = true;
                else {
                    Combo.SelectedItem.Selected = false;
                    Combo.Items.FindByValue(selectValue).Selected = true;
                }
        }

        public List<GridOptions> getWebGridConfig(User UserData, ref string errorMsg)
        {
            /*
                1: Reservation Monitor
                2: Agency Payment Monitor
                3: Package Search
                4: Only Hotel Search
            */
            List<GridOptions> rData = new List<GridOptions>();
            string tsql = @"if OBJECT_ID('dbo.WEB_GridOptions') is not null 
                            Begin
                                Select PAGEID, GRIDID, GRIDCOLUMNS From WEB_GridOptions (NOLOCK)
                                Where AgencyID = @AgencyID 
                                  And UserID = @UserID
                            End ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "AgencyID", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "UserID", DbType.String, UserData.UserID);
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    while (oReader.Read()) {
                        rData.Add(new GridOptions {
                            PageID = (int)oReader["PAGEID"],
                            GridID = (string)oReader["GRIDID"],
                            GridColumns = (string)oReader["GRIDCOLUMNS"],
                            Columns = new List<gridColumns>()
                        });
                    }
                }
                return rData;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return rData;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public GridOptions getGridColConfig(User UserData, int formID, string gridID)
        {
            GridOptions retVal = new GridOptions();
            if (UserData.GridColumnOptions == null || UserData.GridColumnOptions.Count < 1) return retVal;

            var tmpRetVal = from q in UserData.GridColumnOptions.AsEnumerable()
                            where q.PageID == formID
                            select q;
            if (tmpRetVal.Count() > 0) {
                var query = from q in tmpRetVal.AsEnumerable()
                            where q.GridID.ToUpper() == gridID.ToUpper()
                            select new GridOptions { PageID = q.PageID, GridID = q.GridID, GridColumns = q.GridColumns, Columns = q.Columns };
                if (query.Count() > 0) {
                    retVal.PageID = query.FirstOrDefault().PageID;
                    retVal.GridColumns = query.FirstOrDefault().GridColumns;
                    retVal.GridID = query.FirstOrDefault().GridID;
                    retVal.Columns = query.FirstOrDefault().Columns;
                }
            }
            return retVal;
        }

        public bool setWebGridConfig(User UserData, int FormID, string gridName, string showColumns, ref string errorMsg)
        {
            string value = showColumns;

            string tsql = @"if Exists(Select * From WEB_GridOptions Where AgencyID = @AgencyID And UserID = @UserID And PageID = @FormID And GridID = @GridID)
                            Begin
                                Update WEB_GridOptions
                                Set                                    
                                    GRIDCOLUMNS = @value
                                Where 
                                    AgencyID = @AgencyID
                                And UserID = @UserID
                                And PageID = @FormID 
                                And GridID = @GridID
                            End
                            Else
                            Begin
                                Insert Into WEB_GridOptions (AgencyID, UserID, PageID, GridID, GridColumns)
                                Values (@AgencyID, @UserID, @FormID, @GridID, @Value)
                            End";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try {
                db.AddInParameter(dbCommand, "AgencyID", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "UserID", DbType.String, UserData.UserID);
                db.AddInParameter(dbCommand, "FormID", DbType.Int32, FormID);
                db.AddInParameter(dbCommand, "GridID", DbType.String, gridName);
                db.AddInParameter(dbCommand, "value", DbType.String, value);

                if (db.ExecuteNonQuery(dbCommand) == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return false;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getSearchText(User UserData)
        {
            string tsql = @"Select Note
                            From FixNotes (NOLOCK)
                            Where Market = @Market And Type = 0
                            And Section = 'PriceList' ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                return (string)db.ExecuteScalar(dbCommand);
            }
            catch {
                return "";
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public User getGridOptions(User UserData, DataControlFieldCollection gridCol, int Page, string gridID)
        {
            /*  Grid Pages
                1: Reservation Monitor
                2: Agency Payment Monitor
                3: Package Search
                4: Only Hotel Search
                5: Tour Package Search
                6: Only Ticket Search
            */

            string gridOptions = string.Empty;
            string errorMsg = string.Empty;

            GridOptions row = new UICommon().getGridColConfig(UserData, Page, gridID);
            string returnValue = string.Empty;

            string columnsID = string.Empty;
            List<gridColumns> columns = new List<gridColumns>();

            bool empty = true;
            string _gridID = string.Empty;

            if (row != null && !string.IsNullOrEmpty(row.GridColumns)) {
                returnValue = row.GridColumns;
                columns = Newtonsoft.Json.JsonConvert.DeserializeObject<List<gridColumns>>(returnValue);
                _gridID = row.GridID;
                empty = false;
            } else {
                for (int i = 0; i < gridCol.Count; i++) {
                    gridColumns col = new gridColumns();
                    var query =
                    col.ColID = i;
                    col.ColSelected = true;
                    col.ColEnable = true;
                    col.ColVisible = true;
                    col.Compulsory = false;
                    col.ColName = gridCol[i].HeaderText;
                    columns.Add(col);
                }
                returnValue = Newtonsoft.Json.JsonConvert.SerializeObject(columns);
                _gridID = gridID;
                empty = true;
            }

            List<GridOptions> gOpt = new List<GridOptions>();
            UserData.GridColumnOptions.Remove(new GridOptions { PageID = 3 });
            gOpt.Add(new GridOptions { ID = UserData.GridColumnOptions.Count() + 1, PageID = 3, GridColumns = returnValue, Columns = columns, Empty = empty, GridID = _gridID });
            UserData.GridColumnOptions = gOpt;

            return UserData;
        }

        public List<HotelsInfoOptions> getHotelInfoWeb(User UserData, string[] HotelCode, ref string errorMsg)
        {
            object _hotelInfo = new TvBo.Common().getFormConfigValue("SearchPanel", "HotelInfo");
            bool hotelInfo = _hotelInfo != null ? (bool)_hotelInfo : false;

            object _hotelInfoExt = new TvBo.Common().getFormConfigValue("SearchPanel", "HotelInfoExt");
            bool hotelInfoExt = _hotelInfoExt != null ? (bool)_hotelInfo : false;

            object _hotelInfoUrl = new TvBo.Common().getFormConfigValue("SearchPanel", "HotelInfoUrl");
            string hotelInfoUrl = _hotelInfoUrl != null ? (string)_hotelInfo : "HotelInfo.aspx";

            List<HotelsInfoOptions> retVal = new List<HotelsInfoOptions>();

            if (!hotelInfo) return retVal;

            string tsql = string.Empty;
            tsql = "Declare @Hotels TABLE (Hotel VarChar(10)) \n";
            for (int i = 0; i < HotelCode.Length; i++) {
                tsql += string.Format("Insert Into @Hotels(Hotel) Values ('{0}') \n", HotelCode[i].ToString());
            }
            tsql += @"  Select X.Hotel, isnull(X.InfoWeb, ''), useLocal = Case When isnull(X.InfoWeb, '') = '' Then Cast(1 AS bit) Else Cast(0 AS bit) End
                        From 
                        (
                            Select H.Hotel, 
	                            InfoWeb = (Select InfoWeb From HotelMarOpt (NOLOCK) Where Hotel = H.Hotel And Market = @Market)	
                            From @Hotels H
                        ) X";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    while (oReader.Read()) {
                        retVal.Add(new HotelsInfoOptions {
                            Hotel = (string)oReader["Hotel"],
                            UseLocal = (bool)oReader["useLocal"],
                            HotelInfoUrl = (hotelInfoExt ? ((bool)oReader["useLocal"] ? (hotelInfoUrl) : (string)oReader["InfoWeb"]) : hotelInfoUrl)
                        });
                    }
                }
                return retVal;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return retVal;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<DDLData> getNations(string Market, ref string errorMsg)
        {
            List<DDLData> records = new List<DDLData>();
            string tsql = @"Select RecID, Name=isnull(dbo.FindLocalName(NameLID, @Market), Name)
                            From Location (NOLOCK)
                            Where Type=1";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        DDLData rec = new DDLData();
                        rec.FieldData = R["RecID"].ToString();
                        rec.TextData = R["Name"].ToString();
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return records;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<MainMenu> setMainMenuLanguage(User UserData, List<MainMenu> menu)
        {
            foreach (MainMenu rec in menu) {
                rec.MenuResource = new UICommon().GetMenuGlobalResourceObject(UserData, "mainMenu", rec.ResourceName);
            }
            return menu;
        }

        public string nameWrittingRule(User UserData, NameWrittingRuleTypes ruleType)
        {
            string nameRules = string.Empty;
            string surnameRules = string.Empty;
            switch (UserData.TvParams.TvParamReser.NameRules) {
                case 0:
                    nameRules = "text-transform: uppercase;";
                    surnameRules = "text-transform: uppercase;";
                    break;
                case 1:
                    nameRules = "text-transform: capitalize;";
                    surnameRules = "text-transform: capitalize;";
                    break;
                case 2:
                    nameRules = "text-transform: capitalize;";
                    surnameRules = "text-transform: uppercase;";
                    break;
                default:
                    nameRules = string.Empty;
                    surnameRules = string.Empty;
                    break;
            }
            if (ruleType == NameWrittingRuleTypes.nameWrittingRules)
                return nameRules;
            else return surnameRules;
        }

        public string nameWrittingRuleValue(User UserData, NameWrittingRuleTypes ruleType)
        {
            string nameRules = string.Empty;
            string surnameRules = string.Empty;
            switch (UserData.TvParams.TvParamReser.NameRules) {
                case 0:
                    nameRules = "uppercase";
                    surnameRules = "uppercase";
                    break;
                case 1:
                    nameRules = "capitalize";
                    surnameRules = "capitalize";
                    break;
                case 2:
                    nameRules = "capitalize";
                    surnameRules = "uppercase";
                    break;
                default:
                    nameRules = string.Empty;
                    surnameRules = string.Empty;
                    break;
            }
            if (ruleType == NameWrittingRuleTypes.nameWrittingRules)
                return nameRules;
            else return surnameRules;
        }

        public string getAgencyRating(string Market, string AgencyID, string WebVersion, ref string errorMsg)
        {
            string tsql = string.Empty;
            Int16? webVersion = Conversion.getInt16OrNull(WebVersion);
            if (webVersion.HasValue && webVersion.Value > 10)
                tsql = "Exec dbo.sp_AgencyRating @Market, @AgencyID, 0, 0, 2, 1, 3, @date";
            else tsql = "Exec dbo.sp_AgencyRating @Market, @AgencyID, 0, 0, 0, 0, @date";

            string rating = string.Empty;
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "AgencyID", DbType.String, AgencyID);
                db.AddInParameter(dbCommand, "date", DbType.DateTime, Convert.ToInt32(WebVersion) > 10 ? Convert.DBNull : new DateTime(2012, 1, 12));
                //date => DateTime.ToDay, Fatih changed 2012.01.12 ->2012.05.02 
                /* GTalk Sent at 11:19 AM on Wednesday
fbilgen1: Sunrise için
Teste
Exec dbo.sp_AgencyRating @Market, @AgencyID, 0, 0, 2, 1, 0, @date
demiştin ya
onu
Exec dbo.sp_AgencyRating @Market, @AgencyID, 0, 0, 2, 1, 3, '2012-01-02'
şeklinde yapıp
testlerini güncelleyebilir misin*
                 */

                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    if (oReader.Read())
                        rating = Conversion.getStrOrNull(oReader[0]);
                }
                return rating;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return rating;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getResPaymentsPlanHTML(string Market, string ResNo, string ToCur, string DateFormat, ref string errorMsg)
        {
            StringBuilder sb = new StringBuilder();
            string tsql = @"Select dbo.ufn_Exchange((Select ResDate From ResMain (NOLOCK) Where ResNo = @ResNo), (Select SaleCur From ResMain (NOLOCK) Where ResNo = @ResNo), @ToCur, Amount, 0, @Market) Amount, DueDate	
                            From ResPayPlan (NOLOCK) 
                            Where ResNo = @ResNo
                            Order By DueDate";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            DataTable Payments;
            try {
                db.AddInParameter(dbCommand, "ToCur", DbType.String, ToCur);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        decimal? amount = Conversion.getDecimalOrNull(R["Amount"]);
                        DateTime? dueDate = Conversion.getDateTimeOrNull(R["DueDate"]);
                        sb.Append(" <tr>");
                        sb.Append("     <td>" + (amount.HasValue ? amount.Value.ToString("#,###.##") : "") + " " + ToCur + " " + (dueDate.HasValue ? dueDate.Value.ToString(DateFormat) : "") + "</td>");
                        sb.Append(" </tr>");
                    }
                    return sb.Length > 0 ? "<table>" + sb.ToString() + "</table>" : "";
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getCurrentPageForMenuItem(User UserData, Uri uri)
        {
            string currentMenuItem = "ResMonitor";
            string pageUrl = uri.Segments.LastOrDefault(); //uri.AbsolutePath;
            string urlQuery = uri.Query;
            string errorMsg = string.Empty;

            List<TvBo.MainMenu> mainMenuData = CacheObjects.getMainMenuData(UserData, ref errorMsg);
            if (mainMenuData != null) {
                //if (Http lobal.useMixSearch)
                //{
                //    foreach (TvBo.MainMenu row in mainMenuData)
                //    if (string.Equals(row.Url, "OnlyHotelSearch") && string.Equals(row.PageExt, "Blank.aspx"))
                //    {
                //        row.Url = "OnlyHotelMixSearch";
                //    }
                //}
                List<TvBo.MainMenu> queryStringMenu = mainMenuData.Where(w => !string.IsNullOrEmpty(w.QueryStr)).ToList<TvBo.MainMenu>();
                TvBo.MainMenu currentMenu = null;
                if (queryStringMenu.Where(f => urlQuery.Contains(f.QueryStr)).Count() > 0)
                    currentMenu = mainMenuData.Find(f => urlQuery.Contains(f.QueryStr) && string.Equals(f.Url + f.PageExt.Replace("Blank", ""), pageUrl.Replace("Blank", "")));
                else currentMenu = mainMenuData.Find(f => string.Equals(/*"/" + */f.Url + f.PageExt.Replace("Blank", ""), pageUrl.Replace("Blank", "")));
                if (currentMenu != null) {
                    if (string.IsNullOrEmpty(urlQuery)) {
                        currentMenuItem = currentMenu.SubMenuItem.ToString();
                    } else {
                        if (urlQuery.IndexOf("B2BMenuCat") > 0) {
                            string[] qStr = urlQuery.Split('&');
                            var qStrQ = qStr.AsEnumerable().Where(w => w.IndexOf("B2BMenuCat") > -1);
                            if (qStrQ != null && qStrQ.Count() > 0) {
                                string b2bMenuCat = qStrQ.FirstOrDefault().Split('=').Length > 1 ? qStrQ.FirstOrDefault().Split('=')[1] : "";
                                if (pageUrl.IndexOf("Blank") > -1)
                                    currentMenu = mainMenuData.Find(f => string.Equals(f.Url + f.PageExt, pageUrl) && string.Equals(f.B2BMenuCat, b2bMenuCat));
                                else currentMenu = mainMenuData.Find(f => string.Equals(f.Url + f.PageExt.Replace("Blank", ""), pageUrl) && string.Equals(f.B2BMenuCat, b2bMenuCat));

                                currentMenuItem = currentMenu.SubMenuItem.ToString();
                            }
                        } else {
                            currentMenuItem = currentMenu.SubMenuItem.ToString();
                        }
                    }
                }
            }
            return currentMenuItem;
        }

        public string createQueryString(User UserData, ResDataRecord ResData, List<JSonStruc> extParams, ref string errorMsg)
        {
            string retVal = string.Empty;

            foreach (JSonStruc row in extParams) {
                if (retVal.Length > 0) retVal += "&";
                switch (row.Value) {
                    case "Agency":
                        retVal += row.Name + "=";
                        retVal += ResData.ResMain.Agency;
                        break;
                    case "AgencyUser":
                        retVal += row.Name + "=";
                        retVal += ResData.ResMain.AgencyUser;
                        break;
                    case "ResNo":
                        retVal += row.Name + "=";
                        retVal += ResData.ResMain.ResNo;
                        break;
                    case "Days":
                        retVal += row.Name + "=";
                        retVal += ResData.ResMain.Days.ToString();
                        break;
                }
            }

            return "?" + retVal;
        }
    }
}

public static class EncryptionHelper
{
    public static string Encrypt(string input)
    {
        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, String.Empty, DateTime.Now, DateTime.MaxValue, false, input);
        return FormsAuthentication.Encrypt(ticket);
    }

    public static string Decrypt(string input)
    {
        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(input);
        return ticket.UserData;
    }
}