﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Reflection;

namespace TvBo
{
    public class VersionControl
    {
        #region parameters

        /// <summary>
        ///  eq=is equal to, 
        ///  ne=is not equal to, 
        ///  gt=is greater than,
        ///  ge=is greater than or equal to,
        ///  lt=is less than,
        ///  le=is less than or equal to
        /// </summary>
        public enum Equality { eq, ne, gt, ge, lt, le }

        /// <summary>
        /// ResCon table "Remark, SpecSerRQCode1, SpecSerRQCode2" fields added.
        /// </summary>
        public static int WebWersion0005 = 5;

        /// <summary>
        /// ServiceExtMarOpt table "ShowInResDetB2B, ShowInResDetB2C, ShowInResDet" fields added.
        /// </summary>
        public static int WebWersion0007 = 7;

        /// <summary>
        /// HotelRoomMarOpt table added.
        /// HotelAccomMarOpt table added.
        /// HotelBoardMarOpt table added.
        /// </summary>
        public static int WebWersion0008 = 8;

        /// <summary>
        /// ResChgFee table, change date method parameter 30 -> 60 changed.
        /// dbo.sp_AgencyRating sp changed.
        /// </summary>
        public static int WebWersion0010 = 10;

        /// <summary>
        /// sp_CheckExtSerAlllot sp "ServiceItem" parameters added.
        /// </summary>
        public static int WebWersion0012 = 12;

        /// <summary>
        /// ParamSystem table "SejourServerName, SejourDBName" feilds added
        /// </summary>
        public static int WebWersion0013 = 13;

        /// <summary>
        /// ResMain table "PackedPrice" feild added.
        /// </summary>
        public static int WebWersion0015 = 15;

        /// <summary>
        /// PayType table "DiscountVal" field added.
        /// ParamFlight table "MaxPaxonTicket" field added.
        /// </summary>
        public static int WebWersion0016 = 16;

        /// <summary>
        /// 
        /// </summary>
        public static int WebWersion0021 = 21;

        /// <summary>
        ///  ParamReser table "MaxChdAgeB2Bsrc" field added.
        /// </summary>
        public static int WebWersion0022 = 22;

        /// <summary>
        /// ServiceMarOpt table "B2BIndPack" field added.
        /// ParamReser table "SendNotMailResCancelB2B" field added.
        /// </summary>
        public static int WebWersion0023 = 23;

        /// <summary>
        /// CatalogPack table "DepDepCity, DepArrCity, RetDepCity, RetArrCity, DepServiceType, RetServiceType" field added.
        /// </summary>
        public static int WebWersion0024 = 24;

        /// <summary>
        /// CatalogPack table "B2BMenuCat" field added.
        /// HolPack table "B2BMenuCat" field added.
        /// B2BMenuCat table added.      
        /// </summary>
        public static int WebWersion0025 = 25;

        /// <summary>
        /// ExcursionDates table added.        
        /// </summary>
        public static int WebWersion0026 = 26;

        /// <summary>
        /// ResMain table "BonusID2" field added.
        /// </summary>
        public static int WebWersion0027 = 27;

        /// <summary>
        /// ResService table "ChkSel" field added.
        /// ResServiceExt table "ChkSel" field added.
        /// </summary>
        public static int WebWersion0028 = 28;

        /// <summary>
        /// FlightDay table "TDepDate" feild added.
        /// FlightDay table "TArrDate" feild added.
        /// </summary>
        public static int WebWersion0029 = 29;

        /// <summary>
        /// FlightDay table "TDepTime" feild added.
        /// FlightDay table "TArrTime" feild added.
        /// </summary>
        public static int WebWersion0030 = 30;

        /// <summary>
        /// ParamReser table "HoneymoonChkW" feild added.        
        /// </summary>
        public static int WebWersion0031 = 31;

        /// <summary>
        /// FlightDay table "AllowPrintTicB2B" feild added.        
        /// </summary>
        public static int WebWersion0032 = 32;

        /// <summary>
        /// ResCustVisa table "CitizenshipOriginal" field added.
        /// ResCustVisa table "VisaCountry" field added.
        /// ResCustVisa table "VisaCountry1" field added.
        /// ResCustVisa table "VisaCountry2" field added.
        /// ResCustVisa table "VisaCountry3" field added.
        /// ResCustVisa table "CountryOfDestination" field added.
        /// ResCustVisa table "CountryOfFirstEntry" field added.
        /// ResCustVisa table "Mobile" field added.   
        /// ResCustVisa table "OrganizationName" field added.   
        /// ResCustVisa table "OrganizationAddress" field added.   
        /// ResCustVisa table "OrganizationPhone" field added.   
        /// ResCustVisa table "LegalRepresentativeSurname" field added.   
        /// ResCustVisa table "LegalRepresentativeAddress" field added.   
        /// ResCustVisa table "LegalRepresentativeName" field added.   
        /// ResCustVisa table "LegalRepresentativeCitizenship" field added.   
        /// </summary>
        public static int WebWersion0033 = 33;

        /// <summary>
        /// Only ticket OW Stop sale control
        /// </summary>
        public static int WebWersion0034 = 34;

        /// <summary>
        /// PasEBHot table "board" field added.
        /// </summary>
        public static int WebWersion0035 = 35;

        /// <summary>
        /// Hotel table "Chk1" field added.
        /// </summary>
        public static int WebWersion0036 = 36;
        #endregion

        public static System.Version getVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version;
        }
        public static bool CheckWebVersion(string currentVersionStr, int checkVersion, Equality equality)
        {
            try
            {
                int currentVersion = currentVersionStr != null ? Convert.ToInt32(currentVersionStr) : -1;
                if (currentVersion < 1) return false;
                bool retVal = false;
                switch (equality)
                {
                    case Equality.eq: retVal = currentVersion == checkVersion; break;
                    case Equality.ge: retVal = currentVersion >= checkVersion; break;
                    case Equality.gt: retVal = currentVersion > checkVersion; break;
                    case Equality.le: retVal = currentVersion <= checkVersion; break;
                    case Equality.lt: retVal = currentVersion < checkVersion; break;
                    case Equality.ne: retVal = currentVersion != checkVersion; break;
                    default: retVal = false; break;
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
        }

        public static bool tableIsExists(string tableName, ref string errorMsg)
        {
            List<dbInformationShema> records = new List<dbInformationShema>();
            string tsql =
@"
IF OBJECT_ID (@pTableName, N'U') IS NOT NULL 
  SELECT Cast(1 AS Bit)
ELSE
  SELECT Cast(0 AS Bit)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pTableName", DbType.String, tableName);
                return (bool)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static List<dbInformationShema> getSpShema(string spName, ref string errorMsg)
        {
            List<dbInformationShema> records = new List<dbInformationShema>();
            string tsql = @"Select SeqNo=ORDINAL_POSITION,Name=PARAMETER_NAME,DType=DATA_TYPE 
                            From information_schema.PARAMETERS
                            Where specific_name=@SpName";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "SpName", DbType.String, spName);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        records.Add(new dbInformationShema
                        {
                            SeqNo = (int)rdr["SeqNo"],
                            Name = TvTools.Conversion.getStrOrNull(rdr["Name"]),
                            DType = TvTools.Conversion.getStrOrNull(rdr["DType"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static List<dbInformationShema> getTableShema(string tableName, ref string errorMsg)
        {
            List<dbInformationShema> records = new List<dbInformationShema>();
            string tsql = @"Select SeqNo=ORDINAL_POSITION, Name=COLUMN_NAME, DType=DATA_TYPE
                            From information_schema.COLUMNS
                            Where TABLE_NAME=@tableName";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "tableName", DbType.String, tableName);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        records.Add(new dbInformationShema
                        {
                            SeqNo = (int)rdr["SeqNo"],
                            Name = TvTools.Conversion.getStrOrNull(rdr["Name"]),
                            DType = TvTools.Conversion.getStrOrNull(rdr["DType"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static List<dbInformationShema> getTableFunctionShema(string functionName, ref string errorMsg)
        {
            List<dbInformationShema> records = new List<dbInformationShema>();
            string tsql =
@"
SELECT SeqNo=ORDINAL_POSITION,Name=COLUMN_NAME,DType=DATA_TYPE
FROM INFORMATION_SCHEMA.ROUTINE_COLUMNS 
WHERE TABLE_NAME=@FunctionName
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "FunctionName", DbType.String, functionName);
                using (IDataReader rdr = db.ExecuteReader(dbCommand)) {
                    while (rdr.Read()) {
                        records.Add(new dbInformationShema {
                            SeqNo = (int)rdr["SeqNo"],
                            Name = TvTools.Conversion.getStrOrNull(rdr["Name"]),
                            DType = TvTools.Conversion.getStrOrNull(rdr["DType"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static bool getTableField(string tableName, string fieldName)
        {
            List<dbInformationShema> records = new List<dbInformationShema>();
            string tsql = @"Select SeqNo=ORDINAL_POSITION, Name=COLUMN_NAME, DType=DATA_TYPE
                            From information_schema.COLUMNS
                            Where TABLE_NAME=@tableName and COLUMN_NAME=@columnName";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "tableName", DbType.String, tableName);
                db.AddInParameter(dbCommand, "columnName", DbType.String, fieldName);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                        return true;
                    else return false;
                }
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }        
    }
}
