﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public enum SearchType
    {
        NoSearch = 0,
        PackageSearch = 1,
        OnlyHotelSearch = 2,
        TourPackageSearch = 3,
        OnlyFlightSearch = 4,
        OtherSearch = 5,
        OnlyExcursion = 6,
        OnlyTransfer = 7,
        DynamicFlightPackageSearch = 8,
        CruiseSearch = 9,
        OnlyHotelMixSearch = 10,
        HotelSale = 11,
        PackPriceSearch = 12
    };
    public enum NameType { NormalName = 0, LocalName = 1 };
    public enum HandicapTypes : short { None = 0, Package = 1, OnlyTicket = 2 }
    public enum ParametersSection { All = 0, ParamSystem = 1, ParamPrice = 2, ParamReser = 3, ParamFlight = 4, ParamTransport = 5, ParamPay = 6 }
    public enum ParamSystemField { All = 0 }

    [Serializable]
    public enum DayOfWeekTv
    {
        // Summary:
        //     Indicates Sunday.
        Sunday = 7,
        //
        // Summary:
        //     Indicates Monday.
        Monday = 1,
        //
        // Summary:
        //     Indicates Tuesday.
        Tuesday = 2,
        //
        // Summary:
        //     Indicates Wednesday.
        Wednesday = 3,
        //
        // Summary:
        //     Indicates Thursday.
        Thursday = 4,
        //
        // Summary:
        //     Indicates Friday.
        Friday = 5,
        //
        // Summary:
        //     Indicates Saturday.
        Saturday = 6,
    }

    [Serializable]
    public class ReservationCustomCompulsoryCheck
    {
        public string Market { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
    }

    [Serializable()]
    public class DocumentNoRecord
    {
        public DocumentNoRecord()
        {
        }

        string _Serial;
        public string Serial
        {
            get { return _Serial; }
            set { _Serial = value; }
        }

        int? _No;
        public int? No
        {
            get { return _No; }
            set { _No = value; }
        }
    }

    [Serializable()]
    public class HolPackBrochureRecord
    {
        public HolPackBrochureRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _HolPack;
        public string HolPack
        {
            get { return _HolPack; }
            set { _HolPack = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _FileType;
        public string FileType
        {
            get { return _FileType; }
            set { _FileType = value; }
        }

        object _FileImage;
        public object FileImage
        {
            get { return _FileImage; }
            set { _FileImage = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

    }

    [Serializable()]
    public class holPackBrochureReady
    {
        public holPackBrochureReady()
        {
            _brochureReady = false;
        }

        string _HolPack;
        public string HolPack
        {
            get { return _HolPack; }
            set { _HolPack = value; }
        }

        string _HolPackName;
        public string HolPackName
        {
            get { return _HolPackName; }
            set { _HolPackName = value; }
        }

        string _HolPackNameL;
        public string HolPackNameL
        {
            get { return _HolPackNameL; }
            set { _HolPackNameL = value; }
        }

        bool _brochureReady;
        public bool brochureReady
        {
            get { return _brochureReady; }
            set { _brochureReady = value; }
        }

    }

    public class TvParameters
    {
        public TvParameters()
        {
        }

        ParamSystem _TvParamSystem;
        public ParamSystem TvParamSystem
        {
            get { return _TvParamSystem; }
            set { _TvParamSystem = value; }
        }

        ParamReser _TvParamReser;
        public ParamReser TvParamReser
        {
            get { return _TvParamReser; }
            set { _TvParamReser = value; }
        }

        ParamFlight _TvParamFlight;
        public ParamFlight TvParamFlight
        {
            get { return _TvParamFlight; }
            set { _TvParamFlight = value; }
        }

        ParamPay _TvParamPay;
        public ParamPay TvParamPay
        {
            get { return _TvParamPay; }
            set { _TvParamPay = value; }
        }

        ParamPrice _TvParamPrice;
        public ParamPrice TvParamPrice
        {
            get { return _TvParamPrice; }
            set { _TvParamPrice = value; }
        }

        ParamTransport _TvParamTransport;
        public ParamTransport TvParamTransport
        {
            get { return _TvParamTransport; }
            set { _TvParamTransport = value; }
        }

    }

    public class ParamSystem
    {
        public ParamSystem()
        {
        }

        public int? RecID { get; set; }
        public string Market { get; set; }
        public string UseSysParam { get; set; }
        public string BaseCur { get; set; }
        public Int16? BaseCurDec { get; set; }
        public int? LocalLangID { get; set; }
        public int? SysLangID { get; set; }
        public string AutoChgKBLayout { get; set; }
        public string BaseBank { get; set; }
        public Int16? BuyRate { get; set; }
        public Int16? SaleRate { get; set; }
        public Int16? PaymentRate { get; set; }
        public Int16? RateDateOpt { get; set; }
        public Int16? ExChgOpt { get; set; }
        public string DefPrinter { get; set; }
        public string DefFaxPrinter { get; set; }
        public Int16? LogLevel { get; set; }
        public Int16? LogKeepDay { get; set; }
        public Int16? KeyCase { get; set; }
        public string Version { get; set; }
        public Int16? Bonus_AgencyMaxPerc { get; set; }
        public Int16? Bonus_UserMaxPerc { get; set; }
        public Int16? Bonus_PassMaxPerc { get; set; }
        public string Bonus_Version { get; set; }
        public string Bonus_UseSysParam { get; set; }
        public byte? VC_CheckType { get; set; }
        public string VC_SharePoint { get; set; }
        public string VC_FtpUser { get; set; }
        public string VC_FtpPass { get; set; }
        public string VC_ProxyName { get; set; }
        public string VC_ProxyUser { get; set; }
        public string VC_ProxyPass { get; set; }
        public string VC_BackUpdate { get; set; }
        public string VC_ShowDlg { get; set; }
        public string VC_CheckStartup { get; set; }
        public string CustomRegID { get; set; }
        public string SMTPServer { get; set; }
        public int? SMTPPort { get; set; }
        public string SMTPAccount { get; set; }
        public string SMTPPass { get; set; }
        public Int16? SMTPUseSSL { get; set; }
        public Int16? PrtTicketDayOpt { get; set; }
        public Int16? PrtVoucherDayOpt { get; set; }
        public Int16? PrtInsuranceDayOpt { get; set; }
        public Int16? PrtContractDayOpt { get; set; }
        public Int16? PrtTicketDayOptW { get; set; }
        public Int16? PrtVoucherDayOptW { get; set; }
        public Int16? PrtInsuranceDayOptW { get; set; }
        public Int16? PrtContractDayOptW { get; set; }
        public string TvCCServer { get; set; }
        public bool? SMTPauth { get; set; }
        public bool? Bonus_EarnForUse { get; set; }
        public string WVersion { get; set; }
        public decimal? Bonus_AgencyMinLimit { get; set; }
        public decimal? Bonus_UserMinLimit { get; set; }
        public decimal? Bonus_PassMinLimit { get; set; }
        public string SystemAdminMails { get; set; }
        public bool? AutoGetRate { get; set; }
        public string ImpRateSaveBank { get; set; }
        public decimal? ImpRateChgBuyPer { get; set; }
        public decimal? ImpRateChgSalePer { get; set; }
        public bool? BirthdayMsgSend { get; set; }
        public string BirtdayMsgSubject { get; set; }
        public string SejourServerName { get; set; }
        public string SejourDBName { get; set; }
        public bool? TVCCOptionDate { get; set; }
    }

    public class ParamReser
    {
        public ParamReser()
        {
        }
        public int? RecID { get; set; }
        public string Market { get; set; }
        public bool UseSysParam { get; set; }
        public Int16? NameRules { get; set; }
        public bool AdultBirtDayChk { get; set; }
        public bool AdultBirtDayWebChk { get; set; }
        public bool ChildBirtDayChk { get; set; }
        public Int16? MinAdultAge { get; set; }
        public decimal? MaxInfAge { get; set; }
        public decimal? MaxChdAge { get; set; }
        public bool PassReqRes { get; set; }
        public bool AllowDupResCtrl { get; set; }
        public bool DupResSurName { get; set; }
        public bool DupResName { get; set; }
        public bool DupResBirthDay { get; set; }
        public bool DupResPass { get; set; }
        public bool DupResIdNo { get; set; }
        public bool DupResBetPer { get; set; }
        public Int16? DupResBetPerDay { get; set; }
        public bool HotAllotChk { get; set; }
        public bool HotAllotWarnFull { get; set; }
        public bool HotAllotNotAcceptFull { get; set; }
        public bool HotAllotNotFullGA { get; set; }
        public Int16? WarnMaxDay { get; set; }
        public bool AutoLockChk { get; set; }
        public Int16? AutoLockMode { get; set; }
        public Int16? AutoLockDay { get; set; }
        public string AutoLockDayAfBef { get; set; }
        public bool AgencyMailResChg { get; set; }
        public bool AgencyMailResChgConf { get; set; }
        public bool AgencyMailFlgChg { get; set; }
        public bool AgencyMailFlgChgConf { get; set; }
        public bool AgencyMailCommentChg { get; set; }
        public bool AgencyMailCommentChgConf { get; set; }
        public byte? PayRuleOfCancel { get; set; }
        public byte? MaxRoomCnt { get; set; }
        public bool AllowBlackPasRes { get; set; }
        public bool SendOptForPrint { get; set; }
        public bool SendOptForExcel { get; set; }
        public bool AllowDupOTicCtrl { get; set; }
        public bool ExtSerConfToResConf { get; set; }
        public byte? AllowMinAge1 { get; set; }
        public byte? AllowMinAge2 { get; set; }
        public bool ChdPassChk { get; set; }
        public bool B2B_NeedAddress { get; set; }
        public bool B2C_NeedAddress { get; set; }
        public bool NROnCX_Visa { get; set; }
        public bool NROnCX_CanIns { get; set; }
        public bool NROnCX_StdIns { get; set; }
        public bool BonusPasUseW { get; set; }
        public bool BonusAgencyUseW { get; set; }
        public bool BonusUserUseW { get; set; }
        public bool BonusAgencySeeOwnW { get; set; }
        public bool BonusUserSeeOwnW { get; set; }
        public bool BonusUserSeeAgencyW { get; set; }
        public Int16? MinChgCountForFee { get; set; }
        public byte? MaxPaxCnt { get; set; }
        public bool PassIssueReqRes { get; set; }
        public bool ChkResChgforFee { get; set; }
        public bool LeaderEMail { get; set; }
        public byte? AgeCalcType { get; set; }
        public decimal? MaxChdAgeB2Bsrc { get; set; }
        public bool? SendNotMailResCancelB2B { get; set; }
        public bool? HoneymoonChkW { get; set; }
        public bool? NeedPhone { get; set; }
        public bool? NeedPIN { get; set; }
        public bool? AllowNoNameTV { get; set; }
        public bool? AllowNoNameB2B { get; set; }
        public bool? AllowNoNameB2C { get; set; }
    }

    public class ParamFlight
    {
        public ParamFlight()
        {
        }

        public bool UseSysParam { get; set; }
        public bool PrintOWasRT { get; set; }
        public string VRTStat { get; set; }
        public Int16? OpenNVAday { get; set; }
        public bool ShowFlyDateOpt { get; set; }
        public bool AllotChk { get; set; }
        public bool AllotWarnFull { get; set; }
        public bool AllotDoNotOver { get; set; }
        public bool UseOptForAllot { get; set; }
        public bool OptAllotDoNotOver { get; set; }
        public bool OTicSaleReqTel { get; set; }
        public Int16? RetFlightOpt { get; set; }
        public Int16? DispMinFlightAllot { get; set; }
        public Int16? DispMinFlightAllotW { get; set; }
        public Int16? MaxPaxonTicket { get; set; }
        public bool? UseTicPriceForOTic { get; set; }
    }

    public class ParamPay
    {
        public ParamPay()
        {
        }

        public Int16? RoundPayment { get; set; }
        public Int16? PayMode { get; set; }
        public Int16? PayModeDay { get; set; }
        public string AfBef { get; set; }
        public string PayType { get; set; }
        public Int16? ResPayOver { get; set; }
        public Int16? AgencyResPayRule { get; set; }
        public Int16? PayOptTime { get; set; }
        public bool ColWithInvoice { get; set; }
        public Int16? PayPlanDateOpt { get; set; }
    }

    public class ParamPrice
    {
        public ParamPrice()
        {
        }

        public bool UseSysParam { get; set; }
        public Int16? RoundBuyPrice { get; set; }
        public Int16? RoundSalePrice { get; set; }
        public string ChdGrp1Name { get; set; }
        public string ChdGrp2Name { get; set; }
        public string ChdGrp3Name { get; set; }
        public string ChdGrp4Name { get; set; }
        public string ChdGrp1SName { get; set; }
        public string ChdGrp2SName { get; set; }
        public string ChdGrp3SName { get; set; }
        public string ChdGrp4SName { get; set; }
        public Int16? SaleCalcType { get; set; }
        public bool LastPriceListOpt { get; set; }
        public Int16? PPayMode { get; set; }
        public Int16? PPayModeDay { get; set; }
        public string PAfBef { get; set; }
        public string PPayType { get; set; }
        public bool InvPrtNoPay { get; set; }
        public bool ResLockonInvoice { get; set; }
        public Int16? RoundCom { get; set; }
        public Int16? InvSumType { get; set; }
        public Int16? ResSalePriceOpt { get; set; }
        public Int16? CreditMode { get; set; }
        public bool AllowMakeRes { get; set; }
        public string PLAdlCap { get; set; }
        public string PLChdCap { get; set; }
        public Int16? SaleCurOpt { get; set; }
        public bool? CanInsIncStd { get; set; }
        public byte? IndSaleCurOpt { get; set; }
        public byte? PLSPOCalcType { get; set; }
        public Int16? RoundType { get; set; }
        public Int16? RoundOpt { get; set; }
        public decimal? RndOpt1 { get; set; }
        public decimal? RndOpt2 { get; set; }
    }

    public class ParamTransport
    {
        public ParamTransport()
        {
        }

        public bool UseSysParam { get; set; }
        public bool AllotChk { get; set; }
        public bool AllotWarnFull { get; set; }
        public bool AllotDoNotOver { get; set; }
    }

    public class AgencyIPList
    {
        public AgencyIPList()
        {
        }

        string _IpNo;
        public string IpNo
        {
            get { return _IpNo; }
            set { _IpNo = value; }
        }

        bool _ServiceControl;
        public bool ServiceControl
        {
            get { return _ServiceControl; }
            set { _ServiceControl = value; }
        }

    }

    [Serializable]
    public class listInteger
    {
        public listInteger()
        {
        }

        int _Code;
        public int Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
    }

    [Serializable]
    public class listString
    {
        public listString()
        {
            _status = true;
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _ParentCode;
        public string ParentCode
        {
            get { return _ParentCode; }
            set { _ParentCode = value; }
        }

        bool _status;
        public bool status
        {
            get { return _status; }
            set { _status = value; }
        }

    }

    public class SearchCriteriaRooms
    {
        public SearchCriteriaRooms()
        {
            _Adult = 2;
            _Child = 0;
            _Chd1Age = null;
            _Chd2Age = null;
            _Chd3Age = null;
            _Chd4Age = null;
        }

        int _RoomNr;
        public int RoomNr
        {
            get { return _RoomNr; }
            set { _RoomNr = value; }
        }

        Int16 _Adult;
        public Int16 Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16 _Infant;
        public Int16 Infant
        {
            get { return _Infant; }
            set { _Infant = value; }
        }

        Int16 _Child;
        public Int16 Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        Int16? _Chd1Age;
        public Int16? Chd1Age
        {
            get { return _Chd1Age; }
            set { _Chd1Age = value; }
        }

        Int16? _Chd2Age;
        public Int16? Chd2Age
        {
            get { return _Chd2Age; }
            set { _Chd2Age = value; }
        }

        Int16? _Chd3Age;
        public Int16? Chd3Age
        {
            get { return _Chd3Age; }
            set { _Chd3Age = value; }
        }

        Int16? _Chd4Age;
        public Int16? Chd4Age
        {
            get { return _Chd4Age; }
            set { _Chd4Age = value; }
        }
    }

    public class SearchCriteria
    {
        public SearchCriteria()
        {
            _RoomsInfo = new List<SearchCriteriaRooms>();
            _ShowStopSaleHotels = true;
            _ExtAllotControl = false;
            _ShowExpandDay = true;
            _MaxExpendDayForPriceSearch = 7;
            _ShowSecondDay = true;
            _ShowAgencyEB = false;
            _DefaultExpandDate = 0;
        }

        SearchType _SType;
        public SearchType SType
        {
            get { return _SType; }
            set { _SType = value; }
        }

        DateTime _CheckIn;
        public DateTime CheckIn
        {
            get { return _CheckIn; }
            set { _CheckIn = value; }
        }

        Int16 _ExpandDate;
        public Int16 ExpandDate
        {
            get { return _ExpandDate; }
            set { _ExpandDate = value; }
        }

        Int16 _DefaultExpandDate;
        public Int16 DefaultExpandDate
        {
            get { return _DefaultExpandDate; }
            set { _DefaultExpandDate = value; }
        }

        Int16 _NightFrom;
        public Int16 NightFrom
        {
            get { return _NightFrom; }
            set { _NightFrom = value; }
        }

        Int16 _NightTo;
        public Int16 NightTo
        {
            get { return _NightTo; }
            set { _NightTo = value; }
        }

        Int16 _RoomCount;
        public Int16 RoomCount
        {
            get { return _RoomCount; }
            set { _RoomCount = value; }
        }

        List<SearchCriteriaRooms> _RoomsInfo;
        public List<SearchCriteriaRooms> RoomsInfo
        {
            get { return _RoomsInfo; }
            set { _RoomsInfo = value; }
        }

        int? _Country;
        public int? Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _Resort;
        public string Resort
        {
            get { return _Resort; }
            set { _Resort = value; }
        }

        string _Direction;
        public string Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _HolPackCat;
        public Int16? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _Package;
        public string Package
        {
            get { return _Package; }
            set { _Package = value; }
        }

        string _Facility;
        public string Facility
        {
            get { return _Facility; }
            set { _Facility = value; }
        }

        decimal? _PriceFrom;
        public decimal? PriceFrom
        {
            get { return _PriceFrom; }
            set { _PriceFrom = value; }
        }

        decimal? _PriceTo;
        public decimal? PriceTo
        {
            get { return _PriceTo; }
            set { _PriceTo = value; }
        }

        bool _CurControl;
        public bool CurControl
        {
            get { return _CurControl; }
            set { _CurControl = value; }
        }

        string _CurrentCur;
        public string CurrentCur
        {
            get { return _CurrentCur; }
            set { _CurrentCur = value; }
        }

        bool _ShowAvailable;
        public bool ShowAvailable
        {
            get { return _ShowAvailable; }
            set { _ShowAvailable = value; }
        }

        bool _isRT;
        public bool IsRT
        {
            get { return _isRT; }
            set { _isRT = value; }
        }

        bool _UseGroup;
        public bool UseGroup
        {
            get { return _UseGroup; }
            set { _UseGroup = value; }
        }

        bool _b2bAllClass;
        public bool B2bAllClass
        {
            get { return _b2bAllClass; }
            set { _b2bAllClass = value; }
        }

        string _b2bClass;
        public string B2bClass
        {
            get { return _b2bClass; }
            set { _b2bClass = value; }
        }

        string _SClass;
        public string SClass
        {
            get { return _SClass; }
            set { _SClass = value; }
        }

        int _FromRecord;
        public int FromRecord
        {
            get { return _FromRecord; }
            set { _FromRecord = value; }
        }

        int _ToRecord;
        public int ToRecord
        {
            get { return _ToRecord; }
            set { _ToRecord = value; }
        }

        bool _ShowStopSaleHotels;
        public bool ShowStopSaleHotels
        {
            get { return _ShowStopSaleHotels; }
            set { _ShowStopSaleHotels = value; }
        }

        bool _ExtAllotControl;
        public bool ExtAllotControl
        {
            get { return _ExtAllotControl; }
            set { _ExtAllotControl = value; }
        }

        bool _ShowExpandDay;
        public bool ShowExpandDay
        {
            get { return _ShowExpandDay; }
            set { _ShowExpandDay = value; }
        }

        int? _MaxExpendDayForPriceSearch;
        public int? MaxExpendDayForPriceSearch
        {
            get { return _MaxExpendDayForPriceSearch; }
            set { _MaxExpendDayForPriceSearch = value; }
        }

        bool _ShowSecondDay;
        public bool ShowSecondDay
        {
            get { return _ShowSecondDay; }
            set { _ShowSecondDay = value; }
        }

        bool _ShowAgencyEB;
        public bool ShowAgencyEB
        {
            get { return _ShowAgencyEB; }
            set { _ShowAgencyEB = value; }
        }

        string _B2BMenuCat;
        public string B2BMenuCat
        {
            get { return _B2BMenuCat; }
            set { _B2BMenuCat = value; }
        }

        List<calendarColor> _FlightDays;
        public List<calendarColor> FlightDays
        {
            get { return _FlightDays; }
            set { _FlightDays = value; }
        }

        string _PromotionCode;
        public string PromotionCode
        {
            get { return _PromotionCode; }
            set { _PromotionCode = value; }
        }
    }

    [Serializable]
    public class TitleAgeRecord
    {
        public TitleAgeRecord()
        {
            _Adult = true;
        }

        Int16 _TitleNo;
        public Int16 TitleNo
        {
            get { return _TitleNo; }
            set { _TitleNo = value; }
        }

        decimal? _Age1;
        public decimal? Age1
        {
            get { return _Age1; }
            set { _Age1 = value; }
        }

        decimal? _Age2;
        public decimal? Age2
        {
            get { return _Age2; }
            set { _Age2 = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        bool _Adult;
        public bool Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

    }

    [Serializable]
    public class TitleRecord
    {
        public TitleRecord()
        {
        }

        Int16 _TitleNo;
        public Int16 TitleNo
        {
            get { return _TitleNo; }
            set { _TitleNo = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        Int16 _KeyNo;
        public Int16 KeyNo
        {
            get { return _KeyNo; }
            set { _KeyNo = value; }
        }

        string _Sex;
        public string Sex
        {
            get { return _Sex; }
            set { _Sex = value; }
        }

        bool _Enable;
        public bool Enable
        {
            get { return _Enable; }
            set { _Enable = value; }
        }

        string _Explanation;
        public string Explanation
        {
            get { return _Explanation; }
            set { _Explanation = value; }
        }

        string _IntCode;
        public string IntCode
        {
            get { return _IntCode; }
            set { _IntCode = value; }
        }

    }

    [Serializable]
    public class JSonStruc
    {
        public JSonStruc()
        {
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Value;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

    }

    [Serializable]
    public class SaleServiceRecord
    {
        public SaleServiceRecord()
        {
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _WebSale;
        public string WebSale
        {
            get { return _WebSale; }
            set { _WebSale = value; }
        }

        bool? _B2BIndPack;
        public bool? B2BIndPack
        {
            get { return _B2BIndPack; }
            set { _B2BIndPack = value; }
        }

    }

    public class ServiceRecord
    {
        public ServiceRecord()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        Int16? _DispOrd;
        public Int16? DispOrd
        {
            get { return _DispOrd; }
            set { _DispOrd = value; }
        }

        string _Type;
        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        bool? _ResSeparate;
        public bool? ResSeparate
        {
            get { return _ResSeparate; }
            set { _ResSeparate = value; }
        }

    }

    public class Integer
    {
        public Integer()
        {
        }

        Int32 _Value;
        public Int32 Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

    }

    public class HandicapsRecord
    {
        public HandicapsRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Type;
        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        bool? _PrtVoucher;
        public bool? PrtVoucher
        {
            get { return _PrtVoucher; }
            set { _PrtVoucher = value; }
        }

        bool? _ForPackage;
        public bool? ForPackage
        {
            get { return _ForPackage; }
            set { _ForPackage = value; }
        }

        bool? _ForOnlyFlight;
        public bool? ForOnlyFlight
        {
            get { return _ForOnlyFlight; }
            set { _ForOnlyFlight = value; }
        }
    }

    [Serializable]
    public class PhoneMaskRecord
    {
        public PhoneMaskRecord()
        {
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _CountryCode;
        public string CountryCode
        {
            get { return _CountryCode; }
            set { _CountryCode = value; }
        }

        string _MobilPhoneMask;
        public string MobilPhoneMask
        {
            get { return _MobilPhoneMask; }
            set { _MobilPhoneMask = value; }
        }

        string _PhoneMask;
        public string PhoneMask
        {
            get { return _PhoneMask; }
            set { _PhoneMask = value; }
        }

    }

    public class PayTypeRecord
    {
        public PayTypeRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        Int16? _RateType;
        public Int16? RateType
        {
            get { return _RateType; }
            set { _RateType = value; }
        }

        Int16? _ExChgDate;
        public Int16? ExChgDate
        {
            get { return _ExChgDate; }
            set { _ExChgDate = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankName;
        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }

        string _CanReceiptPrt;
        public string CanReceiptPrt
        {
            get { return _CanReceiptPrt; }
            set { _CanReceiptPrt = value; }
        }

        Int16? _PayCat;
        public Int16? PayCat
        {
            get { return _PayCat; }
            set { _PayCat = value; }
        }

        string _PayCatName;
        public string PayCatName
        {
            get { return _PayCatName; }
            set { _PayCatName = value; }
        }

        int? _OprBankID;
        public int? OprBankID
        {
            get { return _OprBankID; }
            set { _OprBankID = value; }
        }

        DateTime? _PayBegDate;
        public DateTime? PayBegDate
        {
            get { return _PayBegDate; }
            set { _PayBegDate = value; }
        }

        DateTime? _PayEndDate;
        public DateTime? PayEndDate
        {
            get { return _PayEndDate; }
            set { _PayEndDate = value; }
        }

        DateTime? _ResBegDate;
        public DateTime? ResBegDate
        {
            get { return _ResBegDate; }
            set { _ResBegDate = value; }
        }

        DateTime? _ResEndDate;
        public DateTime? ResEndDate
        {
            get { return _ResEndDate; }
            set { _ResEndDate = value; }
        }

        Int16? _MinNight;
        public Int16? MinNight
        {
            get { return _MinNight; }
            set { _MinNight = value; }
        }

        Int16? _MaxNight;
        public Int16? MaxNight
        {
            get { return _MaxNight; }
            set { _MaxNight = value; }
        }

        bool? _HasInstalment;
        public bool? HasInstalment
        {
            get { return _HasInstalment; }
            set { _HasInstalment = value; }
        }

        string _CreditCard;
        public string CreditCard
        {
            get { return _CreditCard; }
            set { _CreditCard = value; }
        }

        Int16? _InstalNumFrom;
        public Int16? InstalNumFrom
        {
            get { return _InstalNumFrom; }
            set { _InstalNumFrom = value; }
        }

        Int16? _InstalNumTo;
        public Int16? InstalNumTo
        {
            get { return _InstalNumTo; }
            set { _InstalNumTo = value; }
        }

        decimal? _DiscountPer;
        public decimal? DiscountPer
        {
            get { return _DiscountPer; }
            set { _DiscountPer = value; }
        }

        decimal? _BankComPer;
        public decimal? BankComPer
        {
            get { return _BankComPer; }
            set { _BankComPer = value; }
        }

        Int16? _PosType;
        public Int16? PosType
        {
            get { return _PosType; }
            set { _PosType = value; }
        }

        DateTime? _EndofDay;
        public DateTime? EndofDay
        {
            get { return _EndofDay; }
            set { _EndofDay = value; }
        }

        Int16? _PeriodofInstalment;
        public Int16? PeriodofInstalment
        {
            get { return _PeriodofInstalment; }
            set { _PeriodofInstalment = value; }
        }

        string _SupDisCode;
        public string SupDisCode
        {
            get { return _SupDisCode; }
            set { _SupDisCode = value; }
        }

        Int16? _ValorDay;
        public Int16? ValorDay
        {
            get { return _ValorDay; }
            set { _ValorDay = value; }
        }

        string _PayCur;
        public string PayCur
        {
            get { return _PayCur; }
            set { _PayCur = value; }
        }

        decimal? _ResPayMinPer;
        public decimal? ResPayMinPer
        {
            get { return _ResPayMinPer; }
            set { _ResPayMinPer = value; }
        }

        string _SD;
        public string SD
        {
            get { return _SD; }
            set { _SD = value; }
        }

        bool? _AllowWIS;
        public bool? AllowWIS
        {
            get { return _AllowWIS; }
            set { _AllowWIS = value; }
        }

        decimal? _ResPayMinVal;
        public decimal? ResPayMinVal
        {
            get { return _ResPayMinVal; }
            set { _ResPayMinVal = value; }
        }

        string _PayPalAPIUserName;
        public string PayPalAPIUserName
        {
            get { return _PayPalAPIUserName; }
            set { _PayPalAPIUserName = value; }
        }

        string _PayPalAPIPwd;
        public string PayPalAPIPwd
        {
            get { return _PayPalAPIPwd; }
            set { _PayPalAPIPwd = value; }
        }

        string _PayPalAPISignature;
        public string PayPalAPISignature
        {
            get { return _PayPalAPISignature; }
            set { _PayPalAPISignature = value; }
        }

        string _PayPalSecureMerchantId;
        public string PayPalSecureMerchantId
        {
            get { return _PayPalSecureMerchantId; }
            set { _PayPalSecureMerchantId = value; }
        }

        string _PayPalMerchantName;
        public string PayPalMerchantName
        {
            get { return _PayPalMerchantName; }
            set { _PayPalMerchantName = value; }
        }

        Int16? _ValidRemDaytoCin;
        public Int16? ValidRemDaytoCin
        {
            get { return _ValidRemDaytoCin; }
            set { _ValidRemDaytoCin = value; }
        }

        bool? _ValidForB2B;
        public bool? ValidForB2B
        {
            get { return _ValidForB2B; }
            set { _ValidForB2B = value; }
        }

        bool? _ValidForB2C;
        public bool? ValidForB2C
        {
            get { return _ValidForB2C; }
            set { _ValidForB2C = value; }
        }

        decimal? _DiscountVal;
        public decimal? DiscountVal
        {
            get { return _DiscountVal; }
            set { _DiscountVal = value; }
        }

        string _SpecCode;
        public string SpecCode
        {
            get { return _SpecCode; }
            set { _SpecCode = value; }
        }

    }

    public class MarketRecord
    {
        public MarketRecord()
        {
        }

        Int32 _RecID;
        public Int32 RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Explanation;
        public string Explanation
        {
            get { return _Explanation; }
            set { _Explanation = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        Int32? _PoolAllot;
        public Int32? PoolAllot
        {
            get { return _PoolAllot; }
            set { _PoolAllot = value; }
        }

        Int32? _LangID;
        public Int32? LangID
        {
            get { return _LangID; }
            set { _LangID = value; }
        }

        Int32? _Country;
        public Int32? Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; }
        }

        string _CountryNameL;
        public string CountryNameL
        {
            get { return _CountryNameL; }
            set { _CountryNameL = value; }
        }

    }

    public class OfficeRecord
    {
        public OfficeRecord()
        {
        }

        int? _RecID; public int? RecID { get { return _RecID; } set { _RecID = value; } }
        string _Code; public string Code { get { return _Code; } set { _Code = value; } }
        string _Name; public string Name { get { return _Name; } set { _Name = value; } }
        string _NameL; public string NameL { get { return _NameL; } set { _NameL = value; } }
        string _NameS; public string NameS { get { return _NameS; } set { _NameS = value; } }
        int? _Location; public int? Location { get { return _Location; } set { _Location = value; } }
        string _LocationName; public string LocationName { get { return _LocationName; } set { _LocationName = value; } }
        string _Operator; public string Operator { get { return _Operator; } set { _Operator = value; } }
        string _Market; public string Market { get { return _Market; } set { _Market = value; } }
        string _Description; public string Description { get { return _Description; } set { _Description = value; } }
        string _ResEntry; public string ResEntry { get { return _ResEntry; } set { _ResEntry = value; } }
        Int16? _ResNoOpt; public Int16? ResNoOpt { get { return _ResNoOpt; } set { _ResNoOpt = value; } }
        int? _BegResNo; public int? BegResNo { get { return _BegResNo; } set { _BegResNo = value; } }
        int? _EndResNo; public int? EndResNo { get { return _EndResNo; } set { _EndResNo = value; } }
        string _DefPrinter; public string DefPrinter { get { return _DefPrinter; } set { _DefPrinter = value; } }
        string _DefFaxPrinter; public string DefFaxPrinter { get { return _DefFaxPrinter; } set { _DefFaxPrinter = value; } }
        string _InsurPrinter; public string InsurPrinter { get { return _InsurPrinter; } set { _InsurPrinter = value; } }
        string _TicketPrinter; public string TicketPrinter { get { return _TicketPrinter; } set { _TicketPrinter = value; } }
        string _VoucherPrinter; public string VoucherPrinter { get { return _VoucherPrinter; } set { _VoucherPrinter = value; } }
        string _UseOprMarket; public string UseOprMarket { get { return _UseOprMarket; } set { _UseOprMarket = value; } }
        string _ResSerie; public string ResSerie { get { return _ResSerie; } set { _ResSerie = value; } }
        string _InvoicePrinter; public string InvoicePrinter { get { return _InvoicePrinter; } set { _InvoicePrinter = value; } }
        string _AutoInvoiceNo; public string AutoInvoiceNo { get { return _AutoInvoiceNo; } set { _AutoInvoiceNo = value; } }
        string _InvoiceSerial; public string InvoiceSerial { get { return _InvoiceSerial; } set { _InvoiceSerial = value; } }
        int? _InvoiceNoFrom; public int? InvoiceNoFrom { get { return _InvoiceNoFrom; } set { _InvoiceNoFrom = value; } }
        int? _InvoiceNoTo; public int? InvoiceNoTo { get { return _InvoiceNoTo; } set { _InvoiceNoTo = value; } }
        int? _NameLID; public int? NameLID { get { return _NameLID; } set { _NameLID = value; } }
        string _AutoReceiptNo; public string AutoReceiptNo { get { return _AutoReceiptNo; } set { _AutoReceiptNo = value; } }
        string _ReceiptSerial; public string ReceiptSerial { get { return _ReceiptSerial; } set { _ReceiptSerial = value; } }
        int? _ReceiptNoFrom; public int? ReceiptNoFrom { get { return _ReceiptNoFrom; } set { _ReceiptNoFrom = value; } }
        int? _ReceiptNoTo; public int? ReceiptNoTo { get { return _ReceiptNoTo; } set { _ReceiptNoTo = value; } }
        string _Address; public string Address { get { return _Address; } set { _Address = value; } }
        string _AddrZip; public string AddrZip { get { return _AddrZip; } set { _AddrZip = value; } }
        string _AddrCity; public string AddrCity { get { return _AddrCity; } set { _AddrCity = value; } }
        string _AddrCountry; public string AddrCountry { get { return _AddrCountry; } set { _AddrCountry = value; } }
        string _ContName; public string ContName { get { return _ContName; } set { _ContName = value; } }
        string _Phone1; public string Phone1 { get { return _Phone1; } set { _Phone1 = value; } }
        string _Phone2; public string Phone2 { get { return _Phone2; } set { _Phone2 = value; } }
        string _MobPhone; public string MobPhone { get { return _MobPhone; } set { _MobPhone = value; } }
        string _Fax1; public string Fax1 { get { return _Fax1; } set { _Fax1 = value; } }
        string _Fax2; public string Fax2 { get { return _Fax2; } set { _Fax2 = value; } }
        string _www; public string www { get { return _www; } set { _www = value; } }
        string _email1; public string email1 { get { return _email1; } set { _email1 = value; } }
        string _email2; public string email2 { get { return _email2; } set { _email2 = value; } }
        string _ReceiptPrinter; public string ReceiptPrinter { get { return _ReceiptPrinter; } set { _ReceiptPrinter = value; } }
    }

    public class OperatorRecord
    {
        public OperatorRecord()
        {

        }

        Int32? _RecID;
        public Int32? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        string _MainOperator;
        public string MainOperator
        {
            get { return _MainOperator; }
            set { _MainOperator = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        Int32? _Location;
        public Int32? Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _LocationNameL;
        public string LocationNameL
        {
            get { return _LocationNameL; }
            set { _LocationNameL = value; }
        }

        string _FirmName;
        public string FirmName
        {
            get { return _FirmName; }
            set { _FirmName = value; }
        }

        string _Address;
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        string _InvAddress;
        public string InvAddress
        {
            get { return _InvAddress; }
            set { _InvAddress = value; }
        }

        string _TaxOffice;
        public string TaxOffice
        {
            get { return _TaxOffice; }
            set { _TaxOffice = value; }
        }

        string _TaxAccNo;
        public string TaxAccNo
        {
            get { return _TaxAccNo; }
            set { _TaxAccNo = value; }
        }

        string _BlackList;
        public string BlackList
        {
            get { return _BlackList; }
            set { _BlackList = value; }
        }

        string _Bank1;
        public string Bank1
        {
            get { return _Bank1; }
            set { _Bank1 = value; }
        }

        string _Bank1BankNo;
        public string Bank1BankNo
        {
            get { return _Bank1BankNo; }
            set { _Bank1BankNo = value; }
        }

        string _Bank1AccNo;
        public string Bank1AccNo
        {
            get { return _Bank1AccNo; }
            set { _Bank1AccNo = value; }
        }

        string _Bank1Curr;
        public string Bank1Curr
        {
            get { return _Bank1Curr; }
            set { _Bank1Curr = value; }
        }

        string _Bank1IBAN;
        public string Bank1IBAN
        {
            get { return _Bank1IBAN; }
            set { _Bank1IBAN = value; }
        }

        string _Bank2;
        public string Bank2
        {
            get { return _Bank2; }
            set { _Bank2 = value; }
        }

        string _Bank2BankNo;
        public string Bank2BankNo
        {
            get { return _Bank2BankNo; }
            set { _Bank2BankNo = value; }
        }

        string _Bank2AccNo;
        public string Bank2AccNo
        {
            get { return _Bank2AccNo; }
            set { _Bank2AccNo = value; }
        }

        string _Bank2Curr;
        public string Bank2Curr
        {
            get { return _Bank2Curr; }
            set { _Bank2Curr = value; }
        }

        string _Bank2IBAN;
        public string Bank2IBAN
        {
            get { return _Bank2IBAN; }
            set { _Bank2IBAN = value; }
        }

        string _BossName;
        public string BossName
        {
            get { return _BossName; }
            set { _BossName = value; }
        }

        string _ContName;
        public string ContName
        {
            get { return _ContName; }
            set { _ContName = value; }
        }

        string _ACContacName;
        public string ACContacName
        {
            get { return _ACContacName; }
            set { _ACContacName = value; }
        }

        string _Phone1;
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        string _Phone2;
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }

        string _MobPhone;
        public string MobPhone
        {
            get { return _MobPhone; }
            set { _MobPhone = value; }
        }

        string _Fax1;
        public string Fax1
        {
            get { return _Fax1; }
            set { _Fax1 = value; }
        }

        string _Fax2;
        public string Fax2
        {
            get { return _Fax2; }
            set { _Fax2 = value; }
        }

        string _www;
        public string Www
        {
            get { return _www; }
            set { _www = value; }
        }

        string _email1;
        public string Email1
        {
            get { return _email1; }
            set { _email1 = value; }
        }

        string _email2;
        public string Email2
        {
            get { return _email2; }
            set { _email2 = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        string _ResSerie;
        public string ResSerie
        {
            get { return _ResSerie; }
            set { _ResSerie = value; }
        }

        string _RegisterCode;
        public string RegisterCode
        {
            get { return _RegisterCode; }
            set { _RegisterCode = value; }
        }

        string _AddrZip;
        public string AddrZip
        {
            get { return _AddrZip; }
            set { _AddrZip = value; }
        }

        string _AddrCity;
        public string AddrCity
        {
            get { return _AddrCity; }
            set { _AddrCity = value; }
        }

        string _AddrCountry;
        public string AddrCountry
        {
            get { return _AddrCountry; }
            set { _AddrCountry = value; }
        }

        string _InvAddrZip;
        public string InvAddrZip
        {
            get { return _InvAddrZip; }
            set { _InvAddrZip = value; }
        }

        string _InvAddrCity;
        public string InvAddrCity
        {
            get { return _InvAddrCity; }
            set { _InvAddrCity = value; }
        }

        string _InvAddrCountry;
        public string InvAddrCountry
        {
            get { return _InvAddrCountry; }
            set { _InvAddrCountry = value; }
        }

        string _InsSupplierName;
        public string InsSupplierName
        {
            get { return _InsSupplierName; }
            set { _InsSupplierName = value; }
        }

        string _InsPolicyName;
        public string InsPolicyName
        {
            get { return _InsPolicyName; }
            set { _InsPolicyName = value; }
        }

        string _InsPolicyNo;
        public string InsPolicyNo
        {
            get { return _InsPolicyNo; }
            set { _InsPolicyNo = value; }
        }

        DateTime? _InsIssuedDate;
        public DateTime? InsIssuedDate
        {
            get { return _InsIssuedDate; }
            set { _InsIssuedDate = value; }
        }

        DateTime? _InsExpDate;
        public DateTime? InsExpDate
        {
            get { return _InsExpDate; }
            set { _InsExpDate = value; }
        }

        string _CompanyNo;
        public string CompanyNo
        {
            get { return _CompanyNo; }
            set { _CompanyNo = value; }
        }

        string _Bank1AccId;
        public string Bank1AccId
        {
            get { return _Bank1AccId; }
            set { _Bank1AccId = value; }
        }

        string _Bank2AccId;
        public string Bank2AccId
        {
            get { return _Bank2AccId; }
            set { _Bank2AccId = value; }
        }

        string _CIF;
        public string CIF
        {
            get { return _CIF; }
            set { _CIF = value; }
        }

        string _CapSocial;
        public string CapSocial
        {
            get { return _CapSocial; }
            set { _CapSocial = value; }
        }

        string _CapSocialCur;
        public string CapSocialCur
        {
            get { return _CapSocialCur; }
            set { _CapSocialCur = value; }
        }

        string _LicenseNo;
        public string LicenseNo
        {
            get { return _LicenseNo; }
            set { _LicenseNo = value; }
        }

        bool? _AgencyCanDisCom;
        public bool? AgencyCanDisCom
        {
            get { return _AgencyCanDisCom; }
            set { _AgencyCanDisCom = value; }
        }
    }

    public class ReadyCurrencyRecord
    {
        public ReadyCurrencyRecord()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        decimal? _RateIsReady;
        public decimal? RateIsReady
        {
            get { return _RateIsReady; }
            set { _RateIsReady = value; }
        }
    }

    public class MarketConfigCurr
    {
        public MarketConfigCurr()
        {
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Currency;
        public string Currency
        {
            get { return _Currency; }
            set { _Currency = value; }
        }
    }

    public class regionRecord
    {
        public regionRecord()
        {
        }

        string _Region;
        public string Region
        {
            get { return _Region; }
            set { _Region = value; }
        }

        string _Year;
        public string Year
        {
            get { return _Year; }
            set { _Year = value; }
        }

        string _Month;
        public string Month
        {
            get { return _Month; }
            set { _Month = value; }
        }

        string _Day;
        public string Day
        {
            get { return _Day; }
            set { _Day = value; }
        }
    }

    public class AgencyCommisionListRecord
    {
        public AgencyCommisionListRecord()
        {
        }

        string _AgencyCode;
        public string AgencyCode
        {
            get { return _AgencyCode; }
            set { _AgencyCode=value; }
        }

        DateTime? _SaleDate;
        public DateTime? SaleDate
        {
            get { return _SaleDate; }
            set { _SaleDate = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        string _Currency;
        public string Currency
        {
            get { return _Currency; }
            set { _Currency = value; }
        }

        decimal? _Debit;
        public decimal? Debit
        {
            get { return _Debit; }
            set { _Debit = value; }
        }

        decimal? _DebitCurr;
        public decimal? DebitCurr
        {
            get { return _DebitCurr; }
            set { _DebitCurr = value; }
        }

        decimal? _Commission;
        public decimal? Commission
        {
            get { return _Commission; }
            set { _Commission = value; }
        }

        decimal? _BrokerCommission;
        public decimal? BrokerCommission
        {
            get { return _BrokerCommission; }
            set { _BrokerCommission = value; }
        }

        decimal? _CommissionCurr;
        public decimal? CommissionCurr
        {
            get { return _CommissionCurr; }
            set { _CommissionCurr = value; }
        }

        decimal? _Payment;
        public decimal? Payment
        {
            get { return _Payment; }
            set { _Payment = value; }
        }

        decimal? _PaymentCurr;
        public decimal? PaymentCurr
        {
            get { return _PaymentCurr; }
            set { _PaymentCurr = value; }
        }

        decimal? _Balance;
        public decimal? Balance
        {
            get { return _Balance; }
            set { _Balance = value; }
        }

        decimal? _BalanceCurr;
        public decimal? BalanceCurr
        {
            get { return _BalanceCurr; }
            set { _BalanceCurr = value; }
        }

        int? _Adult;
        public int? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        int? _Child;
        public int? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        int? _Pax;
        public int? Pax
        {
            get { return _Pax; }
            set { _Pax = value; }
        }
    }

    [Serializable]
    public class IntCountryListRecord
    {
        public IntCountryListRecord()
        {
        }

        string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; }
        }

        string _CountryNameL;
        public string CountryNameL
        {
            get { return _CountryNameL; }
            set { _CountryNameL = value; }
        }

        string _Nationality;
        public string Nationality
        {
            get { return _Nationality; }
            set { _Nationality = value; }
        }

        string _NationalityName;
        public string NationalityName
        {
            get { return _NationalityName; }
            set { _NationalityName = value; }
        }

        string _IntCode;
        public string IntCode
        {
            get { return _IntCode; }
            set { _IntCode = value; }
        }

        string _CountryPhoneCode;
        public string CountryPhoneCode
        {
            get { return _CountryPhoneCode; }
            set { _CountryPhoneCode = value; }
        }

        string _PhoneMask;
        public string PhoneMask
        {
            get { return _PhoneMask; }
            set { _PhoneMask = value; }
        }

        string _PhoneMaskMob;
        public string PhoneMaskMob
        {
            get { return _PhoneMaskMob; }
            set { _PhoneMaskMob = value; }
        }

    }

    public class FixNotesRecord
    {
        public FixNotesRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Section;
        public string Section
        {
            get { return _Section; }
            set { _Section = value; }
        }

        byte? _Type;
        public byte? Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
    }

    public class ContractTextRecord
    {
        public ContractTextRecord()
        {
            _ContractDocType = "";
        }

        string _Code;
        public string Code { get { return _Code; } set { _Code = value; } }
        string _UseContText;
        public string UseContText { get { return _UseContText; } set { _UseContText = value; } }
        string _ContractText;
        public string ContractText { get { return _ContractText; } set { _ContractText = value; } }
        object _ContractPict;
        public object ContractPict { get { return _ContractPict; } set { _ContractPict = value; } }
        string _ContractHtml;
        public string ContractHtml { get { return _ContractHtml; } set { _ContractHtml = value; } }
        string _ContractDocType;
        public string ContractDocType { get { return _ContractDocType; } set { _ContractDocType = value; } }
        public DateTime? ChgDate { get; set; }
    }

    public class HolPackRecord
    {
        public HolPackRecord()
        {
        }

        int? _RecID; public int? RecID { get { return _RecID; } set { _RecID = value; } }
        string _Code; public string Code { get { return _Code; } set { _Code = value; } }
        string _Name; public string Name { get { return _Name; } set { _Name = value; } }
        string _NameL; public string NameL { get { return _NameL; } set { _NameL = value; } }
        string _NameS; public string NameS { get { return _NameS; } set { _NameS = value; } }
        int? _DepCity; public int? DepCity { get { return _DepCity; } set { _DepCity = value; } }
        DateTime? _BegDate; public DateTime? BegDate { get { return _BegDate; } set { _BegDate = value; } }
        DateTime? _EndDate; public DateTime? EndDate { get { return _EndDate; } set { _EndDate = value; } }
        string _Status; public string Status { get { return _Status; } set { _Status = value; } }
        string _Ready; public string Ready { get { return _Ready; } set { _Ready = value; } }
        string _webpub; public string webpub { get { return _webpub; } set { _webpub = value; } }
        string _Description; public string Description { get { return _Description; } set { _Description = value; } }
        string _SaleCur; public string SaleCur { get { return _SaleCur; } set { _SaleCur = value; } }
        Int16? _UseDates; public Int16? UseDates { get { return _UseDates; } set { _UseDates = value; } }
        string _AccomNights; public string AccomNights { get { return _AccomNights; } set { _AccomNights = value; } }
        string _PackType; public string PackType { get { return _PackType; } set { _PackType = value; } }
        string _FlightSeat; public string FlightSeat { get { return _FlightSeat; } set { _FlightSeat = value; } }
        string _CalcType; public string CalcType { get { return _CalcType; } set { _CalcType = value; } }
        Int16? _FixCinType; public Int16? FixCinType { get { return _FixCinType; } set { _FixCinType = value; } }
        string _FixCinDay; public string FixCinDay { get { return _FixCinDay; } set { _FixCinDay = value; } }
        decimal? _AgencyCom; public decimal? AgencyCom { get { return _AgencyCom; } set { _AgencyCom = value; } }
        string _EBValid; public string EBValid { get { return _EBValid; } set { _EBValid = value; } }
        int? _ArrCity; public int? ArrCity { get { return _ArrCity; } set { _ArrCity = value; } }
        string _Market; public string Market { get { return _Market; } set { _Market = value; } }
        int? _NamelID; public int? NamelID { get { return _NamelID; } set { _NamelID = value; } }
        string _AccountNo1; public string AccountNo1 { get { return _AccountNo1; } set { _AccountNo1 = value; } }
        string _AccountNo2; public string AccountNo2 { get { return _AccountNo2; } set { _AccountNo2 = value; } }
        string _AccountNo3; public string AccountNo3 { get { return _AccountNo3; } set { _AccountNo3 = value; } }
        decimal? _TaxPer; public decimal? TaxPer { get { return _TaxPer; } set { _TaxPer = value; } }
        string _PasEBValid; public string PasEBValid { get { return _PasEBValid; } set { _PasEBValid = value; } }
        byte? _PNight; public byte? PNight { get { return _PNight; } set { _PNight = value; } }
        string _RefHotel; public string RefHotel { get { return _RefHotel; } set { _RefHotel = value; } }
        string _RefBoard; public string RefBoard { get { return _RefBoard; } set { _RefBoard = value; } }
        string _RefRoom; public string RefRoom { get { return _RefRoom; } set { _RefRoom = value; } }
        Int16? _Direction; public Int16? Direction { get { return _Direction; } set { _Direction = value; } }
        Int16? _Category; public Int16? Category { get { return _Category; } set { _Category = value; } }
        byte? _TANight; public byte? TANight { get { return _TANight; } set { _TANight = value; } }
        string _OprText; public string OprText { get { return _OprText; } set { _OprText = value; } }
    }

    public class CompulsoryMealRecord
    {
        public CompulsoryMealRecord()
        {
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _CompulsoryMeal;
        public string CompulsoryMeal
        {
            get { return _CompulsoryMeal; }
            set { _CompulsoryMeal = value; }
        }
    }

    public class DocumentEmailSender
    {
        public string Sender { get; set; }
        public int? Priority { get; set; }
        public bool? Enabled { get; set; }
        public DocumentEmailSender()
        {
            this.Enabled = false;
        }
    }

    public class DocReportFormID
    {
        public string Market { get; set; }
        public int? FormID { get; set; }
        public DocReportFormID() { }
    }
}

