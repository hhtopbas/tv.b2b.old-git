﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class Ssrcs
    {
        public List<SpecSerRQCodeRecord> getSpecialRQCode(string Market, string plMarket, ref string errorMsg)
        {
            List<SpecSerRQCodeRecord> records = new List<SpecSerRQCodeRecord>();

            string tsql = @"Select Distinct SsrC.RecID, SsrC.Code, SsrC.Name, NameL=isnull(dbo.FindLocalName(SsrC.NameLID, @Market), SsrC.Name),
                                SsrC.ServiceType, SsrC.Category, Priced=Case When ExtSrv.Code is null Then CAST(0 AS bit) Else CAST(1 AS bit) End,
	                            ExtServicePriceID=ExtSrv.ExtPriceID, ExtService=ExtSrv.Code, ExtSrv.Service,	ExtSrv.SaleAdlPrice, ExtSrv.SaleCur, ExtSrv.PriceType, ExtSrv.CalcType, 
	                            ExtSrv.InfAge1, ExtSrv.InfAge2, ExtSrv.ChdAge1, ExtSrv.ChdAge2, ExtSrv.SaleInfPrice, ExtSrv.SaleChdPrice
                            From SpecSerRQCode SsrC (NOLOCK)
                            Left Join SpecSerRQCodeMarOpt SsrcMo (NOLOCK) ON SsrcMo.MasRecID=Ssrc.RecID And SsrcMo.Market=@plMarket
                            Left Join (
	                            Select SE.RecID, SEP.Service, SE.Code, SE.SpecSerRQCode,
		                            SEP.SaleAdlPrice, SEP.SaleCur, SEP.PriceType, SEP.CalcType, 
		                            SEP.InfAge1, SEP.InfAge2, SEP.ChdAge1, SEP.ChdAge2, SEP.SaleInfPrice, SEP.SaleChdPrice, ExtPriceID=SEP.RecID
	                            From ServiceExt SE (NOLOCK)
	                            Join ServiceExtPrice SEP (NOLOCK) ON SEP.Code=SE.Code				
	                            Where isnull(SE.SpecSerRQCode, '')<>''
                                  And SEP.Market=@plMarket
                            ) ExtSrv ON ExtSrv.SpecSerRQCode=SsrC.Code
                            Where isnull(SsrcMo.WebSale, 1)=1";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        SpecSerRQCodeRecord record = new SpecSerRQCodeRecord();
                        record.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.ServiceType = Conversion.getStrOrNull(oReader["ServiceType"]);
                        record.Category = Conversion.getByteOrNull(oReader["Category"]);
                        record.Priced = Conversion.getBoolOrNull(oReader["Priced"]);
                        record.ExtServicePriceID = Conversion.getInt32OrNull(oReader["ExtServicePriceID"]);
                        record.ExtService = Conversion.getStrOrNull(oReader["ExtService"]);
                        record.Service = Conversion.getStrOrNull(oReader["ExtService"]);
                        record.SaleAdlPrice = Conversion.getDecimalOrNull(oReader["SaleAdlPrice"]);
                        record.SaleCur = Conversion.getStrOrNull(oReader["SaleCur"]);
                        record.PriceType = Conversion.getInt16OrNull(oReader["PriceType"]);
                        record.CalcType = Conversion.getInt16OrNull(oReader["CalcType"]);
                        record.InfAge1 = Conversion.getDecimalOrNull(oReader["InfAge1"]);
                        record.InfAge2 = Conversion.getDecimalOrNull(oReader["InfAge2"]);
                        record.ChdAge1 = Conversion.getDecimalOrNull(oReader["ChdAge1"]);
                        record.ChdAge2 = Conversion.getDecimalOrNull(oReader["ChdAge2"]);
                        record.SaleInfPrice = Conversion.getDecimalOrNull(oReader["SaleInfPrice"]);
                        record.SaleChdPrice = Conversion.getDecimalOrNull(oReader["SaleChdPrice"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public SSRQCodeRecord getSSRQCode(string Market, string plMarket, string SsrC, ref string errorMsg)
        {            
            string tsql =
@"
Select SsrC.RecID,SsrC.Code,SsrC.Name,NameL=isnull(dbo.FindLocalName(SsrC.NameLID,@Market),SsrC.Name),
  SsrC.ServiceType,SsrC.Category,SsrC.NotAllowPlaneExitSeat,SsrC.PnlText
From SpecSerRQCode SsrC (NOLOCK)
Left Join SpecSerRQCodeMarOpt SsrcMo (NOLOCK) ON SsrcMo.MasRecID=Ssrc.RecID And SsrcMo.Market=@plMarket                            
Where isnull(SsrcMo.WebSale, 1)=1
  And SsrC.Code=@SsrC
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "SsrC", DbType.String, SsrC);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        SSRQCodeRecord record = new SSRQCodeRecord();
                        record.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.ServiceType = Conversion.getStrOrNull(oReader["ServiceType"]);
                        record.NotAllowPlaneExitSeat = Conversion.getBoolOrNull(oReader["NotAllowPlaneExitSeat"]);
                        record.Category = Conversion.getByteOrNull(oReader["Category"]);

                        return record;
                    }
                    else
                    {
                        return null;
                    }
                }                
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public SpecSerRQCodeRecord getSpecialRequest(string Market, string plMarket, string ServcieItem, string Code, ref string errorMsg)
        {
            List<SpecSerRQCodeRecord> records = new List<SpecSerRQCodeRecord>();

            string tsql = @"Select Distinct SsrC.RecID, SsrC.Code, SsrC.Name, NameL=isnull(dbo.FindLocalName(SsrC.NameLID, @Market), SsrC.Name),
                                SsrC.ServiceType, SsrC.Category, Priced=Case When ExtSrv.Code is null Then CAST(0 AS bit) Else CAST(1 AS bit) End,
	                            ExtServicePriceID=ExtSrv.ExtPriceID, ExtService=ExtSrv.Code, ExtSrv.Service,	ExtSrv.SaleAdlPrice, ExtSrv.SaleCur, ExtSrv.PriceType, ExtSrv.CalcType, 
	                            ExtSrv.InfAge1, ExtSrv.InfAge2, ExtSrv.ChdAge1, ExtSrv.ChdAge2, ExtSrv.SaleInfPrice, ExtSrv.SaleChdPrice
                            From SpecSerRQCode SsrC (NOLOCK)
                            Left Join SpecSerRQCodeMarOpt SsrcMo (NOLOCK) ON SsrcMo.MasRecID=Ssrc.RecID And SsrcMo.Market=@plMarket
                            Left Join (
	                            Select SE.RecID, SEP.Service, SE.Code, SE.SpecSerRQCode,
                                       SEP.SaleAdlPrice, SEP.SaleCur, SEP.PriceType, SEP.CalcType, 
                                       SEP.InfAge1, SEP.InfAge2, SEP.ChdAge1, SEP.ChdAge2, SEP.SaleInfPrice, SEP.SaleChdPrice, ExtPriceID=SEP.RecID
                                From ServiceExt SE (NOLOCK)
                                Join ServiceExtPrice SEP (NOLOCK) ON SEP.Code=SE.Code				
                                Where isnull(SE.SpecSerRQCode, '')<>'' And SEP.Market=@plMarket And (SEP.ServiceItem='' Or SEP.ServiceItem=@ServiceItem) 
                                  And GETDATE() between SEP.SaleBegDate And SEP.SaleEndDate      
                            ) ExtSrv ON ExtSrv.SpecSerRQCode=SsrC.Code
                            Where isnull(SsrcMo.WebSale, 1)=1
                              And SsrC.Code=@Code";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                db.AddInParameter(dbCommand, "ServiceItem", DbType.String, ServcieItem);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        SpecSerRQCodeRecord record = new SpecSerRQCodeRecord();
                        record.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.ServiceType = Conversion.getStrOrNull(oReader["ServiceType"]);
                        record.Category = Conversion.getByteOrNull(oReader["Category"]);
                        record.Priced = Conversion.getBoolOrNull(oReader["Priced"]);
                        record.ExtServicePriceID = Conversion.getInt32OrNull(oReader["ExtServicePriceID"]);
                        record.ExtService = Conversion.getStrOrNull(oReader["ExtService"]);
                        record.Service = Conversion.getStrOrNull(oReader["ExtService"]);
                        record.SaleAdlPrice = Conversion.getDecimalOrNull(oReader["SaleAdlPrice"]);
                        record.SaleCur = Conversion.getStrOrNull(oReader["SaleCur"]);
                        record.PriceType = Conversion.getInt16OrNull(oReader["PriceType"]);
                        record.CalcType = Conversion.getInt16OrNull(oReader["CalcType"]);
                        record.InfAge1 = Conversion.getDecimalOrNull(oReader["InfAge1"]);
                        record.InfAge2 = Conversion.getDecimalOrNull(oReader["InfAge2"]);
                        record.ChdAge1 = Conversion.getDecimalOrNull(oReader["ChdAge1"]);
                        record.ChdAge2 = Conversion.getDecimalOrNull(oReader["ChdAge2"]);
                        record.SaleInfPrice = Conversion.getDecimalOrNull(oReader["SaleInfPrice"]);
                        record.SaleChdPrice = Conversion.getDecimalOrNull(oReader["SaleChdPrice"]);
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<string> getSsrcServiceTypes(string Market, string plMarket, ref string errorMsg)
        {
            List<SpecSerRQCodeRecord> freeSsrcList = new Ssrcs().getFreeSrrCodes(Market, plMarket, ref errorMsg);
            List<string> records = new List<string>();
            var list = from q in freeSsrcList
                       group q by new { ServiceType = q.ServiceType } into k
                       select new { ServiceType = k.Key.ServiceType };
            return list.Select(s => s.ServiceType).ToList<string>();
        }

        public List<PricedSSRCode> getPricedSrrCodes(string Market, string plMarket, ref string errorMsg)
        {
            List<PricedSSRCode> records = new List<PricedSSRCode>();

            string tsql = @"Select SE.Service, SE.Code, SE.Name, NameL=ISNULL(dbo.FindLocalName(SE.NameLID, @Market), SE.Name),
	                            SE.SpecSerRQCode
                            From ServiceExt SE (NOLOCK)
                            Join ServiceExtPrice SEP (NOLOCK) ON SEP.Code=SE.Code
                            Join SpecSerRQCode SSRC (NOLOCK) ON SSRC.Code=SE.SpecSerRQCode
                            Left Join SpecSerRQCodeMarOpt SsrcMo (NOLOCK) ON SsrcMo.MasRecID=Ssrc.RecID And SsrcMo.Market=@plMarket
                            Where isnull(SsrcMo.WebSale, 1)=1
                              And isnull(SE.SpecSerRQCode, '')<>''
                              And SEP.Market=@plMarket ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        PricedSSRCode record = new PricedSSRCode();
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.SpecSerRQCode = Conversion.getStrOrNull(oReader["SpecSerRQCode"]);

                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SpecSerRQCodeRecord> getFreeSrrCodes(string Market, string plMarket, ref string errorMsg)
        {
            List<SpecSerRQCodeRecord> records = new List<SpecSerRQCodeRecord>();

            string tsql = @"Select Distinct SsrC.RecID, SsrC.Code, SsrC.Name, NameL=isnull(dbo.FindLocalName(SsrC.NameLID, @Market), SsrC.Name),
                                SsrC.ServiceType, SsrC.Category, Priced=Case When ExtSrv.Code is null Then CAST(0 AS bit) Else CAST(1 AS bit) End,
	                            ExtServicePriceID=ExtSrv.ExtPriceID, ExtService=ExtSrv.Code, ExtSrv.Service, ExtSrv.ServiceItem, ExtSrv.SaleAdlPrice, ExtSrv.SaleCur, ExtSrv.PriceType, ExtSrv.CalcType, 
	                            ExtSrv.InfAge1, ExtSrv.InfAge2, ExtSrv.ChdAge1, ExtSrv.ChdAge2, ExtSrv.SaleInfPrice, ExtSrv.SaleChdPrice
                            From SpecSerRQCode SsrC (NOLOCK)
                            Left Join SpecSerRQCodeMarOpt SsrcMo (NOLOCK) ON SsrcMo.MasRecID=Ssrc.RecID And SsrcMo.Market=@plMarket
                            Left Join (
	                            Select SE.RecID, SEP.Service, SEP.ServiceItem, SE.Code, SE.SpecSerRQCode,
		                            SEP.SaleAdlPrice, SEP.SaleCur, SEP.PriceType, SEP.CalcType, 
		                            SEP.InfAge1, SEP.InfAge2, SEP.ChdAge1, SEP.ChdAge2, SEP.SaleInfPrice, SEP.SaleChdPrice, ExtPriceID=SEP.RecID
	                            From ServiceExt SE (NOLOCK)
	                            Join ServiceExtPrice SEP (NOLOCK) ON SEP.Code=SE.Code				
	                            Where isnull(SE.SpecSerRQCode, '')<>''
                                  And SEP.Market=@plMarket
                            ) ExtSrv ON ExtSrv.SpecSerRQCode=SsrC.Code
                            Where isnull(SsrcMo.WebSale, 1)=1";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        SpecSerRQCodeRecord record = new SpecSerRQCodeRecord();
                        record.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.ServiceType = Conversion.getStrOrNull(oReader["ServiceType"]);
                        record.Category = Conversion.getByteOrNull(oReader["Category"]);
                        record.Priced = Conversion.getBoolOrNull(oReader["Priced"]);
                        record.ExtServicePriceID = Conversion.getInt32OrNull(oReader["ExtServicePriceID"]);
                        record.ExtService = Conversion.getStrOrNull(oReader["ExtService"]);
                        record.Service = Conversion.getStrOrNull(oReader["ExtService"]);
                        record.ServiceItem = Conversion.getStrOrNull(oReader["ServiceItem"]);
                        record.SaleAdlPrice = Conversion.getDecimalOrNull(oReader["SaleAdlPrice"]);
                        record.SaleCur = Conversion.getStrOrNull(oReader["SaleCur"]);
                        record.PriceType = Conversion.getInt16OrNull(oReader["PriceType"]);
                        record.CalcType = Conversion.getInt16OrNull(oReader["CalcType"]);
                        record.InfAge1 = Conversion.getDecimalOrNull(oReader["InfAge1"]);
                        record.InfAge2 = Conversion.getDecimalOrNull(oReader["InfAge2"]);
                        record.ChdAge1 = Conversion.getDecimalOrNull(oReader["ChdAge1"]);
                        record.ChdAge2 = Conversion.getDecimalOrNull(oReader["ChdAge2"]);
                        record.SaleInfPrice = Conversion.getDecimalOrNull(oReader["SaleInfPrice"]);
                        record.SaleChdPrice = Conversion.getDecimalOrNull(oReader["SaleChdPrice"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}

