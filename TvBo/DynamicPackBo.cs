﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class DynmcRouteToRecord
    { 
        public DynmcRouteToRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _CityName;
        public string CityName
        {
            get { return _CityName; }
            set { _CityName = value; }
        }

        string _CityNameL;
        public string CityNameL
        {
            get { return _CityNameL; }
            set { _CityNameL = value; }
        }

        int? _Country;
        public int? Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; }
        }

        string _CountryNameL;
        public string CountryNameL
        {
            get { return _CountryNameL; }
            set { _CountryNameL = value; }
        }               
    }
}
