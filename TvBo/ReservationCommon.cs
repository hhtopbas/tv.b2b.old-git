﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using TvTools;

namespace TvBo
{
    public class SelectCustRecord
    {
        public SelectCustRecord()
        {
        }

        int _BookID;
        public int BookID
        {
            get { return _BookID; }
            set { _BookID = value; }
        }

        bool _Selected;
        public bool Selected
        {
            get { return _Selected; }
            set { _Selected = value; }
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        string _Customers;
        public string Customers
        {
            get { return _Customers; }
            set { _Customers = value; }
        }

    }

    public class ReservationStatuRecord
    {
        public ReservationStatuRecord()
        {
            _ExtraService = false;
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        DateTime _BegDate;
        public DateTime BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16 _StatConf;
        public Int16 StatConf
        {
            get { return _StatConf; }
            set { _StatConf = value; }
        }

        DateTime? _StatConfDate;
        public DateTime? StatConfDate
        {
            get { return _StatConfDate; }
            set { _StatConfDate = value; }
        }

        Int16 _StatSer;
        public Int16 StatSer
        {
            get { return _StatSer; }
            set { _StatSer = value; }
        }

        DateTime? _StatSerDate;
        public DateTime? StatSerDate
        {
            get { return _StatSerDate; }
            set { _StatSerDate = value; }
        }

        Int16? _ResMainStat;
        public Int16? ResMainStat
        {
            get { return _ResMainStat; }
            set { _ResMainStat = value; }
        }

        bool _Compulsory;
        public bool Compulsory
        {
            get { return _Compulsory; }
            set { _Compulsory = value; }
        }

        bool _ExtraService;
        public bool ExtraService
        {
            get { return _ExtraService; }
            set { _ExtraService = value; }
        }

    }

    public class ReservationCommon
    {
        /// <summary>
        /// Use Old Version For Haluk
        /// </summary>
        /// <param name="ResData"></param>
        /// <returns></returns>
        public DataTable CreateSelectCustTable(DataSet ResData)
        {
            DataTable dt = new DataTable();
            dt.TableName = "SelectCust";
            dt.Columns.Add("BookID", typeof(int));
            dt.Columns.Add("Select", typeof(bool));
            dt.Columns.Add("CustNo", typeof(int));
            dt.Columns.Add("Customers", typeof(string));
            DataTable Cust = ResData.Tables["ResCust"];

            for (int i = 0; i < Cust.Rows.Count; i++)
            {
                if (Cust.Rows[i]["BookID"].ToString() == "")
                    Cust.Rows[i]["BookID"] = 1;
                dt.Rows.Add(new object[] { Cust.Rows[i]["BookID"].ToString(), false, Cust.Rows[i]["CustNo"], Cust.Rows[i]["Surname"].ToString() + "  " + Cust.Rows[i]["Name"].ToString() });
            }
            return dt;
        }

        public List<SelectCustRecord> CreateSelectCustTableList(ResDataRecord ResData)
        {
            List<SelectCustRecord> SelectCustList = new List<SelectCustRecord>();
            List<ResCustRecord> Cust = ResData.ResCust;

            foreach (ResCustRecord row in Cust)
            {
                if (!row.BookID.HasValue) row.BookID = 1;
                SelectCustList.Add(new SelectCustRecord { BookID = row.BookID.Value, Selected = false, CustNo = row.CustNo, Customers = row.Surname + " " + row.Name });
            }
            return SelectCustList;
        }

        public List<SelectCustRecord> CreateSelectCustTableList(DataSet ResData)
        {
            List<SelectCustRecord> SelectCustList = new List<SelectCustRecord>();
            DataTable Cust = ResData.Tables["ResCust"];

            for (int i = 0; i < Cust.Rows.Count; i++)
            {
                if (string.IsNullOrEmpty(Cust.Rows[i]["BookID"].ToString())) Cust.Rows[i]["BookID"] = 1;
                SelectCustList.Add(new SelectCustRecord { BookID = (int)Cust.Rows[i]["BookID"], Selected = false, CustNo = (int)Cust.Rows[i]["CustNo"], Customers = Cust.Rows[i]["Surname"].ToString() + "  " + Cust.Rows[i]["Name"].ToString() });
            }
            return SelectCustList;
        }

        public List<SelectCustRecord> convertSelectCustDataTableToList(DataTable Cust)
        {
            List<SelectCustRecord> SelectCustList = new List<SelectCustRecord>();
            for (int i = 0; i < Cust.Rows.Count; i++)
            {
                SelectCustList.Add(new SelectCustRecord { BookID = (int)Cust.Rows[i]["BookID"], Selected = (bool)Cust.Rows[i]["Select"], CustNo = (int)Cust.Rows[i]["CustNo"], Customers = Cust.Rows[i]["Customers"].ToString() });
            }
            return SelectCustList;
        }

        public bool CheckReservationUser(User UserData, string ResNo, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql = @"   Select RM.ResNo FROM ResMain RM (NOLOCK)                         
                        Left JOIN AgencyUser AU (NOLOCK) ON AU.Code = RM.AgencyUser AND AU.Agency=RM.Agency 
                        JOIN Agency A (NOLOCK) ON A.Code = RM.Agency 
                        Where ";
            tsql += @" (
                        RM.Agency in (Select Code From Agency (NOLOCK) Where Code = @Agency Or MainOffice = @Agency) 
                        ";
            if ((UserData.CustomRegID == "0860901" || UserData.CustomRegID == "0973001") && UserData.ShowAllRes && UserData.MainOffice != "")
            {
                tsql += @"  Or
                        RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@MainOffice Or MainOffice=@MainOffice)
                        )";
            }
            else
                tsql += ") ";
            if (UserData.Aggregator && !string.IsNullOrEmpty(UserData.MerlinxID) && UserData.MainAgency)
                tsql += @"  Or isnull(RM.MerlinxID, '') <> '' ";

            tsql += @"  And RM.ResNo = @ResNo ";


            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "AgencyUser", DbType.String, UserData.UserID);
                db.AddInParameter(dbCommand, "MainOffice", DbType.String, UserData.MainOffice);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);

                DataSet ds = db.ExecuteDataSet(dbCommand);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ReservationStatuRecord> getReservationStatus(string ResNo, ref string errorMsg)
        {
            List<ReservationStatuRecord> records = new List<ReservationStatuRecord>();
            string tsql = @"Select * From 
                            (
	                            Select RecID, BegDate, EndDate, StatConf, StatConfDate, StatSer, StatSerDate, ResMainStat, Compulsory, ExtraService=CAST(0 AS bit)
	                            From ResService (NOLOCK)
	                            Where ResNo=@ResNo
	                            Union All
	                            Select RecID, BegDate, EndDate, StatConf, StatConfDate, StatSer, StatSerDate, null, Compulsory=(Case When Compulsory='Y' Then CAST(1 AS bit) Else CAST(0 AS bit) End), ExtraService=CAST(1 AS bit)
	                            From ResServiceExt (NOLOCK)
	                            Where ResNo=@ResNo	
                            ) X
                            Order By BegDate Desc, RecID";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ReservationStatuRecord rec = new ReservationStatuRecord();
                        rec.RecID = (int)R["RecID"];
                        rec.BegDate = (DateTime)R["BegDate"];
                        rec.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        rec.StatConf = (Int16)R["StatConf"];
                        rec.StatConfDate = Conversion.getDateTimeOrNull(R["StatConfDate"]);
                        rec.StatSer = (Int16)R["StatSer"];
                        rec.StatSerDate = Conversion.getDateTimeOrNull(R["StatSerDate"]);
                        rec.ResMainStat = Conversion.getInt16OrNull(R["ResMainStat"]);
                        rec.Compulsory = (bool)R["Compulsory"];
                        rec.ExtraService = (bool)R["ExtraService"];
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ReservationStatuRecord> getReservationStatu(string ResNo, ref string errorMsg)
        {
            List<ReservationStatuRecord> records = new List<ReservationStatuRecord>();
            string tsql = @"Select * From (
                             Select RecID,BegDate,EndDate,StatConf,StatConfDate,StatSer,StatSerDate,ResMainStat,Compulsory, 
	                             ExtraService=CAST(0 AS bit)
                             From ResService (NOLOCK)
                             Where ResNo=@ResNo                               
                             UNION ALL
                             Select RecID,BegDate,EndDate,StatConf,StatConfDate,StatSer,StatSerDate,ResMainStat,Compulsory=(Case When Compulsory='Y' then CAST(1 AS bit) else CAST(0 AS bit) end), 
	                             ExtraService=CAST(1 AS bit)
                             From ResServiceExt (NOLOCK)
                             Where ResNo=@ResNo
                            ) X
                            Order By BegDate Desc,RecID
                            ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ReservationStatuRecord rec = new ReservationStatuRecord();
                        rec.RecID = (int)R["RecID"];
                        rec.BegDate = (DateTime)R["BegDate"];
                        rec.EndDate = (DateTime)R["EndDate"];
                        rec.StatConf = (Int16)R["StatConf"];
                        rec.StatConfDate = Conversion.getDateTimeOrNull(R["StatConfDate"]);
                        rec.StatSer = (Int16)R["StatSer"];
                        rec.StatSerDate = Conversion.getDateTimeOrNull(R["StatSerDate"]);
                        rec.ResMainStat = Conversion.getInt16OrNull(R["ResMainStat"]);
                        rec.Compulsory = (bool)R["Compulsory"];
                        rec.ExtraService = (bool)R["ExtraService"];
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        /// <summary>
        /// For Web Service (Haluk için)
        /// </summary>
        /// <param name="UserData"></param>
        /// <param name="ResData"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public DataSet getSaveRealData(User UserData, DataSet ResData, ref string errorMsg)
        {
            StringBuilder CalcStrSql = new StringBuilder();
            CalcStrSql.Append("Select * From ResMain (NOLOCK) Where ResNo=@ResNo \n");
            CalcStrSql.Append("Select * From ResCust (NOLOCK) Where ResNo=@ResNo \n");
            CalcStrSql.Append("Select R.*,f.DepAir DepAirCode,f.ArrAir ArrAirCode,f.Airline AirlineCode From ResService R (NOLOCK) " +
                              " LEFT JOIN Flight F (NOLOCK) ON R.Service=F.Code And R.ServiceType='FLIGHT' Where ResNo=@ResNo \n");
            CalcStrSql.Append("Select * From ResCon (NOLOCK) Where ResNo=@ResNo \n");
            CalcStrSql.Append("Select * From ResServiceExt (NOLOCK) Where ResNo=@ResNo \n");
            CalcStrSql.Append("Select * From ResConExt (NOLOCK) Where ResNo=@ResNo \n");
            CalcStrSql.Append("Select * From ResCustPrice (NOLOCK) Where ResNo=@ResNo \n");
            CalcStrSql.Append("Select * From ResSupDis (NOLOCK) Where ResNo=@ResNo \n");
            CalcStrSql.Append(@" SELECT distinct S.Code,
                                       isnull(dbo.FindLocalName(NameLID, @Market), S.Name) Name,
                                       S.FirmName,
                                       S.[Address],
                                       S.AddrCountry,
                                       S.AddrCity,
                                       S.AddrZip,
                                       S.InvAddress,
                                       S.InvAddrCountry,
                                       s.InvAddrCity,
                                       s.InvAddrZip,
                                       s.Phone1,
                                       s.Fax1,
                                       s.Phone2,
                                       s.Fax2
                                FROM Supplier S(NOLOCK)
                                     JOIN ResService R(NOLOCK) ON S.Code = R.Supplier and R.ResNo=@ResNo ");

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(CalcStrSql.ToString());
            DataSet dsHes;
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResData.Tables["ResMain"].Rows[0]["ResNo"].ToString());
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);

                dsHes = db.ExecuteDataSet(dbCommand);

                if (dsHes != null)
                {
                    dsHes.Tables[0].TableName = "ResMain";
                    dsHes.Tables[1].TableName = "ResCust";
                    dsHes.Tables[2].TableName = "ResService";
                    dsHes.Tables[3].TableName = "ResCon";
                    dsHes.Tables[4].TableName = "ResServiceExt";
                    dsHes.Tables[5].TableName = "ResConExt";
                    dsHes.Tables[6].TableName = "ResCustPrice";
                    dsHes.Tables[7].TableName = "ResSupDis";
                    dsHes.Tables[8].TableName = "Suppliers";
                    return dsHes;
                }
                else
                {
                    errorMsg = "Saved reservation not found.";
                    return ResData;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return ResData;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SaleServiceRecord> getSalableServices(User UserData, ResDataRecord ResData, ref string errorMsg)
        {
            List<SaleServiceRecord> records = new List<SaleServiceRecord>();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0022, VersionControl.Equality.gt))
                tsql = @"Select S.Service,S.Code, Name=S1.Name, NameL=isnull(dbo.FindLocalName(S1.NameLID, @Market), isnull(S1.Name, '')),WebSale,B2BIndPack
                         From (
                               Select Distinct X.Service,X.Code, SMP.WebSale, SMP.B2BIndPack From 
	                            (
		                            Select 'HOTEL' AS Service, Code='HOTEL', Hotel As Name From HotelNetPrice (NOLOCK) 
		                            Union 
		                            Select 'FLIGHT' AS Service, Code='FLIGHT', FlightNo AS Name From FlightPrice (NOLOCK) 
		                            Union 
		                            Select Distinct 'TRANSPORT' AS Service, Code='TRANSPORT', TD.TransPort 
                                    From TransportDay TD (NOLOCK)
		                            Join Transport T (NOLOCK) ON T.Code = TD.Transport
		                            Where Exists(Select RecID From Operator (NOLOCK) Where Market=@Market And Code=T.Operator)
			                          And TransDate >= dbo.DateOnly(GetDate())	                                        
		                            Union 
		                            Select Distinct 'RENTING' As Service, Code='RENTING', Renting As Name From RentingPrice (NOLOCK) 
		                            Where 
                                        Market = @Market 
		                            And dbo.DateOnly(GetDate()) between BegDate And EndDate                                    
		                            Union 
		                            Select 'EXCURSION' As Service, Code='EXCURSION', Excursion As Name From ExcursionPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'INSURANCE' As Service, Code='INSURANCE', Insurance As Name From InsurancePrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'VISA' As Service, Code='VISA', Visa As Name From VisaPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'TRANSFER' As Service, Code='TRANSFER', Transfer As Name From TransferPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'HANDFEE' As Service, Code='HANDFEE', HandFee As Name From HandFeePrice (NOLOCK) Where Market = @Market 
		                            Union
		                            Select P.Service As Service , Code=AdService, 'Additional' As Name From AdServicePrice P (NOLOCK)
		                            Where P.Market=@Market 		  
		                              And P.Compulsory=0
		                              And (Exists(Select * From GrpAgencyDet GAD (NOLOCK) 
					                              Join GrpAgency GA (NOLOCK) ON GA.Code=GAD.Groups
					                              Where GAD.GrpType='P' And Agency=@Agency And Market=@Market
					                              And P.AgencyGrp=GA.Code)
			                               Or P.Agency=@Agency Or (isnull(P.Agency,'')='' And isnull(P.AgencyGrp,'')=''))
	                            ) X 
	                            Left Join ServiceMarOpt SMP (NOLOCK) ON SMP.Service=X.Service And SMP.Market=@Market	
                            ) S
                            Left Join Service S1 (NOLOCK) ON S1.Code=S.Service
                            Order By S.Service ";
            else
                tsql = @"Select S.Service, Name=S1.Name, NameL=isnull(dbo.FindLocalName(S1.NameLID, @Market), isnull(S1.Name, '')),WebSale
                            From (
	                            Select Distinct X.Service, SMP.WebSale From 
	                            (
		                            Select 'HOTEL' AS Service, Code='HOTEL', Hotel As Name From HotelNetPrice (NOLOCK) 
		                            Union 
		                            Select 'FLIGHT' AS Service, Code='FLIGHT', FlightNo AS Name From FlightPrice (NOLOCK) 
		                            Union 
		                            Select Distinct 'TRANSPORT' AS Service, Code='TRANSPORT', TD.TransPort 
                                    From TransportDay TD (NOLOCK)
		                            Join Transport T (NOLOCK) ON T.Code = TD.Transport
		                            Where Exists(Select RecID From Operator (NOLOCK) Where Market=@Market And Code=T.Operator)
			                          And TransDate >= dbo.DateOnly(GetDate())	                                        
		                            Union 
		                            Select Distinct 'RENTING' As Service, Code='RENTING', Renting As Name From RentingPrice (NOLOCK) 
		                            Where 
			                            Market = @Market 
		                            And dbo.DateOnly(GetDate()) between BegDate And EndDate
		                            Union 
		                            Select 'EXCURSION' As Service, Code='EXCURSION', Excursion As Name From ExcursionPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'INSURANCE' As Service, Code='INSURANCE', Insurance As Name From InsurancePrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'VISA' As Service, Code='VISA', Visa As Name From VisaPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'TRANSFER' As Service, Code='TRANSFER', Transfer As Name From TransferPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'HANDFEE' As Service, Code='HANDFEE', HandFee As Name From HandFeePrice (NOLOCK) Where Market = @Market 
		                            Union
		                            Select 'ADSERVICE' As Service, Code=Service, 'Additional' As Name From AdServicePrice (NOLOCK) Where Market = @Market 
	                            ) X 
	                            Left Join ServiceMarOpt SMP (NOLOCK) ON SMP.Service=X.Code And SMP.Market=@Market
                            ) S
                            Left Join Service S1 (NOLOCK) ON S1.Code=S.Service
                            Order By S.Service ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0022, VersionControl.Equality.gt))
                    db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        SaleServiceRecord rec = new SaleServiceRecord();
                        rec.Service = Conversion.getStrOrNull(R["Service"]);
                        rec.Code = Conversion.getStrOrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                        rec.WebSale = Conversion.getStrOrNull(R["WebSale"]);
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0022, VersionControl.Equality.gt))
                            rec.B2BIndPack = Conversion.getBoolOrNull(R["B2BIndPack"]);
                        else rec.B2BIndPack = string.Equals(rec.WebSale, "Y");
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<SaleServiceRecord> getSaleServices(User UserData, ResDataRecord ResData, ref string errorMsg)
        {
            List<SaleServiceRecord> records = new List<SaleServiceRecord>();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0022, VersionControl.Equality.gt))
            {
                tsql = @"Select S.Service,Code='', Name=S1.Name, NameL=isnull(dbo.FindLocalName(S1.NameLID, @Market), isnull(S1.Name, '')),WebSale,B2BIndPack
                         From (
                               Select Distinct X.Service,Code='', SMP.WebSale, SMP.B2BIndPack From 
	                            (
		                            Select 'HOTEL' AS Service, Code='HOTEL', Hotel As Name From HotelNetPrice (NOLOCK) 
		                            Union 
		                            Select 'FLIGHT' AS Service, Code='FLIGHT', FlightNo AS Name From FlightPrice (NOLOCK) 
		                            Union 
		                            Select Distinct 'TRANSPORT' AS Service, Code='TRANSPORT', TD.TransPort 
                                    From TransportDay TD (NOLOCK)
		                            Join Transport T (NOLOCK) ON T.Code = TD.Transport
		                            Where Exists(Select RecID From Operator (NOLOCK) Where Market=@Market And Code=T.Operator)
			                          And TransDate >= dbo.DateOnly(GetDate())	                                        
		                            Union 
		                            Select Distinct 'RENTING' As Service, Code='RENTING', Renting As Name From RentingPrice (NOLOCK) 
		                            Where 
                                        Market = @Market 
		                            And dbo.DateOnly(GetDate()) between BegDate And EndDate                                    
		                            Union 
		                            Select 'EXCURSION' As Service, Code='EXCURSION', Excursion As Name From ExcursionPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'INSURANCE' As Service, Code='INSURANCE', Insurance As Name From InsurancePrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'VISA' As Service, Code='VISA', Visa As Name From VisaPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'TRANSFER' As Service, Code='TRANSFER', Transfer As Name From TransferPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'HANDFEE' As Service, Code='HANDFEE', HandFee As Name From HandFeePrice (NOLOCK) Where Market = @Market 
		                            Union
		                            Select P.Service As Service , Code=AdService, 'Additional' As Name From AdServicePrice P (NOLOCK)
		                            Where P.Market=@Market 		  
		                              And P.Compulsory=0
		                              And (Exists(Select * From GrpAgencyDet GAD (NOLOCK) 
					                              Join GrpAgency GA (NOLOCK) ON GA.Code=GAD.Groups
					                              Where GAD.GrpType='P' And Agency=@Agency And Market=@Market
					                              And P.AgencyGrp=GA.Code)
			                               Or P.Agency=@Agency Or (isnull(P.Agency,'')='' And isnull(P.AgencyGrp,'')=''))
	                            ) X 
	                            Left Join ServiceMarOpt SMP (NOLOCK) ON SMP.Service=X.Service And SMP.Market=@Market	
                            ) S
                            Left Join Service S1 (NOLOCK) ON S1.Code=S.Service
                            Order By S.Service ";
            }
            else
                tsql = @"Select S.Service, Name=S1.Name, NameL=isnull(dbo.FindLocalName(S1.NameLID, @Market), isnull(S1.Name, '')),WebSale
                            From (
	                            Select Distinct X.Service, SMP.WebSale From 
	                            (
		                            Select 'HOTEL' AS Service, Code='HOTEL', Hotel As Name From HotelNetPrice (NOLOCK) 
		                            Union 
		                            Select 'FLIGHT' AS Service, Code='FLIGHT', FlightNo AS Name From FlightPrice (NOLOCK) 
		                            Union 
		                            Select Distinct 'TRANSPORT' AS Service, Code='TRANSPORT', TD.TransPort 
                                    From TransportDay TD (NOLOCK)
		                            Join Transport T (NOLOCK) ON T.Code = TD.Transport
		                            Where Exists(Select RecID From Operator (NOLOCK) Where Market=@Market And Code=T.Operator)
			                          And TransDate >= dbo.DateOnly(GetDate())	                                        
		                            Union 
		                            Select Distinct 'RENTING' As Service, Code='RENTING', Renting As Name From RentingPrice (NOLOCK) 
		                            Where 
			                            Market = @Market 
		                            And dbo.DateOnly(GetDate()) between BegDate And EndDate
		                            Union 
		                            Select 'EXCURSION' As Service, Code='EXCURSION', Excursion As Name From ExcursionPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'INSURANCE' As Service, Code='INSURANCE', Insurance As Name From InsurancePrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'VISA' As Service, Code='VISA', Visa As Name From VisaPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'TRANSFER' As Service, Code='TRANSFER', Transfer As Name From TransferPrice (NOLOCK) Where Market = @Market 
		                            Union 
		                            Select 'HANDFEE' As Service, Code='HANDFEE', HandFee As Name From HandFeePrice (NOLOCK) Where Market = @Market 
		                            Union
		                            Select 'ADSERVICE' As Service, Code=Service, 'Additional' As Name From AdServicePrice (NOLOCK) Where Market = @Market 
	                            ) X 
	                            Left Join ServiceMarOpt SMP (NOLOCK) ON SMP.Service=X.Code And SMP.Market=@Market
                            ) S
                            Left Join Service S1 (NOLOCK) ON S1.Code=S.Service
                            Order By S.Service ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0022, VersionControl.Equality.gt))
                    db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        SaleServiceRecord rec = new SaleServiceRecord();
                        rec.Service = Conversion.getStrOrNull(R["Service"]);
                        rec.Code = Conversion.getStrOrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameL = Conversion.getStrOrNull(R["NameL"]);
                        rec.WebSale = Conversion.getStrOrNull(R["WebSale"]);
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0022, VersionControl.Equality.gt))
                            rec.B2BIndPack = Conversion.getBoolOrNull(R["B2BIndPack"]);
                        else rec.B2BIndPack = string.Equals(rec.WebSale, "Y");
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

    }
}
