﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;

namespace TvBo
{
    // Aykut Aras teşekürler.
    public static class ImageResize
    {

        public static bool CropImageV2(int x1, int y1, int x2, int y2, int width, int height, decimal? scalaBase, string fileName, string croppedFileName)
        {
            // create property called save as and save this file to there.            
            //System.Drawing.Bitmap cropedImage = null;
            int imgW = 0;
            int imgH = 0;
            System.Drawing.Image image = null;
            System.Drawing.Image bmpImage = null;
            System.Drawing.Bitmap cropedImage = null;
            string fileNamePath = System.Web.HttpContext.Current.Server.MapPath("~") + "\\CustPicCache\\Loaded\\" + fileName;
            string fileNamePathCropped = System.Web.HttpContext.Current.Server.MapPath("~") + "\\CustPicCache\\" + croppedFileName;            

            try
            {
                image = System.Drawing.Image.FromFile(fileNamePath);
                imgH = image.Height;
                imgW = image.Width;

                decimal scala = (image.Width / (scalaBase.HasValue && scalaBase.Value > 0 ? scalaBase.Value : 1));                
                int x = Convert.ToInt32(x1 * scala);
                int y = Convert.ToInt32(y1 * scala);
                int w = Convert.ToInt32(width * scala);
                int h = Convert.ToInt32(height * scala);
                if (image.Width < w) w = image.Width;
                if (image.Height < h) h = image.Height;
                if (x > image.Width) x = 0;
                if (y > image.Height) y = 0;
                h = Convert.ToInt16(w / 0.75);
                bmpImage = tvCropImage(image, h, w, x, y);                
                cropedImage = new Bitmap(bmpImage);
                cropedImage = FixedSize(cropedImage, 240, 320);
                //cropedImage = FixedSizeDpi(bmpImage, x, y, 240, 320, 300.0f, 300.0f);

                string mimeType = GetMimeType(cropedImage);
                if (mimeType == "image/unknown")
                {
                    cropedImage.Save(fileNamePathCropped, ImageFormat.Jpeg);
                }
                else if (mimeType == "image/jpeg" || mimeType == "image/jpg")
                {
                    cropedImage.Save(fileNamePathCropped, ImageFormat.Jpeg);
                }
                else if (mimeType == "image/gif")
                {
                    cropedImage.Save(fileNamePathCropped, ImageFormat.Gif);
                }
                else if (mimeType == "image/x-png")
                {
                    cropedImage.Save(fileNamePathCropped, ImageFormat.Png);
                }
                else if (mimeType == "image/png")
                {
                    cropedImage.Save(fileNamePathCropped, ImageFormat.Png);
                }
                else if (mimeType == "image/x-ms-bmp")
                {
                    cropedImage.Save(fileNamePathCropped, ImageFormat.Bmp);
                }                                
                return true;
            }
            catch (Exception ex)
            {                
                return false;
            }
            finally
            {
                image.Dispose();
                bmpImage.Dispose();
                cropedImage.Dispose();                
                if (File.Exists(fileNamePath))
                    File.Delete(fileNamePath);                
            }
        }

        //Overload for crop that default starts top left of the image.
        public static System.Drawing.Image tvCropImage(System.Drawing.Image Image, int Height, int Width)
        {
            return tvCropImage(Image, Height, Width, 0, 0);
        }

        //The crop image sub
        public static System.Drawing.Image tvCropImage(System.Drawing.Image img, int Height, int Width, int StartAtX, int StartAtY)
        {
            #region 
            /*
            Image outimage;
            MemoryStream mm = null;
            try
            {
                //check the image height against our desired image height
                if (img.Height < Height)
                {
                    Height = img.Height;
                }

                if (img.Width < Width)
                {
                    Width = img.Width;
                }

                //create a bitmap window for cropping
                Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
                bmPhoto.SetResolution(img.HorizontalResolution, img.VerticalResolution);

                //create a new graphics object from our image and set properties
                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBilinear;
                grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
                grPhoto.CompositingQuality = CompositingQuality.HighQuality;
                //now do the crop
                grPhoto.DrawImage(img,
                    new Rectangle(0, 0, Width, Height),
                    StartAtX * 1.0f,
                    StartAtY * 1.0f,
                    Width * 1.0f,
                    Height * 1.0f,
                    GraphicsUnit.Document);
                // Save out to memory and get an image from it to send back out the method.                

                mm = new MemoryStream();
                bmPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
                img.Dispose();
                bmPhoto.Dispose();
                grPhoto.Dispose();
                outimage = Image.FromStream(mm);

                return outimage;
            }
            catch (Exception ex)
            {
                throw new Exception("Error cropping image, the error was: " + ex.Message);
            }
            */
            #endregion

            Image outimage;
            MemoryStream mm = null;
            try
            {
                //check the image height against our desired image height
                if (img.Height < Height)
                {
                    Height = img.Height;
                }

                if (img.Width < Width)
                {
                    Width = img.Width;
                }

                //create a bitmap window for cropping
                Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
                bmPhoto.SetResolution(72, 72);

                //create a new graphics object from our image and set properties
                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

                //now do the crop
                grPhoto.DrawImage(img, new Rectangle(0, 0, Width, Height), StartAtX, StartAtY, Width, Height, GraphicsUnit.Pixel);

                // Save out to memory and get an image from it to send back out the method.
                mm = new MemoryStream();
                bmPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
                img.Dispose();
                bmPhoto.Dispose();
                grPhoto.Dispose();
                outimage = Image.FromStream(mm);

                return outimage;
            }
            catch (Exception ex)
            {
                throw new Exception("Error cropping image, the error was: " + ex.Message);
            }

        }

        //Hard resize attempts to resize as close as it can to the desired size and then crops the excess
        public static System.Drawing.Image HardResizeImage(int Width, int Height, System.Drawing.Image Image)
        {
            int width = Image.Width;
            int height = Image.Height;
            Image resized = null;
            if (Width > Height)
            {
                resized = ResizeImage(Width, Width, Image);
            }
            else
            {
                resized = ResizeImage(Height, Height, Image);
            }
            Image output = tvCropImage(resized, Height, Width);
            //return the original resized image
            return output;
        }

        //Image resizing
        public static System.Drawing.Image ResizeImage(int maxWidth, int maxHeight, System.Drawing.Image Image)
        {
            int width = Image.Width;
            int height = Image.Height;
            if (width > maxWidth || height > maxHeight)
            {
                //The flips are in here to prevent any embedded image thumbnails -- usually from cameras
                //from displaying as the thumbnail image later, in other words, we want a clean
                //resize, not a grainy one.
                Image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipX);
                Image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipX);

                float ratio = 0;
                if (width > height)
                {
                    ratio = (float)width / (float)height;
                    width = maxWidth;
                    height = Convert.ToInt32(Math.Round((float)width / ratio));
                }
                else
                {
                    ratio = (float)height / (float)width;
                    height = maxHeight;
                    width = Convert.ToInt32(Math.Round((float)height / ratio));
                }

                //return the resized image
                return Image.GetThumbnailImage(width, height, null, IntPtr.Zero);
            }
            //return the original resized image
            return Image;
        }

        internal static Image Crop(Image image, int width, int height, int x, int y)
        {
            try
            {
                //Image image = Image.FromFile(img);
                Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                bmp.SetResolution(80, 60);

                Graphics gfx = Graphics.FromImage(bmp);
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gfx.DrawImage(image, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
                // Dispose to free up resources 
                image.Dispose();
                bmp.Dispose();
                gfx.Dispose();

                return bmp;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool CropImage(int? x1, int? y1, int? x2, int? y2, int? width, int? height, ref string fileName)
        {
            // create property called save as and save this file to there.
            System.Drawing.Graphics g = null;
            System.Drawing.Bitmap cropedImage = null;
            System.Drawing.Image image = null;

            string fileNameImg = fileName.Split('/')[fileName.Split('/').Length - 1];
            fileNameImg = fileNameImg.Split('?')[0];
            fileName = fileNameImg;

            string fileNamePath = System.Web.HttpContext.Current.Server.MapPath("~") + "\\CustPicCache\\" + fileNameImg;
            try
            {
                image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(System.IO.File.ReadAllBytes(fileNamePath)));
            }
            catch (Exception ex)
            {
                return false;
            }
            cropedImage = new System.Drawing.Bitmap(width.HasValue ? width.Value : 0, height.HasValue ? height.Value : 0, image.PixelFormat);
            g = System.Drawing.Graphics.FromImage(cropedImage);
            System.Drawing.Rectangle _rec;
            System.Drawing.Rectangle _rec2;
            _rec = new System.Drawing.Rectangle(0, 0, width.HasValue ? width.Value : 0, height.HasValue ? height.Value : 0);
            _rec2 = new System.Drawing.Rectangle(x1.HasValue ? x1.Value : 0, y1.HasValue ? y1.Value : 0, width.HasValue ? width.Value : 0, height.HasValue ? height.Value : 0);
            g.DrawImage(image, _rec, _rec2.X, _rec2.Y, _rec2.Width, _rec2.Height, System.Drawing.GraphicsUnit.Pixel);
            cropedImage = FixedSize(cropedImage, 240, 320);
            string mimeType = GetMimeType(image);
            if (mimeType == "image/unknown")
            {
                cropedImage.Save(fileNamePath);
            }
            else if (mimeType == "image/jpeg")
            {
                cropedImage.Save(fileNamePath, ImageFormat.Jpeg);
            }
            else if (mimeType == "image/gif")
            {
                cropedImage.Save(fileNamePath, ImageFormat.Gif);
            }
            else if (mimeType == "image/x-png")
            {
                cropedImage.Save(fileNamePath, ImageFormat.Png);
            }
            else if (mimeType == "image/x-ms-bmp")
            {
                cropedImage.Save(fileNamePath, ImageFormat.Bmp);
            }
            image.Dispose();
            g.Dispose();
            cropedImage.Dispose();

            return true;
        }

        public static string GetMimeType(System.Drawing.Image i)
        {
            try
            {
                foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageDecoders())
                {
                    if (codec.FormatID == i.RawFormat.Guid)
                        return codec.MimeType;
                }
                return "image/unknown";
            }
            catch
            {
                return "image/unknown";
            }
        }

        public static Bitmap ScaleByPercent(Bitmap imgPhoto, int Percent)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;

            int destX = 0;
            int destY = 0;
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public static Bitmap ConstrainProportions(Bitmap imgPhoto, int Size, Dimensions Dimension)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;
            float nPercent = 0;

            switch (Dimension)
            {
                case Dimensions.Width:
                    nPercent = ((float)Size / (float)sourceWidth);
                    break;
                default:
                    nPercent = ((float)Size / (float)sourceHeight);
                    break;
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
            new Rectangle(destX, destY, destWidth, destHeight),
            new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
            GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public static Bitmap FixedSize(Bitmap imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);

            //if we have to pad the height pad both the top and the bottom
            //with the difference between the scaled height and the desired height
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = (int)((Width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = (int)((Height - (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Red);
            grPhoto.SmoothingMode = SmoothingMode.HighQuality;
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBilinear;
            grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
            grPhoto.CompositingQuality = CompositingQuality.HighQuality;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, Width, Height),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public static Image FixedSizeDpi(Image imgPhoto, int? x, int? y, int Width, int Height, float? dpiX, float? dpiY)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = x.HasValue ? x.Value : 0;
            int destY = y.HasValue ? y.Value : 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);

            //if we have to pad the height pad both the top and the bottom
            //with the difference between the scaled height and the desired height
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = (int)((Width - (sourceWidth * nPercent)) / 2) + destX;
            }
            else
            {
                nPercent = nPercentW;
                destY = (int)((Height - (sourceHeight * nPercent)) / 2) + destY;
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(dpiY.HasValue ? dpiY.Value : imgPhoto.HorizontalResolution, dpiX.HasValue ? dpiX.Value : imgPhoto.VerticalResolution);            
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Red);
            grPhoto.SmoothingMode = SmoothingMode.HighQuality;
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBilinear;
            grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
            grPhoto.CompositingQuality = CompositingQuality.HighQuality;
            try
            {
                grPhoto.DrawImage(imgPhoto,
                    new Rectangle(destX, destY, destWidth, destHeight),
                    new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                    GraphicsUnit.Document);

                MemoryStream mm = new MemoryStream();
                bmPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
                return Image.FromStream(mm);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                grPhoto.Dispose();
            }            
        }

        public static Bitmap Crop(Bitmap imgPhoto, int Width, int Height, AnchorPosition Anchor)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentW;
                switch (Anchor)
                {
                    case AnchorPosition.Top:
                        destY = 0;
                        break;
                    case AnchorPosition.Bottom:
                        destY = (int)(Height - (sourceHeight * nPercent));
                        break;
                    default:
                        destY = (int)((Height - (sourceHeight * nPercent)) / 2);
                        break;
                }
            }
            else
            {
                nPercent = nPercentH;
                switch (Anchor)
                {
                    case AnchorPosition.Left:
                        destX = 0;
                        break;
                    case AnchorPosition.Right:
                        destX = (int)(Width - (sourceWidth * nPercent));
                        break;
                    default:
                        destX = (int)((Width - (sourceWidth * nPercent)) / 2);
                        break;
                }
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }
    }

    public enum Dimensions
    {
        Width,
        Height
    }

    public enum AnchorPosition
    {
        Top,
        Center,
        Bottom,
        Left,
        Right
    }
}