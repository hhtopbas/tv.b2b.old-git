﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class AdmUser
    {
        public bool tryLogin(ref WebAdminUser user, ref string errorMsg)
        {
            string tsql = @"Select Code, Name, WebPass 
                            From Users (NOLOCK)
                            Where Code='ADMIN' 
                              And @Admin='ADMIN'
                              And WebPass=@Pass";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Admin", DbType.String, user.Code);
                db.AddInParameter(dbCommand, "Pass", DbType.String, user.Password);

                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        user.Code = Conversion.getStrOrNull(rdr["Code"]);
                        user.Name = Conversion.getStrOrNull(rdr["Name"]);
                        user.Password = Conversion.getStrOrNull(rdr["WebPass"]);
                        user.IsLogin = true;
                        return true;
                    }
                    else return false;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
