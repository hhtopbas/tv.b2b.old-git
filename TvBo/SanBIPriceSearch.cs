﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TourVisio.WebService.Adapter.Enums;

namespace TvBo
{
    public class PriceSearchRequest
    {
        //public enmDirection? Direction { get; set; }
        //public int? Category { get; set; }
        public User UserData { get; set; }
        public int? DepCity { get; set; }
        public int? Country { get; set; }
        public int? ArrCity { get; set; }
        public DateTime? CheckInFrom { get; set; }
        public DateTime? CheckInTo { get; set; }
        public int? NightFrom { get; set; }
        public int? NightTo { get; set; }
        public int Adult { get; set; }
        public int Child { get; set; }
        public int? ChdAge1 { get; set; }
        public int? ChdAge2 { get; set; }
        public int? ChdAge3 { get; set; }
        public int? ChdAge4 { get; set; }
        /// <summary>
        /// Check Flight Seat
        /// </summary>
        public bool FlightControl { get; set; }
        /// <summary>
        /// Check Hotel Allotment
        /// </summary>
        public bool HotelControl { get; set; }
        public string PromoCode { get; set; }
        public string HolPack { get; set; }
        public string HotelCode { get; set; }
        public string CatName { get; set; }
        public string Room { get; set; }
        public string Board { get; set; }
        public int? Town { get; set; }
        public int? HotelLocation { get; set; }
        public decimal? MinPPPrice { get; set; }
        public decimal? MaxPPPrice { get; set; }
        public decimal? MinRoomPrice { get; set; }
        public decimal? MaxRoomPrice { get; set; }
        public string Currency { get; set; }
        public bool? GetFlightAllot { get; set; }
        public bool? GetHotelAllot { get; set; }
        public bool? RoomAndBoardIsGroup { get; set; }
        public string PackType { get; set; }
        public bool NoStopSaleControl { get; set; }
        public bool GetFlightCompany { get; set; }
        public bool GetEBDiscountVal { get; set; }
        public string HotLocationGrp { get; set; }
        public bool GetFlightAvailablity { get; set; }
        public bool GetHotelAvailablity { get; set; }
        public bool IsB2CAgency { get; set; }
        public int Direction { get; set; }
        public int PackageCategory { get; set; }
        public string TvVersion { get; set; }
        public bool ShowFlightDetail { get; set; }
        public bool? GetHotelPrice { get; set; }
        public string Airport { get; set; }
        public int? ThemeId { get; set; }
        public int? SaleType { get; set; }
        public bool IsPriceSearchNew { get; set; }
        public enmProductType ProductType { get; set; }
        public string FlightClass { get; set; }
        public string Excursion { get; set; }
        public int PriceSearchTime { get; set; }
    }
}
