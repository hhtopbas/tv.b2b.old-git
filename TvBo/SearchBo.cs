﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public enum AgeGroupType { Adult = 0, OlderChild = 1, TeenAge = 2, Child = 3, Infant = 4 };

    public class CalendarDays
    {
        public CalendarDays()
        {
        }

        int? _Year;
        public int? Year
        {
            get { return _Year; }
            set { _Year = value; }
        }

        int? _Month;
        public int? Month
        {
            get { return _Month; }
            set { _Month = value; }
        }

        int? _Day;
        public int? Day
        {
            get { return _Day; }
            set { _Day = value; }
        }

        string _Text;
        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }
    }

    public class CatalogPackRecord
    {
        public CatalogPackRecord()
        {
        }
        int? _RecID;
        public int? RecID { get { return _RecID; } set { _RecID = value; } }
        int? _CatID;
        public int? CatID { get { return _CatID; } set { _CatID = value; } }
        string _HolPack;
        public string HolPack { get { return _HolPack; } set { _HolPack = value; } }
        string _Name;
        public string Name { get { return _Name; } set { _Name = value; } }
        string _NameL;
        public string NameL { get { return _NameL; } set { _NameL = value; } }
        string _NameS;
        public string NameS { get { return _NameS; } set { _NameS = value; } }
        string _Operator;
        public string Operator { get { return _Operator; } set { _Operator = value; } }
        string _Market;
        public string Market { get { return _Market; } set { _Market = value; } }
        string _TargetMarket;
        public string TargetMarket { get { return _TargetMarket; } set { _TargetMarket = value; } }
        string _SaleSPONo;
        public string SaleSPONo { get { return _SaleSPONo; } set { _SaleSPONo = value; } }
        int? _RevNo;
        public int? RevNo { get { return _RevNo; } set { _RevNo = value; } }
        DateTime? _RevDate;
        public DateTime? RevDate { get { return _RevDate; } set { _RevDate = value; } }
        DateTime? _BegDate;
        public DateTime? BegDate { get { return _BegDate; } set { _BegDate = value; } }
        DateTime? _EndDate;
        public DateTime? EndDate { get { return _EndDate; } set { _EndDate = value; } }
        string _SaleType_X;
        public string SaleType_X { get { return _SaleType_X; } set { _SaleType_X = value; } }
        string _Ready;
        public string Ready { get { return _Ready; } set { _Ready = value; } }
        DateTime? _SaleBegDate;
        public DateTime? SaleBegDate { get { return _SaleBegDate; } set { _SaleBegDate = value; } }
        DateTime? _SaleEndDate;
        public DateTime? SaleEndDate { get { return _SaleEndDate; } set { _SaleEndDate = value; } }
        DateTime? _PubDate;
        public DateTime? PubDate { get { return _PubDate; } set { _PubDate = value; } }
        string _WebPub;
        public string WebPub { get { return _WebPub; } set { _WebPub = value; } }
        DateTime? _WebPubDate;
        public DateTime? WebPubDate { get { return _WebPubDate; } set { _WebPubDate = value; } }
        DateTime? _CalcDate;
        public DateTime? CalcDate { get { return _CalcDate; } set { _CalcDate = value; } }
        string _Description;
        public string Description { get { return _Description; } set { _Description = value; } }
        string _FlightClass;
        public string FlightClass { get { return _FlightClass; } set { _FlightClass = value; } }
        string _AccomNight;
        public string AccomNight { get { return _AccomNight; } set { _AccomNight = value; } }
        string _SaleCur;
        public string SaleCur { get { return _SaleCur; } set { _SaleCur = value; } }
        Int16? _CinDateSource;
        public Int16? CinDateSource { get { return _CinDateSource; } set { _CinDateSource = value; } }
        Int16? _MinNight;
        public Int16? MinNight { get { return _MinNight; } set { _MinNight = value; } }
        Int16? _MaxNight;
        public Int16? MaxNight { get { return _MaxNight; } set { _MaxNight = value; } }
        string _BreakNoPriceChk;
        public string BreakNoPriceChk { get { return _BreakNoPriceChk; } set { _BreakNoPriceChk = value; } }
        string _CheckInDateChk;
        public string CheckInDateChk { get { return _CheckInDateChk; } set { _CheckInDateChk = value; } }
        string _OnlySpoServiceChk;
        public string OnlySpoServiceChk { get { return _OnlySpoServiceChk; } set { _OnlySpoServiceChk = value; } }
        string _UseUFlightPriceChk;
        public string UseUFlightPriceChk { get { return _UseUFlightPriceChk; } set { _UseUFlightPriceChk = value; } }
        string _UFAgency;
        public string UFAgency { get { return _UFAgency; } set { _UFAgency = value; } }
        string _FBlockType;
        public string FBlockType { get { return _FBlockType; } set { _FBlockType = value; } }
        string _FPriceOpt;
        public string FPriceOpt { get { return _FPriceOpt; } set { _FPriceOpt = value; } }
        string _EBValid;
        public string EBValid { get { return _EBValid; } set { _EBValid = value; } }
        string _CanCredit;
        public string CanCredit { get { return _CanCredit; } set { _CanCredit = value; } }
        int? _DepCity;
        public int? DepCity { get { return _DepCity; } set { _DepCity = value; } }
        int? _ArrCity;
        public int? ArrCity { get { return _ArrCity; } set { _ArrCity = value; } }
        int? _ExChgOpt;
        public int? ExChgOpt { get { return _ExChgOpt; } set { _ExChgOpt = value; } }
        string _PasEBValid;
        public string PasEBValid { get { return _PasEBValid; } set { _PasEBValid = value; } }
        string _PackType;
        public string PackType { get { return _PackType; } set { _PackType = value; } }
        bool? _B2CPub;
        public bool? B2CPub { get { return _B2CPub; } set { _B2CPub = value; } }
        DateTime? _B2CPubDate;
        public DateTime? B2CPubDate { get { return _B2CPubDate; } set { _B2CPubDate = value; } }
        string _SupDisCode;
        public string SupDisCode { get { return _SupDisCode; } set { _SupDisCode = value; } }
        string _SupDisCodeB2C;
        public string SupDisCodeB2C { get { return _SupDisCodeB2C; } set { _SupDisCodeB2C = value; } }
        decimal? _MaxDiscountPer;
        public decimal? MaxDiscountPer { get { return _MaxDiscountPer; } set { _MaxDiscountPer = value; } }
        string _RefHotel;
        public string RefHotel { get { return _RefHotel; } set { _RefHotel = value; } }
        Int16? _Direction;
        public Int16? Direction { get { return _Direction; } set { _Direction = value; } }
        Int16? _Category;
        public Int16? Category { get { return _Category; } set { _Category = value; } }
        string _RefBoard;
        public string RefBoard { get { return _RefBoard; } set { _RefBoard = value; } }
        string _RefRoom;
        public string RefRoom { get { return _RefRoom; } set { _RefRoom = value; } }
        Byte? _TANight;
        public Byte? TANight { get { return _TANight; } set { _TANight = value; } }
        bool? _PLSPOExists;
        public bool? PLSPOExists { get { return _PLSPOExists; } set { _PLSPOExists = value; } }
        DateTime? _XMLExportDate;
        public DateTime? XMLExportDate { get { return _XMLExportDate; } set { _XMLExportDate = value; } }
        bool? _ChanceSale;
        public bool? ChanceSale { get { return _ChanceSale; } set { _ChanceSale = value; } }
        DateTime? _EndTime;
        public DateTime? EndTime { get { return _EndTime; } set { _EndTime = value; } }
        Int16? _RoundType;
        public Int16? RoundType { get { return _RoundType; } set { _RoundType = value; } }
        Int16? _RoundOpt;
        public Int16? RoundOpt { get { return _RoundOpt; } set { _RoundOpt = value; } }
        decimal? _RoundVal1;
        public decimal? RoundVal1 { get { return _RoundVal1; } set { _RoundVal1 = value; } }
        int? _DepDepCity;
        public int? DepDepCity { get { return _DepDepCity; } set { _DepDepCity = value; } }
        int? _DepArrCity;
        public int? DepArrCity { get { return _DepArrCity; } set { _DepArrCity = value; } }
        int? _RetDepCity;
        public int? RetDepCity { get { return _RetDepCity; } set { _RetDepCity = value; } }
        int? _RetArrCity;
        public int? RetArrCity { get { return _RetArrCity; } set { _RetArrCity = value; } }
        string _DepServiceType;
        public string DepServiceType { get { return _DepServiceType; } set { _DepServiceType = value; } }
        string _RetServiceType;
        public string RetServiceType { get { return _RetServiceType; } set { _RetServiceType = value; } }
        string _B2BMenuCat;
        public string B2BMenuCat { get { return _B2BMenuCat; } set { _B2BMenuCat = value; } }
    }

    public class HolPackCatRecord
    {
        public HolPackCatRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _Code;
        public Int16? Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

    }

    public class SearchPLData
    {
        public SearchPLData()
        {
            _plLocations = new List<plLocationData>();
            _plCategorys = new List<plCategoryData>();
            _plHolPacks = new List<plHolPackData>();
            _plBoards = new List<plBoardData>();
            _plRooms = new List<plRoomData>();
            _plResorts = new List<plResortData>();
            _plHotels = new List<plHotelData>();
            _plHolPackCat = new List<HolPackCatRecord>();
            _plFacility = new List<plFacilityData>();
        }

        List<plLocationData> _plLocations;
        public List<plLocationData> PlLocations
        {
            get { return _plLocations; }
            set { _plLocations = value; }
        }

        List<plCategoryData> _plCategorys;
        public List<plCategoryData> plCategorys
        {
            get { return _plCategorys; }
            set { _plCategorys = value; }
        }

        List<plHolPackData> _plHolPacks;
        public List<plHolPackData> plHolPacks
        {
            get { return _plHolPacks; }
            set { _plHolPacks = value; }
        }

        List<plBoardData> _plBoards;
        public List<plBoardData> plBoards
        {
            get { return _plBoards; }
            set { _plBoards = value; }
        }

        List<plRoomData> _plRooms;
        public List<plRoomData> plRooms
        {
            get { return _plRooms; }
            set { _plRooms = value; }
        }

        List<plResortData> _plResorts;
        public List<plResortData> plResorts
        {
            get { return _plResorts; }
            set { _plResorts = value; }
        }

        List<plHotelData> _plHotels;
        public List<plHotelData> plHotels
        {
            get { return _plHotels; }
            set { _plHotels = value; }
        }

        List<HolPackCatRecord> _plHolPackCat;
        public List<HolPackCatRecord> PlHolPackCat
        {
            get { return _plHolPackCat; }
            set { _plHolPackCat = value; }
        }

        plMaxPaxCounts _plMaxPaxCount;
        public plMaxPaxCounts PlMaxPaxCount
        {
            get { return _plMaxPaxCount; }
            set { _plMaxPaxCount = value; }
        }

        List<plFacilityData> _plFacility;
        public List<plFacilityData> PlFacility
        {
            get { return _plFacility; }
            set { _plFacility = value; }
        }
    }

    public class plMaxPaxCounts
    {
        public plMaxPaxCounts()
        {
            _maxAdult = 6;
            _maxChd = 4;
            _maxChdAgeG1 = 4;
            _maxChdAgeG2 = 4;
            _maxChdAgeG3 = 4;
            _maxChdAgeG4 = 4;
        }

        int? _maxAdult;
        public int? maxAdult
        {
            get { return _maxAdult; }
            set { _maxAdult = value; }
        }

        int? _maxChd;
        public int? maxChd
        {
            get { return _maxChd; }
            set { _maxChd = value; }
        }

        int? _maxChdAgeG1;
        public int? maxChdAgeG1
        {
            get { return _maxChdAgeG1; }
            set { _maxChdAgeG1 = value; }
        }

        int? _maxChdAgeG2;
        public int? maxChdAgeG2
        {
            get { return _maxChdAgeG2; }
            set { _maxChdAgeG2 = value; }
        }

        int? _maxChdAgeG3;
        public int? maxChdAgeG3
        {
            get { return _maxChdAgeG3; }
            set { _maxChdAgeG3 = value; }
        }

        int? _maxChdAgeG4;
        public int? maxChdAgeG4
        {
            get { return _maxChdAgeG4; }
            set { _maxChdAgeG4 = value; }
        }
    }

    public class plLocationData
    {
        public plLocationData()
        {
        }

        int _DepCity;
        public int DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityNameL;
        public string DepCityNameL
        {
            get { return _DepCityNameL; }
            set { _DepCityNameL = value; }
        }

        int _ArrCountry;
        public int ArrCountry
        {
            get { return _ArrCountry; }
            set { _ArrCountry = value; }
        }

        string _ArrCountryName;
        public string ArrCountryName
        {
            get { return _ArrCountryName; }
            set { _ArrCountryName = value; }
        }

        string _ArrCountryNameL;
        public string ArrCountryNameL
        {
            get { return _ArrCountryNameL; }
            set { _ArrCountryNameL = value; }
        }

        int _ArrCity;
        public int ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityNameL;
        public string ArrCityNameL
        {
            get { return _ArrCityNameL; }
            set { _ArrCityNameL = value; }
        }

        int _HLocation;
        public int HLocation
        {
            get { return _HLocation; }
            set { _HLocation = value; }
        }

        string _HLocationName;
        public string HLocationName
        {
            get { return _HLocationName; }
            set { _HLocationName = value; }
        }

        string _HLocationNameL;
        public string HLocationNameL
        {
            get { return _HLocationNameL; }
            set { _HLocationNameL = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _HolPackCat;
        public Int16? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }

        string _HolPackCatName;
        public string HolPackCatName
        {
            get { return _HolPackCatName; }
            set { _HolPackCatName = value; }
        }

        string _HolPackCatNameL;
        public string HolPackCatNameL
        {
            get { return _HolPackCatNameL; }
            set { _HolPackCatNameL = value; }
        }
    }

    public class plCategoryData
    {
        public plCategoryData()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        int _Country;
        public int Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        int? _PLCountry;
        public int? PLCountry
        {
            get { return _PLCountry; }
            set { _PLCountry = value; }
        }

        int _City;
        public int City
        {
            get { return _City; }
            set { _City = value; }
        }

        int? _PLCity;
        public int? PLCity
        {
            get { return _PLCity; }
            set { _PLCity = value; }
        }

        int? _Town;
        public int? Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        int? _Village;
        public int? Village
        {
            get { return _Village; }
            set { _Village = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _HolPackCat;
        public Int16? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }
    }

    public class plHolPackData
    {
        public plHolPackData()
        {
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _HolPack;
        public string HolPack
        {
            get { return _HolPack; }
            set { _HolPack = value; }
        }

        int _DepCity;
        public int DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int _ArrCity;
        public int ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        int _Country;
        public int Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _HolPackCat;
        public Int16? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }
    }

    public class plBoardData
    {
        public plBoardData()
        {
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Hotel_Code;
        public string Hotel_Code
        {
            get { return _Hotel_Code; }
            set { _Hotel_Code = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        int _City;
        public int City
        {
            get { return _City; }
            set { _City = value; }
        }

        int? _Town;
        public int? Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        int? _Village;
        public int? Village
        {
            get { return _Village; }
            set { _Village = value; }
        }

        int _PLArrCity;
        public int PLArrCity
        {
            get { return _PLArrCity; }
            set { _PLArrCity = value; }
        }

        int _PLDepCity;
        public int PLDepCity
        {
            get { return _PLDepCity; }
            set { _PLDepCity = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _HolPackCat;
        public Int16? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }
    }

    public class plRoomData
    {
        public plRoomData()
        {
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Hotel_Code;
        public string Hotel_Code
        {
            get { return _Hotel_Code; }
            set { _Hotel_Code = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        int _City;
        public int City
        {
            get { return _City; }
            set { _City = value; }
        }

        int? _Town;
        public int? Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        int? _Village;
        public int? Village
        {
            get { return _Village; }
            set { _Village = value; }
        }

        int _PLArrCity;
        public int PLArrCity
        {
            get { return _PLArrCity; }
            set { _PLArrCity = value; }
        }

        int _PLDepCity;
        public int PLDepCity
        {
            get { return _PLDepCity; }
            set { _PLDepCity = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _HolPackCat;
        public Int16? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }
    }

    public class plResortData
    {
        public plResortData()
        {
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int _Country;
        public int Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        int _City;
        public int City
        {
            get { return _City; }
            set { _City = value; }
        }

        int? _Town;
        public int? Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _HolPackCat;
        public Int16? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }
    }

    public class plHotelData
    {
        public plHotelData()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        int _Location;
        public int Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _LocationNameL;
        public string LocationNameL
        {
            get { return _LocationNameL; }
            set { _LocationNameL = value; }
        }

        int _Country;
        public int Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; }
        }

        string _CountryNameL;
        public string CountryNameL
        {
            get { return _CountryNameL; }
            set { _CountryNameL = value; }
        }

        int? _Town;
        public int? Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        string _TownName;
        public string TownName
        {
            get { return _TownName; }
            set { _TownName = value; }
        }

        string _TownNameL;
        public string TownNameL
        {
            get { return _TownNameL; }
            set { _TownNameL = value; }
        }

        int _ArrCity;
        public int ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityNameL;
        public string ArrCityNameL
        {
            get { return _ArrCityNameL; }
            set { _ArrCityNameL = value; }
        }

        int? _CatPackID;
        public int? CatPackID
        {
            get { return _CatPackID; }
            set { _CatPackID = value; }
        }

        string _Holpack;
        public string Holpack
        {
            get { return _Holpack; }
            set { _Holpack = value; }
        }

        string _HolpackName;
        public string HolpackName
        {
            get { return _HolpackName; }
            set { _HolpackName = value; }
        }

        string _HolpackNameL;
        public string HolpackNameL
        {
            get { return _HolpackNameL; }
            set { _HolpackNameL = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _HolPackCat;
        public Int16? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }
    }

    public class plFacilityData
    {
        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Hotel_Code;
        public string Hotel_Code
        {
            get { return _Hotel_Code; }
            set { _Hotel_Code = value; }
        }

        int? _City;
        public int? City
        {
            get { return _City; }
            set { _City = value; }
        }

        int? _Town;
        public int? Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        int? _Village;
        public int? Village
        {
            get { return _Village; }
            set { _Village = value; }
        }

        int? _PLArrCity;
        public int? PLArrCity
        {
            get { return _PLArrCity; }
            set { _PLArrCity = value; }
        }

        int? _PLDepCity;
        public int? PLDepCity
        {
            get { return _PLDepCity; }
            set { _PLDepCity = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _HolPackCat;
        public Int16? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }

        public plFacilityData()
        {
        }
    }

    public class FlyDate
    {
        public FlyDate()
        {
        }

        DateTime _Date;
        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        int _DepCity;
        public int DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int _ArrCity;
        public int ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

    }

    [Serializable()]
    public class SearchResult
    {
        public SearchResult()
        {
            _AgeGroupList = new List<AgeGroup>();
            _UseDynamicFlights = false;
            _DynamicFlights = new dynamicFlights();
            _SearchTime = DateTime.Now;
        }
        public string DepFlightPNLName{ get; set; }
        public string RetFlightPNLName { get; set; }

        int _RefNo;
        public int RefNo
        {
            get { return _RefNo; }
            set { _RefNo = value; }
        }

        int _RoomNr;
        public int RoomNr
        {
            get { return _RoomNr; }
            set { _RoomNr = value; }
        }

        bool _Calculated;
        public bool Calculated
        {
            get { return _Calculated; }
            set { _Calculated = value; }
        }

        string _CurrentCur;
        public string CurrentCur
        {
            get { return _CurrentCur; }
            set { _CurrentCur = value; }
        }

        int? _CatPackID;
        public int? CatPackID
        {
            get { return _CatPackID; }
            set { _CatPackID = value; }
        }

        int? _ARecNo;
        public int? ARecNo
        {
            get { return _ARecNo; }
            set { _ARecNo = value; }
        }

        int? _PRecNo;
        public int? PRecNo
        {
            get { return _PRecNo; }
            set { _PRecNo = value; }
        }

        int? _HAPRecId;
        public int? HAPRecId
        {
            get { return _HAPRecId; }
            set { _HAPRecId = value; }
        }

        Int16 _HAdult;
        public Int16 HAdult
        {
            get { return _HAdult; }
            set { _HAdult = value; }
        }

        Int16 _HChdAgeG1;
        public Int16 HChdAgeG1
        {
            get { return _HChdAgeG1; }
            set { _HChdAgeG1 = value; }
        }

        Int16 _HChdAgeG2;
        public Int16 HChdAgeG2
        {
            get { return _HChdAgeG2; }
            set { _HChdAgeG2 = value; }
        }

        Int16 _HChdAgeG3;
        public Int16 HChdAgeG3
        {
            get { return _HChdAgeG3; }
            set { _HChdAgeG3 = value; }
        }

        Int16 _HChdAgeG4;
        public Int16 HChdAgeG4
        {
            get { return _HChdAgeG4; }
            set { _HChdAgeG4 = value; }
        }

        decimal? _ChdG1Age1;
        public decimal? ChdG1Age1
        {
            get { return _ChdG1Age1; }
            set { _ChdG1Age1 = value; }
        }

        decimal? _ChdG1Age2;
        public decimal? ChdG1Age2
        {
            get { return _ChdG1Age2; }
            set { _ChdG1Age2 = value; }
        }

        decimal? _ChdG2Age1;
        public decimal? ChdG2Age1
        {
            get { return _ChdG2Age1; }
            set { _ChdG2Age1 = value; }
        }

        decimal? _ChdG2Age2;
        public decimal? ChdG2Age2
        {
            get { return _ChdG2Age2; }
            set { _ChdG2Age2 = value; }
        }

        decimal? _ChdG3Age1;
        public decimal? ChdG3Age1
        {
            get { return _ChdG3Age1; }
            set { _ChdG3Age1 = value; }
        }

        decimal? _ChdG3Age2;
        public decimal? ChdG3Age2
        {
            get { return _ChdG3Age2; }
            set { _ChdG3Age2 = value; }
        }

        decimal? _ChdG4Age1;
        public decimal? ChdG4Age1
        {
            get { return _ChdG4Age1; }
            set { _ChdG4Age1 = value; }
        }

        decimal? _ChdG4Age2;
        public decimal? ChdG4Age2
        {
            get { return _ChdG4Age2; }
            set { _ChdG4Age2 = value; }
        }

        string _OprText;
        public string OprText
        {
            get { return _OprText; }
            set { _OprText = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _HotelName;
        public string HotelName
        {
            get { return _HotelName; }
            set { _HotelName = value; }
        }

        string _HotelNameL;
        public string HotelNameL
        {
            get { return _HotelNameL; }
            set { _HotelNameL = value; }
        }

        int? _HotelLocation;
        public int? HotelLocation
        {
            get { return _HotelLocation; }
            set { _HotelLocation = value; }
        }

        string _HotLocationName;
        public string HotLocationName
        {
            get { return _HotLocationName; }
            set { _HotLocationName = value; }
        }

        string _HotLocationNameL;
        public string HotLocationNameL
        {
            get { return _HotLocationNameL; }
            set { _HotLocationNameL = value; }
        }

        string _HotCat;
        public string HotCat
        {
            get { return _HotCat; }
            set { _HotCat = value; }
        }

        DateTime? _CheckIn;
        public DateTime? CheckIn
        {
            get { return _CheckIn; }
            set { _CheckIn = value; }
        }

        DateTime? _CheckOut;
        public DateTime? CheckOut
        {
            get { return _CheckOut; }
            set { _CheckOut = value; }
        }

        Int16? _Night;
        public Int16? Night
        {
            get { return _Night; }
            set { _Night = value; }
        }

        DateTime? _HotCheckIn;
        public DateTime? HotCheckIn
        {
            get { return _HotCheckIn; }
            set { _HotCheckIn = value; }
        }

        Int16? _HotNight;
        public Int16? HotNight
        {
            get { return _HotNight; }
            set { _HotNight = value; }
        }

        int? _FreeRoom;
        public int? FreeRoom
        {
            get { return _FreeRoom; }
            set { _FreeRoom = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _RoomName;
        public string RoomName
        {
            get { return _RoomName; }
            set { _RoomName = value; }
        }

        string _RoomNameL;
        public string RoomNameL
        {
            get { return _RoomNameL; }
            set { _RoomNameL = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _BoardName;
        public string BoardName
        {
            get { return _BoardName; }
            set { _BoardName = value; }
        }

        string _BoardNameL;
        public string BoardNameL
        {
            get { return _BoardNameL; }
            set { _BoardNameL = value; }
        }

        string _Accom;
        public string Accom
        {
            get { return _Accom; }
            set { _Accom = value; }
        }

        string _AccomName;
        public string AccomName
        {
            get { return _AccomName; }
            set { _AccomName = value; }
        }

        string _AccomNameL;
        public string AccomNameL
        {
            get { return _AccomNameL; }
            set { _AccomNameL = value; }
        }

        string _AccomFullName;
        public string AccomFullName
        {
            get { return _AccomFullName; }
            set { _AccomFullName = value; }
        }

        decimal? _CalcSalePrice;
        public decimal? CalcSalePrice
        {
            get { return _CalcSalePrice; }
            set { _CalcSalePrice = value; }
        }

        decimal? _SaleAdl;
        public decimal? SaleAdl
        {
            get { return _SaleAdl; }
            set { _SaleAdl = value; }
        }

        decimal? _SaleChdG1;
        public decimal? SaleChdG1
        {
            get { return _SaleChdG1; }
            set { _SaleChdG1 = value; }
        }

        decimal? _SaleChdG2;
        public decimal? SaleChdG2
        {
            get { return _SaleChdG2; }
            set { _SaleChdG2 = value; }
        }

        decimal? _SaleChdG3;
        public decimal? SaleChdG3
        {
            get { return _SaleChdG3; }
            set { _SaleChdG3 = value; }
        }

        decimal? _SaleChdG4;
        public decimal? SaleChdG4
        {
            get { return _SaleChdG4; }
            set { _SaleChdG4 = value; }
        }

        decimal? _HotChdPrice;
        public decimal? HotChdPrice
        {
            get { return _HotChdPrice; }
            set { _HotChdPrice = value; }
        }

        decimal? _LastPrice;
        public decimal? LastPrice
        {
            get { return _LastPrice; }
            set { _LastPrice = value; }
        }

        decimal? _OldSalePrice;
        public decimal? OldSalePrice
        {
            get { return _OldSalePrice; }
            set { _OldSalePrice = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        string _PLCur;
        public string PLCur
        {
            get { return _PLCur; }
            set { _PLCur = value; }
        }

        string _Offer;
        public string Offer
        {
            get { return _Offer; }
            set { _Offer = value; }
        }

        decimal? _AgencyEB;
        public decimal? AgencyEB
        {
            get { return _AgencyEB; }
            set { _AgencyEB = value; }
        }

        DateTime? _ValidDate;
        public DateTime? ValidDate
        {
            get { return _ValidDate; }
            set { _ValidDate = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityNameL;
        public string DepCityNameL
        {
            get { return _DepCityNameL; }
            set { _DepCityNameL = value; }
        }

        string _DepFlight;
        public string DepFlight
        {
            get { return _DepFlight; }
            set { _DepFlight = value; }
        }

        string _DepFlightMarket;
        public string DepFlightMarket
        {
            get { return _DepFlightMarket; }
            set { _DepFlightMarket = value; }
        }

        string _DepClass;
        public string DepClass
        {
            get { return _DepClass; }
            set { _DepClass = value; }
        }

        DateTime? _DEPFlightTime;
        public DateTime? DEPFlightTime
        {
            get { return _DEPFlightTime; }
            set { _DEPFlightTime = value; }
        }

        int? _DepSeat;
        public int? DepSeat
        {
            get { return _DepSeat; }
            set { _DepSeat = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityNameL;
        public string ArrCityNameL
        {
            get { return _ArrCityNameL; }
            set { _ArrCityNameL = value; }
        }

        string _RetFlight;
        public string RetFlight
        {
            get { return _RetFlight; }
            set { _RetFlight = value; }
        }

        string _RetClass;
        public string RetClass
        {
            get { return _RetClass; }
            set { _RetClass = value; }
        }

        DateTime? _RETFlightTime;
        public DateTime? RETFlightTime
        {
            get { return _RETFlightTime; }
            set { _RETFlightTime = value; }
        }

        int? _RetSeat;
        public int? RetSeat
        {
            get { return _RetSeat; }
            set { _RetSeat = value; }
        }

        int? _StopSaleGuar;
        public int? StopSaleGuar
        {
            get { return _StopSaleGuar; }
            set { _StopSaleGuar = value; }
        }

        int? _StopSaleStd;
        public int? StopSaleStd
        {
            get { return _StopSaleStd; }
            set { _StopSaleStd = value; }
        }

        DateTime? _EBValidDate;
        public DateTime? EBValidDate
        {
            get { return _EBValidDate; }
            set { _EBValidDate = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        string _SpoDesc;
        public string SpoDesc
        {
            get { return _SpoDesc; }
            set { _SpoDesc = value; }
        }

        string _FullName;
        public string FullName
        {
            get { return _FullName; }
            set { _FullName = value; }
        }

        int? _SaleSPONo;
        public int? SaleSPONo
        {
            get { return _SaleSPONo; }
            set { _SaleSPONo = value; }
        }

        decimal? _EBPerc;
        public decimal? EBPerc
        {
            get { return _EBPerc; }
            set { _EBPerc = value; }
        }

        decimal? _EBAmount;
        public decimal? EBAmount
        {
            get { return _EBAmount; }
            set { _EBAmount = value; }
        }

        decimal? _PasEBPer;
        public decimal? PasEBPer
        {
            get { return _PasEBPer; }
            set { _PasEBPer = value; }
        }

        decimal? _PEB_AdlVal;
        public decimal? PEB_AdlVal
        {
            get { return _PEB_AdlVal; }
            set { _PEB_AdlVal = value; }
        }

        DateTime? _PEB_SaleEndDate;
        public DateTime? PEB_SaleEndDate
        {
            get { return _PEB_SaleEndDate; }
            set { _PEB_SaleEndDate = value; }
        }

        DateTime? _PEB_ResEndDate;
        public DateTime? PEB_ResEndDate
        {
            get { return _PEB_ResEndDate; }
            set { _PEB_ResEndDate = value; }
        }

        DateTime? _SaleEndDate;
        public DateTime? SaleEndDate
        {
            get { return _SaleEndDate; }
            set { _SaleEndDate = value; }
        }

        string _HolPack;
        public string HolPack
        {
            get { return _HolPack; }
            set { _HolPack = value; }
        }

        string _HolPackName;
        public string HolPackName
        {
            get { return _HolPackName; }
            set { _HolPackName = value; }
        }

        string _HolPackNameL;
        public string HolPackNameL
        {
            get { return _HolPackNameL; }
            set { _HolPackNameL = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _FlightClass;
        public string FlightClass
        {
            get { return _FlightClass; }
            set { _FlightClass = value; }
        }

        Int16? _Child1Age;
        public Int16? Child1Age
        {
            get { return _Child1Age; }
            set { _Child1Age = value; }
        }

        Int16? _Child2Age;
        public Int16? Child2Age
        {
            get { return _Child2Age; }
            set { _Child2Age = value; }
        }

        Int16? _Child3Age;
        public Int16? Child3Age
        {
            get { return _Child3Age; }
            set { _Child3Age = value; }
        }

        Int16? _Child4Age;
        public Int16? Child4Age
        {
            get { return _Child4Age; }
            set { _Child4Age = value; }
        }

        bool? _plSpoExists;
        public bool? plSpoExists
        {
            get { return _plSpoExists; }
            set { _plSpoExists = value; }
        }

        DateTime? _FlightValidDate;
        public DateTime? FlightValidDate
        {
            get { return _FlightValidDate; }
            set { _FlightValidDate = value; }
        }

        DateTime? _HotelValidDate;
        public DateTime? HotelValidDate
        {
            get { return _HotelValidDate; }
            set { _HotelValidDate = value; }
        }

        int? _Direction;
        public int? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        int? _Category;
        public int? Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        string _InfoWeb;
        public string InfoWeb
        {
            get { return _InfoWeb; }
            set { _InfoWeb = value; }
        }

        decimal? _MaxChdAge;
        public decimal? MaxChdAge
        {
            get { return _MaxChdAge; }
            set { _MaxChdAge = value; }
        }

        string _UseSysParamAllot;
        public string UseSysParamAllot
        {
            get { return _UseSysParamAllot; }
            set { _UseSysParamAllot = value; }
        }

        string _HotAllotChk;
        public string HotAllotChk
        {
            get { return _HotAllotChk; }
            set { _HotAllotChk = value; }
        }

        string _HotAllotNotAcceptFull;
        public string HotAllotNotAcceptFull
        {
            get { return _HotAllotNotAcceptFull; }
            set { _HotAllotNotAcceptFull = value; }
        }

        string _HotAllotWarnFull;
        public string HotAllotWarnFull
        {
            get { return _HotAllotWarnFull; }
            set { _HotAllotWarnFull = value; }
        }

        string _HotAllotNotFullGA;
        public string HotAllotNotFullGA
        {
            get { return _HotAllotNotFullGA; }
            set { _HotAllotNotFullGA = value; }
        }

        string _DepServiceType;
        public string DepServiceType
        {
            get { return _DepServiceType; }
            set { _DepServiceType = value; }
        }

        string _RetServiceType;
        public string RetServiceType
        {
            get { return _RetServiceType; }
            set { _RetServiceType = value; }
        }

        string _DFlight;
        public string DFlight
        {
            get { return _DFlight; }
            set { _DFlight = value; }
        }

        int? _HotelRecID;
        public int? HotelRecID
        {
            get { return _HotelRecID; }
            set { _HotelRecID = value; }
        }

        bool? _AutoStop;
        public bool? AutoStop
        {
            get { return _AutoStop; }
            set { _AutoStop = value; }
        }

        Int16? _TransportType;
        public Int16? TransportType
        {
            get { return _TransportType; }
            set { _TransportType = value; }
        }

        List<AgeGroup> _AgeGroupList;
        public List<AgeGroup> AgeGroupList
        {
            get { return _AgeGroupList; }
            set { _AgeGroupList = value; }
        }

        bool _UseDynamicFlights;
        public bool UseDynamicFlights
        {
            get { return _UseDynamicFlights; }
            set { _UseDynamicFlights = value; }
        }

        dynamicFlights _DynamicFlights;
        public dynamicFlights DynamicFlights
        {
            get { return _DynamicFlights; }
            set { _DynamicFlights = value; }
        }

        Guid? _LogID;
        public Guid? LogID
        {
            get { return _LogID; }
            set { _LogID = value; }
        }

        int? _PartNo;
        public int? PartNo
        {
            get { return _PartNo; }
            set { _PartNo = value; }
        }

        int? _PriceID;
        public int? PriceID
        {
            get { return _PriceID; }
            set { _PriceID = value; }
        }

        DateTime _SearchTime;
        public DateTime SearchTime
        {
            get { return _SearchTime; }
            set { _SearchTime = value; }
        }

        int? _OrderHotel;
        public int? OrderHotel
        {
            get { return _OrderHotel; }
            set { _OrderHotel = value; }
        }

        Int16? _HotelFreeAllot;
        public Int16? HotelFreeAllot
        {
            get { return _HotelFreeAllot; }
            set { _HotelFreeAllot = value; }
        }

        object _G7Customer;
        public object G7Customer
        {
            get { return _G7Customer; }
            set { _G7Customer = value; }
        }

    }

    [Serializable]
    public class dynamicFlights
    {
        public string DFlightNo { get; set; }
        public string DFClass1 { get; set; }
        public int? DFUnit1 { get; set; }
        public string DFClass2 { get; set; }
        public int? DFUnit2 { get; set; }
        public string DFClass3 { get; set; }
        public int? DFUnit3 { get; set; }
        public string RFlightNo { get; set; }
        public string RFClass1 { get; set; }
        public int? RFUnit1 { get; set; }
        public string RFClass2 { get; set; }
        public int? RFUnit2 { get; set; }
        public string RFClass3 { get; set; }
        public int? RFUnit3 { get; set; }
        public decimal? OldPrice { get; set; }
        public decimal? Price { get; set; }

        public dynamicFlights()
        {
        }
    }

    [Serializable()]
    public class AgeGroup
    {
        public AgeGroup()
        {
        }

        int? _GroupNo;
        public int? GroupNo
        {
            get { return _GroupNo; }
            set { _GroupNo = value; }
        }

        int? _RefNo;
        public int? RefNo
        {
            get { return _RefNo; }
            set { _RefNo = value; }
        }

        AgeGroupType _GroupType;
        public AgeGroupType GroupType
        {
            get { return _GroupType; }
            set { _GroupType = value; }
        }

        Int16? _Unit;
        public Int16? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        decimal? _Age;
        public decimal? Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        DateTime? _DateOfBirth;
        public DateTime? DateOfBirth
        {
            get { return _DateOfBirth; }
            set { _DateOfBirth = value; }
        }
    }

    public class UniquePriceRow
    {
        public UniquePriceRow()
        {
            _PriceCalc = false;
        }

        int _RefNo;
        public int RefNo
        {
            get { return _RefNo; }
            set { _RefNo = value; }
        }

        int _CatPackID;
        public int CatPackID
        {
            get { return _CatPackID; }
            set { _CatPackID = value; }
        }

        int _ARecNo;
        public int ARecNo
        {
            get { return _ARecNo; }
            set { _ARecNo = value; }
        }

        int _PRecNo;
        public int PRecNo
        {
            get { return _PRecNo; }
            set { _PRecNo = value; }
        }

        int _HapRecID;
        public int HapRecID
        {
            get { return _HapRecID; }
            set { _HapRecID = value; }
        }

        bool _PriceCalc;
        public bool PriceCalc
        {
            get { return _PriceCalc; }
            set { _PriceCalc = value; }
        }
    }

    public class MultiRoomSelection
    {
        public MultiRoomSelection()
        {
            _RoomPriceList = new List<int>();
            _PriceCalc = false;
        }

        int _RoomGroupRefNo;
        public int RoomGroupRefNo
        {
            get { return _RoomGroupRefNo; }
            set { _RoomGroupRefNo = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        DateTime _CheckIn;
        public DateTime CheckIn
        {
            get { return _CheckIn; }
            set { _CheckIn = value; }
        }

        Int16 _Night;
        public Int16 Night
        {
            get { return _Night; }
            set { _Night = value; }
        }

        List<int> _RoomPriceList;
        public List<int> RoomPriceList
        {
            get { return _RoomPriceList; }
            set { _RoomPriceList = value; }
        }

        bool _PriceCalc;
        public bool PriceCalc
        {
            get { return _PriceCalc; }
            set { _PriceCalc = value; }
        }

        Int16? _RoomCount;
        public Int16? RoomCount
        {
            get { return _RoomCount; }
            set { _RoomCount = value; }
        }

        decimal? _MinPrice;
        public decimal? MinPrice
        {
            get { return _MinPrice; }
            set { _MinPrice = value; }
        }

        int? _OrderDisplay;
        public int? OrderDisplay
        {
            get { return _OrderDisplay; }
            set { _OrderDisplay = value; }
        }
    }

    public class MultiRoomAccomGroup
    {
        public MultiRoomAccomGroup()
        {
        }

        string _AccomName;
        public string AccomName
        {
            get { return _AccomName; }
            set { _AccomName = value; }
        }

        int _RoomGroupRefNo;
        public int RoomGroupRefNo
        {
            get { return _RoomGroupRefNo; }
            set { _RoomGroupRefNo = value; }
        }
    }

    public class MultiRoomResult
    {
        public MultiRoomResult()
        {
            _CurrentPage = 1;
            _PageRowCount = 10;
            _FirstGroup = new List<MultiRoomSelection>();
            _ResultSearch = new List<SearchResult>();
            _ResultSearchOH = new List<SearchResultOH>();
            _filteredResultSearch = new List<SearchResult>();
            _filteredResultSearchOH = new List<SearchResultOH>();
            _filterResult = new ResultFilter();
            _brochureCheck = new List<BrochureCheckRecord>();
        }

        int _CurrentPage;
        public int CurrentPage
        {
            get { return _CurrentPage; }
            set { _CurrentPage = value; }
        }

        int _PageRowCount;
        public int PageRowCount
        {
            get { return _PageRowCount; }
            set { _PageRowCount = value; }
        }

        int _TotalPageCount;
        public int TotalPageCount
        {
            get { return _TotalPageCount; }
            set { _TotalPageCount = value; }
        }

        SearchType _SearchType;
        public SearchType SearchType
        {
            get { return _SearchType; }
            set { _SearchType = value; }
        }

        List<MultiRoomSelection> _Group;
        public List<MultiRoomSelection> Group
        {
            get { return _Group; }
            set { _Group = value; }
        }

        List<MultiRoomSelection> _FirstGroup;
        public List<MultiRoomSelection> FirstGroup
        {
            get { return _FirstGroup; }
            set { _FirstGroup = value; }
        }

        List<SearchResult> _ResultSearch;
        public List<SearchResult> ResultSearch
        {
            get { return _ResultSearch; }
            set { _ResultSearch = value; }
        }

        List<SearchResultOH> _ResultSearchOH;
        public List<SearchResultOH> ResultSearchOH
        {
            get { return _ResultSearchOH; }
            set { _ResultSearchOH = value; }
        }

        List<SearchResult> _filteredResultSearch;
        public List<SearchResult> FilteredResultSearch
        {
            get { return _filteredResultSearch; }
            set { _filteredResultSearch = value; }
        }

        List<SearchResultOH> _filteredResultSearchOH;
        public List<SearchResultOH> FilteredResultSearchOH
        {
            get { return _filteredResultSearchOH; }
            set { _filteredResultSearchOH = value; }
        }

        List<HotelMarOptRecord> _HotelMarOpt;
        public List<HotelMarOptRecord> HotelMarOpt
        {
            get { return _HotelMarOpt; }
            set { _HotelMarOpt = value; }
        }

        List<FlightDetailRecord> _FlightInfo;
        public List<FlightDetailRecord> FlightInfo
        {
            get { return _FlightInfo; }
            set { _FlightInfo = value; }
        }

        ResultFilter _filterResult;
        public ResultFilter FilterResult
        {
            get { return _filterResult; }
            set { _filterResult = value; }
        }

        List<BrochureCheckRecord> _brochureCheck;
        public List<BrochureCheckRecord> brochureCheck
        {
            get { return _brochureCheck; }
            set { _brochureCheck = value; }
        }
    }

    public class BrochureCheckRecord
    {
        public BrochureCheckRecord()
        {
            _Brochure = false;
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        bool _Brochure;
        public bool Brochure
        {
            get { return _Brochure; }
            set { _Brochure = value; }
        }
    }

    public class OnlyTicketCriteria
    {
        public OnlyTicketCriteria()
        {
            _allFlight = false;
        }

        DateTime? _FlyDateBeg;
        public DateTime? FlyDateBeg
        {
            get { return _FlyDateBeg; }
            set { _FlyDateBeg = value; }
        }

        DateTime? _FlyDateEnd;
        public DateTime? FlyDateEnd
        {
            get { return _FlyDateEnd; }
            set { _FlyDateEnd = value; }
        }

        Int16? _Night1;
        public Int16? Night1
        {
            get { return _Night1; }
            set { _Night1 = value; }
        }

        Int16? _Night2;
        public Int16? Night2
        {
            get { return _Night2; }
            set { _Night2 = value; }
        }

        Int16? _Adult;
        public Int16? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16? _Infant;
        public Int16? Infant
        {
            get { return _Infant; }
            set { _Infant = value; }
        }

        Int16? _Child;
        public Int16? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        bool? _CurControl;
        public bool? CurControl
        {
            get { return _CurControl; }
            set { _CurControl = value; }
        }

        string _FlyClass;
        public string FlyClass
        {
            get { return _FlyClass; }
            set { _FlyClass = value; }
        }

        bool _isRT;
        public bool IsRT
        {
            get { return _isRT; }
            set { _isRT = value; }
        }

        bool _b2bAllClass;
        public bool B2bAllClass
        {
            get { return _b2bAllClass; }
            set { _b2bAllClass = value; }
        }

        string _b2bClass;
        public string B2bClass
        {
            get { return _b2bClass; }
            set { _b2bClass = value; }
        }

        bool _allFlight;
        public bool allFlight
        {
            get { return _allFlight; }
            set { _allFlight = value; }
        }

        List<calendarColor> _FlightDays;
        public List<calendarColor> FlightDays
        {
            get { return _FlightDays; }
            set { _FlightDays = value; }
        }

        int? _FlightType;
        public int? FlightType
        {
            get { return _FlightType; }
            set { _FlightType = value; }
        }
    }

    public class packageSearchFilterFormData
    {
        public packageSearchFilterFormData()
        {
            _DisableRoomFilter = false;
        }

        string _fltCheckInDay;
        public string fltCheckInDay
        {
            get { return _fltCheckInDay; }
            set { _fltCheckInDay = value; }
        }

        string _fltNight;
        public string fltNight
        {
            get { return _fltNight; }
            set { _fltNight = value; }
        }

        string _fltNight2;
        public string fltNight2
        {
            get { return _fltNight2; }
            set { _fltNight2 = value; }
        }

        string _fltRoomCount;
        public string fltRoomCount
        {
            get { return _fltRoomCount; }
            set { _fltRoomCount = value; }
        }

        bool _CurrencyConvert;
        public bool CurrencyConvert
        {
            get { return _CurrencyConvert; }
            set { _CurrencyConvert = value; }
        }

        string _MarketCur;
        public string MarketCur
        {
            get { return _MarketCur; }
            set { _MarketCur = value; }
        }

        string _CheckIn;
        public string CheckIn
        {
            get { return _CheckIn; }
            set { _CheckIn = value; }
        }

        bool _ShowStopSaleHotels;
        public bool ShowStopSaleHotels
        {
            get { return _ShowStopSaleHotels; }
            set { _ShowStopSaleHotels = value; }
        }

        string _CustomRegID;
        public string CustomRegID
        {
            get { return _CustomRegID; }
            set { _CustomRegID = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        bool _ShowExpandDay;
        public bool ShowExpandDay
        {
            get { return _ShowExpandDay; }
            set { _ShowExpandDay = value; }
        }

        bool _ShowSecondDay;
        public bool ShowSecondDay
        {
            get { return _ShowSecondDay; }
            set { _ShowSecondDay = value; }
        }

        string _dateFormat;
        public string DateFormat
        {
            get { return _dateFormat; }
            set { _dateFormat = value; }
        }

        bool? _ShowFilterPackage;
        public bool? ShowFilterPackage
        {
            get { return _ShowFilterPackage; }
            set { _ShowFilterPackage = value; }
        }

        bool? _ShowOfferBtn;
        public bool? ShowOfferBtn
        {
            get { return _ShowOfferBtn; }
            set { _ShowOfferBtn = value; }
        }

        bool _DisableRoomFilter;
        public bool DisableRoomFilter
        {
            get { return _DisableRoomFilter; }
            set { _DisableRoomFilter = value; }
        }
    }

    public class fltComboDayRecord
    {
        public List<Int32> maxDayList { get; set; }
        public Int16 selectedDay { get; set; }
    }

    public class packageSearchFilterFormDataV2
    {
        public bool CurrencyConvert { get; set; }
        public bool DisableRoomFilter { get; set; }
        public bool ShowDepCity { get; set; }
        public bool ShowArrCity { get; set; }
        public bool ShowExpandDay { get; set; }
        public bool ShowFirstDay { get; set; }
        public bool ShowSecondDay { get; set; }
        public bool ShowStopSaleHotels { get; set; }
        public bool? ShowFilterPackage { get; set; }
        public bool? ShowOfferBtn { get; set; }
        public DateTime? CheckIn { get; set; }
        public fltComboDayRecord fltCheckInDay { get; set; }
        public fltComboDayRecord fltNight { get; set; }
        public fltComboDayRecord fltNight2 { get; set; }
        public fltComboDayRecord fltRoomCount { get; set; }
        public string CustomRegID { get; set; }
        public string DateFormat { get; set; }
        public string Market { get; set; }
        public string MarketCur { get; set; }
        public packageSearchFilterFormDataV2()
        {
            this.DisableRoomFilter = false;
            this.ShowFirstDay = true;
            this.ShowDepCity = true;
            this.ShowArrCity = true;
        }
    }

    public class longDayPrice
    {
        public longDayPrice()
        {
        }

        Int16? _Day;
        public Int16? Day
        {
            get { return _Day; }
            set { _Day = value; }
        }

        decimal? _Price;
        public decimal? Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
    }

    public class longTotalMinMaxPrice
    {
        public longTotalMinMaxPrice()
        {
            _Price = new List<longDayPrice>();
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        List<longDayPrice> _Price;
        public List<longDayPrice> Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
    }

    public class ResultFilter
    {
        public ResultFilter()
        {
            _filtered = false;
        }

        bool _filtered;
        public bool filtered
        {
            get { return _filtered; }
            set { _filtered = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        int? _HotelLocation;
        public int? HotelLocation
        {
            get { return _HotelLocation; }
            set { _HotelLocation = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        DateTime? _CheckIn;
        public DateTime? CheckIn
        {
            get { return _CheckIn; }
            set { _CheckIn = value; }
        }

        Int16? _Night;
        public Int16? Night
        {
            get { return _Night; }
            set { _Night = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

    }

    public class B2BMenuCat
    {
        public B2BMenuCat()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        bool? _Status;
        public bool? Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
    }

    [Serializable()]
    public class ValidDates
    {
        public ValidDates()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        DateTime? _ValidDate;
        public DateTime? ValidDate
        {
            get { return _ValidDate; }
            set { _ValidDate = value; }
        }
    }

    [Serializable()]
    public class hotelGroups
    {
        public hotelGroups()
        {
            _useDynamicFlights = false;
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        DateTime? _CheckIn;
        public DateTime? CheckIn
        {
            get { return _CheckIn; }
            set { _CheckIn = value; }
        }

        Int16? _Night;
        public Int16? Night
        {
            get { return _Night; }
            set { _Night = value; }
        }

        decimal? _MinPrice;
        public decimal? MinPrice
        {
            get { return _MinPrice; }
            set { _MinPrice = value; }
        }

        int _roomCount;
        public int roomCount
        {
            get { return _roomCount; }
            set { _roomCount = value; }
        }

        bool _useDynamicFlights;
        public bool useDynamicFlights
        {
            get { return _useDynamicFlights; }
            set { _useDynamicFlights = value; }
        }

        int? _OrderHotel;
        public int? OrderHotel
        {
            get { return _OrderHotel; }
            set { _OrderHotel = value; }
        }
    }

    [Serializable()]
    public class AdvertisingRecord
    {
        public AdvertisingRecord()
        {
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _AdvUrl;
        public string AdvUrl
        {
            get { return _AdvUrl; }
            set { _AdvUrl = value; }
        }
    }

    [Serializable()]
    public class OnlyHotelSearchFilterData
    {
        public string Hotel { get; set; }
        public string HotelName { get; set; }
        public string HotelNameL { get; set; }
        public string Room { get; set; }
        public string RoomName { get; set; }
        public string RoomNameL { get; set; }
        public string Board { get; set; }
        public string BoardName { get; set; }
        public string BoardNameL { get; set; }
        public string Category { get; set; }
        public string CategoryName { get; set; }
        public string CategoryNameL { get; set; }
        public int? Location { get; set; }
        public string LocationName { get; set; }
        public string LocationNameL { get; set; }
        public int? City { get; set; }
        public string CityName { get; set; }
        public string CityNameL { get; set; }
        public int? Country { get; set; }
        public string CountryName { get; set; }
        public string CountryNameL { get; set; }

        public OnlyHotelSearchFilterData()
        {
        }
    }

    public class OnlyHotelSearchFilterCity
    {
        public int? Country { get; set; }
        public string CountryName { get; set; }
        public int? City { get; set; }
        public string CityName { get; set; }

        public OnlyHotelSearchFilterCity()
        {
        }
    }

    [Serializable()]
    public class SearchResultOH
    {
        public int RefNo { get; set; }
        public int? RoomNr { get; set; }
        public int? PartNo { get; set; }
        public Int64? PriceID { get; set; }
        public string Market { get; set; }
        public int? HotelID { get; set; }
        public string Hotel { get; set; }
        public int? RoomID { get; set; }
        public string Room { get; set; }
        public int? AccomID { get; set; }
        public string Accom { get; set; }
        public int? BoardID { get; set; }
        public string Board { get; set; }
        public DateTime? CheckIn { get; set; }
        public Int16? Night { get; set; }
        public string OrgCur { get; set; }
        public string SaleCur { get; set; }
        public decimal? ContractPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? AdlPrice { get; set; }
        public decimal? EBed1Price { get; set; }
        public decimal? ChdG1Price { get; set; }
        public decimal? ChdG2Price { get; set; }
        public decimal? ChdG3Price { get; set; }
        public decimal? ChdG4Price { get; set; }
        public decimal? CAdlPrice { get; set; }
        public decimal? CEBed1Price { get; set; }
        public decimal? CChdG1Price { get; set; }
        public decimal? CChdG2Price { get; set; }
        public decimal? CChdG3Price { get; set; }
        public decimal? CChdG4Price { get; set; }
        public decimal? ChdG1Age1 { get; set; }
        public decimal? ChdG1Age2 { get; set; }
        public decimal? ChdG2Age1 { get; set; }
        public decimal? ChdG2Age2 { get; set; }
        public decimal? ChdG3Age1 { get; set; }
        public decimal? ChdG3Age2 { get; set; }
        public decimal? ChdG4Age1 { get; set; }
        public decimal? ChdG4Age2 { get; set; }
        public string HotelName { get; set; }
        public string HotelNameL { get; set; }
        public string HotCat { get; set; }
        public string CatName { get; set; }
        public string CatNameL { get; set; }
        public int? HotLocation { get; set; }
        public string HotLocationName { get; set; }
        public string HotLocationNameL { get; set; }
        public string RoomName { get; set; }
        public string RoomNameL { get; set; }
        public string AccomName { get; set; }
        public string AccomNameL { get; set; }
        public string BoardName { get; set; }
        public string BoardNameL { get; set; }
        public Int16? StopSaleGuar { get; set; }
        public Int16? StopSaleStd { get; set; }
        public int? Allotment { get; set; }
        public int? StdAdult { get; set; }
        public int? AdultCount { get; set; }
        public int? ChildCount { get; set; }
        public int? ChdAge1 { get; set; }
        public int? ChdAge2 { get; set; }
        public int? ChdAge3 { get; set; }
        public int? ChdAge4 { get; set; }
        public Int16? OrderHotel { get; set; }
        public string HotSPO { get; set; }
        public int BookID { get; set; }
        public SearchResultOH()
        {
            this.BookID = 1;
        }
    }

    [Serializable()]
    public class HotelGroup
    {
        public int HotelGroupID { get; set; }
        public int? SortingID { get; set; }
        public bool showHotelImage { get; set; }
        public string hotelImageUrl { get; set; }
        public string hotelInfoUrl { get; set; }
        public string HotelName { get; set; }
        public string HotelCatName { get; set; }
        public string HotelLocationName { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public string Night { get; set; }
        public decimal MinPrice { get; set; }
        public string SelectedPrice { get; set; }
        public string SelectedPriceCur { get; set; }
        public List<PriceDetail> priceDetail { get; set; }
        public bool? Parameter1 { get; set; }
        public string Parameter2 { get; set; }
        public int? Parameter3 { get; set; }
        public Int16? OrderHotel { get; set; }
        public bool buttonStopSale { get; set; }
        public HotelGroup()
        {
            this.priceDetail = new List<PriceDetail>();
            this.MinPrice = 0;
            this.buttonStopSale = false;
        }
    }

    [Serializable()]
    public class PriceDetail
    {
        public int PriceDetailID { get; set; }
        public string AccomInfo { get; set; }
        public string labelFreeRoom { get; set; }
        public string labelRoomName { get; set; }
        public string labelBoardName { get; set; }
        public string labelAccomName { get; set; }
        public string labelPrice { get; set; }
        public string labelSelected { get; set; }
        public string labelOffer { get; set; }
        public List<AccomPrice> accomPrice { get; set; }
        public decimal selectedPrice { get; set; }
        public string priceCurr { get; set; }

        public PriceDetail()
        {
            this.accomPrice = new List<AccomPrice>();
            this.selectedPrice = (decimal)0;
        }
    }

    [Serializable()]
    public class AccomPrice
    {
        public int RefNo { get; set; }
        public int RoomNr { get; set; }
        public long PriceID { get; set; }
        public string FreeRoom { get; set; }
        public string RoomName { get; set; }
        public string BoardName { get; set; }
        public string AccomName { get; set; }
        public string SalePrice { get; set; }
        public decimal? Price { get; set; }
        public string StopSaleMessage { get; set; }
        public string StopSale { get; set; }
        public string SaleCur { get; set; }
        public string PriceNoComma { get; set; }
        public bool Selected { get; set; }
        public bool Discount { get; set; }
        public bool priceOddEven { get; set; }
        public bool showStopSale { get; set; }
        public string discountMsg { get; set; }
        public AccomPrice()
        {
            this.Selected = false;
            this.Discount = false;
            this.priceOddEven = false;
            this.showStopSale = false;
        }
    }

    [Serializable()]
    public class OnlyHotelResultGroupList
    {
        public int SeqNo { get; set; }
        public string Hotel { get; set; }
        public DateTime? CheckIn { get; set; }
        public Int16? Night { get; set; }
        List<SearchResultOH> HotelList { get; set; }

        public OnlyHotelResultGroupList()
        {
        }
    }

    public class OnlyHotelResultGroup
    {
        public int PageRowCount { get; set; }

        List<OnlyHotelResultGroupList> HotelGroupList { get; set; }

        public OnlyHotelResultGroup()
        {
        }
    }

    [Serializable()]
    public class FilterItem
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }

        public FilterItem()
        {
            this.Selected = false;
        }
    }

    [Serializable()]
    public class OnlyHotelResultFilter
    {
        public bool showDeparture { get; set; }
        public List<FilterItem> Departure { get; set; }
        public bool showArrival { get; set; }
        public List<FilterItem> Arrival { get; set; }
        public bool showHotelLocation { get; set; }
        public List<FilterItem> HotelLocation { get; set; }
        public bool showCategory { get; set; }
        public List<FilterItem> Category { get; set; }
        public bool showHotel { get; set; }
        public List<FilterItem> Hotel { get; set; }
        public bool showCheckIn { get; set; }
        public List<FilterItem> CheckIn { get; set; }
        public bool showNight { get; set; }
        public List<FilterItem> Night { get; set; }
        public bool showRoom { get; set; }
        public List<FilterItem> Room { get; set; }
        public bool showBoard { get; set; }
        public List<FilterItem> Board { get; set; }

        public OnlyHotelResultFilter()
        {
            this.showDeparture = false;
            this.Departure = new List<FilterItem>();
            this.showArrival = false;
            this.Arrival = new List<FilterItem>();
            this.showHotelLocation = false;
            this.HotelLocation = new List<FilterItem>();
            this.showCategory = false;
            this.Category = new List<FilterItem>();
            this.showHotel = false;
            this.Hotel = new List<FilterItem>();
            this.showCheckIn = false;
            this.CheckIn = new List<FilterItem>();
            this.showNight = false;
            this.Night = new List<FilterItem>();
            this.showRoom = false;
            this.Room = new List<FilterItem>();
            this.showBoard = false;
            this.Board = new List<FilterItem>();
        }
    }

    public class TourCultureFilterDefaults
    {
        public Int16? DefaultNight1 { get; set; }
        public Int16? DefaultNight2 { get; set; }
        public Int16? DefaultMaxNight { get; set; }
        public Int16? DefaultExpandDay { get; set; }
        public Int16? DefaultMaxExpandDay { get; set; }
        public TourCultureFilterDefaults()
        {
            this.DefaultNight1 = 7;
            this.DefaultNight2 = 7;
            this.DefaultMaxNight = 21;
            this.DefaultExpandDay = 0;
            this.DefaultMaxExpandDay = 28;
        }
    }

    [Serializable()]
    public class HotelSaleResultFilterList
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public bool Select { get; set; }
        public HotelSaleResultFilterList()
        {
            this.Select = false;
        }
    }

    [Serializable()]
    public class HotelSaleResultFilter
    {
        public bool Filtered { get; set; }
        public string FilterResortSelect { get; set; }
        public List<HotelSaleResultFilterList> FilterResort { get; set; }
        public string FilterCategorySelect { get; set; }
        public List<HotelSaleResultFilterList> FilterCategory { get; set; }
        public string FilterHotelSelect { get; set; }
        public List<HotelSaleResultFilterList> FilterHotel { get; set; }
        public string FilterRoomSelect { get; set; }
        public List<HotelSaleResultFilterList> FilterRoom { get; set; }
        public string FilterBoardSelect { get; set; }
        public List<HotelSaleResultFilterList> FilterBoard { get; set; }
        public HotelSaleResultFilter()
        {
            this.Filtered = false;
            this.FilterResort = new List<HotelSaleResultFilterList>();
            this.FilterCategory = new List<HotelSaleResultFilterList>();
            this.FilterHotel = new List<HotelSaleResultFilterList>();
            this.FilterRoom = new List<HotelSaleResultFilterList>();
            this.FilterBoard = new List<HotelSaleResultFilterList>();
        }
    }

    [Serializable()]
    public class PackPriceFilterCache
    {
        public int? MarketID { get; set; }
        public int? HolPackID { get; set; }
        public int? DepCityID { get; set; }
        public int? ArrCityID { get; set; }
        public int? HotelID { get; set; }
        public int? RoomID { get; set; }
        public int? AccomID { get; set; }
        public int? BoardID { get; set; }
        public PackPriceFilterCache()
        {

        }
    }

    [Serializable()]
    public class PackPriceFlightDayCache
    {
        public int MarketID { get; set; }
        public DateTime FlyDate { get; set; }
        public int ArrCity { get; set; }
        public int DepCity { get; set; }
        public string DepAirport { get; set; }
        public string ArrAirport { get; set; }
        public string Flights { get; set; }
        public DateTime? DepTime { get; set; }
        public DateTime? ArrTime { get; set; }
        public string ServiceType { get; set; }
        public PackPriceFlightDayCache()
        {
            this.ServiceType = "F";
        }
    }
}
