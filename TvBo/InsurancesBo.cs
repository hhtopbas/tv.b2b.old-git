﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class InsuranceRecord
    {
        public InsuranceRecord()
        { 
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _InsZone;
        public string InsZone
        {
            get { return _InsZone; }
            set { _InsZone = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        string _CoverExplain;
        public string CoverExplain
        {
            get { return _CoverExplain; }
            set { _CoverExplain = value; }
        }

        Int16 _ConfStat;
        public Int16 ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        decimal? _Coverage;
        public decimal? Coverage
        {
            get { return _Coverage; }
            set { _Coverage = value; }
        }

        string _CoverageCur;
        public string CoverageCur
        {
            get { return _CoverageCur; }
            set { _CoverageCur = value; }
        }

        Int16? _MaxPaxPolicy;
        public Int16? MaxPaxPolicy
        {
            get { return _MaxPaxPolicy; }
            set { _MaxPaxPolicy = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool _PayCom;
        public bool PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        bool _PayComEB;
        public bool PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        Int16 _InsType;
        public Int16 InsType
        {
            get { return _InsType; }
            set { _InsType = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        decimal? _Franchise;
        public decimal? Franchise
        {
            get { return _Franchise; }
            set { _Franchise = value; }
        }

        bool? _B2BPub;
        public bool? B2BPub
        {
            get { return _B2BPub; }
            set { _B2BPub = value; }
        }

        bool? _B2CPub;
        public bool? B2CPub
        {
            get { return _B2CPub; }
            set { _B2CPub = value; }
        }
    }

    public class InsurancePriceRecord
    {
        public InsurancePriceRecord()
        { 
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Insurance;
        public string Insurance
        {
            get { return _Insurance; }
            set { _Insurance = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        int? _Country;
        public int? Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; }
        }

        string _CountryNameL;
        public string CountryNameL
        {
            get { return _CountryNameL; }
            set { _CountryNameL = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _FromDay;
        public Int16? FromDay
        {
            get { return _FromDay; }
            set { _FromDay = value; }
        }

        Int16? _ToDay;
        public Int16? ToDay
        {
            get { return _ToDay; }
            set { _ToDay = value; }
        }

        decimal? _NetPrice;
        public decimal? NetPrice
        {
            get { return _NetPrice; }
            set { _NetPrice = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        string _SaleType;
        public string SaleType
        {
            get { return _SaleType; }
            set { _SaleType = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        string _AutoCalc;
        public string AutoCalc
        {
            get { return _AutoCalc; }
            set { _AutoCalc = value; }
        }

        decimal? _ComVal;
        public decimal? ComVal
        {
            get { return _ComVal; }
            set { _ComVal = value; }
        }

        decimal? _ComPer;
        public decimal? ComPer
        {
            get { return _ComPer; }
            set { _ComPer = value; }
        }

        decimal? _NetChdG1Price;
        public decimal? NetChdG1Price
        {
            get { return _NetChdG1Price; }
            set { _NetChdG1Price = value; }
        }

        decimal? _NetChdG2Price;
        public decimal? NetChdG2Price
        {
            get { return _NetChdG2Price; }
            set { _NetChdG2Price = value; }
        }

        decimal? _NetChdG3Price;
        public decimal? NetChdG3Price
        {
            get { return _NetChdG3Price; }
            set { _NetChdG3Price = value; }
        }

        decimal? _SaleChdG1Price;
        public decimal? SaleChdG1Price
        {
            get { return _SaleChdG1Price; }
            set { _SaleChdG1Price = value; }
        }

        decimal? _SaleChdG2Price;
        public decimal? SaleChdG2Price
        {
            get { return _SaleChdG2Price; }
            set { _SaleChdG2Price = value; }
        }

        decimal? _SaleChdG3Price;
        public decimal? SaleChdG3Price
        {
            get { return _SaleChdG3Price; }
            set { _SaleChdG3Price = value; }
        }

        decimal? _ChdG1MaxAge;
        public decimal? ChdG1MaxAge
        {
            get { return _ChdG1MaxAge; }
            set { _ChdG1MaxAge = value; }
        }

        decimal? _ChdG2MaxAge;
        public decimal? ChdG2MaxAge
        {
            get { return _ChdG2MaxAge; }
            set { _ChdG2MaxAge = value; }
        }

        decimal? _ChdG3MaxAge;
        public decimal? ChdG3MaxAge
        {
            get { return _ChdG3MaxAge; }
            set { _ChdG3MaxAge = value; }
        }

        Int16? _FromPax;
        public Int16? FromPax
        {
            get { return _FromPax; }
            set { _FromPax = value; }
        }

        Int16? _ToPax;
        public Int16? ToPax
        {
            get { return _ToPax; }
            set { _ToPax = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        Int16? _CalcType;
        public Int16? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        bool? _Compulsory;
        public bool? Compulsory
        {
            get { return _Compulsory; }
            set { _Compulsory = value; }
        }

        decimal? _NetPer;
        public decimal? NetPer
        {
            get { return _NetPer; }
            set { _NetPer = value; }
        }
    }

}
