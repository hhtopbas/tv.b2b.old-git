﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Security.Permissions;
using System.Web.Services.Description;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Reflection;
using TvTools;
using System.Collections.Specialized;
using System.Web.Configuration;
using System.Net;

namespace TvBo
{
    public class WebServices
    {
        public static sejourLoginResult getSejourTicket()
        {
            string webServiceUrl = WebConfigurationManager.AppSettings["sejourWebServiceUrl"];
            sejourLogin loginParam = new sejourLogin();
            loginParam.DatabaseName = WebConfigurationManager.AppSettings["sejourWebService_databaseName"];
            loginParam.UserName = WebConfigurationManager.AppSettings["sejourWebService_userName"];
            loginParam.Password = WebConfigurationManager.AppSettings["sejourWebService_password"];
            loginParam.Language = WebConfigurationManager.AppSettings["sejourWebService_Language"];
                        
            string[] args = new string[] { 
                loginParam.DatabaseName, 
                loginParam.UserName, 
                loginParam.Password, 
                loginParam.Language
            };
            object webSrvObj = WsProxy.CallWebService(webServiceUrl + "/authentication.asmx", "authentication", "Login", args);
            sejourLoginResult webSrv = Newtonsoft.Json.JsonConvert.DeserializeObject<sejourLoginResult>(Newtonsoft.Json.JsonConvert.SerializeObject(webSrvObj));
            return webSrv;
        }

        public static bool sejourCheckTicket(string sejourTicket)
        {
            string webServiceUrl = WebConfigurationManager.AppSettings["sejourWebServiceUrl"];
            string[] args = new string[] { sejourTicket };
            object webSrvObj = WsProxy.CallWebService(webServiceUrl + "/authentication.asmx", "authentication", "CheckToken", args);
            bool ticket = Conversion.getBoolOrNull(webSrvObj).HasValue ? Conversion.getBoolOrNull(webSrvObj).Value : false;
            return ticket;
        }

        public static bool HotelAllotControlSejour(sejourHotelAllot param, ref string errorMsg)
        {
            string webServiceUrl = WebConfigurationManager.AppSettings["sejourWebServiceUrl"];
            object[] args = new object[] {
                                param.Token, //Sejour Ticket;
                                param.Tarih1,
                                param.Tarih2,
                                param.HotelCode,
                                param.KontTuru, //Kontenjan Türü
                                param.RoomTypeCode,
                                param.RoomCode,            
                                param.RoomCount,
                                param.IsUrun,
                                param.ReleaseErrorMessage, // Release error message
                                param.AlloterrorMessage // Allot error message
                            };
            object webSrvObj = WsProxy.CallWebService(webServiceUrl + "/Reservation.asmx", "Reservation", "New_Allotment_Control_ForOutgoing", args);

            if (Conversion.getBoolOrNull(webSrvObj).HasValue)
            {
                if (args[9] != null && !string.IsNullOrEmpty(args[9].ToString()))
                    errorMsg = args[9].ToString();
                if (args[10] != null && !string.IsNullOrEmpty(args[10].ToString()))
                {
                    if (errorMsg.Length > 0) errorMsg += " , ";
                    errorMsg += args[10].ToString();
                }
                if (Conversion.getBoolOrNull(webSrvObj).Value)
                    return true;
                else return false;                
            }
            else
            {
                if (args[9] != null && !string.IsNullOrEmpty(args[9].ToString()))
                    errorMsg = args[9].ToString();
                if (args[10] != null && !string.IsNullOrEmpty(args[10].ToString()))
                {
                    if (errorMsg.Length > 0) errorMsg += " , ";
                    errorMsg += args[10].ToString();
                }                
                return false;
            }
        }

        public static User getExtLogin(string token)
        {
            string webServiceUrl = WebConfigurationManager.AppSettings["ExtLoginWebServiceUrl"];
            string[] args = new string[] { token };
            object webSrvObj = WsProxy.CallWebService(webServiceUrl + "/login.asmx", "login", "getLogin", args);
            if (webSrvObj != null)
            {
                System.Data.DataTable webSrv = (System.Data.DataTable)webSrvObj;

                return new User();
            }
            else return null;
        }
    }

    internal class WsProxy
    {
        [SecurityPermissionAttribute(SecurityAction.Demand, Unrestricted = true)]
        public static object CallWebService(string webServiceAsmxUrl, string serviceName, string methodName, object[] args)
        {
            System.Net.WebClient client = new System.Net.WebClient();
            //-Connect To the web service
            Uri wsdlUrl = new Uri(webServiceAsmxUrl + "?wsdl");
            try
            {
                using (System.IO.Stream stream = client.OpenRead(wsdlUrl))
                {
                    //--Now read the WSDL file describing a service.
                    ServiceDescription description = ServiceDescription.Read(stream);
                    ///// LOAD THE DOM /////////
                    //--Initialize a service description importer.
                    ServiceDescriptionImporter importer = new ServiceDescriptionImporter();
                    importer.ProtocolName = "Soap12"; // Use SOAP 1.2.
                    importer.AddServiceDescription(description, null, null);
                    //--Generate a proxy client. importer.Style = ServiceDescriptionImportStyle.Client;
                    //--Generate properties to represent primitive values.
                    importer.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties;
                    //--Initialize a Code-DOM tree into which we will import the service.
                    CodeNamespace nmspace = new CodeNamespace();
                    CodeCompileUnit unit1 = new CodeCompileUnit();
                    unit1.Namespaces.Add(nmspace);
                    //--Import the service into the Code-DOM tree. This creates proxy code
                    //--that uses the service.
                    ServiceDescriptionImportWarnings warning = importer.Import(nmspace, unit1);
                    if (warning == 0) //--If zero then we are good to go
                    {
                        //--Generate the proxy code 
                        CodeDomProvider provider1 = CodeDomProvider.CreateProvider("CSharp");
                        //--Compile the assembly proxy with the appropriate references
                        string[] assemblyReferences = new string[5] { "System.dll", "System.Web.Services.dll", "System.Web.dll", "System.Xml.dll", "System.Data.dll" };
                        CompilerParameters parms = new CompilerParameters(assemblyReferences);
                        CompilerResults results = provider1.CompileAssemblyFromDom(parms, unit1);
                        //-Check For Errors
                        if (results.Errors.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            foreach (CompilerError oops in results.Errors)
                            {
                                sb.AppendLine("========Compiler error============");
                                sb.AppendLine(oops.ErrorText);
                            }
                            throw new System.ApplicationException("Compile Error Occured calling webservice. " + sb.ToString());
                        }
                        //--Finally, Invoke the web service method 
                        Type foundType = null;
                        Type[] types = results.CompiledAssembly.GetTypes();
                        foreach (Type type in types)
                        {
                            if (type.BaseType == typeof(System.Web.Services.Protocols.SoapHttpClientProtocol))
                            {
                                Console.WriteLine(type.ToString());
                                foundType = type;
                            }
                        }

                        object wsvcClass = results.CompiledAssembly.CreateInstance(foundType.ToString());
                        MethodInfo mi = wsvcClass.GetType().GetMethod(methodName);
                        return mi.Invoke(wsvcClass, args);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return null;
            }
        }
    }
    public class WebClientWithTimeout : WebClient
    {
        protected override WebRequest GetWebRequest(Uri address)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls|SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            WebRequest wr = base.GetWebRequest(address);
            wr.Timeout = 120 * 1000;
            return wr;
        }
    }
}
