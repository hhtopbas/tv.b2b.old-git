﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class Locations
    {
        public static List<CodeName> getISO3166CountryList()
        {
            List<CodeName> IsoList = new List<CodeName>();
            IsoList.Add(new CodeName { Code = "AF", Name = "AFGHANISTAN" });
            IsoList.Add(new CodeName { Code = "AX", Name = "ÅLAND ISLANDS" });
            IsoList.Add(new CodeName { Code = "AL", Name = "ALBANIA" });
            IsoList.Add(new CodeName { Code = "DZ", Name = "ALGERIA" });
            IsoList.Add(new CodeName { Code = "AS", Name = "AMERICAN SAMOA" });
            IsoList.Add(new CodeName { Code = "AD", Name = "ANDORRA" });
            IsoList.Add(new CodeName { Code = "AO", Name = "ANGOLA" });
            IsoList.Add(new CodeName { Code = "AI", Name = "ANGUILLA" });
            IsoList.Add(new CodeName { Code = "AQ", Name = "ANTARCTICA" });
            IsoList.Add(new CodeName { Code = "AG", Name = "ANTIGUA AND BARBUDA" });
            IsoList.Add(new CodeName { Code = "AR", Name = "ARGENTINA" });
            IsoList.Add(new CodeName { Code = "AM", Name = "ARMENIA" });
            IsoList.Add(new CodeName { Code = "AW", Name = "ARUBA" });
            IsoList.Add(new CodeName { Code = "AU", Name = "AUSTRALIA" });
            IsoList.Add(new CodeName { Code = "AT", Name = "AUSTRIA" });
            IsoList.Add(new CodeName { Code = "AZ", Name = "AZERBAIJAN" });
            IsoList.Add(new CodeName { Code = "BS", Name = "BAHAMAS" });
            IsoList.Add(new CodeName { Code = "BH", Name = "BAHRAIN" });
            IsoList.Add(new CodeName { Code = "BD", Name = "BANGLADESH" });
            IsoList.Add(new CodeName { Code = "BB", Name = "BARBADOS" });
            IsoList.Add(new CodeName { Code = "BY", Name = "BELARUS" });
            IsoList.Add(new CodeName { Code = "BE", Name = "BELGIUM" });
            IsoList.Add(new CodeName { Code = "BZ", Name = "BELIZE" });
            IsoList.Add(new CodeName { Code = "BJ", Name = "BENIN" });
            IsoList.Add(new CodeName { Code = "BM", Name = "BERMUDA" });
            IsoList.Add(new CodeName { Code = "BT", Name = "BHUTAN" });
            IsoList.Add(new CodeName { Code = "BO", Name = "BOLIVIA, PLURINATIONAL STATE OF" });
            IsoList.Add(new CodeName { Code = "BQ", Name = "BONAIRE, SINT EUSTATIUS AND SABA" });
            IsoList.Add(new CodeName { Code = "BA", Name = "BOSNIA AND HERZEGOVINA" });
            IsoList.Add(new CodeName { Code = "BW", Name = "BOTSWANA" });
            IsoList.Add(new CodeName { Code = "BV", Name = "BOUVET ISLAND" });
            IsoList.Add(new CodeName { Code = "BR", Name = "BRAZIL" });
            IsoList.Add(new CodeName { Code = "IO", Name = "BRITISH INDIAN OCEAN TERRITORY" });
            IsoList.Add(new CodeName { Code = "BN", Name = "BRUNEI DARUSSALAM" });
            IsoList.Add(new CodeName { Code = "BG", Name = "BULGARIA" });
            IsoList.Add(new CodeName { Code = "BF", Name = "BURKINA FASO" });
            IsoList.Add(new CodeName { Code = "BI", Name = "BURUNDI" });
            IsoList.Add(new CodeName { Code = "KH", Name = "CAMBODIA" });
            IsoList.Add(new CodeName { Code = "CM", Name = "CAMEROON" });
            IsoList.Add(new CodeName { Code = "CA", Name = "CANADA" });
            IsoList.Add(new CodeName { Code = "CV", Name = "CAPE VERDE" });
            IsoList.Add(new CodeName { Code = "KY", Name = "CAYMAN ISLANDS" });
            IsoList.Add(new CodeName { Code = "CF", Name = "CENTRAL AFRICAN REPUBLIC" });
            IsoList.Add(new CodeName { Code = "TD", Name = "CHAD" });
            IsoList.Add(new CodeName { Code = "CL", Name = "CHILE" });
            IsoList.Add(new CodeName { Code = "CN", Name = "CHINA" });
            IsoList.Add(new CodeName { Code = "CX", Name = "CHRISTMAS ISLAND" });
            IsoList.Add(new CodeName { Code = "CC", Name = "COCOS (KEELING) ISLANDS" });
            IsoList.Add(new CodeName { Code = "CO", Name = "COLOMBIA" });
            IsoList.Add(new CodeName { Code = "KM", Name = "COMOROS" });
            IsoList.Add(new CodeName { Code = "CG", Name = "CONGO" });
            IsoList.Add(new CodeName { Code = "CD", Name = "CONGO, THE DEMOCRATIC REPUBLIC OF THE" });
            IsoList.Add(new CodeName { Code = "CK", Name = "COOK ISLANDS" });
            IsoList.Add(new CodeName { Code = "CR", Name = "COSTA RICA" });
            IsoList.Add(new CodeName { Code = "CI", Name = "CÔTE D'IVOIRE" });
            IsoList.Add(new CodeName { Code = "HR", Name = "CROATIA" });
            IsoList.Add(new CodeName { Code = "CU", Name = "CUBA" });
            IsoList.Add(new CodeName { Code = "CW", Name = "CURAÇAO" });
            IsoList.Add(new CodeName { Code = "CY", Name = "CYPRUS" });
            IsoList.Add(new CodeName { Code = "CZ", Name = "CZECH REPUBLIC" });
            IsoList.Add(new CodeName { Code = "DK", Name = "DENMARK" });
            IsoList.Add(new CodeName { Code = "DJ", Name = "DJIBOUTI" });
            IsoList.Add(new CodeName { Code = "DM", Name = "DOMINICA" });
            IsoList.Add(new CodeName { Code = "DO", Name = "DOMINICAN REPUBLIC" });
            IsoList.Add(new CodeName { Code = "EC", Name = "ECUADOR" });
            IsoList.Add(new CodeName { Code = "EG", Name = "EGYPT" });
            IsoList.Add(new CodeName { Code = "SV", Name = "EL SALVADOR" });
            IsoList.Add(new CodeName { Code = "GQ", Name = "EQUATORIAL GUINEA" });
            IsoList.Add(new CodeName { Code = "ER", Name = "ERITREA" });
            IsoList.Add(new CodeName { Code = "EE", Name = "ESTONIA" });
            IsoList.Add(new CodeName { Code = "ET", Name = "ETHIOPIA" });
            IsoList.Add(new CodeName { Code = "FK", Name = "FALKLAND ISLANDS (MALVINAS)" });
            IsoList.Add(new CodeName { Code = "FO", Name = "FAROE ISLANDS" });
            IsoList.Add(new CodeName { Code = "FJ", Name = "FIJI" });
            IsoList.Add(new CodeName { Code = "FI", Name = "FINLAND" });
            IsoList.Add(new CodeName { Code = "FR", Name = "FRANCE" });
            IsoList.Add(new CodeName { Code = "GF", Name = "FRENCH GUIANA" });
            IsoList.Add(new CodeName { Code = "PF", Name = "FRENCH POLYNESIA" });
            IsoList.Add(new CodeName { Code = "TF", Name = "FRENCH SOUTHERN TERRITORIES" });
            IsoList.Add(new CodeName { Code = "GA", Name = "GABON" });
            IsoList.Add(new CodeName { Code = "GM", Name = "GAMBIA" });
            IsoList.Add(new CodeName { Code = "GE", Name = "GEORGIA" });
            IsoList.Add(new CodeName { Code = "DE", Name = "GERMANY" });
            IsoList.Add(new CodeName { Code = "GH", Name = "GHANA" });
            IsoList.Add(new CodeName { Code = "GI", Name = "GIBRALTAR" });
            IsoList.Add(new CodeName { Code = "GR", Name = "GREECE" });
            IsoList.Add(new CodeName { Code = "GL", Name = "GREENLAND" });
            IsoList.Add(new CodeName { Code = "GD", Name = "GRENADA" });
            IsoList.Add(new CodeName { Code = "GP", Name = "GUADELOUPE" });
            IsoList.Add(new CodeName { Code = "GU", Name = "GUAM" });
            IsoList.Add(new CodeName { Code = "GT", Name = "GUATEMALA" });
            IsoList.Add(new CodeName { Code = "GG", Name = "GUERNSEY" });
            IsoList.Add(new CodeName { Code = "GN", Name = "GUINEA" });
            IsoList.Add(new CodeName { Code = "GW", Name = "GUINEA-BISSAU" });
            IsoList.Add(new CodeName { Code = "GY", Name = "GUYANA" });
            IsoList.Add(new CodeName { Code = "HT", Name = "HAITI" });
            IsoList.Add(new CodeName { Code = "HM", Name = "HEARD ISLAND AND MCDONALD ISLANDS" });
            IsoList.Add(new CodeName { Code = "VA", Name = "HOLY SEE (VATICAN CITY STATE)" });
            IsoList.Add(new CodeName { Code = "HN", Name = "HONDURAS" });
            IsoList.Add(new CodeName { Code = "HK", Name = "HONG KONG" });
            IsoList.Add(new CodeName { Code = "HU", Name = "HUNGARY" });
            IsoList.Add(new CodeName { Code = "IS", Name = "ICELAND" });
            IsoList.Add(new CodeName { Code = "IN", Name = "INDIA" });
            IsoList.Add(new CodeName { Code = "ID", Name = "INDONESIA" });
            IsoList.Add(new CodeName { Code = "IR", Name = "IRAN, ISLAMIC REPUBLIC OF" });
            IsoList.Add(new CodeName { Code = "IQ", Name = "IRAQ" });
            IsoList.Add(new CodeName { Code = "IE", Name = "IRELAND" });
            IsoList.Add(new CodeName { Code = "IM", Name = "ISLE OF MAN" });
            IsoList.Add(new CodeName { Code = "IL", Name = "ISRAEL" });
            IsoList.Add(new CodeName { Code = "IT", Name = "ITALY" });
            IsoList.Add(new CodeName { Code = "JM", Name = "JAMAICA" });
            IsoList.Add(new CodeName { Code = "JP", Name = "JAPAN" });
            IsoList.Add(new CodeName { Code = "JE", Name = "JERSEY" });
            IsoList.Add(new CodeName { Code = "JO", Name = "JORDAN" });
            IsoList.Add(new CodeName { Code = "KZ", Name = "KAZAKHSTAN" });
            IsoList.Add(new CodeName { Code = "KE", Name = "KENYA" });
            IsoList.Add(new CodeName { Code = "KI", Name = "KIRIBATI" });
            IsoList.Add(new CodeName { Code = "KP", Name = "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF" });
            IsoList.Add(new CodeName { Code = "KR", Name = "KOREA, REPUBLIC OF" });
            IsoList.Add(new CodeName { Code = "KW", Name = "KUWAIT" });
            IsoList.Add(new CodeName { Code = "KG", Name = "KYRGYZSTAN" });
            IsoList.Add(new CodeName { Code = "LA", Name = "LAO PEOPLE'S DEMOCRATIC REPUBLIC" });
            IsoList.Add(new CodeName { Code = "LV", Name = "LATVIA" });
            IsoList.Add(new CodeName { Code = "LB", Name = "LEBANON" });
            IsoList.Add(new CodeName { Code = "LS", Name = "LESOTHO" });
            IsoList.Add(new CodeName { Code = "LR", Name = "LIBERIA" });
            IsoList.Add(new CodeName { Code = "LY", Name = "LIBYA" });
            IsoList.Add(new CodeName { Code = "LI", Name = "LIECHTENSTEIN" });
            IsoList.Add(new CodeName { Code = "LT", Name = "LITHUANIA" });
            IsoList.Add(new CodeName { Code = "LU", Name = "LUXEMBOURG" });
            IsoList.Add(new CodeName { Code = "MO", Name = "MACAO" });
            IsoList.Add(new CodeName { Code = "MK", Name = "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF" });
            IsoList.Add(new CodeName { Code = "MG", Name = "MADAGASCAR" });
            IsoList.Add(new CodeName { Code = "MW", Name = "MALAWI" });
            IsoList.Add(new CodeName { Code = "MY", Name = "MALAYSIA" });
            IsoList.Add(new CodeName { Code = "MV", Name = "MALDIVES" });
            IsoList.Add(new CodeName { Code = "ML", Name = "MALI" });
            IsoList.Add(new CodeName { Code = "MT", Name = "MALTA" });
            IsoList.Add(new CodeName { Code = "MH", Name = "MARSHALL ISLANDS" });
            IsoList.Add(new CodeName { Code = "MQ", Name = "MARTINIQUE" });
            IsoList.Add(new CodeName { Code = "MR", Name = "MAURITANIA" });
            IsoList.Add(new CodeName { Code = "MU", Name = "MAURITIUS" });
            IsoList.Add(new CodeName { Code = "YT", Name = "MAYOTTE" });
            IsoList.Add(new CodeName { Code = "MX", Name = "MEXICO" });
            IsoList.Add(new CodeName { Code = "FM", Name = "MICRONESIA, FEDERATED STATES OF" });
            IsoList.Add(new CodeName { Code = "MD", Name = "MOLDOVA, REPUBLIC OF" });
            IsoList.Add(new CodeName { Code = "MC", Name = "MONACO" });
            IsoList.Add(new CodeName { Code = "MN", Name = "MONGOLIA" });
            IsoList.Add(new CodeName { Code = "ME", Name = "MONTENEGRO" });
            IsoList.Add(new CodeName { Code = "MS", Name = "MONTSERRAT" });
            IsoList.Add(new CodeName { Code = "MA", Name = "MOROCCO" });
            IsoList.Add(new CodeName { Code = "MZ", Name = "MOZAMBIQUE" });
            IsoList.Add(new CodeName { Code = "MM", Name = "MYANMAR" });
            IsoList.Add(new CodeName { Code = "NA", Name = "NAMIBIA" });
            IsoList.Add(new CodeName { Code = "NR", Name = "NAURU" });
            IsoList.Add(new CodeName { Code = "NP", Name = "NEPAL" });
            IsoList.Add(new CodeName { Code = "NL", Name = "NETHERLANDS" });
            IsoList.Add(new CodeName { Code = "NC", Name = "NEW CALEDONIA" });
            IsoList.Add(new CodeName { Code = "NZ", Name = "NEW ZEALAND" });
            IsoList.Add(new CodeName { Code = "NI", Name = "NICARAGUA" });
            IsoList.Add(new CodeName { Code = "NE", Name = "NIGER" });
            IsoList.Add(new CodeName { Code = "NG", Name = "NIGERIA" });
            IsoList.Add(new CodeName { Code = "NU", Name = "NIUE" });
            IsoList.Add(new CodeName { Code = "NF", Name = "NORFOLK ISLAND" });
            IsoList.Add(new CodeName { Code = "MP", Name = "NORTHERN MARIANA ISLANDS" });
            IsoList.Add(new CodeName { Code = "NO", Name = "NORWAY" });
            IsoList.Add(new CodeName { Code = "OM", Name = "OMAN" });
            IsoList.Add(new CodeName { Code = "PK", Name = "PAKISTAN" });
            IsoList.Add(new CodeName { Code = "PW", Name = "PALAU" });
            IsoList.Add(new CodeName { Code = "PS", Name = "PALESTINIAN TERRITORY, OCCUPIED" });
            IsoList.Add(new CodeName { Code = "PA", Name = "PANAMA" });
            IsoList.Add(new CodeName { Code = "PG", Name = "PAPUA NEW GUINEA" });
            IsoList.Add(new CodeName { Code = "PY", Name = "PARAGUAY" });
            IsoList.Add(new CodeName { Code = "PE", Name = "PERU" });
            IsoList.Add(new CodeName { Code = "PH", Name = "PHILIPPINES" });
            IsoList.Add(new CodeName { Code = "PN", Name = "PITCAIRN" });
            IsoList.Add(new CodeName { Code = "PL", Name = "POLAND" });
            IsoList.Add(new CodeName { Code = "PT", Name = "PORTUGAL" });
            IsoList.Add(new CodeName { Code = "PR", Name = "PUERTO RICO" });
            IsoList.Add(new CodeName { Code = "QA", Name = "QATAR" });
            IsoList.Add(new CodeName { Code = "RE", Name = "RÉUNION" });
            IsoList.Add(new CodeName { Code = "RO", Name = "ROMANIA" });
            IsoList.Add(new CodeName { Code = "RU", Name = "RUSSIAN FEDERATION" });
            IsoList.Add(new CodeName { Code = "RW", Name = "RWANDA" });
            IsoList.Add(new CodeName { Code = "BL", Name = "SAINT BARTHÉLEMY" });
            IsoList.Add(new CodeName { Code = "SH", Name = "SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA" });
            IsoList.Add(new CodeName { Code = "KN", Name = "SAINT KITTS AND NEVIS" });
            IsoList.Add(new CodeName { Code = "LC", Name = "SAINT LUCIA" });
            IsoList.Add(new CodeName { Code = "MF", Name = "SAINT MARTIN (FRENCH PART)" });
            IsoList.Add(new CodeName { Code = "PM", Name = "SAINT PIERRE AND MIQUELON" });
            IsoList.Add(new CodeName { Code = "VC", Name = "SAINT VINCENT AND THE GRENADINES" });
            IsoList.Add(new CodeName { Code = "WS", Name = "SAMOA" });
            IsoList.Add(new CodeName { Code = "SM", Name = "SAN MARINO" });
            IsoList.Add(new CodeName { Code = "ST", Name = "SAO TOME AND PRINCIPE" });
            IsoList.Add(new CodeName { Code = "SA", Name = "SAUDI ARABIA" });
            IsoList.Add(new CodeName { Code = "SN", Name = "SENEGAL" });
            IsoList.Add(new CodeName { Code = "RS", Name = "SERBIA" });
            IsoList.Add(new CodeName { Code = "SC", Name = "SEYCHELLES" });
            IsoList.Add(new CodeName { Code = "SL", Name = "SIERRA LEONE" });
            IsoList.Add(new CodeName { Code = "SG", Name = "SINGAPORE" });
            IsoList.Add(new CodeName { Code = "SX", Name = "SINT MAARTEN (DUTCH PART)" });
            IsoList.Add(new CodeName { Code = "SK", Name = "SLOVAKIA" });
            IsoList.Add(new CodeName { Code = "SI", Name = "SLOVENIA" });
            IsoList.Add(new CodeName { Code = "SB", Name = "SOLOMON ISLANDS" });
            IsoList.Add(new CodeName { Code = "SO", Name = "SOMALIA" });
            IsoList.Add(new CodeName { Code = "ZA", Name = "SOUTH AFRICA" });
            IsoList.Add(new CodeName { Code = "GS", Name = "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS" });
            IsoList.Add(new CodeName { Code = "SS", Name = "SOUTH SUDAN" });
            IsoList.Add(new CodeName { Code = "ES", Name = "SPAIN" });
            IsoList.Add(new CodeName { Code = "LK", Name = "SRI LANKA" });
            IsoList.Add(new CodeName { Code = "SD", Name = "SUDAN" });
            IsoList.Add(new CodeName { Code = "SR", Name = "SURINAME" });
            IsoList.Add(new CodeName { Code = "SJ", Name = "SVALBARD AND JAN MAYEN" });
            IsoList.Add(new CodeName { Code = "SZ", Name = "SWAZILAND" });
            IsoList.Add(new CodeName { Code = "SE", Name = "SWEDEN" });
            IsoList.Add(new CodeName { Code = "CH", Name = "SWITZERLAND" });
            IsoList.Add(new CodeName { Code = "SY", Name = "SYRIAN ARAB REPUBLIC" });
            IsoList.Add(new CodeName { Code = "TW", Name = "TAIWAN, PROVINCE OF CHINA" });
            IsoList.Add(new CodeName { Code = "TJ", Name = "TAJIKISTAN" });
            IsoList.Add(new CodeName { Code = "TZ", Name = "TANZANIA, UNITED REPUBLIC OF" });
            IsoList.Add(new CodeName { Code = "TH", Name = "THAILAND" });
            IsoList.Add(new CodeName { Code = "TL", Name = "TIMOR-LESTE" });
            IsoList.Add(new CodeName { Code = "TG", Name = "TOGO" });
            IsoList.Add(new CodeName { Code = "TK", Name = "TOKELAU" });
            IsoList.Add(new CodeName { Code = "TO", Name = "TONGA" });
            IsoList.Add(new CodeName { Code = "TT", Name = "TRINIDAD AND TOBAGO" });
            IsoList.Add(new CodeName { Code = "TN", Name = "TUNISIA" });
            IsoList.Add(new CodeName { Code = "TR", Name = "TURKEY" });
            IsoList.Add(new CodeName { Code = "TM", Name = "TURKMENISTAN" });
            IsoList.Add(new CodeName { Code = "TC", Name = "TURKS AND CAICOS ISLANDS" });
            IsoList.Add(new CodeName { Code = "TV", Name = "TUVALU" });
            IsoList.Add(new CodeName { Code = "UG", Name = "UGANDA" });
            IsoList.Add(new CodeName { Code = "UA", Name = "UKRAINE" });
            IsoList.Add(new CodeName { Code = "AE", Name = "UNITED ARAB EMIRATES" });
            IsoList.Add(new CodeName { Code = "GB", Name = "UNITED KINGDOM" });
            IsoList.Add(new CodeName { Code = "US", Name = "UNITED STATES" });
            IsoList.Add(new CodeName { Code = "UM", Name = "UNITED STATES MINOR OUTLYING ISLANDS" });
            IsoList.Add(new CodeName { Code = "UY", Name = "URUGUAY" });
            IsoList.Add(new CodeName { Code = "UZ", Name = "UZBEKISTAN" });
            IsoList.Add(new CodeName { Code = "VU", Name = "VANUATU" });
            IsoList.Add(new CodeName { Code = "VE", Name = "VENEZUELA, BOLIVARIAN REPUBLIC OF" });
            IsoList.Add(new CodeName { Code = "VN", Name = "VIET NAM" });
            IsoList.Add(new CodeName { Code = "VG", Name = "VIRGIN ISLANDS, BRITISH" });
            IsoList.Add(new CodeName { Code = "VI", Name = "VIRGIN ISLANDS, U.S." });
            IsoList.Add(new CodeName { Code = "WF", Name = "WALLIS AND FUTUNA" });
            IsoList.Add(new CodeName { Code = "EH", Name = "WESTERN SAHARA" });
            IsoList.Add(new CodeName { Code = "YE", Name = "YEMEN" });
            IsoList.Add(new CodeName { Code = "ZM", Name = "ZAMBIA" });
            IsoList.Add(new CodeName { Code = "ZW", Name = "ZIMBABWE" });
            return IsoList;
        }

        public Location getLocation(string Market, Int32? LocationID, ref string errorMsg)
        {
            string tsql = @"Select  RecID,Parent,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),
                                    Code,Name,NameS,Type,Village,Town,City,Country,TaxPer,CountryCode,VisaReq,EUMember
                            From Location (NOLOCK) 
                            Where RecID=@RecID ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "RecID", DbType.Int32, LocationID);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        Location lRecord = new Location();
                        lRecord.RecID = (int)oReader["RecID"];
                        lRecord.Parent = (int)oReader["Parent"];
                        lRecord.Name = Conversion.getStrOrNull(oReader["Name"]);
                        lRecord.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        lRecord.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        lRecord.Code = Conversion.getStrOrNull(oReader["Code"]);
                        lRecord.Type = (Int16)oReader["Type"];
                        lRecord.Village = Conversion.getInt32OrNull(oReader["Village"]);
                        lRecord.Town = Conversion.getInt32OrNull(oReader["Town"]);
                        lRecord.City = Conversion.getInt32OrNull(oReader["City"]);
                        lRecord.Country = (int)oReader["Country"];
                        lRecord.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        lRecord.CountryCode = Conversion.getStrOrNull(oReader["CountryCode"]);
                        lRecord.VisaReq = Conversion.getStrOrNull(oReader["VisaReq"]);
                        lRecord.EUMember = Conversion.getBoolOrNull(oReader["EUMember"]);
                        return lRecord;
                    }
                    else return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Location> getLocationList(string Market, LocationType locationType, int? LocationID, int? Country, int? City, int? Town, ref string errorMsg)
        {
            List<Location> location = new List<Location>();

            string tsql = string.Empty;
            string whereStr = string.Empty;
            int locType = 0;
            switch (locationType)
            {
                case LocationType.None: locType = 0; break;
                case LocationType.Country: locType = 1; break;
                case LocationType.City: locType = 2; break;
                case LocationType.Resort: locType = 3; break;
                case LocationType.Village: locType = 4; break;
            }
            tsql = @"   Select  RecID,Parent,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),
                                Code,Name,NameS,Type,Village,Town,City,Country,TaxPer,CountryCode,VisaReq,EUMember
                        From Location (NOLOCK) ";
            if (locType > 0)
            {
                if (whereStr.Length > 0)
                    whereStr += " And ";
                whereStr += "Type = @LocationType ";
            }

            if (LocationID != null)
            {
                if (whereStr.Length > 0)
                    whereStr += " And ";
                whereStr += "RecID = @LocationID ";
            }

            if (Country != null)
            {
                if (whereStr.Length > 0)
                    whereStr += " And ";
                whereStr += "Country = @Country ";
            }

            if (City != null)
            {
                if (whereStr.Length > 0)
                    whereStr += " And ";
                whereStr += "City = @City ";
            }

            if (Town != null)
            {
                if (whereStr.Length > 0)
                    whereStr += " And ";
                whereStr += "Town = @Town ";
            }

            if (whereStr.Length > 0)
                tsql += " Where " + whereStr;

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                if (locType > 0)
                    db.AddInParameter(dbCommand, "LocationType", DbType.Int16, locType);
                if (LocationID != null)
                    db.AddInParameter(dbCommand, "LocationID", DbType.Int32, LocationID);
                if (Country != null)
                    db.AddInParameter(dbCommand, "Country", DbType.Int32, Country);
                if (City != null)
                    db.AddInParameter(dbCommand, "City", DbType.Int32, City);
                if (Town != null)
                    db.AddInParameter(dbCommand, "Town", DbType.Int32, Town);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        Location lRecord = new Location();
                        lRecord.RecID = (int)oReader["RecID"];
                        lRecord.Parent = (int)oReader["Parent"];
                        lRecord.Name = Conversion.getStrOrNull(oReader["Name"]);
                        lRecord.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        lRecord.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        lRecord.Code = Conversion.getStrOrNull(oReader["Code"]);
                        lRecord.Type = (Int16)oReader["Type"];
                        lRecord.Village = Conversion.getInt32OrNull(oReader["Village"]);
                        lRecord.Town = Conversion.getInt32OrNull(oReader["Town"]);
                        lRecord.City = Conversion.getInt32OrNull(oReader["City"]);
                        lRecord.Country = (int)oReader["Country"];
                        lRecord.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        lRecord.CountryCode = Conversion.getStrOrNull(oReader["CountryCode"]);
                        lRecord.VisaReq = Conversion.getStrOrNull(oReader["VisaReq"]);
                        lRecord.EUMember = Conversion.getBoolOrNull(oReader["EUMember"]);
                        location.Add(lRecord);
                    }
                }

                return location;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return location;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Location> getLocationList(User UserData, LocationType locationType, int? LocationID, int? Country, int? City, int? Town, ref string errorMsg)
        {
            return getLocationList(UserData.Market, locationType, LocationID, Country, City, Town, ref errorMsg);
        }

        public List<LocationIDName> getPLCountryLocations(User UserData, int Country, ref string errorMsg)
        {
            List<LocationIDName> location = new List<LocationIDName>();
            string tsql = string.Empty;
            tsql = @"   Select RecID, 
                            Name = (Case L.Type When 2 Then isnull(dbo.FindLocalName(NameLID, @Market), Name) 
                                                WHen 3 Then (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = L.City And [Type] = 2)
		                                                + '->' + isnull(dbo.FindLocalName(NameLID, @Market), Name)
                                                When 4 Then (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = L.City And [Type] = 2)
		                                                + '->' + (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = L.Town And [Type] = 3)
		                                                + '->' + isnull(dbo.FindLocalName(NameLID, @Market), Name)
                                Else '' End), L.Type
                        From Location L (NOLOCK)
                        Where L.Type > 1 
                          And L.Country = @Country";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, Country);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        LocationIDName lRecord = new LocationIDName();
                        lRecord.RecID = (int)oReader["RecID"];
                        lRecord.Name = (string)oReader["Name"];
                        lRecord.Type = (Int16)oReader["Type"];
                        location.Add(lRecord);
                    }
                }
                return location;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return location;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int? getFlightLocation(User UserData, string FlightNo, DateTime FlyDate, bool Dep, ref string errorMsg)
        {
            string tsql = string.Empty;

            if (Dep)
                tsql += @"  Select ArrCity
	                        From FlightDay FD (NOLOCK)
	                        Where FlyDate = @FlyDate
	                        And FlightNo = @FlightNo ";
            else
                tsql += @"  Select DepCity
                            From FlightDay FD (NOLOCK)
                            Where FlyDate = @FlyDate
                            And FlightNO = @FlightNo ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.Date, FlyDate.Date);
                return (int)db.ExecuteScalar(tsql);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<LocationIDName> getNations(User UserData, ref string errorMsg)
        {
            List<LocationIDName> location = new List<LocationIDName>();
            string tsql = @"Select RecID, Name=isnull(dbo.FindLocalName(NameLID, @Market), Name)
                            From Location (NOLOCK)
                            Where Type = 1";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        LocationIDName lRecord = new LocationIDName();
                        lRecord.RecID = (int)oReader["RecID"];
                        lRecord.Name = (string)oReader["Name"];
                        lRecord.Type = 1;
                        location.Add(lRecord);
                    }
                }
                return location;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return location;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getLocationName(User UserData, int Location, ref string errorMsg)
        {
            string tsql = @"Select [Name]=isnull(dbo.FindLocalName(NameLID, @Market), Name) 
                            From Location (NOLOCK) 
                            Where RecID = @RecID ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "RecId", DbType.Int32, Location);

                return (string)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getLocationName(string Market, int Location, NameType nameType, ref string errorMsg)
        {
            string retVal = string.Empty;
            switch (nameType)
            {
                case NameType.NormalName:
                    retVal = CacheObjects.getLocationList(Market).AsEnumerable().Where(w => w.RecID == Location).Select(s => s.Name).FirstOrDefault();
                    break;
                case NameType.LocalName:
                    retVal = CacheObjects.getLocationList(Market).AsEnumerable().Where(w => w.RecID == Location).Select(s => s.NameL).FirstOrDefault();
                    break;
            }
            return retVal;
        }

        public string getLocationForCountryName(User UserData, int Location, ref string errorMsg)
        {
            string tsql = @"Select [Name]=isnull(dbo.FindLocalName(NameLID, @Market), Name) 
                            From Location (NOLOCK) 
                            Where RecID = (Select Country From Location (NOLOCK) Where RecID = @RecID) ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "RecId", DbType.Int32, Location);

                return (string)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int? getLocationForCity(int Location, ref string errorMsg)
        {
            string tsql = @"Select City
                            From Location (NOLOCK) 
                            Where RecID = @RecID ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "RecId", DbType.Int32, Location);

                return (int?)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getLocationForCityName(User UserData, int Location, ref string errorMsg)
        {
            string tsql = @"Select [Name]=isnull(dbo.FindLocalName(NameLID, @Market), Name) 
                            From Location (NOLOCK) 
                            Where RecID = (Select City From Location (NOLOCK) Where RecID = @RecID) ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "RecId", DbType.Int32, Location);

                return (string)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int? getLocationForCountry(int Location, ref string errorMsg)
        {
            string tsql = @"Select Country  
                            From Location (NOLOCK) 
                            Where RecID=(Select Country From Location (NOLOCK) Where RecID=@RecID) ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "RecId", DbType.Int32, Location);

                return Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Nationality> getNationalityList(ref string errorMsg)
        {
            List<Nationality> records = new List<Nationality>();
            string tsql =
@"
Select * 
From Nationality (NOLOCK)
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                bool haveOrder = false;
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    for (int i = 0; i < R.FieldCount; i++)
                        if (R.GetName(i).Equals("orderno", StringComparison.InvariantCultureIgnoreCase))
                            haveOrder = true;
                    while (R.Read())
                    {
                        Nationality record = new Nationality();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code2 = Conversion.getStrOrNull(R["Code2"]);
                        record.Code3 = Conversion.getStrOrNull(R["Code3"]);
                        record.CodeNum = Conversion.getInt16OrNull(R["CodeNum"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        if (haveOrder && !string.IsNullOrEmpty(R["OrderNo"].ToString()))
                            record.OrderNo = Convert.ToInt32(R["OrderNo"]);
                        else record.OrderNo = 9999;
                        records.Add(record);
                    }
                    return records;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Nationality getDefaultNationality(User UserData, int? location, ref string errorMsg)
        {
            if (Convert.ToDecimal(UserData.TvVersion) <= Convert.ToDecimal("040071120"))
                return null;

            string tsql = string.Empty;
            tsql +=
@"  
Select RecID,Code3,Code2,CodeNum,Name From Nationality N (NOLOCK)
Where Exists(Select CountryCode From Location L (NOLOCK) 
             Where Exists(Select null From Location (NOLOCK) Where RecID=@location And Country=L.RecID)
			   And L.CountryCode=N.Code3)
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "location", DbType.Int32, location);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        Nationality lRecord = new Nationality();
                        lRecord.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        lRecord.Code2 = Conversion.getStrOrNull(oReader["Code2"]);
                        lRecord.Code3 = Conversion.getStrOrNull(oReader["Code3"]);
                        lRecord.CodeNum = Conversion.getInt16OrNull(oReader["CodeNum"]);
                        lRecord.Name = Conversion.getStrOrNull(oReader["Name"]);
                        return lRecord;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
