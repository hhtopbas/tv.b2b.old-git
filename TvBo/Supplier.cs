﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using TvTools;

namespace TvBo
{
    public class Suppliers
    {
        public List<ShortSupplierRecord> getSupplierList(User UserData, ref string errorMsg)
        {
            List<ShortSupplierRecord> records = new List<ShortSupplierRecord>();
            string tsql =
@"
Select RecID,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@pMarket),Name) 
From Supplier (NOLOCK)      
Where Exists(Select * From SupplierSrv (NOLOCK) Where Supplier=Supplier.Code and Service='VISA')
Order by Name
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pMarket", DbType.AnsiString, UserData.Market);
                
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ShortSupplierRecord record = new ShortSupplierRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]); 
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }    
        }
    }
}
