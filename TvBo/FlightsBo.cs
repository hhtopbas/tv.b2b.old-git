﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class PLFlights
    {
        public PLFlights()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        string _PLFlightNo;
        public string PLFlightNo
        {
            get { return _PLFlightNo; }
            set { _PLFlightNo = value; }
        }

        DateTime _FlyDate;
        public DateTime FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        string _Class;
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        string _AllotType;
        public string AllotType
        {
            get { return _AllotType; }
            set { _AllotType = value; }
        }

        string _RetAllotType;
        public string RetAllotType
        {
            get { return _RetAllotType; }
            set { _RetAllotType = value; }
        }

        Int16 _WarnType;
        public Int16 WarnType
        {
            get { return _WarnType; }
            set { _WarnType = value; }
        }

        Int16 _StepNo;
        public Int16 StepNo
        {
            get { return _StepNo; }
            set { _StepNo = value; }
        }

        string _NextFlight;
        public string NextFlight
        {
            get { return _NextFlight; }
            set { _NextFlight = value; }
        }

        DateTime? _NextFlyDate;
        public DateTime? NextFlyDate
        {
            get { return _NextFlyDate; }
            set { _NextFlyDate = value; }
        }

    }

    public class FlightDayRecord
    {
        public FlightDayRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        string _Airline;
        public string Airline
        {
            get { return _Airline; }
            set { _Airline = value; }
        }

        string _FlgOwner;
        public string FlgOwner
        {
            get { return _FlgOwner; }
            set { _FlgOwner = value; }
        }

        DateTime _FlyDate;
        public DateTime FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        int _DepCity;
        public int DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepAirport;
        public string DepAirport
        {
            get { return _DepAirport; }
            set { _DepAirport = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        int _ArrCity;
        public int ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrAirport;
        public string ArrAirport
        {
            get { return _ArrAirport; }
            set { _ArrAirport = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        bool _StopSale;
        public bool StopSale
        {
            get { return _StopSale; }
            set { _StopSale = value; }
        }

        Int16? _BagWeight;
        public Int16? BagWeight
        {
            get { return _BagWeight; }
            set { _BagWeight = value; }
        }

        decimal? _MaxInfAge;
        public decimal? MaxInfAge
        {
            get { return _MaxInfAge; }
            set { _MaxInfAge = value; }
        }

        decimal? _MaxChdAge;
        public decimal? MaxChdAge
        {
            get { return _MaxChdAge; }
            set { _MaxChdAge = value; }
        }

        decimal? _MaxTeenAge;
        public decimal? MaxTeenAge
        {
            get { return _MaxTeenAge; }
            set { _MaxTeenAge = value; }
        }

        string _NextFlight;
        public string NextFlight
        {
            get { return _NextFlight; }
            set { _NextFlight = value; }
        }

        DateTime? _CountCloseTime;
        public DateTime? CountCloseTime
        {
            get { return _CountCloseTime; }
            set { _CountCloseTime = value; }
        }

        bool _WebPub;
        public bool WebPub
        {
            get { return _WebPub; }
            set { _WebPub = value; }
        }

        bool _AllotUseSysSet;
        public bool AllotUseSysSet
        {
            get { return _AllotUseSysSet; }
            set { _AllotUseSysSet = value; }
        }

        bool _AllotChk;
        public bool AllotChk
        {
            get { return _AllotChk; }
            set { _AllotChk = value; }
        }

        bool _AllotWarnFull;
        public bool AllotWarnFull
        {
            get { return _AllotWarnFull; }
            set { _AllotWarnFull = value; }
        }

        bool _AllotDoNotOver;
        public bool AllotDoNotOver
        {
            get { return _AllotDoNotOver; }
            set { _AllotDoNotOver = value; }
        }

        int _FlyDur;
        public int FlyDur
        {
            get { return _FlyDur; }
            set { _FlyDur = value; }
        }

        Int16? _WarnUsedAmount;
        public Int16? WarnUsedAmount
        {
            get { return _WarnUsedAmount; }
            set { _WarnUsedAmount = value; }
        }

        Byte? _RetFlightOpt;
        public Byte? RetFlightOpt
        {
            get { return _RetFlightOpt; }
            set { _RetFlightOpt = value; }
        }

        bool _RetFlightUseSysSet;
        public bool RetFlightUseSysSet
        {
            get { return _RetFlightUseSysSet; }
            set { _RetFlightUseSysSet = value; }
        }

        DateTime? _SaleRelease;
        public DateTime? SaleRelease
        {
            get { return _SaleRelease; }
            set { _SaleRelease = value; }
        }

        decimal? _BagWeightForInf;
        public decimal? BagWeightForInf
        {
            get { return _BagWeightForInf; }
            set { _BagWeightForInf = value; }
        }

        bool? _TVCCSendPNL;
        public bool? TVCCSendPNL
        {
            get { return _TVCCSendPNL; }
            set { _TVCCSendPNL = value; }
        }

        DateTime? _TVCCSendPNLDate;
        public DateTime? TVCCSendPNLDate
        {
            get { return _TVCCSendPNLDate; }
            set { _TVCCSendPNLDate = value; }
        }

        DateTime? _TDepDate;
        public DateTime? TDepDate
        {
            get { return _TDepDate; }
            set { _TDepDate = value; }
        }

        DateTime? _TArrDate;
        public DateTime? TArrDate
        {
            get { return _TArrDate; }
            set { _TArrDate = value; }
        }

        string _PNLName;
        public string PNLName
        {
            get { return _PNLName; }
            set { _PNLName = value; }
        }

        DateTime? _TDepTime;
        public DateTime? TDepTime
        {
            get { return _TDepTime; }
            set { _TDepTime = value; }
        }

        DateTime? _TArrTime;
        public DateTime? TArrTime
        {
            get { return _TArrTime; }
            set { _TArrTime = value; }
        }

        bool? _AllowPrintTicB2B;
        public bool? AllowPrintTicB2B
        {
            get { return _AllowPrintTicB2B; }
            set { _AllowPrintTicB2B = value; }
        }

    }

    public class FlightRecord
    {
        public FlightRecord()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Airline;
        public string Airline
        {
            get { return _Airline; }
            set { _Airline = value; }
        }

        string _AirlineName;
        public string AirlineName
        {
            get { return _AirlineName; }
            set { _AirlineName = value; }
        }

        string _AirlineLocalName;
        public string AirlineLocalName
        {
            get { return _AirlineLocalName; }
            set { _AirlineLocalName = value; }
        }

        string _Company;
        public string Company
        {
            get { return _Company; }
            set { _Company = value; }
        }

        int? _PlaneType;
        public int? PlaneType
        {
            get { return _PlaneType; }
            set { _PlaneType = value; }
        }

        string _PlaneTypeName;
        public string PlaneTypeName
        {
            get { return _PlaneTypeName; }
            set { _PlaneTypeName = value; }
        }

        int? _FlightType;
        public int? FlightType
        {
            get { return _FlightType; }
            set { _FlightType = value; }
        }

        string _FlightTypeName;
        public string FlightTypeName
        {
            get { return _FlightTypeName; }
            set { _FlightTypeName = value; }
        }

        int _DepCity;
        public int DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityLocalName;
        public string DepCityLocalName
        {
            get { return _DepCityLocalName; }
            set { _DepCityLocalName = value; }
        }

        string _DepAir;
        public string DepAir
        {
            get { return _DepAir; }
            set { _DepAir = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        int _ArrCity;
        public int ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityLocalName;
        public string ArrCityLocalName
        {
            get { return _ArrCityLocalName; }
            set { _ArrCityLocalName = value; }
        }

        string _ArrAir;
        public string ArrAir
        {
            get { return _ArrAir; }
            set { _ArrAir = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        DateTime? _CountCloseTime;
        public DateTime? CountCloseTime
        {
            get { return _CountCloseTime; }
            set { _CountCloseTime = value; }
        }

        decimal? _BagWeight;
        public decimal? BagWeight
        {
            get { return _BagWeight; }
            set { _BagWeight = value; }
        }

        decimal? _InfAge;
        public decimal? InfAge
        {
            get { return _InfAge; }
            set { _InfAge = value; }
        }

        decimal? _ChdAge;
        public decimal? ChdAge
        {
            get { return _ChdAge; }
            set { _ChdAge = value; }
        }

        decimal? _TeenAge;
        public decimal? TeenAge
        {
            get { return _TeenAge; }
            set { _TeenAge = value; }
        }

        string _RetCode;
        public string RetCode
        {
            get { return _RetCode; }
            set { _RetCode = value; }
        }

        string _Virtual;
        public string Virtual
        {
            get { return _Virtual; }
            set { _Virtual = value; }
        }

        string _PNLName;
        public string PNLName
        {
            get { return _PNLName; }
            set { _PNLName = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        string _FlyDays;
        public string FlyDays
        {
            get { return _FlyDays; }
            set { _FlyDays = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        int? _FlyDur;
        public int? FlyDur
        {
            get { return _FlyDur; }
            set { _FlyDur = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        bool? _PayCom;
        public bool? PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        bool? _PayComEB;
        public bool? PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool? _PayPasEB;
        public bool? PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool? _isFirstFlight;
        public bool? IsFirstFlight
        {
            get { return _isFirstFlight; }
            set { _isFirstFlight = value; }
        }

        bool? _CanDiscount;
        public bool? CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        string _AccountNo1;
        public string AccountNo1
        {
            get { return _AccountNo1; }
            set { _AccountNo1 = value; }
        }

        string _AccountNo2;
        public string AccountNo2
        {
            get { return _AccountNo2; }
            set { _AccountNo2 = value; }
        }

        string _AccountNo3;
        public string AccountNo3
        {
            get { return _AccountNo3; }
            set { _AccountNo3 = value; }
        }

        bool? _OTickSaveAsRq;
        public bool? OTickSaveAsRq
        {
            get { return _OTickSaveAsRq; }
            set { _OTickSaveAsRq = value; }
        }
    }

    public class AirportRecord
    {
        public AirportRecord()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        int _Location;
        public int Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _LocationNameL;
        public string LocationNameL
        {
            get { return _LocationNameL; }
            set { _LocationNameL = value; }
        }

        int? _TrfLocation;
        public int? TrfLocation
        {
            get { return _TrfLocation; }
            set { _TrfLocation = value; }
        }

        string _TrfLocationName;
        public string TrfLocationName
        {
            get { return _TrfLocationName; }
            set { _TrfLocationName = value; }
        }

        string _TrfLocationNameL;
        public string TrfLocationNameL
        {
            get { return _TrfLocationNameL; }
            set { _TrfLocationNameL = value; }
        }

        bool _PNLFullName;
        public bool PNLFullName
        {
            get { return _PNLFullName; }
            set { _PNLFullName = value; }
        }

        bool _PNLFlightSpace;
        public bool PNLFlightSpace
        {
            get { return _PNLFlightSpace; }
            set { _PNLFlightSpace = value; }
        }

        bool _PNLNameSpace;
        public bool PNLNameSpace
        {
            get { return _PNLNameSpace; }
            set { _PNLNameSpace = value; }
        }

        bool _PNLChdRemark;
        public bool PNLChdRemark
        {
            get { return _PNLChdRemark; }
            set { _PNLChdRemark = value; }
        }

        bool _PNLSitatex;
        public bool PNLSitatex
        {
            get { return _PNLSitatex; }
            set { _PNLSitatex = value; }
        }

        int? _PNLMaxLineInPart;
        public int? PNLMaxLineInPart
        {
            get { return _PNLMaxLineInPart; }
            set { _PNLMaxLineInPart = value; }
        }

        bool _PNLDispExtra;
        public bool PNLDispExtra
        {
            get { return _PNLDispExtra; }
            set { _PNLDispExtra = value; }
        }

        string _PNLDispPNR;
        public string PNLDispPNR
        {
            get { return _PNLDispPNR; }
            set { _PNLDispPNR = value; }
        }

        bool _PNLDotAfterTitle;
        public bool PNLDotAfterTitle
        {
            get { return _PNLDotAfterTitle; }
            set { _PNLDotAfterTitle = value; }
        }

    }

    public class FlightOptRecord
    {
        public FlightOptRecord()
        {
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _Class;
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }

        Int16? _Night;
        public Int16? Night
        {
            get { return _Night; }
            set { _Night = value; }
        }

        DateTime? _AllotDate;
        public DateTime? AllotDate
        {
            get { return _AllotDate; }
            set { _AllotDate = value; }
        }

        DateTime? _AllotEndDate;
        public DateTime? AllotEndDate
        {
            get { return _AllotEndDate; }
            set { _AllotEndDate = value; }
        }

        Int32? _FlightSeat;
        public Int32? FlightSeat
        {
            get { return _FlightSeat; }
            set { _FlightSeat = value; }
        }

        Int32? _FlightSeatTot;
        public Int32? FlightSeatTot
        {
            get { return _FlightSeatTot; }
            set { _FlightSeatTot = value; }
        }

        Int32? _UsedFlightSeat;
        public Int32? UsedFlightSeat
        {
            get { return _UsedFlightSeat; }
            set { _UsedFlightSeat = value; }
        }

        Int16? _Stolen;
        public Int16? Stolen
        {
            get { return _Stolen; }
            set { _Stolen = value; }
        }

        Int16? _Control;
        public Int16? Control
        {
            get { return _Control; }
            set { _Control = value; }
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

    }

    public class FlightDetailRecord
    {
        public FlightDetailRecord()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        DateTime? _FlyDate;
        public DateTime? FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        DateTime? _DepDate;
        public DateTime? DepDate
        {
            get { return _DepDate; }
            set { _DepDate = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        DateTime? _TDepTime;
        public DateTime? TDepTime
        {
            get { return _TDepTime; }
            set { _TDepTime = value; }
        }

        string _DepAirport;
        public string DepAirport
        {
            get { return _DepAirport; }
            set { _DepAirport = value; }
        }

        string _DepAirportName;
        public string DepAirportName
        {
            get { return _DepAirportName; }
            set { _DepAirportName = value; }
        }

        string _DepAirportNameL;
        public string DepAirportNameL
        {
            get { return _DepAirportNameL; }
            set { _DepAirportNameL = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityNameL;
        public string DepCityNameL
        {
            get { return _DepCityNameL; }
            set { _DepCityNameL = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        DateTime? _TArrTime;
        public DateTime? TArrTime
        {
            get { return _TArrTime; }
            set { _TArrTime = value; }
        }

        string _ArrAirport;
        public string ArrAirport
        {
            get { return _ArrAirport; }
            set { _ArrAirport = value; }
        }

        string _ArrAirportName;
        public string ArrAirportName
        {
            get { return _ArrAirportName; }
            set { _ArrAirportName = value; }
        }

        string _ArrAirportNameL;
        public string ArrAirportNameL
        {
            get { return _ArrAirportNameL; }
            set { _ArrAirportNameL = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityNameL;
        public string ArrCityNameL
        {
            get { return _ArrCityNameL; }
            set { _ArrCityNameL = value; }
        }

        string _Airline;
        public string Airline
        {
            get { return _Airline; }
            set { _Airline = value; }
        }

        string _AirlineName;
        public string AirlineName
        {
            get { return _AirlineName; }
            set { _AirlineName = value; }
        }

        string _AirlineNameL;
        public string AirlineNameL
        {
            get { return _AirlineNameL; }
            set { _AirlineNameL = value; }
        }

        decimal? _MaxInfAge;
        public decimal? MaxInfAge
        {
            get { return _MaxInfAge; }
            set { _MaxInfAge = value; }
        }

        decimal? _MaxChdAge;
        public decimal? MaxChdAge
        {
            get { return _MaxChdAge; }
            set { _MaxChdAge = value; }
        }

        decimal? _MaxTeenAge;
        public decimal? MaxTeenAge
        {
            get { return _MaxTeenAge; }
            set { _MaxTeenAge = value; }
        }

        decimal? _BagWeight;
        public decimal? BagWeight
        {
            get { return _BagWeight; }
            set { _BagWeight = value; }
        }

        bool? _CanDiscount;
        public bool? CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }
    }

    public class FlightDays
    {
        public FlightDays()
        {
        }

        DateTime _FlyDate;
        public DateTime FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        int _ArrCity;
        public int ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        int _DepCity;
        public int DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepAirport;
        public string DepAirport
        {
            get { return _DepAirport; }
            set { _DepAirport = value; }
        }

        string _ArrAirport;
        public string ArrAirport
        {
            get { return _ArrAirport; }
            set { _ArrAirport = value; }
        }

        string _Flights;
        public string Flights
        {
            get { return _Flights; }
            set { _Flights = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        DateTime? _TDepTime;
        public DateTime? TDepTime
        {
            get { return _TDepTime; }
            set { _TDepTime = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        DateTime? _TArrTime;
        public DateTime? TArrTime
        {
            get { return _TArrTime; }
            set { _TArrTime = value; }
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        string _ORT;
        public string ORT
        {
            get { return _ORT; }
            set { _ORT = value; }
        }

        int? _Night;
        public int? Night
        {
            get { return _Night; }
            set { _Night = value; }
        }
    }

    public class FlightAndDaysRecord
    {
        public FlightAndDaysRecord()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        DateTime? _FlyDate;
        public DateTime? FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        Int32? _DepCity;
        public Int32? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityNameL;
        public string DepCityNameL
        {
            get { return _DepCityNameL; }
            set { _DepCityNameL = value; }
        }

        string _DepAirport;
        public string DepAirport
        {
            get { return _DepAirport; }
            set { _DepAirport = value; }
        }

        string _DepAirportName;
        public string DepAirportName
        {
            get { return _DepAirportName; }
            set { _DepAirportName = value; }
        }

        string _DepAirportNameL;
        public string DepAirportNameL
        {
            get { return _DepAirportNameL; }
            set { _DepAirportNameL = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        Int32? _ArrCity;
        public Int32? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityNameL;
        public string ArrCityNameL
        {
            get { return _ArrCityNameL; }
            set { _ArrCityNameL = value; }
        }

        string _ArrAirport;
        public string ArrAirport
        {
            get { return _ArrAirport; }
            set { _ArrAirport = value; }
        }

        string _ArrAirportName;
        public string ArrAirportName
        {
            get { return _ArrAirportName; }
            set { _ArrAirportName = value; }
        }

        string _ArrAirportNameL;
        public string ArrAirportNameL
        {
            get { return _ArrAirportNameL; }
            set { _ArrAirportNameL = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }
    }

    public class FlightClassRecord
    {
        public FlightClassRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Class;
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        bool _B2B;
        public bool B2B
        {
            get { return _B2B; }
            set { _B2B = value; }
        }

        bool _B2C;
        public bool B2C
        {
            get { return _B2C; }
            set { _B2C = value; }
        }

    }

    public class FlightData
    {
        public FlightData()
        {
            _ServiceExtra = new List<OnlyFlight_ServiceExtra>();
            _FlightInfo = new List<OnlyFlight_FlightInfo>();
            _CompService = new List<OnlyFlight_AdditionalService>();
        }

        List<OnlyFlight_ServiceExtra> _ServiceExtra;
        public List<OnlyFlight_ServiceExtra> ServiceExtra
        {
            get { return _ServiceExtra; }
            set { _ServiceExtra = value; }
        }

        List<OnlyFlight_AdditionalService> _CompService;
        public List<OnlyFlight_AdditionalService> CompService
        {
            get { return _CompService; }
            set { _CompService = value; }
        }

        List<OnlyFlight_FlightInfo> _FlightInfo;
        public List<OnlyFlight_FlightInfo> FlightInfo
        {
            get { return _FlightInfo; }
            set { _FlightInfo = value; }
        }

        Guid? _LogID;
        public Guid? LogID
        {
            get { return _LogID; }
            set { _LogID = value; }
        }
    }

    public class OnlyFlight_AdServices
    {
        public OnlyFlight_AdServices()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }
    }

    public class OnlyFlight_AdditionalService
    {
        public OnlyFlight_AdditionalService()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _MainService;
        public string MainService
        {
            get { return _MainService; }
            set { _MainService = value; }
        }

        string _ArrDep;
        public string ArrDep
        {
            get { return _ArrDep; }
            set { _ArrDep = value; }
        }

        string _Extra;
        public string Extra
        {
            get { return _Extra; }
            set { _Extra = value; }
        }

        string _ExtraName;
        public string ExtraName
        {
            get { return _ExtraName; }
            set { _ExtraName = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _ExtraNet;
        public decimal? ExtraNet
        {
            get { return _ExtraNet; }
            set { _ExtraNet = value; }
        }

        decimal? _ExtraChdNet;
        public decimal? ExtraChdNet
        {
            get { return _ExtraChdNet; }
            set { _ExtraChdNet = value; }
        }

        decimal? _ExtraInfNet;
        public decimal? ExtraInfNet
        {
            get { return _ExtraInfNet; }
            set { _ExtraInfNet = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _ExtraSale;
        public decimal? ExtraSale
        {
            get { return _ExtraSale; }
            set { _ExtraSale = value; }
        }

        decimal? _ExtraChdSale;
        public decimal? ExtraChdSale
        {
            get { return _ExtraChdSale; }
            set { _ExtraChdSale = value; }
        }

        decimal? _ExtraInfSale;
        public decimal? ExtraInfSale
        {
            get { return _ExtraInfSale; }
            set { _ExtraInfSale = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        Int16? _CalcType;
        public Int16? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        string _PayCom;
        public string PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        string _PayComEB;
        public string PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool? _PayPasEB;
        public bool? PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        Int16? _StatConf;
        public Int16? StatConf
        {
            get { return _StatConf; }
            set { _StatConf = value; }
        }
    }

    public class OnlyFlight_ServiceExtra
    {
        public OnlyFlight_ServiceExtra()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _MainService;
        public string MainService
        {
            get { return _MainService; }
            set { _MainService = value; }
        }

        string _ArrDep;
        public string ArrDep
        {
            get { return _ArrDep; }
            set { _ArrDep = value; }
        }

        string _Extra;
        public string Extra
        {
            get { return _Extra; }
            set { _Extra = value; }
        }

        string _ExtraName;
        public string ExtraName
        {
            get { return _ExtraName; }
            set { _ExtraName = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _ExtraNet;
        public decimal? ExtraNet
        {
            get { return _ExtraNet; }
            set { _ExtraNet = value; }
        }

        decimal? _ExtraChdNet;
        public decimal? ExtraChdNet
        {
            get { return _ExtraChdNet; }
            set { _ExtraChdNet = value; }
        }

        decimal? _ExtraInfNet;
        public decimal? ExtraInfNet
        {
            get { return _ExtraInfNet; }
            set { _ExtraInfNet = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _ExtraSale;
        public decimal? ExtraSale
        {
            get { return _ExtraSale; }
            set { _ExtraSale = value; }
        }

        decimal? _ExtraChdSale;
        public decimal? ExtraChdSale
        {
            get { return _ExtraChdSale; }
            set { _ExtraChdSale = value; }
        }

        decimal? _ExtraInfSale;
        public decimal? ExtraInfSale
        {
            get { return _ExtraInfSale; }
            set { _ExtraInfSale = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        Int16? _CalcType;
        public Int16? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        string _PayCom;
        public string PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        string _PayComEB;
        public string PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        Int16? _StatConf;
        public Int16? StatConf
        {
            get { return _StatConf; }
            set { _StatConf = value; }
        }
    }

    public class OnlyFlight_FlightInfo
    {
        public OnlyFlight_FlightInfo()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        int? _Days;
        public int? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }

        string _DepFlight;
        public string DepFlight
        {
            get { return _DepFlight; }
            set { _DepFlight = value; }
        }

        string _DepFlightAirline;
        public string DepFlightAirline
        {
            get { return _DepFlightAirline; }
            set { _DepFlightAirline = value; }
        }

        string _DFullName;
        public string DFullName
        {
            get { return _DFullName; }
            set { _DFullName = value; }
        }

        Int16? _DepFreeSeat;
        public Int16? DepFreeSeat
        {
            get { return _DepFreeSeat; }
            set { _DepFreeSeat = value; }
        }

        Int16? _ArrFreeSeat;
        public Int16? ArrFreeSeat
        {
            get { return _ArrFreeSeat; }
            set { _ArrFreeSeat = value; }
        }

        Int16? _FreeSeat;
        public Int16? FreeSeat
        {
            get { return _FreeSeat; }
            set { _FreeSeat = value; }
        }

        DateTime? _DepDate;
        public DateTime? DepDate
        {
            get { return _DepDate; }
            set { _DepDate = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        string _DepClass;
        public string DepClass
        {
            get { return _DepClass; }
            set { _DepClass = value; }
        }

        int? _RouteFrom;
        public int? RouteFrom
        {
            get { return _RouteFrom; }
            set { _RouteFrom = value; }
        }

        string _RouteFromName;
        public string RouteFromName
        {
            get { return _RouteFromName; }
            set { _RouteFromName = value; }
        }

        int? _RouteTo;
        public int? RouteTo
        {
            get { return _RouteTo; }
            set { _RouteTo = value; }
        }

        string _RouteToName;
        public string RouteToName
        {
            get { return _RouteToName; }
            set { _RouteToName = value; }
        }

        string _DepSupp;
        public string DepSupp
        {
            get { return _DepSupp; }
            set { _DepSupp = value; }
        }

        string _ArrFlight;
        public string ArrFlight
        {
            get { return _ArrFlight; }
            set { _ArrFlight = value; }
        }

        string _ArrFlightAirline;
        public string ArrFlightAirline
        {
            get { return _ArrFlightAirline; }
            set { _ArrFlightAirline = value; }
        }

        string _AFullName;
        public string AFullName
        {
            get { return _AFullName; }
            set { _AFullName = value; }
        }

        DateTime? _ArrDate;
        public DateTime? ArrDate
        {
            get { return _ArrDate; }
            set { _ArrDate = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        string _ArrClass;
        public string ArrClass
        {
            get { return _ArrClass; }
            set { _ArrClass = value; }
        }

        string _ArrSupp;
        public string ArrSupp
        {
            get { return _ArrSupp; }
            set { _ArrSupp = value; }
        }

        string _MarketCurr;
        public string MarketCurr
        {
            get { return _MarketCurr; }
            set { _MarketCurr = value; }
        }

        bool? _IsArrFlight;
        public bool? IsArrFlight
        {
            get { return _IsArrFlight; }
            set { _IsArrFlight = value; }
        }

        decimal? _TotPrice;
        public decimal? TotPrice
        {
            get { return _TotPrice; }
            set { _TotPrice = value; }
        }

        decimal? _MaxChdAge;
        public decimal? MaxChdAge
        {
            get { return _MaxChdAge; }
            set { _MaxChdAge = value; }
        }

        decimal? _MaxInfAge;
        public decimal? MaxInfAge
        {
            get { return _MaxInfAge; }
            set { _MaxInfAge = value; }
        }

        decimal? _AdlTot;
        public decimal? AdlTot
        {
            get { return _AdlTot; }
            set { _AdlTot = value; }
        }

        decimal? _ChdTot;
        public decimal? ChdTot
        {
            get { return _ChdTot; }
            set { _ChdTot = value; }
        }

        decimal? _InfTot;
        public decimal? InfTot
        {
            get { return _InfTot; }
            set { _InfTot = value; }
        }

        decimal? _DepAdlTot;
        public decimal? DepAdlTot
        {
            get { return _DepAdlTot; }
            set { _DepAdlTot = value; }
        }

        decimal? _DepChdTot;
        public decimal? DepChdTot
        {
            get { return _DepChdTot; }
            set { _DepChdTot = value; }
        }

        decimal? _DepInfTot;
        public decimal? DepInfTot
        {
            get { return _DepInfTot; }
            set { _DepInfTot = value; }
        }

        decimal? _FlightDepAdlTot;
        public decimal? FlightDepAdlTot
        {
            get { return _FlightDepAdlTot; }
            set { _FlightDepAdlTot = value; }
        }

        decimal? _FlightDepChdTot;
        public decimal? FlightDepChdTot
        {
            get { return _FlightDepChdTot; }
            set { _FlightDepChdTot = value; }
        }

        decimal? _FlightDepInfTot;
        public decimal? FlightDepInfTot
        {
            get { return _FlightDepInfTot; }
            set { _FlightDepInfTot = value; }
        }

        decimal? _ExtraDepAdlTot;
        public decimal? ExtraDepAdlTot
        {
            get { return _ExtraDepAdlTot; }
            set { _ExtraDepAdlTot = value; }
        }

        decimal? _ExtraDepChdTot;
        public decimal? ExtraDepChdTot
        {
            get { return _ExtraDepChdTot; }
            set { _ExtraDepChdTot = value; }
        }

        decimal? _ExtraDepInfTot;
        public decimal? ExtraDepInfTot
        {
            get { return _ExtraDepInfTot; }
            set { _ExtraDepInfTot = value; }
        }

        decimal? _ArrAdlTot;
        public decimal? ArrAdlTot
        {
            get { return _ArrAdlTot; }
            set { _ArrAdlTot = value; }
        }

        decimal? _ArrChdTot;
        public decimal? ArrChdTot
        {
            get { return _ArrChdTot; }
            set { _ArrChdTot = value; }
        }

        decimal? _ArrInfTot;
        public decimal? ArrInfTot
        {
            get { return _ArrInfTot; }
            set { _ArrInfTot = value; }
        }

        decimal? _FlightArrAdlTot;
        public decimal? FlightArrAdlTot
        {
            get { return _FlightArrAdlTot; }
            set { _FlightArrAdlTot = value; }
        }

        decimal? _FlightArrChdTot;
        public decimal? FlightArrChdTot
        {
            get { return _FlightArrChdTot; }
            set { _FlightArrChdTot = value; }
        }

        decimal? _FlightArrInfTot;
        public decimal? FlightArrInfTot
        {
            get { return _FlightArrInfTot; }
            set { _FlightArrInfTot = value; }
        }

        decimal? _ExtraArrAdlTot;
        public decimal? ExtraArrAdlTot
        {
            get { return _ExtraArrAdlTot; }
            set { _ExtraArrAdlTot = value; }
        }

        decimal? _ExtraArrChdTot;
        public decimal? ExtraArrChdTot
        {
            get { return _ExtraArrChdTot; }
            set { _ExtraArrChdTot = value; }
        }

        decimal? _ExtraArrInfTot;
        public decimal? ExtraArrInfTot
        {
            get { return _ExtraArrInfTot; }
            set { _ExtraArrInfTot = value; }
        }

        decimal? _RoundDifDepAdlVal;
        public decimal? RoundDifDepAdlVal
        {
            get { return _RoundDifDepAdlVal; }
            set { _RoundDifDepAdlVal = value; }
        }

        decimal? _RoundDifDepChdVal;
        public decimal? RoundDifDepChdVal
        {
            get { return _RoundDifDepChdVal; }
            set { _RoundDifDepChdVal = value; }
        }

        decimal? _RoundDifDepInfVal;
        public decimal? RoundDifDepInfVal
        {
            get { return _RoundDifDepInfVal; }
            set { _RoundDifDepInfVal = value; }
        }

        decimal? _RoundDifArrAdlVal;
        public decimal? RoundDifArrAdlVal
        {
            get { return _RoundDifArrAdlVal; }
            set { _RoundDifArrAdlVal = value; }
        }

        decimal? _RoundDifArrChdVal;
        public decimal? RoundDifArrChdVal
        {
            get { return _RoundDifArrChdVal; }
            set { _RoundDifArrChdVal = value; }
        }

        decimal? _RoundDifArrInfVal;
        public decimal? RoundDifArrInfVal
        {
            get { return _RoundDifArrInfVal; }
            set { _RoundDifArrInfVal = value; }
        }

        int? _isNext;
        public int? IsNext
        {
            get { return _isNext; }
            set { _isNext = value; }
        }

        bool? _AllowRes;
        public bool? AllowRes
        {
            get { return _AllowRes; }
            set { _AllowRes = value; }
        }

        string _AllotType;
        public string AllotType
        {
            get { return _AllotType; }
            set { _AllotType = value; }
        }

        decimal? _EBPasPer;
        public decimal? EBPasPer
        {
            get { return _EBPasPer; }
            set { _EBPasPer = value; }
        }

        decimal? _EBP_AdlVal;
        public decimal? EBP_AdlVal
        {
            get { return _EBP_AdlVal; }
            set { _EBP_AdlVal = value; }
        }

        decimal? _EBP_ChdGrp1Val;
        public decimal? EBP_ChdGrp1Val
        {
            get { return _EBP_ChdGrp1Val; }
            set { _EBP_ChdGrp1Val = value; }
        }

        decimal? _EBP_ChdGrp2Val;
        public decimal? EBP_ChdGrp2Val
        {
            get { return _EBP_ChdGrp2Val; }
            set { _EBP_ChdGrp2Val = value; }
        }

        string _EBP_Cur;
        public string EBP_Cur
        {
            get { return _EBP_Cur; }
            set { _EBP_Cur = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        bool? _DepPriceIsUnion;
        public bool? DepPriceIsUnion
        {
            get { return _DepPriceIsUnion; }
            set { _DepPriceIsUnion = value; }
        }

        bool? _RetPriceIsUnion;
        public bool? RetPriceIsUnion
        {
            get { return _RetPriceIsUnion; }
            set { _RetPriceIsUnion = value; }
        }
    }

    public class OnlyTicket_EB
    {
        public OnlyTicket_EB()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        DateTime? _FlyDate;
        public DateTime? FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        decimal? _EBPasPer;
        public decimal? EBPasPer
        {
            get { return _EBPasPer; }
            set { _EBPasPer = value; }
        }

        decimal? _EBP_AdlVal;
        public decimal? EBP_AdlVal
        {
            get { return _EBP_AdlVal; }
            set { _EBP_AdlVal = value; }
        }

        decimal? _EBP_ChdGrp1Val;
        public decimal? EBP_ChdGrp1Val
        {
            get { return _EBP_ChdGrp1Val; }
            set { _EBP_ChdGrp1Val = value; }
        }

        decimal? _EBP_ChdGrp2Val;
        public decimal? EBP_ChdGrp2Val
        {
            get { return _EBP_ChdGrp2Val; }
            set { _EBP_ChdGrp2Val = value; }
        }

        string _EBP_Cur;
        public string EBP_Cur
        {
            get { return _EBP_Cur; }
            set { _EBP_Cur = value; }
        }
    }

    public class OnlyTicket_Flight
    {
        public OnlyTicket_Flight()
        {
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        DateTime? _DepDate;
        public DateTime? DepDate
        {
            get { return _DepDate; }
            set { _DepDate = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        string _DepFlight;
        public string DepFlight
        {
            get { return _DepFlight; }
            set { _DepFlight = value; }
        }

        string _DFullName;
        public string DFullName
        {
            get { return _DFullName; }
            set { _DFullName = value; }
        }

        string _DepClass;
        public string DepClass
        {
            get { return _DepClass; }
            set { _DepClass = value; }
        }

        string _DepFlightAirline;
        public string DepFlightAirline
        {
            get { return _DepFlightAirline; }
            set { _DepFlightAirline = value; }
        }

        DateTime? _ArrDate;
        public DateTime? ArrDate
        {
            get { return _ArrDate; }
            set { _ArrDate = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        string _ArrFlight;
        public string ArrFlight
        {
            get { return _ArrFlight; }
            set { _ArrFlight = value; }
        }

        string _AFullName;
        public string AFullName
        {
            get { return _AFullName; }
            set { _AFullName = value; }
        }

        string _ArrClass;
        public string ArrClass
        {
            get { return _ArrClass; }
            set { _ArrClass = value; }
        }

        string _ArrFlightAirline;
        public string ArrFlightAirline
        {
            get { return _ArrFlightAirline; }
            set { _ArrFlightAirline = value; }
        }

        Int16? _Days;
        public Int16? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }

        bool? _isRT;
        public bool? IsRT
        {
            get { return _isRT; }
            set { _isRT = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        Int16? _isNext;
        public Int16? IsNext
        {
            get { return _isNext; }
            set { _isNext = value; }
        }

        DateTime? _DepartureDepTime;
        public DateTime? DepartureDepTime
        {
            get { return _DepartureDepTime; }
            set { _DepartureDepTime = value; }
        }

        DateTime? _DepartureArrTime;
        public DateTime? DepartureArrTime
        {
            get { return _DepartureArrTime; }
            set { _DepartureArrTime = value; }
        }

        DateTime? _ReturnDepTime;
        public DateTime? ReturnDepTime
        {
            get { return _ReturnDepTime; }
            set { _ReturnDepTime = value; }
        }

        DateTime? _ReturnArrTime;
        public DateTime? ReturnArrTime
        {
            get { return _ReturnArrTime; }
            set { _ReturnArrTime = value; }
        }

        int? _DepFlightDuration;
        public int? DepFlightDuration
        {
            get { return _DepFlightDuration; }
            set { _DepFlightDuration = value; }
        }

        int? _RetFlightDuration;
        public int? RetFlightDuration
        {
            get { return _RetFlightDuration; }
            set { _RetFlightDuration = value; }
        }

        DateTime? _DepCountCloseTime;
        public DateTime? DepCountCloseTime
        {
            get { return _DepCountCloseTime; }
            set { _DepCountCloseTime = value; }
        }

        DateTime? _RetCountCloseTime;
        public DateTime? RetCountCloseTime
        {
            get { return _RetCountCloseTime; }
            set { _RetCountCloseTime = value; }
        }

    }

    public class FlightUPriceRecord
    {
        public FlightUPriceRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        string _ORT;
        public string ORT
        {
            get { return _ORT; }
            set { _ORT = value; }
        }

        DateTime? _DepDate;
        public DateTime? DepDate
        {
            get { return _DepDate; }
            set { _DepDate = value; }
        }

        string _DepFlight;
        public string DepFlight
        {
            get { return _DepFlight; }
            set { _DepFlight = value; }
        }

        string _DepClass;
        public string DepClass
        {
            get { return _DepClass; }
            set { _DepClass = value; }
        }

        decimal? _DepPrice;
        public decimal? DepPrice
        {
            get { return _DepPrice; }
            set { _DepPrice = value; }
        }

        decimal? _DepInfPrice;
        public decimal? DepInfPrice
        {
            get { return _DepInfPrice; }
            set { _DepInfPrice = value; }
        }

        decimal? _DepChdPrice;
        public decimal? DepChdPrice
        {
            get { return _DepChdPrice; }
            set { _DepChdPrice = value; }
        }

        DateTime? _ArrDate;
        public DateTime? ArrDate
        {
            get { return _ArrDate; }
            set { _ArrDate = value; }
        }

        string _ArrFlight;
        public string ArrFlight
        {
            get { return _ArrFlight; }
            set { _ArrFlight = value; }
        }

        string _ArrClass;
        public string ArrClass
        {
            get { return _ArrClass; }
            set { _ArrClass = value; }
        }

        decimal? _ArrPrice;
        public decimal? ArrPrice
        {
            get { return _ArrPrice; }
            set { _ArrPrice = value; }
        }

        decimal? _ArrInfPrice;
        public decimal? ArrInfPrice
        {
            get { return _ArrInfPrice; }
            set { _ArrInfPrice = value; }
        }

        decimal? _ArrChdPrice;
        public decimal? ArrChdPrice
        {
            get { return _ArrChdPrice; }
            set { _ArrChdPrice = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        string _Stop;
        public string Stop
        {
            get { return _Stop; }
            set { _Stop = value; }
        }

        string _AgencyGrp;
        public string AgencyGrp
        {
            get { return _AgencyGrp; }
            set { _AgencyGrp = value; }
        }

    }

    public class FlightPriceRecord
    {
        public FlightPriceRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int? _BlockID;
        public int? BlockID
        {
            get { return _BlockID; }
            set { _BlockID = value; }
        }

        int? _ParentBlockID;
        public int? ParentBlockID
        {
            get { return _ParentBlockID; }
            set { _ParentBlockID = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        string _Buyer;
        public string Buyer
        {
            get { return _Buyer; }
            set { _Buyer = value; }
        }

        Int16? _SalePriority;
        public Int16? SalePriority
        {
            get { return _SalePriority; }
            set { _SalePriority = value; }
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        DateTime? _FlyDate;
        public DateTime? FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        string _BS;
        public string BS
        {
            get { return _BS; }
            set { _BS = value; }
        }

        int? _Seat;
        public int? Seat
        {
            get { return _Seat; }
            set { _Seat = value; }
        }

        string _SClass;
        public string SClass
        {
            get { return _SClass; }
            set { _SClass = value; }
        }

        string _BlockType;
        public string BlockType
        {
            get { return _BlockType; }
            set { _BlockType = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _NetPrice;
        public decimal? NetPrice
        {
            get { return _NetPrice; }
            set { _NetPrice = value; }
        }

        decimal? _NetChd;
        public decimal? NetChd
        {
            get { return _NetChd; }
            set { _NetChd = value; }
        }

        decimal? _NetInf;
        public decimal? NetInf
        {
            get { return _NetInf; }
            set { _NetInf = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        decimal? _SaleChd;
        public decimal? SaleChd
        {
            get { return _SaleChd; }
            set { _SaleChd = value; }
        }

        decimal? _SaleInf;
        public decimal? SaleInf
        {
            get { return _SaleInf; }
            set { _SaleInf = value; }
        }

        string _Comment;
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        Int16? _FreeSeat;
        public Int16? FreeSeat
        {
            get { return _FreeSeat; }
            set { _FreeSeat = value; }
        }

    }

    public class TicketRestRecord
    {
        public TicketRestRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        string _Class;
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _FromDay;
        public Int16? FromDay
        {
            get { return _FromDay; }
            set { _FromDay = value; }
        }

        Int16? _DayOption;
        public Int16? DayOption
        {
            get { return _DayOption; }
            set { _DayOption = value; }
        }

        Int16? _SalePerc;
        public Int16? SalePerc
        {
            get { return _SalePerc; }
            set { _SalePerc = value; }
        }

        string _StopSale;
        public string StopSale
        {
            get { return _StopSale; }
            set { _StopSale = value; }
        }

        string _TVActive;
        public string TVActive
        {
            get { return _TVActive; }
            set { _TVActive = value; }
        }

        Int16? _ToDay;
        public Int16? ToDay
        {
            get { return _ToDay; }
            set { _ToDay = value; }
        }

        string _FlyDays;
        public string FlyDays
        {
            get { return _FlyDays; }
            set { _FlyDays = value; }
        }

    }

    [Serializable]
    public class TicketCustStrRecord
    {
        public TicketCustStrRecord()
        {
        }

        string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _BirthDate;
        public string BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }

        string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

    }

    public class HolPackFlightRecord
    {
        public HolPackFlightRecord()
        {
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        int? _Departure;
        public int? Departure
        {
            get { return _Departure; }
            set { _Departure = value; }
        }

        int? _Arrival;
        public int? Arrival
        {
            get { return _Arrival; }
            set { _Arrival = value; }
        }

        Int16? _StepType;
        public Int16? StepType
        {
            get { return _StepType; }
            set { _StepType = value; }
        }

        Int16? _StartDay;
        public Int16? StartDay
        {
            get { return _StartDay; }
            set { _StartDay = value; }
        }
    }

    public class LockedPLFlightRecord
    {
        public LockedPLFlightRecord()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        DateTime? _FlyDate;
        public DateTime? FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        string _AllotType;
        public string AllotType
        {
            get { return _AllotType; }
            set { _AllotType = value; }
        }

        string _SClass;
        public string SClass
        {
            get { return _SClass; }
            set { _SClass = value; }
        }

        Int16? _StepNo;
        public Int16? StepNo
        {
            get { return _StepNo; }
            set { _StepNo = value; }
        }

    }

    public class AirlineRecord
    {
        public AirlineRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _Phone1;
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        string _Phone2;
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }

        string _Fax;
        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }

        string _Web;
        public string Web
        {
            get { return _Web; }
            set { _Web = value; }
        }

        string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        string _CarCode;
        public string CarCode
        {
            get { return _CarCode; }
            set { _CarCode = value; }
        }

        string _Explanation;
        public string Explanation
        {
            get { return _Explanation; }
            set { _Explanation = value; }
        }

        string _AirlineCode;
        public string AirlineCode
        {
            get { return _AirlineCode; }
            set { _AirlineCode = value; }
        }

        string _LicenseNo;
        public string LicenseNo
        {
            get { return _LicenseNo; }
            set { _LicenseNo = value; }
        }

        string _Address;
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        string _AddrZip;
        public string AddrZip
        {
            get { return _AddrZip; }
            set { _AddrZip = value; }
        }

        string _AddrCity;
        public string AddrCity
        {
            get { return _AddrCity; }
            set { _AddrCity = value; }
        }

        string _AddrCountry;
        public string AddrCountry
        {
            get { return _AddrCountry; }
            set { _AddrCountry = value; }
        }

        bool? _ETicketExists;
        public bool? ETicketExists
        {
            get { return _ETicketExists; }
            set { _ETicketExists = value; }
        }

        bool? _PNLUse;
        public bool? PNLUse
        {
            get { return _PNLUse; }
            set { _PNLUse = value; }
        }

        string _PNLFullName;
        public string PNLFullName
        {
            get { return _PNLFullName; }
            set { _PNLFullName = value; }
        }

        string _PNLFlightSpace;
        public string PNLFlightSpace
        {
            get { return _PNLFlightSpace; }
            set { _PNLFlightSpace = value; }
        }

        string _PNLNameSpace;
        public string PNLNameSpace
        {
            get { return _PNLNameSpace; }
            set { _PNLNameSpace = value; }
        }

        string _PNLChdRemark;
        public string PNLChdRemark
        {
            get { return _PNLChdRemark; }
            set { _PNLChdRemark = value; }
        }

        string _PNLSitatex;
        public string PNLSitatex
        {
            get { return _PNLSitatex; }
            set { _PNLSitatex = value; }
        }

        int? _PNLMaxLineInPart;
        public int? PNLMaxLineInPart
        {
            get { return _PNLMaxLineInPart; }
            set { _PNLMaxLineInPart = value; }
        }

        string _PNLDispExtra;
        public string PNLDispExtra
        {
            get { return _PNLDispExtra; }
            set { _PNLDispExtra = value; }
        }

        string _PNLDispPNR;
        public string PNLDispPNR
        {
            get { return _PNLDispPNR; }
            set { _PNLDispPNR = value; }
        }

        string _PNLDotAfterTitle;
        public string PNLDotAfterTitle
        {
            get { return _PNLDotAfterTitle; }
            set { _PNLDotAfterTitle = value; }
        }

        bool? _PNLAutoSend;
        public bool? PNLAutoSend
        {
            get { return _PNLAutoSend; }
            set { _PNLAutoSend = value; }
        }

        string _PNLMailTo;
        public string PNLMailTo
        {
            get { return _PNLMailTo; }
            set { _PNLMailTo = value; }
        }

        string _PNLMailCC;
        public string PNLMailCC
        {
            get { return _PNLMailCC; }
            set { _PNLMailCC = value; }
        }

        string _PNLResNoDispPNR;
        public string PNLResNoDispPNR
        {
            get { return _PNLResNoDispPNR; }
            set { _PNLResNoDispPNR = value; }
        }

        bool? _PNLDispGrpNo;
        public bool? PNLDispGrpNo
        {
            get { return _PNLDispGrpNo; }
            set { _PNLDispGrpNo = value; }
        }

        bool? _TicketIssued;
        public bool? TicketIssued
        {
            get { return _TicketIssued; }
            set { _TicketIssued = value; }
        }
    }

    public class getPLFlightsRecord
    {
        public getPLFlightsRecord()
        {
        }

        string _DepFlightNo;
        public string DepFlightNo
        {
            get { return _DepFlightNo; }
            set { _DepFlightNo = value; }
        }

        string _DepFlgClass;
        public string DepFlgClass
        {
            get { return _DepFlgClass; }
            set { _DepFlgClass = value; }
        }

        string _DepFlgClassName;
        public string DepFlgClassName
        {
            get { return _DepFlgClassName; }
            set { _DepFlgClassName = value; }
        }

        DateTime? _DepFlyDate;
        public DateTime? DepFlyDate
        {
            get { return _DepFlyDate; }
            set { _DepFlyDate = value; }
        }

        DateTime? _DepFlyTime;
        public DateTime? DepFlyTime
        {
            get { return _DepFlyTime; }
            set { _DepFlyTime = value; }
        }

        string _DepAirline;
        public string DepAirline
        {
            get { return _DepAirline; }
            set { _DepAirline = value; }
        }

        string _NextFlightNo;
        public string NextFlightNo
        {
            get { return _NextFlightNo; }
            set { _NextFlightNo = value; }
        }

        string _RetFlightNo;
        public string RetFlightNo
        {
            get { return _RetFlightNo; }
            set { _RetFlightNo = value; }
        }

        string _RetFlgClass;
        public string RetFlgClass
        {
            get { return _RetFlgClass; }
            set { _RetFlgClass = value; }
        }

        string _RetFlgClassName;
        public string RetFlgClassName
        {
            get { return _RetFlgClassName; }
            set { _RetFlgClassName = value; }
        }

        DateTime? _RetFlyDate;
        public DateTime? RetFlyDate
        {
            get { return _RetFlyDate; }
            set { _RetFlyDate = value; }
        }

        DateTime? _RetFlyTime;
        public DateTime? RetFlyTime
        {
            get { return _RetFlyTime; }
            set { _RetFlyTime = value; }
        }

        int? _DepSeat;
        public int? DepSeat
        {
            get { return _DepSeat; }
            set { _DepSeat = value; }
        }

        int? _RetSeat;
        public int? RetSeat
        {
            get { return _RetSeat; }
            set { _RetSeat = value; }
        }
    }

    public class FirstFlightRecord
    {
        public FirstFlightRecord()
        {
        }

        Int64? _CheckIn;
        public Int64? CheckIn
        {
            get { return _CheckIn; }
            set { _CheckIn = value; }
        }

        DateTime? _FlyDate;
        public DateTime? FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _dateFormat;
        public string DateFormat
        {
            get { return _dateFormat; }
            set { _dateFormat = value; }
        }
    }

    public class FlightsText
    {
        public FlightsText()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        string _RTFText;
        public string RTFText
        {
            get { return _RTFText; }
            set { _RTFText = value; }
        }

        string _HTMLText;
        public string HTMLText
        {
            get { return _HTMLText; }
            set { _HTMLText = value; }
        }
    }

    public class FlightInfosRecord
    {
        public FlightInfosRecord()
        {
        }

        string _DepFlightNo;
        public string DepFlightNo
        {
            get { return _DepFlightNo; }
            set { _DepFlightNo = value; }
        }

        DateTime? _DepFlyDate;
        public DateTime? DepFlyDate
        {
            get { return _DepFlyDate; }
            set { _DepFlyDate = value; }
        }

        DateTime? _DepFlyTime;
        public DateTime? DepFlyTime
        {
            get { return _DepFlyTime; }
            set { _DepFlyTime = value; }
        }

        string _DepAirline;
        public string DepAirline
        {
            get { return _DepAirline; }
            set { _DepAirline = value; }
        }

        string _NextFlightNo;
        public string NextFlightNo
        {
            get { return _NextFlightNo; }
            set { _NextFlightNo = value; }
        }

        string _RetFlightNo;
        public string RetFlightNo
        {
            get { return _RetFlightNo; }
            set { _RetFlightNo = value; }
        }

        DateTime? _RetFlyDate;
        public DateTime? RetFlyDate
        {
            get { return _RetFlyDate; }
            set { _RetFlyDate = value; }
        }

        DateTime? _RetFlyTime;
        public DateTime? RetFlyTime
        {
            get { return _RetFlyTime; }
            set { _RetFlyTime = value; }
        }

        int? _DepSeat;
        public int? DepSeat
        {
            get { return _DepSeat; }
            set { _DepSeat = value; }
        }

        int? _RetSeat;
        public int? RetSeat
        {
            get { return _RetSeat; }
            set { _RetSeat = value; }
        }

        int? _Priority;
        public int? Priority
        {
            get { return _Priority; }
            set { _Priority = value; }
        }
    }

    public class FlightDayClassRecord
    {
        public FlightDayClassRecord()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        DateTime? _FlyDate;
        public DateTime? FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        string _BlockType;
        public string BlockType
        {
            get { return _BlockType; }
            set { _BlockType = value; }
        }

        string _SClass;
        public string SClass
        {
            get { return _SClass; }
            set { _SClass = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _CompareClass;
        public string CompareClass
        {
            get { return _CompareClass; }
            set { _CompareClass = value; }
        }
    }

    public class PlaneTemp
    {
        public int? RecID { get; set; }
        public string Code { get; set; }
        public string PlaneName { get; set; }
        public string Exits { get; set; }
        public string SkipList { get; set; }
        public PlaneTemp()
        {
        }
    }

    public class PlaneTempZone
    {
        public int? RecID { get; set; }
        public int? PlaneID { get; set; }
        public string ZoneName { get; set; }
        public Int16? RangeStart { get; set; }
        public Int16? RangeEnd { get; set; }
        public Int16? ItemLength { get; set; }
        public int? MaxRow { get; set; }
        public int? MaxCol { get; set; }
        public List<PlaneTempZoneItems> ZoneItems { get; set; }
        public int? SeatWidth { get; set; }
        public PlaneTempZone()
        {
            this.MaxCol = 0;
            this.MaxRow = 0;
            this.ZoneItems = new List<PlaneTempZoneItems>();
        }
    }

    public class PlaneTempZoneItems
    {
        public int? RecID { get; set; }
        public int? PlaneID { get; set; }
        public int? ZoneId { get; set; }
        public string SeatLetter { get; set; }
        public Int16? ItemRow { get; set; }
        public Int16? ItemCol { get; set; }
        public enmItemType ItemType { get; set; }
        public bool? ForB2B { get; set; }
        public bool? ForB2C { get; set; }
        public bool? IsExit { get; set; }
        public Int16? SeatNo { get; set; }
        public bool? ForBackOffice { get; set; }
        public bool? ExtraLegroom { get; set; }
        public string ExtraService { get; set; }
        public string ExtraServicePrice { get; set; }
        public bool isFull { get; set; }
        public bool thisReservation { get; set; }
        public PlaneTempZoneItems()
        {
            this.isFull = false;
            this.thisReservation = false;
        }
    }

    public class FlightCIn
    {
        public int? RecID { get; set; }
        public string Flight { get; set; }
        public DateTime? FlightDate { get; set; }
        public int? FlightTemp { get; set; }
        public int? CustNo { get; set; }
        public int? FlightDay { get; set; }
        public string SeatLetter { get; set; }
        public Int16? SeatNo { get; set; }
        public int? ServiceID { get; set; }
        public int? ServiceExtID { get; set; }
        public string OldSeatImg { get; set; }
        public FlightCIn()
        {
        }
    }

    public class SeatCheckInExtraService
    {
        public string extraServiceCode { get; set; }
        public ExtraServiceListRecord extraPrice { get; set; }
        public CodeName extraPriceStr { get; set; }
        public ServiceExtRecord extraServiceDetail { get; set; }
        public SeatCheckInExtraService()
        {
        }
    }

    public class SeatCheckInPlanDetail
    {
        public List<SeatCheckInExtraService> seatCheckInExtras { get; set; }
        public List<SSRQCodeRecord> ssrqCodes { get; set; }
        public SeatCheckInPlanDetail()
        {
            this.seatCheckInExtras = new List<SeatCheckInExtraService>();
            this.ssrqCodes = new List<SSRQCodeRecord>();
        }
    }

    public class FlightSeatCustomerList
    {
        public int? CustNo { get; set; }
        public Int16? Title { get; set; }
        public string TitleStr { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public FlightCIn Seat { get; set; }

        public FlightSeatCustomerList()
        {
            this.Seat = null;
        }
    }

    public class FlightTypeRecord
    {
        public int? RecID { get; set; }
        public string Name { get; set; }
        public FlightTypeRecord()
        {

        }
    }
}

public struct Profmars
{
    public decimal? GenPer, GenVal, AdlPer, AdlVal, ChdG1Per, ChdG1Val, ChdG2Per, ChdG2Val, ChdG3Per, ChdG4Per, ChdG3Val, ChdG4Val, AgencyCom;
    public int? RuleNo;
    public string Cur;
}
