﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class srchLocations
    {
        public srchLocations() { }
        public int? DepCity { get; set; }
        public string DepCityName { get; set; }
        public string DepCityNameL { get; set; }
        public int? ArrCountry { get; set; }
        public string ArrCountryName { get; set; }
        public string ArrCountryNameL { get; set; }
        public int? ArrCity { get; set; }
        public string ArrCityName { get; set; }
        public string ArrCityNameL { get; set; }
        public int? Direction { get; set; }
        public int? Category { get; set; }
        public string CategoryName { get; set; }
        public string PackType { get; set; }
    }

    public class srchCategory
    {
        public srchCategory() { }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public int? Country { get; set; }
        public int? PL_Country { get; set; }
        public int? City { get; set; }
        public int? PL_City { get; set; }
        public int? Town { get; set; }
        public int? Village { get; set; }
        public int? Direction { get; set; }
        public int? Category { get; set; }
        public string PackType { get; set; }
    }

    public class srchHolPack
    {
        public srchHolPack() { }
        public string Name { get; set; }
        public string NameL { get; set; }
        public string HolPack { get; set; }
        public int? DepCity { get; set; }
        public int? ArrCity { get; set; }
        public int? Country { get; set; }
        public int? CatPackID { get; set; }
        public int? Direction { get; set; }
        public int? Category { get; set; }
        public string PackType { get; set; }
    }

    public class srchBoards
    {
        public srchBoards() { }
        public string Name { get; set; }
        public string NameL { get; set; }
        public string Hotel_Code { get; set; }
        public string Code { get; set; }
        public int? City { get; set; }
        public int? Town { get; set; }
        public int? Village { get; set; }
        public int? PLArrCity { get; set; }
        public int? PLDepCity { get; set; }
        public int? Direction { get; set; }
        public int? Category { get; set; }
        public string PackType { get; set; }
    }

    public class srchRooms
    {
        public srchRooms() { }
        public string Name { get; set; }
        public string NameL { get; set; }
        public string Hotel_Code { get; set; }
        public string Code { get; set; }
        public int? City { get; set; }
        public int? Town { get; set; }
        public int? Village { get; set; }
        public int? PLArrCity { get; set; }
        public int? PLDepCity { get; set; }
        public int? Direction { get; set; }
        public int? Category { get; set; }
        public string PackType { get; set; }
    }

    public class srchResort
    {
        public srchResort() { }
        public string Name { get; set; }
        public string NameL { get; set; }
        public int? RecID { get; set; }
        public int? Country { get; set; }
        public int? City { get; set; }
        public int? Direction { get; set; }
        public int? Category { get; set; }        
        public string PackType { get; set; }
    }

    public class srchHotels
    {
        public srchHotels() { }
        public string Name { get; set; }
        public string NameL { get; set; }
        public string Code { get; set; }
        public string Category { get; set; }
        public int? Location { get; set; }
        public string LocationName { get; set; }
        public string LocationNameL { get; set; }
        public int? Country { get; set; }
        public string CountryName { get; set; }
        public string CountryNameL { get; set; }
        public int? City { get; set; }
        public string CityName { get; set; }
        public string CityNameL { get; set; }
        public int? Town { get; set; }
        public string TownName { get; set; }
        public string TownNameL { get; set; }
        public int? ArrCity { get; set; }
        public string ArrCityName { get; set; }
        public string ArrCityNameL { get; set; }
        public int? CatPackID { get; set; }
        public string Holpack { get; set; }
        public string HolpackName { get; set; }
        public string HolpackNameL { get; set; }        
        public int? Direction { get; set; }
        public int? HolPackCat { get; set; }
        public string PackType { get; set; }
    }

    public class srchHolPackCat
    {
        public srchHolPackCat() { }
        public int? RecID { get; set; }
        public int? Direction { get; set; }
        public int? Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
    }

    public class srchMaxPaxCounts
    {
        public srchMaxPaxCounts() { }
        public int? Adult { get; set; }
        public int? ChdAgeG1 { get; set; }
        public int? ChdAgeG2 { get; set; }
        public int? ChdAgeG3 { get; set; }
        public int? ChdAgeG4 { get; set; }
        public string PackType { get; set; }
    }

    public class srchFilterData
    {
        public srchFilterData() { }
        public List<srchLocations> SrchLocation { get; set; }
        public List<srchCategory> SrchCategory { get; set; }
        public List<srchHolPack> SrchHolPack { get; set; }
        public List<srchBoards> SrchBoards { get; set; }
        public List<srchRooms> SrchRooms { get; set; }
        public List<srchResort> SrchResort { get; set; }
        public List<srchHotels> SrchHotels { get; set; }
        public List<srchHolPackCat> SrchHolPackCat { get; set; }
        public List<srchMaxPaxCounts> SrchMaxPaxCounts { get; set; }
    }
}
