﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace TvBo
{
    public class apiResponse
    {
        public string code { get; set; }
        public string message { get; set; }
        public string token { get; set; }
        public string apiUrl { get; set; }
    }

    [Serializable()]
    public class PaximumUserToken
    {
        public string token { get; set; }
        public string paximumApiUrl { get; set; }
        public string paximumB2BUrl { get; set; }

        public PaximumUserToken()
        {
        }
    }

    public class GenerateTokenForTourvisio
    {
        public string TvCustomerNo { get; set; }
        public string UserId { get; set; }
        public string AccountId { get; set; }
    }

    [Serializable]
    public class User
    {
        public HeaderData HeaderData { get; set; }
        public List<GridOptions> GridColumnOptions { get; set; }
        public TvParameters TvParams { get; set; }
        public AgencyRecord AgencyRec { get; set; }
        public string IpNumber { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string UserNameL { get; set; }
        public string Password { get; set; }
        public string AgencyID { get; set; }
        public string AgencyName { get; set; }
        public string AgencyNameL { get; set; }
        public string OprOffice { get; set; }
        public string Operator { get; set; }
        public string Market { get; set; }
        public string MainOffice { get; set; }
        public string MainOfficeName { get; set; }
        public bool BlackList { get; set; }
        public bool MainAgency { get; set; }
        public Int16? AgencyType { get; set; }
        public string SID { get; set; }
        public string Authenticated { get; set; }
        public bool MasterAgency { get; set; }
        public string SaleCur { get; set; }
        public bool OwnAgency { get; set; }
        public Int16 PayOptTime { get; set; }
        public string CustomRegID { get; set; }
        public bool ShowAllRes { get; set; }
        public bool ShowAllOtherOffRes { get; set; }
        public byte MaxRoomCount { get; set; }
        public CultureInfo Ci { get; set; }
        public string TvVersion { get; set; }
        public string WebVersion { get; set; }
        public bool ShowFlight { get; set; }
        public bool WebService { get; set; }
        public bool IsB2CAgency { get; set; }
        public string CuratorUser { get; set; }
        public string MerlinxID { get; set; }
        public bool Aggregator { get; set; }
        public int? Country { get; set; }
        public int? Location { get; set; }
        public bool AgencyCanDisCom { get; set; }
        public PhoneMaskRecord PhoneMask { get; set; }
        public WDocRecord WDoc { get; set; }
        public BonusRecord Bonus { get; set; }
        public string PIN { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string EMail { get; set; }
        public string HAddress { get; set; }
        public string Bank { get; set; }
        public string BankNo { get; set; }
        public string BankAccNo { get; set; }
        public string AgencyRating { get; set; }
        public bool ExtUser { get; set; }
        public bool CheckMarketLang { get; set; }
        public int? MarketLang { get; set; }
        public bool EqMarketLang { get; set; }
        public bool? CanEditOthRes { get; set; }
        public bool? CanPayment { get; set; }
        public PaximumRecord PaxSetting { get; set; }
        public string LoginId { get; set; }
        public PaximumUserToken PaxToken { get; set; }
        public string Nationality { get; set; }
        public bool Pxm_Use { get; set; }
        public bool IsMobileDevice { get; set; }
        public User()
        {
            this.HeaderData = new HeaderData();
            this.GridColumnOptions = new List<GridOptions>();
            this.TvParams = new TvParameters();
            this.AgencyRec = new AgencyRecord();
            this.PhoneMask = new PhoneMaskRecord();
            this.WDoc = new WDocRecord();
            this.Bonus = new BonusRecord();
            this.ExtUser = false;
            this.EqMarketLang = false;
            this.CheckMarketLang = false;
            this.CanEditOthRes = false;
            this.CanPayment = false;
            this.PaxSetting = null;
            this.Pxm_Use = false;
        }
    }

    [Serializable]
    public class WDocRecord
    {
        public WDocRecord()
        {
            _WDoc_Voucher = true;
            _WDoc_Ticket = true;
            _WDoc_Agree = true;
            _WDoc_InvoiceA = true;
            _WDoc_InvoiceP = true;
            _WDoc_Insurance = true;
            _WDoc_Receipt = true;
            _WDoc_TransportVoucher = true;
            _WDoc_ExcursionVoucher = true;
            _WDoc_Confirmation = true;
        }

        bool _WDoc_Voucher;
        public bool WDoc_Voucher
        {
            get { return _WDoc_Voucher; }
            set { _WDoc_Voucher = value; }
        }

        bool _WDoc_Ticket;
        public bool WDoc_Ticket
        {
            get { return _WDoc_Ticket; }
            set { _WDoc_Ticket = value; }
        }

        bool _WDoc_Agree;
        public bool WDoc_Agree
        {
            get { return _WDoc_Agree; }
            set { _WDoc_Agree = value; }
        }

        bool _WDoc_InvoiceA;
        public bool WDoc_InvoiceA
        {
            get { return _WDoc_InvoiceA; }
            set { _WDoc_InvoiceA = value; }
        }

        bool _WDoc_InvoiceP;
        public bool WDoc_InvoiceP
        {
            get { return _WDoc_InvoiceP; }
            set { _WDoc_InvoiceP = value; }
        }

        bool _WDoc_Insurance;
        public bool WDoc_Insurance
        {
            get { return _WDoc_Insurance; }
            set { _WDoc_Insurance = value; }
        }

        bool _WDoc_Receipt;
        public bool WDoc_Receipt
        {
            get { return _WDoc_Receipt; }
            set { _WDoc_Receipt = value; }
        }

        bool _WDoc_TransportVoucher;
        public bool WDoc_TransportVoucher
        {
            get { return _WDoc_TransportVoucher; }
            set { _WDoc_TransportVoucher = value; }
        }

        bool _WDoc_ExcursionVoucher;
        public bool WDoc_ExcursionVoucher
        {
            get { return _WDoc_ExcursionVoucher; }
            set { _WDoc_ExcursionVoucher = value; }
        }

        bool _WDoc_Confirmation;
        public bool WDoc_Confirmation
        {
            get { return _WDoc_Confirmation; }
            set { _WDoc_Confirmation = value; }
        }
    }

    [Serializable]
    public class BonusRecord
    {
        public BonusRecord()
        {
            _AgencyBonus = false;
            _UserBonus = false;
            _PassBonus = false;
            _BonusUserSeeAgencyW = false;
            _BonusAgencySeeOwnW = false;
            _BonusUserSeeOwnW = false;
        }

        bool _AgencyBonus;
        public bool AgencyBonus
        {
            get { return _AgencyBonus; }
            set { _AgencyBonus = value; }
        }

        bool _UserBonus;
        public bool UserBonus
        {
            get { return _UserBonus; }
            set { _UserBonus = value; }
        }

        bool _PassBonus;
        public bool PassBonus
        {
            get { return _PassBonus; }
            set { _PassBonus = value; }
        }

        bool _BonusUserSeeAgencyW;
        public bool BonusUserSeeAgencyW
        {
            get { return _BonusUserSeeAgencyW; }
            set { _BonusUserSeeAgencyW = value; }
        }

        bool _BonusAgencySeeOwnW;
        public bool BonusAgencySeeOwnW
        {
            get { return _BonusAgencySeeOwnW; }
            set { _BonusAgencySeeOwnW = value; }
        }

        bool _BonusUserSeeOwnW;
        public bool BonusUserSeeOwnW
        {
            get { return _BonusUserSeeOwnW; }
            set { _BonusUserSeeOwnW = value; }
        }

    }

    [Serializable()]
    public class WEBUserLogRecord
    {

        public WEBUserLogRecord()
        {
        }

        int? _RECID;
        public int? RECID
        {
            get { return _RECID; }
            set { _RECID = value; }
        }

        string _SESSIONID;
        public string SESSIONID
        {
            get { return _SESSIONID; }
            set { _SESSIONID = value; }
        }

        string _USERID;
        public string USERID
        {
            get { return _USERID; }
            set { _USERID = value; }
        }

        string _AGENCYID;
        public string AGENCYID
        {
            get { return _AGENCYID; }
            set { _AGENCYID = value; }
        }

        string _USERIP;
        public string USERIP
        {
            get { return _USERIP; }
            set { _USERIP = value; }
        }

        string _BROWSERINFO;
        public string BROWSERINFO
        {
            get { return _BROWSERINFO; }
            set { _BROWSERINFO = value; }
        }

        DateTime? _LOGIN;
        public DateTime? LOGIN
        {
            get { return _LOGIN; }
            set { _LOGIN = value; }
        }

        DateTime? _LOGOUT;
        public DateTime? LOGOUT
        {
            get { return _LOGOUT; }
            set { _LOGOUT = value; }
        }

        bool? _INUSE;
        public bool? INUSE
        {
            get { return _INUSE; }
            set { _INUSE = value; }
        }
    }
}
