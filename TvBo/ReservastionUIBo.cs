﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    [Serializable()]
    public class CompulsoryResCustFields
    {
        public CompulsoryResCustFields()
        { 
        }

        string _tableName;
        public string tableName
        {
            get { return _tableName; }
            set { _tableName = value; }
        }

        string _fieldName;
        public string fieldName
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }
    }

    [Serializable()]
    public class onlyText_ResServiceRecord
    {
        public onlyText_ResServiceRecord()
        {         
        }
        
        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        string _ServiceTypeName;
        public string ServiceTypeName
        {
            get { return _ServiceTypeName; }
            set { _ServiceTypeName = value; }
        }

        string _ServiceTypeNameL;
        public string ServiceTypeNameL
        {
            get { return _ServiceTypeNameL; }
            set { _ServiceTypeNameL = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _ServiceName;
        public string ServiceName
        {
            get { return _ServiceName; }
            set { _ServiceName = value; }
        }

        string _ServiceNameL;
        public string ServiceNameL
        {
            get { return _ServiceNameL; }
            set { _ServiceNameL = value; }
        }

        string _DepLocationName;
        public string DepLocationName
        {
            get { return _DepLocationName; }
            set { _DepLocationName = value; }
        }

        string _DepLocationNameL;
        public string DepLocationNameL
        {
            get { return _DepLocationNameL; }
            set { _DepLocationNameL = value; }
        }

        string _ArrLocationName;
        public string ArrLocationName
        {
            get { return _ArrLocationName; }
            set { _ArrLocationName = value; }
        }

        string _ArrLocationNameL;
        public string ArrLocationNameL
        {
            get { return _ArrLocationNameL; }
            set { _ArrLocationNameL = value; }
        }

        string _BegDate;
        public string BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        string _EndDate;
        public string EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _Duration;
        public string Duration
        {
            get { return _Duration; }
            set { _Duration = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        string _SupplierName;
        public string SupplierName
        {
            get { return _SupplierName; }
            set { _SupplierName = value; }
        }

        string _SupplierNameL;
        public string SupplierNameL
        {
            get { return _SupplierNameL; }
            set { _SupplierNameL = value; }
        }

        string _Unit;
        public string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _RoomName;
        public string RoomName
        {
            get { return _RoomName; }
            set { _RoomName = value; }
        }

        string _RoomNameL;
        public string RoomNameL
        {
            get { return _RoomNameL; }
            set { _RoomNameL = value; }
        }

        string _Accom;
        public string Accom
        {
            get { return _Accom; }
            set { _Accom = value; }
        }

        string _AccomName;
        public string AccomName
        {
            get { return _AccomName; }
            set { _AccomName = value; }
        }

        string _AccomNameL;
        public string AccomNameL
        {
            get { return _AccomNameL; }
            set { _AccomNameL = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _BoardName;
        public string BoardName
        {
            get { return _BoardName; }
            set { _BoardName = value; }
        }

        string _BoardNameL;
        public string BoardNameL
        {
            get { return _BoardNameL; }
            set { _BoardNameL = value; }
        }

        string _Adult;
        public string Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        string _Child;
        public string Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        string _FlgClass;
        public string FlgClass
        {
            get { return _FlgClass; }
            set { _FlgClass = value; }
        }
        
        string _SalePrice;
        public string SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }        

        string _StatConfName;
        public string StatConfName
        {
            get { return _StatConfName; }
            set { _StatConfName = value; }
        }

        string _StatConfNameL;
        public string StatConfNameL
        {
            get { return _StatConfNameL; }
            set { _StatConfNameL = value; }
        }
        
        string _StatSerName;
        public string StatSerName
        {
            get { return _StatSerName; }
            set { _StatSerName = value; }
        }

        string _StatSerNameL;
        public string StatSerNameL
        {
            get { return _StatSerNameL; }
            set { _StatSerNameL = value; }
        }
        
        string _SupNote;
        public string SupNote
        {
            get { return _SupNote; }
            set { _SupNote = value; }
        }

        string _IncPack;
        public string IncPack
        {
            get { return _IncPack; }
            set { _IncPack = value; }
        }
        
        string _Bus;
        public string Bus
        {
            get { return _Bus; }
            set { _Bus = value; }
        }

        string _BusName;
        public string BusName
        {
            get { return _BusName; }
            set { _BusName = value; }
        }
        
        bool? _Compulsory;
        public bool? Compulsory
        {
            get { return _Compulsory; }
            set { _Compulsory = value; }
        }
                        
        string _PickupName;
        public string PickupName
        {
            get { return _PickupName; }
            set { _PickupName = value; }
        }

        string _PickupNameL;
        public string PickupNameL
        {
            get { return _PickupNameL; }
            set { _PickupNameL = value; }
        }        

        string _PickupTime;
        public string PickupTime
        {
            get { return _PickupTime; }
            set { _PickupTime = value; }
        }

        string _RPickupTime;
        public string RPickupTime
        {
            get { return _RPickupTime; }
            set { _RPickupTime = value; }
        }

        string _PickupNote;
        public string PickupNote
        {
            get { return _PickupNote; }
            set { _PickupNote = value; }
        }

        string _RPickupNote;
        public string RPickupNote
        {
            get { return _RPickupNote; }
            set { _RPickupNote = value; }
        }

        string _PnrNo;
        public string PnrNo
        {
            get { return _PnrNo; }
            set { _PnrNo = value; }
        }
        
        string _TrfDTName;
        public string TrfDTName
        {
            get { return _TrfDTName; }
            set { _TrfDTName = value; }
        }

        string _RTrfDTName;
        public string RTrfDTName
        {
            get { return _RTrfDTName; }
            set { _RTrfDTName = value; }
        }

        string _TrfATName;
        public string TrfATName
        {
            get { return _TrfATName; }
            set { _TrfATName = value; }
        }

        string _RTrfATName;
        public string RTrfATName
        {
            get { return _RTrfATName; }
            set { _RTrfATName = value; }
        }

        string _TrfDep;
        public string TrfDep
        {
            get { return _TrfDep; }
            set { _TrfDep = value; }
        }

        string _RTrfDep;
        public string RTrfDep
        {
            get { return _RTrfDep; }
            set { _RTrfDep = value; }
        }

        string _TrfDepName;
        public string TrfDepName
        {
            get { return _TrfDepName; }
            set { _TrfDepName = value; }
        }

        string _RTrfDepName;
        public string RTrfDepName
        {
            get { return _RTrfDepName; }
            set { _RTrfDepName = value; }
        }

        string _TrfDepNameL;
        public string TrfDepNameL
        {
            get { return _TrfDepNameL; }
            set { _TrfDepNameL = value; }
        }

        string _RTrfDepNameL;
        public string RTrfDepNameL
        {
            get { return _RTrfDepNameL; }
            set { _RTrfDepNameL = value; }
        }

        string _TrfArr;
        public string TrfArr
        {
            get { return _TrfArr; }
            set { _TrfArr = value; }
        }

        string _RTrfArr;
        public string RTrfArr
        {
            get { return _RTrfArr; }
            set { _RTrfArr = value; }
        }

        string _TrfArrName;
        public string TrfArrName
        {
            get { return _TrfArrName; }
            set { _TrfArrName = value; }
        }

        string _RTrfArrName;
        public string RTrfArrName
        {
            get { return _RTrfArrName; }
            set { _RTrfArrName = value; }
        }

        string _TrfArrNameL;
        public string TrfArrNameL
        {
            get { return _TrfArrNameL; }
            set { _TrfArrNameL = value; }
        }

        string _RTrfArrNameL;
        public string RTrfArrNameL
        {
            get { return _RTrfArrNameL; }
            set { _RTrfArrNameL = value; }
        }
    }
}
