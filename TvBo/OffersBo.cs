﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class ClientOfferService
    {
        public int? StepNo { get; set; }
        public string Name { get; set; }
        public string LocalName { get; set; }
        public string Description { get; set; }
        public string BegDate { get; set; }

        public ClientOfferService()
        { 
        }
    }
}
