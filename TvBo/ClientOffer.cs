﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using TvTools;

namespace TvBo
{
    public class ClientOffer
    {
        public bool saveClientOfferList(User UserData, List<SearchResult> offerList)
        {
            string offerFilePath = Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferFilePath"]);
            offerFilePath = AppDomain.CurrentDomain.BaseDirectory + offerFilePath + UserData.AgencyID + "_" + UserData.UserID + "_OfferList.jSon";
            if (System.IO.File.Exists(offerFilePath))
                System.IO.File.Delete(offerFilePath);
            System.IO.FileStream f = new System.IO.FileStream(offerFilePath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
            try
            {
                System.IO.StreamWriter writer = new System.IO.StreamWriter(f);
                string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(offerList.OrderByDescending(o => o.SearchTime)));
                writer.Write(compress);
                writer.Close();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
            finally
            {
                f.Close();
            }
        }

        public List<SearchResult> readClientOfferList(User UserData)
        {
            string offerFilePath = Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferFilePath"]);
            offerFilePath = AppDomain.CurrentDomain.BaseDirectory + offerFilePath + UserData.AgencyID + "_" + UserData.UserID + "_OfferList.jSon";
            List<SearchResult> save = new List<SearchResult>();
            List<SearchResult> list = new List<SearchResult>();

            int? durationDay = Conversion.getInt32OrNull(ConfigurationManager.AppSettings["ClientOfferDurationDay"]);

            if (System.IO.File.Exists(offerFilePath))
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(offerFilePath);

                try
                {
                    string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                    List<SearchResult> retVal = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchResult>>(uncompressed);
                    if (retVal.Count() != retVal.Where(w => w.SearchTime >= DateTime.Now.AddDays(durationDay.HasValue ? durationDay.Value * -1 : -3)).Count())
                    {
                        save = retVal.Where(w => w.SearchTime >= DateTime.Now.AddDays(durationDay.HasValue ? durationDay.Value * -1 : -3)).ToList<SearchResult>();
                    }
                    return retVal.Where(w => w.SearchTime >= DateTime.Now.AddDays(durationDay.HasValue ? durationDay.Value * -1 : -3)).ToList<SearchResult>();
                }
                catch (Exception)
                {
                    return new List<SearchResult>();
                }
                finally
                {
                    reader.Close();
                    if (save.Count > 0)
                        saveClientOfferList(UserData, save);
                }
            }
            else
            {
                return new List<SearchResult>();
            }
        }

        public void removeClientOfferList(User UserData)
        {
            string offerFilePath = Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferFilePath"]);
            offerFilePath = AppDomain.CurrentDomain.BaseDirectory + offerFilePath + UserData.AgencyID + "_" + UserData.UserID + "_OfferList.jSon";
            if (System.IO.File.Exists(offerFilePath))
                System.IO.File.Delete(offerFilePath);
        }
    }
}
