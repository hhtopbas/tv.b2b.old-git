﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace TvBo
{
    public class ReservationData
    {
        public DataSet CreateResTables(ref string errorMsg)
        {
            string tsql = @"Select Top 0 * From ResMain (NOLOCK)
                            Select Top 0 * From ResService (NOLOCK)
                            Select Top 0 * From ResServiceExt (NOLOCK)
                            Select Top 0 * From ResCust (NOLOCK)
                            Select Top 0 * From ResCustPrice (NOLOCK)
                            Select Top 0 * From ResCon (NOLOCK)
                            Select Top 0 * From ResConExt (NOLOCK)
                            Select Top 0 * From ResCustInfo (NOLOCK)
                            Select Top 0 * From ResSupDis (NOLOCK)
                            Select Top 0 * From ResPromo (NOLOCK)
                            Select Top 0 *, DepFlight='', RetFlight='' From PriceListExtV
                            Select Top 0 * From ResChgFee (NOLOCK)
                            ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                DataSet ds = db.ExecuteDataSet(dbCommand);
                ds.Tables[0].TableName = "ResMain";
                ds.Tables[1].TableName = "ResService";
                ds.Tables[2].TableName = "ResServiceExt";
                ds.Tables[3].TableName = "ResCust";
                ds.Tables[4].TableName = "ResCustPrice";
                ds.Tables[5].TableName = "ResCon";
                ds.Tables[6].TableName = "ResConExt";
                ds.Tables[7].TableName = "ResCustInfo";
                ds.Tables[8].TableName = "ResSupDis";
                ds.Tables[9].TableName = "ResPromo";
                ds.Tables[10].TableName = "SelectBook";
                ds.Tables[11].TableName = "ResChgFee";
                return ds;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public DataSet CreateResData(ref string errorMsg)
        {
            DataSet ResData = new DataSet();
            DataTable SelectRes = new DataTable();
            DataTable Title = new DataTable();
            DataTable ResMain = new DataTable();
            DataTable ResService = new DataTable();
            DataTable plService = new DataTable();
            DataTable ResServiceExt = new DataTable();
            DataTable ResCust = new DataTable();
            DataTable ResCustPrice = new DataTable();
            DataTable ResCustInfo = new DataTable();
            DataTable ResCon = new DataTable();
            DataTable ResConExt = new DataTable();
            DataTable ResFlightDetail = new DataTable();
            DataTable ResTotalPrice = new DataTable();
            DataTable ResSupDis = new DataTable();
            DataTable ResPromo = new DataTable();
            DataTable ResChgFee = new DataTable();
            DataSet tmpResData = CreateResTables(ref errorMsg);

            SelectRes = tmpResData.Tables[9].Clone();
            SelectRes.TableName = "SelectBook";            
            #region SelectRes Columns
            SelectRes.Columns.Add("BookID", typeof(System.Int32));
            SelectRes.Columns["BookID"].AutoIncrement = true;
            SelectRes.Columns["BookID"].AutoIncrementSeed = 1;
            SelectRes.Columns["BookID"].AutoIncrementStep = 1;
            DataColumn[] BookID = new DataColumn[1];
            BookID[0] = SelectRes.Columns["BookID"];
            SelectRes.PrimaryKey = BookID;            
            #endregion SelectRes Columns
            ResData.Tables.Add(SelectRes);

            Title.TableName = "Title";
            #region Title Columns
            Title.Columns.Add("BookID", typeof(int));
            Title.Columns.Add("TitleNo", typeof(int));
            Title.Columns.Add("Age1", typeof(Decimal));
            Title.Columns.Add("Age2", typeof(Decimal));
            Title.Columns.Add("Code", typeof(string));
            #endregion Title Columns
            ResData.Tables.Add(Title);

            ResMain = tmpResData.Tables[0].Clone();
            ResMain.TableName = "ResMain";
            #region ResMain Column

            #endregion ResMain Column
            ResMain.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            ResData.Tables.Add(ResMain);

            ResService = tmpResData.Tables[1].Clone();
            ResService.TableName = "ResService";
            #region ResService Columns
            DataColumn[] ResServiceKey = new DataColumn[1];
            ResServiceKey[0] = ResService.Columns["RecID"];
            ResService.PrimaryKey = ResServiceKey;
            ResService.Columns.Add("_RecID", typeof(int));
            ResService.Columns.Add("Description", typeof(string));
            ResService.Columns.Add("ResSeparate", typeof(bool));
            ResService.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            ResService.Columns.Add("Direction", typeof(string));
            #endregion ResService Columns
            ResData.Tables.Add(ResService);

            ResServiceExt = tmpResData.Tables[2].Clone();
            ResServiceExt.TableName = "ResServiceExt";
            #region ResServiceExt Columns
            ResServiceExt.Columns["RecID"].AutoIncrement = true;
            ResServiceExt.Columns["RecID"].AutoIncrementSeed = 1;
            ResServiceExt.Columns["RecID"].AutoIncrementStep = 1;
            ResServiceExt.Columns.Add("_RecID", typeof(int));
            ResServiceExt.Columns.Add("ServiceLocalName", typeof(string));
            ResServiceExt.Columns.Add("Description", typeof(string));
            ResServiceExt.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            #endregion ResServiceExt Columns
            ResData.Tables.Add(ResServiceExt);

            ResCust = tmpResData.Tables[3].Clone();
            ResCust.TableName = "ResCust";
            #region ResCust Columns
            ResCust.Columns.Add("BookID", typeof(System.Int32));
            ResCust.Columns["CustNo"].AutoIncrement = true;
            ResCust.Columns["CustNo"].AutoIncrementSeed = 1;
            ResCust.Columns["CustNo"].AutoIncrementStep = 1;
            ResCust.Columns.Add("_CustNo", typeof(int));
            ResCust.Columns.Add("Phone", typeof(string));
            ResCust.Columns.Add("ParentID", typeof(int));
            ResCust.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            #endregion ResCust Columns
            ResData.Tables.Add(ResCust);

            ResCustPrice = tmpResData.Tables[4].Clone();
            ResCustPrice.TableName = "ResCustPrice";
            #region ResCustPrice Columns
            ResCustPrice.Columns["RecID"].AutoIncrement = true;
            ResCustPrice.Columns["RecID"].AutoIncrementSeed = 1;
            ResCustPrice.Columns["RecID"].AutoIncrementStep = 1;
            ResCustPrice.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            #endregion ResCustPrice Columns
            ResData.Tables.Add(ResCustPrice);

            ResCon = tmpResData.Tables[5].Clone();
            ResCon.TableName = "ResCon";
            #region ResCon Columns
            ResCon.Columns["RecID"].AutoIncrement = true;
            ResCon.Columns["RecID"].AutoIncrementSeed = 1;
            ResCon.Columns["RecID"].AutoIncrementStep = 1;
            ResCon.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            #endregion ResCon Columns
            ResData.Tables.Add(ResCon);

            ResConExt = tmpResData.Tables[6].Clone();
            ResConExt.TableName = "ResConExt";
            #region ResConExt Columns
            ResConExt.Columns["RecID"].AutoIncrement = true;
            ResConExt.Columns["RecID"].AutoIncrementSeed = 1;
            ResConExt.Columns["RecID"].AutoIncrementStep = 1;
            ResConExt.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            #endregion ResConExt Columns
            ResData.Tables.Add(ResConExt);

            ResCustInfo = tmpResData.Tables[7].Clone();
            ResCustInfo.TableName = "ResCustInfo";
            #region ResCustInfo Columns
            ResCustInfo.Columns["RecID"].AutoIncrement = true;
            ResCustInfo.Columns["RecID"].AutoIncrementSeed = 1;
            ResCustInfo.Columns["RecID"].AutoIncrementStep = 1;
            ResCustInfo.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            #endregion ResCustInfo Columns
            ResData.Tables.Add(ResCustInfo);

            ResFlightDetail.TableName = "ResFlightDetail";
            #region ResFlightDetail Columns
            ResFlightDetail.Columns.Add("FlightNo", typeof(string));
            ResFlightDetail.Columns.Add("DepAirport", typeof(string));
            ResFlightDetail.Columns.Add("ArrAirport", typeof(string));
            ResFlightDetail.Columns.Add("FlyDate", typeof(DateTime));
            ResFlightDetail.Columns.Add("DepTime", typeof(DateTime));
            ResFlightDetail.Columns.Add("ArrTime", typeof(DateTime));
            #endregion ResFlightDetail Columns
            ResData.Tables.Add(ResFlightDetail);

            ResTotalPrice.TableName = "ResTotalPrice";
            #region ResTotalPrice Columns
            ResTotalPrice.Columns.Add("BookID", typeof(System.Int32));
            ResTotalPrice.Columns.Add("TotalPrice", typeof(decimal));
            ResTotalPrice.Columns.Add("SaleCur", typeof(string));
            #endregion ResTotalPrice Columns
            ResData.Tables.Add(ResTotalPrice);

            ResSupDis = tmpResData.Tables[8].Clone();
            ResSupDis.TableName = "ResSupDis";
            #region ResSupDis Columns
            ResSupDis.Columns["RecID"].AutoIncrement = true;
            ResSupDis.Columns["RecID"].AutoIncrementSeed = 1;
            ResSupDis.Columns["RecID"].AutoIncrementStep = 1;
            ResSupDis.Columns.Add("Description", typeof(string));
            ResSupDis.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            #endregion ResSupDis Columns
            ResData.Tables.Add(ResSupDis);

            ResPromo = tmpResData.Tables[9].Clone();
            ResPromo.TableName = "ResPromo";
            #region ResPromo Columns
            ResPromo.Columns["RecID"].AutoIncrement = true;
            ResPromo.Columns["RecID"].AutoIncrementSeed = 1;
            ResPromo.Columns["RecID"].AutoIncrementStep = 1;
            //ResPromo.Columns.Add("Description", typeof(string));
            ResPromo.Columns.Add("MemTable", typeof(bool)).DefaultValue = false;
            #endregion ResPromo Columns
            ResData.Tables.Add(ResPromo);

            plService.TableName = "plServices";
            #region plService Columns
            plService.Columns.Add("RecID", typeof(int));
            plService.Columns.Add("ServiceType", typeof(string));
            plService.Columns.Add("Service", typeof(string));
            plService.Columns.Add("ConfStat", typeof(Int16));
            plService.Columns.Add("AllotType", typeof(string));
            plService.Columns.Add("Supplier", typeof(string));
            plService.Columns.Add("FlyDate", typeof(DateTime));
            #endregion plService Columns
            ResData.Tables.Add(plService);

            return ResData;
        }
    }

    [Serializable]    
    public struct TChdAgeInfo
    {
        Int16 _Count;
        public Int16 Count
        {
            get { return _Count; }
            set { _Count = value; }
        }

        Decimal _Age;
        public Decimal Age
        {
            get { return _Age; }
            set { _Age = value; }
        }
    }

    public struct TBasket
    {
        Int32 _CatPackID;
        public Int32 CatPackID
        {
            get { return _CatPackID; }
            set { _CatPackID = value; }
        }

        Int32 _ARecNo;
        public Int32 ARecNo
        {
            get { return _ARecNo; }
            set { _ARecNo = value; }
        }

        Int32 _PRecNo;
        public Int32 PRecNo
        {
            get { return _PRecNo; }
            set { _PRecNo = value; }
        }

        Int16 _Adult;
        public Int16 Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16 _Child;
        public Int16 Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        Int16 _RoomCnt;
        public Int16 RoomCnt
        {
            get { return _RoomCnt; }
            set { _RoomCnt = value; }
        }

        TChdAgeInfo[] _ChdInfo;
        public TChdAgeInfo[] ChdInfo
        {
            get { return _ChdInfo; }
            set { _ChdInfo = value; }
        }

        String _AllotType;
        public String AllotType
        {
            get { return _AllotType; }
            set { _AllotType = value; }
        }

        String _StopSale;
        public String StopSale
        {
            get { return _StopSale; }
            set { _StopSale = value; }
        }

        String _OverRelease;
        public String OverRelease
        {
            get { return _OverRelease; }
            set { _OverRelease = value; }
        }

    }    

}
