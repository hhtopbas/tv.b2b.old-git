﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.Globalization;
using System.Threading;
using TvTools;
using System.Web;
using Newtonsoft.Json.Linq;

namespace TvBo
{
    public class CustomerInfos
    {
        public object setField(object fieldValue)
        {

            return null;
        }

        public ResCustInfoRecord ResCustDrToLinq(List<ResCustInfoRecord> dt, int? CustNo)
        {
            ResCustInfoRecord record = new ResCustInfoRecord();
            try
            {
                ResCustInfoRecord rdr = dt.Where(w => w.CustNo == CustNo.Value).FirstOrDefault();
                return record;
            }
            catch
            {
                return record;
            }
        }

        public ResCustInfoRecord jSonToResCustInfo(ResCustjSonResView resCustInfojSon, ResCustInfoRecord resCustInfoRec)
        {
            resCustInfoRec.CustNo = Conversion.getInt32OrNull(resCustInfojSon.CustNo).HasValue ? Conversion.getInt32OrNull(resCustInfojSon.CustNo).Value : -1;
            resCustInfoRec.CTitle = Conversion.getInt16OrNull(resCustInfojSon.CTitle);
            resCustInfoRec.CName = resCustInfojSon.CName;
            resCustInfoRec.CSurName = resCustInfojSon.CSurName;
            resCustInfoRec.MobTel = strFunc.Trim(resCustInfojSon.MobTel, ' ');
            resCustInfoRec.ContactAddr = resCustInfojSon.ContactAddr;
            resCustInfoRec.InvoiceAddr = resCustInfojSon.InvoiceAddr;
            resCustInfoRec.AddrHome = resCustInfojSon.AddrHome;
            resCustInfoRec.AddrHomeCity = resCustInfojSon.AddrHomeCity;
            resCustInfoRec.AddrHomeCountry = resCustInfojSon.AddrHomeCountry;
            resCustInfoRec.AddrHomeCountryCode = resCustInfojSon.AddrHomeCountryCode;
            resCustInfoRec.AddrHomeZip = resCustInfojSon.AddrHomeZip;
            resCustInfoRec.AddrHomeTel = strFunc.Trim(resCustInfojSon.AddrHomeTel, ' ');
            resCustInfoRec.AddrHomeFax = strFunc.Trim(resCustInfojSon.AddrHomeFax, ' ');
            resCustInfoRec.AddrHomeEmail = resCustInfojSon.AddrHomeEmail;
            resCustInfoRec.HomeTaxOffice = resCustInfojSon.HomeTaxOffice;
            resCustInfoRec.HomeTaxAccNo = resCustInfojSon.HomeTaxAccNo;
            resCustInfoRec.WorkFirmName = resCustInfojSon.WorkFirmName;
            resCustInfoRec.AddrWork = resCustInfojSon.AddrWork;
            resCustInfoRec.AddrWorkCity = resCustInfojSon.AddrWorkCity;
            resCustInfoRec.AddrWorkCountry = resCustInfojSon.AddrWorkCountry;
            resCustInfoRec.AddrWorkCountryCode = resCustInfojSon.AddrWorkCountryCode;
            resCustInfoRec.AddrWorkZip = resCustInfojSon.AddrWorkZip;
            resCustInfoRec.AddrWorkTel = strFunc.Trim(resCustInfojSon.AddrWorkTel, ' ');
            resCustInfoRec.AddrWorkFax = strFunc.Trim(resCustInfojSon.AddrWorkFax, ' ');
            resCustInfoRec.AddrWorkEMail = resCustInfojSon.AddrWorkEMail;
            resCustInfoRec.WorkTaxOffice = resCustInfojSon.WorkTaxOffice;
            resCustInfoRec.WorkTaxAccNo = resCustInfojSon.WorkTaxAccNo;
            resCustInfoRec.Bank = resCustInfojSon.Bank;
            resCustInfoRec.BankAccNo = resCustInfojSon.BankAccNo;
            resCustInfoRec.BankIBAN = resCustInfojSon.BankIBAN;

            return resCustInfoRec;
        }

        public DataRow LinqToResCustInfo(ResCustInfoRecord source, DataRow target)
        {
            PropertyInfo[] oProps = null;
            oProps = source.GetType().GetProperties();

            foreach (PropertyInfo pi in oProps)
            {
                Type colType = pi.PropertyType;
                if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                    colType = colType.GetGenericArguments()[0];

                if (target.Table.Columns.Contains(pi.Name))
                    target[pi.Name] = pi.GetValue(source, null) == null ? DBNull.Value : pi.GetValue(source, null);
            }
            return target;
        }

        public ResCustRecord jSonToResCust(User UserData, ResCustjSonResView jSonValues, ResCustRecord target, DateTime? ResBegDate, ref string errorMsg)
        {
            CultureInfo ci = new CultureInfo(jSonValues.LCID);
            if (ci.Name.ToLower() == "lt-lt")
                ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
            Thread.CurrentThread.CurrentCulture = ci;

            target.Surname = jSonValues.EdtSurname;
            target.SurnameL = jSonValues.EdtSurnameL;
            target.Name = jSonValues.EdtName;
            target.NameL = jSonValues.EdtNameL;
            string _dateMask = string.Empty;
            string _dateFormat = string.Empty;
            string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
            string dateSeperator = UserData.Ci.DateTimeFormat.DateSeparator[0].ToString();
            if (UserData.Ci.Name.ToLower() == "lt-lt")
                dateSeperator = ".";
            string[] dateMaskB = dateMaskA.Split(Convert.ToChar(dateSeperator));
            foreach (string row in dateMaskB)
                if (!string.IsNullOrEmpty(row.ToString()))
                    switch (row[0].ToString().ToLower())
                    {
                        case "d": _dateMask += "/99"; _dateFormat += "/dd"; break;
                        case "m": _dateMask += "/99"; _dateFormat += "/MM"; break;
                        case "y": _dateMask += "/9999"; _dateFormat += "/yyyy"; break;
                        default: break;
                    }
            _dateMask = _dateMask.Remove(0, 1);
            _dateFormat = _dateFormat.Remove(0, 1);

            if (!string.IsNullOrEmpty(jSonValues.EdtBirtday))
            {               
                DateTime? birthDay = Conversion.convertDateTime(jSonValues.EdtBirtday, _dateFormat);                
                if (birthDay.HasValue)
                {
                    Int16 age = Convert.ToInt16(Math.Floor((ResBegDate.Value - birthDay.Value).TotalDays / 365.25));

                    decimal maxChdAge =1199/100;
                    if (UserData.TvParams.TvParamReser.MaxChdAge.HasValue)
                        maxChdAge = UserData.TvParams.TvParamReser.MaxChdAge.Value;
                    if ((maxChdAge > age) && Equals(target.Leader, "Y"))
                    {
                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgLeaderCanNotBeChildren").ToString();
                        return null;
                    }
                    else
                    {
                        target.Birtday = birthDay.Value;
                        if (target.Birtday.HasValue && ResBegDate.HasValue)
                            target.Age = age;
                    }
                }
            }
            target.Title = Conversion.getInt16OrNull(jSonValues.EdtTitleCode);
            target.CustNo = Conversion.getInt32OrNull(jSonValues.CustNo).HasValue ? Conversion.getInt32OrNull(jSonValues.CustNo).Value : -1;
            target.IDNo = jSonValues.EdtIDNo;
            target.PassSerie = jSonValues.EdtPassSerie;
            target.PassNo = jSonValues.EdtPassNo;
            if (!string.IsNullOrEmpty(jSonValues.EdtPassIssueDate))
                target.PassIssueDate = Conversion.convertDateTime(jSonValues.EdtPassIssueDate, _dateFormat);                
            if (!string.IsNullOrEmpty(jSonValues.EdtPassExpDate))
                target.PassExpDate = Conversion.convertDateTime(jSonValues.EdtPassExpDate, _dateFormat);                
            target.PassGiven = jSonValues.EdtPassGiven;
            return target;
        }

        public ResCustVisaRecord jSonToResCustVisa(User UserData, object jSonValues, ResCustVisaRecord target, ref string errorMsg)
        {
            List<JToken> data = ((JObject)jSonValues).ToList<JToken>();
            List<JProperty> dataList = new List<JProperty>();
            JToken first = ((JObject)jSonValues).First;
            JProperty token = (JProperty)first;
            dataList.Add(token);
            for (int i = 1; i < ((JObject)jSonValues).Count; i++) { token = (JProperty)token.Next; dataList.Add(token); }
            DateTime? date = null;
            DateTime? nullDate = null;
            target.CustNo = Conversion.getInt32OrNull(dataList.Find(f => string.Equals(f.Name, "CustNo")).Value.ToString());
            target.BirthSurname = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "BirthSurname")).Value.ToString());
            target.PlaceOfBirth = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "PlaceOfBirth")).Value.ToString());
            target.CountryOfBirth = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "CountryOfBirth")).Value.ToString());
            target.RemarkForChild = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "RemarkForChild")).Value.ToString());
            target.GivenBy = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "GivenBy")).Value.ToString());
            target.Job = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "Job")).Value.ToString());
            target.Employer = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "Employer")).Value.ToString());
            target.InvitePerson = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "InvitePerson")).Value.ToString());
            target.Sex = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "Sex")).Value.ToString());
            target.Marital = Conversion.getInt16OrNull(dataList.Find(f => string.Equals(f.Name, "Marital")).Value.ToString());
            target.MaritalRemark = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "MaritalRemark")).Value.ToString());
            target.VisaCount = Conversion.getByteOrNull(dataList.Find(f => string.Equals(f.Name, "VisaCount")).Value.ToString());
            target.Address = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "Address")).Value.ToString());
            target.Tel = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "Tel")).Value.ToString());
            target.Fax = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "Fax")).Value.ToString());
            target.VisaNo = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "VisaNo")).Value.ToString());
            target.DepCity = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "DepCity")).Value.ToString());
            target.ArrCity = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "ArrCity")).Value.ToString());
            target.AgencyName = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "AgencyName")).Value.ToString());
            target.AgencyAddress = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "AgencyAddress")).Value.ToString());
            target.AgencyTel = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "AgencyTel")).Value.ToString());
            target.AgencyFax = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "AgencyFax")).Value.ToString());
            target.EmpAddress = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "EmpAddress")).Value.ToString());
            target.EmpTel = Conversion.getStrOrNull(dataList.Find(f => string.Equals(f.Name, "EmpTel")).Value.ToString());
            date = Conversion.getDateTimeOrNull(dataList.Find(f => string.Equals(f.Name, "ValidFrom")).Value.ToString());
            target.ValidFrom = date.HasValue ? date.Value : nullDate;
            date = Conversion.getDateTimeOrNull(dataList.Find(f => string.Equals(f.Name, "ValidTill")).Value.ToString());
            target.ValidTill = date.HasValue ? date.Value : nullDate;
            date = Conversion.getDateTimeOrNull(dataList.Find(f => string.Equals(f.Name, "ValidFrom2")).Value.ToString());
            target.ValidFrom2 = date.HasValue ? date.Value : nullDate;
            date = Conversion.getDateTimeOrNull(dataList.Find(f => string.Equals(f.Name, "ValidTill2")).Value.ToString());
            target.ValidTill2 = date.HasValue ? date.Value : nullDate;
            date = Conversion.getDateTimeOrNull(dataList.Find(f => string.Equals(f.Name, "ValidFrom3")).Value.ToString());
            target.ValidFrom3 = date.HasValue ? date.Value : nullDate;
            date = Conversion.getDateTimeOrNull(dataList.Find(f => string.Equals(f.Name, "ValidTill3")).Value.ToString());
            target.ValidTill3 = date.HasValue ? date.Value : nullDate;
            date = Conversion.getDateTimeOrNull(dataList.Find(f => string.Equals(f.Name, "ValidFrom4")).Value.ToString());
            target.ValidFrom4 = date.HasValue ? date.Value : nullDate;
            date = Conversion.getDateTimeOrNull(dataList.Find(f => string.Equals(f.Name, "ValidTill4")).Value.ToString());
            target.ValidTill4 = date.HasValue ? date.Value : nullDate;
            return target;
        }
    }
}
