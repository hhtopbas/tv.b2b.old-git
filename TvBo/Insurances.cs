﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using TvTools;

namespace TvBo
{
    public class Insurances
    {
        public InsuranceRecord getInsurance(string Market, string Code, ref string errorMsg)
        {
            InsuranceRecord record = new InsuranceRecord();
            string tsql = string.Empty;
            tsql = @"  Select Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market), [Name]),
	                        NameS, InsZone, [Description], CoverExplain, 
	                        ConfStat, Coverage, CoverageCur, MaxPaxPolicy, TaxPer,
	                        PayCom, PayComEB, PayPasEB, InsType, CanDiscount, Franchise
                       From Insurance (NOLOCK)
                       Where Code = @Code ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.InsZone = Conversion.getStrOrNull(R["InsZone"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.CoverExplain = Conversion.getStrOrNull(R["CoverExplain"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.Coverage = Conversion.getDecimalOrNull(R["Coverage"]);
                        record.CoverageCur = Conversion.getStrOrNull(R["CoverageCur"]);
                        record.MaxPaxPolicy = Conversion.getInt16OrNull(R["MaxPaxPolicy"]);
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = Equals(R["PayPasEB"], "Y");
                        record.InsType = (Int16)R["InsType"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        record.Franchise = Conversion.getDecimalOrNull(R["Franchise"]);
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<InsurancePriceRecord> getInsurancePrice(string Market, string plMarket, int? Country, ref string errorMsg)
        {
            List<InsurancePriceRecord> list = new List<InsurancePriceRecord>();
            string tsql = @"Select IP.RecID, IP.Insurance, IP.Market, 
	                            IP.Country, CountryName=L.Name, CountryNameL=ISNULL(dbo.FindLocalName(L.NameLID, @Market), Name),
	                            IP.Supplier, IP.BegDate, IP.EndDate, IP.FromDay, IP.ToDay, IP.NetPrice, IP.NetCur, IP.SaleType, IP.SalePrice, IP.SaleCur, 
	                            IP.AutoCalc, IP.ComVal, IP.ComPer, IP.NetChdG1Price, IP.NetChdG2Price, IP.NetChdG3Price, IP.SaleChdG1Price, IP.SaleChdG2Price, IP.SaleChdG3Price, 
	                            IP.ChdG1MaxAge, IP.ChdG2MaxAge, IP.ChdG3MaxAge, IP.FromPax, IP.ToPax, IP.PriceType, IP.CalcType, IP.Compulsory, IP.NetPer
                            From InsurancePrice IP (NOLOCK)
                            Left Join Location L (NOLOCK) ON L.RecID=IP.Country 
                            Where IP.Market=@plMarket
                            ";
            if (Country != null)
                tsql += " And IP.Country=@Country ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                if (Country != null)
                    db.AddInParameter(dbCommand, "Country", DbType.Int32, Country.Value);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        InsurancePriceRecord record = new InsurancePriceRecord();
                        record.RecID = (int)R["RecID"];
                        record.Insurance = Conversion.getStrOrNull(R["Insurance"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Country = Conversion.getInt32OrNull(R["Country"]);
                        record.CountryName = Conversion.getStrOrNull(R["CountryName"]);
                        record.CountryNameL = Conversion.getStrOrNull(R["CountryNameL"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.FromDay = Conversion.getInt16OrNull(R["FromDay"]);
                        record.ToDay = Conversion.getInt16OrNull(R["ToDay"]);
                        record.NetPrice = Conversion.getDecimalOrNull(R["NetPrice"]);
                        record.NetCur = Conversion.getStrOrNull(R["NetCur"]);
                        record.SaleType = Conversion.getStrOrNull(R["SaleType"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.AutoCalc = Conversion.getStrOrNull(R["AutoCalc"]);
                        record.ComVal = Conversion.getDecimalOrNull(R["ComVal"]);
                        record.ComPer = Conversion.getDecimalOrNull(R["ComPer"]);
                        record.NetChdG1Price = Conversion.getDecimalOrNull(R["NetChdG1Price"]);
                        record.NetChdG2Price = Conversion.getDecimalOrNull(R["NetChdG2Price"]);
                        record.NetChdG3Price = Conversion.getDecimalOrNull(R["NetChdG3Price"]);
                        record.SaleChdG1Price = Conversion.getDecimalOrNull(R["SaleChdG1Price"]);
                        record.SaleChdG2Price = Conversion.getDecimalOrNull(R["SaleChdG2Price"]);
                        record.SaleChdG3Price = Conversion.getDecimalOrNull(R["SaleChdG3Price"]);
                        record.ChdG1MaxAge = Conversion.getDecimalOrNull(R["ChdG1MaxAge"]);
                        record.ChdG2MaxAge = Conversion.getDecimalOrNull(R["ChdG2MaxAge"]);
                        record.ChdG3MaxAge = Conversion.getDecimalOrNull(R["ChdG3MaxAge"]);
                        record.FromPax = Conversion.getInt16OrNull(R["FromPax"]);
                        record.ToPax = Conversion.getInt16OrNull(R["ToPax"]);
                        record.PriceType = Conversion.getInt16OrNull(R["PriceType"]);
                        record.CalcType = Conversion.getInt16OrNull(R["CalcType"]);
                        record.Compulsory = Conversion.getBoolOrNull(R["Compulsory"]);
                        record.NetPer = Conversion.getDecimalOrNull(R["NetPer"]);
                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<InsuranceRecord> getInsuranceForCountry(User UserData, string plMarket, int? Country, Int16? RemainDay, decimal? PasAmount, ref string errorMsg)
        {
            List<InsuranceRecord> list = new List<InsuranceRecord>();
            string tsql =
@"
Select I.Code, I.[Name], LocalName=isnull(dbo.FindLocalName(I.NameLID, @Market), I.[Name]),
    I.NameS, I.InsZone, I.[Description], I.CoverExplain, 
    I.ConfStat, I.Coverage, I.CoverageCur, I.MaxPaxPolicy, I.TaxPer,
    I.PayCom, I.PayComEB, I.PayPasEB, I.InsType, I.CanDiscount, I.Franchise, I.B2BPub, I.B2CPub
From (
	    Select Distinct Insurance
	    From InsurancePrice IP (NOLOCK)
	    Where IP.Market=@plMarket
        And GetDate() between BegDate and EndDate
";
            if (string.Equals(UserData.CustomRegID, Common.crID_FilipTravel))
                tsql +=
@"
          And (@Country is null or Country=@Country)
";
            else
                tsql +=
@"
          And Country=@Country
";                                              
            if (RemainDay.HasValue)
                tsql += string.Format(" And {0} between IsNull(IP.RemDayFrom,0) And IsNull(IP.RemDayTo,999) ", RemainDay.Value);

            tsql += @") X
                      Join Insurance I (NOLOCK) ON I.Code=X.Insurance
                        where I.B2BPub=1

                    "; 
            //And I.B2BPub=1";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Country", DbType.String, Country);
                
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        InsuranceRecord record = new InsuranceRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.InsZone = Conversion.getStrOrNull(R["InsZone"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.CoverExplain = Conversion.getStrOrNull(R["CoverExplain"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.Coverage = Conversion.getDecimalOrNull(R["Coverage"]);
                        record.CoverageCur = Conversion.getStrOrNull(R["CoverageCur"]);
                        record.MaxPaxPolicy = Conversion.getInt16OrNull(R["MaxPaxPolicy"]);
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = Equals(R["PayPasEB"], "Y");
                        record.InsType = (Int16)R["InsType"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        record.Franchise = Conversion.getDecimalOrNull(R["Franchise"]);
                        record.B2BPub = Conversion.getBoolOrNull(R["B2BPub"]);
                        record.B2CPub = Conversion.getBoolOrNull(R["B2CPub"]);
                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int? getNewPolNo(string Insurance, ref string errorMsg)
        {
            string tsql = @"Declare @NewPolNo int
                            Exec dbo.usp_Get_NewPolNo @Insurance, @NewPolNo OUTPUT
                            Select NewPolNo=@NewPolNo";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Insurance", DbType.String, Insurance);
                return Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Dispose();
            }
        }

        public bool setInsurancePolicy(User UserData, string Insurance, int? PolNo, int? CustNo, string Supplier, string ResNo,
                                        DateTime? BegDate, DateTime? EndDate, Int16? Days, decimal? Coverage,
                                        string CoverageCur, int? Territory, string InsZone, int? PrintLocation,
                                        string CrtUser, string PrintStat, ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string tsql = @"
                            if not exists(Select * from InsurePolicy (NOLOCK) Where PolNo=@PolNo and CustNo=@CustNo and Insurance=@Insurance) 
	                            Insert Into InsurePolicy (Status, PolNo, CustNo, Insurance, Supplier, ResNo, BegDate, EndDate, Days, Coverage, CoverageCur, Territory, InsZone, PrintDate, PrintLocation, CrtDate, CrtUser, PrintStat) 
	                            Values('Y', @PolNo, @CustNo, @Insurance, @Supplier, @ResNo, @BegDate, @EndDate, @Days, @Coverage, @CoverageCur, @Territory, @InsZone, @PrintDate, @PrintLocation, @CrtDate, @CrtUser, @PrintStat) 
                            else Update InsurePolicy Set PrintDate=@PrintDate Where Insurance = @Insurance and PolNo=@PolNo and CustNo=@CustNo 
                           ";
             
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "Insurance", DbType.String, Insurance);
                db.AddInParameter(dbCommand, "PolNo", DbType.Int32, PolNo);
                db.AddInParameter(dbCommand, "CustNo", DbType.Int32, CustNo);
                db.AddInParameter(dbCommand, "Supplier", DbType.String, Supplier);
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate);
                db.AddInParameter(dbCommand, "Days", DbType.Int16, Days);
                db.AddInParameter(dbCommand, "Coverage", DbType.Decimal, Coverage);
                db.AddInParameter(dbCommand, "CoverageCur", DbType.String, CoverageCur);
                db.AddInParameter(dbCommand, "Territory", DbType.Int32, Territory);
                db.AddInParameter(dbCommand, "InsZone", DbType.String, InsZone);
                db.AddInParameter(dbCommand, "PrintDate", DbType.DateTime, DateTime.Today);
                db.AddInParameter(dbCommand, "PrintLocation", DbType.Int32, PrintLocation);
                db.AddInParameter(dbCommand, "CrtDate", DbType.DateTime, DateTime.Now);
                db.AddInParameter(dbCommand, "CrtUser", DbType.String, CrtUser);
                db.AddInParameter(dbCommand, "PrintStat", DbType.String, "Y");

                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Dispose();
            }
        }
    }
}
