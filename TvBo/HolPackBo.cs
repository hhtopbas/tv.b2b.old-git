﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class HolPackSerSale
    {
        public int? RecID { get; set; }
        public string HolPack { get; set; }
        public string ServiceType { get; set; }
        public string Service { get; set; }
        public bool? CanSale { get; set; }

        public HolPackSerSale()
        {
        }
    }

    public class HolPackFixDate
    {
        public DateTime? CheckIn { get; set; }
        public string HolPackName { get; set; }
        public string HolPackNameL { get; set; }

        public HolPackFixDate()
        {
        }
    }

    public class RRFHolPack
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public int? DepCity { get; set; }
        public string DepCityName { get; set; }
        public string DepCityNameL { get; set; }
        public int? ArrCity { get; set; }
        public string ArrCityName { get; set; }
        public string ArrCityNameL { get; set; }
        public DateTime? BegDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public string SaleCur { get; set; }
        public int? PNight { get; set; }
        public int? TANight { get; set; }
        public bool? Template { get; set; }
        public bool? Hajj { get; set; }
        public bool? Umrah { get; set; }
        public RRFHolPack()
        {
        }
    }

    public class HolPackSer
    {
        public int? RecID { get; set; }
        public string HolPack { get; set; }
        public int? StepNo { get; set; }
        public string Service { get; set; }
        public string ServiceItem { get; set; }
        public string IncPack { get; set; }
        public string ValAccomNights { get; set; }
        public Int16? DispNo { get; set; }
        public bool? Pilot { get; set; }
        public string Board { get; set; }
        public string SClass { get; set; }
        public string Room { get; set; }
        public HolPackSer()
        {
        }
    }

    public class HolPackPlan
    {
        public int? RecID { get; set; }
        public string HolPack { get; set; }
        public int? StepNo { get; set; }
        public string Service { get; set; }
        public string Direction { get; set; }
        public Int16? StartDay { get; set; }
        public Int16? Duration { get; set; }
        public int? Departure { get; set; }
        public int? Arrival { get; set; }
        public string IncPack { get; set; }
        public string Priced { get; set; }
        public string AccomNights { get; set; }
        public string Explanation { get; set; }
        public Int16? DispOrd { get; set; }
        public string PayCom { get; set; }
        public string PayComEB { get; set; }
        public bool? PayPasEB { get; set; }
        public byte? StepType { get; set; }
        public bool? CanDiscount { get; set; }
        public byte? NPS2SpoOrder { get; set; }
        public decimal? NPS2SpoPerc { get; set; }
        public bool? CanEdit { get; set; }
        public bool? CanDelete { get; set; }
        public string DefSupplier { get; set; }
        public HolPackPlan()
        {
        }
    }
}
