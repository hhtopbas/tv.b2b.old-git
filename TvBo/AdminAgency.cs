﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class AdminAgency
    {
        public AdmAgencyUserRecord getUserRecord(string agencyID, string userID, ref string errorMsg)
        {
            AdmAgencyUserRecord rec = new AdmAgencyUserRecord();
            string tsql = @"
                            Select AU.RecID, AU.Agency, AgencyName=A.Name,
	                            AgencyNameLocal=isnull(dbo.FindLocalName(A.NameLID, O.Market),A.Name),
                                AU.Code, AU.Name, NameLocal=isnull(dbo.FindLocalName(AU.NameLID, O.Market),AU.Name),
                                AU.Pass, AU.ExpDate, AU.Status, 
                                AU.PIN, AU.Phone, AU.Mobile, AU.EMail, AU.HAddress, AU.ShowAllRes, 
                                AU.ExpedientID
                            From AgencyUser AU (NOLOCK)
                            Join Agency A (NOLOCK) ON A.Code=AU.Agency
                            Join Office O (NOLOCK) ON O.Code=A.OprOffice
                            Where AU.Agency=@Agency And AU.Code=@Code 
                            ";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommmand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommmand, "Agency", DbType.String, agencyID);
                db.AddInParameter(dbCommmand, "Code", DbType.String, userID);
                using (IDataReader R = db.ExecuteReader(dbCommmand))
                {
                    if (R.Read())
                    {
                        rec.RecID = (int)R["RecID"];
                        rec.Agency = Conversion.getStrOrNull(R["Agency"]);
                        rec.AgencyName = Conversion.getStrOrNull(R["AgencyName"]);
                        rec.AgencyNameLocal = Conversion.getStrOrNull(R["AgencyNameLocal"]);
                        rec.Code = Conversion.getStrOrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameLocal = Conversion.getStrOrNull(R["NameLocal"]);
                        rec.Pass = Conversion.getStrOrNull(R["Pass"]);
                        rec.ExpDate = Conversion.getDateTimeOrNull(R["ExpDate"]);
                        rec.Status = Equals(R["Status"], "Y");
                        rec.PIN = Conversion.getStrOrNull(R["PIN"]);
                        rec.Phone = Conversion.getStrOrNull(R["Phone"]);
                        rec.Mobile = Conversion.getStrOrNull(R["Mobile"]);
                        rec.EMail = Conversion.getStrOrNull(R["EMail"]);
                        rec.HAddress = Conversion.getStrOrNull(R["HAddress"]);
                        rec.ShowAllRes = Equals(R["ShowAllRes"], "Y");
                        rec.ExpedientID = Conversion.getStrOrNull(R["ExpedientID"]);
                    }
                    return rec;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return rec;
            }
            finally
            {
                dbCommmand.Connection.Close();
                dbCommmand.Connection.Dispose();
                dbCommmand.Dispose();
            }
        }

        public List<AdmAgencyUserRecord> getUserRecordList(string agencyID, ref string errorMsg)
        {
            List<AdmAgencyUserRecord> records = new List<AdmAgencyUserRecord>();
            string tsql = @"
                            Select AU.RecID, AU.Agency, AgencyName=A.Name,
	                            AgencyNameLocal=isnull(dbo.FindLocalName(A.NameLID, O.Market),A.Name),
                                AU.Code, AU.Name, NameLocal=isnull(dbo.FindLocalName(AU.NameLID, O.Market),AU.Name),
                                AU.Pass, AU.ExpDate, AU.Status, 
                                AU.PIN, AU.Phone, AU.Mobile, AU.EMail, AU.HAddress, AU.ShowAllRes, 
                                AU.ExpedientID
                            From AgencyUser AU (NOLOCK)
                            Join Agency A (NOLOCK) ON A.Code=AU.Agency
                            Join Office O (NOLOCK) ON O.Code=A.OprOffice
                            Where AU.Agency=@Agency 
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, agencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AdmAgencyUserRecord rec = new AdmAgencyUserRecord();
                        rec.Agency = Conversion.getStrOrNull(R["Agency"]);
                        rec.AgencyName = Conversion.getStrOrNull(R["AgencyName"]);
                        rec.AgencyNameLocal = Conversion.getStrOrNull(R["AgencyNameLocal"]);
                        rec.Code = Conversion.getStrOrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.NameLocal = Conversion.getStrOrNull(R["NameLocal"]);
                        rec.Pass = Conversion.getStrOrNull(R["Pass"]);
                        rec.ExpDate = Conversion.getDateTimeOrNull(R["ExpDate"]);
                        rec.Status = Equals(R["Status"], "Y");
                        rec.PIN = Conversion.getStrOrNull(R["PIN"]);
                        rec.Phone = Conversion.getStrOrNull(R["Phone"]);
                        rec.Mobile = Conversion.getStrOrNull(R["Mobile"]);
                        rec.EMail = Conversion.getStrOrNull(R["EMail"]);
                        rec.HAddress = Conversion.getStrOrNull(R["HAddress"]);
                        rec.ShowAllRes = Equals(R["ShowAllRes"], "Y");
                        rec.ExpedientID = Conversion.getStrOrNull(R["ExpedientID"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AdmAgencyRecord> getAgencys(string agency, ref string errorMsg)
        {
            List<AdmAgencyRecord> records = new List<AdmAgencyRecord>();

            string tsql = @"
                            Select A.RecID, A.Code, A.Name, NameLocal=isnull(dbo.FindLocalName(A.NameLID, O.Market), A.Name), A.NameS, A.FirmName, 
                                A.OprOffice, OprOfficeName=O.Name, OprOfficeNameLocal=isnull(dbo.FindLocalName(O.NameLID, O.Market), O.Name), 
                                A.MainOffice, A.Status, 
                                A.Location, LocationName=L.Name, LocationNameLocal=isnull(dbo.FindLocalName(L.NameLID, O.Market), L.Name),
                                A.AgencyType, A.Parent, A.MasterPass, A.WorkType, A.WebDisp
                            From Agency A (NOLOCK)
                            JOIN Office O (NOLOCK) ON O.Code=A.OprOffice
                            LEFT JOIN Location L (NOLOCK) ON L.RecID=A.Location
                            Where (A.MainOffice=@AgencyID And A.AgencyType<>2) OR A.Code=@AgencyID
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "AgencyID", System.Data.DbType.String, agency);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(new AdmAgencyRecord
                        {
                            RecID = (int)R["RecID"],
                            Code = Conversion.getStrOrNull(R["Code"]),
                            AgencyName = Conversion.getStrOrNull(R["Name"]),
                            AgencyNameLocal = Conversion.getStrOrNull(R["NameLocal"]),
                            AgencyNameS = Conversion.getStrOrNull(R["NameS"]),
                            AgencyFirmName = Conversion.getStrOrNull(R["FirmName"]),
                            OprOffice = Conversion.getStrOrNull(R["OprOffice"]),
                            OprOfficeName = Conversion.getStrOrNull(R["OprOfficeName"]),
                            OprOfficeNameLocal = Conversion.getStrOrNull(R["OprOfficeNameLocal"]),
                            MainOffice = Conversion.getStrOrNull(R["MainOffice"]),
                            Status = Equals(R["Status"], "Y"),
                            Location = Conversion.getInt32OrNull(R["Location"]),
                            LocationName = Conversion.getStrOrNull(R["LocationName"]),
                            LocationNameLocal = Conversion.getStrOrNull(R["LocationNameLocal"]),
                            AgencyType = (Int16)R["AgencyType"],
                            Parent = Conversion.getInt32OrNull(R["Parent"]),
                            MasterPass = Conversion.getStrOrNull(R["MasterPass"]),
                            WorkType = (byte)R["WorkType"],
                            WebDisp = (bool)R["WebDisp"]
                        });
                    }
                }
                return records;
            } catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
