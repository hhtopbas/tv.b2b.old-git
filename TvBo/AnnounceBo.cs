﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    [Serializable]
    public class AnnounceRecord
    {
        public AnnounceRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        bool? _ValidTV;
        public bool? ValidTV
        {
            get { return _ValidTV; }
            set { _ValidTV = value; }
        }

        bool? _ValidWeb;
        public bool? ValidWeb
        {
            get { return _ValidWeb; }
            set { _ValidWeb = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        string _CrtUser;
        public string CrtUser
        {
            get { return _CrtUser; }
            set { _CrtUser = value; }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set { _CrtDate = value; }
        }

    }

    [Serializable]
    public class AnnounceReadRecord
    {
        public AnnounceReadRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int? _AnnounceID;
        public int? AnnounceID
        {
            get { return _AnnounceID; }
            set { _AnnounceID = value; }
        }

        string _UserCode;
        public string UserCode
        {
            get { return _UserCode; }
            set { _UserCode = value; }
        }

        string _UserType;
        public string UserType
        {
            get { return _UserType; }
            set { _UserType = value; }
        }

        DateTime _ReadDate;
        public DateTime ReadDate
        {
            get { return _ReadDate; }
            set { _ReadDate = value; }
        }

    }

    [Serializable]
    public class AnnounceForUser
    {
        public AnnounceForUser()
        { 
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        bool? _Read;
        public bool? Read
        {
            get { return _Read; }
            set { _Read = value; }
        }
    }
}
