﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;

namespace TvBo
{
    public class WEBLog
    {
        public bool checkLogTables(string tableName)
        {
            string errorMsg = string.Empty;
            List<dbInformationShema> logTable = VersionControl.getTableShema(tableName, ref errorMsg);
            if (logTable != null && logTable.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool createLogTable(string tsql, ref string errorMsg)
        {
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool checkAndCreateLogTables(ref string errorMsg)
        {
            string tsql_WEBSearchLog =
            #region tsql_WEBSearchLog
 @"
/****** Object:  Table [dbo].[WEBSearchLog]    Script Date: 06/28/2013 13:33:08 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WEBSearchLog]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[WEBSearchLog](
	    [RECID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	    [SESSIONID] [varchar](50) NULL,
	    [USERID] [varchar](10) NULL,
	    [AGENCYID] [varchar](10) NULL,
	    [CREATEDATE] [datetime] NULL,
	    [SALERESOURCE] [smallint] NULL,
	    [CHECKIN] [smalldatetime] NULL,
	    [EXPENDDATE] [smallint] NULL,
	    [NIGHTFROM] [smallint] NULL,
	    [NIGHTTO] [smallint] NULL,
	    [DEPCITY] [int] NULL,
	    [ARRCITY] [int] NULL,
	    [PRICEFROM] [decimal](18, 2) NULL,
	    [PRICETO] [decimal](18, 2) NULL,
	    [PACKAGE] [varchar](10) NULL,
	    [ONLYTICKETOW] [bit] NULL,
        [PACKTYPE] [varchar](1) NULL,
     CONSTRAINT [PK_WEBSearchLog] PRIMARY KEY CLUSTERED 
    (
	    [RECID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
SET ANSI_PADDING OFF
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[WEBSearchLog]') AND name = N'I1_WEBSearchLog')
CREATE NONCLUSTERED INDEX [I1_WEBSearchLog] ON [dbo].[WEBSearchLog] 
(
	[AGENCYID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[WEBSearchLog]') AND name = N'I2_WEBSearchLog')
CREATE NONCLUSTERED INDEX [I2_WEBSearchLog] ON [dbo].[WEBSearchLog] 
(
	[USERID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[WEBSearchLog]') AND name = N'I3_WEBSearchLog')
CREATE NONCLUSTERED INDEX [I3_WEBSearchLog] ON [dbo].[WEBSearchLog] 
(
	[SALERESOURCE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[WEBSearchLog]') AND name = N'I5_WEBSearchLog')
CREATE NONCLUSTERED INDEX [I5_WEBSearchLog] ON [dbo].[WEBSearchLog] 
(
	[CHECKIN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[WEBSearchLog]') AND name = N'I6_WEBSearchLog')
CREATE NONCLUSTERED INDEX [I6_WEBSearchLog] ON [dbo].[WEBSearchLog] 
(
	[NIGHTFROM] ASC,
	[NIGHTTO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[WEBSearchLog]') AND name = N'I7_WEBSearchLog')
CREATE NONCLUSTERED INDEX [I7_WEBSearchLog] ON [dbo].[WEBSearchLog] 
(
	[DEPCITY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[WEBSearchLog]') AND name = N'I8_WEBSearchLog')
CREATE NONCLUSTERED INDEX [I8_WEBSearchLog] ON [dbo].[WEBSearchLog] 
(
	[ARRCITY] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_WEBSearchLog_RECID]') AND type = 'D')
BEGIN
  ALTER TABLE [dbo].[WEBSearchLog] ADD  CONSTRAINT [DF_WEBSearchLog_RECID]  DEFAULT (newid()) FOR [RECID]
END
";
            #endregion

            string tsql_WEBRoomInfoLog =
            #region tsql_WEBRoomInfoLog
 @"
/****** Object:  Table [dbo].[WEBRoomInfoLog]    Script Date: 06/28/2013 16:06:05 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WEBRoomInfoLog]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[WEBRoomInfoLog](
	    [RECID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	    [PARENTID] [uniqueidentifier] NULL,
	    [ADULT] [smallint] NULL,
	    [CHILD] [smallint] NULL,
     CONSTRAINT [PK_WEBRoomInfoLog] PRIMARY KEY CLUSTERED 
    (
	    [RECID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_WEBRoomInfoLog_RECID]') AND type = 'D')
BEGIN
  ALTER TABLE [dbo].[WEBRoomInfoLog] ADD  CONSTRAINT [DF_WEBRoomInfoLog_RECID]  DEFAULT (newid()) FOR [RECID]
END
";
            #endregion

            string tsql_WEBResortLog =
            #region tsql_WEBResortLog
 @"
/****** Object:  Table [dbo].[WEBResortLog]    Script Date: 06/28/2013 16:07:05 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WEBResortLog]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[WEBResortLog](
	    [RECID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	    [PARENTID] [uniqueidentifier] NULL,
	    [RESORT] [int] NULL,
     CONSTRAINT [PK_WEBResortLog] PRIMARY KEY CLUSTERED 
    (
	    [RECID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_WEBResortLog_PARENTID]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[WEBResortLog] ADD  CONSTRAINT [DF_WEBResortLog_PARENTID]  DEFAULT (newid()) FOR [PARENTID]
END
";
            #endregion

            string tsql_WEBHotelLog =
            #region tsql_WEBHotelLog
 @"
/****** Object:  Table [dbo].[WEBHotelLog]    Script Date: 06/28/2013 16:09:45 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WEBHotelLog]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[WEBHotelLog](
	    [RECID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	    [PARENTID] [uniqueidentifier] NULL,
	    [HOTEL] [varchar](10) NULL,
     CONSTRAINT [PK_WEBHotelLog] PRIMARY KEY CLUSTERED 
    (
	    [RECID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
SET ANSI_PADDING OFF
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_WEBHotelLog_RECID]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[WEBHotelLog] ADD  CONSTRAINT [DF_WEBHotelLog_RECID]  DEFAULT (newid()) FOR [RECID]
END
";
            #endregion

            string tsql_WEBRoomLog =
            #region tsql_WEBRoomLog
 @"
/****** Object:  Table [dbo].[WEBRoomLog]    Script Date: 06/28/2013 16:10:50 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WEBRoomLog]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[WEBRoomLog](
	    [RECID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	    [PARENTID] [uniqueidentifier] NULL,
	    [ROOM] [varchar](10) NULL,
     CONSTRAINT [PK_WEBRoomLog] PRIMARY KEY CLUSTERED 
    (
	    [RECID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
SET ANSI_PADDING OFF
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_WEBRoomLog_RECID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[WEBRoomLog] ADD  CONSTRAINT [DF_WEBRoomLog_RECID]  DEFAULT (newid()) FOR [RECID]
END
";
            #endregion

            string tsql_WEBBoardLog =
            #region tsql_WEBBoardLog
 @"
/****** Object:  Table [dbo].[WEBBoardLog]    Script Date: 06/28/2013 16:11:35 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WEBBoardLog]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[WEBBoardLog](
	    [RECID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	    [PARENTID] [uniqueidentifier] NULL,
	    [BOARD] [varchar](10) NULL,
     CONSTRAINT [PK_WEBBoardLog] PRIMARY KEY CLUSTERED 
    (
	    [RECID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
SET ANSI_PADDING OFF
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_WEBBoardLog_RECID]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[WEBBoardLog] ADD  CONSTRAINT [DF_WEBBoardLog_RECID]  DEFAULT (newid()) FOR [RECID]
END
";
            #endregion

            string tsql_WEBCategoryLog =
            #region tsql_WEBCategoryLog
 @"
/****** Object:  Table [dbo].[WEBCategoryLog]    Script Date: 06/28/2013 16:12:12 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WEBCategoryLog]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[WEBCategoryLog](
	    [RECID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	    [PARENTID] [uniqueidentifier] NULL,
	    [CATEGORY] [varchar](10) NULL,
     CONSTRAINT [PK_WEBCategoryLog] PRIMARY KEY CLUSTERED 
    (
	    [RECID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
SET ANSI_PADDING OFF
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_WEBCategoryLog_RECID]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[WEBCategoryLog] ADD  CONSTRAINT [DF_WEBCategoryLog_RECID]  DEFAULT (newid()) FOR [RECID]
END
";
            #endregion

            string tsql_WEBBookLog =
            #region tsql_WEBBookLog
 @"
/****** Object:  Table [dbo].[WEBBookLog]    Script Date: 07/09/2013 14:24:34 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WEBBookLog]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[WEBBookLog](
	    [RECID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	    [PARENTID] [uniqueidentifier] NULL,
	    [BEGINBOOK] [datetime] NULL,
	    [ENDBOOK] [datetime] NULL,
	    [RESNO] [varchar](10) NULL
    ) ON [PRIMARY]
END
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_WEBBookLog_RECID]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[WEBBookLog] ADD  CONSTRAINT [DF_WEBBookLog_RECID]  DEFAULT (newid()) FOR [RECID]
END
";
            #endregion

            bool ok = true;
            if (!checkLogTables("WEBSearchLog"))
                if (!createLogTable(tsql_WEBSearchLog, ref errorMsg))
                    ok = false;
            if (!checkLogTables("WEBRoomInfoLog"))
                if (!createLogTable(tsql_WEBRoomInfoLog, ref errorMsg))
                    ok = false;
            if (!checkLogTables("WEBResortLog"))
                if (!createLogTable(tsql_WEBResortLog, ref errorMsg))
                    ok = false;
            if (!checkLogTables("WEBHotelLog"))
                if (!createLogTable(tsql_WEBHotelLog, ref errorMsg))
                    ok = false;
            if (!checkLogTables("WEBRoomLog"))
                if (!createLogTable(tsql_WEBRoomLog, ref errorMsg))
                    ok = false;
            if (!checkLogTables("WEBBoardLog"))
                if (!createLogTable(tsql_WEBBoardLog, ref errorMsg))
                    ok = false;
            if (!checkLogTables("WEBCategoryLog"))
                if (!createLogTable(tsql_WEBCategoryLog, ref errorMsg))
                    ok = false;

            if (!checkLogTables("WEBBookLog"))
                if (!createLogTable(tsql_WEBBookLog, ref errorMsg))
                    ok = false;

            return ok;
        }

        public Guid? saveWEBSearchLog(WEBSearchLog data, ref string errorMsg)
        {
            string tsql =
@"
DECLARE @RECID uniqueidentifier;
SET @RECID = NewID();
INSERT INTO WEBSearchLog (RECID,SESSIONID,USERID,AGENCYID,CREATEDATE,SALERESOURCE,CHECKIN,EXPENDDATE,NIGHTFROM,NIGHTTO,DEPCITY,ARRCITY,PRICEFROM,PRICETO,PACKAGE,ONLYTICKETOW,PACKTYPE)
VALUES (@RECID,@SESSIONID,@USERID,@AGENCYID,GetDate(),@SALERESOURCE,@CHECKIN,@EXPENDDATE,@NIGHTFROM,@NIGHTTO,@DEPCITY,@ARRCITY,@PRICEFROM,@PRICETO,@PACKAGE,@ONLYTICKETOW,@PACKTYPE)
Select @RECID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "SESSIONID", DbType.AnsiString, data.SESSIONID);
                db.AddInParameter(dbCommand, "USERID", DbType.AnsiString, data.USERID);
                db.AddInParameter(dbCommand, "AGENCYID", DbType.AnsiString, data.AGENCYID);
                db.AddInParameter(dbCommand, "SALERESOURCE", DbType.Int16, data.SALERESOURCE);
                db.AddInParameter(dbCommand, "CHECKIN", DbType.DateTime, data.CHECKIN);
                db.AddInParameter(dbCommand, "EXPENDDATE", DbType.Int16, data.EXPENDDATE);
                db.AddInParameter(dbCommand, "NIGHTFROM", DbType.Int16, data.NIGHTFROM);
                db.AddInParameter(dbCommand, "NIGHTTO", DbType.Int16, data.NIGHTTO);
                db.AddInParameter(dbCommand, "DEPCITY", DbType.Int32, data.DEPCITY);
                db.AddInParameter(dbCommand, "ARRCITY", DbType.Int32, data.ARRCITY);
                db.AddInParameter(dbCommand, "PRICEFROM", DbType.Decimal, data.PRICEFROM);
                db.AddInParameter(dbCommand, "PRICETO", DbType.Decimal, data.PRICETO);
                db.AddInParameter(dbCommand, "PACKAGE", DbType.AnsiString, data.PACKAGE);
                db.AddInParameter(dbCommand, "PACKTYPE", DbType.AnsiString, data.PACKTYPE);
                db.AddInParameter(dbCommand, "ONLYTICKETOW", DbType.Boolean, data.ONLYTICKETOW);
                
                return (Guid?)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Guid? saveWEBSearchLog(string sessionID, string userID, string agencyID, SaleResource saleResource,
                DateTime checkIn, Int16? expandDate, Int16? nightFrom, Int16? nightTo, int? depCity, int? arrCity,
                decimal? priceFrom, decimal? priceTo, string package, string packType, bool? onlyTicketOW, ref string errorMsg)
        {
            string tsql =
@"
DECLARE @RECID uniqueidentifier;
SET @RECID = NewID();
INSERT INTO WEBSearchLog (RECID,SESSIONID,USERID,AGENCYID,CREATEDATE,SALERESOURCE,CHECKIN,EXPENDDATE,NIGHTFROM,NIGHTTO,DEPCITY,ARRCITY,PRICEFROM,PRICETO,PACKAGE,ONLYTICKETOW,PACKTYPE)
VALUES (@RECID,@SESSIONID,@USERID,@AGENCYID,GetDate(),@SALERESOURCE,@CHECKIN,@EXPENDDATE,@NIGHTFROM,@NIGHTTO,@DEPCITY,@ARRCITY,@PRICEFROM,@PRICETO,@PACKAGE,@ONLYTICKETOW,@PACKTYPE)
Select @RECID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "SESSIONID", DbType.AnsiString, sessionID);
                db.AddInParameter(dbCommand, "USERID", DbType.AnsiString, userID);
                db.AddInParameter(dbCommand, "AGENCYID", DbType.AnsiString, agencyID);
                db.AddInParameter(dbCommand, "SALERESOURCE", DbType.Int16, saleResource);
                db.AddInParameter(dbCommand, "CHECKIN", DbType.DateTime, checkIn);
                db.AddInParameter(dbCommand, "EXPENDDATE", DbType.Int16, expandDate);
                db.AddInParameter(dbCommand, "NIGHTFROM", DbType.Int16, nightFrom);
                db.AddInParameter(dbCommand, "NIGHTTO", DbType.Int16, nightTo);
                db.AddInParameter(dbCommand, "DEPCITY", DbType.Int32, depCity);
                db.AddInParameter(dbCommand, "ARRCITY", DbType.Int32, arrCity);
                db.AddInParameter(dbCommand, "PRICEFROM", DbType.Decimal, priceFrom);
                db.AddInParameter(dbCommand, "PRICETO", DbType.Decimal, priceTo);
                db.AddInParameter(dbCommand, "PACKAGE", DbType.AnsiString, package);
                db.AddInParameter(dbCommand, "PACKTYPE", DbType.AnsiString, packType);
                db.AddInParameter(dbCommand, "ONLYTICKETOW", DbType.Boolean, onlyTicketOW);

                return (Guid?)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBRoomInfoLog(WEBRoomInfoLog data, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBRoomInfoLog (PARENTID,ADULT,CHILD)
VALUES (@PARENTID,@ADULT,@CHILD)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, data.PARENTID);
                db.AddInParameter(dbCommand, "ADULT", DbType.Int16, data.ADULT);
                db.AddInParameter(dbCommand, "CHILD", DbType.Int16, data.CHILD);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBRoomInfoLog(Guid parentID, Int16? adult, Int16? child, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBRoomInfoLog (PARENTID,ADULT,CHILD)
VALUES (@PARENTID,@ADULT,@CHILD)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, parentID);
                db.AddInParameter(dbCommand, "ADULT", DbType.Int16, adult);
                db.AddInParameter(dbCommand, "CHILD", DbType.Int16, child);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBLocationLog(WEBLocationLog data, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBResortLog (PARENTID,RESORT)
VALUES (@PARENTID,@RESORT)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, data.PARENTID);
                db.AddInParameter(dbCommand, "RESORT", DbType.Int32, data.LOCATION);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBLocationLog(Guid parentID, int? location, ref string errorMsg)
        {
            if (!location.HasValue) return true;
            string tsql =
@"
INSERT INTO WEBResortLog (PARENTID,RESORT)
VALUES (@PARENTID,@RESORT)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, parentID);
                db.AddInParameter(dbCommand, "RESORT", DbType.Int32, location);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBHotelLog(WEBHotelLog data, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBHotelLog (PARENTID,HOTEL)
VALUES (@PARENTID,@HOTEL)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, data.PARENTID);
                db.AddInParameter(dbCommand, "HOTEL", DbType.AnsiString, data.HOTEL);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBHotelLog(Guid parentID, string hotel, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBHotelLog (PARENTID,HOTEL)
VALUES (@PARENTID,@HOTEL)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, parentID);
                db.AddInParameter(dbCommand, "HOTEL", DbType.AnsiString, hotel);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBRoomLog(WEBRoomLog data, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBRoomLog (PARENTID,ROOM)
VALUES (@PARENTID,@ROOM)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, data.PARENTID);
                db.AddInParameter(dbCommand, "ROOM", DbType.AnsiString, data.ROOM);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBRoomLog(Guid parentID, string room, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBRoomLog (PARENTID,ROOM)
VALUES (@PARENTID,@ROOM)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, parentID);
                db.AddInParameter(dbCommand, "ROOM", DbType.AnsiString, room);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBBoardLog(WEBBoardLog data, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBBoardLog (PARENTID,BOARD)
VALUES (@PARENTID,@BOARD)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, data.PARENTID);
                db.AddInParameter(dbCommand, "BOARD", DbType.AnsiString, data.BOARD);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBBoardLog(Guid parentID, string board, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBBoardLog (PARENTID,BOARD)
VALUES (@PARENTID,@BOARD)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, parentID);
                db.AddInParameter(dbCommand, "BOARD", DbType.AnsiString, board);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBCategoryLog(WEBCategoryLog data, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBCategoryLog (PARENTID,CATEGORY)
VALUES (@PARENTID,@CATEGORY)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, data.PARENTID);
                db.AddInParameter(dbCommand, "CATEGORY", DbType.AnsiString, data.CATEGORY);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool saveWEBCategoryLog(Guid parentID, string category, ref string errorMsg)
        {
            string tsql =
@"
INSERT INTO WEBCategoryLog (PARENTID,CATEGORY)
VALUES (@PARENTID,@CATEGORY)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, parentID);
                db.AddInParameter(dbCommand, "CATEGORY", DbType.AnsiString, category);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Guid? saveWEBBookLog(WEBBookLog data, ref string errorMsg)
        {
            string tsql =
@"
DECLARE @RECID uniqueidentifier;
SET @RECID = NewID();
INSERT INTO WEBBookLog (PARENTID,BEGINBOOK,ENDBOOK)
VALUES (@PARENTID,@BEGINBOOK,@ENDBOOK)
Select @RECID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, data.PARENTID);
                db.AddInParameter(dbCommand, "BEGINBOOK", DbType.DateTime, data.BEGINBOOK);
                db.AddInParameter(dbCommand, "ENDBOOK", DbType.DateTime, data.ENDBOOK);                
                return (Guid?)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Guid? saveWEBBookLog(Guid parentID, DateTime? beginBook, DateTime? endBook, ref string errorMsg)
        {
            string tsql =
@"
DECLARE @RECID uniqueidentifier;
SET @RECID=NewID();
INSERT INTO WEBBookLog (RECID,PARENTID,BEGINBOOK,ENDBOOK)
VALUES (@RECID,@PARENTID,@BEGINBOOK,@ENDBOOK)
Select @RECID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PARENTID", DbType.Guid, parentID);
                db.AddInParameter(dbCommand, "BEGINBOOK", DbType.DateTime, beginBook);
                db.AddInParameter(dbCommand, "ENDBOOK", DbType.DateTime, endBook);
                return (Guid?)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool updateWEBBookLog(Guid bookLogID, DateTime? endBook, string resNo, ref string errorMsg)
        {
            string tsql =
            @"
if (Exists(Select RecID From WEBBookLog (NOLOCK) Where RecID=@recID))
Begin
  UPDATE WEBBookLog 
  SET ENDBOOK=@ENDBOOK,
      RESNO=@resNo
  WHERE RECID=@recID
End
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "recID", DbType.Guid, bookLogID);                
                db.AddInParameter(dbCommand, "ENDBOOK", DbType.DateTime, endBook);
                db.AddInParameter(dbCommand, "resNo", DbType.AnsiString, resNo);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public void SaveWebServiceLog(Guid pId, string pTransactionId, string pRequestUrl, string pRequest, string pResponse,string pExceptionMessage)
        {
            try
            {

                string sqlCommand = "";
                sqlCommand = @"
                if exists(select null from WebServiceLog(nolock) where Id=@Id)
                    UPDATE WebServiceLog Set ResponseMessage=@ResponseMessage,ExceptionMessage=@ExceptionMessage where Id=@Id
                else
                    INSERT INTO WebServiceLog(
                [Id],[ServiceUrl],[RequestId],[RequestMessage],[ResponseMessage],[ExceptionMessage],[ElapsedTime],[CreateDate]) 
                    values(
                @Id,@RequestUrl,@TransactionId,@RequestMessage,@ResponseMessage,@ExceptionMessage,0,GETDATE()  

                )";

                Database db = (Database)DatabaseFactory.CreateDatabase();
                DbCommand dbCommand = db.GetSqlStringCommand(sqlCommand);
                db.AddInParameter(dbCommand, "Id", DbType.String, pId);
                db.AddInParameter(dbCommand, "TransactionId", DbType.String, pTransactionId);
                db.AddInParameter(dbCommand, "RequestUrl", DbType.String, pRequestUrl);
                db.AddInParameter(dbCommand, "RequestMessage", DbType.String, pRequest);
                db.AddInParameter(dbCommand, "ResponseMessage", DbType.String, pResponse);
                db.AddInParameter(dbCommand, "ExceptionMessage", DbType.String, pExceptionMessage);
                db.ExecuteNonQuery(dbCommand);
            }
            catch(Exception ex)
            {

            }
        }
    }
}
