﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;
using System.Web;

namespace TvBo
{
    public class Visas
    {
        public List<VisaFormSex> getVisaFormSexList()
        {
            List<VisaFormSex> retVal = new List<VisaFormSex>();
            retVal.Add(new VisaFormSex { Sex = "", SexName = "---", SexNameL = "---" });
            retVal.Add(new VisaFormSex { Sex = "M", SexName = "Male", SexNameL = "Мужской" });
            retVal.Add(new VisaFormSex { Sex = "F", SexName = "Female", SexNameL = "Женский" });
            return retVal;
        }

        public List<PassportTypeRecord> getPassportTypeList()
        {
            List<PassportTypeRecord> retVal = new List<PassportTypeRecord>();
            retVal.Add(new PassportTypeRecord { Value = 0, Name = "Ordinary passport", NameL = "Обычный паспорт" });
            retVal.Add(new PassportTypeRecord { Value = 1, Name = "Diplomatic passport", NameL = "Дипломатический паспорт" });
            retVal.Add(new PassportTypeRecord { Value = 2, Name = "Passport service", NameL = "Служебный паспорт" });
            retVal.Add(new PassportTypeRecord { Value = 3, Name = "Official passport", NameL = "Официальный паспорт" });
            retVal.Add(new PassportTypeRecord { Value = 4, Name = "Special passport", NameL = "Особый паспорт" });
            retVal.Add(new PassportTypeRecord { Value = 5, Name = "Other travel document (please specify)", NameL = "иной проездной документ (указать какой)" });
            return retVal;
        }

        public List<PurposeTravelRecord> getPurposeTravelList()
        {
            List<PurposeTravelRecord> retVal = new List<PurposeTravelRecord>();
            retVal.Add(new PurposeTravelRecord { Value = 0, Name = "Tourism", NameL = "Туризм" });
            retVal.Add(new PurposeTravelRecord { Value = 1, Name = "Business", NameL = "Деловая" });
            retVal.Add(new PurposeTravelRecord { Value = 2, Name = "Visit family or friends", NameL = "Посещение родственников или друзей" });
            retVal.Add(new PurposeTravelRecord { Value = 3, Name = "Cultural", NameL = "Культура" });
            retVal.Add(new PurposeTravelRecord { Value = 4, Name = "sport", NameL = "Спорт" });
            retVal.Add(new PurposeTravelRecord { Value = 5, Name = "Official visit", NameL = "Официальная" });
            retVal.Add(new PurposeTravelRecord { Value = 6, Name = "Medical reasons", NameL = "Лечение" });
            retVal.Add(new PurposeTravelRecord { Value = 7, Name = "Studies", NameL = "Учеба" });
            retVal.Add(new PurposeTravelRecord { Value = 8, Name = "Transit", NameL = "Транзит" });
            retVal.Add(new PurposeTravelRecord { Value = 9, Name = "Airport transit", NameL = "Транзит ч. аэропорт" });
            retVal.Add(new PurposeTravelRecord { Value = 10, Name = "Other (specify)", NameL = "Иная (указать)" });
            return retVal;
        }

        public List<MaritalRecord> getMaritalList()
        {
            List<MaritalRecord> retVal = new List<MaritalRecord>();
            retVal.Add(new MaritalRecord { Value = null, Name = "---", NameL = "---" });
            retVal.Add(new MaritalRecord { Value = 0, Name = "Single", NameL = "один" });
            retVal.Add(new MaritalRecord { Value = 1, Name = "Married", NameL = "женат" });
            retVal.Add(new MaritalRecord { Value = 2, Name = "Separated", NameL = "разделенный" });
            retVal.Add(new MaritalRecord { Value = 3, Name = "Divorced", NameL = "разведенный" });
            retVal.Add(new MaritalRecord { Value = 4, Name = "Widowed", NameL = "овдовевший" });
            retVal.Add(new MaritalRecord { Value = 5, Name = "Others", NameL = "другие" });
            return retVal;
        }

        public VisaRecord getVisa(string Market, string Code, ref string errorMsg)
        {
            VisaRecord record = new VisaRecord();
            string tsql = string.Empty;
            if (VersionControl.getTableField("Visa", "RestSingleSale"))
                tsql =
@"
Select Code,[Name],LocalName=isnull(dbo.FindLocalName(NameLID,@Market),[Name]),
  NameS,VisaType,VisaLocation,PrepareDay,PrepareHour, 
  EmbassAddr,EmbassCity,EmbassPhone,EmbassFax,VisaDoc,VisaCondition,
  [Description],ConfStat,TaxPer,PayCom,PayComEB,PayPasEB,CanDiscount,RestSingleSale
From Visa (NOLOCK) 
Where Code=@Code
";
            else
                tsql =
@"
Select Code,[Name],LocalName=isnull(dbo.FindLocalName(NameLID,@Market),[Name]),
  NameS,VisaType,VisaLocation,PrepareDay,PrepareHour, 
  EmbassAddr,EmbassCity,EmbassPhone,EmbassFax,VisaDoc,VisaCondition,
  [Description],ConfStat,TaxPer,PayCom,PayComEB,PayPasEB,CanDiscount,RestSingleSale=Cast(0 As Bit)
From Visa (NOLOCK) 
Where Code=@Code
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", System.Data.DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", System.Data.DbType.String, Code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.VisaType = Conversion.getStrOrNull(R["VisaType"]);
                        record.VisaLocation = Conversion.getStrOrNull(R["VisaLocation"]);
                        record.PrepareDay = Conversion.getInt16OrNull(R["PrepareDay"]);
                        record.PrepareHour = Conversion.getInt16OrNull(R["PrepareHour"]);
                        record.EmbassAddr = Conversion.getStrOrNull(R["EmbassAddr"]);
                        record.EmbassCity = Conversion.getStrOrNull(R["EmbassCity"]);
                        record.EmbassPhone = Conversion.getStrOrNull(R["EmbassPhone"]);
                        record.EmbassFax = Conversion.getStrOrNull(R["EmbassFax"]);
                        record.VisaDoc = Conversion.getStrOrNull(R["VisaDoc"]);
                        record.VisaCondition = Conversion.getStrOrNull(R["VisaCondition"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        record.RestSingleSale = Conversion.getBoolOrNull(R["RestSingleSale"]);
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Location> getVisaLocations(string Market, string plMarket, ref string errorMsg)
        {
            List<Location> records = new List<Location>();
            string tsql =
@"
Select RecID,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),
    Code,Name,NameS,Type,Village,Town,City,L.Country,TaxPer 
From (	
    Select Distinct Country 
	From VisaPrice (NOLOCK)
	Where Market=@plMarket      
) X
Join Location L (NOLOCK) ON L.RecID=X.Country
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", System.Data.DbType.String, Market);
                db.AddInParameter(dbCommand, "PLMarket", System.Data.DbType.String, plMarket);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        Location record = new Location();
                        record.RecID = (int)oReader["RecID"];
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Type = (Int16)oReader["Type"];
                        record.Village = Conversion.getInt32OrNull(oReader["Village"]);
                        record.Town = Conversion.getInt32OrNull(oReader["Town"]);
                        record.City = Conversion.getInt32OrNull(oReader["City"]);
                        record.Country = (int)oReader["Country"];
                        record.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<VisaRecord> getVisaCountry(string Market, string plMarket, int? Country, DateTime? BegDate, DateTime? EndDate, ref string errorMsg)
        {
            List<VisaRecord> records = new List<VisaRecord>();
            string tsql = string.Empty;
            if (VersionControl.getTableField("Visa", "RestSingleSale"))
            {
                tsql =
@"
Select Code,[Name],LocalName=isnull(dbo.FindLocalName(NameLID,@Market),[Name]),
	NameS,VisaType,VisaLocation,PrepareDay,PrepareHour, 
	EmbassAddr,EmbassCity,EmbassPhone,EmbassFax,VisaDoc,VisaCondition,
	[Description],ConfStat,TaxPer,PayCom,PayComEB,PayPasEB,CanDiscount,RestSingleSale
From (	
	Select Distinct Visa
	From VisaPrice (NOLOCK)
	Where Market=@plMarket
	And Country=@Country
	And BegDate<=@BegDate
	And (@EndDate is null or EndDate>=@EndDate)
) X
Join Visa V (NOLOCK) ON V.Code=X.Visa
where B2BPub=1
";
            }
            else
            {
                tsql =
@"
Select Code,[Name],LocalName=isnull(dbo.FindLocalName(NameLID,@Market),[Name]),
	NameS,VisaType,VisaLocation,PrepareDay,PrepareHour, 
	EmbassAddr,EmbassCity,EmbassPhone,EmbassFax,VisaDoc,VisaCondition,
	[Description],ConfStat,TaxPer,PayCom,PayComEB,PayPasEB,CanDiscount,RestSingleSale=null
From (	
	Select Distinct Visa
	From VisaPrice (NOLOCK)
	Where Market=@plMarket
	And Country=@Country
	And BegDate<=@BegDate
	And (@EndDate is null or EndDate>=@EndDate)
) X
Join Visa V (NOLOCK) ON V.Code=X.Visa 
where B2BPub=1
";
            }
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", System.Data.DbType.AnsiString, Market);
                db.AddInParameter(dbCommand, "plMarket", System.Data.DbType.AnsiString, plMarket);
                db.AddInParameter(dbCommand, "Country", System.Data.DbType.Int32, Country);
                db.AddInParameter(dbCommand, "BegDate", System.Data.DbType.DateTime, BegDate);
                db.AddInParameter(dbCommand, "EndDate", System.Data.DbType.DateTime, EndDate);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        VisaRecord record = new VisaRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.VisaType = Conversion.getStrOrNull(R["VisaType"]);
                        record.VisaLocation = Conversion.getStrOrNull(R["VisaLocation"]);
                        record.PrepareDay = Conversion.getInt16OrNull(R["PrepareDay"]);
                        record.PrepareHour = Conversion.getInt16OrNull(R["PrepareHour"]);
                        record.EmbassAddr = Conversion.getStrOrNull(R["EmbassAddr"]);
                        record.EmbassCity = Conversion.getStrOrNull(R["EmbassCity"]);
                        record.EmbassPhone = Conversion.getStrOrNull(R["EmbassPhone"]);
                        record.EmbassFax = Conversion.getStrOrNull(R["EmbassFax"]);
                        record.VisaDoc = Conversion.getStrOrNull(R["VisaDoc"]);
                        record.VisaCondition = Conversion.getStrOrNull(R["VisaCondition"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        record.RestSingleSale = Conversion.getBoolOrNull(R["RestSingleSale"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<EntrancePortRecord> getEntrancePortList(User UserData, ref string errorMsg)
        {
            List<EntrancePortRecord> records = new List<EntrancePortRecord>();
            string tsql =
@"
Select RecID,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@pMarket),Name),
  Location,LocationName=dbo.ufn_GetLocationName(Location),LocationNameL=dbo.GetLocationNameL(Location,@pMarket),
  TrfLocation,TrfLocationName=dbo.ufn_GetLocationName(TrfLocation),TrfLocationNameL=dbo.GetLocationNameL(TrfLocation,@pMarket),
  PortType 
From EntrancePort (NOLOCK)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pMarket", System.Data.DbType.AnsiString, UserData.Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        EntrancePortRecord record = new EntrancePortRecord();
                        record.RecID = (int)oReader["RecID"];
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.Location = Conversion.getInt32OrNull(oReader["Location"]);
                        record.LocationName = Conversion.getStrOrNull(oReader["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(oReader["LocationNameL"]);
                        record.TrfLocation = Conversion.getInt32OrNull(oReader["TrfLocation"]);
                        record.TrfLocationName = Conversion.getStrOrNull(oReader["TrfLocationName"]);
                        record.TrfLocationNameL = Conversion.getStrOrNull(oReader["TrfLocationNameL"]);
                        record.PortType = Conversion.getInt16OrNull(oReader["PortType"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<VisaRestricRecord> getVisaRestricList(User UserData, ref string errorMsg)
        {
            List<VisaRestricRecord> records = new List<VisaRestricRecord>();
            if (VersionControl.tableIsExists("", ref errorMsg)) {
                return records;
            }
            string tsql =
@"
Select RecID,Nationality From VisaRestric (NOLOCK)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    while (oReader.Read()) {
                        VisaRestricRecord record = new VisaRestricRecord();
                        record.RecID = (int)oReader["RecID"];
                        record.Nationality = Conversion.getStrOrNull(oReader["Nationality"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return records;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }

    public class OnlyVisas
    {

        public ResDataRecord GenerateResMain(User UserData, ResDataRecord ResData, DateTime BegDate, DateTime EndDate, Int32? DepCity, Int32? ArrCity, Int16 Adult, Int16 Child, Int16 Infant, ref string errorMsg)
        {
            errorMsg = string.Empty;

            ResMainRecord ResMain = new ResMainRecord(null);

            ResMain.PackType = string.Empty;

            Int16? SaleCurOpt = (ResMain.PackType != "") ? new Reservation().getSaleCurOption(UserData) : new Reservation().getIndSaleCurOption(UserData);
            string SaleCur = string.Empty;
            if (!SaleCurOpt.HasValue)
                SaleCur = UserData.SaleCur;
            else
                SaleCur = UserData.SaleCur;

            try
            {
                ResMain.ResNo = UserData.SID.Substring(0, 10);
                ResMain.Operator = UserData.Operator;
                ResMain.Market = UserData.Market;
                ResMain.Office = UserData.OprOffice;
                ResMain.Agency = UserData.AgencyID;
                ResMain.AgencyName = UserData.AgencyName;
                ResMain.AgencyNameL = UserData.AgencyName;
                ResMain.HolPack = string.Empty;
                ResMain.HolPackName = string.Empty;
                ResMain.HolPackNameL = string.Empty;
                ResMain.BegDate = BegDate;
                ResMain.EndDate = EndDate;
                ResMain.Days = Convert.ToInt16((EndDate - BegDate).TotalDays.ToString());
                ResMain.PriceSource = (Int16)0;
                ResMain.PriceListNo = 0;
                ResMain.SaleResource = (Int16)1;
                ResMain.ResLock = 0;
                ResMain.ResStat = 9;
                ResMain.ConfStat = 0;
                ResMain.AgencyUser = UserData.UserID;
                ResMain.AgencyUserName = UserData.UserName;
                ResMain.AgencyUserNameL = UserData.UserName;
                ResMain.ResDate = DateTime.Today.Date;
                ResMain.ChgUser = "";
                ResMain.NetCur = SaleCur;
                ResMain.SaleCur = SaleCur;
                ResMain.Ballon = "N";
                ResMain.ReceiveDate = DateTime.Now;
                ResMain.RecCrtDate = DateTime.Now;
                ResMain.RecCrtUser = UserData.UserID;
                ResMain.RecChgUser = "";
                ResMain.Code1 = "";
                ResMain.Code2 = "";
                ResMain.Code3 = "";
                ResMain.Code4 = "";
                ResMain.Adult = Adult;
                ResMain.Child = Child;
                ResMain.ConfToAgency = "N";
                ResMain.PaymentStat = "U";
                ResMain.Discount = 0;
                ResMain.AgencyComMan = "N";
                ResMain.SendInComing = "W";
                ResMain.AllDocPrint = "N";
                ResMain.SendAgency = "N";
                ResMain.EB = "N";
                ResMain.EBAgency = 0;
                ResMain.EBSupID = 0;
                ResMain.EBSupMaxPer = 0;
                ResMain.EBPasID = 0;
                ResMain.EBAgencyPer = 0;
                ResMain.EBPasPer = 0;
                ResMain.EBPas = 0;
                ResMain.DepCity = DepCity;
                ResMain.DepCityName = new Locations().getLocationName(UserData.Market, ResMain.DepCity.Value, NameType.NormalName, ref errorMsg);
                ResMain.DepCityNameL = new Locations().getLocationName(UserData.Market, ResMain.DepCity.Value, NameType.LocalName, ref errorMsg);
                ResMain.ArrCity = ArrCity;
                ResMain.ArrCityName = new Locations().getLocationName(UserData.Market, ResMain.ArrCity.Value, NameType.NormalName, ref errorMsg);
                ResMain.ArrCityNameL = new Locations().getLocationName(UserData.Market, ResMain.ArrCity.Value, NameType.LocalName, ref errorMsg);
                ResMain.Credit = "N";
                ResMain.CreditExpenseCur = "";
                ResMain.Broker = "";
                ResMain.BrokerCom = 0;
                ResMain.BrokerComPer = 0;
                ResMain.PLOperator = UserData.Operator;
                ResMain.PLMarket = UserData.Market;
                ResMain.AgencyBonus = 0;
                ResMain.AgencyBonusAmount = 0;
                ResMain.UserBonus = 0;
                ResMain.UserBonusAmount = 0;
                ResMain.PasBonus = 0;
                ResMain.PasBonusAmount = 0;
                ResMain.PasPayable = 0;
                ResMain.Complete = 0;
                ResMain.ResNote = "";
                ResMain.AgencyDisPasPer = 0;
                ResMain.InvoiceTo = UserData.AgencyRec.InvoiceTo;
                ResMain.PaymentFrom = UserData.AgencyRec.PaymentFrom;
                ResMain.WebIP = UserData.IpNumber;
                ResMain.PLSpoChk = 0;
                ResMain.PBegDate = ResMain.BegDate;
                ResMain.PNight = ResMain.Days;
                ResMain.MemTable = false;
                ResData.ResMain = ResMain;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
            }

            return ResData;
        }

        public ResDataRecord GenerateTourists(User UserData, ResDataRecord ResData, Int16 Adult, Int16 Child, Int16 Infant, ref string errorMsg)
        {
            errorMsg = string.Empty;
            string ResNo = ResData.ResMain.ResNo.Substring(0, ResData.ResMain.ResNo.Length > 10 ? 10 : ResData.ResMain.ResNo.Length);
            int? defaultNation = UserData.Country != null ? UserData.Country.Value : 1;

            Nationality nationality = new Locations().getDefaultNationality(UserData, defaultNation, ref errorMsg);

            List<ResCustRecord> resCustList = ResData.ResCust;
            resCustList.Clear();

            bool ChdPassChk = UserData.TvParams.TvParamReser.ChdPassChk;

            decimal? maxInfAge = UserData.TvParams.TvParamReser.MaxInfAge.HasValue ? UserData.TvParams.TvParamReser.MaxInfAge.Value : (decimal)1.99;
            decimal? maxChdAge = UserData.TvParams.TvParamReser.MaxChdAge.HasValue ? UserData.TvParams.TvParamReser.MaxChdAge.Value : (decimal)11.99;

            List<TitleRecord> titleList = new TvBo.Common().getTitle(ref errorMsg);
            ResData.Title = titleList;
            Int16 SeqNo = 0;
            try
            {
                #region Adult Turist

                Int16 tmpTitle = 1;
                for (int i = 0; i < Adult; i++)
                {
                    try
                    {
                        SeqNo++;
                        ResCustRecord row = new ResCustRecord();
                        row.BookID = 1;
                        row.CustNo = resCustList.Count() + 1;
                        row.CustNoT = row.CustNo;
                        row.ResNo = ResNo;
                        row.SeqNo = SeqNo;
                        row.Title = tmpTitle;
                        if (tmpTitle == 1)
                        {
                            tmpTitle = 2;
                        }
                        else
                        {
                            tmpTitle = 1;
                        }
                        TitleRecord titleRec = titleList.Find(f => f.TitleNo == row.Title);
                        row.TitleStr = titleRec != null ? titleRec.Code : string.Empty;

                        row.Surname = "";
                        row.Name = "";
                        row.SurnameL = "";
                        row.NameL = "";
                        row.PassSerie = "";
                        row.PassNo = "";
                        row.PassGiven = "";
                        row.IDNo = "";
                        row.Leader = "N";
                        row.DocVoucher = 0;
                        row.DocTicket = 0;
                        row.DocInsur = 0;
                        row.Bonus = 0;
                        row.BonusAmount = 0;
                        row.Nationality = nationality != null ? nationality.Code3 : string.Empty;
                        row.NationalityName = nationality != null ? nationality.Name : string.Empty;
                        row.HasPassport = true;
                        row.MemTable = false;
                        resCustList.Add(row);
                    }
                    catch (Exception Ex)
                    {
                        resCustList.Clear();
                        errorMsg = Ex.Message;
                        return null;
                    }
                }
                #endregion Adult Turist

                #region Child Turist
                tmpTitle = 6;
                for (int i = 0; i < Child; i++)
                {
                    try
                    {
                        SeqNo++;
                        ResCustRecord row = new ResCustRecord();
                        row.BookID = 1;
                        row.CustNo = resCustList.Count() + 1;
                        row.CustNoT = row.CustNo;
                        row.ResNo = ResNo;
                        row.SeqNo = SeqNo;
                        row.Title = tmpTitle;

                        TitleRecord titleRec = titleList.Find(f => f.TitleNo == row.Title);
                        row.TitleStr = titleRec != null ? titleRec.Code : string.Empty;

                        row.Surname = "";
                        row.Name = "";
                        row.SurnameL = "";
                        row.NameL = "";
                        row.Age = (Int16)maxChdAge;
                        row.Birtday = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.AddYears((-1) * row.Age.Value) : ResData.ResMain.EndDate.Value.AddYears(-11);
                        row.PassSerie = "";
                        row.PassNo = "";
                        row.PassGiven = "";
                        row.IDNo = "";
                        row.Leader = "N";
                        row.DocVoucher = 0;
                        row.DocTicket = 0;
                        row.DocInsur = 0;
                        row.Bonus = 0;
                        row.BonusAmount = 0;
                        row.Nationality = nationality != null ? nationality.Code3 : string.Empty;
                        row.NationalityName = nationality != null ? nationality.Name : string.Empty;
                        if (ChdPassChk)
                            row.HasPassport = ChdPassChk;
                        row.MemTable = false;
                        resCustList.Add(row);
                    }
                    catch (Exception Ex)
                    {
                        resCustList.Clear();
                        errorMsg = Ex.Message;
                        return null;
                    }
                }
                #endregion

                #region Infant Turist
                tmpTitle = 8;
                for (int i = 0; i < Infant; i++)
                {
                    try
                    {
                        SeqNo++;
                        ResCustRecord row = new ResCustRecord();
                        row.BookID = 1;
                        row.CustNo = resCustList.Count() + 1;
                        row.CustNoT = row.CustNo;
                        row.ResNo = ResNo;
                        row.SeqNo = SeqNo;
                        row.Title = tmpTitle;

                        TitleRecord titleRec = titleList.Find(f => f.TitleNo == row.Title);
                        row.TitleStr = titleRec != null ? titleRec.Code : string.Empty;

                        row.Surname = "";
                        row.Name = "";
                        row.SurnameL = "";
                        row.NameL = "";
                        row.Age = (Int16)maxInfAge;
                        row.Birtday = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.AddYears((-1) * row.Age.Value) : ResData.ResMain.EndDate.Value.AddYears(-1);
                        row.PassSerie = "";
                        row.PassNo = "";
                        row.PassGiven = "";
                        row.IDNo = "";
                        row.Leader = "N";
                        row.DocVoucher = 0;
                        row.DocTicket = 0;
                        row.DocInsur = 0;
                        row.Bonus = 0;
                        row.BonusAmount = 0;
                        row.Nationality = nationality != null ? nationality.Code3 : string.Empty;
                        row.NationalityName = nationality != null ? nationality.Name : string.Empty;
                        if (ChdPassChk)
                            row.HasPassport = ChdPassChk;
                        row.MemTable = false;
                        resCustList.Add(row);
                    }
                    catch (Exception Ex)
                    {
                        resCustList.Clear();
                        errorMsg = Ex.Message;
                        return null;
                    }
                }
                #endregion

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }

            if (ResData.ResCust.Count > 0)
                ResData.ResCust[0].Leader = "Y";

            return ResData;
        }

        public ResDataRecord GenerateServices(User UserData, ResDataRecord ResData, string Service, DateTime BegDate, DateTime EndDate, int? DepLocation, int? ArrLocation, ref string errorMsg)
        {
            List<ResServiceRecord> resServiceList = ResData.ResService;
            List<ResServiceExtRecord> resServiceExtList = ResData.ResServiceExt;

            List<ResConRecord> resConList = ResData.ResCon;
            List<ResConExtRecord> resConExtList = ResData.ResConExt;


            bool ResSeparate = new TvBo.Common().getResServiceSeperate(Service);
            plResServiceRecord CompService = new plResServiceRecord();
            try
            {
                int ServiceID = 0;
                string DepRet = string.Empty;

                VisaRecord visa = new Visas().getVisa(UserData.Market, Service, ref errorMsg);

                CompService.CatPackID = 0;
                CompService.RecNo = 0;
                CompService.ServiceType = "VISA";
                CompService.Service = Service;
                CompService.Room = string.Empty;
                CompService.Accom = string.Empty;
                CompService.Board = string.Empty;
                CompService.NetCur = ResData.ResMain.NetCur;
                CompService.Supplier = string.Empty;
                CompService.Class = string.Empty;
                CompService.SClass = string.Empty;
                CompService.PriceType = 0;
                CompService.NetPriceID = 0;
                CompService.StepNo = 1;
                CompService.Night = Convert.ToInt16((EndDate - BegDate).TotalDays.ToString());
                CompService.StartDay = 0;
                CompService.Duration = Convert.ToInt16((EndDate - BegDate).TotalDays.ToString());
                CompService.CatPricePID = 0;
                CompService.Departure = DepLocation;
                CompService.Arrival = ArrLocation;
                CompService.Direction = string.Empty;
                CompService.Bus = string.Empty;
                CompService.ResSeparate = ResSeparate;
                CompService.AllotType = string.Empty;
                CompService.DepRet = DepRet;
                CompService.Class = string.Empty;
                CompService.PayCom = visa != null && visa.PayCom ? "Y" : "N";
                CompService.PayComEB = visa != null && visa.PayComEB ? "Y" : "N";
                CompService.PayPasEB = visa != null && visa.PayPasEB;

                List<SelectCustRecord> SelectCust = (from q in ResData.ResCust
                                                     select new SelectCustRecord
                                                     {
                                                         BookID = 1,
                                                         CustNo = q.CustNo,
                                                         Customers = q.Surname + " " + q.Name,
                                                         Selected = true
                                                     }).ToList<SelectCustRecord>();

                resServiceList = new Reservation().AddResService(UserData, ResData, CompService, (int)1, SearchType.TourPackageSearch, false, false, false, SelectCust, ref errorMsg);

                if (errorMsg != "" || resServiceList == null || resServiceList.Count == 0)
                {
                    return ResData;
                }

                ServiceID = resServiceList[resServiceList.Count - 1].RecID;
                resConList = new Reservation().AddResCon(UserData, ResData, SelectCust, 1, ServiceID, false, ref errorMsg);
                if (errorMsg != "")
                {
                    return ResData;
                }
                resServiceExtList = new Reservation().AddResServiceExt(UserData, ResData, resServiceList[resServiceList.Count - 1], ref resConExtList, 1, 1, ServiceID, null, ref errorMsg);
                if (errorMsg != "")
                {
                    return ResData;
                }
                return ResData;

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
            }


            return ResData;
        }

        public ResDataRecord getOnlyVisaResData(User UserData, int? Country, string Service, DateTime? CheckIn, DateTime? CheckOut, Int16? Adult, Int16? Child, Int16? Infant, ref string errorMsg)
        {
            ResDataRecord ResData = new ResDataRecord();

            OfficeRecord office = new Common().getOffice(UserData.Market, UserData.AgencyRec.OprOffice, ref errorMsg);
            try
            {
                SearchResult rowS = new SearchResult();
                #region Age Group
                List<AgeGroup> ageGroups = new List<AgeGroup>();
                int roomCnt = 0;
                int refNo = 0;
                roomCnt++;
                refNo++;
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Adult, GroupNo = roomCnt, RefNo = refNo, Unit = Adult, Age = null, DateOfBirth = null });
                if (Infant > 0)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Infant, GroupNo = roomCnt, RefNo = refNo, Unit = Infant, Age = Convert.ToDecimal(199.0 / 100), DateOfBirth = null });
                if (Child > 0)
                    ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = Child, Age = Convert.ToDecimal(1299.0 / 100), DateOfBirth = null });
                #endregion
                rowS.CatPackID = 0;
                rowS.RefNo = 1;
                rowS.ARecNo = 0;
                rowS.PRecNo = 0;
                rowS.CheckIn = CheckIn;
                rowS.CheckOut = CheckOut;
                //rowS.Night = ;
                rowS.NetCur = UserData.SaleCur;
                rowS.ChdG1Age1 = 0;
                rowS.ChdG1Age2 = Convert.ToDecimal(199.0 / 100);
                rowS.ChdG2Age1 = Conversion.getInt32OrNull(rowS.ChdG1Age2);
                rowS.ChdG2Age2 = Convert.ToDecimal(1299.0 / 100);
                rowS.ChdG3Age1 = 0;
                rowS.ChdG3Age2 = 0;
                rowS.ChdG4Age1 = 0;
                rowS.ChdG4Age2 = 0;
                rowS.SaleCur = UserData.SaleCur;
                rowS.HolPack = "";
                rowS.Operator = UserData.Operator;
                rowS.Market = UserData.Market;
                rowS.FlightClass = "";
                rowS.DepCity = UserData.AgencyRec.Location;
                rowS.ArrCity = Country;
                rowS.HAdult = Convert.ToInt16(Adult.ToString());
                rowS.HChdAgeG1 = Convert.ToInt16(Infant.ToString());
                rowS.HChdAgeG2 = Convert.ToInt16(Child.ToString());
                rowS.HChdAgeG3 = 0;
                rowS.HChdAgeG4 = 0;
                if (Infant > 0)
                    rowS.Child1Age = rowS.ChdG1Age2.HasValue ? Convert.ToInt16(Math.Truncate(rowS.ChdG1Age2.Value)) : Convert.ToInt16(1);
                if (Child > 0)
                    rowS.Child2Age = rowS.ChdG2Age2.HasValue ? Convert.ToInt16(Math.Truncate(rowS.ChdG2Age2.Value)) : Convert.ToInt16(15);
                rowS.DepCityName = new Locations().getLocationName(UserData, UserData.AgencyRec.Location.Value, ref errorMsg);
                rowS.ArrCityName = new Locations().getLocationName(UserData, Country.Value, ref errorMsg);
                rowS.DepSeat = 0;
                rowS.RetSeat = 0;
                rowS.StopSaleGuar = 0;
                rowS.StopSaleStd = 0;
                rowS.CurrentCur = UserData.SaleCur;
                rowS.AgeGroupList = ageGroups;
                rowS.PLCur = UserData.SaleCur;
                ResData.SelectBook = new List<SearchResult>();
                ResData.SelectBook.Add(rowS);

                ResData = GenerateResMain(UserData,
                                          ResData,
                                          CheckIn.HasValue ? CheckIn.Value : DateTime.Today,
                                          CheckOut.HasValue ? CheckOut.Value : DateTime.Today.AddDays(1),
                                          UserData.AgencyRec.Location, office.Location,
                                          Adult.HasValue ? Adult.Value : (Int16)0,
                                          Child.HasValue ? Child.Value : (Int16)0,
                                          Infant.HasValue ? Infant.Value : (Int16)0, ref errorMsg);

                ResData = GenerateTourists(UserData,
                                           ResData,
                                           Adult.HasValue ? Adult.Value : (Int16)0,
                                           Child.HasValue ? Child.Value : (Int16)0,
                                           Infant.HasValue ? Infant.Value : (Int16)0, ref errorMsg);

                ResData = GenerateServices(UserData,
                                           ResData,
                                           Service,
                                           CheckIn.HasValue ? CheckIn.Value : DateTime.Today,
                                           CheckOut.HasValue ? CheckOut.Value : DateTime.Today.AddDays(1),
                                           Country,
                                           UserData.AgencyRec.Location, ref errorMsg);

                #region Calculate ResCustPrice

                string ResNo = ResData.ResMain.ResNo;
                ResNo = ResNo.Length > 10 ? ResNo.Substring(0, 10) : ResNo;
                ResMainRecord resMain = ResData.ResMain;
                string agencyBak = resMain.Agency;
                if (!string.IsNullOrEmpty(ResData.ResMain.PromoCode) && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba))
                {
                    ResData.OwnAgency = new Promos().getPromotionAgencyInfo(UserData, ResData.ResMain.PromoCode, ref errorMsg);
                }
                if (!string.IsNullOrEmpty(ResData.ResMain.PromoCode) && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba) && ResData.Resource == ResResource.B2CToB2B && !string.IsNullOrEmpty(ResData.OwnAgency))
                {
                    resMain.Agency = ResData.OwnAgency;
                }

                StringBuilder CalcStrSql = new Reservation().BuildCalcSqlString(ResData);
                if (!string.IsNullOrEmpty(ResData.ResMain.PromoCode) && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba))
                {
                    CalcStrSql.AppendFormat("exec dbo.SetResPromoCode '{0}', 1 \n", ResNo);
                }
                CalcStrSql.Append("Declare  @PassEB bit \n");
                CalcStrSql.Append("Declare  @ErrCode SmallInt \n");
                CalcStrSql.Append("Declare @Supplier varchar(10) \n");
                CalcStrSql.AppendFormat("exec dbo.usp_Calc_Res_Price '{0}', Default, Default, @PassEB OutPut, @ErrCode OutPut, 1 \n", ResNo);
                CalcStrSql.Append("Select * From #ResMain \n");
                CalcStrSql.Append("Select * From #ResService \n");
                CalcStrSql.Append("Select * From #ResServiceExt \n");
                CalcStrSql.Append("Select * From #ResCust \n");
                CalcStrSql.Append("Select * From #ResCustPrice \n");
                CalcStrSql.Append("Select * From #ResSupDis \n");
                CalcStrSql.Append("Select ErrorCode=@ErrCode \n");
                CalcStrSql.Append("Select * From #ResPromo \n");
                CalcStrSql.AppendFormat("Exec dbo.GetPromoList '{0}', 1 \n", ResNo);
                if (string.Equals(UserData.CustomRegID, Common.crID_SunFun))
                {
                    CalcStrSql.Append("Select * From #ResCon \n");
                    CalcStrSql.Append("Select * From #ResConExt \n");
                }
                CalcStrSql.AppendFormat("Select * From #CalcErrTbl \n");
                Database db = (Database)DatabaseFactory.CreateDatabase();
                DbCommand dbCommand = db.GetSqlStringCommand(CalcStrSql.ToString());
                //dbCommand.CommandTimeout = 120;
                DataSet ds;
                try
                {
                    ds = db.ExecuteDataSet(dbCommand);
                    if (ds != null)
                    {
                        ds.Tables[0].TableName = "ResMain";
                        ds.Tables[1].TableName = "ResService";
                        ds.Tables[2].TableName = "ResServiceExt";
                        ds.Tables[3].TableName = "ResCust";
                        ds.Tables[4].TableName = "ResCustPrice";
                        ds.Tables[5].TableName = "ResSupDis";
                        ds.Tables[6].TableName = "ErrorTable";
                        ds.Tables[7].TableName = "ResPromo";
                        ds.Tables[8].TableName = "PromoList";
                        if (string.Equals(UserData.CustomRegID, Common.crID_SunFun))
                        {
                            ds.Tables[9].TableName = "ResCon";
                            //ds.Tables[10].TableName = "ResConExt";
                        }
                        ds.Tables[ds.Tables.Count - 1].TableName = "CalcErrTbl";
                    }
                    else
                    {
                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationPriceNotCalculated").ToString();
                        ds = null;
                        return ResData;
                    }

                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    ds = null;
                    return ResData;
                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
                #endregion Calculate ResCustPrice

                int ErrorCode = Convert.ToInt32(ds.Tables["ErrorTable"].Rows[0]["ErrorCode"].ToString());
                if (ErrorCode != 0)
                {
                    errorMsg = new ResCalcError().calcError(ErrorCode, ResData);
                    return ResData;
                }
                if (ds.Tables["CalcErrTbl"].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables["CalcErrTbl"].Rows)
                    {
                        if (errorMsg.Length > 0)
                            errorMsg += "<br />";
                        errorMsg += new ResCalcError().calcError(Convert.ToInt32(row["ErrCode"]), ResData);
                    }
                    return ResData;
                }


                ResData = new Reservation().SetCalcDataToResData(UserData, ResData, ds, false, ref errorMsg);

                return ResData;
            }
            catch
            {
                return null;
            }
        }
    }
}
