﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class HandFees
    {
        public HandFeeRecord getHandFee(string Market, string Code, ref string errorMsg)
        {
            HandFeeRecord record = new HandFeeRecord();
            string tsql = @"Select Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market), [Name]),
	                            NameS, [Description], ConfStat, TaxPer, PayCom, PayComEB, PayPasEB, CanDiscount
                            From HandFee (NOLOCK) ";
            if (!string.IsNullOrEmpty(Code)) tsql += string.Format("Where Code= '{0}' ", Code);

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", System.Data.DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }            
        }

        public List<HandFeePriceRecord> getHandFeePrices(string Market, string Code, int? Country, int? Location, string Hotel, DateTime? BegDate, DateTime? EndDate, ref string errorMsg)
        {
            List<HandFeePriceRecord> records = new List<HandFeePriceRecord>();
            string tsql = @"Select HandFee, Market, Supplier, 
	                            Country,
	                            CountryName=(Select [Name] From Location (NOLOCK) Where RecID=HandFeePrice.Country),
	                            CountryLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=HandFeePrice.Country),
	                            Location,
	                            LocationName=(Select [Name] From Location (NOLOCK) Where RecID=HandFeePrice.Location),
	                            LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=HandFeePrice.Location),
	                            Hotel,
	                            HotelName=(Select [Name] From Hotel (NOLOCK) Where Code=HandFeePrice.Hotel),
	                            HotelLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Hotel (NOLOCK) Where Code=HandFeePrice.Hotel),
	                            HotelGrp, HotelCat, BegDate, EndDate, CalcType, NetCur, SaleCur,
	                            ChdG1MaxAge, ChdG2MaxAge, ChdG3MaxAge
                            From HandFeePrice (NOLOCK) 
                            Where Market=@Market ";

            string whereStr = " ";
            if (!string.IsNullOrEmpty(Code))
            {
                if (whereStr.Length > 0) whereStr += " And";
                whereStr += string.Format(" HandFee ='{0}' ", Code);
            }
            if (Country != null)
            {
                if (whereStr.Length > 0) whereStr += " And";
                whereStr += string.Format(" Country={0} ", Country.Value);
            }
            if (Location != null)
            {
                if (whereStr.Length > 0) whereStr += " And";
                whereStr += string.Format(" Location={0} ", Location.Value);
            }
            if (!string.IsNullOrEmpty(Hotel))
            {
                if (whereStr.Length > 0) whereStr += " And";
                whereStr += string.Format(" Hotel ='{0}' ", Hotel);
            }
            if (!Convert.IsDBNull(BegDate))
            {
                if (whereStr.Length > 0) whereStr += " And";
                whereStr += " @BegDate >= BegDate ";
            }
            if (!Convert.IsDBNull(EndDate))
            {
                if (whereStr.Length > 0) whereStr += " And";
                whereStr += " @EndDate <= EndDate ";
            }            

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                if (!Convert.IsDBNull(BegDate)) 
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate.Value.Date);
                if (!Convert.IsDBNull(EndDate))
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate.Value.Date);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HandFeePriceRecord record = new HandFeePriceRecord
                                                        {
                                                            HandFee = Conversion.getStrOrNull(R["HandFee"]),
                                                            Market = Conversion.getStrOrNull(R["Market"]),
                                                            Supplier = Conversion.getStrOrNull(R["Supplier"]),
                                                            Country = (int) R["Country"],
                                                            CountryName = Conversion.getStrOrNull(R["CountryName"]),
                                                            CountryLocalName =
                                                                Conversion.getStrOrNull(R["CountryLocalName"]),
                                                            Location = (int) R["Location"],
                                                            LocationName = Conversion.getStrOrNull(R["LocationName"]),
                                                            LocationLocalName =
                                                                Conversion.getStrOrNull(R["LocationLocalName"]),
                                                            Hotel = Conversion.getStrOrNull(R["Hotel"]),
                                                            HotelName = Conversion.getStrOrNull(R["HotelName"]),
                                                            HotelLocalName =
                                                                Conversion.getStrOrNull(R["HotelLocalName"]),
                                                            HotelGrp = Conversion.getStrOrNull(R["HotelGrp"]),
                                                            HotelCat = Conversion.getStrOrNull(R["HotelCat"]),
                                                            CalcType = Conversion.getInt16OrNull(R["CalcType"]),
                                                            BegDate = (DateTime) R["BegDate"],
                                                            EndDate = (DateTime) R["EndDate"],
                                                            NetCur = Conversion.getStrOrNull(R["NetCur"]),
                                                            SaleCur = Conversion.getStrOrNull(R["SaleCur"]),
                                                            ChdG1MaxAge = Conversion.getDecimalOrNull(R["ChdG1MaxAge"]),
                                                            ChdG2MaxAge = Conversion.getDecimalOrNull(R["ChdG2MaxAge"]),
                                                            ChdG3MaxAge = Conversion.getDecimalOrNull(R["ChdG3MaxAge"])
                                                        };
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HandFeeRecord> getHandFeeListCountryAndLocation(string Market, int? Country, int? Location, DateTime? BegDate, DateTime? EndDate, ref string errorMsg)
        {
            List<HandFeeRecord> list = new List<HandFeeRecord>();
            string whereStr = " ";
            if (Country != null)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += string.Format(" Country={0} ", Country.Value);
            }
            if (Location != null)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += string.Format(" Location={0} ", Location.Value);
            }
            if (!Convert.IsDBNull(BegDate))
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += " @BegDate >= BegDate ";
            }
            if (!Convert.IsDBNull(EndDate))
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += " @EndDate <= EndDate ";
            }

            string tsql = string.Format(@"Select HF.Code, HF.Name, LocalName=isnull(dbo.FindLocalName(HF.NameLID, @Market), HF.Name),
	                                            HF.NameS, HF.Description, HF.ConfStat, HF.TaxPer, HF.PayCom, HF.PayComEB, HF.PayPasEB, HF.CanDiscount
                                            From (	
	                                            Select Distinct HandFee 
	                                            From HandfeePrice (NOLOCK) 
	                                            Where Market = @Market	
                                                {0}
                                            ) X 
                                            Join HandFee HF (NOLOCK) ON HF.Code=X.HandFee", whereStr);            

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", System.Data.DbType.String, Market);
                if (!Convert.IsDBNull(BegDate))
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate.Value.Date);
                if (!Convert.IsDBNull(EndDate))
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate.Value.Date);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HandFeeRecord record = new HandFeeRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = (bool)R["PayPasEB"];
                        record.CanDiscount = (bool)R["CanDiscount"];
                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }  
        }
    }
}
