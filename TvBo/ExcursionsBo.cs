﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public enum ExcursionType { Regular = 0, Transit = 1, Private = 2, Tour = 3 }

    public class ExcursionRecord
    {
        public ExcursionRecord()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        Int16 _ConfStat;
        public Int16 ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool _PayCom;
        public bool PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        bool _PayComEB;
        public bool PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        ExcursionType _Type;
        public ExcursionType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        Int16? _MinAgeRest;
        public Int16? MinAgeRest
        {
            get { return _MinAgeRest; }
            set { _MinAgeRest = value; }
        }
    }

    public class ExcursionPriceRecord
    {
        public ExcursionPriceRecord()
        {
        }

        string _Excursion;
        public string Excursion
        {
            get { return _Excursion; }
            set { _Excursion = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        int _Location;
        public int Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _LocationLocalName;
        public string LocationLocalName
        {
            get { return _LocationLocalName; }
            set { _LocationLocalName = value; }
        }

        DateTime _BegDate;
        public DateTime BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime _EndDate;
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        string _SaleType;
        public string SaleType
        {
            get { return _SaleType; }
            set { _SaleType = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _FreeMaxAge;
        public decimal? FreeMaxAge
        {
            get { return _FreeMaxAge; }
            set { _FreeMaxAge = value; }
        }

        decimal? _ChdG1MaxAge;
        public decimal? ChdG1MaxAge
        {
            get { return _ChdG1MaxAge; }
            set { _ChdG1MaxAge = value; }
        }

        decimal? _ChdG2MaxAge;
        public decimal? ChdG2MaxAge
        {
            get { return _ChdG2MaxAge; }
            set { _ChdG2MaxAge = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

    }

    public class ExcursionSearchFilterData
    {
        public int? RecID { get; set; }
        public string Excursion { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public int Location { get; set; }
        public string LocationName { get; set; }
        public string LocationNameL { get; set; }
        public int? IndSale { get; set; }
        public string PriceCode { get; set; }
        public string Category { get; set; }
        public string CategoryName { get; set; }
        public string CategoryNameL { get; set; }
        public DateTime? EstTime { get; set; }
        public ExcursionType Type { get; set; }

        public ExcursionSearchFilterData()
        {
            this.Type = ExcursionType.Regular;
        }
    }

    public class ExcursionSearchCriteria
    {
        public ExcursionSearchCriteria()
        {
            _RoomsInfo = new List<SearchCriteriaRooms>();
            _SType = SearchType.OnlyExcursion;
            _Type = ExcursionType.Regular;
        }

        SearchType _SType;
        public SearchType SType
        {
            get { return _SType; }
            set { _SType = value; }
        }

        ExcursionType _Type;
        public ExcursionType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        DateTime _CheckIn;
        public DateTime CheckIn
        {
            get { return _CheckIn; }
            set { _CheckIn = value; }
        }

        Int16 _ExpandDate;
        public Int16 ExpandDate
        {
            get { return _ExpandDate; }
            set { _ExpandDate = value; }
        }

        DateTime? _Hour;
        public DateTime? Hour
        {
            get { return _Hour; }
            set { _Hour = value; }
        }

        Int16? _HourExpand;
        public Int16? HourExpand
        {
            get { return _HourExpand; }
            set { _HourExpand = value; }
        }

        Int16? _Duration;
        public Int16? Duration
        {
            get { return _Duration; }
            set { _Duration = value; }
        }

        Int16 _RoomCount;
        public Int16 RoomCount
        {
            get { return _RoomCount; }
            set { _RoomCount = value; }
        }

        List<SearchCriteriaRooms> _RoomsInfo;
        public List<SearchCriteriaRooms> RoomsInfo
        {
            get { return _RoomsInfo; }
            set { _RoomsInfo = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        bool _CurControl;
        public bool CurControl
        {
            get { return _CurControl; }
            set { _CurControl = value; }
        }

        string _CurrentCur;
        public string CurrentCur
        {
            get { return _CurrentCur; }
            set { _CurrentCur = value; }
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        string _AirPort;
        public string AirPort
        {
            get { return _AirPort; }
            set { _AirPort = value; }
        }
    }

    [Serializable()]
    public class ExcursionSearchResult
    {
        public int? RefNo { get; set; }
        public bool Calculated { get; set; }
        public string CurrentCur { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public DateTime? CheckIn { get; set; }
        public int? Location { get; set; }
        public string LocationName { get; set; }
        public string LocationNameL { get; set; }
        public decimal? SaleAdl { get; set; }
        public decimal? SaleChdG1 { get; set; }
        public decimal? SaleChdG2 { get; set; }
        public decimal? SaleChdG3 { get; set; }
        public decimal? ChdG1MaxAge { get; set; }
        public decimal? ChdG2MaxAge { get; set; }
        public decimal? ChdG3MaxAge { get; set; }
        public string SaleCur { get; set; }
        public string PriceCode { get; set; }
        public DateTime? TransFromTime { get; set; }
        public DateTime? TransToTime { get; set; }
        public int? FreeFromTime { get; set; }
        public int? FreeToTime { get; set; }

        public DateTime? ExcTransTime { get; set; }
        public int? ExcFreeTime { get; set; }
        public int? ExcTimeID { get; set; }

        public decimal? TotalPrice { get; set; }
        public string PackageName { get; set; }
        public string PackageNameL { get; set; }
        public List<ExcursionSearchResult> PackageDetails { get; set; }
        public int? VehicleCatID { get; set; }
        public string VehicleCatName { get; set; }
        public string PriceType { get; set; }//P:perPerson,S:perService

        public ExcursionSearchResult()
        {
            this.Calculated = false;
            this.PackageDetails = new List<ExcursionSearchResult>();
        }
    }

    [Serializable()]
    public class ExcursionPack
    {
        public int? RecID { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public string Market { get; set; }
        public int? MinSaleUnit { get; set; }
        public ExcursionPack()
        {
        }
    }

    [Serializable()]
    public class ExcursionPackService
    {
        public int? RecID { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public string Excursion { get; set; }
        public DateTime? ExcDate { get; set; }
        public int? PriceRecID { get; set; }

        public ExcursionPackService()
        {
        }
    }

    public class ExcursionTimeTable
    {
        public int? RecID { get; set; }
        public string Excursion { get; set; }
        public string Market { get; set; }
        public DateTime? TransFromTime { get; set; }
        public DateTime? TransToTime { get; set; }
        public int? FreeFromTime { get; set; }
        public int? FreeToTime { get; set; }
        public string PriceCode { get; set; }
        public string Explanation { get; set; }
        public ExcursionTimeTable()
        {
        }
    }
}
