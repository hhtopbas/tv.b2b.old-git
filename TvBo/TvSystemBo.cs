﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class SupplierRecord
    {
        public SupplierRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        int? _Location;
        public int? Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _FirmName;
        public string FirmName
        {
            get { return _FirmName; }
            set { _FirmName = value; }
        }

        string _Address;
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        string _AddrZip;
        public string AddrZip
        {
            get { return _AddrZip; }
            set { _AddrZip = value; }
        }

        string _AddrCity;
        public string AddrCity
        {
            get { return _AddrCity; }
            set { _AddrCity = value; }
        }

        string _AddrCountry;
        public string AddrCountry
        {
            get { return _AddrCountry; }
            set { _AddrCountry = value; }
        }

        string _InvAddress;
        public string InvAddress
        {
            get { return _InvAddress; }
            set { _InvAddress = value; }
        }

        string _InvAddrZip;
        public string InvAddrZip
        {
            get { return _InvAddrZip; }
            set { _InvAddrZip = value; }
        }

        string _InvAddrCity;
        public string InvAddrCity
        {
            get { return _InvAddrCity; }
            set { _InvAddrCity = value; }
        }

        string _InvAddrCountry;
        public string InvAddrCountry
        {
            get { return _InvAddrCountry; }
            set { _InvAddrCountry = value; }
        }

        string _TaxOffice;
        public string TaxOffice
        {
            get { return _TaxOffice; }
            set { _TaxOffice = value; }
        }

        string _TaxAccNo;
        public string TaxAccNo
        {
            get { return _TaxAccNo; }
            set { _TaxAccNo = value; }
        }

        bool _BlackList;
        public bool BlackList
        {
            get { return _BlackList; }
            set { _BlackList = value; }
        }

        string _MainOffice;
        public string MainOffice
        {
            get { return _MainOffice; }
            set { _MainOffice = value; }
        }

        string _Bank1;
        public string Bank1
        {
            get { return _Bank1; }
            set { _Bank1 = value; }
        }

        string _Bank1AccNo;
        public string Bank1AccNo
        {
            get { return _Bank1AccNo; }
            set { _Bank1AccNo = value; }
        }

        string _Bank1Curr;
        public string Bank1Curr
        {
            get { return _Bank1Curr; }
            set { _Bank1Curr = value; }
        }

        string _Bank2;
        public string Bank2
        {
            get { return _Bank2; }
            set { _Bank2 = value; }
        }

        string _Bank2AccNo;
        public string Bank2AccNo
        {
            get { return _Bank2AccNo; }
            set { _Bank2AccNo = value; }
        }

        string _Bank2Curr;
        public string Bank2Curr
        {
            get { return _Bank2Curr; }
            set { _Bank2Curr = value; }
        }

        string _BossName;
        public string BossName
        {
            get { return _BossName; }
            set { _BossName = value; }
        }

        string _ContName;
        public string ContName
        {
            get { return _ContName; }
            set { _ContName = value; }
        }

        string _ACContacName;
        public string ACContacName
        {
            get { return _ACContacName; }
            set { _ACContacName = value; }
        }

        string _Phone1;
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        string _Phone2;
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }

        string _MobPhone;
        public string MobPhone
        {
            get { return _MobPhone; }
            set { _MobPhone = value; }
        }

        string _Fax1;
        public string Fax1
        {
            get { return _Fax1; }
            set { _Fax1 = value; }
        }

        string _Fax2;
        public string Fax2
        {
            get { return _Fax2; }
            set { _Fax2 = value; }
        }

        string _www;
        public string www
        {
            get { return _www; }
            set { _www = value; }
        }

        string _email1;
        public string Email1
        {
            get { return _email1; }
            set { _email1 = value; }
        }

        string _email2;
        public string Email2
        {
            get { return _email2; }
            set { _email2 = value; }
        }

        decimal? _CreditLimit;
        public decimal? CreditLimit
        {
            get { return _CreditLimit; }
            set { _CreditLimit = value; }
        }

        string _Curr;
        public string Curr
        {
            get { return _Curr; }
            set { _Curr = value; }
        }

        Int16? _PayDay;
        public Int16? PayDay
        {
            get { return _PayDay; }
            set { _PayDay = value; }
        }

        Int16? _PayMode;
        public Int16? PayMode
        {
            get { return _PayMode; }
            set { _PayMode = value; }
        }

        Int16? _PayModeDay;
        public Int16? PayModeDay
        {
            get { return _PayModeDay; }
            set { _PayModeDay = value; }
        }

        string _PayType;
        public string PayType
        {
            get { return _PayType; }
            set { _PayType = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        string _AllowSendRes;
        public string AllowSendRes
        {
            get { return _AllowSendRes; }
            set { _AllowSendRes = value; }
        }

        Int16? _SendType;
        public Int16? SendType
        {
            get { return _SendType; }
            set { _SendType = value; }
        }

        string _TAcCode;
        public string TAcCode
        {
            get { return _TAcCode; }
            set { _TAcCode = value; }
        }

        string _CIF;
        public string CIF
        {
            get { return _CIF; }
            set { _CIF = value; }
        }

        decimal? _CapSocial;
        public decimal? CapSocial
        {
            get { return _CapSocial; }
            set { _CapSocial = value; }
        }

        string _CapSocialCur;
        public string CapSocialCur
        {
            get { return _CapSocialCur; }
            set { _CapSocialCur = value; }
        }

        string _ConPas;
        public string ConPas
        {
            get { return _ConPas; }
            set { _ConPas = value; }
        }

        bool? _SendOptDate;
        public bool? SendOptDate
        {
            get { return _SendOptDate; }
            set { _SendOptDate = value; }
        }

    }

    public enum PxmVoucherOption { TourVisio = 0, Paximum = 1 }
    public enum PxmWorkType { All = 0, UsePaximumButton = 1, UsePaximumInline = 2 }

    public class PaximumRecord
    {
        public bool? Pxm_Use { get; set; }
        public string Pxm_AutMainCode { get; set; }
        public string Pxm_APIAddr { get; set; }
        public string Pxm_RedictionUrl { get; set; }
        public PxmWorkType Pxm_WorkType { get; set; }
        public PxmVoucherOption Pxm_VoucherOpt { get; set; } //0: TourVisio , 1:Paximum
        public bool Pxm_UseLimit { get; set; }
        public bool Pxm_UseLimit_UseMarOpt { get; set; }
        public bool Pxm_CancelDeadLine { get; set; }
        public bool Pxm_CancelDeadLine_UseMarOpt { get; set; }
        public bool? Pxm_DoNotShowOwnCntry { get; set; }

        public PaximumRecord()
        {
            this.Pxm_Use = false;
            this.Pxm_VoucherOpt = PxmVoucherOption.TourVisio;
            this.Pxm_WorkType = PxmWorkType.UsePaximumButton;
            this.Pxm_DoNotShowOwnCntry = true;
        }
    }
}
