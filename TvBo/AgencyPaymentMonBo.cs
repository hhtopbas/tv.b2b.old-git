﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    [Serializable()]
    public class AgencyPaymentMonitorFields
    {
        public AgencyPaymentMonitorFields()
        {
            _SelectedRow = false;
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        string _Office;
        public string Office
        {
            get { return _Office; }
            set { _Office = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        string _AgencyName;
        public string AgencyName
        {
            get { return _AgencyName; }
            set { _AgencyName = value; }
        }

        string _AgencyUser;
        public string AgencyUser
        {
            get { return _AgencyUser; }
            set { _AgencyUser = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Leader;
        public string Leader
        {
            get { return _Leader; }
            set { _Leader = value; }
        }

        string _MainAgency;
        public string MainAgency
        {
            get { return _MainAgency; }
            set { _MainAgency = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _Days;
        public Int16? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }

        Int16? _ResStat;
        public Int16? ResStat
        {
            get { return _ResStat; }
            set { _ResStat = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        DateTime? _ResDate;
        public DateTime? ResDate
        {
            get { return _ResDate; }
            set { _ResDate = value; }
        }

        Int16? _Adult;
        public Int16? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16? _Child;
        public Int16? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        decimal? _AgencyCom;
        public decimal? AgencyCom
        {
            get { return _AgencyCom; }
            set { _AgencyCom = value; }
        }

        decimal? _DiscountFromCom;
        public decimal? DiscountFromCom
        {
            get { return _DiscountFromCom; }
            set { _DiscountFromCom = value; }
        }

        decimal? _Discount;
        public decimal? Discount
        {
            get { return _Discount; }
            set { _Discount = value; }
        }

        decimal? _BrokerCom;
        public decimal? BrokerCom
        {
            get { return _BrokerCom; }
            set { _BrokerCom = value; }
        }

        decimal? _UserBonus;
        public decimal? UserBonus
        {
            get { return _UserBonus; }
            set { _UserBonus = value; }
        }

        decimal? _AgencyBonus;
        public decimal? AgencyBonus
        {
            get { return _AgencyBonus; }
            set { _AgencyBonus = value; }
        }

        decimal? _AgencySupDis;
        public decimal? AgencySupDis
        {
            get { return _AgencySupDis; }
            set { _AgencySupDis = value; }
        }

        decimal? _AgencyComSup;
        public decimal? AgencyComSup
        {
            get { return _AgencyComSup; }
            set { _AgencyComSup = value; }
        }

        decimal? _EBAgency;
        public decimal? EBAgency
        {
            get { return _EBAgency; }
            set { _EBAgency = value; }
        }

        decimal? _AgencyPayment;
        public decimal? AgencyPayment
        {
            get { return _AgencyPayment; }
            set { _AgencyPayment = value; }
        }

        decimal? _AgencyPayable;
        public decimal? AgencyPayable
        {
            get { return _AgencyPayable; }
            set { _AgencyPayable = value; }
        }

        decimal? _AgencyPayable2;
        public decimal? AgencyPayable2
        {
            get { return _AgencyPayable2; }
            set { _AgencyPayable2 = value; }
        }

        decimal? _EBPas;
        public decimal? EBPas
        {
            get { return _EBPas; }
            set { _EBPas = value; }
        }

        string _OfficeName;
        public string OfficeName
        {
            get { return _OfficeName; }
            set { _OfficeName = value; }
        }

        string _ResStatStr;
        public string ResStatStr
        {
            get { return _ResStatStr; }
            set { _ResStatStr = value; }
        }

        string _ConfStatStr;
        public string ConfStatStr
        {
            get { return _ConfStatStr; }
            set { _ConfStatStr = value; }
        }

        DateTime? _OptDate;
        public DateTime? OptDate
        {
            get { return _OptDate; }
            set { _OptDate = value; }
        }

        decimal? _Balance;
        public decimal? Balance
        {
            get { return _Balance; }
            set { _Balance = value; }
        }

        bool _SelectedRow;
        public bool SelectedRow
        {
            get { return _SelectedRow; }
            set { _SelectedRow = value; }
        }
    }

    [Serializable()]
    public class AgencyPayList
    {
        public string ResNo { get; set; }
        public decimal? PayAmount { get; set; }
        public string Currency { get; set; }
        public AgencyPayList()
        { 
        }
    }

    [Serializable()]
    public class AgencyPaymentMonitorJournalRef
    {
        public string ResNo { get; set; }
        public string Reference { get; set; }
        public AgencyPaymentMonitorJournalRef()
        {
            this.Reference = "-";
        }
    }

    [Serializable()]
    public class AgencyRisk
    {
        public string MainOffice { get; set; }
        public decimal? CreditLimit { get; set; }
        public decimal? Sale { get; set; }
        public decimal? Payment { get; set; }
        public decimal? Com { get; set; }
        public decimal? Balance { get; set; }
        public int? Adl { get; set; }
        public int? Chd { get; set; }
        public int? Pax { get; set; }
        public decimal? AgencyPayable { get; set; }
        public decimal? AgencySupDis { get; set; }
        public decimal? PasSupDis { get; set; }
        public decimal? EBPas { get; set; }
        public decimal? Risk { get; set; }
        public AgencyRisk()
        {
        }
    }
}
