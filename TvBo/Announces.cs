﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class Announces
    {

        public List<AnnounceRecord> getAllAnnounce(User UserData, ref string errorMsg)
        {
            List<AnnounceRecord> records = new List<AnnounceRecord>();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 42, VersionControl.Equality.gt))
                tsql =
@"
Select A.RecID,A.Active,A.BegDate,A.EndDate,A.ValidTV,A.ValidWeb,A.Market,A.[Message],A.CrtUser,A.CrtDate,A.Agency,A.AgencyGrp
From Announce A (NOLOCK) 
Where A.Market=@Market 
  AND A.Active=1 
  AND A.ValidWeb=1
  AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
  And (isnull(A.Agency,'')='' Or 
	      A.Agency=@Agency Or 
		  Exists(Select RecId From Agency Aof (NOLOCK) Where Aof.Code=@Agency And ((Aof.MainOffice=A.Agency And Aof.AgencyType=1) Or (Aof.Code=A.Agency And Aof.AgencyType=0)) And isnull(A.SendOffice,0)=1) Or
		  Exists(Select RecId From Agency Aof1 (NOLOCK) Where Aof1.Code=@Agency And ((Aof1.MainOffice=A.Agency And Aof1.AgencyType=2) Or (Aof1.Code=A.Agency And Aof1.AgencyType=0)) And isnull(A.SendSubAgen,0)=1) 
	  )
  And (isnull(A.AgencyGrp,'')='' Or 
		  Exists(Select RecID From GrpAgencyDet (NOLOCK) 
			     Where GrpAgencyDet.Groups=A.AgencyGrp 
			       And GrpAgencyDet.Agency=@Agency)
	  )
Order By BegDate Desc
";
            else
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 41, VersionControl.Equality.gt))
                tsql =
@"
Select RecID,Active,BegDate,EndDate,ValidTV,ValidWeb,Market,[Message],CrtUser,CrtDate,Agency,AgencyGrp
From Announce (NOLOCK) 
Where Market=@Market 
  AND Active=1 
  AND ValidWeb=1
  AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
  And (isnull(Agency,'')='' Or Exists(Select RecID From Agency (NOLOCK) Where Agency.Code=Announce.Agency And Announce.Agency=@Agency))
  And (isnull(AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet (NOLOCK) Where GrpAgencyDet.Groups=Announce.AgencyGrp And GrpAgencyDet.Agency=@Agency))
Order By BegDate Desc 
";
            else
                tsql = @"Select RecID, Active, BegDate, EndDate, ValidTV, ValidWeb, Market, [Message], CrtUser, CrtDate
                            From Announce (NOLOCK) 
                            Where 
                                Market=@Market 
                            AND Active=1 
                            AND ValidWeb=1
                            AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
                            Order By BegDate Desc ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", System.Data.DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "UserCode", System.Data.DbType.AnsiString, UserData.UserID);
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 41, VersionControl.Equality.gt))
                    db.AddInParameter(dbCommand, "Agency", System.Data.DbType.AnsiString, UserData.AgencyID);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        AnnounceRecord rec = new AnnounceRecord();
                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.Active = Conversion.getBoolOrNull(R["Active"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        rec.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        rec.ValidTV = Conversion.getBoolOrNull(R["ValidTV"]);
                        rec.ValidWeb = Conversion.getBoolOrNull(R["ValidWeb"]);
                        rec.Market = Conversion.getStrOrNull(R["Market"]);
                        rec.Message = Conversion.getStrOrNull(R["Message"]);
                        rec.CrtUser = Conversion.getStrOrNull(R["CrtUser"]);
                        rec.CrtDate = Conversion.getDateTimeOrNull(R["CrtDate"]);
                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AnnounceForUser> getAnnounce(User UserData, ref string errorMsg)
        {
            List<AnnounceForUser> records = new List<AnnounceForUser>();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 42, VersionControl.Equality.gt))
                tsql =
@"
Select A.RecID, A.Message, (Case When isnull(AR.UserCode, '') = '' Then 0 Else 1 End) AS ARead 
From Announce A (NOLOCK) 
Left Join AnnounceRead AR (NOLOCK) ON AR.AnnounceID = A.RecID And AR.UserCode = @UserCode
Where A.Market = @Market
    AND A.Active = 1 
    AND A.ValidWeb = 1 
    AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
    AND not Exists(Select * From AnnounceRead Where AnnounceID = A.RecID AND UserCode = @UserCode and UserType = 'WEB')
    And (isnull(A.Agency,'')='' Or 
		    A.Agency=@Agency Or 
		    Exists(Select RecId From Agency Aof (NOLOCK) Where Aof.Code=@Agency And ((Aof.MainOffice=A.Agency And Aof.AgencyType=1) Or (Aof.Code=A.Agency And Aof.AgencyType=0)) And isnull(A.SendOffice,0)=1) Or
		    Exists(Select RecId From Agency Aof1 (NOLOCK) Where Aof1.Code=@Agency And ((Aof1.MainOffice=A.Agency And Aof1.AgencyType=2) Or (Aof1.Code=A.Agency And Aof1.AgencyType=0)) And isnull(A.SendSubAgen,0)=1) 
	    )
    And (isnull(AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet (NOLOCK) Where GrpAgencyDet.Groups=A.AgencyGrp And GrpAgencyDet.Agency=@Agency))
Order By BegDate Desc 
";
            else
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 41, VersionControl.Equality.gt))
                tsql = @"
Select A.RecID, A.Message, (Case When isnull(AR.UserCode, '') = '' Then 0 Else 1 End) AS ARead 
From Announce A (NOLOCK) 
Left Join AnnounceRead AR (NOLOCK) ON AR.AnnounceID = A.RecID And AR.UserCode = @UserCode
Where A.Market = @Market
  AND A.Active = 1 
  AND A.ValidWeb = 1 
  AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
  AND not Exists(Select * From AnnounceRead Where AnnounceID = A.RecID AND UserCode = @UserCode and UserType = 'WEB')
  And (isnull(Agency,'')='' Or Exists(Select RecID From Agency (NOLOCK) Where Agency.Code=A.Agency And A.Agency=@Agency))
  And (isnull(AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet (NOLOCK) Where GrpAgencyDet.Groups=A.AgencyGrp And GrpAgencyDet.Agency=@Agency))
Order By BegDate Desc 
";
            else
                tsql = @"
Select A.RecID, A.Message, (Case When isnull(AR.UserCode, '') = '' Then 0 Else 1 End) AS ARead 
From Announce A (NOLOCK) 
Left Join AnnounceRead AR (NOLOCK) ON AR.AnnounceID = A.RecID And AR.UserCode = @UserCode
Where A.Market = @Market
  AND A.Active = 1 
  AND A.ValidWeb = 1 
  AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
  AND not Exists(Select * From AnnounceRead Where AnnounceID = A.RecID AND UserCode = @UserCode and UserType = 'WEB')                         
Order By BegDate Desc 
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", System.Data.DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "UserCode", System.Data.DbType.String, UserData.UserID);
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 41, VersionControl.Equality.gt))
                    db.AddInParameter(dbCommand, "Agency", System.Data.DbType.AnsiString, UserData.AgencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        AnnounceForUser rec = new AnnounceForUser();
                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.Message = Conversion.getStrOrNull(R["Message"]);
                        rec.Read = Conversion.getBoolOrNull(R["ARead"]);
                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public DataTable getAnnounce(User UserData)
        {
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 42, VersionControl.Equality.gt))
                tsql =
@"
Select A.RecID, A.Message, (Case When isnull(AR.UserCode, '') = '' Then 0 Else 1 End) AS ARead 
From Announce A (NOLOCK) 
Left Join AnnounceRead AR (NOLOCK) ON AR.AnnounceID = A.RecID And AR.UserCode = @UserCode
Where 
    A.Market = @Market
    AND A.Active = 1 
    AND A.ValidWeb = 1 
    AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
    AND not Exists(Select * From AnnounceRead Where AnnounceID = A.RecID AND UserCode = @UserCode and UserType = 'WEB')
    And (isnull(A.Agency,'')='' Or 
		    A.Agency=@Agency Or 
		    Exists(Select RecId From Agency Aof (NOLOCK) Where Aof.Code=@Agency And ((Aof.MainOffice=A.Agency And Aof.AgencyType=1) Or (Aof.Code=A.Agency And Aof.AgencyType=0)) And isnull(A.SendOffice,0)=1) Or
		    Exists(Select RecId From Agency Aof1 (NOLOCK) Where Aof1.Code=@Agency And ((Aof1.MainOffice=A.Agency And Aof1.AgencyType=2) Or (Aof1.Code=A.Agency And Aof1.AgencyType=0)) And isnull(A.SendSubAgen,0)=1) 
	    )
    And (isnull(AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet (NOLOCK) Where GrpAgencyDet.Groups=A.AgencyGrp And GrpAgencyDet.Agency=@Agency))
Order By BegDate Desc
";
            else
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 41, VersionControl.Equality.gt))
                tsql = @"
Select A.RecID, A.Message, (Case When isnull(AR.UserCode, '') = '' Then 0 Else 1 End) AS ARead 
From Announce A (NOLOCK) 
Left Join AnnounceRead AR (NOLOCK) ON AR.AnnounceID = A.RecID And AR.UserCode = @UserCode
Where A.Market = @Market
  AND A.Active = 1 
  AND A.ValidWeb = 1 
  AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
  AND not Exists(Select * From AnnounceRead Where AnnounceID = A.RecID AND UserCode = @UserCode and UserType = 'WEB')
  And (isnull(A.Agency,'')='' Or Exists(Select RecID From Agency (NOLOCK) Where Agency.Code=A.Agency And A.Agency=@Agency))
  And (isnull(A.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet (NOLOCK) Where GrpAgencyDet.Groups=A.AgencyGrp And GrpAgencyDet.Agency=@Agency))
Order By BegDate Desc";
            else
                tsql = @"
Select A.RecID, A.Message, (Case When isnull(AR.UserCode, '') = '' Then 0 Else 1 End) AS ARead 
From Announce A (NOLOCK) 
Left Join AnnounceRead AR (NOLOCK) ON AR.AnnounceID = A.RecID And AR.UserCode = @UserCode
Where A.Market = @Market
  AND A.Active = 1 
  AND A.ValidWeb = 1 
  AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
  AND not Exists(Select * From AnnounceRead Where AnnounceID = A.RecID AND UserCode = @UserCode and UserType = 'WEB')
Order By BegDate Desc";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "UserCode", DbType.String, UserData.UserID);
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 41, VersionControl.Equality.gt))
                    db.AddInParameter(dbCommand, "Agency", System.Data.DbType.AnsiString, UserData.AgencyID);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch {
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public DataTable getAllAnnounce(User UserData)
        {
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 42, VersionControl.Equality.gt))
                tsql =
@"Select RecID, Message 
From Announce (NOLOCK) 
Where Market = @Market 
    AND Active = 1 
    AND ValidWeb = 1
    AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
    And (isnull(A.Agency,'')='' Or 
		    A.Agency=@Agency Or 
		    Exists(Select RecId From Agency Aof (NOLOCK) Where Aof.Code=@Agency And ((Aof.MainOffice=Announce.Agency And Aof.AgencyType=1) Or (Aof.Code=Announce.Agency And Aof.AgencyType=0)) And isnull(Announce.SendOffice,0)=1) Or
		    Exists(Select RecId From Agency Aof1 (NOLOCK) Where Aof1.Code=@Agency And ((Aof1.MainOffice=Announce.Agency And Aof1.AgencyType=2) Or (Aof1.Code=Announce.Agency And Aof1.AgencyType=0)) And isnull(Announce.SendSubAgen,0)=1) 
	    )
    And (isnull(AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet (NOLOCK) Where GrpAgencyDet.Groups=Announce.AgencyGrp And GrpAgencyDet.Agency=@Agency))
Order By BegDate Desc ";
            else
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 41, VersionControl.Equality.gt))
                tsql = @"
Select RecID, Message 
From Announce (NOLOCK) 
Where Market = @Market 
  AND Active = 1 
  AND ValidWeb = 1
  AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
  And (isnull(Agency,'')='' Or Exists(Select RecID From Agency (NOLOCK) Where Agency.Code=Announce.Agency And Announce.Agency=@Agency))
  And (isnull(AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet (NOLOCK) Where GrpAgencyDet.Groups=Announce.AgencyGrp And GrpAgencyDet.Agency=@Agency))
Order By BegDate Desc ";
            else
                tsql = @"
Select RecID, Message 
From Announce (NOLOCK) 
Where Market = @Market 
  AND Active = 1 
  AND ValidWeb = 1
  AND GetDate() between A.BegDate And isnull(A.EndDate,GetDate()+99)
Order By BegDate Desc  ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "UserCode", DbType.String, UserData.UserID);
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 41, VersionControl.Equality.gt))
                    db.AddInParameter(dbCommand, "Agency", System.Data.DbType.AnsiString, UserData.AgencyID);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch {
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool readAnnounce(User UserData, int? AnnounceID, ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string tsql = @"
Insert Into AnnounceRead (AnnounceID, UserCode, UserType, ReadDate) 
Values(@AnnounceID, @UserCode, 'WEB', GetDate()) 
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try {
                db.AddInParameter(dbCommand, "UserCode", DbType.String, UserData.UserID);
                db.AddInParameter(dbCommand, "AnnounceID", DbType.Int32, AnnounceID);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return false;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
