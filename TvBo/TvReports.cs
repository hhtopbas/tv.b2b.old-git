﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using TourVisio.WebService.Adapter.Enums;
using TourVisio.Library.Document.Models;
using TvTools;

namespace TvBo
{
    public class TvReports
    {
        public string getReportStatus(tvReportPDFRecord report)
        {
            string errorMsg = string.Empty;
            if (report != null)
            {
                Int16 status = report.Status.HasValue ? report.Status.Value : (Int16)(-1);
                switch (status)
                {
                    case 0:
                        if (report.RptImage == null)
                            errorMsg = HttpContext.GetGlobalResourceObject("Reports", "lblPrintError0").ToString();// "Unable to create document. Please contact your Operator.(0)";
                        else
                            errorMsg = string.Empty;
                        break;
                    case 1:
                        errorMsg = string.Empty;
                        break;
                    case 2:
                        errorMsg = "Reservation not found.";
                        break;
                    case 3:
                        errorMsg = HttpContext.GetGlobalResourceObject("Reports", "lblPrintError999").ToString() + "(3)";
                        break;
                    case 4:
                        errorMsg = "Your document request is timed out. Please try again.";
                        break;
                    case 5:
                        errorMsg = "Reservation no paid.";
                        break;
                    case 8:
                        errorMsg = "Timeout.";
                        break;
                    case 10:
                        errorMsg = "No amount for the invoice.";
                        break;
                    default:
                        errorMsg = HttpContext.GetGlobalResourceObject("Reports", "lblPrintError999").ToString() + (string.IsNullOrEmpty(errorMsg) ? "" : "(" + errorMsg + ")");
                        break;
                }
            }
            else
                errorMsg = HttpContext.GetGlobalResourceObject("Reports", "lblPrintError999").ToString() + "(" + errorMsg + ")";
            return errorMsg;
        }

        public string getReportV2PDF(User UserData, ResDataRecord ResData, string DocName, string tempFolder, int? langCode, Int16? DocOutputType, int? FormID, string param1, string docFolder, bool isExternal, ref string errorMsg)
        {
            bool? documentGenerationModeIsApplication = TvTools.Conversion.getBoolOrNull(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationModeIsApplication"]);
            TourVisio.Library.Document.Models.Enums.enmDocumentGenerationMode documentGenerationMode =
                documentGenerationModeIsApplication.HasValue && documentGenerationModeIsApplication.Value ?
                TourVisio.Library.Document.Models.Enums.enmDocumentGenerationMode.Application :
                TourVisio.Library.Document.Models.Enums.enmDocumentGenerationMode.Procedure;

            TourVisio.WebService.Adapter.ServiceModels.ContentModels.GetDocumentFileResponse rptNew = null;
            TourVisio.Library.Document.Models.Enums.enmDocumentFileType fileType = TourVisio.Library.Document.Models.Enums.enmDocumentFileType.Pdf;
            if (DocOutputType.HasValue)
                fileType = (TourVisio.Library.Document.Models.Enums.enmDocumentFileType)DocOutputType.Value;
            bool Pdf3 = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex);
            string fileName = ResData.ResMain.ResNo + "_" + System.Guid.NewGuid() + ".html";
            Int16 DocType = 1;
            string param = string.Empty;
            List<tvReportPDFRecord> reportList = new List<tvReportPDFRecord>();
            string strCulture = string.Empty;

            if(langCode!=null)
            strCulture = new System.Globalization.CultureInfo(langCode.Value).Name;
            if (string.Equals(DocName, "AgencyConfirmation") || string.Equals(DocName, "ClientConfirmation"))
            {
                DocType = 6;
                //if (langCode != null)
                //    param = string.Format(" /LI:{0}", langCode);
                tvReportPDFRecord rpt = null;
                if (Pdf3)

                    rpt = generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, null, param, ref errorMsg);
                else
                {
                    rptNew = new TourVisio.Library.Document.Document(
                    new TourVisio.Library.Document.Models.GetDocumentFileRequest
                    {
                        Agency = UserData.AgencyID,
                        AgencyUser = UserData.UserID,
                        ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"]) ? "TvDataBase" : System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                        DocumentType = enmDocumentType.AgencyConfirmation,// GetDocumentTypeById(DocType),
                        EncryptedReservationNumber = Guid.Empty,
                        DocumentGenerationMode = documentGenerationMode,
                        ReservationNumber = ResData.ResMain.ResNo,
                        DocumentId = FormID,
                        Culture = strCulture
                    }).GetFile();
                    if (rptNew != null)
                        rpt = new tvReportPDFRecord { RptImage = rptNew.Data, Status = (short)rptNew.Status };
                    //rpt = generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, FormID, DocOutputType, param, ref errorMsg);
                }


                if (!string.IsNullOrEmpty(errorMsg))
                    return errorMsg;
                string returnErr = getReportStatus(rpt);
                if (!string.IsNullOrEmpty(returnErr))
                {
                    errorMsg = returnErr;
                    return string.Empty;
                }
                reportList.Add(rpt);
            }
            else if (string.Equals(DocName, "Contract"))
            {
                DocType = 5;
                //if (langCode != null)
                //    param = string.Format(" /LI:{0}", langCode);
                tvReportPDFRecord rpt = null;
                if (Pdf3)
                    rpt = generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, null, param, ref errorMsg);
                else
                {
                    rptNew = new TourVisio.Library.Document.Document(
                    new TourVisio.Library.Document.Models.GetDocumentFileRequest
                    {
                        Agency = UserData.AgencyID,
                        AgencyUser = UserData.UserID,
                        ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"])? "TvDataBase": System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                        DocumentType = GetDocumentTypeById(DocType),
                        EncryptedReservationNumber = Guid.Empty,
                        DocumentGenerationMode = documentGenerationMode,
                        ReservationNumber = ResData.ResMain.ResNo,
                        DocumentId = FormID,
                        Culture=strCulture,
                        FileType= fileType
                    }).GetFile();
                    if (rptNew != null)
                        rpt = new tvReportPDFRecord { RptImage = rptNew.Data, Status = (short)rptNew.Status };
                    //generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, FormID, DocOutputType, param, ref errorMsg);
                }


                if (!string.IsNullOrEmpty(errorMsg))
                    return errorMsg;
                string returnErr = getReportStatus(rpt);
                if (!string.IsNullOrEmpty(returnErr))
                {
                    errorMsg = returnErr;
                    return string.Empty;
                }
                if (DocOutputType == 1 && rpt.RptImage != null)
                {
                    rpt.RptImage = addContractHeader(rpt.RptImage, docFolder, WebRoot.BasePageRoot + "ACE/", fileName);
                }
                reportList.Add(rpt);
            }
            else if (string.Equals(DocName, "FlyTicket"))
            {
                DocType = 4;
                //if (langCode != null)
                //    param = string.Format(" /LI:{0}", langCode);
                //param += " /AL:";
                List<string> partialReportList = new List<string>();
                var flights = from q in ResData.ResService
                              where string.Equals(q.ServiceType, "FLIGHT")
                              group q by new { q.Service, q.DepLocation, q.ArrLocation, q.BegDate, q.EndDate } into k
                              select new { k.Key.Service, k.Key.DepLocation, k.Key.ArrLocation, k.Key.BegDate, k.Key.EndDate };
                List<FlightDayRecord> flgDetail = new List<FlightDayRecord>();
                foreach (var row in flights)
                {
                    FlightDayRecord flg = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    if (flg != null)
                        flgDetail.Add(flg);
                }
                var flightList = from q in ResData.ResService
                                 join q2 in ResData.ResCon on q.RecID equals q2.ServiceID
                                 join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                                 //join q4 in flgDetail on q.Service equals q4.FlightNo
                                 where string.Equals(q.ServiceType, "FLIGHT") && q.StatSer < 2 && q3.Status == 0
                                 group q by new { q.Supplier,  q2.CustNo } into k
                                 select new { k.Key.Supplier,  k.Key.CustNo,ServiceID=(int)k.Min(m=>m.RecID) };
                if (flightList != null && flightList.Count() > 0)
                    foreach (var row in flightList)
                    {
                        tvReportPDFRecord rpt = null;
                        if (Pdf3)
                            rpt = generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, row.Supplier, string.Empty, null, row.CustNo, null, param, ref errorMsg);
                        else
                        {
                            rptNew = new TourVisio.Library.Document.Document(
                                new TourVisio.Library.Document.Models.GetDocumentFileRequest
                                {
                                    Agency = UserData.AgencyID,
                                    AgencyUser = UserData.UserID,
                                    ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"])? "TvDataBase": System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                                    DocumentType = GetDocumentTypeById(DocType),
                                    EncryptedReservationNumber = Guid.Empty,
                                    DocumentGenerationMode = documentGenerationMode,
                                    ServiceId = row. ServiceID.ToString(),
                                    TravellerId = row.CustNo,
                                    ReservationNumber = ResData.ResMain.ResNo,
                                    DocumentId = FormID,
                                    Culture=strCulture,
                                    FileType = fileType

                                }).GetFile();
                            //rpt = generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, row.Supplier, string.Empty, null, row.CustNo, FormID, DocOutputType, param + row.Airline, ref errorMsg);
                            if (rptNew != null)
                                rpt = new tvReportPDFRecord { RptImage = rptNew.Data, Status = (short)rptNew.Status };
                        }

                        if (!string.IsNullOrEmpty(errorMsg))
                            return errorMsg;
                        string returnErr = getReportStatus(rpt);
                        if (!string.IsNullOrEmpty(returnErr) && rpt.RptImage==null)
                        {
                            errorMsg = returnErr;
                            return string.Empty;
                        }
                        reportList.Add(rpt);
                    }
            }
            else if (string.Equals(DocName, "Invoice"))
            {
                DocType = 1;
                if (langCode != null)
                    param = string.Format(" /LI:{0}", langCode);
                param += " /01:0";
                tvReportPDFRecord rpt = null;
                if (Pdf3)
                    rpt = generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, null, param, ref errorMsg);
                else
                {
                    rptNew = new TourVisio.Library.Document.Document(
                                new TourVisio.Library.Document.Models.GetDocumentFileRequest
                                {
                                    Agency = UserData.AgencyID,
                                    AgencyUser = UserData.UserID,
                                    ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"])? "TvDataBase": System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                                    DocumentType = GetDocumentTypeById(DocType),
                                    EncryptedReservationNumber = Guid.Empty,
                                    DocumentGenerationMode = documentGenerationMode,
                                    //ServiceId = ServiceID.HasValue ? ServiceID.Value.ToString() : null,
                                    ReservationNumber = ResData.ResMain.ResNo,
                                    DocumentId = FormID,
                                    Culture=strCulture,
                                    FileType = fileType
                                }).GetFile();
                    if (rptNew != null)
                        rpt = new tvReportPDFRecord { RptImage = rptNew.Data, Status = (short)rptNew.Status };
                    //rpt = generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, FormID, DocOutputType, param, ref errorMsg);
                }

                if (!string.IsNullOrEmpty(errorMsg))
                    return errorMsg;
                string returnErr = getReportStatus(rpt);
                if (!string.IsNullOrEmpty(returnErr))
                {
                    errorMsg = returnErr;
                    return string.Empty;
                }
                reportList.Add(rpt);
            }
            else if (string.Equals(DocName, "Proforma"))
            {

                DocType = 1;
                if (langCode != null)
                    param = string.Format(" /LI:{0}", langCode);
                param += " /01:1";
                tvReportPDFRecord rpt = Pdf3 ?
                                           generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, null, param, ref errorMsg) :
                                           generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, FormID, DocOutputType, param, ref errorMsg);
                if (!string.IsNullOrEmpty(errorMsg))
                    return errorMsg;
                string returnErr = getReportStatus(rpt);
                if (!string.IsNullOrEmpty(returnErr))
                {
                    errorMsg = returnErr;
                    return string.Empty;
                }
                reportList.Add(rpt);
                //}
            }
            else if (string.Equals(DocName, "Voucher"))
            {
                DocType = 2;
                if (langCode != null)
                    param = string.Format(" /LI:{0}", langCode);
                tvReportPDFRecord rpt = null;
                if (Pdf3)
                    rpt =
                                           generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, null, param, ref errorMsg);
                else
                {
                    rptNew = new TourVisio.Library.Document.Document(
                                new TourVisio.Library.Document.Models.GetDocumentFileRequest
                                {
                                    Agency = UserData.AgencyID,
                                    AgencyUser = UserData.UserID,
                                    ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"])? "TvDataBase": System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                                    DocumentType = GetDocumentTypeById(DocType),
                                    EncryptedReservationNumber = Guid.Empty,
                                    DocumentGenerationMode = documentGenerationMode,
                                    //ServiceId = ServiceID.HasValue ? ServiceID.Value.ToString() : null,
                                    ReservationNumber = ResData.ResMain.ResNo,
                                    DocumentId = FormID,
                                    Culture=strCulture,
                                    FileType = fileType
                                }).GetFile();
                    if (rptNew != null)
                        rpt = new tvReportPDFRecord { RptImage = rptNew.Data, Status = (short)rptNew.Status };
                    //rpt = generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, FormID, DocOutputType, param, ref errorMsg);
                }
                                           
                if (!string.IsNullOrEmpty(errorMsg))
                    return errorMsg;
                string returnErr = getReportStatus(rpt);
                if (!string.IsNullOrEmpty(returnErr))
                {
                    errorMsg = returnErr;
                    return string.Empty;
                }
                reportList.Add(rpt);
                //}
            }
            else if (string.Equals(DocName, "Excursion"))
            {
                DocType = 7;
                if (langCode != null)
                    param = string.Format(" /LI:{0}", langCode);
                tvReportPDFRecord rpt = null;
                if (Pdf3)
                    rpt = generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, null, param, ref errorMsg);
                else
                {
                    rptNew = new TourVisio.Library.Document.Document(
                               new TourVisio.Library.Document.Models.GetDocumentFileRequest
                               {
                                   Agency = UserData.AgencyID,
                                   AgencyUser = UserData.UserID,
                                   ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"])? "TvDataBase": System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                                   DocumentType = GetDocumentTypeById(DocType),
                                   EncryptedReservationNumber = Guid.Empty,
                                   DocumentGenerationMode = documentGenerationMode,
                                    //ServiceId = ServiceID.HasValue ? ServiceID.Value.ToString() : null,
                                    ReservationNumber = ResData.ResMain.ResNo,
                                   DocumentId = FormID,
                                   Culture=strCulture,
                                   FileType = fileType
                               }).GetFile();
                    if (rptNew != null)
                        rpt = new tvReportPDFRecord { RptImage = rptNew.Data, Status = (short)rptNew.Status };
                    //rpt=generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, FormID, DocOutputType, param, ref errorMsg);
                }

                if (!string.IsNullOrEmpty(errorMsg))
                    return errorMsg;
                string returnErr = getReportStatus(rpt);
                if (!string.IsNullOrEmpty(returnErr))
                {
                    errorMsg = returnErr;
                    return string.Empty;
                }
                reportList.Add(rpt);
            }
            else if (string.Equals(DocName, "Transfer"))
            {
                DocType = 8;
                if (langCode != null)
                    param = string.Format(" /LI:{0}", langCode);
                tvReportPDFRecord rpt = null;
                if (Pdf3)
                    rpt = generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, null, param, ref errorMsg);
                else
                {
                    rptNew = new TourVisio.Library.Document.Document(
                               new TourVisio.Library.Document.Models.GetDocumentFileRequest
                               {
                                   Agency = UserData.AgencyID,
                                   AgencyUser = UserData.UserID,
                                   ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"])? "TvDataBase": System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                                   DocumentType = GetDocumentTypeById(DocType),
                                   EncryptedReservationNumber = Guid.Empty,
                                   DocumentGenerationMode = documentGenerationMode,
                                   //ServiceId = ServiceID.HasValue ? ServiceID.Value.ToString() : null,
                                   ReservationNumber = ResData.ResMain.ResNo,
                                   DocumentId = FormID,
                                   Culture=strCulture,
                                   FileType = fileType
                               }).GetFile();
                    if (rptNew != null)
                        rpt = new tvReportPDFRecord { RptImage = rptNew.Data, Status = (short)rptNew.Status };
                    //rpt= generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, FormID, DocOutputType, param, ref errorMsg);
                }
                                           
                if (!string.IsNullOrEmpty(errorMsg))
                    return errorMsg;
                string returnErr = getReportStatus(rpt);
                if (!string.IsNullOrEmpty(returnErr))
                {
                    errorMsg = returnErr;
                    return string.Empty;
                }
                reportList.Add(rpt);
            }
            else if (string.Equals(DocName, "Insurance"))
            {
                DocType = 3;
                if (langCode != null)
                    param = string.Format(" /LI:{0}", langCode);
                tvReportPDFRecord rpt = null;
                if (Pdf3)
                    rpt = null; 
                //rpt = generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, row.Supplier, row.Service, null, null, null, param, ref errorMsg);
                else
                {
                    rptNew = new TourVisio.Library.Document.Document(
                              new TourVisio.Library.Document.Models.GetDocumentFileRequest
                              {
                                  Agency = UserData.AgencyID,
                                  AgencyUser = UserData.UserID,
                                  ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"])? "TvDataBase": System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                                  DocumentType = GetDocumentTypeById(DocType),
                                  EncryptedReservationNumber = Guid.Empty,
                                  DocumentGenerationMode = documentGenerationMode,
                                  //ServiceId = ServiceID.HasValue ? ServiceID.Value.ToString() : null,
                                  ReservationNumber = ResData.ResMain.ResNo,
                                  DocumentId = FormID,
                                  Culture=strCulture,
                                  FileType = fileType
                              }).GetFile();
                    if (rptNew != null)
                        rpt = new tvReportPDFRecord { RptImage = rptNew.Data, Status = (short)rptNew.Status };
                    //rpt = generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, row.Supplier, row.Service, null, null, FormID, DocOutputType, param, ref errorMsg);
                }

                //List<string> partialReportList = new List<string>();
                //var insuranceList = from q in ResData.ResService
                //                    where string.Equals(q.ServiceType, "INSURANCE") && q.StatSer < 2
                //                    group q by new { q.Supplier, q.Service } into k
                //                    select new { k.Key.Supplier, k.Key.Service };

                //if (insuranceList != null && insuranceList.Count() > 0)
                //    foreach (var row in insuranceList)
                //    {
                //        tvReportPDFRecord rpt = Pdf3 ?
                //                                   generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, row.Supplier, row.Service, null, null, null, param, ref errorMsg) :
                //                                   generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, row.Supplier, row.Service, null, null, FormID, DocOutputType, param, ref errorMsg);
                //        if (!string.IsNullOrEmpty(errorMsg))
                //            return errorMsg;
                //        string returnErr = getReportStatus(rpt);
                //        if (!string.IsNullOrEmpty(returnErr))
                //        {
                //            errorMsg = returnErr;
                //            return string.Empty;
                //        }
                //        reportList.Add(rpt);
                //    }
                if (!string.IsNullOrEmpty(errorMsg))
                    return errorMsg;
                string returnErr = getReportStatus(rpt);
                if (!string.IsNullOrEmpty(returnErr))
                {
                    errorMsg = returnErr;
                    return string.Empty;
                }
                reportList.Add(rpt);
            }
            else if (string.Equals(DocName, "TicketPriceRefound"))
            {
                DocType = 99;
                if (langCode != null)
                    param = string.Format(" /LI:{0}", langCode);
                List<string> partialReportList = new List<string>();
                var flightList = from q in ResData.ResService
                                 join q2 in ResData.ResCon on q.RecID equals q2.ServiceID
                                 join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                                 where string.Equals(q.ServiceType, "FLIGHT") && q.StatSer < 2 && q3.Status == 0
                                 group q by new { q2.CustNo } into k
                                 select new { k.Key.CustNo };
                if (flightList != null && flightList.Count() > 0)
                    foreach (var row in flightList)
                    {
                        tvReportPDFRecord rpt = Pdf3 ? generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, row.CustNo, null, param, ref errorMsg)
                                                     : generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, row.CustNo, FormID, DocOutputType, param, ref errorMsg);
                        if (!string.IsNullOrEmpty(errorMsg))
                            return errorMsg;
                        string returnErr = getReportStatus(rpt);
                        if (!string.IsNullOrEmpty(returnErr))
                        {
                            errorMsg = returnErr;
                            return string.Empty;
                        }
                        reportList.Add(rpt);
                    }
            }
            else if (string.Equals(DocName, "Receipt"))
            {
                int? JournallID = !string.IsNullOrEmpty(param1) ? Conversion.getInt32OrNull(param1) : null;
                DocType = 9;
                if (langCode != null)
                    param = string.Format(" /LI:{0}", langCode);
                param += string.Format(" /JI:{0}", JournallID);
                List<string> partialReportList = new List<string>();

                tvReportPDFRecord rpt = null;
                if (Pdf3)
                    rpt = generateReportV2PDF3(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, null, param, ref errorMsg);
                else
                {
                    rptNew = new TourVisio.Library.Document.Document(
                              new TourVisio.Library.Document.Models.GetDocumentFileRequest
                              {
                                  Agency = UserData.AgencyID,
                                  AgencyUser = UserData.UserID,
                                  ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"])? "TvDataBase": System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                                  DocumentType = enmDocumentType.Receipt,//  GetDocumentTypeById(DocType),
                                  EncryptedReservationNumber = Guid.Empty,
                                  DocumentGenerationMode = documentGenerationMode,
                                  //ServiceId = ServiceID.HasValue ? ServiceID.Value.ToString() : null,
                                  ReservationNumber = ResData.ResMain.ResNo,
                                  JournalID=JournallID,
                                  DocumentId = FormID,
                                  Culture=strCulture,
                                  FileType = fileType
                              }).GetFile();
                    if (rptNew != null)
                        rpt = new tvReportPDFRecord { RptImage = rptNew.Data, Status = (short)rptNew.Status };
                    //rpt = generateReportV2PDF(UserData.AgencyID, UserData.UserID, ResData.ResMain.ResNo, DocType, string.Empty, string.Empty, null, null, FormID, DocOutputType, param, ref errorMsg);
                }
                                             
                if (!string.IsNullOrEmpty(errorMsg))
                    return errorMsg;
                string returnErr = getReportStatus(rpt);
                if (!string.IsNullOrEmpty(returnErr))
                {
                    errorMsg = returnErr;
                    return string.Empty;
                }
                reportList.Add(rpt);

            }

            if (reportList != null && reportList.Count > 0)
            {
                string retVal = string.Empty;
                if (DocOutputType == 1)
                {
                    retVal = saveHtmlFile(UserData, tempFolder, fileName, reportList);
                }
                else
                    retVal = mergeReportFiles(UserData, tempFolder, ResData.ResMain.ResNo, reportList);
                return retVal;
            }
            else
                return string.Empty;
        }

        public object addContractHeader(object htmlContentBytes, string docFolder, string tempFolder, string fileName)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EmulateIE7\" />");
            sb.AppendLine("<script src=\"" + WebRoot.BasePageRoot + "Scripts/jquery.min.js\" type=\"text/javascript\"></script>");
            sb.AppendLine("<script src=\"" + WebRoot.BasePageRoot + "Scripts/jquery.json.js\" type=\"text/javascript\"></script>");
            sb.AppendLine("<script src=\"" + WebRoot.BasePageRoot + "Scripts/jquery.query-2.1.7.js\" type=\"text/javascript\"></script>");
            sb.AppendLine("<script src=\"" + WebRoot.BasePageRoot + "Scripts/jquery.url.js\" type=\"text/javascript\"></script>");
            sb.AppendLine("<script src=\"" + WebRoot.BasePageRoot + "Scripts/jquery.printPage.js\" type=\"text/javascript\"></script>");
            sb.AppendLine("<script type=\"text/javascript\">");

            sb.AppendLine("    function logout() {");
            sb.AppendLine("        self.parent.logout();");
            sb.AppendLine("    }");
            //$("input").parent().css("z-index",10);
            sb.AppendLine("    $(document).ready(function () {");
            sb.AppendLine("        $('#page1').css('overflow', 'visible');");
            sb.AppendLine("        $('input').parent().css('z-index', 10);");
            sb.AppendLine("        $('input:checkbox').click(function () {");
            sb.AppendLine("            if ($(this).attr(\"checked\") != \"checked\") {");
            sb.AppendLine("                $(this).removeAttr(\"checked\");");
            sb.AppendLine("            }");
            sb.AppendLine("            else {");
            sb.AppendLine("                $(this).attr(\"checked\", \"checked\");");
            sb.AppendLine("            }");
            sb.AppendLine("        });");

            sb.AppendLine("        $.query = $.query.load(location.href);");
            sb.AppendLine("        var resNo = $.query.get('ResNo');");
            sb.AppendLine("        var docName = $.query.get('docName');");

            sb.AppendLine("        if ($.query.get('print')) {");
            sb.AppendLine("            $('input:checkbox').removeAttr('checked');");
            sb.AppendLine("            $.each($.query.keys, function (i) {");
            sb.AppendLine("                if (i != 'ResNo' && i != 'docName' && i != 'print' && i != '__VIEWSTATE') {");
            sb.AppendLine("                    var elm = $(\"input[name='\"+i+\"']\");");
            sb.AppendLine("                    if (this == 'on' || this == 'off') {");
            sb.AppendLine("                        if (this == 'on')");
            sb.AppendLine("                            elm.attr('checked', 'checked');");
            sb.AppendLine("                        else");
            sb.AppendLine("                            elm.removeAttr('checked');");
            sb.AppendLine("                    }");
            sb.AppendLine("                    else if (!(this == true || this == false)) {");
            sb.AppendLine("                        elm.replaceWith(\"<span>\" + this + \"</span>\");");
            sb.AppendLine("                    }");
            sb.AppendLine("                }");
            sb.AppendLine("            });");
            sb.AppendLine("            window.print();");
            sb.AppendLine("            window.close();");
            sb.AppendLine("        }");
            sb.AppendLine("        else {");

            sb.AppendLine("            self.parent.setViewReportSrc(location.href);");
            sb.AppendLine("            try {");
            sb.AppendLine("                self.parent.viewReportRemoveButton(docName);");
            sb.AppendLine("            }");
            sb.AppendLine("            catch (err) {");
            sb.AppendLine("            }");
            sb.AppendLine("            var baseUrl = $.url(location.href);");
            sb.AppendLine("            try {");
            sb.AppendFormat("                self.parent.viewReportRemoveButton('{0}');", HttpContext.GetGlobalResourceObject("LibraryResource", "btnPrintNext"));
            sb.AppendLine("            }");
            sb.AppendLine("            catch (err) {");
            sb.AppendLine("            }");
            sb.AppendFormat("      self.parent.viewReportAddButton('{0}', function () ", HttpContext.GetGlobalResourceObject("LibraryResource", "btnPrintNext"));
            sb.AppendLine("            {");
            sb.AppendLine("               window.location = '" + docFolder + "' + 'AgreementNext.aspx?ResNo=' + resNo + '&docName=' + docName + '&next=" + tempFolder + fileName + "'");
            sb.AppendLine("            });");
            sb.AppendLine("        }");
            sb.AppendLine("    });");
            sb.AppendLine("</script>");
            sb.AppendLine("<style>#page1,#page2,#page3{page-break-after: always;}</style>");
            string htmlContent = Conversion.GetString((byte[])htmlContentBytes);

            htmlContent = htmlContent.Insert(htmlContent.IndexOf("</head>"), sb.ToString());
            htmlContent = htmlContent.Replace("&quot;", "\"");
            //htmlContent = htmlContent.Replace("\"\"", "\"");
            htmlContent = htmlContent.Replace('◘', '<').Replace('○', '>');
            htmlContent = htmlContent.Replace("class=\"page\"", "");
            return (object)Conversion.GetBytes(htmlContent);
        }

        public tvReportPDFRecord generateReportV2PDF(string AgencyID, string UserID, string ResNo, Int16 DocType, string Supplier, string Service, int? ServiceID, int? CustNo, int? FormID, Int16? docOutputType, string param, ref string errorMsg)
        {
            string tsql = string.Empty;
            //return generateReportV3(AgencyID,UserID,ResNo,DocType,Supplier,Service,ServiceID,CustNo,FormID,docOutputType,param, ref errorMsg);


            List<dbInformationShema> spShemaV2 = VersionControl.getSpShema(@"Get_Document_Report_V2", ref errorMsg);
            List<dbInformationShema> spShema = VersionControl.getSpShema(@"Get_Document_Report", ref errorMsg);
            bool test = false;
            int recID = 0;
            if (test)
            {
                tsql = "Select * From DocReportTmp Where RecID=@pRecID";
            }
            /* else if (FormID.HasValue && FormID.Value > 0) {
                 Supplier = string.Empty;
                 Service = string.Empty;
                 ServiceID = null;
                 CustNo = null;
                 param = string.Empty;
             }
            */
            if (spShemaV2.Count > 0)
            {
                tsql = @"Declare @Status smallint, @RptImage varbinary(MAX)
                         Exec dbo.Get_Document_Report_V2 @ResNo,@DocType,@Agency,@AgencyUser,@Supplier,@Service,@ServiceID,@CustNo,@FormID,@FileType,@Params,@Status OUTPUT,@RptImage OUTPUT
                         Select Status=@Status, RptImage=@RptImage, FileType=@FileType";
            }
            else
            {
                if (spShema.Find(f => f.Name == "@CustNo") != null)
                    tsql = @"Declare @Status smallint, @RptImage varbinary(MAX)
                         Exec dbo.Get_Document_Report @ResNo,@DocType,@Agency,@AgencyUser,@Supplier,@Service,@ServiceID,@CustNo,@FormID,@Params,@Status OUTPUT,@RptImage OUTPUT
                         Select Status=@Status, RptImage=@RptImage ";
                else
                    if (spShema.Find(f => f.Name == "@Service") != null)
                    tsql = @"Declare @Status smallint, @RptImage varbinary(MAX)
                         Exec dbo.Get_Document_Report @ResNo,@DocType,@Agency,@AgencyUser,@Supplier,@Service,@ServiceID,@FormID,@Params,@Status OUTPUT,@RptImage OUTPUT
                         Select Status=@Status, RptImage=@RptImage ";
                else
                    tsql = @"Declare @Status smallint, @RptImage varbinary(MAX)
                         Exec dbo.Get_Document_Report @ResNo,@DocType,@Agency,@AgencyUser,@Supplier,@FormID,@Params,@Status OUTPUT,@RptImage OUTPUT
                         Select Status=@Status, RptImage=@RptImage ";
            }
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            //dbCommand.CommandTimeout = 180;
            try
            {
                if (test)
                    db.AddInParameter(dbCommand, "pRecID", DbType.Int32, recID);

                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "DocType", DbType.Int16, DocType);
                db.AddInParameter(dbCommand, "Agency", DbType.String, AgencyID);
                db.AddInParameter(dbCommand, "AgencyUser", DbType.String, UserID);
                db.AddInParameter(dbCommand, "Supplier", DbType.String, Supplier);
                if (spShema.Find(f => f.Name == "@Service") != null)
                {
                    db.AddInParameter(dbCommand, "Service", DbType.String, Service);
                    db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, ServiceID);
                }
                if (spShema.Find(f => f.Name == "@CustNo") != null)
                {
                    db.AddInParameter(dbCommand, "CustNo", DbType.Int32, CustNo);
                }
                db.AddInParameter(dbCommand, "FormID", DbType.Int32, FormID);
                if (spShemaV2.Count > 0)
                {
                    db.AddInParameter(dbCommand, "FileType", DbType.Int16, docOutputType);
                }
                db.AddInParameter(dbCommand, "Params", DbType.String, param);
                dbCommand.CommandTimeout = 180;
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        Int16? status = Conversion.getInt16OrNull(rdr["Status"]);
                        object rptImage = rdr["RptImage"];
                        Int16? fileType = null;
                        if (spShemaV2.Count > 0)
                        {
                            fileType = Conversion.getInt16OrNull(rdr["FileType"]);
                        }
                        if (status.HasValue && status.Value != 1)
                        {
                            errorMsg = getReportStatus(new tvReportPDFRecord()
                            {
                                Status = status.Value,
                                RptImage = null
                            });
                        }
                        if (rptImage == System.DBNull.Value)
                            return null;
                        tvReportPDFRecord record = new tvReportPDFRecord
                        {
                            Status = status,
                            RptImage = rptImage,
                            FileType = fileType
                        };
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public tvReportPDFRecord generateReportV3(string AgencyID, string UserID, string ResNo, Int16 DocType, string Supplier, string Service, int? ServiceID, int? CustNo, int? FormID, Int16? docOutputType, string param, ref string errorMsg)
        {
            bool? documentGenerationModeIsApplication = TvTools.Conversion.getBoolOrNull(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationModeIsApplication"]);
            TourVisio.Library.Document.Models.Enums.enmDocumentGenerationMode documentGenerationMode =
                documentGenerationModeIsApplication.HasValue && documentGenerationModeIsApplication.Value ?
                TourVisio.Library.Document.Models.Enums.enmDocumentGenerationMode.Application :
                TourVisio.Library.Document.Models.Enums.enmDocumentGenerationMode.Procedure;

            TourVisio.WebService.Adapter.ServiceModels.ContentModels.GetDocumentFileResponse rpt = new TourVisio.Library.Document.Document(
                    new TourVisio.Library.Document.Models.GetDocumentFileRequest
                    {
                        Agency = AgencyID,
                        AgencyUser = UserID,
                        ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"])? "TvDataBase": System.Configuration.ConfigurationManager.AppSettings["DocumentGenerationConnectionKey"].ToString()].ToString(),
                        DocumentType = GetDocumentTypeById(DocType),
                        EncryptedReservationNumber = Guid.Empty,
                        DocumentGenerationMode = documentGenerationMode,
                        ServiceId = ServiceID.HasValue ? ServiceID.Value.ToString() : null,
                        TravellerId = CustNo,
                        ReservationNumber = ResNo,
                        DocumentId = FormID
                    }).GetFile();
            if (rpt.Data != null && rpt.Data.Length > 0)
                return new tvReportPDFRecord { RptImage = rpt.Data, Status = 1 };
            else
                return new tvReportPDFRecord { RptImage = null, Status = (short)rpt.Status };
        }
        private enmDocumentType GetDocumentTypeById(Int16 pDocType)
        {
            Type type = typeof(enmDocumentType);
            return (enmDocumentType)Enum.ToObject(type, pDocType);
        }

        public tvReportPDFRecord generateReportV2PDF3(string AgencyID, string UserID, string ResNo, Int16 DocType, string Supplier, string Service, int? ServiceID, int? CustNo, int? FormID, string param, ref string errorMsg)
        {
            tvReportPDFRecord item = new tvReportPDFRecord();
            string wsUrl = System.Configuration.ConfigurationManager.AppSettings["WSDocumentUrl"].ToString();
            //WSDocument.WSDocument doc = new TvBo.WSDocument.WSDocument();
            //doc.Url = wsUrl;
            //TvBo.WSDocument.TOutParams prm = doc.GetDocument(ResNo, DocType, AgencyID, UserID, Supplier, Service, ServiceID, CustNo, FormID, param);
            //if (prm != null && string.IsNullOrEmpty(prm.ErrMessage)) {
            //    item.RptImage = prm.RptImage;
            //    item.Status = Conversion.getInt16OrNull(prm.Status);
            //} else
            //    errorMsg = prm.ErrMessage;
            return item;
        }

        public tvReportPDFRecord generateReportV2PDF2(string AgencyID, string UserID, string ResNo, Int16 DocType, string Supplier, string Service, int? ServiceID, int? CustNo, int? FormID, string param, ref string errorMsg)
        {
            tvReportPDFRecord myOutParams = new tvReportPDFRecord();
            Database dbTv = (Database)DatabaseFactory.CreateDatabase();
            try
            {
                string SqlStr0 = "Select RefNo From ResMain (NOLOCK) Where ResNo=@ResNo";

                DbCommand dbCommand = dbTv.GetSqlStringCommand(SqlStr0);

                dbTv.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);

                using (IDataReader rdr = dbTv.ExecuteReader(dbCommand))
                {
                    if (!rdr.Read())
                    {
                        errorMsg = "Reservation not found!";
                        return null;
                    }
                }


                int sleepCount = 0;
                while (1 == 1)
                {
                    Process[] ps = Process.GetProcessesByName("TourVISIO.exe");
                    if (ps.Length < 10 || sleepCount > 10)
                        break;
                    else
                    {
                        System.Threading.Thread.Sleep(1000);
                        sleepCount++;
                    }
                }
                string SqlStr = @"
                                  Insert Into DocReportTmp (ResNo,DocType,Status,CrtDate,CrtUser,CrtAgency)
                                  Values(@ResNo,@DocType,0,GetDate(),@AgencyUser,@Agency) 
                                  Select SCOPE_IDENTITY() as NewID 
                                 ";

                dbCommand = dbTv.GetSqlStringCommand(SqlStr);
                dbTv.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                dbTv.AddInParameter(dbCommand, "DocType", DbType.Int16, DocType);
                dbTv.AddInParameter(dbCommand, "Agency", DbType.String, AgencyID);
                dbTv.AddInParameter(dbCommand, "AgencyUser", DbType.String, UserID);
                DataTable dt = dbTv.ExecuteDataSet(dbCommand).Tables[0];


                string NewIDStr = dt.Rows[0]["NewID"].ToString();
                string StatusStr = "";
                byte[] RptImageOut = null;

                //C:\TourVisioDoc\TourVISIO.exe /DE /ID:451451 /RN:RU072982 /DT:5

                string CmdStr = "/DE /ID:" + NewIDStr + " /RN:" + ResNo + " /DT:" + DocType;

                //Additional parameters

                if (Supplier != "")
                    CmdStr = CmdStr + " /SP:" + (char)34 + Supplier + (char)34;

                if (Service != "")
                {
                    CmdStr = CmdStr + " /SV:" + (char)34 + Service + (char)34;
                }

                if (ServiceID != null)
                {
                    CmdStr = CmdStr + " /SI:" + ServiceID;
                }
                if (CustNo != null)
                {
                    CmdStr = CmdStr + " /CN:" + CustNo;
                }
                if (FormID != null)
                {
                    CmdStr = CmdStr + " /DI:" + FormID;
                }

                if (param != "")
                {
                    CmdStr = CmdStr + " " + param;
                }

                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(@"C:\TourVisioDoc\TourVISIO.exe");
                psi.RedirectStandardOutput = true;
                psi.Arguments = CmdStr; //"/DE /ID:" + NewIDStr + " /RN:" + ResNo + " /DT:" + DocType;
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;
                System.Diagnostics.Process listFiles;
                listFiles = System.Diagnostics.Process.Start(psi);
                System.IO.StreamReader myOutput = listFiles.StandardOutput;
                // buraya süre yazmazsak bitene kadar bekler süre yazarsak o kadar süre biter sonra hasexited ile exenin sonlanıp sonlanmadığını görebiliriz
                listFiles.WaitForExit(60000);
                if (listFiles.HasExited)
                {
                    string SqlStr2 = "Select Status,RptImage From DocReportTmp (NOLOCK) Where RecID=@RecID";
                    dbCommand = dbTv.GetSqlStringCommand(SqlStr2);
                    dbTv.AddInParameter(dbCommand, "RecID", DbType.Int32, NewIDStr);
                    DataTable dt2 = dbTv.ExecuteDataSet(dbCommand).Tables[0];

                    StatusStr = dt2.Rows[0]["Status"].ToString();
                    RptImageOut = ((byte[])dt2.Rows[0]["RptImage"]);

                }
                //return NewIDStr + ";" + StatusStr;

                myOutParams.Status = Conversion.getInt16OrNull(StatusStr);
                myOutParams.RptImage = RptImageOut;
                return myOutParams;
            }
            catch (Exception ExcStr)
            {
                errorMsg = ExcStr.Message;
                return null;//ExcStr.Message;
            }
            finally
            {
            }
        }

        public tvReportPDFRecord generateReportPDF(User UserData, ResDataRecord ResData, string DocName, ref string errorMsg)
        {
            Int16 DocType = 1;
            string param = string.Empty;
            switch (DocName)
            {
                case "Invoice":
                    DocType = 1;
                    param = " /01:0";
                    break;
                case "Proforma":
                    DocType = 1;
                    param = " /01:1";
                    break;
                case "Voucher":
                    DocType = 2;
                    break;
            }

            string tsql = string.Empty;

            tsql = @"Declare @Status smallint, @RptImage varbinary(MAX)
                     Exec dbo.Get_Document_Report @ResNo, @DocType, @Agency, @AgencyUser, @Status OUTPUT, @RptImage OUTPUT
                     Select Status=@Status, RptImage=@RptImage ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResData.ResMain.ResNo);
                db.AddInParameter(dbCommand, "DocType", DbType.Int16, DocType);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "AgencyUser", DbType.String, UserData.UserID);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        tvReportPDFRecord record = new tvReportPDFRecord
                        {
                            Status = Conversion.getInt16OrNull(rdr["Status"]),
                            RptImage = rdr["RptImage"]
                        };
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string mergeReportFiles(string tempFolder, string ResNo, List<tvReportPDFRecord> reportList)
        {
            string fileName = ResNo + "_" + System.Guid.NewGuid() + ".PDF";

            Document doc = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(tempFolder + fileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            foreach (tvReportPDFRecord row in reportList)
            {
                PdfReader reader2 = new PdfReader((byte[])row.RptImage);
                for (int currentpage = 1; currentpage <= reader2.NumberOfPages; currentpage++)
                {
                    byte[] pdfBytes2 = reader2.GetPageContent(currentpage);
                    doc.SetPageSize(reader2.GetPageSizeWithRotation(currentpage));
                    doc.NewPage();
                    PdfImportedPage page = pdfWriter.GetImportedPage(reader2, currentpage);
                    PdfContentByte cb2 = pdfWriter.DirectContent;
                    cb2.AddTemplate(page, 0, 0);
                    cb2.ClosePath();
                }
                reader2.Close();
            }
            doc.Close();
            return fileName;
        }

        public string saveHtmlFile(User UserData, string tempFolder, string fileName, List<tvReportPDFRecord> reportList)
        {
            //string fileName = ResNo + "_" + System.Guid.NewGuid() + ".html";
            try
            {
                if (File.Exists(tempFolder + fileName))
                    File.Delete(tempFolder + fileName);
                System.IO.File.WriteAllText(tempFolder + fileName, Conversion.getStrOrNull(System.Text.Encoding.UTF8.GetString((byte[])reportList.FirstOrDefault().RptImage)));
                return fileName;
            }
            catch
            {
                return "";
            }
        }

        public string mergeReportFiles(User UserData, string tempFolder, string ResNo, List<tvReportPDFRecord> reportList)
        {
            string fileName = ResNo + "_" + System.Guid.NewGuid() + ".PDF";
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                switch (UserData.Market)
                {
                    case "SWEMAR":
                        fileName = "Biljett_" + fileName;
                        break;
                    case "NORMAR":
                        fileName = "Billett_" + fileName;
                        break;
                    case "DENMAR":
                        fileName = "Rejsebevis_" + fileName;
                        break;
                    case "FINMAR":
                        fileName = "Lippu_" + fileName;
                        break;
                }
            }
            Document doc = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(tempFolder + fileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            foreach (tvReportPDFRecord row in reportList)
            {
                PdfReader reader2 = new PdfReader((byte[])row.RptImage);
                for (int currentpage = 1; currentpage <= reader2.NumberOfPages; currentpage++)
                {
                    byte[] pdfBytes2 = reader2.GetPageContent(currentpage);
                    doc.SetPageSize(reader2.GetPageSizeWithRotation(currentpage));
                    doc.NewPage();
                    PdfImportedPage page = pdfWriter.GetImportedPage(reader2, currentpage);
                    PdfContentByte cb2 = pdfWriter.DirectContent;
                    cb2.AddTemplate(page, 0, 0);
                    cb2.ClosePath();
                }
                reader2.Close();
            }
            doc.Close();
            return fileName;
        }
    }
}
