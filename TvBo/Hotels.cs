﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TvTools;

namespace TvBo
{
    public class Hotels
    {
        public List<FlightDays> getFixedCheckinDates(string HolPack, ref string errorMsg)
        {
            string tsql = @"Select FixDate,AccomNights From HolPackFixDate (NOLOCK)
                            Where 
                                HolPack = @HolPack And 
                                FixDate > GetDate()
                            Order By FixDate";
            List<FlightDays> checkinDates = new List<FlightDays>();
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "HolPack", DbType.String, HolPack);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        checkinDates.Add(new FlightDays()
                        {
                             FlyDate=Convert.ToDateTime(oReader["FixDate"]),
                             Night = Conversion.getInt32OrNull(oReader["AccomNights"])
                        });
                    }
                }
                return checkinDates;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return checkinDates;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public string getRoomConseptInfo(string Market, string Hotel, string Room, DateTime? CheckIn, ref string errorMsg)
        {
            string tsql = @"Select CText, CHtml From HotelRoomCont
                            Where 
                                Market = @Market And 
                                Hotel = @Hotel And 
                                Room = @Room And
                                @CheckIn  between ValBegDate And ValEndDate
                            Order By ValBegDate";
            string retVal = string.Empty;
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Room", DbType.String, Room);
                db.AddInParameter(dbCommand, "CheckIn", DbType.Date, CheckIn);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        if (retVal.Length > 0)
                            retVal += "<br />";
                        retVal += (string)oReader["CHtml"];
                    }
                }
                if (string.IsNullOrEmpty(retVal))
                    retVal = HttpContext.GetGlobalResourceObject("LibraryResource", "RoomContentNotFound").ToString();
                return retVal;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return retVal;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getBoardConseptInfo(string Market, string Hotel, string Board, DateTime CheckIn, ref string errorMsg)
        {
            string tsql = @"Select CText, CHtml From HotelBoardCont
                            Where 
                                Market = @Market And 
                                Hotel = @Hotel And 
                                Board = @Board And
                                @CheckIn  between ValBegDate And ValEndDate
                            Order By ValBegDate";
            string retVal = string.Empty;
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Board", DbType.String, Board);
                db.AddInParameter(dbCommand, "CheckIn", DbType.Date, CheckIn);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        if (retVal.Length > 0)
                            retVal += "<br />";
                        retVal += (string)oReader["CHtml"];
                    }
                }
                if (string.IsNullOrEmpty(retVal))
                    retVal = HttpContext.GetGlobalResourceObject("LibraryResource", "BoardContentNotFound").ToString();
                return retVal;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return retVal;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<HotelRecord> GetAllHotelInfo(User UserData, string hotelCode, ref string errorMsg)
        {
            if (hotelCode == "") { errorMsg = "Hotel Code is empty"; return null; }

            string tsql = @"Select  RecId HotelId,Code HotelCode,Name HotelName 
                    From Hotel (NOLOCK) Where Status='Y'   ";
            if (hotelCode.Trim() != "")
                tsql += " AND  Code=@HotelCode";
            tsql += " Order by Code ";
            List<HotelRecord> lstHotel = new List<HotelRecord>();

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        HotelRecord hr = new HotelRecord();
                        hr.Code = oReader["HotelCode"].ToString();
                        hr.Name = oReader["HotelName"].ToString();
                        hr.RecID =Convert.ToInt32(oReader["HotelId"]);
                        lstHotel.Add(hr);
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            return lstHotel;
        }
        public List<HotelRecord> GetAllHotelInfoForCache(User UserData, ref string errorMsg)
        {

            string tsql = @"Select  RecId HotelId,Code HotelCode,Name HotelName 
                    From Hotel (NOLOCK) Where Status='Y'   ";
            List<HotelRecord> lstHotel = new List<HotelRecord>();

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        HotelRecord hr = new HotelRecord();
                        hr.Code = oReader["HotelCode"].ToString();
                        hr.Name = oReader["HotelName"].ToString();
                        hr.RecID = Convert.ToInt32(oReader["HotelId"]);
                        lstHotel.Add(hr);
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            return lstHotel;
        }
        public HotelRecord getHotelDetail(User UserData, string Code, ref string errorMsg)
        {
            HotelRecord record = new HotelRecord();
            string maxChdAgeSql = Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200003") ? "isnull(HO.MaxChdAge, H.MaxChdAge)" : "H.MaxChdAge";
            string tsql = string.Format(
                          @"Select H.RecID,H.Code, H.[Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market), [Name]), 
	                            H.NameS, H.HotelType, H.Category, H.AirportDist, H.CenterDist, H.SeaDist, 
	                            H.CheckInTime, H.CheckOutTime, H.PostAddress, 
	                            H.Location,
	                            LocationName=(Select Name From Location (NOLOCK) Where RecID=H.Location),
                                LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=H.Location),
	                            H.TrfLocation, 
	                            TrfLocationName=(Select Name From Location (NOLOCK) Where RecID=H.TrfLocation),
                                TrfLocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=H.TrfLocation),
	                            H.PostZip, H.InvAddress, H.Phone1, H.Phone2, H.Fax, H.FaxRes, H.email, H.emailres, H.homepage, 
	                            H.ManagerName, H.RoomCount, H.BossName, H.ContactName, H.TaxOffice, H.TaxAccNo, H.AirPort1,
	                            H.ChdAgeGrpCnt, H.PayCom, H.PayComEB, H.PayPasEB, H.ConfStat, H.TaxPer, 
	                            H.UseSysParamAllot, H.HotAllotChk, H.HotAllotWarnFull, H.HotAllotNotAcceptFull, H.HotAllotNotFullGA,
	                            H.DefSupplier, H.CanDiscount, MaxChdAge={0}, H.Lat, H.Lon, H.OfCategory, H.WithoutLady
                            From Hotel H (NOLOCK)                                 
                            Left Join HotelMarOpt HO (NOLOCK) on HO.Hotel=H.Code and HO.Market=@Market
                            Where H.Status='Y' ", maxChdAgeSql);
            tsql += string.Format(" And Code='{0}' ", Code);

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        record.RecID = (int)oReader["RecID"];
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.LocalName = Conversion.getStrOrNull(oReader["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        record.HotelType = Conversion.getStrOrNull(oReader["HotelType"]);
                        record.Category = Conversion.getStrOrNull(oReader["Category"]);
                        record.AirportDist = Conversion.getInt32OrNull(oReader["AirportDist"]);
                        record.CenterDist = Conversion.getInt32OrNull(oReader["CenterDist"]);
                        record.SeaDist = Conversion.getInt32OrNull(oReader["SeaDist"]);
                        record.CheckInTime = Conversion.getDateTimeOrNull(oReader["CheckInTime"]);
                        record.CheckOutTime = Conversion.getDateTimeOrNull(oReader["CheckOutTime"]);
                        record.PostAddress = Conversion.getStrOrNull(oReader["PostAddress"]);
                        record.Location = (int)oReader["Location"];
                        record.LocationName = Conversion.getStrOrNull(oReader["LocationName"]);
                        record.LocationLocalName = Conversion.getStrOrNull(oReader["LocationLocalName"]);
                        record.TrfLocation = (int)oReader["TrfLocation"];
                        record.TrfLocationName = Conversion.getStrOrNull(oReader["TrfLocationName"]);
                        record.TrfLocationLocalName = Conversion.getStrOrNull(oReader["TrfLocationLocalName"]);
                        record.PostZip = Conversion.getStrOrNull(oReader["PostZip"]);
                        record.InvAddress = Conversion.getStrOrNull(oReader["InvAddress"]);
                        record.Phone1 = Conversion.getStrOrNull(oReader["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(oReader["Phone2"]);
                        record.Fax = Conversion.getStrOrNull(oReader["Fax"]);
                        record.Email = Conversion.getStrOrNull(oReader["email"]);
                        record.Homepage = Conversion.getStrOrNull(oReader["homepage"]);
                        record.ManagerName = Conversion.getStrOrNull(oReader["ManagerName"]);
                        record.RoomCount = Conversion.getInt16OrNull(oReader["RoomCount"]);
                        record.BossName = Conversion.getStrOrNull(oReader["BossName"]);
                        record.ContactName = Conversion.getStrOrNull(oReader["ContactName"]);
                        record.TaxOffice = Conversion.getStrOrNull(oReader["TaxOffice"]);
                        record.TaxAccNo = Conversion.getStrOrNull(oReader["TaxAccNo"]);
                        record.AirPort1 = Conversion.getStrOrNull(oReader["AirPort1"]);
                        record.ChdAgeGrpCnt = Conversion.getInt16OrNull(oReader["ChdAgeGrpCnt"]);
                        record.ConfStat = Conversion.getInt16OrNull(oReader["ConfStat"]);
                        record.PayCom = Equals(oReader["PayCom"], "Y");
                        record.PayComEB = Equals(oReader["PayComEB"], "Y");
                        record.PayPasEB = Equals(oReader["PayPasEB"], "Y");
                        record.CanDiscount = (bool)oReader["CanDiscount"];
                        record.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        record.UseSysParamAllot = Equals(oReader["UseSysParamAllot"], "Y");
                        record.HotAllotChk = Equals(oReader["HotAllotChk"], "Y");
                        record.HotAllotWarnFull = Equals(oReader["HotAllotWarnFull"], "Y");
                        record.HotAllotNotAcceptFull = Equals(oReader["HotAllotNotAcceptFull"], "Y");
                        record.HotAllotNotFullGA = Equals(oReader["HotAllotNotFullGA"], "Y");
                        record.DefSupplier = Conversion.getStrOrNull(oReader["DefSupplier"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        record.Lat = Conversion.getDecimalOrNull(oReader["Lat"]);
                        record.Lon = Conversion.getDecimalOrNull(oReader["Lon"]);
                        record.OfCategory = Conversion.getStrOrNull(oReader["OfCategory"]);
                        record.WithoutLady = (bool)oReader["WithoutLady"];
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelRecord> getHotelDetail(User UserData, int? Location, int? TrfLocation, ref string errorMsg)
        {
            List<HotelRecord> records = new List<HotelRecord>();
            string maxChdAgeSql = Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200003") ? "isnull(HO.MaxChdAge, H.MaxChdAge)" : "H.MaxChdAge";
            string tsql = string.Format(
                          @"Select H.RecID,H.Code, H.[Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market), [Name]), 
	                            H.NameS, H.HotelType, H.Category, H.AirportDist, H.CenterDist, H.SeaDist, 
	                            H.CheckInTime, H.CheckOutTime, H.PostAddress, 
	                            H.Location,
	                            LocationName = (Select Name From Location (NOLOCK) Where RecID = H.Location),
                                LocationLocalName = (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = H.Location),
	                            H.TrfLocation, 
	                            TrfLocationName = (Select Name From Location (NOLOCK) Where RecID = H.TrfLocation),
                                TrfLocationLocalName = (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = H.TrfLocation),
	                            H.PostZip, H.InvAddress, H.Phone1, H.Phone2, H.Fax, H.FaxRes, H.email, H.emailres, H.homepage, 
	                            H.ManagerName, H.RoomCount, H.BossName, H.ContactName, H.TaxOffice, H.TaxAccNo, H.AirPort1,
	                            H.ChdAgeGrpCnt, H.PayCom, H.PayComEB, H.PayPasEB, H.ConfStat, H.TaxPer, 
	                            H.UseSysParamAllot, H.HotAllotChk, H.HotAllotWarnFull, H.HotAllotNotAcceptFull, H.HotAllotNotFullGA,
	                            H.DefSupplier, H.CanDiscount, MaxChdAge={0}, H.Lat, H.Lon, H.OfCategory, H.WithoutLady
                            From Hotel H (NOLOCK)                                 
                            Left Join HotelMarOpt HO (NOLOCK) on HO.Hotel=H.Code and HO.Market=@Market
                            Where H.Status = 'Y' ", maxChdAgeSql);
            if (Location.HasValue)
                tsql += string.Format(" And Location={0}", Location.Value);
            if (TrfLocation.HasValue)
                tsql += string.Format(" And TrfLocation={0}", TrfLocation.Value);

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        HotelRecord record = new HotelRecord();
                        record.RecID = (int)oReader["RecID"];
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        record.LocalName = Conversion.getStrOrNull(oReader["LocalName"]);
                        record.HotelType = Conversion.getStrOrNull(oReader["HotelType"]);
                        record.Category = Conversion.getStrOrNull(oReader["Category"]);
                        record.AirportDist = Conversion.getInt32OrNull(oReader["AirportDist"]);
                        record.CenterDist = Conversion.getInt32OrNull(oReader["CenterDist"]);
                        record.SeaDist = Conversion.getInt32OrNull(oReader["SeaDist"]);
                        record.CheckInTime = Conversion.getDateTimeOrNull(oReader["CheckInTime"]);
                        record.CheckOutTime = Conversion.getDateTimeOrNull(oReader["CheckOutTime"]);
                        record.PostAddress = Conversion.getStrOrNull(oReader["PostAddress"]);
                        record.Location = (int)oReader["Location"];
                        record.LocationName = Conversion.getStrOrNull(oReader["LocationName"]);
                        record.LocationLocalName = Conversion.getStrOrNull(oReader["LocationLocalName"]);
                        record.TrfLocation = (int)oReader["TrfLocation"];
                        record.TrfLocationName = Conversion.getStrOrNull(oReader["TrfLocationName"]);
                        record.TrfLocationLocalName = Conversion.getStrOrNull(oReader["TrfLocationLocalName"]);
                        record.PostZip = Conversion.getStrOrNull(oReader["PostZip"]);
                        record.InvAddress = Conversion.getStrOrNull(oReader["InvAddress"]);
                        record.Phone1 = Conversion.getStrOrNull(oReader["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(oReader["Phone2"]);
                        record.Fax = Conversion.getStrOrNull(oReader["Fax"]);
                        record.Email = Conversion.getStrOrNull(oReader["email"]);
                        record.Homepage = Conversion.getStrOrNull(oReader["homepage"]);
                        record.ManagerName = Conversion.getStrOrNull(oReader["ManagerName"]);
                        record.RoomCount = Conversion.getInt16OrNull(oReader["RoomCount"]);
                        record.BossName = Conversion.getStrOrNull(oReader["BossName"]);
                        record.ContactName = Conversion.getStrOrNull(oReader["ContactName"]);
                        record.TaxOffice = Conversion.getStrOrNull(oReader["TaxOffice"]);
                        record.TaxAccNo = Conversion.getStrOrNull(oReader["TaxAccNo"]);
                        record.AirPort1 = Conversion.getStrOrNull(oReader["AirPort1"]);
                        record.ChdAgeGrpCnt = Conversion.getInt16OrNull(oReader["ChdAgeGrpCnt"]);
                        record.ConfStat = Conversion.getInt16OrNull(oReader["ConfStat"]);
                        record.PayCom = Equals(oReader["PayCom"], "Y");
                        record.PayComEB = Equals(oReader["PayComEB"], "Y");
                        record.PayPasEB = Equals(oReader["PayPasEB"], "Y");
                        record.CanDiscount = (bool)oReader["CanDiscount"];
                        record.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        record.UseSysParamAllot = Equals(oReader["UseSysParamAllot"], "Y");
                        record.HotAllotChk = Equals(oReader["HotAllotChk"], "Y");
                        record.HotAllotWarnFull = Equals(oReader["HotAllotWarnFull"], "Y");
                        record.HotAllotNotAcceptFull = Equals(oReader["HotAllotNotAcceptFull"], "Y");
                        record.HotAllotNotFullGA = Equals(oReader["HotAllotNotFullGA"], "Y");
                        record.DefSupplier = Conversion.getStrOrNull(oReader["DefSupplier"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        record.Lat = Conversion.getDecimalOrNull(oReader["Lat"]);
                        record.Lon = Conversion.getDecimalOrNull(oReader["Lon"]);
                        record.OfCategory = Conversion.getStrOrNull(oReader["OfCategory"]);
                        record.WithoutLady = (bool)oReader["WithoutLady"];
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int getHotelTrfLocation(User UserData, string Code, ref string errorMsg)
        {
            HotelRecord hotel = getHotelDetail(UserData, Code, ref errorMsg);
            if (hotel != null)
                return hotel.TrfLocation;
            else
                return 0;
        }

        public int getHotelLocation(User UserData, string Code, ref string errorMsg)
        {
            HotelRecord hotel = getHotelDetail(UserData, Code, ref errorMsg);
            if (hotel != null)
                return hotel.Location;
            else
                return 0;
        }

        public HotelAccomRecord getHotelAccom(string Market, string Hotel, string Room, string Code, ref string errorMsg)
        {
            HotelAccomRecord record = new HotelAccomRecord();
            string tsql = string.Empty;
            tsql = @"   Select Hotel, Room, Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market),[Name]),
                            NameS, StdAdl, MinAdl, MaxAdl, MaxChd, MaxPax, DispNo
                        From HotelAccom (NOLOCK)
                        Where VisiblePL = 'Y'
                          And Hotel=@Hotel 
                          And Room=@Room
                          And Code=@Code ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Room", DbType.String, Room);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.StdAdl = Conversion.getInt16OrNull(R["StdAdl"]);
                        record.MinAdl = Conversion.getInt16OrNull(R["MinAdl"]);
                        record.MaxAdl = Conversion.getInt16OrNull(R["MaxAdl"]);
                        record.MaxChd = Conversion.getInt16OrNull(R["MaxChd"]);
                        record.MaxPax = Conversion.getInt16OrNull(R["MaxPax"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;

            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelAccomRecord> getHotelAccomPriceList(string Market, string Hotel, string Room, int? CatPackID, ref string errorMsg)
        {
            List<HotelAccomRecord> records = new List<HotelAccomRecord>();
            string tsql = @"Select HA.Hotel, HA.Room, HA.Code, HA.Name, LocalName=isnull(dbo.FindLocalName(HA.NameLID, @Market),HA.Name),
                                  HA.NameS, HA.StdAdl, HA.MinAdl, HA.MaxAdl, HA.MaxChd, HA.MaxPax, HA.DispNo
                            From HotelAccom HA (NOLOCK)
                            Join (                                                                                    
                                    Select Distinct  CPA.Hotel, CPA.Accom
                                    From CatPriceA CPA (NOLOCK)
                                    Join Hotel H (NOLOCK) ON H.Code=CPA.Hotel		                        
                                    Join HotelAccom HA (NOLOCK) ON HA.Hotel=CPA.Hotel And HA.Code=CPA.Accom 
                                    Where CatPackID=@CatPackID And H.Code=@Hotel 
                                    And CPA.Status=1
                            ) X on X.Accom=HA.Code And X.Hotel=HA.Hotel ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Room", DbType.String, Room);
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelAccomRecord record = new HotelAccomRecord();
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.StdAdl = Conversion.getInt16OrNull(R["StdAdl"]);
                        record.MinAdl = Conversion.getInt16OrNull(R["MinAdl"]);
                        record.MaxAdl = Conversion.getInt16OrNull(R["MaxAdl"]);
                        record.MaxChd = Conversion.getInt16OrNull(R["MaxChd"]);
                        record.MaxPax = Conversion.getInt16OrNull(R["MaxPax"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelAccomRecord> getHotelAccom(string Market, string Hotel, ref string errorMsg)
        {
            List<HotelAccomRecord> records = new List<HotelAccomRecord>();
            string tsql = string.Empty;
            tsql = @"   Select Hotel, Room, Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market),[Name]),
                            NameS, StdAdl, MinAdl, MaxAdl, MaxChd, MaxPax, DispNo
                        From HotelAccom (NOLOCK)
                        Where VisiblePL = 'Y'
                          And Hotel=@Hotel ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelAccomRecord record = new HotelAccomRecord();
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.StdAdl = Conversion.getInt16OrNull(R["StdAdl"]);
                        record.MinAdl = Conversion.getInt16OrNull(R["MinAdl"]);
                        record.MaxAdl = Conversion.getInt16OrNull(R["MaxAdl"]);
                        record.MaxChd = Conversion.getInt16OrNull(R["MaxChd"]);
                        record.MaxPax = Conversion.getInt16OrNull(R["MaxPax"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<RoomRecord> getRoomList(string Market, ref string errorMsg)
        {
            List<RoomRecord> records = new List<RoomRecord>();
            string tsql = string.Empty;
            if (VersionControl.getTableField("Room", "RoomViewID"))
                tsql =
@"
Select RecID,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),
  NameS,DispNo=isnull(DispNo, 9999),Explanation,RoomViewID
From Room (NOLOCK)
Order By DispNo 
";
            else
                tsql =
@"
Select RecID,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),
  NameS,DispNo=isnull(DispNo, 9999),Explanation,RoomViewID=null
From Room (NOLOCK)
Order By DispNo 
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        RoomRecord record = new RoomRecord();
                        record.RecID = (int)R["RecID"];
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.Explanation = Conversion.getStrOrNull(R["Explanation"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.RoomViewID = Conversion.getInt32OrNull(R["RoomViewID"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelRoomRecord> getPriceListRoomList(string Market, string Hotel, int? CatPackID, ref string errorMsg)
        {
            List<HotelRoomRecord> records = new List<HotelRoomRecord>();
            string tsql = string.Empty;
            if (VersionControl.getTableField("Room", "RoomViewID"))
                tsql =
@"
Select R.RecID,R.Hotel,R.Code,R.Name,NameL=isnull(dbo.FindLocalName(R.NameLID,@Market),(Select isnull(dbo.FindLocalName(NameLID,@Market),R.Name) From Room (NOLOCK) Where Code=R.Code)),
  R.NameS,DispNo=isnull(R.DispNo,9999),R.BaseRoom,R.RoomViewID
From HotelRoom R (NOLOCK)
Join (  Select Distinct CPA.Hotel, CPA.Room	
		From CatPriceA CPA (NOLOCK)
		Join Hotel H (NOLOCK) ON H.Code=CPA.Hotel
		Join HotelRoom HR (NOLOCK) ON HR.Hotel=CPA.Hotel And HR.Code=CPA.Room
		Where CatPackID=@CatPackID And H.Code=@Hotel
		And CPA.Status=1
) X on X.Room=R.Code And X.Hotel=R.Hotel 
";
            else
                tsql =
@"
Select R.RecID,R.Hotel,R.Code,R.Name,NameL=isnull(dbo.FindLocalName(R.NameLID,@Market),(Select isnull(dbo.FindLocalName(NameLID,@Market),R.Name) From Room (NOLOCK) Where Code=R.Code)),
  R.NameS,DispNo=isnull(R.DispNo,9999),R.BaseRoom,RoomViewID=null
From HotelRoom R (NOLOCK)
Join (  Select Distinct CPA.Hotel, CPA.Room	
		From CatPriceA CPA (NOLOCK)
		Join Hotel H (NOLOCK) ON H.Code=CPA.Hotel
		Join HotelRoom HR (NOLOCK) ON HR.Hotel=CPA.Hotel And HR.Code=CPA.Room
		Where CatPackID=@CatPackID And H.Code=@Hotel
		And CPA.Status=1
) X on X.Room=R.Code And X.Hotel=R.Hotel 
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelRoomRecord record = new HotelRoomRecord();
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.BaseRoom = Conversion.getStrOrNull(R["BaseRoom"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.RoomViewID = Conversion.getInt32OrNull(R["RoomViewID"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelRoomRecord> getHotelRoomList(string Market, ref string errorMsg)
        {
            List<HotelRoomRecord> records = new List<HotelRoomRecord>();
            string tsql = string.Empty;
            if (VersionControl.getTableField("HotelRoom", "RoomViewID"))
                tsql =
@"
Select RecID, Hotel, Code, Name, 
  NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),
  NameS,DispNo=isnull(DispNo,9999),BaseRoom,RoomViewID
From HotelRoom (NOLOCK)
Where 
	VisiblePL='Y' 	
Order By DispNo 
";
            else
                tsql =
@"
Select RecID, Hotel, Code, Name, 
  NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),
  NameS,DispNo=isnull(DispNo,9999),BaseRoom,RoomViewID=null
From HotelRoom (NOLOCK)
Where 
	VisiblePL='Y' 	
Order By DispNo 
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelRoomRecord record = new HotelRoomRecord();
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.BaseRoom = Conversion.getStrOrNull(R["BaseRoom"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.RoomViewID = Conversion.getInt32OrNull(R["RoomViewID"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelRoomRecord> getHotelRoomList(string Market, string HotelCode, ref string errorMsg)
        {
            List<HotelRoomRecord> records = new List<HotelRoomRecord>();
            string tsql = string.Empty;
            if (VersionControl.getTableField("Room", "RoomViewID"))
                tsql =
@"
Select RecID, Hotel, Code, Name, 
  NameL=isnull(dbo.FindLocalName(NameLID,@Market),(Select isnull(dbo.FindLocalName(NameLID,@Market),HotelRoom.Name) From Room (NOLOCK) Where Code=HotelRoom.Code)),
  NameS,DispNo=isnull(DispNo,9999),BaseRoom,RoomViewID
From HotelRoom (NOLOCK)
Where 
	VisiblePL='Y' And
	Hotel=@Hotel
Order By DispNo 
";
            else
                tsql =
@"
Select RecID, Hotel, Code, Name, 
  NameL=isnull(dbo.FindLocalName(NameLID,@Market),(Select isnull(dbo.FindLocalName(NameLID,@Market),HotelRoom.Name) From Room (NOLOCK) Where Code=HotelRoom.Code)),
  NameS,DispNo=isnull(DispNo,9999),BaseRoom,RoomViewID=null
From HotelRoom (NOLOCK)
Where 
	VisiblePL='Y' And
	Hotel=@Hotel
Order By DispNo 
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, HotelCode);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelRoomRecord record = new HotelRoomRecord();
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.BaseRoom = Conversion.getStrOrNull(R["BaseRoom"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.RoomViewID = Conversion.getInt32OrNull(R["RoomViewID"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BoardRecord> getBoardList(string Market, ref string errorMsg)
        {
            List<BoardRecord> records = new List<BoardRecord>();
            string tsql = @"Select RecID, Code, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name),
	                            NameS, Description, DispNo=isnull(DispNo, 9999)
                            From Board (NOLOCK)
                            Order By DispNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        BoardRecord record = new BoardRecord();
                        record.RecID = (int)R["RecID"];
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelBoardRecord> getHotelBoardListPriceList(string Market, string HotelCode, int? CatPackID, ref string errorMsg)
        {
            List<HotelBoardRecord> records = new List<HotelBoardRecord>();
            string tsql = @"Select RecID, Hotel, Code, Name, 
                                NameL=isnull(dbo.FindLocalName(NameLID, @Market), (Select isnull(dbo.FindLocalName(NameLID, @Market), HB.Name) From Board (NOLOCK) Where Code=HB.Code)),
                                NameS, DispNo=isnull(DispNo, 9999), BegDate, EndDate
                            From HotelBoard HB (NOLOCK)
                            Join (                                                                                    
                                    Select Distinct CPA.Board
                                    From CatPriceA CPA (NOLOCK)
                                    Join Hotel H (NOLOCK) ON H.Code=CPA.Hotel
                                    Join HotelBoard HB (NOLOCK) ON HB.Hotel=CPA.Hotel And HB.Code=CPA.Board
                                    Where CatPackID=@CatPackID And H.Code=@Hotel
                                    And CPA.Status=1
                            ) X ON X.Board=HB.Code
                            Where VisiblePL='Y'     
                            And Hotel=@Hotel
                            Order By DispNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, HotelCode);
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelBoardRecord record = new HotelBoardRecord();
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelBoardRecord> getHotelBoardList(string Market, ref string errorMsg)
        {
            List<HotelBoardRecord> records = new List<HotelBoardRecord>();
            string tsql = @"Select
	                            RecID, Hotel, Code, Name, 
	                            NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name),
	                            NameS, DispNo=isnull(DispNo, 9999), BegDate, EndDate
                            From HotelBoard (NOLOCK)
                            Where 
	                            VisiblePL='Y'                            
                            Order By DispNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelBoardRecord record = new HotelBoardRecord();
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelBoardRecord> getHotelBoardList(string Market, string HotelCode, ref string errorMsg)
        {
            List<HotelBoardRecord> records = new List<HotelBoardRecord>();
            string tsql = @"Select
	                            RecID, Hotel, Code, Name, 
	                            NameL=isnull(dbo.FindLocalName(NameLID, @Market), (Select isnull(dbo.FindLocalName(NameLID, @Market), HotelBoard.Name) From Board (NOLOCK) Where Code=HotelBoard.Code)),
	                            NameS, DispNo=isnull(DispNo, 9999), BegDate, EndDate
                            From HotelBoard (NOLOCK)
                            Where 
	                            VisiblePL='Y' And
	                            Hotel=@Hotel
                            Order By DispNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, HotelCode);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelBoardRecord record = new HotelBoardRecord();
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool IsAllotmentDefined(string Hotel, string Room, Int16 AllotType, string Market, DateTime BegDate, int Night)
        {
            bool aCnt;
            bool retVal = true;
            string errorMsg = string.Empty;

            string sqlstr = @"  Select Distinct Date From HotelAllotRoom (NOLOCK) 
                                Where AllotID = (Select Parent From HotelAllot (NOLOCK)
                                                 Where Parent in (Select RecID From HotelAllot (NOLOCK)
                                                                  Where Hotel = @Hotel and Market = ''
                                                                 ) And 
                                                        Office = '' and 
                                                        Agency = '' and 
                                                        Market = @Market
                                                ) And 
                                    (Date between @BegDate And @EndDate) And 
                                    (AllotType between @AllotType1 And @AllotType2) And 
                                    Room = @Room ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sqlstr);
            if (AllotType == -1)
            {
                try
                {
                    db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                    db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, BegDate.Date.AddDays(Night - 1));
                    db.AddInParameter(dbCommand, "AllotType1", DbType.Int16, 0);
                    db.AddInParameter(dbCommand, "AllotType2", DbType.Int16, 2);
                    db.AddInParameter(dbCommand, "Room", DbType.String, Room);
                    DataSet ds = db.ExecuteDataSet(dbCommand);
                    aCnt = (ds.Tables[0].Rows.Count == Night) ? true : false;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    aCnt = false;
                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
                if (!aCnt)
                    retVal = false;

                return retVal;
            }

            try
            {
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, BegDate.Date.AddDays(Night - 1));
                db.AddInParameter(dbCommand, "AllotType1", DbType.Int16, AllotType);
                db.AddInParameter(dbCommand, "AllotType2", DbType.Int16, AllotType);
                db.AddInParameter(dbCommand, "Room", DbType.String, Room);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                aCnt = (ds.Tables[0].Rows.Count == Night) ? true : false;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                aCnt = false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

            return aCnt;
        }

        public bool AllotmentControlForMultipleRoom(User UserData, List<SearchResult> SelectBook, string PackType, ref string errorMsg)
        {
            var query = from q in SelectBook
                        group q by new
                        {
                            q.Hotel,
                            q.Room,
                            //q.Accom,
                            CheckIn = q.HotCheckIn.HasValue ? q.HotCheckIn.Value : q.CheckIn,
                            q.CheckOut,
                            Night = q.HotNight.HasValue ? q.HotNight.Value : q.Night,
                            Market=UserData.CustomRegID.Equals(Common.crID_Detur) && !string.IsNullOrEmpty(q.DepFlightMarket.Trim())?q.DepFlightMarket: q.Market,
                            q.Operator
                        } into k
                        select new
                        {
                            Hotel = k.Key.Hotel,
                            Room = k.Key.Room,
                            //Accom = k.Key.Accom,
                            CheckIn = k.Key.CheckIn,
                            CheckOut = k.Key.CheckOut,
                            Night = k.Key.Night,
                            Market = k.Key.Market,
                            Operator = k.Key.Operator,
                            RoomCount = (
                                SelectBook.Where(w => w.Hotel == k.Key.Hotel &&
                                                      w.Room == k.Key.Room &&
                                    //w.Accom == k.Key.Accom &&
                                                      (w.HotCheckIn.HasValue ? w.HotCheckIn.Value : w.CheckIn) == k.Key.CheckIn &&
                                                      w.CheckOut == k.Key.CheckOut &&
                                                      (w.HotNight.HasValue ? w.HotNight.Value : w.Night) == k.Key.Night &&
                                                      w.Market == k.Key.Market &&
                                                      w.Operator == k.Key.Operator).Count()
                            )
                        };
            bool allotIsOK = true;
            foreach (var row in query)
            {
                string OverRelease = string.Empty;
                string OverAllot = string.Empty;
                string StopSale = string.Empty;
                string gtAllot = string.Empty;
                string DailyHotelAllot = string.Empty;
                if (!new Hotels().HotelAllotKontrol(UserData, row.Hotel, row.Market, row.Operator,
                                row.Room, string.Empty, row.CheckIn.Value, row.CheckOut.Value, row.Night.Value,
                                0, 0, 0, ref OverRelease, ref OverAllot, ref StopSale, ref gtAllot, ref DailyHotelAllot,
                                -1, -1, PackType, ref errorMsg, row.RoomCount))
                    allotIsOK = false;
            }
            return allotIsOK;
        }

        public bool AllotmentControlForMultipleRoomOH(User UserData, List<SearchResultOH> SelectBook, ref string errorMsg)
        {
            var query = from q in SelectBook
                        group q by new
                        {
                            q.Hotel,
                            q.Room,
                            q.CheckIn,
                            CheckOut = q.CheckIn.Value.AddDays(q.Night.Value),
                            Night = q.Night,
                            q.Market
                        } into k
                        select new
                        {
                            Hotel = k.Key.Hotel,
                            Room = k.Key.Room,
                            CheckIn = k.Key.CheckIn,
                            CheckOut = k.Key.CheckOut,
                            Night = k.Key.Night,
                            Market = k.Key.Market,
                            RoomCount = (
                                SelectBook.Where(w => w.Hotel == k.Key.Hotel &&
                                                      w.Room == k.Key.Room &&
                                                      w.CheckIn == k.Key.CheckIn &&
                                                      w.CheckIn.Value.AddDays(w.Night.Value) == k.Key.CheckOut &&
                                                      w.Night == k.Key.Night &&
                                                      w.Market == k.Key.Market).Count()
                            )
                        };
            bool allotIsOK = true;
            foreach (var row in query)
            {
                string OverRelease = string.Empty;
                string OverAllot = string.Empty;
                string StopSale = string.Empty;
                string gtAllot = string.Empty;
                string DailyHotelAllot = string.Empty;
                if (!new Hotels().HotelAllotKontrol(UserData, row.Hotel, row.Market, UserData.Operator,
                                row.Room, string.Empty, row.CheckIn.Value, row.CheckOut, row.Night.Value,
                                0, 0, 0, ref OverRelease, ref OverAllot, ref StopSale, ref gtAllot, ref DailyHotelAllot,
                                -1, -1, "", ref errorMsg, row.RoomCount))
                    allotIsOK = false;
            }
            return allotIsOK;
        }

        public bool HotelAllotKontrol(User UserData, string Hotel, string Market, string Operator, string Room, string Accom,
                                DateTime CheckIn, DateTime CheckOut, int Night, int CatPackID, int ARecNo, int PRecNo,
                                ref string OverRelease, ref string OverAllot, ref string StopSale, ref string htAllot, ref string DailyHotelAllot,
                                int DepCity, int ArrCity, string PackType, ref string errorMsg, int RoomCount)
        {
            string sqlstr = string.Empty;
            htAllot = "";
            bool aHotelAllotCheck = false;
            DailyHotelAllot = string.Empty;
            OverRelease = "N";
            OverAllot = "N";
            StopSale = "N";

            HotelRecord hotelParams = getHotelDetail(UserData, Hotel, ref errorMsg);
            if (hotelParams != null)
            {
                if (hotelParams.UseSysParamAllot)
                    aHotelAllotCheck = UserData.TvParams.TvParamReser.HotAllotChk;
                else
                    aHotelAllotCheck = hotelParams.HotAllotChk;
            }

            if (aHotelAllotCheck)
            {
                string nullstring = "";
                for (int i = 0; i < Night; i++)
                {
                    nullstring += "0";
                }

                int GuarCnt = 0;
                int GuarSoftCnt = 0;
                int StdCnt = 0;
                bool ResOk = true;
                bool CanMake = true;
                string UseGuar = string.Empty;
                string UseStd = string.Empty;

                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200000"))
                    sqlstr = @" Select * From dbo.ufn_HotelAllotCheck(@PriceListNo, @ARecNo, @PRecNo, @Market, @Operator, @Agency, @Office, 
                                            @ResNo, @ServiceID, @Hotel, @Room, @Accom, @AllotType, @BegDate, @EndDate, @Unit, @ReceiveDate, 
                                            @ServiceType, @FlightNo, @Class, @DailyHotelAllot, @VirtualUseGuar, @VirtualUseStd, 0, 1) ";
                else
                    sqlstr = @" Select * From dbo.ufn_HotelAllotCheck(@PriceListNo, @ARecNo, @PRecNo, @Market, @Operator, @Agency, @Office, 
                                            @ResNo, @ServiceID, @Hotel, @Room, @Accom, @AllotType, @BegDate, @EndDate, @Unit, @ReceiveDate, 
                                            @ServiceType, @FlightNo, @Class, @DailyHotelAllot, @VirtualUseGuar, @VirtualUseStd, 0) ";
                if (!string.IsNullOrEmpty(UserData.TvParams.TvParamSystem.SejourDBName) || IsAllotmentDefined(Hotel, Room, -1, Market, CheckIn, Night))
                {
                    try
                    {
                        List<ufnHotelAllotCheckRecord> hotelAllots = new List<ufnHotelAllotCheckRecord>();
                        Database db = (Database)DatabaseFactory.CreateDatabase();
                        DbCommand dbCommand = db.GetSqlStringCommand(sqlstr);

                        try
                        {
                            db.AddInParameter(dbCommand, "PriceListNo", DbType.Int32, CatPackID > 0 ? CatPackID : SqlInt32.Null);
                            db.AddInParameter(dbCommand, "ARecNo", DbType.Int32, ARecNo > 0 ? ARecNo : SqlInt32.Null);
                            db.AddInParameter(dbCommand, "PRecNo", DbType.Int32, PRecNo > 0 ? PRecNo : SqlInt32.Null);
                            db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                            db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                            db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                            db.AddInParameter(dbCommand, "Office", DbType.String, UserData.OprOffice);
                            db.AddInParameter(dbCommand, "ResNo", DbType.String, string.Empty);
                            db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, 0);
                            db.AddInParameter(dbCommand, "Hotel", DbType.String, CatPackID > 0 ? string.Empty : Hotel);
                            db.AddInParameter(dbCommand, "Room", DbType.String, CatPackID > 0 ? string.Empty : Room);
                            db.AddInParameter(dbCommand, "Accom", DbType.String, CatPackID > 0 ? string.Empty : Accom);
                            db.AddInParameter(dbCommand, "AllotType", DbType.String, "%");
                            db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, CheckIn);
                            db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, CheckIn.Date.AddDays(Night));
                            db.AddInParameter(dbCommand, "Unit", DbType.Int16, RoomCount);
                            db.AddInParameter(dbCommand, "ReceiveDate", DbType.DateTime, DateTime.Now.Date);
                            db.AddInParameter(dbCommand, "ServiceType", DbType.String, "HOTEL");
                            db.AddInParameter(dbCommand, "FlightNo", DbType.String, string.Empty);
                            db.AddInParameter(dbCommand, "Class", DbType.String, string.Empty);
                            db.AddInParameter(dbCommand, "DailyHotelAllot", DbType.String, string.Empty);
                            db.AddInParameter(dbCommand, "VirtualUseGuar", DbType.String, nullstring);
                            db.AddInParameter(dbCommand, "VirtualUseStd", DbType.String, nullstring);
                            using (IDataReader R = db.ExecuteReader(dbCommand))
                            {
                                while (R.Read())
                                {
                                    ufnHotelAllotCheckRecord record = new ufnHotelAllotCheckRecord();
                                    record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                                    record.Room = Conversion.getStrOrNull(R["Room"]);
                                    record.AllotType = Conversion.getInt16OrNull(R["AllotType"]);
                                    record.AllotDate = Conversion.getDateTimeOrNull(R["AllotDate"]);
                                    record.Avail = Conversion.getInt16OrNull(R["Avail"]);
                                    record.AAllot = Conversion.getInt16OrNull(R["aAllot"]);
                                    record.AUse = Conversion.getInt16OrNull(R["aUse"]);
                                    record.Release = Conversion.getInt16OrNull(R["Release"]);
                                    record.Type = Conversion.getStrOrNull(R["Type"]);
                                    record.ErrorCode = Conversion.getInt16OrNull(R["ErrorCode"]);
                                    record.CanMakeRes = Equals(R["CanMakeRes"], "Y");
                                    hotelAllots.Add(record);
                                }
                            }

                        }
                        catch (Exception Ex)
                        {
                            errorMsg = Ex.Message;
                        }
                        finally
                        {
                            dbCommand.Connection.Close();
                            dbCommand.Dispose();
                        }

                        foreach (var r in hotelAllots.AsEnumerable())
                        {
                            ResOk = ResOk && (r.ErrorCode.Value == 0 && r.CanMakeRes.Value);
                            if (r.ErrorCode == 100)
                                StopSale = "Y";
                            CanMake = CanMake && r.CanMakeRes.Value;

                            if (r.AllotType == 0)
                            {
                                GuarCnt++;
                                UseGuar += "G";
                                UseStd += " ";
                                DailyHotelAllot += r.AllotType.ToString();
                            }
                            else
                                if (r.AllotType == 1)
                                {
                                    GuarSoftCnt++;
                                    UseGuar += "G";
                                    UseStd += " ";
                                    DailyHotelAllot += r.AllotType;
                                }
                                else
                                {
                                    UseGuar += " ";
                                    UseStd += "S";
                                    StdCnt++;
                                    DailyHotelAllot += "2";
                                }

                            if (OverRelease == "N" && (r.ErrorCode == 3 || r.ErrorCode == 4 || r.ErrorCode == 30 || r.ErrorCode == 40 || r.ErrorCode == 300 || r.ErrorCode == 400))
                                OverRelease = "Y";

                            if (OverAllot == "N" && (r.ErrorCode == 2 || r.ErrorCode == 3 || r.ErrorCode == 20 || r.ErrorCode == 30 || r.ErrorCode == 200 || r.ErrorCode == 300))
                                OverAllot = "Y";

                            if (StopSale == "N" && (r.ErrorCode == 200 || r.ErrorCode == 300 || r.ErrorCode == 400))
                                StopSale = "Y";
                        }


                        if (!ResOk && !CanMake)
                        {
                            if (Equals(PackType, "T"))
                                return true;
                            else
                            {
                                errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                                return false;
                            }
                        }

                        if (OverAllot == "Y")
                            htAllot = "O";
                        else
                            if (StdCnt == hotelAllots.Count)
                                htAllot = "S";
                            else
                                if (GuarCnt == hotelAllots.Count || GuarSoftCnt == hotelAllots.Count)
                                    htAllot = "G";
                                else
                                    htAllot = "M";
                    }
                    catch (Exception Ex)
                    {
                        errorMsg = Ex.Message;
                        return false;
                    }
                }
                else
                {
                    if (Equals(PackType, "T"))
                    {
                        htAllot = "M";
                        return true;
                    }
                    else
                    {
                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotFound").ToString();
                        return false;
                    }
                }
            }
            else
            {
                htAllot = "M";
                string AType = htAllot;
                string DailyAType = "";

                for (int i = 0; i < Night; i++)
                {
                    string tsql =
@"
Declare @Holpack varChar(10)

Select @Holpack=Holpack From CatalogPack (NOLOCK) Where RecID=@CatPackId

Select top 1 AllotType From HotelSeason (NOLOCK) S
LEFT join HotelNetPrice P (NOLOCK) on P.Hotel = S.Hotel and P.SeasonID = S.RecID 
Where   S.Hotel=@Hotel And 
        @Date between S.BegDate and S.EndDate and
        (S.Market=@Market or @Market in (Select Market From HotelSeasonMark (NOLOCK) where Hotel = S.Hotel and SeasonID = S.RecID)) and 
        (
            (IsNull(S.ContType,0)=0) or 
            (IsNull(S.ContType,0)=1 and Exists(Select Null From HotelSeasonPack (NOLOCK) Where HotelSeasonPack.SeasonID=S.RecID and HotelSeasonPack.HolPack=@Holpack) ) 
        ) and
        Exists(Select null From HotelPeriod (NOLOCK) Where SeasonID= S.RecID and @Date Between BegDate and EndDate) and 
        P.Room = @Room 
Order by PricePriority    
";
                    Database db = (Database)DatabaseFactory.CreateDatabase();
                    DbCommand dbCommand = db.GetSqlStringCommand(tsql);

                    try
                    {
                        db.AddInParameter(dbCommand, "Hotel", DbType.AnsiString, Hotel);
                        db.AddInParameter(dbCommand, "Date", DbType.DateTime, CheckIn.AddDays(i));
                        db.AddInParameter(dbCommand, "Market", DbType.AnsiString, Market);
                        db.AddInParameter(dbCommand, "Room", DbType.AnsiString, Room);
                        db.AddInParameter(dbCommand, "CatPackId", DbType.Int32, CatPackID);
                        object allotType = db.ExecuteScalar(dbCommand);
                        if (allotType == null)
                        {
                            AType = "X";
                            DailyAType = "";
                            for (int j = 0; j < Night; j++) { DailyAType += "X"; }
                            break;
                        }
                        else
                            if ((string)allotType == "G")
                                DailyAType += "0";
                            else
                                DailyAType += "2";
                    }
                    catch
                    {
                        AType = "X";
                        DailyAType = "";
                        for (int j = 0; j < Night; j++) { DailyAType += "X"; }
                        break;
                    }
                    finally
                    {
                        dbCommand.Connection.Close();
                        dbCommand.Dispose();
                    }
                    if (Common.Pos(DailyAType, "0") > -1 && Common.Pos(DailyAType, "2") > -1)
                        AType = "M";
                    else
                        if (Common.Pos(DailyAType, "0") > -1)
                            AType = "G";
                        else
                            if (Common.Pos(DailyAType, "2") > -1)
                                AType = "S";
                            else
                                AType = "X";
                }

                htAllot = AType;
                DailyHotelAllot = DailyAType;
            }
            return true;
        }
        public string StopSaleControl(User UserData,string hotelCode, string room, string accom, string board, string market, DateTime checkIn, DateTime checkOut,int night, ref string errorMsg)
        {
            Database db = (Database)DatabaseFactory.CreateDatabase();
            string tsql = @"Select top 1 MayIgnore From Stopsale 
                      Where (Hotel=@Hotel or Hotel='')  and (Room=@Room or Room='') 
                        and (Location is Null )
                        and (Accom=@Accom or Accom='') and (Board=@Board or Board='') 
                        and (MinNight is null or MinNight<=@Night) and (MaxNight is NULL or MaxNight>=@Night)
                        and (Market=@Market or Market='') and Agency='' and Status='Y' 
                        and ( PriceListNo is Null) 
                        and  ( AppType='A' and  (((@checkIn between BegDate and EndDate) or (@checkOut between BegDate and EndDate) 
                            or (BegDate between @checkIn and @checkOut) or (EndDate between @checkIn and @checkOut)))
                         or  ( AppType='C' and @checkIn between BegDate and EndDate )                               
                        )  ";
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                
                db.AddInParameter(dbCommand, "Hotel", DbType.AnsiString, hotelCode);
                db.AddInParameter(dbCommand, "Room", DbType.AnsiString, room);
                db.AddInParameter(dbCommand, "Accom", DbType.AnsiString, accom);
                db.AddInParameter(dbCommand, "Board", DbType.AnsiString, board);
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Night", DbType.Int32,night);
                db.AddInParameter(dbCommand, "checkIn", DbType.DateTime, checkIn);
                db.AddInParameter(dbCommand, "checkOut", DbType.DateTime, checkOut);
                DataSet ds= db.ExecuteDataSet(dbCommand);
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 0)
                    return "Ok";
                else if (dt.Rows[0][0].ToString() == "Y")
                    return "Request";
                else
                    return "No";
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return ex.Message;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }
        public decimal GetHotelSeasonMaxChildAge(string pMarket, string pHotelCode, DateTime pBegDate,ref string errorMsg)
        {
            string tsql = @"Select 
                            ISNULL(ChdG4Age2,ISNULL(ChdG3Age2,ISNULL(ChdG2Age2,ISNULL(ChdG1Age2,0)))) as MaxChildAge
                            From HotelSeason (NOLOCK) 
                             where Hotel=@pHotelCode and Market=@pMarket
	                            and @pBegDate BETWEEN BegDate and EndDate
	                            order by PricePriority,SalePriority";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pHotelCode", DbType.String, pHotelCode);
                db.AddInParameter(dbCommand, "pMarket", DbType.String, pMarket);
                db.AddInParameter(dbCommand, "pBegDate", DbType.DateTime, pBegDate);
                object retObj = db.ExecuteScalar(dbCommand);
                if (retObj != null)
                    return (decimal)retObj;
                else
                    return 0;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return 0;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }
        public bool getHotelWithOutLadyOption(string HotelCode, ref string errorMsg)
        {
            string tsql = @"Select WithOutLady From Hotel (NOLOCK) Where Code = @Hotel";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Hotel", DbType.String, HotelCode);
                return (bool)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return true;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Int16? getHotelDefConf(User UserData, string Hotel, string AllotType, string Room, string Board, string Accom, string Supplier, DateTime Date, string PLMarket, int? night, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040015041"))
                tsql =
@"
Select ConfStat=DefConf 
From HotelDefConf (NOLOCK) 
Where Market = @Market And 
  Hotel = @Hotel and 
  (AllotType = '' or AllotType = @AllotType) and 
  (Room = '' or Room = @Room) and 
  (Board = '' or Board = @Board) and 
  (Accom = '' or Accom = @Accom) and 
  (Supplier = '' or Supplier = @Supplier) and 
  (@Date Between BegDate and EndDate) and
  @Night Between isnull(MinNight, 0) and isnull(MaxNight, 999)
Order By AllotType desc, Room desc, Board desc, Accom desc, Supplier desc 
";
            else
                tsql =
@"
Select ConfStat=DefConf 
From HotelDefConf (NOLOCK) 
Where Market = @Market And 
  Hotel = @Hotel and 
  (AllotType = '' or AllotType = @AllotType) and 
  (Room = '' or Room = @Room) and 
  (Board = '' or Board = @Board) and 
  (Accom = '' or Accom = @Accom) and 
  (Supplier = '' or Supplier = @Supplier) and 
  @Date Between BegDate and EndDate 
Order By AllotType desc, Room desc, Board desc, Accom desc, Supplier desc 
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {

                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Room", DbType.String, Room);
                db.AddInParameter(dbCommand, "AllotType", DbType.String, AllotType);
                db.AddInParameter(dbCommand, "Board", DbType.String, Board);
                db.AddInParameter(dbCommand, "Accom", DbType.String, Accom);
                db.AddInParameter(dbCommand, "Supplier", DbType.String, Supplier);
                db.AddInParameter(dbCommand, "Date", DbType.DateTime, Date);
                db.AddInParameter(dbCommand, "Market", DbType.String, PLMarket);
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040015040"))
                    db.AddInParameter(dbCommand, "Night", DbType.Int32, night);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    object val = db.ExecuteScalar(dbCommand);
                    return Conversion.getInt16OrNull(val);
                    //if (R.Read())
                    //{
                    //    return Conversion.getInt16OrNull(R["ConfStat"]);
                    //}
                    //else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Int16? getHotelDefaltStatus(string Hotel, ref string errorMsg)
        {
            string tsql = @"Select ConfStat From Hotel (NOLOCK) Where Code = @Code ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, Hotel);
                return (Int16)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Int16? getHotelStatus(User UserData, string Hotel, string AllotType, string Room, string Board, string Accom, string Supplier, DateTime Date, string PLMarket, string DailyAllot, int? night, ref string errorMsg)
        {

            if (AllotType == "M")
            {
                Int16? tmpStat = getHotelDefConf(UserData, Hotel, "", Room, Board, Accom, Supplier, Date, PLMarket, night, ref errorMsg);
                if (tmpStat != null)
                    return tmpStat.Value;
                else
                    return null;
            }
            else
            {
                string CurAllotType = string.Empty;
                Int16 TmpConfStat = 0;
                for (int i = 0; i < DailyAllot.Length; i++)
                {
                    if (CurAllotType != DailyAllot[i].ToString())
                    {
                        CurAllotType = DailyAllot[i].ToString();
                        string dailyAllotType = string.Empty;
                        switch (DailyAllot[i].ToString())
                        {
                            case "2":
                                dailyAllotType = "S";
                                break;
                            case "1":
                                dailyAllotType = "G";
                                break;
                            case "0":
                                dailyAllotType = "G";
                                break;
                        }
                        Int16? tmpStat = getHotelDefConf(UserData, Hotel, dailyAllotType, Room, Board, Accom, Supplier, Date.AddDays(i), PLMarket, night, ref errorMsg);
                        if (tmpStat != null)
                            TmpConfStat = tmpStat.Value;
                        else
                        {
                            Int16? hotelStat = getHotelDefaltStatus(Hotel, ref errorMsg);

                            if (hotelStat == null)
                                TmpConfStat = 0;
                            else
                                TmpConfStat = hotelStat.Value;
                        }
                    }
                    if (TmpConfStat != 1)
                        break;
                }
                return TmpConfStat;
            }
        }

        public Int16? getMayIgnore(string Hotel, string Room, string Accom, string Board, string Market, string Agency, int? CatPackID, int HotLocation, DateTime? CheckIn, Int16? Night, bool B2C, string AllotType, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (AllotType == "S")
                tsql = "Select dbo.ufn_StopSaleCnt(@Hotel, @Room, @Accom, @Board, @Market, @Agency, @CatPackID, @HotLocation, 2, @CheckIn, @CheckIn+@Night, 0, @SaleResource)";
            else
                tsql = "Select dbo.ufn_StopSaleCnt(@Hotel, @Room, @Accom, @Board, @Market, @Agency, @CatPackID, @HotLocation, -2, @CheckIn, @CheckIn+@Night, 0, @SaleResource)";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Room", DbType.String, Room);
                db.AddInParameter(dbCommand, "Accom", DbType.String, Accom);
                db.AddInParameter(dbCommand, "Board", DbType.String, Board);
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                db.AddInParameter(dbCommand, "HotLocation", DbType.Int32, HotLocation);
                db.AddInParameter(dbCommand, "CheckIn", DbType.DateTime, CheckIn);
                db.AddInParameter(dbCommand, "Night", DbType.Int16, Night);
                db.AddInParameter(dbCommand, "SaleResource", DbType.Int16, B2C ? 2 : 1);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    object _retVal = db.ExecuteScalar(dbCommand);
                    return Conversion.getInt16OrNull(_retVal);
                    //if (!retVal.HasValue)
                    //    return null;
                    //else return (retVal == 1) ? true : false;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getHotelLocationCodeName(User UserData, int? City, int? Country, int? CatPackID, int? HotLocation, string packType, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0035, VersionControl.Equality.gt))
            {
                if (Country.HasValue)
                {
                    #region Country
                    tsql =
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID, isnull(dbo.FindLocalName(NameLID, @Market), Name) [Name], Country, City, Town, Village 
Into #Location
From Location (NOLOCK)         

Select Distinct
    Name=(Select Name From #Location Where RecID = L.City) +
            Case When L.Town is not null Then ' => ' + (Select Name From #Location Where RecID = L.Town) Else '' End +
            Case When L.Village is not null Then ' => ' + (Select Name From #Location Where RecID = L.Village) Else '' End,
    RecID=H.Location
From Hotel (NOLOCK) H
Join #Location L ON L.RecID = H.Location
Where H.Status='Y'
  And H.HotelType='H'      
";
                    tsql += string.Format(
@"
  And L.Country={0}
", Country.Value);
                    #endregion
                }
                else
                {
                    if (CatPackID != null)
                        tsql =
    @"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID, isnull(dbo.FindLocalName(NameLID, @Market), Name) [Name], Country, City, Town, Village 
Into #Location
From Location (NOLOCK)         
Where @City is null or City=@City

Select Distinct
    Name =  (Case When @City is null Then (Select Name From #Location Where RecID = L.Country) + ' => ' Else '' End) +
		    (Select Name From #Location Where RecID = L.City) +
		    Case When L.Town is not null Then ' => ' + (Select Name From #Location Where RecID = L.Town) Else '' End +
		    Case When L.Village is not null Then ' => ' + (Select Name From #Location Where RecID = L.Village) Else '' End,
		    RecID=H.Location
From Hotel (NOLOCK) H
Join CatPackPlanSer CPPS (NOLOCK) ON CPPS.ServiceItem=H.Code And CPPS.CatPackID=@CatPackID And CPPS.Service='HOTEL'
Join #Location L ON L.RecID = H.Location
Where 
    H.Status = 'Y' AND
    H.HotelType = 'H' 
";
                    else
                        tsql =
    @"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID, isnull(dbo.FindLocalName(NameLID, @Market), Name) [Name], Country, City, Town, Village 
Into #Location
From Location (NOLOCK)         

Select Distinct
    Name =  (Select Name From #Location Where RecID = L.City) +
            Case When L.Town is not null Then ' => ' + (Select Name From #Location Where RecID = L.Town) Else '' End +
            Case When L.Village is not null Then ' => ' + (Select Name From #Location Where RecID = L.Village) Else '' End,
            RecID=H.Location
From Hotel (NOLOCK) H
Join #Location L ON L.RecID=H.Location
Where H.Status='Y' And
    H.HotelType='H'
";
                    if (string.Equals(packType, "T"))
                    {
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Qasswa))
                            tsql += " And isnull(H.Chk1,0)=0 ";
                        else if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet))
                            tsql += " And isnull(H.Chk1,0)=1 ";
                        else
                            tsql += " And isnull(H.Chk1,1)=1 ";
                    }
                }
            }
            else
            {
                if (CatPackID == null)
                    tsql = @"if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
                        Select RecID, isnull(dbo.FindLocalName(NameLID, @Market), Name) [Name], Country, City, Town, Village 
                        Into #Location
                        From Location (NOLOCK)         
                        Where @City is null or City=@City

                        Select Distinct
                            Name =  (Case When @City is null Then (Select Name From #Location Where RecID = L.Country) + ' => ' Else '' End) +
		                            (Select Name From #Location Where RecID = L.City) +
		                            Case When L.Town is not null Then ' => ' + (Select Name From #Location Where RecID = L.Town) Else '' End +
		                            Case When L.Village is not null Then ' => ' + (Select Name From #Location Where RecID = L.Village) Else '' End,
		                            RecID=H.Location
                        From Hotel (NOLOCK) H
                        Join #Location L ON L.RecID = H.Location
                        Where 
                            H.Status = 'Y' AND
                            H.HotelType = 'H' ";
                else
                    tsql = @"if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
                        Select RecID, isnull(dbo.FindLocalName(NameLID, @Market), Name) [Name], Country, City, Town, Village 
                        Into #Location
                        From Location (NOLOCK)         

                        Select Distinct
                            Name =  (Select Name From #Location Where RecID = L.City) +
                                    Case When L.Town is not null Then ' => ' + (Select Name From #Location Where RecID = L.Town) Else '' End +
                                    Case When L.Village is not null Then ' => ' + (Select Name From #Location Where RecID = L.Village) Else '' End,
                                    RecID=H.Location
                        From Hotel (NOLOCK) H
                        Join CatPackPlanSer CPPS (NOLOCK) ON CPPS.ServiceItem=H.Code And CPPS.CatPackID=@CatPackID And CPPS.Service='HOTEL'
                        Join #Location L ON L.RecID = H.Location
                        Where 
                            H.Status = 'Y' AND
                            H.HotelType = 'H' 
                            ";
            }
            if (!Country.HasValue && HotLocation.HasValue)
                tsql += string.Format(" AND L.City={0} ", HotLocation.Value.ToString());
            List<CodeName> retVal = new List<CodeName>();
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "City", DbType.Int32, City);
                if (CatPackID != null)
                    db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        int recID = (int)R["RecID"];
                        string name = Conversion.getStrOrNull(R["Name"]);
                        retVal.Add(new CodeName
                        {
                            Code = recID.ToString(),
                            Name = name
                        });
                    }
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return retVal;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<int?> getCultureTourAddHotelList(int? city, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"
Select H.Location
From Hotel (NOLOCK) H                                
Where 
	H.Status='Y' AND
	H.HotelType='H' AND        
    isnull(H.Chk1,1)=1
";
            List<int?> retVal = new List<int?>();
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        retVal.Add(Conversion.getInt32OrNull(R["Location"]));
                    }
                    return retVal;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return retVal;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getHotelLocationJSonForCultureTourAddHotel(string Market, ResDataRecord ResData, ref string errorMsg)
        {
            int? depLocation = new Locations().getLocationForCity(ResData.ResMain.ArrCity.HasValue ? ResData.ResMain.ArrCity.Value : -1, ref errorMsg);

            List<Location> list = CacheObjects.getLocationList(Market);
            List<int?> hotLocation = new Hotels().getCultureTourAddHotelList(ResData.ResMain.ArrCity.HasValue ? ResData.ResMain.ArrCity.Value : -1, ref errorMsg);

            List<Location> location = (from q in list.AsEnumerable()
                                       join q1 in hotLocation on q.RecID equals q1.Value
                                       where q.Type > 1 && q.City == depLocation
                                       select q).ToList<Location>();
            string retVal = string.Empty;
            foreach (Location r in location.OrderBy(o => new { o.Town, o.Village }))
            {
                string name = string.Empty;

                if (r.Village.HasValue)
                    name += list.Where(w => w.RecID == r.Village.Value).Select(s => s.NameL).FirstOrDefault();
                if (r.Town.HasValue)
                    name = list.Where(w => w.RecID == r.Town.Value).Select(s => s.NameL).FirstOrDefault() + (name.Length > 0 ? " -> " : "") + name;
                if (r.City.HasValue)
                    name = list.Where(w => w.RecID == r.City.Value).Select(s => s.NameL).FirstOrDefault() + (name.Length > 0 ? " -> " : "") + name;
                if (depLocation.HasValue)
                    name = list.Where(w => w.RecID == r.Country).Select(s => s.NameL).FirstOrDefault() + (name.Length > 0 ? " -> " : "") + name;

                retVal += ",{\"RecID\":" + r.RecID.ToString() + ",\"Name\":\"" + name + "\"}";
            }
            retVal = retVal.Length > 0 ? "[" + retVal.ToString().Remove(0, 1) + "]" : "";
            return retVal;
        }

        //public List<CodeName> getHotelLocation(string Market, ResDataRecord ResData, int? RecID, ref string errorMsg)
        //{
        //    ResServiceRecord ResService = ResData.ResService.Find(f => f.RecID == RecID.Value);
        //    List<CodeName> hotelLocation = new List<CodeName>();
        //    List<Location> list = CacheObjects.getLocationList(Market);
        //    List<Location> location = (from q in list.AsEnumerable()
        //                               where q.Type > 1 && (!RecID.HasValue || q.City == list.Find(f => f.RecID == ResService.DepLocation.Value).City.Value)
        //                               select q).ToList<Location>();
        //    foreach (Location r in location.OrderBy(o => new { o.Town, o.Village }))
        //    {
        //        string name = string.Empty;

        //        if (r.Village.HasValue) name += list.Where(w => w.RecID == r.Village.Value).Select(s => s.NameL).FirstOrDefault();
        //        if (r.Town.HasValue) name = list.Where(w => w.RecID == r.Town.Value).Select(s => s.NameL).FirstOrDefault() + (name.Length > 0 ? " -> " : "") + name;
        //        if (r.City.HasValue) name = list.Where(w => w.RecID == r.City.Value).Select(s => s.NameL).FirstOrDefault() + (name.Length > 0 ? " -> " : "") + name;
        //        if (!RecID.HasValue) name = list.Where(w => w.RecID == r.Country).Select(s => s.NameL).FirstOrDefault() + (name.Length > 0 ? " -> " : "") + name;

        //        hotelLocation.Add(new CodeName
        //        {
        //            Code = r.RecID.ToString(),
        //            Name = name
        //        });
        //    }

        //    return hotelLocation;
        //}

        public string getHotelLocationJSon(string Market, ResDataRecord ResData, int? RecID, ref string errorMsg)
        {
            ResServiceRecord ResService = ResData.ResService.Find(f => f.RecID == RecID.Value);

            List<Location> list = CacheObjects.getLocationList(Market);
            List<Location> location = (from q in list.AsEnumerable()
                                       where q.Type > 1 && (!RecID.HasValue || q.City == list.Find(f => f.RecID == ResService.DepLocation.Value).City.Value)
                                       select q).ToList<Location>();
            string retVal = string.Empty;
            foreach (Location r in location.OrderBy(o => new { o.Town, o.Village }))
            {
                string name = string.Empty;

                if (r.Village.HasValue)
                    name += list.Where(w => w.RecID == r.Village.Value).Select(s => s.NameL).FirstOrDefault();
                if (r.Town.HasValue)
                    name = list.Where(w => w.RecID == r.Town.Value).Select(s => s.NameL).FirstOrDefault() + (name.Length > 0 ? " -> " : "") + name;
                if (r.City.HasValue)
                    name = list.Where(w => w.RecID == r.City.Value).Select(s => s.NameL).FirstOrDefault() + (name.Length > 0 ? " -> " : "") + name;
                if (!RecID.HasValue)
                    name = list.Where(w => w.RecID == r.Country).Select(s => s.NameL).FirstOrDefault() + (name.Length > 0 ? " -> " : "") + name;

                retVal += ",{\"RecID\":" + r.RecID.ToString() + ",\"Name\":\"" + name + "\"}";
            }
            retVal = retVal.Length > 0 ? "[" + retVal.ToString().Remove(0, 1) + "]" : "";
            return retVal;
        }

        //public IEnumerable getRoomJSon(string Market, string Hotel, int? CatPackID, ref string errorMsg)
        //{
        //    if (string.IsNullOrEmpty(Hotel))
        //    {
        //        List<RoomRecord> list = new Hotels().getRoomList(Market, ref errorMsg);
        //        string retVal = string.Empty;
        //        var query = from q in list.AsEnumerable()
        //                    group q by new { Code = q.Code, NameL = q.NameL } into k
        //                    select new { Code = k.Key.Code, Name = k.Key.NameL };
        //        return query;
        //    }
        //    else
        //    {
        //        List<HotelRoomRecord> list = new List<HotelRoomRecord>();
        //        if (CatPackID.HasValue)
        //            list = new Hotels().getPriceListRoomList(Market, Hotel, CatPackID, ref errorMsg);
        //        else list = new Hotels().getHotelRoomList(Market, Hotel, ref errorMsg);
        //        string retVal = string.Empty;
        //        var query = from q in list.AsEnumerable()
        //                    group q by new { Code = q.Code, NameL = q.NameL } into k
        //                    select new { Code = k.Key.Code, Name = k.Key.NameL };
        //        return query;
        //    }
        //}

        public string getRoomJSon(string Market, string Hotel, int? CatPackID, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel))
            {
                List<RoomRecord> list = new Hotels().getRoomList(Market, ref errorMsg);
                string retVal = string.Empty;
                var query = from q in list.AsEnumerable()
                            group q by new { Code = q.Code, NameL = q.NameL } into k
                            select new { Code = k.Key.Code, Name = k.Key.NameL };
                retVal = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                return retVal;
            }
            else
            {
                List<HotelRoomRecord> list = new List<HotelRoomRecord>();
                if (CatPackID.HasValue)
                    list = new Hotels().getPriceListRoomList(Market, Hotel, CatPackID, ref errorMsg);
                else
                    list = new Hotels().getHotelRoomList(Market, Hotel, ref errorMsg);
                string retVal = string.Empty;
                var query = from q in list.AsEnumerable()
                            group q by new { Code = q.Code, NameL = q.NameL } into k
                            select new { Code = k.Key.Code, Name = k.Key.NameL };
                retVal = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                return retVal;
            }
        }

        //public IEnumerable getAccomJSon(string Market, string Hotel, string Room, int? CatPackID, ref string errorMsg)
        //{
        //    List<HotelAccomRecord> list = new List<HotelAccomRecord>();
        //    if (CatPackID.HasValue)
        //        list = new Hotels().getHotelAccomPriceList(Market, Hotel, Room, CatPackID, ref errorMsg);
        //    else list = new Hotels().getHotelAccom(Market, Hotel, ref errorMsg);
        //    string retVal = string.Empty;
        //    var query = from q in list
        //                group q by new { Code = q.Code, Name = q.LocalName } into k
        //                select new { Code = k.Key.Code, Name = k.Key.Name };
        //    return query;
        //}

        public string getAccomJSon(string Market, string Hotel, string Room, int? CatPackID, ref string errorMsg)
        {
            List<HotelAccomRecord> list = new List<HotelAccomRecord>();
            if (CatPackID.HasValue)
                list = new Hotels().getHotelAccomPriceList(Market, Hotel, Room, CatPackID, ref errorMsg);
            else
                list = new Hotels().getHotelAccom(Market, Hotel, ref errorMsg);
            string retVal = string.Empty;
            var query = from q in list
                        group q by new { Code = q.Code, Name = q.LocalName } into k
                        select new { Code = k.Key.Code, Name = k.Key.Name };
            retVal = Newtonsoft.Json.JsonConvert.SerializeObject(query);
            return retVal;
        }

        //public IEnumerable getBoardJSon(string Market, string Hotel, int? CatPackID, ref string errorMsg)
        //{
        //    List<HotelBoardRecord> list = new List<HotelBoardRecord>();
        //    if (CatPackID.HasValue)
        //        list = new Hotels().getHotelBoardListPriceList(Market, Hotel, CatPackID, ref errorMsg);
        //    else list = new Hotels().getHotelBoardList(Market, Hotel, ref errorMsg);
        //    string retVal = string.Empty;
        //    var query = from q in list
        //                group q by new { Code = q.Code, NameL = q.NameL } into k
        //                select new { Code = k.Key.Code, Name = k.Key.NameL };
        //    return query;
        //}

        public string getBoardJSon(string Market, string Hotel, int? CatPackID, ref string errorMsg)
        {
            List<HotelBoardRecord> list = new List<HotelBoardRecord>();
            if (CatPackID.HasValue)
                list = new Hotels().getHotelBoardListPriceList(Market, Hotel, CatPackID, ref errorMsg);
            else
                list = new Hotels().getHotelBoardList(Market, Hotel, ref errorMsg);
            string retVal = string.Empty;
            var query = from q in list
                        group q by new { Code = q.Code, NameL = q.NameL } into k
                        select new { Code = k.Key.Code, Name = k.Key.NameL };
            retVal = Newtonsoft.Json.JsonConvert.SerializeObject(query);
            return retVal;
        }

        public List<CodeName> getLocationHotelCodeName(string Market, int HotelLocation, int? CatPackID, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (CatPackID == null)
                tsql = @"   Select * From (
                                Select Distinct H.Code, Name=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name)
                                From Hotel (NOLOCK) H                                
                                Where 
	                                H.Status='Y' AND
	                                H.HotelType='H' AND
                                    H.Location=@HotelLocation
                            ) X
                            Order By Name ";
            else
                tsql = @"   Select Distinct Name, Code From (
                                Select Code=CPA.Hotel, Name=ISNULL(dbo.FindLocalName(H.NameLID, @Market), H.Name)	
                                From CatPriceA CPA (NOLOCK)
                                Join Hotel H (NOLOCK) ON H.Code=CPA.Hotel
                                Where CatPackID=@CatPackID
                                  And CPA.Status=1
                                  And H.Location=@HotelLocation
                            ) X
                            Order By Name ";
            List<CodeName> retVal = new List<CodeName>();
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "HotelLocation", DbType.Int32, HotelLocation);
                if (CatPackID != null)
                    db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        retVal.Add(new CodeName
                        {
                            Code = Conversion.getStrOrNull(R["Code"]),
                            Name = Conversion.getStrOrNull(R["Name"])
                        });
                    }
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return retVal;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getTrfLocationHotelCodeName(string Market, int HotelTrfLocation, int? CatPackID, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (CatPackID == null)
                tsql = @"   Select * From (
                                Select Distinct H.Code, Name=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name)
                                From Hotel (NOLOCK) H                                
                                Where 
	                                H.Status='Y' AND
	                                H.HotelType='H' AND
                                    H.TrfLocation=@HotelLocation
                            ) X
                            Order By Name ";
            else
                tsql = @"   Select Distinct Name, Code From (
                                Select Code=CPA.Hotel, Name=ISNULL(dbo.FindLocalName(H.NameLID, @Market), H.Name)	
                                From CatPriceA CPA (NOLOCK)
                                Join Hotel H (NOLOCK) ON H.Code=CPA.Hotel
                                Where CatPackID=@CatPackID
                                  And CPA.Status=1
                                  And H.TrfLocation=@HotelLocation
                            ) X
                            Order By Name ";
            List<CodeName> retVal = new List<CodeName>();
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "HotelLocation", DbType.Int32, HotelTrfLocation);
                if (CatPackID != null)
                    db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        retVal.Add(new CodeName
                        {
                            Code = Conversion.getStrOrNull(R["Code"]),
                            Name = Conversion.getStrOrNull(R["Name"])
                        });
                    }
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return retVal;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getLocationHotelCodeNameForCultureTourAddHotel(User UserData, int HotelLocation, int? CatPackID, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0035, VersionControl.Equality.gt))
            {
                if (CatPackID == null)
                    tsql = @"Select * From (
                                Select H.Code, Name=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name),H.Chk1
                                From Hotel (NOLOCK) H                                
                                Where 
	                                H.Status='Y' AND
	                                H.HotelType='H' AND
                                    H.Location=@HotelLocation AND
                                    isnull(H.Chk1,1)=1
                            ) X
                            Order By Name ";
                else
                    tsql = @"Select Distinct Name, Code, Chk1 From (
                                Select Code=CPA.Hotel, Name=ISNULL(dbo.FindLocalName(H.NameLID, @Market), H.Name),H.Chk1
                                From CatPriceA CPA (NOLOCK)
                                Join Hotel H (NOLOCK) ON H.Code=CPA.Hotel
                                Where CatPackID=@CatPackID
                                  And CPA.Status=1
                                  And H.Location=@HotelLocation
                                  And isnull(H.Chk1,1)=1
                            ) X
                            Order By Name ";
            }
            else
            {
                if (CatPackID == null)
                    tsql = @"   Select * From (
                                Select Distinct H.Code, Name=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name)
                                From Hotel (NOLOCK) H                                
                                Where 
	                                H.Status='Y' AND
	                                H.HotelType='H' AND
                                    H.Location=@HotelLocation
                            ) X
                            Order By Name ";
                else
                    tsql = @"   Select Distinct Name, Code From (
                                Select Code=CPA.Hotel, Name=ISNULL(dbo.FindLocalName(H.NameLID, @Market), H.Name)	
                                From CatPriceA CPA (NOLOCK)
                                Join Hotel H (NOLOCK) ON H.Code=CPA.Hotel
                                Where CatPackID=@CatPackID
                                  And CPA.Status=1
                                  And H.Location=@HotelLocation
                            ) X
                            Order By Name ";
            }
            List<CodeName> retVal = new List<CodeName>();
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "HotelLocation", DbType.Int32, HotelLocation);
                if (CatPackID != null)
                    db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0035, VersionControl.Equality.gt))
                        {
                            bool? chk1 = Conversion.getBoolOrNull(R["Chk1"]);
                            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet))
                            {
                                if (chk1.HasValue && chk1.Value == true)
                                    retVal.Add(new CodeName
                                    {
                                        Code = Conversion.getStrOrNull(R["Code"]),
                                        Name = Conversion.getStrOrNull(R["Name"])
                                    });
                            }
                            else
                            {
                                retVal.Add(new CodeName
                                {
                                    Code = Conversion.getStrOrNull(R["Code"]),
                                    Name = Conversion.getStrOrNull(R["Name"])
                                });
                            }
                        }
                        else
                        {
                            retVal.Add(new CodeName
                            {
                                Code = Conversion.getStrOrNull(R["Code"]),
                                Name = Conversion.getStrOrNull(R["Name"])
                            });
                        }
                    }
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return retVal;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public HotelRoomRecord getHotelRoom(string Market, string Hotel, string Room, ref string errorMsg)
        {
            HotelRoomRecord record = new HotelRoomRecord();
            string tsql = @"Select RecID, Hotel, Code, Name, 
	                            NameL=isnull(dbo.FindLocalName(NameLID, @Market), (Select isnull(dbo.FindLocalName(NameLID, @Market), HotelRoom.Name) From Room (NOLOCK) Where Code=HotelRoom.Code)),
	                            NameS, DispNo=isnull(DispNo, 9999), BaseRoom
                            From HotelRoom (NOLOCK)
                            Where 
	                            VisiblePL='Y' And
	                            Hotel=@Hotel And
                                Code=@Code
                            Order By DispNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Code", DbType.String, Room);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.BaseRoom = Conversion.getStrOrNull(R["BaseRoom"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                    }
                }
                return record;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public HotelBoardRecord getHotelBoard(string Market, string Hotel, string Board, ref string errorMsg)
        {
            HotelBoardRecord record = new HotelBoardRecord();
            string tsql = @"Select RecID, Hotel, Code, Name, 
	                            NameL=isnull(dbo.FindLocalName(NameLID, @Market), (Select isnull(dbo.FindLocalName(NameLID, @Market), HotelBoard.Name) From Board (NOLOCK) Where Code=HotelBoard.Code)),
	                            NameS, DispNo, BegDate, EndDate
                            From HotelBoard (NOLOCK)
                            Where VisiblePL='Y' And Hotel=@Hotel And Code=@Code ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Code", DbType.String, Board);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                    }
                }
                return record;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelAccomPaxRecord> getHotelAccomPax(string Market, string Hotel, ref string errorMsg)
        {
            List<HotelAccomPaxRecord> records = new List<HotelAccomPaxRecord>();
            string tsql = @"Select HAP.RecID, HAP.Hotel, HAP.Room, RoomName=HR.Name, HAP.Accom, AccomName=HA.Name,
	                               HAP.Adult, HAP.ChdAgeG1, HAP.ChdAgeG2, HAP.ChdAgeG3, HAP.ChdAgeG4, HAP.DispNo, HAP.VisiblePL 
                            From HotelAccomPax HAP (NOLOCK)
                            Join HotelAccom HA (NOLOCK) ON HA.Code=HAP.Accom And HA.Hotel=HAP.Hotel
                            Join HotelRoom HR (NOLOCK) ON HR.Hotel=HAP.Hotel And HR.Code=HAP.Room
                            Where HAP.Hotel=@Hotel";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelAccomPaxRecord record = new HotelAccomPaxRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        record.Accom = Conversion.getStrOrNull(R["Accom"]);
                        record.AccomName = Conversion.getStrOrNull(R["AccomName"]);
                        record.Adult = Conversion.getInt16OrNull(R["Adult"]);
                        record.ChdAgeG1 = Conversion.getInt16OrNull(R["ChdAgeG1"]);
                        record.ChdAgeG2 = Conversion.getInt16OrNull(R["ChdAgeG2"]);
                        record.ChdAgeG3 = Conversion.getInt16OrNull(R["ChdAgeG3"]);
                        record.ChdAgeG4 = Conversion.getInt16OrNull(R["ChdAgeG4"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.VisiblePL = Conversion.getStrOrNull(R["VisiblePL"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public HotelMarOptRecord getHotelMarOpt(string Market, string Hotel, ref string errorMsg)
        {
            HotelMarOptRecord record = new HotelMarOptRecord();
            string tsql = string.Empty;

            if (VersionControl.getTableField("HotelMarOpt", "MinAccomAge"))
            {
                tsql +=
@"
Select RecID,Hotel,Market,InfoWeb,MaxChdAge,MinAccomAge
From HotelMarOpt (NOLOCK)
Where Market=@Market And Hotel=@Hotel 
";
            }
            else
            {
                tsql +=
@"
Select RecID,Hotel,Market,InfoWeb,MaxChdAge=null,MinAccomAge=null
From HotelMarOpt (NOLOCK)
Where Market=@Market And Hotel=@Hotel 
";
            }
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.InfoWeb = Conversion.getStrOrNull(R["InfoWeb"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(R["MaxChdAge"]);
                        record.MinAccomAge = Conversion.getDecimalOrNull(R["MinAccomAge"]);
                        return record;
                    }
                    else
                        return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<HotelHandicapRecord> getAllHotelHandicaps(string Market, string plMarket)
        {
            List<HotelHandicapRecord> records = new List<HotelHandicapRecord>();
            string tsql = @"Select RecID, Hotel, BegDate, EndDate, 
	                            Name, NameL=isnull(dbo.FindLocalNameEx(NameLID, @Market), Name), PrtVoucher
                            From HotelHandicap (NOLOCK)
                            Where EndDate>GetDate()
                              And Market=@plMarket
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelHandicapRecord record = new HotelHandicapRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.PrtVoucher = Conversion.getBoolOrNull(R["PrtVoucher"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }
        public List<HotelHandicapRecord> getHotelHandicaps(string Market, string plMarket, string Hotel, DateTime? BegDate, DateTime? EndDate, ref string errorMsg)
        {
            List<HotelHandicapRecord> records = new List<HotelHandicapRecord>();
            string tsql = @"Select RecID, Hotel, BegDate, EndDate, 
	                            Name, NameL=isnull(dbo.FindLocalNameEx(NameLID, @Market), Name), PrtVoucher
                            From HotelHandicap (NOLOCK)
                            Where Hotel=@Hotel 
                              And Market=@plMarket
                            ";
            if (BegDate.HasValue && EndDate.HasValue)
                tsql += " And BegDate <= @EndDate And EndDate >= @BegDate ";
            if (BegDate.HasValue && !EndDate.HasValue)
                tsql += " And @BegDate between BegDate And EndDate ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                if (BegDate.HasValue && EndDate.HasValue)
                {
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate);
                }
                if (BegDate.HasValue && !EndDate.HasValue)
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelHandicapRecord record = new HotelHandicapRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.PrtVoucher = Conversion.getBoolOrNull(R["PrtVoucher"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public List<HotelPressRecord> getHotelPres(string Market, string Hotel, ref string errorMsg)
        {
            List<HotelPressRecord> records = new List<HotelPressRecord>();
            string tsql = @"Select HP.RecID, HP.Hotel, HP.Market, HP.TextType, HP.TextCat, TextCatName=HTC.Name,
	                            TextCatNameL=isnull(dbo.FindLocalName(HTC.NameLID, @Market), HTC.Name), HP.PresText
                            From HotelPres  HP (NOLOCK)
                            Left Join HotelTextCat HTC (NOLOCK) ON HTC.Code = HP.TextCat
                            Where HP.Hotel = @Hotel
                              And HP.Market = @Market ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelPressRecord record = new HotelPressRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.TextType = Conversion.getStrOrNull(R["TextType"]);
                        record.TextCat = Conversion.getStrOrNull(R["TextCat"]);
                        record.TextCatName = Conversion.getStrOrNull(R["TextCatName"]);
                        record.TextCatNameL = Conversion.getStrOrNull(R["TextCatNameL"]);
                        record.PresText = Conversion.getStrOrNull(R["PresText"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelFacilityRecord> getHotelFacilitys(string Market, string Hotel, ref string errorMsg)
        {
            List<HotelFacilityRecord> records = new List<HotelFacilityRecord>();
            string tsql =
@"
Select Distinct HF.Hotel, HF.FacNo, FacilityName=F.Name,
    FacilityNameL=ISNULL(dbo.FindLocalName(F.NameLID, @Market), F.Name),
    HF.Unit, HF.Priced, DispNo=isnull(HF.DispNo,9999), HF.Note,
    NoteLocalName=ISNULL(dbo.FindLocalName(HF.NameLID, @Market), HF.Note),
    F.UseinWebFilter
From HotelFacility HF (NOLOCK)
Join Facility F (NOLOCK) ON F.RecId=HF.FacNo
Where F.WebDisp=1
 And HF.Hotel=@Hotel
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelFacilityRecord record = new HotelFacilityRecord();
                        //record.RecId = Conversion.getInt32OrNull(R["RecId"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.FacNo = Conversion.getInt32OrNull(R["FacNo"]);
                        record.FacilityName = Conversion.getStrOrNull(R["FacilityName"]);
                        record.FacilityNameL = Conversion.getStrOrNull(R["FacilityNameL"]);
                        record.Unit = Conversion.getInt32OrNull(R["Unit"]);
                        record.Priced = Conversion.getStrOrNull(R["Priced"]);
                        record.DispNo = Conversion.getInt32OrNull(R["DispNo"]);
                        record.Note = Conversion.getStrOrNull(R["Note"]);
                        record.NoteLocalName = Conversion.getStrOrNull(R["NoteLocalName"]);
                        record.UseinWebFilter = Conversion.getBoolOrNull(R["UseinWebFilter"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelRoomFacRec> getHotelRoomFacilitys(string Market, string Hotel, ref string errorMsg)
        {
            List<HotelRoomFacRec> records = new List<HotelRoomFacRec>();
            string tsql = @"Select HF.RecID, HF.Hotel, HF.Room, RoomName=HR.Name, 
	                            RoomNameL=ISNULL(dbo.FindLocalName(HR.NameLID, @Market), ISNULL(dbo.FindLocalName(R.NamelID, @Market),HR.Name)), 
	                            HF.FacNo, FacilityName=F.Name,
                                FacilityNameL=ISNULL(dbo.FindLocalName(F.NameLID, @Market), F.Name),
                                HF.Unit, HF.Priced, HF.DispNo, HF.Note,
                                NoteLocalName=ISNULL(dbo.FindLocalName(HF.NameLID, @Market), HF.Note)
                            From HotelRoomFac HF (NOLOCK)
                            Join (
                                Select Distinct Room, FacNo 
                                From HotelRoomFac (NOLOCK)
                                Where Hotel = @Hotel
                            ) DistinctHF ON DistinctHF.FacNo=HF.FacNo And DistinctHF.Room=HF.Room And HF.Hotel=@Hotel
                            Join RoomFacility F (NOLOCK) ON F.RecId=HF.FacNo
                            Join HotelRoom HR (NOLOCK) ON HR.Code=HF.Room And HR.Hotel=HF.Hotel
                            Join Room R (NOLOCK) ON HR.Code=R.Code
                            Where F.WebDisp = 1
                            Order By HF.Room";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelRoomFacRec record = new HotelRoomFacRec();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        record.RoomNameL = Conversion.getStrOrNull(R["RoomNameL"]);
                        record.FacNo = Conversion.getInt32OrNull(R["FacNo"]);
                        record.FacilityName = Conversion.getStrOrNull(R["FacilityName"]);
                        record.FacilityNameL = Conversion.getStrOrNull(R["FacilityNameL"]);
                        record.Unit = Conversion.getInt32OrNull(R["Unit"]);
                        record.Priced = Conversion.getStrOrNull(R["Priced"]);
                        record.DispNo = Conversion.getInt32OrNull(R["DispNo"]);
                        record.Note = Conversion.getStrOrNull(R["Note"]);
                        record.NoteLocalName = Conversion.getStrOrNull(R["NoteLocalName"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public HotelProperty getHotelProperty(string Market, string Hotel, ref string errorMsg)
        {
            HotelProperty record = new HotelProperty();
            record.HotelFacility = new Hotels().getHotelFacilitys(Market, Hotel, ref errorMsg);
            record.HotelAccom = new Hotels().getHotelAccom(Market, Hotel, ref errorMsg);
            record.HotelBoard = new Hotels().getHotelBoardList(Market, Hotel, ref errorMsg);
            record.HotelRoom = new Hotels().getHotelRoomList(Market, Hotel, ref errorMsg);
            record.HotelRoomFacility = new Hotels().getHotelRoomFacilitys(Market, Hotel, ref errorMsg);
            return record;
        }

        public HotelTextCat getHotelTextCat(string Market, int? RecID, string Code, ref string errorMsg)
        {
            string tsql = @"Select RecId, Code, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name)
                            From HotelTextCat (NOLOCK) ";
            if (RecID.HasValue)
                tsql += string.Format("Where RecId={0} ", RecID.Value);
            else
                tsql += string.Format("Where Code='{0}' ", Code);
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        HotelTextCat record = new HotelTextCat();
                        record.RecId = Conversion.getInt32OrNull(R["RecId"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelTextCat> getHotelTextCat(string Market, ref string errorMsg)
        {
            List<HotelTextCat> records = new List<HotelTextCat>();
            string tsql = @"Select RecId, Code, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name)
                            From HotelTextCat (NOLOCK) ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelTextCat record = new HotelTextCat();
                        record.RecId = Conversion.getInt32OrNull(R["RecId"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelPictTextRecord> getHotelPictText(string Market, string Hotel, ref string errorMsg)
        {
            List<HotelPictTextRecord> records = new List<HotelPictTextRecord>();
            string tsql = @"Select HPT.RecID, HPT.PicID, HPT.Hotel, HPT.Market, HPT.PicText, 
	                            HPT.TextCat, TextCatName=HTC.Name,TextCatNameL=ISNULL(dbo.FindLocalName(HTC.NameLID, @Market), HTC.Name),
	                            HPT.[Status], HPT.TextType
                            From HotelPictText (NOLOCK) HPT
                            Left Join HotelTextCat (NOLOCK) HTC ON HTC.Code=HPT.TextCat
                            Where HPT.Hotel=@Hotel And HPT.Market=@Market";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelPictTextRecord record = new HotelPictTextRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.PicID = Conversion.getInt32OrNull(R["PicID"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.PicText = Conversion.getStrOrNull(R["PicText"]);
                        record.TextCat = Conversion.getStrOrNull(R["TextCat"]);
                        record.TextCatName = Conversion.getStrOrNull(R["TextCatName"]);
                        record.TextCatNameL = Conversion.getStrOrNull(R["TextCatNameL"]);
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        record.TextType = Conversion.getStrOrNull(R["TextType"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public static string writePicture(object _image, string imgName, string imgPath, string H, string W)
        {
            if (_image != null && _image != Convert.DBNull)
            {
                byte[] image = (byte[])_image;
                HttpContext.Current.Response.ContentType = "image/jpeg";
                using (MemoryStream ms = new MemoryStream(image))
                {
                    Bitmap bmp;
                    bmp = (Bitmap)Bitmap.FromStream(ms);
                    try
                    {
                        if (H != "" && W != "")
                        {
                            bmp = ImageResize.FixedSize(bmp, Convert.ToInt32(H), Convert.ToInt32(H));
                        }
                        else
                            if (W == "" && H != "")
                            {
                                bmp = ImageResize.ConstrainProportions(bmp, Convert.ToInt32(H), Dimensions.Height);
                            }
                            else
                                if (H == "" && W != "")
                                {
                                    bmp = ImageResize.ConstrainProportions(bmp, Convert.ToInt32(W), Dimensions.Width);
                                }

                    }
                    catch { }

                    try
                    {
                        if (File.Exists(imgPath + imgName))
                            File.Delete(imgPath + imgName);
                        bmp.Save(imgPath + imgName, ImageFormat.Jpeg);
                        return imgName;
                    }
                    catch (Exception Ex)
                    {
                        string errorMsg = Ex.Message;
                        return "";
                    }
                }
            }
            else
            {
                return "";
            }

        }

        public string saveHotelPicture(HotelPictRecord imgRecord, ref string errorMsg)
        {
            string imgPath = AppDomain.CurrentDomain.BaseDirectory + "HotelImageCache";
            DirectoryInfo fileDir = new System.IO.DirectoryInfo(imgPath + "\\" + imgRecord.Hotel);
            System.Security.AccessControl.DirectorySecurity dS = new System.Security.AccessControl.DirectorySecurity();
            if (!fileDir.Exists)
                fileDir.Create();
            FileInfo[] fileList = fileDir.GetFiles();
            for (int i = 0; i < fileList.Length; i++)
            {
                string[] fileName = fileList[i].Name.Split('_');
                if (string.Equals(fileName[0], imgRecord.RecId.ToString()))
                {
                    if (string.Equals(fileName.Length > 1 ? fileName[1] : "", (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString()) + "_S" + ".jpg"))
                    {
                        return imgRecord.Hotel + "/" + imgRecord.RecId.ToString() + "_" + (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString());
                    }
                    else
                    {
                        try
                        {
                            fileList[i].Delete();
                        }
                        catch { }
                    }

                    if (string.Equals(fileName.Length > 1 ? fileName[1] : "", (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString()) + "_M" + ".jpg"))
                    {
                        return imgRecord.Hotel + "/" + imgRecord.RecId.ToString() + "_" + (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString());
                    }
                    else
                    {
                        try
                        {
                            fileList[i].Delete();
                        }
                        catch { }
                    }

                    if (string.Equals(fileName.Length > 1 ? fileName[1] : "", (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString()) + "_L" + ".jpg"))
                    {
                        return imgRecord.Hotel + "/" + imgRecord.RecId.ToString() + "_" + (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString());
                    }
                    else
                    {
                        try
                        {
                            fileList[i].Delete();
                        }
                        catch { }
                    }
                }
            }

            object image = new Hotels().getHotelImage(imgRecord.RecId, ref errorMsg);

            writePicture(image, imgRecord.RecId.ToString() + "_" + (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString()) + "_S.jpg", AppDomain.CurrentDomain.BaseDirectory + "HotelImageCache\\" + imgRecord.Hotel + "\\", "110", string.Empty);
            writePicture(image, imgRecord.RecId.ToString() + "_" + (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString()) + "_M.jpg", AppDomain.CurrentDomain.BaseDirectory + "HotelImageCache\\" + imgRecord.Hotel + "\\", "320", string.Empty);
            writePicture(image, imgRecord.RecId.ToString() + "_" + (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString()) + "_L.jpg", AppDomain.CurrentDomain.BaseDirectory + "HotelImageCache\\" + imgRecord.Hotel + "\\", "480", string.Empty);

            return WebRoot.BasePageRoot + imgRecord.Hotel + "/" + imgRecord.RecId.ToString() + "_" + (imgRecord.CrtDateJ.HasValue ? imgRecord.ChgDateJ.ToString() : imgRecord.CrtDateJ.ToString());
        }

        public List<HotelPictRecord> getHotelPict(string Hotel, ref string errorMsg)
        {
            List<HotelPictRecord> records = new List<HotelPictRecord>();
            string tsql = @"Select RecId, Hotel, DispNo, Picture, [Status], CrtDate, CrtUser, ChgDate, ChgUser
                            From HotelPict (NOLOCK)
                            Where isnull([Status], 'N')='Y' And
                                Hotel=@Hotel
                            Order By Hotel, DispNo";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelPictRecord record = new HotelPictRecord();
                        record.RecId = Conversion.getInt32OrNull(R["RecId"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        //record.Picture = R["Picture"];
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        record.CrtDate = Conversion.getDateTimeOrNull(R["CrtDate"]);
                        record.CrtDateJ = Conversion.ConvertToJulian(record.CrtDate);
                        record.CrtUser = Conversion.getStrOrNull(R["CrtUser"]);
                        record.ChgDate = Conversion.getDateTimeOrNull(R["ChgDate"]);
                        record.ChgDateJ = Conversion.ConvertToJulian(record.ChgDate);
                        record.ChgUser = Conversion.getStrOrNull(R["ChgUser"]);
                        record.imgPath = saveHotelPicture(record, ref errorMsg);
                        records.Add(record);
                    }

                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public object getHotelImage(int? RecID, ref string errorMsg)
        {
            string tsql = @"Select Picture
                            From HotelPict (NOLOCK)
                            Where isnull([Status], 'N')='Y' And
                                RecId=@RecId ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "RecId", DbType.Int32, RecID);
                return db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelAirportRecord> getHotelAirportList(string Hotel, ref string errorMsg)
        {
            List<HotelAirportRecord> records = new List<HotelAirportRecord>();
            string tsql = @"Select RecID, Hotel, AirPort, Distance 
                            From HotelAirPort (NOLOCK)
                            Where Hotel=@Hotel";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelAirportRecord record = new HotelAirportRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.AirPort = Conversion.getStrOrNull(R["AirPort"]);
                        record.Distance = Conversion.getInt32OrNull(R["Distance"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AirportRecord> getHotelAirports(User UserData, string Hotel, ref string errorMsg)
        {
            List<HotelAirportRecord> hotelAirportList = new Hotels().getHotelAirportList(Hotel, ref errorMsg);
            List<AirportRecord> airPortList = new Flights().getAirportLists(UserData, ref errorMsg);
            List<AirportRecord> retVal = (from q1 in hotelAirportList
                                          join q2 in airPortList on q1.AirPort equals q2.Code
                                          orderby q1.Distance.HasValue ? q1.Distance : (int)(999)
                                          select q2).ToList<AirportRecord>();
            return retVal;
        }

        public List<HotelPresTextWidthCategory> getHotelPressText(User UserData, string Hotel, DateTime? BegDate, DateTime? EndDate, string TextCat, ref string errorMsg)
        {
            List<HotelPresTextWidthCategory> Result = new List<HotelPresTextWidthCategory>();
            string tsql = string.Empty;

            tsql =
@"
Select Hp.Hotel,TextCatName=Htc.Name,TextCatLocalName=IsNull(dbo.FindLocalName(Htc.NameLID,@market),Htc.Name),PresText 
From HotelPres Hp (NOLOCK)
Join HotelMediaSeason Hms (NOLOCK) ON Hms.RecID=Hp.MediaID
Join HotelTextCat Htc (NOLOCK) ON Htc.Code=Hp.TextCat
Where Hp.Market=@market
  And Hp.TextType='H'
  And Hp.Hotel=@hotel
  And Hms.Status=1
  And isnull(Hms.BegDate,@begDate)<=@begDate
  And isnull(Hms.EndDate,@endDate)>=@endDate 
";
            string retVal = string.Empty;
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "hotel", DbType.AnsiString, Hotel);
                db.AddInParameter(dbCommand, "BegDate", DbType.Date, BegDate.HasValue ? BegDate.Value : DateTime.Today);
                db.AddInParameter(dbCommand, "EndDate", DbType.Date, EndDate.HasValue ? BegDate.Value : DateTime.Today);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        Result.Add(new HotelPresTextWidthCategory
                        {
                            Hotel = Conversion.getStrOrNull(R["Hotel"]),
                            TextCatName = Conversion.getStrOrNull(R["TextCatName"]),
                            TextCatLocalName = Conversion.getStrOrNull(R["TextCatLocalName"]),
                            PresText = Conversion.getStrOrNull(R["PresText"])
                        });
                    }
                    return Result;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getHotelSeasonAllotType(string plMarket, string hotel, DateTime? begDate, int? duration, string holpack, string room, string supplier, ref string dailyHotelAllot, ref string errorMsg)
        {
            string tsql =
@"
--Declare @plMarket varChar(10),@Hotel varChar(10),@Date DateTime,@Holpack varChar(10),@Room varChar(10),@Supplier varChar(10)
Select Top 1 AllotType From HotelSeason (NOLOCK) S 
Left join HotelNetPrice P (NOLOCK) on P.Hotel=S.Hotel and P.SeasonID=S.RecID
Where S.Hotel=@Hotel and 
  @Date between S.BegDate and S.EndDate and 
  (S.Market=@plMarket or @plMarket in (Select Market From HotelSeasonMark (NOLOCK) where Hotel=S.Hotel and SeasonID=S.RecID)) and 
  ((IsNull(S.ContType,0)=0) or 
   (IsNull(S.ContType,0)=1 and Exists(Select * from HotelSeasonPack (NOLOCK) Where HotelSeasonPack.SeasonID=S.RecID and HotelSeasonPack.HolPack=@Holpack))) and 
  Exists(Select * From HotelPeriod (NOLOCK) Where SeasonID= S.RecID and @Date Between BegDate and EndDate) and 
  P.Room=@Room and 
  S.Supplier like '%'+@Supplier
Order by ContType Desc,PricePriority
";
            string allotType = string.Empty;
            dailyHotelAllot = string.Empty;
            for (int i = 0; i < duration; i++)
            {
                Database db = (Database)DatabaseFactory.CreateDatabase();
                DbCommand dbCommand = db.GetSqlStringCommand(tsql);
                try
                {
                    db.AddInParameter(dbCommand, "plMarket", DbType.AnsiString, plMarket);
                    db.AddInParameter(dbCommand, "Hotel", DbType.AnsiString, hotel);
                    db.AddInParameter(dbCommand, "Date", DbType.DateTime, begDate.Value.AddDays(i));
                    db.AddInParameter(dbCommand, "Holpack", DbType.AnsiString, holpack);
                    db.AddInParameter(dbCommand, "Room", DbType.AnsiString, room);
                    db.AddInParameter(dbCommand, "Supplier", DbType.AnsiString, supplier);

                    string aType = Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
                    if (string.IsNullOrEmpty(aType))
                    {
                        allotType = "X";
                        dailyHotelAllot += allotType;
                        break;
                    }
                    if (string.Equals(aType, "G"))
                    {
                        dailyHotelAllot += "0";
                    }
                    else
                    {
                        dailyHotelAllot += "2";
                    }
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
            }
            if (dailyHotelAllot.IndexOf('0') > -1 && dailyHotelAllot.IndexOf('2') > -1)
                allotType = "M";
            else
                if (dailyHotelAllot.IndexOf('0') > -1)
                    allotType = "G";
                else
                    if (dailyHotelAllot.IndexOf('2') > -1)
                        allotType = "S";
                    else
                        allotType = "X";
            return allotType;
        }

        public List<HotelDispOrderRecord> getHotelOrders(User UserData, string plOperator, string plMarket, ref string errorMsg)
        {
            List<HotelDispOrderRecord> records = new List<HotelDispOrderRecord>();
            string tsql =
@"
Select RecID,Operator,Market,Hotel,DispNo  
From HotelDispOrder (NOLOCK)
Where Operator=@pOperator
  And Market=@pMarket
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pOperator", DbType.AnsiString, plOperator);
                db.AddInParameter(dbCommand, "pMarket", DbType.AnsiString, plMarket);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelDispOrderRecord record = new HotelDispOrderRecord();
                        record.RecID = (int)R["RecID"];
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HotelCatRecord> getHotelCatList(User UserData, ref string errorMsg)
        {
            List<HotelCatRecord> records = new List<HotelCatRecord>();
            string tsql =
@"
Select RecId,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID, @pMarket),Name),
  DispOrderWeb,StarCount,PxmRec 
From HotelCat (NOLOCK)
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pMarket", DbType.AnsiString, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HotelCatRecord record = new HotelCatRecord();
                        record.RecId = Conversion.getInt32OrNull(R["RecId"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.DispOrderWeb = Conversion.getInt16OrNull(R["DispOrderWeb"]);
                        record.StarCount = Conversion.getInt16OrNull(R["StarCount"]);
                        record.PxmRec = Conversion.getBoolOrNull(R["PxmRec"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
