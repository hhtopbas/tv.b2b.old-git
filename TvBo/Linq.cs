﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public static class Linq
    {
        public static IEnumerable<int> inElement(this IEnumerable<int> source, int? element)
        {
            foreach (var record in source)
            {
                if (element != null)
                {
                    if (element.Equals(element))
                        yield return record;
                }
                else
                    yield return record;
            }
        }

        public static IEnumerable<int?> inElement(this IEnumerable<int?> source, int? element)
        {
            foreach (var record in source)
            {
                if (element != null)
                {
                    if (element.Equals(element))
                        yield return record;
                }
                else
                    yield return record;
            }
        }

        public static IEnumerable<string> inElement(this IEnumerable<string> source, string element)
        {
            foreach (var record in source)
            {
                if (string.IsNullOrEmpty(element))
                {
                    if (element.Equals(element))
                        yield return record;
                }
                else
                    yield return record;
            }
        }
    }
}
