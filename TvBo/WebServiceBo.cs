﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TvBo
{        
    public class roomConseptParameters
    {
        public roomConseptParameters()
        { 
        }

        string _market;
        public string Market
        {
            get { return _market; }
            set { _market = value; }
        }

        string _hotel;
        public string Hotel
        {
            get { return _hotel; }
            set { _hotel = value; }
        }

        string _room;
        public string Room
        {
            get { return _room; }
            set { _room = value; }
        }

        long _date;
        public long Date
        {
            get { return _date; }
            set { _date = value; }
        }
    }

    [Serializable]
    public class sejourLogin
    { 
        public sejourLogin()
        {
        }

        string _databaseName;
        public string DatabaseName
        {
            get { return _databaseName; }
            set { _databaseName = value; }
        }

        string _userName;
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        string _Language;
        public string Language
        {
            get { return _Language; }
            set { _Language = value; }
        }
    }
    
    [Serializable]
    public class sejourLoginResult
    {
        public sejourLoginResult()
        {
        }
        bool? _Authenticated; public bool? Authenticated { get { return _Authenticated; } set { _Authenticated = value; } }
        string _Description; public string Description { get { return _Description; } set { _Description = value; } }
        string _AuthKey; public string AuthKey { get { return _AuthKey; } set { _AuthKey = value; } }
        string _Language; public string Language { get { return _Language; } set { _Language = value; } }
        string _TourOperatorCode; public string TourOperatorCode { get { return _TourOperatorCode; } set { _TourOperatorCode = value; } }
        string _TourOperatorName; public string TourOperatorName { get { return _TourOperatorName; } set { _TourOperatorName = value; } }
        string _TourOperatorNationalty; public string TourOperatorNationalty { get { return _TourOperatorNationalty; } set { _TourOperatorNationalty = value; } }
        string _TuropAdmin; public string TuropAdmin { get { return _TuropAdmin; } set { _TuropAdmin = value; } }
        string _ArrFlightNumber; public string ArrFlightNumber { get { return _ArrFlightNumber; } set { _ArrFlightNumber = value; } }
        string _DepFlightNumber; public string DepFlightNumber { get { return _DepFlightNumber; } set { _DepFlightNumber = value; } }
        string _DefaultTrfType; public string DefaultTrfType { get { return _DefaultTrfType; } set { _DefaultTrfType = value; } }
        string _connStr; public string connStr { get { return _connStr; } set { _connStr = value; } }
        decimal? _Ratio; public decimal? Ratio { get { return _Ratio; } set { _Ratio = value; } }
        decimal? _Amount; public decimal? Amount { get { return _Amount; } set { _Amount = value; } }
        decimal? _FlightTotal; public decimal? FlightTotal { get { return _FlightTotal; } set { _FlightTotal = value; } }
        decimal? _OtherExtras; public decimal? OtherExtras { get { return _OtherExtras; } set { _OtherExtras = value; } }
        bool? _ComissionType; public bool? ComissionType { get { return _ComissionType; } set { _ComissionType = value; } }
        string _Yetki; public string Yetki { get { return _Yetki; } set { _Yetki = value; } }
        string _DefaultRegion; public string DefaultRegion { get { return _DefaultRegion; } set { _DefaultRegion = value; } }
        bool? _IsFlightTotal; public bool? IsFlightTotal { get { return _IsFlightTotal; } set { _IsFlightTotal = value; } }
        decimal? _FlightYetFiy; public decimal? FlightYetFiy { get { return _FlightYetFiy; } set { _FlightYetFiy = value; } }
        int? _FlightCocAge1Min; public int? FlightCocAge1Min { get { return _FlightCocAge1Min; } set { _FlightCocAge1Min = value; } }
        int? _FlightCocAge1Max; public int? FlightCocAge1Max { get { return _FlightCocAge1Max; } set { _FlightCocAge1Max = value; } }
        decimal? _FlightCocFiy1; public decimal? FlightCocFiy1 { get { return _FlightCocFiy1; } set { _FlightCocFiy1 = value; } }
        int? _FlightCocAge2Min; public int? FlightCocAge2Min { get { return _FlightCocAge2Min; } set { _FlightCocAge2Min = value; } }
        int? _FlightCocAge2Max; public int? FlightCocAge2Max { get { return _FlightCocAge2Max; } set { _FlightCocAge2Max = value; } }
        decimal? _FlightCocFiy2; public decimal? FlightCocFiy2 { get { return _FlightCocFiy2; } set { _FlightCocFiy2 = value; } }
        int? _FlightCocAge3Min; public int? FlightCocAge3Min { get { return _FlightCocAge3Min; } set { _FlightCocAge3Min = value; } }
        int? _FlightCocAge3Max; public int? FlightCocAge3Max { get { return _FlightCocAge3Max; } set { _FlightCocAge3Max = value; } }
        decimal? _FlightCocFiy3; public decimal? FlightCocFiy3 { get { return _FlightCocFiy3; } set { _FlightCocFiy3 = value; } }
        bool? _IsInsuranceTotal; public bool? IsInsuranceTotal { get { return _IsInsuranceTotal; } set { _IsInsuranceTotal = value; } }
        decimal? _InsurancePercent; public decimal? InsurancePercent { get { return _InsurancePercent; } set { _InsurancePercent = value; } }
        decimal? _InsuranceYetFiy; public decimal? InsuranceYetFiy { get { return _InsuranceYetFiy; } set { _InsuranceYetFiy = value; } }
        int? _InsuranceCocAge1Min; public int? InsuranceCocAge1Min { get { return _InsuranceCocAge1Min; } set { _InsuranceCocAge1Min = value; } }
        int? _InsuranceCocAge1Max; public int? InsuranceCocAge1Max { get { return _InsuranceCocAge1Max; } set { _InsuranceCocAge1Max = value; } }
        decimal? _InsuranceCocFiy1; public decimal? InsuranceCocFiy1 { get { return _InsuranceCocFiy1; } set { _InsuranceCocFiy1 = value; } }
        int? _InsuranceCocAge2Min; public int? InsuranceCocAge2Min { get { return _InsuranceCocAge2Min; } set { _InsuranceCocAge2Min = value; } }
        int? _InsuranceCocAge2Max; public int? InsuranceCocAge2Max { get { return _InsuranceCocAge2Max; } set { _InsuranceCocAge2Max = value; } }
        decimal? _InsuranceCocFiy2; public decimal? InsuranceCocFiy2 { get { return _InsuranceCocFiy2; } set { _InsuranceCocFiy2 = value; } }
        int? _InsuranceCocAge3Min; public int? InsuranceCocAge3Min { get { return _InsuranceCocAge3Min; } set { _InsuranceCocAge3Min = value; } }
        int? _InsuranceCocAge3Max; public int? InsuranceCocAge3Max { get { return _InsuranceCocAge3Max; } set { _InsuranceCocAge3Max = value; } }
        decimal? _InsuranceCocFiy3; public decimal? InsuranceCocFiy3 { get { return _InsuranceCocFiy3; } set { _InsuranceCocFiy3 = value; } }
        string _InsuranceCurr; public string InsuranceCurr { get { return _InsuranceCurr; } set { _InsuranceCurr = value; } }
        string _FlightCurr; public string FlightCurr { get { return _FlightCurr; } set { _FlightCurr = value; } }
        string _OtherCurr; public string OtherCurr { get { return _OtherCurr; } set { _OtherCurr = value; } }
        DateTime? _TodayDate; public DateTime? TodayDate { get { return _TodayDate; } set { _TodayDate = value; } }
    }

    [Serializable]
    public class sejourHotelAllot
    {
        public sejourHotelAllot()
        {
        }

        string _token;
        public string Token
        {
            get { return _token; }
            set { _token = value; }
        }

        DateTime? _Tarih1;
        public DateTime? Tarih1
        {
            get { return _Tarih1; }
            set { _Tarih1 = value; }
        }

        DateTime? _Tarih2;
        public DateTime? Tarih2
        {
            get { return _Tarih2; }
            set { _Tarih2 = value; }
        }

        string _hotelCode;
        public string HotelCode
        {
            get { return _hotelCode; }
            set { _hotelCode = value; }
        }

        string _KontTuru;
        public string KontTuru
        {
            get { return _KontTuru; }
            set { _KontTuru = value; }
        }

        string _roomTypeCode;
        public string RoomTypeCode
        {
            get { return _roomTypeCode; }
            set { _roomTypeCode = value; }
        }

        string _roomCode;
        public string RoomCode
        {
            get { return _roomCode; }
            set { _roomCode = value; }
        }

        int? _roomCount;
        public int? RoomCount
        {
            get { return _roomCount; }
            set { _roomCount = value; }
        }

        bool? _isUrun;
        public bool? IsUrun
        {
            get { return _isUrun; }
            set { _isUrun = value; }
        }

        string _releaseErrorMessage;
        public string ReleaseErrorMessage
        {
            get { return _releaseErrorMessage; }
            set { _releaseErrorMessage = value; }
        }

        string _alloterrorMessage;
        public string AlloterrorMessage
        {
            get { return _alloterrorMessage; }
            set { _alloterrorMessage = value; }
        }

    }

    [Serializable]
    public class sejourHotelAllotResult
    {
        public sejourHotelAllotResult()
        {
        }

        bool? _New_Allotment_Control_NEWResult;
        public bool? New_Allotment_Control_NEWResult
        {
            get { return _New_Allotment_Control_NEWResult; }
            set { _New_Allotment_Control_NEWResult = value; }
        }

        string _releaseErrorMessage;
        public string ReleaseErrorMessage
        {
            get { return _releaseErrorMessage; }
            set { _releaseErrorMessage = value; }
        }

        string _alloterrorMessage;
        public string AlloterrorMessage
        {
            get { return _alloterrorMessage; }
            set { _alloterrorMessage = value; }
        }

    }

    [Serializable]
    public class getLoginParam
    {
        public getLoginParam()
        { 
        }

        string _token;
        public string token
        {
            get { return _token; }
            set { _token = value; }
        }

        DataTable _getLoginResult;
        public DataTable getLoginResult
        {
            get { return _getLoginResult; }
            set { _getLoginResult = value; }
        }        
    }

    [Serializable]
    public class tryTokenParam
    {
        public tryTokenParam()
        {         
        }

        string _db;
        public string db
        {
            get { return _db; }
            set { _db = value; }
        }

        string _agency;
        public string agency
        {
            get { return _agency; }
            set { _agency = value; }
        }

        string _user;
        public string user
        {
            get { return _user; }
            set { _user = value; }
        }

        string _pwd;
        public string pwd
        {
            get { return _pwd; }
            set { _pwd = value; }
        }

        bool _LoginOk;
        public bool LoginOk
        {
            get { return _LoginOk; }
            set { _LoginOk = value; }
        }

        string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        string _UserKey;
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }

    }
}
