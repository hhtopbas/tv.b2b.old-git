﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class ResCustInfojSon
    {
        public ResCustInfojSon()
        { 
        }

        string _CTitle;
        public string CTitle
        {
            get { return _CTitle; }
            set { _CTitle = value; }
        }

        string _CName;
        public string CName
        {
            get { return _CName; }
            set { _CName = value; }
        }

        string _CSurName;
        public string CSurName
        {
            get { return _CSurName; }
            set { _CSurName = value; }
        }

        string _MobTel;
        public string MobTel
        {
            get { return _MobTel; }
            set { _MobTel = value; }
        }

        string _ContactAddr;
        public string ContactAddr
        {
            get { return _ContactAddr; }
            set { _ContactAddr = value; }
        }

        string _InvoiceAddr;
        public string InvoiceAddr
        {
            get { return _InvoiceAddr; }
            set { _InvoiceAddr = value; }
        }

        string _AddrHome;
        public string AddrHome
        {
            get { return _AddrHome; }
            set { _AddrHome = value; }
        }

        string _AddrHomeCity;
        public string AddrHomeCity
        {
            get { return _AddrHomeCity; }
            set { _AddrHomeCity = value; }
        }        

        string _AddrHomeCountry;
        public string AddrHomeCountry
        {
            get { return _AddrHomeCountry; }
            set { _AddrHomeCountry = value; }
        }

        string _AddrHomeCountryCode;
        public string AddrHomeCountryCode
        {
            get { return _AddrHomeCountryCode; }
            set { _AddrHomeCountryCode = value; }
        }

        string _AddrHomeZip;
        public string AddrHomeZip
        {
            get { return _AddrHomeZip; }
            set { _AddrHomeZip = value; }
        }

        string _AddrHomeTel;
        public string AddrHomeTel
        {
            get { return _AddrHomeTel; }
            set { _AddrHomeTel = value; }
        }

        string _AddrHomeFax;
        public string AddrHomeFax
        {
            get { return _AddrHomeFax; }
            set { _AddrHomeFax = value; }
        }

        string _AddrHomeEmail;
        public string AddrHomeEmail
        {
            get { return _AddrHomeEmail; }
            set { _AddrHomeEmail = value; }
        }

        string _HomeTaxOffice;
        public string HomeTaxOffice
        {
            get { return _HomeTaxOffice; }
            set { _HomeTaxOffice = value; }
        }

        string _HomeTaxAccNo;
        public string HomeTaxAccNo
        {
            get { return _HomeTaxAccNo; }
            set { _HomeTaxAccNo = value; }
        }

        string _WorkFirmName;
        public string WorkFirmName
        {
            get { return _WorkFirmName; }
            set { _WorkFirmName = value; }
        }

        string _AddrWork;
        public string AddrWork
        {
            get { return _AddrWork; }
            set { _AddrWork = value; }
        }

        string _AddrWorkCity;
        public string AddrWorkCity
        {
            get { return _AddrWorkCity; }
            set { _AddrWorkCity = value; }
        }

        string _AddrWorkCountry;
        public string AddrWorkCountry
        {
            get { return _AddrWorkCountry; }
            set { _AddrWorkCountry = value; }
        }

        string _AddrWorkCountryCode;
        public string AddrWorkCountryCode
        {
            get { return _AddrWorkCountryCode; }
            set { _AddrWorkCountryCode = value; }
        }

        string _AddrWorkZip;
        public string AddrWorkZip
        {
            get { return _AddrWorkZip; }
            set { _AddrWorkZip = value; }
        }

        string _AddrWorkTel;
        public string AddrWorkTel
        {
            get { return _AddrWorkTel; }
            set { _AddrWorkTel = value; }
        }

        string _AddrWorkFax;
        public string AddrWorkFax
        {
            get { return _AddrWorkFax; }
            set { _AddrWorkFax = value; }
        }

        string _AddrWorkEMail;
        public string AddrWorkEMail
        {
            get { return _AddrWorkEMail; }
            set { _AddrWorkEMail = value; }
        }

        string _WorkTaxOffice;
        public string WorkTaxOffice
        {
            get { return _WorkTaxOffice; }
            set { _WorkTaxOffice = value; }
        }

        string _WorkTaxAccNo;
        public string WorkTaxAccNo
        {
            get { return _WorkTaxAccNo; }
            set { _WorkTaxAccNo = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankAccNo;
        public string BankAccNo
        {
            get { return _BankAccNo; }
            set { _BankAccNo = value; }
        }

        string _BankIBAN;
        public string BankIBAN
        {
            get { return _BankIBAN; }
            set { _BankIBAN = value; }
        }

        string _CustNo;
        public string CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        string _LCID;
        public string LCID
        {
            get { return _LCID; }
            set { _LCID = value; }
        }

    }

    public class ResCustInfojSonV2
    {
        public ResCustInfojSonV2()
        {
            _AddrHome = string.Empty;
            _AddrHomeCity = string.Empty;
            _AddrHomeCountry = string.Empty;
            _AddrHomeCountryCode = string.Empty;
            _AddrHomeEmail = string.Empty;
            _AddrHomeFax = string.Empty;
            _AddrHomeTel = string.Empty;
            _AddrHomeZip = string.Empty;
            _AddrWork = string.Empty;
            _AddrWorkCity = string.Empty;
            _AddrWorkCountry = string.Empty;
            _AddrWorkCountryCode = string.Empty;
            _AddrWorkEMail = string.Empty;
            _AddrWorkFax = string.Empty;
            _AddrWorkTel = string.Empty;
            _AddrWorkZip = string.Empty;
            _Bank = string.Empty;
            _BankAccNo = string.Empty;
            _BankIBAN = string.Empty;
            _CName = string.Empty;
            _ContactAddr = string.Empty;
            _CSurName = string.Empty;
            _CTitle = string.Empty;
            _HomeTaxAccNo = string.Empty;
            _HomeTaxOffice = string.Empty;
            _InvoiceAddr = string.Empty;
            _MobTel = string.Empty;
            _WorkFirmName = string.Empty;
            _WorkTaxAccNo = string.Empty;
            _WorkTaxOffice = string.Empty;            
        }

        string _CTitle;
        public string CTitle
        {
            get { return _CTitle; }
            set { _CTitle = value; }
        }

        string _CName;
        public string CName
        {
            get { return _CName; }
            set { _CName = value; }
        }

        string _CSurName;
        public string CSurName
        {
            get { return _CSurName; }
            set { _CSurName = value; }
        }

        string _MobTel;
        public string MobTel
        {
            get { return _MobTel; }
            set { _MobTel = value; }
        }

        string _ContactAddr;
        public string ContactAddr
        {
            get { return _ContactAddr; }
            set { _ContactAddr = value; }
        }

        string _InvoiceAddr;
        public string InvoiceAddr
        {
            get { return _InvoiceAddr; }
            set { _InvoiceAddr = value; }
        }

        string _AddrHome;
        public string AddrHome
        {
            get { return _AddrHome; }
            set { _AddrHome = value; }
        }

        string _AddrHomeCity;
        public string AddrHomeCity
        {
            get { return _AddrHomeCity; }
            set { _AddrHomeCity = value; }
        }

        string _AddrHomeCountry;
        public string AddrHomeCountry
        {
            get { return _AddrHomeCountry; }
            set { _AddrHomeCountry = value; }
        }

        string _AddrHomeCountryCode;
        public string AddrHomeCountryCode
        {
            get { return _AddrHomeCountryCode; }
            set { _AddrHomeCountryCode = value; }
        }

        string _AddrHomeZip;
        public string AddrHomeZip
        {
            get { return _AddrHomeZip; }
            set { _AddrHomeZip = value; }
        }

        string _AddrHomeTel;
        public string AddrHomeTel
        {
            get { return _AddrHomeTel; }
            set { _AddrHomeTel = value; }
        }

        string _AddrHomeFax;
        public string AddrHomeFax
        {
            get { return _AddrHomeFax; }
            set { _AddrHomeFax = value; }
        }

        string _AddrHomeEmail;
        public string AddrHomeEmail
        {
            get { return _AddrHomeEmail; }
            set { _AddrHomeEmail = value; }
        }

        string _HomeTaxOffice;
        public string HomeTaxOffice
        {
            get { return _HomeTaxOffice; }
            set { _HomeTaxOffice = value; }
        }

        string _HomeTaxAccNo;
        public string HomeTaxAccNo
        {
            get { return _HomeTaxAccNo; }
            set { _HomeTaxAccNo = value; }
        }

        string _WorkFirmName;
        public string WorkFirmName
        {
            get { return _WorkFirmName; }
            set { _WorkFirmName = value; }
        }

        string _AddrWork;
        public string AddrWork
        {
            get { return _AddrWork; }
            set { _AddrWork = value; }
        }

        string _AddrWorkCity;
        public string AddrWorkCity
        {
            get { return _AddrWorkCity; }
            set { _AddrWorkCity = value; }
        }

        string _AddrWorkCountry;
        public string AddrWorkCountry
        {
            get { return _AddrWorkCountry; }
            set { _AddrWorkCountry = value; }
        }

        string _AddrWorkCountryCode;
        public string AddrWorkCountryCode
        {
            get { return _AddrWorkCountryCode; }
            set { _AddrWorkCountryCode = value; }
        }

        string _AddrWorkZip;
        public string AddrWorkZip
        {
            get { return _AddrWorkZip; }
            set { _AddrWorkZip = value; }
        }

        string _AddrWorkTel;
        public string AddrWorkTel
        {
            get { return _AddrWorkTel; }
            set { _AddrWorkTel = value; }
        }

        string _AddrWorkFax;
        public string AddrWorkFax
        {
            get { return _AddrWorkFax; }
            set { _AddrWorkFax = value; }
        }

        string _AddrWorkEMail;
        public string AddrWorkEMail
        {
            get { return _AddrWorkEMail; }
            set { _AddrWorkEMail = value; }
        }

        string _WorkTaxOffice;
        public string WorkTaxOffice
        {
            get { return _WorkTaxOffice; }
            set { _WorkTaxOffice = value; }
        }

        string _WorkTaxAccNo;
        public string WorkTaxAccNo
        {
            get { return _WorkTaxAccNo; }
            set { _WorkTaxAccNo = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankAccNo;
        public string BankAccNo
        {
            get { return _BankAccNo; }
            set { _BankAccNo = value; }
        }

        string _BankIBAN;
        public string BankIBAN
        {
            get { return _BankIBAN; }
            set { _BankIBAN = value; }
        }       
    }

    public class ResCustjSonResView
    {
        public ResCustjSonResView()
        {
        }

        string _edtTitleCode;
        public string EdtTitleCode
        {
            get { return _edtTitleCode; }
            set { _edtTitleCode = value; }
        }

        string _edtSurname;
        public string EdtSurname
        {
            get { return _edtSurname; }
            set { _edtSurname = value; }
        }

        string _edtSurnameL;
        public string EdtSurnameL
        {
            get { return _edtSurnameL; }
            set { _edtSurnameL = value; }
        }

        string _edtName;
        public string EdtName
        {
            get { return _edtName; }
            set { _edtName = value; }
        }

        string _edtNameL;
        public string EdtNameL
        {
            get { return _edtNameL; }
            set { _edtNameL = value; }
        }

        string _edtBirtday;
        public string EdtBirtday
        {
            get { return _edtBirtday; }
            set { _edtBirtday = value; }
        }

        string _edtIDNo;
        public string EdtIDNo
        {
            get { return _edtIDNo; }
            set { _edtIDNo = value; }
        }

        string _edtPassSerie;
        public string EdtPassSerie
        {
            get { return _edtPassSerie; }
            set { _edtPassSerie = value; }
        }

        string _edtPassNo;
        public string EdtPassNo
        {
            get { return _edtPassNo; }
            set { _edtPassNo = value; }
        }

        string _edtPassIssueDate;
        public string EdtPassIssueDate
        {
            get { return _edtPassIssueDate; }
            set { _edtPassIssueDate = value; }
        }

        string _edtPassExpDate;
        public string EdtPassExpDate
        {
            get { return _edtPassExpDate; }
            set { _edtPassExpDate = value; }
        }

        string _edtPassGiven;
        public string EdtPassGiven
        {
            get { return _edtPassGiven; }
            set { _edtPassGiven = value; }
        }

        string _CTitle;
        public string CTitle
        {
            get { return _CTitle; }
            set { _CTitle = value; }
        }

        string _CName;
        public string CName
        {
            get { return _CName; }
            set { _CName = value; }
        }

        string _CSurName;
        public string CSurName
        {
            get { return _CSurName; }
            set { _CSurName = value; }
        }

        string _MobTel;
        public string MobTel
        {
            get { return _MobTel; }
            set { _MobTel = value; }
        }

        string _ContactAddr;
        public string ContactAddr
        {
            get { return _ContactAddr; }
            set { _ContactAddr = value; }
        }

        string _InvoiceAddr;
        public string InvoiceAddr
        {
            get { return _InvoiceAddr; }
            set { _InvoiceAddr = value; }
        }

        string _AddrHome;
        public string AddrHome
        {
            get { return _AddrHome; }
            set { _AddrHome = value; }
        }

        string _AddrHomeCity;
        public string AddrHomeCity
        {
            get { return _AddrHomeCity; }
            set { _AddrHomeCity = value; }
        }

        string _AddrHomeCountry;
        public string AddrHomeCountry
        {
            get { return _AddrHomeCountry; }
            set { _AddrHomeCountry = value; }
        }

        string _AddrHomeCountryCode;
        public string AddrHomeCountryCode
        {
            get { return _AddrHomeCountryCode; }
            set { _AddrHomeCountryCode = value; }
        }

        string _AddrHomeZip;
        public string AddrHomeZip
        {
            get { return _AddrHomeZip; }
            set { _AddrHomeZip = value; }
        }

        string _AddrHomeTel;
        public string AddrHomeTel
        {
            get { return _AddrHomeTel; }
            set { _AddrHomeTel = value; }
        }

        string _AddrHomeFax;
        public string AddrHomeFax
        {
            get { return _AddrHomeFax; }
            set { _AddrHomeFax = value; }
        }

        string _AddrHomeEmail;
        public string AddrHomeEmail
        {
            get { return _AddrHomeEmail; }
            set { _AddrHomeEmail = value; }
        }

        string _HomeTaxOffice;
        public string HomeTaxOffice
        {
            get { return _HomeTaxOffice; }
            set { _HomeTaxOffice = value; }
        }

        string _HomeTaxAccNo;
        public string HomeTaxAccNo
        {
            get { return _HomeTaxAccNo; }
            set { _HomeTaxAccNo = value; }
        }

        string _WorkFirmName;
        public string WorkFirmName
        {
            get { return _WorkFirmName; }
            set { _WorkFirmName = value; }
        }

        string _AddrWork;
        public string AddrWork
        {
            get { return _AddrWork; }
            set { _AddrWork = value; }
        }

        string _AddrWorkCity;
        public string AddrWorkCity
        {
            get { return _AddrWorkCity; }
            set { _AddrWorkCity = value; }
        }

        string _AddrWorkCountry;
        public string AddrWorkCountry
        {
            get { return _AddrWorkCountry; }
            set { _AddrWorkCountry = value; }
        }

        string _AddrWorkCountryCode;
        public string AddrWorkCountryCode
        {
            get { return _AddrWorkCountryCode; }
            set { _AddrWorkCountryCode = value; }
        }

        string _AddrWorkZip;
        public string AddrWorkZip
        {
            get { return _AddrWorkZip; }
            set { _AddrWorkZip = value; }
        }

        string _AddrWorkTel;
        public string AddrWorkTel
        {
            get { return _AddrWorkTel; }
            set { _AddrWorkTel = value; }
        }

        string _AddrWorkFax;
        public string AddrWorkFax
        {
            get { return _AddrWorkFax; }
            set { _AddrWorkFax = value; }
        }

        string _AddrWorkEMail;
        public string AddrWorkEMail
        {
            get { return _AddrWorkEMail; }
            set { _AddrWorkEMail = value; }
        }

        string _WorkTaxOffice;
        public string WorkTaxOffice
        {
            get { return _WorkTaxOffice; }
            set { _WorkTaxOffice = value; }
        }

        string _WorkTaxAccNo;
        public string WorkTaxAccNo
        {
            get { return _WorkTaxAccNo; }
            set { _WorkTaxAccNo = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankAccNo;
        public string BankAccNo
        {
            get { return _BankAccNo; }
            set { _BankAccNo = value; }
        }

        string _BankIBAN;
        public string BankIBAN
        {
            get { return _BankIBAN; }
            set { _BankIBAN = value; }
        }

        string _CustNo;
        public string CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        string _LCID;
        public string LCID
        {
            get { return _LCID; }
            set { _LCID = value; }
        }

    }
}
