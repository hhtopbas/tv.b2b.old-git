﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace TvBo
{
    [Serializable()]
    public class resMonitorFilterRecordV2
    {
        public string ResNo1 { get; set; }
        public string ResNo2 { get; set; }
        public string BegDate1 { get; set; }
        public string BegDate2 { get; set; }
        public string EndDate1 { get; set; }
        public string EndDate2 { get; set; }
        public string ResDate1 { get; set; }
        public string ResDate2 { get; set; }
        public long? PayDate1 { get; set; }
        public long? PayDate2 { get; set; }
        public int? ArrCity { get; set; }
        public int? DepCity { get; set; }
        public string AgencyOffice { get; set; }
        public string AgencyUser { get; set; }
        public string LeaderSurname { get; set; }
        public string LeaderName { get; set; }
        public string HolPack { get; set; }
        public string ResStatus { get; set; }
        public string ConfStatus { get; set; }
        public string PayStatus { get; set; }
        public bool? UserSeeAllReservation { get; set; }
        public bool? ShowDraft { get; set; }
        public bool? ShowFilterDraft { get; set; }
        public Int16? ShowOptionRes { get; set; }
        public string DateLang { get; set; }
        public string Currency { get; set; }
        public bool ShowOnlyActiveUsers { get; set; }
        public bool ShowFilterOnlyPxmRes { get; set; }
        public bool ShowOnlyPxmRes { get; set; }
        public resMonitorFilterRecordV2()
        {
            this.ResStatus = "0;1;3";
            this.ShowDraft = false;
            this.ShowFilterDraft = false;
            this.UserSeeAllReservation = false;
            this.ShowOnlyActiveUsers = false;
            this.ShowFilterOnlyPxmRes = false;
            this.ShowOnlyPxmRes = false;
        }

    }

    [Serializable()]
    public class resMonFilterV2SaveFilter
    {
        public resMonFilterV2SaveFilter()
        {
            _data = new resMonitorFilterRecordV2();
        }

        resMonitorFilterRecordV2 _data;
        public resMonitorFilterRecordV2 data
        {
            get { return _data; }
            set { _data = value; }
        }
    }

    public class resMonitorFilterRecord
    {
        public resMonitorFilterRecord()
        {
            _resStatus = "0;1;3";
            _showDraft = false;
            _pageNo = 1;
        }

        string _resNo1;
        public string ResNo1
        {
            get { return _resNo1; }
            set { _resNo1 = value; }
        }

        string _resNo2;
        public string ResNo2
        {
            get { return _resNo2; }
            set { _resNo2 = value; }
        }

        DateTime? _begDate1;
        public DateTime? BegDate1
        {
            get { return _begDate1; }
            set { _begDate1 = value; }
        }

        DateTime? _begDate2;
        public DateTime? BegDate2
        {
            get { return _begDate2; }
            set { _begDate2 = value; }
        }

        DateTime? _endDate1;
        public DateTime? EndDate1
        {
            get { return _endDate1; }
            set { _endDate1 = value; }
        }

        DateTime? _endDate2;
        public DateTime? EndDate2
        {
            get { return _endDate2; }
            set { _endDate2 = value; }
        }

        DateTime? _resDate1;
        public DateTime? ResDate1
        {
            get { return _resDate1; }
            set { _resDate1 = value; }
        }

        DateTime? _resDate2;
        public DateTime? ResDate2
        {
            get { return _resDate2; }
            set { _resDate2 = value; }
        }

        DateTime? _payDate1;
        public DateTime? PayDate1
        {
            get { return _payDate1; }
            set { _payDate1 = value; }
        }

        DateTime? _payDate2;
        public DateTime? PayDate2
        {
            get { return _payDate2; }
            set { _payDate2 = value; }
        }

        string _arrCity;
        public string ArrCity
        {
            get { return _arrCity; }
            set { _arrCity = value; }
        }

        string _depCity;
        public string DepCity
        {
            get { return _depCity; }
            set { _depCity = value; }
        }

        string _agencyUser;
        public string AgencyUser
        {
            get { return _agencyUser; }
            set { _agencyUser = value; }
        }

        string _leaderNameF;
        public string LeaderNameF
        {
            get { return _leaderNameF; }
            set { _leaderNameF = value; }
        }

        string _leaderNameS;
        public string LeaderNameS
        {
            get { return _leaderNameS; }
            set { _leaderNameS = value; }
        }

        string _holPack;
        public string HolPack
        {
            get { return _holPack; }
            set { _holPack = value; }
        }

        string _resStatus;
        public string ResStatus
        {
            get { return _resStatus; }
            set { _resStatus = value; }
        }

        string _confStatus;
        public string ConfStatus
        {
            get { return _confStatus; }
            set { _confStatus = value; }
        }

        string _payStatus;
        public string PayStatus
        {
            get { return _payStatus; }
            set { _payStatus = value; }
        }

        string _agencyOffice;
        public string AgencyOffice
        {
            get { return _agencyOffice; }
            set { _agencyOffice = value; }
        }

        bool? _showDraft;
        public bool? ShowDraft
        {
            get { return _showDraft; }
            set { _showDraft = value; }
        }

        bool? _showOptionRes;
        public bool? ShowOptionRes
        {
            get { return _showOptionRes; }
            set { _showOptionRes = value; }
        }

        string _dateFormat;
        public string DateFormat
        {
            get { return _dateFormat; }
            set { _dateFormat = value; }
        }

        string _dateLang;
        public string DateLang
        {
            get { return _dateLang; }
            set { _dateLang = value; }
        }

        int? _pageNo;
        public int? PageNo
        {
            get { return _pageNo; }
            set { _pageNo = value; }
        }

        bool? _ShowAllRes;
        public bool? ShowAllRes
        {
            get { return _ShowAllRes; }
            set { _ShowAllRes = value; }
        }

        string _Currency;
        public string Currency
        {
            get { return _Currency; }
            set { _Currency = value; }
        }


    }

    public class resMonitorDefaultRecord
    {
        public resMonitorDefaultRecord()
        {
            _isAgencyUserActive = "Y";
        }

        string _agency;
        public string Agency
        {
            get { return _agency; }
            set { _agency = value; }
        }

        string _agencyName;
        public string AgencyName
        {
            get { return _agencyName; }
            set { _agencyName = value; }
        }

        string _agencyNameL;
        public string AgencyNameL
        {
            get { return _agencyNameL; }
            set { _agencyNameL = value; }
        }

        string _agencyUser;
        public string AgencyUser
        {
            get { return _agencyUser; }
            set { _agencyUser = value; }
        }

        string _agencyUserName;
        public string AgencyUserName
        {
            get { return _agencyUserName; }
            set { _agencyUserName = value; }
        }

        string _agencyUserNameL;
        public string AgencyUserNameL
        {
            get { return _agencyUserNameL; }
            set { _agencyUserNameL = value; }
        }

        string _isAgencyUserActive;
        public string isAgencyUserActive
        {
            get { return _isAgencyUserActive; }
            set { _isAgencyUserActive = value; }
        }

        string _holPack;
        public string HolPack
        {
            get { return _holPack; }
            set { _holPack = value; }
        }

        string _holPackName;
        public string HolPackName
        {
            get { return _holPackName; }
            set { _holPackName = value; }
        }

        string _holPackNameL;
        public string HolPackNameL
        {
            get { return _holPackNameL; }
            set { _holPackNameL = value; }
        }

        int _depCity;
        public int DepCity
        {
            get { return _depCity; }
            set { _depCity = value; }
        }

        string _depCityName;
        public string DepCityName
        {
            get { return _depCityName; }
            set { _depCityName = value; }
        }

        string _depCityNameL;
        public string DepCityNameL
        {
            get { return _depCityNameL; }
            set { _depCityNameL = value; }
        }

        int _arrCity;
        public int ArrCity
        {
            get { return _arrCity; }
            set { _arrCity = value; }
        }

        string _arrCityName;
        public string ArrCityName
        {
            get { return _arrCityName; }
            set { _arrCityName = value; }
        }

        string _arrCityNameL;
        public string ArrCityNameL
        {
            get { return _arrCityNameL; }
            set { _arrCityNameL = value; }
        }

        string _currency;
        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
    }

    public class resMonitorRecord
    {
        public resMonitorRecord()
        {
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        string _HolPackName;
        public string HolPackName
        {
            get { return _HolPackName; }
            set { _HolPackName = value; }
        }

        string _HolPackNameL;
        public string HolPackNameL
        {
            get { return _HolPackNameL; }
            set { _HolPackNameL = value; }
        }

        string _HotelName;
        public string HotelName
        {
            get { return _HotelName; }
            set { _HotelName = value; }
        }

        string _HotelNameL;
        public string HotelNameL
        {
            get { return _HotelNameL; }
            set { _HotelNameL = value; }
        }

        int? _Adult;
        public int? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        int? _Child;
        public int? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _Days;
        public Int16? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        decimal? _PackedPrice;
        public decimal? PackedPrice
        {
            get { return _PackedPrice; }
            set { _PackedPrice = value; }
        }

        decimal? _OutOfPackage;
        public decimal? OutOfPackage
        {
            get { return _OutOfPackage; }
            set { _OutOfPackage = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _AgencyPayable;
        public decimal? AgencyPayable
        {
            get { return _AgencyPayable; }
            set { _AgencyPayable = value; }
        }

        decimal? _Discount;
        public decimal? Discount
        {
            get { return _Discount; }
            set { _Discount = value; }
        }

        decimal? _EBAgency;
        public decimal? EBAgency
        {
            get { return _EBAgency; }
            set { _EBAgency = value; }
        }

        decimal? _EBAgencyPer;
        public decimal? EBAgencyPer
        {
            get { return _EBAgencyPer; }
            set { _EBAgencyPer = value; }
        }

        decimal? _EBPas;
        public decimal? EBPas
        {
            get { return _EBPas; }
            set { _EBPas = value; }
        }

        decimal? _EBPasPer;
        public decimal? EBPasPer
        {
            get { return _EBPasPer; }
            set { _EBPasPer = value; }
        }

        decimal? _AgencyCom;
        public decimal? AgencyCom
        {
            get { return _AgencyCom; }
            set { _AgencyCom = value; }
        }

        decimal? _AgencyComPer;
        public decimal? AgencyComPer
        {
            get { return _AgencyComPer; }
            set { _AgencyComPer = value; }
        }

        decimal? _AgencyDisPasVal;
        public decimal? AgencyDisPasVal
        {
            get { return _AgencyDisPasVal; }
            set { _AgencyDisPasVal = value; }
        }

        decimal? _AgencyDisPasPer;
        public decimal? AgencyDisPasPer
        {
            get { return _AgencyDisPasPer; }
            set { _AgencyDisPasPer = value; }
        }

        decimal? _AgencyPayment;
        public decimal? AgencyPayment
        {
            get { return _AgencyPayment; }
            set { _AgencyPayment = value; }
        }

        string _ConfToAgency;
        public string ConfToAgency
        {
            get { return _ConfToAgency; }
            set { _ConfToAgency = value; }
        }

        DateTime? _ConfToAgencyDate;
        public DateTime? ConfToAgencyDate
        {
            get { return _ConfToAgencyDate; }
            set { _ConfToAgencyDate = value; }
        }

        DateTime? _ResDate;
        public DateTime? ResDate
        {
            get { return _ResDate; }
            set { _ResDate = value; }
        }

        DateTime? _ChgDate;
        public DateTime? ChgDate
        {
            get { return _ChgDate; }
            set { _ChgDate = value; }
        }

        string _ReadByOpr;
        public string ReadByOpr
        {
            get { return _ReadByOpr; }
            set { _ReadByOpr = value; }
        }

        string _ReadByOprUser;
        public string ReadByOprUser
        {
            get { return _ReadByOprUser; }
            set { _ReadByOprUser = value; }
        }

        string _ReadByOprUserL;
        public string ReadByOprUserL
        {
            get { return _ReadByOprUserL; }
            set { _ReadByOprUserL = value; }
        }

        DateTime? _ReadByOprDate;
        public DateTime? ReadByOprDate
        {
            get { return _ReadByOprDate; }
            set { _ReadByOprDate = value; }
        }

        string _AgencyUser;
        public string AgencyUser
        {
            get { return _AgencyUser; }
            set { _AgencyUser = value; }
        }

        string _AgencyUserL;
        public string AgencyUserL
        {
            get { return _AgencyUserL; }
            set { _AgencyUserL = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityNameL;
        public string DepCityNameL
        {
            get { return _DepCityNameL; }
            set { _DepCityNameL = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityNameL;
        public string ArrCityNameL
        {
            get { return _ArrCityNameL; }
            set { _ArrCityNameL = value; }
        }

        string _ResNote;
        public string ResNote
        {
            get { return _ResNote; }
            set { _ResNote = value; }
        }

        decimal? _Balance;
        public decimal? Balance
        {
            get { return _Balance; }
            set { _Balance = value; }
        }

        decimal? _SPrice;
        public decimal? SPrice
        {
            get { return _SPrice; }
            set { _SPrice = value; }
        }

        decimal? _DueAgentPayment;
        public decimal? DueAgentPayment
        {
            get { return _DueAgentPayment; }
            set { _DueAgentPayment = value; }
        }

        string _LeaderName;
        public string LeaderName
        {
            get { return _LeaderName; }
            set { _LeaderName = value; }
        }

        string _InvoiceNo;
        public string InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        bool _NewComment;
        public bool NewComment
        {
            get { return _NewComment; }
            set { _NewComment = value; }
        }

        string _CommisionInvoiceNumber;
        public string CommisionInvoiceNumber
        {
            get { return _CommisionInvoiceNumber; }
            set { _CommisionInvoiceNumber = value; }
        }

        Int16? _SaleResource;
        public Int16? SaleResource
        {
            get { return _SaleResource; }
            set { _SaleResource = value; }
        }

        Int16? _ResStat;
        public Int16? ResStat
        {
            get { return _ResStat; }
            set { _ResStat = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        Int16? _HotConfStat;
        public Int16? HotConfStat
        {
            get { return _HotConfStat; }
            set { _HotConfStat = value; }
        }

        string _PaymentStat;
        public string PaymentStat
        {
            get { return _PaymentStat; }
            set { _PaymentStat = value; }
        }

        DateTime? _OptDate;
        public DateTime? OptDate
        {
            get { return _OptDate; }
            set { _OptDate = value; }
        }

        int _RefNo;
        public int RefNo
        {
            get { return _RefNo; }
            set { _RefNo = value; }
        }

        decimal? _Supplement;
        public decimal? Supplement
        {
            get { return _Supplement; }
            set { _Supplement = value; }
        }

        decimal? _Bonus;
        public decimal? Bonus
        {
            get { return _Bonus; }
            set { _Bonus = value; }
        }

        decimal? _AgencyBonusAmount;
        public decimal? AgencyBonusAmount
        {
            get { return _AgencyBonusAmount; }
            set { _AgencyBonusAmount = value; }
        }

        decimal? _UserBonusAmount;
        public decimal? UserBonusAmount
        {
            get { return _UserBonusAmount; }
            set { _UserBonusAmount = value; }
        }

        decimal? _PasBonusAmount;
        public decimal? PasBonusAmount
        {
            get { return _PasBonusAmount; }
            set { _PasBonusAmount = value; }
        }

        decimal? _BrokerCom;
        public decimal? BrokerCom
        {
            get { return _BrokerCom; }
            set { _BrokerCom = value; }
        }

        decimal? _BrokerComPer;
        public decimal? BrokerComPer
        {
            get { return _BrokerComPer; }
            set { _BrokerComPer = value; }
        }
    }

    public class resMonitorRecordV2
    {
        public string ResNo { get; set; }
        public int RefNo { get; set; }
        public string LeaderName { get; set; }
        public string HotelName { get; set; }
        public string HotelNameL { get; set; }
        public int? Adult { get; set; }
        public int? Child { get; set; }
        public DateTime? BegDate { get; set; }
        public Int16? Days { get; set; }
        public decimal? SPrice { get; set; }
        public string SaleCur { get; set; }
        public byte? ReadByOpr { get; set; }
        public int? DepCity { get; set; }
        public string DepCityName { get; set; }
        public string DepCityNameL { get; set; }
        public int? ArrCity { get; set; }
        public string ArrCityName { get; set; }
        public string ArrCityNameL { get; set; }
        public string ResNote { get; set; }
        public bool NewComment { get; set; }
        public Int16? ResStat { get; set; }
        public Int16? ConfStat { get; set; }
        public string PaymentStat { get; set; }
        public DateTime? ResDate { get; set; }
        public DateTime? OptDate { get; set; }
        public bool isPxmReservation { get; set; }
        public resMonitorRecordV2()
        {
            this.isPxmReservation = false;
        }
    }

    public class resCommentRecord
    {
        public resCommentRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        string _UserType;
        public string UserType
        {
            get { return _UserType; }
            set { _UserType = value; }
        }

        string _Comment;
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        DateTime? _CDate;
        public DateTime? CDate
        {
            get { return _CDate; }
            set { _CDate = value; }
        }

        bool _isRead;
        public bool IsRead
        {
            get { return _isRead; }
            set { _isRead = value; }
        }

        DateTime? _RDate;
        public DateTime? RDate
        {
            get { return _RDate; }
            set { _RDate = value; }
        }

        string _UserIDName;
        public string UserIDName
        {
            get { return _UserIDName; }
            set { _UserIDName = value; }
        }

        string _RUserIDName;
        public string RUserIDName
        {
            get { return _RUserIDName; }
            set { _RUserIDName = value; }
        }
        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

    }

    public class resPayPlanRecord
    {
        public resPayPlanRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int16 _PayNo;
        public Int16 PayNo
        {
            get { return _PayNo; }
            set { _PayNo = value; }
        }

        DateTime _DueDate;
        public DateTime DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        bool? _isEB;
        public bool? isEB
        {
            get { return _isEB; }
            set { _isEB = value; }
        }

        byte? _Status;
        public byte? Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        byte? _PlanType;
        public byte? PlanType
        {
            get { return _PlanType; }
            set { _PlanType = value; }
        }

        decimal? _Adult;
        public decimal? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        decimal? _Child;
        public decimal? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        decimal? _Infant;
        public decimal? Infant
        {
            get { return _Infant; }
            set { _Infant = value; }
        }

        decimal? _Balance;
        public decimal? Balance
        {
            get { return _Balance; }
            set { _Balance = value; }
        }

    }

    public class resMonitorFilterData
    {
        public resMonitorFilterData()
        {
            _AgencyOfficeData = new List<listString>();
            _AgencyUserData = new List<listString>();
            _ArrCityData = new List<listString>();
            _DepCityData = new List<listString>();
            _HolPackData = new List<listString>();
            _filter = new resMonitorFilterRecord();
            _ShowAllRes = false;
            _ShowDraft = false;
            _CurrencyData = new List<listString>();
        }

        List<listString> _HolPackData;
        public List<listString> HolPackData
        {
            get { return _HolPackData; }
            set { _HolPackData = value; }
        }

        List<listString> _DepCityData;
        public List<listString> DepCityData
        {
            get { return _DepCityData; }
            set { _DepCityData = value; }
        }

        List<listString> _ArrCityData;
        public List<listString> ArrCityData
        {
            get { return _ArrCityData; }
            set { _ArrCityData = value; }
        }

        List<listString> _AgencyOfficeData;
        public List<listString> AgencyOfficeData
        {
            get { return _AgencyOfficeData; }
            set { _AgencyOfficeData = value; }
        }

        List<listString> _AgencyUserData;
        public List<listString> AgencyUserData
        {
            get { return _AgencyUserData; }
            set { _AgencyUserData = value; }
        }

        List<listString> _CurrencyData;
        public List<listString> CurrencyData
        {
            get { return _CurrencyData; }
            set { _CurrencyData = value; }
        }

        resMonitorFilterRecord _filter;
        public resMonitorFilterRecord Filter
        {
            get { return _filter; }
            set { _filter = value; }
        }

        bool _ShowAllRes;
        public bool ShowAllRes
        {
            get { return _ShowAllRes; }
            set { _ShowAllRes = value; }
        }

        bool _ShowDraft;
        public bool ShowDraft
        {
            get { return _ShowDraft; }
            set { _ShowDraft = value; }
        }

        bool _ShowOptionControl;
        public bool ShowOptionControl
        {
            get { return _ShowOptionControl; }
            set { _ShowOptionControl = value; }
        }

    }

    public class resMonitorFilterDataV2
    {
        public List<listString> HolPackData { get; set; }
        public List<listString> DepCityData { get; set; }
        public List<listString> ArrCityData { get; set; }
        public List<listString> AgencyOfficeData { get; set; }
        public List<listString> AgencyUserData { get; set; }
        public List<listString> CurrencyData { get; set; }
        public resMonitorFilterRecordV2 Filter { get; set; }
        public string CustomRegID { get; set; }
        public bool OptionDateControl { get; set; }
        public bool ShowDraftControl { get; set; }
        public bool? ShowFilterDraft { get; set; }
        public bool NoCheckOnlyActiveUsers { get; set; }
        public bool ShowFilterOnlyPxmRes { get; set; }
        public Int16 DetailType { get; set; }
        public resMonitorFilterDataV2()
        {
            this.AgencyOfficeData = new List<listString>();
            this.AgencyUserData = new List<listString>();
            this.ArrCityData = new List<listString>();
            this.DepCityData = new List<listString>();
            this.HolPackData = new List<listString>();
            this.Filter = new resMonitorFilterRecordV2();
            this.CurrencyData = new List<listString>();
            this.OptionDateControl = false;
            this.NoCheckOnlyActiveUsers = false;
            this.DetailType = 0;
            this.ShowFilterOnlyPxmRes = false;
            this.ShowFilterDraft = false;
        }
    }

    public class resMonitorFilterJSonRecord
    {
        public resMonitorFilterJSonRecord()
        {
        }

        string _resNo1;
        public string ResNo1
        {
            get { return _resNo1; }
            set { _resNo1 = value; }
        }

        string _resNo2;
        public string ResNo2
        {
            get { return _resNo2; }
            set { _resNo2 = value; }
        }

        string _begDate1;
        public string BegDate1
        {
            get { return _begDate1; }
            set { _begDate1 = value; }
        }

        string _begDate2;
        public string BegDate2
        {
            get { return _begDate2; }
            set { _begDate2 = value; }
        }

        string _endDate1;
        public string EndDate1
        {
            get { return _endDate1; }
            set { _endDate1 = value; }
        }

        string _endDate2;
        public string EndDate2
        {
            get { return _endDate2; }
            set { _endDate2 = value; }
        }

        string _resDate1;
        public string ResDate1
        {
            get { return _resDate1; }
            set { _resDate1 = value; }
        }

        string _resDate2;
        public string ResDate2
        {
            get { return _resDate2; }
            set { _resDate2 = value; }
        }

        string _payDate1;
        public string PayDate1
        {
            get { return _payDate1; }
            set { _payDate1 = value; }
        }

        string _payDate2;
        public string PayDate2
        {
            get { return _payDate2; }
            set { _payDate2 = value; }
        }

        string _arrCity;
        public string ArrCity
        {
            get { return _arrCity; }
            set { _arrCity = value; }
        }

        string _depCity;
        public string DepCity
        {
            get { return _depCity; }
            set { _depCity = value; }
        }

        string _agencyUser;
        public string AgencyUser
        {
            get { return _agencyUser; }
            set { _agencyUser = value; }
        }

        string _leaderNameF;
        public string LeaderNameF
        {
            get { return _leaderNameF; }
            set { _leaderNameF = value; }
        }

        string _leaderNameS;
        public string LeaderNameS
        {
            get { return _leaderNameS; }
            set { _leaderNameS = value; }
        }

        string _holPack;
        public string HolPack
        {
            get { return _holPack; }
            set { _holPack = value; }
        }

        string _resStatus;
        public string ResStatus
        {
            get { return _resStatus; }
            set { _resStatus = value; }
        }

        string _confStatus;
        public string ConfStatus
        {
            get { return _confStatus; }
            set { _confStatus = value; }
        }

        string _payStatus;
        public string PayStatus
        {
            get { return _payStatus; }
            set { _payStatus = value; }
        }

        string _showDraft;
        public string ShowDraft
        {
            get { return _showDraft; }
            set { _showDraft = value; }
        }

        string _showOptionRes;
        public string ShowOptionRes
        {
            get { return _showOptionRes; }
            set { _showOptionRes = value; }
        }

        string _agencyOffice;
        public string AgencyOffice
        {
            get { return _agencyOffice; }
            set { _agencyOffice = value; }
        }

        string _dateFormat;
        public string DateFormat
        {
            get { return _dateFormat; }
            set { _dateFormat = value; }
        }

        string _pageNo;
        public string PageNo
        {
            get { return _pageNo; }
            set { _pageNo = value; }
        }

        string _currency;
        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }

    }

    public class paymentMonitorRecord
    {
        public paymentMonitorRecord()
        {
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        string _Office;
        public string Office
        {
            get { return _Office; }
            set { _Office = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        string _AgencyUser;
        public string AgencyUser
        {
            get { return _AgencyUser; }
            set { _AgencyUser = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Leader;
        public string Leader
        {
            get { return _Leader; }
            set { _Leader = value; }
        }

        string _MainAgency;
        public string MainAgency
        {
            get { return _MainAgency; }
            set { _MainAgency = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _Days;
        public Int16? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }

        Int16? _ResStat;
        public Int16? ResStat
        {
            get { return _ResStat; }
            set { _ResStat = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        DateTime? _ResDate;
        public DateTime? ResDate
        {
            get { return _ResDate; }
            set { _ResDate = value; }
        }

        Int16? _Adult;
        public Int16? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16? _Child;
        public Int16? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        decimal? _AgencyCom;
        public decimal? AgencyCom
        {
            get { return _AgencyCom; }
            set { _AgencyCom = value; }
        }

        decimal? _DisCount;
        public decimal? DisCount
        {
            get { return _DisCount; }
            set { _DisCount = value; }
        }

        decimal? _BrokerCom;
        public decimal? BrokerCom
        {
            get { return _BrokerCom; }
            set { _BrokerCom = value; }
        }

        decimal? _UserBonus;
        public decimal? UserBonus
        {
            get { return _UserBonus; }
            set { _UserBonus = value; }
        }

        decimal? _AgencyBonus;
        public decimal? AgencyBonus
        {
            get { return _AgencyBonus; }
            set { _AgencyBonus = value; }
        }

        decimal? _AgencySupDis;
        public decimal? AgencySupDis
        {
            get { return _AgencySupDis; }
            set { _AgencySupDis = value; }
        }

        decimal? _AgencyComSup;
        public decimal? AgencyComSup
        {
            get { return _AgencyComSup; }
            set { _AgencyComSup = value; }
        }

        decimal? _EBAgency;
        public decimal? EBAgency
        {
            get { return _EBAgency; }
            set { _EBAgency = value; }
        }

        decimal? _AgencyPayment;
        public decimal? AgencyPayment
        {
            get { return _AgencyPayment; }
            set { _AgencyPayment = value; }
        }

        decimal? _AgencyPayable;
        public decimal? AgencyPayable
        {
            get { return _AgencyPayable; }
            set { _AgencyPayable = value; }
        }

        decimal? _AgencyPayable2;
        public decimal? AgencyPayable2
        {
            get { return _AgencyPayable2; }
            set { _AgencyPayable2 = value; }
        }

        decimal? _EBPas;
        public decimal? EBPas
        {
            get { return _EBPas; }
            set { _EBPas = value; }
        }

        string _OfficeName;
        public string OfficeName
        {
            get { return _OfficeName; }
            set { _OfficeName = value; }
        }

        string _ResStatStr;
        public string ResStatStr
        {
            get { return _ResStatStr; }
            set { _ResStatStr = value; }
        }

        string _ConfStatStr;
        public string ConfStatStr
        {
            get { return _ConfStatStr; }
            set { _ConfStatStr = value; }
        }

        DateTime? _OptDate;
        public DateTime? OptDate
        {
            get { return _OptDate; }
            set { _OptDate = value; }
        }

    }

    public class paymentMonitorFilterRecord
    {
        public paymentMonitorFilterRecord()
        {
            _ShowAllRes = false;
            _IncludeDueDate = true;
        }

        DateTime? _DueDate;
        public DateTime? DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }

        DateTime? _begDate1;
        public DateTime? BegDate1
        {
            get { return _begDate1; }
            set { _begDate1 = value; }
        }

        DateTime? _begDate2;
        public DateTime? BegDate2
        {
            get { return _begDate2; }
            set { _begDate2 = value; }
        }

        DateTime? _resDate1;
        public DateTime? ResDate1
        {
            get { return _resDate1; }
            set { _resDate1 = value; }
        }

        DateTime? _resDate2;
        public DateTime? ResDate2
        {
            get { return _resDate2; }
            set { _resDate2 = value; }
        }

        string _resNo1;
        public string ResNo1
        {
            get { return _resNo1; }
            set { _resNo1 = value; }
        }

        string _resNo2;
        public string ResNo2
        {
            get { return _resNo2; }
            set { _resNo2 = value; }
        }

        int? _arrCity;
        public int? ArrCity
        {
            get { return _arrCity; }
            set { _arrCity = value; }
        }

        int? _depCity;
        public int? DepCity
        {
            get { return _depCity; }
            set { _depCity = value; }
        }

        string _agencyUser;
        public string AgencyUser
        {
            get { return _agencyUser; }
            set { _agencyUser = value; }
        }

        string _leaderNameF;
        public string LeaderNameF
        {
            get { return _leaderNameF; }
            set { _leaderNameF = value; }
        }

        string _leaderNameS;
        public string LeaderNameS
        {
            get { return _leaderNameS; }
            set { _leaderNameS = value; }
        }

        string _resStatus;
        public string ResStatus
        {
            get { return _resStatus; }
            set { _resStatus = value; }
        }

        string _confStatus;
        public string ConfStatus
        {
            get { return _confStatus; }
            set { _confStatus = value; }
        }

        string _payStatus;
        public string PayStatus
        {
            get { return _payStatus; }
            set { _payStatus = value; }
        }

        bool? _showDraft;
        public bool? ShowDraft
        {
            get { return _showDraft; }
            set { _showDraft = value; }
        }

        bool? _showOptionRes;
        public bool? ShowOptionRes
        {
            get { return _showOptionRes; }
            set { _showOptionRes = value; }
        }

        string _agencyOffice;
        public string AgencyOffice
        {
            get { return _agencyOffice; }
            set { _agencyOffice = value; }
        }

        string _dateFormat;
        public string DateFormat
        {
            get { return _dateFormat; }
            set { _dateFormat = value; }
        }

        string _dateLang;
        public string DateLang
        {
            get { return _dateLang; }
            set { _dateLang = value; }
        }

        int? _pageNo;
        public int? PageNo
        {
            get { return _pageNo; }
            set { _pageNo = value; }
        }

        bool _ShowAllRes;
        public bool ShowAllRes
        {
            get { return _ShowAllRes; }
            set { _ShowAllRes = value; }
        }

        bool _IncludeDueDate;
        public bool IncludeDueDate
        {
            get { return _IncludeDueDate; }
            set { _IncludeDueDate = value; }
        }
    }

    public class paymentMonitorDefaultRecord
    {
        public paymentMonitorDefaultRecord()
        {
        }

        string _agency;
        public string Agency
        {
            get { return _agency; }
            set { _agency = value; }
        }

        string _agencyName;
        public string AgencyName
        {
            get { return _agencyName; }
            set { _agencyName = value; }
        }

        string _agencyUser;
        public string AgencyUser
        {
            get { return _agencyUser; }
            set { _agencyUser = value; }
        }

        string _agencyUserName;
        public string AgencyUserName
        {
            get { return _agencyUserName; }
            set { _agencyUserName = value; }
        }

        int _depCity;
        public int DepCity
        {
            get { return _depCity; }
            set { _depCity = value; }
        }

        string _depCityName;
        public string DepCityName
        {
            get { return _depCityName; }
            set { _depCityName = value; }
        }

        int _arrCity;
        public int ArrCity
        {
            get { return _arrCity; }
            set { _arrCity = value; }
        }

        string _arrCityName;
        public string ArrCityName
        {
            get { return _arrCityName; }
            set { _arrCityName = value; }
        }

    }

    public class paymentMonitorFilterData
    {
        public paymentMonitorFilterData()
        {
            _AgencyOfficeData = new List<listString>();
            _AgencyUserData = new List<listString>();
            _ArrCityData = new List<listString>();
            _DepCityData = new List<listString>();
            _filter = new paymentMonitorFilterRecord();
            _ShowAllRes = false;
            _ShowDraft = false;
            this.showAgencyPayment = false;
            this.currentCultureNumber = new NumberFormatInfo();
        }

        List<listString> _HolPackData;
        public List<listString> HolPackData
        {
            get { return _HolPackData; }
            set { _HolPackData = value; }
        }

        List<listString> _DepCityData;
        public List<listString> DepCityData
        {
            get { return _DepCityData; }
            set { _DepCityData = value; }
        }

        List<listString> _ArrCityData;
        public List<listString> ArrCityData
        {
            get { return _ArrCityData; }
            set { _ArrCityData = value; }
        }

        List<listString> _AgencyOfficeData;
        public List<listString> AgencyOfficeData
        {
            get { return _AgencyOfficeData; }
            set { _AgencyOfficeData = value; }
        }

        List<listString> _AgencyUserData;
        public List<listString> AgencyUserData
        {
            get { return _AgencyUserData; }
            set { _AgencyUserData = value; }
        }

        paymentMonitorFilterRecord _filter;
        public paymentMonitorFilterRecord Filter
        {
            get { return _filter; }
            set { _filter = value; }
        }

        bool _ShowAllRes;
        public bool ShowAllRes
        {
            get { return _ShowAllRes; }
            set { _ShowAllRes = value; }
        }

        bool _ShowDraft;
        public bool ShowDraft
        {
            get { return _ShowDraft; }
            set { _ShowDraft = value; }
        }

        public NumberFormatInfo currentCultureNumber { get; set; }
        public bool showAgencyPayment { get; set; }
    }

    public class paymentMonitorFilterJSonRecord
    {
        public paymentMonitorFilterJSonRecord()
        {
        }

        string _DueDate;
        public string DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }

        string _begDate1;
        public string BegDate1
        {
            get { return _begDate1; }
            set { _begDate1 = value; }
        }

        string _begDate2;
        public string BegDate2
        {
            get { return _begDate2; }
            set { _begDate2 = value; }
        }

        string _resDate1;
        public string ResDate1
        {
            get { return _resDate1; }
            set { _resDate1 = value; }
        }

        string _resDate2;
        public string ResDate2
        {
            get { return _resDate2; }
            set { _resDate2 = value; }
        }

        string _arrCity;
        public string ArrCity
        {
            get { return _arrCity; }
            set { _arrCity = value; }
        }

        string _depCity;
        public string DepCity
        {
            get { return _depCity; }
            set { _depCity = value; }
        }

        string _agencyUser;
        public string AgencyUser
        {
            get { return _agencyUser; }
            set { _agencyUser = value; }
        }

        string _leaderNameF;
        public string LeaderNameF
        {
            get { return _leaderNameF; }
            set { _leaderNameF = value; }
        }

        string _leaderNameS;
        public string LeaderNameS
        {
            get { return _leaderNameS; }
            set { _leaderNameS = value; }
        }

        string _resStatus;
        public string ResStatus
        {
            get { return _resStatus; }
            set { _resStatus = value; }
        }

        string _confStatus;
        public string ConfStatus
        {
            get { return _confStatus; }
            set { _confStatus = value; }
        }

        string _payStatus;
        public string PayStatus
        {
            get { return _payStatus; }
            set { _payStatus = value; }
        }

        string _showDraft;
        public string ShowDraft
        {
            get { return _showDraft; }
            set { _showDraft = value; }
        }

        string _showOptionRes;
        public string ShowOptionRes
        {
            get { return _showOptionRes; }
            set { _showOptionRes = value; }
        }

        string _agencyOffice;
        public string AgencyOffice
        {
            get { return _agencyOffice; }
            set { _agencyOffice = value; }
        }

        string _dateFormat;
        public string DateFormat
        {
            get { return _dateFormat; }
            set { _dateFormat = value; }
        }

        string _pageNo;
        public string PageNo
        {
            get { return _pageNo; }
            set { _pageNo = value; }
        }
        string _IncludeDueDate;
        public string IncludeDueDate
        {
            get { return _IncludeDueDate; }
            set { _IncludeDueDate = value; }
        }
    }

    public class CommisionInvoiceNumberRecord
    {
        public CommisionInvoiceNumberRecord()
        {
        }

        string _InvSerial;
        public string InvSerial
        {
            get { return _InvSerial; }
            set { _InvSerial = value; }
        }

        string _InvNo;
        public string InvNo
        {
            get { return _InvNo; }
            set { _InvNo = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }
    }

    public class AgencyCanShareRecord
    {
        public string Agency { get; set; }
        public string CanShareAgency { get; set; }

        public AgencyCanShareRecord()
        {
        }
    }

    public class resMonitorExcelExportRecord
    {
        public int? RefNo { get; set; }
        public string ResNo { get; set; }
        public string LeaderName { get; set; }
        public string HotelName { get; set; }
        public string HotelNameL { get; set; }
        public string HolPackName { get; set; }
        public string HolPackNameL { get; set; }
        public Int16? Adult { get; set; }
        public Int16? Child { get; set; }
        public DateTime? BegDate { get; set; }
        public int? Days { get; set; }
        public DateTime? ResDate { get; set; }
        public Int16? ResStat { get; set; }
        public Int16? ConfStat { get; set; }
        public string PaymentStat { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? Discount { get; set; }
        public decimal? EBAgency { get; set; }
        public decimal? EBAgencyPer { get; set; }
        public decimal? EBPas { get; set; }
        public decimal? AgencyBonusAmount { get; set; }
        public decimal? UserBonusAmount { get; set; }
        public decimal? PasBonusAmount { get; set; }
        public decimal? PasAmount { get; set; }
        public decimal? AgencyCom { get; set; }
        public decimal? AgencyComPer { get; set; }
        public decimal? AgencyComSup { get; set; }
        public decimal? AgencyComSupPer { get; set; }
        public decimal? AgencyDisPasVal { get; set; }
        public decimal? AgencyDisPasPer { get; set; }
        public decimal? BrokerCom { get; set; }
        public decimal? BrokerComPer { get; set; }
        public decimal? AgencyPayable { get; set; }
        public decimal? AgencyPayment { get; set; }
        public decimal? Balance { get; set; }
        public decimal? SPrice { get; set; }
        public string SaleCur { get; set; }
        public int? DepCity { get; set; }
        public string DepCityName { get; set; }
        public string DepCityNameL { get; set; }
        public int? ArrCity { get; set; }
        public string ArrCityName { get; set; }
        public string ArrCityNameL { get; set; }
        public DateTime? ChgDate { get; set; }
        public string AgencyName { get; set; }
        public string AgencyNameL { get; set; }
        public string AgencyUser { get; set; }
        public string AgencyUserL { get; set; }
        public string ReadByOprUser { get; set; }
        public DateTime? ReadByOprDate { get; set; }

        public resMonitorExcelExportRecord()
        {
        }
    }
}
