﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class Banks
    {
        public BankRecord getBank(string Market, string Code, ref string errorMsg)
        {
            string tsql = @"Select RecID, Code, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name),
	                            NameS, SwiftCode, BankNo, Description
                            From Bank (NOLOCK)
                            Where Code=@Code";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        BankRecord record = new BankRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.SwiftCode = Conversion.getStrOrNull(R["SwiftCode"]);
                        record.BankNo = Conversion.getStrOrNull(R["BankNo"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        return record;
                    }
                    else return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public OperatorBankRecord getOperatorBank(string Operator, string Code, ref string errorMsg)
        {
            string tsql = @"Select RecID, Operator, Bank, BankNo, BranchNo, AccNo, Cur, IBAN, AccId, DefAcc, PrintDoc
                            From OperatorBank (NOLOCK)
                            Where Operator=@Operator And Bank=@Code
                            ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        OperatorBankRecord record = new OperatorBankRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.Bank= Conversion.getStrOrNull(R["Bank"]);
                        record.BankNo= Conversion.getStrOrNull(R["BankNo"]);
                        record.BranchNo= Conversion.getStrOrNull(R["BranchNo"]);
                        record.AccNo= Conversion.getStrOrNull(R["AccNo"]);
                        record.Cur= Conversion.getStrOrNull(R["Cur"]);
                        record.IBAN= Conversion.getStrOrNull(R["IBAN"]);
                        record.AccId= Conversion.getStrOrNull(R["AccId"]);
                        record.DefAcc= Conversion.getBoolOrNull(R["DefAcc"]);
                        record.PrintDoc= Conversion.getBoolOrNull(R["PrintDoc"]);                        
                        return record;
                    }
                    else return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public CurrencyRecord getCurrency(string Market, string Code, ref string errorMsg)
        {
            string tsql = @"Select RecID, Code, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name),
	                            NameS, FracName, FracNameL, FracNameS, IntCode
                            From Currency (NOLOCK)
                            Where Code=@Code";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        CurrencyRecord record = new CurrencyRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.FracName = Conversion.getStrOrNull(R["FracName"]);
                        record.FracNameL = Conversion.getStrOrNull(R["FracNameL"]);
                        record.FracNameS = Conversion.getStrOrNull(R["FracNameS"]);
                        record.IntCode = Conversion.getStrOrNull(R["IntCode"]);

                        return record;
                    }
                    else return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CurrencyRecord> getCurrencys(string Market, ref string errorMsg)
        {
            List<CurrencyRecord> records = new List<CurrencyRecord>();

            string tsql = @"Select RecID, Code, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name),
	                            NameS, FracName, FracNameL, FracNameS, IntCode
                            From Currency (NOLOCK) ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {                
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        CurrencyRecord record = new CurrencyRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.FracName = Conversion.getStrOrNull(R["FracName"]);
                        record.FracNameL = Conversion.getStrOrNull(R["FracNameL"]);
                        record.FracNameS = Conversion.getStrOrNull(R["FracNameS"]);
                        record.IntCode = Conversion.getStrOrNull(R["IntCode"]);
                        records.Add(record);
                    }
                    return records;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BankVPosRecord> getBankVPos(string BankCode, ref string errorMsg)
        {
            List<BankVPosRecord> records = new List<BankVPosRecord>();

            string tsql = @"Select RecID,Bank,Cur,Host,ClientID,UserID,[Password],BankCur,Interface,Operator,ProvUserID,ProvUserPass,MerchantID,PosVersion
                            From BankVPos (NOLOCK)
                            ";
            if (!string.IsNullOrEmpty(BankCode))
                tsql += string.Format("Where Bank='{0}'", BankCode);

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {                
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        BankVPosRecord record = new BankVPosRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Bank = Conversion.getStrOrNull(R["Bank"]);
                        record.Cur = Conversion.getStrOrNull(R["Cur"]);
                        record.Host = Conversion.getStrOrNull(R["Host"]);
                        record.ClientID = Conversion.getStrOrNull(R["ClientID"]);
                        record.UserID = Conversion.getStrOrNull(R["UserID"]);
                        record.Password = Conversion.getStrOrNull(R["Password"]);
                        record.BankCur = Conversion.getStrOrNull(R["BankCur"]);
                        record.Interface = Conversion.getStrOrNull(R["Interface"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.ProvUserID = Conversion.getStrOrNull(R["ProvUserID"]);
                        record.ProvUserPass = Conversion.getStrOrNull(R["ProvUserPass"]);
                        record.MerchantID = Conversion.getStrOrNull(R["MerchantID"]);
                        record.PosVersion = Conversion.getStrOrNull(R["PosVersion"]);
                        records.Add(record);
                    }
                    return records;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
