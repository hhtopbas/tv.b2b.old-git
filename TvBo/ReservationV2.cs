﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Web;
using System.Collections;
using System.Data.SqlTypes;
using System.Reflection;
using TvTools;
using System.Text.RegularExpressions;

namespace TvBo
{
    public class ReservationV2
    {
        public ResDataRecord fillResData(User UserData, ResDataRecord ResData, ref string errorMsg)
        {
            ResDataRecord orjData = new ResTables().copyData(ResData);
            ResDataRecord retData = new ResDataRecord();

            return retData;
        }

        public bool reCalcResData(User UserData, ref ResDataRecord ResData, bool reCalcCom, bool reCalcService, ref string errorMsg)
        {
            #region Calculate ResCustPrice

            string ResNo = ResData.ResMain.ResNo;
            ResNo = ResNo.Length > 10 ? ResNo.Substring(0, 10) : ResNo;

            StringBuilder CalcStrSql = new TvBo.Reservation().BuildCalcSqlString(ResData);

            CalcStrSql.Append("Declare  @PassEB bit \n");
            CalcStrSql.Append("Declare  @ErrCode SmallInt \n");
            CalcStrSql.Append("Declare @Supplier varchar(10) \n");
            CalcStrSql.Append("exec dbo.usp_Calc_Res_Price '" + ResNo + "', @ReCalcCom, @ReCalcService, @PassEB OutPut, @ErrCode OutPut, 1 \n");
            CalcStrSql.Append("Select * From #ResMain \n");
            CalcStrSql.Append("Select * From #ResService \n");
            CalcStrSql.Append("Select * From #ResServiceExt \n");
            CalcStrSql.Append("Select * From #ResCust \n");
            CalcStrSql.Append("Select * From #ResCustPrice \n");
            CalcStrSql.Append("Select * From #ResSupDis \n");
            CalcStrSql.Append("Select ErrorCode=@ErrCode \n");
            CalcStrSql.Append("Select * From #ResPromo \n");
            CalcStrSql.AppendFormat("Exec dbo.GetPromoList '{0}', 1 \n", ResNo);
            CalcStrSql.AppendFormat("Select * From #CalcErrTbl \n");
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(CalcStrSql.ToString());
            DataSet ds;
            try {
                db.AddInParameter(dbCommand, "ReCalcCom", DbType.String, reCalcCom);
                db.AddInParameter(dbCommand, "ReCalcService", DbType.String, reCalcService);
                ds = db.ExecuteDataSet(dbCommand);
                if (ds != null) {
                    ds.Tables[0].TableName = "ResMain";
                    ds.Tables[1].TableName = "ResService";
                    ds.Tables[2].TableName = "ResServiceExt";
                    ds.Tables[3].TableName = "ResCust";
                    ds.Tables[4].TableName = "ResCustPrice";
                    ds.Tables[5].TableName = "ResSupDis";
                    ds.Tables[6].TableName = "ErrorTable";
                    ds.Tables[7].TableName = "ResPromo";
                    ds.Tables[8].TableName = "PromoList";
                    ds.Tables[ds.Tables.Count - 1].TableName = "CalcErrTbl";
                } else {
                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationPriceNotCalculated").ToString();
                    ds = null;
                    return false;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                ds = null;
                return false;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
            #endregion Calculate ResCustPrice

            int ErrorCode = Convert.ToInt32(ds.Tables["ErrorTable"].Rows[0]["ErrorCode"].ToString());
            //if (ErrorCode != 0) {
            //    errorMsg = new ResCalcError().calcError(ErrorCode, ResData);
            //    return false;
            //} else if (ds.Tables["CalcErrTbl"].Rows.Count > 0) {
            //    ResData = new TvBo.Reservation().SetCalcDataToResData(UserData, ResData, ds, false, ref errorMsg);
            //    return false;
            //}

            if (ErrorCode != 0) {
                errorMsg = new ResCalcError().calcError(ErrorCode, ResData);
            }

            ResData = new TvBo.Reservation().SetCalcDataToResData(UserData, ResData, ds, false, ref errorMsg);

            if (ds.Tables["CalcErrTbl"].Rows.Count > 0) {
                return false;
            }

            //ResData = new TvBo.Reservation().SetCalcDataToResData(UserData, ResData, ds, false, ref errorMsg);

            List<PromoListRecord> PromoList = ResData.PromoList;
            PromoList = new Promos().getPromoListTemplate(UserData.Market, PromoList, ref errorMsg);

            if (string.IsNullOrEmpty(errorMsg))
                return true;
            else
                return false;
        }

        public bool reCalcExtResData(User UserData, ref ResDataRecord ResData, int? serviceID, ref string errorMsg)
        {
            string ResNo = ResData.ResMain.ResNo;
            ResNo = ResNo.Length > 10 ? ResNo.Substring(0, 10) : ResNo;

            StringBuilder CalcStrSql = new TvBo.Reservation().BuildCalcSqlString(ResData);

            CalcStrSql.Append("Declare  @ErrCode SmallInt \n");
            CalcStrSql.Append("Declare @Supplier varchar(10) \n");
            CalcStrSql.Append("exec dbo.usp_Calc_ExtService @ResNo, @ServiceID, @Supplier Output, @ErrCode OutPut, 1 \n");
            CalcStrSql.Append("Select @ErrCode");
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(CalcStrSql.ToString());
            try {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, serviceID);

                int? errCode = Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
                if (errCode.HasValue && errCode == 0)
                    return true;
                else {
                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationPriceNotCalculated").ToString();
                    return false;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return false;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ResDataRecord setExtraService(User UserData, ResDataRecord ResData, List<ExtraServiceListRecord> extraServiceList, List<SelectCustRecord> SelectCust, int? mainServiceID, int? extServiceRecID, int? dayDuration, bool oneTime, ref string errorMsg)
        {
            ResDataRecord _ResData = new ResDataRecord();
            _ResData = new ResTables().copyData(ResData);
            ServiceExtPriceRecord extServiceRow = new Reservation().getServiceExtPrice(UserData, extServiceRecID, null, null, false, ref errorMsg);
            ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == mainServiceID);
            //List<ExtraServiceListRecord> extraServiceList = new Reservation().getExtraServiceList(UserData, ResData, ref errorMsg);
            ExtraServiceListRecord extSerRec = extraServiceList.Find(f => f.RecID == extServiceRecID);

            List<SelectCustRecord> selectedCust = (from q1 in ResData.ResCon
                                                   join q2 in SelectCust on q1.CustNo equals q2.CustNo
                                                   where q1.ServiceID == mainServiceID
                                                   select q2).ToList<SelectCustRecord>();

            if (extSerRec.PriceType == 2 && UserData.CustomRegID == TvBo.Common.crID_Detur) {
                foreach (SelectCustRecord row in selectedCust)
                    row.Selected = true;
            } else
                if (selectedCust.Where(w => w.Selected == true).Count() < 1) {
                errorMsg = "Please select Tourist.";
                return ResData;
            }
            Int16 Adult = 0;
            Int16 Child = 0;

            foreach (SelectCustRecord row in selectedCust.Where(w => w.Selected).Select(s => s).ToList<SelectCustRecord>()) {
                ResCustRecord selectedResCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);

                if (ResData.ResCon.Where(w => w.ServiceID == mainServiceID && w.CustNo == row.CustNo).Count() < 1) {
                    errorMsg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "serviceSelectedCustomerIsNotValid").ToString() + " {0}", row.CustNo);
                    return ResData;
                }
                if (selectedResCust.Title < 6)
                    ++Adult;
                else
                    ++Child;
            }

            ResServiceExtRecord extRec = new Reservation().PrepareServiceExtCompulsory(UserData, _ResData, extServiceRow, resService, "N", "N", resService.Compulsory.HasValue ? (resService.Compulsory.Value ? "Y" : "N") : "N", Adult, Child, dayDuration, oneTime, ref errorMsg);

            if (extRec == null || !string.IsNullOrEmpty(errorMsg))
                return ResData;

            extRec.TakeAllUser = extServiceRow.TakeAllUser;

            int ExtServiceID = new Reservation().ExtServiceCompare(extRec, ResData.ResServiceExt.Where(w => w.StatConf.HasValue && w.StatConf.Value < 2).ToList<ResServiceExtRecord>(), selectedCust);

            if (!(new Reservation().getExtSerAllotUnit(UserData, ref extRec, resService, ResData.ResMain, extSerRec, extRec.Unit.Value, Convert.ToInt32("0"), ref errorMsg)))
                return ResData;

            if (ExtServiceID == -1) {
                extRec.SeqNo = Convert.ToInt16(ResData.ResServiceExt.Count > 0 ? ResData.ResServiceExt.OrderBy(o => o.SeqNo).Last().SeqNo + 1 : 1);
                _ResData.ResServiceExt.Add(extRec);
            } else {
                foreach (SelectCustRecord row in selectedCust.Where(w => w.Selected).Select(s => s).ToList<SelectCustRecord>()) {
                    ResCustRecord selectedResCust = _ResData.ResCust.Find(f => f.CustNo == row.CustNo);
                    var extServiceCon = from q1 in ResData.ResConExt
                                        join q2 in ResData.ResServiceExt on q1.ServiceID equals q2.RecID
                                        where q2.StatConf.HasValue &&
                                              q2.StatConf.Value < 2 &&
                                              q2.ServiceID == mainServiceID &&
                                              q1.CustNo == row.CustNo &&
                                              q2.ExtService == extRec.ExtService
                                        select q1;
                    //if (_ResData.ResCon.Where(w => w.ServiceID == mainServiceID && w.CustNo == row.CustNo).Count() > 0)
                    if (extServiceCon != null && extServiceCon.Count() > 0) {
                        errorMsg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "extraServiceSelectedCustomerIsNotValid").ToString() + " {0}", row.CustNo);
                        return ResData;
                    }
                }
            }

            foreach (SelectCustRecord row in selectedCust.Where(w => w.Selected).Select(s => s).ToList<SelectCustRecord>()) {
                _ResData.ResConExt.Add(new ResConExtRecord {
                    RecID = ResData.ResConExt.Count > 0 ? ResData.ResConExt.Last().RecID : 1,
                    RecordID = ResData.ResConExt.Count > 0 ? ResData.ResConExt.Last().RecID : 1,
                    ResNo = ResData.ResMain.ResNo,
                    ServiceID = extRec.RecID,
                    ServiceIDT = extRec.RecID,
                    MemTable = false,
                    UnitNo = 1,
                    CustNo = row.CustNo,
                    CustNoT = row.CustNo

                });
            }

            //bool calcData = new ReservationV2().reCalcExtResData(UserData, ref _ResData, extRec.RecID, ref errorMsg);
            //if (calcData)
            return _ResData;
            //else return ResData;
        }

        public ResDataRecord getResDataExtras(User UserData, ResDataRecord _ResData, ref string errorMsg)
        {
            ResDataRecord ResData = new ResTables().copyData(_ResData);
            ResDataRecord realData = new ResTables().copyData(ResData);

            List<ResServiceRecord> resServices = ResData.ResService;
            string[] notAddingServices = new string[4] { "HOTEL", "FLIGHT", "TRANSPORT", "NOSERVICE" };
            if ((string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && string.Equals(UserData.Market, "NORMAR") && ResData.ResMain.SaleResource == 3) ||
                (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && string.IsNullOrEmpty(ResData.ResMain.PackType))) {
                notAddingServices[3] = "TRANSFER";
            }
            var currentMainServices = from q in resServices
                                      group q by new { ServiceType = q.ServiceType } into k
                                      select new { ServiceType = k.Key.ServiceType };

            List<SaleServiceRecord> saleRecords = new ReservationCommon().getSalableServices(UserData, ResData, ref errorMsg);
            foreach (SaleServiceRecord row in saleRecords) {
                if (!notAddingServices.Contains(row.Service) &&
                    !currentMainServices.Select(s => s.ServiceType).Contains(row.Service) &&
                    string.Equals(row.WebSale, "Y")) {
                    ResData = new ReservationV2().addingExtras(UserData, ResData, row, ref errorMsg);
                }
            }

            List<ExtraServiceListRecord> resServiceExtList = new Reservation().getExtraServiceList(UserData, ResData, null, false, ref errorMsg);
            List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
            var mainServiceCusts = from q1 in ResData.ResCon
                                   join q2 in SelectCust on q1.CustNo equals q2.CustNo
                                   select new { custNo = q1.CustNo };
            foreach (var s in mainServiceCusts)
                SelectCust.Find(f => f.CustNo == s.custNo).Selected = true;

            foreach (ExtraServiceListRecord row in resServiceExtList) {
                ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == row.MainService);
                if (resService != null) {
                    string tmpErrorMsg = string.Empty;
                    ResData = new ReservationV2().setExtraService(UserData, ResData, resServiceExtList, SelectCust, row.MainService, row.RecID, resService.Duration, resService.Duration.HasValue ? true : false, ref tmpErrorMsg);
                    List<ResServiceExtRecord> resServExtList = ResData.ResServiceExt;
                    if (string.IsNullOrEmpty(tmpErrorMsg)) {
                        ResServiceExtRecord resServExt = resServExtList.LastOrDefault();
                        resServExt.ExcludeService = true;
                        resServExt.ChkSel = true;
                    }
                }
            }

            /*  */
            bool calcOK = false;
            errorMsg = string.Empty;

            while (!calcOK) {
                errorMsg = string.Empty;
                if (new ReservationV2().reCalcResData(UserData, ref ResData, true, true, ref errorMsg)) {
                    calcOK = true;
                    realData.ExtrasData = ResData;
                    break;
                } else {
                    List<ResServiceRecord> _resServiceList = ResData.ResService;
                    List<ResServiceExtRecord> _resServiceExtList = ResData.ResServiceExt;
                    var query = from q1 in ResData.ResCalcErrList
                                join q2 in _resServiceList on q1.SerID equals q2.RecID
                                join q3 in _resServiceExtList on q1.ExtSerID equals q3.RecID
                                where (q2.Compulsory.HasValue && q2.Compulsory.Value) || string.Equals(q2.IncPack, "Y")
                                //&& (
                                //    (q1.ErrCode.HasValue && q1.ErrCode.Value > 10000 && q1.ErrCode.Value < 20000 && Convert.ToInt32(q1.ErrCode.Value.ToString().Substring(2, 1)) == q1.SerID) ||
                                //    (q1.ErrCode.HasValue && q1.ErrCode.Value > 20000)
                                //   )
                                select q1;
                    if (query.Count() > 0) {
                        calcOK = false;
                        break;
                    }
                    foreach (ResCalcErr row in ResData.ResCalcErrList) {
                        if (row.ExtSerID.Value == 0 && row.SerID.HasValue && row.SerID.Value > 0) {
                            if (row.ErrCode.HasValue && row.ErrCode.Value >= 10000 && row.ErrCode.Value < 20000 && Convert.ToInt32(row.ErrCode.Value.ToString().Substring(1, 2)) == row.SerID) {
                                ResServiceRecord resService = _resServiceList.Find(f => f.RecID == row.SerID && !(f.IncPack == "Y" || f.Compulsory == true));
                                if (resService != null) {
                                    List<ResConRecord> resConList = ResData.ResCon;
                                    foreach (ResConRecord rcRow in ResData.ResCon.Where(w => w.ServiceID == row.SerID).ToList<ResConRecord>())
                                        resConList.Remove(rcRow);
                                    _resServiceList.Remove(resService);
                                }
                            }
                        } else {
                            if (row.SerID.Value == 0 && row.ExtSerID.HasValue && row.ExtSerID.Value > 0) {
                                if (row.ErrCode.HasValue && row.ErrCode.Value >= 20000 && Convert.ToInt32(row.ErrCode.Value.ToString().Substring(1, 2)) == row.ExtSerID) {
                                    ResServiceExtRecord resServiceExt = _resServiceExtList.Find(f => f.RecID == row.ExtSerID && !(f.IncPack == "Y" || f.Compulsory == "Y"));
                                    if (resServiceExt != null) {
                                        List<ResConExtRecord> resConExtList = ResData.ResConExt;
                                        foreach (ResConExtRecord rcRow in ResData.ResConExt.Where(w => w.ServiceID == row.ExtSerID).ToList<ResConExtRecord>())
                                            resConExtList.Remove(rcRow);
                                        _resServiceExtList.Remove(resServiceExt);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            List<ResServiceRecord> resServiceList = ResData.ResService;

            foreach (ResServiceRecord row in resServiceList) {
                row.ChkSel = true;
                List<ResConRecord> resConList = ResData.ResCon.Where(w => w.ServiceID == row.RecID).ToList<ResConRecord>();
                foreach (ResConRecord rcRow in resConList)
                    rcRow.ChkSel = true;
            }

            List<ResServiceExtRecord> resServiceExtListTmp = ResData.ResServiceExt;

            foreach (ResServiceExtRecord row in resServiceExtListTmp) {
                row.ChkSel = true;
                List<ResConExtRecord> resConExtList = ResData.ResConExt.Where(w => w.ServiceID == row.RecID).ToList<ResConExtRecord>();
                foreach (ResConExtRecord rcRow in resConExtList)
                    rcRow.ChkSel = true;
            }

            new ReservationV2().reCalcResData(UserData, ref ResData, true, true, ref errorMsg);

            if (!calcOK) {
                realData.ExtrasData = new ResTables().copyData(realData);
                return realData;
            }

            resServiceList = ResData.ResService;

            foreach (ResServiceRecord row in resServiceList) {
                row.ChkSel = row.ExcludeService ? false : true;
                List<ResConRecord> resConList = ResData.ResCon.Where(w => w.ServiceID == row.RecID).ToList<ResConRecord>();
                foreach (ResConRecord rcRow in resConList)
                    rcRow.ChkSel = row.ChkSel;
            }

            resServiceExtListTmp = ResData.ResServiceExt;

            foreach (ResServiceExtRecord row in resServiceExtListTmp) {
                row.ChkSel = string.Equals(row.Compulsory, "Y") || string.Equals(row.IncPack, "Y") ? true : false;
                List<ResConExtRecord> resConExtList = ResData.ResConExt.Where(w => w.ServiceID == row.RecID).ToList<ResConExtRecord>();
                foreach (ResConExtRecord rcRow in resConExtList)
                    rcRow.ChkSel = row.ChkSel;
            }

            if (new ReservationV2().reCalcResData(UserData, ref ResData, true, false, ref errorMsg)) {
                realData.ExtrasData = ResData;
            } else {
                realData.ExtrasData = new ResTables().copyData(realData);
                return realData;
            }

            bool CreateChildAge = true;

            if (UserData.WebService)
                CreateChildAge = true;
            else {
                object _createChildAge = new TvBo.Common().getFormConfigValue("MakeRes", "CreateChildAge");
                CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
            }

            if (!CreateChildAge) {
                foreach (ResCustRecord row in realData.ResCust) {
                    row.Age = null;
                    row.Birtday = null;
                }
            }

            return realData;
        }

        public ResDataRecord addingExtras(User UserData, ResDataRecord ResData, SaleServiceRecord addingServices, ref string errorMsg)
        {
            switch (addingServices.Service) {
                case "EXCURSION":
                    ResData = addingExcursionExtra(UserData, ResData, ref errorMsg);
                    break;
                case "INSURANCE":
                    ResData = addingInsuranceExtra(UserData, ResData, ref errorMsg);
                    break;
                case "RENTING":
                    ResData = addingRentingExtra(UserData, ResData, ref errorMsg);
                    break;
                case "TRANSFER":
                    ResData = addingTransferExtra(UserData, ResData, ref errorMsg);
                    break;
                case "VISA":
                    ResData = addingVisaExtra(UserData, ResData, ref errorMsg);
                    break;
                default:
                    ResData = addingOtherExtra(UserData, ResData, addingServices, ref errorMsg);
                    break;

            }
            return ResData;
        }

        public ResDataRecord addingExcursionExtra(User UserData, ResDataRecord _ResData, ref string errorMsg)
        {
            ResDataRecord ResData = new ResTables().copyData(_ResData);
            List<ResServiceRecord> newServices = new List<ResServiceRecord>();
            ResServiceRecord hotelService = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).FirstOrDefault();
            if (hotelService == null)
                return _ResData;
            HotelRecord hotel = new Hotels().getHotelDetail(UserData, hotelService.Service, ref errorMsg);
            List<ExcursionRecord> excursion = new Excursions().getExcursionListForLocationAndDate(UserData, ResData.ResMain.PLMarket, hotel.Location, ResData.ResMain.BegDate, ResData.ResMain.EndDate, false, ref errorMsg);
            foreach (ExcursionRecord row in excursion) {
                List<calendarColor> excDates = new Excursions().getExcursionDates(UserData, ResData.ResMain.PLMarket,
                                                                    string.IsNullOrEmpty(ResData.ResMain.PackType) ? DateTime.Today : ResData.ResMain.BegDate,
                                                                    string.IsNullOrEmpty(ResData.ResMain.PackType) ? DateTime.Today.AddYears(1) : ResData.ResMain.EndDate,
                                                                    hotel.Location, row.Code, ref errorMsg);
                DateTime excursionDate = ResData.ResMain.BegDate.Value;
                if (excDates != null && excDates.Count > 0) {
                    var query = from q in excDates
                                orderby q.Year, q.Month, q.Day
                                select new { date = new DateTime(q.Year.Value, q.Month.Value, q.Day.Value) };
                    excursionDate = query.FirstOrDefault().date;
                }
                ResData = new Reservation().AddService(UserData, ResData, null, 1, excursionDate, excursionDate, "EXCURSION", row.Code, "", "", "", "", (Int16)1, (Int16)0, (Int16)1, hotel.Location, hotel.Location, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, null, null);
                if (errorMsg == "") {
                    int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                    ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
                    service.ExcludeService = true;
                }
                _ResData = new ResTables().copyData(ResData);
                ResData = new ResTables().copyData(_ResData);

            }
            errorMsg = string.Empty;
            return _ResData;
        }

        public ResDataRecord addingInsuranceExtra(User UserData, ResDataRecord _ResData, ref string errorMsg)
        {
            decimal? Amount = null;
            ResDataRecord ResData = new ResTables().copyData(_ResData);
            Int16? RemainDay = Convert.ToInt16((DateTime.Today - ResData.ResMain.ResDate.Value).TotalDays);
            int? country = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
            List<InsuranceRecord> insuranceList = new Insurances().getInsuranceForCountry(UserData, ResData.ResMain.PLMarket, country, RemainDay, Amount, ref errorMsg);
            Int16 StartDay = 0;
            Int16 Night = Convert.ToInt16((ResData.ResMain.EndDate.Value - ResData.ResMain.BegDate.Value).Days);
            foreach (InsuranceRecord row in insuranceList/*.Where(w => w.InsType == 0)*/) {
                ResData = new TvBo.Reservation().AddService(UserData, ResData, null, 1, ResData.ResMain.BegDate.Value, ResData.ResMain.EndDate.Value, "INSURANCE", row.Code, string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, country, country, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, null, null);
                if (errorMsg == "") {
                    int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                    ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
                    service.ExcludeService = true;
                }
                _ResData = new ResTables().copyData(ResData);
                ResData = new ResTables().copyData(_ResData);
            }
            errorMsg = string.Empty;
            return _ResData;
        }

        public ResDataRecord addingRentingExtra(User UserData, ResDataRecord _ResData, ref string errorMsg)
        {
            ResDataRecord ResData = new ResTables().copyData(_ResData);
            int? location = ResData.ResMain.ArrCity;
            List<RentingRecord> rentingList = new Rentings().getRentingList(UserData.Market, ResData.ResMain.PLMarket, location, ResData.ResMain.BegDate, ResData.ResMain.EndDate, ref errorMsg);
            Int16 StartDay = 0;
            Int16 Night = Convert.ToInt16((ResData.ResMain.EndDate.Value - ResData.ResMain.BegDate.Value).Days);
            foreach (RentingRecord row in rentingList) {
                ResData = new TvBo.Reservation().AddService(UserData, ResData, null, 1, ResData.ResMain.BegDate.Value, ResData.ResMain.BegDate.Value, "RENTING", row.Code, string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, location, location, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, null, null);
                if (errorMsg == "") {
                    int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                    ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
                    service.ExcludeService = true;
                }
                _ResData = new ResTables().copyData(ResData);
                ResData = new ResTables().copyData(_ResData);
            }
            errorMsg = string.Empty;
            return _ResData;
        }

        public ResDataRecord addingVisaExtra(User UserData, ResDataRecord _ResData, ref string errorMsg)
        {
            ResDataRecord ResData = new ResTables().copyData(_ResData);
            Int16? RemainDay = Convert.ToInt16((DateTime.Today - ResData.ResMain.ResDate.Value).TotalDays);
            int? country = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
            List<VisaRecord> visaList = new Visas().getVisaCountry(UserData.Market, ResData.ResMain.PLMarket, country, ResData.ResMain.BegDate, ResData.ResMain.EndDate, ref errorMsg);
            Int16 StartDay = 0;
            Int16 Night = Convert.ToInt16((ResData.ResMain.EndDate.Value - ResData.ResMain.BegDate.Value).Days);
            foreach (VisaRecord row in visaList) {
                ResData = new TvBo.Reservation().AddService(UserData, ResData, null, 1, ResData.ResMain.BegDate.Value, ResData.ResMain.EndDate.Value, "VISA", row.Code, string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, country, country, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, null, null);
                if (errorMsg == "") {
                    int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                    ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
                    service.ExcludeService = true;
                }
                _ResData = new ResTables().copyData(ResData);
                ResData = new ResTables().copyData(_ResData);
            }
            errorMsg = string.Empty;
            return _ResData;
        }

        public ResDataRecord addingOtherExtra(User UserData, ResDataRecord _ResData, SaleServiceRecord addingServices, ref string errorMsg)
        {
            ResDataRecord ResData = new ResTables().copyData(_ResData);
            Int16? RemainDay = Convert.ToInt16((DateTime.Today - ResData.ResMain.ResDate.Value).TotalDays);
            int? country = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
            ResServiceRecord hotelService = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).FirstOrDefault();
            if (hotelService == null)
                return _ResData;
            HotelRecord hotel = new Hotels().getHotelDetail(UserData, hotelService.Service, ref errorMsg);
            List<AdServiceRecord> adServiceList = new AdServices().getAdServiceLocations(UserData.Market, ResData.ResMain.PLMarket, ResData.ResMain.BegDate, ResData.ResMain.EndDate, country, hotel.Location, ref errorMsg);
            Int16 StartDay = 0;
            Int16 Night = Convert.ToInt16((ResData.ResMain.EndDate.Value - ResData.ResMain.BegDate.Value).Days);
            foreach (AdServiceRecord row in adServiceList) {
                if (ResData.ResService.Where(w => w.ServiceType == row.Service && w.Service == row.Code).Count() < 1) {
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, null, 1, ResData.ResMain.BegDate.Value, ResData.ResMain.EndDate.Value, row.Service, row.Code, string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, country, hotel.Location, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, null, null);
                    if (errorMsg == "") {
                        int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                        ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
                        service.ExcludeService = true;
                    }
                    _ResData = new ResTables().copyData(ResData);
                    ResData = new ResTables().copyData(_ResData);
                }
            }
            errorMsg = string.Empty;
            return _ResData;
        }

        public ResDataRecord addingTransferExtra(User UserData, ResDataRecord _ResData, ref string errorMsg)
        {
            ResDataRecord ResData = new ResTables().copyData(_ResData);
            Int16? RemainDay = Convert.ToInt16((DateTime.Today - ResData.ResMain.ResDate.Value).TotalDays);
            ResServiceRecord hotelService = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).FirstOrDefault();
            List<ResServiceRecord> flightServices = ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).ToList<ResServiceRecord>();
            int? depLocation = ResData.ResMain.ArrCity;
            int? arrLocation = null;
            if (hotelService != null) {
                HotelRecord hotel = new Hotels().getHotelDetail(UserData, hotelService.Service, ref errorMsg);
                arrLocation = hotel.TrfLocation;
            }
            if (flightServices != null && flightServices.Count > 0) {
                depLocation = flightServices.OrderBy(o => o.BegDate).FirstOrDefault() != null ? flightServices.OrderBy(o => o.BegDate).FirstOrDefault().ArrLocation : ResData.ResMain.ArrCity;
            }
            List<TransferRecordV2> transferList = new Transfers().getTransfersV2(UserData.Market, ResData.ResMain, depLocation, arrLocation, ref errorMsg);
            Int16 StartDay = 0;
            Int16 Night = 1;
            DateTime? Date = _ResData.ResMain.BegDate;

            foreach (TransferRecordV2 row in transferList) {
                if (string.Equals(row.Direction, "F"))
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, null, 1, Date.Value, Date.Value, "TRANSFER", row.Code,
                        string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, row.Departure, row.Arrival.HasValue ? row.Arrival.Value : ResData.ResMain.ArrCity.Value, row.Direction, "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, null, null);
                else
                    if (string.Equals(row.Direction, "B")) {
                    Date = ResData.ResMain.EndDate;
                    StartDay = Convert.ToInt16((Date.Value - ResData.ResMain.BegDate.Value).Days);
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, null, 1, Date.Value, Date.Value, "TRANSFER", row.Code,
                        string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, row.Departure, row.Arrival.HasValue ? row.Arrival.Value : ResData.ResMain.ArrCity.Value, row.Direction, "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, null, null);
                } else {
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, null, 1, _ResData.ResMain.BegDate.Value, _ResData.ResMain.EndDate.Value, "TRANSFER", row.Code,
                        string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, row.Departure, row.Arrival.HasValue ? row.Arrival : ResData.ResMain.ArrCity.Value, row.Direction, "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, null, null);
                }
                if (errorMsg == "") {
                    int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                    ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
                    service.ExcludeService = true;
                }
                _ResData = new ResTables().copyData(ResData);
                ResData = new ResTables().copyData(_ResData);
            }
            errorMsg = string.Empty;
            return _ResData;
        }
    }
}

