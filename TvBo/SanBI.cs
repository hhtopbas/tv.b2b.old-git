﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TourVisio.WebService.Adapter.Enums;

namespace TvBo
{
    public class SanBI
    {
        public static void TrySend(PriceSearchRequest pRequest, int pResponseCount)
        {
            try
            {
                Send(pRequest, pResponseCount);
            }
            catch (Exception ex)
            {

            }
        }

        private static void Send(PriceSearchRequest pRequest, int pResponse)
        {
            SingletonSanBI.Instance.logger.Log(MapToSanBIModel(pRequest, pResponse));
        }

        private static Sanbi.Integration.Models.Tourvisio.Search.TvSearch MapToSanBIModel(PriceSearchRequest pRequest, int pResponse)
        {

            return new Sanbi.Integration.Models.Tourvisio.Search.TvSearch()
            {
                Agency = pRequest.UserData.AgencyID,// pRequest.GenericParam.TokenData.AgencyID,
                AgencyName = pRequest.UserData.AgencyName,// pRequest.GenericParam.TokenData.AgencyName,
                CheckInDate = pRequest.CheckInFrom.Value,

                //CheckInTime = new TimeSpan(),
                CheckInToDate = pRequest.CheckInTo,
                //CheckInToTime = fcdHelper.GetTimeSpan(pRequest.CheckInTo),
                ChildAges = MapToChildAges(pRequest),
                Culture = pRequest.UserData.Ci != null ? pRequest.UserData.Ci.Name : string.Empty,// pRequest.GenericParam.TokenData.Culture != null ? pRequest.GenericParam.TokenData.Culture.Name : null,
                Currency = pRequest.Currency,
                CustomerId = pRequest.UserData.CustomRegID,
                //DriverAge = pRequest.DriverAge,
                //DropOffDifferentThenPickUp = MapToDropOffDifferentThenPickUpProperty(pRequest),
                FromLocations = pRequest.DepCity.HasValue ? MapToFromLocationsProperty(pRequest, pRequest.DepCity.Value) : null,
                ToLocations = pRequest.ArrCity.HasValue ? MapToFromLocationsProperty(pRequest, pRequest.ArrCity.Value) : null,
                Market = pRequest.UserData.Market,
                MarketName = pRequest.UserData.Market,
                Nationality = null,
                Night = pRequest.NightFrom,
                NightTo = pRequest.NightTo,
                Operator = pRequest.UserData.Operator,
                OperatorName = pRequest.UserData.Operator,
                PriceFrom = pRequest.MinRoomPrice.HasValue ? Convert.ToDouble(pRequest.MinRoomPrice.Value) : (double?)null,
                PriceTo = pRequest.MaxRoomPrice.HasValue ? Convert.ToDouble(pRequest.MaxRoomPrice.Value) : (double?)null,
                Products = MapToProductsProperty(pRequest),
                SearchDate = DateTime.Now,
                SearchId = null,
                ResultCount = pResponse,
                SearchTypes = MapToSearchTypeProperties(pRequest),
                Source = "B2BV1",
                Travellers = MapToTravellers(pRequest),
                Unit = 1,
                AdditionalParams = MapToAdditionalParams(pRequest),
                ResponseTime = pRequest.PriceSearchTime
            };
        }

        //private static bool? MapToDropOffDifferentThenPickUpProperty(PriceSearchRequest pRequest)
        //{
        //    if (pRequest.ProductType != Adapter.Enums.enmProductType.Renting) return null;
        //    if (pRequest.DepartureLocations.Any() && pRequest.ArrivalLocations.Any()) return true;
        //    return false;
        //}

        private static Sanbi.Integration.Models.Tourvisio.Search.Location[] MapToFromLocationsProperty(PriceSearchRequest pRequest, int pLocId)
        {
            Location location = null;
            location = SanBICache.GetLocation(pRequest.UserData, pRequest.UserData.Market, pLocId);
            return new Sanbi.Integration.Models.Tourvisio.Search.Location[] {
                    new Sanbi.Integration.Models.Tourvisio.Search.Location {
                          Type = Sanbi.Integration.Models.Tourvisio.Search.LocationType.City,
                          Id=pLocId.ToString(),
                          Source= Sanbi.Integration.Models.Tourvisio.Search.Source.Own,
                          Name =location!=null? location.Name:null
                    }

                };
        }

        private static Sanbi.Integration.Models.Tourvisio.Search.SearchType[] MapToSearchTypeProperties(PriceSearchRequest pRequest)
        {
            List<Sanbi.Integration.Models.Tourvisio.Search.SearchType> result = new List<Sanbi.Integration.Models.Tourvisio.Search.SearchType>();
            result.Add(MapToSearchTypeProperty(pRequest.ProductType));
            return result.ToArray();
        }

        private static Sanbi.Integration.Models.Tourvisio.Search.SearchType MapToSearchTypeProperty(enmProductType pProductType)
        {
            switch (pProductType)
            {
                case enmProductType.Undefined:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Undefined;
                case enmProductType.HolidayPackage:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.HolidayPackage;
                case enmProductType.Hotel:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Hotel;
                case enmProductType.Flight:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Flight;
                case enmProductType.Excursion:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Excursion;
                case enmProductType.Transfer:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Transfer;
                case enmProductType.Tour:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Tour;
                case enmProductType.Cruise:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Cruise;
                case enmProductType.Transport:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Transport;
                case enmProductType.Ferry:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Ferry;
                case enmProductType.Visa:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Visa;
                case enmProductType.AdditionalService:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.AdditionalService;
                case enmProductType.Insurance:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Insurance;
                case enmProductType.Dynamic:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Dynamic;
                case enmProductType.Renting:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Renting;
                case enmProductType.ExtraServices:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.ExtraServices;
                case enmProductType.ExcursionPackage:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.ExcursionPackage;
                case enmProductType.HandFee:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.HandFee;
                default:
                    return Sanbi.Integration.Models.Tourvisio.Search.SearchType.Undefined;
            }
        }

        private static Sanbi.Integration.Models.Tourvisio.Search.Product[] MapToProductsProperty(PriceSearchRequest pRequest)
        {
            List<Sanbi.Integration.Models.Tourvisio.Search.Product> result = new List<Sanbi.Integration.Models.Tourvisio.Search.Product>();
            if (!string.IsNullOrEmpty(pRequest.HolPack))
            {
                var holpack = SanBICache.GetHolPack(pRequest.UserData, pRequest.UserData.Market, pRequest.HolPack);
                result.Add(
                    new Sanbi.Integration.Models.Tourvisio.Search.Product
                    {
                        Source = Sanbi.Integration.Models.Tourvisio.Search.Source.Own,
                        Id = pRequest.HolPack,
                        Name = holpack != null ? holpack.Name : null

                    });
            }
            if (!string.IsNullOrEmpty(pRequest.HotelCode))
            {

                var hotel = SanBICache.GetHotel(pRequest.UserData, pRequest.UserData.Market, pRequest.HotelCode);
                result.Add(
                    new Sanbi.Integration.Models.Tourvisio.Search.Product
                    {
                        Source = Sanbi.Integration.Models.Tourvisio.Search.Source.Own,
                        Id = hotel.Code,
                        Name = hotel != null ? hotel.Name : null
                    });
            }
            return result.ToArray();
        }



        private static Sanbi.Integration.Models.Tourvisio.Search.LocationType MapToLocationTypeProperty(enmLocationType? pLocationType)
        {
            switch (pLocationType)
            {
                case enmLocationType.Airport:
                    return Sanbi.Integration.Models.Tourvisio.Search.LocationType.Airport;
                default:
                    return Sanbi.Integration.Models.Tourvisio.Search.LocationType.City;
            }
        }

        private static Dictionary<string, int> MapToTravellers(PriceSearchRequest pRequest)
        {
            var passengers = new Dictionary<string, int>();
            passengers.Add("Adult", pRequest.Adult);
            if (pRequest.Child > 0)
                passengers.Add("Child", pRequest.Child);
            return passengers;
        }

        private static int[] MapToChildAges(PriceSearchRequest pRequest)
        {
            var result = new List<int>();
            if (pRequest.Child > 0 && pRequest.ChdAge1 > -1)
            {
                result.Add(pRequest.ChdAge1.Value);
            }
            if (pRequest.Child > 1 && pRequest.ChdAge2 > -1)
            {
                result.Add(pRequest.ChdAge2.Value);
            }
            if (pRequest.Child > 2 && pRequest.ChdAge2 > -1)
            {
                result.Add(pRequest.ChdAge2.Value);
            }
            if (pRequest.Child > 3 && pRequest.ChdAge4 > -1)
            {
                result.Add(pRequest.ChdAge4.Value);
            }
            return result.ToArray();
        }

        private static Sanbi.Integration.Models.Tourvisio.Search.AdditionalParam[] MapToAdditionalParams(PriceSearchRequest pRequest)
        {
            var result = new List<Sanbi.Integration.Models.Tourvisio.Search.AdditionalParam>();

            if (pRequest.ProductType == enmProductType.Hotel || pRequest.ProductType == enmProductType.HolidayPackage)
            {
                if (!string.IsNullOrEmpty(pRequest.HolPack))
                {
                    var holpack = SanBICache.GetHolPack(pRequest.UserData, pRequest.UserData.Market, pRequest.HolPack);
                    result.Add(new Sanbi.Integration.Models.Tourvisio.Search.AdditionalParam()
                    {
                        Type = fcdSanBIContract.AdditionalParam_Type_Package,
                        Id = pRequest.HolPack,
                        Name = holpack != null ? holpack.Name : null
                    });
                }
            }
            if (pRequest.ProductType == enmProductType.Excursion)
            {
                if (!string.IsNullOrEmpty(pRequest.Excursion))
                {
                    var excursion = SanBICache.GetExcursion(pRequest.UserData, pRequest.Excursion);
                    result.Add(new Sanbi.Integration.Models.Tourvisio.Search.AdditionalParam()
                    {
                        Type = fcdSanBIContract.AdditionalParam_Type_Excursion,
                        Id = pRequest.Excursion,
                        Name = excursion != null ? excursion.Name : null
                    });
                }
            }
            if (pRequest.ProductType == enmProductType.Flight)
            {
                if (!string.IsNullOrEmpty(pRequest.FlightClass))
                {
                    result.Add(new Sanbi.Integration.Models.Tourvisio.Search.AdditionalParam()
                    {
                        Type = fcdSanBIContract.AdditionalParam_Type_Excursion,
                        Id = pRequest.Excursion,
                        Name = null
                    });
                }
            }

            return result.ToArray();
        }
    }
    internal static class fcdSanBIContract
    {
        internal const string Undefined = "Undefined";
        internal const string AdditionalParam_Type_FlightClass = "FLIGHTCLASS";
        internal const string AdditionalParam_Type_ServiceType = "ServiceType";
        internal const string AdditionalParam_Type_Category = "Category";
        internal const string AdditionalParam_Type_PackageType = "PackageType";
        internal const string AdditionalParam_Type_Package = "Package";
        internal const string AdditionalParam_Type_Excursion = "Excursion";

        internal const string AdditionalParam_Id_ONEWAY = "ONEWAY";
        internal const string AdditionalParam_Id_ROUNDTRIP = "ROUNDTRIP";
        internal const string AdditionalParam_Id_FROMAIRPORT = "FROMAIRPORT";
        internal const string AdditionalParam_Id_TOAIRPORT = "TOAIRPORT";
    }
    public sealed class SingletonSanBI
    {
        private static readonly Lazy<SingletonSanBI>
            lazy = new Lazy<SingletonSanBI>(() => new SingletonSanBI());

        public static SingletonSanBI Instance { get { return lazy.Value; } }
        public Sanbi.Integration.TvSearch.DbSearchLogger logger;
        private SingletonSanBI()
        {
            var dataConfig = (Microsoft.Practices.EnterpriseLibrary.Data.Configuration.DatabaseSettings)System.Configuration.ConfigurationManager.GetSection("dataConfiguration");
            string connectionString = ConfigurationManager.ConnectionStrings[dataConfig.DefaultDatabase].ConnectionString;
            var connectionStringBuilder = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionString);

            Sanbi.Integration.TvSearch.ISearchRepository searchRepository =
                new Sanbi.Integration.TvSearch.SearchRepository(connectionStringBuilder.DataSource, connectionStringBuilder.UserID, connectionStringBuilder.Password);
            logger = new Sanbi.Integration.TvSearch.DbSearchLogger(searchRepository);

        }
    }
}
