﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;
using System.Globalization;
using System.Web;
using System.Threading;

namespace TvBo
{
    public class Bonus
    {
        private List<BonusDataRecord> getBonusList(string BType, string Code)
        {
            List<BonusDataRecord> list = new List<BonusDataRecord>();
            string tsql = @"Declare @RecID int
                            Select @RecID=RecID From Bonus (NOLOCK)
                            Where @Code=(Case When @BType='A' Then Agency Else IDNo End) And Type=@BType

                            Select D.MasRecID,D.ResNo,D.HolPack,D.Agency,M.BegDate,M.ResDate,UserBonus=M.AgencyBonus,      
                              MinExpire=(Select Top 1 [ExpireDate] From BonusDet (NOLOCK) Where ResNo=D.ResNo and [ExpireDate] is not null),              
                              D.Curr, D.Manual, 
                              hasRemark=case when isnull(D.Remark,'')<>'' then 1 else 0 end,
                              Bonus=Sum(isnull(BonusVal,0)), 
                              Price=Sum(isnull(Price,0)),      
                              ExpBonus=(Select isnull(sum(isnull(V.BonusVal,0)),0)-isnull(sum(isnull(U.BonusVal,0)),0)
                                        From BonusDet2 (NOLOCK) V
                                        join BonusDet DD on V.MasRecID=DD.RecID      				   
                                        Outer Apply 
                                        (
                                           Select BonusVal=isnull(sum(isnull(BonusVal,0)),0) 
                                           From BonusUsed (NOLOCK) BU 
                                           where BU.MasRecID=DD.RecID
                                        ) U      
                                        Where DD.MasRecID=@RecID and DD.ResNo=D.ResNo and V.[ExpireDate]<GetDate()),      
                              UsedBonus=(Select Sum(isnull(BonusVal,0)) 
                                         From BonusUsed (NOLOCK)
                                         Join
                                         (
				                            Select DD.RecID From BonusDet (NOLOCK) DD       
                                            Join Bonus (NOLOCK) B on B.RecID=DD.MasRecID      
	                                        Where DD.ResNo=D.ResNo and B.[Type]=@BType 
                                         ) Bd ON Bd.RecID=BonusUsed.MasRecID             
	                                    ),
                              D.[ExpireDate]
                            From BonusDet D (NOLOCK)
                            Left join ResMain M (NOLOCK) on M.ResNo=D.ResNo
                            Where D.MasRecID=@RecID
                            Group by D.MasRecID,D.ResNo,D.HolPack,D.Agency,D.Curr,D.[Manual],D.Remark,M.BegDate,M.ResDate,M.AgencyBonus,D.[ExpireDate]
                            Order by D.ResNo
                            ";            
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "BType", DbType.String, BType);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BonusDataRecord b = new BonusDataRecord();
                        b.ResNo = Conversion.getStrOrNull(reader["ResNo"]);
                        b.Price = Conversion.getDecimalOrNull(reader["Price"]);
                        b.Curr = Conversion.getStrOrNull(reader["Curr"]);
                        b.ExpireDate = Conversion.getDateTimeOrNull(reader["ExpireDate"]);
                        b.Bonus = Conversion.getDecimalOrNull(reader["Bonus"]);
                        b.UsedBonus = Conversion.getDecimalOrNull(reader["UsedBonus"]);
                        b.ExpBonus = Conversion.getDecimalOrNull(reader["ExpBonus"]);
                        list.Add(b);
                    }
                }
                return list;
            }
            catch
            {
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getBonusHtml(User UserData, string BType, string Code)
        {
            CultureInfo ci = UserData.Ci;
            Thread.CurrentThread.CurrentCulture = ci;
            if (!string.IsNullOrEmpty(BType) && !string.IsNullOrEmpty(Code))
            {
                List<BonusDataRecord> dt = getBonusList(BType, Code);
                StringBuilder sb = new StringBuilder();

                if (dt != null && dt.Count > 0)
                {                    
                    sb.AppendFormat("<b>{0}</b> {1}<br />",
                            HttpContext.GetGlobalResourceObject("Bonus", "BLBonusList").ToString(),
                            DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                    sb.AppendFormat("<b>{0}</b><br />", BType == "U" ? UserData.UserName : UserData.AgencyName);
                    sb.Append("<table cellpadding=\"5\">");
                    sb.AppendFormat("<tr><td class=\"ResNoH\">{0}</td><td class=\"BonusH\">{1}</td><td class=\"BonusH\">{2}</td><td class=\"BonusH\">{3}</td><td class=\"PriceH\">{4}</td><td class=\"CurrH\">{5}</td><td class=\"ExpDateH\">{6}</td></tr>",
                            HttpContext.GetGlobalResourceObject("Bonus", "BLResNo").ToString(),
                            HttpContext.GetGlobalResourceObject("Bonus", "BLBonus").ToString(), 
                            HttpContext.GetGlobalResourceObject("Bonus", "BLUsedBonus").ToString(),
                            HttpContext.GetGlobalResourceObject("Bonus", "BLExpiredBonus").ToString(),
                            HttpContext.GetGlobalResourceObject("Bonus", "BLPrice").ToString(),
                            HttpContext.GetGlobalResourceObject("Bonus", "BLCurr").ToString(),
                            HttpContext.GetGlobalResourceObject("Bonus", "BLExpDate").ToString());
                    foreach (BonusDataRecord row in dt)
                    {
                        sb.AppendFormat("<tr><td class=\"ResNo\">{0}</td><td class=\"Bonus\">{1}</td><td class=\"Bonus\">{2}</td><td class=\"Bonus\">{3}</td><td class=\"Price\">{4}</td><td class=\"Curr\">{5}</td><td class=\"ExpDate\">{6}</td></tr>",
                            row.ResNo,
                            row.Bonus.Value.ToString(),                            
                            row.UsedBonus.HasValue ? row.UsedBonus.Value.ToString("#,###.00") : "&nbsp;",
                            row.ExpBonus.HasValue ? row.ExpBonus.Value.ToString("#,###.00") : "&nbsp;",
                            row.Price.Value.ToString(),
                            row.Curr,
                            row.ExpireDate.HasValue ? row.ExpireDate.Value.ToShortDateString() : "&nbsp;");
                    }
                    sb.Append("</table>");                    
                }
                return sb.ToString();
            }
            else return "";
        }

        public AgencyBonusTotalRecord getBonus(string Market, string AgencyID, ref string errorMsg)
        {
            string tsql = @"Select X.RecID, X.Agency, X.TotBonus, X.TotUsed, X. TotExpBonus,
                                A.Name, A.Location, A.Phone1, A.Phone2, A.Fax1, A.Fax2, A.Email1,
	                            TotUseable=RealBonus-TotUsed-TotExpBonus, MinExpDate
                            From
                            (
                              Select Distinct B.RecID, B.Agency, 
                              TotBonus = (Select isnull(Sum(isnull(BonusVal,0)),0) From BonusDet (NOLOCK) Where MasRecID=B.RecID And Market = @Market),
                              TotUsed = (Select isnull(Sum(isnull(U.BonusVal,0)),0) 
                                         From BonusDet (NOLOCK) D 
                                         Join BonusUsed (NOLOCK) U ON U.MasRecID=D.RecID 
                                         Where D.MasRecID=B.RecID),
                              TotExpBonus = (Select isnull(Sum(isnull(V.BonusVal,0)),0) From BonusDet (NOLOCK) D
                                             Join BonusDet2 (NOLOCK) V ON V.MasRecID = D.RecID
                                             Where D.Market=@Market and D.MasRecID=B.RecID and V.ExpireDate<GetDate()),
                              RealBonus = (Select isnull(Sum(isnull(BonusVal,0)),0) 
				                            From BonusDet (NOLOCK)  
				                            Where MasRecID=B.RecID And 
				                            (((Select BegDate From ResMain (NOLOCK) Where ResNo=BonusDet.ResNo)<=GetDate()) or 
                                             (Manual='Y' And Market=@Market And ExpireDate>=GetDate() And TourBegDate<=GetDate()))),
                              MinExpDate = (Select Min(ExpireDate) From BonusDet (NOLOCK) Where MasRecID=B.RecID And 
                                            (((Select BegDate From ResMain (NOLOCK) Where ResNo=BonusDet.ResNo)<=GetDate()) or 
                                              (Manual='Y' And ExpireDate>=GetDate() And TourBegDate<=GetDate())))
                              From Bonus B (NOLOCK)
                              Join BonusDet D (NOLOCK) on D.MasRecID=B.RecID
                              where B.Type='A' And D.Market=@Market
                            ) X
                            Join Agency(NOLOCK) A on A.Code=X.Agency
                            Where Agency=@Agency";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {

                db.AddInParameter(dbCommand, "Agency", DbType.String, AgencyID);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        AgencyBonusTotalRecord record = new AgencyBonusTotalRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Agency = Conversion.getStrOrNull(R["Agency"]);
                        record.TotBonus = Conversion.getDecimalOrNull(R["TotBonus"]);
                        record.TotUsed = Conversion.getDecimalOrNull(R["TotUsed"]);
                        record.TotExpBonus = Conversion.getDecimalOrNull(R["TotExpBonus"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.Location = Conversion.getInt32OrNull(R["Location"]);
                        record.Phone1 = Conversion.getStrOrNull(R["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(R["Phone2"]);
                        record.Fax1 = Conversion.getStrOrNull(R["Fax1"]);
                        record.Fax2 = Conversion.getStrOrNull(R["Fax2"]);
                        record.Email1 = Conversion.getStrOrNull(R["Email1"]);
                        record.TotUseable = Conversion.getDecimalOrNull(R["TotUseable"]);
                        record.MinExpDate = Conversion.getDateTimeOrNull(R["MinExpDate"]);
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Dispose();
            }
        }

        public UserBonusTotalRecord getBonus(DateTime? Date, string SaleCur, string BType, string IDNo, ref string errorMsg)
        {
            string tsql = @"Declare @TotBonus dec(18,2), @TotBonusAmount dec(18,2), @UsedBonus dec(18,2), @UsedBonusAmount dec(18,2),
                              @UseableBonus dec(18,2), @UseableBonusAmount dec(18,2), @TargetBonus dec(18,2), @ErrCode smallint

                            Exec usp_GetBonus @BType, @IDNo, @BegDate, @Curr, @Price, 
	                            @TotBonus output, @TotBonusAmount output, @UsedBonus output, @UsedBonusAmount output,
	                            @UseableBonus output, @UseableBonusAmount output, @TargetBonus output, @ErrCode output
                            	
                            Select TotBonus=isnull(@TotBonus, 0.0), TotBonusAmount=isnull(@TotBonusAmount, 0.0), 
	                               UsedBonus=isnull(@UsedBonus, 0.0), UsedBonusAmount=isnull(@UsedBonusAmount, 0.0), 
	                               UseableBonus=isnull(@UseableBonus, 0.0), UseableBonusAmount=isnull(@UseableBonusAmount, 0.0), 
	                               TargetBonus=isnull(@TargetBonus, 0.0), ErrCode=@ErrCode ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {

                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, Date);
                db.AddInParameter(dbCommand, "Curr", DbType.String, SaleCur);
                db.AddInParameter(dbCommand, "BType", DbType.String, BType);
                db.AddInParameter(dbCommand, "IDNo", DbType.String, IDNo);
                db.AddInParameter(dbCommand, "Price", DbType.Decimal, -1);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        UserBonusTotalRecord record = new UserBonusTotalRecord();
                        record.TotBonus = Conversion.getDecimalOrNull(R["TotBonus"]);
                        record.TotBonusAmount = Conversion.getDecimalOrNull(R["TotBonusAmount"]);
                        record.UsedBonus = Conversion.getDecimalOrNull(R["UsedBonus"]);
                        record.UsedBonusAmount = Conversion.getDecimalOrNull(R["UsedBonusAmount"]);
                        record.UseableBonus = Conversion.getDecimalOrNull(R["UseableBonus"]);
                        record.UseableBonusAmount = Conversion.getDecimalOrNull(R["UseableBonusAmount"]);
                        record.TargetBonus = Conversion.getDecimalOrNull(R["TargetBonus"]);
                        record.ErrCode = Conversion.getInt16OrNull(R["ErrCode"]);
                        if (record.ErrCode == 2)
                        {
                            record.UseableBonusAmount = 0;
                            record.UseableBonus = 0;
                        }
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Dispose();
            }
        }

        public UsedBonusRecord usedBonus(User UserData, ResDataRecord ResData, string BType, string IDNo, decimal? BonusAmount, Database db, DbTransaction dbT, ref int? ErrCode, ref string errorMsg)
        {
            /* 1=No Bonus Amount 
               2=Conversion table is not specified. 
               3=Bonus conversion table has some mistake. */
            StringBuilder sb = new StringBuilder();
            sb = new Reservation().BuildCalcSqlString(ResData);

            string tsql = @"Declare @UsedBonus dec(18,2), @UsedBonusAmount dec(18,2)
                            Exec dbo.usp_UseBonus @ResNo, @BType, @IDNo, @ReceiceDate, @BonusAmount, 
                                    @UsedBonus output, @UsedBonusAmount output, @ErrCode output 
                            Select UsedBonus=@UsedBonus, UsedBonusAmount=@UsedBonusAmount, ErrCode=@ErrCode ";            
            if (BType == "A") IDNo = ResData.ResMain.Agency;            
            DbCommand dbCommandBonus = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommandBonus, "ResNo", DbType.String, ResData.ResMain.ResNo);
                db.AddInParameter(dbCommandBonus, "BType", DbType.String, BType);
                db.AddInParameter(dbCommandBonus, "IDNo", DbType.String, string.IsNullOrEmpty(IDNo) ? null : IDNo);
                db.AddInParameter(dbCommandBonus, "ReceiceDate", DbType.DateTime, DateTime.Today);
                db.AddInParameter(dbCommandBonus, "ErrCode", DbType.Int32, ErrCode);
                db.AddInParameter(dbCommandBonus, "BonusAmount", DbType.Decimal, BonusAmount);

                using (IDataReader R = db.ExecuteReader(dbCommandBonus))
                {
                    if (R.Read())
                    {
                        UsedBonusRecord record = new UsedBonusRecord();
                        record.UsedBonus = Conversion.getDecimalOrNull(R["UsedBonus"]);
                        record.UsedBonusAmount = Conversion.getDecimalOrNull(R["UsedBonusAmount"]);
                        record.ErrCode = Conversion.getInt32OrNull(R["ErrCode"]);

                        return record;
                    }
                    else
                    {
                        ErrCode = -1;
                        errorMsg = "No row count.";
                        return null;
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrCode = -2;
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommandBonus.Dispose();
            }
        }

        public UsedBonusRecord usedBonusTemp(User UserData, ResDataRecord ResData, string BType, string IDNo, decimal? BonusAmount, ref int? ErrCode, ref string errorMsg)
        {
            /* 1=No Bonus Amount 
               2=Conversion table is not specified. 
               3=Bonus conversion table has some mistake. */            
            StringBuilder sb = new StringBuilder();
            sb = new Reservation().BuildCalcSqlString(ResData);

            string tsql = @"Declare @UsedBonus dec(18,2), @UsedBonusAmount dec(18,2)
                            Exec dbo.usp_UseBonus @ResNo, @BType, @IDNo, @ReceiceDate, @BonusAmount, 
                                    @UsedBonus output, @UsedBonusAmount output, @ErrCode output 
                            Select UsedBonus=@UsedBonus, UsedBonusAmount=@UsedBonusAmount, ErrCode=@ErrCode ";

            if (ErrCode == 9) tsql = sb.ToString() + tsql;
            if (BType == "A") IDNo = ResData.ResMain.Agency;

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {     
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResData.ResMain.ResNo);
                db.AddInParameter(dbCommand, "BType", DbType.String, BType);
                db.AddInParameter(dbCommand, "IDNo", DbType.String, string.IsNullOrEmpty(IDNo) ? null : IDNo);
                db.AddInParameter(dbCommand, "ReceiceDate", DbType.DateTime, DateTime.Today);
                db.AddInParameter(dbCommand, "ErrCode", DbType.Int32, ErrCode);
                db.AddInParameter(dbCommand, "BonusAmount", DbType.Decimal, BonusAmount);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        UsedBonusRecord record = new UsedBonusRecord();
                        record.UsedBonus = Conversion.getDecimalOrNull(R["UsedBonus"]);
                        record.UsedBonusAmount = Conversion.getDecimalOrNull(R["UsedBonusAmount"]);
                        record.ErrCode = Conversion.getInt32OrNull(R["ErrCode"]);
                        ErrCode = record.ErrCode;
                        return record;
                    }
                    else
                    {
                        ErrCode = -1;
                        errorMsg = "No row count.";
                        return null;
                    }
                }                
            }
            catch (Exception Ex)
            {
                ErrCode = -2;
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Dispose();                
            }
        }

    }
}
