﻿using System;
using System.Data;
using System.Collections.Generic;
using TvTools;

namespace TvBo
{
    public enum ResResource : short { B2B = 0, B2CToB2B = 1, PytonG7 = 2, ReservationReqestForm = 3 }
    public enum SaleResource : short { Backoffice = 0, B2B = 1, BackofficeTicket = 2, B2BTicket = 3, B2C = 4, B2CTicket = 5, Portal = 6, OnlyTicket = 90, Package = 91 }
    public enum RecordStatus : short { New = 0, Old = 1, Changed = 2, Deleted = 3 }
    public enum ResUserCheck : short { NotAllowed = 0, ResIsUser = 1, ResIsOtherUser = 2, ResIsTvUser = 3, ResIsOnlyView = 4, ResIsViewAndPrintDocument = 5, ResNotFound = 6, SuperUser = 7 }
    public enum PytonG7BaseGender { m, v, M, V }
    public enum B2CToB2BSource : short { PaketSearch = 0, OnlyTicket = 1, OnlyTransfer = 2, Excursion = 3 }
    public enum SaveAndUpdate : short { ResMain = 0, ResCust = 1, ResCustInfo = 2, ResService = 3, ResServiceExt = 4, ResCon = 5, ResConExt = 6, ResSupDis = 7, Bonus = 8, ResPromo = 9, ResCustVisa = 10, ResCustVisa2 = 11 }

    [Serializable()]
    public class ReCalculateStatus
    {
        public SaveAndUpdate StatusSection { get; set; }
        public bool Status { get; set; }
        public ReCalculateStatus()
        {
            this.Status = true;
        }
    }

    [Serializable()]
    public class ExternalReservationService
    {
        public string ServiceType { get; set; }
        public string Service { get; set; }
        public DateTime? BegDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Int16? Night { get; set; }
        public Int16? Duration { get; set; }
        public Int16? StartDay { get; set; }
        public Int32? Departure { get; set; }
        public Int32? Arrival { get; set; }
        public string Direction { get; set; }
        public Int16? PriceType { get; set; }

        public ExternalReservationService()
        {
            this.PriceType = 0;
            this.StartDay = 0;
            this.Duration = 1;
        }

    }

    [Serializable()]
    public class ExternalReservation
    {
        public Int16? SaleResource { get; set; }
        public string SaleCur { get; set; }
        public Int16? Adult { get; set; }
        public Int16? Child { get; set; }
        public Int16? ChdAge1 { get; set; }
        public Int16? ChdAge2 { get; set; }
        public Int16? ChdAge3 { get; set; }
        public Int16? ChdAge4 { get; set; }
        public List<ExternalReservationService> Services { get; set; }

        public ExternalReservation()
        {
            this.Services = new List<ExternalReservationService>();
        }
    }

    public class ServiceExtRecord
    {
        public ServiceExtRecord()
        {
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        bool _Status;
        public bool Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        Int16? _DispNo;
        public Int16? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        bool _PayCom;
        public bool PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        bool _PayComEB;
        public bool PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool _WebSale;
        public bool WebSale
        {
            get { return _WebSale; }
            set { _WebSale = value; }
        }

        bool _SepInInv;
        public bool SepInInv
        {
            get { return _SepInInv; }
            set { _SepInInv = value; }
        }

        bool _DispInPNL;
        public bool DispInPNL
        {
            get { return _DispInPNL; }
            set { _DispInPNL = value; }
        }

        bool _DispInFPasLst;
        public bool DispInFPasLst
        {
            get { return _DispInFPasLst; }
            set { _DispInFPasLst = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        bool? _TakeAllUser;
        public bool? TakeAllUser
        {
            get { return _TakeAllUser; }
            set { _TakeAllUser = value; }
        }

        string _SpecSerRQCode;
        public string SpecSerRQCode
        {
            get { return _SpecSerRQCode; }
            set { _SpecSerRQCode = value; }
        }

        bool? _UseSPinPack;
        public bool? UseSPinPack
        {
            get { return _UseSPinPack; }
            set { _UseSPinPack = value; }
        }

        bool? _DispInTrpPaxList;
        public bool? DispInTrpPaxList
        {
            get { return _DispInTrpPaxList; }
            set { _DispInTrpPaxList = value; }
        }

        bool? _RTSale;
        public bool? RTSale
        {
            get { return _RTSale; }
            set { _RTSale = value; }
        }

        int? _ExtSerCat;
        public int? ExtSerCat
        {
            get { return _ExtSerCat; }
            set { _ExtSerCat = value; }
        }

        bool? _MultiSaleSer;
        public bool? MultiSaleSer
        {
            get { return _MultiSaleSer; }
            set { _MultiSaleSer = value; }
        }

    }

    public class PriceListExtraServiceRecord
    {
        public PriceListExtraServiceRecord()
        {
        }

        private int _CatPackID;
        public int CatPackID
        {
            get { return _CatPackID; }
            set { _CatPackID = value; }
        }

        int _RecNo;
        public int RecNo
        {
            get { return _RecNo; }
            set { _RecNo = value; }
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        Int16? _CalcType;
        public Int16? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        Int16? _StepNo;
        public Int16? StepNo
        {
            get { return _StepNo; }
            set { _StepNo = value; }
        }

        int? _Departure;
        public int? Departure
        {
            get { return _Departure; }
            set { _Departure = value; }
        }

        int? _Arrival;
        public int? Arrival
        {
            get { return _Arrival; }
            set { _Arrival = value; }
        }

        string _Direction;
        public string Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

    }

    public class plResServiceRecord
    {
        public int? CatPackID { get; set; }
        public int? RecNo { get; set; }
        public string ServiceType { get; set; }
        public string Service { get; set; }
        public string NetCur { get; set; }
        public string Supplier { get; set; }
        public string SClass { get; set; }
        public int? PriceType { get; set; }
        public int? NetPriceID { get; set; }
        public Int16? StepNo { get; set; }
        public Int16? Night { get; set; }
        public Int16? StartDay { get; set; }
        public Int16? Duration { get; set; }
        public int? CatPricePID { get; set; }
        public int? Departure { get; set; }
        public int? Arrival { get; set; }
        public string Direction { get; set; }
        public bool? ResSeparate { get; set; }
        public string Room { get; set; }
        public string Board { get; set; }
        public string Accom { get; set; }
        public string Class { get; set; }
        public string DepRet { get; set; }
        public string PLService { get; set; }
        public string Bus { get; set; }
        public string AllotType { get; set; }
        public int? Pickup { get; set; }
        public DateTime? PickupTime { get; set; }
        public DateTime? RPickupTime { get; set; }
        public string PickupNote { get; set; }
        public string RPickupNote { get; set; }
        public byte? TrfDT { get; set; }
        public byte? TrfAT { get; set; }
        public byte? RTrfDT { get; set; }
        public byte? RTrfAT { get; set; }
        public string TrfDep { get; set; }
        public string TrfArr { get; set; }
        public string RTrfDep { get; set; }
        public string RTrfArr { get; set; }
        public int? GrupPax { get; set; }
        public string PayCom { get; set; }
        public string PayComEB { get; set; }
        public bool PayPasEB { get; set; }
        public Int16? StepType { get; set; }
        public int? ExcurPackID { get; set; }
        public string PriceCode { get; set; }
        public DateTime? ExcTransTime { get; set; }
        public int? ExcFreeTime { get; set; }
        public int? ExcTimeID { get; set; }
        public string NameS { get; set; }
        public int? VehicleCatID { get; set; }
        public Int16? VehicleUnit { get; set; }
        public plResServiceRecord()
        {
        }
    }

    public class serviceDefaultStatusRecord
    {
        public serviceDefaultStatusRecord()
        {
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _PayCom;
        public string PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        Int16 _ConfStat;
        public Int16 ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        string _PayComEB;
        public string PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

    }

    [Serializable()]
    public class FirstResData
    {
        public SearchType ReservationType { get; set; }
        public ResMainRecord ResMain { get; set; }
        public List<ResCustRecord> ResCust { get; set; }
        public List<ResCustInfoRecord> ResCustInfo { get; set; }
        public List<ResCustVisaRecord> ResCustVisa { get; set; }
        public List<ResCustVisa2Record> ResCustVisa2 { get; set; }
        public List<ResServiceRecord> ResService { get; set; }
        public List<ResConRecord> ResCon { get; set; }
        public List<ResServiceExtRecord> ResServiceExt { get; set; }
        public List<ResConExtRecord> ResConExt { get; set; }
        public List<ResSupDisRecord> ResSupDis { get; set; }
        public List<ResPromoRecord> ResPromo { get; set; }
        public FirstResData()
        {
            this.ReservationType = SearchType.NoSearch;
            this.ResMain = new ResMainRecord(null);
            this.ResCust = new List<ResCustRecord>();
            this.ResCustInfo = new List<ResCustInfoRecord>();
            this.ResCustVisa = new List<ResCustVisaRecord>();
            this.ResCustVisa2 = new List<ResCustVisa2Record>();
            this.ResService = new List<ResServiceRecord>();
            this.ResCon = new List<ResConRecord>();
            this.ResServiceExt = new List<ResServiceExtRecord>();
            this.ResConExt = new List<ResConExtRecord>();
            this.ResSupDis = new List<ResSupDisRecord>();
            this.ResPromo = new List<ResPromoRecord>();
        }
    }

    [Serializable()]
    public class ResDataRecord
    {
        public ResDataRecord()
        {
            _ResMain = new ResMainRecord(null);
            _ResCust = new List<ResCustRecord>();
            _ResCustInfo = new List<ResCustInfoRecord>();
            _ResCustVisa = new List<ResCustVisaRecord>();
            _ResService = new List<ResServiceRecord>();
            _ResCon = new List<ResConRecord>();
            _ResServiceExt = new List<ResServiceExtRecord>();
            _ResConExt = new List<ResConExtRecord>();
            _ResCustPrice = new List<ResCustPriceRecord>();
            _ResSupDis = new List<ResSupDisRecord>();
            _ResPromo = new List<ResPromoRecord>();
            _Title = new List<TitleRecord>();
            _SelectBook = new List<SearchResult>();
            _SelectBookOH = new List<SearchResultOH>();
            _SelectBookPP = new List<PackPriceSearchResult>();
            _ResFlightDetail = new List<ResFlightDetailRecord>();
            _PromoList = new List<PromoListRecord>();
            _ResChgFee = new List<ResChgFeeRecord>();
            _ResHotelList = new List<ResHotelRecord>();
            _ResCalcErrList = new List<ResCalcErr>();
            _Resource = ResResource.B2B;
            _onlyTransfer = false;
            this.FirstData = new FirstResData();
        }

        ResResource _Resource;
        public ResResource Resource
        {
            get { return _Resource; }
            set { _Resource = value; }
        }

        string _OwnAgency;
        public string OwnAgency
        {
            get { return _OwnAgency; }
            set { _OwnAgency = value; }
        }

        string _OwnUser;
        public string OwnUser
        {
            get { return _OwnUser; }
            set { _OwnUser = value; }
        }

        ResMainRecord _ResMain;
        public ResMainRecord ResMain
        {
            get { return _ResMain; }
            set { _ResMain = value; }
        }

        List<ResCustRecord> _ResCust;
        public List<ResCustRecord> ResCust
        {
            get { return _ResCust; }
            set { _ResCust = value; }
        }

        List<ResCustInfoRecord> _ResCustInfo;
        public List<ResCustInfoRecord> ResCustInfo
        {
            get { return _ResCustInfo; }
            set { _ResCustInfo = value; }
        }

        List<ResCustVisaRecord> _ResCustVisa;
        public List<ResCustVisaRecord> ResCustVisa
        {
            get { return _ResCustVisa; }
            set { _ResCustVisa = value; }
        }

        List<ResCustVisa2Record> _ResCustVisa2;
        public List<ResCustVisa2Record> ResCustVisa2
        {
            get { return _ResCustVisa2; }
            set { _ResCustVisa2 = value; }
        }

        List<ResServiceRecord> _ResService;
        public List<ResServiceRecord> ResService
        {
            get { return _ResService; }
            set { _ResService = value; }
        }

        List<ResConRecord> _ResCon;
        public List<ResConRecord> ResCon
        {
            get { return _ResCon; }
            set { _ResCon = value; }
        }

        List<ResServiceExtRecord> _ResServiceExt;
        public List<ResServiceExtRecord> ResServiceExt
        {
            get { return _ResServiceExt; }
            set { _ResServiceExt = value; }
        }

        List<ResConExtRecord> _ResConExt;
        public List<ResConExtRecord> ResConExt
        {
            get { return _ResConExt; }
            set { _ResConExt = value; }
        }

        List<ResCustPriceRecord> _ResCustPrice;
        public List<ResCustPriceRecord> ResCustPrice
        {
            get { return _ResCustPrice; }
            set { _ResCustPrice = value; }
        }

        List<ResSupDisRecord> _ResSupDis;
        public List<ResSupDisRecord> ResSupDis
        {
            get { return _ResSupDis; }
            set { _ResSupDis = value; }
        }

        List<ResPromoRecord> _ResPromo;
        public List<ResPromoRecord> ResPromo
        {
            get { return _ResPromo; }
            set { _ResPromo = value; }
        }

        List<TitleRecord> _Title;
        public List<TitleRecord> Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        List<TitleAgeRecord> _TitleCust;
        public List<TitleAgeRecord> TitleCust
        {
            get { return _TitleCust; }
            set { _TitleCust = value; }
        }

        List<SearchResult> _SelectBook;
        public List<SearchResult> SelectBook
        {
            get { return _SelectBook; }
            set { _SelectBook = value; }
        }

        List<SearchResultOH> _SelectBookOH;
        public List<SearchResultOH> SelectBookOH
        {
            get { return _SelectBookOH; }
            set { _SelectBookOH = value; }
        }

        List<PackPriceSearchResult> _SelectBookPP;
        public List<PackPriceSearchResult> SelectBookPP
        {
            get { return _SelectBookPP; }
            set { _SelectBookPP = value; }
        }

        List<ResFlightDetailRecord> _ResFlightDetail;
        public List<ResFlightDetailRecord> ResFlightDetail
        {
            get { return _ResFlightDetail; }
            set { _ResFlightDetail = value; }
        }

        List<PromoListRecord> _PromoList;
        public List<PromoListRecord> PromoList
        {
            get { return _PromoList; }
            set { _PromoList = value; }
        }

        List<ResChgFeeRecord> _ResChgFee;
        public List<ResChgFeeRecord> ResChgFee
        {
            get { return _ResChgFee; }
            set { _ResChgFee = value; }
        }

        List<ResHotelRecord> _ResHotelList;
        public List<ResHotelRecord> ResHotelList
        {
            get { return _ResHotelList; }
            set { _ResHotelList = value; }
        }

        ResDataRecord _ExtrasData;
        public ResDataRecord ExtrasData
        {
            get { return _ExtrasData; }
            set { _ExtrasData = value; }
        }

        List<ResCalcErr> _ResCalcErrList;
        public List<ResCalcErr> ResCalcErrList
        {
            get { return _ResCalcErrList; }
            set { _ResCalcErrList = value; }
        }

        Guid? _LogID;
        public Guid? LogID
        {
            get { return _LogID; }
            set { _LogID = value; }
        }

        private bool _onlyTransfer;
        public bool OnlyTransfer
        {
            get { return _onlyTransfer; }
            set { _onlyTransfer = value; }
        }
        public FirstResData FirstData { get; set; }
    }

    [Serializable]
    public class ResCalcErr
    {
        public ResCalcErr()
        {
        }

        int? _SerID;
        public int? SerID
        {
            get { return _SerID; }
            set { _SerID = value; }
        }

        int? _ExtSerID;
        public int? ExtSerID
        {
            get { return _ExtSerID; }
            set { _ExtSerID = value; }
        }

        int? _ErrCode;
        public int? ErrCode
        {
            get { return _ErrCode; }
            set { _ErrCode = value; }
        }
    }

    [Serializable]
    public class ResHotelRecord
    {
        public ResHotelRecord()
        {
            _AllotChecked = false;
        }

        int? _RefNo;
        public int? RefNo
        {
            get { return _RefNo; }
            set { _RefNo = value; }
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Accom;
        public string Accom
        {
            get { return _Accom; }
            set { _Accom = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        Int16? _StatConf;
        public Int16? StatConf
        {
            get { return _StatConf; }
            set { _StatConf = value; }
        }

        Int16? _StatSer;
        public Int16? StatSer
        {
            get { return _StatSer; }
            set { _StatSer = value; }
        }

        string _OverRelease;
        public string OverRelease
        {
            get { return _OverRelease; }
            set { _OverRelease = value; }
        }

        string _OverAllot;
        public string OverAllot
        {
            get { return _OverAllot; }
            set { _OverAllot = value; }
        }

        string _StopSale;
        public string StopSale
        {
            get { return _StopSale; }
            set { _StopSale = value; }
        }

        string _Allot;
        public string Allot
        {
            get { return _Allot; }
            set { _Allot = value; }
        }

        bool _AllotChecked;
        public bool AllotChecked
        {
            get { return _AllotChecked; }
            set { _AllotChecked = value; }
        }

    }

    [Serializable]
    public class ResMainRecord
    {
        public ResMainRecord(DataRow row)
        {
            _InvoiceTo = 0;
            _RecStatus = RecordStatus.New;
            if (row != null) {
                _ResNo = Conversion.getStrOrNull(row["ResNo"]);
                _Operator = Conversion.getStrOrNull(row["Operator"]);
                _Market = Conversion.getStrOrNull(row["Market"]);
                _Office = Conversion.getStrOrNull(row["Office"]);
                _Agency = Conversion.getStrOrNull(row["Agency"]);
                _HolPack = Conversion.getStrOrNull(row["HolPack"]);
                _BegDate = Conversion.getDateTimeOrNull(row["BegDate"]);
                _EndDate = Conversion.getDateTimeOrNull(row["EndDate"]);
                _Days = Conversion.getInt16OrNull(row["Days"]);
                _PriceSource = Conversion.getInt16OrNull(row["PriceSource"]);
                _PriceListNo = Conversion.getInt32OrNull(row["PriceListNo"]);
                _SaleResource = Conversion.getInt16OrNull(row["SaleResource"]);
                _ResLock = Conversion.getInt16OrNull(row["ResLock"]);
                _ResStat = Conversion.getInt16OrNull(row["ResStat"]);
                _ConfStat = Conversion.getInt16OrNull(row["ConfStat"]);
                _OptDate = Conversion.getDateTimeOrNull(row["OptDate"]);
                _AgencyUser = Conversion.getStrOrNull(row["AgencyUser"]);
                _ResDate = Conversion.getDateTimeOrNull(row["ResDate"]);
                _SalePrice = Conversion.getDecimalOrNull(row["SalePrice"]);
                _SaleCur = Conversion.getStrOrNull(row["SaleCur"]);
                _ResNote = Conversion.getStrOrNull(row["ResNote"]);
                _IntNote = Conversion.getStrOrNull(row["IntNote"]);
                _Adult = Conversion.getInt16OrNull(row["Adult"]);
                _Child = Conversion.getInt16OrNull(row["Child"]);
                _DepCity = Conversion.getInt32OrNull(row["DepCity"]);
                _ArrCity = Conversion.getInt32OrNull(row["ArrCity"]);
                _PLOperator = Conversion.getStrOrNull(row["PLOperator"]);
                _PLMarket = Conversion.getStrOrNull(row["PLMarket"]);
                _Complete = Conversion.getByteOrNull(row["Complete"]);
                _AceCustomerCode = Conversion.getStrOrNull(row["AceCustomerCode"]);
                _AceTfNumber = Conversion.getStrOrNull(row["AceTfNumber"]);
                _PackType = Conversion.getStrOrNull(row["PackType"]);
                _MerlinxID = Conversion.getStrOrNull(row["MerlinxID"]);
                _MemTable = (bool)row["MemTable"];
                _RecStatus = RecordStatus.Old;
            }
        }

        int _RefNo;
        public int RefNo
        {
            get { return _RefNo; }
            set { _RefNo = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        int? _GroupNo;
        public int? GroupNo
        {
            get { return _GroupNo; }
            set { _GroupNo = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _OperatorName;
        public string OperatorName
        {
            get { return _OperatorName; }
            set { _OperatorName = value; }
        }

        string _OperatorNameL;
        public string OperatorNameL
        {
            get { return _OperatorNameL; }
            set { _OperatorNameL = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _MarketName;
        public string MarketName
        {
            get { return _MarketName; }
            set { _MarketName = value; }
        }

        string _MarketNameL;
        public string MarketNameL
        {
            get { return _MarketNameL; }
            set { _MarketNameL = value; }
        }

        string _Office;
        public string Office
        {
            get { return _Office; }
            set { _Office = value; }
        }

        string _OfficeName;
        public string OfficeName
        {
            get { return _OfficeName; }
            set { _OfficeName = value; }
        }

        string _OfficeNameL;
        public string OfficeNameL
        {
            get { return _OfficeNameL; }
            set { _OfficeNameL = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        string _AgencyName;
        public string AgencyName
        {
            get { return _AgencyName; }
            set { _AgencyName = value; }
        }

        string _AgencyNameL;
        public string AgencyNameL
        {
            get { return _AgencyNameL; }
            set { _AgencyNameL = value; }
        }

        string _HolPack;
        public string HolPack
        {
            get { return _HolPack; }
            set { _HolPack = value; }
        }

        string _HolPackName;
        public string HolPackName
        {
            get { return _HolPackName; }
            set { _HolPackName = value; }
        }

        string _HolPackNameL;
        public string HolPackNameL
        {
            get { return _HolPackNameL; }
            set { _HolPackNameL = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _Days;
        public Int16? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }

        Int16? _PriceSource;
        public Int16? PriceSource
        {
            get { return _PriceSource; }
            set { _PriceSource = value; }
        }

        int? _PriceListNo;
        public int? PriceListNo
        {
            get { return _PriceListNo; }
            set { _PriceListNo = value; }
        }

        Int16? _SaleResource;
        public Int16? SaleResource
        {
            get { return _SaleResource; }
            set { _SaleResource = value; }
        }

        byte? _SaleType;
        public byte? SaleType
        {
            get { return _SaleType; }
            set { _SaleType = value; }
        }

        Int16? _ResType;
        public Int16? ResType
        {
            get { return _ResType; }
            set { _ResType = value; }
        }

        Int16? _ResLock;
        public Int16? ResLock
        {
            get { return _ResLock; }
            set { _ResLock = value; }
        }

        Int16? _ResStat;
        public Int16? ResStat
        {
            get { return _ResStat; }
            set { _ResStat = value; }
        }

        DateTime? _ResStatDate;
        public DateTime? ResStatDate
        {
            get { return _ResStatDate; }
            set { _ResStatDate = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        DateTime? _ConfStatDate;
        public DateTime? ConfStatDate
        {
            get { return _ConfStatDate; }
            set { _ConfStatDate = value; }
        }

        int? _CanReason;
        public int? CanReason
        {
            get { return _CanReason; }
            set { _CanReason = value; }
        }

        DateTime? _OptDate;
        public DateTime? OptDate
        {
            get { return _OptDate; }
            set { _OptDate = value; }
        }

        string _AgencyUser;
        public string AgencyUser
        {
            get { return _AgencyUser; }
            set { _AgencyUser = value; }
        }

        string _AgencyUserName;
        public string AgencyUserName
        {
            get { return _AgencyUserName; }
            set { _AgencyUserName = value; }
        }

        string _AgencyUserNameL;
        public string AgencyUserNameL
        {
            get { return _AgencyUserNameL; }
            set { _AgencyUserNameL = value; }
        }

        string _AuthorUser;
        public string AuthorUser
        {
            get { return _AuthorUser; }
            set { _AuthorUser = value; }
        }

        DateTime? _ResDate;
        public DateTime? ResDate
        {
            get { return _ResDate; }
            set { _ResDate = value; }
        }

        DateTime? _ChgDate;
        public DateTime? ChgDate
        {
            get { return _ChgDate; }
            set { _ChgDate = value; }
        }

        string _ChgUser;
        public string ChgUser
        {
            get { return _ChgUser; }
            set { _ChgUser = value; }
        }

        decimal? _NetPrice;
        public decimal? NetPrice
        {
            get { return _NetPrice; }
            set { _NetPrice = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _AgencyCom;
        public decimal? AgencyCom
        {
            get { return _AgencyCom; }
            set { _AgencyCom = value; }
        }

        decimal? _AgencyComPer;
        public decimal? AgencyComPer
        {
            get { return _AgencyComPer; }
            set { _AgencyComPer = value; }
        }

        decimal? _AgencyComSupPer;
        public decimal? AgencyComSupPer
        {
            get { return _AgencyComSupPer; }
            set { _AgencyComSupPer = value; }
        }

        string _Ballon;
        public string Ballon
        {
            get { return _Ballon; }
            set { _Ballon = value; }
        }

        DateTime? _ReceiveDate;
        public DateTime? ReceiveDate
        {
            get { return _ReceiveDate; }
            set { _ReceiveDate = value; }
        }

        DateTime? _RecCrtDate;
        public DateTime? RecCrtDate
        {
            get { return _RecCrtDate; }
            set { _RecCrtDate = value; }
        }

        string _RecCrtUser;
        public string RecCrtUser
        {
            get { return _RecCrtUser; }
            set { _RecCrtUser = value; }
        }

        DateTime? _RecChgDate;
        public DateTime? RecChgDate
        {
            get { return _RecChgDate; }
            set { _RecChgDate = value; }
        }

        string _RecChgUser;
        public string RecChgUser
        {
            get { return _RecChgUser; }
            set { _RecChgUser = value; }
        }

        string _ResNote;
        public string ResNote
        {
            get { return _ResNote; }
            set { _ResNote = value; }
        }

        string _IntNote;
        public string IntNote
        {
            get { return _IntNote; }
            set { _IntNote = value; }
        }

        string _Code1;
        public string Code1
        {
            get { return _Code1; }
            set { _Code1 = value; }
        }

        string _Code2;
        public string Code2
        {
            get { return _Code2; }
            set { _Code2 = value; }
        }

        string _Code3;
        public string Code3
        {
            get { return _Code3; }
            set { _Code3 = value; }
        }

        string _Code4;
        public string Code4
        {
            get { return _Code4; }
            set { _Code4 = value; }
        }

        Int16? _Adult;
        public Int16? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16? _Child;
        public Int16? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        string _ConfToAgency;
        public string ConfToAgency
        {
            get { return _ConfToAgency; }
            set { _ConfToAgency = value; }
        }

        DateTime? _ConfToAgencyDate;
        public DateTime? ConfToAgencyDate
        {
            get { return _ConfToAgencyDate; }
            set { _ConfToAgencyDate = value; }
        }

        decimal? _AgencyPayable;
        public decimal? AgencyPayable
        {
            get { return _AgencyPayable; }
            set { _AgencyPayable = value; }
        }

        DateTime? _PayLastDate;
        public DateTime? PayLastDate
        {
            get { return _PayLastDate; }
            set { _PayLastDate = value; }
        }

        decimal? _AgencyPayment;
        public decimal? AgencyPayment
        {
            get { return _AgencyPayment; }
            set { _AgencyPayment = value; }
        }

        string _PaymentStat;
        public string PaymentStat
        {
            get { return _PaymentStat; }
            set { _PaymentStat = value; }
        }

        decimal? _Discount;
        public decimal? Discount
        {
            get { return _Discount; }
            set { _Discount = value; }
        }

        string _AgencyComMan;
        public string AgencyComMan
        {
            get { return _AgencyComMan; }
            set { _AgencyComMan = value; }
        }

        string _SendInComing;
        public string SendInComing
        {
            get { return _SendInComing; }
            set { _SendInComing = value; }
        }

        DateTime? _SendInComingDate;
        public DateTime? SendInComingDate
        {
            get { return _SendInComingDate; }
            set { _SendInComingDate = value; }
        }

        string _AllDocPrint;
        public string AllDocPrint
        {
            get { return _AllDocPrint; }
            set { _AllDocPrint = value; }
        }

        string _SendAgency;
        public string SendAgency
        {
            get { return _SendAgency; }
            set { _SendAgency = value; }
        }

        DateTime? _SendAgencyDate;
        public DateTime? SendAgencyDate
        {
            get { return _SendAgencyDate; }
            set { _SendAgencyDate = value; }
        }

        string _EB;
        public string EB
        {
            get { return _EB; }
            set { _EB = value; }
        }

        int? _EBSupID;
        public int? EBSupID
        {
            get { return _EBSupID; }
            set { _EBSupID = value; }
        }

        decimal? _EBSupMaxPer;
        public decimal? EBSupMaxPer
        {
            get { return _EBSupMaxPer; }
            set { _EBSupMaxPer = value; }
        }

        int? _EBPasID;
        public int? EBPasID
        {
            get { return _EBPasID; }
            set { _EBPasID = value; }
        }

        decimal? _EBAgencyPer;
        public decimal? EBAgencyPer
        {
            get { return _EBAgencyPer; }
            set { _EBAgencyPer = value; }
        }

        decimal? _EBPasPer;
        public decimal? EBPasPer
        {
            get { return _EBPasPer; }
            set { _EBPasPer = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityNameL;
        public string DepCityNameL
        {
            get { return _DepCityNameL; }
            set { _DepCityNameL = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityNameL;
        public string ArrCityNameL
        {
            get { return _ArrCityNameL; }
            set { _ArrCityNameL = value; }
        }

        string _Credit;
        public string Credit
        {
            get { return _Credit; }
            set { _Credit = value; }
        }

        decimal? _CreditExpense;
        public decimal? CreditExpense
        {
            get { return _CreditExpense; }
            set { _CreditExpense = value; }
        }

        string _CreditExpenseCur;
        public string CreditExpenseCur
        {
            get { return _CreditExpenseCur; }
            set { _CreditExpenseCur = value; }
        }

        string _Broker;
        public string Broker
        {
            get { return _Broker; }
            set { _Broker = value; }
        }

        decimal? _BrokerCom;
        public decimal? BrokerCom
        {
            get { return _BrokerCom; }
            set { _BrokerCom = value; }
        }

        decimal? _BrokerComPer;
        public decimal? BrokerComPer
        {
            get { return _BrokerComPer; }
            set { _BrokerComPer = value; }
        }

        string _PLOperator;
        public string PLOperator
        {
            get { return _PLOperator; }
            set { _PLOperator = value; }
        }

        string _PLOperatorName;
        public string PLOperatorName
        {
            get { return _PLOperatorName; }
            set { _PLOperatorName = value; }
        }

        string _PLOperatorNameL;
        public string PLOperatorNameL
        {
            get { return _PLOperatorNameL; }
            set { _PLOperatorNameL = value; }
        }

        string _PLMarket;
        public string PLMarket
        {
            get { return _PLMarket; }
            set { _PLMarket = value; }
        }

        string _PLMarketName;
        public string PLMarketName
        {
            get { return _PLMarketName; }
            set { _PLMarketName = value; }
        }

        string _PLMarketNameL;
        public string PLMarketNameL
        {
            get { return _PLMarketNameL; }
            set { _PLMarketNameL = value; }
        }

        decimal? _AgencyBonus;
        public decimal? AgencyBonus
        {
            get { return _AgencyBonus; }
            set { _AgencyBonus = value; }
        }

        decimal? _AgencyBonusAmount;
        public decimal? AgencyBonusAmount
        {
            get { return _AgencyBonusAmount; }
            set { _AgencyBonusAmount = value; }
        }

        decimal? _UserBonus;
        public decimal? UserBonus
        {
            get { return _UserBonus; }
            set { _UserBonus = value; }
        }

        decimal? _UserBonusAmount;
        public decimal? UserBonusAmount
        {
            get { return _UserBonusAmount; }
            set { _UserBonusAmount = value; }
        }

        decimal? _PasBonus;
        public decimal? PasBonus
        {
            get { return _PasBonus; }
            set { _PasBonus = value; }
        }

        decimal? _PasBonusAmount;
        public decimal? PasBonusAmount
        {
            get { return _PasBonusAmount; }
            set { _PasBonusAmount = value; }
        }

        byte? _Complete;
        public byte? Complete
        {
            get { return _Complete; }
            set { _Complete = value; }
        }

        byte? _ReadByOpr;
        public byte? ReadByOpr
        {
            get { return _ReadByOpr; }
            set { _ReadByOpr = value; }
        }

        string _ReadByOprUser;
        public string ReadByOprUser
        {
            get { return _ReadByOprUser; }
            set { _ReadByOprUser = value; }
        }

        DateTime? _ReadByOprDate;
        public DateTime? ReadByOprDate
        {
            get { return _ReadByOprDate; }
            set { _ReadByOprDate = value; }
        }

        byte? _ReadByAgency;
        public byte? ReadByAgency
        {
            get { return _ReadByAgency; }
            set { _ReadByAgency = value; }
        }

        string _ReadByAgencyUser;
        public string ReadByAgencyUser
        {
            get { return _ReadByAgencyUser; }
            set { _ReadByAgencyUser = value; }
        }

        DateTime? _ReadByAgencyDate;
        public DateTime? ReadByAgencyDate
        {
            get { return _ReadByAgencyDate; }
            set { _ReadByAgencyDate = value; }
        }

        string _InvoiceIssued;
        public string InvoiceIssued
        {
            get { return _InvoiceIssued; }
            set { _InvoiceIssued = value; }
        }

        DateTime? _InvoiceDate;
        public DateTime? InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }

        decimal? _Balance;
        public decimal? Balance
        {
            get { return _Balance; }
            set { _Balance = value; }
        }

        decimal? _InvoicedAmount;
        public decimal? InvoicedAmount
        {
            get { return _InvoicedAmount; }
            set { _InvoicedAmount = value; }
        }

        decimal? _PasSupDis;
        public decimal? PasSupDis
        {
            get { return _PasSupDis; }
            set { _PasSupDis = value; }
        }

        decimal? _AgencySupDis;
        public decimal? AgencySupDis
        {
            get { return _AgencySupDis; }
            set { _AgencySupDis = value; }
        }

        decimal? _PasPayable;
        public decimal? PasPayable
        {
            get { return _PasPayable; }
            set { _PasPayable = value; }
        }

        decimal? _PasPayment;
        public decimal? PasPayment
        {
            get { return _PasPayment; }
            set { _PasPayment = value; }
        }

        decimal? _PasBalance;
        public decimal? PasBalance
        {
            get { return _PasBalance; }
            set { _PasBalance = value; }
        }

        decimal? _AgencyComSup;
        public decimal? AgencyComSup
        {
            get { return _AgencyComSup; }
            set { _AgencyComSup = value; }
        }

        decimal? _EBAgency;
        public decimal? EBAgency
        {
            get { return _EBAgency; }
            set { _EBAgency = value; }
        }

        decimal? _EBPas;
        public decimal? EBPas
        {
            get { return _EBPas; }
            set { _EBPas = value; }
        }

        decimal? _PasAmount;
        public decimal? PasAmount
        {
            get { return _PasAmount; }
            set { _PasAmount = value; }
        }

        decimal? _Tax;
        public decimal? Tax
        {
            get { return _Tax; }
            set { _Tax = value; }
        }

        decimal? _AgencyComInvAmount;
        public decimal? AgencyComInvAmount
        {
            get { return _AgencyComInvAmount; }
            set { _AgencyComInvAmount = value; }
        }

        string _SendToAcc;
        public string SendToAcc
        {
            get { return _SendToAcc; }
            set { _SendToAcc = value; }
        }

        DateTime? _SendToAccDate;
        public DateTime? SendToAccDate
        {
            get { return _SendToAccDate; }
            set { _SendToAccDate = value; }
        }

        string _SendToAccUser;
        public string SendToAccUser
        {
            get { return _SendToAccUser; }
            set { _SendToAccUser = value; }
        }

        string _AllowDocPrint;
        public string AllowDocPrint
        {
            get { return _AllowDocPrint; }
            set { _AllowDocPrint = value; }
        }

        string _CallFromResSer;
        public string CallFromResSer
        {
            get { return _CallFromResSer; }
            set { _CallFromResSer = value; }
        }

        decimal? _AgencyDisPasPer;
        public decimal? AgencyDisPasPer
        {
            get { return _AgencyDisPasPer; }
            set { _AgencyDisPasPer = value; }
        }

        decimal? _AgencyDisPasVal;
        public decimal? AgencyDisPasVal
        {
            get { return _AgencyDisPasVal; }
            set { _AgencyDisPasVal = value; }
        }

        string _LastUser;
        public string LastUser
        {
            get { return _LastUser; }
            set { _LastUser = value; }
        }

        string _AceCustomerCode;
        public string AceCustomerCode
        {
            get { return _AceCustomerCode; }
            set { _AceCustomerCode = value; }
        }

        string _AceTfNumber;
        public string AceTfNumber
        {
            get { return _AceTfNumber; }
            set { _AceTfNumber = value; }
        }

        string _SendToAccPL;
        public string SendToAccPL
        {
            get { return _SendToAccPL; }
            set { _SendToAccPL = value; }
        }

        byte? _EBPasChk;
        public byte? EBPasChk
        {
            get { return _EBPasChk; }
            set { _EBPasChk = value; }
        }

        byte? _EBAgencyChk;
        public byte? EBAgencyChk
        {
            get { return _EBAgencyChk; }
            set { _EBAgencyChk = value; }
        }

        string _WebIP;
        public string WebIP
        {
            get { return _WebIP; }
            set { _WebIP = value; }
        }

        int? _HowFindUs;
        public int? HowFindUs
        {
            get { return _HowFindUs; }
            set { _HowFindUs = value; }
        }

        DateTime? _EBPasDueDate;
        public DateTime? EBPasDueDate
        {
            get { return _EBPasDueDate; }
            set { _EBPasDueDate = value; }
        }

        decimal? _EBPasDuePer;
        public decimal? EBPasDuePer
        {
            get { return _EBPasDuePer; }
            set { _EBPasDuePer = value; }
        }

        DateTime? _EBPasDueDate2;
        public DateTime? EBPasDueDate2
        {
            get { return _EBPasDueDate2; }
            set { _EBPasDueDate2 = value; }
        }

        decimal? _EBPasDuePer2;
        public decimal? EBPasDuePer2
        {
            get { return _EBPasDuePer2; }
            set { _EBPasDuePer2 = value; }
        }

        byte? _PayPlanMode;
        public byte? PayPlanMode
        {
            get { return _PayPlanMode; }
            set { _PayPlanMode = value; }
        }

        decimal? _PasDiscBase;
        public decimal? PasDiscBase
        {
            get { return _PasDiscBase; }
            set { _PasDiscBase = value; }
        }

        decimal? _AgencyDiscBase;
        public decimal? AgencyDiscBase
        {
            get { return _AgencyDiscBase; }
            set { _AgencyDiscBase = value; }
        }

        byte? _InvoiceTo;
        public byte? InvoiceTo
        {
            get { return _InvoiceTo; }
            set { _InvoiceTo = value; }
        }

        decimal? _AgencyComPayAmount;
        public decimal? AgencyComPayAmount
        {
            get { return _AgencyComPayAmount; }
            set { _AgencyComPayAmount = value; }
        }

        byte? _AgencyEarnBonus;
        public byte? AgencyEarnBonus
        {
            get { return _AgencyEarnBonus; }
            set { _AgencyEarnBonus = value; }
        }

        byte? _UserEarnBonus;
        public byte? UserEarnBonus
        {
            get { return _UserEarnBonus; }
            set { _UserEarnBonus = value; }
        }

        byte? _PasEarnBonus;
        public byte? PasEarnBonus
        {
            get { return _PasEarnBonus; }
            set { _PasEarnBonus = value; }
        }

        bool? _AllowParent;
        public bool? AllowParent
        {
            get { return _AllowParent; }
            set { _AllowParent = value; }
        }

        string _PackType;
        public string PackType
        {
            get { return _PackType; }
            set { _PackType = value; }
        }

        Int16? _Pax;
        public Int16? Pax
        {
            get { return _Pax; }
            set { _Pax = value; }
        }

        byte? _PasPayRule;
        public byte? PasPayRule
        {
            get { return _PasPayRule; }
            set { _PasPayRule = value; }
        }

        bool? _HasDrReport;
        public bool? HasDrReport
        {
            get { return _HasDrReport; }
            set { _HasDrReport = value; }
        }

        decimal? _OldResPrice;
        public decimal? OldResPrice
        {
            get { return _OldResPrice; }
            set { _OldResPrice = value; }
        }

        bool? _HoneyMoonRes;
        public bool? HoneyMoonRes
        {
            get { return _HoneyMoonRes; }
            set { _HoneyMoonRes = value; }
        }

        bool? _SendToAce;
        public bool? SendToAce
        {
            get { return _SendToAce; }
            set { _SendToAce = value; }
        }

        DateTime? _SendToAceDate;
        public DateTime? SendToAceDate
        {
            get { return _SendToAceDate; }
            set { _SendToAceDate = value; }
        }

        string _MerlinxID;
        public string MerlinxID
        {
            get { return _MerlinxID; }
            set { _MerlinxID = value; }
        }

        string _SendTo3Pgm;
        public string SendTo3Pgm
        {
            get { return _SendTo3Pgm; }
            set { _SendTo3Pgm = value; }
        }

        DateTime? _SendTo3PgmDate;
        public DateTime? SendTo3PgmDate
        {
            get { return _SendTo3PgmDate; }
            set { _SendTo3PgmDate = value; }
        }

        DateTime? _ResTime;
        public DateTime? ResTime
        {
            get { return _ResTime; }
            set { _ResTime = value; }
        }

        string _PromoCode;
        public string PromoCode
        {
            get { return _PromoCode; }
            set { _PromoCode = value; }
        }

        decimal? _PromoAmount;
        public decimal? PromoAmount
        {
            get { return _PromoAmount; }
            set { _PromoAmount = value; }
        }

        bool? _ContractIssued;
        public bool? ContractIssued
        {
            get { return _ContractIssued; }
            set { _ContractIssued = value; }
        }

        DateTime? _ContractDate;
        public DateTime? ContractDate
        {
            get { return _ContractDate; }
            set { _ContractDate = value; }
        }

        Int16? _CalcErrNo;
        public Int16? CalcErrNo
        {
            get { return _CalcErrNo; }
            set { _CalcErrNo = value; }
        }

        bool? _SaleComp;
        public bool? SaleComp
        {
            get { return _SaleComp; }
            set { _SaleComp = value; }
        }

        decimal? _PLSpoVal;
        public decimal? PLSpoVal
        {
            get { return _PLSpoVal; }
            set { _PLSpoVal = value; }
        }

        byte? _PaymentFrom;
        public byte? PaymentFrom
        {
            get { return _PaymentFrom; }
            set { _PaymentFrom = value; }
        }

        byte? _PLSpoChk;
        public byte? PLSpoChk
        {
            get { return _PLSpoChk; }
            set { _PLSpoChk = value; }
        }

        decimal? _PackedPrice;
        public decimal? PackedPrice
        {
            get { return _PackedPrice; }
            set { _PackedPrice = value; }
        }

        decimal? _OutOfPackedPrice;
        public decimal? OutOfPackedPrice
        {
            get { return _OutOfPackedPrice; }
            set { _OutOfPackedPrice = value; }
        }

        string _BonusID2;
        public string BonusID2
        {
            get { return _BonusID2; }
            set { _BonusID2 = value; }
        }

        DateTime? _PBegDate;
        public DateTime? PBegDate
        {
            get { return _PBegDate; }
            set { _PBegDate = value; }
        }

        Int16? _PNight;
        public Int16? PNight
        {
            get { return _PNight; }
            set { _PNight = value; }
        }

        int? _PxmResNo;
        public int? PxmResNo
        {
            get { return _PxmResNo; }
            set { _PxmResNo = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable]
    public class ResCustRecord
    {
        public ResCustRecord()
        {
            _Status = 0;
            _MemTable = false;
            _RecStatus = RecordStatus.New;
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        Int32? _CustNoT;
        public Int32? CustNoT
        {
            get { return _CustNoT; }
            set { _CustNoT = value; }
        }

        Int32 _CustNo;
        public Int32 CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _Title;
        public Int16? Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        string _TitleStr;
        public string TitleStr
        {
            get { return _TitleStr; }
            set { _TitleStr = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _SurnameL;
        public string SurnameL
        {
            get { return _SurnameL; }
            set { _SurnameL = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        DateTime? _Birtday;
        public DateTime? Birtday
        {
            get { return _Birtday; }
            set { _Birtday = value; }
        }

        Int16? _Age;
        public Int16? Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        string _PassSerie;
        public string PassSerie
        {
            get { return _PassSerie; }
            set { _PassSerie = value; }
        }

        string _PassNo;
        public string PassNo
        {
            get { return _PassNo; }
            set { _PassNo = value; }
        }

        DateTime? _PassIssueDate;
        public DateTime? PassIssueDate
        {
            get { return _PassIssueDate; }
            set { _PassIssueDate = value; }
        }

        DateTime? _PassExpDate;
        public DateTime? PassExpDate
        {
            get { return _PassExpDate; }
            set { _PassExpDate = value; }
        }

        string _PassGiven;
        public string PassGiven
        {
            get { return _PassGiven; }
            set { _PassGiven = value; }
        }

        string _IDNo;
        public string IDNo
        {
            get { return _IDNo; }
            set { _IDNo = value; }
        }

        int? _Nation;
        public int? Nation
        {
            get { return _Nation; }
            set { _Nation = value; }
        }

        string _NationName;
        public string NationName
        {
            get { return _NationName; }
            set { _NationName = value; }
        }

        string _NationNameL;
        public string NationNameL
        {
            get { return _NationNameL; }
            set { _NationNameL = value; }
        }

        string _Leader;
        public string Leader
        {
            get { return _Leader; }
            set { _Leader = value; }
        }

        byte? _DocVoucher;
        public byte? DocVoucher
        {
            get { return _DocVoucher; }
            set { _DocVoucher = value; }
        }

        byte? _DocTicket;
        public byte? DocTicket
        {
            get { return _DocTicket; }
            set { _DocTicket = value; }
        }

        byte? _DocInsur;
        public byte? DocInsur
        {
            get { return _DocInsur; }
            set { _DocInsur = value; }
        }

        decimal? _Bonus;
        public decimal? Bonus
        {
            get { return _Bonus; }
            set { _Bonus = value; }
        }

        decimal? _BonusAmount;
        public decimal? BonusAmount
        {
            get { return _BonusAmount; }
            set { _BonusAmount = value; }
        }

        bool? _HasPassport;
        public bool? HasPassport
        {
            get { return _HasPassport; }
            set { _HasPassport = value; }
        }

        byte? _Status;
        public byte? Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        decimal? _ppSalePrice;
        public decimal? ppSalePrice
        {
            get { return _ppSalePrice; }
            set { _ppSalePrice = value; }
        }

        decimal? _ppPromoAmount;
        public decimal? ppPromoAmount
        {
            get { return _ppPromoAmount; }
            set { _ppPromoAmount = value; }
        }

        decimal? _ppSupDisAmount;
        public decimal? ppSupDisAmount
        {
            get { return _ppSupDisAmount; }
            set { _ppSupDisAmount = value; }
        }

        decimal? _ppPasPayable;
        public decimal? ppPasPayable
        {
            get { return _ppPasPayable; }
            set { _ppPasPayable = value; }
        }

        decimal? _ppBaseCanAmount;
        public decimal? ppBaseCanAmount
        {
            get { return _ppBaseCanAmount; }
            set { _ppBaseCanAmount = value; }
        }

        decimal? _ppPasEB;
        public decimal? PpPasEB
        {
            get { return _ppPasEB; }
            set { _ppPasEB = value; }
        }

        decimal? _ppPLSpoVal;
        public decimal? PpPLSpoVal
        {
            get { return _ppPLSpoVal; }
            set { _ppPLSpoVal = value; }
        }

        string _Nationality;
        public string Nationality
        {
            get { return _Nationality; }
            set { _Nationality = value; }
        }

        string _NationalityName;
        public string NationalityName
        {
            get { return _NationalityName; }
            set { _NationalityName = value; }
        }

        int? _BookID;
        public int? BookID
        {
            get { return _BookID; }
            set { _BookID = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        int? _ParentID;
        public int? ParentID
        {
            get { return _ParentID; }
            set { _ParentID = value; }
        }

        string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable]
    public class ResCustInfoRecord
    {
        public ResCustInfoRecord()
        {
            _InvoiceAddr = "H";
            _ContactAddr = "H";
            _RecStatus = RecordStatus.New;
            _MemTable = false;
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _CTitle;
        public Int16? CTitle
        {
            get { return _CTitle; }
            set { _CTitle = value; }
        }

        string _CTitleName;
        public string CTitleName
        {
            get { return _CTitleName; }
            set { _CTitleName = value; }
        }

        string _CName;
        public string CName
        {
            get { return _CName; }
            set { _CName = value; }
        }

        string _CSurName;
        public string CSurName
        {
            get { return _CSurName; }
            set { _CSurName = value; }
        }

        string _AddrHome;
        public string AddrHome
        {
            get { return _AddrHome; }
            set { _AddrHome = value; }
        }

        string _AddrHomeCity;
        public string AddrHomeCity
        {
            get { return _AddrHomeCity; }
            set { _AddrHomeCity = value; }
        }

        string _AddrHomeZip;
        public string AddrHomeZip
        {
            get { return _AddrHomeZip; }
            set { _AddrHomeZip = value; }
        }

        string _AddrHomeTel;
        public string AddrHomeTel
        {
            get { return _AddrHomeTel; }
            set { _AddrHomeTel = value; }
        }

        string _AddrHomeFax;
        public string AddrHomeFax
        {
            get { return _AddrHomeFax; }
            set { _AddrHomeFax = value; }
        }

        string _AddrHomeEmail;
        public string AddrHomeEmail
        {
            get { return _AddrHomeEmail; }
            set { _AddrHomeEmail = value; }
        }

        string _AddrHomeCountry;
        public string AddrHomeCountry
        {
            get { return _AddrHomeCountry; }
            set { _AddrHomeCountry = value; }
        }

        string _AddrHomeCountryCode;
        public string AddrHomeCountryCode
        {
            get { return _AddrHomeCountryCode; }
            set { _AddrHomeCountryCode = value; }
        }

        string _HomeTaxOffice;
        public string HomeTaxOffice
        {
            get { return _HomeTaxOffice; }
            set { _HomeTaxOffice = value; }
        }

        string _HomeTaxAccNo;
        public string HomeTaxAccNo
        {
            get { return _HomeTaxAccNo; }
            set { _HomeTaxAccNo = value; }
        }

        string _AddrWork;
        public string AddrWork
        {
            get { return _AddrWork; }
            set { _AddrWork = value; }
        }

        string _AddrWorkCity;
        public string AddrWorkCity
        {
            get { return _AddrWorkCity; }
            set { _AddrWorkCity = value; }
        }

        string _AddrWorkZip;
        public string AddrWorkZip
        {
            get { return _AddrWorkZip; }
            set { _AddrWorkZip = value; }
        }

        string _AddrWorkTel;
        public string AddrWorkTel
        {
            get { return _AddrWorkTel; }
            set { _AddrWorkTel = value; }
        }

        string _AddrWorkFax;
        public string AddrWorkFax
        {
            get { return _AddrWorkFax; }
            set { _AddrWorkFax = value; }
        }

        string _AddrWorkEMail;
        public string AddrWorkEMail
        {
            get { return _AddrWorkEMail; }
            set { _AddrWorkEMail = value; }
        }

        string _AddrWorkCountry;
        public string AddrWorkCountry
        {
            get { return _AddrWorkCountry; }
            set { _AddrWorkCountry = value; }
        }

        string _AddrWorkCountryCode;
        public string AddrWorkCountryCode
        {
            get { return _AddrWorkCountryCode; }
            set { _AddrWorkCountryCode = value; }
        }

        string _WorkTaxOffice;
        public string WorkTaxOffice
        {
            get { return _WorkTaxOffice; }
            set { _WorkTaxOffice = value; }
        }

        string _WorkTaxAccNo;
        public string WorkTaxAccNo
        {
            get { return _WorkTaxAccNo; }
            set { _WorkTaxAccNo = value; }
        }

        string _ContactAddr;
        public string ContactAddr
        {
            get { return _ContactAddr; }
            set { _ContactAddr = value; }
        }

        string _MobTel;
        public string MobTel
        {
            get { return _MobTel; }
            set { _MobTel = value; }
        }

        string _Jobs;
        public string Jobs
        {
            get { return _Jobs; }
            set { _Jobs = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        string _InvoiceAddr;
        public string InvoiceAddr
        {
            get { return _InvoiceAddr; }
            set { _InvoiceAddr = value; }
        }

        string _WorkFirmName;
        public string WorkFirmName
        {
            get { return _WorkFirmName; }
            set { _WorkFirmName = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankAccNo;
        public string BankAccNo
        {
            get { return _BankAccNo; }
            set { _BankAccNo = value; }
        }

        string _BankIBAN;
        public string BankIBAN
        {
            get { return _BankIBAN; }
            set { _BankIBAN = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable]
    public class ResCustVisaRecord
    {
        public ResCustVisaRecord()
        {
            _MemTable = false;
            _RecStatus = RecordStatus.New;
        }

        int? _RecId;
        public int? RecId
        {
            get { return _RecId; }
            set { _RecId = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        string _BirthSurname;
        public string BirthSurname
        {
            get { return _BirthSurname; }
            set { _BirthSurname = value; }
        }

        string _PlaceOfBirth;
        public string PlaceOfBirth
        {
            get { return _PlaceOfBirth; }
            set { _PlaceOfBirth = value; }
        }

        string _CountryOfBirth;
        public string CountryOfBirth
        {
            get { return _CountryOfBirth; }
            set { _CountryOfBirth = value; }
        }

        string _RemarkForChild;
        public string RemarkForChild
        {
            get { return _RemarkForChild; }
            set { _RemarkForChild = value; }
        }

        string _GivenBy;
        public string GivenBy
        {
            get { return _GivenBy; }
            set { _GivenBy = value; }
        }

        string _Job;
        public string Job
        {
            get { return _Job; }
            set { _Job = value; }
        }

        string _Employer;
        public string Employer
        {
            get { return _Employer; }
            set { _Employer = value; }
        }

        string _InvitePerson;
        public string InvitePerson
        {
            get { return _InvitePerson; }
            set { _InvitePerson = value; }
        }

        string _Sex;
        public string Sex
        {
            get { return _Sex; }
            set { _Sex = value; }
        }

        Int16? _Marital;
        public Int16? Marital
        {
            get { return _Marital; }
            set { _Marital = value; }
        }

        string _MaritalRemark;
        public string MaritalRemark
        {
            get { return _MaritalRemark; }
            set { _MaritalRemark = value; }
        }

        byte? _VisaCount;
        public byte? VisaCount
        {
            get { return _VisaCount; }
            set { _VisaCount = value; }
        }

        DateTime? _ValidFrom;
        public DateTime? ValidFrom
        {
            get { return _ValidFrom; }
            set { _ValidFrom = value; }
        }

        DateTime? _ValidTill;
        public DateTime? ValidTill
        {
            get { return _ValidTill; }
            set { _ValidTill = value; }
        }

        string _Address;
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        string _Tel;
        public string Tel
        {
            get { return _Tel; }
            set { _Tel = value; }
        }

        string _Fax;
        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }

        string _VisaNo;
        public string VisaNo
        {
            get { return _VisaNo; }
            set { _VisaNo = value; }
        }

        decimal? _SalePriceForVisa;
        public decimal? SalePriceForVisa
        {
            get { return _SalePriceForVisa; }
            set { _SalePriceForVisa = value; }
        }

        string _DepCity;
        public string DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _ArrCity;
        public string ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _AgencyName;
        public string AgencyName
        {
            get { return _AgencyName; }
            set { _AgencyName = value; }
        }

        string _AgencyAddress;
        public string AgencyAddress
        {
            get { return _AgencyAddress; }
            set { _AgencyAddress = value; }
        }

        string _AgencyTel;
        public string AgencyTel
        {
            get { return _AgencyTel; }
            set { _AgencyTel = value; }
        }

        string _AgencyFax;
        public string AgencyFax
        {
            get { return _AgencyFax; }
            set { _AgencyFax = value; }
        }

        string _EmpAddress;
        public string EmpAddress
        {
            get { return _EmpAddress; }
            set { _EmpAddress = value; }
        }

        string _EmpTel;
        public string EmpTel
        {
            get { return _EmpTel; }
            set { _EmpTel = value; }
        }

        DateTime? _ValidFrom2;
        public DateTime? ValidFrom2
        {
            get { return _ValidFrom2; }
            set { _ValidFrom2 = value; }
        }

        DateTime? _ValidTill2;
        public DateTime? ValidTill2
        {
            get { return _ValidTill2; }
            set { _ValidTill2 = value; }
        }

        DateTime? _ValidFrom3;
        public DateTime? ValidFrom3
        {
            get { return _ValidFrom3; }
            set { _ValidFrom3 = value; }
        }

        DateTime? _ValidTill3;
        public DateTime? ValidTill3
        {
            get { return _ValidTill3; }
            set { _ValidTill3 = value; }
        }

        DateTime? _ValidFrom4;
        public DateTime? ValidFrom4
        {
            get { return _ValidFrom4; }
            set { _ValidFrom4 = value; }
        }

        DateTime? _ValidTill4;
        public DateTime? ValidTill4
        {
            get { return _ValidTill4; }
            set { _ValidTill4 = value; }
        }

        string _VisaFormData;
        public string VisaFormData
        {
            get { return _VisaFormData; }
            set { _VisaFormData = value; }
        }

        int? _CitizenshipOriginal;
        public int? CitizenshipOriginal
        {
            get { return _CitizenshipOriginal; }
            set { _CitizenshipOriginal = value; }
        }

        int? _VisaCountry;
        public int? VisaCountry
        {
            get { return _VisaCountry; }
            set { _VisaCountry = value; }
        }

        int? _VisaCountry1;
        public int? VisaCountry1
        {
            get { return _VisaCountry1; }
            set { _VisaCountry1 = value; }
        }

        int? _VisaCountry2;
        public int? VisaCountry2
        {
            get { return _VisaCountry2; }
            set { _VisaCountry2 = value; }
        }

        int? _VisaCountry3;
        public int? VisaCountry3
        {
            get { return _VisaCountry3; }
            set { _VisaCountry3 = value; }
        }

        int? _CountryOfDestination;
        public int? CountryOfDestination
        {
            get { return _CountryOfDestination; }
            set { _CountryOfDestination = value; }
        }

        int? _CountryOfFirstEntry;
        public int? CountryOfFirstEntry
        {
            get { return _CountryOfFirstEntry; }
            set { _CountryOfFirstEntry = value; }
        }

        string _Mobile;
        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }

        string _OrganizationName;
        public string OrganizationName
        {
            get { return _OrganizationName; }
            set { _OrganizationName = value; }
        }

        string _OrganizationAddress;
        public string OrganizationAddress
        {
            get { return _OrganizationAddress; }
            set { _OrganizationAddress = value; }
        }

        string _OrganizationPhone;
        public string OrganizationPhone
        {
            get { return _OrganizationPhone; }
            set { _OrganizationPhone = value; }
        }

        string _LegalRepresentativeSurname;
        public string LegalRepresentativeSurname
        {
            get { return _LegalRepresentativeSurname; }
            set { _LegalRepresentativeSurname = value; }
        }

        string _LegalRepresentativeAddress;
        public string LegalRepresentativeAddress
        {
            get { return _LegalRepresentativeAddress; }
            set { _LegalRepresentativeAddress = value; }
        }

        string _LegalRepresentativeName;
        public string LegalRepresentativeName
        {
            get { return _LegalRepresentativeName; }
            set { _LegalRepresentativeName = value; }
        }

        string _LegalRepresentativeCitizenship;
        public string LegalRepresentativeCitizenship
        {
            get { return _LegalRepresentativeCitizenship; }
            set { _LegalRepresentativeCitizenship = value; }
        }

        int? _CitizenShip;
        public int? CitizenShip
        {
            get { return _CitizenShip; }
            set { _CitizenShip = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable]
    public class ResCustVisa2Record
    {
        public ResCustVisa2Record()
        {
            _MemTable = false;
            _RecStatus = RecordStatus.New;
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        string _SurnameOfBirth;
        public string SurnameOfBirth
        {
            get { return _SurnameOfBirth; }
            set { _SurnameOfBirth = value; }
        }

        int? _CurrentNationality;
        public int? CurrentNationality
        {
            get { return _CurrentNationality; }
            set { _CurrentNationality = value; }
        }

        string _CurrentNationalityName;
        public string CurrentNationalityName
        {
            get { return _CurrentNationalityName; }
            set { _CurrentNationalityName = value; }
        }

        string _CurrentNationalityNameL;
        public string CurrentNationalityNameL
        {
            get { return _CurrentNationalityNameL; }
            set { _CurrentNationalityNameL = value; }
        }

        string _PlaceOfBirth;
        public string PlaceOfBirth
        {
            get { return _PlaceOfBirth; }
            set { _PlaceOfBirth = value; }
        }

        int? _CountryOfBirth;
        public int? CountryOfBirth
        {
            get { return _CountryOfBirth; }
            set { _CountryOfBirth = value; }
        }

        string _CountryOfBirthName;
        public string CountryOfBirthName
        {
            get { return _CountryOfBirthName; }
            set { _CountryOfBirthName = value; }
        }

        string _CountryOfBirthNameL;
        public string CountryOfBirthNameL
        {
            get { return _CountryOfBirthNameL; }
            set { _CountryOfBirthNameL = value; }
        }

        int? _NationalityAtBirth;
        public int? NationalityAtBirth
        {
            get { return _NationalityAtBirth; }
            set { _NationalityAtBirth = value; }
        }

        string _NationalityAtBirthName;
        public string NationalityAtBirthName
        {
            get { return _NationalityAtBirthName; }
            set { _NationalityAtBirthName = value; }
        }

        string _NationalityAtBirthNameL;
        public string NationalityAtBirthNameL
        {
            get { return _NationalityAtBirthNameL; }
            set { _NationalityAtBirthNameL = value; }
        }

        string _Sex;
        public string Sex
        {
            get { return _Sex; }
            set { _Sex = value; }
        }

        string _FatherName;
        public string FatherName
        {
            get { return _FatherName; }
            set { _FatherName = value; }
        }

        string _MotherName;
        public string MotherName
        {
            get { return _MotherName; }
            set { _MotherName = value; }
        }

        Int16? _MaritalStatus;
        public Int16? MaritalStatus
        {
            get { return _MaritalStatus; }
            set { _MaritalStatus = value; }
        }

        string _MaritalStatusOther;
        public string MaritalStatusOther
        {
            get { return _MaritalStatusOther; }
            set { _MaritalStatusOther = value; }
        }

        string _ParentInfo;
        public string ParentInfo
        {
            get { return _ParentInfo; }
            set { _ParentInfo = value; }
        }

        Int16? _PassportType;
        public Int16? PassportType
        {
            get { return _PassportType; }
            set { _PassportType = value; }
        }

        string _HomeAddress;
        public string HomeAddress
        {
            get { return _HomeAddress; }
            set { _HomeAddress = value; }
        }

        string _HomePhone;
        public string HomePhone
        {
            get { return _HomePhone; }
            set { _HomePhone = value; }
        }

        string _HomeEmail;
        public string HomeEmail
        {
            get { return _HomeEmail; }
            set { _HomeEmail = value; }
        }

        int? _Job;
        public int? Job
        {
            get { return _Job; }
            set { _Job = value; }
        }

        string _JobName;
        public string JobName
        {
            get { return _JobName; }
            set { _JobName = value; }
        }

        string _JobNameL;
        public string JobNameL
        {
            get { return _JobNameL; }
            set { _JobNameL = value; }
        }

        Int16? _PurposeOf;
        public Int16? PurposeOf
        {
            get { return _PurposeOf; }
            set { _PurposeOf = value; }
        }

        string _PurposeOfName;
        public string PurposeOfName
        {
            get { return _PurposeOfName; }
            set { _PurposeOfName = value; }
        }

        string _PurposeOfNameL;
        public string PurposeOfNameL
        {
            get { return _PurposeOfNameL; }
            set { _PurposeOfNameL = value; }
        }

        string _PurposeOfOther;
        public string PurposeOfOther
        {
            get { return _PurposeOfOther; }
            set { _PurposeOfOther = value; }
        }

        string _DestinationCountry;
        public string DestinationCountry
        {
            get { return _DestinationCountry; }
            set { _DestinationCountry = value; }
        }

        string _FirstEntry;
        public string FirstEntry
        {
            get { return _FirstEntry; }
            set { _FirstEntry = value; }
        }

        Int16? _NumberOfEntries;
        public Int16? NumberOfEntries
        {
            get { return _NumberOfEntries; }
            set { _NumberOfEntries = value; }
        }

        int? _NumberOfDays;
        public int? NumberOfDays
        {
            get { return _NumberOfDays; }
            set { _NumberOfDays = value; }
        }

        string _OldShengenVisas;
        public string OldShengenVisas
        {
            get { return _OldShengenVisas; }
            set { _OldShengenVisas = value; }
        }

        string _OldSpainVisas;
        public string OldSpainVisas
        {
            get { return _OldSpainVisas; }
            set { _OldSpainVisas = value; }
        }

        string _EmployerName;
        public string EmployerName
        {
            get { return _EmployerName; }
            set { _EmployerName = value; }
        }

        string _EmployerAddress;
        public string EmployerAddress
        {
            get { return _EmployerAddress; }
            set { _EmployerAddress = value; }
        }

        string _EmployerPhone;
        public string EmployerPhone
        {
            get { return _EmployerPhone; }
            set { _EmployerPhone = value; }
        }

        Int16? _PayingTheCosts;
        public Int16? PayingTheCosts
        {
            get { return _PayingTheCosts; }
            set { _PayingTheCosts = value; }
        }

        string _PayingTheCosts1;
        public string PayingTheCosts1
        {
            get { return _PayingTheCosts1; }
            set { _PayingTheCosts1 = value; }
        }

        string _PayingTheCosts2;
        public string PayingTheCosts2
        {
            get { return _PayingTheCosts2; }
            set { _PayingTheCosts2 = value; }
        }

        string _PayingTheCosts1Remark1;
        public string PayingTheCosts1Remark1
        {
            get { return _PayingTheCosts1Remark1; }
            set { _PayingTheCosts1Remark1 = value; }
        }

        string _PayingTheCosts2Remark1;
        public string PayingTheCosts2Remark1
        {
            get { return _PayingTheCosts2Remark1; }
            set { _PayingTheCosts2Remark1 = value; }
        }

        string _PayingTheCosts2Remark2;
        public string PayingTheCosts2Remark2
        {
            get { return _PayingTheCosts2Remark2; }
            set { _PayingTheCosts2Remark2 = value; }
        }

        string _IssuedPlaced;
        public string IssuedPlaced
        {
            get { return _IssuedPlaced; }
            set { _IssuedPlaced = value; }
        }

        string _IssuedDate;
        public string IssuedDate
        {
            get { return _IssuedDate; }
            set { _IssuedDate = value; }
        }

        object _Photo;
        public object Photo
        {
            get { return _Photo; }
            set { _Photo = value; }
        }

        object _RprPdf;
        public object RprPdf
        {
            get { return _RprPdf; }
            set { _RprPdf = value; }
        }

        Int16? _ConsStat;
        public Int16? ConsStat
        {
            get { return _ConsStat; }
            set { _ConsStat = value; }
        }

        DateTime? _ConsStatDate;
        public DateTime? ConsStatDate
        {
            get { return _ConsStatDate; }
            set { _ConsStatDate = value; }
        }

        string _ConsVisaNum;
        public string ConsVisaNum
        {
            get { return _ConsVisaNum; }
            set { _ConsVisaNum = value; }
        }

        string _ErrMsg;
        public string ErrMsg
        {
            get { return _ErrMsg; }
            set { _ErrMsg = value; }
        }

        Int16? _ErrCount;
        public Int16? ErrCount
        {
            get { return _ErrCount; }
            set { _ErrCount = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable]
    public class ResCustPriceRecord
    {
        public ResCustPriceRecord()
        {
            _ChkSel = true;
            _MemTable = false;
        }

        Int32 _RecordID;
        public Int32 RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        Int32 _RecID;
        public Int32 RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int32 _CustNo;
        public Int32 CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int32? _ServiceID;
        public Int32? ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        Int32? _ExtServiceID;
        public Int32? ExtServiceID
        {
            get { return _ExtServiceID; }
            set { _ExtServiceID = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        decimal? _NetPrice;
        public decimal? NetPrice
        {
            get { return _NetPrice; }
            set { _NetPrice = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        Int32? _PLSpoNo;
        public Int32? PLSpoNo
        {
            get { return _PLSpoNo; }
            set { _PLSpoNo = value; }
        }

        Int32? _PLSpoValNo;
        public Int32? PLSpoValNo
        {
            get { return _PLSpoValNo; }
            set { _PLSpoValNo = value; }
        }

        bool? _ExtBed;
        public bool? ExtBed
        {
            get { return _ExtBed; }
            set { _ExtBed = value; }
        }

        bool? _SpMan;
        public bool? SpMan
        {
            get { return _SpMan; }
            set { _SpMan = value; }
        }

        decimal? _PLSpoVal;
        public decimal? PLSpoVal
        {
            get { return _PLSpoVal; }
            set { _PLSpoVal = value; }
        }

        decimal? _SPCorrect;
        public decimal? SPCorrect
        {
            get { return _SPCorrect; }
            set { _SPCorrect = value; }
        }

        bool _ChkSel;
        public bool ChkSel
        {
            get { return _ChkSel; }
            set { _ChkSel = value; }
        }

        decimal? _NetPromo;
        public decimal? NetPromo
        {
            get { return _NetPromo; }
            set { _NetPromo = value; }
        }

        int? _RefPRecNo;
        public int? RefPRecNo
        {
            get { return _RefPRecNo; }
            set { _RefPRecNo = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

    }

    [Serializable]
    public class ResServiceRecord
    {
        public int RecordID { get; set; }
        public int RecID { get; set; }
        public string ResNo { get; set; }
        public Int16? SeqNo { get; set; }
        public string ServiceType { get; set; }
        public string ServiceTypeName { get; set; }
        public string ServiceTypeNameL { get; set; }
        public string Service { get; set; }
        public string ServiceName { get; set; }
        public string ServiceNameL { get; set; }
        public int? DepLocation { get; set; }
        public string DepLocationName { get; set; }
        public string DepLocationNameL { get; set; }
        public int? ArrLocation { get; set; }
        public string ArrLocationName { get; set; }
        public string ArrLocationNameL { get; set; }
        public DateTime? BegDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Int16? Duration { get; set; }
        public string Supplier { get; set; }
        public string SupplierName { get; set; }
        public string SupplierNameL { get; set; }
        public Int16? Unit { get; set; }
        public string Room { get; set; }
        public string RoomName { get; set; }
        public string RoomNameL { get; set; }
        public string Accom { get; set; }
        public string AccomName { get; set; }
        public string AccomNameL { get; set; }
        public string Board { get; set; }
        public string BoardName { get; set; }
        public string BoardNameL { get; set; }
        public Int16? Adult { get; set; }
        public Int16? Child { get; set; }
        public string FlgClass { get; set; }
        public Int16? PriceSource { get; set; }
        public decimal? NetPrice { get; set; }
        public string NetCur { get; set; }
        public decimal? SalePrice { get; set; }
        public string SaleCur { get; set; }
        public Int16? StatConf { get; set; }
        public string StatConfName { get; set; }
        public string StatConfNameL { get; set; }
        public DateTime? StatConfDate { get; set; }
        public Int16? StatSer { get; set; }
        public string StatSerName { get; set; }
        public string StatSerNameL { get; set; }
        public DateTime? StatSerDate { get; set; }
        public Int16? StepNo { get; set; }
        public string SupNote { get; set; }
        public decimal? FixCom { get; set; }
        public string PayCom { get; set; }
        public string SendSupplier { get; set; }
        public DateTime? SendSupplierDate { get; set; }
        public string SendSupplierStat { get; set; }
        public decimal? PayToSup { get; set; }
        public DateTime? PayDueDate { get; set; }
        public int? FlyTicketID { get; set; }
        public string AllotType { get; set; }
        public int? CatPRecNo { get; set; }
        public string HotSPO { get; set; }
        public byte? ResMainStat { get; set; }
        public string IncPack { get; set; }
        public string PayComEB { get; set; }
        public string RoomNo { get; set; }
        public string Bus { get; set; }
        public string BusName { get; set; }
        public byte? AllotFHOpt { get; set; }
        public decimal? Tax { get; set; }
        public decimal? SupInvAmount { get; set; }
        public string CallFromResMain { get; set; }
        public string DailyHotelAllot { get; set; }
        public bool? PayPasEB { get; set; }
        public bool? Compulsory { get; set; }
        public decimal? NetPriceBEB { get; set; }
        public decimal? TaxPer { get; set; }
        public bool? CanDiscount { get; set; }
        public bool? SerCheck { get; set; }
        public DateTime? SerCheckDate { get; set; }
        public string SerCheckUser { get; set; }
        public int? Pickup { get; set; }
        public string PickupName { get; set; }
        public string PickupNameL { get; set; }
        public decimal? PasDisAmount { get; set; }
        public decimal? AgDisAmount { get; set; }
        public DateTime? PickupTime { get; set; }
        public DateTime? RPickupTime { get; set; }
        public string PickupNote { get; set; }
        public string RPickupNote { get; set; }
        public decimal? PasEBAmount { get; set; }
        public decimal? AgComAmount { get; set; }
        public decimal? AgEBAmount { get; set; }
        public decimal? PSalePrice { get; set; }
        public decimal? ASalePrice { get; set; }
        public string PnrNo { get; set; }
        public Int16? AllotUnit { get; set; }
        public byte? TrfDT { get; set; }
        public string TrfDTName { get; set; }
        public byte? TrfAT { get; set; }
        public string TrfATName { get; set; }
        public string TrfDep { get; set; }
        public string TrfDepName { get; set; }
        public string TrfDepNameL { get; set; }
        public string TrfArr { get; set; }
        public string TrfArrName { get; set; }
        public string TrfArrNameL { get; set; }
        public Int16? GrpPax { get; set; }
        public decimal? NetKBAmount { get; set; }
        public decimal? SaleKBAmount { get; set; }
        public DateTime? CrtDate { get; set; }
        public DateTime? ChgDate { get; set; }
        public bool? CinAllot { get; set; }
        public string NoteFromIncome { get; set; }
        public byte? StepType { get; set; }
        public bool ChkSel { get; set; }
        public bool ChkCelMakeRes2 { get; set; }
        public Int16? SaleType { get; set; }
        public int? GrpPackID { get; set; }
        public string PriceCode { get; set; }
        public byte? RTrfDT { get; set; }
        public string RTrfDTName { get; set; }
        public string RTrfDep { get; set; }
        public string RTrfDepName { get; set; }
        public string RTrfDepNameL { get; set; }
        public byte? RTrfAT { get; set; }
        public string RTrfATName { get; set; }
        public string RTrfArr { get; set; }
        public string RTrfArrName { get; set; }
        public string RTrfArrNameL { get; set; }
        public DateTime? ExcTransTime { get; set; }
        public int? ExcFreeTime { get; set; }
        public int? ExcTimeID { get; set; }
        public int? PriceID { get; set; }
        public bool ExcludeService { get; set; }
        public bool SaleableService { get; set; }
        public bool MemTable { get; set; }
        public RecordStatus RecStatus { get; set; }
        public string NameS { get; set; }
        public int? PLSNo { get; set; }
        public long? PackPLID { get; set; }
        public int? VehicleCatID { get; set; }
        public bool CreateSystem { get; set; }
        public string Description { get; set; }
        public Int16? VehicleUnit { get; set; }

        public ResServiceRecord()
        {
            this.SaleableService = false;
            this.ExcludeService = false;
            this.ChkSel = true;
            this.ChkCelMakeRes2 = true;
            this.RecStatus = RecordStatus.New;
            this.MemTable = false;
            this.PLSNo = null;
            this.CreateSystem = false;
        }
    }

    [Serializable]
    public class ResConRecord
    {
        public ResConRecord()
        {
            _ChkSel = true;
            _RecStatus = RecordStatus.New;
            _MemTable = false;
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        int? _CustNoT;
        public int? CustNoT
        {
            get { return _CustNoT; }
            set { _CustNoT = value; }
        }

        int _ServiceID;
        public int ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        int? _ServiceIDT;
        public int? ServiceIDT
        {
            get { return _ServiceIDT; }
            set { _ServiceIDT = value; }
        }

        Int16? _UnitNo;
        public Int16? UnitNo
        {
            get { return _UnitNo; }
            set { _UnitNo = value; }
        }

        Int16? _DocStat;
        public Int16? DocStat
        {
            get { return _DocStat; }
            set { _DocStat = value; }
        }

        string _DocNo;
        public string DocNo
        {
            get { return _DocNo; }
            set { _DocNo = value; }
        }

        DateTime? _DocIsDate;
        public DateTime? DocIsDate
        {
            get { return _DocIsDate; }
            set { _DocIsDate = value; }
        }

        string _Remark;
        public string Remark
        {
            get { return _Remark; }
            set { _Remark = value; }
        }

        string _SpecSerRQCode1;
        public string SpecSerRQCode1
        {
            get { return _SpecSerRQCode1; }
            set { _SpecSerRQCode1 = value; }
        }

        string _SpecSerRQCode2;
        public string SpecSerRQCode2
        {
            get { return _SpecSerRQCode2; }
            set { _SpecSerRQCode2 = value; }
        }

        bool _ChkSel;
        public bool ChkSel
        {
            get { return _ChkSel; }
            set { _ChkSel = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable]
    public class ResServiceExtRecord
    {
        public ResServiceExtRecord()
        {
            _TakeAllUser = false;
            _Removed = false;
            _ExcludeService = false;
            _ChkSel = true;
            _ChkCelMakeRes2 = true;
            _RecStatus = RecordStatus.New;
            _MemTable = false;
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int32? _ServiceID;
        public Int32? ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        string _ServiceTypeName;
        public string ServiceTypeName
        {
            get { return _ServiceTypeName; }
            set { _ServiceTypeName = value; }
        }

        string _ServiceTypeNameL;
        public string ServiceTypeNameL
        {
            get { return _ServiceTypeNameL; }
            set { _ServiceTypeNameL = value; }
        }

        string _ExtService;
        public string ExtService
        {
            get { return _ExtService; }
            set { _ExtService = value; }
        }

        string _ExtServiceName;
        public string ExtServiceName
        {
            get { return _ExtServiceName; }
            set { _ExtServiceName = value; }
        }

        string _ExtServiceNameL;
        public string ExtServiceNameL
        {
            get { return _ExtServiceNameL; }
            set { _ExtServiceNameL = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _Duration;
        public Int16? Duration
        {
            get { return _Duration; }
            set { _Duration = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        string _SupplierName;
        public string SupplierName
        {
            get { return _SupplierName; }
            set { _SupplierName = value; }
        }

        string _SupplierNameL;
        public string SupplierNameL
        {
            get { return _SupplierNameL; }
            set { _SupplierNameL = value; }
        }

        Int16? _Unit;
        public Int16? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        Int16? _Adult;
        public Int16? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16? _Child;
        public Int16? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        decimal? _NetPrice;
        public decimal? NetPrice
        {
            get { return _NetPrice; }
            set { _NetPrice = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        Int16? _StatSer;
        public Int16? StatSer
        {
            get { return _StatSer; }
            set { _StatSer = value; }
        }

        DateTime? _StatSerDate;
        public DateTime? StatSerDate
        {
            get { return _StatSerDate; }
            set { _StatSerDate = value; }
        }

        Int16? _StatConf;
        public Int16? StatConf
        {
            get { return _StatConf; }
            set { _StatConf = value; }
        }

        DateTime? _StatConfDate;
        public DateTime? StatConfDate
        {
            get { return _StatConfDate; }
            set { _StatConfDate = value; }
        }

        string _IntNote;
        public string IntNote
        {
            get { return _IntNote; }
            set { _IntNote = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        decimal? _FixCom;
        public decimal? FixCom
        {
            get { return _FixCom; }
            set { _FixCom = value; }
        }

        string _PayCom;
        public string PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        string _SendSupplier;
        public string SendSupplier
        {
            get { return _SendSupplier; }
            set { _SendSupplier = value; }
        }

        DateTime? _SendSupplierDate;
        public DateTime? SendSupplierDate
        {
            get { return _SendSupplierDate; }
            set { _SendSupplierDate = value; }
        }

        string _SendSupplierStat;
        public string SendSupplierStat
        {
            get { return _SendSupplierStat; }
            set { _SendSupplierStat = value; }
        }

        decimal? _PayToSup;
        public decimal? PayToSup
        {
            get { return _PayToSup; }
            set { _PayToSup = value; }
        }

        string _Compulsory;
        public string Compulsory
        {
            get { return _Compulsory; }
            set { _Compulsory = value; }
        }

        byte? _PriceType;
        public byte? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        byte? _CalcType;
        public byte? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        string _IncPack;
        public string IncPack
        {
            get { return _IncPack; }
            set { _IncPack = value; }
        }

        string _PayComEB;
        public string PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        Int16? _PriceSource;
        public Int16? PriceSource
        {
            get { return _PriceSource; }
            set { _PriceSource = value; }
        }

        decimal? _Tax;
        public decimal? Tax
        {
            get { return _Tax; }
            set { _Tax = value; }
        }

        decimal? _SupInvAmount;
        public decimal? SupInvAmount
        {
            get { return _SupInvAmount; }
            set { _SupInvAmount = value; }
        }

        bool? _PayPasEB;
        public bool? PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool? _CanDiscount;
        public bool? CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        decimal? _PasDisAmount;
        public decimal? PasDisAmount
        {
            get { return _PasDisAmount; }
            set { _PasDisAmount = value; }
        }

        decimal? _AgDisAmount;
        public decimal? AgDisAmount
        {
            get { return _AgDisAmount; }
            set { _AgDisAmount = value; }
        }

        decimal? _PasEBAmount;
        public decimal? PasEBAmount
        {
            get { return _PasEBAmount; }
            set { _PasEBAmount = value; }
        }

        decimal? _AgComAmount;
        public decimal? AgComAmount
        {
            get { return _AgComAmount; }
            set { _AgComAmount = value; }
        }

        decimal? _AgEBAmount;
        public decimal? AgEBAmount
        {
            get { return _AgEBAmount; }
            set { _AgEBAmount = value; }
        }

        decimal? _PSalePrice;
        public decimal? PSalePrice
        {
            get { return _PSalePrice; }
            set { _PSalePrice = value; }
        }

        decimal? _ASalePrice;
        public decimal? ASalePrice
        {
            get { return _ASalePrice; }
            set { _ASalePrice = value; }
        }

        Int16? _AllotUnit;
        public Int16? AllotUnit
        {
            get { return _AllotUnit; }
            set { _AllotUnit = value; }
        }

        Int16? _ResMainStat;
        public Int16? ResMainStat
        {
            get { return _ResMainStat; }
            set { _ResMainStat = value; }
        }

        bool _ChkSel;
        public bool ChkSel
        {
            get { return _ChkSel; }
            set { _ChkSel = value; }
        }

        bool _ChkCelMakeRes2;
        public bool ChkCelMakeRes2
        {
            get { return _ChkCelMakeRes2; }
            set { _ChkCelMakeRes2 = value; }
        }

        int? _PriceID;
        public int? PriceID
        {
            get { return _PriceID; }
            set { _PriceID = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        bool _Removed;
        public bool Removed
        {
            get { return _Removed; }
            set { _Removed = value; }
        }

        bool _ExcludeService;
        public bool ExcludeService
        {
            get { return _ExcludeService; }
            set { _ExcludeService = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }

        bool? _TakeAllUser;
        public bool? TakeAllUser
        {
            get { return _TakeAllUser; }
            set { _TakeAllUser = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

    }

    [Serializable]
    public class ResConExtRecord
    {
        public ResConExtRecord()
        {
            _Removed = false;
            _ChkSel = true;
            _RecStatus = RecordStatus.New;
            _MemTable = false;
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        int? _CustNoT;
        public int? CustNoT
        {
            get { return _CustNoT; }
            set { _CustNoT = value; }
        }

        int _ServiceID;
        public int ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        int? _ServiceIDT;
        public int? ServiceIDT
        {
            get { return _ServiceIDT; }
            set { _ServiceIDT = value; }
        }

        Int16? _UnitNo;
        public Int16? UnitNo
        {
            get { return _UnitNo; }
            set { _UnitNo = value; }
        }

        bool _ChkSel;
        public bool ChkSel
        {
            get { return _ChkSel; }
            set { _ChkSel = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        bool _Removed;
        public bool Removed
        {
            get { return _Removed; }
            set { _Removed = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable()]
    public class ResSupDisRecord
    {
        public ResSupDisRecord()
        {
            _RecStatus = RecordStatus.New;
            _MemTable = false;
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        string _SupDis;
        public string SupDis
        {
            get { return _SupDis; }
            set { _SupDis = value; }
        }

        string _SupDisName;
        public string SupDisName
        {
            get { return _SupDisName; }
            set { _SupDisName = value; }
        }

        string _SupDisNameL;
        public string SupDisNameL
        {
            get { return _SupDisNameL; }
            set { _SupDisNameL = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        decimal? _PerVal;
        public decimal? PerVal
        {
            get { return _PerVal; }
            set { _PerVal = value; }
        }

        string _PayCom;
        public string PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        string _Explain;
        public string Explain
        {
            get { return _Explain; }
            set { _Explain = value; }
        }

        string _CanChg;
        public string CanChg
        {
            get { return _CanChg; }
            set { _CanChg = value; }
        }

        string _SD;
        public string SD
        {
            get { return _SD; }
            set { _SD = value; }
        }

        string _ApplyType;
        public string ApplyType
        {
            get { return _ApplyType; }
            set { _ApplyType = value; }
        }

        Int16? _Priority;
        public Int16? Priority
        {
            get { return _Priority; }
            set { _Priority = value; }
        }

        string _Ok;
        public string Ok
        {
            get { return _Ok; }
            set { _Ok = value; }
        }

        string _PayComEB;
        public string PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool? _PayPasEB;
        public bool? PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        Int32? _JournalID;
        public Int32? JournalID
        {
            get { return _JournalID; }
            set { _JournalID = value; }
        }

        Int32? _PLNo;
        public Int32? PLNo
        {
            get { return _PLNo; }
            set { _PLNo = value; }
        }

        Int32? _CustNo;
        public Int32? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    public class ResPaymentsRecord
    {
        public ResPaymentsRecord()
        {
            _RecStatus = RecordStatus.New;
        }

        int? _JournalId;
        public int? JournalId
        {
            get { return _JournalId; }
            set { _JournalId = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        Int16? _RecType;
        public Int16? RecType
        {
            get { return _RecType; }
            set { _RecType = value; }
        }

        DateTime? _PayDate;
        public DateTime? PayDate
        {
            get { return _PayDate; }
            set { _PayDate = value; }
        }

        string _PayType;
        public string PayType
        {
            get { return _PayType; }
            set { _PayType = value; }
        }

        decimal? _PaidAmount;
        public decimal? PaidAmount
        {
            get { return _PaidAmount; }
            set { _PaidAmount = value; }
        }

        string _PaidCur;
        public string PaidCur
        {
            get { return _PaidCur; }
            set { _PaidCur = value; }
        }

        string _PayTypeName;
        public string PayTypeName
        {
            get { return _PayTypeName; }
            set { _PayTypeName = value; }
        }

        string _PayTypeNameL;
        public string PayTypeNameL
        {
            get { return _PayTypeNameL; }
            set { _PayTypeNameL = value; }
        }

        string _Reference;
        public string Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        string _ClientName;
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    public class ResCommentRecord
    {
        public ResCommentRecord()
        {
            _RecStatus = RecordStatus.New;
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        string _UserID;
        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        string _UserType;
        public string UserType
        {
            get { return _UserType; }
            set { _UserType = value; }
        }

        string _Comment;
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        DateTime? _CDate;
        public DateTime? CDate
        {
            get { return _CDate; }
            set { _CDate = value; }
        }

        string _isRead;
        public string IsRead
        {
            get { return _isRead; }
            set { _isRead = value; }
        }

        string _RUserID;
        public string RUserID
        {
            get { return _RUserID; }
            set { _RUserID = value; }
        }

        DateTime? _RDate;
        public DateTime? RDate
        {
            get { return _RDate; }
            set { _RDate = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable]
    public class ResChgFeeRecord
    {
        public ResChgFeeRecord()
        {
            _MemTable = false;
            _RecStatus = RecordStatus.New;
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int16? _ChgType;
        public Int16? ChgType
        {
            get { return _ChgType; }
            set { _ChgType = value; }
        }

        string _ChgTypeName;
        public string ChgTypeName
        {
            get { return _ChgTypeName; }
            set { _ChgTypeName = value; }
        }

        DateTime? _ChgDate;
        public DateTime? ChgDate
        {
            get { return _ChgDate; }
            set { _ChgDate = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        int? _ServiceID;
        public int? ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        int? _ExtServiceID;
        public int? ExtServiceID
        {
            get { return _ExtServiceID; }
            set { _ExtServiceID = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }

        RecordStatus _RecStatus;
        public RecordStatus RecStatus
        {
            get { return _RecStatus; }
            set { _RecStatus = value; }
        }
    }

    [Serializable]
    public class ResFlightDetailRecord
    {
        public ResFlightDetailRecord()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        string _DepAirport;
        public string DepAirport
        {
            get { return _DepAirport; }
            set { _DepAirport = value; }
        }

        string _ArrAirport;
        public string ArrAirport
        {
            get { return _ArrAirport; }
            set { _ArrAirport = value; }
        }

        DateTime? _FlyDate;
        public DateTime? FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }
    }

    [Serializable]
    public class ServiceExtPriceRecord
    {
        public ServiceExtPriceRecord()
        {
        }

        Int32? _RecID;
        public Int32? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _ServiceItem;
        public string ServiceItem
        {
            get { return _ServiceItem; }
            set { _ServiceItem = value; }
        }

        Int32? _Location;
        public Int32? Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _HotCat;
        public string HotCat
        {
            get { return _HotCat; }
            set { _HotCat = value; }
        }

        string _ChkType;
        public string ChkType
        {
            get { return _ChkType; }
            set { _ChkType = value; }
        }

        string _AirPort;
        public string AirPort
        {
            get { return _AirPort; }
            set { _AirPort = value; }
        }

        string _SClass;
        public string SClass
        {
            get { return _SClass; }
            set { _SClass = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _NetAdlPrice;
        public decimal? NetAdlPrice
        {
            get { return _NetAdlPrice; }
            set { _NetAdlPrice = value; }
        }

        decimal? _ChdAge1;
        public decimal? ChdAge1
        {
            get { return _ChdAge1; }
            set { _ChdAge1 = value; }
        }

        decimal? _ChdAge2;
        public decimal? ChdAge2
        {
            get { return _ChdAge2; }
            set { _ChdAge2 = value; }
        }

        decimal? _NetChdPrice;
        public decimal? NetChdPrice
        {
            get { return _NetChdPrice; }
            set { _NetChdPrice = value; }
        }

        decimal? _InfAge1;
        public decimal? InfAge1
        {
            get { return _InfAge1; }
            set { _InfAge1 = value; }
        }

        decimal? _InfAge2;
        public decimal? InfAge2
        {
            get { return _InfAge2; }
            set { _InfAge2 = value; }
        }

        decimal? _NetInfPrice;
        public decimal? NetInfPrice
        {
            get { return _NetInfPrice; }
            set { _NetInfPrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _SaleAdlPrice;
        public decimal? SaleAdlPrice
        {
            get { return _SaleAdlPrice; }
            set { _SaleAdlPrice = value; }
        }

        decimal? _SaleChdPrice;
        public decimal? SaleChdPrice
        {
            get { return _SaleChdPrice; }
            set { _SaleChdPrice = value; }
        }

        decimal? _SaleInfPrice;
        public decimal? SaleInfPrice
        {
            get { return _SaleInfPrice; }
            set { _SaleInfPrice = value; }
        }

        string _Compulsory;
        public string Compulsory
        {
            get { return _Compulsory; }
            set { _Compulsory = value; }
        }

        Int16? _CalcType;
        public Int16? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        Int32? _SeasonID;
        public Int32? SeasonID
        {
            get { return _SeasonID; }
            set { _SeasonID = value; }
        }

        string _PayCom;
        public string PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _PayComEB;
        public string PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        string _WebSale;
        public string WebSale
        {
            get { return _WebSale; }
            set { _WebSale = value; }
        }

        string _Bus;
        public string Bus
        {
            get { return _Bus; }
            set { _Bus = value; }
        }

        DateTime? _SaleBegDate;
        public DateTime? SaleBegDate
        {
            get { return _SaleBegDate; }
            set { _SaleBegDate = value; }
        }

        DateTime? _SaleEndDate;
        public DateTime? SaleEndDate
        {
            get { return _SaleEndDate; }
            set { _SaleEndDate = value; }
        }

        bool? _PayPasEB;
        public bool? PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool? _CanDiscount;
        public bool? CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        bool? _IncFlightCost;
        public bool? IncFlightCost
        {
            get { return _IncFlightCost; }
            set { _IncFlightCost = value; }
        }

        Int16? _MinNight;
        public Int16? MinNight
        {
            get { return _MinNight; }
            set { _MinNight = value; }
        }

        Int16? _MaxNight;
        public Int16? MaxNight
        {
            get { return _MaxNight; }
            set { _MaxNight = value; }
        }

        bool? _ChdG1UseAllot;
        public bool? ChdG1UseAllot
        {
            get { return _ChdG1UseAllot; }
            set { _ChdG1UseAllot = value; }
        }

        bool? _ChdG2UseAllot;
        public bool? ChdG2UseAllot
        {
            get { return _ChdG2UseAllot; }
            set { _ChdG2UseAllot = value; }
        }

        string _ValDays;
        public string ValDays
        {
            get { return _ValDays; }
            set { _ValDays = value; }
        }

        bool? _TakeAllUser;
        public bool? TakeAllUser
        {
            get { return _TakeAllUser; }
            set { _TakeAllUser = value; }
        }
    }

    public class GetPassAmountForPayRecord
    {
        public GetPassAmountForPayRecord()
        {
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        decimal? _PasDiscBase;
        public decimal? PasDiscBase
        {
            get { return _PasDiscBase; }
            set { _PasDiscBase = value; }
        }

        decimal? _TotPayment;
        public decimal? TotPayment
        {
            get { return _TotPayment; }
            set { _TotPayment = value; }
        }

        decimal? _ResAmount;
        public decimal? ResAmount
        {
            get { return _ResAmount; }
            set { _ResAmount = value; }
        }

    }

    public class CanReasonRecord
    {
        public CanReasonRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Penalized;
        public string Penalized
        {
            get { return _Penalized; }
            set { _Penalized = value; }
        }

        string _Comment;
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        bool _ValidForB2B;
        public bool ValidForB2B
        {
            get { return _ValidForB2B; }
            set { _ValidForB2B = value; }
        }

        bool _ValidForB2C;
        public bool ValidForB2C
        {
            get { return _ValidForB2C; }
            set { _ValidForB2C = value; }
        }
    }

    public class PenaltyRecord
    {
        public PenaltyRecord()
        {
        }

        Int32 _RecID;
        public Int32 RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        string _HolPack;
        public string HolPack
        {
            get { return _HolPack; }
            set { _HolPack = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _AccFromDay;
        public Int16? AccFromDay
        {
            get { return _AccFromDay; }
            set { _AccFromDay = value; }
        }

        Int16? _AccToDay;
        public Int16? AccToDay
        {
            get { return _AccToDay; }
            set { _AccToDay = value; }
        }

        Int16? _FromDay;
        public Int16? FromDay
        {
            get { return _FromDay; }
            set { _FromDay = value; }
        }

        Int16? _ToDay;
        public Int16? ToDay
        {
            get { return _ToDay; }
            set { _ToDay = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _HotelGrp;
        public string HotelGrp
        {
            get { return _HotelGrp; }
            set { _HotelGrp = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        decimal? _PerVal;
        public decimal? PerVal
        {
            get { return _PerVal; }
            set { _PerVal = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        decimal? _Adult;
        public decimal? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        decimal? _ChdG1;
        public decimal? ChdG1
        {
            get { return _ChdG1; }
            set { _ChdG1 = value; }
        }

        decimal? _ChdG2;
        public decimal? ChdG2
        {
            get { return _ChdG2; }
            set { _ChdG2 = value; }
        }

        decimal? _ChdG3;
        public decimal? ChdG3
        {
            get { return _ChdG3; }
            set { _ChdG3 = value; }
        }

        decimal? _ChdG1MaxAge;
        public decimal? ChdG1MaxAge
        {
            get { return _ChdG1MaxAge; }
            set { _ChdG1MaxAge = value; }
        }

        decimal? _ChdG2MaxAge;
        public decimal? ChdG2MaxAge
        {
            get { return _ChdG2MaxAge; }
            set { _ChdG2MaxAge = value; }
        }

        decimal? _ChdG3MaxAge;
        public decimal? ChdG3MaxAge
        {
            get { return _ChdG3MaxAge; }
            set { _ChdG3MaxAge = value; }
        }

        bool _DrReport;
        public bool DrReport
        {
            get { return _DrReport; }
            set { _DrReport = value; }
        }

        bool _CancelIns;
        public bool CancelIns
        {
            get { return _CancelIns; }
            set { _CancelIns = value; }
        }

        string _AgencyGrp;
        public string AgencyGrp
        {
            get { return _AgencyGrp; }
            set { _AgencyGrp = value; }
        }

        string _HolPackGrp;
        public string HolPackGrp
        {
            get { return _HolPackGrp; }
            set { _HolPackGrp = value; }
        }

    }

    public class PenaltyPriceRecord
    {
        public PenaltyPriceRecord()
        {
        }

        decimal? _Price;
        public decimal? Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        decimal? _Per;
        public decimal? Per
        {
            get { return _Per; }
            set { _Per = value; }
        }

    }

    public class ExtraServiceListRecord
    {
        public ExtraServiceListRecord()
        {
        }

        int? _MainService;
        public int? MainService
        {
            get { return _MainService; }
            set { _MainService = value; }
        }

        string _MainServiceName;
        public string MainServiceName
        {
            get { return _MainServiceName; }
            set { _MainServiceName = value; }
        }

        string _MainServiceNameL;
        public string MainServiceNameL
        {
            get { return _MainServiceNameL; }
            set { _MainServiceNameL = value; }
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _ServiceItem;
        public string ServiceItem
        {
            get { return _ServiceItem; }
            set { _ServiceItem = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _AirPort;
        public string AirPort
        {
            get { return _AirPort; }
            set { _AirPort = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _SaleAdlPrice;
        public decimal? SaleAdlPrice
        {
            get { return _SaleAdlPrice; }
            set { _SaleAdlPrice = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        string _DescriptionL;
        public string DescriptionL
        {
            get { return _DescriptionL; }
            set { _DescriptionL = value; }
        }

        string _SClass;
        public string SClass
        {
            get { return _SClass; }
            set { _SClass = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        Int16? _CalcType;
        public Int16? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        decimal? _InfAge1;
        public decimal? InfAge1
        {
            get { return _InfAge1; }
            set { _InfAge1 = value; }
        }

        decimal? _InfAge2;
        public decimal? InfAge2
        {
            get { return _InfAge2; }
            set { _InfAge2 = value; }
        }

        decimal? _ChdAge1;
        public decimal? ChdAge1
        {
            get { return _ChdAge1; }
            set { _ChdAge1 = value; }
        }

        decimal? _ChdAge2;
        public decimal? ChdAge2
        {
            get { return _ChdAge2; }
            set { _ChdAge2 = value; }
        }

        decimal? _SaleInfPrice;
        public decimal? SaleInfPrice
        {
            get { return _SaleInfPrice; }
            set { _SaleInfPrice = value; }
        }

        decimal? _SaleChdPrice;
        public decimal? SaleChdPrice
        {
            get { return _SaleChdPrice; }
            set { _SaleChdPrice = value; }
        }

        decimal? _CalcSalePrice;
        public decimal? CalcSalePrice
        {
            get { return _CalcSalePrice; }
            set { _CalcSalePrice = value; }
        }

        string _DispInFPasLst;
        public string DispInFPasLst
        {
            get { return _DispInFPasLst; }
            set { _DispInFPasLst = value; }
        }

        //Haluk Supplieri servicele aynı olanı ilk getirmek için eklendi. 06,10,2010
        byte? _OrderNumber;
        public byte? OrderNumber
        {
            get { return _OrderNumber; }
            set { _OrderNumber = value; }
        }

        //Haluk bütün müşteriler extrayı almak zorundadır seçeneği 07/10/2010
        bool? _TakeAllUser;
        public bool? TakeAllUser
        {
            get { return _TakeAllUser; }
            set { _TakeAllUser = value; }
        }

        bool? _MultiSaleSer;
        public bool? MultiSaleSer
        {
            get { return _MultiSaleSer; }
            set { _MultiSaleSer = value; }
        }

        string _ChkType;
        public string ChkType
        {
            get { return _ChkType; }
            set { _ChkType = value; }
        }

        string _WebSale;
        public string WebSale
        {
            get { return _WebSale; }
            set { _WebSale = value; }
        }

        string _TextType;
        public string TextType
        {
            get { return _TextType; }
            set { _TextType = value; }
        }

        string _DescText;
        public string DescText
        {
            get { return _DescText; }
            set { _DescText = value; }
        }

        bool? _NoShowInvZero;
        public bool? NoShowInvZero
        {
            get { return _NoShowInvZero; }
            set { _NoShowInvZero = value; }
        }

        bool? _ShowInResDetB2B;
        public bool? ShowInResDetB2B
        {
            get { return _ShowInResDetB2B; }
            set { _ShowInResDetB2B = value; }
        }

        bool? _ShowInResDetB2C;
        public bool? ShowInResDetB2C
        {
            get { return _ShowInResDetB2C; }
            set { _ShowInResDetB2C = value; }
        }
    }

    public class ConfStatRecords
    {
        public ConfStatRecords()
        {
        }

        int? _ServiceRecID;
        public int? ServiceRecID
        {
            get { return _ServiceRecID; }
            set { _ServiceRecID = value; }
        }

        int? _ExtraServiceRecID;
        public int? ExtraServiceRecID
        {
            get { return _ExtraServiceRecID; }
            set { _ExtraServiceRecID = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        Int16? _StatSer;
        public Int16? StatSer
        {
            get { return _StatSer; }
            set { _StatSer = value; }
        }
    }

    public class ReservastionSaveErrorRecord
    {
        public ReservastionSaveErrorRecord()
        {
            _sendEmailB2C = "";
            _GotoPaymentPage = false;
            _DirectPaymentPage = false;
            _GotoReservation = true;
        }

        bool _ControlOK;
        public bool ControlOK
        {
            get { return _ControlOK; }
            set { _ControlOK = value; }
        }

        string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        string _OtherReturnValue;
        public string OtherReturnValue
        {
            get { return _OtherReturnValue; }
            set { _OtherReturnValue = value; }
        }

        string _sendEmailB2C;
        public string SendEmailB2C
        {
            get { return _sendEmailB2C; }
            set { _sendEmailB2C = value; }
        }

        bool _GotoPaymentPage;
        public bool GotoPaymentPage
        {
            get { return _GotoPaymentPage; }
            set { _GotoPaymentPage = value; }
        }

        string _PaymentPageUrl;
        public string PaymentPageUrl
        {
            get { return _PaymentPageUrl; }
            set { _PaymentPageUrl = value; }
        }

        bool _DirectPaymentPage;
        public bool DirectPaymentPage
        {
            get { return _DirectPaymentPage; }
            set { _DirectPaymentPage = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        bool _GotoReservation;
        public bool GotoReservation
        {
            get { return _GotoReservation; }
            set { _GotoReservation = value; }
        }

        bool _GotoSearchResult;
        public bool GotoSearchResult
        {
            get { return _GotoSearchResult; }
            set { _GotoSearchResult = value; }
        }
    }

    [Serializable]
    public class ReservationFlightInfoRecord
    {
        public ReservationFlightInfoRecord()
        {
        }

        string _DepFlight;
        public string DepFlight
        {
            get { return _DepFlight; }
            set { _DepFlight = value; }
        }

        DateTime? _DepDate;
        public DateTime? DepDate
        {
            get { return _DepDate; }
            set { _DepDate = value; }
        }

        int? _DepSeat;
        public int? DepSeat
        {
            get { return _DepSeat; }
            set { _DepSeat = value; }
        }

        string _RetFlight;
        public string RetFlight
        {
            get { return _RetFlight; }
            set { _RetFlight = value; }
        }

        DateTime? _RetDate;
        public DateTime? RetDate
        {
            get { return _RetDate; }
            set { _RetDate = value; }
        }

        int? _RetSeat;
        public int? RetSeat
        {
            get { return _RetSeat; }
            set { _RetSeat = value; }
        }
    }

    public class VisaFollowUpRecord
    {
        public VisaFollowUpRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        int? _ServiceID;
        public int? ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        DateTime? _CreateDate;
        public DateTime? CreateDate
        {
            get { return _CreateDate; }
            set { _CreateDate = value; }
        }

        Int16? _CustTake;
        public Int16? CustTake
        {
            get { return _CustTake; }
            set { _CustTake = value; }
        }

        DateTime? _CustTakeDate;
        public DateTime? CustTakeDate
        {
            get { return _CustTakeDate; }
            set { _CustTakeDate = value; }
        }

        string _CustTakeUser;
        public string CustTakeUser
        {
            get { return _CustTakeUser; }
            set { _CustTakeUser = value; }
        }

        Int16? _EmbGive;
        public Int16? EmbGive
        {
            get { return _EmbGive; }
            set { _EmbGive = value; }
        }

        DateTime? _EmbGiveDate;
        public DateTime? EmbGiveDate
        {
            get { return _EmbGiveDate; }
            set { _EmbGiveDate = value; }
        }

        string _EmbGiveUser;
        public string EmbGiveUser
        {
            get { return _EmbGiveUser; }
            set { _EmbGiveUser = value; }
        }

        Int16? _EmbTake;
        public Int16? EmbTake
        {
            get { return _EmbTake; }
            set { _EmbTake = value; }
        }

        DateTime? _EmbTakeDate;
        public DateTime? EmbTakeDate
        {
            get { return _EmbTakeDate; }
            set { _EmbTakeDate = value; }
        }

        string _EmbTakeUser;
        public string EmbTakeUser
        {
            get { return _EmbTakeUser; }
            set { _EmbTakeUser = value; }
        }

        Int16? _CustGive;
        public Int16? CustGive
        {
            get { return _CustGive; }
            set { _CustGive = value; }
        }

        DateTime? _CustGiveDate;
        public DateTime? CustGiveDate
        {
            get { return _CustGiveDate; }
            set { _CustGiveDate = value; }
        }

        string _CustGiveUser;
        public string CustGiveUser
        {
            get { return _CustGiveUser; }
            set { _CustGiveUser = value; }
        }

        Int16? _Status;
        public Int16? Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        Int16? _LastPosition;
        public Int16? LastPosition
        {
            get { return _LastPosition; }
            set { _LastPosition = value; }
        }

        string _Comment;
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }


    }

    public class ReservationPaymentInfo
    {
        public ReservationPaymentInfo()
        {
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        Int16? _PaymentStat;
        public Int16? PaymentStat
        {
            get { return _PaymentStat; }
            set { _PaymentStat = value; }
        }

        Int16? _ResStatus;
        public Int16? ResStatus
        {
            get { return _ResStatus; }
            set { _ResStatus = value; }
        }

        decimal? _PasPayable;
        public decimal? PasPayable
        {
            get { return _PasPayable; }
            set { _PasPayable = value; }
        }

        decimal? _Balance;
        public decimal? Balance
        {
            get { return _Balance; }
            set { _Balance = value; }
        }

        string _AgencyUser;
        public string AgencyUser
        {
            get { return _AgencyUser; }
            set { _AgencyUser = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

    }

    public class ResChgFeePrice
    {
        public ResChgFeePrice()
        {
        }

        decimal? _ChgAmount;
        public decimal? ChgAmount
        {
            get { return _ChgAmount; }
            set { _ChgAmount = value; }
        }

        string _ChgCur;
        public string ChgCur
        {
            get { return _ChgCur; }
            set { _ChgCur = value; }
        }

        decimal? _ChgPer;
        public decimal? ChgPer
        {
            get { return _ChgPer; }
            set { _ChgPer = value; }
        }

        string _ApplyType;
        public string ApplyType
        {
            get { return _ApplyType; }
            set { _ApplyType = value; }
        }
    }

    public enum ControlSection : short { General = 0, Header = 1, Customers = 2, Services = 3, ExtraServices = 4 }
    public class LastControlErrorRecord
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public bool ReCalc { get; set; }
        public ControlSection Section { get; set; }
        public LastControlErrorRecord()
        {
            this.ReCalc = false;
            this.Section = ControlSection.General;
        }
    }

    [Serializable]
    public class CustControlErrorRecord
    {
        public CustControlErrorRecord()
        {
        }

        Int16 _ErrorCode;
        public Int16 ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }

        string _ErrorMessage;
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

    }

    [Serializable]
    public class ServiceExtMarOpt
    {
        public ServiceExtMarOpt()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int? _ServiceID;
        public int? ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        string _ServiceCode;
        public string ServiceCode
        {
            get { return _ServiceCode; }
            set { _ServiceCode = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _WebSale;
        public string WebSale
        {
            get { return _WebSale; }
            set { _WebSale = value; }
        }

        string _TextType;
        public string TextType
        {
            get { return _TextType; }
            set { _TextType = value; }
        }

        string _DescText;
        public string DescText
        {
            get { return _DescText; }
            set { _DescText = value; }
        }

        bool? _ShowInResDetB2B;
        public bool? ShowInResDetB2B
        {
            get { return _ShowInResDetB2B; }
            set { _ShowInResDetB2B = value; }
        }

        bool? _ShowInResDetB2C;
        public bool? ShowInResDetB2C
        {
            get { return _ShowInResDetB2C; }
            set { _ShowInResDetB2C = value; }
        }

        bool? _ShowInResDet;
        public bool? ShowInResDet
        {
            get { return _ShowInResDet; }
            set { _ShowInResDet = value; }
        }

    }

    [Serializable]
    public class VisaFormData
    {
        public VisaFormData()
        {
        }

        string _fieldName;
        public string FieldName
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }

        string _value;
        public string value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    [Serializable()]
    public class PytonG7PaxDescription
    {
        public string PaxID { get; set; }
        public string GivenNames { get; set; }
        public string Initials { get; set; }
        public string Infix { get; set; }
        public string Name { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public PytonG7BaseGender Gender { get; set; }
        public string Nationality { get; set; }

        public PytonG7PaxDescription()
        {
        }
    }

    /// <summary>
    /// sample -> [{"Checked":true,"Market":"market","PassSerie":true,"PassNo":true,"PassIssueDate":true,"PassExpDate":true,"PassGiven":true}]
    /// </summary>
    [Serializable()]
    public class PassControlV2
    {
        public bool Checked { get; set; }
        public string Market { get; set; }
        public bool PassSerie { get; set; }
        public bool PassNo { get; set; }
        public bool PassIssueDate { get; set; }
        public bool PassExpDate { get; set; }
        public bool PassGiven { get; set; }

        public PassControlV2()
        {
            this.Checked = false;
        }
    }

    public class ExtSerAllotDef
    {
        public int? RecID { get; set; }
        public string Market { get; set; }
        public string Service { get; set; }
        public string ExtSer { get; set; }
        public string Supplier { get; set; }
        public DateTime? BegDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Int16? Unit { get; set; }
        public Int16? RelDay { get; set; }
        public string ServiceItem { get; set; }
        public ExtSerAllotDef()
        {
        }
    }

    public class ResFixNote
    {
        public int? RecID { get; set; }
        public string Market { get; set; }
        public string Description { get; set; }
        public string DescriptionL { get; set; }
        public ResFixNote()
        {
        }
    }

    public class ResGroup
    {
        public string Agency { get; set; }
        public int? GroupNo { get; set; }
        public DateTime? CheckIn { get; set; }
        public string CheckInUnix { get; set; }
        public Int16? Days { get; set; }
        public string FullName { get; set; }
        public ResGroup()
        {
        }
    }

    public class RRFCriteria
    {
        public Int16? ResType { get; set; }
        public int? GroupNo { get; set; }
        public DateTime? CheckIn { get; set; }
        public Int16? Night { get; set; }
        public Int16? Adult { get; set; }
        public Int16? Child { get; set; }
        public Int16? Infant { get; set; }
        public string LeaderSurname { get; set; }
        public string LeaderName { get; set; }
        public string HolPack { get; set; }
        public int? DepCity { get; set; }
        public int? ArrCity { get; set; }
        public string ResNote { get; set; }
        public string Nationality { get; set; }
        public string VisaSupplier { get; set; }
        public string EntrancePort { get; set; }
        public RRFCriteria()
        {
            this.Adult = 2;
            this.Child = 0;
            this.Infant = 0;
        }
    }

    public class VehicleCategoryRecord
    {
        public int? RecID { get; set; }
        public string Name { get; set; }
        public VehicleCategoryRecord()
        {
        }
    }
}
