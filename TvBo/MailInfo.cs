﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace TvBo
{
    public class MailInfo
    {
        public MailInfo()
        {
            this.MailTo = new List<MailAddress>();
            this.CC = new List<MailAddress>();
            this.BCC = new List<MailAddress>();
            this.Attachments = new List<MailAttachment>();
        }

        public MailInfo(string pSubject, String pBody)
            : this()
        {
            this.Subject = pSubject;
            this.Body = pBody;
        }

        public MailInfo(string pSubject, String pBody, string pMailFrom, string pMailTo)
            : this(pSubject, pBody)
        {
            this.MailFrom = new MailAddress(pMailFrom);
            this.MailTo.Add(new MailAddress(pMailTo));
        }

        public MailAddress MailFrom;
        public List<MailAddress> MailTo;
        public List<MailAddress> CC;
        public List<MailAddress> BCC;
        public string Subject;
        public string Body;
        public List<MailAttachment> Attachments;

    }

    class MailSetting
    {
        public MailSetting()
        {

        }

        public MailSetting(string pSmtpUserName, string pSmtpPassword, string pSmtpDisplayName, string pSmtpServer, int pSmtpPort, bool pEnableSSL)
            : this()
        {
            this.SmtpUserName = pSmtpUserName;
            this.SmtpPassword = pSmtpPassword;
            this.SmtpDisplayName = pSmtpDisplayName;
            this.SmtpServer = pSmtpServer;
            this.SmtpPort = pSmtpPort;
            this.EnableSSL = pEnableSSL;
        }

        public string SmtpUserName;
        public string SmtpPassword;
        public string SmtpDisplayName;
        public string SmtpServer;
        public int SmtpPort;
        public bool EnableSSL;

    }

    public class MailAttachment
    {
        public MailAttachment()
        {
        }

        public MailAttachment(byte[] pBinary, string pName)
            : this()
        {
            this.Initialize(pBinary, pName);
        }

        public MailAttachment(string pFile)
            : this()
        {
            this.Initialize(System.IO.File.ReadAllBytes(pFile), pFile);
        }

        private void Initialize(byte[] pBinary, string pName)
        {
            this.Binary = pBinary;
            this.Name = System.IO.Path.GetFileName(pName).ToLowerInvariant();
        }

        public byte[] Binary;
        public string Name;

    }

    class MailSender : IDisposable
    {

        public MailSender(MailInfo pMailInfo)
        {
            this.Info = pMailInfo;
        }

        public MailSender(MailInfo pMailInfo, MailSetting pMailSetting, string ConnectionString)
            : this(pMailInfo)
        {
            this.Setting = pMailSetting;
            this.ConnectionString = ConnectionString;
        }

        public MailInfo Info;
        public MailSetting Setting;
        public string ConnectionString;
        private SqlConnection con = null;
        private SqlTransaction trn = null;

        public void Send()
        {
            con = new SqlConnection(this.ConnectionString);
            con.Open();
            trn = con.BeginTransaction();
            try
            {
                //Setting Migration
                MigrateSettings();
                //Insert Mail Queue
                long MailQueueRecId = InsertMailQueue();
                //Add Attachments
                AddAttachments(MailQueueRecId);
                //Commit
                trn.Commit();
            }
            catch (Exception)
            {
                trn.Rollback();
                throw;
            };
            con.Close();
        }

        private void MigrateSettings()
        {
            if (this.Setting != null)
            {
                this.Info.MailFrom = new MailAddress(this.Setting.SmtpUserName);

                StringBuilder tsql = new StringBuilder();
                tsql.AppendLine("DECLARE @SmtpUserName	    AS VARCHAR(100); SET @SmtpUserName		= '" + this.Setting.SmtpUserName + "';");
                tsql.AppendLine("DECLARE @SmtpPassword	    AS VARCHAR(100); SET @SmtpPassword		= '" + this.Setting.SmtpPassword + "';");
                tsql.AppendLine("DECLARE @SmtpDisplayName   AS VARCHAR(100); SET @SmtpDisplayName   = '" + this.Setting.SmtpDisplayName + "';");
                tsql.AppendLine("DECLARE @SmtpServer		AS VARCHAR(100); SET @SmtpServer		= '" + this.Setting.SmtpServer + "';");
                tsql.AppendLine("DECLARE @SmtpPort		    AS INT;			 SET @SmtpPort			=  " + this.Setting.SmtpPort.ToString() + ";");
                tsql.AppendLine("DECLARE @EnableSSL		    AS BIT;			 SET @EnableSSL			=  " + (this.Setting.EnableSSL ? 1 : 0) + ";");

                tsql.AppendLine("UPDATE MS SET MS.SmtpPassword = @SmtpPassword, MS.SmtpDisplayName = @SmtpDisplayName, MS.SmtpServer = @SmtpServer, MS.SmtpPort = @SmtpPort,MS.EnableSSL=@EnableSSL");
                tsql.AppendLine("FROM MailSettings AS MS WITH (NOLOCK) WHERE MS.SmtpUserName = @SmtpUserName;");

                tsql.AppendLine("IF (@@ROWCOUNT = 0) BEGIN");
                tsql.AppendLine("	INSERT INTO MailSettings (SmtpUserName, SmtpPassword, SmtpDisplayName, SmtpServer, SmtpPort,EnableSSL)");
                tsql.AppendLine("	VALUES (@SmtpUserName, @SmtpPassword, @SmtpDisplayName, @SmtpServer, @SmtpPort,@EnableSSL);");
                tsql.AppendLine("END;");

                using (SqlCommand cmd = new SqlCommand(tsql.ToString(), this.con) { Transaction = trn })
                {
                    cmd.ExecuteNonQuery();
                };
            };
        }

        private long InsertMailQueue()
        {
            string tsql = "INSERT INTO MailQueue ([MailFrom], [MailTo], [CC], [BCC], [Subject], [Body]) VALUES (@MailFrom, @MailTo, @CC, @BCC, @Subject, @Body); SELECT @@IDENTITY;";

            using (SqlCommand cmd = new SqlCommand(tsql, this.con) { Transaction = trn })
            {
                cmd.Parameters.Add(new SqlParameter("@MailFrom", this.Info.MailFrom.Address));
                cmd.Parameters.Add(new SqlParameter("@MailTo", String.Join(";", this.Info.MailTo.Select(mt => mt.Address).ToArray())));
                cmd.Parameters.Add(new SqlParameter("@CC", this.Info.CC.Count > 0 ? String.Join(";", this.Info.CC.Select(mt => mt.Address).ToArray()) : Convert.DBNull));
                cmd.Parameters.Add(new SqlParameter("@BCC", this.Info.BCC.Count > 0 ? String.Join(";", this.Info.BCC.Select(mt => mt.Address).ToArray()) : Convert.DBNull));
                cmd.Parameters.Add(new SqlParameter("@Subject", this.Info.Subject));
                cmd.Parameters.Add(new SqlParameter("@Body", this.Info.Body));
                return Convert.ToInt64((decimal)cmd.ExecuteScalar());
            };
        }

        private void AddAttachments(long pMailQueueRecId)
        {
            if (this.Info.Attachments != null)
            {
                foreach (MailAttachment atch in this.Info.Attachments)
                {
                    string tsql = "INSERT INTO MailAttachments (MailQueueRecId, Data, Name) VALUES (@MailQueueRecId, @Data, @Name);";
                    using (SqlCommand cmd = new SqlCommand(tsql, this.con) { Transaction = trn })
                    {
                        cmd.Parameters.Add(new SqlParameter("@MailQueueRecId", pMailQueueRecId));
                        cmd.Parameters.Add(new SqlParameter("@Data", atch.Binary));
                        cmd.Parameters.Add(new SqlParameter("@Name", atch.Name));
                        cmd.ExecuteNonQuery();
                    };
                };
            };
        }

        public void Dispose()
        {
            trn.Dispose();
            con.Dispose();
        }
    }

    public class SendMail
    {
        internal string ConnectionString = string.Empty;

        public bool AsyncMailSender(AsyncMailSenderParams param)
        {
            bool isComplete = false;
            MailInfo mo = new MailInfo(param.Subject, param.Body);
            mo.MailFrom = new MailAddress(param.SenderMail);
            if (!param.ToAddress.Contains(";"))
                mo.MailTo.Add(new MailAddress(param.ToAddress));
            else
            {
                string[] ToAddresses = param.ToAddress.Split(';');
                for (int i = 0; i < ToAddresses.Length; i++)
                    mo.MailTo.Add(new MailAddress(ToAddresses[i]));
            }
            string AutoBCC = System.Configuration.ConfigurationManager.AppSettings["AutoBCC"] ?? string.Empty;
            if (!string.IsNullOrEmpty(AutoBCC))
            {
                if (!AutoBCC.Contains(";"))
                    mo.BCC.Add(new MailAddress(AutoBCC));
                else
                {
                    string[] ToAddresses = AutoBCC.Split(';');
                    for (int i = 0; i < ToAddresses.Length; i++)
                        mo.BCC.Add(new MailAddress(ToAddresses[i]));
                }
            }
            if (!String.IsNullOrEmpty(param.FileName))
            {
                if (param.FileBinary != null && param.FileBinary.Length > 0)
                    mo.Attachments.Add(new MailAttachment(param.FileBinary, param.FileName));
                else
                    mo.Attachments.Add(new MailAttachment(param.FileName));

            }
            MailSetting ms = new MailSetting(param.SenderMail, param.SenderPassword, param.SenderMail, param.SMTPServer, param.Port ?? 25, (param.EnableSSL ?? false));
            string MailSenderServerConnectionStr = System.Configuration.ConfigurationManager.AppSettings["MailSenderDB"] ?? this.ConnectionString;
            using (var mm = new MailSender(mo, ms, MailSenderServerConnectionStr))
            {
                try
                {
                    mm.Send();
                    isComplete = true;
                }
                catch (Exception Ex)
                {
                    isComplete = false;
                }
            };
            return isComplete;
        }

        public bool MailSender(AsyncMailSenderParams param)
        {
            bool isComplete = false;
            MailMessage message = new MailMessage();
            if (!param.ToAddress.Contains(";"))
                message.To.Add(new MailAddress(param.ToAddress));
            else
            {
                string[] ToAddresses = param.ToAddress.Split(';');
                for (int i = 0; i < ToAddresses.Length; i++)
                    message.To.Add(new MailAddress(ToAddresses[i]));
            }
                        
            if (!String.IsNullOrEmpty(param.FileName))
            {
                if (param.FileBinary != null && param.FileBinary.Length > 0)
                    message.Attachments.Add(new Attachment(new MemoryStream(param.FileBinary), param.FileName));
                else
                    message.Attachments.Add(new Attachment(param.FileName));

            }
            message.From = new MailAddress(param.SenderMail);            
            message.Subject = param.Subject;
            message.IsBodyHtml = true;
            message.Body = param.Body;
            SmtpClient smtp = new SmtpClient(param.SMTPServer);
            smtp.UseDefaultCredentials =false;
            if (!string.IsNullOrEmpty(param.SenderPassword))
                smtp.Credentials = new NetworkCredential(param.SenderMail, param.SenderPassword);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Port = param.Port ?? 25;
            smtp.EnableSsl = param.EnableSSL ?? false;
            try
            {
                smtp.Send(message);
                isComplete = true;
            }
            catch (Exception ex)
            {
                isComplete = false;
            }
            finally
            {
                message.Dispose();
            }
            return isComplete;
        }
    }

    public class AsyncMailSenderParams
    {
        public string SenderMail { get; set; }
        public string SenderPassword { get; set; }
        public string SMTPServer { get; set; }
        public int? Port { get; set; }
        public bool? EnableSSL { get; set; }
        public string DisplayName { get; set; }
        public string ToAddress { get; set; }
        public string FileName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public byte[] FileBinary { get; set; }
    }

}
