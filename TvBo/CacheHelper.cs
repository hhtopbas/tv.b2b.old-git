﻿using System;
using System.Web;

public static class CacheHelper
{
   /// <summary>
   /// 
   /// </summary>
   /// <typeparam name="T"></typeparam>
   /// <param name="o"></param>
   /// <param name="key"></param>
   /// <param name="hours"></param>
   /// <param name="minutes"></param>
    public static void Add<T>(T o, string key,int hours, int minutes)
    {
        // NOTE: Apply expiration parameters as you see fit.
        // I typically pull from configuration file.

        // In this example, I want an absolute
        // timeout so changes will always be reflected
        // at that time. Hence, the NoSlidingExpiration.
        HttpContext.Current.Cache.Insert(
            key,
            o,
            null,
            System.Web.Caching.Cache.NoAbsoluteExpiration,
            new TimeSpan(0, hours, minutes, 0, 0));
        //HttpContext.Current.Cache.Add(
        //   key,
        //   o,
        //   null,
        //   System.Web.Caching.Cache.NoAbsoluteExpiration,
        //   TimeSpan.FromHours(6),
        //   System.Web.Caching.CacheItemPriority.NotRemovable,
        //   null); 
    }
    /// <summary>
    /// Insert value into the cache using
    /// appropriate name/value pairs
    /// </summary>
    /// <typeparam name="T">Type of cached item</typeparam>
    /// <param name="o">Item to be cached</param>
    /// <param name="key">Name of item</param>
    /// <param name="hours">Cache duration</param>
    public static void Add<T>(T o, string key, int hours)
    {
        // NOTE: Apply expiration parameters as you see fit.
        // I typically pull from configuration file.

        // In this example, I want an absolute
        // timeout so changes will always be reflected
        // at that time. Hence, the NoSlidingExpiration.
        HttpContext.Current.Cache.Insert(
            key,
            o,
            null,
            System.Web.Caching.Cache.NoAbsoluteExpiration,
            new TimeSpan(0, hours, 0, 0, 0));
        //HttpContext.Current.Cache.Add(
        //   key,
        //   o,
        //   null,
        //   System.Web.Caching.Cache.NoAbsoluteExpiration,
        //   TimeSpan.FromHours(6),
        //   System.Web.Caching.CacheItemPriority.NotRemovable,
        //   null); 
    }

    /// <summary>
    /// Insert value into the cache using
    /// appropriate name/value pairs
    /// </summary>
    /// <typeparam name="T">Type of cached item</typeparam>
    /// <param name="o">Item to be cached</param>
    /// <param name="key">Name of item</param>    
    public static void Add<T>(T o, string key)
    {
        // NOTE: Apply expiration parameters as you see fit.
        // I typically pull from configuration file.

        // In this example, I want an absolute
        // timeout so changes will always be reflected
        // at that time. Hence, the NoSlidingExpiration.
        HttpContext.Current.Cache.Insert(
            key,
            o,
            null,
            System.Web.Caching.Cache.NoAbsoluteExpiration,
            new TimeSpan(0, 1, 0, 0, 0));
        //HttpContext.Current.Cache.Add(
        //   key,
        //   o,
        //   null,
        //   System.Web.Caching.Cache.NoAbsoluteExpiration,
        //   TimeSpan.FromHours(6),
        //   System.Web.Caching.CacheItemPriority.NotRemovable,
        //   null); 
    }

    /// <summary>
    /// Remove item from cache
    /// </summary>
    /// <param name="key">Name of cached item</param>
    public static void Clear(string key)
    {
        HttpContext.Current.Cache.Remove(key);
    }

    /// <summary>
    /// Check for item in cache
    /// </summary>
    /// <param name="key">Name of cached item</param>
    /// <returns></returns>
    public static bool Exists(string key)
    {
        return HttpContext.Current.Cache[key] != null;
    }

    /// <summary>
    /// Retrieve cached item
    /// </summary>
    /// <typeparam name="T">Type of cached item</typeparam>
    /// <param name="key">Name of cached item</param>
    /// <param name="value">Cached value. Default(T) if item doesn't exist.</param>
    /// <returns>Cached item as type</returns>
    public static bool Get<T>(string key, out T value)
    {
        try
        {
            if (!Exists(key))
            {
                value = default(T);
                return false;
            }
            value = (T)HttpContext.Current.Cache[key];
        }
        catch
        {
            value = default(T);
            return false;
        }

        return true;
    }
}

