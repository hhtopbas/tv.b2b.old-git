﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using TvTools;

namespace TvBo
{
    public class AgencyPaymentMonitors
    {
        public List<paymentMonitorDefaultRecord> getResMonDefaultData(User UserData, ref string errorMsg)
        {
            List<paymentMonitorDefaultRecord> records = new List<paymentMonitorDefaultRecord>();

            string tsql =
@"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

Declare @MainAgency VarChar(10),@Market VarChar(10),@Top1 int
Select @Top1=1
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID,Country,City,Town,Village,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name)
Into #Location
From Location
";
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                tsql += 
@"  
Select @MainAgency=MainOffice,@Market=O.Market
From Agency A
Join Office O ON O.Code=A.OprOffice
Where A.Code=@Agency 
if (@MainAgency<>@Agency) 
   Set @MainAgency=''                         
";
            else
                tsql += 
@"  
Select @MainAgency=Case When AgencyType=0 Then @Agency Else (Case when AgencyType<>2 Then MainOffice Else @Agency End) End,@Market=O.Market
From Agency A (NOLOCK) 
Join Office O (NOLOCK) ON O.Code=A.OprOffice
Where A.Code=@Agency 
";

            tsql +=
@"  
SELECT Distinct RM.Agency, 
AgencyName=A.Name,AgencyNameL=isnull(dbo.FindLocalName(A.NameLID, @Market), A.Name),
  AgencyUser=isnull(RM.AgencyUser,''),AgencyUserName=isnull(Au.Name,'') ,AgencyUserNameL=isnull(Au.NameL, '') ,
  HolPack=isnull(RM.HolPack,''),HolPackName=isnull(Hp.Name,''),HolPackNameL=isnull(Hp.NameL,''),
  RM.DepCity,DepCityName=DepL.Name,DepCityNameL=DepL.NameL,
  RM.ArrCity,ArrCityName=ArrL.Name,ArrCityNameL=ArrL.NameL,  
  RM.SaleCur
From ResMain RM
Join Agency A ON A.Code=RM.Agency
Left Join #Location DepL ON DepL.RecID=RM.DepCity
Left Join #Location ArrL ON ArrL.RecID=RM.ArrCity
OUTER APPLY
(
	Select Agency,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name) 
	From AgencyUser 
	Where Agency=RM.Agency And Code=RM.AgencyUser
) Au
OUTER APPLY
(
  Select Code,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name) 
  From HolPack
  Where Code=RM.HolPack
) Hp
Where 
";
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                tsql += 
@"  
  ((Case When @MainAgency<>'' Then @MainAgency Else @Agency End)=(Select Agency=Case When AgencyType=0 then Code else MainOffice end From Agency Where Code=RM.Agency) 
";
            else 
                tsql += 
@"  
  (@MainAgency=(Select Agency=(case when AgencyType=0 Then Code Else MainOffice End) From Agency Where Code=RM.Agency) 
";

            if (UserData.Aggregator && !string.IsNullOrEmpty(UserData.MerlinxID) && UserData.MainAgency)
                tsql += 
@" 
  Or isnull(RM.MerlinxID,'')<>'' 
";

            if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
                if (string.IsNullOrEmpty(UserData.UserID))
                    tsql += 
@"
  And @ShowAllRes='Y' 
";
                else tsql += string.Format(
@" 
  And (@ShowAllRes='Y' Or AgencyUser='{0}') 
", UserData.UserID);

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) && UserData.ShowAllRes && !string.Equals(UserData.MainOffice, UserData.AgencyID))
                tsql += string.Format(
@" 
  Or RM.Agency in (Select Code From Agency (NOLOCK) Where Code='{0}' OR MainOffice='{0}'))
", UserData.MainOffice);
            else
                tsql += 
@"
)
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Agency", System.Data.DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "ShowAllRes", System.Data.DbType.String, UserData.ShowAllRes ? "Y" : "N");
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        paymentMonitorDefaultRecord rec = new paymentMonitorDefaultRecord();
                        rec.Agency = Conversion.getStrOrNull(oReader["Agency"]);
                        rec.AgencyName = UserData.EqMarketLang ? Conversion.getStrOrNull(oReader["AgencyNameL"]) : Conversion.getStrOrNull(oReader["AgencyName"]);
                        rec.AgencyUser = Conversion.getStrOrNull(oReader["AgencyUser"]);
                        rec.AgencyUserName = UserData.EqMarketLang ? Conversion.getStrOrNull(oReader["AgencyUserNameL"]) : Conversion.getStrOrNull(oReader["AgencyUserName"]);
                        rec.ArrCity = (int)oReader["ArrCity"];
                        rec.ArrCityName = UserData.EqMarketLang ? Conversion.getStrOrNull(oReader["ArrCityNameL"]) : Conversion.getStrOrNull(oReader["ArrCityName"]);
                        rec.DepCity = (int)oReader["DepCity"];
                        rec.DepCityName = UserData.EqMarketLang ? Conversion.getStrOrNull(oReader["DepCityNameL"]) : Conversion.getStrOrNull(oReader["DepCityName"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<listString> getHolPacks(List<paymentMonitorDefaultRecord> data)
        {
            return null;
        }

        public List<listString> getAgencyOffices(List<paymentMonitorDefaultRecord> data)
        {
            var Result = from rst in data.AsEnumerable()
                         group rst by new { Code = rst.Agency.ToUpper(), Name = rst.AgencyName } into r
                         select new listString { Code = r.Key.Code, Name = r.Key.Name };
            return Result.ToList<listString>();
        }

        public List<listString> getAgencyUser(List<paymentMonitorDefaultRecord> data)
        {
            var Result = from rst in data.AsEnumerable()
                         group rst by new { Code = rst.AgencyUser.ToUpper(), Name = rst.AgencyUserName, Agency = rst.Agency } into r
                         select new listString { Code = r.Key.Code, Name = r.Key.Name, ParentCode = r.Key.Agency };
            return Result.ToList<listString>();
        }

        public List<listString> getArrCity(List<paymentMonitorDefaultRecord> data)
        {
            var Result = from rst in data.AsEnumerable()
                         group rst by new { Code = rst.ArrCity, Name = rst.ArrCityName } into r
                         select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
            return Result.ToList<listString>();
        }

        public List<listString> getDepCity(List<paymentMonitorDefaultRecord> data)
        {
            var Result = from rst in data.AsEnumerable()
                         group rst by new { Code = rst.DepCity, Name = rst.DepCityName } into r
                         select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
            return Result.ToList<listString>();
        }

        public List<AgencyPaymentMonitorFields> getAgencyPaymentMonitorData_(User UserData, string AgencyUser, string AgencyOffice,
                            int? ArrCity, int? DepCity, DateTime? BegDateFirst, DateTime? BegDateLast,
                            DateTime? ResDateFirst, DateTime? ResDateLast, DateTime? DueDate, string fltBalance, string ResStatusStr,
                            string ConfirmStatusStr, bool IncludeDueDate, string SurName, string Name,
                            ref string errorMsg)
        {

            List<AgencyPaymentMonitorFields> records = new List<AgencyPaymentMonitorFields>();

            string tsql = @"SELECT *, Balance=AgencyPayable2-AgencyPayment 
                        FROM
                        (	SELECT	R.ResNo, R.Office, R.Agency, R.AgencyUser,
                                    R.DepCity, 
                                    DepCityName=(Select Name From Location (NOLOCK) Where RecID = R.DepCity), 
                                    DepCityNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = R.DepCity), 
                                    R.SaleCur, 
                                    Surname = (Select Top 1 Surname From ResCust C where C.ResNo = R.ResNo and C.Leader = 'Y'),
                                    Name = (Select Top 1 Name From ResCust C where C.ResNo = R.ResNo and C.Leader = 'Y'),
                                    Leader = (Select Top 1 Surname + ' ' + Name From ResCust C where C.ResNo = R.ResNo and C.Leader = 'Y'),
		                            MainAgency = (	SELECT Agency = CASE WHEN AgencyType = 0 THEN R.Agency ELSE MainOffice END 
						                            FROM Agency (NOLOCK) WHERE Code = R.Agency),      
		                            R.ArrCity, 
                                    ArrCityName=(Select Name From Location (NOLOCK) Where RecID = R.ArrCity), 
                                    ArrCityNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = R.ArrCity), 
                                    R.BegDate, R.EndDate, R.Days, R.ResStat, R.ConfStat, R.ResDate, R.Adult, R.Child,      
		                            R.SalePrice, R.AgencyCom, AgencyDisPasPer, AgencyDisPasVal,
                                    R.DisCount, R.BrokerCom, R.UserBonus, R.AgencyBonus, R.AgencySupDis, R.AgencyComSup, 
		                            R.EBAgency, isnull(R.AgencyPayment,0) AgencyPayment, 
                                    AgencyPayable = Case When dbo.ufn_AgencyResPayRule(R.Agency, R.PLMarket)=0 then IsNull(R.AgencyPayable,0) else IsNull(R.PasAmount,0) end,
		                            AgencyPayable2 = isnull(P.Amount, 0), R.EBPas,
                                    OfficeName=(Select Name From Agency (NOLOCK) Where Code = R.Agency),
                                    OfficeNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Agency (NOLOCK) Where Code = R.Agency),
                                    ResStatStr = Case R.ResStat 
                                                       When 0 Then 'New' 
                                                       When 1 Then 'Modified' 
                                                       When 2 Then 'Cancel' 
                                                       When 3 Then 'Cancel X' 
                                                       When 9 Then 'Draft' Else '' End, 
                                    ConfStatStr = Case R.ConfStat 
                                                       When 0 Then 'Request' 
                                                       When 1 Then 'OK' 
                                                       When 2 Then 'Not Confirm' 
                                                       When 3 Then 'No Show' Else '' End,
                                    R.OptDate                                  
                            FROM ResMain R (NOLOCK)
                            LEFT JOIN (	SELECT ResNo, Sum(isnull(Amount,0)) Amount, Cur
			                            FROM ResPayPlan ";
            if (IncludeDueDate)
                tsql += @"
			                            WHERE DueDate <= dbo.DateOnly(@DueDate) ";
            else
                tsql += @"
			                            WHERE DueDate < dbo.DateOnly(@DueDate) ";

            tsql += @"
			                            GROUP BY ResNo, Cur) P ON P.ResNo = R.ResNo      
                            WHERE 
	                            isnull(R.Ballon, '') <> '' 
                            AND R.Operator = @Operator
                        ) X
                        OUTER APPLY
                        (
	                        Select Agency= Case When 
						                        AgencyType=0 then X.Agency 
					                        Else 
					                           (Case When AgencyType<>2 Then MainOffice Else Code End)
					                        End,
		                           AgencyType				
	                        From Agency (NOLOCK) 
	                        Where Code=X.Agency
                        ) A
                        WHERE
                            @MainAgency=A.Agency
                        And (@ArrCity is null or X.ArrCity = @ArrCity) 
                        And (@DepCity is null or X.DepCity = @DepCity) 
                        And (@BegDate1 is null or X.BegDate >= @BegDate1) 
                        And (@BegDate2 is null or X.BegDate <= @BegDate2) 
                        And (@ResDate1 is null or X.ResDate >= @ResDate1) 
                        And (@ResDate2 is null or X.ResDate <= @ResDate2) 
                        And (A.AgencyType=2 Or (isnull(@AgencyOffice,'')='' or X.Agency = @AgencyOffice))
                        And (isnull(@AgencyUser,'')='' Or X.AgencyUser = @AgencyUser)
                        And (isnull(@SurName,'')='' Or X.Surname Like @SurName)
                        And (isnull(@Name,'')='' Or X.Name Like @Name) ";

            if (fltBalance != string.Empty)
            {
                var queryBalance = from q in fltBalance.Split(',').AsEnumerable()
                                   select new { s = Convert.ToInt32(q) };
                if (queryBalance.Count() > 0)
                {
                    string tmptsql = string.Empty;
                    if (queryBalance.Where(w => w.s == 2).Count() > 0)
                        tmptsql += "OR (X.AgencyPayable2 - X.AgencyPayment) > 0 ";

                    if (queryBalance.Where(w => w.s == 3).Count() > 0)
                        tmptsql += "OR (X.AgencyPayable2 - X.AgencyPayment) < 0 ";

                    if (queryBalance.Where(w => w.s == 4).Count() > 0)
                        tmptsql += "OR (X.AgencyPayable2 - X.AgencyPayment) = 0 ";

                    if (queryBalance.Where(w => w.s == 1).Count() > 0)
                    {
                    }
                    else
                        if (tmptsql.Length > 0)
                        {
                            tmptsql = tmptsql.Remove(0, 3);
                            tmptsql = " AND (" + tmptsql + ") ";
                            tsql += tmptsql;
                        }
                }
            }
            if (ResStatusStr != "")
                tsql = tsql + " And X.ResStat in(" + ResStatusStr + ") ";
            if (ConfirmStatusStr != "")
                tsql = tsql + " And X.ConfStat in (" + ConfirmStatusStr + ") ";

            tsql += " Order By X.ResNo Desc ";

            DateTime minDate = new DateTime(1950, 1, 1);
            DateTime maxDate = new DateTime(2099, 12, 31);
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "AgencyOffice", DbType.String, string.IsNullOrEmpty(AgencyOffice) ? UserData.MainOffice : AgencyOffice);
                db.AddInParameter(dbCommand, "AgencyUser", DbType.String, !UserData.ShowAllRes ? UserData.UserID : AgencyUser);
                db.AddInParameter(dbCommand, "SurName", DbType.String, SurName);
                db.AddInParameter(dbCommand, "Name", DbType.String, Name);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, ArrCity);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, DepCity);
                db.AddInParameter(dbCommand, "BegDate1", DbType.DateTime, BegDateFirst.HasValue ? BegDateFirst.Value.ToLocalTime() : minDate);
                db.AddInParameter(dbCommand, "BegDate2", DbType.DateTime, BegDateLast.HasValue ? BegDateLast.Value.ToLocalTime() : maxDate);
                db.AddInParameter(dbCommand, "ResDate1", DbType.DateTime, ResDateFirst.HasValue ? ResDateFirst.Value.ToLocalTime() : minDate);
                db.AddInParameter(dbCommand, "ResDate2", DbType.DateTime, ResDateLast.HasValue ? ResDateLast.Value.ToLocalTime() : maxDate);
                db.AddInParameter(dbCommand, "DueDate", DbType.DateTime, DueDate.HasValue ? DueDate.Value.ToLocalTime() : DateTime.Today);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                string mainAgency = string.Empty;
                if (UserData.MasterAgency)
                    mainAgency = UserData.AgencyID;
                else mainAgency = !UserData.ShowAllRes ? UserData.AgencyID : (string.Equals(UserData.MainOffice, UserData.AgencyID) ? UserData.AgencyID : (UserData.AgencyType != 2 ? UserData.MainOffice : UserData.AgencyID));
                db.AddInParameter(dbCommand, "MainAgency", DbType.String, mainAgency);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    int cnt = 0;
                    while (reader.Read())
                    {
                        ++cnt;
                        AgencyPaymentMonitorFields rec = new AgencyPaymentMonitorFields();
                        rec.RecID = cnt;
                        rec.Adult = Conversion.getInt16OrNull(reader["Adult"]);
                        rec.Agency = Conversion.getStrOrNull(reader["Agency"]);
                        rec.AgencyBonus = Conversion.getDecimalOrNull(reader["AgencyBonus"]);
                        rec.AgencyCom = Conversion.getDecimalOrNull(reader["AgencyCom"]);
                        rec.DiscountFromCom = Conversion.getDecimalOrNull(reader["AgencyDisPasVal"]);
                        rec.AgencyComSup = Conversion.getDecimalOrNull(reader["AgencyComSup"]);
                        rec.AgencyPayable = Conversion.getDecimalOrNull(reader["AgencyPayable"]);
                        rec.AgencyPayable2 = Conversion.getDecimalOrNull(reader["AgencyPayable2"]);
                        rec.AgencyPayment = Conversion.getDecimalOrNull(reader["AgencyPayment"]);
                        rec.AgencySupDis = Conversion.getDecimalOrNull(reader["AgencySupDis"]);
                        rec.AgencyUser = Conversion.getStrOrNull(reader["AgencyUser"]);
                        rec.ArrCity = Conversion.getInt32OrNull(reader["ArrCity"]);
                        rec.ArrCityName = UserData.EqMarketLang ? Conversion.getStrOrNull(reader["ArrCityNameL"]) : Conversion.getStrOrNull(reader["ArrCityName"]);
                        rec.Balance = Conversion.getDecimalOrNull(reader["Balance"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(reader["BegDate"]);
                        rec.BrokerCom = Conversion.getDecimalOrNull(reader["BrokerCom"]);
                        rec.Child = Conversion.getInt16OrNull(reader["Child"]);
                        rec.ConfStat = Conversion.getInt16OrNull(reader["ConfStat"]);
                        rec.ConfStatStr = Conversion.getStrOrNull(reader["ConfStatStr"]);
                        rec.Days = Conversion.getInt16OrNull(reader["Days"]);
                        rec.DepCity = Conversion.getInt32OrNull(reader["DepCity"]);
                        rec.DepCityName = UserData.EqMarketLang ? Conversion.getStrOrNull(reader["DepCityNameL"]) : Conversion.getStrOrNull(reader["DepCityName"]);
                        rec.Discount = Conversion.getDecimalOrNull(reader["Discount"]);
                        rec.EBAgency = Conversion.getDecimalOrNull(reader["EBAgency"]);
                        rec.EBPas = Conversion.getDecimalOrNull(reader["EBPas"]);
                        rec.EndDate = Conversion.getDateTimeOrNull(reader["EndDate"]);
                        rec.Leader = Conversion.getStrOrNull(reader["Leader"]);
                        rec.MainAgency = Conversion.getStrOrNull(reader["MainAgency"]);
                        rec.Name = Conversion.getStrOrNull(reader["Name"]);
                        rec.Office = Conversion.getStrOrNull(reader["Office"]);
                        rec.OfficeName = UserData.EqMarketLang ? Conversion.getStrOrNull(reader["OfficeNameL"]) : Conversion.getStrOrNull(reader["OfficeName"]);
                        rec.OptDate = Conversion.getDateTimeOrNull(reader["OptDate"]);
                        rec.ResDate = Conversion.getDateTimeOrNull(reader["ResDate"]);
                        rec.ResNo = Conversion.getStrOrNull(reader["ResNo"]);
                        rec.ResStat = Conversion.getInt16OrNull(reader["ResStat"]);
                        rec.ResStatStr = Conversion.getStrOrNull(reader["ResStatStr"]);
                        rec.SaleCur = Conversion.getStrOrNull(reader["SaleCur"]);
                        rec.SalePrice = Conversion.getDecimalOrNull(reader["SalePrice"]);
                        rec.Surname = Conversion.getStrOrNull(reader["Surname"]);
                        rec.UserBonus = Conversion.getDecimalOrNull(reader["UserBonus"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AgencyPaymentMonitorFields> getAgencyPaymentMonitorData(User UserData, string AgencyUser, string AgencyOffice,
                            int? ArrCity, int? DepCity, DateTime? BegDateFirst, DateTime? BegDateLast,
                            DateTime? ResDateFirst, DateTime? ResDateLast, DateTime? DueDate, string fltBalance, string ResStatusStr,
                            string ConfirmStatusStr, bool IncludeDueDate, string SurName, string Name, string ResNoFirst, string ResNoLast,
                            ref string errorMsg)
        {

            List<AgencyPaymentMonitorFields> records = new List<AgencyPaymentMonitorFields>();

            StringBuilder sql = new StringBuilder();

            sql.AppendLine("Set @DueDate=dbo.DateOnly(@DueDate)");
            sql.AppendLine("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location");
            sql.AppendLine("Select RecID,Country,City,Town,Village,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name)");
            sql.AppendLine("INTO #Location");
            sql.AppendLine("From Location (NOLOCK)");            
            sql.AppendLine("SELECT *, Balance=case when ResStat in (2,3) then AgencyPayable-AgencyPayment else AgencyPayable2-AgencyPayment end");
            sql.Append("FROM ( \n");
            sql.Append("  SELECT R.ResNo, R.Office, R.Agency, R.AgencyUser, \n");
            sql.Append("     R.DepCity, \n");
            sql.Append("     DepCityName=(Select Name From #Location Where RecID=R.DepCity), \n");
            sql.Append("     DepCityNameL=(Select NameL From #Location Where RecID=R.DepCity), \n");
            sql.Append("     R.SaleCur, \n");
            sql.Append("     Surname=C.Surname, \n");
            sql.Append("     Name=C.Name, \n");
            sql.Append("     Leader=C.Surname + ' ' + C.Name, \n");
            sql.Append("     MainAgency=CASE WHEN AG.AgencyType=0 THEN R.Agency ELSE MainOffice END, \n");
            sql.Append("     AgencyName=AG.Name, \n");
            sql.Append("     R.ArrCity, \n");
            sql.Append("     ArrCityName=(Select Name From #Location Where RecID=R.ArrCity), \n");
            sql.Append("     ArrCityNameL=(Select NameL From #Location Where RecID=R.ArrCity), \n");
            sql.Append("     R.BegDate, R.EndDate, R.Days, R.ResStat, R.ConfStat, R.ResDate, R.Adult, R.Child, \n");
            sql.Append("     R.SalePrice, R.AgencyCom, AgencyDisPasPer, AgencyDisPasVal, \n");
            sql.Append("     R.DisCount, R.BrokerCom, R.UserBonus, R.AgencyBonus, R.AgencySupDis, R.AgencyComSup, \n");
            sql.Append("     R.EBAgency, isnull(R.AgencyPayment,0) AgencyPayment, \n");
            sql.Append("     AgencyPayable=Case When dbo.ufn_AgencyResPayRule(R.Agency, R.PLMarket)=0 then IsNull(R.AgencyPayable+isnull(R.BrokerCom,0),0) else IsNull(R.PasAmount,0) end, \n");
            sql.Append("     AgencyPayable2=isnull(P.Amount, 0), R.EBPas, \n");
            sql.Append("     OfficeName=AG.Name, \n");
            sql.Append("     OfficeNameL=isnull(dbo.FindLocalName(AG.NameLID, @Market), AG.Name), \n");
            sql.Append("     ResStatStr=Case R.ResStat \n");
            sql.Append("                   When 0 Then 'New' \n");
            sql.Append("                   When 1 Then 'Modified' \n");
            sql.Append("                   When 2 Then 'Cancel' \n");
            sql.Append("                   When 3 Then 'Cancel X' \n");
            sql.Append("                   When 9 Then 'Draft' Else '' End, \n");
            sql.Append("     ConfStatStr=Case R.ConfStat \n");
            sql.Append("                   When 0 Then 'Request' \n");
            sql.Append("                   When 1 Then 'OK' \n");
            sql.Append("                   When 2 Then 'Not Confirm' \n");
            sql.Append("                   When 3 Then 'No Show' Else '' End, \n");
            sql.Append("     R.OptDate \n");
            sql.Append("FROM ResMain R (NOLOCK) \n");
            sql.Append("JOIN Agency AG (NOLOCK) ON AG.Code=R.Agency \n");
            sql.Append("OUTER APPLY \n");
            sql.Append("( \n");
            sql.Append("  SELECT Top 1 Surname,Name \n");
            sql.Append("  FROM ResCust (NOLOCK) \n");
            sql.Append("  WHERE ResNo=R.ResNo And Leader='Y' \n");
            sql.Append(") C \n");
            sql.Append("OUTER APPLY \n");
            sql.Append("( \n");
            sql.Append("	SELECT ResNo, Cur, Sum(isnull(Amount,0)) Amount \n");
            sql.Append("    FROM ResPayPlan (NOLOCK) \n");
            if (IncludeDueDate)
                sql.AppendLine("    WHERE ResNo=R.ResNo And DueDate<=@DueDate");
            else
                sql.AppendLine("    WHERE ResNo=R.ResNo And DueDate<@DueDate");
            sql.Append("		 \n");
            sql.Append("    GROUP BY ResNo, Cur \n");
            sql.Append(") P \n");
            sql.Append("OUTER APPLY \n");
            sql.Append("( \n");
            sql.Append("	Select Agency= Case When \n");
            sql.Append("						AgencyType=0 then R.Agency \n");
            sql.Append("					Else \n");
            sql.Append("					   (Case When AgencyType<>2 Then MainOffice Else Code End) \n");
            sql.Append("					End, \n");
            sql.Append("		   AgencyType \n");
            sql.Append("	From Agency (NOLOCK) \n");
            sql.Append("	Where Code=R.Agency \n");
            sql.Append(") A \n");
            sql.Append("WHERE \n");
            sql.Append("    isnull(R.Ballon, '') <> '' \n");
            sql.Append(" And R.Operator=@Operator \n");
            if (!string.IsNullOrEmpty(ResNoFirst) && !string.IsNullOrEmpty(ResNoLast))
            {
                sql.AppendFormat(" And R.ResNo>='{0}' And R.ResNo<='{1}' \n", ResNoFirst, ResNoLast);
            }
            else
            {
                if (!string.IsNullOrEmpty(ResNoFirst))
                    sql.AppendFormat(" And R.ResNo='{0}' \n", ResNoFirst);
                else if (!string.IsNullOrEmpty(ResNoLast))
                    sql.AppendFormat(" And R.ResNo='{0}' \n", ResNoLast);
            }
            string mainAgency = string.Empty;
            if (UserData.MasterAgency)
                mainAgency = UserData.AgencyID;
            else mainAgency = !UserData.ShowAllRes ? UserData.AgencyID : (string.Equals(UserData.MainOffice, UserData.AgencyID) ? UserData.AgencyID : (UserData.AgencyType != 2 ? UserData.MainOffice : UserData.AgencyID));
            if (UserData.MainAgency && string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt))
            {
                sql.AppendFormat(" And (AG.Code='{0}' Or AG.MainOffice='{0}') \n", UserData.AgencyID);
            }
            else
            {
                sql.AppendFormat(" And A.Agency='{0}' \n", mainAgency);
            }
            if (!string.IsNullOrEmpty(fltBalance))
            {
                var queryBalance = from q in fltBalance.Split(',').AsEnumerable() select new { s = Convert.ToInt32(q) };
                if (queryBalance.Count() > 0)
                {
                    string tmptsql = string.Empty;
                    if (queryBalance.Where(w => w.s == 2).Count() > 0) tmptsql += "OR (isnull(P.Amount,0)-isnull(AgencyPayment,0))>0 ";
                    if (queryBalance.Where(w => w.s == 3).Count() > 0) tmptsql += "OR (isnull(P.Amount,0)-isnull(AgencyPayment,0))<0 ";
                    if (queryBalance.Where(w => w.s == 4).Count() > 0) tmptsql += "OR (isnull(P.Amount,0)-isnull(AgencyPayment,0))=0 ";
                    if (!(queryBalance.Where(w => w.s == 1).Count() > 0) && tmptsql.Length > 0)
                    {
                        tmptsql = tmptsql.Remove(0, 3);
                        tmptsql = " AND (" + tmptsql + ") ";
                        sql.Append(tmptsql + " \n");
                    }
                }
            }
            if (!string.IsNullOrEmpty(ResStatusStr)) sql.Append(" And R.ResStat in(" + ResStatusStr + ") \n");
            if (!string.IsNullOrEmpty(ConfirmStatusStr)) sql.Append(" And R.ConfStat in (" + ConfirmStatusStr + ") \n");
            if (ArrCity.HasValue) sql.AppendFormat(" And R.ArrCity={0} \n", ArrCity);
            if (DepCity.HasValue) sql.AppendFormat(" And R.DepCity={1} \n", DepCity);
            if (BegDateFirst.HasValue) sql.Append(" And R.BegDate>=@BegDate1 \n");
            if (BegDateLast.HasValue) sql.Append(" And R.BegDate<=@BegDate2 \n");
            if (ResDateFirst.HasValue) sql.Append(" And R.ResDate>=@ResDate1 \n");
            if (ResDateLast.HasValue) sql.Append(" And R.ResDate>=@ResDate2 \n");
            string agencyOffice = string.IsNullOrEmpty(AgencyOffice) ? (UserData.ShowAllRes ? string.Empty : UserData.MainOffice) : AgencyOffice;
            if (!string.IsNullOrEmpty(agencyOffice)) sql.AppendFormat(" And R.Agency='{0}' \n", agencyOffice);
            string agencyUser = !UserData.ShowAllRes ? UserData.UserID : AgencyUser;
            if (!string.IsNullOrEmpty(agencyUser)) sql.AppendFormat(" And R.AgencyUser='{0}' \n", agencyUser);
            if (!string.IsNullOrEmpty(SurName)) sql.AppendFormat(" And C.Surname Like '%{0}%' \n", SurName);
            if (!string.IsNullOrEmpty(Name)) sql.AppendFormat(" And C.Name Like '%{0}%' \n", Name);
            if (string.Equals(UserData.CustomRegID, Common.crID_SunFun)) {
                sql.AppendLine(" And R.OptDate is null");
            }
            sql.Append(") X \n");
            sql.Append(" Order By X.ResNo Desc \n");

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sql.ToString());
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "DueDate", DbType.DateTime, DueDate.HasValue ? DueDate.Value.ToLocalTime() : DateTime.Today);
                if (BegDateFirst.HasValue)
                    db.AddInParameter(dbCommand, "BegDate1", DbType.DateTime, BegDateFirst.Value);
                if (BegDateLast.HasValue)
                    db.AddInParameter(dbCommand, "BegDate2", DbType.DateTime, BegDateLast.Value);
                if (ResDateFirst.HasValue)
                    db.AddInParameter(dbCommand, "ResDate1", DbType.DateTime, ResDateFirst.Value);
                if (ResDateLast.HasValue)
                    db.AddInParameter(dbCommand, "ResDate2", DbType.DateTime, ResDateLast.Value);

                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    int cnt = 0;
                    while (reader.Read())
                    {
                        ++cnt;
                        AgencyPaymentMonitorFields rec = new AgencyPaymentMonitorFields();
                        rec.RecID = cnt;
                        rec.Adult = Conversion.getInt16OrNull(reader["Adult"]);
                        rec.Agency = Conversion.getStrOrNull(reader["Agency"]);
                        rec.AgencyBonus = Conversion.getDecimalOrNull(reader["AgencyBonus"]);
                        rec.AgencyCom = Conversion.getDecimalOrNull(reader["AgencyCom"]);
                        rec.DiscountFromCom = Conversion.getDecimalOrNull(reader["AgencyDisPasVal"]);
                        rec.AgencyComSup = Conversion.getDecimalOrNull(reader["AgencyComSup"]);
                        rec.AgencyPayable = Conversion.getDecimalOrNull(reader["AgencyPayable"]);
                        rec.AgencyPayable2 = Conversion.getDecimalOrNull(reader["AgencyPayable2"]);
                        rec.AgencyPayment = Conversion.getDecimalOrNull(reader["AgencyPayment"]);
                        rec.AgencySupDis = Conversion.getDecimalOrNull(reader["AgencySupDis"]);
                        rec.AgencyUser = Conversion.getStrOrNull(reader["AgencyUser"]);
                        rec.AgencyName = Conversion.getStrOrNull(reader["AgencyName"]);
                        rec.ArrCity = Conversion.getInt32OrNull(reader["ArrCity"]);
                        rec.ArrCityName = UserData.EqMarketLang ? Conversion.getStrOrNull(reader["ArrCityNameL"]) : Conversion.getStrOrNull(reader["ArrCityName"]);
                        rec.Balance = Conversion.getDecimalOrNull(reader["Balance"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(reader["BegDate"]);
                        rec.BrokerCom = Conversion.getDecimalOrNull(reader["BrokerCom"]);
                        rec.Child = Conversion.getInt16OrNull(reader["Child"]);
                        rec.ConfStat = Conversion.getInt16OrNull(reader["ConfStat"]);
                        rec.ConfStatStr = Conversion.getStrOrNull(reader["ConfStatStr"]);
                        rec.Days = Conversion.getInt16OrNull(reader["Days"]);
                        rec.DepCity = Conversion.getInt32OrNull(reader["DepCity"]);
                        rec.DepCityName = UserData.EqMarketLang ? Conversion.getStrOrNull(reader["DepCityNameL"]) : Conversion.getStrOrNull(reader["DepCityName"]);
                        rec.Discount = Conversion.getDecimalOrNull(reader["Discount"]);
                        rec.EBAgency = Conversion.getDecimalOrNull(reader["EBAgency"]);
                        rec.EBPas = Conversion.getDecimalOrNull(reader["EBPas"]);
                        rec.EndDate = Conversion.getDateTimeOrNull(reader["EndDate"]);
                        rec.Leader = Conversion.getStrOrNull(reader["Leader"]);
                        rec.MainAgency = Conversion.getStrOrNull(reader["MainAgency"]);
                        rec.Name = Conversion.getStrOrNull(reader["Name"]);
                        rec.Office = Conversion.getStrOrNull(reader["Office"]);
                        rec.OfficeName = UserData.EqMarketLang ? Conversion.getStrOrNull(reader["OfficeNameL"]) : Conversion.getStrOrNull(reader["OfficeName"]);
                        rec.OptDate = Conversion.getDateTimeOrNull(reader["OptDate"]);
                        rec.ResDate = Conversion.getDateTimeOrNull(reader["ResDate"]);
                        rec.ResNo = Conversion.getStrOrNull(reader["ResNo"]);
                        rec.ResStat = Conversion.getInt16OrNull(reader["ResStat"]);
                        rec.ResStatStr = Conversion.getStrOrNull(reader["ResStatStr"]);
                        rec.SaleCur = Conversion.getStrOrNull(reader["SaleCur"]);
                        rec.SalePrice = Conversion.getDecimalOrNull(reader["SalePrice"]);
                        rec.Surname = Conversion.getStrOrNull(reader["Surname"]);
                        rec.UserBonus = Conversion.getDecimalOrNull(reader["UserBonus"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AgencyPaymentMonitorJournalRef> getJournalReferance(string ResNo, ref string errorMsg)
        {
            List<AgencyPaymentMonitorJournalRef> records = new List<AgencyPaymentMonitorJournalRef>();
            string tsql =
@"
Select Jc.ResNo,J.Reference
From Journal J (NOLOCK)
Join JournalCDet Jc (NOLOCK) ON J.RecID=Jc.JournalID
Where Jc.ResNo=@ResNo
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "ResNo", DbType.AnsiString, ResNo);                
                using (IDataReader R = db.ExecuteReader(dbCommand)) {                    
                    while (R.Read()) {
                        AgencyPaymentMonitorJournalRef rec = new AgencyPaymentMonitorJournalRef();
                        rec.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        rec.Reference = Conversion.getStrOrNull(R["Reference"]);
                        if (string.IsNullOrEmpty(rec.Reference))
                            rec.Reference = "-";
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return records;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public AgencyRisk getAgencyRisk(User UserData, ref string errorMsg)
        {
            string tsql =
@"
Select MainOffice=Case When isnull(A.MainOffice,'')='' Then A.Code Else A.MainOffice End,
  CreditLimit=isnull(A.CreditLimit,0), 
  Sale=Sum(isnull(TotSale,0)), 
  Payment=Sum(isnull(TotPayment,0)),
  Com=Sum(isnull(TotCom,0)), 
  Balance=Sum(isnull(TotBalance,0)),
  Adl=Sum(isnull(TotAdl,0)),
  Chd=Sum(isnull(TotChd,0)), 
  Pax=Sum(isnull(TotPax,0)), 
  AgencyPayable=Sum(isnull(TotAgencyPayable,0)),
  AgencySupDis=Sum(isnull(TotAgencySupDis,0)),
  PasSupDis=Sum(isnull(TotPasSupDis,0)),
  EBPas=Sum(isnull(TotPasEBPrice,0)),
  Risk=isnull(A.CreditLimit,0)-Sum(isnull(TotBalance,0)) 
From Agency A (NOLOCK) 
Left Join AgencyDayBook DB (NOLOCK) on DB.Agency = A.Code 
Where (Select Operator From Office Where Office.Code=A.OprOffice)=@Operator
  And A.Code=@Agency
Group By A.Code,A.CreditLimit,A.MainOffice
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, UserData.Operator);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    if (R.Read()) {
                        AgencyRisk rec = new AgencyRisk();
                        rec.MainOffice = Conversion.getStrOrNull(R["MainOffice"]);
                        rec.CreditLimit = Conversion.getDecimalOrNull(R["CreditLimit"]);
                        rec.Sale = Conversion.getDecimalOrNull(R["Sale"]);
                        rec.Payment = Conversion.getDecimalOrNull(R["Payment"]);
                        rec.Com = Conversion.getDecimalOrNull(R["Com"]);
                        rec.Balance = Conversion.getDecimalOrNull(R["Balance"]);
                        rec.Adl = Conversion.getInt32OrNull(R["Adl"]);
                        rec.Chd = Conversion.getInt32OrNull(R["Chd"]);
                        rec.Pax = Conversion.getInt32OrNull(R["Pax"]);
                        rec.AgencyPayable = Conversion.getDecimalOrNull(R["AgencyPayable"]);
                        rec.AgencySupDis = Conversion.getDecimalOrNull(R["AgencySupDis"]);
                        rec.PasSupDis = Conversion.getDecimalOrNull(R["PasSupDis"]);
                        rec.EBPas = Conversion.getDecimalOrNull(R["EBPas"]);
                        rec.Risk = Conversion.getDecimalOrNull(R["Risk"]);
                        return rec;
                    } else {
                        return null;
                    }
                }                
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
