﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using TvTools;

namespace TvBo
{
    public class MixSearch
    {
        public List<MixCountry> getCountryList(User UserData, ref string errorMsg)
        {
            CountryListResponse countryList = new MixSearchPaximum().getCountryList(UserData, ref errorMsg);

            string path = AppDomain.CurrentDomain.BaseDirectory + "Data\\IsoCountryList.json";
            List<MixCountry> list = new List<MixCountry>();
            if (System.IO.File.Exists(path))
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(path);
                try
                {
                    string uncompressed = reader.ReadToEnd();
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MixCountry>>(uncompressed);
                    if (list == null)
                        list = new List<MixCountry>();
                    if (countryList == null)
                    {
                        countryList = new CountryListResponse();
                        countryList.CountryList = new List<IdNamePair>();
                    }
                    List<MixCountry> returnList = (from q1 in countryList.CountryList
                                                   join q2 in list on q1.id equals q2.ISO2
                                                   select new MixCountry { ISO2 = q1.id, ISO3 = q2.ISO3, Name = q1.name }).ToList<MixCountry>();
                    return returnList;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    reader.Close();
                }
            }
            else
                return null;
        }

        public static MixSearchFilterCacheData getMixPackageSearchData(User UserData, bool GroupSearch, SearchType sType)
        {
            string errorMsg = string.Empty;

            string onlyHotelVersion = string.Empty;
            if (sType != SearchType.HotelSale)
                onlyHotelVersion = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "OnlyHotelVersion"));
            else if (sType == SearchType.HotelSale)
                onlyHotelVersion = "HS";

            string key = string.Empty;
            switch (sType)
            {
                case SearchType.OnlyHotelMixSearch:
                    key = "OnlyHotelMix" + "_" + UserData.Operator + "_" + UserData.Market;
                    break;
                case SearchType.HotelSale:
                    key = "HotelSale" + "_" + UserData.Operator + "_" + UserData.Market;
                    break;
            }
            MixSearchFilterCacheData obj;
            if (!CacheHelper.Get(key, out obj))
            {
                if (sType == SearchType.OnlyHotelMixSearch || sType == SearchType.HotelSale)
                {
                    obj = new MixSearch().getMixSearchFilterCacheData(UserData, GroupSearch, sType, onlyHotelVersion, ref errorMsg);
                }
                CacheHelper.Add(obj, key);
            }
            return obj;
        }

        public MixSearchFilterCacheData addMixPlLocation(DataSet plSearchData, MixSearchFilterCacheData baseData, bool useLocalName)
        {
            List<MixPlLocation> plData = new List<MixPlLocation>();
            DataTable Locations = plSearchData.Tables["Locations"];
            foreach (DataRow r in Locations.Rows)
            {
                try
                {
                    MixPlLocation plLocRecord = new MixPlLocation();
                    plLocRecord.DepCity = (int)r["DepCity"];
                    plLocRecord.DepCityName = r["DepCityName"].ToString();
                    //plLocRecord.DepCityNameL = r["DepCityNameL"].ToString();
                    plLocRecord.ArrCity = (int)r["ArrCity"];
                    plLocRecord.ArrCityName = r["ArrCityName"].ToString();
                    //plLocRecord.ArrCityNameL = r["ArrCityNameL"].ToString();
                    plLocRecord.ArrCountry = (int)r["ArrCountry"];
                    plLocRecord.ArrCountryName = r["ArrCountryName"].ToString();
                    //plLocRecord.ArrCountryNameL = r["ArrCountryNameL"].ToString();                    
                    if (Locations.Columns.Contains("Category"))
                        plLocRecord.HolPackCategory = Conversion.getInt16OrNull(r["Category"]);
                    if (Locations.Columns.Contains("CategoryName"))
                        plLocRecord.HolPackCategoryName = Conversion.getStrOrNull(r["CategoryName"]);
                    //if (Locations.Columns.Contains("CategoryNameL"))
                    //    plLocRecord.HolPackCatNameL = Conversion.getStrOrNull(r["CategoryNameL"]);
                    plData.Add(plLocRecord);
                }
                catch { }
            }
            baseData.LocationList = plData;
            return baseData;
        }

        public MixSearchFilterCacheData addMixPlCategory(DataSet plSearchData, MixSearchFilterCacheData baseData, bool useLocalName)
        {
            List<MixPlCategory> plData = new List<MixPlCategory>();
            DataTable Categorys = plSearchData.Tables["Category"];
            foreach (DataRow r in Categorys.Rows)
            {
                MixPlCategory plCatRecord = new MixPlCategory();
                plCatRecord.Code = r["Code"].ToString();
                plCatRecord.Name = r["Name"].ToString();
                //plCatRecord.NameL = r["NameL"].ToString();
                plCatRecord.Country = (int)r["Country"];
                plCatRecord.PLCountry = Conversion.getInt32OrNull(r["PL_Country"]);
                plCatRecord.City = (int)r["City"];
                plCatRecord.PLCity = Conversion.getInt32OrNull(r["PL_City"]);
                plCatRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plCatRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                if (Categorys.Columns.Contains("Category"))
                    plCatRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plCatRecord);
            }
            baseData.CategoryList = plData;
            return baseData;
        }

        public MixSearchFilterCacheData addMixPlHolPack(DataSet plSearchData, MixSearchFilterCacheData baseData, bool useLocalName)
        {
            List<MixPlHolPack> plData = new List<MixPlHolPack>();
            DataTable HolPacks = plSearchData.Tables["HolPack"];
            foreach (DataRow r in HolPacks.Rows)
            {
                MixPlHolPack plRecord = new MixPlHolPack();
                plRecord.HolPack = r["HolPack"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r["NameL"].ToString();
                plRecord.DepCity = (int)r["DepCity"];
                plRecord.ArrCity = (int)r["ArrCity"];
                plRecord.Country = (int)r["Country"];

                if (HolPacks.Columns.Contains("Category"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.HolPackList = plData;
            return baseData;
        }

        public MixSearchFilterCacheData addMixPlBoard(DataSet plSearchData, MixSearchFilterCacheData baseData, bool useLocalName)
        {
            List<MixPlBoard> plData = new List<MixPlBoard>();
            DataTable Boards = plSearchData.Tables["Board"];
            foreach (DataRow r in Boards.Rows)
            {
                MixPlBoard plRecord = new MixPlBoard();
                plRecord.Code = r["Code"].ToString();
                plRecord.Hotel_Code = r["Hotel_Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                //plRecord.NameL = r["NameL"].ToString();
                plRecord.City = (int)r["City"];
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plRecord.PLDepCity = (int)r["PLDepCity"];
                plRecord.PLArrCity = (int)r["PLArrCity"];

                if (Boards.Columns.Contains("Category"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.BoardList = plData;
            return baseData;
        }

        public MixSearchFilterCacheData addMixPlRoomView(DataSet plSearchData, MixSearchFilterCacheData baseData, bool useLocalName)
        {
            List<MixPlRoomView> plData = new List<MixPlRoomView>();
            DataTable Rooms = plSearchData.Tables["RoomView"];
            foreach (DataRow r in Rooms.Rows)
            {
                MixPlRoomView plRecord = new MixPlRoomView();
                plRecord.Code = Conversion.getInt32OrNull(r["Code"]);
                plRecord.Hotel_Code = r["Hotel_Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.City = (int)r["City"];
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plRecord.PLDepCity = (int)r["PLDepCity"];
                plRecord.PLArrCity = (int)r["PLArrCity"];

                plData.Add(plRecord);
            }
            baseData.RoomViewList = plData;
            return baseData;
        }

        public MixSearchFilterCacheData addMixPlRoom(DataSet plSearchData, MixSearchFilterCacheData baseData, bool useLocalName)
        {
            List<MixPlRoom> plData = new List<MixPlRoom>();
            DataTable Rooms = plSearchData.Tables["Room"];
            foreach (DataRow r in Rooms.Rows)
            {
                MixPlRoom plRecord = new MixPlRoom();
                plRecord.Code = r["Code"].ToString();
                plRecord.Hotel_Code = r["Hotel_Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                //plRecord.NameL = r["NameL"].ToString();
                plRecord.City = (int)r["City"];
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.Village = Conversion.getInt32OrNull(r["Village"]);
                plRecord.PLDepCity = (int)r["PLDepCity"];
                plRecord.PLArrCity = (int)r["PLArrCity"];
                if (Rooms.Columns.Contains("Category"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                if (Rooms.Columns.Contains("RoomViewID"))
                    plRecord.RoomViewID = Conversion.getInt16OrNull(r["RoomViewID"]);
                plData.Add(plRecord);
            }
            baseData.RoomList = plData;
            return baseData;
        }

        public MixSearchFilterCacheData addMixPlResort(DataSet plSearchData, MixSearchFilterCacheData baseData, bool useLocalName)
        {
            List<MixPlResort> plData = new List<MixPlResort>();
            DataTable Resorts = plSearchData.Tables["Resort"];
            foreach (DataRow r in Resorts.Rows)
            {
                MixPlResort plRecord = new MixPlResort();
                plRecord.RecID = Conversion.getInt32OrNull(r["RecID"]);
                plRecord.Name = r["Name"].ToString();
                //plRecord.NameL = r["NameL"].ToString();
                plRecord.Country = (int)r["Country"];
                plRecord.City = (int)r["City"];

                if (Resorts.Columns.Contains("Category"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["Category"]);
                plData.Add(plRecord);
            }
            baseData.ResortList = plData;
            return baseData;
        }

        public MixSearchFilterCacheData addMixPlHotel(DataSet plSearchData, MixSearchFilterCacheData baseData, List<Location> _Locations, bool useLocalName)
        {
            List<MixPlHotel> plData = new List<MixPlHotel>();
            DataTable Hotels = plSearchData.Tables["Hotels"];
            foreach (DataRow r in Hotels.Rows)
            {
                MixPlHotel plRecord = new MixPlHotel();
                plRecord.Code = r["Code"].ToString();
                plRecord.Name = r["Name"].ToString();
                plRecord.NameL = r.Table.Columns.Contains("NameL") ? r["NameL"].ToString() : plRecord.Name;
                plRecord.Category = r["Category"].ToString();
                plRecord.Holpack = r["Holpack"].ToString();
                plRecord.Location = (int)r["Location"];
                plRecord.LocationName = _Locations.Find(f => f.RecID == plRecord.Location) != null ? _Locations.Find(f => f.RecID == plRecord.Location).Name : "";
                //plRecord.LocationNameL = _Locations.Find(f => f.RecID == plRecord.Location) != null ? _Locations.Find(f => f.RecID == plRecord.Location).NameL : "";
                plRecord.ArrCity = (int)r["ArrCity"];
                plRecord.ArrCityName = _Locations.Find(f => f.RecID == plRecord.ArrCity) != null ? _Locations.Find(f => f.RecID == plRecord.ArrCity).Name : "";
                //plRecord.ArrCityNameL = _Locations.Find(f => f.RecID == plRecord.ArrCity) != null ? _Locations.Find(f => f.RecID == plRecord.ArrCity).NameL : "";
                plRecord.Country = (int)r["Country"];
                plRecord.CountryName = _Locations.Find(f => f.RecID == plRecord.Country) != null ? _Locations.Find(f => f.RecID == plRecord.Country).Name : "";
                //plRecord.CountryNameL = _Locations.Find(f => f.RecID == plRecord.Country) != null ? _Locations.Find(f => f.RecID == plRecord.Country).NameL : "";
                plRecord.Town = Conversion.getInt32OrNull(r["Town"]);
                plRecord.TownName = _Locations.Find(f => f.RecID == plRecord.Town) != null ? _Locations.Find(f => f.RecID == plRecord.Town).Name : "";
                //plRecord.TownNameL = _Locations.Find(f => f.RecID == plRecord.Town) != null ? _Locations.Find(f => f.RecID == plRecord.Town).NameL : "";
                plRecord.CatPackID = Conversion.getInt32OrNull(r["CatPackID"]);

                if (Hotels.Columns.Contains("HolPackCat"))
                    plRecord.HolPackCat = Conversion.getInt16OrNull(r["HolPackCat"]);
                plData.Add(plRecord);
            }
            baseData.HotelList = plData;
            return baseData;
        }

        public MixSearchFilterCacheData addMixPlMaxPax(DataSet plSearchData, MixSearchFilterCacheData baseData)
        {
            MixPlMaxPaxCounts plMaxPax = new MixPlMaxPaxCounts();
            if (!plSearchData.Tables.Contains("MaxPaxCount"))
                return baseData;
            DataTable MaxPaxT = plSearchData.Tables["MaxPaxCount"];
            plMaxPax.maxAdult = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["Adult"]);
            plMaxPax.maxAdult = plMaxPax.maxAdult.HasValue ? plMaxPax.maxAdult.Value : 4;
            plMaxPax.maxChdAgeG1 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG1"]);
            plMaxPax.maxChdAgeG2 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG2"]);
            plMaxPax.maxChdAgeG3 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG3"]);
            plMaxPax.maxChdAgeG4 = Conversion.getInt32OrNull(MaxPaxT.Rows[0]["ChdAgeG4"]);
            List<int?> maxChd = new List<int?>();
            maxChd.Add(plMaxPax.maxChdAgeG1);
            maxChd.Add(plMaxPax.maxChdAgeG2);
            maxChd.Add(plMaxPax.maxChdAgeG3);
            maxChd.Add(plMaxPax.maxChdAgeG4);

            plMaxPax.maxChd = maxChd.OrderBy(o => (o.HasValue ? o.Value : 4)).LastOrDefault().HasValue ? maxChd.OrderBy(o => (o.HasValue ? o.Value : 4)).LastOrDefault().Value : 4;
            baseData.MixPlMaxPaxCount = plMaxPax;
            return baseData;
        }

        public StringBuilder getPriceCacheDataForOnlyHotelMixSearch(string Operator, string Market, bool Groups, ref string errorMsg)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ");
            sql.Append("SET NOCOUNT ON ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CPM') is not null Drop Table dbo.#CPM ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#Hotel') is not null Drop Table dbo.#Hotel ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#CatPriceA') is not null Drop Table dbo.#CatPriceA ");
            sql.Append("if OBJECT_ID('TempDB.dbo.#HolPackCat') is not null Drop Table dbo.#HolPackCat \n");
            sql.Append("Select *,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name)  Into #CatalogPack ");
            sql.Append("From CatalogPack CP ");
            sql.Append("Where CP.ReadY='Y' ");
            sql.Append("  And CP.WebPub='Y' And CP.WebPubDate<=GetDate() ");
            sql.Append("  And CP.PackType='O' ");
            sql.Append("  And (isnull(CP.B2BMenuCat,'')='' Or CP.B2BMenuCat='O') ");
            sql.Append("  And EndDate > GetDate()-1 ");
            sql.Append("  And dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("Select Direction,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name) ");
            sql.Append("Into #HolPackCat ");
            sql.Append("From HolPackCat ");
            sql.Append("Select Distinct CatPackID,Hotel ");
            sql.Append("Into #CatPriceA ");
            sql.Append("From CatPriceA A ");
            sql.Append("Join #CatalogPack CP on CP.RecID=A.CatPAckID ");
            sql.Append("Where A.Status=1 ");
            sql.Append("Select CPM.RecID,CPM.CatPackID,CPM.Market,CPM.Operator ");
            sql.Append("Into #CPM ");
            sql.Append("From CatPackMarket CPM ");
            sql.Append("Join #CatalogPack CP ON CP.RecID=CPM.CatPackID ");
            sql.Append("Where CPM.Market=@Market ");
            sql.Append(" And (CPM.Operator=@Operator Or CPM.Operator='') ");
            sql.Append("Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] ");
            sql.Append("Into #Location ");
            sql.Append("From Location ");
            sql.Append("Select [Name],NameL,Code,HotelType,Category,Location ");
            sql.Append("Into #Hotel ");
            sql.Append("From ");
            sql.Append("( ");
            sql.Append("    Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),H.HotelType,H.Category,H.Location ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
            sql.Append("    Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") H ");
            sql.Append("Group By [Name],NameL,Code,HotelType,Category,Location \n");
            sql.Append("--Locations \n");
            sql.Append("Select DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL ");
            sql.Append("From ( ");
            sql.Append("    SELECT CP.DepCity, ");
            sql.Append("        DepCityName=(Select Name From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        DepCityNameL=(Select NameL From #Location L Where RecID=CP.DepCity), ");
            sql.Append("        ArrCountry=(Select Country From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCountryName=(Select Name From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        ArrCountryNameL=(Select NameL From #Location L Where RecID in (Select Country From #Location L Where RecID=CP.ArrCity)), ");
            sql.Append("        CP.ArrCity, ");
            sql.Append("        ArrCityName=(Select Name From #Location L Where RecID=CP.ArrCity), ");
            sql.Append("        ArrCityNameL=(Select NameL From #Location L Where RecID=CP.ArrCity) ");
            sql.Append("    FROM #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON CPA.Hotel=H.Code ");
            sql.Append("    JOIN #Location L ON L.RecID=(Select Country From #Location Where RecID=H.Location) ");
            sql.Append("    LEFT JOIN #HolPackCat HPC ON HPC.Code=CP.Category And HPC.Direction=CP.Direction ");
            sql.Append("    WHERE 	dbo.DateOnly(GetDate()) between CP.SaleBegDate And CP.SaleEndDate ");
            sql.Append("        And Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
            sql.Append(") Locations ");
            sql.Append("Group By DepCity,DepCityName,DepCityNameL,ArrCountry,ArrCountryName,ArrCountryNameL,ArrCity,ArrCityName,ArrCityNameL \n");
            sql.Append("--Category \n");
            sql.Append("SELECT Distinct HC.Code,HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID, @Market),HC.Name),L.Country, ");
            sql.Append("    PL_Country=(Select Country From #Location Where RecID=CP.ArrCity), ");
            sql.Append("    L.City, PL_City=CP.ArrCity, L.Town, L.Village ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("JOIN HotelCat HC  ON HC.Code=H.Category ");
            sql.Append("JOIN #Location L ON L.RecID=H.Location \n");
            sql.Append("--HolPack \n");
            sql.Append("Select [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID ");
            sql.Append("From ( ");
            sql.Append("    Select CP.HolPack, ");
            sql.Append("        Name=H.Name +  ");
            sql.Append("        ' ( ' + (Select Name From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select Name From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) +  ");
            sql.Append("        ' ( ' + (Select NameL From #Location Where RecID=CP.DepCity) + ");
            sql.Append("        ' , ' + (Select NameL From #Location Where RecID=CP.ArrCity) + ' ) ',  ");
            sql.Append("        CP.DepCity , CP.ArrCity, L.Country, CatPackID=CP.RecID ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    Join HolPack H  ON H.Code=CP.HolPack ");
            sql.Append("    Join #Location L ON L.RecID=CP.ArrCity ");
            sql.Append("    Where  Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append("      And isnull(H.Ready,'N')='Y' ");
            sql.Append("      And isnull(H.WebPub,'N')='Y' ");
            sql.Append(") Holpacks ");
            sql.Append("Group By [Name],NameL,HolPack,DepCity,ArrCity,Country,CatPackID \n");
            if (Groups)
            {
                #region with groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GBD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    JOIN GrpBoardDet  GBD ON GBD.Board=HB.Code ");
                sql.Append("    JOIN GrpBoard  GB ON GB.Code=GBD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,Code=GRD.Groups,GB.Name,NameL=isnull(dbo.FindLocalName(GB.NameLID,@Market),GB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA  ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR  ON HR.Hotel=H.Code ");
                sql.Append("    JOIN GrpRoomDet  GRD ON GRD.Room=HR.Code ");
                sql.Append("    JOIN GrpRoom  GB ON GB.Code=GRD.Groups And GB.GrpType='P' ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            else
            {
                #region without groups
                sql.Append("--Board \n");
                sql.Append("SELECT [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("FROM ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HB.Code,HB.Name,NameL=isnull(dbo.FindLocalName(HB.NameLID,@Market),HB.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelBoard HB  ON HB.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Boards ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                sql.Append("--Rooms \n");
                sql.Append("Select [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity ");
                sql.Append("From ( ");
                sql.Append("    SELECT Hotel_Code=H.Code,HR.Code,HR.Name,NameL=isnull(dbo.FindLocalName(HR.NameLID,@Market),HR.Name),L.City,L.Town,L.Village, ");
                sql.Append("        PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity ");
                sql.Append("    From #CatalogPack CP ");
                sql.Append("    Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID ");
                sql.Append("    Join #Hotel H ON H.Code=CPA.Hotel ");
                sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
                sql.Append("    JOIN HotelRoom HR ON HR.Hotel=H.Code ");
                sql.Append("    Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) ");
                sql.Append(") Rooms ");
                sql.Append("Group By [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
                #endregion
            }
            sql.Append("--Resort \n");
            sql.Append("Select [Name],NameL,RecID,Country,City ");
            sql.Append("From ( ");
            sql.Append("    Select RecID=L.RecID,Name=(Select Name From #Location Where RecID=L.RecID),NameL=(Select NameL From #Location Where RecID=L.RecID), ");
            sql.Append("         L.Country,City=CP.ArrCity ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append("    Where Exists (Select * From #CPM Where CatPackID=CP.RecID) ");
            sql.Append(") Resort ");
            sql.Append("Group By [Name],NameL,RecID,Country,City \n");
            sql.Append("--Hotels \n");
            sql.Append("Select [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack ");
            sql.Append("From ( ");
            sql.Append("    Select H.Code,Name=H.Name,NameL=H.NameL,H.Category,H.Location,L.Country,L.City, ");
            sql.Append("		Town=isnull(L.Town,-1),CP.ArrCity,CatPackID=CP.RecID,CP.Holpack ");
            sql.Append("    From #CatalogPack CP ");
            sql.Append("    JOIN #CatPriceA CPA  ON CPA.CatPackID=CP.RecID ");
            sql.Append("    JOIN #Hotel H ON H.Code=CPA.Hotel ");
            sql.Append("    JOIN #Location L ON L.RecID=H.Location ");
            sql.Append(") Hotels ");
            sql.Append("Group By [Name],NameL,Code,Category,Location,Country,City,Town,ArrCity,CatPackID,Holpack \n");
            sql.Append("-- Max Pax Count \n");
            sql.Append("Select Adult=Max(isnull(Adult,6)),ChdAgeG1=Max(isnull(ChdAgeG1,4)),ChdAgeG2=Max(isnull(ChdAgeG2,4)),ChdAgeG3=Max(isnull(ChdAgeG3,4)),ChdAgeG4=Max(isnull(ChdAgeG4,4)) ");
            sql.Append("From #CatalogPack CP ");
            sql.Append("Join CatPriceA CPA  ON CP.RecID=CPA.CatPackID And CPA.Status=1 ");
            sql.Append("Join Hotel H  ON H.Code=CPA.Hotel ");
            sql.Append("Join HotelAccomPax HAP  on HAP.Hotel=CPA.Hotel and HAP.Room=CPA.Room and HAP.Accom=CPA.Accom and IsNull(HAP.VisiblePL,'Y')='Y' ");
            sql.Append("-- Room View \n");
            sql.Append("Select [Name],Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
            sql.Append("From ( \n");
            sql.Append("  SELECT Hotel_Code=H.Code,Code=RW.RecID,RW.Name,L.City,L.Town,L.Village, \n");
            sql.Append("    PLArrCity=CP.ArrCity,PLDepCity=CP.DepCity \n");
            sql.Append("	From #CatalogPack CP \n");
            sql.Append("	Join #CatPriceA CPA ON CP.RecID=CPA.CatPackID \n");
            sql.Append("	Join #Hotel H ON H.Code=CPA.Hotel \n");
            sql.Append("	JOIN #Location L ON L.RecID=H.Location \n");
            sql.Append("	JOIN HotelRoom HR ON HR.Hotel=H.Code \n");
            sql.Append("	JOIN RoomView RW ON HR.RoomViewID=RW.RecID \n");
            sql.Append("	Where Exists(Select RecID From #CPM Where CatPackID=CPA.CatPackID) \n");
            sql.Append(") Rooms \n");
            sql.Append("Group By [Name],Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity \n");
            sql.Append("-- Room View \n");
            return sql;
        }

        public string getPriceCacheDataForHotelSale(bool Groups, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"
Declare @SaleDate Date,@DepCity int,@DepCityName varChar(30),@DepCityNameL varChar(400),@MarketId int
Select @SaleDate=GETDATE()
Select @MarketId=RecId from Market(nolock) where Code=@Market

SET NOCOUNT ON

Declare @Location TABLE (RecID int,Name varchar(100),NameL nvarchar(100),Country int,City int,Town int,Village int,Type smallint)

Declare @Hotel TABLE (RecID int,RoomID int,BoardID int)

Insert Into @Location (RecID,Name,NameL,Country,City,Town,Village,Type)
Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] 
 From Location (NOLOCK)


Select @DepCity=L.RecID,@DepCityName=L.Name,@DepCityNameL=L.NameL From Agency A  (NOLOCK)
Join Office O (NOLOCK) ON A.OprOffice=O.Code
Join @Location L ON O.Location=L.RecID
Where A.Code=@Agency

Insert Into @Hotel (RecID,RoomID,BoardID)
Select distinct HotelID,RoomID,BoardID From OHotelSalePrice P (NOLOCK) Where P.CheckIn>=@SaleDate and P.MarketId=@MarketId

--Locations
Select Distinct DepCity=@DepCity,DepCityName=@DepCityName,DepCityNameL=@DepCityNameL,
  ArrCountry=Lc.Country,ArrCountryName=Lu.Name,ArrCountryNameL=Lu.NameL,
  ArrCity=Lc.City,ArrCityName=Lc.Name,ArrCityNameL=Lc.NameL  
From @Hotel P
Join Hotel H (NOLOCK) ON H.RecID=P.RecID    
Join @Location L ON H.Location=L.RecID
Join @Location Lc ON L.City=Lc.RecID
Join @Location Lu ON L.Country=Lu.RecID
Where Lc.Type=2
--Locations

--Category 
Select Distinct Code=H.Category,Name=HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID,@Market),HC.Name),
    Country=L.Country,PL_Country=L.Country,City=L.City,PL_City=L.City,Town=L.Town,Village=L.Village  
From @Hotel P 
Join Hotel H (NOLOCK) ON P.RecID=H.RecID
Join HotelCat HC (NOLOCK) ON HC.Code=H.Category     
Join @Location L ON H.Location=L.RecID
--Category

--HolPack 
Select Top 0 [Name],NameL='',HolPack='',DepCity=null,ArrCity=null,Country=null,CatPackID=null From Holpack (NOLOCK)
--HolPack 

";
            if (!Groups)
                tsql +=
@"
--Board
    Select Distinct Name=B.Name,NameL=isnull(dbo.FindLocalName(B.NameLID,@Market),B.Name),
         Hotel_Code=H.Code,B.Code,L.City,L.Town,L.Village,PLArrCity=L.City,PLDepCity=@DepCity
    From @Hotel P
    Join Hotel H (NOLOCK) ON H.RecID=P.RecID        
    Join HotelBoard B (NOLOCK) ON B.RecID=P.BoardID
    Join @Location L ON H.Location=L.RecID    
--Board

--Rooms 
   Select Distinct Name=R.Name,NameL=isnull(dbo.FindLocalName(R.NameLID,@Market),R.Name),
                  Hotel_Code=H.Code,R.Code,L.City,L.Town,L.Village,PLArrCity=L.City,PLDepCity=@DepCity,
				  R.RoomViewID        
    From @Hotel P 
    Join Hotel H (NOLOCK) ON P.RecID=H.RecID  
    Join HotelRoom R (NOLOCK) ON R.RecID=P.RoomID    
    Join @Location L ON H.Location=L.RecID    
--Rooms 
";
            else
                tsql +=
@"
--Board [Name],NameL,Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity 
	Select Name=BG.Name,NameL=isnull(dbo.FindLocalName(BG.NameLID,@Market),BG.Name),
	  Hotel_Code=H.Code,BG.Code,L.City,L.Town,L.Village,PLArrCity=L.City,PLDepCity=@DepCity
    From OHotelSalePrice P 
    Join Hotel H ON P.HotelID=H.RecID        
    Join HotelBoard B ON B.RecID=P.BoardID
    Join Location L ON H.Location=L.RecID    
	OUTER APPLY
    (
      Select GB.Code,GB.Name,GB.NameLID From GrpBoard GB 
      Join GrpBoardDet GBD ON GBD.Groups=GB.Code 
      Where GBD.Board=B.Code
    ) BG
    Where P.CheckIn>=@SaleDate    
	Group By BG.Name,BG.NameLID,H.Code,BG.Code,L.City,L.Town,L.Village
--Board 

--Rooms 
    Select Name=RG.Name,NameL=isnull(dbo.FindLocalName(RG.NameLID,@Market),RG.Name),
	  Hotel_Code=H.Code,RG.Code,L.City,L.Town,L.Village,PLArrCity=L.City,PLDepCity=@DepCity,
      R.RoomViewID	
    From OHotelSalePrice P 
    Join Hotel H ON P.HotelID=H.RecID    
    Join HotelRoom R ON R.RecID=P.RoomID    
    Join Location L ON H.Location=L.RecID    
	OUTER APPLY
    (
      Select RM.Code,RM.Name,RM.NameLID From GrpRoom RM
      Join GrpRoomDet RMD ON RMD.Groups=RM.Code
      Where RMD.Room=R.Code
    ) RG
    Where P.CheckIn>=@SaleDate
    Group By RG.Name,RG.NameLID,H.Code,RG.Code,L.City,L.Town,L.Village,R.RoomViewID	
--Rooms   
";
            tsql +=
@"
--Resort  
Select Distinct RecID=L.RecID,Name=L.Name,NameL, --=isnull(dbo.FindLocalName(L.NameLID,@Market),L.Name),
L.Country,L.City 
From @Hotel P
Join Hotel H (NOLOCK) ON P.RecID=H.RecID    
Join @Location L ON H.Location=L.RecID
--Resort

--Hotels 
Select Distinct H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),
  H.Code,H.Category,Location=L.RecID,L.Country,L.City,L.Town,ArrCity=L.City,CatPackID=null,Holpack=null
From @Hotel P
Join Hotel H (NOLOCK) ON P.RecID=H.RecID    
Join @Location L ON H.Location=L.RecID
--Hotels 

--Max Pax Count
Select Adult=8,ChdAgeG1=4,ChdAgeG2=4,ChdAgeG3=4,ChdAgeG4=4
--Max Pax Count

-- Room View
Select [Name],Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity 
From (     
  SELECT Hotel_Code=H.Code,Code=RW.RecID,RW.Name,L.City,L.Town,L.Village,         
    PLArrCity=L.City,PLDepCity=@DepCity
  From @Hotel P 
  Join Hotel H (NOLOCK) ON P.RecID=H.RecID    
  JOIN @Location L ON L.RecID=H.Location     
  JOIN HotelRoom HR (NOLOCK) ON HR.Hotel=H.Code     
  JOIN RoomView RW (NOLOCK) ON HR.RoomViewID=RW.RecID  
) Rooms 
Group By [Name],Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity 
-- Room View

";
            return tsql;
        }

        public MixSearchFilterCacheData getMixHotelSaleFilterCacheData(User UserData, bool Groups, SearchType searchType, List<Location> location, ref string errorMsg)
        {
            MixSearchFilterCacheData sPLData = new MixSearchFilterCacheData();
            List<MixPlCountry> mixCountry = new List<MixPlCountry>();
            List<MixCountry> countryList = getCountryList(UserData, ref errorMsg);

            bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
            string sqlStr = string.Empty;
            StringBuilder tSqlStr = new StringBuilder();
            if (searchType != SearchType.HotelSale)
            {
                tSqlStr = getPriceCacheDataForOnlyHotelMixSearch(UserData.Operator, UserData.Market, Groups, ref errorMsg);
                sqlStr = tSqlStr.ToString();
            }
            else
            {
                sqlStr = getPriceCacheDataForHotelSale(Groups, ref errorMsg);
            }
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(sqlStr);
            dbCommand.CommandTimeout = 120;
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                if (searchType == SearchType.HotelSale)
                    db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);

                DataSet ds = db.ExecuteDataSet(dbCommand);
                ds.Tables[0].TableName = "Locations";
                ds.Tables[1].TableName = "Category";
                ds.Tables[2].TableName = "HolPack";
                ds.Tables[3].TableName = "Board";
                ds.Tables[4].TableName = "Room";
                ds.Tables[5].TableName = "Resort";
                ds.Tables[6].TableName = "Hotels";
                ds.Tables[7].TableName = "MaxPaxCount";
                ds.Tables[8].TableName = "RoomView";

                sPLData = addMixPlLocation(ds, sPLData, useLocalName);
                sPLData = addMixPlCategory(ds, sPLData, useLocalName);
                sPLData = addMixPlHolPack(ds, sPLData, useLocalName);
                sPLData = addMixPlBoard(ds, sPLData, useLocalName);
                sPLData = addMixPlRoom(ds, sPLData, useLocalName);
                sPLData = addMixPlResort(ds, sPLData, useLocalName);
                sPLData = addMixPlHotel(ds, sPLData, location, useLocalName);
                sPLData = addMixPlMaxPax(ds, sPLData);
                sPLData = addMixPlRoomView(ds, sPLData, useLocalName);

                if (countryList != null && countryList.Count > 0)
                {
                    var countryQ = from q1 in sPLData.LocationList
                                   join q2 in location on q1.ArrCountry equals q2.Country
                                   group q2 by new { q2.CountryCode } into k
                                   select new { CountryCode = k.Key.CountryCode.ToString() };

                    foreach (MixCountry row in countryList)
                    {
                        if (countryQ.Where(w => w.CountryCode == row.ISO3 || w.CountryCode == row.ISO2).Count() > 0)
                        {
                            if (UserData.PaxSetting != null && UserData.PaxSetting.Pxm_DoNotShowOwnCntry.HasValue && UserData.PaxSetting.Pxm_DoNotShowOwnCntry.Value == false)
                                mixCountry.Add(new MixPlCountry()
                                {
                                    Code = row.ISO2,
                                    Name = row.Name,
                                    CountryType = mixCountryType.All
                                });
                            else
                                mixCountry.Add(new MixPlCountry()
                                {
                                    Code = row.ISO2,
                                    Name = row.Name,
                                    CountryType = mixCountryType.TourVisio
                                });
                        }
                        else
                        {
                            mixCountry.Add(new MixPlCountry()
                            {
                                Code = row.ISO2,
                                Name = row.Name,
                                CountryType = mixCountryType.Paximum
                            });
                        }
                    }
                }
                else
                {
                    var countryQ = from q1 in sPLData.LocationList
                                   join q2 in location on q1.ArrCountry equals q2.Country
                                   where q2.Type == 1
                                   group q2 by new { q2.Code, q2.Name } into k
                                   select new { CountryCode = k.Key.Code.ToString(), Name = k.Key.Name };

                    foreach (var row in countryQ)
                        mixCountry.Add(new MixPlCountry()
                        {
                            Code = row.CountryCode,
                            Name = row.Name,
                            CountryType = mixCountryType.TourVisio
                        });
                }

                sPLData.CountryList = mixCountry;

                return sPLData;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return sPLData;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public MixSearchFilterCacheData getMixSearchFilterCacheData(User UserData, bool Groups, SearchType searchType, string OnlyHotelVersion, ref string errorMsg)
        {
            MixSearchFilterCacheData sPLData = new MixSearchFilterCacheData();

            List<Location> location = new Locations().getLocationList(UserData.Market, LocationType.None, null, null, null, null, ref errorMsg);

            List<MixCountry> countryList = getCountryList(UserData, ref errorMsg);

            List<MixPlCountry> mixCountry = new List<MixPlCountry>();

            sPLData = getMixHotelSaleFilterCacheData(UserData, Groups, searchType, location, ref errorMsg);

            return sPLData;
        }

        public List<CodeName> getMixSearchWithOutCTHolPackCat(bool useMarketLang, MixSearchFilterCacheData plSearchData, ref string errorMsg)
        {
            return (from q in plSearchData.LocationList.AsEnumerable()
                    where q.HolPackCategory.HasValue
                    group q by new { Code = q.HolPackCategory, Name = q.HolPackCategoryName } into k
                    select new CodeName { Code = k.Key.Code.ToString(), Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<MixPlLocation> getMixSearchArrCitys(bool useMarketLang, MixSearchFilterCacheData plSearchData, int? DepCity, ref string errorMsg)
        {
            DepCity = DepCity.HasValue && DepCity > 0 ? DepCity : null;

            if (plSearchData == null || plSearchData.LocationList == null)
                return new List<MixPlLocation>();

            return (from q in plSearchData.LocationList
                    where (string.IsNullOrEmpty(DepCity.ToString()) || q.DepCity == DepCity)
                    select q).ToList<MixPlLocation>();
        }

        public List<CodeName> getMixSearchHolPack(bool useMarketLang, MixSearchFilterCacheData plSearchData, int? DepCity, int? Country, int? City, Int16? HolPackCat, ref string errorMsg)
        {
            DepCity = DepCity > 0 ? DepCity : null;
            Country = Country > 0 ? Country : null;
            City = City > 0 ? City : null;
            return (from q in plSearchData.HolPackList.AsEnumerable()
                    where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                    (string.IsNullOrEmpty(City.ToString()) || q.ArrCity == City) &&
                    (string.IsNullOrEmpty(DepCity.ToString()) || q.DepCity == DepCity) &&
                    ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                    group q by new { Code = q.HolPack, Name = useMarketLang ? q.NameL : q.Name } into k
                    select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }

        public List<LocationIDName> getMixSearchResorts(bool useMarketLang, MixSearchFilterCacheData plSearchData, int? Country, int? ArrCity, Int16? HolPackCat, ref string errorMsg)
        {
            Country = Country > 0 ? Country : null;
            ArrCity = ArrCity > 0 ? ArrCity : null;

            return (from q in plSearchData.ResortList.AsEnumerable()
                    where (string.IsNullOrEmpty(Country.ToString()) || q.Country == Country) &&
                    (string.IsNullOrEmpty(ArrCity.ToString()) || q.City == ArrCity) &&
                    ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                    !string.IsNullOrEmpty(q.Name)
                    orderby q.Name
                    group q by new { RecID = q.RecID, Name = q.Name } into k
                    select new LocationIDName { RecID = k.Key.RecID, Name = k.Key.Name, Type = 1 }).ToList<LocationIDName>();
        }

        public List<CodeName> getMixSearchCategorys(bool useMarketLang, MixSearchFilterCacheData plSearchData, int? Country, int? City, string Resort, Int16? HolPackCat, ref string errorMsg)
        {
            Country = Country > 0 ? Country : null;
            City = City > 0 ? City : null;
            if (string.IsNullOrEmpty(Resort))
            {
                List<CodeName> query1 = (from q in plSearchData.CategoryList.AsEnumerable()
                                         where (string.IsNullOrEmpty(Country.ToString()) || q.PLCountry == Country) &&
                                               (string.IsNullOrEmpty(City.ToString()) || q.PLCity == City) &&
                                               ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) ||
                                               (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                                         group q by new { Code = q.Code, Name = q.Name } into k
                                         select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                return query1;
            }
            else
            {
                var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                List<CodeName> query1 = (from q in plSearchData.CategoryList.AsEnumerable()
                                         join r in resort on (q.Village.HasValue ? q.Village.Value : q.Town) equals r
                                         where ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                                         group q by new { Code = q.Code, Name = q.Name } into k
                                         select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                return query1;
            }
        }

        public List<CodeHotelName> getMixSearchExtHotels(User UserData, int? PaxLocation, ref string errorMsg)
        {
            if (!PaxLocation.HasValue)
                return null;
            GetHotelDetailsByIdsResponse hotelList = new MixSearchPaximum().getHotelDetailsByIds(UserData, PaxLocation, ref errorMsg);
            List<CodeHotelName> query = (from q in hotelList.HotelDetails
                                         orderby q.Name
                                         select new CodeHotelName { Code = q.Id.ToString(), Name = q.Name, Category = q.Star.ToString(), LocationName = q.DestinationName }).ToList<CodeHotelName>();
            return query;
        }

        public List<CodeHotelName> getMixSearchHotels(bool useMarketLang, MixSearchFilterCacheData plSearchData, int? Location, int? Country, int? City, string Resort, string Category, string Holpack, Int16? HolPackCat, ref string errorMsg)
        {
            if (Location != null)
            {
                return (from q in plSearchData.HotelList.AsEnumerable()
                        where q.Location == Location &&
                              (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                              ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                        group q by new
                        {
                            Code = q.Code,
                            Name = useMarketLang ? q.NameL : q.Name,
                            Category = q.Category,
                            HLocationName = q.LocationName,
                            HLocationNameL = q.LocationName
                        } into k
                        orderby k.Key.Name
                        select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
            }
            else
            {
                if (Resort.IndexOf('|') > -1)
                {
                    Country = Country > 0 ? Country : null;
                    City = City > 0 ? City : null;
                    return (from q in plSearchData.HotelList.AsEnumerable()
                            where (string.IsNullOrEmpty(Holpack) || q.Holpack == Holpack) &&
                                  (!(HolPackCat.HasValue && q.HolPackCat.HasValue) || (HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value)) &&
                                  (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                                  (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Location.ToString()))
                            group q by new
                            {
                                Code = q.Code,
                                Name = useMarketLang ? q.NameL : q.Name,
                                Category = q.Category,
                                HLocationName = q.LocationName,
                                HLocationNameL = q.LocationName
                            } into k
                            orderby k.Key.Name
                            select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
                }
                else
                {
                    Country = Country > 0 ? Country : null;
                    City = City > 0 ? City : null;
                    if (City.HasValue && !string.IsNullOrEmpty(Resort) && City.ToString() == Resort)
                    {
                        return (from q in plSearchData.HotelList.AsEnumerable()
                                where (!Country.HasValue || q.Country == Country) &&
                                      (!City.HasValue || q.ArrCity == City) &&
                                      (string.IsNullOrEmpty(Holpack) || q.Holpack == Holpack) &&
                                      (!(HolPackCat.HasValue && q.HolPackCat.HasValue) || (HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value)) &&
                                      (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                                      (q.Town.HasValue && q.Town.Value == -1)
                                group q by new
                                {
                                    Code = q.Code,
                                    Name = useMarketLang ? q.NameL : q.Name,
                                    Category = q.Category,
                                    HLocationName = q.LocationName,
                                    HLocationNameL = q.LocationName
                                } into k
                                orderby k.Key.Name
                                select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
                    }
                    else
                    {
                        return (from q in plSearchData.HotelList.AsEnumerable()
                                where (!Country.HasValue || q.Country == Country) &&
                                      (!City.HasValue || q.ArrCity == City) &&
                                      (string.IsNullOrEmpty(Holpack) || q.Holpack == Holpack) &&
                                      (!(HolPackCat.HasValue && q.HolPackCat.HasValue) || (HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value)) &&
                                      (string.IsNullOrEmpty(Category) || Category.Split('|').Contains(q.Category)) &&
                                      (string.IsNullOrEmpty(Resort) || Resort == (q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new
                                {
                                    Code = q.Code,
                                    Name = useMarketLang ? q.NameL : q.Name,
                                    Category = q.Category,
                                    HLocationName = q.LocationName,
                                    HLocationNameL = q.LocationName
                                } into k
                                orderby k.Key.Name
                                select new CodeHotelName { Code = k.Key.Code, Name = k.Key.Name, Category = k.Key.Category, LocationName = k.Key.HLocationNameL }).ToList<CodeHotelName>();
                    }
                }
            }
        }

        public List<CodeName> getMixSearchRooms(bool useMarketLang, MixSearchFilterCacheData plSearchData, int? DepCity, int? ArrCity, string Resort, int? Village, string Hotel_Code, Int16? HolPackCat, string RoomViewID, ref string errorMsg)
        {
            var roomViewList = RoomViewID.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();

            if (string.IsNullOrEmpty(Hotel_Code))
            {
                List<Location> locations = TvBo.CacheObjects.getLocationList(string.Empty);
                Location loc = locations.Find(f => f.RecID == ArrCity);
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (!string.IsNullOrEmpty(Resort) && Resort.IndexOf('|') > -1)
                {
                    var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                    return (from q in plSearchData.RoomList
                            join rw in roomViewList on q.RoomViewID equals rw.Value
                            where ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                (roomViewList == null || roomViewList.Count == 0 || (roomViewList.Contains(q.RoomViewID)))
                            group q by new { Code = q.Code, Name = q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();

                }
                else
                {
                    if (Village.HasValue)
                    {
                        return (from q in plSearchData.RoomList
                                where q.Village == Village.Value &&
                                     ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                     (roomViewList == null || roomViewList.Count == 0 || (roomViewList.Contains(q.RoomViewID)))
                                group q by new { Code = q.Code, Name = q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    }
                    else
                    {
                        if (loc != null && loc.Parent == loc.RecID)
                            return (from q in plSearchData.RoomList
                                    join q1 in locations on q.PLArrCity equals q1.RecID
                                    where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                        (string.IsNullOrEmpty(ArrCity.ToString()) || q1.Country == ArrCity) &&
                                        ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                        (roomViewList == null || roomViewList.Count == 0 || (roomViewList.Contains(q.RoomViewID))) &&
                                        (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                    group q by new { Code = q.Code, Name = q.Name } into k
                                    select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                        else
                            return (from q in plSearchData.RoomList
                                    where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                    (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                                    ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                    (roomViewList == null || roomViewList.Count == 0 || (roomViewList.Contains(q.RoomViewID))) &&
                                    (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                    group q by new { Code = q.Code, Name = q.Name } into k
                                    select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    }
                }
            }
            else
            {
                return (from q in plSearchData.RoomList
                        where Hotel_Code.Split('|').Contains(q.Hotel_Code) &&
                             ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                             (roomViewList == null || roomViewList.Count == 0 || (roomViewList.Contains(q.RoomViewID)))
                        group q by new { Code = q.Code, Name = q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
        }

        public List<CodeName> getMixSearchRoomView(bool useMarketLang, MixSearchFilterCacheData plSearchData, int? DepCity, int? ArrCity, string Resort, int? Village, string Hotel_Code, Int16? HolPackCat, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                List<Location> locations = TvBo.CacheObjects.getLocationList(string.Empty);
                Location loc = locations.Find(f => f.RecID == ArrCity);
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (!string.IsNullOrEmpty(Resort) && Resort.IndexOf('|') > -1)
                {
                    var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                    return (from q in plSearchData.RoomViewList
                            join r in resort on (q.Village.HasValue ? q.Village.Value : q.Town) equals r
                            group q by new { Code = q.Code.ToString(), Name = q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
                else
                {
                    if (Village.HasValue)
                        return (from q in plSearchData.RoomViewList
                                where q.Village == Village.Value
                                group q by new { Code = q.Code, Name = q.Name } into k
                                select new CodeName { Code = k.Key.Code.ToString(), Name = k.Key.Name }).ToList<CodeName>();
                    else
                        if (loc != null && loc.Parent == loc.RecID)
                        return (from q in plSearchData.RoomViewList
                                join q1 in locations on q.PLArrCity equals q1.RecID
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q1.Country == ArrCity) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code.ToString(), Name = q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        return (from q in plSearchData.RoomViewList
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code.ToString(), Name = q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
            }
            else
            {
                return (from q in plSearchData.RoomViewList
                        where Hotel_Code.Split('|').Contains(q.Hotel_Code)
                        group q by new { Code = q.Code.ToString(), Name = q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
            }
        }

        public List<CodeName> getMixSearchBoards(bool useMarketLang, MixSearchFilterCacheData plSearchData, int? DepCity, int? ArrCity, string Resort, int? Village, string Hotel_Code, Int16? HolPackCat, ref string errorMsg)
        {
            if (string.IsNullOrEmpty(Hotel_Code))
            {
                List<Location> locations = TvBo.CacheObjects.getLocationList(string.Empty);
                Location loc = locations.Find(f => f.RecID == ArrCity);
                DepCity = DepCity > 0 ? DepCity : null;
                ArrCity = ArrCity > 0 ? ArrCity : null;
                if (!string.IsNullOrEmpty(Resort) && Resort.Split('|').Count() > 0)
                {
                    var resort = Resort.Split('|').Select(s => Conversion.getInt32OrNull(s)).ToList();
                    return (from q in plSearchData.BoardList.AsEnumerable()
                            join r in resort on (q.Village.HasValue ? q.Village.Value : q.Town) equals r
                            where ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                            group q by new { Code = q.Code, Name = q.Name } into k
                            select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
                else
                {
                    if (Village.HasValue)
                        return (from q in plSearchData.BoardList.AsEnumerable()
                                where q.Village == Village.Value &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                                group q by new { Code = q.Code, Name = q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        if (loc != null && loc.Parent == loc.RecID)
                        return (from q in plSearchData.BoardList.AsEnumerable()
                                join q1 in locations on q.PLArrCity equals q1.RecID
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q1.Country == ArrCity) &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code, Name = q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                    else
                        return (from q in plSearchData.BoardList.AsEnumerable()
                                where (string.IsNullOrEmpty(DepCity.ToString()) || q.PLDepCity == DepCity) &&
                                (string.IsNullOrEmpty(ArrCity.ToString()) || q.PLArrCity == ArrCity) &&
                                ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue)) &&
                                (string.IsNullOrEmpty(Resort) || Resort.Split('|').Contains(q.Town.HasValue ? q.Town.Value.ToString() : ""))
                                group q by new { Code = q.Code, Name = q.Name } into k
                                select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
                }
            }
            else
                return (from q in plSearchData.BoardList.AsEnumerable()
                        where Hotel_Code.Split('|').Contains(q.Hotel_Code) &&
                        ((HolPackCat.HasValue && q.HolPackCat == HolPackCat.Value) || (!HolPackCat.HasValue && !q.HolPackCat.HasValue))
                        group q by new { Code = q.Code, Name = q.Name } into k
                        select new CodeName { Code = k.Key.Code, Name = k.Key.Name }).ToList<CodeName>();
        }
    }

    public class MixSearchPaximum
    {
        public static byte[] Post(string uri, NameValueCollection pairs)
        {
            byte[] response = null;
            using (WebClient client = new WebClient())
            {
                response = client.UploadValues(uri, pairs);
            }
            return response;
        }

        public CountryListResponse getCountryList(User UserData, ref string errorMsg)
        {
            if (UserData.Pxm_Use && UserData.PaxToken != null && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value 
                && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton 
                && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode) && (!UserData.PaxSetting.Pxm_DoNotShowOwnCntry.HasValue || !UserData.PaxSetting.Pxm_DoNotShowOwnCntry.Value))
            {
                //if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode)) {
                string baseUrl = string.Format("{0}content/getcountrylist", UserData.PaxToken.paximumApiUrl);
                try
                {
                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.DownloadString(baseUrl);
                    CountryListResponse locationData = Newtonsoft.Json.JsonConvert.DeserializeObject<CountryListResponse>(response,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });

                    return locationData;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public GetDestinationListResponse getDestinationList(User UserData, string pCountryId, ref string errorMsg)
        {
            GetDestinationListRequest request = new GetDestinationListRequest();

            request.CountryId = pCountryId;
            request.Type = ""; //"City"; 21-08-2015

            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}content/getdestinationlist", UserData.PaxToken.paximumApiUrl);
                try
                {
                    //WebRequest
                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                    GetDestinationListResponse locationData = Newtonsoft.Json.JsonConvert.DeserializeObject<GetDestinationListResponse>(response,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });

                    return locationData;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public GetHotelDetailsByIdsResponse getHotelDetailsByIds(User UserData, int? pLocId, ref string errorMsg)
        {
            GetHotelDetailsByIds request = new GetHotelDetailsByIds();

            request.Ids = new string[] { pLocId.ToString() };
            request.IdType = GetHotelDetailsByIds.IdTypes.Destination;

            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}content/gethoteldetailsbyids", UserData.PaxToken.paximumApiUrl);
                try
                {
                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                    GetHotelDetailsByIdsResponse locationData = Newtonsoft.Json.JsonConvert.DeserializeObject<GetHotelDetailsByIdsResponse>(response,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });

                    return locationData;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        private class PaximumWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {

                HttpWebRequest w = base.GetWebRequest(uri) as HttpWebRequest;
                w.Timeout = 10 * 60 * 1000;
                w.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                return w;
            }
        }

        public SearchHotelsResponse searchHotel(User UserData, DestinationSearchCriteria[] pLocIds, List<MixSearchCriteriaRoom> rooms, DateTime pCheckIn, int pNight, string pCurrency, string pDefaultNationality, bool pAvailable)
        {
            string errorMsg = string.Empty;
            SearchHotelRequest request = new SearchHotelRequest();

            request.Destinations = pLocIds.Where(w => w.Type == "city").ToArray();

            List<RoomSearchCriteria> roomList = new List<RoomSearchCriteria>();
            foreach (MixSearchCriteriaRoom row in rooms)
            {
                RoomSearchCriteria room = new RoomSearchCriteria();
                room.Adults = row.AdultCount;
                List<int> childAgeList = new List<int>();
                foreach (var cRow in row.ChildAge)
                    childAgeList.Add(cRow);
                room.ChildrenAges = childAgeList.ToArray();
                roomList.Add(room);
            }
            //new RoomSearchCriteria { Adults = pAdl, ChildrenAges = pChildAges.ToArray() };
            Location defaultCountry = new Locations().getLocation(UserData.Market, new Locations().getLocationForCountry(UserData.AgencyRec.Location.HasValue ? UserData.AgencyRec.Location.Value : 0, ref errorMsg), ref errorMsg);
            List<MixCountry> paxCountries = new MixSearch().getCountryList(UserData, ref errorMsg);
            MixCountry paxCountry = paxCountries.Find(f => f.ISO3 == (defaultCountry != null ? defaultCountry.CountryCode : ""));
            request.Rooms = roomList.ToArray();
            request.CheckinDate = new DateTime(pCheckIn.Ticks, DateTimeKind.Utc);
            request.CheckoutDate = new DateTime(pCheckIn.AddDays(pNight).Ticks, DateTimeKind.Utc);
            request.Currency = string.IsNullOrEmpty(pCurrency) ? "EUR" : pCurrency;
            request.CustomerNationality = string.IsNullOrEmpty(pDefaultNationality) ? (paxCountry != null ? paxCountry.ISO2 : "TR") : pDefaultNationality;
            request.Language = "en";
            request.FilterUnavailable = pAvailable;
            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}search/hotels", UserData.PaxToken.paximumApiUrl);
                try
                {
                    string response = string.Empty;
                    using (PaximumWebClient client = new PaximumWebClient())
                    {
                        client.BaseAddress = baseUrl;

                        client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                        //client.Headers.Add("Accept-Encoding", "gzip,deflate");
                        client.Headers.Add("content-type", "application/json");
                        client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                        client.Encoding = Encoding.UTF8;

                        response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                    }
                    if (string.IsNullOrEmpty(response)) return null;
                    SearchHotelsResponse locationData = Newtonsoft.Json.JsonConvert.DeserializeObject<SearchHotelsResponse>(response,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });

                    Hotel[] allHotels = locationData.Hotels.Where(w => w.Offers != null && w.Offers.Any()).ToArray();

                    Hotel[] filterHotels = (from q in locationData.Hotels
                                            join q1 in pLocIds on q.Id equals q1.Id
                                            where q1.Type == "hotel"
                                            select q).ToArray();

                    locationData.Hotels = pLocIds.Any(w => w.Type == "hotel") ? filterHotels : allHotels;
                    locationData.Requests = request;
                    return locationData;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public GetHotelOfferDetailsResponse getOfferDetails(User UserData, string pOfferId, ref string errorMsg)
        {
            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string paximumApiUrl = UserData.PaxSetting.Pxm_APIAddr + (UserData.PaxSetting.Pxm_APIAddr.Substring(UserData.PaxSetting.Pxm_APIAddr.Length - 1, 1) == "/" ? "" : "/");
                string paximumAccess_Token = UserData.PaxSetting.Pxm_AutMainCode;

                string baseUrl = string.Format("{0}search/offerdetails", UserData.PaxToken.paximumApiUrl);
                try
                {
                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(new
                    {
                        offerId = pOfferId
                    }));
                    JsonSerializerSettings serializerSetting = new JsonSerializerSettings
                    {
                        ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                        NullValueHandling = NullValueHandling.Ignore,
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        ObjectCreationHandling = ObjectCreationHandling.Auto,
                        DateFormatHandling = DateFormatHandling.IsoDateFormat,
                        DateTimeZoneHandling = DateTimeZoneHandling.Utc
                    };
                    GetHotelOfferDetailsResponse offerData = Newtonsoft.Json.JsonConvert.DeserializeObject<GetHotelOfferDetailsResponse>(response, serializerSetting);

                    return offerData;
                }
                catch (WebException Ex)
                {
                    if (Ex.Status == WebExceptionStatus.ProtocolError)
                    {
                        HttpWebResponse response = (HttpWebResponse)Ex.Response;

                        if ((int)response.StatusCode == 500)
                        {
                            using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                            {
                                string result = sr.ReadToEnd();
                                OfferDetailException resultObj = Newtonsoft.Json.JsonConvert.DeserializeObject<OfferDetailException>(result);
                                errorMsg = resultObj.code.ToString();
                            }
                        }
                    }
                    else
                    {
                        errorMsg = Ex.Message;
                    }
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public CreateOrderResponse createOrder(User UserData, CreateOrder pRequest, ref string errorMsg)
        {
            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}booking/createorder", UserData.PaxToken.paximumApiUrl);
                try
                {
                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(pRequest)); //.DownloadString(baseUrl);                    
                    CreateOrderResponse locationData = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateOrderResponse>(response,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });
                    return locationData;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public GetPaymentMethodsByOrderIdResponse GetPaymentMethodsByOrderId(User UserData, string pOrderId, ref string errorMsg)
        {
            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}payment/GetPaymentMethodsByOrderId", UserData.PaxToken.paximumApiUrl);
                try
                {
                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(new { OrderId = pOrderId }));
                    GetPaymentMethodsByOrderIdResponse data = Newtonsoft.Json.JsonConvert.DeserializeObject<GetPaymentMethodsByOrderIdResponse>(response,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });
                    return data;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public string preAuth(User UserData, PreAuthRequest pRequest, ref string errorMsg)
        {
            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}payment/preauth", UserData.PaxToken.paximumApiUrl);
                try
                {
                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(pRequest));
                    apiResponse b2bToken = Newtonsoft.Json.JsonConvert.DeserializeObject<apiResponse>(response);
                    return b2bToken.token;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public MakeOrderResponse makeOrder(User UserData, string pToken, ref string errorMsg)
        {
            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}booking/makeorder", UserData.PaxToken.paximumApiUrl);
                try
                {
                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    makeOrderRequest pParams = new makeOrderRequest();
                    pParams.Token = pToken;
                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(pParams));
                    MakeOrderResponse data = Newtonsoft.Json.JsonConvert.DeserializeObject<MakeOrderResponse>(response,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });
                    return data;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public GetUserInformationResponse getUserInformation(User UserData, ref string errorMsg)
        {
            if (UserData.PaxSetting != null && UserData.PaxToken != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}profile/getuserinformation", UserData.PaxToken.paximumApiUrl);
                try
                {
                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.DownloadString(baseUrl);
                    GetUserInformationResponse data = Newtonsoft.Json.JsonConvert.DeserializeObject<GetUserInformationResponse>(response,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });

                    return data;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public HotelDetailsResponse getHotelDetais(User UserData, string pHotelId, ref string errorMsg)
        {
            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}search/hoteldetails", UserData.PaxToken.paximumApiUrl);
                try
                {
                    HotelDetailsRequest pRequest = new HotelDetailsRequest();
                    pRequest.HotelId = pHotelId;

                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(pRequest));
                    HotelDetailsResponse locationData = Newtonsoft.Json.JsonConvert.DeserializeObject<HotelDetailsResponse>(response.Replace("\r", "").Replace("\n", ""),
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });
                    return locationData;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public GetBookingMatchingByPaximumOrderIdResponse getBookingMatchingByPaximumOrderId(User UserData, string pPaximumOrderIds, ref string errorMsg)
        {
            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}tourvisio/getbookingmatchingbypaximumorderid", UserData.PaxToken.paximumApiUrl);
                try
                {
                    GetBookingMatchingByPaximumOrderIdRequest pRequest = new GetBookingMatchingByPaximumOrderIdRequest();
                    pRequest.PaximumOrderIds = new string[] { pPaximumOrderIds };

                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(pRequest));
                    GetBookingMatchingByPaximumOrderIdResponse locationData = Newtonsoft.Json.JsonConvert.DeserializeObject<GetBookingMatchingByPaximumOrderIdResponse>(response.Replace("\r", "").Replace("\n", ""),
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });
                    return locationData;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public GetBookingsByUserResponse getBookingsByUser(User UserData, string pBookingId, ref string errorMsg)
        {
            if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
            {
                string baseUrl = string.Format("{0}booking/getbookingsbyuser", UserData.PaxToken.paximumApiUrl);
                try
                {
                    GetBookingsByUserRequest pRequest = new GetBookingsByUserRequest();
                    pRequest.Criteria = new BookingCriteria();
                    pRequest.Criteria.OrderNumber = pBookingId;

                    WebClient client = new WebClient();
                    client.BaseAddress = baseUrl;
                    client.Headers.Add("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
                    client.Headers.Add("content-type", "application/json");
                    client.Headers.Add("Authorization", "Bearer " + UserData.PaxToken.token);
                    client.Encoding = Encoding.UTF8;

                    string response = client.UploadString(baseUrl, "POST", Newtonsoft.Json.JsonConvert.SerializeObject(pRequest));
                    GetBookingsByUserResponse locationData = Newtonsoft.Json.JsonConvert.DeserializeObject<GetBookingsByUserResponse>(response.Replace("\r", "").Replace("\n", ""),
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                            NullValueHandling = NullValueHandling.Ignore,
                            DefaultValueHandling = DefaultValueHandling.Ignore,
                            ObjectCreationHandling = ObjectCreationHandling.Auto,
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Utc
                        });
                    return locationData;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
