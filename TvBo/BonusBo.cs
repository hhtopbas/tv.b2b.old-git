﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class BonusDataRecord
    {                
        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }
        
        decimal? _Price;
        public decimal? Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        
        string _Curr;
        public string Curr
        {
            get { return _Curr; }
            set { _Curr = value; }
        }
        
        DateTime? _ExpireDate;
        public DateTime? ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }
        
        decimal? _Bonus;          
        public decimal? Bonus
        {
            get { return _Bonus; }
            set { _Bonus = value; }
        }

        decimal? _UsedBonus;
        public decimal? UsedBonus
        {
            get { return _UsedBonus; }
            set { _UsedBonus = value; }
        }

        decimal? _ExpBonus;
        public decimal? ExpBonus
        {
            get { return _ExpBonus; }
            set { _ExpBonus = value; }
        }        
    }

    public class UserBonusTotalRecord
    {
        public UserBonusTotalRecord()
        {
        }

        decimal? _TotBonus;
        public decimal? TotBonus
        {
            get { return _TotBonus; }
            set { _TotBonus = value; }
        }

        decimal? _TotBonusAmount;
        public decimal? TotBonusAmount
        {
            get { return _TotBonusAmount; }
            set { _TotBonusAmount = value; }
        }

        decimal? _UsedBonus;
        public decimal? UsedBonus
        {
            get { return _UsedBonus; }
            set { _UsedBonus = value; }
        }

        decimal? _UsedBonusAmount;
        public decimal? UsedBonusAmount
        {
            get { return _UsedBonusAmount; }
            set { _UsedBonusAmount = value; }
        }

        decimal? _UseableBonus;
        public decimal? UseableBonus
        {
            get { return _UseableBonus; }
            set { _UseableBonus = value; }
        }

        decimal? _UseableBonusAmount;
        public decimal? UseableBonusAmount
        {
            get { return _UseableBonusAmount; }
            set { _UseableBonusAmount = value; }
        }

        decimal? _TargetBonus;
        public decimal? TargetBonus
        {
            get { return _TargetBonus; }
            set { _TargetBonus = value; }
        }

        Int16? _ErrCode;
        public Int16? ErrCode
        {
            get { return _ErrCode; }
            set { _ErrCode = value; }
        }

    }

    public class AgencyBonusTotalRecord
    {
        public AgencyBonusTotalRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        decimal? _TotBonus;
        public decimal? TotBonus
        {
            get { return _TotBonus; }
            set { _TotBonus = value; }
        }

        decimal? _TotUsed;
        public decimal? TotUsed
        {
            get { return _TotUsed; }
            set { _TotUsed = value; }
        }

        decimal? _TotExpBonus;
        public decimal? TotExpBonus
        {
            get { return _TotExpBonus; }
            set { _TotExpBonus = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        int? _Location;
        public int? Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _Phone1;
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        string _Phone2;
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }

        string _Fax1;
        public string Fax1
        {
            get { return _Fax1; }
            set { _Fax1 = value; }
        }

        string _Fax2;
        public string Fax2
        {
            get { return _Fax2; }
            set { _Fax2 = value; }
        }

        string _Email1;
        public string Email1
        {
            get { return _Email1; }
            set { _Email1 = value; }
        }

        decimal? _TotUseable;
        public decimal? TotUseable
        {
            get { return _TotUseable; }
            set { _TotUseable = value; }
        }

        DateTime? _MinExpDate;
        public DateTime? MinExpDate
        {
            get { return _MinExpDate; }
            set { _MinExpDate = value; }
        }

    }

    public class UsedBonusRecord
    {
        public UsedBonusRecord()
        { 
        }

        decimal? _UsedBonus;
        public decimal? UsedBonus
        {
            get { return _UsedBonus; }
            set { _UsedBonus = value; }
        }

        decimal? _UsedBonusAmount;
        public decimal? UsedBonusAmount
        {
            get { return _UsedBonusAmount; }
            set { _UsedBonusAmount = value; }
        }

        int? _ErrCode;
        public int? ErrCode
        {
            get { return _ErrCode; }
            set { _ErrCode = value; }
        }
    }
}
