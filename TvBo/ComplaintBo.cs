﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class ComplaintRecord
    {
        public int? RecID { get; set; }
        public string ResNo { get; set; }
        public int? PasTrfID { get; set; }
        public int? GroupNo { get; set; }
        public string GuestName { get; set; }
        public int? Agency { get; set; }
        public int? TrfFrom { get; set; }
        public int? TrfTo { get; set; }
        public Int16? Status { get; set; }
        public int? Category { get; set; }
        public DateTime? ComplaintDate { get; set; }
        public Int16? ComplaintSource { get; set; }
        public string ComplaintExplain { get; set; }
        public string Investigation { get; set; }
        public string DepartmentAction { get; set; }
        public string ManagementAction { get; set; }
        public bool? Active { get; set; }        
        public int? DepartmentID { get; set; }
        public string ReporterName { get; set; }
        public int? Supplier { get; set; }
        public int? Hotel { get; set; }
        public Int16? Type { get; set; }
        public DateTime? LastInformToGuest { get; set; }
        public bool? GuestRight { get; set; }
        public string CompExplain { get; set; }
        public decimal? CompAmount { get; set; }
        public string CompAmountCur { get; set; }
        public Int16? CompTo { get; set; }
        public ComplaintRecord()
        {

        }
    }
    public class ComplaintList
    {
        public int? RecID { get; set; }
        public bool HaveAttach { get; set; }
        public int? DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentNameL { get; set; }
        public string HotelCode { get; set; }
        public string HotelName { get; set; }
        public string HotelNameL { get; set; }
        public int? ComplaintSource { get; set; }
        public string ComplaintSourceName { get; set; }
        public string ComplaintSourceNameL { get; set; }
        public int? Status { get; set; }
        public string StatusName { get; set; }
        public string StatusNameL { get; set; }
        public int? Category { get; set; }
        public string CategoryName { get; set; }
        public string CategoryNameL { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public bool? Active { get; set; }
        public Int16? ComplaintType { get; set; }
        public ComplaintList()
        {
            this.HaveAttach = false;
        }

        internal static void Add(ComplaintList complaintList)
        {
            throw new NotImplementedException();
        }
    }
    public class CompDepartment
    {
        public int? RecID { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public CompDepartment()
        {
        }
    }
    public class CompCategory
    {
        public int? RecID { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public int? CatType { get; set; }
        public CompCategory()
        {
        }
    }
    public class CompReporter
    {
        public int? RecID { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }        
        public CompReporter()
        {
        }
    }
    public class CompStatus
    {
        public int? RecID { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public CompStatus()
        {
        }
    }
}
