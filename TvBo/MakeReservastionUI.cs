﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    [Serializable()]
    public class resCustjSonData
    {
        public resCustjSonData()
        {
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        Int16? _Title;
        public Int16? Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _SurnameL;
        public string SurnameL
        {
            get { return _SurnameL; }
            set { _SurnameL = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Birtday;
        public string Birtday
        {
            get { return _Birtday; }
            set { _Birtday = value; }
        }

        string _Age;
        public string Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        string _IDNo;
        public string IDNo
        {
            get { return _IDNo; }
            set { _IDNo = value; }
        }

        string _PassSerie;
        public string PassSerie
        {
            get { return _PassSerie; }
            set { _PassSerie = value; }
        }

        string _PassNo;
        public string PassNo
        {
            get { return _PassNo; }
            set { _PassNo = value; }
        }

        string _PassIssueDate;
        public string PassIssueDate
        {
            get { return _PassIssueDate; }
            set { _PassIssueDate = value; }
        }

        string _PassExpDate;
        public string PassExpDate
        {
            get { return _PassExpDate; }
            set { _PassExpDate = value; }
        }

        string _PassGiven;
        public string PassGiven
        {
            get { return _PassGiven; }
            set { _PassGiven = value; }
        }

        string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        int? _Nation;
        public int? Nation
        {
            get { return _Nation; }
            set { _Nation = value; }
        }

        bool? _Passport;
        public bool? Passport
        {
            get { return _Passport; }
            set { _Passport = value; }
        }

        bool? _Leader;
        public bool? Leader
        {
            get { return _Leader; }
            set { _Leader = value; }
        }

    }

    public class resMainDivRecord
    {
        public resMainDivRecord()
        {
        }
        string _data; public string data { get { return _data; } set { _data = value; } }
        string _statusPromotion; public string statusPromotion { get { return _statusPromotion; } set { _statusPromotion = value; } }
        bool _divACE; public bool divACE { get { return _divACE; } set { _divACE = value; } }
        bool _divDiscountAgencyCom; public bool divDiscountAgencyCom { get { return _divDiscountAgencyCom; } set { _divDiscountAgencyCom = value; } }
        bool _divWhereInvoice; public bool divWhereInvoice { get { return _divWhereInvoice; } set { _divWhereInvoice = value; } }
        string _divSaveOptionDiv; public string divSaveOptionDiv { get { return _divSaveOptionDiv; } set { _divSaveOptionDiv = value; } }
        bool _divSaveOption; public bool divSaveOption { get { return _divSaveOption; } set { _divSaveOption = value; } }
        bool _divResNote; public bool divResNote { get { return _divResNote; } set { _divResNote = value; } }
        bool _divBonusID; public bool divBonusID { get { return _divBonusID; } set { _divBonusID = value; } }
    }
}
