﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class JournalRecord
    {
        public JournalRecord()
        {
        }

        Int32? _RecID;
        public Int32? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Office;
        public string Office
        {
            get { return _Office; }
            set { _Office = value; }
        }

        string _CP;
        public string CP
        {
            get { return _CP; }
            set { _CP = value; }
        }

        Int16? _AccountType;
        public Int16? AccountType
        {
            get { return _AccountType; }
            set { _AccountType = value; }
        }

        string _Account;
        public string Account
        {
            get { return _Account; }
            set { _Account = value; }
        }

        DateTime? _PayDate;
        public DateTime? PayDate
        {
            get { return _PayDate; }
            set { _PayDate = value; }
        }

        string _PayType;
        public string PayType
        {
            get { return _PayType; }
            set { _PayType = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        decimal? _Allocated;
        public decimal? Allocated
        {
            get { return _Allocated; }
            set { _Allocated = value; }
        }

        string _OppAccount;
        public string OppAccount
        {
            get { return _OppAccount; }
            set { _OppAccount = value; }
        }

        string _InvoiceSerial;
        public string InvoiceSerial
        {
            get { return _InvoiceSerial; }
            set { _InvoiceSerial = value; }
        }

        Int32? _InvoiceNo;
        public Int32? InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        DateTime? _InvoiceDate;
        public DateTime? InvoiceDate
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }

        string _ReceiptSerial;
        public string ReceiptSerial
        {
            get { return _ReceiptSerial; }
            set { _ReceiptSerial = value; }
        }

        Int32? _ReceiptNo;
        public Int32? ReceiptNo
        {
            get { return _ReceiptNo; }
            set { _ReceiptNo = value; }
        }

        DateTime? _ReceiptDate;
        public DateTime? ReceiptDate
        {
            get { return _ReceiptDate; }
            set { _ReceiptDate = value; }
        }

        string _ReceiptPrt;
        public string ReceiptPrt
        {
            get { return _ReceiptPrt; }
            set { _ReceiptPrt = value; }
        }

        string _AllocStat;
        public string AllocStat
        {
            get { return _AllocStat; }
            set { _AllocStat = value; }
        }

        decimal? _FixExRate;
        public decimal? FixExRate
        {
            get { return _FixExRate; }
            set { _FixExRate = value; }
        }

        string _FixExCur;
        public string FixExCur
        {
            get { return _FixExCur; }
            set { _FixExCur = value; }
        }

        string _Locked;
        public string Locked
        {
            get { return _Locked; }
            set { _Locked = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        decimal? _BankComPerc;
        public decimal? BankComPerc
        {
            get { return _BankComPerc; }
            set { _BankComPerc = value; }
        }

        decimal? _PayAmount;
        public decimal? PayAmount
        {
            get { return _PayAmount; }
            set { _PayAmount = value; }
        }

        bool? _SendToAcc;
        public bool? SendToAcc
        {
            get { return _SendToAcc; }
            set { _SendToAcc = value; }
        }

        string _UseInvRate;
        public string UseInvRate
        {
            get { return _UseInvRate; }
            set { _UseInvRate = value; }
        }

        decimal? _Remain;
        public decimal? Remain
        {
            get { return _Remain; }
            set { _Remain = value; }
        }

        Int16? _RecType;
        public Int16? RecType
        {
            get { return _RecType; }
            set { _RecType = value; }
        }

        decimal? _InvPaidRate;
        public decimal? InvPaidRate
        {
            get { return _InvPaidRate; }
            set { _InvPaidRate = value; }
        }

        decimal? _InvPaidAmount;
        public decimal? InvPaidAmount
        {
            get { return _InvPaidAmount; }
            set { _InvPaidAmount = value; }
        }

        string _CCNo;
        public string CCNo
        {
            get { return _CCNo; }
            set { _CCNo = value; }
        }

        Int32? _CCTypeID;
        public Int32? CCTypeID
        {
            get { return _CCTypeID; }
            set { _CCTypeID = value; }
        }

        string _CCBank;
        public string CCBank
        {
            get { return _CCBank; }
            set { _CCBank = value; }
        }

        string _CCExpDate;
        public string CCExpDate
        {
            get { return _CCExpDate; }
            set { _CCExpDate = value; }
        }

        string _CCSecNo;
        public string CCSecNo
        {
            get { return _CCSecNo; }
            set { _CCSecNo = value; }
        }

        Int16? _InstalNum;
        public Int16? InstalNum
        {
            get { return _InstalNum; }
            set { _InstalNum = value; }
        }

        Int16? _PosType;
        public Int16? PosType
        {
            get { return _PosType; }
            set { _PosType = value; }
        }

        Int32? _PayNo;
        public Int32? PayNo
        {
            get { return _PayNo; }
            set { _PayNo = value; }
        }

        Int32? _TrfNo;
        public Int32? TrfNo
        {
            get { return _TrfNo; }
            set { _TrfNo = value; }
        }

        string _TaxAccNo;
        public string TaxAccNo
        {
            get { return _TaxAccNo; }
            set { _TaxAccNo = value; }
        }

        string _CrtAgency;
        public string CrtAgency
        {
            get { return _CrtAgency; }
            set { _CrtAgency = value; }
        }

        string _ChgAgency;
        public string ChgAgency
        {
            get { return _ChgAgency; }
            set { _ChgAgency = value; }
        }

        byte? _Resource;
        public byte? Resource
        {
            get { return _Resource; }
            set { _Resource = value; }
        }

        string _ClientName;
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        string _ClientAddress;
        public string ClientAddress
        {
            get { return _ClientAddress; }
            set { _ClientAddress = value; }
        }

        string _ClientZip;
        public string ClientZip
        {
            get { return _ClientZip; }
            set { _ClientZip = value; }
        }

        string _ClientCity;
        public string ClientCity
        {
            get { return _ClientCity; }
            set { _ClientCity = value; }
        }

        string _ClientCountry;
        public string ClientCountry
        {
            get { return _ClientCountry; }
            set { _ClientCountry = value; }
        }

        string _CCHolder;
        public string CCHolder
        {
            get { return _CCHolder; }
            set { _CCHolder = value; }
        }

        string _TaxOffice;
        public string TaxOffice
        {
            get { return _TaxOffice; }
            set { _TaxOffice = value; }
        }

        string _Reference;
        public string Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        string _Comment;
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        string _IntNote;
        public string IntNote
        {
            get { return _IntNote; }
            set { _IntNote = value; }
        }

        DateTime? _PayTime;
        public DateTime? PayTime
        {
            get { return _PayTime; }
            set { _PayTime = value; }
        }

    }

    public class JournalCDetRecord
    {
        public JournalCDetRecord()
        {
        }

        Int32 _RecID;
        public Int32 RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int32 _JournalID;
        public Int32 JournalID
        {
            get { return _JournalID; }
            set { _JournalID = value; }
        }

        Int16 _PayOrder;
        public Int16 PayOrder
        {
            get { return _PayOrder; }
            set { _PayOrder = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        decimal? _PaidAmount;
        public decimal? PaidAmount
        {
            get { return _PaidAmount; }
            set { _PaidAmount = value; }
        }

        string _PaidCur;
        public string PaidCur
        {
            get { return _PaidCur; }
            set { _PaidCur = value; }
        }

        decimal? _CurRate;
        public decimal? CurRate
        {
            get { return _CurRate; }
            set { _CurRate = value; }
        }

        decimal? _ResPaidAmount;
        public decimal? ResPaidAmount
        {
            get { return _ResPaidAmount; }
            set { _ResPaidAmount = value; }
        }

        string _ResPaidCur;
        public string ResPaidCur
        {
            get { return _ResPaidCur; }
            set { _ResPaidCur = value; }
        }

        DateTime? _AllocDate;
        public DateTime? AllocDate
        {
            get { return _AllocDate; }
            set { _AllocDate = value; }
        }

        string _AllocUser;
        public string AllocUser
        {
            get { return _AllocUser; }
            set { _AllocUser = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

    }

    public class JournalCInstRecord
    {
        public JournalCInstRecord()
        {
        }

        Int32 _RecID;
        public Int32 RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int32? _JournalID;
        public Int32? JournalID
        {
            get { return _JournalID; }
            set { _JournalID = value; }
        }

        Int32? _PayTypeID;
        public Int32? PayTypeID
        {
            get { return _PayTypeID; }
            set { _PayTypeID = value; }
        }

        Int16? _InsNo;
        public Int16? InsNo
        {
            get { return _InsNo; }
            set { _InsNo = value; }
        }

        DateTime? _InsDate;
        public DateTime? InsDate
        {
            get { return _InsDate; }
            set { _InsDate = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

    }

    public class JournalPDetRecord
    {
        public JournalPDetRecord()
        {
        }

        Int32 _RecID;
        public Int32 RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int32 _JournalID;
        public Int32 JournalID
        {
            get { return _JournalID; }
            set { _JournalID = value; }
        }

        Int16? _PayOrder;
        public Int16? PayOrder
        {
            get { return _PayOrder; }
            set { _PayOrder = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int32? _ResSerID;
        public Int32? ResSerID
        {
            get { return _ResSerID; }
            set { _ResSerID = value; }
        }

        decimal? _PaidAmount;
        public decimal? PaidAmount
        {
            get { return _PaidAmount; }
            set { _PaidAmount = value; }
        }

        string _PaidCur;
        public string PaidCur
        {
            get { return _PaidCur; }
            set { _PaidCur = value; }
        }

        decimal? _SerPaidAmount;
        public decimal? SerPaidAmount
        {
            get { return _SerPaidAmount; }
            set { _SerPaidAmount = value; }
        }

        string _SerPaidCur;
        public string SerPaidCur
        {
            get { return _SerPaidCur; }
            set { _SerPaidCur = value; }
        }

        decimal? _CurRate;
        public decimal? CurRate
        {
            get { return _CurRate; }
            set { _CurRate = value; }
        }

        DateTime? _AllocDate;
        public DateTime? AllocDate
        {
            get { return _AllocDate; }
            set { _AllocDate = value; }
        }

        string _AllocUser;
        public string AllocUser
        {
            get { return _AllocUser; }
            set { _AllocUser = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

    }

    public class InvoiceRecord
    {
        public InvoiceRecord() { }
        int? _RecID; public int? RecID { get { return _RecID; } set { _RecID = value; } }
        string _Operator; public string Operator { get { return _Operator; } set { _Operator = value; } }
        string _Office; public string Office { get { return _Office; } set { _Office = value; } }
        string _InvSerial; public string InvSerial { get { return _InvSerial; } set { _InvSerial = value; } }
        int? _InvNo; public int? InvNo { get { return _InvNo; } set { _InvNo = value; } }
        DateTime? _InvDate; public DateTime? InvDate { get { return _InvDate; } set { _InvDate = value; } }
        Int16? _InvType; public Int16? InvType { get { return _InvType; } set { _InvType = value; } }
        Int16? _SumType; public Int16? SumType { get { return _SumType; } set { _SumType = value; } }
        byte? _ClientType; public byte? ClientType { get { return _ClientType; } set { _ClientType = value; } }
        string _ClientCode; public string ClientCode { get { return _ClientCode; } set { _ClientCode = value; } }
        string _ClientName; public string ClientName { get { return _ClientName; } set { _ClientName = value; } }
        string _ClientAddress; public string ClientAddress { get { return _ClientAddress; } set { _ClientAddress = value; } }
        string _ClientZip; public string ClientZip { get { return _ClientZip; } set { _ClientZip = value; } }
        string _ClientCity; public string ClientCity { get { return _ClientCity; } set { _ClientCity = value; } }
        string _ClientCountry; public string ClientCountry { get { return _ClientCountry; } set { _ClientCountry = value; } }
        bool? _Proforma; public bool? Proforma { get { return _Proforma; } set { _Proforma = value; } }
        decimal? _InvAmount; public decimal? InvAmount { get { return _InvAmount; } set { _InvAmount = value; } }
        string _InvCur; public string InvCur { get { return _InvCur; } set { _InvCur = value; } }
        decimal? _Tax; public decimal? Tax { get { return _Tax; } set { _Tax = value; } }
        DateTime? _DueDate; public DateTime? DueDate { get { return _DueDate; } set { _DueDate = value; } }
        string _Language; public string Language { get { return _Language; } set { _Language = value; } }
        string _Explanation; public string Explanation { get { return _Explanation; } set { _Explanation = value; } }
        bool? _InBatch; public bool? InBatch { get { return _InBatch; } set { _InBatch = value; } }
        decimal? _ResAmount; public decimal? ResAmount { get { return _ResAmount; } set { _ResAmount = value; } }
        string _ResCur; public string ResCur { get { return _ResCur; } set { _ResCur = value; } }
        decimal? _Rate; public decimal? Rate { get { return _Rate; } set { _Rate = value; } }
        DateTime? _RateDate; public DateTime? RateDate { get { return _RateDate; } set { _RateDate = value; } }
        Int16? _Status; public Int16? Status { get { return _Status; } set { _Status = value; } }
        string _ReceiptSerial; public string ReceiptSerial { get { return _ReceiptSerial; } set { _ReceiptSerial = value; } }
        int? _ReceiptNo; public int? ReceiptNo { get { return _ReceiptNo; } set { _ReceiptNo = value; } }
        string _SendToAcc; public string SendToAcc { get { return _SendToAcc; } set { _SendToAcc = value; } }
        DateTime? _SendToAccDate; public DateTime? SendToAccDate { get { return _SendToAccDate; } set { _SendToAccDate = value; } }
        string _SendToAccUser; public string SendToAccUser { get { return _SendToAccUser; } set { _SendToAccUser = value; } }
        int? _FormID; public int? FormID { get { return _FormID; } set { _FormID = value; } }
        string _Note; public string Note { get { return _Note; } set { _Note = value; } }
        DateTime? _CrtDate; public DateTime? CrtDate { get { return _CrtDate; } set { _CrtDate = value; } }
        string _CrtUser; public string CrtUser { get { return _CrtUser; } set { _CrtUser = value; } }
        string _BankName; public string BankName { get { return _BankName; } set { _BankName = value; } }
        string _BankNo; public string BankNo { get { return _BankNo; } set { _BankNo = value; } }
        string _BankLocation; public string BankLocation { get { return _BankLocation; } set { _BankLocation = value; } }
        string _BankAccNo; public string BankAccNo { get { return _BankAccNo; } set { _BankAccNo = value; } }
        string _BankCur; public string BankCur { get { return _BankCur; } set { _BankCur = value; } }
        string _BankIBAN; public string BankIBAN { get { return _BankIBAN; } set { _BankIBAN = value; } }
        string _TaxOffice; public string TaxOffice { get { return _TaxOffice; } set { _TaxOffice = value; } }
        string _TaxAccNo; public string TaxAccNo { get { return _TaxAccNo; } set { _TaxAccNo = value; } }
        string _Phone1; public string Phone1 { get { return _Phone1; } set { _Phone1 = value; } }
        string _Phone2; public string Phone2 { get { return _Phone2; } set { _Phone2 = value; } }
        string _Fax1; public string Fax1 { get { return _Fax1; } set { _Fax1 = value; } }
        string _Fax2; public string Fax2 { get { return _Fax2; } set { _Fax2 = value; } }
        string _email1; public string email1 { get { return _email1; } set { _email1 = value; } }
        string _email2; public string email2 { get { return _email2; } set { _email2 = value; } }
        decimal? _PaidAmount; public decimal? PaidAmount { get { return _PaidAmount; } set { _PaidAmount = value; } }
        Int16? _InvImageType; public Int16? InvImageType { get { return _InvImageType; } set { _InvImageType = value; } }
        string _CrtAgency; public string CrtAgency { get { return _CrtAgency; } set { _CrtAgency = value; } }
    }

    public class InvoiceResRecord
    {
        public InvoiceResRecord() { }
        int? _RecID; public int? RecID { get { return _RecID; } set { _RecID = value; } }
        int? _InvoiceID; public int? InvoiceID { get { return _InvoiceID; } set { _InvoiceID = value; } }
        string _ResNo; public string ResNo { get { return _ResNo; } set { _ResNo = value; } }
        decimal? _ResInvAmount; public decimal? ResInvAmount { get { return _ResInvAmount; } set { _ResInvAmount = value; } }
        decimal? _ResInvAmountPrt; public decimal? ResInvAmountPrt { get { return _ResInvAmountPrt; } set { _ResInvAmountPrt = value; } }
    }

    public class PaymentPageDefination
    {
        public PaymentPageDefination()
        {
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _B2BUrl;
        public string B2BUrl
        {
            get { return _B2BUrl; }
            set { _B2BUrl = value; }
        }

        string _B2CUrl;
        public string B2CUrl
        {
            get { return _B2CUrl; }
            set { _B2CUrl = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

    }

    public class PaymentTypeData
    {
        public PaymentTypeData()
        {
        }

        int? _PayTypeID;
        public int? PayTypeID
        {
            get { return _PayTypeID; }
            set { _PayTypeID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        int? _PayCatID;
        public int? PayCatID
        {
            get { return _PayCatID; }
            set { _PayCatID = value; }
        }

        Int16? _PosType;
        public Int16? PosType
        {
            get { return _PosType; }
            set { _PosType = value; }
        }

        string _PayCatName;
        public string PayCatName
        {
            get { return _PayCatName; }
            set { _PayCatName = value; }
        }

        Int16 _InstalNumFrom;
        public Int16 InstalNumFrom
        {
            get { return _InstalNumFrom; }
            set { _InstalNumFrom = value; }
        }

        Int16 _InstalNumTo;
        public Int16 InstalNumTo
        {
            get { return _InstalNumTo; }
            set { _InstalNumTo = value; }
        }

        int? _CreditCardID;
        public int? CreditCardID
        {
            get { return _CreditCardID; }
            set { _CreditCardID = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _PayCur;
        public string PayCur
        {
            get { return _PayCur; }
            set { _PayCur = value; }
        }

        string _SupDisCode;
        public string SupDisCode
        {
            get { return _SupDisCode; }
            set { _SupDisCode = value; }
        }

        decimal? _DiscountPer;
        public decimal? DiscountPer
        {
            get { return _DiscountPer; }
            set { _DiscountPer = value; }
        }

        string _CreditCardName;
        public string CreditCardName
        {
            get { return _CreditCardName; }
            set { _CreditCardName = value; }
        }

        string _CreditCardCode;
        public string CreditCardCode
        {
            get { return _CreditCardCode; }
            set { _CreditCardCode = value; }
        }

        decimal? _ResPayMinPer;
        public decimal? ResPayMinPer
        {
            get { return _ResPayMinPer; }
            set { _ResPayMinPer = value; }
        }

        string _WPosBank;
        public string WPosBank
        {
            get { return _WPosBank; }
            set { _WPosBank = value; }
        }

        bool _isPicture;
        public bool isPicture
        {
            get { return _isPicture; }
            set { _isPicture = value; }
        }
    }

    public class PaymentInfo
    {
        public PaymentInfo()
        {
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        int? _PriceListNo;
        public int? PriceListNo
        {
            get { return _PriceListNo; }
            set { _PriceListNo = value; }
        }

        decimal? _DiscountAmount;
        public decimal? DiscountAmount
        {
            get { return _DiscountAmount; }
            set { _DiscountAmount = value; }
        }

        decimal? _ResAmount;
        public decimal? ResAmount
        {
            get { return _ResAmount; }
            set { _ResAmount = value; }
        }

        decimal? _PasAmount;
        public decimal? PasAmount
        {
            get { return _PasAmount; }
            set { _PasAmount = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        DateTime? _OptionTill;
        public DateTime? OptionTill
        {
            get { return _OptionTill; }
            set { _OptionTill = value; }
        }

        string _ConfStat;
        public string ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        Int16? _RConfStat;
        public Int16? RConfStat
        {
            get { return _RConfStat; }
            set { _RConfStat = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        DateTime? _ResBegDate;
        public DateTime? ResBegDate
        {
            get { return _ResBegDate; }
            set { _ResBegDate = value; }
        }

        Int16? _Night;
        public Int16? Night
        {
            get { return _Night; }
            set { _Night = value; }
        }

        decimal? _PrePayment;
        public decimal? PrePayment
        {
            get { return _PrePayment; }
            set { _PrePayment = value; }
        }

        decimal? _PrePerVal;
        public decimal? PrePerVal
        {
            get { return _PrePerVal; }
            set { _PrePerVal = value; }
        }

        decimal? _Remain;
        public decimal? Remain
        {
            get { return _Remain; }
            set { _Remain = value; }
        }

        Int16? _InstalNum;
        public Int16? InstalNum
        {
            get { return _InstalNum; }
            set { _InstalNum = value; }
        }

        byte? _PasPayRule;
        public byte? PasPayRule
        {
            get { return _PasPayRule; }
            set { _PasPayRule = value; }
        }
    }

    public class CreditCardAndDetail
    {
        public CreditCardAndDetail()
        {
            _card_Detail = new List<CardDetail>();
            _cardList = new List<Cards>();
        }

        List<CardDetail> _card_Detail;

        public List<CardDetail> card_Detail
        {
            get { return _card_Detail; }
            set { _card_Detail = value; }
        }

        List<Cards> _cardList;
        public List<Cards> CardList
        {
            get { return _cardList; }
            set { _cardList = value; }
        }
    }

    public class CardDetail
    {
        public CardDetail()
        {
            _Selected = false;
        }

        int? _PayTypeID;
        public int? PayTypeID
        {
            get { return _PayTypeID; }
            set { _PayTypeID = value; }
        }

        int? _PayCatID;
        public int? PayCatID
        {
            get { return _PayCatID; }
            set { _PayCatID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _FullName;
        public string FullName
        {
            get { return _FullName; }
            set { _FullName = value; }
        }

        int? _CreditCardID;
        public int? CreditCardID
        {
            get { return _CreditCardID; }
            set { _CreditCardID = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        int? _InstalNum;
        public int? InstalNum
        {
            get { return _InstalNum; }
            set { _InstalNum = value; }
        }

        decimal? _DiscountPrice;
        public decimal? DiscountPrice
        {
            get { return _DiscountPrice; }
            set { _DiscountPrice = value; }
        }

        string _Currency;
        public string Currency
        {
            get { return _Currency; }
            set { _Currency = value; }
        }

        string _SupDisCode;
        public string SupDisCode
        {
            get { return _SupDisCode; }
            set { _SupDisCode = value; }
        }

        string _WPosBank;
        public string WPosBank
        {
            get { return _WPosBank; }
            set { _WPosBank = value; }
        }

        decimal? _MinPayPrice;
        public decimal? MinPayPrice
        {
            get { return _MinPayPrice; }
            set { _MinPayPrice = value; }
        }

        int? _PosType;
        public int? PosType
        {
            get { return _PosType; }
            set { _PosType = value; }
        }

        bool _Selected;
        public bool Selected
        {
            get { return _Selected; }
            set { _Selected = value; }
        }

        decimal? _PrePayment;
        public decimal? PrePayment
        {
            get { return _PrePayment; }
            set { _PrePayment = value; }
        }
    }

    public class Cards
    {
        public Cards()
        {
        }

        int? _CreditCardID;
        public int? CreditCardID
        {
            get { return _CreditCardID; }
            set { _CreditCardID = value; }
        }

        string _CreditCardName;
        public string CreditCardName
        {
            get { return _CreditCardName; }
            set { _CreditCardName = value; }
        }
    }    

    [Serializable()]
    public class ReservationInvoice
    {
        public int? RecID { get; set; }
        public string InvoiceNo { get; set; }
        public string InvSerial { get; set; }
        public string InvNo { get; set; }
        public DateTime? InvDate { get; set; }
        public decimal? InvAmount { get; set; }
        public string InvCurr { get; set; }
        public bool? Status { get; set; }
        public ReservationInvoice()
        {
        }
    }

    [Serializable()]
    public class NovaGatewayApi
    {
        public string Market { get; set; }
        public string ProjectId { get; set; }
        public string Secret { get; set; }
        public string ApiKey { get; set; }
        public string ResponseConfirmMessage { get; set; }
        public string DefaultLangCode { get; set; }
        public string BankLink { get; set; }
        public NovaGatewayApi()
        {
        }
    }

    [Serializable()]
    public class NovaBankMaping
    {
        public string Market { get; set; }
        public string NovaCode { get; set; }
        public string TvCode { get; set; }
        public NovaBankMaping()
        {
        }
    }

    [Serializable()]
    public class NovaGatewayCurreny
    {
        public string currencyId { get; set; }
        public string name { get; set; }
        public NovaGatewayCurreny()
        {
        }
    }

    [Serializable()]
    public class NovaGatewayPaymentDetail
    {
        public string paymentId { get; set; }
        public string name { get; set; }
        public int? limitAmount { get; set; }
        public List<NovaGatewayCurreny> currencies { get; set; }
        public List<string> languages { get; set; }
        public int? orderWeight { get; set; }
        public NovaGatewayPaymentDetail()
        {
        }
    }

    [Serializable()]
    public class NovaGatewayPayment
    {
        public List<NovaGatewayPaymentDetail> payment { get; set; }
        public NovaGatewayPayment()
        {
        }
    }

    [Serializable()]
    public class NovaGatewayPaymentResult
    {
        public string status { get; set; }
        public NovaGatewayPayment payments { get; set; }
        public NovaGatewayPaymentResult()
        {
        }
    }

    [Serializable()]
    public class NovaSelectedReservastion
    {
        public string ResNo { get; set; }
        public decimal? Payment { get; set; }        
        public NovaSelectedReservastion()
        {
        }
    }

    [Serializable()]
    public class NovaBeforePament
    {
        public int? RefNo { get; set; }
        public string AgencyID { get; set; }
        public string PaymentId { get; set; }
        public string Curr { get; set; }        
        public decimal? TotalAmount { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool NovaSuccess { get; set; }
        public DateTime? NovaSuccessDate { get; set; }
        public bool TvSuccess { get; set; }
        public DateTime? TvSuccessDate { get; set; }
        public int? TvRefNo { get; set; }
        public List<AgencyPayList> PaymentResList { get; set; }
        public int? PayTypeID { get; set; }

        public NovaBeforePament()
        {
            this.PaymentResList = new List<AgencyPayList>();
            this.NovaSuccess = false;
            this.TvSuccess = false;
        }
    }

    [Serializable()]
    public class NovaResponceService
    {
        public string VK_SERVICE { get; set; }
        public string VK_TRANSACTION_ID { get; set; }
        public string VK_REF { get; set; }
        public string VK_REF_UNIQUE { get; set; }
        public string VK_PAYMENT_ID { get; set; }
        public string VK_PAYMENT_NAME { get; set; }
        public string VK_CURR { get; set; }
        public string VK_AMOUNT { get; set; }
        public string VK_SND_ACC { get; set; }
        public string VK_SND_NAME { get; set; }
        public string VK_PARTNER_ID { get; set; }
        public string VK_MAC { get; set; }
        public string VK_LANG { get; set; }
        public string VK_AUTO { get; set; }

        public NovaResponceService()
        {
        }
    }
}
