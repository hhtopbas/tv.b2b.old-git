﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace TvBo
{
    public enum NameWrittingRuleTypes { nameWrittingRules = 0, surnameWrittingRules = 1 };

    public enum PaximumWorkingType { UsePaximumButtonAndInline = 0, UsePaximumButton = 1, UsePaximumInline = 2 }

    [Serializable()]
    public class DDLData
    {
        public DDLData()
        {
        }

        string _FieldData;
        public string FieldData
        {
            get { return _FieldData; }
            set { _FieldData = value; }
        }

        string _TextData;
        public string TextData
        {
            get { return _TextData; }
            set { _TextData = value; }
        }
    }

    [Serializable()]
    public class MainMenu
    {
        public int IDNo { get; set; }
        public bool SubMenu { get; set; }
        public string MenuItem { get; set; }
        public string SubMenuItem { get; set; }
        public string ResourceName { get; set; }
        public string MenuResource { get; set; }
        public bool Disable { get; set; }
        public string Url { get; set; }
        public bool ResNo { get; set; }
        public string NoShow { get; set; }
        public bool Selected { get; set; }
        public bool ShowAllUser { get; set; }
        public bool OnlyMainAgency { get; set; }
        public int? HolPackCategory { get; set; }
        public string PackageType { get; set; }
        public string B2BMenuCat { get; set; }
        public string PageExt { get; set; }
        public string QueryStr { get; set; }
        public bool External { get; set; }
        public bool NewWindow { get; set; }
        public string Style { get; set; }
        public Int16? AuthorityID { get; set; }
        public string ServiceType { get; set; }
        public string ShowMarket { get; set; }
        public MainMenu()
        {
            this.ShowAllUser = true;
            this.OnlyMainAgency = false;
            this.External = false;
            this.NewWindow = false;
            this.AuthorityID = null;
            this.ServiceType = string.Empty;
            this.ShowMarket = string.Empty;
        }
    }

    [Serializable()]
    public class WebConfig
    {
        public WebConfig()
        {
        }

        string _FORMNAME;
        public string FORMNAME
        {
            get { return _FORMNAME; }
            set { _FORMNAME = value; }
        }

        string _PARAMETERNAME;
        public string PARAMETERNAME
        {
            get { return _PARAMETERNAME; }
            set { _PARAMETERNAME = value; }
        }

        string _TYPES;
        public string TYPES
        {
            get { return _TYPES; }
            set { _TYPES = value; }
        }

        string _FIELDTYPE;
        public string FIELDTYPE
        {
            get { return _FIELDTYPE; }
            set { _FIELDTYPE = value; }
        }

        string _FIELDVALUE;
        public string FIELDVALUE
        {
            get { return _FIELDVALUE; }
            set { _FIELDVALUE = value; }
        }
    }

    [Serializable()]
    public class ResViewMenu
    {
        public string Code { get; set; }
        public string ResourceName { get; set; }
        public bool Disable { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        public string Service { get; set; }
        public bool SubFolder { get; set; }
        public bool PaymentControl { get; set; }
        public bool ResStatusControl { get; set; }
        public string NoShowMarket { get; set; }
        public bool ResBusyControl { get; set; }
        public bool afterCheckout { get; set; }
        public bool MultiLang { get; set; }
        public Int16? ShowType { get; set; }
        public bool External { get; set; }
        public int? FormID { get; set; }
        public string MarketFormID { get; set; }
        public ResViewMenu()
        {
            this.Disable = true;
            this.SubFolder = true;
            this.PaymentControl = true;
            this.ResStatusControl = true;
            this.ResBusyControl = false;
            this.afterCheckout = true;
            this.MultiLang = false;
            this.External = false;
        }
    }

    [Serializable()]
    public class ExternalServiceMenu
    {
        public ExternalServiceMenu()
        {
            _Disable = false;
            _ExternalWindow = false;
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _ResourceName;
        public string ResourceName
        {
            get { return _ResourceName; }
            set { _ResourceName = value; }
        }

        bool _Disable;
        public bool Disable
        {
            get { return _Disable; }
            set { _Disable = value; }
        }

        string _Url;
        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }

        string _Params;
        public string Params
        {
            get { return _Params; }
            set { _Params = value; }
        }

        bool _ExternalWindow;
        public bool ExternalWindow
        {
            get { return _ExternalWindow; }
            set { _ExternalWindow = value; }
        }
    }

    [Serializable()]
    public class MakeResMenu
    {
        public MakeResMenu()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        bool _Disable;
        public bool Disable
        {
            get { return _Disable; }
            set { _Disable = value; }
        }
    }

    [Serializable()]
    public class CodeName
    {
        public CodeName()
        {

        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
    }

    [Serializable()]
    public class CodeHotelName
    {
        public CodeHotelName()
        {

        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }
    }

    [Serializable()]
    public class HeaderData
    {
        public HeaderData()
        {
        }

        string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        string _UserNameL;
        public string UserNameL
        {
            get { return _UserNameL; }
            set { _UserNameL = value; }
        }

        string _AgencyName;
        public string AgencyName
        {
            get { return _AgencyName; }
            set { _AgencyName = value; }
        }

        string _AgencyNameL;
        public string AgencyNameL
        {
            get { return _AgencyNameL; }
            set { _AgencyNameL = value; }
        }

        string _MainAgency;
        public string MainAgency
        {
            get { return _MainAgency; }
            set { _MainAgency = value; }
        }

        string _MainAgencyL;
        public string MainAgencyL
        {
            get { return _MainAgencyL; }
            set { _MainAgencyL = value; }
        }

        string _Office;
        public string Office
        {
            get { return _Office; }
            set { _Office = value; }
        }

        string _OfficeL;
        public string OfficeL
        {
            get { return _OfficeL; }
            set { _OfficeL = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _OperatorL;
        public string OperatorL
        {
            get { return _OperatorL; }
            set { _OperatorL = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _MarketL;
        public string MarketL
        {
            get { return _MarketL; }
            set { _MarketL = value; }
        }

        string _OprAddress;
        public string OprAddress
        {
            get { return _OprAddress; }
            set { _OprAddress = value; }
        }

    }

    [Serializable()]
    public class OperatorLogo
    {
        public OperatorLogo()
        {
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _OperatorLogoPath;
        public string OperatorLogoPath
        {
            get { return _OperatorLogoPath; }
            set { _OperatorLogoPath = value; }
        }
    }

    [Serializable()]
    public class SavePageData
    {
        public SavePageData()
        {
        }

        int? _Country;
        public int? Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _Resort;
        public string Resort
        {
            get { return _Resort; }
            set { _Resort = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        int? _Package;
        public int? Package
        {
            get { return _Package; }
            set { _Package = value; }
        }

        DateTime? _CheckIn;
        public DateTime? CheckIn
        {
            get { return _CheckIn; }
            set { _CheckIn = value; }
        }

        int? _ExpandDate;
        public int? ExpandDate
        {
            get { return _ExpandDate; }
            set { _ExpandDate = value; }
        }

        int? _Night1;
        public int? Night1
        {
            get { return _Night1; }
            set { _Night1 = value; }
        }

        int? _Night2;
        public int? Night2
        {
            get { return _Night2; }
            set { _Night2 = value; }
        }

        int? _Adult;
        public int? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        int? _Child;
        public int? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        int? _ChdAge1;
        public int? ChdAge1
        {
            get { return _ChdAge1; }
            set { _ChdAge1 = value; }
        }

        int? _ChdAge2;
        public int? ChdAge2
        {
            get { return _ChdAge2; }
            set { _ChdAge2 = value; }
        }

        int? _ChdAge3;
        public int? ChdAge3
        {
            get { return _ChdAge3; }
            set { _ChdAge3 = value; }
        }

        int? _ChdAge4;
        public int? ChdAge4
        {
            get { return _ChdAge4; }
            set { _ChdAge4 = value; }
        }

        string _FromPrice;
        public string FromPrice
        {
            get { return _FromPrice; }
            set { _FromPrice = value; }
        }

        string _ToPrice;
        public string ToPrice
        {
            get { return _ToPrice; }
            set { _ToPrice = value; }
        }

        int? _ShowAvailable;
        public int? ShowAvailable
        {
            get { return _ShowAvailable; }
            set { _ShowAvailable = value; }
        }

        bool? _CurrencyCheck;
        public bool? CurrencyCheck
        {
            get { return _CurrencyCheck; }
            set { _CurrencyCheck = value; }
        }

        int? _Direction;
        public int? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        int? _HolPackCat;
        public int? HolPackCat
        {
            get { return _HolPackCat; }
            set { _HolPackCat = value; }
        }

        bool? _IsRT;
        public bool? IsRT
        {
            get { return _IsRT; }
            set { _IsRT = value; }
        }

    }

    [Serializable()]
    public struct gridColumns
    {
        int _colID;
        public int ColID
        {
            get { return _colID; }
            set { _colID = value; }
        }

        bool _compulsory;
        public bool Compulsory
        {
            get { return _compulsory; }
            set { _compulsory = value; }
        }

        bool _colSelected;
        public bool ColSelected
        {
            get { return _colSelected; }
            set { _colSelected = value; }
        }

        bool _colEnable;
        public bool ColEnable
        {
            get { return _colEnable; }
            set { _colEnable = value; }
        }

        bool _colVisible;
        public bool ColVisible
        {
            get { return _colVisible; }
            set { _colVisible = value; }
        }

        string _colName;
        public string ColName
        {
            get { return _colName; }
            set { _colName = value; }
        }

    }

    [Serializable()]
    public class GridOptions
    {
        public GridOptions()
        {
        }

        int _ID;
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        int _PageID;
        public int PageID
        {
            get { return _PageID; }
            set { _PageID = value; }
        }

        string _gridID;
        public string GridID
        {
            get { return _gridID; }
            set { _gridID = value; }
        }

        string _GridColumns;
        public string GridColumns
        {
            get { return _GridColumns; }
            set { _GridColumns = value; }
        }

        List<gridColumns> _columns;
        public List<gridColumns> Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }

        bool _empty;
        public bool Empty
        {
            get { return _empty; }
            set { _empty = value; }
        }
    }

    [Serializable()]
    public class HotelsInfoOptions
    {
        public HotelsInfoOptions()
        {
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        bool _useLocal;
        public bool UseLocal
        {
            get { return _useLocal; }
            set { _useLocal = value; }
        }

        string _HotelInfoUrl;
        public string HotelInfoUrl
        {
            get { return _HotelInfoUrl; }
            set { _HotelInfoUrl = value; }
        }
    }

    [Serializable()]
    public class pageMakeRes
    {
        public pageMakeRes()
        {
        }

        string _CustomerCode;
        public string CustomerCode
        {
            get { return _CustomerCode; }
            set { _CustomerCode = value; }
        }

        string _Dosier;
        public string Dosier
        {
            get { return _Dosier; }
            set { _Dosier = value; }
        }

        string _AgencyDiscount;
        public string AgencyDiscount
        {
            get { return _AgencyDiscount; }
            set { _AgencyDiscount = value; }
        }

        string _ResNote;
        public string ResNote
        {
            get { return _ResNote; }
            set { _ResNote = value; }
        }

        StringBuilder _handicaps;
        public StringBuilder Handicaps
        {
            get { return _handicaps; }
            set { _handicaps = value; }
        }

        List<TransportStatRecord> _pickups;
        public List<TransportStatRecord> Pickups
        {
            get { return _pickups; }
            set { _pickups = value; }
        }

    }

    [Serializable()]
    public class SubMenuResourceRecord
    {
        public SubMenuResourceRecord()
        {
        }

        string _MSG_KEY;
        public string MSG_KEY
        {
            get { return _MSG_KEY; }
            set { _MSG_KEY = value; }
        }

        string _MESSAGE;
        public string MESSAGE
        {
            get { return _MESSAGE; }
            set { _MESSAGE = value; }
        }
    }

    [Serializable()]
    public class dataTableColumnsDef
    {
        public dataTableColumnsDef()
        {
            _bSortable = false;
        }

        int? _IDNo;
        public int? IDNo
        {
            get { return _IDNo; }
            set { _IDNo = value; }
        }

        string _ColName;
        public string ColName
        {
            get { return _ColName; }
            set { _ColName = value; }
        }

        string _sTitle;
        public string sTitle
        {
            get { return _sTitle; }
            set { _sTitle = value; }
        }

        string _sWidth;
        public string sWidth
        {
            get { return _sWidth; }
            set { _sWidth = value; }
        }

        bool _bSortable;
        public bool bSortable
        {
            get { return _bSortable; }
            set { _bSortable = value; }
        }

        bool _bSearchable;
        public bool bSearchable
        {
            get { return _bSearchable; }
            set { _bSearchable = value; }
        }

        string _sType;
        public string sType
        {
            get { return _sType; }
            set { _sType = value; }
        }

        string _sClass;
        public string sClass
        {
            get { return _sClass; }
            set { _sClass = value; }
        }

        int? _SeqNo;
        public int? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }
    }

    [Serializable()]
    public class dataTableParams
    {
        public dataTableParams()
        {
        }

        string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        object _value;
        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    [Serializable()]
    public class dataTableResult
    {
        public dataTableResult()
        {
            _aaData = new List<dataTableAgencyPaymentMon>();
        }

        int? _sEcho;
        public int? sEcho
        {
            get { return _sEcho; }
            set { _sEcho = value; }
        }

        int? _iTotalRecords;
        public int? iTotalRecords
        {
            get { return _iTotalRecords; }
            set { _iTotalRecords = value; }
        }

        int? _iTotalDisplayRecords;
        public int? iTotalDisplayRecords
        {
            get { return _iTotalDisplayRecords; }
            set { _iTotalDisplayRecords = value; }
        }

        List<dataTableAgencyPaymentMon> _aaData;
        public List<dataTableAgencyPaymentMon> aaData
        {
            get { return _aaData; }
            set { _aaData = value; }
        }
    }

    [Serializable()]
    public class dataTableAgencyPaymentMon
    {
        public dataTableAgencyPaymentMon() { }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _Days;
        public Int16? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }

        string _statusStr;
        public string statusStr
        {
            get { return _statusStr; }
            set { _statusStr = value; }
        }

        DateTime? _ResDate;
        public DateTime? ResDate
        {
            get { return _ResDate; }
            set { _ResDate = value; }
        }

        string _LeaderName;
        public string LeaderName
        {
            get { return _LeaderName; }
            set { _LeaderName = value; }
        }

        Int16? _Adult;
        public Int16? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16? _Child;
        public Int16? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        decimal? _AgencyCom;
        public decimal? AgencyCom
        {
            get { return _AgencyCom; }
            set { _AgencyCom = value; }
        }

        decimal? _Discount;
        public decimal? Discount
        {
            get { return _Discount; }
            set { _Discount = value; }
        }

        decimal? _BrokerCom;
        public decimal? BrokerCom
        {
            get { return _BrokerCom; }
            set { _BrokerCom = value; }
        }

        decimal? _AgencySupDis;
        public decimal? AgencySupDis
        {
            get { return _AgencySupDis; }
            set { _AgencySupDis = value; }
        }

        decimal? _AgencyComSup;
        public decimal? AgencyComSup
        {
            get { return _AgencyComSup; }
            set { _AgencyComSup = value; }
        }

        decimal? _EBAgency;
        public decimal? EBAgency
        {
            get { return _EBAgency; }
            set { _EBAgency = value; }
        }

        decimal? _EBPas;
        public decimal? EBPas
        {
            get { return _EBPas; }
            set { _EBPas = value; }
        }

        decimal? _AgencyPayable;
        public decimal? AgencyPayable
        {
            get { return _AgencyPayable; }
            set { _AgencyPayable = value; }
        }

        decimal? _AgencyPayable2;
        public decimal? AgencyPayable2
        {
            get { return _AgencyPayable2; }
            set { _AgencyPayable2 = value; }
        }

        decimal? _AgencyPayment;
        public decimal? AgencyPayment
        {
            get { return _AgencyPayment; }
            set { _AgencyPayment = value; }
        }

        decimal? _Balance;
        public decimal? Balance
        {
            get { return _Balance; }
            set { _Balance = value; }
        }

        decimal? _UserBonus;
        public decimal? UserBonus
        {
            get { return _UserBonus; }
            set { _UserBonus = value; }
        }

        decimal? _AgencyBonus;
        public decimal? AgencyBonus
        {
            get { return _AgencyBonus; }
            set { _AgencyBonus = value; }
        }
    }

    [Serializable()]
    public class filterDataRecord
    {
        public IEnumerable fltBook { get; set; }
        public string fltBookSelected { get; set; }
        public IEnumerable fltAddOffer { get; set; }
        public string fltAddOfferSelected { get; set; }
        public IEnumerable fltOprText { get; set; }
        public string fltOprTextSelected { get; set; }
        public IEnumerable fltHotel { get; set; }
        public string fltHotelSelected { get; set; }
        public IEnumerable fltCategory { get; set; }
        public string fltCategorySelected { get; set; }
        public IEnumerable fltRoom { get; set; }
        public string fltRoomSelected { get; set; }
        public IEnumerable fltFreeRoom { get; set; }
        public string fltFreeRoomSelected { get; set; }
        public IEnumerable fltLocation { get; set; }
        public string fltLocationSelected { get; set; }
        public IEnumerable fltCheckIn { get; set; }
        public string fltCheckInSelected { get; set; }
        public IEnumerable fltNights { get; set; }
        public string fltNightsSelected { get; set; }
        public IEnumerable fltBoard { get; set; }
        public string fltBoardSelected { get; set; }
        public IEnumerable fltAccom { get; set; }
        public string fltAccomSelected { get; set; }
        public IEnumerable fltPrice { get; set; }
        public decimal? fltPriceBegin { get; set; }
        public decimal? fltPriceEnd { get; set; }
        public IEnumerable fltDiscount { get; set; }
        public IEnumerable fltDeparture { get; set; }
        public string fltDepartureSelected { get; set; }
        public IEnumerable fltReturn { get; set; }
        public string fltReturnSelected { get; set; }
    }

    [Serializable()]
    public class tableSumResMonitor
    {
        public int totalRecord { get; set; }
        public int totalAdult { get; set; }
        public int totalChild { get; set; }
        public IEnumerable totalPrice { get; set; }
    }

    [Serializable()]
    public class lightTable
    {
        public int sEcho { get; set; }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }
        public IEnumerable aaData { get; set; }
        public filterDataRecord filterData { get; set; }
        public tableSumResMonitor tableSum { get; set; }
    }

    public class currencyOptions
    {
        public currencyOptions()
        {
            this.showFlyType = false;
            this.flyTypeList = new List<FlightTypeRecord>();
        }
        public long currentDate { get; set; }
        public bool showCurrencyDiv { get; set; }
        public bool currencyCheck { get; set; }
        public string currency { get; set; }
        public List<string> currencyList { get; set; }
        public bool typeLock { get; set; }
        public Int16 curOption { get; set; }
        public List<Int32> days { get; set; }
        public int? defaultDays1 { get; set; }
        public int? defaultDays2 { get; set; }
        public bool SearchAllFlight { get; set; }
        public bool SearchAllFlightDefaultValue { get; set; }
        public List<Int16> expandDay { get; set; }
        public bool showFlyType { get; set; }
        public List<FlightTypeRecord> flyTypeList { get; set; }

    }
    [Serializable()]
    public class calendarColorListWithDay
    {
        public List<calendarColor> listCalendarColor { get; set; }
        public int? minDay { get; set; }
        public int? maxDay { get; set; }
        public calendarColorListWithDay()
        {
            listCalendarColor = new List<calendarColor>();
        }
    }

    [Serializable()]
    public class calendarColor
    {
        public calendarColor()
        {
        }

        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public string Text { get; set; }
        public int? ColorType { get; set; }
    }

    [Serializable()]
    public class searchCriteriaRoomInfo
    {
        public searchCriteriaRoomInfo()
        {
        }
        public Int16? Adult { get; set; }
        public Int16? child { get; set; }
        public Int16? Chd1Age { get; set; }
        public Int16? Chd2Age { get; set; }
        public Int16? Chd3Age { get; set; }
        public Int16? Chd4Age { get; set; }
    }

    [Serializable()]
    public class searchCriteria
    {
        public searchCriteria()
        {
        }
        public DateTime? CheckIn { get; set; }
        public Int16? ExpandDate { get; set; }
        public Int16? NightFrom { get; set; }
        public Int16? NightTo { get; set; }
        public Int16? RoomCount { get; set; }
        public List<searchCriteriaRoomInfo> roomsInfo { get; set; }
        public List<string> Board { get; set; }
        public List<string> Room { get; set; }
        public int? DepCity { get; set; }
        public int? ArrCity { get; set; }
        public List<int?> Resort { get; set; }
        public List<string> Category { get; set; }
        public List<string> Hotel { get; set; }
        public string CurControl { get; set; }
        public string CurrentCur { get; set; }
        public string ShowStopSaleHotels { get; set; }
        public int? HolPack { get; set; }
        public string B2BMenuCatStr { get; set; }
    }

    [Serializable()]
    public class requestFormData
    {
        public requestFormData()
        {
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Value;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
    }

    [Serializable()]
    public class editServiceReturnData
    {
        public editServiceReturnData() { }
        public bool Calc { get; set; }
        public string CalcPrice { get; set; }
        public string CalcCur { get; set; }
        public int? Adult { get; set; }
        public int? Child { get; set; }
        public int? Unit { get; set; }
        public string OldResPrice { get; set; }
        public string NewResPrice { get; set; }
        public string OldServicePrice { get; set; }
        public string Supplier { get; set; }
        public string Msg { get; set; }
    }

    [Serializable()]
    public class addServiceReturnData
    {
        public addServiceReturnData() { }
        public bool Calc { get; set; }
        public string CalcPrice { get; set; }
        public string CalcCur { get; set; }
        public int? Adult { get; set; }
        public int? Child { get; set; }
        public int? Unit { get; set; }
        public string OldResPrice { get; set; }
        public string NewResPrice { get; set; }
        public string OldServicePrice { get; set; }
        public string Supplier { get; set; }
        public string Msg { get; set; }
    }

    [Serializable()]
    public class searchFilterPaxCounts
    {
        public int maxRooms { get; set; }
        public string roomsLabel { get; set; }
        public int maxAdult { get; set; }
        public string adultLabel { get; set; }
        public int maxChild { get; set; }
        public string childLabel { get; set; }
        public int maxChildAge { get; set; }
        public string childAge1Label { get; set; }
        public string childAge2Label { get; set; }
        public string childAge3Label { get; set; }
        public string childAge4Label { get; set; }
        public searchFilterPaxCounts()
        {
            this.maxRooms = 2;
            this.roomsLabel = "Room";
            this.maxAdult = 4;
            this.adultLabel = "Adult";
            this.maxChild = 4;
            this.childLabel = "Child";
            this.maxChildAge = 12;
            this.childAge1Label = "Child Age 1";
            this.childAge2Label = "Child Age 2";
            this.childAge3Label = "Child Age 3";
            this.childAge4Label = "Child Age 4";
        }
    }

    [Serializable()]
    public class ResDocument
    {
        public int? RecID { get; set; }
        public string ResNo { get; set; }
        public string DocName { get; set; }
        public string FileType { get; set; }
        public ResDocument()
        {

        }
    }

    public class LogoutToUrlRecord
    {
        public string Market { get; set; }
        public string GoToUrl { get; set; }
        public LogoutToUrlRecord()
        {

        }
    }

    public class MakeReservationSaveButtonResponse
    {
        public bool Err { get; set; }
        public bool Promotion { get; set; }
        public bool SaleableService { get; set; }
        public string ErrorMsg { get; set; }
        public string SaleableServiceMsg { get; set; }

        public MakeReservationSaveButtonResponse()
        {
            this.Err = false;
            this.Promotion = false;
            this.SaleableService = false;
            this.ErrorMsg = string.Empty;
            this.SaleableServiceMsg = string.Empty;
        }
    }
}
