﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using TvTools;

namespace TvBo
{
    public class B2BSystems
    {
        public static bool PackPriceFilterCreateTable(ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE B2BPackPriceFilter(
	[MarketID] [int] NOT NULL,
	[HolPackID] [int] NOT NULL,
	[DepCityID] [int] NOT NULL,
	[ArrCityID] [int] NOT NULL,
	[HotelID] [int] NOT NULL,
	[RoomID] [int] NOT NULL,
	[AccomID] [int] NOT NULL,
	[BoardID] [int] NOT NULL,
 CONSTRAINT PK_B2BPackPriceFilter PRIMARY KEY CLUSTERED 
(
	[MarketID] ASC,
	[HolPackID] ASC,
	[DepCityID] ASC,
	[ArrCityID] ASC,
	[HotelID] ASC,
	[RoomID] ASC,
	[AccomID] ASC,
	[BoardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                int retVal = db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return false;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public static List<PackPriceFilterCache> PackPriceFilterCreateCacheData(ref string errorMsg)
        {
            List<PackPriceFilterCache> records = new List<PackPriceFilterCache>();
            string tsql = string.Empty;
            tsql =
@"
Declare @toDay DateTime
Select @toDay=dbo.DateOnly(GetDate())

Select Distinct P.MarketID,P.HolPackID,P.DepCity,P.ArrCity,P.HotelID,P.RoomID,P.AccomID,P.BoardID
From PackPriceList P (NOLOCK)
Where CheckIn>=@toDay
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        PackPriceFilterCache record = new PackPriceFilterCache();
                        record.MarketID = Conversion.getInt32OrNull(R["MarketID"]);
                        record.HolPackID = Conversion.getInt32OrNull(R["HolPackID"]);
                        record.DepCityID = Conversion.getInt32OrNull(R["DepCity"]);
                        record.ArrCityID = Conversion.getInt32OrNull(R["ArrCity"]);
                        record.HotelID = Conversion.getInt32OrNull(R["HotelID"]);
                        record.RoomID = Conversion.getInt32OrNull(R["RoomID"]);
                        record.AccomID= Conversion.getInt32OrNull(R["AccomID"]);
                        record.BoardID = Conversion.getInt32OrNull(R["BoardID"]);
                        records.Add(record);
                    }                    
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

    }
}
