﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web;
using TvTools;

namespace TvBo
{
    public class UIReservation
    {
        public bool firstControl(string HolPack)
        {
            string tsql = string.Format(@"Select cnt=Count(Service) From CatPackPlan (NOLOCK) Where Service = 'FLIGHT' And HolPack = '{0}'", HolPack);

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {

                object oReader = db.ExecuteScalar(dbCommand);
                if (oReader == null || Convert.ToInt32(oReader.ToString()) == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public bool bookFlightAllotControl(User UserData, List<SearchResult> SelectBook, SearchResult searchRows, bool tourPackage, ref string errorMsg)
        {
            if (SelectBook.Count < 1)
            {
                errorMsg = "Error code: 1";
                return false;
            }

            string tsql = string.Empty;

            bool flightOk =true;
            bool transportOk = true;
            bool depOk = false;
            bool retOk = false;
            int? isOpt = 0;
            int? paxCnt = 0;
            int? DepFree = 0;
            int? RetFree = 0;
            bool firstControl = false;

            paxCnt = SelectBook.AsEnumerable().Sum(s => s.HAdult);
            //First child is free 
            paxCnt += SelectBook.AsEnumerable().Sum(s => s.HChdAgeG2);
            paxCnt += SelectBook.AsEnumerable().Sum(s => s.HChdAgeG3);
            paxCnt += SelectBook.AsEnumerable().Sum(s => s.HChdAgeG4);

            if (SelectBook.Where(w => w.DepServiceType == "TRANSPORT").Count() > 0)
            {
                transportOk = false;
                if (SelectBook.Where(w => w.DepSeat < paxCnt && w.TransportType == 2).Count() > 0)
                {
                    transportOk = false;
                }
                else
                {
                    transportOk = true;
                    depOk = true;
                }
                firstControl = true;
            }
            if (SelectBook.Where(w => w.RetServiceType == "TRANSPORT").Count() > 0)
            {
                //transportOk = false;
                if (SelectBook.Where(w => w.RetSeat < paxCnt && w.TransportType == 2).Count() > 0)
                {
                    transportOk = false;
                }
                else
                {
                    transportOk = transportOk ? true : false;
                    retOk = true;
                }
                firstControl = true;
            }
            if (!transportOk && UserData.TvParams.TvParamTransport.AllotChk && UserData.TvParams.TvParamTransport.AllotDoNotOver)
            {
                errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                return false;
            }
            if (SelectBook.Where(w => w.DepServiceType == "TRANSPORT").Count() > 0 && SelectBook.Where(w => w.RetServiceType == "TRANSPORT").Count() > 0)
                flightOk = false;

            List<HolPackFlightRecord> holPackFlight = new Flights().getHolPackFlight(searchRows.HolPack, ref errorMsg);

            if (!firstControl)
                return true;

            if (flightOk)
            {
                tsql = @"   Declare @DepFree int, @RetFree int
                            Set @DepFree = 0
                            Set @RetFree = 0
                            Exec dbo.usp_Get_DepRet_FlightSeat @CatPackID, @PRecNo, @HolPack, @pFlightNo, @FlyDate, @Night,	@FlightClass,	@DepCity, @ArrCity,	@Operator, @Market,	
                            @DepFree OUTPUT, @RetFree OUTPUT, @IsOpt OUTPUT
                            Select DepFree=@DepFree, RetFree=@RetFree, IsOpt=@IsOpt ";



                DateTime? depFlightDate = null;
                List<HolPackFlightRecord> depFlights = new List<HolPackFlightRecord>();
                if (tourPackage)
                {
                    depFlights = holPackFlight.OrderBy(o => o.StartDay).Select(s => s).ToList<HolPackFlightRecord>();
                    depFlightDate = searchRows.CheckIn.HasValue ? searchRows.CheckIn.Value.AddDays(depFlights.FirstOrDefault().StartDay.Value - 1) : searchRows.CheckIn;
                }
                else
                    depFlights = holPackFlight.Where(w => w.StepType == 1).Select(s => s).ToList<HolPackFlightRecord>();
                HolPackFlightRecord depFlight = depFlights != null && depFlights.Count > 0 ? depFlights.First() : null;
                Database db = DatabaseFactory.CreateDatabase() as Database;
                DbCommand dbCommand = db.GetSqlStringCommand(tsql);
                try
                {

                    db.AddInParameter(dbCommand, "DepServiceType", DbType.String, searchRows.DepServiceType);
                    db.AddInParameter(dbCommand, "RetServiceType", DbType.String, searchRows.RetServiceType);
                    db.AddInParameter(dbCommand, "HolPack", DbType.String, searchRows.HolPack);
                    db.AddInParameter(dbCommand, "TransportType", DbType.Int32, searchRows.TransportType);
                    db.AddInParameter(dbCommand, "CustomRegID", DbType.String, UserData.CustomRegID);
                    db.AddInParameter(dbCommand, "Operator", DbType.String, searchRows.Operator);
                    db.AddInParameter(dbCommand, "Market", DbType.String, searchRows.Market);
                    db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                    db.AddInParameter(dbCommand, "Office", DbType.String, UserData.OprOffice);
                    db.AddInParameter(dbCommand, "Checkin", DbType.Date, searchRows.CheckIn);
                    db.AddInParameter(dbCommand, "Checkout", DbType.Date, searchRows.CheckOut);
                    db.AddInParameter(dbCommand, "DepCity", DbType.Int32, searchRows.DepCity);
                    db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, searchRows.ArrCity);
                    db.AddInParameter(dbCommand, "Night", DbType.Int16, searchRows.Night);
                    db.AddInParameter(dbCommand, "DepFlight", DbType.String, searchRows.DepFlight);
                    db.AddInParameter(dbCommand, "RetFlight", DbType.String, searchRows.RetFlight);
                    db.AddInParameter(dbCommand, "FlightClass", DbType.String, searchRows.FlightClass);
                    db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, searchRows.CatPackID);
                    db.AddInParameter(dbCommand, "PRecNo", DbType.Int32, searchRows.PRecNo);
                    db.AddInParameter(dbCommand, "pFlightNo", DbType.String, searchRows.DepFlight);
                    db.AddInParameter(dbCommand, "FlyDate", DbType.Date, tourPackage ? depFlightDate : searchRows.CheckIn);
                    db.AddInParameter(dbCommand, "IsOpt", DbType.Int32, paxCnt);
                    using (IDataReader oReader = db.ExecuteReader(dbCommand))
                    {
                        if (oReader.Read())
                        {
                            isOpt = Conversion.getInt32OrNull(oReader[2]);
                            DepFree = depOk ? paxCnt : Conversion.getInt32OrNull(oReader[0]);
                            RetFree = retOk ? paxCnt : Conversion.getInt32OrNull(oReader[1]);

                            if (UserData.TvParams.TvParamFlight.AllotDoNotOver && UserData.TvParams.TvParamFlight.AllotChk)
                                if ((DepFree < paxCnt || RetFree < paxCnt) && isOpt <= 1)
                                {
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                                    return false;
                                }

                            if (UserData.TvParams.TvParamFlight.OptAllotDoNotOver && UserData.TvParams.TvParamFlight.AllotChk)
                                if ((DepFree < paxCnt || RetFree < paxCnt) && isOpt == 2)
                                {
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                                    return false;
                                }
                        }
                        else
                            return false;
                    }
                    return true;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return false;
                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
            }
            else
                return true;
        }
        /// <summary>
        /// Orjinal
        /// </summary>
        /// <param name="UserData"></param>
        /// <param name="SelectBook"></param>
        /// <param name="searchRows"></param>
        /// <param name="tourPackage"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public bool bookFlightAllotControlBackup(User UserData, List<SearchResult> SelectBook, SearchResult searchRows, bool tourPackage, ref string errorMsg)
        {
            if (SelectBook.Count < 1)
            {
                errorMsg = "Error code: 1";
                return false;
            }

            string tsql = string.Empty;

            bool flightOk = true;
            bool transportOk = false;
            int? isOpt = 0;
            int? paxCnt = 0;
            int? DepFree = 0;
            int? RetFree = 0;
            bool firstControl = false;

            paxCnt = SelectBook.AsEnumerable().Sum(s => s.HAdult);
            //First child is free 
            paxCnt += SelectBook.AsEnumerable().Sum(s => s.HChdAgeG2);
            paxCnt += SelectBook.AsEnumerable().Sum(s => s.HChdAgeG3);
            paxCnt += SelectBook.AsEnumerable().Sum(s => s.HChdAgeG4);

            List<HolPackFlightRecord> holPackFlight = new Flights().getHolPackFlight(searchRows.HolPack, ref errorMsg);
            if (holPackFlight == null || holPackFlight.Count() < 1)
            {
                List<HolPackFlightRecord> holPackTransport = new Flights().getHolPackTransport(searchRows.HolPack, ref errorMsg);
                if (holPackTransport != null || holPackTransport.Count() > 0)
                {
                    if (UserData.TvParams.TvParamTransport.AllotChk && UserData.TvParams.TvParamTransport.AllotDoNotOver && SelectBook.Where(w => w.TransportType == 2).Count() > 0)
                    {
                        if (SelectBook.Where(w => w.DepSeat < paxCnt && w.TransportType == 2).Count() > 0)
                        {
                            transportOk = false;
                        }
                        else
                        {
                            transportOk = true;
                        }
                        if (SelectBook.Where(w => w.RetSeat < paxCnt && w.TransportType == 2).Count() > 0)
                        {
                            transportOk = false;
                        }
                        else
                        {
                            transportOk = true;
                        }
                        if (!transportOk)
                        {
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
                flightOk = false;
                firstControl = false;
            }
            else
            {
                firstControl = true;
            }

            if (!firstControl)
                return true;

            if (flightOk)
            {
                tsql = @"   Declare @DepFree int, @RetFree int
                            Set @DepFree = 0
                            Set @RetFree = 0
                            Exec dbo.usp_Get_DepRet_FlightSeat @CatPackID, @PRecNo, @HolPack, @pFlightNo, @FlyDate, @Night,	@FlightClass,	@DepCity, @ArrCity,	@Operator, @Market,	
                            @DepFree OUTPUT, @RetFree OUTPUT, @IsOpt OUTPUT
                            Select DepFree=@DepFree, RetFree=@RetFree, IsOpt=@IsOpt ";



                DateTime? depFlightDate = null;
                List<HolPackFlightRecord> depFlights = new List<HolPackFlightRecord>();
                if (tourPackage)
                {
                    depFlights = holPackFlight.OrderBy(o => o.StartDay).Select(s => s).ToList<HolPackFlightRecord>();
                    depFlightDate = searchRows.CheckIn.HasValue ? searchRows.CheckIn.Value.AddDays(depFlights.FirstOrDefault().StartDay.Value - 1) : searchRows.CheckIn;
                }
                else
                    depFlights = holPackFlight.Where(w => w.StepType == 1).Select(s => s).ToList<HolPackFlightRecord>();
                HolPackFlightRecord depFlight = depFlights != null && depFlights.Count > 0 ? depFlights.First() : null;
                Database db = DatabaseFactory.CreateDatabase() as Database;
                DbCommand dbCommand = db.GetSqlStringCommand(tsql);
                try
                {

                    db.AddInParameter(dbCommand, "DepServiceType", DbType.String, searchRows.DepServiceType);
                    db.AddInParameter(dbCommand, "RetServiceType", DbType.String, searchRows.RetServiceType);
                    db.AddInParameter(dbCommand, "HolPack", DbType.String, searchRows.HolPack);
                    db.AddInParameter(dbCommand, "TransportType", DbType.Int32, searchRows.TransportType);
                    db.AddInParameter(dbCommand, "CustomRegID", DbType.String, UserData.CustomRegID);
                    db.AddInParameter(dbCommand, "Operator", DbType.String, searchRows.Operator);
                    db.AddInParameter(dbCommand, "Market", DbType.String, searchRows.Market);
                    db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                    db.AddInParameter(dbCommand, "Office", DbType.String, UserData.OprOffice);
                    db.AddInParameter(dbCommand, "Checkin", DbType.Date, searchRows.CheckIn);
                    db.AddInParameter(dbCommand, "Checkout", DbType.Date, searchRows.CheckOut);
                    db.AddInParameter(dbCommand, "DepCity", DbType.Int32, searchRows.DepCity);
                    db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, searchRows.ArrCity);
                    db.AddInParameter(dbCommand, "Night", DbType.Int16, searchRows.Night);
                    db.AddInParameter(dbCommand, "DepFlight", DbType.String, searchRows.DepFlight);
                    db.AddInParameter(dbCommand, "RetFlight", DbType.String, searchRows.RetFlight);
                    db.AddInParameter(dbCommand, "FlightClass", DbType.String, searchRows.FlightClass);
                    db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, searchRows.CatPackID);
                    db.AddInParameter(dbCommand, "PRecNo", DbType.Int32, searchRows.PRecNo);
                    db.AddInParameter(dbCommand, "pFlightNo", DbType.String, searchRows.DepFlight);
                    db.AddInParameter(dbCommand, "FlyDate", DbType.Date, tourPackage ? depFlightDate : searchRows.CheckIn);
                    db.AddInParameter(dbCommand, "IsOpt", DbType.Int32, paxCnt);
                    using (IDataReader oReader = db.ExecuteReader(dbCommand))
                    {
                        if (oReader.Read())
                        {
                            isOpt = Conversion.getInt32OrNull(oReader[2]);
                            DepFree = Conversion.getInt32OrNull(oReader[0]);
                            RetFree = Conversion.getInt32OrNull(oReader[1]);

                            if (UserData.TvParams.TvParamFlight.AllotDoNotOver && UserData.TvParams.TvParamFlight.AllotChk)
                                if ((DepFree < paxCnt || RetFree < paxCnt) && isOpt <= 1)
                                {
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                                    return false;
                                }

                            if (UserData.TvParams.TvParamFlight.OptAllotDoNotOver && UserData.TvParams.TvParamFlight.AllotChk)
                                if ((DepFree < paxCnt || RetFree < paxCnt) && isOpt == 2)
                                {
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                                    return false;
                                }
                        }
                        else
                            return false;
                    }
                    return true;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return false;
                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
            }
            else
                return true;
        }

        public bool bookFlightAllotControl(User UserData, DataTable SelectBook, SearchResult searchRows, ref string errorMsg)
        {
            if (SelectBook.Rows.Count < 1)
            {
                errorMsg = "Error code: 1";
                return false;
            }
            bool flightOk = true;
            int isOpt = 0;
            int paxCnt = 0;
            int DepFree = 0;
            int RetFree = 0;
            bool firstControl = true;
            bool transportOk = false;

            string tsql = string.Format(@"Select cnt=Count(Service) From CatPackPlan (NOLOCK) Where Service = 'FLIGHT' And HolPack = '{0}'", SelectBook.Rows[0]["HolPack"].ToString());

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                object oReader = db.ExecuteScalar(dbCommand);
                if (oReader == null || Convert.ToInt32(oReader.ToString()) == 0)
                    flightOk = false;
                firstControl = true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                firstControl = false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
            if (!firstControl)
                return false;
            paxCnt = SelectBook.AsEnumerable().Sum(s => s.Field<Int16>("HAdult"));
            //First child is free 
            paxCnt += SelectBook.AsEnumerable().Sum(s => s.Field<Int16>("HChdAgeG2"));
            paxCnt += SelectBook.AsEnumerable().Sum(s => s.Field<Int16>("HChdAgeG3"));
            paxCnt += SelectBook.AsEnumerable().Sum(s => s.Field<Int16>("HChdAgeG4"));

            #region ..:: Transport ::..
            if (searchRows.DepServiceType == "TRANSPORT" || searchRows.RetServiceType == "TRANSPORT")
            {
                if (UserData.TvParams.TvParamTransport.AllotChk && UserData.TvParams.TvParamTransport.AllotDoNotOver)
                {
                    if (SelectBook.Select(string.Format("DepSeat > {0} And RetSeat > {0}", paxCnt)).Length > 0)
                    {
                        transportOk = true;
                    }
                    else
                    {
                        transportOk = false;
                    }
                    if (!transportOk)
                    {
                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                        return false;
                    }
                }
            }
            #endregion
            if (flightOk)
            {


                tsql = @"   Declare @DepFree int, @RetFree int
                            Set @DepFree = 0
                            Set @RetFree = 0
                            Exec dbo.usp_Get_DepRet_FlightSeat @CatPackID, @PRecNo, @HolPack, @pFlightNo, @FlyDate, @Night,	@Class,	@DepCity, @ArrCity,	@Operator, @Market,	
                            @DepFree OUTPUT, @RetFree OUTPUT, @IsOpt OUTPUT
                            Select DepFree=@DepFree, RetFree=@RetFree, paxCnt=@IsOpt ";

                db = DatabaseFactory.CreateDatabase() as Database;
                dbCommand = db.GetSqlStringCommand(tsql);
                try
                {
                    db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, searchRows.CatPackID);
                    db.AddInParameter(dbCommand, "PRecNo", DbType.Int32, searchRows.PRecNo);
                    db.AddInParameter(dbCommand, "HolPack", DbType.String, searchRows.HolPack);
                    db.AddInParameter(dbCommand, "pFlightNo", DbType.String, searchRows.DepFlight);
                    db.AddInParameter(dbCommand, "FlyDate", DbType.Date, searchRows.CheckIn);
                    db.AddInParameter(dbCommand, "Night", DbType.Int16, searchRows.Night);
                    db.AddInParameter(dbCommand, "Class", DbType.String, searchRows.FlightClass);
                    db.AddInParameter(dbCommand, "DepCity", DbType.Int32, searchRows.DepCity);
                    db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, searchRows.ArrCity);
                    db.AddInParameter(dbCommand, "Operator", DbType.String, searchRows.Operator);
                    db.AddInParameter(dbCommand, "Market", DbType.String, searchRows.Market);
                    db.AddInParameter(dbCommand, "IsOpt", DbType.Int32, paxCnt);
                    using (IDataReader oReader = db.ExecuteReader(dbCommand))
                    {
                        if (oReader.RecordsAffected > 0)
                        {
                            oReader.Read();

                            isOpt = Convert.ToInt32(oReader["paxCnt"].ToString());
                            DepFree = Convert.ToInt32(oReader["DepFree"].ToString());
                            RetFree = Convert.ToInt32(oReader["RetFree"].ToString());
                            if (UserData.TvParams.TvParamFlight.AllotDoNotOver && UserData.TvParams.TvParamFlight.AllotChk)
                                if ((DepFree < paxCnt || RetFree < paxCnt) && isOpt <= 1)
                                {
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                                    return false;
                                }

                            if (UserData.TvParams.TvParamFlight.OptAllotDoNotOver && UserData.TvParams.TvParamFlight.AllotChk)
                                if ((DepFree < paxCnt || RetFree < paxCnt) && isOpt == 2)
                                {
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                                    return false;
                                }
                        }
                    }

                    return true;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return false;
                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
            }
            else
                return true;
        }

        public StringBuilder getHotelsHandicap(string Market, ResDataRecord ResData)
        {
            StringBuilder sb = new StringBuilder();
            string tsql = string.Empty;
            tsql = @"   Select BegDate, EndDate, isnull(dbo.FindLocalNameEx(NameLID, @Market), '') LName, Name
                        From HotelHandicap (NOLOCK)
                        Where 
	                        Hotel = @Hotel
                        And BegDate <= @EndDate
                        And EndDate >= @BegDate
                        And Market = @Market";
            List<ResServiceRecord> ResService = ResData.ResService;
            var query = from q in ResService
                        where q.ServiceType == "HOTEL"
                        group q by new { Service = q.Service, BegDate = q.BegDate, EndDate = q.EndDate, Desc = q.ServiceNameL + ", " + q.DepLocationNameL + " " + q.BegDate.Value.ToShortDateString() + "-" + q.EndDate.Value.ToShortDateString() } into k
                        select new { Service = k.Key.Service, BegDate = k.Key.BegDate, EndDate = k.Key.EndDate, Desc = k.Key.Desc };

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            foreach (var q in query)
            {
                try
                {
                    db.AddInParameter(dbCommand, "Hotel", DbType.String, q.Service);
                    db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                    db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, q.BegDate);
                    db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, q.EndDate);
                    using (IDataReader R = db.ExecuteReader(dbCommand))
                    {
                        sb.Append("<b>" + q.Desc + "</b><br />");
                        while (R.Read())
                        {
                            DateTime begDate = (DateTime)R["BegDate"];
                            DateTime endDate = (DateTime)R["EndDate"];
                            string name = Conversion.getStrOrNull(R["Name"]);
                            string lNAme = Conversion.getStrOrNull(R["LName"]);
                            sb.Append(begDate.ToShortDateString() + " - " + endDate.ToShortDateString() + " " + name + " / " + lNAme);
                            sb.Append("<br />");
                        }
                    }
                }
                catch
                {

                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
            }
            return sb;
        }

        public List<CompulsoryResCustFields> getCompulsoryResCustFields(User UserData, string PLMarket, ref string errorMsg)
        {
            List<CompulsoryResCustFields> compFields = new List<CompulsoryResCustFields>();

            return compFields;
        }

        public List<ResFixNote> getResFixNote(User UserData, ref string errorMsg)
        {
            List<ResFixNote> list = new List<ResFixNote>();

            string tsql =
@"
Select RecID,[Description],DescriptionL=ISNULL(dbo.FindLocalNameEx(DescriptionLID, @pMarket),[Description]),Market 
From ResFixNote (NOLOCK)
Where Market=@pMarket
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "pMarket", DbType.AnsiString, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        list.Add(new ResFixNote()
                        {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            Market = Conversion.getStrOrNull(R["Market"]),
                            Description = Conversion.getStrOrNull(R["Description"]),
                            DescriptionL = Conversion.getStrOrNull(R["DescriptionL"])
                        });
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ResDocument> getReadyDocument(User UserData, string ResNo, ref string errorMsg)
        {
            List<ResDocument> list = new List<ResDocument>();

            string tsql =
@"
Select RecID,ResNo,DocName,FileType 
From ResDocFile (NOLOCK)
Where ResNo=@ResNo
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.AnsiString, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        list.Add(new ResDocument()
                        {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            ResNo = Conversion.getStrOrNull(R["ResNo"]),
                            DocName = Conversion.getStrOrNull(R["DocName"]),
                            FileType = Conversion.getStrOrNull(R["FileType"])
                        });
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ResDocument getDocument(User UserData, int? RecID, ref string errorMsg)
        {
            string tsql =
@"
Select RecID,ResNo,DocName,FileType
From ResDocFile (NOLOCK)
Where RecID=@RecID
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "RecID", DbType.AnsiString, RecID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        return new ResDocument()
                        {
                            RecID = Conversion.getInt32OrNull(R["RecID"]),
                            ResNo = Conversion.getStrOrNull(R["ResNo"]),
                            DocName = Conversion.getStrOrNull(R["DocName"]),
                            FileType = Conversion.getStrOrNull(R["FileType"])
                        };
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public byte[] getDocumentObject(User UserData, int? RecID, ref string errorMsg)
        {
            string tsql =
@"
Select RptImage
From ResDocFile (NOLOCK)
Where RecID=@RecID
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "RecID", DbType.AnsiString, RecID);
                return (byte[])db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
