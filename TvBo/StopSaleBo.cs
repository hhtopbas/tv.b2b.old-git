﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class StopSaleDatabaseRecord
    {
        public StopSaleDatabaseRecord()
        {
        }

        int _RecId;
        public int RecId
        {
            get { return _RecId; }
            set { _RecId = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Accom;
        public string Accom
        {
            get { return _Accom; }
            set { _Accom = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        DateTime _BegDate;
        public DateTime BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime _EndDate;
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        string _Explanation;
        public string Explanation
        {
            get { return _Explanation; }
            set { _Explanation = value; }
        }

        DateTime _PubDate;
        public DateTime PubDate
        {
            get { return _PubDate; }
            set { _PubDate = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        string _DispMsg;
        public string DispMsg
        {
            get { return _DispMsg; }
            set { _DispMsg = value; }
        }

        string _MayIgnore;
        public string MayIgnore
        {
            get { return _MayIgnore; }
            set { _MayIgnore = value; }
        }

        int _PriceListNo;
        public int PriceListNo
        {
            get { return _PriceListNo; }
            set { _PriceListNo = value; }
        }

        string _AppType;
        public string AppType
        {
            get { return _AppType; }
            set { _AppType = value; }
        }

        int _Location;
        public int Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        byte _AllotType;
        public byte AllotType
        {
            get { return _AllotType; }
            set { _AllotType = value; }
        }

        bool _ForTV;
        public bool ForTV
        {
            get { return _ForTV; }
            set { _ForTV = value; }
        }

        bool _ForB2B;
        public bool ForB2B
        {
            get { return _ForB2B; }
            set { _ForB2B = value; }
        }

        bool _ForB2C;
        public bool ForB2C
        {
            get { return _ForB2C; }
            set { _ForB2C = value; }
        }

        byte _Resource;
        public byte Resource
        {
            get { return _Resource; }
            set { _Resource = value; }
        }

    }

    [Serializable]
    public class StopSaleResultRecord
    {
        public StopSaleResultRecord()
        {
        }

        string _HotelCode;
        public string HotelCode
        {
            get { return _HotelCode; }
            set { _HotelCode = value; }
        }
        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Accom;
        public string Accom
        {
            get { return _Accom; }
            set { _Accom = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }
        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
        string _AppType;
        public string AppType
        {
            get { return _AppType; }
            set { _AppType = value; }
        }

        int? _Location;
        public int? Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        byte? _AllotType;
        public byte? AllotType
        {
            get { return _AllotType; }
            set { _AllotType = value; }
        }

        string _City;
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }
        string _PriceList;
        public string PriceList
        {
            get { return _PriceList; }
            set { _PriceList = value; }
        }

    }
    
    public class StopSaleCityRecord
    {
        public StopSaleCityRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
    }
    
    public class StopSaleDefaultRecord
    {
        public StopSaleDefaultRecord()
        {
            _OptionType = 0;
        }

        int _OptionType;
        public int OptionType
        {
            get { return _OptionType; }
            set { _OptionType = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }
        
        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _DateFormat;
        public string DateFormat
        {
            get { return _DateFormat; }
            set { _DateFormat = value; }
        }

        List<CodeName> _HotelList;
        public List<CodeName> HotelList
        {
            get { return _HotelList; }
            set { _HotelList = value; }
        }

        List<CodeName> _FlgCls;
        public List<CodeName> FlgCls
        {
            get { return _FlgCls; }
            set { _FlgCls = value; }
        }
        
        List<StopSaleCityRecord> _DepCity;
        public List<StopSaleCityRecord> DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }        
    }

    public class StopSaleFlightSeatOpt
    {
        public StopSaleFlightSeatOpt()
        {
        }

        DateTime _FlyDate;
        public DateTime FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        int? _Night;
        public int? Night
        {
            get { return _Night; }
            set { _Night = value; }
        }

        int? _Seat;
        public int? Seat
        {
            get { return _Seat; }
            set { _Seat = value; }
        }
    }
}
