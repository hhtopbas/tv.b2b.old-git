﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class Promos
    {
        public List<PromoRecord> getListPromo(string Market, string JoinTable, ref string errorMsg)
        {
            List<PromoRecord> records = new List<PromoRecord>();
            string tsql = @"Select P.RecID, P.Name, NameL=isnull(dbo.FindLocalName(P.NameLID, @Market), P.Name),
	                            P.PromoType, P.Description, P.PayCom, P.PayComEB, P.PayPasEB, P.CanDiscount, P.PasEBValid, 
                                P.SpoValid, P.ValidWithPromo
                            From Promo (NOLOCK) P
                            Join #JoinTable jT ON jT.PromoID=P.RecID";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(JoinTable + tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        PromoRecord record = new PromoRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.PromoType = Conversion.getInt16OrNull(R["PromoType"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.PayCom = Conversion.getStrOrNull(R["PayCom"]);
                        record.PayComEB = Conversion.getStrOrNull(R["PayComEB"]);
                        record.PayPasEB = Conversion.getBoolOrNull(R["PayPasEB"]);
                        record.CanDiscount = Conversion.getBoolOrNull(R["CanDiscount"]);
                        record.PasEBValid = Conversion.getBoolOrNull(R["PasEBValid"]);
                        record.SpoValid = Conversion.getBoolOrNull(R["SpoValid"]);
                        record.ValidWithPromo = Conversion.getBoolOrNull(R["ValidWithPromo"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public PromoRecord getPromo(string Market, int? RecID, ref string errorMsg)
        {
            string tsql = @"Select RecID, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name),
	                            PromoType, [Description], PayCom, PayComEB, PayPasEB, CanDiscount, PasEBValid, 
                                SpoValid, ValidWithPromo
                            From Promo (NOLOCK)
                            Where RecID=@RecID";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        PromoRecord record = new PromoRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.PromoType = Conversion.getInt16OrNull(R["PromoType"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.PayCom = Conversion.getStrOrNull(R["PayCom"]);
                        record.PayComEB = Conversion.getStrOrNull(R["PayComEB"]);
                        record.PayPasEB = Conversion.getBoolOrNull(R["PayPasEB"]);
                        record.CanDiscount = Conversion.getBoolOrNull(R["CanDiscount"]);
                        record.PasEBValid = Conversion.getBoolOrNull(R["PasEBValid"]);
                        record.SpoValid = Conversion.getBoolOrNull(R["SpoValid"]);
                        record.ValidWithPromo = Conversion.getBoolOrNull(R["ValidWithPromo"]);
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PromoListRecord> getPromoListTemplate(string Market, List<PromoListRecord> source, ref string errorMsg)
        {
            List<PromoListRecord> records = new List<PromoListRecord>();
            var query = from q in source
                        group q by new { q.PromoID } into g
                        select new { RecID = g.Key.PromoID };
            string joinTable = string.Empty;
            foreach (var row in query)
            {
                joinTable += string.Format("Insert Into #JoinTable (PromoID) Values ({0}) \n", row.RecID);
            }
            joinTable = "Create Table #JoinTable (PromoID int) \n" + joinTable;
            List<PromoRecord> listPromo = new Promos().getListPromo(Market, joinTable, ref errorMsg);
            records = (from r1 in source
                       join r2 in listPromo on r1.PromoID equals r2.RecID
                       select new PromoListRecord
                       {
                           Amount = r1.Amount,
                           CalcType = r1.CalcType,
                           ChdG1MaxAge = r1.ChdG1MaxAge,
                           ChdG2MaxAge = r1.ChdG2MaxAge,
                           ChdG3MaxAge = r1.ChdG3MaxAge,
                           ConfReq = r1.ConfReq,
                           CustNo = r1.CustNo,
                           Message = r1.Message,
                           PriceType = r1.PriceType,
                           PromoID = r1.PromoID,
                           PromoName = r2.Name,
                           PromoNameL = r2.NameL,
                           PromoPriceID = r1.PromoPriceID,
                           PromoType = r1.PromoType,
                           RecID = r1.RecID,
                           ResCur = r1.ResCur,
                           SaleChdG1Price = r1.SaleChdG1Price,
                           SaleChdG2Price = r1.SaleChdG2Price,
                           SaleChdG3Price = r1.SaleChdG3Price,
                           SaleCur = r1.SaleCur,
                           SalePrice = r1.SalePrice,
                           PasEBValid = r2.PasEBValid,
                           SpoValid = r2.SpoValid,
                           ValidWithPromo = r2.ValidWithPromo
                       }).ToList<PromoListRecord>();
            return records;
        }

        public List<PromoListRecord> getPromotionList(string Market, string ResNo, ref string errorMsg)
        {
            List<PromoListRecord> records = new List<PromoListRecord>();
            string tsql = @"Select PL.RecID, PL.PromoType, PL.PromoID, PromoName=P.Name, PromoNameL=isnull(dbo.FindLocalName(P.NameLID, @Market), P.Name),
	                            PL.PromoPriceID, PL.ConfReq, PL.Amount, PL.ResCur, PL.[Message], PL.CustNo, 
	                            PL.PriceType, PL.CalcType, PL.SaleCur, PL.SalePrice, 
	                            PL.ChdG1MaxAge, PL.SaleChdG1Price, PL.ChdG2MaxAge, PL.SaleChdG2Price, PL.ChdG3MaxAge, PL.SaleChdG3Price
                            From dbo.getPromoList(@ResNo) PL
                            Join Promo P (NOLOCK) ON P.RecID=PL.PromoID";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        PromoListRecord record = new PromoListRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.PromoType = Conversion.getInt16OrNull(R["PromoType"]);
                        record.PromoID = Conversion.getInt32OrNull(R["PromoID"]);
                        record.PromoName = Conversion.getStrOrNull(R["PromoName"]);
                        record.PromoNameL = Conversion.getStrOrNull(R["PromoNameL"]);
                        record.PromoPriceID = Conversion.getInt32OrNull(R["PromoPriceID"]);
                        record.ConfReq = Conversion.getBoolOrNull(R["ConfReq"]);
                        record.Amount = Conversion.getDecimalOrNull(R["Amount"]);
                        record.ResCur = Conversion.getStrOrNull(R["ResCur"]);
                        record.Message = Conversion.getStrOrNull(R["Message"]);
                        record.CustNo = Conversion.getInt32OrNull(R["CustNo"]);
                        record.PriceType = Conversion.getInt16OrNull(R["PriceType"]);
                        record.CalcType = Conversion.getInt16OrNull(R["CalcType"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.ChdG1MaxAge = Conversion.getDecimalOrNull(R["ChdG1MaxAge"]);
                        record.SaleChdG1Price = Conversion.getDecimalOrNull(R["SaleChdG1Price"]);
                        record.ChdG2MaxAge = Conversion.getDecimalOrNull(R["ChdG2MaxAge"]);
                        record.SaleChdG2Price = Conversion.getDecimalOrNull(R["SaleChdG2Price"]);
                        record.ChdG3MaxAge = Conversion.getDecimalOrNull(R["ChdG3MaxAge"]);
                        record.SaleChdG3Price = Conversion.getDecimalOrNull(R["SaleChdG3Price"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool savePromotion(User UserData, ResPromoRecord record, ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string tsql = @"
                            Insert Into ResPromo (ResNo, CustNo, PromoType, PromoID, SeqNo, Confirmed, Amount, SaleCur, Status, PromoPriceID) 
                            Values (@ResNo, @CustNo, @PromoType, @PromoID, @SeqNo, @Confirmed, @Amount, @SaleCur, @Status, @PromoPriceID)
                           ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, record.ResNo);
                db.AddInParameter(dbCommand, "CustNo", DbType.Int32, record.CustNo);
                db.AddInParameter(dbCommand, "PromoType", DbType.Int16, record.PromoType);
                db.AddInParameter(dbCommand, "PromoID", DbType.Int32, record.PromoID);
                db.AddInParameter(dbCommand, "SeqNo", DbType.Int16, record.SeqNo);
                db.AddInParameter(dbCommand, "Confirmed", DbType.Boolean, record.Confirmed);
                db.AddInParameter(dbCommand, "Amount", DbType.Decimal, record.Amount);
                db.AddInParameter(dbCommand, "SaleCur", DbType.String, record.SaleCur);
                db.AddInParameter(dbCommand, "Status", DbType.Boolean, record.Status);
                db.AddInParameter(dbCommand, "PromoPriceID", DbType.Int32, record.PromoPriceID);
                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getPromotionAgencyInfo(User UserData, string promoCode, ref string errorMsg)
        {
            string tsql = @"Exec dbo.sp_GetPromotionInfo @promoCode, @market";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "promoCode", DbType.AnsiString, promoCode);
                db.AddInParameter(dbCommand, "market", DbType.AnsiString, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                        return Conversion.getStrOrNull(R["Agency"]);
                    else
                        return string.Empty;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool checkThisReservationPromo(User UserData, ResDataRecord ResData, ref string errorMsg)
        {
            string tsql =
@"
if Exists(Select null From PromoPrice R (NOLOCK)
		  Join Promo P (NOLOCK) on P.RecID=R.PromoID
		  Where
			Market=@Market and
			IsNull(LocFrom,@DepCity)=@DepCity and 
			IsNull(LocTo,@ArrCity)=@ArrCity and
			(HolPack='' or HolPack=IsNull(@HolPack,'')) and
			(Agency='' or Agency=IsNull(@Agency,'')) and
			(Hotel='' or Hotel=IsNull(@Hotel,'')) and
			(HotelCat='' or HotelCat=IsNull(@HotelCat,'')) and
			(Room='' or Room=IsNull(@Room,'')) and
			(Accom='' or Accom=IsNull(@Accom,'')) and
			(Board='' or Board=IsNull(@Board,'')) and
			(Flight='' or Flight=IsNull(@Flight,'')) and
			(SClass='' or SClass=IsNull(@SClass,'')) and
			@SaleDate between SaleBegDate and SaleEndDate and
			@ResBegDate between ResBegDate and ResEndDate and
			@Night between IsNull(MinNight,0) and IsNull(MaxNight,999) and
			(SaleResource Is Null or SaleResource=@SaleResource) and			
			P.PromoType=3)
  Select Cast(1 As Bit)
Else
  Select Cast(0 As Bit)
";

            ResServiceRecord hotelService = ResData.ResService.Where(w => w.ServiceType == "HOTEL").FirstOrDefault();
            HotelRecord hotel = null;
            if (hotelService != null)            
                hotel = new Hotels().getHotelDetail(UserData, hotelService.Service, ref errorMsg);
            
            ResServiceRecord flightService = ResData.ResService.Where(f => f.ServiceType == "FLIGHT").FirstOrDefault();

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, ResData.ResMain.PLMarket);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, ResData.ResMain.DepCity);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, ResData.ResMain.ArrCity);
                db.AddInParameter(dbCommand, "HolPack", DbType.AnsiString, ResData.ResMain.HolPack);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Hotel", DbType.AnsiString, hotelService != null ? hotelService.Service : null);
                db.AddInParameter(dbCommand, "HotelCat", DbType.AnsiString, hotel != null ? hotel.Category : null);
                db.AddInParameter(dbCommand, "Room", DbType.AnsiString, hotelService != null ? hotelService.Room:null);
                db.AddInParameter(dbCommand, "Accom", DbType.AnsiString, hotelService != null ? hotelService.Accom:null);
                db.AddInParameter(dbCommand, "Board", DbType.AnsiString, hotelService != null ? hotelService.Board : null);
                db.AddInParameter(dbCommand, "Flight", DbType.AnsiString, flightService != null ? flightService.Service : null);
                db.AddInParameter(dbCommand, "SClass", DbType.AnsiString, flightService != null ? flightService.FlgClass : null);
                db.AddInParameter(dbCommand, "SaleDate", DbType.DateTime, ResData.ResMain.ResDate);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.DateTime, ResData.ResMain.BegDate);
                db.AddInParameter(dbCommand, "Night", DbType.Int16, ResData.ResMain.Days);
                db.AddInParameter(dbCommand, "SaleResource", DbType.Int16, ResData.ResMain.SaleResource);


                bool? retVal = Conversion.getBoolOrNull(db.ExecuteScalar(dbCommand));
                return retVal.HasValue && retVal.Value;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
