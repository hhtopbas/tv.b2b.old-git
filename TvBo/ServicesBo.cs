﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    [Serializable]
    public class CatPackPlanSerRecord
    {
        public CatPackPlanSerRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int? _CatPackID;
        public int? CatPackID
        {
            get { return _CatPackID; }
            set { _CatPackID = value; }
        }

        int? _StepNo;
        public int? StepNo
        {
            get { return _StepNo; }
            set { _StepNo = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _ServiceItem;
        public string ServiceItem
        {
            get { return _ServiceItem; }
            set { _ServiceItem = value; }
        }

        string _IncPack;
        public string IncPack
        {
            get { return _IncPack; }
            set { _IncPack = value; }
        }

        string _ValAccomNights;
        public string ValAccomNights
        {
            get { return _ValAccomNights; }
            set { _ValAccomNights = value; }
        }

        Int16? _DispNo;
        public Int16? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

        bool? _Pilot;
        public bool? Pilot
        {
            get { return _Pilot; }
            set { _Pilot = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _SClass;
        public string SClass
        {
            get { return _SClass; }
            set { _SClass = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }
    }

    [Serializable]
    public class CatPackPlanRecord
    {
        public CatPackPlanRecord() { }
        int? _RecID; public int? RecID { get { return _RecID; } set { _RecID = value; } }
        int? _CatPackID; public int? CatPackID { get { return _CatPackID; } set { _CatPackID = value; } }
        string _HolPack; public string HolPack { get { return _HolPack; } set { _HolPack = value; } }
        int? _StepNo; public int? StepNo { get { return _StepNo; } set { _StepNo = value; } }
        string _Service; public string Service { get { return _Service; } set { _Service = value; } }
        Int16? _StartDay; public Int16? StartDay { get { return _StartDay; } set { _StartDay = value; } }
        Int16? _Duration; public Int16? Duration { get { return _Duration; } set { _Duration = value; } }
        int? _Departure; public int? Departure { get { return _Departure; } set { _Departure = value; } }
        int? _Arrival; public int? Arrival { get { return _Arrival; } set { _Arrival = value; } }
        string _IncPack; public string IncPack { get { return _IncPack; } set { _IncPack = value; } }
        string _Priced; public string Priced { get { return _Priced; } set { _Priced = value; } }
        string _AccomNights; public string AccomNights { get { return _AccomNights; } set { _AccomNights = value; } }
        string _Explanation; public string Explanation { get { return _Explanation; } set { _Explanation = value; } }
        Int16? _DispOrd; public Int16? DispOrd { get { return _DispOrd; } set { _DispOrd = value; } }
        string _Direction; public string Direction { get { return _Direction; } set { _Direction = value; } }
        string _PayCom; public string PayCom { get { return _PayCom; } set { _PayCom = value; } }
        string _PayComEB; public string PayComEB { get { return _PayComEB; } set { _PayComEB = value; } }
        bool? _PayPasEB; public bool? PayPasEB { get { return _PayPasEB; } set { _PayPasEB = value; } }
        bool? _CanDiscount; public bool? CanDiscount { get { return _CanDiscount; } set { _CanDiscount = value; } }
        byte? _StepType; public byte? StepType { get { return _StepType; } set { _StepType = value; } }
        bool? _CanEdit; public bool? CanEdit { get { return _CanEdit; } set { _CanEdit = value; } }
        bool? _CanDelete; public bool? CanDelete { get { return _CanDelete; } set { _CanDelete = value; } }
    }
}
