﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;
using System.Collections;
using System.Web;

namespace TvBo
{
    public class Transfers
    {
        public TransferRecord getTransfer(string Market, string Code, ref string errorMsg)
        {
            TransferRecord record = new TransferRecord();
            string tsql = string.Empty;
            tsql = @"   Select T.Code,T.Name,LocalName=isnull(dbo.FindLocalName(T.NameLID,@Market),T.Name),
	                        T.NameS,T.TrfType,TrfTypeName=TT.Name,TrfTypeNameL=isnull(dbo.FindLocalName(TT.NameLID,@Market),TT.Name),
                            T.Direction,T.ConfStat,T.TaxPer,T.PayCom,T.PayComEB,T.PayPasEB,T.CanDiscount,T.B2CPub,T.B2BPub
                        From Transfer T (NOLOCK)
                        Left Join TransType TT (NOLOCK) ON TT.Code=T.TrfType
                        Where T.Code = @Code";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.TrfType = Conversion.getStrOrNull(R["TrfType"]);
                        record.TrfTypeName = Conversion.getStrOrNull(R["TrfTypeName"]);
                        record.TrfTypeNameL = Conversion.getStrOrNull(R["TrfTypeNameL"]);
                        record.Direction = Conversion.getStrOrNull(R["Direction"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = Equals(R["PayPasEB"], "Y");
                        record.CanDiscount = (bool)R["CanDiscount"];
                        record.B2BPub = (bool)R["B2BPub"];
                        record.B2CPub = (bool)R["B2CPub"];
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransferPriceRecord> getTransferPrices(string transferCode, int? depLocation, int? arrLocation, int? country, int? trfLocation, string plMarket, DateTime trfDate, ref string errorMsg)
        {
            List<TransferPriceRecord> records = new List<TransferPriceRecord>();

            #region 2011.12.20 Tourvisio
            /*
            string tsql = @"if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
                                Select RecID, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
                                Into #Location
                                From Location (NOLOCK)
                                Select Transfer, Market, Supplier, 
	                                Country, CountryName=(Select Name From #Location Where RecID=TP.Country), 
	                                CountryNameL=(Select NameL From #Location Where RecID=TP.Country),
	                                BegDate, EndDate, 
	                                TrfFrom, TrfFromName=(Select Name From #Location Where RecID=TP.TrfFrom),
	                                TrfFromNameL=(Select NameL From #Location Where RecID=TP.TrfFrom),
	                                TrfTo, TrfToName=(Select Name From #Location Where RecID=TP.TrfTo), 
	                                TrfToNameL=(Select NameL From #Location Where RecID=TP.TrfTo), 
	                                FromPax, 
	                                ToPax,
                                    PriceType, NetCur, SaleCur, FreeMaxAge, ChdG1MaxAge,ChdG2MaxAge  
                                From TransferPrice TP (NOLOCK) 
                                Where Market=@plMarket
                                  And @trfDate between BegDate And EndDate
                                  And Transfer=@code ";
            */
            #endregion

            string tsql = @"Declare @RecID int, @RowCnt int
                            Select @RecID=RecID
                            From TransferPrice P (NOLOCK)
                            Where Transfer=@Service and Market=@PLMarket
                            and IsNull(TrfFrom,0)=@DepLocation
                            and IsNull(TrfTo,0)=@ArrLocation
                            and @BegDate between BegDate and EndDate --and @GrpPax between FromPax and ToPax
                            Order By NetAdlPrice
                            Set @RowCnt=@@ROWCOUNT
                            if @RowCnt=0
                             begin
	                            Select @RecID=RecID
	                            From TransferPrice P (NOLOCK)
	                            Where Transfer=@Service and Market=@PLMarket
	                            and IsNull(TrfFrom,0)=@DepLocation
	                            and IsNull(TrfTo,0)=@TrfLocation
	                            and @BegDate between BegDate and EndDate --and @GrpPax between FromPax and ToPax
	                            Order By NetAdlPrice                            	
	                            Set @RowCnt=@@ROWCOUNT
	                            Select top 1 @RecID=RecID
	                            From TransferPrice P (NOLOCK)
	                            Where Transfer=@Service and Market=@PLMarket
	                            and (
	                            IsNull(TrfFrom,0) in (Select LocID from dbo.ufn_LocationUp(@DepLocation,1)) or
	                            IsNull(TrfFrom,0) in (Select LocID from dbo.ufn_LocationUp(@TrfLocation,1)) or
	                            TrfFrom is Null
	                            )
	                            and (
	                            IsNull(TrfTo,0) in (Select LocID from dbo.ufn_LocationUp(@ArrLocation,1)) or
	                            IsNull(TrfTo,0) in (Select LocID from dbo.ufn_LocationUp(@TrfLocation,1)) or
	                            TrfTo is Null
	                            )
	                            and @BegDate between BegDate and EndDate --and @GrpPax between FromPax and ToPax
	                            Order By NetAdlPrice
                             end
                            IF @RowCnt=0 --Global search for upper locations
                             Begin
                               Select top 1 @RecID=RecID
                               From TransferPrice P (NOLOCK)
                               Where Transfer=@Service and Market=@PLMarket
                                     and (
                                          IsNull(TrfFrom,0) in (Select LocID from dbo.ufn_LocationUp(@DepLocation,1)) or
                                          IsNull(TrfFrom,0) in (Select LocID from dbo.ufn_LocationUp(@TrfLocation,1)) or
                                          TrfFrom is Null
                                         ) 
                                     and (
                                          IsNull(TrfTo,0) in (Select LocID from dbo.ufn_LocationUp(@ArrLocation,1)) or
                                          IsNull(TrfTo,0) in (Select LocID from dbo.ufn_LocationUp(@TrfLocation,1)) or
                                          TrfTo is Null
                                         )
                                     and @BegDate between BegDate and EndDate --and @GrpPax between FromPax and ToPax
                               Order By NetAdlPrice
                               Set @RowCnt=@@ROWCOUNT
                             end
                            if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
                            Select RecID, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
                            Into #Location
                            From Location (NOLOCK)
                            Select Transfer, Market, Supplier, 
                                Country, CountryName=(Select Name From #Location Where RecID=TP.Country), 
                                CountryNameL=(Select NameL From #Location Where RecID=TP.Country),
                                BegDate, EndDate, 
                                TrfFrom, TrfFromName=(Select Name From #Location Where RecID=TP.TrfFrom),
                                TrfFromNameL=(Select NameL From #Location Where RecID=TP.TrfFrom),
                                TrfTo, TrfToName=(Select Name From #Location Where RecID=TP.TrfTo), 
                                TrfToNameL=(Select NameL From #Location Where RecID=TP.TrfTo), 
                                FromPax, 
                                ToPax,
                                PriceType, NetCur, SaleCur, FreeMaxAge, ChdG1MaxAge,ChdG2MaxAge  
                            From TransferPrice TP (NOLOCK) 
                            Where TP.RecID=@RecID
                        ";

            //Declare @Service VarChar(10), @PLMarket VarChar(10), @Market VarChar(10), @DepLocation int, @ArrLocation int, @BegDate DateTime, @GrpPax int, @RowCnt int, @TrfLocation int

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PLMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Market", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "BegDate", DbType.Date, trfDate.Date);
                db.AddInParameter(dbCommand, "Service", DbType.String, transferCode);
                db.AddInParameter(dbCommand, "DepLocation", DbType.Int32, depLocation);
                db.AddInParameter(dbCommand, "ArrLocation", DbType.Int32, arrLocation);
                db.AddInParameter(dbCommand, "TrfLocation", DbType.Int32, trfLocation);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TransferPriceRecord record = new TransferPriceRecord();
                        record.Transfer = Conversion.getStrOrNull(R["Transfer"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        record.Country = Conversion.getInt32OrNull(R["Country"]);
                        record.CountryName = Conversion.getStrOrNull(R["CountryName"]);
                        record.CountryNameL = Conversion.getStrOrNull(R["CountryNameL"]);
                        record.BegDate = (DateTime)R["BegDate"];
                        record.EndDate = (DateTime)R["EndDate"];
                        record.TrfFrom = Conversion.getInt32OrNull(R["TrfFrom"]);
                        record.TrfFromName = Conversion.getStrOrNull(R["TrfFromName"]);
                        record.TrfFromNameL = Conversion.getStrOrNull(R["TrfFromNameL"]);
                        record.TrfTo = Conversion.getInt32OrNull(R["TrfTo"]);
                        record.TrfToName = Conversion.getStrOrNull(R["TrfToName"]);
                        record.TrfToNameL = Conversion.getStrOrNull(R["TrfToNameL"]);
                        record.FromPax = Conversion.getInt32OrNull(R["FromPax"]);
                        record.ToPax = Conversion.getInt32OrNull(R["ToPax"]);
                        record.PriceType = Conversion.getStrOrNull(R["PriceType"]);
                        record.NetCur = Conversion.getStrOrNull(R["NetCur"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.FreeMaxAge = Conversion.getDecimalOrNull(R["FreeMaxAge"]);
                        record.ChdG1MaxAge = Conversion.getDecimalOrNull(R["ChdG1MaxAge"]);
                        record.ChdG2MaxAge = Conversion.getDecimalOrNull(R["ChdG2MaxAge"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public DataRow getTransferDetail(string Market, string TransferCode, int? DepLocation, int? ArrLocation, int? Country, int? trfLocation, string PLMarket, DateTime TDate, ref string errorMsg)
        {
            TransferRecord transfer = getTransfer(Market, TransferCode, ref errorMsg);
            List<TransferPriceRecord> trfPrices = getTransferPrices(TransferCode, DepLocation, ArrLocation, Country, trfLocation, PLMarket, TDate, ref errorMsg);
            var result = from q in trfPrices.AsEnumerable()
                         where q.Transfer == transfer.Code
                         select new
                         {
                             Name = transfer.Name,
                             LocalName = transfer.LocalName,
                             CanDiscount = transfer.CanDiscount,
                             FreeMaxAge = q.FreeMaxAge
                         };
            return new TvBo.Common().LINQToDataRow(result);
        }

        public List<TransferPriceRecord> getTransferLocations(string Market, string plMarket, List<int?> Country, List<int?> city, DateTime? resBegDate, DateTime? resEndDate, bool? RT, ref string errorMsg)
        {
            List<TransferPriceRecord> list = new List<TransferPriceRecord>();
            string tsql = @"if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
                            Select RecID, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
                            Into #Location
                            From Location (NOLOCK)

                            Select Transfer, Market, Supplier, 
                                Country, CountryName=(Select Name From #Location Where RecID=TP.Country), 
                                CountryNameL=(Select NameL From #Location Where RecID=TP.Country),
                                BegDate, EndDate, 
                                TrfFrom, TrfFromName=(Select Name From #Location Where RecID=TP.TrfFrom),
                                TrfFromNameL=(Select NameL From #Location Where RecID=TP.TrfFrom),
                                TrfTo, TrfToName=(Select Name From #Location Where RecID=TP.TrfTo), 
                                TrfToNameL=(Select NameL From #Location Where RecID=TP.TrfTo), 
                                FromPax, ToPax, PriceType, NetCur, SaleCur, FreeMaxAge, ChdG1MaxAge,ChdG2MaxAge      
                            From TransferPrice TP (NOLOCK) 
                            Where Market=@plMarket
                            and GetDate() between IsNull(SaleBegDate,GetDate()) and IsNull(SaleEndDate,GetDate()) --24.09.2018 ic.added,tv-65
                            --And TP.TrfFrom is not null
                            --And TP.TrfTo is not null
                            And Exists(Select RecID From Transfer Where Code=TP.Transfer And B2BPub=1 And (@Direction='' Or Direction=@Direction)) ";
            if (city != null && city.Count > 0)
            {
                if (city.Count == 1)
                    tsql += string.Format(" And Exists(Select RecId From Location (NOLOCK) Where RecID=TP.TrfFRom And City={0})", city.First().ToString());
                else
                {
                    string cityStr = string.Empty;
                    foreach (var r in city.Where(w => w.HasValue))
                    {
                        if (cityStr.Length > 0)
                            cityStr += ",";
                        cityStr += r.Value.ToString();
                    }
                    tsql += string.Format(" And Exists(Select null From Location (NOLOCK) Where (RecID=TP.TrfFRom Or RecID=TP.TrfTo) And City in ({0})) ", cityStr);
                }
            }
            else
                if (Country != null && Country.Count > 0)
            {
                if (Country.Count == 1)
                    tsql += string.Format(" And (Country is null or Country={0}) ", Country.FirstOrDefault().Value);
                else
                {
                    string countryStr = string.Empty;
                    foreach (var r in Country.Where(w => w.HasValue))
                    {
                        if (countryStr.Length > 0)
                            countryStr += ",";
                        countryStr += r.Value.ToString();
                    }
                    tsql += string.Format(" And (Country is null or Country in ({0})) ", countryStr);
                }
            }
            if (resBegDate.HasValue)
                tsql += " And BegDate<=@resEndDate ";
            if (resEndDate.HasValue)
                tsql += " And EndDate>=@resBegDate ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Direction", DbType.String, RT.HasValue && RT.Value ? "R" : "");
                //if (Country != null && )
                //    db.AddInParameter(dbCommand, "Country", DbType.Int32, Country.Value);
                if (resBegDate.HasValue)
                    db.AddInParameter(dbCommand, "resEndDate", DbType.DateTime, resBegDate.Value);
                if (resEndDate.HasValue)
                    db.AddInParameter(dbCommand, "resBegDate", DbType.DateTime, resEndDate.Value);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TransferPriceRecord record = new TransferPriceRecord();
                        record.Transfer = Conversion.getStrOrNull(R["Transfer"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        record.Country = Conversion.getInt32OrNull(R["Country"]);
                        record.CountryName = record.Country.HasValue ? Conversion.getStrOrNull(R["CountryName"]) : "All Country";
                        record.CountryNameL = record.Country.HasValue ? Conversion.getStrOrNull(R["CountryNameL"]) : HttpContext.GetGlobalResourceObject("Controls", "comboAllLocation").ToString();
                        record.BegDate = (DateTime)R["BegDate"];
                        record.EndDate = (DateTime)R["EndDate"];
                        record.TrfFrom = Conversion.getInt32OrNull(R["TrfFrom"]);
                        record.TrfFromName = record.TrfFrom.HasValue ? Conversion.getStrOrNull(R["TrfFromName"]) : "Alle lokaties";
                        record.TrfFromNameL = record.TrfFrom.HasValue ? Conversion.getStrOrNull(R["TrfFromNameL"]) : HttpContext.GetGlobalResourceObject("Controls", "comboAllLocation").ToString();
                        record.TrfTo = Conversion.getInt32OrNull(R["TrfTo"]);
                        record.TrfToName = record.TrfTo.HasValue ? Conversion.getStrOrNull(R["TrfToName"]) : "Alle lokaties";
                        record.TrfToNameL = record.TrfTo.HasValue ? Conversion.getStrOrNull(R["TrfToNameL"]) : HttpContext.GetGlobalResourceObject("Controls", "comboAllLocation").ToString();
                        record.FromPax = Conversion.getInt32OrNull(R["FromPax"]);
                        record.ToPax = Conversion.getInt32OrNull(R["ToPax"]);
                        record.PriceType = Conversion.getStrOrNull(R["PriceType"]);
                        record.NetCur = Conversion.getStrOrNull(R["NetCur"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.FreeMaxAge = Conversion.getDecimalOrNull(R["FreeMaxAge"]);
                        record.ChdG1MaxAge = Conversion.getDecimalOrNull(R["ChdG1MaxAge"]);
                        record.ChdG2MaxAge = Conversion.getDecimalOrNull(R["ChdG2MaxAge"]);
                        list.Add(record);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransferRecord> getTransfers(User UserData, ResDataRecord ResData, int? DepLocation, int? ArrLocation, bool? B2BPub, bool? B2CPub, ref string errorMsg)
        {
            ResMainRecord resMain = ResData.ResMain;
            string Market = UserData.Market;
            List<TransferRecord> list = new List<TransferRecord>();
            string priceCase = string.Empty;
            bool withHotel = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).Count() > 0;
            bool withFlight = ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).Count() > 0;
            int paxCount = ResData != null && ResData.ResCust != null ? ResData.ResCust.Count : 2;
            bool withExcurPack = ResData.ResService.Where(w => string.Equals(w.ServiceType, "EXCURSION") && (w.GrpPackID.HasValue && w.GrpPackID.Value > 0)).Count() > 0;
            bool withTourPackage = ResData.ResMain.PackType == "T";

            priceCase += string.Format(" And isnull(ValidHotel,0)={0}", withHotel ? 1 : 0);
            priceCase += string.Format(" And isnull(ValidFlight,0)={0}", withFlight ? 1 : 0);
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040016046"))
                priceCase += string.Format(" And isnull(ValidExcurPack,0)={0}", withExcurPack ? 1 : 0);

            if (VersionControl.CheckWebVersion(UserData.WebVersion, 40, VersionControl.Equality.gt))
                if (withTourPackage)
                    priceCase = " And isnull(ValidTour, 0)=1";
            string tsql = string.Format(@"Select T.Code, T.[Name], LocalName=isnull(dbo.FindLocalName(T.NameLID, @Market), T.[Name]),
	                                             T.NameS, T.TrfType, T.ConfStat, T.TaxPer, T.PayCom, T.PayComEB, T.PayPasEB, T.CanDiscount, 
                                                 B2CPub=isnull(T.B2CPub,1), B2BPub=isnull(T.B2BPub,1), Direction=isnull(T.Direction,'')
                                          From (
                                                    Select Distinct Transfer
                                                    From TransferPrice TP (NOLOCK) 
                                                    Where Market=@plMarket
                                                    And dbo.DateOnly(GetDate()) between IsNull(SaleBegDate,dbo.DateOnly(GetDate())) and IsNull(SaleEndDate,dbo.DateOnly(GetDate()))
                                                    And (@EndDate is null or BegDate<=@EndDate)
                                                    And (@BegDate is null or EndDate>=@BegDate)
                                                    And @paxCount between isnull(FromPax,@paxCount) and isnull(ToPax,@paxCount)
                                                    {0} {1} {2}
                                                    And Exists(Select RecID From Transfer Where Code=TP.Transfer And B2BPub=1)
                                                    And ((isnull(TP.Agency,'')='' Or TP.Agency=@AgencyId) And (isnull(TP.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@AgencyId And Groups=TP.AgencyGrp))) 
                                                ) X
                                          Join Transfer T (NOLOCK) ON T.Code=X.Transfer ",
                                          DepLocation != null ? " And TP.TrfFrom=" + DepLocation.Value.ToString() : "",
                                          ArrLocation != null ? " And TP.TrfTo=" + ArrLocation.Value.ToString() : "",
                                          string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) ? (string.IsNullOrEmpty(priceCase) ? string.Empty : priceCase) : string.Empty);
            string whereStr = string.Empty;
            if (B2BPub.HasValue && B2BPub.Value)
            {
                if (whereStr.Length > 0)
                    whereStr += " And ";
                whereStr += " B2BPub=1 ";
            }
            if (B2CPub.HasValue && B2CPub.Value)
            {
                if (whereStr.Length > 0)
                    whereStr += " And ";
                whereStr += " B2CPub=1 ";
            }
            if (whereStr.Length > 0)
                tsql += "Where " + whereStr;

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "plMarket", DbType.AnsiString, resMain.PLMarket);
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, Market);
                db.AddInParameter(dbCommand, "paxCount", DbType.Int32, paxCount);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, resMain.BegDate);
                db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, !Equals(resMain.BegDate, resMain.EndDate) ? resMain.EndDate : null);
                db.AddInParameter(dbCommand, "AgencyId", DbType.AnsiString, UserData.AgencyID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TransferRecord record = new TransferRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.TrfType = Conversion.getStrOrNull(R["TrfType"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = Equals(R["PayPasEB"], "Y");
                        record.CanDiscount = (bool)R["CanDiscount"];
                        record.B2BPub = (bool)R["B2BPub"];
                        record.B2CPub = (bool)R["B2CPub"];
                        record.Direction = Conversion.getStrOrNull(R["Direction"]);
                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransferRecordV2> getTransfersV2(string Market, ResMainRecord resMain, int? DepLocation, int? ArrLocation, ref string errorMsg)
        {
            List<TransferRecordV2> list = new List<TransferRecordV2>();
            /*
             TVB2B-3683 Detur talebine istinaden düzenlenmiştir.
             */
            string tsql = @"Select T.Code, T.[Name], LocalName=isnull(dbo.FindLocalName(T.NameLID, @Market), T.[Name]),
                                     T.NameS, T.TrfType, T.ConfStat, T.TaxPer, T.PayCom, T.PayComEB, T.PayPasEB, T.CanDiscount, 
                                     B2CPub=isnull(T.B2CPub,1), B2BPub=isnull(T.B2BPub,1), Direction=isnull(T.Direction,''),TrfFrom,TrfTo
                              From (
                                        Select Distinct Transfer,TP.TrfFrom,TP.TrfTo
                                        From TransferPrice TP (NOLOCK) 
                                        Where Market=@plMarket
                                        And (@EndDate is null or BegDate<=@EndDate)
                                        And (@BegDate is null or EndDate>=@BegDate)
                                        And ((TP.TrfFrom=@Departure And (@Arrival is null Or TP.TrfTo=@Arrival  or TP.TrfTo is null)) 
                                        Or ((@Arrival is null Or TP.TrfFrom=@Arrival) And TP.TrfTo=@Departure))
                                        And Exists(Select RecID From Transfer Where Code=TP.Transfer And B2BPub=1)	                            
                                        And TP.TrfFrom is not null
                                        --And TP.TrfTo is not null
                                    ) X
                              Join Transfer T (NOLOCK) ON T.Code=X.Transfer 
                              Where B2BPub=1
                              Order By (Case When Direction='F' Then 0 
                                             When Direction='B' Then 1 Else 2 End)
                           ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "plMarket", DbType.String, resMain.PLMarket);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, resMain.BegDate);
                db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, !Equals(resMain.BegDate, resMain.EndDate) ? resMain.EndDate : null);
                db.AddInParameter(dbCommand, "Departure", DbType.Int32, DepLocation);
                db.AddInParameter(dbCommand, "Arrival", DbType.Int32, ArrLocation);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TransferRecordV2 record = new TransferRecordV2();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.TrfType = Conversion.getStrOrNull(R["TrfType"]);
                        record.ConfStat = (Int16)R["ConfStat"];
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = Equals(R["PayPasEB"], "Y");
                        record.CanDiscount = (bool)R["CanDiscount"];
                        record.B2BPub = (bool)R["B2BPub"];
                        record.B2CPub = (bool)R["B2CPub"];
                        record.Direction = Conversion.getStrOrNull(R["Direction"]);
                        record.Departure = Conversion.getInt32OrNull(R["TrfFrom"]);
                        record.Arrival = Conversion.getInt32OrNull(R["TrfTo"]);
                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransTypeRecord> getTransTypes(ref string errorMsg)
        {
            List<TransTypeRecord> T = new List<TransTypeRecord>();
            string tsql =
@"
Select RecID,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),NameS,[Description]
From TransType (NOLOCK)
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        TransTypeRecord rec = new TransTypeRecord();
                        rec.RecID = Conversion.getInt32OrNull(reader["RecID"]);
                        rec.Code = Conversion.getStrOrNull(reader["Code"]);
                        rec.Name = Conversion.getStrOrNull(reader["Name"]);
                        rec.NameL = Conversion.getStrOrNull(reader["NameL"]);
                        rec.NameS = Conversion.getStrOrNull(reader["NameS"]);
                        rec.Description = Conversion.getStrOrNull(reader["Description"]);
                        T.Add(rec);
                    }
                }
                return T;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }
        /*
        public TransTypeRecord getTransTypeByRecID(string Market, int RecID, ref string errorMsg)
        {
            TransTypeRecord T = new TransTypeRecord();
            string tsql =
@"
Select RecID,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),NameS,[Description]
From TransType (NOLOCK)
Where RecID=@RecID
";                
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "RecID", DbType.Int32, RecID);
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, RecID);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        T.RecID = Conversion.getInt32OrNull(reader["RecID"]);
                        T.Code = Conversion.getStrOrNull(reader["Code"]);
                        T.Name = Conversion.getStrOrNull(reader["Name"]);
                        T.NameL = Conversion.getStrOrNull(reader["NameL"]);
                        T.NameS = Conversion.getStrOrNull(reader["NameS"]);
                        T.Description = Conversion.getStrOrNull(reader["Description"]);
                    }
                }
                return T;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        */
        public List<calendarColor> getTransferDates(User UserData, ResDataRecord ResData, int? trfFrom, int? trfTo, string Transfer, ref string errorMsg)
        {
            List<calendarColor> dates = new List<calendarColor>();

            List<string> records = new List<string>();

            string tsql =
@"
Select BegDate,EndDate,[DayofWeek]
From TransferDates (NOLOCK) Td
Where  Td.[Transfer]=@Transfer
    And Market=@plMarket
    And TrfFrom=@trfFrom
    And TrfTo=@trfTo
    And BegDate<=@ResEndDate
    And EndDate>=@ResBegDate
Order By RecID Desc
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Transfer", DbType.String, Transfer);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, ResData.ResMain.PLMarket);
                db.AddInParameter(dbCommand, "trfFrom", DbType.Int32, trfFrom);
                db.AddInParameter(dbCommand, "trfTo", DbType.Int32, trfTo);
                db.AddInParameter(dbCommand, "ResEndDate", DbType.DateTime, ResData.ResMain.BegDate);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.DateTime, ResData.ResMain.EndDate);
                DataSet dbData = db.ExecuteDataSet(dbCommand);
                if (dbData != null && dbData.Tables[0] != null && dbData.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = dbData.Tables[0];
                    for (DateTime exdt = ResData.ResMain.BegDate.Value; exdt <= ResData.ResMain.EndDate.Value; exdt = exdt.AddDays(1))
                    {
                        int day = 0;
                        switch (exdt.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                day = 0;
                                break;
                            case DayOfWeek.Tuesday:
                                day = 1;
                                break;
                            case DayOfWeek.Wednesday:
                                day = 2;
                                break;
                            case DayOfWeek.Thursday:
                                day = 3;
                                break;
                            case DayOfWeek.Friday:
                                day = 4;
                                break;
                            case DayOfWeek.Saturday:
                                day = 5;
                                break;
                            case DayOfWeek.Sunday:
                                day = 6;
                                break;
                            default:
                                day = 0;
                                break;
                        }

                        if ((from d in dt.AsEnumerable()
                             where exdt >= d.Field<DateTime>("BegDate")
                             && exdt <= d.Field<DateTime>("EndDate")
                             && d.Field<string>("DayofWeek").ToString()[day] == '1'
                             select d).Count() > 0)
                            dates.Add(new calendarColor
                            {
                                Day = exdt.Day,
                                Month = exdt.Month,
                                Year = exdt.Year
                            });
                    }
                }
                return dates;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return dates;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }

    public class OnlyTransfers
    {
        public Profmars FindTransferProfit(User UserData, DateTime? trfDate, int? trfFrom, int? trfTo, string trfType, int? day, string NetCur, ref string errorMsg)
        {
            Profmars pf = new Profmars();
            string tsql =
@"
Select Top 1 P.RecID, P.Service, P.RuleNoID, R.RuleNo, R.BegDate, R.EndDate, R.HotCat, R.Room, R.Accom, R.Board, 
    R.FromCity, R.FromAirport, R.ToCity, R.ToAirport, R.Country, R.Location, R.TrfType, R.InsZone, R.VisaType, 
    R.RentCat, R.MinDay, R.MaxDay, R.GenPer, R.GenVal, R.AdlPer, R.AdlVal,
    R.ChdG1Per,R.ChdG1Val, R.ChdG2Per, R.ChdG2Val, R.ChdG3Per, R.ChdG3Val, R.Cur
From GenProfMar P (NOLOCK)
Join ProfRuleDet R on R.RuleNoID=P.RuleNoID and R.Service=P.Service
Where ProfType='I' and 
	P.Market=@Market and 
	P.Service='TRANSFER' and 
	@TrfDate between BegDate and EndDate and 
	(@Day >= MinDay or MinDay is Null) and 
	(@Day <= MaxDay or MaxDay is Null) and 
	(FromCity=@FromLoc or FromCity is Null) and 
	(ToCity=@ToLoc or ToCity is Null) and 
	TrfType=@TrfType
Order by FromCity Desc,ToCity Desc,EndDate-BegDate,P.RecID
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "TrfDate", DbType.DateTime, trfDate);
                db.AddInParameter(dbCommand, "Day", DbType.Int32, day);
                db.AddInParameter(dbCommand, "FromLoc", DbType.Int32, trfFrom);
                db.AddInParameter(dbCommand, "ToLoc", DbType.Int32, trfTo);
                db.AddInParameter(dbCommand, "TrfType", DbType.AnsiString, trfType);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        pf.GenPer = Conversion.getDecimalOrNull(R["GenPer"]);
                        pf.GenVal = Conversion.getDecimalOrNull(R["GenVal"]);
                        pf.AdlPer = Conversion.getDecimalOrNull(R["AdlPer"]);
                        pf.AdlVal = Conversion.getDecimalOrNull(R["AdlVal"]);
                        pf.ChdG1Per = Conversion.getDecimalOrNull(R["ChdG1Per"]);
                        pf.ChdG1Val = Conversion.getDecimalOrNull(R["ChdG1Val"]);
                        pf.ChdG2Per = Conversion.getDecimalOrNull(R["ChdG2Per"]);
                        pf.ChdG2Val = Conversion.getDecimalOrNull(R["ChdG2Val"]);
                        pf.ChdG3Per = Conversion.getDecimalOrNull(R["ChdG3Per"]);
                        pf.ChdG3Val = Conversion.getDecimalOrNull(R["ChdG3Val"]);
                        pf.Cur = Conversion.getStrOrNull(R["Cur"]);
                        if (pf.GenVal.HasValue && pf.GenVal.Value != 0)
                            pf.GenVal = Equals(pf.Cur, NetCur) ? pf.GenVal.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.GenVal.Value, true, ref errorMsg);
                        if (pf.AdlVal.HasValue && pf.AdlVal.Value != 0)
                            pf.AdlVal = Equals(pf.Cur, NetCur) ? pf.AdlVal.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.AdlVal.Value, true, ref errorMsg);
                        if (pf.ChdG1Val.HasValue && pf.ChdG1Val.Value != 0)
                            pf.ChdG1Val = Equals(pf.Cur, NetCur) ? pf.ChdG1Val.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.ChdG1Val.Value, true, ref errorMsg);
                        if (pf.ChdG2Val.HasValue && pf.ChdG2Val.Value != 0)
                            pf.ChdG2Val = Equals(pf.Cur, NetCur) ? pf.ChdG2Val.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.ChdG2Val.Value, true, ref errorMsg);
                        if (pf.ChdG3Val.HasValue && pf.ChdG3Val.Value != 0)
                            pf.ChdG3Val = Equals(pf.Cur, NetCur) ? pf.ChdG3Val.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.ChdG3Val.Value, true, ref errorMsg);
                    }
                    return pf;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return pf;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Location> getTransferCountry(User UserData, ref string errorMsg)
        {
            List<Location> location = new List<Location>();
            string tsql =
@"
Select L.RecID,L.Parent,NameL=isnull(dbo.FindLocalName(L.NameLID,@Market),L.Name),
  L.Code,L.Name,L.NameS,L.Type,L.Village,L.Town,L.City,L.Country,L.TaxPer,L.CountryCode,L.VisaReq
From Location (NOLOCK) L
Join (Select Distinct Country 
      From TransferPrice (NOLOCK)
) C ON L.RecID=C.Country
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        Location lRecord = new Location();
                        lRecord.RecID = (int)oReader["RecID"];
                        lRecord.Parent = (int)oReader["Parent"];
                        lRecord.Name = (string)oReader["Name"];
                        lRecord.NameS = (string)oReader["NameS"];
                        lRecord.NameL = (string)oReader["NameL"];
                        lRecord.Code = (string)oReader["Code"];
                        lRecord.Type = (Int16)oReader["Type"];
                        lRecord.Village = Conversion.getInt32OrNull(oReader["Village"]);
                        lRecord.Town = Conversion.getInt32OrNull(oReader["Town"]);
                        lRecord.City = Conversion.getInt32OrNull(oReader["City"]);
                        lRecord.Country = (int)oReader["Country"];
                        lRecord.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        lRecord.CountryCode = Conversion.getStrOrNull(oReader["CountryCode"]);
                        lRecord.VisaReq = Conversion.getStrOrNull(oReader["VisaReq"]);
                        location.Add(lRecord);
                    }
                }

                return location;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return location;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OnlyTransferFilterData> getOnlyTransferFilters(User UserData, ref string errorMsg)
        {
            string tsql =
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name),City
Into #Location
From Location (NOLOCK)                                

Select Distinct T.Code,
  Country,
  CountryName=L.Name,
  CountryNameL=L.NameL,
  City=LFrom.City,
  CityName=(Select Name From #Location Where RecID=LFrom.City),
  CityNameL=(Select NameL From #Location Where RecID=LFrom.City),
  T.TrfType,
  TrfTypeName=Tt.Name,
  TrfTypeNameL=isnull(dbo.FindLocalName(Tt.NameLID,@Market),Tt.Name),
  TrfFrom,
  TrfFromName=LFrom.Name,
  TrfFromNameL=LFrom.NameL, 
  TrfTo,
  TrfToName=LTo.Name,
  TrfToNameL=LTo.NameL,
  T.Direction,
  T.SerArea
From TransferPrice Tp (NOLOCK) 
JOIN [Transfer] T (NOLOCK) ON T.Code=Tp.[Transfer]
JOIN TransType Tt (NOLOCK) ON Tt.Code=T.TrfType
Join #Location L ON Tp.Country=L.RecID
Join #Location LFrom ON Tp.TrfFrom=LFrom.RecID
Join #Location LTo ON Tp.TrfTo=LTo.RecID
Where Tp.Market=@Market
  and TrfFrom is not null 
  and TrfTo is not null
  and T.B2BPub=1  
  and Tp.EndDate>=dbo.DateOnly(GETDATE())
";

            List<OnlyTransferFilterData> records = new List<OnlyTransferFilterData>();
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        OnlyTransferFilterData rec = new OnlyTransferFilterData();
                        rec.Code = Conversion.getStrOrNull(reader["Code"]);
                        rec.Country = Conversion.getInt32OrNull(reader["Country"]);
                        rec.CountryName = Conversion.getStrOrNull(reader["CountryName"]);
                        rec.CountryNameL = Conversion.getStrOrNull(reader["CountryNameL"]);
                        rec.City = Conversion.getInt32OrNull(reader["City"]);
                        rec.CityName = Conversion.getStrOrNull(reader["CityName"]);
                        rec.CityNameL = Conversion.getStrOrNull(reader["CityNameL"]);
                        rec.TrfType = Conversion.getStrOrNull(reader["TrfType"]);
                        rec.TrfTypeName = Conversion.getStrOrNull(reader["TrfTypeName"]);
                        rec.TrfTypeNameL = Conversion.getStrOrNull(reader["TrfTypeNameL"]);
                        rec.TrfFrom = Conversion.getInt32OrNull(reader["TrfFrom"]);
                        rec.TrfFromName = Conversion.getStrOrNull(reader["TrfFromName"]);
                        rec.TrfFromNameL = Conversion.getStrOrNull(reader["TrfFromNameL"]);
                        rec.TrfTo = Conversion.getInt32OrNull(reader["TrfTo"]);
                        rec.TrfToName = Conversion.getStrOrNull(reader["TrfToName"]);
                        rec.TrfToNameL = Conversion.getStrOrNull(reader["TrfToNameL"]);
                        rec.Direction = Conversion.getStrOrNull(reader["Direction"]);
                        rec.SerArea = Conversion.getInt16OrNull(reader["SerArea"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getCountryFlights(User UserData, int? country, DateTime? flyDate, bool dep, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            if (dep)
                tsql +=
@"  
Select FlightNo,Name=FlightNo + ' | ' + Airline + ' (' + DepAirport + ' [' + Convert(VarChar(5), dbo.TimeOnly(DepTime), 108) +'] -> ' + ArrAirport + ' ['+Convert(VarChar(5), dbo.TimeOnly(ArrTime), 108)+'])'
From FlightDay FD (NOLOCK)
Where FlyDate = @FlyDate
And Exists(Select RecID From Location (NOLOCK) Where RecID = FD.ArrCity And Country=@Country) 
";
            else
                tsql +=
@"
Select FlightNo,Name=FlightNo + ' | ' + Airline + ' (' + DepAirport + ' [' + Convert(VarChar(5), dbo.TimeOnly(DepTime), 108) +'] -> ' + ArrAirport + ' ['+Convert(VarChar(5), dbo.TimeOnly(ArrTime), 108)+'])'
From FlightDay FD (NOLOCK)
Where FlyDate = @FlyDate
And Exists(Select RecID From Location (NOLOCK) Where RecID = FD.DepCity And Country=@Country) 
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Country", DbType.Int32, country);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, flyDate);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = Conversion.getStrOrNull(reader["FlightNo"]),
                            Name = Conversion.getStrOrNull(reader["Name"])
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getLocationFlights(User UserData, int? location, DateTime? flyDate, bool dep, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            if (dep)
                tsql +=
@"  
Select FlightNo,Name=FlightNo + ' | ' + Airline + ' (' + DepAirport + ' [' + Convert(VarChar(5), dbo.TimeOnly(isnull(DepTime,0)), 108) +'] -> ' + ArrAirport + ' ['+Convert(VarChar(5), dbo.TimeOnly(isnull(ArrTime,0)), 108)+'])'
From FlightDay FD (NOLOCK)
Join AirPort A (NOLOCK) ON A.Code=FD.DepAirport
Where FlyDate = @FlyDate
And Exists(Select RecID From Location (NOLOCK) Where RecID=isnull(A.TrfLocation,A.Location) And RecID=@location) 
";
            else
                tsql +=
@"
Select FlightNo,Name=FlightNo + ' | ' + Airline + ' (' + DepAirport + ' [' + Convert(VarChar(5), dbo.TimeOnly(isnull(DepTime,0)), 108) +'] -> ' + ArrAirport + ' ['+Convert(VarChar(5), dbo.TimeOnly(isnull(ArrTime,0)), 108)+'])'
From FlightDay FD (NOLOCK)
Join AirPort A (NOLOCK) ON A.Code=FD.ArrAirport
Where FlyDate = @FlyDate
And Exists(Select RecID From Location (NOLOCK) Where RecID=isnull(A.TrfLocation,A.Location) And RecID=@location) 
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "location", DbType.Int32, location);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, flyDate);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = Conversion.getStrOrNull(reader["FlightNo"]),
                            Name = Conversion.getStrOrNull(reader["Name"])
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getHotelsFlights(User UserData, string[] hotels, DateTime? flyDate, bool dep, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();

            string tsql = string.Empty;
            if (dep)
                tsql +=
@"  
Select FlightNo,Name=FlightNo + ' | ' + Airline + ' (' + DepAirport + ' [' + Convert(VarChar(5), dbo.TimeOnly(isnull(DepTime,0)), 108) +'] -> ' + ArrAirport + ' ['+Convert(VarChar(5), dbo.TimeOnly(isnull(ArrTime,0)), 108)+'])'
From FlightDay FD (NOLOCK)
Join AirPort A (NOLOCK) ON A.Code=FD.DepAirport
Where FlyDate = @FlyDate
And Exists(Select RecID From Location (NOLOCK) Where RecID=isnull(A.TrfLocation,A.Location) And RecID=@location) 
";
            else
                tsql +=
@"
Select FlightNo,Name=FlightNo + ' | ' + Airline + ' (' + DepAirport + ' [' + Convert(VarChar(5), dbo.TimeOnly(isnull(DepTime,0)), 108) +'] -> ' + ArrAirport + ' ['+Convert(VarChar(5), dbo.TimeOnly(isnull(ArrTime,0)), 108)+'])'
From FlightDay FD (NOLOCK)
Join AirPort A (NOLOCK) ON A.Code=FD.ArrAirport
Where FlyDate = @FlyDate
And Exists(Select RecID From Location (NOLOCK) Where RecID=isnull(A.TrfLocation,A.Location) And RecID=@location) 
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                // db.AddInParameter(dbCommand, "location", DbType.Int32, location);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, flyDate);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = Conversion.getStrOrNull(reader["FlightNo"]),
                            Name = Conversion.getStrOrNull(reader["Name"])
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getCountryHotels(User UserData, int? country, bool useLocalName, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            tsql +=
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID,
    Name=(Case L.Type When 2 Then Name
        WHen 3 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + Name
        When 4 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 Name From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + Name
        Else '' End),
    NameL=(Case L.Type When 2 Then isnull(dbo.FindLocalName(NameLID,@Market),Name) 
        WHen 3 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        When 4 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        Else '' End), L.Type
Into #Location
From Location L (NOLOCK)
Where L.Type <> 1 And L.Country=@Country

Select H.Code, 
    Name=H.Name + '(' + H.Category + ')' + '>>(' + L.Name + ')', 
    NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) + '(' + H.Category + ')' + '>>(' + L.NameL + ')', 
	H.Location
From Hotel H (NOLOCK)
Join #Location L ON L.RecID = H.Location
Where H.Status='Y'
  And Exists(Select RecID From Location (NOLOCK) Where RecID=H.Location And Country=@Country) 
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, country);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = Conversion.getStrOrNull(reader["Code"]),
                            Name = useLocalName ? Conversion.getStrOrNull(reader["NameL"]) : Conversion.getStrOrNull(reader["Name"])
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getCityHotels(User UserData, int? city, bool useLocalName, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            tsql +=
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID,
    Name=(Case L.Type When 2 Then Name
        WHen 3 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + Name
        When 4 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 Name From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + Name
        Else '' End),
    NameL=(Case L.Type When 2 Then isnull(dbo.FindLocalName(NameLID,@Market),Name) 
        WHen 3 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        When 4 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        Else '' End), L.Type
Into #Location
From Location L (NOLOCK)
Where L.City=@City

Select H.Code, 
    Name=H.Name + '(' + H.Category + ')' + '>>(' + L.Name + ')', 
    NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) + '(' + H.Category + ')' + '>>(' + L.NameL + ')', 
	H.Location
From Hotel H (NOLOCK)
Join #Location L ON L.RecID = H.Location
Where H.Status='Y'
  And Exists(Select RecID From Location (NOLOCK) Where RecID=H.Location And City=@City) 
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "City", DbType.Int32, city);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = Conversion.getStrOrNull(reader["Code"]),
                            Name = useLocalName ? Conversion.getStrOrNull(reader["NameL"]) : Conversion.getStrOrNull(reader["Name"])
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getLocationHotels(User UserData, int? location, bool useLocalName, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            tsql +=
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID,
    Name=(Case L.Type When 2 Then Name
        WHen 3 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + Name
        When 4 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 Name From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + Name
        Else '' End),
    NameL=(Case L.Type When 2 Then isnull(dbo.FindLocalName(NameLID,@Market),Name) 
        WHen 3 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        When 4 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        Else '' End), L.Type
Into #Location
From Location L (NOLOCK)

Select H.Code, 
    Name=H.Name + '(' + H.Category + ')' + '>>(' + L.Name + ')', 
    NameL=isnull(dbo.FindLocalName(H.NameLID, @Market), H.Name) + '(' + H.Category + ')' + '>>(' + L.NameL + ')', 
	H.TrfLocation
From Hotel H (NOLOCK)
Join #Location L ON L.RecID = H.Location
Where H.Status='Y'
  And H.TrfLocation=@location
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "location", DbType.Int32, location);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = Conversion.getStrOrNull(reader["Code"]),
                            Name = useLocalName ? Conversion.getStrOrNull(reader["NameL"]) : Conversion.getStrOrNull(reader["Name"])
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getCountryLocations(User UserData, int? country, bool useLocalName, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            tsql +=
@"
Select RecID,
    Name=(Case L.Type When 2 Then Name
        WHen 3 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + Name
        When 4 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 Name From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + Name
        Else '' End),
    NameL=(Case L.Type When 2 Then isnull(dbo.FindLocalName(NameLID,@Market),Name) 
        WHen 3 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        When 4 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        Else '' End), L.Type
From Location L (NOLOCK)
Where L.Type<>1 And L.Country=@Country
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, country);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = reader["RecID"].ToString(),
                            Name = useLocalName ? reader["NameL"].ToString() : reader["Name"].ToString()
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getLocations(User UserData, int? location, bool useLocalName, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            tsql +=
@"
Select RecID,
    Name=(Case L.Type When 2 Then Name
        WHen 3 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + Name
        When 4 Then (Select Top 1 Name From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 Name From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + Name
        Else '' End),
    NameL=(Case L.Type When 2 Then isnull(dbo.FindLocalName(NameLID,@Market),Name) 
        WHen 3 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        When 4 Then (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.City)
		        + '->' + (Select Top 1 isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=L.Town)
		        + '->' + isnull(dbo.FindLocalName(NameLID, @Market),Name)
        Else '' End), L.Type
From Location L (NOLOCK)
Where L.Type<>1 And L.City=@location
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "location", DbType.Int32, new Locations().getLocationForCity(location.HasValue ? location.Value : -1, ref errorMsg));
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = reader["RecID"].ToString(),
                            Name = useLocalName ? reader["NameL"].ToString() : reader["Name"].ToString()
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getCountryTransfers(User UserData, int? country, int? departure, int? arrival, DateTime? date, int? pax, bool useLocalName, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            tsql +=
@"
SET DATEFIRST 1
Select Distinct TP.Transfer,T.Name,NameL=isnull(dbo.FindLocalName(T.NameLID,@Market),T.Name),Code=TP.Transfer+';'+T.Direction
From TransferPrice TP (NOLOCK) 
Join Transfer T (NOLOCK) ON T.Code=TP.Transfer 
Where TP.Market=@Market
  And B2BPub=1  
  And (@Date between TP.BegDate And TP.EndDate)
  And
   (
	 not Exists(Select * From TransferDates (NOLOCK) TD Where TD.Transfer=T.Code and Market=TP.Market)
	 or
	 Exists(Select RecID From TransferDates (NOLOCK) TD
			Where TD.[Transfer]=T.Code and TD.Market=TP.Market
			and (TD.TrfFrom Is Null or isnull(TD.TrfFrom,0)=TP.TrfFrom)
			and (TD.TrfTo Is Null or isnull(TD.TrfTo,0)=TP.TrfTo)
			and SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@Date),1)=1)
   )
  And ((isnull(TP.Agency,'')='' Or TP.Agency=@AgencyId) And (isnull(TP.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@AgencyId And Groups=TP.AgencyGrp))) 
";
            if (departure.HasValue)
                tsql +=
@" 
  And TP.TrfFrom=@departure 
";
            if (arrival.HasValue)
                tsql +=
@" 
  And TP.TrfTo=@arrival
";
            if (pax.HasValue)
                tsql +=
@" 
  And @GrpPax between isnull(TP.FromPax,0) and isnull(TP.ToPax,999)
";
            if (country.HasValue)
                tsql +=
@" 
  And Country=@country
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "AgencyID", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Date", DbType.DateTime, date);
                if (departure.HasValue)
                    db.AddInParameter(dbCommand, "departure", DbType.Int32, departure.Value);
                if (arrival.HasValue)
                    db.AddInParameter(dbCommand, "arrival", DbType.Int32, arrival.Value);
                if (pax.HasValue)
                    db.AddInParameter(dbCommand, "GrpPax", DbType.Int32, pax.Value);
                if (country.HasValue)
                    db.AddInParameter(dbCommand, "country", DbType.Int32, country.Value);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = reader["Code"].ToString(),
                            Name = useLocalName ? reader["NameL"].ToString() : reader["Name"].ToString()
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CodeName> getTransferArrival(User UserData, int? departure, DateTime? date, bool useLocalName, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            tsql +=
@"
SET DATEFIRST 1
Select L.RecID,Name,NameL=isnull(dbo.FindLocalName(L.NameLID,@Market),Name) 
From Location L (NOLOCK)
Where Exists(Select T.RecID From TransferPrice TP (NOLOCK) 
			 Join Transfer T (NOLOCK) ON T.Code=TP.Transfer 
		     Where TP.Market=@Market
			  And B2BPub=1  
			  And (@Date between TP.BegDate And TP.EndDate)  
			  And TrfFrom=@Departure
			  And ((isnull(TP.Agency,'')='' Or TP.Agency=@AgencyId) And (isnull(TP.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@AgencyId And Groups=TP.AgencyGrp))) 
			  And TP.TrfTo=L.RecID
			 )
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "AgencyID", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Date", DbType.DateTime, date);
                db.AddInParameter(dbCommand, "Departure", DbType.Int32, departure);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = reader["Code"].ToString(),
                            Name = useLocalName ? reader["NameL"].ToString() : reader["Name"].ToString()
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OnlyTransfer_Transfers> searchOnlyTransfer(User UserData, int? Pax, string Direction, int? TrfFrom, int? TrfTo, DateTime? BegDate, string TrfType, ref string errorMsg)
        {
            List<OnlyTransfer_Transfers> records = new List<OnlyTransfer_Transfers>();
            string tsql = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050018155"))
                tsql =
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
Into #Location
From Location (NOLOCK)                                

Select Tp.RecID,T.Direction,
  T.TrfType,TrfTypeName=Tt.Name,TrfTypeNameL=isnull(dbo.FindLocalName(Tt.NameLID,@Market),Tt.Name),
  TrfFrom,
  TrfFromName=(Select Name From #Location Where RecID=TP.TrfFrom),
  TrfFromNameL=(Select NameL From #Location Where RecID=TP.TrfFrom),
  TrfTo,
  TrfToName=(Select Name From #Location Where RecID=TP.TrfTo), 
  TrfToNameL=(Select NameL From #Location Where RecID=TP.TrfTo), 
  TransferCode=T.Code,Tp.Market,Tp.BegDate,Tp.EndDate,
  Tp.PriceType,Tp.SaleCur,Tp.SaleAdlPrice,Tp.SaleChdG1Price,Tp.SaleChdG2Price,
  Tp.FreeMaxAge,Tp.ChdG1MaxAge,Tp.ChdG2MaxAge,Tp.VehicleCatID,VehicleCatName=Vc.Name
From TransferPrice Tp (NOLOCK) 
JOIN [Transfer] T (NOLOCK) ON T.Code=Tp.[Transfer]
LEFT JOIN TransType Tt (NOLOCK) ON Tt.Code=T.TrfType
LEFT JOIN VehicleCategory Vc (NOLOCK) ON Tp.VehicleCatID=Vc.RecID
Where Tp.Market=@Market
  And T.Direction=@Direction
  And TrfFrom=@TrfFrom
  And TrfTo=@TrfTo
  And @Pax Between isnull(Tp.FromPax,0) and isnull(ToPax,999)  
  And @BegDate between Tp.BegDate and Tp.EndDate   
  And T.B2BPub=1
  And Tp.EndDate>=dbo.DateOnly(GETDATE())
  And ((isnull(TP.Agency,'')='' Or TP.Agency=@Agency) And (isnull(TP.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@Agency And Groups=TP.AgencyGrp)))  
  And IsNull(Tp.ValidHotel,0)=0
  And IsNull(Tp.ValidFlight,0)=0
  And IsNull(Tp.ValidExcurPack,0)=0
  And IsNull(Tp.ValidTour,0)=0
  And (isnull(@TrfType,'')='' Or T.TrfType=@TrfType)
";
            else
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 40, VersionControl.Equality.gt))
                tsql =
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
Into #Location
From Location (NOLOCK)                                

Select Tp.RecID,T.Direction,
  T.TrfType,TrfTypeName=Tt.Name,TrfTypeNameL=isnull(dbo.FindLocalName(Tt.NameLID,@Market),Tt.Name),
  TrfFrom,
  TrfFromName=(Select Name From #Location Where RecID=TP.TrfFrom),
  TrfFromNameL=(Select NameL From #Location Where RecID=TP.TrfFrom),
  TrfTo,
  TrfToName=(Select Name From #Location Where RecID=TP.TrfTo), 
  TrfToNameL=(Select NameL From #Location Where RecID=TP.TrfTo), 
  TransferCode=T.Code,Tp.Market,Tp.BegDate,Tp.EndDate,
  Tp.PriceType,Tp.SaleCur,Tp.SaleAdlPrice,Tp.SaleChdG1Price,Tp.SaleChdG2Price,
  Tp.FreeMaxAge,Tp.ChdG1MaxAge,Tp.ChdG2MaxAge
From TransferPrice Tp (NOLOCK) 
JOIN [Transfer] T (NOLOCK) ON T.Code=Tp.[Transfer]
LEFT JOIN TransType Tt (NOLOCK) ON Tt.Code=T.TrfType
Where Tp.Market=@Market
  And T.Direction=@Direction
  And TrfFrom=@TrfFrom
  And TrfTo=@TrfTo
  And @Pax Between isnull(Tp.FromPax,0) and isnull(ToPax,999)  
  And @BegDate between Tp.BegDate and Tp.EndDate   
  And T.B2BPub=1
  And Tp.EndDate>=dbo.DateOnly(GETDATE())
  And ((isnull(TP.Agency,'')='' Or TP.Agency=@Agency) And (isnull(TP.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@Agency And Groups=TP.AgencyGrp)))  
  And IsNull(Tp.ValidHotel,0)=0
  And IsNull(Tp.ValidFlight,0)=0
  And IsNull(Tp.ValidExcurPack,0)=0
  And IsNull(Tp.ValidTour,0)=0
  And (isnull(@TrfType,'')='' Or T.TrfType=@TrfType)
";
            else
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040016045"))
                tsql =
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
Into #Location
From Location (NOLOCK)                                

Select Tp.RecID,T.Direction,
  T.TrfType,TrfTypeName=Tt.Name,TrfTypeNameL=isnull(dbo.FindLocalName(Tt.NameLID,@Market),Tt.Name),
  TrfFrom,
  TrfFromName=(Select Name From #Location Where RecID=TP.TrfFrom),
  TrfFromNameL=(Select NameL From #Location Where RecID=TP.TrfFrom),
  TrfTo,
  TrfToName=(Select Name From #Location Where RecID=TP.TrfTo), 
  TrfToNameL=(Select NameL From #Location Where RecID=TP.TrfTo), 
  TransferCode=T.Code,Tp.Market,Tp.BegDate,Tp.EndDate,
  Tp.PriceType,Tp.SaleCur,Tp.SaleAdlPrice,Tp.SaleChdG1Price,Tp.SaleChdG2Price,
  Tp.FreeMaxAge,Tp.ChdG1MaxAge,Tp.ChdG2MaxAge
From TransferPrice Tp (NOLOCK) 
JOIN [Transfer] T (NOLOCK) ON T.Code=Tp.[Transfer]
LEFT JOIN TransType Tt (NOLOCK) ON Tt.Code=T.TrfType
Where Tp.Market=@Market
  And T.Direction=@Direction
  And TrfFrom=@TrfFrom
  And TrfTo=@TrfTo
  And @Pax Between isnull(Tp.FromPax,0) and isnull(ToPax,999)  
  And @BegDate between Tp.BegDate and Tp.EndDate   
  And T.B2BPub=1
  And Tp.EndDate>=dbo.DateOnly(GETDATE())
  And ((isnull(TP.Agency,'')='' Or TP.Agency=@Agency) And (isnull(TP.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@Agency And Groups=TP.AgencyGrp)))  
  And IsNull(Tp.ValidHotel,0)=0
  And IsNull(Tp.ValidFlight,0)=0
  And IsNull(Tp.ValidExcurPack,0)=0  
  And (isnull(@TrfType,'')='' Or T.TrfType=@TrfType)
";
            else
                tsql =
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID, Name, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
Into #Location
From Location (NOLOCK)                                

Select Tp.RecID,T.Direction,
  T.TrfType,TrfTypeName=Tt.Name,TrfTypeNameL=isnull(dbo.FindLocalName(Tt.NameLID,@Market),Tt.Name),
  TrfFrom,
  TrfFromName=(Select Name From #Location Where RecID=TP.TrfFrom),
  TrfFromNameL=(Select NameL From #Location Where RecID=TP.TrfFrom),
  TrfTo,
  TrfToName=(Select Name From #Location Where RecID=TP.TrfTo), 
  TrfToNameL=(Select NameL From #Location Where RecID=TP.TrfTo), 
  TransferCode=T.Code,Tp.Market,Tp.BegDate,Tp.EndDate,
  Tp.PriceType,Tp.SaleCur,Tp.SaleAdlPrice,Tp.SaleChdG1Price,Tp.SaleChdG2Price,
  Tp.FreeMaxAge,Tp.ChdG1MaxAge,Tp.ChdG2MaxAge
From TransferPrice Tp (NOLOCK) 
JOIN [Transfer] T (NOLOCK) ON T.Code=Tp.[Transfer]
LEFT JOIN TransType Tt (NOLOCK) ON Tt.Code=T.TrfType
Where Tp.Market=@Market
  And T.Direction=@Direction
  And TrfFrom=@TrfFrom
  And TrfTo=@TrfTo
  And @Pax Between isnull(Tp.FromPax,0) and isnull(ToPax,999)  
  And @BegDate between Tp.BegDate and Tp.EndDate   
  And T.B2BPub=1
  And Tp.EndDate>=dbo.DateOnly(GETDATE())
  And ((isnull(TP.Agency,'')='' Or TP.Agency=@Agency) And (isnull(TP.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@Agency And Groups=TP.AgencyGrp)))  
  And IsNull(Tp.ValidHotel,0)=0
  And IsNull(Tp.ValidFlight,0)=0
  And (isnull(@TrfType,'')='' Or T.TrfType=@TrfType)
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Pax", DbType.Int32, Pax);
                db.AddInParameter(dbCommand, "TrfFrom", DbType.Int32, TrfFrom);
                db.AddInParameter(dbCommand, "TrfTo", DbType.Int32, TrfTo);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                db.AddInParameter(dbCommand, "Direction", DbType.AnsiString, Direction);
                db.AddInParameter(dbCommand, "TrfType", DbType.AnsiString, TrfType);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    int refNo = 0;
                    while (R.Read())
                    {
                        refNo++;
                        OnlyTransfer_Transfers rec = new OnlyTransfer_Transfers();
                        rec.RefNo = refNo;
                        rec.IsRT = string.Equals(Direction, "R");
                        rec.Market = Conversion.getStrOrNull(R["Market"]);
                        rec.DepDate = BegDate;
                        rec.DepTransfer = Conversion.getStrOrNull(R["TransferCode"]);
                        rec.DepTrfType = Conversion.getStrOrNull(R["TrfType"]);
                        rec.DepTrfTypeName = Conversion.getStrOrNull(R["TrfTypeName"]);
                        rec.DepTrfTypeNameL = Conversion.getStrOrNull(R["TrfTypeNameL"]);
                        rec.DepTrfFrom = Conversion.getInt32OrNull(R["TrfFrom"]);
                        rec.DepTrfFromName = Conversion.getStrOrNull(R["TrfFromName"]);
                        rec.DepTrfFromNameL = Conversion.getStrOrNull(R["TrfFromNameL"]);
                        rec.DepTrfTo = Conversion.getInt32OrNull(R["TrfTo"]);
                        rec.DepTrfToName = Conversion.getStrOrNull(R["TrfToName"]);
                        rec.DepTrfToNameL = Conversion.getStrOrNull(R["TrfToNameL"]);

                        rec.DepPriceType = Conversion.getStrOrNull(R["PriceType"]);
                        rec.DepSaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        rec.DepSaleAdlPrice = Conversion.getDecimalOrNull(R["SaleAdlPrice"]);
                        rec.DepSaleChdG1Price = Conversion.getDecimalOrNull(R["SaleChdG1Price"]);
                        rec.DepSaleChdG2Price = Conversion.getDecimalOrNull(R["SaleChdG2Price"]);
                        rec.DepFreeMaxAge = Conversion.getDecimalOrNull(R["FreeMaxAge"]);
                        rec.DepChdG1MaxAge = Conversion.getDecimalOrNull(R["ChdG1MaxAge"]);
                        rec.DepChdG2MaxAge = Conversion.getDecimalOrNull(R["ChdG2MaxAge"]);

                        rec.DepDirection = Conversion.getStrOrNull(R["Direction"]);

                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("050018155"))
                        {
                            rec.DepVehicleCatID = Conversion.getInt32OrNull(R["VehicleCatID"]);
                            rec.DepVehicleCatName = Conversion.getStrOrNull(R["VehicleCatName"]);
                        }

                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OnlyTransfer_Transfers> getOnlyTransfers(User UserData, OnlyTransferFilter filter, ref string errorMsg)
        {
            bool? onlyTransferSearchRTisOnlyRT = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "OnlyTransferSearchRTisOnlyRT"));
            List<OnlyTransfer_Transfers> depTransfers = new List<OnlyTransfer_Transfers>();
            List<OnlyTransfer_Transfers> retTransfers = new List<OnlyTransfer_Transfers>();
            List<OnlyTransfer_Transfers> transfers = new List<OnlyTransfer_Transfers>();
            switch (filter.Direction)
            {
                case "F":
                    depTransfers = searchOnlyTransfer(UserData, filter.RoomsInfo.Sum(s => s.Adult + s.Child), filter.Direction, filter.TrfFrom, filter.TrfTo, filter.BegDate, filter.TrfType, ref errorMsg);
                    foreach (OnlyTransfer_Transfers row in depTransfers)
                    {
                        OnlyTransfer_Transfers addRow = new OnlyTransfer_Transfers();
                        row.SaleCur = !filter.CurControl && string.IsNullOrEmpty(filter.CurrentCur) ? UserData.SaleCur : row.DepSaleCur;
                        addRow = calcSalePrice(UserData, row, filter);
                        addRow.depTransferRecord = new Transfers().getTransfer(UserData.Market, row.DepTransfer, ref errorMsg);
                        if (addRow.DepPriceCalculated)
                            transfers.Add(addRow);
                    }
                    break;
                case "B":
                    depTransfers = searchOnlyTransfer(UserData, filter.RoomsInfo.Sum(s => s.Adult + s.Child), filter.Direction, filter.TrfFrom, filter.TrfTo, filter.BegDate, filter.TrfType, ref errorMsg);
                    foreach (OnlyTransfer_Transfers row in depTransfers)
                    {
                        OnlyTransfer_Transfers addRow = new OnlyTransfer_Transfers();
                        addRow = calcSalePrice(UserData, row, filter);
                        row.SaleCur = !filter.CurControl && string.IsNullOrEmpty(filter.CurrentCur) ? UserData.SaleCur : row.DepSaleCur;
                        addRow.depTransferRecord = new Transfers().getTransfer(UserData.Market, row.DepTransfer, ref errorMsg);
                        if (addRow.DepPriceCalculated)
                            transfers.Add(addRow);
                    }
                    break;
                case "R":
                    bool? onlyRT = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "OnlyTransferSearchRTisOnlyRT"));
                    depTransfers = searchOnlyTransfer(UserData, filter.RoomsInfo.Sum(s => s.Adult + s.Child), "R", filter.TrfFrom, filter.TrfTo, filter.BegDate, filter.TrfType, ref errorMsg);
                    foreach (OnlyTransfer_Transfers row in depTransfers)
                    {
                        OnlyTransfer_Transfers addRow = new OnlyTransfer_Transfers();
                        row.SaleCur = !filter.CurControl && string.IsNullOrEmpty(filter.CurrentCur) ? UserData.SaleCur : row.DepSaleCur;
                        addRow = calcSalePrice(UserData, row, filter);
                        addRow.depTransferRecord = new Transfers().getTransfer(UserData.Market, row.DepTransfer, ref errorMsg);
                        if (addRow.DepPriceCalculated)
                        {
                            addRow.RetDate = filter.EndDate;
                            if (filter.EndDate.HasValue && filter.BegDate.HasValue)
                                addRow.Days = Conversion.getInt16OrNull((filter.EndDate.Value - filter.BegDate.Value).Days.ToString());
                            transfers.Add(addRow);
                        }
                    }
                    if (!onlyTransferSearchRTisOnlyRT.HasValue || (onlyTransferSearchRTisOnlyRT.Value == false))
                    {
                        if (!onlyRT.HasValue || (onlyRT.HasValue && onlyRT.Value == true))
                        {
                            depTransfers = searchOnlyTransfer(UserData, filter.RoomsInfo.Sum(s => s.Adult + s.Child), "F", filter.TrfFrom, filter.TrfTo, filter.BegDate, filter.TrfType, ref errorMsg);
                            retTransfers = searchOnlyTransfer(UserData, filter.RoomsInfo.Sum(s => s.Adult + s.Child), "B", filter.TrfTo, filter.TrfFrom, filter.EndDate, filter.TrfType, ref errorMsg);
                            if (retTransfers.Count > 0)
                            {
                                foreach (OnlyTransfer_Transfers row in depTransfers)
                                {
                                    List<OnlyTransfer_Transfers> tmpTrans = retTransfers
                                                                    .Where(w =>
                                                                        w.DepTrfFrom == row.DepTrfTo &&
                                                                        w.DepDate == (row.DepDate.HasValue && row.Days.HasValue ? row.DepDate.Value.AddDays(row.Days.Value) : w.DepDate)
                                                                          )
                                                                    .ToList<OnlyTransfer_Transfers>();
                                    if (tmpTrans.Count > 0)
                                    {
                                        foreach (OnlyTransfer_Transfers r in tmpTrans)
                                        {
                                            OnlyTransfer_Transfers addRow = new OnlyTransfer_Transfers();
                                            addRow.IsRT = true;
                                            addRow.Days = row.Days;
                                            addRow.SaleCur = !filter.CurControl && string.IsNullOrEmpty(filter.CurrentCur) ? UserData.SaleCur : row.DepSaleCur;
                                            addRow.DepDate = row.DepDate;
                                            addRow.DepTransfer = row.DepTransfer;
                                            addRow.DepTrfType = row.DepTrfType;
                                            addRow.DepTrfTypeName = row.DepTrfTypeName;
                                            addRow.DepTrfTypeNameL = row.DepTrfTypeNameL;
                                            addRow.depTransferRecord = new Transfers().getTransfer(UserData.Market, row.DepTransfer, ref errorMsg);
                                            addRow.DepTrfFrom = row.DepTrfFrom;
                                            addRow.DepTrfFromName = row.DepTrfFromName;
                                            addRow.DepTrfFromNameL = row.DepTrfFromNameL;
                                            addRow.DepTrfTo = row.DepTrfTo;
                                            addRow.DepTrfToName = row.DepTrfToName;
                                            addRow.DepTrfToNameL = row.DepTrfToNameL;
                                            addRow.DepDirection = row.DepDirection;
                                            addRow.DepSaleCur = row.DepSaleCur;
                                            addRow.DepFreeMaxAge = row.DepFreeMaxAge;
                                            addRow.DepChdG1MaxAge = row.DepChdG1MaxAge;
                                            addRow.DepChdG2MaxAge = row.DepChdG2MaxAge;
                                            addRow.DepPriceType = row.DepPriceType;
                                            addRow.DepSaleAdlPrice = row.DepSaleAdlPrice;
                                            addRow.DepSaleChdG1Price = row.DepSaleChdG1Price;
                                            addRow.DepSaleChdG2Price = row.DepSaleChdG2Price;
                                            addRow.DepVehicleCatID = row.DepVehicleCatID;
                                            addRow.DepVehicleCatName = row.DepVehicleCatName;

                                            addRow.RetDate = r.DepDate;
                                            addRow.RetTransfer = r.DepTransfer;
                                            addRow.RetTrfType = r.DepTrfType;
                                            addRow.RetTrfTypeName = r.DepTrfTypeName;
                                            addRow.RetTrfTypeNameL = r.DepTrfTypeNameL;
                                            addRow.retTransferRecord = new Transfers().getTransfer(UserData.Market, r.DepTransfer, ref errorMsg);
                                            addRow.RetTrfFrom = r.DepTrfFrom;
                                            addRow.RetTrfFromName = r.DepTrfFromName;
                                            addRow.RetTrfFromNameL = r.DepTrfFromNameL;
                                            addRow.RetTrfTo = r.DepTrfTo;
                                            addRow.RetTrfToName = r.DepTrfToName;
                                            addRow.RetTrfToNameL = r.DepTrfToNameL;
                                            addRow.RetDirection = r.DepDirection;
                                            addRow.RetSaleCur = r.DepSaleCur;
                                            addRow.RetFreeMaxAge = r.DepFreeMaxAge;
                                            addRow.RetChdG1MaxAge = r.DepChdG1MaxAge;
                                            addRow.RetChdG2MaxAge = r.DepChdG2MaxAge;
                                            addRow.RetPriceType = r.DepPriceType;
                                            addRow.RetSaleAdlPrice = r.DepSaleAdlPrice;
                                            addRow.RetSaleChdG1Price = r.DepSaleChdG1Price;
                                            addRow.RetSaleChdG2Price = r.DepSaleChdG2Price;
                                            addRow.RetVehicleCatID = r.RetVehicleCatID;
                                            addRow.RetVehicleCatName = r.RetVehicleCatName;

                                            addRow = calcSalePrice(UserData, addRow, filter);
                                            if (addRow.DepPriceCalculated && addRow.RetPriceCalculated)
                                                transfers.Add(addRow);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
            return transfers;
        }

        public decimal CalcPrice2(Decimal NetPrice, Profmars ProfMars, int CalcItem, Int16? SaleCalcType)
        {
            if ((NetPrice == 0) ||
               (
                 (ProfMars.GenPer == 0) && (ProfMars.GenVal == 0) &&
                 (ProfMars.AdlPer == 0) && (ProfMars.AdlVal == 0) &&
                 (ProfMars.ChdG1Per == 0) && (ProfMars.ChdG1Val == 0) &&
                 (ProfMars.ChdG2Per == 0) && (ProfMars.ChdG2Val == 0) &&
                 (ProfMars.ChdG3Per == 0) && (ProfMars.ChdG3Val == 0) &&
                 (ProfMars.ChdG4Per == 0) && (ProfMars.ChdG4Val == 0) &&
                 (ProfMars.AgencyCom == 0)
               ))
                return NetPrice;

            decimal ComPer = 0;
            decimal ComVal = 0;

            if (CalcItem == 1)
            {
                if (ProfMars.AdlPer != 0)
                    ComPer = Convert.ToDecimal(ProfMars.AdlPer);
                else
                    ComVal = Convert.ToDecimal(ProfMars.AdlVal);
            }
            else if (CalcItem == 2)
            {
                if (ProfMars.ChdG1Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG1Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG1Val);
            }
            else if (CalcItem == 3)
            {
                if (ProfMars.ChdG2Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG2Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG2Val);
            }
            else if (CalcItem == 4)
            {
                if (ProfMars.ChdG3Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG3Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG3Val);
            }
            else if (CalcItem == 5)
            {
                if (ProfMars.ChdG4Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG4Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG4Val);
            }


            if (ComPer == 0 && ComVal == 0)
            {
                ComPer = Convert.ToDecimal(ProfMars.GenPer);
                ComVal = Convert.ToDecimal(ProfMars.GenVal);
            }

            if (ComPer != 0)
            {
                if (SaleCalcType.HasValue && SaleCalcType.Value == 1)
                    return NetPrice / (1 - (ComPer / 100)); //Divide method
                return NetPrice + (NetPrice * ComPer / 100); //Multiple method
            }
            else
                return NetPrice + ComVal;
        }

        public OnlyTransfer_Transfers calcSalePrice(User UserData, OnlyTransfer_Transfers rec, OnlyTransferFilter criteria)
        {
            string errorMsg = string.Empty;
            decimal? depTotalPrice = (decimal)0;
            decimal? retTotalPrice = (decimal)0;
            Int16 Adl = criteria.RoomsInfo.FirstOrDefault().Adult;
            Int16 Chd = criteria.RoomsInfo.FirstOrDefault().Child;
            decimal? chd1Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd1Age);
            decimal? chd2Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd2Age);
            decimal? chd3Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd3Age);
            decimal? chd4Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd4Age);

            if (string.Equals(rec.DepPriceType, "P"))
            {
                depTotalPrice = 0;
                int totalChd = 0;
                List<int> chdAgeList = new List<int>();
                int grp1 = 0;
                int grp2 = 0;
                int grp3 = 0;
                if (criteria.RoomsInfo.FirstOrDefault().Child != 0)
                {
                    if (criteria.RoomsInfo.FirstOrDefault().Chd1Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value >= 0) chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value);
                    if (criteria.RoomsInfo.FirstOrDefault().Chd2Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value >= 0) chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value);
                    if (criteria.RoomsInfo.FirstOrDefault().Chd3Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value >= 0) chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value);
                    if (criteria.RoomsInfo.FirstOrDefault().Chd4Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value >= 0) chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value);

                    if (rec.DepFreeMaxAge.HasValue)
                    {
                        grp1 += chdAgeList.Where(w => w <= rec.DepFreeMaxAge).Count();
                        totalChd += grp1;
                    }
                    if (rec.DepFreeMaxAge.HasValue && rec.DepChdG1MaxAge.HasValue)
                    {
                        grp2 += chdAgeList.Where(w => w > rec.DepFreeMaxAge && w <= rec.DepChdG1MaxAge.Value).Count();
                        depTotalPrice += ((rec.DepSaleChdG1Price.HasValue ? rec.DepSaleChdG1Price.Value : (decimal)0)) * grp2;
                        totalChd += grp2;
                    }
                    if (rec.DepChdG1MaxAge.HasValue && rec.DepChdG2MaxAge.HasValue)
                    {
                        grp3 += chdAgeList.Where(w => w > rec.DepChdG1MaxAge && w <= rec.DepChdG2MaxAge.Value).Count();
                        depTotalPrice += ((rec.DepSaleChdG2Price.HasValue ? rec.DepSaleChdG2Price.Value : (decimal)0)) * grp3;
                        totalChd += grp3;
                    }
                }
                depTotalPrice += rec.DepSaleAdlPrice * Adl;
                if (totalChd < Chd)
                {
                    depTotalPrice += rec.DepSaleAdlPrice * (Chd - totalChd);
                }
                else if (totalChd > Chd)
                {
                    depTotalPrice += rec.DepSaleAdlPrice * (Chd - totalChd);
                }
            }
            else
            {
                depTotalPrice = rec.DepSaleAdlPrice;
            }
            rec.DepPriceCalculated = true;
            errorMsg = string.Empty;
            if (criteria.CurControl)
                if (!string.Equals(criteria.CurrentCur, rec.SaleCur))
                {
                    decimal? price = new TvBo.Common().Exchange(UserData.Market, DateTime.Today, rec.DepSaleCur, criteria.CurrentCur, depTotalPrice, true, ref errorMsg);
                    if (string.IsNullOrEmpty(errorMsg) && price.HasValue)
                    {
                        depTotalPrice = price;
                        rec.SaleCur = criteria.CurrentCur;
                        rec.DepSaleCur = criteria.CurrentCur;
                    }
                }

            rec.DepTotalPrice = depTotalPrice;

            if (!string.IsNullOrEmpty(rec.RetTransfer) && rec.RetTrfFrom.HasValue && rec.RetTrfTo.HasValue && rec.IsRT.HasValue && rec.IsRT.Value)
            {
                if (string.Equals(rec.RetPriceType, "P"))
                {
                    retTotalPrice = rec.RetSaleAdlPrice;
                    retTotalPrice = rec.RetSaleAdlPrice * Adl;
                    if (criteria.RoomsInfo.FirstOrDefault().Child != 0)
                    {
                        if (criteria.RoomsInfo.FirstOrDefault().Chd1Age.HasValue)
                        {
                            if (rec.RetChdG1MaxAge.HasValue && (rec.RetChdG1MaxAge.Value <= criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value))
                                retTotalPrice += (rec.RetSaleChdG1Price.HasValue ? rec.RetSaleChdG1Price.Value : (decimal)0);
                            else
                                if (rec.RetChdG2MaxAge.HasValue && (rec.RetChdG2MaxAge.Value <= criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value))
                                retTotalPrice += (rec.RetSaleChdG2Price.HasValue ? rec.RetSaleChdG2Price.Value : (decimal)0);
                            else
                                retTotalPrice += rec.RetSaleAdlPrice;
                        }
                        if (criteria.RoomsInfo.FirstOrDefault().Chd2Age.HasValue)
                        {
                            if (rec.RetChdG1MaxAge.HasValue && (rec.RetChdG1MaxAge.Value <= criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value))
                                retTotalPrice += (rec.RetSaleChdG1Price.HasValue ? rec.RetSaleChdG1Price.Value : (decimal)0);
                            else
                                if (rec.RetChdG2MaxAge.HasValue && (rec.RetChdG2MaxAge.Value <= criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value))
                                retTotalPrice += (rec.RetSaleChdG2Price.HasValue ? rec.RetSaleChdG2Price.Value : (decimal)0);
                            else
                                retTotalPrice += rec.RetSaleAdlPrice;
                        }
                        if (criteria.RoomsInfo.FirstOrDefault().Chd3Age.HasValue)
                        {
                            if (rec.RetChdG1MaxAge.HasValue && (rec.RetChdG1MaxAge.Value <= criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value))
                                retTotalPrice += (rec.RetSaleChdG1Price.HasValue ? rec.RetSaleChdG1Price.Value : (decimal)0);
                            else
                                if (rec.RetChdG2MaxAge.HasValue && (rec.RetChdG2MaxAge.Value <= criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value))
                                retTotalPrice += (rec.RetSaleChdG2Price.HasValue ? rec.RetSaleChdG2Price.Value : (decimal)0);
                            else
                                retTotalPrice += rec.RetSaleAdlPrice;
                        }
                        if (criteria.RoomsInfo.FirstOrDefault().Chd4Age.HasValue)
                        {
                            if (rec.RetChdG1MaxAge.HasValue && (rec.RetChdG1MaxAge.Value <= criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value))
                                retTotalPrice += (rec.RetSaleChdG1Price.HasValue ? rec.RetSaleChdG1Price.Value : (decimal)0);
                            else
                                if (rec.RetChdG2MaxAge.HasValue && (rec.RetChdG2MaxAge.Value <= criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value))
                                retTotalPrice += (rec.RetSaleChdG2Price.HasValue ? rec.RetSaleChdG2Price.Value : (decimal)0);
                            else
                                retTotalPrice += rec.RetSaleAdlPrice;
                        }
                    }
                }
                else
                {
                    retTotalPrice = rec.RetSaleAdlPrice;
                }
                rec.RetPriceCalculated = true;
                errorMsg = string.Empty;
                if (criteria.CurControl)
                {
                    if (!string.Equals(criteria.CurrentCur, rec.SaleCur))
                    {
                        decimal? price = new TvBo.Common().Exchange(UserData.Market, DateTime.Today, rec.RetSaleCur, criteria.CurrentCur, retTotalPrice, true, ref errorMsg);
                        if (string.IsNullOrEmpty(errorMsg) && price.HasValue)
                        {
                            retTotalPrice = price;
                            rec.SaleCur = criteria.CurrentCur;
                            rec.RetSaleCur = criteria.CurrentCur;
                        }
                    }
                }

                rec.RetTotalPrice = retTotalPrice;
            }

            rec.TotalPrice = (rec.DepTotalPrice.HasValue ? rec.DepTotalPrice.Value : (decimal)0) + (rec.RetTotalPrice.HasValue ? rec.RetTotalPrice.Value : (decimal)0);
            return rec;
        }

        public AirportRecord getTransferAirport(User UserData, string Code, ref string errorMsg)
        {
            AirportRecord record = new AirportRecord();

            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 36, VersionControl.Equality.gt))
                tsql =
@"
Select A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, @Market), A.Name),
	A.NameS, A.Location, LocationName=L.Name, LocationNameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
    A.PNLFullName, A.PNLFlightSpace, A.PNLNameSpace, A.PNLChdRemark, 
	A.PNLSitatex, A.PNLMaxLineInPart, A.PNLDispExtra, A.PNLDispPNR, A.PNLDotAfterTitle,
    A.TrfLocation,
    TrfLocationName=L1.Name,TrfLocationNameL=isnull(dbo.FindLocalName(L1.NameLID,@Market),L1.Name)
From Airport A (NOLOCK)
Join Location L (NOLOCK) ON A.Location=L.RecID
Join Location L1 (NOLOCK) ON A.TrfLocation=L1.RecID
Where A.Code=@Code
";
            else
                tsql =
@"
Select A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, @Market), A.Name),
	A.NameS, A.Location, LocationName=L.Name, LocationNameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
    A.PNLFullName, A.PNLFlightSpace, A.PNLNameSpace, A.PNLChdRemark, 
	A.PNLSitatex, A.PNLMaxLineInPart, A.PNLDispExtra, A.PNLDispPNR, A.PNLDotAfterTitle,
    TrfLocation=null,TrfLocationName=null,TrfLocationNameL=null
From Airport A (NOLOCK)
Join Location L (NOLOCK) ON A.Location=L.RecID
Where A.Code=@Code
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Location = (Int32)R["Location"];
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        record.PNLFullName = Equals(R["PNLFullName"], "Y");
                        record.PNLFlightSpace = Equals(R["PNLFlightSpace"], "Y");
                        record.PNLNameSpace = Equals(R["PNLNameSpace"], "Y");
                        record.PNLChdRemark = Equals(R["PNLChdRemark"], "Y");
                        record.PNLSitatex = Equals(R["PNLSitatex"], "Y");
                        record.PNLMaxLineInPart = Conversion.getInt32OrNull(R["PNLMaxLineInPart"]);
                        record.PNLDispExtra = Equals(R["PNLDispExtra"], "Y");
                        record.PNLDotAfterTitle = Equals(R["PNLDotAfterTitle"], "Y");
                        record.TrfLocation = Conversion.getInt32OrNull(R["TrfLocation"]);
                        record.TrfLocationName = Conversion.getStrOrNull(R["TrfLocationName"]);
                        record.TrfLocationNameL = Conversion.getStrOrNull(R["TrfLocationNameL"]);

                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ResDataRecord getTransferReservation(User UserData, OnlyTransferFilter criteria, OnlyTransfer_Transfers bookTrns, ref string errorMsg)
        {
            bool CreateChildAge = true;
            if (UserData.WebService)
                CreateChildAge = true;
            else
            {
                object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
                CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
            }

            ResDataRecord ResData = new ResDataRecord();
            ResData.SelectBook = new OnlyTransfers().fillSelectBook(UserData, bookTrns, criteria,/* adult, child, infant, */ref errorMsg);
            ResData = new OnlyTransfers().bookTransfer(UserData, ResData, bookTrns, CreateChildAge, string.Empty, ref errorMsg);

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            {
                ResMainRecord resMain = ResData.ResMain;
                resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            }

            return ResData;
        }

        public List<SearchResult> fillSelectBook(User UserData, OnlyTransfer_Transfers book, OnlyTransferFilter criteria, /*int Adult, int Child, int Infant, */ref string errorMsg)
        {
            List<SearchResult> SelectBook = new List<SearchResult>();
            SearchResult row = new SearchResult();
            #region Age Group
            List<AgeGroup> ageGroups = new List<AgeGroup>();
            int roomCnt = 0;
            int refNo = 0;
            roomCnt++;
            refNo++;
            Int16 adult = Conversion.getInt16OrNull(criteria.RoomsInfo.FirstOrDefault().Adult).HasValue ? Conversion.getInt16OrNull(criteria.RoomsInfo.FirstOrDefault().Adult).Value : Convert.ToInt16(2);
            int teenAge = 0;
            int child = 0;
            int infant = 0;

            #region calc child and infant count
            Int16 Adl = criteria.RoomsInfo.FirstOrDefault().Adult;
            Int16 Chd = criteria.RoomsInfo.FirstOrDefault().Child;
            decimal? chd1Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd1Age);
            decimal? chd2Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd2Age);
            decimal? chd3Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd3Age);
            decimal? chd4Age = Conversion.getDecimalOrNull(criteria.RoomsInfo.FirstOrDefault().Chd4Age);

            int totalChd = 0;
            List<int> chdAgeList = new List<int>();
            if (criteria.RoomsInfo.FirstOrDefault().Child != 0)
            {
                if (criteria.RoomsInfo.FirstOrDefault().Chd1Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value >= 0) chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd1Age.Value);
                if (criteria.RoomsInfo.FirstOrDefault().Chd2Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value >= 0) chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd2Age.Value);
                if (criteria.RoomsInfo.FirstOrDefault().Chd3Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value >= 0) chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd3Age.Value);
                if (criteria.RoomsInfo.FirstOrDefault().Chd4Age.HasValue && criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value >= 0) chdAgeList.Add(criteria.RoomsInfo.FirstOrDefault().Chd4Age.Value);

                if (book.DepFreeMaxAge.HasValue)
                {
                    infant += chdAgeList.Where(w => w <= book.DepFreeMaxAge).Count();
                }
                if (book.DepFreeMaxAge.HasValue && book.DepChdG1MaxAge.HasValue)
                {
                    child += chdAgeList.Where(w => w > book.DepFreeMaxAge && w <= book.DepChdG1MaxAge.Value).Count();
                }
                if (book.DepChdG1MaxAge.HasValue && book.DepChdG2MaxAge.HasValue)
                {
                    teenAge += chdAgeList.Where(w => w > book.DepChdG1MaxAge && w <= book.DepChdG2MaxAge.Value).Count();
                }
                totalChd = infant + child + teenAge;
            }
            #endregion

            ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Adult, GroupNo = roomCnt, RefNo = refNo, Unit = (short)Adl, Age = null, DateOfBirth = null });

            #endregion
            row.CatPackID = 0;
            row.RefNo = 1;
            row.ARecNo = 0;
            row.PRecNo = 0;
            row.CheckIn = book.DepDate;
            if (book.IsRT.HasValue && book.IsRT.Value && !book.RetDate.HasValue)
                row.CheckOut = book.RetDate.HasValue ? book.RetDate.Value : (criteria.EndDate.HasValue ? criteria.EndDate.Value : row.CheckIn);
            else
                row.CheckOut = book.RetDate.HasValue && book.RetDate.HasValue ? book.RetDate.Value : book.DepDate;
            row.Night = Conversion.getInt16OrNull((row.CheckOut.Value - row.CheckIn.Value).Days - 1);
            row.NetCur = book.SaleCur;

            row.HAdult = (short)Adl;

            if (book.DepFreeMaxAge.HasValue)
            {
                row.ChdG1Age1 = 0;
                row.ChdG1Age2 = book.DepFreeMaxAge.Value;
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Infant, GroupNo = roomCnt, RefNo = refNo, Unit = (short)infant, Age = row.ChdG1Age1, DateOfBirth = null });
                row.HChdAgeG1 = (short)infant;
            }
            if (book.DepFreeMaxAge.HasValue && book.DepChdG1MaxAge.HasValue)
            {
                row.ChdG2Age1 = row.ChdG1Age2.Value;
                row.ChdG2Age2 = book.DepChdG1MaxAge.Value;
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = (short)child, Age = row.ChdG2Age2, DateOfBirth = null });
                row.HChdAgeG2 = (short)child;
            }
            if (book.DepChdG1MaxAge.HasValue && book.DepChdG2MaxAge.HasValue)
            {
                row.ChdG3Age1 = row.ChdG2Age2.Value;
                row.ChdG3Age2 = book.DepChdG2MaxAge.Value;
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.TeenAge, GroupNo = roomCnt, RefNo = refNo, Unit = (short)teenAge, Age = row.ChdG3Age2, DateOfBirth = null });
                row.HChdAgeG3 = (short)teenAge;
            }
            row.HChdAgeG4 = 0;

            row.SaleCur = book.SaleCur;
            row.HolPack = string.Empty;
            row.Operator = UserData.Operator;
            row.Market = UserData.Market;
            row.FlightClass = "";
            row.DepCity = book.DepTrfFrom;
            row.ArrCity = book.DepTrfTo;
            row.DepCityName = new Locations().getLocationName(UserData, row.DepCity.Value, ref errorMsg);
            row.ArrCityName = new Locations().getLocationName(UserData, row.ArrCity.Value, ref errorMsg);
            row.DepSeat = 0;
            row.RetSeat = 0;
            row.StopSaleGuar = 0;
            row.StopSaleStd = 0;
            row.CurrentCur = UserData.SaleCur;
            row.AgeGroupList = ageGroups;
            row.PLCur = book.DepSaleCur;
            SelectBook.Add(row);

            return SelectBook;
        }

        public ResDataRecord bookTransfer(User UserData, ResDataRecord _ResData, OnlyTransfer_Transfers bookRow, bool CreateChildAge, string ResNote, ref string errorMsg)
        {

            ResDataRecord ResData = new ResDataRecord();
            ResData = new ResTables().copyData(_ResData);

            int BookID = 1;

            ResData.Title = new TvBo.Common().getTitle(ref errorMsg);

            #region Create ResMain
            ResData = new Reservation().GenerateResMain(UserData, ResData, SearchType.OnlyTransfer, string.Empty, ref errorMsg);
            ResMainRecord resMain = ResData.ResMain;
            resMain.ResNote = ResNote;
            //resMain.SaleCur = bookRow.SaleCur;
            if (errorMsg != "")
                return null;
            #endregion

            #region Create ResCust
            ResData.ResCust = new List<ResCustRecord>();
            for (int i = 0; i < ResData.SelectBook.Count; i++)
            {
                //if (ResData.SelectBook[i].RefNo != null)
                BookID = ResData.SelectBook[i].RefNo;
                //else
                //    BookID = i + 1;
                ResData = new Reservation().GenerateTourists(UserData, ResData, BookID, ref errorMsg);
                if (errorMsg != "")
                    return null;
            }

            ResData.ResCust[0].Leader = "Y";
            #endregion Create ResCust

            ResData.ResService = new List<ResServiceRecord>();
            ResData.ResCon = new List<ResConRecord>();
            ResData.ResServiceExt = new List<ResServiceExtRecord>();
            ResData.ResConExt = new List<ResConExtRecord>();
            ResData.ResCustPrice = new List<ResCustPriceRecord>();
            ResData.ResCustInfo = new List<ResCustInfoRecord>();
            ResData.ResFlightDetail = new List<ResFlightDetailRecord>();
            ResData.ResSupDis = new List<ResSupDisRecord>();

            SearchResult selectBook = ResData.SelectBook.FirstOrDefault();

            List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
            foreach (SelectCustRecord s in SelectCust)
                s.Selected = true;

            if (bookRow.retTransferRecord != null && bookRow.RetPriceCalculated)
            {
                if (string.Equals(bookRow.DepDirection, "R"))
                {
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, selectBook.CheckIn.Value, selectBook.CheckOut.Value, "TRANSFER", bookRow.DepTransfer,
                                        string.Empty, string.Empty, string.Empty, string.Empty, 1, 0, 1, bookRow.DepTrfFrom, bookRow.DepTrfTo, bookRow.DepDirection, string.Empty, 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, bookRow.DepVehicleCatID, null);
                }
                else
                {
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, selectBook.CheckIn.Value, selectBook.CheckIn.Value, "TRANSFER", bookRow.DepTransfer,
                                        string.Empty, string.Empty, string.Empty, string.Empty, 1, 0, 1, bookRow.DepTrfFrom, bookRow.DepTrfTo, bookRow.DepDirection, string.Empty, 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, bookRow.DepVehicleCatID, null);

                    ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, selectBook.CheckOut.Value, selectBook.CheckOut.Value, "TRANSFER", bookRow.RetTransfer,
                                        string.Empty, string.Empty, string.Empty, string.Empty, 1, 0, 1, bookRow.RetTrfFrom, bookRow.RetTrfTo, bookRow.RetDirection, string.Empty, 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, bookRow.RetVehicleCatID, null);
                }

            }
            else
            {
                ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, selectBook.CheckIn.Value, selectBook.CheckOut.Value, "TRANSFER", bookRow.DepTransfer,
                                    string.Empty, string.Empty, string.Empty, string.Empty, 1, 0, 1, bookRow.DepTrfFrom, bookRow.DepTrfTo, bookRow.DepDirection, string.Empty, 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, null, null, null, null, bookRow.DepVehicleCatID, null);
            }
            List<ResServiceRecord> resServcies = ResData.ResService;
            ResServiceRecord service = resServcies.FirstOrDefault();
            if (!string.IsNullOrEmpty(errorMsg))
            {
                return ResData;
            }

            List<ResServiceRecord> resServiceList = ResData.ResService;
            foreach (ResServiceRecord row in resServiceList)
                row.ExcludeService = false;
            List<ResServiceExtRecord> resServiceExtList = ResData.ResServiceExt;
            foreach (ResServiceExtRecord row in resServiceExtList)
                row.ExcludeService = false;

            #region Calculate ResCustPrice

            string ResNo = ResData.ResMain.ResNo;
            ResNo = ResNo.Length > 10 ? ResNo.Substring(0, 10) : ResNo;

            StringBuilder CalcStrSql = new Reservation().BuildCalcSqlString(ResData);

            CalcStrSql.Append("Declare  @PassEB bit \n");
            CalcStrSql.Append("Declare  @ErrCode SmallInt \n");
            CalcStrSql.Append("Declare @Supplier varchar(10) \n");
            CalcStrSql.Append("exec dbo.usp_Calc_Res_Price '" + ResNo + "', Default, Default, @PassEB OutPut, @ErrCode OutPut, 1 \n");
            CalcStrSql.Append("Select * From #ResMain \n");
            CalcStrSql.Append("Select * From #ResService \n");
            CalcStrSql.Append("Select * From #ResServiceExt \n");
            CalcStrSql.Append("Select * From #ResCust \n");
            CalcStrSql.Append("Select * From #ResCustPrice \n");
            CalcStrSql.Append("Select * From #ResSupDis \n");
            CalcStrSql.Append("Select ErrorCode=@ErrCode \n");
            CalcStrSql.Append("Select * From #CalcErrTbl \n");

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(CalcStrSql.ToString());
            DataSet ds;
            try
            {
                ds = db.ExecuteDataSet(dbCommand);
                if (ds != null)
                {
                    ds.Tables[0].TableName = "ResMain";
                    ds.Tables[1].TableName = "ResService";
                    ds.Tables[2].TableName = "ResServiceExt";
                    ds.Tables[3].TableName = "ResCust";
                    ds.Tables[4].TableName = "ResCustPrice";
                    ds.Tables[5].TableName = "ResSupDis";
                    ds.Tables[6].TableName = "ErrorTable";
                    ds.Tables[ds.Tables.Count - 1].TableName = "CalcErrTbl";
                }
                else
                {
                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationPriceNotCalculated").ToString();
                    ds = null;
                    return ResData;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                ds = null;
                return ResData;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
            #endregion Calculate ResCustPrice

            int ErrorCode = ds.Tables["ErrorTable"] != null ? Convert.ToInt32(ds.Tables["ErrorTable"].Rows[0]["ErrorCode"].ToString()) : 0;
            if (ErrorCode != 0)
            {
                errorMsg = new ResCalcError().calcError(ErrorCode, ResData);
                return ResData;
            }
            else
                if (ds.Tables["CalcErrTbl"] != null && ds.Tables["CalcErrTbl"].Rows.Count > 0)
            {
                ErrorCode = ds.Tables["CalcErrTbl"] != null ? Convert.ToInt32(ds.Tables["CalcErrTbl"].Rows[0]["ErrCode"].ToString()) : 0;
                if (ErrorCode != 0)
                {
                    errorMsg = new ResCalcError().calcError(ErrorCode, ResData);
                    return ResData;
                }
            }

            ResData = new Reservation().SetCalcDataToResData(UserData, ResData, ds, false, ref errorMsg);

            if (!CreateChildAge)
            {
                foreach (ResCustRecord row in ResData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
            }

            return ResData;
        }
    }

    public class OnlyTransfersV3
    {
        public List<CodeName> getCountryTransfers(User UserData, int? country, int? departure, int? arrival, DateTime? date, int? pax, bool useLocalName, ref string errorMsg)
        {
            List<CodeName> records = new List<CodeName>();
            string tsql = string.Empty;
            tsql +=
@"
SET DATEFIRST 1
Select Distinct TP.Transfer,T.Name,NameL=isnull(dbo.FindLocalName(T.NameLID,@Market),T.Name),Code=TP.Transfer+';'+T.Direction
From TransferPrice TP (NOLOCK) 
Join Transfer T (NOLOCK) ON T.Code=TP.Transfer 
Where TP.Market=@Market
  And B2BPub=1  
  And (@Date between TP.BegDate And TP.EndDate)
  And
   (
	 not Exists(Select * From TransferDates (NOLOCK) TD Where TD.Transfer=T.Code and Market=TP.Market)
	 or
	 Exists(Select RecID From TransferDates (NOLOCK) TD
			Where TD.[Transfer]=T.Code and TD.Market=TP.Market
			and (TD.TrfFrom Is Null or isnull(TD.TrfFrom,0)=TP.TrfFrom)
			and (TD.TrfTo Is Null or isnull(TD.TrfTo,0)=TP.TrfTo)
			and SubString(IsNull([DayofWeek],'1111111'),DATEPART(dw,@Date),1)=1)
   )
  And ((isnull(TP.Agency,'')='' Or TP.Agency=@AgencyId) And (isnull(TP.AgencyGrp,'')='' Or Exists(Select RecID From GrpAgencyDet Where Agency=@AgencyId And Groups=TP.AgencyGrp))) 
";
            if (departure.HasValue)
                tsql +=
@" 
  And TP.TrfFrom=@departure 
";
            if (arrival.HasValue)
                tsql +=
@" 
  And TP.TrfTo=@arrival
";
            if (pax.HasValue)
                tsql +=
@" 
  And @GrpPax between isnull(TP.FromPax,0) and isnull(TP.ToPax,999)
";
            if (country.HasValue)
                tsql +=
@" 
  And Country=@country
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "AgencyID", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Date", DbType.DateTime, date);
                if (departure.HasValue)
                    db.AddInParameter(dbCommand, "departure", DbType.Int32, departure.Value);
                if (arrival.HasValue)
                    db.AddInParameter(dbCommand, "arrival", DbType.Int32, arrival.Value);
                if (pax.HasValue)
                    db.AddInParameter(dbCommand, "GrpPax", DbType.Int32, pax.Value);
                if (country.HasValue)
                    db.AddInParameter(dbCommand, "country", DbType.Int32, country.Value);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        records.Add(new CodeName
                        {
                            Code = reader["Code"].ToString(),
                            Name = useLocalName ? reader["NameL"].ToString() : reader["Name"].ToString()
                        });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
