﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class SupDisRecord
    {
        public SupDisRecord()
        { 
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        decimal? _PerVal;
        public decimal? PerVal
        {
            get { return _PerVal; }
            set { _PerVal = value; }
        }

        bool _CanChg;
        public bool CanChg
        {
            get { return _CanChg; }
            set { _CanChg = value; }
        }

        string _SD;
        public string SD
        {
            get { return _SD; }
            set { _SD = value; }
        }

        string _SDName;
        public string SDName
        {
            get { return _SDName; }
            set { _SDName = value; }
        }

        bool _IncCom;
        public bool IncCom
        {
            get { return _IncCom; }
            set { _IncCom = value; }
        }

        string _ApplyType;
        public string ApplyType
        {
            get { return _ApplyType; }
            set { _ApplyType = value; }
        }

        string _applyTypeName;
        public string ApplyTypeName
        {
            get { return _applyTypeName; }
            set { _applyTypeName = value; }
        }

        Int16 _Priority;
        public Int16 Priority
        {
            get { return _Priority; }
            set { _Priority = value; }
        }

        bool _Ok;
        public bool Ok
        {
            get { return _Ok; }
            set { _Ok = value; }
        }

        bool _IncComEB;
        public bool IncComEB
        {
            get { return _IncComEB; }
            set { _IncComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        bool _WebDisp;
        public bool WebDisp
        {
            get { return _WebDisp; }
            set { _WebDisp = value; }
        }

    }

}
