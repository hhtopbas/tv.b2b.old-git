﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TvBo
{
    public static class SanBICache
    {
        private static void Write2Cache(string cacheKey, object data, int? cacheMinute)
        {
            if (HttpContext.Current != null)
            {
                if (cacheMinute.HasValue)
                    HttpContext.Current.Cache.Insert(cacheKey, data, null, DateTime.Now.AddMinutes(cacheMinute.Value), System.Web.Caching.Cache.NoSlidingExpiration);
                else
                    HttpContext.Current.Cache.Insert(cacheKey, data);
            }
            else
            {
                if (cacheMinute.HasValue)
                    HttpRuntime.Cache.Add(cacheKey, data, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, (int)cacheMinute.Value, 0), System.Web.Caching.CacheItemPriority.Default, null);
                else
                    HttpRuntime.Cache.Add(cacheKey, data, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 30, 0), System.Web.Caching.CacheItemPriority.Default, null);
            }
        }
        private static object GetFromCacheByCode(string cacheKey)
        {
            if (HttpContext.Current != null)
                return HttpContext.Current.Cache.Get(cacheKey);
            else
                return HttpRuntime.Cache[cacheKey];
        }

        public static Location GetLocation(User UserData, string pMarket, int pRecId)
        {
            string cacheKey = string.Format("SanBI.AllLocation");
            object obj = GetFromCacheByCode(cacheKey);
            if (obj == null)
            {
                string errorMsg = string.Empty;
                obj = new TvBo.Locations().getLocationList(UserData.Market, LocationType.None, null, null, null, null, ref errorMsg);
                Write2Cache(cacheKey, obj, 60);
            }
            return ((IEnumerable<Location>)obj).FirstOrDefault(w => w.RecID == pRecId);
        }
        public static RRFHolPack GetHolPack(User UserData, string pMarket, string pCode)
        {
            string cacheKey = string.Format("SanBI.AllHolPack");
            object obj = GetFromCacheByCode(cacheKey);
            if (obj == null)
            {
                string errorMsg = string.Empty;
                obj = new TvBo.HolPacks().GetAllHolPackInfo(UserData, ref errorMsg);
                Write2Cache(cacheKey, obj, 60);
            }
            return ((IEnumerable<RRFHolPack>)obj).FirstOrDefault(w => w.Code == pCode);
        }
        public static HotelRecord GetHotel(User UserData, string pMarket, string pCode)
        {
            string cacheKey = string.Format("SanBI.AllHotel");
            object obj = GetFromCacheByCode(cacheKey);
            if (obj == null)
            {
                string errorMsg = string.Empty;
                obj = new TvBo.Hotels().GetAllHotelInfoForCache(UserData, ref errorMsg);
                Write2Cache(cacheKey, obj, 60);
            }
            if (pCode.Contains("|"))
                return ((List<HotelRecord>)obj).FirstOrDefault(w => w.Code == pCode.Split('|')[0]);
            else
                return ((List<HotelRecord>)obj).FirstOrDefault(w => w.Code == pCode);
        }
        public static ExcursionRecord GetExcursion(User UserData, string pCode)
        {
            string cacheKey = string.Format("SanBI.AllExcursions");
            object obj = GetFromCacheByCode(cacheKey);
            if (obj == null)
            {
                string errorMsg = string.Empty;
                obj = new TvBo.Excursions().getAllExcursionInfo(UserData, ref errorMsg);
                Write2Cache(cacheKey, obj, 60);
            }
            return ((List<ExcursionRecord>)obj).FirstOrDefault(w => w.Code == pCode);
        }
    }
}
