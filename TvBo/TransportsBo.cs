﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Web;

namespace TvBo
{
    [Serializable()]
    public class TransportRecord
    {
        public TransportRecord()
        {
        }

        int? _RecID; public int? RecID { get { return _RecID; } set { _RecID = value; } }
        byte? _TpType; public byte? TpType { get { return _TpType; } set { _TpType = value; } }
        string _Code; public string Code { get { return _Code; } set { _Code = value; } }
        string _Name; public string Name { get { return _Name; } set { _Name = value; } }
        string _NameL; public string NameL { get { return _NameL; } set { _NameL = value; } }
        string _NameS; public string NameS { get { return _NameS; } set { _NameS = value; } }
        string _Operator; public string Operator { get { return _Operator; } set { _Operator = value; } }
        int? _Departure; public int? Departure { get { return _Departure; } set { _Departure = value; } }
        string _DepartureName; public string DepartureName { get { return _DepartureName; } set { _DepartureName = value; } }
        int? _Arrival; public int? Arrival { get { return _Arrival; } set { _Arrival = value; } }
        string _ArrivalName; public string ArrivalName { get { return _ArrivalName; } set { _ArrivalName = value; } }
        DateTime? _DepTime; public DateTime? DepTime { get { return _DepTime; } set { _DepTime = value; } }
        DateTime? _ArrTime; public DateTime? ArrTime { get { return _ArrTime; } set { _ArrTime = value; } }
        int? _Distance; public int? Distance { get { return _Distance; } set { _Distance = value; } }
        int? _Duration; public int? Duration { get { return _Duration; } set { _Duration = value; } }
        string _Explanation; public string Explanation { get { return _Explanation; } set { _Explanation = value; } }
        Int16? _RoundBuyPrice; public Int16? RoundBuyPrice { get { return _RoundBuyPrice; } set { _RoundBuyPrice = value; } }
        Int16? _RoundSalePrice; public Int16? RoundSalePrice { get { return _RoundSalePrice; } set { _RoundSalePrice = value; } }
        Int16? _RoundPayment; public Int16? RoundPayment { get { return _RoundPayment; } set { _RoundPayment = value; } }
        string _PayCom; public string PayCom { get { return _PayCom; } set { _PayCom = value; } }
        string _PayComEB; public string PayComEB { get { return _PayComEB; } set { _PayComEB = value; } }
        Int16? _ConfStat; public Int16? ConfStat { get { return _ConfStat; } set { _ConfStat = value; } }
        string _DepDays; public string DepDays { get { return _DepDays; } set { _DepDays = value; } }
        decimal? _TaxPer; public decimal? TaxPer { get { return _TaxPer; } set { _TaxPer = value; } }
        string _AccountNo1; public string AccountNo1 { get { return _AccountNo1; } set { _AccountNo1 = value; } }
        string _AccountNo2; public string AccountNo2 { get { return _AccountNo2; } set { _AccountNo2 = value; } }
        string _AccountNo3; public string AccountNo3 { get { return _AccountNo3; } set { _AccountNo3 = value; } }
        string _NextRoute; public string NextRoute { get { return _NextRoute; } set { _NextRoute = value; } }
        Int16? _DepRet; public Int16? DepRet { get { return _DepRet; } set { _DepRet = value; } }
        bool? _PayPasEB; public bool? PayPasEB { get { return _PayPasEB; } set { _PayPasEB = value; } }
        bool? _CanDiscount; public bool? CanDiscount { get { return _CanDiscount; } set { _CanDiscount = value; } }
        Int16? _SerArea; public Int16? SerArea { get { return _SerArea; } set { _SerArea = value; } }
    }

    [Serializable()]
    public class TransportDayRecord
    {
        public TransportDayRecord()
        {
        }

        string _Transport;
        public string Transport
        {
            get { return _Transport; }
            set { _Transport = value; }
        }

        int _RouteFrom;
        public int RouteFrom
        {
            get { return _RouteFrom; }
            set { _RouteFrom = value; }
        }

        string _RouteFromName;
        public string RouteFromName
        {
            get { return _RouteFromName; }
            set { _RouteFromName = value; }
        }

        string _RouteFromNameL;
        public string RouteFromNameL
        {
            get { return _RouteFromNameL; }
            set { _RouteFromNameL = value; }
        }

        int _RouteTo;
        public int RouteTo
        {
            get { return _RouteTo; }
            set { _RouteTo = value; }
        }

        string _RouteToName;
        public string RouteToName
        {
            get { return _RouteToName; }
            set { _RouteToName = value; }
        }

        string _RouteToNameL;
        public string RouteToNameL
        {
            get { return _RouteToNameL; }
            set { _RouteToNameL = value; }
        }

        DateTime _TransDate;
        public DateTime TransDate
        {
            get { return _TransDate; }
            set { _TransDate = value; }
        }

        string _Bus;
        public string Bus
        {
            get { return _Bus; }
            set { _Bus = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        Int16? _Seat;
        public Int16? Seat
        {
            get { return _Seat; }
            set { _Seat = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        string _NextRoute;
        public string NextRoute
        {
            get { return _NextRoute; }
            set { _NextRoute = value; }
        }

        bool _AllotUseSysSet;
        public bool AllotUseSysSet
        {
            get { return _AllotUseSysSet; }
            set { _AllotUseSysSet = value; }
        }

        bool _AllotChk;
        public bool AllotChk
        {
            get { return _AllotChk; }
            set { _AllotChk = value; }
        }

        bool _AllotWarnFull;
        public bool AllotWarnFull
        {
            get { return _AllotWarnFull; }
            set { _AllotWarnFull = value; }
        }

        bool _AllotDoNotOver;
        public bool AllotDoNotOver
        {
            get { return _AllotDoNotOver; }
            set { _AllotDoNotOver = value; }
        }

        decimal? _ChdG1MaxAge;
        public decimal? ChdG1MaxAge
        {
            get { return _ChdG1MaxAge; }
            set { _ChdG1MaxAge = value; }
        }

        decimal? _ChdG2MaxAge;
        public decimal? ChdG2MaxAge
        {
            get { return _ChdG2MaxAge; }
            set { _ChdG2MaxAge = value; }
        }

        decimal? _ChdG3MaxAge;
        public decimal? ChdG3MaxAge
        {
            get { return _ChdG3MaxAge; }
            set { _ChdG3MaxAge = value; }
        }
    }

    [Serializable()]
    public class BusRecord
    {
        public BusRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Plate;
        public string Plate
        {
            get { return _Plate; }
            set { _Plate = value; }
        }

        Int16? _Seat;
        public Int16? Seat
        {
            get { return _Seat; }
            set { _Seat = value; }
        }

        string _Producer;
        public string Producer
        {
            get { return _Producer; }
            set { _Producer = value; }
        }

        string _Model;
        public string Model
        {
            get { return _Model; }
            set { _Model = value; }
        }

        string _ModelYear;
        public string ModelYear
        {
            get { return _ModelYear; }
            set { _ModelYear = value; }
        }

        string _Explanation;
        public string Explanation
        {
            get { return _Explanation; }
            set { _Explanation = value; }
        }

    }

    [Serializable()]
    public class BusSeatPlanRecord
    {
        public BusSeatPlanRecord()
        { 
            _isExit=false;
            _ChdRestriction=false;
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Bus;
        public string Bus
        {
            get { return _Bus; }
            set { _Bus = value; }
        }

        Int16? _CellNo;
        public Int16? CellNo
        {
            get { return _CellNo; }
            set { _CellNo = value; }
        }

        Int16? _CellType;
        public Int16? CellType
        {
            get { return _CellType; }
            set { _CellType = value; }
        }

        Int16? _SeatNo;
        public Int16? SeatNo
        {
            get { return _SeatNo; }
            set { _SeatNo = value; }
        }

        string _SeatLetter;
        public string SeatLetter
        {
            get { return _SeatLetter; }
            set { _SeatLetter = value; }
        }

        bool? _isExit;
        public bool? IsExit
        {
            get { return _isExit; }
            set { _isExit = value; }
        }

        bool? _ChdRestriction;
        public bool? ChdRestriction
        {
            get { return _ChdRestriction; }
            set { _ChdRestriction = value; }
        }

        Int16? _RowNo;
        public Int16? RowNo
        {
            get { return _RowNo; }
            set { _RowNo = value; }
        }

        Int16? _ColNo;
        public Int16? ColNo
        {
            get { return _ColNo; }
            set { _ColNo = value; }
        }

        bool? _forB2B;
        public bool? ForB2B
        {
            get { return _forB2B; }
            set { _forB2B = value; }
        }

        bool? _forB2C;
        public bool? ForB2C
        {
            get { return _forB2C; }
            set { _forB2C = value; }
        }

        bool? _forTV;
        public bool? ForTV
        {
            get { return _forTV; }
            set { _forTV = value; }
        }
    }

    [Serializable()]
    public class TransportPriceRecord
    {
        public TransportPriceRecord()
        {
        }

        int _TransportDayID;
        public int TransportDayID
        {
            get { return _TransportDayID; }
            set { _TransportDayID = value; }
        }

        string _Transport;
        public string Transport
        {
            get { return _Transport; }
            set { _Transport = value; }
        }

        int _RouteFrom;
        public int RouteFrom
        {
            get { return _RouteFrom; }
            set { _RouteFrom = value; }
        }

        int _RouteTo;
        public int RouteTo
        {
            get { return _RouteTo; }
            set { _RouteTo = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _NetAdl;
        public decimal? NetAdl
        {
            get { return _NetAdl; }
            set { _NetAdl = value; }
        }

        decimal? _NetChdG1Price;
        public decimal? NetChdG1Price
        {
            get { return _NetChdG1Price; }
            set { _NetChdG1Price = value; }
        }

        decimal? _NetChdG2Price;
        public decimal? NetChdG2Price
        {
            get { return _NetChdG2Price; }
            set { _NetChdG2Price = value; }
        }

        decimal? _NetChdG3Price;
        public decimal? NetChdG3Price
        {
            get { return _NetChdG3Price; }
            set { _NetChdG3Price = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _SaleAdl;
        public decimal? SaleAdl
        {
            get { return _SaleAdl; }
            set { _SaleAdl = value; }
        }

        decimal? _SaleChdG1Price;
        public decimal? SaleChdG1Price
        {
            get { return _SaleChdG1Price; }
            set { _SaleChdG1Price = value; }
        }

        decimal? _SaleChdG2Price;
        public decimal? SaleChdG2Price
        {
            get { return _SaleChdG2Price; }
            set { _SaleChdG2Price = value; }
        }

        decimal? _SaleChdG3Price;
        public decimal? SaleChdG3Price
        {
            get { return _SaleChdG3Price; }
            set { _SaleChdG3Price = value; }
        }

        string _RT;
        public string RT
        {
            get { return _RT; }
            set { _RT = value; }
        }

    }

    [Serializable()]
    public class TransportRouteRecord
    {
        public TransportRouteRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Transport;
        public string Transport
        {
            get { return _Transport; }
            set { _Transport = value; }
        }

        Int16? _StepNo;
        public Int16? StepNo
        {
            get { return _StepNo; }
            set { _StepNo = value; }
        }

        int? _Location;
        public int? Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        Int16? _DisFromDep;
        public Int16? DisFromDep
        {
            get { return _DisFromDep; }
            set { _DisFromDep = value; }
        }

        Int16? _DisToArr;
        public Int16? DisToArr
        {
            get { return _DisToArr; }
            set { _DisToArr = value; }
        }

        int? _Duration;
        public int? Duration
        {
            get { return _Duration; }
            set { _Duration = value; }
        }

    }

    [Serializable()]
    public class TransportStatRecord
    {
        public TransportStatRecord()
        {
        }

        string _Transport;
        public string Transport
        {
            get { return _Transport; }
            set { _Transport = value; }
        }

        int _RouteID;
        public int RouteID
        {
            get { return _RouteID; }
            set { _RouteID = value; }
        }

        int _Location;
        public int Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _LocationLocalName;
        public string LocationLocalName
        {
            get { return _LocationLocalName; }
            set { _LocationLocalName = value; }
        }

        byte? _StepNo;
        public byte? StepNo
        {
            get { return _StepNo; }
            set { _StepNo = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

    }

    [Serializable()]
    public class TransportDetailRecord
    {
        public TransportDetailRecord()
        {
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        int? _RouteFrom;
        public int? RouteFrom
        {
            get { return _RouteFrom; }
            set { _RouteFrom = value; }
        }

        string _RouteFromName;
        public string RouteFromName
        {
            get { return _RouteFromName; }
            set { _RouteFromName = value; }
        }

        string _RouteFromNameL;
        public string RouteFromNameL
        {
            get { return _RouteFromNameL; }
            set { _RouteFromNameL = value; }
        }

        int? _RouteTo;
        public int? RouteTo
        {
            get { return _RouteTo; }
            set { _RouteTo = value; }
        }

        string _RouteToName;
        public string RouteToName
        {
            get { return _RouteToName; }
            set { _RouteToName = value; }
        }

        string _RouteToNameL;
        public string RouteToNameL
        {
            get { return _RouteToNameL; }
            set { _RouteToNameL = value; }
        }

        decimal? _ChdG1MaxAge;
        public decimal? ChdG1MaxAge
        {
            get { return _ChdG1MaxAge; }
            set { _ChdG1MaxAge = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        DateTime _TransDate;
        public DateTime TransDate
        {
            get { return _TransDate; }
            set { _TransDate = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

    }

    [Serializable()]
    public class TransportDays
    {
        public TransportDays()
        {
        }

        DateTime? _TransDate;
        public DateTime? TransDate
        {
            get { return _TransDate; }
            set { _TransDate = value; }
        }

        int? _RouteFrom;
        public int? RouteFrom
        {
            get { return _RouteFrom; }
            set { _RouteFrom = value; }
        }

        string _RouteFromName;
        public string RouteFromName
        {
            get { return _RouteFromName; }
            set { _RouteFromName = value; }
        }

        int? _RouteTo;
        public int? RouteTo
        {
            get { return _RouteTo; }
            set { _RouteTo = value; }
        }

        string _RouteToName;
        public string RouteToName
        {
            get { return _RouteToName; }
            set { _RouteToName = value; }
        }

        Int16? _SerArea;
        public Int16? SerArea
        {
            get { return _SerArea; }
            set { _SerArea = value; }
        }

    }

    [Serializable()]
    public class TransportDayRouteV
    {
        public TransportDayRouteV()
        {
        }
        DateTime? _TransDate;
        public DateTime? TransDate { get { return _TransDate; } set { _TransDate = value; } }
        string _Transport;
        public string Transport { get { return _Transport; } set { _Transport = value; } }
        int? _Departure;
        public int? Departure { get { return _Departure; } set { _Departure = value; } }
        string _DepartureName;
        public string DepartureName { get { return _DepartureName; } set { _DepartureName = value; } }
        int? _Arrival;
        public int? Arrival { get { return _Arrival; } set { _Arrival = value; } }
        string _ArrivalName;
        public string ArrivalName { get { return _ArrivalName; } set { _ArrivalName = value; } }        
        Int16? _DepRet;
        public Int16? DepRet { get { return _DepRet; } set { _DepRet = value; } }
        Int16? _SerArea;
        public Int16? SerArea { get { return _SerArea; } set { _SerArea = value; } }
        string _Bus;
        public string Bus { get { return _Bus; } set { _Bus = value; } }
    }

    [Serializable()]
    public class TransportCInRecord
    {
        public TransportCInRecord()
        { 
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Transport;
        public string Transport
        {
            get { return _Transport; }
            set { _Transport = value; }
        }

        DateTime? _TransportDate;
        public DateTime? TransportDate
        {
            get { return _TransportDate; }
            set { _TransportDate = value; }
        }

        string _Bus;
        public string Bus
        {
            get { return _Bus; }
            set { _Bus = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _BusSeatNo;
        public Int16? BusSeatNo
        {
            get { return _BusSeatNo; }
            set { _BusSeatNo = value; }
        }

        int? _ServiceID;
        public int? ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        bool? _Reserve;
        public bool? Reserve
        {
            get { return _Reserve; }
            set { _Reserve = value; }
        }

        string _SeatLetter;
        public string SeatLetter
        {
            get { return _SeatLetter; }
            set { _SeatLetter = value; }
        }

        string _Sex;
        public string Sex
        {
            get { return _Sex; }
            set { _Sex = value; }
        }

        string _Comment;
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        bool? _ExtSeat;
        public bool? ExtSeat
        {
            get { return _ExtSeat; }
            set { _ExtSeat = value; }
        }
    }
}
