﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class PackageSearchMakeRes
    {
        public bool resOK { get; set; }
        public string errMsg { get; set; }
        public string version { get; set; }
    }

    [Serializable()]
    public class reCalcPriceRecord
    {
        public reCalcPriceRecord() { }

        bool? _reCalcOk;
        public bool? ReCalcOk
        {
            get { return _reCalcOk; }
            set { _reCalcOk = value; }
        }

        string _errorMsg;
        public string ErrorMsg
        {
            get { return _errorMsg; }
            set { _errorMsg = value; }
        }

        string _salePrice;
        public string SalePrice
        {
            get { return _salePrice; }
            set { _salePrice = value; }
        }

        string _totalSalePrice;
        public string TotalSalePrice
        {
            get { return _totalSalePrice; }
            set { _totalSalePrice = value; }
        }
    }

    [Serializable()]
    public class resCustjSonData
    {
        public resCustjSonData()
        {
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        Int16? _Title;
        public Int16? Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _SurnameL;
        public string SurnameL
        {
            get { return _SurnameL; }
            set { _SurnameL = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Birtday;
        public string Birtday
        {
            get { return _Birtday; }
            set { _Birtday = value; }
        }

        string _Age;
        public string Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        string _IDNo;
        public string IDNo
        {
            get { return _IDNo; }
            set { _IDNo = value; }
        }

        string _PassSerie;
        public string PassSerie
        {
            get { return _PassSerie; }
            set { _PassSerie = value; }
        }

        string _PassNo;
        public string PassNo
        {
            get { return _PassNo; }
            set { _PassNo = value; }
        }

        string _PassIssueDate;
        public string PassIssueDate
        {
            get { return _PassIssueDate; }
            set { _PassIssueDate = value; }
        }

        string _PassExpDate;
        public string PassExpDate
        {
            get { return _PassExpDate; }
            set { _PassExpDate = value; }
        }

        string _PassGiven;
        public string PassGiven
        {
            get { return _PassGiven; }
            set { _PassGiven = value; }
        }

        string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        int? _Nation;
        public int? Nation
        {
            get { return _Nation; }
            set { _Nation = value; }
        }

        string _Nationality;
        public string Nationality
        {
            get { return _Nationality; }
            set { _Nationality = value; }
        }

        bool? _Passport;
        public bool? Passport
        {
            get { return _Passport; }
            set { _Passport = value; }
        }

        bool? _Leader;
        public bool? Leader
        {
            get { return _Leader; }
            set { _Leader = value; }
        }

    }

    public class resMainDivRecord
    {
        public resMainDivRecord()
        {
            _divFixResNote = false;
        }
        string _data; public string data { get { return _data; } set { _data = value; } }
        string _statusPromotion; public string statusPromotion { get { return _statusPromotion; } set { _statusPromotion = value; } }
        bool _divACE; public bool divACE { get { return _divACE; } set { _divACE = value; } }
        bool _divDiscountAgencyCom; public bool divDiscountAgencyCom { get { return _divDiscountAgencyCom; } set { _divDiscountAgencyCom = value; } }
        bool _divWhereInvoice; public bool divWhereInvoice { get { return _divWhereInvoice; } set { _divWhereInvoice = value; } }        
        string _divSaveOptionDiv; public string divSaveOptionDiv { get { return _divSaveOptionDiv; } set { _divSaveOptionDiv = value; } }
        bool _divSaveOption; public bool divSaveOption { get { return _divSaveOption; } set { _divSaveOption = value; } }
        bool _divResNote; public bool divResNote { get { return _divResNote; } set { _divResNote = value; } }
        bool _divFixResNote; public bool divFixResNote { get { return _divFixResNote; } set { _divFixResNote = value; } }
        string _ResNote; public string ResNote { get { return _ResNote; } set { _ResNote = value; } }
        string _ResNoteOpt; public string ResNoteOpt { get { return _ResNoteOpt; } set { _ResNoteOpt = value; } }
        bool _divBonusID; public bool divBonusID { get { return _divBonusID; } set { _divBonusID = value; } }
        CodeName[] _ppPrice; public CodeName[] ppPrice { get { return _ppPrice; } set { _ppPrice = value; } }
        string _CustomRegID; public string CustomRegID { get { return _CustomRegID; } set { _CustomRegID = value; } }
        string _PromoCode; public string PromoCode { get { return _PromoCode; } set { _PromoCode = value; } }
        bool _showArrivalInformation; public bool showArrivalInformation { get { return _showArrivalInformation; } set { _showArrivalInformation = value; } }
        List<CodeName> _arrivalAirPorts; public List<CodeName> arrivalAirPorts { get { return _arrivalAirPorts; } set { _arrivalAirPorts = value; } }
        string _mobPhoneMask; public string mobPhoneMask { get { return _mobPhoneMask; } set { _mobPhoneMask = value; } }
        string _phoneMask; public string phoneMask { get { return _phoneMask; } set { _phoneMask = value; } }
        bool _showSpecialCode; public bool showSpecialCode { get { return _showSpecialCode; } set { _showSpecialCode = value; } }
        bool _isMobile; public bool isMobile{ get { return _isMobile; } set { _isMobile = value; } }
    }

    [Serializable]
    public class resCustjSonDataF
    {
        public resCustjSonDataF()
        {
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        Int16? _Title;
        public Int16? Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _SurnameL;
        public string SurnameL
        {
            get { return _SurnameL; }
            set { _SurnameL = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Birtday;
        public string Birtday
        {
            get { return _Birtday; }
            set { _Birtday = value; }
        }

        string _Age;
        public string Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        string _IDNo;
        public string IDNo
        {
            get { return _IDNo; }
            set { _IDNo = value; }
        }

        string _PassSerie;
        public string PassSerie
        {
            get { return _PassSerie; }
            set { _PassSerie = value; }
        }

        string _PassNo;
        public string PassNo
        {
            get { return _PassNo; }
            set { _PassNo = value; }
        }

        string _PassIssueDate;
        public string PassIssueDate
        {
            get { return _PassIssueDate; }
            set { _PassIssueDate = value; }
        }

        string _PassExpDate;
        public string PassExpDate
        {
            get { return _PassExpDate; }
            set { _PassExpDate = value; }
        }

        string _PassGiven;
        public string PassGiven
        {
            get { return _PassGiven; }
            set { _PassGiven = value; }
        }

        string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        int? _Nation;
        public int? Nation
        {
            get { return _Nation; }
            set { _Nation = value; }
        }

        string _Nationality;
        public string Nationality
        {
            get { return _Nationality; }
            set { _Nationality = value; }
        }

        bool? _Passport;
        public bool? Passport
        {
            get { return _Passport; }
            set { _Passport = value; }
        }

        bool? _Leader;
        public bool? Leader
        {
            get { return _Leader; }
            set { _Leader = value; }
        }

    }

    [Serializable()]
    public class resCustjSonDataV2
    {
        public resCustjSonDataV2()
        {
            _ResCustInfo = new ResCustInfojSonV2();
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        Int16? _Title;
        public Int16? Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _SurnameL;
        public string SurnameL
        {
            get { return _SurnameL; }
            set { _SurnameL = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Birtday;
        public string Birtday
        {
            get { return _Birtday; }
            set { _Birtday = value; }
        }

        string _Age;
        public string Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        string _IDNo;
        public string IDNo
        {
            get { return _IDNo; }
            set { _IDNo = value; }
        }

        string _PassSerie;
        public string PassSerie
        {
            get { return _PassSerie; }
            set { _PassSerie = value; }
        }

        string _PassNo;
        public string PassNo
        {
            get { return _PassNo; }
            set { _PassNo = value; }
        }

        string _PassIssueDate;
        public string PassIssueDate
        {
            get { return _PassIssueDate; }
            set { _PassIssueDate = value; }
        }

        string _PassExpDate;
        public string PassExpDate
        {
            get { return _PassExpDate; }
            set { _PassExpDate = value; }
        }

        string _PassGiven;
        public string PassGiven
        {
            get { return _PassGiven; }
            set { _PassGiven = value; }
        }

        string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        int? _Nation;
        public int? Nation
        {
            get { return _Nation; }
            set { _Nation = value; }
        }

        string _Nationality;
        public string Nationality
        {
            get { return _Nationality; }
            set { _Nationality = value; }
        }

        bool? _Passport;
        public bool? Passport
        {
            get { return _Passport; }
            set { _Passport = value; }
        }

        bool? _Leader;
        public bool? Leader
        {
            get { return _Leader; }
            set { _Leader = value; }
        }

        ResCustInfojSonV2 _ResCustInfo;
        public ResCustInfojSonV2 ResCustInfo
        {
            get { return _ResCustInfo; }
            set { _ResCustInfo = value; }
        }
    }

    [Serializable()]
    public class ResNoteOptDetail
    {
        public string Code { get; set; }
        public string Descrition { get; set; }
        public bool Visible { get; set; }
        public ResNoteOptDetail()
        {
            this.Visible = true;
        }
    }

    [Serializable()]
    public class ResNoteOpt
    {
        public string Market { get; set; }
        public List<ResNoteOptDetail> Details { get; set; }

        public ResNoteOpt()
        {
            this.Details = new List<ResNoteOptDetail>();
        }
    }

    public class ServiceVehicleID
    {
        public int? ServiceID { get; set; }
        public int? VehicleCatID { get; set; }
        public Int16? VehicleUnit { get; set; }
        public ServiceVehicleID()
        { 
        }
    }
}
