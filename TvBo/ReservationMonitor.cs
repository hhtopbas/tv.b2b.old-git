﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Data;
using System.Web;
using System.Globalization;
using System.Threading;
using TvTools;
using System.Collections;

namespace TvBo
{
    public class ReservationMonitor
    {
        public List<resMonitorDefaultRecord> getResMonDefaultData(User UserData, ref string errorMsg)
        {
            List<AgencyCanShareRecord> shareAgency = new ReservationMonitor().getShareAgency(UserData, ref errorMsg);
            bool agencyResShare = shareAgency != null && shareAgency.Count() > 0;

            List<resMonitorDefaultRecord> records = new List<resMonitorDefaultRecord>();
            bool? groupHolpack = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "UseHolpackGroup"));

            string tsql =
@" 
Declare @MainAgency VarChar(10), @Market VarChar(10), @AgencyType SmallInt 
";

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                tsql +=
@"  
Select @MainAgency=MainOffice,
    @Market=O.Market
From Agency A (NOLOCK) 
Join Office O (NOLOCK) ON O.Code=A.OprOffice
Where A.Code=@Agency                             
";
            }
            else
            {
                tsql +=
@"Select 
    @MainAgency=Case When AgencyType=0 Then @Agency Else (Case when AgencyType<>2 Then MainOffice Else @Agency End) End,
	@AgencyType=AgencyType,
    @Market=O.Market
From Agency A (NOLOCK) 
Join Office O (NOLOCK) ON O.Code=A.OprOffice
Where A.Code=@Agency 
";
            }

            if (groupHolpack.HasValue && groupHolpack.Value)
            {
                tsql +=
@"  
if OBJECT_ID('TempDB.dbo.#GrpHolPack') is not null Drop Table dbo.#GrpHolPack
if OBJECT_ID('TempDB.dbo.#GrpHolPackDet') is not null Drop Table dbo.#GrpHolPackDet
 
Select *, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
INTO #GrpHolPack 
From GrpHolPack (NOLOCK)
Where GrpType='R' And Status=1 And Market=@Market

Select * INTO #GrpHolPackDet
From GrpHolPackDet (NOLOCK)
Where GrpType='R'
";
            }

            tsql +=
@"if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,Type 
Into #Location
From Location (NOLOCK)

SELECT Distinct RM.Agency, 
    AgencyName=A.Name,AgencyNameL=isnull(dbo.FindLocalName(A.NameLID, @Market), A.Name),
    AgencyUser=isnull(RM.AgencyUser,''), 
    AgencyUserName=isnull(AU.Name,''),AgencyUserNameL=isnull(dbo.FindLocalName(AU.NameLID,@Market),isnull(AU.Name,'')),
    isAgencyUserActive=isnull(AU.Status,'Y'),
";
            if (groupHolpack.HasValue && groupHolpack.Value)
                tsql +=
@"  
    HolPack=isnull(gH.Code, ''),HolPackName=isnull(gH.Name, ''),HolPackNameL=isnull(gH.NameL, ''),
";
            else
                tsql +=
@"  
    HolPack=isnull(RM.HolPack, ''),HolPackName=isnull(H.Name, ''),HolPackNameL=isnull(dbo.FindLocalName(H.NameLID,@Market),isnull(H.Name,'')),
";

            tsql +=
@"  RM.DepCity, 
    DepCityName=(Select Name From #Location (NOLOCK) Where RecID = RM.DepCity),
    DepCityNameL=(Select NameL From #Location (NOLOCK) Where RecID = RM.DepCity),
    RM.ArrCity,
    ArrCityName=(Select Name From #Location (NOLOCK) Where RecID = RM.ArrCity),
    ArrCityNameL=(Select NameL From #Location (NOLOCK) Where RecID = RM.ArrCity),
    RM.SaleCur
FROM ResMain RM (NOLOCK)
Join Agency A (NOLOCK) ON RM.Agency=A.Code
LEFT JOIN AgencyUser AU (NOLOCK) ON RM.Agency=AU.Agency And RM.AgencyUser=AU.Code
";
            if (groupHolpack.HasValue && groupHolpack.Value)
                tsql +=
@"
LEFT JOIN #GrpHolPackDet gHD ON gHD.HolPack=RM.HolPack
LEFT JOIN #GrpHolPack gH ON gH.Code=gHD.Groups And gH.Market=RM.PLMarket
Where 
";
            else
                tsql +=
@"
LEFT JOIN HolPack H (NOLOCK) ON RM.HolPack=H.Code
Where
";
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || Equals(UserData.CustomRegID, TvBo.Common.crID_FilipTravel))
            {
                if (agencyResShare)

                    tsql +=
@"  
   (
        Exists(Select null From Agency A1 (NOLOCK)               
        Where Code=RM.Agency 
            And (((Case When @MainAgency<>@Agency then Code Else MainOffice End)=@Agency or 
                (Case When @MainAgency=@Agency then Code Else MainOffice End)=@Agency                                 
            And (@ShowAllRes = 'Y' Or AgencyUser = @Agency))
            Or Exists(Select null From AgencyCanShare(NOLOCK) Where CanShareAgency=@Agency And Agency=A.Code))            
   )
";
                else
                    tsql +=
@"  
   (
        Exists(Select null From Agency A1 (NOLOCK)               
        Where Code=RM.Agency 
            And (((Case When @MainAgency<>@Agency then Code Else MainOffice End)=@Agency or 
                (Case When @MainAgency=@Agency then '' Else MainOffice End)=@Agency                                 
            And (@ShowAllRes = 'Y' Or AgencyUser = @Agency))
            )            
   )
";
            }
            else
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba) && UserData.CanEditOthRes.HasValue && UserData.CanEditOthRes.Value == true)
                tsql +=
@"
  (
";
            else
                tsql +=
@"  
  (@MainAgency=Case When @AgencyType<>2 Then
				(Select Agency=(Case When AgencyType=0 Then Code Else MainOffice End) From Agency (NOLOCK) Where Code=RM.Agency) 
			   Else
				(Select Agency=(Case When AgencyType=2 Then Code Else MainOffice End) From Agency (NOLOCK) Where Code=RM.Agency) 
			   End 
";

            if (UserData.Aggregator && !string.IsNullOrEmpty(UserData.MerlinxID) && UserData.MainAgency)
                tsql +=
@" 
   Or isnull(RM.MerlinxID, '') <> '' 
";

            if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
                if (string.IsNullOrEmpty(UserData.UserID))
                    tsql +=
@" 
   And @ShowAllRes='Y' 
";
                else
                    if (Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba) && UserData.CanEditOthRes.HasValue && UserData.CanEditOthRes.Value == true)
                    tsql +=
@" 
  @ShowAllRes='Y' 
";
                else
                    tsql += string.Format(@" And (@ShowAllRes = 'Y' Or (AgencyUser = '{0}' and RM.Agency=@Agency)) ", UserData.UserID);

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) && UserData.ShowAllRes && !string.Equals(UserData.MainOffice, UserData.AgencyID))
                tsql += " Or RM.Agency in (Select Code From Agency (NOLOCK) Where Code='" + UserData.MainOffice + "' OR MainOffice='" + UserData.MainOffice + "')) And RM.Market=@UserMarket ";
            else
                tsql += ") And RM.Market=@UserMarket ";
            tsql += "\n  And RM.DepCity is not null and RM.ArrCity is not null";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            dbCommand.CommandTimeout = 120;
            try
            {
                db.AddInParameter(dbCommand, "UserMarket", System.Data.DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", System.Data.DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "ShowAllRes", System.Data.DbType.AnsiString, UserData.ShowAllRes ? "Y" : "N");
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        resMonitorDefaultRecord rec = new resMonitorDefaultRecord();
                        rec.Agency = Conversion.getStrOrNull(oReader["Agency"]);
                        rec.AgencyName = Conversion.getStrOrNull(oReader["AgencyName"]);
                        rec.AgencyNameL = Conversion.getStrOrNull(oReader["AgencyNameL"]);
                        rec.AgencyUser = Conversion.getStrOrNull(oReader["AgencyUser"]);
                        rec.AgencyUserName = Conversion.getStrOrNull(oReader["AgencyUserName"]);
                        rec.AgencyUserNameL = Conversion.getStrOrNull(oReader["AgencyUserNameL"]);
                        rec.isAgencyUserActive = Conversion.getStrOrNull(oReader["isAgencyUserActive"]);
                        rec.ArrCity = (int)oReader["ArrCity"];
                        rec.ArrCityName = Conversion.getStrOrNull(oReader["ArrCityName"]);
                        rec.ArrCityNameL = Conversion.getStrOrNull(oReader["ArrCityNameL"]);
                        rec.DepCity = (int)oReader["DepCity"];
                        rec.DepCityName = Conversion.getStrOrNull(oReader["DepCityName"]);
                        rec.DepCityNameL = Conversion.getStrOrNull(oReader["DepCityNameL"]);
                        rec.HolPack = Conversion.getStrOrNull(oReader["HolPack"]);
                        rec.HolPackName = Conversion.getStrOrNull(oReader["HolPackName"]);
                        rec.HolPackNameL = Conversion.getStrOrNull(oReader["HolPackNameL"]);
                        rec.Currency = Conversion.getStrOrNull(oReader["SaleCur"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<listString> getHolPacks(bool useLocalName, List<resMonitorDefaultRecord> data)
        {
            if (useLocalName)
            {
                var Result = from rst in data.AsEnumerable()
                             group rst by new { HolPack = rst.HolPack.ToUpper(), HolPackName = rst.HolPackNameL } into r
                             select new listString { Code = r.Key.HolPack, Name = r.Key.HolPackName };
                return Result.ToList<listString>();
            }
            else
            {
                var Result = from rst in data.AsEnumerable()
                             group rst by new { HolPack = rst.HolPack.ToUpper(), HolPackName = rst.HolPackName } into r
                             select new listString { Code = r.Key.HolPack, Name = r.Key.HolPackName };
                return Result.ToList<listString>();
            }
        }

        public List<listString> getAgencyOffices(bool useLocalName, List<resMonitorDefaultRecord> data)
        {
            if (useLocalName)
            {
                var Result = from rst in data
                             group rst by new { Code = rst.Agency.ToUpper(), Name = rst.AgencyNameL } into r
                             select new listString { Code = r.Key.Code, Name = string.IsNullOrEmpty(r.Key.Name) ? "The agency has been deleted." : r.Key.Name };
                return Result.ToList<listString>();
            }
            else
            {
                var Result = from rst in data
                             group rst by new { Code = rst.Agency.ToUpper(), Name = rst.AgencyName } into r
                             select new listString { Code = r.Key.Code, Name = string.IsNullOrEmpty(r.Key.Name) ? "The agency has been deleted." : r.Key.Name };
                return Result.ToList<listString>();
            }
        }

        public List<listString> getAgencyUser(bool useLocalName, List<resMonitorDefaultRecord> data)
        {
            if (useLocalName)
            {
                var Result = from rst in data.AsEnumerable()
                             group rst by new { Code = rst.AgencyUser.ToUpper(), Name = string.IsNullOrEmpty(rst.AgencyUser) ? "(" + rst.AgencyNameL + ") Admin" : rst.AgencyUserNameL, Agency = rst.Agency, Status = string.Equals(rst.isAgencyUserActive, "N") ? false : true } into r
                             select new listString { Code = r.Key.Code, Name = string.IsNullOrEmpty(r.Key.Name) ? "The user has been deleted." : r.Key.Name, ParentCode = r.Key.Agency, status = r.Key.Status };
                return Result.ToList<listString>();
            }
            else
            {
                var Result = from rst in data.AsEnumerable()
                             group rst by new { Code = rst.AgencyUser.ToUpper(), Name = string.IsNullOrEmpty(rst.AgencyUser) ? "(" + rst.AgencyName + ") Admin" : rst.AgencyUserName, Agency = rst.Agency, Status = string.Equals(rst.isAgencyUserActive, "N") ? false : true } into r
                             select new listString { Code = r.Key.Code, Name = string.IsNullOrEmpty(r.Key.Name) ? "The user has been deleted." : r.Key.Name, ParentCode = r.Key.Agency, status = r.Key.Status };
                return Result.ToList<listString>();
            }
        }

        public List<listString> getArrCity(bool useLocalName, List<resMonitorDefaultRecord> data)
        {
            if (useLocalName)
            {
                var Result = from rst in data.AsEnumerable()
                             group rst by new { Code = rst.ArrCity, Name = rst.ArrCityNameL } into r
                             select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
                return Result.ToList<listString>();
            }
            else
            {
                var Result = from rst in data.AsEnumerable()
                             group rst by new { Code = rst.ArrCity, Name = rst.ArrCityName } into r
                             select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
                return Result.ToList<listString>();
            }
        }

        public List<listString> getDepCity(bool useLocalName, List<resMonitorDefaultRecord> data)
        {
            if (useLocalName)
            {
                var Result = from rst in data.AsEnumerable()
                             group rst by new { Code = rst.DepCity, Name = rst.DepCityNameL } into r
                             select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
                return Result.ToList<listString>();
            }
            else
            {
                var Result = from rst in data.AsEnumerable()
                             group rst by new { Code = rst.DepCity, Name = rst.DepCityName } into r
                             select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
                return Result.ToList<listString>();
            }
        }

        public List<listString> getCurrency(bool useLocalName, string Market, List<resMonitorDefaultRecord> data)
        {
            string errorMsg = string.Empty;
            List<CurrencyRecord> currList = new Banks().getCurrencys(Market, ref errorMsg);
            if (useLocalName)
            {
                var Result = from rst in data.AsEnumerable()
                             join cur in currList on rst.Currency equals cur.Code
                             group rst by new { Code = cur.Code, Name = cur.NameL } into r
                             select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
                return Result.ToList<listString>();
            }
            else
            {
                var Result = from rst in data.AsEnumerable()
                             join cur in currList on rst.Currency equals cur.Code
                             group rst by new { Code = cur.Code, Name = cur.Name } into r
                             select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
                return Result.ToList<listString>();
            }
        }

        public List<resMonitorRecordV2> getResMonitorData_V2(User UserData, string AgencyUser, string AgencyOffice, string BegResNo, string EndResNo,
                                                        int? DepCity, int? ArrCity, DateTime? BegDateFirst, DateTime? BegDateLast, DateTime? EndDateFirst, DateTime? EndDateLast,
                                                        DateTime? ResDateFirst, DateTime? ResDateLast, string HolPack, string Surname, string Name, string ResStatusStr,
                                                        string ConfirmStatusStr, string PaymentStatusStr, Int16 OptionControl, bool ShowDraft,
                                                        string Currency, bool showOnlyActiveUsers, bool ShowOnlyPxmRes, ref string errorMsg)
        {
            List<resMonitorRecordV2> records = new List<resMonitorRecordV2>();

            List<AgencyCanShareRecord> shareAgency = new ReservationMonitor().getShareAgency(UserData, ref errorMsg);
            bool agencyResShare = shareAgency != null && shareAgency.Count() > 0;

            bool? groupHolpack = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "UseHolpackGroup"));
            string tsql = string.Empty;
            #region Main Sql
            if (VersionControl.getTableField("ResMain", "PxmResNo"))
                tsql = string.Format(
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location

Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type]
Into #Location
From Location (NOLOCK)

Select * From (   
    Select RM.RefNo,RM.ResNo,LeaderName=(RC.Surname+' '+RC.Name), 
		HotelName=isnull(H.Name,''),HotelNameL=isnull(dbo.FindLocalName(H.NameLID,@Market),isnull(H.Name,'')),
		RM.Adult,RM.Child,RM.BegDate,{0},RM.Days,RM.OptDate,RM.ResStat,RM.ConfStat,RM.PaymentStat,
		SPrice=(isnull(RM.SalePrice,0)+isnull(RM.Discount,0)-isnull(RM.EBPas,0)-isnull(RM.EBAgency,0)),RM.SaleCur, 
		RM.DepCity, DepCityName=(Select Name From #Location Where RecID=RM.DepCity),DepCityNameL=(Select NameL From #Location Where RecID=RM.DepCity), 
		RM.ArrCity, ArrCityName=(Select Name From #Location Where RecID=RM.ArrCity),ArrCityNameL=(Select NameL From #Location Where RecID=RM.ArrCity),
		NewComment=Case When ISNULL(NC.RecID,0)>0 then Cast(1 AS bit) Else Cast(0 AS bit) End,
		RM.ResNote, RM.ReadByOpr,isPxmReservation=(Case When isnull(RM.PxmResNo,'')<>'' Then Cast(1 As bit) Else Cast(0 As bit) End)
	FROM ResMain RM (NOLOCK) 		
	JOIN ResCust RC (NOLOCK) ON RM.ResNo = RC.ResNo And RC.Leader = 'Y' 
	Left JOIN AgencyUser AU (NOLOCK) ON AU.Code = RM.AgencyUser AND AU.Agency=RM.Agency 
	JOIN Agency A (NOLOCK) ON A.Code = RM.Agency                         
    OUTER APPLY
    (
		Select RecID=Count(RecID) From ResComment Where ResNo=RM.ResNo AND UserType='O'AND IsRead='N'
    ) NC
	OUTER APPLY
	(
        Select Top 1 Hotel=Service From ResService (NOLOCK) Where ResNo=RM.ResNo And ServiceType='HOTEL' And StatConf<=1 And StatSer<=1
    ) HS
    LEFT JOIN Hotel H (NOLOCK) ON H.Code=HS.Hotel
", string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ? "ResDate=(RM.ResDate+RM.ResTime)" : "RM.ResDate");
            else
                tsql = string.Format(
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location

Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type]
Into #Location
From Location (NOLOCK)

Select * From (   
    Select RM.RefNo,RM.ResNo,LeaderName=(RC.Surname+' '+RC.Name), 
		HotelName=isnull(H.Name,''),HotelNameL=isnull(dbo.FindLocalName(H.NameLID,@Market),isnull(H.Name,'')),
		RM.Adult,RM.Child,RM.BegDate,{0},RM.Days,RM.OptDate,RM.ResStat,RM.ConfStat,RM.PaymentStat,
		SPrice=(isnull(RM.SalePrice,0)+isnull(RM.Discount,0)-isnull(RM.EBPas,0)-isnull(RM.EBAgency,0)),RM.SaleCur, 
		RM.DepCity, DepCityName=(Select Name From #Location Where RecID=RM.DepCity),DepCityNameL=(Select NameL From #Location Where RecID=RM.DepCity), 
		RM.ArrCity, ArrCityName=(Select Name From #Location Where RecID=RM.ArrCity),ArrCityNameL=(Select NameL From #Location Where RecID=RM.ArrCity),
		NewComment=Case When ISNULL(NC.RecID,0)>0 then Cast(1 AS bit) Else Cast(0 AS bit) End,
		RM.ResNote, RM.ReadByOpr,isPxmReservation=Cast(0 As bit)
	FROM ResMain RM (NOLOCK) 		
	JOIN ResCust RC (NOLOCK) ON RM.ResNo = RC.ResNo And RC.Leader = 'Y' 
	Left JOIN AgencyUser AU (NOLOCK) ON AU.Code = RM.AgencyUser AND AU.Agency=RM.Agency 
	JOIN Agency A (NOLOCK) ON A.Code = RM.Agency                         
    OUTER APPLY
    (
		Select RecID=Count(RecID) From ResComment Where ResNo=RM.ResNo AND UserType='O'AND IsRead='N'
    ) NC
	OUTER APPLY
	(
        Select Top 1 Hotel=Service From ResService (NOLOCK) Where ResNo=RM.ResNo And ServiceType='HOTEL' And StatConf<=1 And StatSer<=1
    ) HS
    LEFT JOIN Hotel H (NOLOCK) ON H.Code=HS.Hotel
", string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ? "ResDate=(RM.ResDate+RM.ResTime)" : "RM.ResDate");


            if (groupHolpack.HasValue && groupHolpack.Value && !string.IsNullOrEmpty(HolPack))
            {
                tsql +=
@"
JOIN GrpHolPackDet gHD (NOLOCK) ON gHD.GrpType='R' And gHD.HolPack=RM.HolPack 
";
                tsql += string.Format(
@"
JOIN GrpHolPack gH (NOLOCK) ON gH.Market=RM.PLMarket And gH.GrpType='R' And gH.Code=gHD.Groups And gH.Code='{0}' 
", HolPack);
            }
            tsql +=
@"
Where RM.SaleCur is not null 
  And RM.Market=@Market 
";
            #endregion Main Sql

            #region Where

            if (agencyResShare)
            {
                if (string.IsNullOrEmpty(AgencyOffice))
                {
                    tsql +=
@" 
  And (RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@Agency Or MainOffice=@Agency) Or Exists(Select null From AgencyCanShare Where CanShareAgency=@Agency And Agency=A.Code) )
";
                }
                else
                    tsql += string.IsNullOrEmpty(AgencyOffice) ? "" : string.Format(@" And A.Code='{0}' ", AgencyOffice);
            }
            else
                if (string.IsNullOrEmpty(AgencyOffice))
            {
                //if (UserData.CanEditOthRes.HasValue && UserData.CanEditOthRes.Value == true && UserData.ShowAllRes && !UserData.ShowAllOtherOffRes)
                //{
                //    tsql += @" And (RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@Agency) ";
                //}
                //else
                //{

                    //tsql += @" And (RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@Agency Or MainOffice=@Agency) ";
                    if (UserData.ShowAllRes && UserData.ShowAllOtherOffRes)
                        tsql += @"  And RM.Agency in (Select Code From Agency (NOLOCK) Where (MainOffice in(@AgencyMainOffice,@Agency) or Code=@Agency))";
                    else if (UserData.ShowAllRes)
                        tsql += @"  And A.Code = @Agency";
                    else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && !string.Equals(UserData.MainOffice, UserData.AgencyID))
                        tsql += @"  And RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@MainOffice Or MainOffice=@MainOffice))";
                    else
                            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) && UserData.ShowAllRes && !string.Equals(UserData.MainOffice, UserData.AgencyID))
                        tsql += @" And RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@MainOffice Or MainOffice=@MainOffice))";
                   
                //}
            }
            else
                tsql += string.IsNullOrEmpty(AgencyOffice) ? "" : string.Format(" And A.Code='{0}' ", AgencyOffice);
            if(!string.IsNullOrEmpty(AgencyUser))
                tsql += string.Format(" And AU.Code='{0}' ", AgencyUser);

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && !UserData.ShowAllRes)
            {
                if (string.IsNullOrEmpty(AgencyUser))
                    tsql += @"  And (isnull(RM.AgencyUser, '') = '') ";
                else
                    tsql += string.Format(@"  And (RM.AgencyUser = '{0}' Or isnull(RM.AgencyUser, '') = '') ", AgencyUser);
            }
            else
                if (!UserData.ShowAllRes)
            {
                if (!string.IsNullOrEmpty(AgencyUser))
                    tsql += string.Format(@"  And RM.AgencyUser = '{0}' And RM.Agency='{1}' ", AgencyUser,UserData.AgencyID);
            }


            //if (!(UserData.CanEditOthRes.HasValue && UserData.CanEditOthRes.Value == true))
            //    if (!string.IsNullOrEmpty(AgencyUser))
            //        tsql += string.Format("  And RM.AgencyUser='{0}' ", AgencyUser);

            if (!string.IsNullOrEmpty(Currency))
                tsql += string.IsNullOrEmpty(Currency) ? string.Empty : string.Format(" And RM.SaleCur='{0}' ", Currency);

            if (string.IsNullOrEmpty(EndResNo))
            {
                tsql += string.IsNullOrEmpty(BegResNo) ? string.Empty : string.Format(" And RM.ResNo='{0}' ", BegResNo);
            }
            else
                if (string.IsNullOrEmpty(BegResNo))
            {
                tsql += string.IsNullOrEmpty(EndResNo) ? string.Empty : string.Format(" And RM.ResNo='{0}' ", EndResNo);
            }
            else
            {
                tsql += string.IsNullOrEmpty(BegResNo) ? string.Empty : string.Format(" And RM.ResNo>='{0}' ", BegResNo);
                tsql += string.IsNullOrEmpty(EndResNo) ? string.Empty : string.Format(" And RM.ResNo<='{0}' ", EndResNo);
            }
            tsql += ArrCity.HasValue && ArrCity.Value > -1 ? string.Format(" And RM.ArrCity={0} ", ArrCity) : string.Empty;
            tsql += DepCity.HasValue && DepCity.Value > -1 ? string.Format(" And RM.DepCity={0} ", DepCity) : string.Empty;
            tsql += BegDateFirst.HasValue && BegDateFirst.Value > DateTime.MinValue ? " And dbo.DateOnly(RM.BegDate) >= dbo.DateOnly(@BegDate1) " : string.Empty;
            tsql += BegDateLast.HasValue && BegDateLast.Value > DateTime.MinValue ? " And dbo.DateOnly(RM.BegDate) <= dbo.DateOnly(@BegDate2) " : string.Empty;
            tsql += EndDateFirst.HasValue && EndDateFirst.Value > DateTime.MinValue ? " And dbo.DateOnly(RM.EndDate) >= dbo.DateOnly(@EndDate1) " : string.Empty;
            tsql += EndDateLast.HasValue && EndDateLast.Value > DateTime.MinValue ? " And dbo.DateOnly(RM.EndDate) <= dbo.DateOnly(@EndDate2) " : string.Empty;
            tsql += ResDateFirst.HasValue && ResDateFirst.Value > DateTime.MinValue ? " And dbo.DateOnly(RM.ResDate) >= dbo.DateOnly(@ResDate1) " : string.Empty;
            tsql += ResDateLast.HasValue && ResDateLast.Value > DateTime.MinValue ? " And dbo.DateOnly(RM.ResDate) <= dbo.DateOnly(@ResDate2) " : string.Empty;
            if (UserData.Aggregator && !string.IsNullOrEmpty(UserData.MerlinxID) && UserData.MainAgency)
                tsql += " Or isnull(RM.MerlinxID, '') <> '' ";

            if (!ShowDraft)
            {
                if (ResStatusStr != "")
                    tsql += " And RM.ResStat in(" + ResStatusStr.Replace(';', ',') + ") ";
                if (ConfirmStatusStr != "")
                    tsql += " And RM.ConfStat in (" + ConfirmStatusStr.Replace(';', ',') + ") ";
                if (PaymentStatusStr != "")
                    tsql += " And RM.PaymentStat in ('" + PaymentStatusStr.Replace(";", "','") + "') ";
                if (OptionControl == 1)
                    tsql += " And RM.OptDate is not null ";
            }

            if ((groupHolpack.HasValue && groupHolpack.Value == false) || groupHolpack.HasValue == false)
                if (!string.IsNullOrEmpty(HolPack))
                    tsql += string.IsNullOrEmpty(HolPack) ? string.Empty : string.Format(" And RM.HolPack = '{0}'", HolPack);


            if (!Equals(Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "SearchCustomerOptions")), "1"))
            {
                string customers = string.Empty;
                if (!string.IsNullOrEmpty(Surname))
                    customers += string.Format("RC.Surname Like '%{0}%'", Surname);
                bool onlyNameSurnameSearch = string.Equals(UserData.CustomRegID, Common.crID_FilipTravel);
                if (!string.IsNullOrEmpty(Name))
                {
                    if (customers.Length > 0)
                        customers = "(" + customers + (onlyNameSurnameSearch ? " And " : " Or ") + string.Format("RC.Name Like '%{0}%'", Name) + ")";
                    else
                        customers += string.Format("RC.Name Like '%{0}%'", Name);
                }
                if (customers.Length > 0)
                    tsql += " And " + customers;
            }
            else
            {
                string customers = string.Empty;
                if (!string.IsNullOrEmpty(Surname))
                    customers += string.Format("Surname Like '{0}%'", Surname);
                if (!string.IsNullOrEmpty(Name))
                {
                    if (customers.Length > 0)
                        customers = "(" + customers + " Or " + string.Format("Name Like '{0}%'", Name) + ")";
                    else
                        customers += string.Format("Name Like '{0}%'", Name);
                }
                if (customers.Length > 0)
                    tsql += " And Exists(Select ResNo From ResCust (NOLOCK) Where ResNo=RC.ResNo And " + customers + ")";
            }

            if (showOnlyActiveUsers)
                tsql += " And isnull(AU.Status,'N')='Y' ";
            if (ShowOnlyPxmRes)
                tsql += " And isnull(RM.PxmResNo,'')<>'' ";
            #endregion Where

            tsql += @"
                      ) X 
                      Order By NewComment Desc, ResDate Desc, RefNo Desc
                     ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            dbCommand.CommandTimeout = 120;
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "MainOffice", DbType.String, string.IsNullOrEmpty(AgencyOffice) ? SqlString.Null : AgencyOffice);
                db.AddInParameter(dbCommand, "AgencyMainOffice", DbType.String, UserData.MainOffice);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                if (BegDateFirst.HasValue && BegDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "BegDate1", DbType.DateTime, BegDateFirst.Value);
                if (BegDateLast.HasValue && BegDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "BegDate2", DbType.DateTime, BegDateLast.Value);
                if (EndDateFirst.HasValue && EndDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "EndDate1", DbType.DateTime, EndDateFirst.Value);
                if (EndDateLast.HasValue && EndDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "EndDate2", DbType.DateTime, EndDateLast.Value);
                if (ResDateFirst.HasValue && ResDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "ResDate1", DbType.DateTime, ResDateFirst.Value);
                if (ResDateLast.HasValue && ResDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "ResDate2", DbType.DateTime, ResDateLast.Value);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        resMonitorRecordV2 rec = new resMonitorRecordV2();
                        rec.RefNo = (Int32)R["RefNo"];
                        rec.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        rec.LeaderName = Conversion.getStrOrNull(R["LeaderName"]);
                        rec.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        rec.HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]);
                        rec.Adult = Conversion.getInt16OrNull(R["Adult"]);
                        rec.Child = Conversion.getInt16OrNull(R["Child"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        rec.ResDate = Conversion.getDateTimeOrNull(R["ResDate"]);
                        rec.Days = Conversion.getInt16OrNull(R["Days"]);
                        rec.ResStat = Conversion.getInt16OrNull(R["ResStat"]);
                        rec.ConfStat = Conversion.getInt16OrNull(R["ConfStat"]);
                        rec.PaymentStat = Conversion.getStrOrNull(R["PaymentStat"]);
                        rec.SPrice = Conversion.getDecimalOrNull(R["SPrice"]);
                        rec.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        rec.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        rec.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        rec.DepCityNameL = Conversion.getStrOrNull(R["DepCityNameL"]);
                        rec.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        rec.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        rec.ArrCityNameL = Conversion.getStrOrNull(R["ArrCityNameL"]);
                        rec.NewComment = (bool)R["NewComment"];
                        rec.OptDate = Conversion.getDateTimeOrNull(R["OptDate"]);
                        rec.ReadByOpr = Conversion.getByteOrNull(R["ReadByOpr"]);
                        rec.ResNote = Conversion.getStrOrNull(R["ResNote"]);
                        rec.isPxmReservation = (bool)R["isPxmReservation"];
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<resMonitorExcelExportRecord> getExcelExportData(User UserData, string AgencyUser, string AgencyOffice, string BegResNo, string EndResNo,
                                                        int? DepCity, int? ArrCity, DateTime? BegDateFirst, DateTime? BegDateLast, DateTime? EndDateFirst, DateTime? EndDateLast,
                                                        DateTime? ResDateFirst, DateTime? ResDateLast, string HolPack, string Surname, string Name, string ResStatusStr,
                                                        string ConfirmStatusStr, string PaymentStatusStr, Int16 OptionControl, bool ShowDraft,
                                                        string Currency, bool showOnlyActiveUsers, ref string errorMsg)
        {
            List<resMonitorExcelExportRecord> records = new List<resMonitorExcelExportRecord>();

            List<AgencyCanShareRecord> shareAgency = new ReservationMonitor().getShareAgency(UserData, ref errorMsg);
            bool agencyResShare = shareAgency != null && shareAgency.Count() > 0;

            bool? groupHolpack = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "UseHolpackGroup"));
            string tsql = string.Empty;
            #region Main Sql

            tsql =
@"
if OBJECT_ID('TempDB.dbo.#Location') is not null Drop Table dbo.#Location
Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type]
Into #Location
From Location (NOLOCK)

Select * From (   
    Select RM.RefNo,RM.ResNo,
	    LeaderName=(RC.Surname+' '+RC.Name), 
		HotelName=isnull(H.Name,''),HotelNameL=isnull(dbo.FindLocalName(H.NameLID,@Market),isnull(H.Name,'')),
		HolPackName=isnull(Hp.Name,''),HolPackNameL=isnull(dbo.FindLocalName(Hp.NameLID,@Market),isnull(Hp.Name,'')),
		RM.Adult,RM.Child,RM.BegDate,RM.[Days],RM.ResDate,RM.ResStat,RM.ConfStat,RM.PaymentStat,RM.SalePrice,RM.Discount,RM.EBAgency,RM.EBAgencyPer,
		RM.EBPas,RM.AgencyBonusAmount,RM.UserBonusAmount,RM.PasBonusAmount,RM.PasAmount,RM.AgencyCom,RM.AgencyComPer,RM.AgencyComSup,RM.AgencyComSupPer,
		RM.AgencyDisPasVal,RM.AgencyDisPasPer,RM.BrokerCom,RM.BrokerComPer,RM.AgencyPayable,RM.AgencyPayment,RM.Balance,
		SPrice=(isnull(RM.SalePrice,0)+isnull(RM.Discount,0)-isnull(RM.EBPas,0)-isnull(RM.EBAgency,0)),RM.SaleCur, 
		RM.DepCity, DepCityName=(Select Name From #Location Where RecID=RM.DepCity),DepCityNameL=(Select NameL From #Location Where RecID=RM.DepCity), 
		RM.ArrCity, ArrCityName=(Select Name From #Location Where RecID=RM.ArrCity),ArrCityNameL=(Select NameL From #Location Where RecID=RM.ArrCity),
		RM.ChgDate,AgencyName=A.Name,AgencyNameL=isnull(dbo.FindLocalName(A.NameLID,@Market),isnull(A.Name,'')),
		AgencyUser=AU.Name,AgencyUserL=isnull(dbo.FindLocalName(AU.NameLID,@Market),isnull(AU.Name,'')),
		RM.ReadByOprUser,RM.ReadByOprDate
	FROM ResMain RM (NOLOCK) 		
	JOIN ResCust RC (NOLOCK) ON RM.ResNo = RC.ResNo And RC.Leader = 'Y' 
	LEFT JOIN AgencyUser AU (NOLOCK) ON AU.Code = RM.AgencyUser AND AU.Agency=RM.Agency 
	Left JOIN HolPack Hp (NOLOCK) ON RM.HolPack=Hp.Code
	JOIN Agency A (NOLOCK) ON A.Code = RM.Agency
    OUTER APPLY
    (
		Select RecID=Count(RecID) From ResComment Where ResNo=RM.ResNo AND UserType='O'AND IsRead='N'
    ) NC
	OUTER APPLY
	(
        Select Top 1 Hotel=Service From ResService (NOLOCK) Where ResNo=RM.ResNo And ServiceType='HOTEL' And StatConf<=1 And StatSer<=1
    ) HS
    LEFT JOIN Hotel H (NOLOCK) ON H.Code=HS.Hotel
";

            if (groupHolpack.HasValue && groupHolpack.Value && !string.IsNullOrEmpty(HolPack))
            {
                tsql += @"    JOIN GrpHolPackDet gHD (NOLOCK) ON gHD.GrpType='R' And gHD.HolPack=RM.HolPack 
                         ";
                tsql += string.Format("    JOIN GrpHolPack gH (NOLOCK) ON gH.Market=RM.PLMarket And gH.GrpType='R' And gH.Code=gHD.Groups And gH.Code='{0}' \n", HolPack);
            }
            tsql +=
@"    Where RM.SaleCur is not null 
        And RM.Market=@Market 
";
            #endregion Main Sql

            #region Where

            if (agencyResShare)
            {
                if (string.IsNullOrEmpty(AgencyOffice))
                {
                    tsql += "        And (RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@Agency Or MainOffice=@Agency) Or Exists(Select null From AgencyCanShare Where CanShareAgency=@Agency And Agency=A.Code) )";
                }
                else
                    tsql += string.IsNullOrEmpty(AgencyOffice) ? "" : string.Format("        And A.Code='{0}' ", AgencyOffice);
            }
            else
                if (string.IsNullOrEmpty(AgencyOffice))
            {
                if (UserData.CanEditOthRes.HasValue && UserData.CanEditOthRes.Value == true && UserData.ShowAllRes)
                {

                }
                else
                {
                    tsql += @"        And (RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@Agency Or MainOffice=@Agency) ";

                    if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && !string.Equals(UserData.MainOffice, UserData.AgencyID))
                        tsql += @"          Or RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@MainOffice Or MainOffice=@MainOffice))";
                    else
                        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) && UserData.ShowAllRes && !string.Equals(UserData.MainOffice, UserData.AgencyID))
                        tsql += @"         Or RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@MainOffice Or MainOffice=@MainOffice))";
                    else
                        tsql += ") ";
                }
            }
            else
                tsql += string.IsNullOrEmpty(AgencyOffice) ? "" : string.Format("        And A.Code='{0}' ", AgencyOffice);

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && !UserData.ShowAllRes)
            {
                if (string.IsNullOrEmpty(AgencyUser))
                    tsql += @"        And (isnull(RM.AgencyUser, '') = '') ";
                else
                    tsql += string.Format(@"        And (RM.AgencyUser = '{0}' Or isnull(RM.AgencyUser, '') = '') ", AgencyUser);
            }
            else
                if (!UserData.ShowAllRes)
            {
                if (!string.IsNullOrEmpty(AgencyUser))
                    tsql += string.Format(@"        And RM.AgencyUser = '{0}' ", AgencyUser);
            }

            if (!(UserData.CanEditOthRes.HasValue && UserData.CanEditOthRes.Value == true))
                if (!string.IsNullOrEmpty(AgencyUser))
                    tsql += string.Format("        And RM.AgencyUser='{0}' ", AgencyUser);

            if (!string.IsNullOrEmpty(Currency))
                tsql += string.IsNullOrEmpty(Currency) ? string.Empty : string.Format("        And RM.SaleCur='{0}' ", Currency);

            if (string.IsNullOrEmpty(EndResNo))
            {
                tsql += string.IsNullOrEmpty(BegResNo) ? string.Empty : string.Format("        And RM.ResNo='{0}' ", BegResNo);
            }
            else
                if (string.IsNullOrEmpty(BegResNo))
            {
                tsql += string.IsNullOrEmpty(EndResNo) ? string.Empty : string.Format("        And RM.ResNo='{0}' ", EndResNo);
            }
            else
            {
                tsql += string.IsNullOrEmpty(BegResNo) ? string.Empty : string.Format("        And RM.ResNo>='{0}' ", BegResNo);
                tsql += string.IsNullOrEmpty(EndResNo) ? string.Empty : string.Format("        And RM.ResNo<='{0}' ", EndResNo);
            }
            tsql += ArrCity.HasValue && ArrCity.Value > -1 ? string.Format("        And RM.ArrCity={0} ", ArrCity) : string.Empty;
            tsql += DepCity.HasValue && DepCity.Value > -1 ? string.Format("        And RM.DepCity={0} ", DepCity) : string.Empty;
            tsql += BegDateFirst.HasValue && BegDateFirst.Value > DateTime.MinValue ? "        And RM.BegDate >= @BegDate1 " : string.Empty;
            tsql += BegDateLast.HasValue && BegDateLast.Value > DateTime.MinValue ? "        And RM.BegDate <= @BegDate2 " : string.Empty;
            tsql += EndDateFirst.HasValue && EndDateFirst.Value > DateTime.MinValue ? "        And RM.EndDate >= @EndDate1 " : string.Empty;
            tsql += EndDateLast.HasValue && EndDateLast.Value > DateTime.MinValue ? "        And RM.EndDate <= @EndDate2 " : string.Empty;
            tsql += ResDateFirst.HasValue && ResDateFirst.Value > DateTime.MinValue ? "        And RM.ResDate >= @ResDate1 " : string.Empty;
            tsql += ResDateLast.HasValue && ResDateLast.Value > DateTime.MinValue ? "        And RM.ResDate <= @ResDate2 " : string.Empty;
            if (UserData.Aggregator && !string.IsNullOrEmpty(UserData.MerlinxID) && UserData.MainAgency)
                tsql += "         Or isnull(RM.MerlinxID, '') <> '' ";

            if (!ShowDraft)
            {
                if (ResStatusStr != "")
                    tsql += "        And RM.ResStat in(" + ResStatusStr.Replace(';', ',') + ") ";
                if (ConfirmStatusStr != "")
                    tsql += "        And RM.ConfStat in (" + ConfirmStatusStr.Replace(';', ',') + ") ";
                if (PaymentStatusStr != "")
                    tsql += "        And RM.PaymentStat in ('" + PaymentStatusStr.Replace(";", "','") + "') ";
                if (OptionControl == 1)
                    tsql += "        And RM.OptDate is not null ";
            }

            if ((groupHolpack.HasValue && groupHolpack.Value == false) || groupHolpack.HasValue == false)
                if (!string.IsNullOrEmpty(HolPack))
                    tsql += string.IsNullOrEmpty(HolPack) ? string.Empty : string.Format("        And RM.HolPack = '{0}'", HolPack);


            if (!Equals(Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "SearchCustomerOptions")), "1"))
            {
                string customers = string.Empty;
                if (!string.IsNullOrEmpty(Surname))
                    customers += string.Format("RC.Surname Like '%{0}%'", Surname);
                bool onlyNameSurnameSearch = string.Equals(UserData.CustomRegID, Common.crID_FilipTravel);
                if (!string.IsNullOrEmpty(Name))
                {
                    if (customers.Length > 0)
                        customers = "(" + customers + (onlyNameSurnameSearch ? " And " : " Or ") + string.Format("RC.Name Like '%{0}%'", Name) + ")";
                    else
                        customers += string.Format("RC.Name Like '%{0}%'", Name);
                }
                if (customers.Length > 0)
                    tsql += " And " + customers;
            }
            else
            {
                string customers = string.Empty;
                if (!string.IsNullOrEmpty(Surname))
                    customers += string.Format("Surname Like '{0}%'", Surname);
                if (!string.IsNullOrEmpty(Name))
                {
                    if (customers.Length > 0)
                        customers = "(" + customers + " Or " + string.Format("Name Like '{0}%'", Name) + ")";
                    else
                        customers += string.Format("Name Like '{0}%'", Name);
                }
                if (customers.Length > 0)
                    tsql += "        And Exists(Select ResNo From ResCust (NOLOCK) Where ResNo=RC.ResNo And " + customers + ")";
            }

            if (showOnlyActiveUsers)
                tsql += "        And isnull(AU.Status,'N')='Y' ";

            #endregion Where

            tsql +=
@"
) X 
Order By ResDate Desc, RefNo Desc
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            dbCommand.CommandTimeout = 120;
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "MainOffice", DbType.String, string.IsNullOrEmpty(AgencyOffice) ? SqlString.Null : AgencyOffice);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                if (BegDateFirst.HasValue && BegDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "BegDate1", DbType.DateTime, BegDateFirst.Value);
                if (BegDateLast.HasValue && BegDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "BegDate2", DbType.DateTime, BegDateLast.Value);
                if (EndDateFirst.HasValue && EndDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "EndDate1", DbType.DateTime, EndDateFirst.Value);
                if (EndDateLast.HasValue && EndDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "EndDate2", DbType.DateTime, EndDateLast.Value);
                if (ResDateFirst.HasValue && ResDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "ResDate1", DbType.DateTime, ResDateFirst.Value);
                if (ResDateLast.HasValue && ResDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "ResDate2", DbType.DateTime, ResDateLast.Value);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        resMonitorExcelExportRecord rec = new resMonitorExcelExportRecord();
                        rec.RefNo = (Int32)R["RefNo"];
                        rec.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        rec.LeaderName = Conversion.getStrOrNull(R["LeaderName"]);
                        rec.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        rec.HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]);
                        rec.HolPackName = Conversion.getStrOrNull(R["HolPackName"]);
                        rec.HolPackNameL = Conversion.getStrOrNull(R["HolPackNameL"]);
                        rec.Adult = Conversion.getInt16OrNull(R["Adult"]);
                        rec.Child = Conversion.getInt16OrNull(R["Child"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        rec.ResDate = Conversion.getDateTimeOrNull(R["ResDate"]);
                        rec.Days = Conversion.getInt32OrNull(R["Days"]);
                        rec.ResStat = Conversion.getInt16OrNull(R["ResStat"]);
                        rec.ConfStat = Conversion.getInt16OrNull(R["ConfStat"]);
                        rec.PaymentStat = Conversion.getStrOrNull(R["PaymentStat"]);
                        rec.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        rec.Discount = Conversion.getDecimalOrNull(R["Discount"]);
                        rec.EBAgency = Conversion.getDecimalOrNull(R["EBAgency"]);
                        rec.EBAgencyPer = Conversion.getDecimalOrNull(R["EBAgencyPer"]);
                        rec.EBPas = Conversion.getDecimalOrNull(R["EBPas"]);
                        rec.AgencyBonusAmount = Conversion.getDecimalOrNull(R["AgencyBonusAmount"]);
                        rec.UserBonusAmount = Conversion.getDecimalOrNull(R["UserBonusAmount"]);
                        rec.PasBonusAmount = Conversion.getDecimalOrNull(R["PasBonusAmount"]);
                        rec.PasAmount = Conversion.getDecimalOrNull(R["PasAmount"]);
                        rec.AgencyCom = Conversion.getDecimalOrNull(R["AgencyCom"]);
                        rec.AgencyComPer = Conversion.getDecimalOrNull(R["AgencyComPer"]);
                        rec.AgencyComSup = Conversion.getDecimalOrNull(R["AgencyComSup"]);
                        rec.AgencyComSupPer = Conversion.getDecimalOrNull(R["AgencyComSupPer"]);
                        rec.AgencyDisPasPer = Conversion.getDecimalOrNull(R["AgencyDisPasPer"]);
                        rec.AgencyDisPasVal = Conversion.getDecimalOrNull(R["AgencyDisPasVal"]);
                        rec.BrokerCom = Conversion.getDecimalOrNull(R["BrokerCom"]);
                        rec.BrokerComPer = Conversion.getDecimalOrNull(R["BrokerComPer"]);
                        rec.AgencyPayable = Conversion.getDecimalOrNull(R["AgencyPayable"]);
                        rec.AgencyPayment = Conversion.getDecimalOrNull(R["AgencyPayment"]);
                        rec.Balance = Conversion.getDecimalOrNull(R["Balance"]);
                        rec.SPrice = Conversion.getDecimalOrNull(R["SPrice"]);
                        rec.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        rec.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        rec.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        rec.DepCityNameL = Conversion.getStrOrNull(R["DepCityNameL"]);
                        rec.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        rec.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        rec.ArrCityNameL = Conversion.getStrOrNull(R["ArrCityNameL"]);
                        rec.ChgDate = Conversion.getDateTimeOrNull(R["ChgDate"]);
                        rec.AgencyName = Conversion.getStrOrNull(R["AgencyName"]);
                        rec.AgencyNameL = Conversion.getStrOrNull(R["AgencyNameL"]);
                        rec.AgencyUser = Conversion.getStrOrNull(R["AgencyUser"]);
                        rec.AgencyUserL = Conversion.getStrOrNull(R["AgencyUserL"]);
                        rec.ReadByOprUser = Conversion.getStrOrNull(R["ReadByOprUser"]);
                        rec.ReadByOprDate = Conversion.getDateTimeOrNull(R["ReadByOprDate"]);

                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public resMonitorRecord getReservation(User UserData, string ResNo, ref string errorMsg)
        {
            string tsql = string.Format(
@"
Select RM.ResNo, 
    HolPackName=(Select isnull(dbo.FindLocalName(HolPack.NameLID, @Market), HolPack.Name) From HolPack (NOLOCK) Where Code=RM.HolPack), 
    HotelName=isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Hotel (NOLOCK) Where Code = (Select Top 1 Service From ResService (NOLOCK) Where ResNo = RM.ResNo And ServiceType = 'HOTEL')), ''),
    RM.Adult, RM.Child, RM.BegDate, RM.EndDate, RM.Days, 
    PackedPrice=isnull(RM.PackedPrice, 0), OutOfPackage=(isnull(RM.SalePrice, 0)-isnull(RM.PackedPrice, 0)),
    SalePrice=isnull(RM.SalePrice, 0), RM.SaleCur, AgencyPayable=isnull(RM.AgencyPayable, 0), Discount=isnull(RM.Discount, 0), 
    EBAgency=isnull(RM.EBAgency, 0), EBPas=isnull(RM.EBPas, 0), EBAgencyPer=isnull(RM.EBAgencyPer, 0), EBPasPer=isnull(RM.EBPasPer, 0),
    AgencyCom = (isnull(RM.AgencyCom,0) + isnull(RM.AgencyComSup, 0) - isnull(RM.AgencyDisPasVal, 0)), 
    AgencyComPer=(isnull(RM.AgencyComPer, 0) + isnull(RM.AgencyComSupPer, 0) - isnull(RM.AgencyDisPasPer, 0)),
    isnull(RM.AgencyDisPasVal, 0) AgencyDisPasVal, isnull(RM.AgencyDisPasPer, 0) AgencyDisPasPer, 
    AgencyPayment=isnull(RM.AgencyPayment, 0), ResDate=(RM.ResDate+RM.ResTime), RM.ChgDate, (Case When isnull(RM.ReadByOpr, 0)=1 Then Cast('1' As bit) Else Cast('0' As bit) End) AS ReadByOpr, 
    ReadByOprUser=(Select Name From Users (NOLOCK) Where Code= RM.ReadByOprUser), 
    RM.ReadByOprDate, AgencyUser = isnull(isnull(dbo.FindLocalName(AU.NameLID, @Market), AU.Name),''), 
    RM.DepCity, DepCityName=(Select isnull(dbo.FindLocalName(Location.NameLID, @Market),Name) From Location (NOLOCK) Where RecID=RM.DepCity),
    RM.ArrCity, ArrCityNAme=(Select isnull(dbo.FindLocalName(Location.NameLID, @Market),Name) From Location (NOLOCK) Where RecID=RM.ArrCity), RM.ResNote, 
	Balance=((Case When dbo.ufn_AgencyResPayRule(RM.Agency, RM.PLMarket)=0 then IsNull(RM.AgencyPayable,0) else IsNull(RM.PasPayable,0) end) - isnull(RM.AgencyPayment, 0)), 
    SPrice=(isnull(RM.SalePrice, 0) + isnull(RM.Discount, 0) - isnull(RM.EBPas, 0) - isnull(RM.EBAgency, 0)),                             
    DueAgentPayment=Case When dbo.ufn_AgencyResPayRule(RM.Agency, RM.PLMarket)=0 then IsNull(RM.AgencyPayable,0) else IsNull(RM.PasPayable,0) end,
    (RC.Surname + ' ' + RC.Name) AS LeaderName, 
    InvoiceNo=( 
                Select Top 1 I.InvSerial+' '+Cast(I.InvNo as varchar(8)) From Invoice I (NOLOCK) 
                    JOIN InvoiceRes IR (NOLOCK) on IR.InvoiceID=I.RecID 
                Where IR.ResNo = RM.ResNo 
                ), 
    NewComment=Case When Exists(Select RecID From ResComment Where ResNo = RM.ResNo AND UserType = 'O' AND IsRead = 'N') then Cast('1' AS bit) Else Cast('0' AS bit) End,
    CommisionInvoiceNumber = '', RM.SaleResource, RM.ResStat, RM.ConfStat, RM.PaymentStat,
    RM.OptDate, RM.RefNo, Supplement=(Select Sum(isnull(Amount,0)) From ResSupDis R (NOLOCK) Where R.ResNo=RM.ResNo),
    Bonus=(
            Select Sum(isnull(BonusVal,0)) 
            From BonusDet (NOLOCK) 
            Where ResNo=RM.ResNo And 
                (exists(Select * From Bonus Where RecID=BonusDet.MasRecID and Type ='A') OR
                    exists(Select * From Bonus Where RecID=BonusDet.MasRecID and Type ='U')) And
                BonusDet.Market=@Market
            ),
    AgencyBonusAmount,UserBonusAmount,PasBonusAmount,RM.BrokerCom,RM.BrokerComPer
FROM ResMain RM (NOLOCK) 
JOIN ResCust RC (NOLOCK) ON RM.ResNo = RC.ResNo And RC.Leader = 'Y' 
Left JOIN AgencyUser AU (NOLOCK) ON AU.Code = RM.AgencyUser AND AU.Agency=RM.Agency 
JOIN Agency A (NOLOCK) ON A.Code = RM.Agency 
Where RM.ResNo=@ResNo 
", string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ? "ResDate=(RM.ResDate+RM.ResTime)" : "RM.ResDate");
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        resMonitorRecord rec = new resMonitorRecord();
                        rec.Adult = Conversion.getInt16OrNull(R["Adult"]);
                        rec.AgencyCom = Conversion.getDecimalOrNull(R["AgencyCom"]);
                        rec.AgencyComPer = Conversion.getDecimalOrNull(R["AgencyComPer"]);
                        rec.AgencyDisPasPer = Conversion.getDecimalOrNull(R["AgencyDisPasPer"]);
                        rec.AgencyDisPasVal = Conversion.getDecimalOrNull(R["AgencyDisPasVal"]);
                        rec.AgencyPayable = Conversion.getDecimalOrNull(R["AgencyPayable"]);
                        rec.AgencyPayment = Conversion.getDecimalOrNull(R["AgencyPayment"]);
                        rec.AgencyUser = Conversion.getStrOrNull(R["AgencyUser"]);
                        rec.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        rec.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        rec.ChgDate = Conversion.getDateTimeOrNull(R["ChgDate"]);
                        rec.Child = Conversion.getInt16OrNull(R["Child"]);
                        rec.CommisionInvoiceNumber = Conversion.getStrOrNull(R["CommisionInvoiceNumber"]);
                        rec.ConfStat = Conversion.getInt16OrNull(R["ConfStat"]);
                        rec.Days = Conversion.getInt16OrNull(R["Days"]);
                        rec.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        rec.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        rec.Discount = Conversion.getDecimalOrNull(R["Discount"]);
                        rec.EBAgency = Conversion.getDecimalOrNull(R["EBAgency"]);
                        rec.EBAgencyPer = Conversion.getDecimalOrNull(R["EBAgencyPer"]);
                        rec.EBPas = Conversion.getDecimalOrNull(R["EBPas"]);
                        rec.EBPasPer = Conversion.getDecimalOrNull(R["EBPasPer"]);
                        rec.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        rec.HolPackName = Conversion.getStrOrNull(R["HolPackName"]);
                        rec.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        rec.InvoiceNo = Conversion.getStrOrNull(R["InvoiceNo"]);
                        rec.LeaderName = Conversion.getStrOrNull(R["LeaderName"]);
                        rec.NewComment = (bool)R["NewComment"];
                        rec.OptDate = Conversion.getDateTimeOrNull(R["OptDate"]);
                        rec.PaymentStat = Conversion.getStrOrNull(R["PaymentStat"]);
                        bool? readByOpr = Conversion.getBoolOrNull(R["ReadByOpr"]);
                        rec.ReadByOpr = readByOpr.HasValue && readByOpr.Value ? HttpContext.GetGlobalResourceObject("LibraryResource", "Read").ToString() : "&nbsp;";
                        rec.ReadByOprDate = Conversion.getDateTimeOrNull(R["ReadByOprDate"]);
                        rec.ReadByOprUser = Conversion.getStrOrNull(R["ReadByOprUser"]);
                        rec.RefNo = (Int32)R["RefNo"];
                        rec.ResDate = Conversion.getDateTimeOrNull(R["ResDate"]);
                        rec.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        rec.ResNote = Conversion.getStrOrNull(R["ResNote"]);
                        rec.ResStat = Conversion.getInt16OrNull(R["ResStat"]);
                        rec.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        rec.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0015, VersionControl.Equality.gt))
                        {
                            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                            {
                                rec.PackedPrice = Conversion.getDecimalOrNull(R["PackedPrice"]);
                                rec.OutOfPackage = ((rec.SalePrice.HasValue ? rec.SalePrice.Value : 0) - (rec.EBPas.HasValue ? rec.EBPas.Value : 0)) -
                                                   ((rec.PackedPrice.HasValue ? rec.PackedPrice.Value : 0) - (rec.EBPas.HasValue ? rec.EBPas.Value : 0));

                            }
                            else
                            {
                                rec.PackedPrice = Conversion.getDecimalOrNull(R["PackedPrice"]);
                                rec.OutOfPackage = Conversion.getDecimalOrNull(R["OutOfPackage"]);
                            }
                        }
                        rec.SaleResource = Conversion.getInt16OrNull(R["SaleResource"]);
                        rec.SPrice = Conversion.getDecimalOrNull(R["SPrice"]);
                        rec.Supplement = Conversion.getDecimalOrNull(R["Supplement"]);
                        rec.Bonus = Conversion.getDecimalOrNull(R["Bonus"]);
                        rec.AgencyBonusAmount = Conversion.getDecimalOrNull(R["AgencyBonusAmount"]);
                        rec.UserBonusAmount = Conversion.getDecimalOrNull(R["UserBonusAmount"]);
                        rec.PasBonusAmount = Conversion.getDecimalOrNull(R["PasBonusAmount"]);
                        rec.BrokerCom = Conversion.getDecimalOrNull(R["BrokerCom"]);
                        rec.BrokerComPer = Conversion.getDecimalOrNull(R["BrokerComPer"]);

                        rec.Balance = Conversion.getDecimalOrNull(R["Balance"]);
                        rec.DueAgentPayment = Conversion.getDecimalOrNull(R["DueAgentPayment"]);

                        if (UserData.AgencyRec.AgencyType == 2)
                        {
                            rec.Balance += rec.BrokerCom.HasValue ? rec.BrokerCom.Value : (decimal?)0;
                            rec.DueAgentPayment += rec.BrokerCom.HasValue ? rec.BrokerCom.Value : (decimal?)0;
                        }
                        return rec;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public List<resMonitorRecord> getResMonitorData(User UserData, string AgencyUser, string AgencyOffice, string BegResNo, string EndResNo,
                                                        int? DepCity, int? ArrCity, DateTime? BegDateFirst, DateTime? BegDateLast, DateTime? EndDateFirst, DateTime? EndDateLast,
                                                        DateTime? ResDateFirst, DateTime? ResDateLast, string HolPack, string Surname, string Name, string ResStatusStr,
                                                        string ConfirmStatusStr, string PaymentStatusStr, string OptionControl, bool ShowDraft,
                                                        string Currency, ref string errorMsg)
        {
            List<resMonitorRecord> records = new List<resMonitorRecord>();

            string tsql = string.Empty;
            #region Main Sql
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0015, VersionControl.Equality.gt))
                tsql = string.Format(
                        @"Select * From (   
                            Select
		                        RM.ResNo, 
		                        HolPackName=HolP.Name,HolPackNameL=isnull(dbo.FindLocalName(HolP.NameLID,@Market),HolP.Name), 
		                        HotelName=H.Name,HotelNameL=isnull(dbo.FindLocalName(H.NameLID, @Market),H.Name),
                                RM.Adult, RM.Child, RM.BegDate, RM.EndDate, RM.Days, 
                                PackedPrice=isnull(RM.PackedPrice,0),OutOfPackage=(isnull(RM.SalePrice,0)-isnull(RM.PackedPrice,0)),
                                SalePrice=isnull(RM.SalePrice,0),RM.SaleCur,AgencyPayable=isnull(RM.AgencyPayable,0),Discount=isnull(RM.Discount,0), 
                                EBAgency=isnull(RM.EBAgency,0),EBPas=isnull(RM.EBPas,0),EBAgencyPer=isnull(RM.EBAgencyPer,0),EBPasPer=isnull(RM.EBPasPer,0),
                                AgencyCom=(isnull(RM.AgencyCom,0)+isnull(RM.AgencyComSup,0)-isnull(RM.AgencyDisPasVal,0)), 
                                AgencyComPer=(isnull(RM.AgencyComPer,0)+isnull(RM.AgencyComSupPer,0)-isnull(RM.AgencyDisPasPer,0)),
                                AgencyDisPasVal=isnull(RM.AgencyDisPasVal,0),AgencyDisPasPer=isnull(RM.AgencyDisPasPer,0), 
                                AgencyPayment=isnull(RM.AgencyPayment,0),{0},RM.ChgDate,(Case When isnull(RM.ReadByOpr,0)=1 Then Cast('1' As Bit) Else Cast('0' As Bit) End) AS ReadByOpr, 
		                        ReadByOprUser=(Select Name From Users (NOLOCK) Where Code= RM.ReadByOprUser), 
                                ReadByOprUserL=(Select isnull(dbo.FindLocalName(NameLID,@Market),Name) From Users (NOLOCK) Where Code= RM.ReadByOprUser), 
                                RM.ReadByOprDate, 
		                        AgencyUser=isnull(AU.Name,''),AgencyUserL=isnull(isnull(dbo.FindLocalName(AU.NameLID,@Market),AU.Name),''), 
                                RM.DepCity, 
                                DepCityName=(Select Name From Location (NOLOCK) Where RecID=RM.DepCity),
                                DepCityNameL=(Select isnull(dbo.FindLocalName(Location.NameLID, @Market),Name) From Location (NOLOCK) Where RecID=RM.DepCity),
                                RM.ArrCity, 
                                ArrCityName=(Select Name From Location (NOLOCK) Where RecID=RM.ArrCity),
                                ArrCityNameL=(Select isnull(dbo.FindLocalName(Location.NameLID, @Market),Name) From Location (NOLOCK) Where RecID=RM.ArrCity),
                                RM.ResNote, ((Case When dbo.ufn_AgencyResPayRule(RM.Agency, RM.PLMarket)=0 then IsNull(RM.AgencyPayable,0) else IsNull(RM.PasPayable,0) end) - isnull(RM.AgencyPayment, 0)) AS Balance, 
                                RM.ConfToAgency, RM.ConfToAgencyDate,
                                (isnull(RM.SalePrice, 0) + isnull(RM.Discount, 0) - isnull(RM.EBPas, 0) - isnull(RM.EBAgency, 0)) As SPrice,                             
                                DueAgentPayment = Case When dbo.ufn_AgencyResPayRule(RM.Agency, RM.PLMarket)=0 then IsNull(RM.AgencyPayable,0) else IsNull(RM.PasPayable,0) end,
                                (RC.Surname + ' ' + RC.Name) AS LeaderName, 
                                InvoiceNo=( 
                                            Select Top 1 I.InvSerial+' '+Cast(I.InvNo as varchar(8)) From Invoice I (NOLOCK) 
                                                JOIN InvoiceRes IR (NOLOCK) on IR.InvoiceID=I.RecID 
                                            Where IR.ResNo = RM.ResNo 
                                           ), 
                                NewComment=Case When Exists(Select RecID From ResComment Where ResNo = RM.ResNo AND UserType = 'O' AND IsRead = 'N') then Cast('1' AS bit) Else Cast('0' AS bit) End,
                                CommisionInvoiceNumber = '', RM.SaleResource, RM.ResStat, RM.ConfStat, RM.PaymentStat,
                                RM.OptDate, RM.RefNo, Supplement=(Select Sum(isnull(Amount,0)) From ResSupDis R (NOLOCK) Where R.ResNo=RM.ResNo),
                                Bonus=(
                                        Select Sum(isnull(BonusVal,0)) 
                                        From BonusDet (NOLOCK) 
                                        Where ResNo=RM.ResNo And 
                                            (--exists(Select * From Bonus Where RecID=BonusDet.MasRecID and Type ='A') OR
                                             exists(Select * From Bonus Where RecID=BonusDet.MasRecID and Type ='U')) And
                                            BonusDet.Market=@Market
                                      ),
                                HotConfStat=(Select Top 1 StatConf From ResService (NOLOCK) Where ResNo=RM.ResNo And ServiceType='HOTEL' And StatConf<=1 And StatSer<=1)
                            FROM ResMain RM (NOLOCK) 
                            JOIN ResCust RC (NOLOCK) ON RM.ResNo = RC.ResNo And RC.Leader = 'Y' 
                            Left JOIN AgencyUser AU (NOLOCK) ON AU.Code = RM.AgencyUser AND AU.Agency=RM.Agency 
                            JOIN Agency A (NOLOCK) ON A.Code = RM.Agency 
                            LEFT JOIN HolPack HolP (NOLOCK) ON HolP.Code=RM.Holpack
                            LEFT JOIN Hotel H (NOLOCK) ON H.Code=(Select Top 1 Service From ResService (NOLOCK) Where ResNo=RM.ResNo And ServiceType='HOTEL' And StatConf<=1 And StatSer<=1)
                            Where ", string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ? "ResDate=(RM.ResDate+RM.ResTime)" : "RM.ResDate");
            else
                tsql = string.Format(
                        @"Select * From (   
                            Select RM.ResNo, 
                                HolPackName=(Select isnull(dbo.FindLocalName(HolPack.NameLID, @Market), HolPack.Name) From HolPack (NOLOCK) Where Code=RM.HolPack), 
                                HotelName=isnull((Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Hotel (NOLOCK) Where Code = (Select Top 1 Service From ResService (NOLOCK) Where ResNo = RM.ResNo And ServiceType = 'HOTEL')), ''),
                                RM.Adult, RM.Child, RM.BegDate, RM.EndDate, RM.Days, 
                                PackedPrice=null, OutOfPackage=null,
                                SalePrice=isnull(RM.SalePrice, 0), RM.SaleCur, AgencyPayable=isnull(RM.AgencyPayable, 0), Discount=isnull(RM.Discount, 0), 
                                EBAgency=isnull(RM.EBAgency, 0), EBPas=isnull(RM.EBPas, 0), EBAgencyPer=isnull(RM.EBAgencyPer, 0), EBPasPer=isnull(RM.EBPasPer, 0),
                                AgencyCom = (isnull(RM.AgencyCom,0) + isnull(RM.AgencyComSup, 0) - isnull(RM.AgencyDisPasVal, 0)), 
                                AgencyComPer=(isnull(RM.AgencyComPer, 0) + isnull(RM.AgencyComSupPer, 0) - isnull(RM.AgencyDisPasPer, 0)),
                                isnull(RM.AgencyDisPasVal, 0) AgencyDisPasVal, isnull(RM.AgencyDisPasPer, 0) AgencyDisPasPer, 
                                AgencyPayment=isnull(RM.AgencyPayment, 0), {0}, RM.ChgDate, isnull(RM.ReadByOpr, 0) AS ReadByOpr, 
                                ReadByOprUser=(Select Name From Users (NOLOCK) Where Code= RM.ReadByOprUser), 
                                RM.ReadByOprDate, AgencyUser = isnull(isnull(dbo.FindLocalName(AU.NameLID, @Market), AU.Name),''), 
                                RM.DepCity, DepCityName=(Select isnull(dbo.FindLocalName(Location.NameLID, @Market),Name) From Location (NOLOCK) Where RecID=RM.DepCity),
                                RM.ArrCity, ArrCityNAme=(Select isnull(dbo.FindLocalName(Location.NameLID, @Market),Name) From Location (NOLOCK) Where RecID=RM.ArrCity),
                                RM.ResNote, ((Case When dbo.ufn_AgencyResPayRule(RM.Agency, RM.PLMarket)=0 then IsNull(RM.AgencyPayable,0) else IsNull(RM.PasPayable,0) end) - isnull(RM.AgencyPayment, 0)) AS Balance, 
                                RM.ConfToAgency, RM.ConfToAgencyDate,
                                (isnull(RM.SalePrice, 0) + isnull(RM.Discount, 0) - isnull(RM.EBPas, 0) - isnull(RM.EBAgency, 0)) As SPrice,                             
                                DueAgentPayment = Case When dbo.ufn_AgencyResPayRule(RM.Agency, RM.PLMarket)=0 then IsNull(RM.AgencyPayable,0) else IsNull(RM.PasPayable,0) end,
                                (RC.Surname + ' ' + RC.Name) AS LeaderName, 
                                InvoiceNo=( 
                                            Select Top 1 I.InvSerial+' '+Cast(I.InvNo as varchar(8)) From Invoice I (NOLOCK) 
                                                JOIN InvoiceRes IR (NOLOCK) on IR.InvoiceID=I.RecID 
                                            Where IR.ResNo = RM.ResNo 
                                           ), 
                                NewComment=Case When Exists(Select RecID From ResComment Where ResNo = RM.ResNo AND UserType = 'O' AND IsRead = 'N') then Cast('1' AS bit) Else Cast('0' AS bit) End,
                                CommisionInvoiceNumber = '', RM.SaleResource, RM.ResStat, RM.ConfStat, RM.PaymentStat,
                                RM.OptDate, RM.RefNo, Supplement=(Select Sum(isnull(Amount,0)) From ResSupDis R (NOLOCK) Where R.ResNo=RM.ResNo),
                                Bonus=(
                                        Select Sum(isnull(BonusVal,0)) 
                                        From BonusDet (NOLOCK) 
                                        Where ResNo=RM.ResNo And 
                                            (--exists(Select * From Bonus Where RecID=BonusDet.MasRecID and Type ='A') OR
                                             exists(Select * From Bonus Where RecID=BonusDet.MasRecID and Type ='U')) And
                                            BonusDet.Market=@Market
                                      ),
                                HotConfStat=(Select Top 1 StatConf From ResService (NOLOCK) Where ResNo=RM.ResNo And ServiceType='HOTEL' And StatConf<=1 And StatSer<=1)
                            FROM ResMain RM (NOLOCK) 
                            JOIN ResCust RC (NOLOCK) ON RM.ResNo = RC.ResNo And RC.Leader = 'Y' 
                            Left JOIN AgencyUser AU (NOLOCK) ON AU.Code = RM.AgencyUser AND AU.Agency=RM.Agency 
                            JOIN Agency A (NOLOCK) ON A.Code = RM.Agency 
                            Where ", string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) ? "ResDate=(RM.ResDate+RM.ResTime)" : "RM.ResDate");
            #endregion Main Sql

            #region Where

            //if (string.IsNullOrEmpty(AgencyOffice) && UserData.ShowAllRes)
            tsql += @" (RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@Agency Or MainOffice=@Agency) ";
            //else tsql += @" (RM.Agency=@Agency ";

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && !string.Equals(UserData.MainOffice, UserData.AgencyID))
                tsql += @"  Or RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@MainOffice Or MainOffice=@MainOffice))";
            else
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) && UserData.ShowAllRes && !string.Equals(UserData.MainOffice, UserData.AgencyID))
                tsql += @" Or RM.Agency in (Select Code From Agency (NOLOCK) Where Code=@MainOffice Or MainOffice=@MainOffice))";
            else
                tsql += ") ";

            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && !UserData.ShowAllRes)
            {
                if (string.IsNullOrEmpty(AgencyUser))
                    tsql += @"  And (isnull(RM.AgencyUser, '') = '') ";
                else
                    tsql += string.Format(@"  And (RM.AgencyUser = '{0}' Or isnull(RM.AgencyUser, '') = '') ", AgencyUser);
            }
            else
            {
                if (!string.IsNullOrEmpty(AgencyUser))
                    tsql += string.Format(@"  And RM.AgencyUser = '{0}' ", AgencyUser);
            }
            tsql += @"  And RC.Leader = 'Y' ";
            //if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex)) {
            //    if (ShowDraft)
            //        tsql += @" And RM.ResStat=9  ";
            //} else {
            if (!ShowDraft)
                tsql += @" And RM.ResStat <> 9  ";
            else
                tsql += @" And RM.ResStat = 9  ";
            //}
            tsql += " And RM.SaleCur is not null ";

            if (!string.IsNullOrEmpty(Currency))
                tsql += string.IsNullOrEmpty(Currency) ? string.Empty : string.Format(" And RM.SaleCur='{0}' ", Currency);

            if (string.IsNullOrEmpty(EndResNo))
            {
                tsql += string.IsNullOrEmpty(BegResNo) ? string.Empty : string.Format(" And RM.ResNo='{0}' ", BegResNo);
            }
            else
                if (string.IsNullOrEmpty(BegResNo))
            {
                tsql += string.IsNullOrEmpty(EndResNo) ? string.Empty : string.Format(" And RM.ResNo='{0}' ", EndResNo);
            }
            else
            {
                tsql += string.IsNullOrEmpty(BegResNo) ? string.Empty : string.Format(" And RM.ResNo>='{0}' ", BegResNo);
                tsql += string.IsNullOrEmpty(EndResNo) ? string.Empty : string.Format(" And RM.ResNo<='{0}' ", EndResNo);
            }
            tsql += ArrCity.HasValue && ArrCity.Value > -1 ? string.Format(" And RM.ArrCity={0} ", ArrCity) : string.Empty;
            tsql += DepCity.HasValue && DepCity.Value > -1 ? string.Format(" And RM.DepCity={0} ", DepCity) : string.Empty;
            tsql += BegDateFirst.HasValue && BegDateFirst.Value > DateTime.MinValue ? " And RM.BegDate >= @BegDate1 " : string.Empty;
            tsql += BegDateLast.HasValue && BegDateLast.Value > DateTime.MinValue ? " And RM.BegDate <= @BegDate2 " : string.Empty;
            tsql += EndDateFirst.HasValue && EndDateFirst.Value > DateTime.MinValue ? " And RM.EndDate >= @EndDate1 " : string.Empty;
            tsql += EndDateLast.HasValue && EndDateLast.Value > DateTime.MinValue ? " And RM.EndDate <= @EndDate2 " : string.Empty;
            tsql += ResDateFirst.HasValue && ResDateFirst.Value > DateTime.MinValue ? " And RM.ResDate >= @ResDate1 " : string.Empty;
            tsql += ResDateLast.HasValue && ResDateLast.Value > DateTime.MinValue ? " And RM.ResDate <= @ResDate2 " : string.Empty;
            if (UserData.Aggregator && !string.IsNullOrEmpty(UserData.MerlinxID) && UserData.MainAgency)
                tsql += " Or isnull(RM.MerlinxID, '') <> '' ";

            if (!ShowDraft)
            {
                if (ResStatusStr != "")
                    tsql += " And RM.ResStat in(" + ResStatusStr.Replace(';', ',') + ") ";
                if (ConfirmStatusStr != "")
                    tsql += " And RM.ConfStat in (" + ConfirmStatusStr.Replace(';', ',') + ") ";
                if (PaymentStatusStr != "")
                    tsql += " And RM.PaymentStat in ('" + PaymentStatusStr.Replace(";", "','") + "') ";
                if (OptionControl == "1")
                    tsql += " And RM.OptDate is not null ";
            }

            tsql += string.IsNullOrEmpty(HolPack) ? string.Empty : string.Format(" And RM.HolPack = '{0}'", HolPack);
            tsql += string.IsNullOrEmpty(Surname) ? string.Empty : string.Format(" And RC.Surname Like '{0}'", Surname);
            tsql += string.IsNullOrEmpty(Name) ? string.Empty : string.Format(" And RC.Name Like '{0}'", Name);

            tsql += string.IsNullOrEmpty(AgencyOffice) ? "" : string.Format(" And A.Code = '{0}' ", AgencyOffice);

            #endregion Where

            tsql += ") X --Order By NewComment Desc, ResDate Desc, RefNo Desc";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, string.IsNullOrEmpty(AgencyOffice) ? UserData.AgencyID : AgencyOffice);
                db.AddInParameter(dbCommand, "MainOffice", DbType.String, string.IsNullOrEmpty(AgencyOffice) ? SqlString.Null : AgencyOffice);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                if (BegDateFirst.HasValue && BegDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "BegDate1", DbType.DateTime, BegDateFirst.Value);
                if (BegDateLast.HasValue && BegDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "BegDate2", DbType.DateTime, BegDateLast.Value);
                if (EndDateFirst.HasValue && EndDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "EndDate1", DbType.DateTime, EndDateFirst.Value);
                if (EndDateLast.HasValue && EndDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "EndDate2", DbType.DateTime, EndDateLast.Value);
                if (ResDateFirst.HasValue && ResDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "ResDate1", DbType.DateTime, ResDateFirst.Value);
                if (ResDateLast.HasValue && ResDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "ResDate2", DbType.DateTime, ResDateLast.Value);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        resMonitorRecord rec = new resMonitorRecord();
                        rec.Adult = Conversion.getInt16OrNull(R["Adult"]);
                        rec.AgencyCom = Conversion.getDecimalOrNull(R["AgencyCom"]);
                        rec.AgencyComPer = Conversion.getDecimalOrNull(R["AgencyComPer"]);
                        rec.AgencyDisPasPer = Conversion.getDecimalOrNull(R["AgencyDisPasPer"]);
                        rec.AgencyDisPasVal = Conversion.getDecimalOrNull(R["AgencyDisPasVal"]);
                        rec.AgencyPayable = Conversion.getDecimalOrNull(R["AgencyPayable"]);
                        rec.AgencyPayment = Conversion.getDecimalOrNull(R["AgencyPayment"]);
                        rec.AgencyUser = Conversion.getStrOrNull(R["AgencyUser"]);
                        rec.AgencyUserL = Conversion.getStrOrNull(R["AgencyUserL"]);
                        rec.ConfToAgency = Conversion.getStrOrNull(R["ConfToAgency"]);
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                            rec.ConfToAgencyDate = string.Equals(rec.ConfToAgency, "Y") ? Conversion.getDateTimeOrNull(R["ConfToAgencyDate"]) : null;
                        else
                            rec.ConfToAgencyDate = Conversion.getDateTimeOrNull(R["ConfToAgencyDate"]);
                        rec.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        rec.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        rec.ArrCityNameL = Conversion.getStrOrNull(R["ArrCityNameL"]);
                        rec.Balance = Conversion.getDecimalOrNull(R["Balance"]);
                        rec.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        rec.ChgDate = Conversion.getDateTimeOrNull(R["ChgDate"]);
                        rec.Child = Conversion.getInt16OrNull(R["Child"]);
                        rec.CommisionInvoiceNumber = Conversion.getStrOrNull(R["CommisionInvoiceNumber"]);
                        rec.ConfStat = Conversion.getInt16OrNull(R["ConfStat"]);
                        rec.Days = Conversion.getInt16OrNull(R["Days"]);
                        rec.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        rec.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        rec.DepCityNameL = Conversion.getStrOrNull(R["DepCityNameL"]);
                        rec.Discount = Conversion.getDecimalOrNull(R["Discount"]);
                        rec.DueAgentPayment = Conversion.getDecimalOrNull(R["DueAgentPayment"]);
                        rec.EBAgency = Conversion.getDecimalOrNull(R["EBAgency"]);
                        rec.EBAgencyPer = Conversion.getDecimalOrNull(R["EBAgencyPer"]);
                        rec.EBPas = Conversion.getDecimalOrNull(R["EBPas"]);
                        rec.EBPasPer = Conversion.getDecimalOrNull(R["EBPasPer"]);
                        rec.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        rec.HolPackName = Conversion.getStrOrNull(R["HolPackName"]);
                        rec.HolPackNameL = Conversion.getStrOrNull(R["HolPackNameL"]);
                        rec.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        rec.HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]);
                        rec.InvoiceNo = Conversion.getStrOrNull(R["InvoiceNo"]);
                        rec.LeaderName = Conversion.getStrOrNull(R["LeaderName"]);
                        rec.NewComment = (bool)R["NewComment"];
                        rec.OptDate = Conversion.getDateTimeOrNull(R["OptDate"]);
                        rec.PaymentStat = Conversion.getStrOrNull(R["PaymentStat"]);
                        bool? readByOpr = Conversion.getBoolOrNull(R["ReadByOpr"]);
                        rec.ReadByOpr = readByOpr.HasValue && readByOpr.Value ? HttpContext.GetGlobalResourceObject("LibraryResource", "Read").ToString() : "&nbsp;";
                        rec.ReadByOprDate = Conversion.getDateTimeOrNull(R["ReadByOprDate"]);
                        rec.ReadByOprUser = Conversion.getStrOrNull(R["ReadByOprUser"]);
                        rec.ReadByOprUserL = Conversion.getStrOrNull(R["ReadByOprUserL"]);
                        rec.RefNo = (Int32)R["RefNo"];
                        rec.ResDate = Conversion.getDateTimeOrNull(R["ResDate"]);
                        rec.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        rec.ResNote = Conversion.getStrOrNull(R["ResNote"]);
                        rec.ResStat = Conversion.getInt16OrNull(R["ResStat"]);
                        rec.HotConfStat = Conversion.getInt16OrNull(R["HotConfStat"]);
                        rec.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        rec.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0015, VersionControl.Equality.gt))
                        {
                            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                            {
                                rec.PackedPrice = Conversion.getDecimalOrNull(R["PackedPrice"]);
                                rec.OutOfPackage = ((rec.SalePrice.HasValue ? rec.SalePrice.Value : 0) - (rec.EBPas.HasValue ? rec.EBPas.Value : 0)) -
                                                   ((rec.PackedPrice.HasValue ? rec.PackedPrice.Value : 0) - (rec.EBPas.HasValue ? rec.EBPas.Value : 0));

                            }
                            else
                            {
                                rec.PackedPrice = Conversion.getDecimalOrNull(R["PackedPrice"]);
                                rec.OutOfPackage = Conversion.getDecimalOrNull(R["OutOfPackage"]);
                            }
                        }
                        rec.SaleResource = Conversion.getInt16OrNull(R["SaleResource"]);
                        rec.SPrice = Conversion.getDecimalOrNull(R["SPrice"]);
                        rec.Supplement = Conversion.getDecimalOrNull(R["Supplement"]);
                        rec.Bonus = Conversion.getDecimalOrNull(R["Bonus"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<resCommentRecord> getComment(string ResNo)
        {
            List<resCommentRecord> records = new List<resCommentRecord>();
            string tsql = string.Format(@"  Select RC.RecID, RC.UserType, RC.Comment, RC.CDate, RC.isRead, RC.RDate,RC.Agency,
                                                UserIDName=(Case When RC.UserType = 'O' 
                                                            Then 
                                                                (Select Name From Users (NOLOCK) Where Code=RC.UserID) 
                                                            Else 
            				Case when rc.Agency is NULL
            				Then
            					(Select Name From AgencyUser (NOLOCK) Where Code=RC.UserID AND Agency=RM.Agency)
            				Else
            					(Select Name From AgencyUser (NOLOCK) Where Code=RC.UserID AND Agency=RC.Agency)
            				End
            			End),


                                                RUserIDName=(Case When RC.UserType = 'A' 
                                                            Then 
                                                                (Select Name From Users (NOLOCK) Where Code=RC.RUserID) 
                                                            Else 
            				Case when rc.Agency is NULL
            				Then
            					(Select Name From AgencyUser (NOLOCK) Where Code=RC.RUserID AND Agency=RM.Agency)
            				Else
            					(Select Name From AgencyUser (NOLOCK) Where Code=RC.RUserID AND Agency=RC.Agency)
            				End
            			End),
                                                RM.ResNo
                                            From ResComment RC (NOLOCK) 
                                                 JOIN ResMain RM (NOLOCK) ON RM.ResNo = RC.ResNo 
                                            Where RC.ResNo = '{0}'
                                            Order By RC.CDate Desc", ResNo);
           
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        resCommentRecord rec = new resCommentRecord();
                        rec.RecID = Conversion.getInt32OrNull(rdr["RecID"]);
                        rec.ResNo = Conversion.getStrOrNull(rdr["ResNo"]);
                        rec.UserType = Conversion.getStrOrNull(rdr["UserType"]);
                        rec.Comment = Conversion.getStrOrNull(rdr["Comment"]);
                        rec.CDate = Conversion.getDateTimeOrNull(rdr["CDate"]);
                        rec.IsRead = Equals(rdr["IsRead"], "Y");
                        rec.RDate = Conversion.getDateTimeOrNull(rdr["RDate"]);
                        rec.UserIDName = Conversion.getStrOrNull(rdr["UserIDName"]);
                        rec.RUserIDName = Conversion.getStrOrNull(rdr["RUserIDName"]);
                        rec.Agency = Conversion.getStrOrNull(rdr["Agency"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                string err = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getCommentDataString(string ResNo, string UserID, int ciID)
        {
            CultureInfo ci = new CultureInfo(ciID);

            List<resCommentRecord> listComment = new List<resCommentRecord>();
            StringBuilder sb = new StringBuilder();
            listComment = getComment(ResNo);
            sb.AppendFormat("<input id=\"commnetCount\" type=\"hidden\" value=\"{0}\">", listComment.Where(w => !w.IsRead && w.UserType == "O").Count());
            sb.Append("<table class=\"gridComment\">");
            sb.Append("<thead>");
            sb.Append("<tr class=\"ui-widget-header\">");
            sb.Append("<th class=\"tdUserName\"><b>" + HttpContext.GetGlobalResourceObject("Comment", "lblUserName", ci).ToString() + "</b></th>");
            sb.Append("<th class=\"tdType\"><b>" + HttpContext.GetGlobalResourceObject("Comment", "lblType", ci).ToString() + "</b></th>");
            sb.Append("<th class=\"tdComment\"><b>" + HttpContext.GetGlobalResourceObject("Comment", "lblComment", ci).ToString() + "</b></th>");
            sb.Append("<th class=\"tdCommentDate\"><b>" + HttpContext.GetGlobalResourceObject("Comment", "lblCommentDate", ci).ToString() + "</b></th>");
            //sb.Append("<th class=\"tdIsRead\"><b>" + HttpContext.GetGlobalResourceObject("Comment", "lblIsRead", ci).ToString() + "</b></th>");
            sb.Append("<th class=\"tdReadUser\"><b>" + HttpContext.GetGlobalResourceObject("Comment", "lblReadUser", ci).ToString() + "</b></th>");
            sb.Append("<th class=\"tdReadDate\"><b>" + HttpContext.GetGlobalResourceObject("Comment", "lblReadDate", ci).ToString() + "</b></th>");
            sb.Append("<th class=\"tdRead\"><b>#</b></th>");
            sb.Append("</thead>");
            sb.Append("</tr>");
            sb.Append("<tbody>");
            foreach (resCommentRecord row in listComment)
            {
                sb.Append("<tr>");
                sb.AppendFormat("<td valign=\"top\">{0}</td>", row.UserIDName);
                sb.AppendFormat("<td valign=\"top\">{0}</td>", row.UserType);
                sb.AppendFormat("<td valign=\"top\">{0}</td>", row.Comment.ToString().Replace("\r\n", "<br />"));
                sb.AppendFormat("<td valign=\"top\">{0}</td>", row.CDate.HasValue ? row.CDate.Value.ToShortDateString() : "&nbsp;");
                //sb.AppendFormat("<td valign=\"top\">{0}</td>", row.IsRead ? HttpContext.GetGlobalResourceObject("LibraryResource", "btnYes", ci).ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "btnNo", ci).ToString());
                sb.AppendFormat("<td valign=\"top\">{0}</td>", row.RUserIDName);
                sb.AppendFormat("<td valign=\"top\">{0}</td>", row.RDate.HasValue ? row.RDate.Value.ToShortDateString() +" "+ row.RDate.Value.ToShortTimeString() : "&nbsp;");
                string readStr = "<span style=\"cursor: hand;\" onclick=\"readComment('" + ResNo + "','" + UserID + "','" + row.RecID.Value + "','" + ciID.ToString() + "')\">" + HttpContext.GetGlobalResourceObject("LibraryResource", "Read", ci).ToString() + "</span>";
                sb.AppendFormat("<td valign=\"top\">{0}</td>", (!row.IsRead && row.UserType == "O") ? readStr : "&nbsp;");
                sb.Append("</tr>");
            }
            sb.Append("</tbody>");
            sb.Append("</table>");
            return listComment.Count > 0 ? sb.ToString() : "";
        }

        public bool insertComment(User UserData, string ResNo, string Comment, string Agency, ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);

            string UserID = UserData.UserID;
            string tsql = @"
                            INSERT INTO ResComment (ResNo, UserID, UserType, Comment, CDate, isRead, RUserID, RDate,Agency) 
                            VALUES (@ResNo, @UserID, 'A', @Comment, GetDate(), 'N', null, null,@Agency) 
                           ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "UserID", DbType.String, UserID);
                db.AddInParameter(dbCommand, "Comment", DbType.String, Comment);
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string setComment(User UserData, string ResNo, string Comment, int lcID)
        {
            string errorMsg = string.Empty;
            if (!insertComment(UserData, ResNo, Comment, UserData.AgencyID, ref errorMsg))
            {
                return errorMsg;
            }
            else
                return getCommentDataString(ResNo, UserData.UserID, lcID);
        }

        public bool readCommentUpdate(string UserID, int RecordID)
        {
            string tsql = @"
                            UPDATE ResComment 
                            SET isRead='Y', 
                                RUserID=@RUserID, 
                                RDate=GetDate() 
                            WHERE RecID=@RecID 
                              And UserType='O' 
                              And isRead<>'Y'
                           ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "RUserID", DbType.String, UserID);
                db.AddInParameter(dbCommand, "RecID", DbType.Int32, RecordID);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                string err = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool readCommentAllRecord(string UserID, string ResNo)
        {
            string tsql = @"UPDATE ResComment 
                            SET isRead='Y', 
                                RUserID=@RUserID, 
                                RDate=GetDate() 
                            WHERE ResNo=@ResNo 
                              And UserType='O' 
                              And isRead<>'Y'
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "RUserID", DbType.String, UserID);
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                string err = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string setReadComment(string UserID, string ResNo, int RecID, int lcID)
        {
            bool saved = false;
            if (RecID > 0)
                saved = readCommentUpdate(UserID, RecID);
            else
                saved = readCommentAllRecord(UserID, ResNo);

            return getCommentDataString(ResNo, UserID, lcID);
        }

        public List<resPayPlanRecord> getPaymentPlan(string ResNo, ref string errorMsg)
        {
            List<resPayPlanRecord> records = new List<resPayPlanRecord>();
            string tsql = @"Select RecID,ResNo,PayNo,DueDate,Amount,Cur,isEB,Status,PlanType,Adult,Child,Infant,
                                 Balance=dbo.ufn_GetResAmountForPay(@ResNo)
                            From ResPayPlan (NOLOCK)
                            Where ResNo=@ResNo
                            Order By DueDate";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        resPayPlanRecord rec = new resPayPlanRecord();
                        rec.RecID = Conversion.getInt32OrNull(rdr["RecID"]);
                        rec.ResNo = Conversion.getStrOrNull(rdr["ResNo"]);
                        rec.PayNo = (Int16)rdr["PayNo"];
                        rec.DueDate = (DateTime)rdr["DueDate"];
                        rec.Cur = Conversion.getStrOrNull(rdr["Cur"]);
                        rec.Amount = Conversion.getDecimalOrNull(rdr["Amount"]);
                        rec.isEB = Conversion.getBoolOrNull(rdr["isEB"]);
                        rec.Status = Conversion.getByteOrNull(rdr["Status"]);
                        rec.PlanType = Conversion.getByteOrNull(rdr["PlanType"]);
                        rec.Adult = Conversion.getDecimalOrNull(rdr["Adult"]);
                        rec.Child = Conversion.getDecimalOrNull(rdr["Child"]);
                        rec.Infant = Conversion.getDecimalOrNull(rdr["Infant"]);
                        rec.Balance = Conversion.getDecimalOrNull(rdr["Balance"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getPaymentPlanHTML(string ResNo, int lcID)
        {
            CultureInfo ci = new CultureInfo(lcID);
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            string errorMsg = string.Empty;
            List<resPayPlanRecord> list = new List<resPayPlanRecord>();
            list = getPaymentPlan(ResNo, ref errorMsg);
            if (list != null && list.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<div width=\"300px;\">");
                sb.Append("<table style=\"font-size: 8pt;\" width=\"100%\">");
                sb.Append(" <tr>");
                sb.Append("     <td align=\"center\"><b>" + HttpContext.GetGlobalResourceObject("LibraryResource", "DueDate_Date", ci).ToString() + "</b></td>");
                sb.Append("     <td style=\"width:100px; text-align:right;\"><b>" + HttpContext.GetGlobalResourceObject("LibraryResource", "DueDate_Amount").ToString() + "</b></td>");
                sb.Append("     <td style=\"width:75px; text-align:right;\"><b>" + HttpContext.GetGlobalResourceObject("LibraryResource", "DueDate_Cur").ToString() + "</b></td>");
                sb.Append(" </tr>");
                foreach (resPayPlanRecord row in list)
                {
                    sb.Append(" <tr>");
                    sb.AppendFormat("  <td>{0}</td>", row.DueDate.ToShortDateString());
                    sb.AppendFormat("  <td align=\"right\">{0}</td>", row.Amount.HasValue ? row.Amount.Value.ToString("#,###.00") : "&nbsp;");
                    sb.AppendFormat("  <td align=\"right\">{0}</td>", row.Cur);
                    sb.Append(" </tr>");
                }
                sb.Append("</table>");
                sb.Append("</div>");
                return sb.ToString();
            }
            else
                return HttpContext.GetGlobalResourceObject("LibraryResource", "DueDate_NoRecord", ci).ToString();
        }

        public List<listString> getAgencyOfficesPayment(List<paymentMonitorDefaultRecord> data)
        {
            var Result = from rst in data.AsEnumerable()
                         group rst by new { Code = rst.Agency.ToUpper(), Name = rst.AgencyName } into r
                         select new listString { Code = r.Key.Code, Name = r.Key.Name };
            return Result.ToList<listString>();
        }

        public List<listString> getAgencyUserPayment(List<paymentMonitorDefaultRecord> data)
        {
            var Result = from rst in data.AsEnumerable()
                         group rst by new { Code = rst.AgencyUser.ToUpper(), Name = rst.AgencyUserName } into r
                         select new listString { Code = r.Key.Code, Name = r.Key.Name };
            return Result.ToList<listString>();
        }

        public List<listString> getArrCityPayment(List<paymentMonitorDefaultRecord> data)
        {
            var Result = from rst in data.AsEnumerable()
                         group rst by new { Code = rst.ArrCity, Name = rst.ArrCityName } into r
                         select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
            return Result.ToList<listString>();
        }

        public List<listString> getDepCityPayment(List<paymentMonitorDefaultRecord> data)
        {
            var Result = from rst in data.AsEnumerable()
                         group rst by new { Code = rst.DepCity, Name = rst.DepCityName } into r
                         select new listString { Code = r.Key.Code.ToString(), Name = r.Key.Name };
            return Result.ToList<listString>();
        }

        public List<paymentMonitorDefaultRecord> getPaymentMonDefaultData(User UserData, ref string errorMsg)
        {
            List<paymentMonitorDefaultRecord> records = new List<paymentMonitorDefaultRecord>();

            string tsql = @"    Declare @MainAgency VarChar(10), @Market VarChar(10)
                                Select @MainAgency=Case When AgencyType = 0 Then @Agency Else MainOffice End,
	                                @Market=O.Market
                                From Agency A (NOLOCK) 
                                Join Office O (NOLOCK) ON O.Code=A.OprOffice
                                Where A.Code = @Agency

                                SELECT Distinct RM.Agency, 
		                                AgencyName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Agency (NOLOCK) Where Code = RM.Agency),
		                                isnull(RM.AgencyUser, '') AgencyUser, 
		                                AgencyUserName=isnull((Select Top 1 isnull(isnull(dbo.FindLocalName(NameLID, @Market), [Name]), '') From AgencyUser (NOLOCK) Where Code = RM.AgencyUser And Agency = RM.Agency), '') ,		                                
                                        RM.DepCity, 
                                        DepCityName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = RM.DepCity),
                                        RM.ArrCity,
                                        ArrCityName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = RM.ArrCity)        
                                FROM ResMain RM (NOLOCK)
                                Where 
	                                (@MainAgency = (Select Agency = case when AgencyType = 0 then RM.Agency else MainOffice end From Agency where Code = RM.Agency)                                    
                        ";
            if (UserData.Aggregator && !string.IsNullOrEmpty(UserData.MerlinxID) && UserData.MainAgency)
                tsql += " Or isnull(RM.MerlinxID, '') <> '' ";

            if (!Equals(UserData.CustomRegID, "0860901"))
                if (string.IsNullOrEmpty(UserData.UserID))
                    tsql += " And @ShowAllRes = 'Y' ";
                else
                    tsql += string.Format(" And (@ShowAllRes = 'Y' (AgencyUser = '{0}' and RM.Agency=@Agency)) ", UserData.UserID);

            if (Equals(UserData.CustomRegID, "0860901") && UserData.ShowAllRes && !string.Equals(UserData.MainOffice, UserData.AgencyID))
                tsql += " Or RM.Agency in (Select Code From Agency (NOLOCK) Where Code='" + UserData.MainOffice + "' OR MainOffice='" + UserData.MainOffice + "'))";
            else
                tsql += ")";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Agency", System.Data.DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "ShowAllRes", System.Data.DbType.String, UserData.ShowAllRes ? "Y" : "N");
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        paymentMonitorDefaultRecord rec = new paymentMonitorDefaultRecord();
                        rec.Agency = Conversion.getStrOrNull(oReader["Agency"]);
                        rec.AgencyName = Conversion.getStrOrNull(oReader["AgencyName"]);
                        rec.AgencyUser = Conversion.getStrOrNull(oReader["AgencyUser"]);
                        rec.AgencyUserName = Conversion.getStrOrNull(oReader["AgencyUserName"]);
                        rec.ArrCity = (int)oReader["ArrCity"];
                        rec.ArrCityName = Conversion.getStrOrNull(oReader["ArrCityName"]);
                        rec.DepCity = (int)oReader["DepCity"];
                        rec.DepCityName = Conversion.getStrOrNull(oReader["DepCityName"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<paymentMonitorRecord> getPaymentMonitorData(User UserData, string AgencyUser, string AgencyOffice,
                                            int? ArrCity, int? DepCity, DateTime? BegDateFirst, DateTime? BegDateLast,
                                            DateTime? ResDateFirst, DateTime? ResDateLast, DateTime? DueDate, string fltBalance, string ResStatusStr,
                                            string ConfirmStatusStr, string OptionControl, bool IncludeDueDate, string SurName, string Name,
                                            ref string errorMsg)
        {
            List<paymentMonitorRecord> records = new List<paymentMonitorRecord>();

            string tsql = string.Empty;
            #region Main Sql
            tsql = string.Format(@"
                    Select *, Balance = AgencyPayable2 - AgencyPayment From (   
                        Select R.ResNo, R.Office, R.Agency, R.AgencyUser,
                            R.DepCity, DepCityName = (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = R.DepCity), 
                            R.SaleCur, 
                            Surname = (Select Top 1 Surname From ResCust C where C.ResNo = R.ResNo and C.Leader = 'Y'),
                            Name = (Select Top 1 Name From ResCust C where C.ResNo = R.ResNo and C.Leader = 'Y'),
                            Leader = (Select Top 1 Surname + ' ' + Name From ResCust C where C.ResNo = R.ResNo and C.Leader = 'Y'),
                            MainAgency = (	SELECT Agency = CASE WHEN AgencyType = 0 THEN R.Agency ELSE MainOffice END 
				                            FROM Agency (NOLOCK) WHERE Code = R.Agency),      
                            R.ArrCity, ArrCityName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = R.ArrCity), 
                            R.BegDate, R.EndDate, R.Days, R.ResStat, R.ConfStat, R.ResDate, R.Adult, R.Child,      
                            R.SalePrice, R.AgencyCom, R.DisCount, R.BrokerCom, R.UserBonus, R.AgencyBonus, R.AgencySupDis, R.AgencyComSup, 
                            R.EBAgency, isnull(R.AgencyPayment,0) AgencyPayment, 
                            AgencyPayable = Case When dbo.ufn_AgencyResPayRule(R.Agency, R.PLMarket)=0 then IsNull(R.AgencyPayable,0) else IsNull(R.PasAmount,0) end,
                            AgencyPayable2 = isnull(P.Amount, 0), R.EBPas,
                            OfficeName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Agency (NOLOCK) Where Code = R.Agency),
                            ResStatStr = Case R.ResStat 
                                               When 0 Then 'New' 
                                               When 1 Then 'Modified' 
                                               When 2 Then 'Cancel' 
                                               When 3 Then 'Cancel X' 
                                               When 9 Then 'Draft' Else '' End, 
                            ConfStatStr = Case R.ConfStat 
                                               When 0 Then 'Request' 
                                               When 1 Then 'OK' 
                                               When 2 Then 'Not Confirm' 
                                               When 3 Then 'No Show' Else '' End,
                            R.OptDate  
                        FROM ResMain R (NOLOCK)
                        LEFT JOIN (	SELECT ResNo, Sum(isnull(Amount,0)) Amount, Cur
			                        FROM ResPayPlan
                                    WHERE {0} 
                                    GROUP BY ResNo, Cur) P ON P.ResNo = R.ResNo      
                        WHERE isnull(R.Ballon, '') <> '' 
                          AND R.Operator = @Operator 
                        ) X
                     WHERE @MainAgency = (Select Agency = case when AgencyType = 0 then X.Agency else MainOffice end From Agency where Code = X.Agency)
                     ", IncludeDueDate ? "DueDate <= dbo.DateOnly(@DueDate)" : "DueDate < dbo.DateOnly(@DueDate)");

            #endregion Main Sql

            //#region Where

            if (ArrCity.HasValue)
                tsql += string.Format(" And X.DepCity={0} ", DepCity);
            if (DepCity.HasValue)
                tsql += string.Format(" And X.ArrCity={0} ", ArrCity);
            if (BegDateFirst.HasValue)
                tsql += " And X.BegDate >= @BegDate1 ";
            if (BegDateLast.HasValue)
                tsql += " And X.BegDate <= @BegDate2 ";
            if (ResDateFirst.HasValue)
                tsql += " And X.ResDate >= @ResDate1 ";
            if (ResDateLast.HasValue)
                tsql += " And X.ResDate <= @ResDate2 ";
            if (!string.IsNullOrEmpty(AgencyOffice))
                tsql += string.Format(" And X.Agency={0} ", AgencyOffice);
            if (!string.IsNullOrEmpty(AgencyUser))
                tsql += string.Format(" And X.AgencyUser={0} ", AgencyUser);
            if (!string.IsNullOrEmpty(SurName))
                tsql += string.Format(" And X.Surname Like '%{0}%' ", SurName);
            if (!string.IsNullOrEmpty(Name))
                tsql += string.Format(" And X.Name Like '%{0}%' ", Name);

            if (fltBalance[0].ToString() != "1")
            {
                string tmptsql = string.Empty;

                if (fltBalance[1].ToString() == "1")
                    tmptsql += "OR (X.AgencyPayable2 - X.AgencyPayment) > 0 ";

                if (fltBalance[2].ToString() == "1")
                    tmptsql += "OR (X.AgencyPayable2 - X.AgencyPayment) < 0 ";

                if (fltBalance[3].ToString() == "1")
                    tmptsql += "OR (X.AgencyPayable2 - X.AgencyPayment) = 0 ";

                if (tmptsql.Length > 0)
                {
                    tmptsql = tmptsql.Remove(0, 3);
                    tmptsql = " AND (" + tmptsql + ") ";
                    tsql += tmptsql;
                }
            }

            if (ResStatusStr != "")
                tsql = tsql + " And X.ResStat in(" + ResStatusStr + ") ";
            if (ConfirmStatusStr != "")
                tsql = tsql + " And X.ConfStat in (" + ConfirmStatusStr + ") ";

            tsql += " Order By X.ResNo Desc ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "MainAgency", DbType.String, UserData.MainOffice);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "DueDate", DbType.DateTime, DueDate.HasValue ? DueDate.Value : DateTime.Today);
                if (BegDateFirst.HasValue && BegDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "BegDate1", DbType.DateTime, BegDateFirst.Value);
                if (BegDateLast.HasValue && BegDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "BegDate2", DbType.DateTime, BegDateLast.Value);
                if (ResDateFirst.HasValue && ResDateFirst.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "ResDate1", DbType.DateTime, ResDateFirst.Value);
                if (ResDateLast.HasValue && ResDateLast.Value > DateTime.MinValue)
                    db.AddInParameter(dbCommand, "ResDate2", DbType.DateTime, ResDateLast.Value);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        paymentMonitorRecord rec = new paymentMonitorRecord();

                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CommisionInvoiceNumberRecord> getCommisionInvoiceNumber(string ResNo, ref string errorMsg)
        {
            List<CommisionInvoiceNumberRecord> records = new List<CommisionInvoiceNumberRecord>();

            string tsql = @"Select I.InvSerial, I.InvNo, IR.ResNo
                            From PInvoice I (NOLOCK)
                            Join PInvoiceRes IR (NOLOCK) ON IR.InvoiceID = I.RecID
                            Where IR.ResNo = @ResNo
                              And ClientType = 1 ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        CommisionInvoiceNumberRecord rec = new CommisionInvoiceNumberRecord();
                        rec.ResNo = Conversion.getStrOrNull(rdr["ResNo"]);
                        rec.InvSerial = Conversion.getStrOrNull(rdr["InvSerial"]);
                        rec.InvNo = Conversion.getStrOrNull(rdr["InvNo"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AgencyCanShareRecord> getShareAgency(User UserData, ref string errorMsg)
        {
            if (Convert.ToDecimal(UserData.TvVersion) < Convert.ToDecimal("040057103"))
                return null;

            List<AgencyCanShareRecord> records = new List<AgencyCanShareRecord>();

            string tsql = @"Select Agency,CanShareAgency From AgencyCanShare (NOLOCK) Where CanShareAgency=@agency";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "agency", DbType.String, UserData.AgencyID);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        AgencyCanShareRecord rec = new AgencyCanShareRecord();
                        rec.Agency = Conversion.getStrOrNull(rdr["Agency"]);
                        rec.CanShareAgency = Conversion.getStrOrNull(rdr["CanShareAgency"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
