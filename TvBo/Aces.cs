﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace TvBo
{
    public class Aces
    {
        public bool SendAceTo(User UserData, ResDataRecord ResData, string AcePath, ref string AceFileName, ref string errorMsg)
        {

            AgencyRecord AgencyTable = UserData.AgencyRec;
            Int16 aceSendTo = AgencyTable.AceSendTo;
            if (AgencyTable != null)
            {
                if (!(AgencyTable.AceExport && (aceSendTo == 2 || aceSendTo == 3)))
                {
                    errorMsg = "Agency Ace Export not defined.";
                    return true;
                }
            }
            else
            {
                errorMsg = "Agency information not found. Please contact your operator.";
                return true;
            }

            ResMainRecord resMain = ResData.ResMain;
            List<ResCustRecord> resCust = ResData.ResCust.Where(w => w.Status == 0).ToList<ResCustRecord>();
            List<ResServiceRecord> resSer = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).ToList<ResServiceRecord>();
            if (resSer == null) resSer = new List<ResServiceRecord>();
            if (resSer.Count == 0) resSer.Add(new ResServiceRecord());            
            string FileN = "ACE_" + resMain.ResNo + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
            string filename = AcePath + FileN;
            AceFileName = FileN;
            Location loc = new Location();
            AgencyRecord resAgency = new Agency().getAgency(resMain.Agency, false, ref errorMsg);
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlTextWriter xmlWriter = new XmlTextWriter(filename, Encoding.UTF8);
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("TFCR-REC");
                xmlWriter.WriteStartElement("HEADER");
                xmlWriter.WriteElementString("CRS_CODE", "WWW");
                xmlWriter.WriteElementString("AGENT_USER_SIGN", resMain.AgencyUser);
                xmlWriter.WriteElementString("CRS_RECORD_LOCATOR", resMain.ResNo);
                xmlWriter.WriteElementString("CURRENCY_CODE", resMain.SaleCur);
                xmlWriter.WriteElementString("CUSTOMER_CODE", resMain.AceCustomerCode);
                loc = new Locations().getLocation(UserData.Market, resMain.ArrCity, ref errorMsg);
                xmlWriter.WriteElementString("DESTINATION_CODE", loc.Code);                
                xmlWriter.WriteElementString("DESTINATION_NAME", new Locations().getLocationName(UserData.Market, new Locations().getLocationForCountry(resMain.ArrCity.Value, ref errorMsg).Value, NameType.NormalName, ref errorMsg));
                xmlWriter.WriteElementString("PCC", resMain.Agency);
                xmlWriter.WriteElementString("PNR_DATE", resMain.ReceiveDate.Value.ToString("yyyyMMdd"));
                xmlWriter.WriteElementString("SALES_CHANNEL", "DIR");
                xmlWriter.WriteElementString("TF_NUMBER", resMain.AceTfNumber);

                xmlWriter.WriteStartElement("PAX_REFS");
                xmlWriter.WriteStartElement("PAX_REF");
                foreach (ResCustRecord row in resCust)
                {
                    xmlWriter.WriteElementString("PAX_SEQUENCE", row.SeqNo.ToString());
                }
                xmlWriter.WriteEndElement();    //PAX_REF
                xmlWriter.WriteEndElement();    //PAX_REFS
                xmlWriter.WriteEndElement();    //HEADER

                xmlWriter.WriteStartElement("PASSENGERS");
                foreach (ResCustRecord row in resCust)
                {
                    xmlWriter.WriteStartElement("PASSENGER");
                    xmlWriter.WriteElementString("BIRTHDATE", row.Birtday.HasValue ? row.Birtday.Value.ToString("yyyyMMdd") : "");
                    xmlWriter.WriteElementString("PAX_NAME", row.Surname);
                    xmlWriter.WriteElementString("PAX_NAME_FIRST", row.Name);
                    xmlWriter.WriteElementString("PAX_NAME_TITLE", row.TitleStr);
                    xmlWriter.WriteElementString("PAX_SEQUENCE", row.SeqNo.ToString());
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("PACKAGES");
                foreach (ResServiceRecord row in resSer)
                {
                    xmlWriter.WriteStartElement("PACKAGE");
                    xmlWriter.WriteElementString("DESCRIPTION", resMain.HolPackName);
                    xmlWriter.WriteElementString("ACCOMM_DESCRIPTION", row.AccomName);
                    xmlWriter.WriteElementString("ACCOMM_TYPE", row.Accom);
                    xmlWriter.WriteElementString("BOARD_CODE", row.Board);
                    xmlWriter.WriteElementString("BOARD_DESCRIPTION", row.Board);
                    xmlWriter.WriteElementString("BOOKING_REFERENCE", resMain.ResNo);

                    decimal agencyCom = resMain.AgencyCom.HasValue ? resMain.AgencyCom.Value : 0;
                    decimal agencyDisPasVal = resMain.AgencyDisPasVal.HasValue ? resMain.AgencyDisPasVal.Value : 0;
                    decimal EBAgency = resMain.EBAgency.HasValue ? resMain.EBAgency.Value : 0;
                    decimal agencyComAgencyEB = agencyCom - agencyDisPasVal + EBAgency;

                    xmlWriter.WriteElementString("COMM_AMT", Convert.ToInt64(Math.Round(agencyComAgencyEB * 100)).ToString());
                    xmlWriter.WriteElementString("DEPARTURE_DATE", row.BegDate.HasValue ? row.BegDate.Value.ToString("yyyyMMdd") : resMain.BegDate.Value.ToString("yyyyMMdd"));
                    loc = new Locations().getLocation(UserData.Market, resMain.DepCity, ref errorMsg);
                    xmlWriter.WriteElementString("DEPARTURE_PLACE", loc != null ? loc.Code : "");
                    loc = new Locations().getLocation(UserData.Market, resMain.ArrCity, ref errorMsg);
                    xmlWriter.WriteElementString("DESTINATION_PLACE", loc != null ? loc.Code : "");
                    HotelRecord hotel = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
                    xmlWriter.WriteElementString("HOTELNAME", hotel != null ? hotel.Name : "");
                    string pax = !string.IsNullOrEmpty(row.Service) ? (row.Adult.Value + row.Child.Value).ToString() : (resMain.Adult.Value + resMain.Child.Value).ToString();
                    xmlWriter.WriteElementString("NUMBER_OF_PAX", pax);
                    xmlWriter.WriteElementString("NUMBER_OF_ROOMS", row.Unit.ToString());
                    List<short> onlyFlight = new List<short>();
                    onlyFlight.Add(2); onlyFlight.Add(3); onlyFlight.Add(5);
                    xmlWriter.WriteElementString("PRODUCT_CODE", onlyFlight.Contains(resMain.SaleResource.Value) ? "Charter" : resMain.HolPack);
                    xmlWriter.WriteElementString("PROD_SEQUENCE", resMain.RefNo.ToString());
                    xmlWriter.WriteElementString("RETURN_DATE", row.EndDate.HasValue ? row.EndDate.Value.ToString("yyyyMMdd") : (resMain.EndDate.Value.ToString("yyyyMMdd")));
                    decimal salePrice = resMain.PasPayable.HasValue ? resMain.PasPayable.Value : 0;

                    xmlWriter.WriteElementString("SALES_PRICE", Convert.ToInt64(Math.Round(salePrice * 100)).ToString());
                    xmlWriter.WriteElementString("STATUS", !string.IsNullOrEmpty(row.ServiceType) ? (row.StatConf == 1 ? "OK" : "RQ") : (resMain.ConfStat == 1 ? "OK" : "RQ"));
                    xmlWriter.WriteElementString("SUPPLIER_CODE", resAgency.SupplierCode.ToString());
                    xmlWriter.WriteElementString("SERVICE_TRANSFER_DOCUMENT", "Y");
                    decimal? query1 = ResData.ResService.Where(w => w.IncPack == "N" && w.PayCom == "N").Sum(s => s.SalePrice.HasValue ? s.SalePrice.Value : 0);
                    decimal? query2 = ResData.ResServiceExt.Where(w => w.IncPack == "N" && w.PayCom == "N").Sum(s => s.SalePrice.HasValue ? s.SalePrice.Value : 0);
                    decimal taxes = (query1.HasValue ? query1.Value : 0) + (query2.HasValue ? query2.Value : 0);
                    xmlWriter.WriteElementString("TAXES", Convert.ToInt64(taxes * 100).ToString());
                    decimal pasPayable = resMain.PasPayable.HasValue ? resMain.PasPayable.Value : 0;
                    decimal pasAmount = resMain.PasAmount.HasValue ? resMain.PasAmount.Value : 0;

                    decimal purchasePrice = salePrice - agencyComAgencyEB;
                    xmlWriter.WriteElementString("PURCHASE_PRICE", Convert.ToInt64(Math.Round(purchasePrice * 100)).ToString());
                    xmlWriter.WriteStartElement("PAX_REFS");
                    xmlWriter.WriteStartElement("PAX_REF");
                    var query = from q1 in ResData.ResCust
                                join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                                where q2.ServiceID == row.RecID
                                select q1;
                    if (query.Count() > 0)
                    {
                        foreach (var row1 in query)
                            xmlWriter.WriteElementString("PAX_SEQUENCE", row1.SeqNo.ToString());
                    }
                    else
                    {                        
                        foreach (var row1 in resCust)
                            xmlWriter.WriteElementString("PAX_SEQUENCE", row1.SeqNo.ToString());
                    }
                    xmlWriter.WriteEndElement();    //PAX_REF
                    xmlWriter.WriteEndElement();    //PAX_REFS
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
                xmlWriter.Close();

                if (aceSendTo == 2)
                {
                    FileInfo fileInf = new FileInfo(filename);
                    string uri = "ftp://" + AgencyTable.FtpHost + AgencyTable.FtpDir + "/" + FileN;
                    FtpWebRequest reqFTP;

                    // Create FtpWebRequest object from the Uri provided
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(uri));

                    // Provide the WebPermission Credintials
                    reqFTP.Credentials = new NetworkCredential(AgencyTable.FtpUser, AgencyTable.FtpPas);

                    // By default KeepAlive is true, where the control connection is 
                    // not closed after a command is executed.
                    reqFTP.KeepAlive = false;

                    // Specify the command to be executed.
                    reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

                    // Specify the data transfer type.
                    reqFTP.UseBinary = true;

                    // Notify the server about the size of the uploaded file
                    reqFTP.ContentLength = fileInf.Length;

                    // The buffer size is set to 2kb
                    int buffLength = 2048;
                    byte[] buff = new byte[2048];
                    int contentLen;

                    // Opens a file stream (System.IO.FileStream) to read 
                    //the file to be uploaded
                    FileStream fs = fileInf.OpenRead();

                    try
                    {
                        // Stream to which the file to be upload is written
                        Stream strm = reqFTP.GetRequestStream();

                        // Read from the file stream 2kb at a time
                        contentLen = fs.Read(buff, 0, buffLength);

                        // Till Stream content ends
                        while (contentLen != 0)
                        {
                            // Write Content from the file stream to the 
                            // FTP Upload Stream
                            strm.Write(buff, 0, contentLen);
                            contentLen = fs.Read(buff, 0, buffLength);
                        }

                        // Close the file stream and the Request Stream
                        strm.Close();
                        fs.Close();
                        UpdateResMainAceFields(UserData, resMain.ResNo);
                    }
                    catch (Exception ex)
                    {
                        errorMsg = ex.Message;
                        return false;
                        //  MessageBox.Show(ex.Message, "Upload Error");
                    }
                    return true;
                }
                else return false;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
        }

        public void UpdateResMainAceFields(User UserData, string ResNo)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string tsql = @"
                            Update ResMain 
                            Set SendToAce=1,SendToAceDate=GetDate() 
                            Where ResNo=@ResNo
                           ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", System.Data.DbType.String, ResNo);
                db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
