﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TvTools;

namespace TvBo
{
    #region
    public enum mixCountryType { None = 0, TourVisio = 1, Paximum = 2, All = 3 }

    public class CountryListResponse
    {
        public List<IdNamePair> CountryList { get; set; }
    }

    [Serializable()]
    public class resultFilterMix
    {
        public string ID { get; set; }
        public string FieldName { get; set; }
        public int? Width { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public resultFilterMix()
        {
        }
    }

    [Serializable()]
    public class mixResultFilter
    {
        public bool filtered { get; set; }
        public string PaxLocation { get; set; }
        public string Category { get; set; }
        public string Hotel { get; set; }
        public DateTime? CheckIn { get; set; }
        public Int16? Night { get; set; }
        public string Room { get; set; }
        public string Board { get; set; }
        public mixResultFilter()
        {
            this.filtered = false;
        }
    }

    [Serializable()]
    public class mixOnlyHotelResultFilter
    {
        public bool showPaxLocation { get; set; }
        public List<FilterItem> PaxLocation { get; set; }
        public bool showCategory { get; set; }
        public List<FilterItem> Category { get; set; }
        public bool showHotel { get; set; }
        public List<FilterItem> Hotel { get; set; }
        public bool showCheckIn { get; set; }
        public List<FilterItem> CheckIn { get; set; }
        public bool showNight { get; set; }
        public List<FilterItem> Night { get; set; }
        public bool showRoom { get; set; }
        public List<FilterItem> Room { get; set; }
        public bool showBoard { get; set; }
        public List<FilterItem> Board { get; set; }

        public mixOnlyHotelResultFilter()
        {
            this.showPaxLocation = false;
            this.PaxLocation = new List<FilterItem>();
            this.showCategory = false;
            this.Category = new List<FilterItem>();
            this.showHotel = false;
            this.Hotel = new List<FilterItem>();
            this.showCheckIn = false;
            this.CheckIn = new List<FilterItem>();
            this.showNight = false;
            this.Night = new List<FilterItem>();
            this.showRoom = false;
            this.Room = new List<FilterItem>();
            this.showBoard = false;
            this.Board = new List<FilterItem>();
        }
    }

    public class MixCountry
    {
        public string Name { get; set; }
        public string ISO2 { get; set; }
        public string ISO3 { get; set; }
        public MixCountry()
        {
        }
    }

    [Serializable()]
    public class MixPlCountry
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public mixCountryType CountryType { get; set; }
        public MixPlCountry()
        {
            this.CountryType = mixCountryType.TourVisio;
        }
    }

    public class MixSearchCriteriaRoom
    {
        public Int16 RoomNr { get; set; }
        public Int16 AdultCount { get; set; }
        public Int16 ChildCount { get; set; }
        public Int16 InfantCount { get; set; }
        public List<Int16> ChildAge { get; set; }
        public MixSearchCriteriaRoom()
        {
            this.RoomNr = 1;
            this.AdultCount = 2;
            this.ChildCount = 0;
            this.InfantCount = 0;
            this.ChildAge = new List<short>();
        }
    }

    public class MixSearchCriteria
    {
        public SearchType SType { get; set; }
        public DateTime CheckIn { get; set; }
        public Int16 ExpandDay { get; set; }
        public Int16 DefaultExpandDay { get; set; }
        public Int16 NightFrom { get; set; }
        public Int16 NightTo { get; set; }
        public List<string> PaxLocation { get; set; }
        public Int16 SearchRoomCount { get; set; }
        public List<MixSearchCriteriaRoom> RoomInfoList { get; set; }
        public Int32? Country { get; set; }
        public Int32? DepCity { get; set; }
        public Int32? ArrCity { get; set; }
        public Int32? Location { get; set; }
        public Int16? HolPackCategory { get; set; }
        public string Resort { get; set; }
        public string Category { get; set; }
        public string Hotel { get; set; }
        public string Room { get; set; }
        public string RoomView { get; set; }
        public string Board { get; set; }
        public string Package { get; set; }
        public string HolidayPackage { get; set; }
        public string Facility { get; set; }
        public decimal? MinPrice { get; set; }
        public decimal? MaxPrice { get; set; }
        public bool CurrencyControl { get; set; }
        public string CurrentCurrency { get; set; }
        public bool DoNotShowAvailable { get; set; }
        public bool UseRoomGroup { get; set; }
        public bool UseBoardGroup { get; set; }
        public bool ShowStopSaleHotel { get; set; }
        public bool UseExternalAllotControl { get; set; }
        public bool ShowExpandDay { get; set; }
        public Int16? MaxExpandDayForPriceSearch { get; set; }
        public bool ShowSecondDay { get; set; }
        public bool ShowAgencyEB { get; set; }
        public List<calendarColor> CalendarDay { get; set; }
        public string PromotionCode { get; set; }
        public int? FromRecord { get; set; }
        public int? ToRecord { get; set; }
        public bool CurControl { get; set; }
        public string CurrentCur { get; set; }
        public string PaxNationality { get; set; }
        public bool PaxAvailable { get; set; }        
        public MixSearchCriteria()
        {
            this.SType = SearchType.OnlyHotelMixSearch;
            this.CheckIn = DateTime.Today;
            this.ExpandDay = 0;
            this.DefaultExpandDay = 0;
            this.NightFrom = 7;
            this.NightTo = 7;
            this.SearchRoomCount = 1;
            this.RoomInfoList = new List<MixSearchCriteriaRoom>();
            this.CurrencyControl = false;
            this.CurrentCurrency = string.Empty;
            this.DoNotShowAvailable = false;
            this.UseRoomGroup = false;
            this.UseBoardGroup = false;
            this.ShowStopSaleHotel = true;
            this.UseExternalAllotControl = false;
            this.ShowExpandDay = true;
            this.MaxExpandDayForPriceSearch = 14;
            this.ShowSecondDay = false;
            this.ShowAgencyEB = false;
            this.CalendarDay = new List<calendarColor>();
            this.CurControl = true;
            this.PaxAvailable = false;            
        }
    }

    public class MixSearchFilterData
    {
        public bool CurrencyConvert { get; set; }
        public bool DisableRoomFilter { get; set; }
        public bool ShowDepCity { get; set; }
        public bool ShowArrCity { get; set; }
        public bool ShowExpandDay { get; set; }
        public bool ShowFirstDay { get; set; }
        public bool ShowSecondDay { get; set; }
        public bool ShowStopSaleHotels { get; set; }
        public bool? ShowFilterPackage { get; set; }
        public bool? ShowOfferBtn { get; set; }
        public DateTime? CheckIn { get; set; }
        public IEnumerable fltCountry { get; set; }
        public fltComboDayRecord fltCheckInDay { get; set; }
        public fltComboDayRecord fltNight { get; set; }
        public fltComboDayRecord fltNight2 { get; set; }
        public fltComboDayRecord fltRoomCount { get; set; }
        public fltComboDayRecord fltMixRoomCount { get; set; }
        public string CustomRegID { get; set; }
        public string Market { get; set; }
        public string MarketCur { get; set; }
        public string PaxCurrency { get; set; }
        public string PaxNationality { get; set; }
        public bool AvailableHotel { get; set; }
        public bool ShowAvailableHotel { get; set; }
        public bool ShowRoomView { get; set; }
        public MixSearchFilterData()
        {
            this.DisableRoomFilter = false;
            this.ShowFirstDay = true;
            this.ShowDepCity = true;
            this.ShowArrCity = true;
            this.AvailableHotel = false;
            this.ShowAvailableHotel = true;
            this.ShowRoomView = false;
        }
    }

    public class MixPlLocation
    {
        public int? ArrCountry { get; set; }
        public string ArrCountryName { get; set; }
        public int? DepCity { get; set; }
        public string DepCityName { get; set; }
        public int? ArrCity { get; set; }
        public string ArrCityName { get; set; }
        public int? HotelLocation { get; set; }
        public string HotelLocationName { get; set; }
        public Int16? HolPackCategory { get; set; }
        public string HolPackCategoryName { get; set; }
        public MixPlLocation()
        {
        }
    }

    public class MixPlCategory
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int? Country { get; set; }
        public int? PLCountry { get; set; }
        public int? City { get; set; }
        public int? PLCity { get; set; }
        public int? Town { get; set; }
        public int? Village { get; set; }
        public Int16? HolPackCat { get; set; }
        public MixPlCategory()
        {
        }
    }

    public class MixPlHolPack
    {
        public string Name { get; set; }
        public string NameL { get; set; }
        public string HolPack { get; set; }
        public int DepCity { get; set; }
        public int ArrCity { get; set; }
        public int Country { get; set; }
        public Int16? HolPackCat { get; set; }
        public MixPlHolPack()
        {
        }
    }

    public class MixPlBoard
    {
        public string Name { get; set; }
        public string Hotel_Code { get; set; }
        public string Code { get; set; }
        public int City { get; set; }
        public int? Town { get; set; }
        public int? Village { get; set; }
        public int PLArrCity { get; set; }
        public int PLDepCity { get; set; }
        public Int16? HolPackCat { get; set; }
        public MixPlBoard()
        {
        }
    }

    public class MixPlRoomView
    {
        public string Name { get; set; }
        public string Hotel_Code { get; set; }
        public int? Code { get; set; }
        public int City { get; set; }
        public int? Town { get; set; }
        public int? Village { get; set; }
        public int PLArrCity { get; set; }
        public int PLDepCity { get; set; }               

        public MixPlRoomView()
        {
        }
    }

    public class MixPlRoom
    {
        public string Name { get; set; }
        public string Hotel_Code { get; set; }
        public string Code { get; set; }
        public int City { get; set; }
        public int? Town { get; set; }
        public int? Village { get; set; }
        public int PLArrCity { get; set; }
        public int PLDepCity { get; set; }
        public Int16? HolPackCat { get; set; }
        public int? RoomViewID { get; set; }
        public MixPlRoom()
        {
        }
    }

    public class MixPlResort
    {
        public string Name { get; set; }
        public int? RecID { get; set; }
        public int Country { get; set; }
        public int City { get; set; }
        public int? Town { get; set; }
        public Int16? HolPackCat { get; set; }
        public MixPlResort()
        {
        }
    }

    public class MixPlHotel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public string Category { get; set; }
        public int Location { get; set; }
        public string LocationName { get; set; }
        public int Country { get; set; }
        public string CountryName { get; set; }
        public int? Town { get; set; }
        public string TownName { get; set; }
        public int ArrCity { get; set; }
        public string ArrCityName { get; set; }
        public int? CatPackID { get; set; }
        public string Holpack { get; set; }
        public string HolpackName { get; set; }
        public Int16? HolPackCat { get; set; }
        public MixPlHotel()
        {
        }
    }

    public class MixPlHolPackCat
    {
        public int? RecID { get; set; }
        public Int16? Code { get; set; }
        public string Name { get; set; }
        public MixPlHolPackCat()
        {
        }
    }

    public class MixPlFacility
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Hotel_Code { get; set; }
        public int? City { get; set; }
        public int? Town { get; set; }
        public int? Village { get; set; }
        public int? PLArrCity { get; set; }
        public int? PLDepCity { get; set; }
        public Int16? HolPackCat { get; set; }
        public MixPlFacility()
        {
        }
    }

    public class MixPlMaxPaxCounts
    {
        public int? maxAdult { get; set; }
        public int? maxChd { get; set; }
        public int? maxChdAgeG1 { get; set; }
        public int? maxChdAgeG2 { get; set; }
        public int? maxChdAgeG3 { get; set; }
        public int? maxChdAgeG4 { get; set; }
        public MixPlMaxPaxCounts()
        {
            this.maxAdult = 6;
            this.maxChd = 4;
            this.maxChdAgeG1 = 4;
            this.maxChdAgeG2 = 4;
            this.maxChdAgeG3 = 4;
            this.maxChdAgeG4 = 4;
        }
    }

    public class MixSearchFilterCacheData
    {
        public List<MixPlCountry> CountryList { get; set; }
        public List<MixPlLocation> LocationList { get; set; }
        public List<MixPlCategory> CategoryList { get; set; }
        public List<MixPlHolPack> HolPackList { get; set; }
        public List<MixPlBoard> BoardList { get; set; }
        public List<MixPlRoom> RoomList { get; set; }
        public List<MixPlResort> ResortList { get; set; }
        public List<MixPlHotel> HotelList { get; set; }
        public List<MixPlHolPackCat> PlHolPackCatList { get; set; }
        public MixPlMaxPaxCounts MixPlMaxPaxCount { get; set; }
        public List<MixPlFacility> FacilityList { get; set; }
        public List<MixPlRoomView> RoomViewList { get; set; }
        public MixSearchFilterCacheData()
        {
            this.CountryList = new List<MixPlCountry>();
            this.LocationList = new List<MixPlLocation>();
            this.CategoryList = new List<MixPlCategory>();
            this.HolPackList = new List<MixPlHolPack>();
            this.BoardList = new List<MixPlBoard>();
            this.RoomList = new List<MixPlRoom>();
            this.ResortList = new List<MixPlResort>();
            this.HotelList = new List<MixPlHotel>();
            this.PlHolPackCatList = new List<MixPlHolPackCat>();
            this.FacilityList = new List<MixPlFacility>();
            this.MixPlMaxPaxCount = new MixPlMaxPaxCounts();
            this.RoomViewList = new List<MixPlRoomView>();
        }
    }

    public class MixResData
    {
        public GetHotelOfferDetailsResponse offerDetail { get; set; }
        public CreateOrder order { get; set; }
        public Hotel hotelDetail { get; set; }
        public MixResData()
        {
            this.offerDetail = new GetHotelOfferDetailsResponse();
            this.order = new CreateOrder();
            this.hotelDetail = new Hotel();
        }
    }

    #endregion

    #region Paximum

    public enum PaxPaymentType
    {
        AccountCredit,
        AccountCreditCard,
        CustomerCreditCard
    }

    public enum TravellerType
    {
        Adult,
        Child,
        Infant
    }

    public enum TravellerTitle
    {
        Mr,
        Ms,
        Mrs,
        Miss,
        Child,
        Infant
    }

    [Serializable()]
    public class DestinationSearchCriteria
    {
        public string Id { get; set; }
        public string Type { get; set; }
    }

    [Serializable()]
    public class RoomSearchCriteria
    {
        public int Adults { get; set; }
        public int[] ChildrenAges { get; set; }
    }

    [Serializable()]
    public class SearchHotelRequest
    {
        public SearchHotelRequest()
        {
            SearchId = Guid.NewGuid().ToString();
            FilterUnavailable = true;
        }

        public string SearchId { get; private set; }
        public DestinationSearchCriteria[] Destinations { get; set; }
        public DateTime CheckinDate { get; set; }
        public string CheckIn { get; set; }
        public DateTime CheckoutDate { get; set; }
        public string CheckOut { get; set; }
        public string Night { get; set; }
        public RoomSearchCriteria[] Rooms { get; set; }
        public string CustomerNationality { get; set; }
        public string Currency { get; set; }
        public bool FilterUnavailable { get; set; }
        public bool WithPromotion { get; set; }
        public string Language { get; set; }

        public void Validate()
        {
            Language = Language ?? "en";
        }
    }

    [Serializable()]
    public class SearchHotelsResponse
    {
        public string SearchId { get; set; }
        public DateTime ExpiresOn { get; set; }
        public Hotel[] Hotels { get; set; }
        public Hotel[] FilterHotels { get; set; }
        public SearchHotelRequest Requests { get; set; }
        public mixResultFilter FilterResult { get; set; }
        public string Sort { get; set; }
    }

    [Serializable()]
    public class Hotel
    {
        public int OrderNo { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CropedDescription { get; set; }
        public bool ShowDescription { get; set; }
        public IdNamePair City { get; set; }
        public string CityName { get; set; }
        public IdNamePair Country { get; set; }
        public double Stars { get; set; }
        public string StarStr { get; set; }
        public double Rating { get; set; }
        public string RatingStr { get; set; }
        public string ReviewUrl { get; set; }
        public string Thumbnail { get; set; }
        public string[] Themes { get; set; }
        public string[] Facilities { get; set; }
        public Geolocation Geolocation { get; set; }
        public bool IsFavorite { get; set; }
        public HotelOffer[] Offers { get; set; }
        public Money SelectedPrice { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsPriced { get; set; }
    }

    [Serializable()]
    public class Geolocation
    {
        public float Longitude { get; set; }
        public float Latitude { get; set; }
    }

    [Serializable()]
    public class HotelOffer
    {
        public string Id { get; set; }
        public DateTime ExpiresOn { get; set; }
        public string HotelId { get; set; }
        public string Board { get; set; }
        public string[] BoardCategories { get; set; }
        public Room[] Rooms { get; set; }
        public string RoomName { get; set; }
        public Money Price { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsAvailable { get; set; }
        public string AvailableMsg { get; set; }
        public bool Selected { get; set; }
    }

    [Serializable()]
    public class IdNamePair
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    [Serializable()]
    public class Money
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string AmountStr { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", Amount.ToString("#,###.00"), Currency);
        }
    }

    [Serializable()]
    public class Room
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string[] Categories { get; set; }
        public string[] Promotions { get; set; }
        public Traveller[] Travellers { get; set; }
        public int SeqNo { get; set; }
    }

    public class BookRoom
    {
        public string RoomId { get; set; }
        public string[] Travellers { get; set; }
    }

    [Serializable()]
    public class Traveller
    {
        public string TravellerNo { get; set; }
        public string Type { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public bool isLead { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Nationality { get; set; }
    }

    public class inputTraveller
    {
        public string TravellerNo { get; set; }
        public string Type { get; set; }
        public string BirtDate { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public bool isLead { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Nationality { get; set; }
    }

    public class GetHotelsByQueryResponse
    {
        public GetHotelsByQuery Query { get; set; }
        public QueryResult[] Suggestions { get; set; }
    }

    public class GetHotelsByQuery
    {
        public string Term { get; set; }
        public string[] ServiceProviders { get; set; }
        public string Language { get; set; }

    }

    public class QueryResult
    {
        public string Id { get; set; }
        public string LanguageCode { get; set; }
        public string[] ServiceProviders { get; set; }
        public string Type { get; set; }
        public IdNamePair Hotel { get; set; }
        public IdNamePair Airport { get; set; }
        public IdNamePair City { get; set; }
        public IdNamePair Country { get; set; }
        public IdNamePair[] Destinations { get; set; }
    }

    public class SupplementSummary
    {
        public string Name { get; set; }
        public Money Price { get; set; }
        public bool PaidAtHotel { get; set; }
    }

    public class PricePerNight
    {
        public string RoomId { get; set; }
        public DateTime Date { get; set; }
        public Money Price { get; set; }
        public Money SalePrice { get; set; }
    }

    public class CancellationPolicySummary
    {
        public Money Fee { get; set; }
        public DateTime AfterDate { get; set; }
    }

    public class HotelOfferDetails
    {
        public string Id { get; set; }
        public DateTime ExpiresOn { get; set; }
        public string SearchId { get; set; }
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }
        public string HotelId { get; set; }
        public string Board { get; set; }
        public string[] RoomTypes { get; set; }
        public Money Price { get; set; }
        public PricePerNight[] PriceBreakdown { get; set; }
        public string[] CancelPolicy { get; set; }
        public CancellationPolicySummary[] CancellationPolicies { get; set; }
        public SupplementSummary[] Supplements { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsAvailable { get; set; }
        public string[] Notes { get; set; }
    }

    public class GetHotelOfferDetailsResponse
    {
        public Room[] Rooms { get; set; }
        public string Id { get; set; }
        public DateTime ExpiresOn { get; set; }
        public string SearchId { get; set; }        
        public DateTime CheckinDate { get; set; }        
        public DateTime CheckoutDate { get; set; }
        public string HotelId { get; set; }
        public string Board { get; set; }
        public string[] RoomTypes { get; set; }
        public Money Price { get; set; }
        public PricePerNight[] PriceBreakdown { get; set; }
        public string[] CancelPolicy { get; set; }
        public CancellationPolicySummary[] CancellationPolicies { get; set; }
        public SupplementSummary[] Supplements { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsAvailable { get; set; }
        public string[] Notes { get; set; }
        public string[] Warnings { get; set; }
    }

    public class CreateOrderResponse
    {
        public string OrderId { get; set; }
        public string[] BookingIds { get; set; }
    }

    public class CreateOrder
    {
        public CreateOrder()
        {
            Travellers = new Traveller[0];
            HotelBookings = new HotelBooking[0];
            FlightBookings = new FlightBooking[0];
        }
        public string UserId { get; set; }
        public Traveller[] Travellers { get; set; }
        public HotelBooking[] HotelBookings { get; set; }
        public FlightBooking[] FlightBookings { get; set; }
    }

    public class FlightBooking
    {
        public string OfferId { get; set; }
        public string ReservationNote { get; set; }
        public string[] Travellers { get; set; }
    }

    public class HotelBooking
    {
        public string OfferId { get; set; }
        public string ReservationNote { get; set; }
        public BookRoom[] Rooms { get; set; }
    }

    public class CreditCardInfo
    {
        public string CardNumber { get; set; }
        public string Cvv2 { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
    }

    public class ServiceTransaction
    {
        public string ServiceId { get; set; }
        public Money Amount { get; set; }
    }

    public class PreAuthRequest
    {
        public string Token { get; set; }
        public ServiceTransaction[] Services { get; set; }
    }

    public class GetPaymentMethodsByOrderIdResponse
    {
        public AllowedPaymentMethod[] PaymentMethods { get; set; }
    }

    public class AllowedPaymentMethod
    {
        public string Token { get; set; }
        public PaxPaymentType Type { get; set; }
    }

    public class MakeOrderResponse
    {
        public string OrderId { get; set; }
        public string OrderNumber { get; set; }
        public string AgencyReferenceNumber { get; set; }
        public Booking[] Bookings { get; set; }
    }

    public enum PaxBookingStatus { Pending, Confirmed, Cancelled, OnRequest, Rejected }
    public enum PaxServiceType { Hotel, Flight }
    public enum PaxCancellationFeeType { Amount, Percent, Night }

    public class Booking
    {
        public string Id { get; set; }
        public string BookingNumber { get; set; }
        public string AgencyReferenceNumber { get; set; }
        public string OfferId { get; set; }
        public string DocumentUrl { get; set; }
        public Money Price { get; set; }
        public CancellationPolicy[] CancellationPolicies { get; set; }
        public PaxBookingStatus Status { get; set; }
        public PaxServiceType Type { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public DateTime CancellationDeadline { get; set; }
    }

    public class CancellationPolicy
    {
        public string RoomId { get; set; }
        public DateTime PermittedDate { get; set; }
        public PaxCancellationFeeType FeeType { get; set; }
        public Money Fee { get; set; }
        public Money BuyingFeePrice { get; set; }
        public string Description { get; set; }
    }

    public class GetHotelDetailsByIds
    {
        public string[] Ids { get; set; }
        public IdTypes IdType { get; set; }
        public string Language { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Language))
                Language = "en-US";
        }

        public enum IdTypes
        {
            Hotel,
            Destination
        }
    }

    public class GetHotelDetailsByIdsResponse
    {
        public HotelDetail[] HotelDetails { get; set; }
    }

    public class HotelDetail
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double? Star { get; set; }
        public double Rating { get; set; }
        public string RatingReviewUrl { get; set; }
        public HotelDescription[] Descriptions { get; set; }
        public string DestinationId { get; set; }
        public string DestinationName { get; set; }
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public Theme[] Themes { get; set; }
        public Facility[] Facilities { get; set; }
        public HotelImage[] Images { get; set; }
        public GeoCode GeoCode { get; set; }

    }

    public class Theme
    {
        public string ThemeId { get; set; }
        public string ThemeName { get; set; }
    }

    public class HotelDescription
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }

    public class Facility
    {
        public string FacilityId { get; set; }
        public string FacilityName { get; set; }
        public bool Fee { get; set; }
    }

    public class HotelImage
    {
        public string ImageLocation { get; set; }
        public HotelImageSizeUrl[] ImageSizeUrls { get; set; }
    }

    public class GeoCode
    {
        public GeoCode()
        {
            Accuracy = new GeoCodeAccuracy();
        }

        public string Latitude { get; set; }
        public string Longtitude { get; set; }
        public GeoCodeAccuracy Accuracy { get; set; }
    }

    public enum GeoCodeAccuracy
    {
        Premise,
        Address,
        Street,
        Locality,
        City,
        Unknown,
    }

    public class HotelImageSizeUrl
    {
        public ImageSizes ImageSize { get; set; }
        public string ImageUrl { get; set; }
    }

    public enum ImageSizes
    {
        Small,
        Medium,
        Large
    }

    public class GetDestinationListRequest
    {
        public string CountryId { get; set; }
        public string Type { get; set; }
    }

    public class Destination
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public string ParentId { get; set; }
        public string ParentName { get; set; }
    }

    public class GetDestinationListResponse
    {
        public Destination[] DestinationList { get; set; }
    }

    public class GetUserInformation
    {
        public string Id { get; set; }
    }

    public class GetUserInformationResponse
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Picture { get; set; }
        public string BuyerLogo { get; set; }
        public string AccountName { get; set; }
        public AccountNationality[] AccountNationalities { get; set; }
        public string[] WorkingCurrencies { get; set; }
        public bool CanCreateMarkupRules { get; set; }
        public bool EnableZopim { get; set; }
        public bool ShowPaximumLogo { get; set; }
        public string Language { get; set; }
        public string[] AvailableServices { get; set; }
    }

    public class AccountNationality
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
    }

    public class HotelDetailsRequest
    {
        public string HotelId { get; set; }
    }

    public class HotelPhone
    {
        public string CountryAccessCode { get; set; }
        public string AreaCityCode { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsDefault { get; set; }
        public string Type { get; set; }
    }

    public class HotelAddress
    {
        public string[] AddressLines { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
        public string CityName { get; set; }
        public string ZipCode { get; set; }
        public string CountryCode { get; set; }
        public HotelPhone[] Phones { get; set; }
        public string Mail { get; set; }
        public string HomePage { get; set; }
        public Geolocation Geolocation { get; set; }
    }

    public class HotelFacility
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }

    public class HotelDetailsResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IdNamePair City { get; set; }
        public IdNamePair Country { get; set; }
        public Double Stars { get; set; }
        public Double Rating { get; set; }
        public string ReviewUrl { get; set; }
        public string[] Photos { get; set; }
        public string[] Themes { get; set; }
        public HotelFacility[] Facilities { get; set; }
        public HotelDescription[] Content { get; set; }
        public HotelAddress Address { get; set; }
    }

    public class GetBookingMatchingByPaximumOrderIdRequest
    {
        public string[] PaximumOrderIds { get; set; }
    }

    public class TvReservationMatching
    {
        public string Id { get; set; }
        public string PaximumOrderId { get; set; }
        public string TourvisioResNo { get; set; }
    }

    public class GetBookingMatchingByPaximumOrderIdResponse
    {
        public TvReservationMatching[] BookingMatching { get; set; }
    }

    public class GetBookingsByUserRequest
    {
        public BookingCriteria Criteria { get; set; }
    }

    public class BookingCriteria
    {
        public BookingCriteria()
        {
            BookingStatus = new BookingStatusCriteria[0];
        }
        public string[] AccountIds { get; set; }
        public string OrderNumber { get; set; }
        public string BookingNumber { get; set; }
        public string SupplierBookingNumber { get; set; }
        public string AgencyReferenceNumber { get; set; }
        public string CountryCode { get; set; }
        public string DestinationCode { get; set; }
        public string LeadName { get; set; }
        public string LeadSurname { get; set; }
        public string VoucherPnrNo { get; set; }
        public string[] ServiceProviders { get; set; }
        public DateCriteria Date { get; set; }
        public BookingStatusCriteria[] BookingStatus { get; set; }
    }

    public class DateCriteria
    {
        public DateCriteriaType Type { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }

    public enum DateCriteriaType { Creation, CheckIn, Confirmation, CancellationDeadline, CancelledOn, InHousedDate }

    public enum BookingStatusCriteria { Confirmed, OnRequest, Cancelled, Rejected }

    public enum BookingStatus { Pending, Confirmed, Cancelled, OnRequest, Rejected }

    public class GetBookingsByUserResponse
    {
        public string UserId { get; set; }
        public UserBookingInfo[] Bookings { get; set; }
    }

    public class LeaderTraveller
    {
        public string TravellerNo { get; set; }
        public string Type { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public bool isLead { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Nationality { get; set; }
        public IdentityDocument IdentityDocument { get; set; }
    }

    public class UserBookingInfo
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string OrderNumber { get; set; }
        public string BookingNumber { get; set; }
        public string AgencyReferenceNumber { get; set; }
        public string ProductName { get; set; }
        public DateTime CheckInDeparture { get; set; }
        public DateTime CheckOutReturn { get; set; }
        public LeaderTraveller Leader { get; set; }
        public bool Availability { get; set; }
        public bool Confirmation { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public Money TotalAmount { get; set; }
        public string VoucherPnrUrl { get; set; }
        public string ProformaInvoiceUrl { get; set; }
        public string ReceiptUrl { get; set; }
        public string ReturnInvoiceUrl { get; set; }
        public BookingStatus BookingStatus { get; set; }
        public DateTime CreationDate { get; set; }
    }

    public class IdentityDocument
    {
        public string Type { get; set; }
        public string DocumentNo { get; set; }
        public string Citizenship { get; set; }
        public string IssueCountry { get; set; }
        public string SerialNo { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpirationDate { get; set; }
    }

    public class makeOrderRequest
    {
        public string Token { get; set; }
    }

    #endregion

    #region Paximum Search UI

    public class mixFilterSelectedList
    {
        public string FieldName { get; set; }
        public string Value { get; set; }
        public mixFilterSelectedList()
        {
        }
    }

    public class mixMakeResRooms
    {
        public int SeqNo { get; set; }
        public string Board { get; set; }
        public string RoomType { get; set; }
        public string Price { get; set; }
    }

    #endregion

    public class OfferDetailException
    {
        public string code { get; set; }
        public string message { get; set; }
        public object data { get; set; }
        public OfferDetailException()
        { 
        }
    }
}
