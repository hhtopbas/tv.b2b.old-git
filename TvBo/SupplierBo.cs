﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class ShortSupplierRecord
    {
        public int? RecID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }        
        
        public ShortSupplierRecord()
        { 
        }
    }
}
