﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data;
using System.Configuration;
using TvTools;

namespace TvBo
{
    public class LoginUser
    {
        public string IpAddress()
        {
            string strIpAddress;
            strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (strIpAddress == null)
            {
                strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return strIpAddress;
        }

        private bool IpControl(User UserData)
        {
            string errorMsg = string.Empty;
            if (UserData.MasterAgency)
                return true;
            else
            {
                List<AgencyIPList> Ips = new TvBo.Common().AllowIps(UserData.AgencyID, ref errorMsg);
                if (Ips == null || Ips.Count < 1)
                    return true;
                else
                {
                    string myIpStr = IpAddress();
                    if (string.Equals(myIpStr.Trim(), "127.0.0.1")) return true;
                    var q1 = from ip in Ips.AsEnumerable()
                             where ip.ServiceControl == true
                             select new { IpNo = ip.IpNo };

                    var q = from ip in q1
                            where ip.IpNo == myIpStr
                            select ip;
                    if (q1.Count() > 0)
                    {
                        if (q.Count() > 0)
                            return true;
                        else
                            return false;
                    }
                    else
                        return true;
                }
            }
        }

        public string toLogin(User UserData, ref string errorMsg)
        {
            List<TvBo.MainMenu> mainMenuData = CacheObjects.getMainMenuData(UserData, ref errorMsg);
            string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "Version"));
            string firstMenu = string.Empty;

            var host = HttpContext.Current.Request.Url.Host;
            var serverIP = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            object _sslControl = new TvBo.Common().getFormConfigValue("General", "UseSSL");
            bool useSSL = TvTools.Conversion.getBoolOrNull(_sslControl).HasValue ? TvTools.Conversion.getBoolOrNull(_sslControl).Value : false;
            string absolutePath = VirtualPathUtility.ToAbsolute("~/");
            string sslPath = string.Format("http://{0}{1}", HttpContext.Current.Request.ServerVariables["HTTP_HOST"], absolutePath);
            if (useSSL && /*HttpContext.Current.Request.IsLocal.Equals(false) &&*/ host != serverIP && !HttpContext.Current.Request.IsSecureConnection)
            {
                sslPath = string.Format("https://{0}{1}", HttpContext.Current.Request.ServerVariables["HTTP_HOST"], absolutePath);
            }
            if(!sslPath.EndsWith("/"))
            sslPath += "/";
            try
            {
                object value = new TvBo.Common().getFormConfigValue("General", "FirstPage");
                string currentUrl = (value != null ? (string)value : "ResMonitor" + version + ".aspx");
                HttpContext.Current.Session["Menu"] = mainMenuData.Find(f => string.Equals(f.Url, currentUrl)) != null ? 
                                                            mainMenuData.Find(f => string.Equals(f.Url, currentUrl)).SubMenuItem.ToString() : "ResMonitor";
            }
            catch
            {
                HttpContext.Current.Session["Menu"] = "ResMonitor";
            }

            TvBo.MainMenu currentMenu = mainMenuData.Find(f => string.Equals(f.SubMenuItem, HttpContext.Current.Session["Menu"].ToString()));
            firstMenu = currentMenu.Url + currentMenu.PageExt;

            if (string.IsNullOrEmpty(UserData.UserID))
            {
                if (!string.IsNullOrEmpty(UserData.LoginId))
                    UserData.MasterAgency = false;
                else
                    if (!string.IsNullOrEmpty(UserData.EMail))
                        UserData.MasterAgency = false;
                    else
                        UserData.MasterAgency = true;
            }
            else UserData.MasterAgency = false;

            if (!IpControl(UserData)) {
                errorMsg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "UnauthorizedIpNumber").ToString(), IpAddress());
                if (errorMsg != "")
                    return sslPath + "Default.aspx?Message=" + TvBo.UICommon.EncodeJsString(errorMsg);
                else
                    return sslPath + "Default.aspx";
            }

            if (!UserData.MasterAgency)
            {
                if (new Users().UserControl(ref UserData, IpAddress(), ref errorMsg, HttpContext.Current.Session.SessionID))
                {
                    UserData.HeaderData = new UICommon().getHeaderData(UserData, ref errorMsg);

                    UserData.CheckMarketLang = Equals(ConfigurationManager.AppSettings["CheckMarketLang"], "1");
                    UserData.EqMarketLang = UserData.MarketLang == UserData.Ci.LCID;
                    string restrictionMarketBase  = (string)new TvBo.Common().getFormConfigValue("General", "RestrictMarketForLogin");
                    if(!string.IsNullOrEmpty(restrictionMarketBase))
                    {
                        List<string> _restrictMarkets = restrictionMarketBase.Split('æ').ToList();
                        var extMarket = _restrictMarkets.Find(w => w.IndexOf(UserData.Market) >= 0);
                        if (extMarket != null)
                            errorMsg = extMarket.Split('|')[1];

                    }
                    HttpContext.Current.Session["UserData"] = UserData;
                    return sslPath + firstMenu + (firstMenu.IndexOf('?') > 0 ? "&First=ok" : "?First=ok");
                }
                else
                {
                    HttpContext.Current.Session["UserData"] = UserData;
                    if (errorMsg != "")
                        return sslPath + "Default.aspx?Message=" + TvBo.UICommon.EncodeJsString(errorMsg);
                    else
                        return sslPath + "Default.aspx";
                }
            }
            else
            {
                if (new Users().AgencyControl(ref UserData, IpAddress(), ref errorMsg, HttpContext.Current.Session.SessionID,true))
                {
                    HttpContext.Current.Session["Menu"] = "ResMonitor";
                    firstMenu = "ResMonitor" + version + ".aspx";
                    UserData.HeaderData = new UICommon().getHeaderData(UserData, ref errorMsg);

                    UserData.CheckMarketLang = Equals(ConfigurationManager.AppSettings["CheckMarketLang"], "1");
                    UserData.EqMarketLang = UserData.MarketLang == UserData.Ci.LCID;
                    HttpContext.Current.Session["UserData"] = UserData;
                    return sslPath + firstMenu + (firstMenu.IndexOf('?') > 0 ? "&First=ok" : "?First=ok");
                }
                else
                {
                    HttpContext.Current.Session["UserData"] = UserData;
                    if (errorMsg != "")
                        return sslPath + "Default.aspx?Message=" + TvBo.UICommon.EncodeJsString(errorMsg);
                    else
                        return sslPath + "Default.aspx";
                }
            }
        }

        public string toLogin(User UserData,bool checkAgencyValidation, ref string errorMsg)
        {
            List<TvBo.MainMenu> mainMenuData = CacheObjects.getMainMenuData(UserData, ref errorMsg);
            string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "Version"));
            string firstMenu = string.Empty;
            try
            {
                object value = new TvBo.Common().getFormConfigValue("General", "FirstPage");
                string currentUrl = (value != null ? (string)value : "ResMonitor" + version + ".aspx");
                HttpContext.Current.Session["Menu"] = mainMenuData.Find(f => string.Equals(f.Url, currentUrl)) != null ?
                                                            mainMenuData.Find(f => string.Equals(f.Url, currentUrl)).SubMenuItem.ToString() : "ResMonitor";
            }
            catch
            {
                HttpContext.Current.Session["Menu"] = "ResMonitor";
            }

            TvBo.MainMenu currentMenu = mainMenuData.Find(f => string.Equals(f.SubMenuItem, HttpContext.Current.Session["Menu"].ToString()));
            firstMenu = currentMenu.Url + currentMenu.PageExt;

            if (string.IsNullOrEmpty(UserData.UserID))
            {
                if (!string.IsNullOrEmpty(UserData.LoginId))
                    UserData.MasterAgency = false;
                else
                    if (!string.IsNullOrEmpty(UserData.EMail))
                    UserData.MasterAgency = false;
                else
                    UserData.MasterAgency = true;
            }
            else UserData.MasterAgency = false;

            if (!IpControl(UserData))
            {
                errorMsg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "UnauthorizedIpNumber").ToString(), IpAddress());
                if (errorMsg != "")
                    return WebRoot.BasePageRoot + "Default.aspx?Message=" + TvBo.UICommon.EncodeJsString(errorMsg);
                else
                    return WebRoot.BasePageRoot + "Default.aspx";
            }

            if (!UserData.MasterAgency)
            {
                if (new Users().UserControl(ref UserData, IpAddress(), ref errorMsg, HttpContext.Current.Session.SessionID))
                {
                    UserData.HeaderData = new UICommon().getHeaderData(UserData, ref errorMsg);

                    UserData.CheckMarketLang = Equals(ConfigurationManager.AppSettings["CheckMarketLang"], "1");
                    UserData.EqMarketLang = UserData.MarketLang == UserData.Ci.LCID;
                    HttpContext.Current.Session["UserData"] = UserData;
                    return WebRoot.BasePageRoot + firstMenu + (firstMenu.IndexOf('?') > 0 ? "&First=ok" : "?First=ok");
                }
                else
                {
                    HttpContext.Current.Session["UserData"] = UserData;
                    if (errorMsg != "")
                        return WebRoot.BasePageRoot + "Default.aspx?Message=" + TvBo.UICommon.EncodeJsString(errorMsg);
                    else
                        return WebRoot.BasePageRoot + "Default.aspx";
                }
            }
            else
            {
                if (new Users().AgencyControl(ref UserData, IpAddress(), ref errorMsg, HttpContext.Current.Session.SessionID, checkAgencyValidation))
                {
                    HttpContext.Current.Session["Menu"] = "ResMonitor";
                    firstMenu = "ResMonitor" + version + ".aspx";
                    UserData.HeaderData = new UICommon().getHeaderData(UserData, ref errorMsg);

                    UserData.CheckMarketLang = Equals(ConfigurationManager.AppSettings["CheckMarketLang"], "1");
                    UserData.EqMarketLang = UserData.MarketLang == UserData.Ci.LCID;
                    HttpContext.Current.Session["UserData"] = UserData;
                    return WebRoot.BasePageRoot + firstMenu + (firstMenu.IndexOf('?') > 0 ? "&First=ok" : "?First=ok");
                }
                else
                {
                    HttpContext.Current.Session["UserData"] = UserData;
                    if (errorMsg != "")
                        return WebRoot.BasePageRoot + "Default.aspx?Message=" + TvBo.UICommon.EncodeJsString(errorMsg);
                    else
                        return WebRoot.BasePageRoot + "Default.aspx";
                }
            }
        }
    }
}
