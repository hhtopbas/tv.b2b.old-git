﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace TvBo
{
    public static class EnumHelper
    {

        public static string GetEnumDescription(this System.Enum pEnum)
        {
            Type type = pEnum.GetType();
            var field = type.GetField(pEnum.ToString());

            var customAttribute = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return customAttribute.Length > 0 ? ((DescriptionAttribute)customAttribute[0]).Description : pEnum.ToString();
        }
    }
    public enum TypeOfOwnerShip : short
    {
        [Description("LLC")]
        LLC = 0,
        [Description("Sole Proprietor")]
        SoleProprietor = 1,
        [Description("N/A")]
        NotAvailable =99
    }
    public enum B2BRoleCategory : short
    {
        [Description("Reservation")]
        Reservation = 1,

        [Description("Customer")]
        Customer = 2,

        [Description("Service")]
        Service = 3,

        [Description("Main Menu Monitors")]
        MainMenuMonitors = 4,

        [Description("Main Menu Search")]
        MainMenuSearch = 5,

        [Description("Other")]
        Other = 999
    }


    [AttributeUsage(AttributeTargets.Field)]
    public class B2BRoleAttribute : System.Attribute
    {
        public bool Default { get; set; }
        public string Description { get; set; }
        public int? Order { get; set; }
        public B2BRoleCategory Category { get; set; }

        public B2BRoleAttribute(B2BRoleCategory pCategory, string pDescription, int pOrder = 0, bool pDefault = false)
        {
            this.Category = pCategory;
            this.Description = pDescription;
            if (pOrder > 0)
                this.Order = pOrder;
            this.Default = pDefault;
        }
    }

    public enum B2BRoles
    {
        [B2BRole(B2BRoleCategory.Reservation, "Reservation Cancel", 1, true)]
        ReservastionCancel = 1,

        [B2BRole(B2BRoleCategory.Reservation, "Reservation Date Change", 2, true)]
        ReservationDateChange = 2,

        [B2BRole(B2BRoleCategory.Reservation, "Ignore Lock", 3, false)]
        ReservationIgnoreLock = 3,

        [B2BRole(B2BRoleCategory.Service, "Service add", 1, true)]
        ServiceAdd = 100,

        [B2BRole(B2BRoleCategory.Service, "Service edit", 2, true)]
        ServiceEdit = 101,

        [B2BRole(B2BRoleCategory.Service, "Service delete", 3, true)]
        ServiceDelete = 102,

        [B2BRole(B2BRoleCategory.Customer, "Customer add", 4, true)]
        AddCustomer = 500,

        [B2BRole(B2BRoleCategory.Customer, "Customer edit", 1, true)]
        EditCustomer = 501,

        [B2BRole(B2BRoleCategory.Customer, "Customer delete", 2, true)]
        DeleteCustomer = 502,

        [B2BRole(B2BRoleCategory.Customer, "Customer search", 4, true)]
        SearchCustomer = 504,

        [B2BRole(B2BRoleCategory.Customer, "Black list customer booking", 5, true)]
        BlackListCustomerBooking = 505,

        [B2BRole(B2BRoleCategory.MainMenuMonitors, "Reservation Monitor", 1, true)]
        MenuReservationMonitor = 1000,

        [B2BRole(B2BRoleCategory.MainMenuMonitors, "Payment Monitor", 2, true)]
        MenuPaymentMonitor = 1001,

        [B2BRole(B2BRoleCategory.MainMenuMonitors, "Commision Report", 3, true)]
        MenuCommissionReport = 1002,

        [B2BRole(B2BRoleCategory.MainMenuMonitors, "Stop Sale List", 4, true)]
        MenuStopSale = 1003,

        [B2BRole(B2BRoleCategory.MainMenuSearch, "Package Search", 1, true)]
        MenuPackageSearch = 1100,

        [B2BRole(B2BRoleCategory.MainMenuSearch, "Hotel Search", 2, true)]
        MenuOnlyHotelSearch = 1101,

        [B2BRole(B2BRoleCategory.MainMenuSearch, "Cruise Search", 3, true)]
        MenuCruiseSearch = 1102,

        [B2BRole(B2BRoleCategory.MainMenuSearch, "Tour/Culture Search", 4, true)]
        MenuTourCultureSearch = 1103,

        [B2BRole(B2BRoleCategory.MainMenuSearch, "Flight Search", 5, true)]
        MenuOnlyFlightSearch = 1104,

        [B2BRole(B2BRoleCategory.MainMenuSearch, "Dynamic Package", 6, true)]
        MenuDynamicPackage = 1105,

        [B2BRole(B2BRoleCategory.MainMenuSearch, "Dynamic Hotel", 7, true)]
        MenuDynamicHotelSearch = 1106,

        [B2BRole(B2BRoleCategory.MainMenuSearch, "Only Excursion", 8, true)]
        MenuOnlyExcursionSearch = 1107,

        [B2BRole(B2BRoleCategory.MainMenuSearch, "Only Transfer", 9, true)]
        MenuOnlyTransferSearch = 1108

    }

    public class activeRules
    {
        public Int16 RoleID { get; set; }
        public bool Default { get; set; }
        public activeRules()
        {
            this.Default = true;
        }
    }

    public class AgencyRoles
    {
        public int? RoleID { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public bool Authority { get; set; }
        public Int16? OrderNo { get; set; }
        public AgencyRoles()
        {
        }
    }

    public class AgencyRecord
    {
        public int RecID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string LocalName { get; set; }
        public string NameS { get; set; }
        public string FirmName { get; set; }
        public string BossName { get; set; }
        public string ContName { get; set; }
        public string OprOffice { get; set; }
        public string OprOfficeName { get; set; }
        public string OprOfficeNameL { get; set; }
        public string MainOffice { get; set; }
        public byte WorkType { get; set; }
        public Int16 VocAddrType { get; set; }
        public byte InvoiceTo { get; set; }
        public byte PaymentFrom { get; set; }
        public int? Location { get; set; }
        public string LocationName { get; set; }
        public string LocationLocalName { get; set; }
        public string Address { get; set; }
        public string AddrZip { get; set; }
        public string AddrCity { get; set; }
        public string AddrCountry { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string MobPhone { get; set; }
        public string Fax1 { get; set; }
        public string Fax2 { get; set; }
        public string Www { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string InvAddress { get; set; }
        public string InvAddrZip { get; set; }
        public string InvAddrCity { get; set; }
        public string InvAddrCountry { get; set; }
        public string DocAddress { get; set; }
        public string DocAddrZip { get; set; }
        public string DocAddrCity { get; set; }
        public string DocAddrCountry { get; set; }
        public string DocPhone { get; set; }
        public string DocFax { get; set; }
        public string VocAddr_Address { get; set; }
        public string VocAddr_Zip { get; set; }
        public string VocAddr_City { get; set; }
        public string VocAddr_Country { get; set; }
        public string VocAddr_Phone1 { get; set; }
        public string VocAddr_Phone2 { get; set; }
        public string VocAddr_Fax1 { get; set; }
        public string VocAddr_Fax2 { get; set; }
        public string VocAddr_EMail1 { get; set; }
        public string VocAddr_EMail2 { get; set; }
        public bool BlackList { get; set; }
        public bool AllowMakeRes { get; set; }
        public bool AgreeCheck { get; set; }
        public bool OwnAgency { get; set; }
        public string Bank1 { get; set; }
        public string Bank1BankNo { get; set; }
        public string Bank1AccNo { get; set; }
        public string Bank1IBAN { get; set; }
        public string Bank1Curr { get; set; }
        public string Bank1Location { get; set; }
        public string Bank2 { get; set; }
        public string Bank2BankNo { get; set; }
        public string Bank2AccNo { get; set; }
        public string Bank2IBAN { get; set; }
        public string Bank2Curr { get; set; }
        public string Bank2Location { get; set; }
        public string ACContacName { get; set; }
        public decimal? CreditLimit { get; set; }
        public bool UseSysPay { get; set; }
        public string Curr { get; set; }
        public Int16? PayMode { get; set; }
        public string AfBef { get; set; }
        public Int16? PayModeDay { get; set; }
        public Int16? PayDay { get; set; }
        public string PayType { get; set; }
        public int? Quota { get; set; }
        public string TAcCode { get; set; }
        public bool UseOffMarket { get; set; }
        public Int16 AgencyType { get; set; }
        public bool DocPrtNoPay { get; set; }
        public bool UseMainCont { get; set; }
        public bool UseMainAcc { get; set; }
        public bool UseMainPay { get; set; }
        public byte? SupComType { get; set; }
        public bool UseBonus { get; set; }
        public string AccountCode { get; set; }
        public string RegisterCode { get; set; }
        public string SupplierCode { get; set; }
        public bool AceExport { get; set; }
        public Int16 AceSendTo { get; set; }
        public string AcePath { get; set; }
        public string AceEMail { get; set; }
        public string FtpHost { get; set; }
        public string FtpDir { get; set; }
        public string FtpUser { get; set; }
        public string FtpPas { get; set; }
        public string AceWSAddress { get; set; }
        public string AceWSUserID { get; set; }
        public string AceWSPass { get; set; }
        public string TaxOffice { get; set; }
        public string TaxAccNo { get; set; }
        public bool EarnBonus { get; set; }
        public bool UserEarnBonus { get; set; }
        public Int16 ResPayRule { get; set; }
        public Int16? PayOptTime { get; set; }
        public bool UseSysParamCredit { get; set; }
        public Int16? CreditMode { get; set; }
        public string LicenseNo { get; set; }
        public string CIF { get; set; }
        public decimal? CapSocial { get; set; }
        public string CapSocialCur { get; set; }
        public string LogoUrl { get; set; }
        public bool WebDisp { get; set; }
        public string CuratorUser { get; set; }
        public string MerlinxID { get; set; }
        public bool Aggregator { get; set; }
        public byte? MaxPaxCnt { get; set; }
        public byte? MaxRoomCnt { get; set; }
        public bool? Pxm_Use { get; set; }
        public bool? Pxm_UseLimit { get; set; }
        public bool? CanPayment { get; set; }
        public bool? UseRoles { get; set; }
        public List<AgencyRoles> Roles { get; set; }
        public bool? Pxm_DoNotShowOwnCntry { get; set; }
        public bool? Pxm_DoNotShowOwnCntry_UseMarOpt { get; set; }
        public string IATA { get; set; }
        public string MOHCode { get; set; }
        public bool? VisaReq { get; set; }
        public bool? SaleUmrah { get; set; }
        public bool? SaleHajj { get; set; }
        public bool? SaleIndRes { get; set; }
        public bool? LockSale { get; set; }
        public bool? LockFinance { get; set; }
        public bool? VATPayer { get; set; }
        public DateTime? RegisterDate { get; set; }
        public TypeOfOwnerShip? TypeOfOwnerShip { get; set; }
        public string MasterPass { get; set; }
        public AgencyRecord()
        {
            this.PaymentFrom = 0;
            this.CanPayment = false;
            this.UseRoles = false;
            this.Roles = null;
            this.Pxm_DoNotShowOwnCntry = true;
            this.Pxm_DoNotShowOwnCntry_UseMarOpt = true;
        }
    }

    public class AgencyPasPaymentCategory
    {
        public AgencyPasPaymentCategory()
        {
        }

        Int16? _PayCatID;
        public Int16? PayCatID
        {
            get { return _PayCatID; }
            set { _PayCatID = value; }
        }

        Int16? _OptTime;
        public Int16? OptTime
        {
            get { return _OptTime; }
            set { _OptTime = value; }
        }

    }

    public class MainAgencyData
    {
        public MainAgencyData()
        { }
        string _UseMainCont;

        public string UseMainCont
        {
            get { return _UseMainCont; }
            set { _UseMainCont = value; }
        }
        string _MainOffice;

        public string MainOffice
        {
            get { return _MainOffice; }
            set { _MainOffice = value; }
        }
    }

    public class AgencyDocAddressRecord
    {
        public AgencyDocAddressRecord()
        {
        }

        string _MainOffice;
        public string MainOffice
        {
            get { return _MainOffice; }
            set { _MainOffice = value; }
        }

        bool _UseMainCont;
        public bool UseMainCont
        {
            get { return _UseMainCont; }
            set { _UseMainCont = value; }
        }

        string _VocAddrType;
        public string VocAddrType
        {
            get { return _VocAddrType; }
            set { _VocAddrType = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _FirmName;
        public string FirmName
        {
            get { return _FirmName; }
            set { _FirmName = value; }
        }

        string _DAddress;
        public string DAddress
        {
            get { return _DAddress; }
            set { _DAddress = value; }
        }

        string _Address;
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        string _DAddrZip;
        public string DAddrZip
        {
            get { return _DAddrZip; }
            set { _DAddrZip = value; }
        }

        string _AddrZip;
        public string AddrZip
        {
            get { return _AddrZip; }
            set { _AddrZip = value; }
        }

        string _AddrCity;
        public string AddrCity
        {
            get { return _AddrCity; }
            set { _AddrCity = value; }
        }

        string _DAddrCity;
        public string DAddrCity
        {
            get { return _DAddrCity; }
            set { _DAddrCity = value; }
        }

        string _AddrCountry;
        public string AddrCountry
        {
            get { return _AddrCountry; }
            set { _AddrCountry = value; }
        }

        string _DAddrCountry;
        public string DAddrCountry
        {
            get { return _DAddrCountry; }
            set { _DAddrCountry = value; }
        }

        string _Phone1;
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        string _DPhone;
        public string DPhone
        {
            get { return _DPhone; }
            set { _DPhone = value; }
        }

        string _Fax1;
        public string Fax1
        {
            get { return _Fax1; }
            set { _Fax1 = value; }
        }

        string _DFax;
        public string DFax
        {
            get { return _DFax; }
            set { _DFax = value; }
        }

        string _EMail1;
        public string EMail1
        {
            get { return _EMail1; }
            set { _EMail1 = value; }
        }

        bool _DocPrtNoPay;
        public bool DocPrtNoPay
        {
            get { return _DocPrtNoPay; }
            set { _DocPrtNoPay = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _TaxAccNo;
        public string TaxAccNo
        {
            get { return _TaxAccNo; }
            set { _TaxAccNo = value; }
        }

        string _TaxOffice;
        public string TaxOffice
        {
            get { return _TaxOffice; }
            set { _TaxOffice = value; }
        }

        string _BossName;
        public string BossName
        {
            get { return _BossName; }
            set { _BossName = value; }
        }

        string _InvAddress;
        public string InvAddress
        {
            get { return _InvAddress; }
            set { _InvAddress = value; }
        }

        string _InvAddrZip;
        public string InvAddrZip
        {
            get { return _InvAddrZip; }
            set { _InvAddrZip = value; }
        }

        string _InvAddrCity;
        public string InvAddrCity
        {
            get { return _InvAddrCity; }
            set { _InvAddrCity = value; }
        }

        string _InvAddrCountry;
        public string InvAddrCountry
        {
            get { return _InvAddrCountry; }
            set { _InvAddrCountry = value; }
        }

        string _Bank1;
        public string Bank1
        {
            get { return _Bank1; }
            set { _Bank1 = value; }
        }

        string _Bank1Name;
        public string Bank1Name
        {
            get { return _Bank1Name; }
            set { _Bank1Name = value; }
        }

        string _Bank1BankNo;
        public string Bank1BankNo
        {
            get { return _Bank1BankNo; }
            set { _Bank1BankNo = value; }
        }

        string _Bank1AccNo;
        public string Bank1AccNo
        {
            get { return _Bank1AccNo; }
            set { _Bank1AccNo = value; }
        }

        string _Bank1IBAN;
        public string Bank1IBAN
        {
            get { return _Bank1IBAN; }
            set { _Bank1IBAN = value; }
        }

        string _Bank1Curr;
        public string Bank1Curr
        {
            get { return _Bank1Curr; }
            set { _Bank1Curr = value; }
        }
    }

    public class AgencyAgreeRecord
    {
        public AgencyAgreeRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _HolPack;
        public string HolPack
        {
            get { return _HolPack; }
            set { _HolPack = value; }
        }

        decimal? _DepositAmount;
        public decimal? DepositAmount
        {
            get { return _DepositAmount; }
            set { _DepositAmount = value; }
        }

        string _DepositCur;
        public string DepositCur
        {
            get { return _DepositCur; }
            set { _DepositCur = value; }
        }

        int? _MinPax;
        public int? MinPax
        {
            get { return _MinPax; }
            set { _MinPax = value; }
        }

        int? _MinRes;
        public int? MinRes
        {
            get { return _MinRes; }
            set { _MinRes = value; }
        }

        int? _MinRoom;
        public int? MinRoom
        {
            get { return _MinRoom; }
            set { _MinRoom = value; }
        }

        decimal? _MinGiro;
        public decimal? MinGiro
        {
            get { return _MinGiro; }
            set { _MinGiro = value; }
        }

        string _MinGiroCur;
        public string MinGiroCur
        {
            get { return _MinGiroCur; }
            set { _MinGiroCur = value; }
        }

        DateTime? _SignDate;
        public DateTime? SignDate
        {
            get { return _SignDate; }
            set { _SignDate = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        string _ContractNo;
        public string ContractNo
        {
            get { return _ContractNo; }
            set { _ContractNo = value; }
        }

        string _AgencyAuthority;
        public string AgencyAuthority
        {
            get { return _AgencyAuthority; }
            set { _AgencyAuthority = value; }
        }

        string _OprAuthority;
        public string OprAuthority
        {
            get { return _OprAuthority; }
            set { _OprAuthority = value; }
        }

        string _SignLocation;
        public string SignLocation
        {
            get { return _SignLocation; }
            set { _SignLocation = value; }
        }

        string _Explanation;
        public string Explanation
        {
            get { return _Explanation; }
            set { _Explanation = value; }
        }

        string _AgreeType;
        public string AgreeType
        {
            get { return _AgreeType; }
            set { _AgreeType = value; }
        }

        string _AgreeTypeName;
        public string AgreeTypeName
        {
            get { return _AgreeTypeName; }
            set { _AgreeTypeName = value; }
        }
    }

    public class AgencyAgreeType1Record
    {
        public AgencyAgreeType1Record()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        decimal? _AdlPrice;
        public decimal? AdlPrice
        {
            get { return _AdlPrice; }
            set { _AdlPrice = value; }
        }

        decimal? _ChdPrice;
        public decimal? ChdPrice
        {
            get { return _ChdPrice; }
            set { _ChdPrice = value; }
        }

        decimal? _InfPrice;
        public decimal? InfPrice
        {
            get { return _InfPrice; }
            set { _InfPrice = value; }
        }
    }

    public class AgencySaleSer
    {
        public int? RecID { get; set; }
        public string Agency { get; set; }
        public string ServiceType { get; set; }
        public string Service { get; set; }
        public bool? CanSale { get; set; }
        public AgencySaleSer()
        { 
        }
    }
}
