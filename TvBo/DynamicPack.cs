﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using TvTools;

namespace TvBo
{
    public class DynamicPack
    {
        public List<DynmcRouteToRecord> getRouteTo(User UserData, ref string errorMsg)
        {
            List<DynmcRouteToRecord> records = new List<DynmcRouteToRecord>();
            string tsql = 
@"
Select Distinct
  RecID=ArrCity, 	
  CityName=(Select Name From Location (NoLock) Where RecID=ArrCity),
  CityNameL=dbo.GetLocationNameL(L.City,@Market),
  L.Country,     
  CountryName=(Select Name From Location (NoLock) Where RecID = L.Country),
  CountryNameL=dbo.GetLocationNameL(L.Country,@Market)    
From CatalogPack (NOLOCK) CP 
Join Location (NOLOCK) L ON L.RecID=ArrCity
Where CP.Ready = 'Y'  
  And CP.EndDate > GetDate()-1  
  And CP.WebPub = 'Y'  
  And CP.WebPubDate < GetDate() 
  And dbo.DateOnly(GETDATE()) BetWeen CP.SaleBegDate And CP.SaleEndDate 
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {                
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        DynmcRouteToRecord rec = new DynmcRouteToRecord();
                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.CityName = Conversion.getStrOrNull(R["CityName"]);
                        rec.CityNameL = Conversion.getStrOrNull(R["CityNameL"]);
                        rec.Country = Conversion.getInt32OrNull(R["Country"]);
                        rec.CountryName = Conversion.getStrOrNull(R["CountryName"]);
                        rec.CountryNameL = Conversion.getStrOrNull(R["CountryNameL"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ResDataRecord GenerateResMain(User UserData, ResDataRecord ResData, ref string errorMsg)
        {
            errorMsg = string.Empty;
            SearchType ResType = SearchType.NoSearch;
            ResMainRecord ResMain = new ResMainRecord(null);

            Int16? SaleCurOpt = new TvBo.Reservation().getSaleCurOption(UserData);
            string SaleCur = string.Empty;
            if (!SaleCurOpt.HasValue)
                SaleCur = UserData.SaleCur;
            else
                if (SaleCurOpt.Value == 0)
                    SaleCur = UserData.SaleCur;
                else
                    if (SaleCurOpt.Value == 1)
                        SaleCur = ResData.SelectBook.FirstOrDefault().PLCur;
                    else
                        SaleCur = UserData.SaleCur;

            var rr = from k in ResData.SelectBook
                     select new { Adult = k.HAdult, Child = k.HChdAgeG1 + k.HChdAgeG2 + k.HChdAgeG3 + k.HChdAgeG4 };
            Int16 Adult = Convert.ToInt16(rr.Sum(s => s.Adult).ToString());
            Int16 Child = Convert.ToInt16(rr.Sum(s => s.Child).ToString());            
            try
            {
                ResMain.ResNo = UserData.SID.Substring(0, 10);
                ResMain.Operator = UserData.Operator;
                ResMain.Market = UserData.Market;
                ResMain.Office = UserData.OprOffice;
                ResMain.Agency = UserData.AgencyID;
                ResMain.AgencyName = UserData.AgencyName;
                ResMain.AgencyNameL = UserData.AgencyName;                
                ResMain.BegDate = ResData.SelectBook.FirstOrDefault().CheckIn;
                ResMain.EndDate = ((DateTime)ResData.SelectBook.FirstOrDefault().CheckIn.Value).AddDays(ResData.SelectBook.FirstOrDefault().Night.Value);
                ResMain.Days = ResData.SelectBook.FirstOrDefault().Night.Value;
                ResMain.PriceSource = 0;
                ResMain.PriceListNo = null;
                if (UserData.IsB2CAgency)
                    ResMain.SaleResource = Convert.ToInt16(ResType != SearchType.OnlyFlightSearch ? 4 : 5);
                else ResMain.SaleResource = Convert.ToInt16(ResType != SearchType.OnlyFlightSearch ? 1 : 3);
                ResMain.ResLock = 0;
                ResMain.ResStat = 9;
                ResMain.ConfStat = 0;
                ResMain.AgencyUser = UserData.UserID;
                ResMain.AgencyUserName = UserData.UserName;
                ResMain.AgencyUserNameL = UserData.UserName;
                ResMain.AuthorUser = UserData.UserID;
                ResMain.ResDate = DateTime.Today.Date;
                ResMain.ChgUser = "";
                ResMain.NetCur = ResData.SelectBook.FirstOrDefault().NetCur;
                ResMain.SaleCur = SaleCur;
                ResMain.Ballon = "N";
                ResMain.ReceiveDate = DateTime.Now;
                ResMain.RecCrtDate = DateTime.Now;
                ResMain.RecCrtUser = UserData.UserID;
                ResMain.RecChgUser = "";
                ResMain.Code1 = "";
                ResMain.Code2 = "";
                ResMain.Code3 = "";
                ResMain.Code4 = "";
                ResMain.Adult = Adult;
                ResMain.Child = Child;
                ResMain.ConfToAgency = "N";
                ResMain.PaymentStat = "U";
                ResMain.Discount = 0;
                ResMain.AgencyComMan = "N";
                ResMain.SendInComing = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ? "N" : "W";
                ResMain.AllDocPrint = "N";
                ResMain.SendAgency = "N";
                ResMain.EB = "N";
                ResMain.EBAgency = 0;
                ResMain.EBSupID = 0;
                ResMain.EBSupMaxPer = 0;
                ResMain.EBPasID = 0;
                ResMain.EBAgencyPer = 0;
                ResMain.EBPasPer = 0;
                ResMain.EBPas = 0;
                ResMain.DepCity = ResData.SelectBook.FirstOrDefault().DepCity;
                ResMain.ArrCity = ResData.SelectBook.FirstOrDefault().ArrCity;
                ResMain.Credit = "N";
                ResMain.CreditExpenseCur = "";
                ResMain.Broker = "";
                ResMain.BrokerCom = 0;
                ResMain.BrokerComPer = 0;
                ResMain.PLOperator = ResData.SelectBook.FirstOrDefault().Operator;
                ResMain.PLMarket = ResData.SelectBook.FirstOrDefault().Market;
                ResMain.AgencyBonus = 0;
                ResMain.AgencyBonusAmount = 0;
                ResMain.UserBonus = 0;
                ResMain.UserBonusAmount = 0;
                ResMain.PasBonus = 0;
                ResMain.PasBonusAmount = 0;
                ResMain.PasPayable = 0;
                ResMain.Complete = 0;
                ResMain.ResNote = "";
                ResMain.AgencyDisPasPer = 0;
                ResMain.InvoiceTo = UserData.AgencyRec.InvoiceTo;
                ResMain.WebIP = UserData.IpNumber;
                ResMain.PLSpoChk = 0;
                switch (ResType)
                {
                    case SearchType.TourPackageSearch: ResMain.PackType = "T"; break;
                    case SearchType.OnlyHotelSearch: ResMain.PackType = "O"; break;
                    case SearchType.PackageSearch: ResMain.PackType = "H"; break;
                    default: ResMain.PackType = ""; break;
                }
                ResMain.MemTable = false;
                ResData.ResMain = ResMain;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
            }

            return ResData;
        }
    }
}
