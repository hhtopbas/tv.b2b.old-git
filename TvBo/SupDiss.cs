﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Web;
using TvTools;

namespace TvBo
{
    public class SupDiss
    {
        public SupDisRecord getSupDisDetail(User UserData, string Code, bool forPayment, ref string errorMsg)
        {
            SupDisRecord rec = new SupDisRecord();
            string tsql = string.Empty;
            tsql = @"   Select Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market), [Name]),
	                        Amount, Cur, PerVal, CanChg, SD, 
	                        IncCom, ApplyType, Priority=isnull(Priority,999), Ok, IncComEB, PayPasEB, Market, WebDisp     
                        From Supdis (NOLOCK) Sd
                        Where   Ok='Y' 
                            And WebDisp=1 
                            And Market=@Market
                            And (@ForPayment=1 Or Not Exists(Select * From PayType (NOLOCK) PT Where PT.SupDisCode=Sd.Code))
	                        And Code=@Code
                        Order By Priority";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", System.Data.DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Code", System.Data.DbType.String, Code);
                db.AddInParameter(dbCommand, "ForPayment", System.Data.DbType.Boolean, forPayment);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        rec.Code = Conversion.getStrOrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        rec.SD = Conversion.getStrOrNull(R["SD"]);
                        rec.SDName = HttpContext.GetGlobalResourceObject("LibraryResource", "SDName_" + rec.SD).ToString();
                        rec.Amount = Conversion.getDecimalOrNull(R["Amount"]);
                        rec.Cur = Conversion.getStrOrNull(R["Cur"]);
                        rec.PerVal = Conversion.getDecimalOrNull(R["PerVal"]);
                        rec.IncCom = Equals(R["IncCom"], "Y");
                        rec.IncComEB = Equals(R["IncComEB"], "Y");
                        rec.Priority = (Int16)R["Priority"];
                        rec.CanChg = Equals(R["CanChg"], "Y");
                        rec.ApplyType = Conversion.getStrOrNull(R["ApplyType"]);
                        rec.ApplyTypeName = HttpContext.GetGlobalResourceObject("LibraryResource", "ApplyTypeName_" + rec.ApplyType).ToString();
                        rec.Ok = Equals(R["Ok"], "Y");
                        rec.PayPasEB = (bool)R["PayPasEB"];
                        rec.Market = Conversion.getStrOrNull(R["Market"]);
                        rec.WebDisp = (bool)R["WebDisp"];
                    }
                    return rec;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return rec;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
