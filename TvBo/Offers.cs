﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using TvTools;

namespace TvBo
{
    public class Offers
    {
        public List<ClientOfferService> getPriceListServices(User UserData, int? CatPackID, ref string errorMsg)
        {
            List<ClientOfferService> Result = new List<ClientOfferService>();
            string tsql = string.Empty;
            tsql =
@"
Select Cpp.StepNo,Name=Cpp.[Service],LocalName=IsNull(dbo.FindLocalName(S.NameLID,@Market),S.Name),[Description]=IsNull(S.[Description],'')
From CatPackPlan Cpp (NOLOCK)
Join [Service] S (NOLOCK) ON Cpp.[Service]=S.Code
Where CatPackID=@CatPackID
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, CatPackID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        Result.Add(new ClientOfferService
                        {
                            StepNo = Conversion.getInt32OrNull(R["StepNo"]),
                            Name = Conversion.getStrOrNull(R["Name"]),
                            LocalName = Conversion.getStrOrNull(R["LocalName"]),
                            Description = Conversion.getStrOrNull(R["Description"])
                        });
                    }
                    return Result;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
