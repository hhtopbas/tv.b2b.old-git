﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    [Serializable]
    public class AdmAgencyUserRecord
    {
        public AdmAgencyUserRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Agency;
        public string Agency
        {
            get { return _Agency; }
            set { _Agency = value; }
        }

        string _AgencyName;
        public string AgencyName
        {
            get { return _AgencyName; }
            set { _AgencyName = value; }
        }

        string _AgencyNameLocal;
        public string AgencyNameLocal
        {
            get { return _AgencyNameLocal; }
            set { _AgencyNameLocal = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameLocal;
        public string NameLocal
        {
            get { return _NameLocal; }
            set { _NameLocal = value; }
        }

        string _Pass;
        public string Pass
        {
            get { return _Pass; }
            set { _Pass = value; }
        }

        DateTime? _ExpDate;
        public DateTime? ExpDate
        {
            get { return _ExpDate; }
            set { _ExpDate = value; }
        }

        bool _Status;
        public bool Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        string _PIN;
        public string PIN
        {
            get { return _PIN; }
            set { _PIN = value; }
        }

        string _Phone;
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        string _Mobile;
        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }

        string _EMail;
        public string EMail
        {
            get { return _EMail; }
            set { _EMail = value; }
        }

        string _HAddress;
        public string HAddress
        {
            get { return _HAddress; }
            set { _HAddress = value; }
        }

        bool _ShowAllRes;
        public bool ShowAllRes
        {
            get { return _ShowAllRes; }
            set { _ShowAllRes = value; }
        }

        string _ExpedientID;
        public string ExpedientID
        {
            get { return _ExpedientID; }
            set { _ExpedientID = value; }
        }

    }

    [Serializable]
    public class AdmAgencyRecord
    {
        public AdmAgencyRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _AgencyName;
        public string AgencyName
        {
            get { return _AgencyName; }
            set { _AgencyName = value; }
        }

        string _AgencyNameLocal;
        public string AgencyNameLocal
        {
            get { return _AgencyNameLocal; }
            set { _AgencyNameLocal = value; }
        }

        string _AgencyNameS;
        public string AgencyNameS
        {
            get { return _AgencyNameS; }
            set { _AgencyNameS = value; }
        }

        string _AgencyFirmName;
        public string AgencyFirmName
        {
            get { return _AgencyFirmName; }
            set { _AgencyFirmName = value; }
        }

        string _OprOffice;
        public string OprOffice
        {
            get { return _OprOffice; }
            set { _OprOffice = value; }
        }

        string _OprOfficeName;
        public string OprOfficeName
        {
            get { return _OprOfficeName; }
            set { _OprOfficeName = value; }
        }

        string _OprOfficeNameLocal;
        public string OprOfficeNameLocal
        {
            get { return _OprOfficeNameLocal; }
            set { _OprOfficeNameLocal = value; }
        }

        string _MainOffice;
        public string MainOffice
        {
            get { return _MainOffice; }
            set { _MainOffice = value; }
        }

        bool _Status;
        public bool Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        int? _Location;
        public int? Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _LocationNameLocal;
        public string LocationNameLocal
        {
            get { return _LocationNameLocal; }
            set { _LocationNameLocal = value; }
        }

        Int16 _AgencyType;
        public Int16 AgencyType
        {
            get { return _AgencyType; }
            set { _AgencyType = value; }
        }

        int? _Parent;
        public int? Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }

        string _MasterPass;
        public string MasterPass
        {
            get { return _MasterPass; }
            set { _MasterPass = value; }
        }

        byte _WorkType;
        public byte WorkType
        {
            get { return _WorkType; }
            set { _WorkType = value; }
        }

        bool _WebDisp;
        public bool WebDisp
        {
            get { return _WebDisp; }
            set { _WebDisp = value; }
        }

    }

}
