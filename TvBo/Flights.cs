﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web;
using System.Data.SqlTypes;
using TvTools;
using System.Threading.Tasks;
using System.Diagnostics;

namespace TvBo
{
    public enum enmItemType : short { Seat = 0, Corridor = 1, WC = 2, Cloakroom = 3, Kitchen = 4 }

    public class Flights
    {
        public FirstFlightRecord getFirstFlight(User UserData, string market, string agencyID, int? DepCity, int? ArrCity, bool priceListFlights, ref string errorMsg)
        {
            string tsql = string.Format(@"Declare @AgencyCountry int
                                            Select @AgencyCountry=dbo.GetCountry(Location) From Office O (NOLOCK)						   
                                            Where Exists(Select RecID From Agency (NOLOCK) Where OprOffice=O.Code And Code=@Agency)

                                            Select Top 1 P.FlyDate, D.DepCity, D.ArrCity
                                            From FlightPrice P (NOLOCK)    
                                            Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                                            Where  P.Buyer = '' 
                                              AND P.FlyDate >= dbo.DateOnly(GetDate())                           	
                                              AND Exists(	Select Distinct CP.Operator From CatPackMarket CPM (NOLOCK)
			                                                Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
			                                                Where CP.Ready = 'Y' 
			                                                  And CP.WebPub = 'Y' 
			                                                  And CP.WebPubDate <= GetDate()
			                                                  And CP.SaleEndDate >= dbo.DateOnly(GetDate())
			                                                  And CPM.Market = @Market
			                                                  And CP.Operator = P.Operator
		                                                  )
                                              AND Exists(Select Country From Location L
		                                                 Where RecID=D.DepCity 
		                                                   And Country=@AgencyCountry)
                                              {0} {1}
                                            Order By P.FlyDate, D.DepCity ",
                                                DepCity.HasValue ? "AND D.DepCity=" + DepCity.ToString() : "",
                                                ArrCity.HasValue ? "AND D.ArrCity=" + ArrCity.ToString() : "");

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, agencyID);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        FirstFlightRecord firstFlight = new FirstFlightRecord();
                        firstFlight.FlyDate = Conversion.getDateTimeOrNull(oReader["FlyDate"]);
                        firstFlight.DepCity = Conversion.getInt32OrNull(oReader["DepCity"]);
                        firstFlight.ArrCity = Conversion.getInt32OrNull(oReader["ArrCity"]);
                        return firstFlight;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightDays> getFlightDaysWebWersion0025(User UserData, int? DepCity, int? ArrCity, bool PriceListFlights, string packType, string b2bMenuCat, string holPack, ref string errorMsg)
        {

            bool saleOtherOperator = false;

            if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.Market, "LTAIP"))
            {
                if (string.Equals(UserData.CustomRegID, Common.crID_Go2Holiday) && string.Equals(UserData.Market, "LITHUANIA"))
                    saleOtherOperator = true;
                else
                    saleOtherOperator = false;
            }
            else
            {
                saleOtherOperator = true;
            }

            string tsql = string.Empty;
            if (PriceListFlights)
            {
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                {
                    bool isCountry = false;
                    Location loc = new Locations().getLocation(UserData.Market, ArrCity, ref errorMsg);
                    errorMsg = string.Empty;
                    if (loc != null && loc.Type == 1)
                        isCountry = true;

                    tsql = @"if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack                            
                            Select Distinct CP.Operator,CP.DepCity,CP.DepDepCity,CP.ArrCity,CP.DepArrCity,CP.DepServiceType,CP.BegDate,CP.EndDate,
                                CP.HolPack,CP.AccomNight PLNight,CP.RecId CatPackId,CP.FlightClass
                            Into #CatalogPack
                            From CatPackMarket CPM (NOLOCK)
                            Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                            Where CP.Ready='Y' 
                              And CP.WebPub='Y' 
                              And CP.WebPubDate<=GetDate()
                              And CP.SaleEndDate>=dbo.DateOnly(GetDate())
                              And Exists(Select * From CatPackMarket Where CatPackID=CP.RecID and Market=@Market and (Operator=@Operator or Operator=''))
                              And ((isnull(@B2BMenuCat,'')='' and CP.B2BMenuCat='') or (isnull(@B2BMenuCat,'')='' and CP.B2BMenuCat=Cp.PackType) OR CP.B2BMenuCat=@B2BMenuCat)
                              And CP.PackType=@PackType
                              And CPM.Market=@Market
                            ";
                    if (!string.IsNullOrEmpty(holPack))
                        tsql += string.Format("  And CP.Holpack='{0}' ", holPack);

                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_HolidayPlus) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                    {
                        tsql += @"
                            Select SDate,DCity,DepCity,ACity,ArrCity,SCode,DepLoc,ArrLoc,DepServiceType,DepTime,ArrTime,TDepTime,TArrTime,Night
                            From (
	                            Select * From (
		                            Select Distinct SDate=P.FlyDate,DCity=CP.DepCity,DepCity=CP.DepDepCity,ACity=CP.ArrCity,ArrCity=CP.DepArrCity,SCode=P.FlightNo,
			                            DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,CP.DepServiceType,D.DepTime,D.ArrTime,D.TDepTime,D.TArrTime,Night
		                             From FlightPrice P (NOLOCK)    
		                             /*
                                     HolidayPlus 17971
                                     Join FlightDay D (NOLOCK) ON P.FlightNo=D.FlightNo And P.FlyDate=D.FlyDate
		                             Join #CatalogPack CP ON CP.Operator=P.Operator And CP.DepDepCity=D.DepCity And CP.DepArrCity=D.ArrCity
                                     */
									 JOIN FlightDay D (NOLOCK)  on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate 
									 JOIN HolpackSer S (NOLOCK) ON s.ServiceItem=d.FlightNo                                                                 
									 JOIN HolPackPlan H (NOLOCK) on  H.HolPack=S.Holpack and H.StepNo=S.StepNo and H.Service='FLIGHT' and StepType=1
									 JOIN #CatalogPack CP (NOLOCK) on H.HolPack=CP.HolPack
									 CROSS APPLY split(CP.PLNight,',') AC 
									 OUTER APPLY (Select Top 1 * From FlightDay R(nolock) Where R.DepCity=D.ArrCity and R.ArrCity=D.DepCity AND R.FlyDate=D.FlyDate+cast(AC.Data as Integer)) RF                             
									 CROSS APPLY (Select Top 1 isnull(HotNight,Night) HotNight,Night from CatPriceP (NOLOCK) CPP 
														WHERE CPP.CatPackID=CP.CatPackId and d.FlyDate=CPP.CheckIn and CPP.Night=cast(AC.Data as Integer)) HotNight
		                             Where  P.Buyer = '' 
			                            And CP.DepServiceType='FLIGHT'
			                            And P.FlyDate>=dbo.DateOnly(GetDate())                                      
                                        And D.FlyDate between CP.BegDate And CP.EndDate
										And dbo.ufn_Get_DepRet_FlightSeatWeb(@Operator, @Market,  P.FlightNo, P.FlyDate, Night, FlightClass,null, CP.HolPack, 0,null)>0
	                            ) F 
	                            Union All    
	                            Select * From (
		                            Select Distinct SDate=TD.TransDate,DCity=TD.RouteFrom,DepCity=TD.RouteFrom,ACity=TD.RouteTo,ArrCity=TD.RouteTo,SCode=TD.Bus,
			                            DepLoc=dbo.GetLocationNameL(TD.RouteFrom,@Market),
			                            ArrLoc=dbo.GetLocationNameL(TD.RouteTo,@Market),
			                            CP.DepServiceType,TD.DepTime,ArrTime=null,TDepTime=null,TArrTime=null,Night=null
		                            From TransportPrice TP (NOLOCK)
		                            Join Transport T (NOLOCK) ON T.Code=TP.Transport
		                            Join TransportDay TD (NOLOCK) ON TP.Transport=TD.Transport And TP.TransportDayID=TD.RecID
		                            Join #CatalogPack CP ON CP.DepDepCity=TD.RouteFrom And CP.DepArrCity=TD.RouteTo
		                            Where T.Operator=@Operator
	                            ) T	
                            ) CP
                            Where SDate>=dbo.DateOnly(GetDate())  
                            ";
                    }
                    else
                    {
                        tsql += @"
                            Select SDate,DCity,DepCity,ACity,ArrCity,SCode,DepLoc,ArrLoc,DepServiceType,DepTime,ArrTime,TDepTime,TArrTime
                            From (
	                            Select * From (
		                            Select Distinct SDate=P.FlyDate,DCity=CP.DepCity,DepCity=CP.DepDepCity,ACity=CP.ArrCity,ArrCity=CP.DepArrCity,SCode=P.FlightNo,
			                            DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,CP.DepServiceType,D.DepTime,D.ArrTime,D.TDepTime,D.TArrTime
		                             From FlightPrice P (NOLOCK)    
		                             /*
                                     HolidayPlus 17971
                                     Join FlightDay D (NOLOCK) ON P.FlightNo=D.FlightNo And P.FlyDate=D.FlyDate
		                             Join #CatalogPack CP ON CP.Operator=P.Operator And CP.DepDepCity=D.DepCity And CP.DepArrCity=D.ArrCity
                                     */
									 JOIN FlightDay D (NOLOCK)  on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate 
									 JOIN HolpackSer S (NOLOCK) ON s.ServiceItem=d.FlightNo                                                                 
									 JOIN HolPackPlan H (NOLOCK) on  H.HolPack=S.Holpack and H.StepNo=S.StepNo and H.Service='FLIGHT' and StepType=1
									 JOIN #CatalogPack CP (NOLOCK) on H.HolPack=CP.HolPack
		                             Where  P.Buyer = '' 
			                            And CP.DepServiceType='FLIGHT'
			                            And P.FlyDate>=dbo.DateOnly(GetDate())                                      
                                        And D.FlyDate between CP.BegDate And CP.EndDate
										
	                            ) F 
	                            Union All    
	                            Select * From (
		                            Select Distinct SDate=TD.TransDate,DCity=TD.RouteFrom,DepCity=TD.RouteFrom,ACity=TD.RouteTo,ArrCity=TD.RouteTo,SCode=TD.Bus,
			                            DepLoc=dbo.GetLocationNameL(TD.RouteFrom,@Market),
			                            ArrLoc=dbo.GetLocationNameL(TD.RouteTo,@Market),
			                            CP.DepServiceType,TD.DepTime,ArrTime=null,TDepTime=null,TArrTime=null
		                            From TransportPrice TP (NOLOCK)
		                            Join Transport T (NOLOCK) ON T.Code=TP.Transport
		                            Join TransportDay TD (NOLOCK) ON TP.Transport=TD.Transport And TP.TransportDayID=TD.RecID
		                            Join #CatalogPack CP ON CP.DepDepCity=TD.RouteFrom And CP.DepArrCity=TD.RouteTo
		                            Where T.Operator=@Operator and TD.TransDate>=CP.BegDate-- And SubString(T.DepDays,DATEPART(dw,dbo.DateOnly(TD.TransDate)),1)=1
									and exists(SELECT null from CatPriceP(nolock) where CatPackId=CP.CatPackId and TD.TransDate=CheckIn)
	                            ) T	
                            ) CP
                            Where SDate>=dbo.DateOnly(GetDate())  
                            ";
                    }



                    tsql += string.Format("  And CP.DCity={0} ", DepCity.HasValue ? DepCity.Value : -1);
                    if (ArrCity.HasValue)
                    {
                        if (isCountry)
                            tsql += string.Format(" And Exists(Select RecID From Location (NOLOCK) Where City=CP.ACity And Country={0})", ArrCity.Value);
                        else
                            tsql += string.Format("  And CP.ACity={0} ", ArrCity.HasValue ? ArrCity.Value : -1);
                    }
                    tsql += @"
                              Order By SDate 
                             ";
                }
                else
                {
                    tsql = @"if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack
                            Select Distinct CP.Operator,CP.DepCity,CP.DepDepCity,CP.ArrCity,CP.DepArrCity,CP.DepServiceType,CP.BegDate,CP.EndDate 
                            Into #CatalogPack
                            From CatPackMarket CPM (NOLOCK)
                            Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                            Where CP.Ready='Y' 
                              And CP.WebPub='Y' 
                              And CP.WebPubDate<=GetDate()
                              And CP.SaleEndDate>=dbo.DateOnly(GetDate())
                              And Exists(Select * From CatPackMarket Where CatPackID=CP.RecID and Market=@Market and (Operator=@Operator or Operator=''))
                              And (((isnull(@B2BMenuCat,'')='' and CP.B2BMenuCat='') OR CP.B2BMenuCat=CP.PackType) OR CP.B2BMenuCat=@B2BMenuCat)
                              And CP.PackType=@PackType
                              And CPM.Market=@Market
                            ";
                    if (!string.IsNullOrEmpty(holPack))
                        tsql += string.Format("  And CP.Holpack='{0}' ", holPack);



                    tsql += @"Select SDate,DCity,DepCity,ACity,ArrCity,SCode,DepLoc,ArrLoc,DepServiceType,DepTime,ArrTime
                            From (
	                            Select * From (
		                            Select Distinct SDate=P.FlyDate,DCity=CP.DepCity,DepCity=CP.DepDepCity,ACity=CP.ArrCity,ArrCity=CP.DepArrCity,SCode=P.FlightNo,
			                            DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,CP.DepServiceType,D.DepTime,D.ArrTime
		                             From FlightPrice P (NOLOCK)    
		                             Join FlightDay D (NOLOCK) ON P.FlightNo=D.FlightNo And P.FlyDate=D.FlyDate
		                             Join #CatalogPack CP ON CP.Operator=P.Operator And CP.DepDepCity=D.DepCity And CP.DepArrCity=D.ArrCity
		                             Where  P.Buyer = '' 
			                            And CP.DepServiceType='FLIGHT'
			                            And P.FlyDate>=dbo.DateOnly(GetDate())  
                                        And D.FlyDate between CP.BegDate And CP.EndDate
	                            ) F 
	                            Union All    
	                            Select * From (
		                            Select Distinct SDate=TD.TransDate,DCity=TD.RouteFrom,DepCity=TD.RouteFrom,ACity=TD.RouteTo,ArrCity=TD.RouteTo,SCode=TD.Bus,
			                            DepLoc=dbo.GetLocationNameL(TD.RouteFrom,@Market),
			                            ArrLoc=dbo.GetLocationNameL(TD.RouteTo,@Market),
			                            CP.DepServiceType,TD.DepTime,ArrTime=null
		                            From TransportPrice TP (NOLOCK)
		                            Join Transport T (NOLOCK) ON T.Code=TP.Transport
		                            Join TransportDay TD (NOLOCK) ON TP.Transport=TD.Transport And TP.TransportDayID=TD.RecID
		                            Join #CatalogPack CP ON CP.DepDepCity=TD.RouteFrom And CP.DepArrCity=TD.RouteTo
		                            Where T.Operator=@Operator                                  
	                            ) T	
                            ) CP
                            Where SDate>=dbo.DateOnly(GetDate())  
                            ";
                    tsql += string.Format("  And CP.DCity={0} ", DepCity.HasValue ? DepCity.Value : -1);
                    if (ArrCity.HasValue)
                        tsql += string.Format("  And CP.ACity={0} ", ArrCity.HasValue ? ArrCity.Value : -1);
                    tsql += @"
                              Order By SDate 
                             ";
                }
            }
            else
            {
                if (!saleOtherOperator)
                {
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && VersionControl.CheckWebVersion(UserData.WebVersion, 43, VersionControl.Equality.gt))
                    {
                        tsql = @"
                                    Select Distinct SDate=P.DepDate,D.DepCity,D.ArrCity,SCode=P.DepFlight,DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,DepServiceType='',D.DepTime,D.ArrTime,D.TDepTime,D.TArrTime,P.ORT
                                    From FlightUPrice P (NOLOCK)    
                                    Join FlightDay D (NOLOCK) ON P.DepFlight = D.FlightNo and P.DepDate = D.FlyDate
                                    Where D.WebPub='Y'
                                      And D.StopSale='N'
                                      AND isnull(D.WebPub, 'N')='Y'  
                                      And P.DepDate >= dbo.DateOnly(GetDate())                                      
                                    ";
                    }
                    else
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                        {
                            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                                tsql = @"
                                    Select Distinct SDate=P.DepDate,D.DepCity,D.ArrCity,SCode=P.DepFlight,DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,DepServiceType='',D.DepTime,D.ArrTime,D.TDepTime,D.TArrTime,P.ORT
                                    From FlightUPrice P (NOLOCK)    
                                    Join FlightDay D (NOLOCK) ON P.DepFlight = D.FlightNo and P.DepDate = D.FlyDate
                                    Where D.WebPub='Y'
                                      And D.StopSale='N'
                                      AND isnull(D.WebPub, 'N')='Y'  
                                      And P.DepDate >= dbo.DateOnly(GetDate())
                                      And Operator=@Operator                       
                                    ";

                            else
                                tsql = @"Select Distinct SDate=P.FlyDate,D.DepCity,D.ArrCity,SCode=P.FlightNo,DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,DepServiceType='',D.DepTime,D.ArrTime,D.TDepTime,D.TArrTime
                                 From FlightPrice P (NOLOCK)    
                                 Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                                 Where D.WebPub='Y'
                                   And D.StopSale='N'
                                   AND isnull(D.WebPub, 'N')='Y'
                                   And P.Buyer='' 
                                   And P.FlyDate >= dbo.DateOnly(GetDate())
                                   And Operator=@Operator 
                                ";
                        }
                        else
                            tsql = @"Select Distinct SDate=P.FlyDate,DepCity=D.DepCity,ArrCity=D.ArrCity,SCode=P.FlightNo,DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,DepServiceType='',D.DepTime,D.ArrTime
                                 From FlightPrice P (NOLOCK)    
                                 Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                                 Where D.WebPub='Y'
                                   And D.StopSale='N'
                                   AND isnull(D.WebPub, 'N')='Y'
                                   And P.Buyer='' 
                                   And P.FlyDate >= dbo.DateOnly(GetDate())
                                   And Operator=@Operator 
                                ";
                }
                else
                {
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                        tsql = @"Select Distinct SDate=P.FlyDate,DepCity=D.DepCity,ArrCity=D.ArrCity,SCode=P.FlightNo,DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,DepServiceType='',D.DepTime,D.ArrTime,D.TDepTime,D.TArrTime
                                 From FlightPrice P (NOLOCK)    
                                 Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                                 Where D.WebPub='Y'
                                   And D.StopSale='N'
                                   AND isnull(D.WebPub, 'N')='Y'
                                   And P.Buyer='' 
                                   And P.FlyDate >= dbo.DateOnly(GetDate())                               
                                ";
                    else
                        tsql = @"Select Distinct SDate=P.FlyDate,DepCity=D.DepCity,ArrCity=D.ArrCity,SCode=P.FlightNo,DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,DepServiceType='',D.DepTime,D.ArrTime
                                 From FlightPrice P (NOLOCK)    
                                 Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                                 Where D.WebPub='Y'
                                   And D.StopSale='N'
                                   AND isnull(D.WebPub, 'N')='Y'
                                   And P.Buyer='' 
                                   And P.FlyDate >= dbo.DateOnly(GetDate())                               
                                ";
                }
                tsql += string.Format("  And D.DepCity={0} ", DepCity.HasValue ? DepCity.Value : -1);
                if (ArrCity.HasValue)
                    tsql += string.Format("  And D.ArrCity={0} ", ArrCity.HasValue ? ArrCity.Value : -1);
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                    tsql += @"
                         Order By P.DepDate 
                        ";
                else
                    tsql += @"
                         Order By P.FlyDate 
                        ";
            }

            List<FlightDays> flightDayList = new List<FlightDays>();

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "PackType", DbType.String, packType);
                db.AddInParameter(dbCommand, "B2BMenuCat", DbType.String, b2bMenuCat);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightDays row = new FlightDays();

                        row.FlyDate = (DateTime)oReader["SDate"];

                        row.DepCity = (int)oReader["DepCity"];
                        row.ArrCity = (int)oReader["ArrCity"];
                        row.Flights = Conversion.getStrOrNull(oReader["SCode"]);
                        row.DepAirport = Conversion.getStrOrNull(oReader["DepLoc"]);
                        row.ArrAirport = Conversion.getStrOrNull(oReader["ArrLoc"]);
                        row.ServiceType = Conversion.getStrOrNull(oReader["DepServiceType"]);
                        row.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        row.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                        {
                            row.TDepTime = Conversion.getDateTimeOrNull(oReader["TDepTime"]);
                            row.TArrTime = Conversion.getDateTimeOrNull(oReader["TArrTime"]);
                        }
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_HolidayPlus))
                            row.Night = Conversion.getInt32OrNull(oReader["Night"]);
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && !PriceListFlights)
                        {
                            row.ORT = Conversion.getStrOrNull(oReader["ORT"]);
                        }
                        flightDayList.Add(row);
                    }
                }
                return flightDayList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return flightDayList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightDays> getFlightDays(User UserData, int? DepCity, int? ArrCity, bool PriceListFlights, string packType, string b2bMenuCat, string holPack, ref string errorMsg)
        {
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0024, VersionControl.Equality.gt))
                return getFlightDaysWebWersion0025(UserData, DepCity, ArrCity, PriceListFlights, packType, b2bMenuCat, holPack, ref errorMsg);
            string tsql = string.Empty;
            bool saleOtherOperator = false;

            if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.Market, "LTAIP"))
            {
                if (string.Equals(UserData.CustomRegID, Common.crID_Go2Holiday) && string.Equals(UserData.Market, "LITHUANIA"))
                    saleOtherOperator = true;
                else
                    saleOtherOperator = false;
            }
            else
            {
                saleOtherOperator = true;
            }
            if (PriceListFlights)
            {
                if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0023, VersionControl.Equality.gt))
                {
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                        tsql = @"if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack
                                Select Distinct CP.Operator,CP.DepCity,CP.DepDepCity,CP.ArrCity,CP.DepArrCity,CP.DepServiceType
                                Into #CatalogPack
                                From CatPackMarket CPM (NOLOCK)
                                Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                                Where CP.Ready='Y' 
                                  And CP.WebPub='Y' 
                                  And CP.WebPubDate<=GetDate()
                                  And CP.SaleEndDate>=dbo.DateOnly(GetDate())
                                  And CPM.Market=@Market
                                  And (isnull(@HolPack,'')='' Or CP.HolPack=@HolPack)
                                Select SDate,DCity,DepCity,ACity,ArrCity,SCode,DepLoc,ArrLoc,DepServiceType,DepTime,ArrTime,TDepTime,TArrTime
                                From (
	                                Select * From (
		                                Select Distinct SDate=P.FlyDate,DCity=CP.DepCity,DepCity=CP.DepDepCity,ACity=CP.ArrCity,ArrCity=CP.DepArrCity,SCode=P.FlightNo,
			                                DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,CP.DepServiceType,D.DepTime,D.ArrTime
		                                 From FlightPrice P (NOLOCK)    
		                                 Join FlightDay D (NOLOCK) ON P.FlightNo=D.FlightNo And P.FlyDate=D.FlyDate
		                                 Join #CatalogPack CP ON CP.Operator=P.Operator And CP.DepDepCity=D.DepCity And CP.DepArrCity=D.ArrCity
		                                 Where  P.Buyer = '' 
			                                And CP.DepServiceType='FLIGHT'
			                                And P.FlyDate>=dbo.DateOnly(GetDate())   
	                                ) F 
	                                Union All    
	                                Select * From (
		                                Select Distinct SDate=TD.TransDate,DCity=TD.RouteFrom,DepCity=TD.RouteFrom,ACity=TD.RouteTo,ArrCity=TD.RouteTo,SCode=TD.Bus,
			                                DepLoc=dbo.GetLocationNameL(TD.RouteFrom,@Market),
			                                ArrLoc=dbo.GetLocationNameL(TD.RouteTo,@Market),
			                                CP.DepServiceType,DepTime=null,ArrTime=null
		                                From TransportPrice TP (NOLOCK)
		                                Join Transport T (NOLOCK) ON T.Code=TP.Transport
		                                Join TransportDay TD (NOLOCK) ON TP.Transport=TD.Transport And TP.TransportDayID=TD.RecID
		                                Join #CatalogPack CP ON CP.DepDepCity=TD.RouteFrom And CP.DepArrCity=TD.RouteTo
		                                Where T.Operator=@Operator
	                                ) T	
                                ) CP  
                                Where CP.FlyDate>=dbo.DateOnly(GetDate())
                                ";
                    else
                        tsql = @"if OBJECT_ID('TempDB.dbo.#CatalogPack') is not null Drop Table dbo.#CatalogPack
                                Select Distinct CP.Operator,CP.DepCity,CP.DepDepCity,CP.ArrCity,CP.DepArrCity,CP.DepServiceType
                                Into #CatalogPack
                                From CatPackMarket CPM (NOLOCK)
                                Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                                Where CP.Ready='Y' 
                                  And CP.WebPub='Y' 
                                  And CP.WebPubDate<=GetDate()
                                  And CP.SaleEndDate>=dbo.DateOnly(GetDate())
                                  And CPM.Market=@Market
                                  And (isnull(@HolPack,'')='' Or CP.HolPack=@HolPack)
                                Select SDate,DCity,DepCity,ACity,ArrCity,SCode,DepLoc,ArrLoc,DepServiceType,DepTime,ArrTime,TDepTime=DepTime,TArrTime=ArrTime
                                From (
	                                Select * From (
		                                Select Distinct SDate=P.FlyDate,DCity=CP.DepCity,DepCity=CP.DepDepCity,ACity=CP.ArrCity,ArrCity=CP.DepArrCity,SCode=P.FlightNo,
			                                DepLoc=D.DepAirport,ArrLoc=D.ArrAirport,CP.DepServiceType,D.DepTime,D.ArrTime
		                                 From FlightPrice P (NOLOCK)    
		                                 Join FlightDay D (NOLOCK) ON P.FlightNo=D.FlightNo And P.FlyDate=D.FlyDate
		                                 Join #CatalogPack CP ON CP.Operator=P.Operator And CP.DepDepCity=D.DepCity And CP.DepArrCity=D.ArrCity
		                                 Where  P.Buyer = '' 
			                                And CP.DepServiceType='FLIGHT'
			                                And P.FlyDate>=dbo.DateOnly(GetDate())   
	                                ) F 
	                                Union All    
	                                Select * From (
		                                Select Distinct SDate=TD.TransDate,DCity=TD.RouteFrom,DepCity=TD.RouteFrom,ACity=TD.RouteTo,ArrCity=TD.RouteTo,SCode=TD.Bus,
			                                DepLoc=dbo.GetLocationNameL(TD.RouteFrom,@Market),
			                                ArrLoc=dbo.GetLocationNameL(TD.RouteTo,@Market),
			                                CP.DepServiceType,DepTime=null,ArrTime=null
		                                From TransportPrice TP (NOLOCK)
		                                Join Transport T (NOLOCK) ON T.Code=TP.Transport
		                                Join TransportDay TD (NOLOCK) ON TP.Transport=TD.Transport And TP.TransportDayID=TD.RecID
		                                Join #CatalogPack CP ON CP.DepDepCity=TD.RouteFrom And CP.DepArrCity=TD.RouteTo
		                                Where T.Operator=@Operator
	                                ) T	
                                ) CP  
                                Where CP.FlyDate>=dbo.DateOnly(GetDate())
                                ";
                }
                else
                    if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0023, VersionControl.Equality.gt))
                    {
                        tsql = @"Select Distinct P.FlyDate,DepCity=CP.DepDepCity,ArrCity=CP.DepArrCity,P.FlightNo,D.DepAirport,D.ArrAirport
                                 From FlightPrice P (NOLOCK)    
                                 Join FlightDay D (NOLOCK) ON P.FlightNo=D.FlightNo And P.FlyDate=D.FlyDate
                                 Join (	Select Distinct CP.Operator,CP.DepCity,CP.DepDepCity,CP.ArrCity,CP.DepArrCity From CatPackMarket CPM (NOLOCK)
                                        Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                                        Where CP.Ready = 'Y' 
                                          And CP.WebPub = 'Y' 
                                          And CP.WebPubDate <= GetDate()
                                          And CP.SaleEndDate >= dbo.DateOnly(GetDate())
                                          And CPM.Market = @Market
                                       ) CP ON CP.Operator=P.Operator And CP.DepDepCity=D.DepCity And CP.DepArrCity=D.ArrCity
                                 Where  P.Buyer = '' 
                                    AND P.FlyDate>=dbo.DateOnly(GetDate())
                                ";
                        tsql += string.Format("  And CP.DCity={0} ", DepCity.HasValue ? DepCity.Value : -1);
                        if (ArrCity.HasValue)
                            tsql += string.Format("  And CP.ACity={0} ", ArrCity.HasValue ? ArrCity.Value : -1);
                    }
                    else
                    {
                        tsql = @"Select Distinct P.FlyDate, D.DepCity, D.ArrCity, P.FlightNo, D.DepAirport, D.ArrAirport
                             From FlightPrice P (NOLOCK)    
                             Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                             Where  P.Buyer = '' 
                                AND P.FlyDate >= dbo.DateOnly(GetDate())                                                       
                                AND Exists(	Select Distinct CP.Operator From CatPackMarket CPM (NOLOCK)
                                            Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                                            Where CP.Ready = 'Y' 
                                              And CP.WebPub = 'Y' 
                                              And CP.WebPubDate <= GetDate()
                                              And CP.SaleEndDate >= dbo.DateOnly(GetDate())
                                              And CPM.Market = @Market
                                              And CP.Operator = P.Operator
                                          )
                            ";
                        tsql += string.Format("  And D.DepCity={0} ", DepCity.HasValue ? DepCity.Value : -1);
                        if (ArrCity.HasValue)
                            tsql += string.Format("  And D.ArrCity={0} ", ArrCity.HasValue ? ArrCity.Value : -1);
                    }
            }
            else
            {

                //                if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                //                    tsql = @"Select Distinct P.FlyDate, D.DepCity, D.ArrCity, P.FlightNo, D.DepAirport, D.ArrAirport
                //                             From FlightPrice P (NOLOCK)    
                //                             Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                //                             Where D.WebPub='Y'
                //                               And D.StopSale='N'
                //                               AND isnull(D.WebPub, 'N')='Y'
                //                               And P.Buyer='' 
                //                               And P.FlyDate >= dbo.DateOnly(GetDate())
                //                               And Operator=@Operator 
                //                            ";
                //                else
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && VersionControl.CheckWebVersion(UserData.WebVersion, 44, VersionControl.Equality.lt))
                    tsql = @"Select Distinct FlyDate=P.DepDate,D.DepCity,D.ArrCity,FlightNo=P.DepFlight,D.DepAirport,D.ArrAirport,P.ORT
                        From FlightUPrice P (NOLOCK)    
                        Join FlightDay D (NOLOCK) ON P.DepFlight = D.FlightNo and P.DepDate = D.FlyDate
                        Where D.WebPub='Y'
                          And D.StopSale='N'
                          AND isnull(D.WebPub, 'N')='Y'  
                          And P.DepDate >= dbo.DateOnly(GetDate())
                          And Operator=@Operator                       
                        ";
                else
                    if (!saleOtherOperator)
                        tsql = @"Select Distinct P.FlyDate, D.DepCity, D.ArrCity, P.FlightNo, D.DepAirport, D.ArrAirport
                             From FlightPrice P (NOLOCK)    
                             Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                             Where D.WebPub='Y'
                               And D.StopSale='N'
                               AND isnull(D.WebPub, 'N')='Y'
                               And P.Buyer='' 
                               And P.FlyDate >= dbo.DateOnly(GetDate())                               
                            ";
                    else
                        tsql = @"Select Distinct P.FlyDate, D.DepCity, D.ArrCity, P.FlightNo, D.DepAirport, D.ArrAirport
                             From FlightPrice P (NOLOCK)    
                             Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                             Where D.WebPub='Y'
                               And D.StopSale='N'
                               AND isnull(D.WebPub, 'N')='Y'
                               And P.Buyer='' 
                               And P.FlyDate >= dbo.DateOnly(GetDate())
                               And Operator=@Operator 
                            ";
                tsql += string.Format("  And D.DepCity={0} ", DepCity.HasValue ? DepCity.Value : -1);
                if (ArrCity.HasValue)
                    tsql += string.Format("  And D.ArrCity={0} ", ArrCity.HasValue ? ArrCity.Value : -1);
            }

            List<FlightDays> flightDayList = new List<FlightDays>();

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                if (PriceListFlights)
                {
                    db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                    if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0023, VersionControl.Equality.gt))
                        db.AddInParameter(dbCommand, "HolPack", DbType.String, holPack);
                }
                else
                    if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                        db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightDays row = new FlightDays();
                        row.FlyDate = (DateTime)oReader["FlyDate"];
                        row.DepCity = (int)oReader["DepCity"];
                        row.ArrCity = (int)oReader["ArrCity"];
                        row.Flights = Conversion.getStrOrNull(oReader["FlightNo"]);
                        row.DepAirport = Conversion.getStrOrNull(oReader["DepAirport"]);
                        row.ArrAirport = Conversion.getStrOrNull(oReader["ArrAirport"]);
                        if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0023, VersionControl.Equality.gt))
                        {
                            row.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                            row.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        }
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && !PriceListFlights)
                        {
                            row.ORT = Conversion.getStrOrNull(oReader["ORT"]);
                        }
                        flightDayList.Add(row);
                    }
                }
                return flightDayList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return flightDayList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightDays> getFlightDays(string Market, string Operator, int? DepCity, int? ArrCity, Int16? Direction, Int16? Category, bool PriceListFlights, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (PriceListFlights)
            {
                tsql = @"Select Distinct P.FlyDate,D.DepCity,D.ArrCity,P.FlightNo,D.DepAirport,D.ArrAirport
                         From FlightPrice P (NOLOCK)    
                         Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                         Where  P.Buyer = '' 
                            AND P.FlyDate >= dbo.DateOnly(GetDate())                                                       
                            AND Exists(	Select Distinct CP.Operator From CatPackMarket CPM (NOLOCK)
                                        Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                                        Where CP.Ready = 'Y' 
                                          And CP.WebPub = 'Y' 
                                          And CP.WebPubDate <= GetDate()
                                          And CP.SaleEndDate >= dbo.DateOnly(GetDate())
                                          And CPM.Market = @Market
                                          And CP.Operator = P.Operator
                                          And CP.Direction = @Direction
                                          And CP.Category = @Category
                                      ) ";
            }
            else
            {
                tsql = @"Select Distinct P.FlyDate, D.DepCity, D.ArrCity, P.FlightNo, D.DepAirport, D.ArrAirport
                         From FlightPrice P (NOLOCK)    
                         Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                         Where D.WebPub='Y'
                           And D.StopSale='N'
                           AND isnull(D.WebPub, 'N')='Y'
                           And P.Buyer='' 
                           And P.FlyDate >= dbo.DateOnly(GetDate())
                           And Operator=@Operator 
                        ";
            }
            tsql += string.Format("  And D.DepCity={0} ", DepCity.HasValue ? DepCity.Value : -1);
            if (ArrCity.HasValue)
                tsql += string.Format("  And D.ArrCity={0} ", ArrCity.HasValue ? ArrCity.Value : -1);

            List<FlightDays> flightDayList = new List<FlightDays>();

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                if (PriceListFlights)
                {
                    db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                    db.AddInParameter(dbCommand, "Direction", DbType.Int16, Direction);
                    db.AddInParameter(dbCommand, "Category", DbType.Int16, Category);
                }
                else
                    db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightDays row = new FlightDays
                        {
                            FlyDate = (DateTime)oReader["FlyDate"],
                            DepCity = (int)oReader["DepCity"],
                            ArrCity = (int)oReader["ArrCity"],
                            Flights = Conversion.getStrOrNull(oReader["FlightNo"]),
                            DepAirport = Conversion.getStrOrNull(oReader["DepAirport"]),
                            ArrAirport = Conversion.getStrOrNull(oReader["ArrAirport"])
                        };
                        flightDayList.Add(row);
                    }
                }
                return flightDayList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return flightDayList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightDays> getFlightDays(string Market, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql = @"   Select Distinct P.FlyDate, D.DepCity, D.ArrCity
                         From FlightPrice P (NOLOCK)    
                         Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                         Where  P.Buyer = '' 
                            AND P.FlyDate >= dbo.DateOnly(GetDate())
                            AND Exists(	Select Distinct CP.Operator From CatPackMarket CPM (NOLOCK)
                                        Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                                        Where 		
                                            CP.Ready = 'Y' 
                                            And CP.WebPub = 'Y' 
                                            And CP.WebPubDate <= GetDate()
                                            And CP.SaleEndDate >= dbo.DateOnly(GetDate())
                                            And CPM.Market = @Market
                                            AND CP.Operator = P.Operator
                                       )";
            List<FlightDays> flightDayList = new List<FlightDays>();

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightDays row = new FlightDays
                        {
                            FlyDate = (DateTime)oReader["FlyDate"],
                            DepCity = (int)oReader["DepCity"],
                            ArrCity = (int)oReader["ArrCity"]
                        };
                        flightDayList.Add(row);
                    }
                }
                return flightDayList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return flightDayList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PackPriceFlightDayCache> getFlightDaysPackPrice(ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"   
Declare @toDay DateTime
Set @toDay=dbo.DateOnly(GetDate())
Select Distinct P.MarketID,P.CheckIn,P.DepCity,P.ArrCity,P.PackDepFlight,FD.DepAirport,FD.ArrAirport,FD.DepTime,FD.ArrTime
From FlightDay FD (NOLOCK)
Join PackPriceList P (NOLOCK) ON P.CheckIn=FD.FlyDate and P.PackDepFlight=FD.FlightNo and P.DepCity=FD.DepCity and P.ArrCity=FD.ArrCity
Where P.CheckIn>=@toDay
";

            List<PackPriceFlightDayCache> flightDayList = new List<PackPriceFlightDayCache>();

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        PackPriceFlightDayCache row = new PackPriceFlightDayCache();
                        row.MarketID = (int)oReader["MarketID"];
                        row.FlyDate = (DateTime)oReader["CheckIn"];
                        row.DepCity = (int)oReader["DepCity"];
                        row.ArrCity = (int)oReader["ArrCity"];
                        row.Flights = Conversion.getStrOrNull(oReader["PackDepFlight"]);
                        row.DepAirport = Conversion.getStrOrNull(oReader["DepAirport"]);
                        row.ArrAirport = Conversion.getStrOrNull(oReader["ArrAirport"]);
                        row.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        row.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        flightDayList.Add(row);
                    }
                }
                return flightDayList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return flightDayList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightDayRecord> getFlightDay(User UserData, DateTime BegDate, DateTime EndDate, ref string errorMsg)
        {
            List<FlightDayRecord> records = new List<FlightDayRecord>();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0031, VersionControl.Equality.gt))
                tsql = @"Select FlightNo,Airline,FlgOwner,FlyDate,DepCity,DepAirport,DepTime,ArrCity,ArrAirport,ArrTime,
                            StopSale,BagWeight,MaxInfAge,MaxChdAge,NextFlight,CountCloseTime,WebPub, 
                            AllotUseSysSet,AllotChk,AllotWarnFull,AllotDoNotOver,FlyDur, 
                            MaxTeenAge,WarnUsedAmount,RetFlightOpt,RetFlightUseSysSet,SaleRelease,BagWeightForInf, 
                            TVCCSendPNL,TVCCSendPNLDate,TDepDate,TArrDate,PNLName,TDepTime,TArrTime,AllowPrintTicB2B=isnull(AllowPrintTicB2B, Cast(1 As Bit))
                        From FlightDay (NOLOCK)
                        Where FlyDate between @BegDate And @EndDate ";
            else
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                    tsql = @"Select FlightNo,Airline,FlgOwner,FlyDate,DepCity,DepAirport,DepTime,ArrCity,ArrAirport,ArrTime,
                            StopSale,BagWeight,MaxInfAge,MaxChdAge,NextFlight,CountCloseTime,WebPub, 
                            AllotUseSysSet,AllotChk,AllotWarnFull,AllotDoNotOver,FlyDur, 
                            MaxTeenAge,WarnUsedAmount,RetFlightOpt,RetFlightUseSysSet,SaleRelease,BagWeightForInf, 
                            TVCCSendPNL,TVCCSendPNLDate,TDepDate,TArrDate,PNLName,TDepTime,TArrTime,AllowPrintTicB2B=Cast(1 As Bit)
                        From FlightDay (NOLOCK)
                        Where FlyDate between @BegDate And @EndDate ";
                else
                    tsql = @"Select FlightNo,Airline,FlgOwner,FlyDate,DepCity,DepAirport,DepTime,ArrCity,ArrAirport,ArrTime,
                            StopSale,BagWeight,MaxInfAge,MaxChdAge,NextFlight,CountCloseTime,WebPub, 
                            AllotUseSysSet,AllotChk,AllotWarnFull,AllotDoNotOver,FlyDur, 
                            MaxTeenAge,WarnUsedAmount,RetFlightOpt,RetFlightUseSysSet,SaleRelease,BagWeightForInf, 
                            TVCCSendPNL,TVCCSendPNLDate,TDepDate,TArrDate,PNLName,AllowPrintTicB2B=Cast(1 As Bit)
                        From FlightDay (NOLOCK)
                        Where FlyDate between @BegDate And @EndDate ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, EndDate);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightDayRecord record = new FlightDayRecord();
                        record.FlightNo = (string)oReader["FlightNo"];
                        record.Airline = (string)oReader["Airline"];
                        record.FlgOwner = (string)oReader["FlgOwner"];
                        record.FlyDate = (DateTime)oReader["Flydate"];
                        record.DepCity = (int)oReader["DepCity"];
                        record.DepAirport = (string)oReader["DepAirport"];
                        record.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        record.ArrCity = (int)oReader["ArrCity"];
                        record.ArrAirport = (string)oReader["ArrAirport"];
                        record.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        record.StopSale = Equals(oReader["StopSale"], "Y");
                        record.BagWeight = Conversion.getInt16OrNull(oReader["BagWeight"]);
                        record.MaxInfAge = Conversion.getDecimalOrNull(oReader["MaxInfAge"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        record.MaxTeenAge = Conversion.getDecimalOrNull(oReader["MaxTeenAge"]);
                        record.NextFlight = Conversion.getStrOrNull(oReader["NextFlight"]);
                        record.CountCloseTime = Conversion.getDateTimeOrNull(oReader["CountCloseTime"]);
                        record.WebPub = Equals(oReader["WebPub"], "Y");
                        record.AllotUseSysSet = Equals(oReader["AllotUseSysSet"], "Y");
                        record.AllotChk = Equals(oReader["AllotChk"], "Y");
                        record.AllotWarnFull = Equals(oReader["AllotWarnFull"], "Y");
                        record.AllotDoNotOver = Equals(oReader["AllotDoNotOver"], "Y");
                        record.FlyDur = (int)oReader["FlyDur"];
                        record.WarnUsedAmount = Conversion.getInt16OrNull(oReader["WarnUsedAmount"]);
                        record.RetFlightOpt = Conversion.getByteOrNull(oReader["RetFlightOpt"]);
                        record.RetFlightUseSysSet = Equals(oReader["RetFlightUseSysSet"], "Y");
                        record.SaleRelease = Conversion.getDateTimeOrNull(oReader["SaleRelease"]);
                        record.BagWeightForInf = Conversion.getDecimalOrNull(oReader["BagWeightForInf"]);
                        record.TVCCSendPNL = Conversion.getBoolOrNull(oReader["TVCCSendPNL"]);
                        record.TVCCSendPNLDate = Conversion.getDateTimeOrNull(oReader["TVCCSendPNLDate"]);
                        record.TDepDate = Conversion.getDateTimeOrNull(oReader["TDepDate"]);
                        record.TArrDate = Conversion.getDateTimeOrNull(oReader["TArrDate"]);
                        record.PNLName = Conversion.getStrOrNull(oReader["PNLName"]);
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                        {
                            record.TDepTime = Conversion.getDateTimeOrNull(oReader["TDepTime"]);
                            record.TArrTime = Conversion.getDateTimeOrNull(oReader["TArrTime"]);
                        }
                        else
                        {
                            record.TDepTime = record.DepTime;
                            record.TArrTime = record.ArrTime;
                        }
                        record.AllowPrintTicB2B = Conversion.getBoolOrNull(oReader["AllowPrintTicB2B"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightDayRecord getFlightDay(User UserData, string FlightNo, DateTime FlightDate, ref string errorMsg)
        {
            FlightDayRecord record = new FlightDayRecord();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, VersionControl.WebWersion0031, VersionControl.Equality.gt))
                tsql = @"Select RecID,FlightNo,Airline,FlgOwner,FlyDate,DepCity,DepAirport,DepTime,ArrCity,ArrAirport,ArrTime,TDepDate,TArrDate,
                            StopSale,BagWeight,MaxInfAge,MaxChdAge,NextFlight,CountCloseTime,WebPub, 
                            AllotUseSysSet,AllotChk,AllotWarnFull,AllotDoNotOver,FlyDur, 
                            MaxTeenAge,WarnUsedAmount,RetFlightOpt,RetFlightUseSysSet,SaleRelease,BagWeightForInf, 
                            TVCCSendPNL,TVCCSendPNLDate,TDepDate,TArrDate,PNLName,TDepTime,TArrTime,AllowPrintTicB2B=isnull(AllowPrintTicB2B, Cast(1 As Bit))
                        From FlightDay (NOLOCK)
                        Where dbo.DateOnly(FlyDate)=@FlightDate
                          And FlightNo=@FlightNo
                        ";
            else
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                    tsql = @"Select RecID,FlightNo,Airline,FlgOwner,FlyDate,DepCity,DepAirport,DepTime,ArrCity,ArrAirport,ArrTime,
                                StopSale,BagWeight,MaxInfAge,MaxChdAge,NextFlight,CountCloseTime,WebPub, 
                                AllotUseSysSet,AllotChk,AllotWarnFull,AllotDoNotOver,FlyDur, 
                                MaxTeenAge,WarnUsedAmount,RetFlightOpt,RetFlightUseSysSet,SaleRelease,BagWeightForInf, 
                                TVCCSendPNL,TVCCSendPNLDate,TDepDate,TArrDate,PNLName,TDepTime,TArrTime,AllowPrintTicB2B=Cast(1 As Bit)
                            From FlightDay (NOLOCK)
                            Where dbo.DateOnly(FlyDate)=@FlightDate
                              And FlightNo=@FlightNo
                            ";
                else
                    tsql = @"Select RecID,FlightNo,Airline,FlgOwner,FlyDate,DepCity,DepAirport,DepTime,ArrCity,ArrAirport,ArrTime,
                                StopSale,BagWeight,MaxInfAge,MaxChdAge,NextFlight,CountCloseTime,WebPub, 
                                AllotUseSysSet,AllotChk,AllotWarnFull,AllotDoNotOver,FlyDur, 
                                MaxTeenAge,WarnUsedAmount,RetFlightOpt,RetFlightUseSysSet,SaleRelease,BagWeightForInf, 
                                TVCCSendPNL,TVCCSendPNLDate,TDepDate,TArrDate,PNLName,AllowPrintTicB2B=Cast(1 As Bit)
                            From FlightDay (NOLOCK)
                            Where dbo.DateOnly(FlyDate)=@FlightDate
                              And FlightNo=@FlightNo
                            ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlightDate", DbType.DateTime, FlightDate);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        record.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        record.FlightNo = (string)oReader["FlightNo"];
                        record.Airline = (string)oReader["Airline"];
                        record.FlgOwner = (string)oReader["FlgOwner"];
                        record.FlyDate =oReader["TArrDate"]==DBNull.Value?(DateTime)oReader["Flydate"]:(DateTime)oReader["TArrDate"];
                        record.DepCity = (int)oReader["DepCity"];
                        record.DepAirport = (string)oReader["DepAirport"];
                        record.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        record.ArrCity = (int)oReader["ArrCity"];
                        record.ArrAirport = (string)oReader["ArrAirport"];
                        record.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        record.StopSale = Equals(oReader["StopSale"], "Y");
                        record.BagWeight = Conversion.getInt16OrNull(oReader["BagWeight"]);
                        record.MaxInfAge = Conversion.getDecimalOrNull(oReader["MaxInfAge"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        record.MaxTeenAge = Conversion.getDecimalOrNull(oReader["MaxTeenAge"]);
                        record.NextFlight = Conversion.getStrOrNull(oReader["NextFlight"]);
                        record.CountCloseTime = Conversion.getDateTimeOrNull(oReader["CountCloseTime"]);
                        record.WebPub = Equals(oReader["WebPub"], "Y");
                        record.AllotUseSysSet = Equals(oReader["AllotUseSysSet"], "Y");
                        record.AllotChk = Equals(oReader["AllotChk"], "Y");
                        record.AllotWarnFull = Equals(oReader["AllotWarnFull"], "Y");
                        record.AllotDoNotOver = Equals(oReader["AllotDoNotOver"], "Y");
                        record.FlyDur = (int)oReader["FlyDur"];
                        record.WarnUsedAmount = Conversion.getInt16OrNull(oReader["WarnUsedAmount"]);
                        record.RetFlightOpt = Conversion.getByteOrNull(oReader["RetFlightOpt"]);
                        record.RetFlightUseSysSet = Equals(oReader["RetFlightUseSysSet"], "Y");
                        record.SaleRelease = Conversion.getDateTimeOrNull(oReader["SaleRelease"]);
                        record.BagWeightForInf = Conversion.getDecimalOrNull(oReader["BagWeightForInf"]);
                        record.TVCCSendPNL = Conversion.getBoolOrNull(oReader["TVCCSendPNL"]);
                        record.TVCCSendPNLDate = Conversion.getDateTimeOrNull(oReader["TVCCSendPNLDate"]);
                        record.TDepDate = Conversion.getDateTimeOrNull(oReader["TDepDate"]);
                        record.TArrDate = Conversion.getDateTimeOrNull(oReader["TArrDate"]);
                        record.PNLName = Conversion.getStrOrNull(oReader["PNLName"]);
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030401003"))
                        {
                            record.TDepTime = Conversion.getDateTimeOrNull(oReader["TDepTime"]);
                            record.TArrTime = Conversion.getDateTimeOrNull(oReader["TArrTime"]);
                        }
                        else
                        {
                            record.TDepTime = record.DepTime;
                            record.TArrTime = record.ArrTime;
                        }
                        record.AllowPrintTicB2B = Conversion.getBoolOrNull(oReader["AllowPrintTicB2B"]);
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PLFlights> getPLFlights(User UserData, ResDataRecord ResData, Int32? _BookID, ref string errorMsg)
        {
            string tsql = string.Empty;
            int BookID = _BookID != null ? _BookID.Value : 0;

            List<PLFlights> plFlights = new List<PLFlights>();
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200010"))
            {
                #region SQL Ver: 03.02.00.011
                tsql = @"   SET NOCOUNT ON

                        if not OBJECT_ID('TempDB.dbo.#FlightUse') is null drop table #FlightUse
                          CREATE TABLE #FlightUse
                          (
                            FlightNo varchar(10) Collate database_default,
                            FlyDate DateTime,
                            FlightUnit SmallInt
                          )
                        truncate table #FlightUse

                        --Select * From #FlightUse

                        if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
                          CREATE TABLE #Flight
                          (
                            FlightNo varchar(10),
                            PLFlightNo varchar(10),
                            FlyDate DateTime,
                            Class varchar(2),
                            Supplier varchar(10),
                            AllotType varchar(1),
                            RetAllotType varchar(1),
                            WarnType SmallInt,
                            StepNo SmallInt,
                            NextFlight varchar(10),
                            NextFlyDate DateTime
                          )
                        truncate table #Flight

                        Declare @FlightNo varchar(10), @DefFlightNo varchar(10), @DefRetFlightNo varchar(10), @HolPack varchar(10), @Class varchar(2), @CheckIn DateTime, @Operator varchar(10), @Night SmallInt,
                                    @DepCity int, @ArrCity int, @Departure int, @Arrival int, @FlyDate DateTime, @StepType SmallInt, @AirLine varchar(10),
                                    @NextFlight varchar(10), @NextFlyDate Datetime, @RetFlight varchar(10), @RetFlyDate Datetime, @StepNo int,
                                    @CurStepNo int, @DefCurStepNo int, @Supplier varchar(10),@DepSupplier varchar(10), @AllotType varchar(1), @RetAllotType varchar(1), @FlightStat SmallInt, @OldFlightStat SmallInt, @FreeSeat smallint,
                                    @FlightOk TinyInt, @RetFlightOk TinyInt,
                                    @WarnType SmallInt, @FirstFlight varchar(10), @FirstSupplier varchar(10), @FirstAllotType varchar(1), @FirstDate DateTime, 
                                    @PFAllotChk varchar(1), @PFAllotWarnFull varchar(1), @PFAllotDoNotOver varchar(1), @FlightUnit SmallInt, @MaxInfAge dec(5,2),
                                    @FDAllotChk varchar(1), @FDAllotWarnFull varchar(1), @FDAllotDoNotOver varchar(1), @FDAllotUseSysSet varchar(1),
                                    @CatPackID int, @PRecNo int, @Priority SmallInt, @PLMarket varchar(10), @OldUnit int,@RetFlightOrder SmallInt, @NextFlight2 varchar(10),
                                    @RoomCnt SmallInt, @Adl SmallInt, @Chd1Age dec(5,2), @Chd2Age dec(5,2), @Chd3Age dec(5,2), @Chd4Age dec(5,2), @RoomChd1Age dec(5,2), @RoomChd2Age dec(5,2), @RoomChd3Age dec(5,2), @RoomChd4Age dec(5,2),
                                    @RoomChd1Count int, @RoomChd2Count int, @RoomChd3Count int, @RoomChd4Count int, @ChkNext TinyInt, @NextFlightAllow TinyInt, @BaseFlight varchar(10), @svOperator varchar(10)

                        Set @BaseFlight = @BaseFlight1
                        Set @CatPackID = @CatPackID1
                        Set @PRecNo = @PRecNo1
                        Set @PLMarket = @PLMarket1
                        Set @RoomCnt = @RoomCnt1
                        Set @Adl = @Adl1
                        Set @Chd1Age = @Chd1Age1
                        Set @Chd2Age = @Chd2Age1
                        Set @Chd3Age = @Chd3Age1
                        Set @Chd4Age = @Chd4Age1
                        Set @RoomChd1Age = @RoomChd1Age1
                        Set @RoomChd2Age = @RoomChd2Age1
                        Set @RoomChd3Age = @RoomChd3Age1
                        Set @RoomChd4Age = @RoomChd4Age1
                        Set @RoomChd1Count = @RoomChd1Count1
                        Set @RoomChd2Count = @RoomChd2Count1
                        Set @RoomChd3Count = @RoomChd3Count1
                        Set @RoomChd4Count = @RoomChd4Count1
                        Set @svOperator = @Operator1

                        if isnull(@BaseFlight,'') = '' Set @BaseFlight = '%'

                        Select @FlightUnit = @Adl

                        Select @CurStepNo = -1, @DefCurStepNo = -1, @FlightOk = 0, @WarnType = 0, @FirstFlight = '', @FirstSupplier = '', @FirstAllotType = '', @FirstDate = 0

                        Select @PFAllotChk = pf_AllotChk, @PFAllotWarnFull = pf_AllotWarnFull, @PFAllotDoNotOver = pf_AllotDoNotOver From dbo.ufn_GetMarketParams(@PLMarket,'pf')

                        Declare MyCursor CURSOR LOCAL FAST_FORWARD FOR
                        Select FlightNo, DefFlightNo, HolPack, Class, CheckIn, Operator, DepCity, ArrCity, Departure, Arrival, FlyDate, StepType, NextFlight, StepNo, Night,
                               Priority = isnull((Select Top 1 isnull(FB.SalePriority,0)
                                                  from FlightPrice FP
                                                  join FlightBlock FB on FB.FlightNo = X.FlightNo and FB.RecID = FP.BlockID
                                                  where FP.FlightNo = X.FlightNo and FP.FlyDate = X.FlyDate and FP.Operator = @svOperator),0)
                        From
                        (
                          select S.ServiceItem FlightNo, CS.Service DefFlightNo, P.HolPack, CS.SClass Class,  PP.CheckIn, C.Operator, C.DepCity, C.ArrCity, P.Departure, P.Arrival, X.Night,
                                 FlyDate = case when C.PackType = 'T' then PP.CheckIn + isnull(P.StartDay, 1) - 1
                                                else case when C.DepCity = P.Departure then PP.CheckIn else PP.CheckIn+X.Night end
                                           end,
                                 StepType = isnull(P.StepType,9), S.StepNo,
                                 FlgOrder = Case when CS.Service = S.ServiceItem then 0 else 1 end,
                                 NextFlight = (Select RetCode From Flight where Code = S.ServiceItem)
                          from CatPriceP PP (NOLOCK) 
                          join CatalogPack C (NOLOCK) on C.RecID = PP.CatPackID 
                          join HolPackPlan P (NOLOCK) on P.HolPack = C.HolPack and P.Service = 'FLIGHT' and (P.Departure = C.DepCity or P.Arrival = C.ArrCity)
                          join HolPackSer S (NOLOCK) on S.HolPack = P.HolPack and S.StepNo = P.StepNo 
                          join CatService CS (NOLOCK) on CS.CatPackID=PP.CatPackID and CS.ServiceType = 'FLIGHT' 
                          join CatServiceNdx X (NOLOCK) on X.CatPackID=CS.CatPackID and X.CatServiceID=CS.RecNo and X.CatPricePID = PP.RecNo and X.StepNo = S.StepNo
                          where PP.CatPackID = @CatPackID and PP.RecNo = @PRecNo and S.ServiceItem LIKE @BaseFlight and P.StepType in (1,2)
                        ) X
                        order by StepType, FlyDate, Priority, FlgOrder
                        Open MyCursor
                        Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                        While @@FETCH_STATUS = 0
                        Begin
                          --Select @FlightUnit = @Adl, @ChkNext = 0, @NextFlightAllow = 1
                          Select @FlightUnit = @Adl, @ChkNext = 0, @NextFlightAllow = 1, @RetAllotType = null

                          if @CurStepNo = -1 Set @CurStepNo = @StepNo
                          if @FlightOk = 1 
                            while @CurStepNo = @StepNo and @@FETCH_STATUS = 0
                              Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                          if @@FETCH_STATUS <> 0 break
                          if @CurStepNo <> @StepNo Select @CurStepNo = @StepNo, @FlightOk = 0, @FirstFlight = '', @FirstSupplier = '', @FirstAllotType = '', @FirstDate = 0, @FlightOk = 0
                         
                          Delete From #Flight
                          Delete From #FlightUse

                          Declare SupCursor CURSOR LOCAL FAST_FORWARD FOR  
                          Select Distinct P.Supplier, B.SalePriority, D.MaxInfAge
                          from FlightDay D (NOLOCK)
                          Join FlightPrice P (NOLOCK) on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                          Join FlightBlock B (NOLOCK) on B.FlightNo = D.FlightNo and B.RecID = P.BlockID
                          where D.FlightNo = @FlightNo and D.FlyDate = @FlyDate and isnull(P.Supplier,'') <> '' and 
                            --P.Operator = @svOperator
                            (P.Operator=@svOperator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID = D.RecID And Operator = @svOperator))
                          order by B.SalePriority
                          Open SupCursor
                          Fetch Next FROM SupCursor INTO @Supplier, @Priority, @MaxInfAge
                          While @@FETCH_STATUS = 0
                          Begin  
                            select @FlightStat = 0, @FreeSeat = 0, @AllotType = 'H', @WarnType = 0, @FlightOk = 0
                            select @FDAllotChk = AllotChk, @FDAllotWarnFull = AllotWarnFull, @FDAllotDoNotOver = AllotDoNotOver, @FDAllotUseSysSet = AllotUseSysSet From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate

                            if @Chd1Age>0 and @RoomChd1Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd1Count
                            if @Chd2Age>0 and @RoomChd2Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd2Count
                            if @Chd3Age>0 and @RoomChd3Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd3Count
                            if @Chd4Age>0 and @RoomChd4Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd4Count

                            --Select @FlightUnit = @FlightUnit * @RoomCnt, @OldUnit = 0
                            Select @FlightUnit = @FlightUnit, @OldUnit = 0 -- 2013.06.17 Fatih bey
                            Select @OldUnit = FlightUnit From #FlightUse where FlightNo = @FlightNo and FlyDate = @FlyDate
                            Set @FlightUnit = @FlightUnit + @OldUnit
                            
                            if (@FDAllotUseSysSet = 'Y' and @PFAllotChk = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotChk = 'Y')
                            begin
                              execute sp_CheckFlightAlllot @FlightNo, @FlyDate, @Class, @svOperator, @Supplier, '', @AllotType, @FlightUnit, @ResDate, @FlightStat output, @FreeSeat output,@Night
                              if @FlightStat<> 0
                              begin
                                Select @OldFlightStat = @FlightStat, @AllotType = 'S'
                                execute sp_CheckFlightAlllot @FlightNo, @FlyDate, @Class, @svOperator, @Supplier, '', @AllotType, @FlightUnit, @ResDate, @FlightStat output, @FreeSeat output,@Night
                                if @FlightStat = 0
                                  Set @FlightOk = 1
                                else
                                  Set @FlightStat = @OldFlightStat
                              end
                              else
                                Set @FlightOk = 1

                              if @FlightOk = 0
                              begin
                                if (@FDAllotUseSysSet = 'Y' and @PFAllotWarnFull = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotWarnFull = 'Y')
                                begin
                                  if @FlightStat = -1 Set @WarnType = -1 --Allotment not found
                                  if @FlightStat = 1 Set @WarnType = 1   --Allotment not enough
                                end

                                if not ((@FDAllotUseSysSet = 'Y' and @PFAllotDoNotOver = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotDoNotOver = 'Y'))
                                  if @FirstFlight = '' Select @FirstFlight = @FlightNo, @FirstSupplier = @Supplier, @FirstAllotType = @AllotType, @FirstDate = @FlyDate
                              end
                              else
                                Select @FlightOk = 1, @FirstFlight = @FlightNo, @FirstSupplier = @Supplier, @FirstAllotType = @AllotTYpe, @FirstDate = @FlyDate
                            end
                            else
                              Set @FlightOk = 1

                            if @FlightOk = 1 break
                            Fetch Next FROM SupCursor INTO @Supplier, @Priority, @MaxInfAge
                          end
                          Close SupCursor
                          DeAllocate SupCursor
                          Set @DepSupplier=@Supplier
                          if @FlightOk = 1
                          begin      
                            if @StepType = 1 
                            begin
                              Set @RetFlightOk = 0
                              Set @ChkNext = 0
                              if exists (Select * From FlightDay where FlightNo = @FlightNo and FlyDate = @FlyDate and isnull(RetFlightUseSysSet, 'N') = 'N')
                                Select @ChkNext = RetFlightOpt From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate
                              else
                                if exists (Select * From ParamFlight where isnull(UseSysParam, 'N') = 'N')
                                  Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = @PLMarket
                                else
                                  Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = ''

                              if @ChkNext = 1 and isnull(@NextFlight,'') <> '' -- Return Only Next Flight
                              begin
                                Select @RetFlight = @NextFlight, @RetFlyDate = @CheckIn + @Night

                                Select Top 1 @DefRetFlightNo = S.Service, @DefCurStepNo = X.StepNo
                                From CatService S (NOLOCK)
                                join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo
                                join CatPackPlan P (NOLOCK) on P.CatPackID = S.CatPackID and P.StepNo = X.StepNo
                                Where S.CatPackID=@CatPackID and X.CatPricePID=@PRecNo and P.StepType = 2";
                if (Convert.ToDecimal(UserData.TvVersion) >= Convert.ToDecimal(070005013))
                {
                    tsql += " execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output,@Night";
                }
                else
                {
                    tsql += " execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output";
                }

                tsql += @"
                                if @RetFlightOk = 1
                                begin
                                  if exists (select * from #Flight where StepNo = @DefCurStepNo) 
                                    delete from #Flight where StepNo = @DefCurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                                   
                                  if not exists (select * from #Flight where StepNo = @DefCurStepNo)  
                                  begin
                                    insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                        (@RetFlight, @DefRetFlightNo, @RetFlyDate, @Class, @Supplier, @AllotType, @AllotType, @WarnType, @DefCurStepNo, @RetFlight, @RetFlyDate)
                                    if exists (Select * From #FlightUse where FlightNo = @RetFlight and FlyDate = @RetFlyDate)
                                      update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @RetFlight and FlyDate = @RetFlyDate
                                    else
                                      insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@RetFlight, @RetFlyDate, @FlightUnit)
                                  end            
                                end 
                              end
                              else
                        --        if @ChkNext = 2 -- Return Only own Airline
                                begin            
                                  Set @RetFlyDate = @CheckIn + @Night
                                  
                                  Set @AirLine = '%'
                                  if @ChkNext = 2 -- Return Only own Airline
                                    Select @AirLine = AirLine From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate

                                  Select @NextFlight2 = NextFlight From FlightDay where FlightNo = @FlightNo and FlyDate = @RetFlyDate
                                  Select Top 1 @DefRetFlightNo = S.Service, @DefCurStepNo = X.StepNo
                                  From CatService S (NOLOCK)
                                  join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo
                                  join CatPackPlan P (NOLOCK) on P.CatPackID = S.CatPackID and P.StepNo = X.StepNo
                                  Where S.CatPackID=@CatPackID and X.CatPricePID=@PRecNo and S.ServiceType = 'FLIGHT' and P.StepType = 2

                                  Declare FCursor CURSOR LOCAL FAST_FORWARD FOR  
                                  Select Distinct S.ServiceItem, Priority = case when S.ServiceItem = @NextFlight2 then 0 else 1 end
                                  from CatalogPack C (NOLOCK)
                                  join HolPackPlan P (NOLOCK) on P.HolPack = C.HolPack and P.Service = 'FLIGHT' and P.Departure <> C.DepCity --and P.Arrival = C.ArrCity
                                  join HolPackSer S (NOLOCK) on S.HolPack = P.HolPack and S.StepNo = P.StepNo 
                                  join FlightDay F (NOLOCK) on F.FlightNo = S.ServiceItem and F.FlyDate = @RetFlyDate
                                  Where C.RecID=@CatPackID and S.Service = 'FLIGHT' and P.StepType = 2 and F.AirLine LIKE @AirLine
                                  Order By Priority
                                  Open FCursor
                                  Fetch Next FROM FCursor INTO @RetFlight, @RetFlightOrder
                                  While @@FETCH_STATUS = 0
                                  Begin     ";
                if (Convert.ToDecimal(UserData.TvVersion) >= Convert.ToDecimal(070005013))
                {
                    tsql += " execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output,@Night";
                }
                else
                {
                    tsql += " execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output";
                }

                tsql += @"       
                                    
                                    if @RetFlightOk = 1
                                    begin
                                      if exists (select * from #Flight where StepNo = @DefCurStepNo) 
                                        delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                                       
                                      if not exists (select * from #Flight where StepNo = @DefCurStepNo)  
                                      begin
                                        insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                            (@RetFlight, @DefRetFlightNo, @RetFlyDate, @Class, @Supplier, @AllotType, @AllotType, @WarnType, @DefCurStepNo, @RetFlight, @RetFlyDate)
                                        if exists (Select * From #FlightUse where FlightNo = @RetFlight and FlyDate = @RetFlyDate)
                                          update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @RetFlight and FlyDate = @RetFlyDate
                                        else
                                          insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@RetFlight, @RetFlyDate, @FlightUnit)
                                      end            
                                      break
                                    end               
                                    Fetch Next FROM FCursor INTO @RetFlight, @RetFlightOrder
                                  end
                                  Close FCursor
                                  DeAllocate FCursor
                                end         
                              if @RetFlightOk = 0 and @ChkNext > 0 Select @FlightOk = 0, @NextFlightAllow = 0
                            end
                          end

                          if @FlightOk = 1
                          begin
                        --    if exists (select * from #Flight where StepNo = @CurStepNo and isnull(FlightNo,'') = '' ) delete from #Flight where StepNo = @CurStepNo 
                            if @FlightNo <> ''
                              if exists (select * from #Flight where StepNo = @CurStepNo) delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                             
                            if not exists (select * from #Flight where StepNo = @CurStepNo)  
                            begin
                              insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                  (@FlightNo, @DefFlightNo, @FlyDate, @Class, @DepSupplier, @AllotType, isnull(@RetAllotType, @AllotType), @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                              if exists (Select * From #FlightUse where FlightNo = @FlightNo and FlyDate = @FlyDate)
                                update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @FlightNo and FlyDate = @FlyDate
                              else
                                insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@FlightNo, @FlyDate, @FlightUnit)
                            end
                        --    break
                          end

                          if @FlightOk = 0
                          begin
                        --    if @NextFlightAllow = 0 Set @WarnType = 100
                            Set @AllotType = @FirstAllotType
                            Set @RetAllotType = @AllotType
                            if @NextFlightAllow = 0 
                              Set @RetAllotType = 'O'
                            else
                              Set @AllotType = 'O'

                            if @FirstFlight <> ''
                            begin
                              if @FlightNo <> ''
                                if exists (select * from #Flight where StepNo = @CurStepNo) delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)

                              if not exists (select * from #Flight where StepNo = @CurStepNo)  
                              begin 
                                insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                    (@FirstFlight, @DefFlightNo, @FirstDate, @Class, @FirstSupplier, @AllotType, isnull(@RetAllotType, @AllotType), @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                                if exists (Select * From #FlightUse where FlightNo = @FirstFlight and FlyDate = @FirstDate)
                                  update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @FirstFlight and FlyDate = @FirstDate
                                else
                                  insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@FirstFlight, @FirstDate, @FlightUnit)
                              end
                            end
                            else
                              if not exists (select * from #Flight where StepNo = @CurStepNo)
                                insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                    ('', @DefFlightNo, @FlyDate, '', '', '', '', @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                          end

                          Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                        end
                        Close MyCursor
                        DeAllocate MyCursor
                        SET NOCOUNT OFF

                        Select * From #Flight 
                        order by FlyDate ";
            }
            #endregion
            else
                #region SQL
                tsql = @"   SET NOCOUNT ON

                        if not OBJECT_ID('TempDB.dbo.#FlightUse') is null drop table #FlightUse
                          CREATE TABLE #FlightUse
                          (
                            FlightNo varchar(10) Collate database_default,
                            FlyDate DateTime,
                            FlightUnit SmallInt
                          )
                        truncate table #FlightUse

                        --Select * From #FlightUse

                        if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
                          CREATE TABLE #Flight
                          (
                            FlightNo varchar(10),
                            PLFlightNo varchar(10),
                            FlyDate DateTime,
                            Class varchar(2),
                            Supplier varchar(10),
                            AllotType varchar(1),
                            RetAllotType varchar(1),
                            WarnType SmallInt,
                            StepNo SmallInt,
                            NextFlight varchar(10),
                            NextFlyDate DateTime
                          )
                        truncate table #Flight

                        Declare @FlightNo varchar(10), @DefFlightNo varchar(10), @DefRetFlightNo varchar(10), @HolPack varchar(10), @Class varchar(2), @CheckIn DateTime, @Operator varchar(10), @Night SmallInt,
                                    @DepCity int, @ArrCity int, @Departure int, @Arrival int, @FlyDate DateTime, @StepType SmallInt, @AirLine varchar(10),
                                    @NextFlight varchar(10), @NextFlyDate Datetime, @RetFlight varchar(10), @RetFlyDate Datetime, @StepNo int,
                                    @CurStepNo int, @DefCurStepNo int, @Supplier varchar(10),@DepSupplier varchar(10), @AllotType varchar(1), @RetAllotType varchar(1), @FlightStat SmallInt, @OldFlightStat SmallInt, @FreeSeat smallint,
                                    @FlightOk TinyInt, @RetFlightOk TinyInt,
                                    @WarnType SmallInt, @FirstFlight varchar(10), @FirstSupplier varchar(10), @FirstAllotType varchar(1), @FirstDate DateTime, 
                                    @PFAllotChk varchar(1), @PFAllotWarnFull varchar(1), @PFAllotDoNotOver varchar(1), @FlightUnit SmallInt, @MaxInfAge dec(5,2),
                                    @FDAllotChk varchar(1), @FDAllotWarnFull varchar(1), @FDAllotDoNotOver varchar(1), @FDAllotUseSysSet varchar(1),
                                    @CatPackID int, @PRecNo int, @Priority SmallInt, @PLMarket varchar(10), @OldUnit int,@RetFlightOrder SmallInt, @NextFlight2 varchar(10),
                                    @RoomCnt SmallInt, @Adl SmallInt, @Chd1Age dec(5,2), @Chd2Age dec(5,2), @Chd3Age dec(5,2), @Chd4Age dec(5,2), @RoomChd1Age dec(5,2), @RoomChd2Age dec(5,2), @RoomChd3Age dec(5,2), @RoomChd4Age dec(5,2),
                                    @RoomChd1Count int, @RoomChd2Count int, @RoomChd3Count int, @RoomChd4Count int, @ChkNext TinyInt, @NextFlightAllow TinyInt, @BaseFlight varchar(10), @svOperator varchar(10)

                        Set @BaseFlight = @BaseFlight1
                        Set @CatPackID = @CatPackID1
                        Set @PRecNo = @PRecNo1
                        Set @PLMarket = @PLMarket1
                        Set @RoomCnt = @RoomCnt1
                        Set @Adl = @Adl1
                        Set @Chd1Age = @Chd1Age1
                        Set @Chd2Age = @Chd2Age1
                        Set @Chd3Age = @Chd3Age1
                        Set @Chd4Age = @Chd4Age1
                        Set @RoomChd1Age = @RoomChd1Age1
                        Set @RoomChd2Age = @RoomChd2Age1
                        Set @RoomChd3Age = @RoomChd3Age1
                        Set @RoomChd4Age = @RoomChd4Age1
                        Set @RoomChd1Count = @RoomChd1Count1
                        Set @RoomChd2Count = @RoomChd2Count1
                        Set @RoomChd3Count = @RoomChd3Count1
                        Set @RoomChd4Count = @RoomChd4Count1
                        Set @svOperator = @Operator1

                        if isnull(@BaseFlight,'') = '' Set @BaseFlight = '%'

                        Select @FlightUnit = @Adl

                        Select @CurStepNo = -1, @DefCurStepNo = -1, @FlightOk = 0, @WarnType = 0, @FirstFlight = '', @FirstSupplier = '', @FirstAllotType = '', @FirstDate = 0

                        Select @PFAllotChk = pf_AllotChk, @PFAllotWarnFull = pf_AllotWarnFull, @PFAllotDoNotOver = pf_AllotDoNotOver From dbo.ufn_GetMarketParams(@PLMarket,'pf')

                        Declare MyCursor CURSOR LOCAL FAST_FORWARD FOR
                        Select FlightNo, DefFlightNo, HolPack, Class, CheckIn, Operator, DepCity, ArrCity, Departure, Arrival, FlyDate, StepType, NextFlight, StepNo, Night,
                               Priority = isnull((Select Top 1 isnull(FB.SalePriority,0)
                                                  from FlightPrice FP
                                                  join FlightBlock FB on FB.FlightNo = X.FlightNo and FB.RecID = FP.BlockID
                                                  where FP.FlightNo = X.FlightNo and FP.FlyDate = X.FlyDate and FP.Operator = @svOperator),0)
                        From
                        (
                          select S.ServiceItem FlightNo, CS.Service DefFlightNo, P.HolPack, CS.SClass Class,  PP.CheckIn, C.Operator, C.DepCity, C.ArrCity, P.Departure, P.Arrival, X.Night,
                                 FlyDate = case when C.PackType = 'T' then PP.CheckIn + isnull(P.StartDay, 1) - 1
                                                else case when C.DepCity = P.Departure then PP.CheckIn else PP.CheckIn+X.Night end
                                           end,
                                 StepType = isnull(P.StepType,9), S.StepNo,
                                 FlgOrder = Case when CS.Service = S.ServiceItem then 0 else 1 end,
                                 NextFlight = (Select RetCode From Flight where Code = S.ServiceItem)
                          from CatPriceP PP (NOLOCK) 
                          join CatalogPack C (NOLOCK) on C.RecID = PP.CatPackID 
                          join HolPackPlan P (NOLOCK) on P.HolPack = C.HolPack and P.Service = 'FLIGHT' and P.Departure = C.DepCity and P.Arrival = C.ArrCity 
                          join HolPackSer S (NOLOCK) on S.HolPack = P.HolPack and S.StepNo = P.StepNo 
                          join CatService CS (NOLOCK) on CS.CatPackID=PP.CatPackID and CS.ServiceType = 'FLIGHT' 
                          join CatServiceNdx X (NOLOCK) on X.CatPackID=CS.CatPackID and X.CatServiceID=CS.RecNo and X.CatPricePID = PP.RecNo and X.StepNo = S.StepNo
                          where PP.CatPackID = @CatPackID and PP.RecNo = @PRecNo and S.ServiceItem LIKE @BaseFlight
                        ) X
                        order by StepType, FlyDate, Priority, FlgOrder
                        Open MyCursor
                        Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                        While @@FETCH_STATUS = 0
                        Begin
                          --Select @FlightUnit = @Adl, @ChkNext = 0, @NextFlightAllow = 1
                          Select @FlightUnit = @Adl, @ChkNext = 0, @NextFlightAllow = 1, @RetAllotType = null

                          if @CurStepNo = -1 Set @CurStepNo = @StepNo
                          if @FlightOk = 1 
                            while @CurStepNo = @StepNo and @@FETCH_STATUS = 0
                              Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                          if @@FETCH_STATUS <> 0 break
                          if @CurStepNo <> @StepNo Select @CurStepNo = @StepNo, @FlightOk = 0, @FirstFlight = '', @FirstSupplier = '', @FirstAllotType = '', @FirstDate = 0, @FlightOk = 0
                         
                          Delete From #Flight
                          Delete From #FlightUse

                          Declare SupCursor CURSOR LOCAL FAST_FORWARD FOR  
                          Select Distinct P.Supplier, B.SalePriority, D.MaxInfAge
                          from FlightDay D (NOLOCK)
                          Join FlightPrice P (NOLOCK) on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                          Join FlightBlock B (NOLOCK) on B.FlightNo = D.FlightNo and B.RecID = P.BlockID
                          where D.FlightNo = @FlightNo and D.FlyDate = @FlyDate and isnull(P.Supplier,'') <> '' and P.Operator = @svOperator
                          order by B.SalePriority
                          Open SupCursor
                          Fetch Next FROM SupCursor INTO @Supplier, @Priority, @MaxInfAge
                          While @@FETCH_STATUS = 0
                          Begin  
                            select @FlightStat = 0, @FreeSeat = 0, @AllotType = 'H', @WarnType = 0, @FlightOk = 0
                            select @FDAllotChk = AllotChk, @FDAllotWarnFull = AllotWarnFull, @FDAllotDoNotOver = AllotDoNotOver, @FDAllotUseSysSet = AllotUseSysSet From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate

                            if @Chd1Age>0 and @RoomChd1Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd1Count
                            if @Chd2Age>0 and @RoomChd2Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd2Count
                            if @Chd3Age>0 and @RoomChd3Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd3Count
                            if @Chd4Age>0 and @RoomChd4Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd4Count

                            Select @FlightUnit = @FlightUnit * @RoomCnt, @OldUnit = 0
                            Select @OldUnit = FlightUnit From #FlightUse where FlightNo = @FlightNo and FlyDate = @FlyDate
                            Set @FlightUnit = @FlightUnit + @OldUnit
                            
                            if (@FDAllotUseSysSet = 'Y' and @PFAllotChk = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotChk = 'Y')
                            begin
                              execute sp_CheckFlightAlllot @FlightNo, @FlyDate, @Class, @svOperator, @Supplier, '', @AllotType, @FlightUnit, @FlightStat output, @FreeSeat output
                              if @FlightStat<> 0
                              begin
                                Select @OldFlightStat = @FlightStat, @AllotType = 'S'
                                execute sp_CheckFlightAlllot @FlightNo, @FlyDate, @Class, @svOperator, @Supplier, '', @AllotType, @FlightUnit, @FlightStat output, @FreeSeat output
                                if @FlightStat = 0
                                  Set @FlightOk = 1
                                else
                                  Set @FlightStat = @OldFlightStat
                              end
                              else
                                Set @FlightOk = 1

                              if @FlightOk = 0
                              begin
                                if (@FDAllotUseSysSet = 'Y' and @PFAllotWarnFull = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotWarnFull = 'Y')
                                begin
                                  if @FlightStat = -1 Set @WarnType = -1 --Allotment not found
                                  if @FlightStat = 1 Set @WarnType = 1   --Allotment not enough
                                end

                                if not ((@FDAllotUseSysSet = 'Y' and @PFAllotDoNotOver = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotDoNotOver = 'Y'))
                                  if @FirstFlight = '' Select @FirstFlight = @FlightNo, @FirstSupplier = @Supplier, @FirstAllotType = @AllotType, @FirstDate = @FlyDate
                              end
                              else
                                Select @FlightOk = 1, @FirstFlight = @FlightNo, @FirstSupplier = @Supplier, @FirstAllotType = @AllotTYpe, @FirstDate = @FlyDate
                            end
                            else
                              Set @FlightOk = 1

                            if @FlightOk = 1 break
                            Fetch Next FROM SupCursor INTO @Supplier, @Priority, @MaxInfAge
                          end
                          Close SupCursor
                          DeAllocate SupCursor
                          Set @DepSupplier=@Supplier
                          if @FlightOk = 1
                          begin      
                            if @StepType = 1 
                            begin
                              Set @RetFlightOk = 0
                              Set @ChkNext = 0
                              if exists (Select * From FlightDay where FlightNo = @FlightNo and FlyDate = @FlyDate and isnull(RetFlightUseSysSet, 'N') = 'N')
                                Select @ChkNext = RetFlightOpt From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate
                              else
                                if exists (Select * From ParamFlight where isnull(UseSysParam, 'N') = 'N')
                                  Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = @PLMarket
                                else
                                  Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = ''

                              if @ChkNext = 1 and isnull(@NextFlight,'') <> '' -- Return Only Next Flight
                              begin
                                Select @RetFlight = @NextFlight, @RetFlyDate = @CheckIn + @Night

                                Select Top 1 @DefRetFlightNo = S.Service, @DefCurStepNo = X.StepNo
                                From CatService S (NOLOCK)
                                join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo
                                join CatPackPlan P (NOLOCK) on P.CatPackID = S.CatPackID and P.StepNo = X.StepNo
                                Where S.CatPackID=@CatPackID and X.CatPricePID=@PRecNo and P.StepType = 2

                                execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output

                                if @RetFlightOk = 1
                                begin
                                  if exists (select * from #Flight where StepNo = @DefCurStepNo) 
                                    delete from #Flight where StepNo = @DefCurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                                   
                                  if not exists (select * from #Flight where StepNo = @DefCurStepNo)  
                                  begin
                                    insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                        (@RetFlight, @DefRetFlightNo, @RetFlyDate, @Class, @Supplier, @AllotType, @AllotType, @WarnType, @DefCurStepNo, @RetFlight, @RetFlyDate)
                                    if exists (Select * From #FlightUse where FlightNo = @RetFlight and FlyDate = @RetFlyDate)
                                      update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @RetFlight and FlyDate = @RetFlyDate
                                    else
                                      insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@RetFlight, @RetFlyDate, @FlightUnit)
                                  end            
                                end 
                              end
                              else
                        --        if @ChkNext = 2 -- Return Only own Airline
                                begin            
                                  Set @RetFlyDate = @CheckIn + @Night
                                  
                                  Set @AirLine = '%'
                                  if @ChkNext = 2 -- Return Only own Airline
                                    Select @AirLine = AirLine From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate

                                  Select @NextFlight2 = NextFlight From FlightDay where FlightNo = @FlightNo and FlyDate = @RetFlyDate
                                  Select Top 1 @DefRetFlightNo = S.Service, @DefCurStepNo = X.StepNo
                                  From CatService S (NOLOCK)
                                  join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo
                                  join CatPackPlan P (NOLOCK) on P.CatPackID = S.CatPackID and P.StepNo = X.StepNo
                                  Where S.CatPackID=@CatPackID and X.CatPricePID=@PRecNo and S.ServiceType = 'FLIGHT' and P.StepType = 2

                                  Declare FCursor CURSOR LOCAL FAST_FORWARD FOR  
                                  Select Distinct S.ServiceItem, Priority = case when S.ServiceItem = @NextFlight2 then 0 else 1 end
                                  from CatalogPack C (NOLOCK)
                                  join HolPackPlan P (NOLOCK) on P.HolPack = C.HolPack and P.Service = 'FLIGHT' and P.Departure <> C.DepCity --and P.Arrival = C.ArrCity
                                  join HolPackSer S (NOLOCK) on S.HolPack = P.HolPack and S.StepNo = P.StepNo 
                                  join FlightDay F (NOLOCK) on F.FlightNo = S.ServiceItem and F.FlyDate = @RetFlyDate
                                  Where C.RecID=@CatPackID and S.Service = 'FLIGHT' and P.StepType = 2 and F.AirLine LIKE @AirLine
                                  Order By Priority
                                  Open FCursor
                                  Fetch Next FROM FCursor INTO @RetFlight, @RetFlightOrder
                                  While @@FETCH_STATUS = 0
                                  Begin            
                                    execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output
                                    if @RetFlightOk = 1
                                    begin
                                      if exists (select * from #Flight where StepNo = @DefCurStepNo) 
                                        delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                                       
                                      if not exists (select * from #Flight where StepNo = @DefCurStepNo)  
                                      begin
                                        insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                            (@RetFlight, @DefRetFlightNo, @RetFlyDate, @Class, @Supplier, @AllotType, @AllotType, @WarnType, @DefCurStepNo, @RetFlight, @RetFlyDate)
                                        if exists (Select * From #FlightUse where FlightNo = @RetFlight and FlyDate = @RetFlyDate)
                                          update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @RetFlight and FlyDate = @RetFlyDate
                                        else
                                          insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@RetFlight, @RetFlyDate, @FlightUnit)
                                      end            
                                      break
                                    end               
                                    Fetch Next FROM FCursor INTO @RetFlight, @RetFlightOrder
                                  end
                                  Close FCursor
                                  DeAllocate FCursor
                                end         
                              if @RetFlightOk = 0 and @ChkNext > 0 Select @FlightOk = 0, @NextFlightAllow = 0
                            end
                          end

                          if @FlightOk = 1
                          begin
                        --    if exists (select * from #Flight where StepNo = @CurStepNo and isnull(FlightNo,'') = '' ) delete from #Flight where StepNo = @CurStepNo 
                            if @FlightNo <> ''
                              if exists (select * from #Flight where StepNo = @CurStepNo) delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                             
                            if not exists (select * from #Flight where StepNo = @CurStepNo)  
                            begin
                              insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                  (@FlightNo, @DefFlightNo, @FlyDate, @Class, @DepSupplier, @AllotType, isnull(@RetAllotType, @AllotType), @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                              if exists (Select * From #FlightUse where FlightNo = @FlightNo and FlyDate = @FlyDate)
                                update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @FlightNo and FlyDate = @FlyDate
                              else
                                insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@FlightNo, @FlyDate, @FlightUnit)
                            end
                        --    break
                          end

                          if @FlightOk = 0
                          begin
                        --    if @NextFlightAllow = 0 Set @WarnType = 100
                            Set @AllotType = @FirstAllotType
                            Set @RetAllotType = @AllotType
                            if @NextFlightAllow = 0 
                              Set @RetAllotType = 'O'
                            else
                              Set @AllotType = 'O'

                            if @FirstFlight <> ''
                            begin
                              if @FlightNo <> ''
                                if exists (select * from #Flight where StepNo = @CurStepNo) delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)

                              if not exists (select * from #Flight where StepNo = @CurStepNo)  
                              begin 
                                insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                    (@FirstFlight, @DefFlightNo, @FirstDate, @Class, @FirstSupplier, @AllotType, isnull(@RetAllotType, @AllotType), @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                                if exists (Select * From #FlightUse where FlightNo = @FirstFlight and FlyDate = @FirstDate)
                                  update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @FirstFlight and FlyDate = @FirstDate
                                else
                                  insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@FirstFlight, @FirstDate, @FlightUnit)
                              end
                            end
                            else
                              if not exists (select * from #Flight where StepNo = @CurStepNo)
                                insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                    ('', @DefFlightNo, @FlyDate, '', '', '', '', @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                          end

                          Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                        end
                        Close MyCursor
                        DeAllocate MyCursor
                        SET NOCOUNT OFF

                        Select * From #Flight 
                        order by FlyDate ";
                #endregion

            SearchResult SelectBook;
            try
            {
                SelectBook = ResData.SelectBook.Find(f => f.RefNo == BookID);
                if (SelectBook == null)
                    return plFlights;
            }
            catch
            {
                return plFlights;
            }

            int roomCount = 1;
            if (ResData.SelectBook.Where(w => !string.IsNullOrEmpty(w.Hotel)).Count() > 0)
                roomCount = ResData.SelectBook.Where(w => !string.IsNullOrEmpty(w.Hotel)).Count();
            int Adult = Convert.ToInt32(SelectBook.HAdult);
            if (ResData.SelectBook.Where(w => !string.IsNullOrEmpty(w.Hotel)).Count() > 1)
                Adult = ResData.SelectBook.Sum(s => s.HAdult);

            int chdAge1 = Convert.ToInt32(SelectBook.HChdAgeG1);
            if (ResData.SelectBook.Where(w => !string.IsNullOrEmpty(w.Hotel)).Count() > 1)
                chdAge1 = ResData.SelectBook.Sum(s => s.HChdAgeG1);

            int chdAge2 = Convert.ToInt32(SelectBook.HChdAgeG2);
            if (ResData.SelectBook.Where(w => !string.IsNullOrEmpty(w.Hotel)).Count() > 1)
                chdAge2 = ResData.SelectBook.Sum(s => s.HChdAgeG2);

            int chdAge3 = Convert.ToInt32(SelectBook.HChdAgeG3);
            if (ResData.SelectBook.Where(w => !string.IsNullOrEmpty(w.Hotel)).Count() > 1)
                chdAge3 = ResData.SelectBook.Sum(s => s.HChdAgeG3);

            int chdAge4 = Convert.ToInt32(SelectBook.HChdAgeG4);
            if (ResData.SelectBook.Where(w => !string.IsNullOrEmpty(w.Hotel)).Count() > 1)
                chdAge4 = ResData.SelectBook.Sum(s => s.HChdAgeG4);

            decimal? roomChd1Age1 = SelectBook.ChdG1Age2;
            decimal? roomChd2Age1 = SelectBook.ChdG2Age2;
            decimal? roomChd3Age1 = SelectBook.ChdG3Age2;
            decimal? roomChd4Age1 = SelectBook.ChdG4Age2;
            int roomChd1Count1 = chdAge1;
            int roomChd2Count1 = chdAge2;
            int roomChd3Count1 = chdAge3;
            int roomChd4Count1 = chdAge4;

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "BaseFlight1", DbType.String, SelectBook.DepFlight);
                db.AddInParameter(dbCommand, "CatPackID1", DbType.Int32, SelectBook.CatPackID);
                db.AddInParameter(dbCommand, "PRecNo1", DbType.Int32, SelectBook.PRecNo);
                db.AddInParameter(dbCommand, "PLMarket1", DbType.String, SelectBook.Market);
                db.AddInParameter(dbCommand, "RoomCnt1", DbType.Int32, roomCount);
                db.AddInParameter(dbCommand, "Adl1", DbType.Int32, Adult);
                db.AddInParameter(dbCommand, "Chd1Age1", DbType.Int32, chdAge1);
                db.AddInParameter(dbCommand, "Chd2Age1", DbType.Int32, chdAge2);
                db.AddInParameter(dbCommand, "Chd3Age1", DbType.Int32, chdAge3);
                db.AddInParameter(dbCommand, "Chd4Age1", DbType.Int32, chdAge4);
                db.AddInParameter(dbCommand, "RoomChd1Age1", DbType.Decimal, roomChd1Age1);
                db.AddInParameter(dbCommand, "RoomChd2Age1", DbType.Decimal, roomChd2Age1);
                db.AddInParameter(dbCommand, "RoomChd3Age1", DbType.Decimal, roomChd3Age1);
                db.AddInParameter(dbCommand, "RoomChd4Age1", DbType.Decimal, roomChd4Age1);
                db.AddInParameter(dbCommand, "RoomChd1Count1", DbType.Int32, roomChd1Count1);
                db.AddInParameter(dbCommand, "RoomChd2Count1", DbType.Int32, roomChd2Count1);
                db.AddInParameter(dbCommand, "RoomChd3Count1", DbType.Int32, roomChd3Count1);
                db.AddInParameter(dbCommand, "RoomChd4Count1", DbType.Int32, roomChd4Count1);
                db.AddInParameter(dbCommand, "Operator1", DbType.String, SelectBook.Operator);
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200010"))
                    db.AddInParameter(dbCommand, "ResDate", DbType.DateTime, ResData.ResMain != null && ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value : Convert.DBNull);


                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        PLFlights row = new PLFlights
                        {
                            AllotType = Conversion.getStrOrNull(oReader["AllotType"]),
                            Class = Conversion.getStrOrNull(oReader["Class"]),
                            FlightNo = Conversion.getStrOrNull(oReader["FlightNo"]),
                            FlyDate = (DateTime)oReader["FlyDate"],
                            NextFlight = Conversion.getStrOrNull(oReader["NextFlight"]),
                            NextFlyDate = Conversion.getDateTimeOrNull(oReader["NextFlyDate"]),
                            PLFlightNo = Conversion.getStrOrNull(oReader["PLFlightNo"]),
                            RetAllotType = Conversion.getStrOrNull(oReader["RetAllotType"]),
                            StepNo = Convert.ToInt16(oReader["StepNo"]),
                            Supplier = Conversion.getStrOrNull(oReader["Supplier"]),
                            WarnType = Convert.ToInt16(oReader["WarnType"])
                        };
                        plFlights.Add(row);
                    }
                }
                return plFlights;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return plFlights;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PLFlights> getPLFlights(User UserData, DataSet ResData, Int32? _BookID, ref string errorMsg)
        {
            string tsql = string.Empty;
            int BookID = _BookID != null ? _BookID.Value : 0;

            List<PLFlights> plFlights = new List<PLFlights>();

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200010"))
                #region SQL Ver:03.02.00.011
                tsql = @"SET NOCOUNT ON
                        if not OBJECT_ID('TempDB.dbo.#FlightUse') is null drop table #FlightUse
                          CREATE TABLE #FlightUse
                          (
                            FlightNo varchar(10) Collate database_default,
                            FlyDate DateTime,
                            FlightUnit SmallInt
                          )
                        truncate table #FlightUse

                        --Select * From #FlightUse

                        if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
                          CREATE TABLE #Flight
                          (
                            FlightNo varchar(10),
                            PLFlightNo varchar(10),
                            FlyDate DateTime,
                            Class varchar(2),
                            Supplier varchar(10),
                            AllotType varchar(1),
                            RetAllotType varchar(1),
                            WarnType SmallInt,
                            StepNo SmallInt,
                            NextFlight varchar(10),
                            NextFlyDate DateTime
                          )
                        truncate table #Flight

                        Declare @FlightNo varchar(10), @DefFlightNo varchar(10), @DefRetFlightNo varchar(10), @HolPack varchar(10), @Class varchar(2), @CheckIn DateTime, @Operator varchar(10), @Night SmallInt,
                                    @DepCity int, @ArrCity int, @Departure int, @Arrival int, @FlyDate DateTime, @StepType SmallInt, @AirLine varchar(10),
                                    @NextFlight varchar(10), @NextFlyDate Datetime, @RetFlight varchar(10), @RetFlyDate Datetime, @StepNo int,
                                    @CurStepNo int, @DefCurStepNo int, @Supplier varchar(10),@DepSupplier varchar(10), @AllotType varchar(1), @RetAllotType varchar(1), @FlightStat SmallInt, @OldFlightStat SmallInt, @FreeSeat smallint,
                                    @FlightOk TinyInt, @RetFlightOk TinyInt,
                                    @WarnType SmallInt, @FirstFlight varchar(10), @FirstSupplier varchar(10), @FirstAllotType varchar(1), @FirstDate DateTime, 
                                    @PFAllotChk varchar(1), @PFAllotWarnFull varchar(1), @PFAllotDoNotOver varchar(1), @FlightUnit SmallInt, @MaxInfAge dec(5,2),
                                    @FDAllotChk varchar(1), @FDAllotWarnFull varchar(1), @FDAllotDoNotOver varchar(1), @FDAllotUseSysSet varchar(1),
                                    @CatPackID int, @PRecNo int, @Priority SmallInt, @PLMarket varchar(10), @OldUnit int,
                                    @RoomCnt SmallInt, @Adl SmallInt, @Chd1Age dec(5,2), @Chd2Age dec(5,2), @Chd3Age dec(5,2), @Chd4Age dec(5,2), @RoomChd1Age dec(5,2), @RoomChd2Age dec(5,2), @RoomChd3Age dec(5,2), @RoomChd4Age dec(5,2),
                                    @RoomChd1Count int, @RoomChd2Count int, @RoomChd3Count int, @RoomChd4Count int, @ChkNext TinyInt, @NextFlightAllow TinyInt, @BaseFlight varchar(10), @svOperator varchar(10)

                        Set @BaseFlight = @BaseFlight1
                        Set @CatPackID = @CatPackID1
                        Set @PRecNo = @PRecNo1
                        Set @PLMarket = @PLMarket1
                        Set @RoomCnt = @RoomCnt1
                        Set @Adl = @Adl1
                        Set @Chd1Age = @Chd1Age1
                        Set @Chd2Age = @Chd2Age1
                        Set @Chd3Age = @Chd3Age1
                        Set @Chd4Age = @Chd4Age1
                        Set @RoomChd1Age = @RoomChd1Age1
                        Set @RoomChd2Age = @RoomChd2Age1
                        Set @RoomChd3Age = @RoomChd3Age1
                        Set @RoomChd4Age = @RoomChd4Age1
                        Set @RoomChd1Count = @RoomChd1Count1
                        Set @RoomChd2Count = @RoomChd2Count1
                        Set @RoomChd3Count = @RoomChd3Count1
                        Set @RoomChd4Count = @RoomChd4Count1
                        Set @svOperator = @Operator1

                        if isnull(@BaseFlight,'') = '' Set @BaseFlight = '%'

                        Select @FlightUnit = @Adl

                        Select @CurStepNo = -1, @DefCurStepNo = -1, @FlightOk = 0, @WarnType = 0, @FirstFlight = '', @FirstSupplier = '', @FirstAllotType = '', @FirstDate = 0

                        Select @PFAllotChk = pf_AllotChk, @PFAllotWarnFull = pf_AllotWarnFull, @PFAllotDoNotOver = pf_AllotDoNotOver From dbo.ufn_GetMarketParams(@PLMarket,'pf')

                        Declare MyCursor CURSOR LOCAL FAST_FORWARD FOR
                        Select FlightNo, DefFlightNo, HolPack, Class, CheckIn, Operator, DepCity, ArrCity, Departure, Arrival, FlyDate, StepType, NextFlight, StepNo, Night,
                               Priority = isnull((Select Top 1 isnull(FB.SalePriority,0)
                                                  from FlightPrice FP
                                                  join FlightBlock FB on FB.FlightNo = X.FlightNo and FB.RecID = FP.BlockID
                                                  where FP.FlightNo = X.FlightNo and FP.FlyDate = X.FlyDate and FP.Operator = @svOperator),0)
                        From
                        (
                          select S.ServiceItem FlightNo, CS.Service DefFlightNo, P.HolPack, CS.SClass Class,  PP.CheckIn, C.Operator, C.DepCity, C.ArrCity, P.Departure, P.Arrival, X.Night,
                                 FlyDate = case when C.PackType = 'T' then PP.CheckIn + isnull(P.StartDay, 1) - 1
                                                else case when C.DepCity = P.Departure then PP.CheckIn else PP.CheckIn+X.Night end
                                           end,
                                 StepType = isnull(P.StepType,9), S.StepNo,
                                 FlgOrder = Case when CS.Service = S.ServiceItem then 0 else 1 end,
                                 NextFlight = (Select RetCode From Flight where Code = S.ServiceItem)
                          from CatPriceP PP (NOLOCK) 
                          join CatalogPack C (NOLOCK) on C.RecID = PP.CatPackID 
                          join HolPackPlan P (NOLOCK) on P.HolPack = C.HolPack and P.Service = 'FLIGHT' and P.Departure = C.DepCity and P.Arrival = C.ArrCity 
                          join HolPackSer S (NOLOCK) on S.HolPack = P.HolPack and S.StepNo = P.StepNo 
                          join CatService CS (NOLOCK) on CS.CatPackID=PP.CatPackID and CS.ServiceType = 'FLIGHT' 
                          join CatServiceNdx X (NOLOCK) on X.CatPackID=CS.CatPackID and X.CatServiceID=CS.RecNo and X.CatPricePID = PP.RecNo and X.StepNo = S.StepNo
                          where PP.CatPackID = @CatPackID and PP.RecNo = @PRecNo and S.ServiceItem LIKE @BaseFlight
                        ) X
                        order by StepType, FlyDate, Priority, FlgOrder
                        Open MyCursor
                        Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                        While @@FETCH_STATUS = 0
                        Begin
                          Select @FlightUnit = @Adl, @ChkNext = 0, @NextFlightAllow = 1
                          if @CurStepNo = -1 Set @CurStepNo = @StepNo
                          if @FlightOk = 1 
                            while @CurStepNo = @StepNo and @@FETCH_STATUS = 0
                              Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                          if @@FETCH_STATUS <> 0 break
                          if @CurStepNo <> @StepNo Select @CurStepNo = @StepNo, @FlightOk = 0, @FirstFlight = '', @FirstSupplier = '', @FirstAllotType = '', @FirstDate = 0, @FlightOk = 0

                          Declare SupCursor CURSOR LOCAL FAST_FORWARD FOR  
                          Select Distinct P.Supplier, B.SalePriority, D.MaxInfAge
                          from FlightDay D (NOLOCK)
                          Join FlightPrice P (NOLOCK) on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                          Join FlightBlock B (NOLOCK) on B.FlightNo = D.FlightNo and B.RecID = P.BlockID
                          where D.FlightNo = @FlightNo and D.FlyDate = @FlyDate and isnull(P.Supplier,'') <> '' and 
                          --P.Operator = @svOperator
                          (P.Operator=@svOperator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID = D.RecID And Operator = @svOperator))
                          order by B.SalePriority
                          Open SupCursor
                          Fetch Next FROM SupCursor INTO @Supplier, @Priority, @MaxInfAge
                          While @@FETCH_STATUS = 0
                          Begin  
                            select @FlightStat = 0, @FreeSeat = 0, @AllotType = 'H', @WarnType = 0, @FlightOk = 0
                            select @FDAllotChk = AllotChk, @FDAllotWarnFull = AllotWarnFull, @FDAllotDoNotOver = AllotDoNotOver, @FDAllotUseSysSet = AllotUseSysSet From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate

                            if @Chd1Age>0 and @RoomChd1Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd1Count
                            if @Chd2Age>0 and @RoomChd2Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd2Count
                            if @Chd3Age>0 and @RoomChd3Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd3Count
                            if @Chd4Age>0 and @RoomChd4Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd4Count

                            Select @FlightUnit = @FlightUnit * @RoomCnt, @OldUnit = 0
                            Select @OldUnit = FlightUnit From #FlightUse where FlightNo = @FlightNo and FlyDate = @FlyDate
                            Set @FlightUnit = @FlightUnit + @OldUnit
                            
                            if (@FDAllotUseSysSet = 'Y' and @PFAllotChk = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotChk = 'Y')
                            begin
                              execute sp_CheckFlightAlllot @FlightNo, @FlyDate, @Class, @svOperator, @Supplier, '', @AllotType, @FlightUnit, @ResDate, @FlightStat output, @FreeSeat output
                              if @FlightStat<> 0
                              begin
                                Select @OldFlightStat = @FlightStat, @AllotType = 'S'
                                execute sp_CheckFlightAlllot @FlightNo, @FlyDate, @Class, @svOperator, @Supplier, '', @AllotType, @FlightUnit, @ResDate, @FlightStat output, @FreeSeat output
                                if @FlightStat = 0
                                  Set @FlightOk = 1
                                else
                                  Set @FlightStat = @OldFlightStat
                              end
                              else
                                Set @FlightOk = 1

                              if @FlightOk = 0
                              begin
                                if (@FDAllotUseSysSet = 'Y' and @PFAllotWarnFull = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotWarnFull = 'Y')
                                begin
                                  if @FlightStat = -1 Set @WarnType = -1 --Allotment not found
                                  if @FlightStat = 1 Set @WarnType = 1   --Allotment not enough
                                end

                                if not ((@FDAllotUseSysSet = 'Y' and @PFAllotDoNotOver = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotDoNotOver = 'Y'))
                                  if @FirstFlight = '' Select @FirstFlight = @FlightNo, @FirstSupplier = @Supplier, @FirstAllotType = @AllotType, @FirstDate = @FlyDate
                              end
                              else
                                Select @FlightOk = 1, @FirstFlight = @FlightNo, @FirstSupplier = @Supplier, @FirstAllotType = @AllotTYpe, @FirstDate = @FlyDate
                            end
                            else
                              Set @FlightOk = 1

                            if @FlightOk = 1 break
                            Fetch Next FROM SupCursor INTO @Supplier, @Priority, @MaxInfAge
                          end
                          Close SupCursor
                          DeAllocate SupCursor
                          Set @DepSupplier=@Supplier
                          if @FlightOk = 1
                          begin      
                            if @StepType = 1 
                            begin
                              Set @RetFlightOk = 0
                              Set @ChkNext = 0
                              if exists (Select * From FlightDay where FlightNo = @FlightNo and FlyDate = @FlyDate and isnull(RetFlightUseSysSet, 'N') = 'N')
                                Select @ChkNext = RetFlightOpt From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate
                              else
                                if exists (Select * From ParamFlight where isnull(UseSysParam, 'N') = 'N')
                                  Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = @PLMarket
                                else
                                  Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = ''

                              if @ChkNext = 1 and isnull(@NextFlight,'') <> '' -- Return Only Next Flight
                              begin
                                Select @RetFlight = @NextFlight, @RetFlyDate = @CheckIn + @Night

                                Select Top 1 @DefRetFlightNo = S.Service, @DefCurStepNo = X.StepNo
                                From CatService S (NOLOCK)
                                join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo
                                join CatPackPlan P (NOLOCK) on P.CatPackID = S.CatPackID and P.StepNo = X.StepNo
                                Where S.CatPackID=@CatPackID and X.CatPricePID=@PRecNo and P.StepType = 2

                                execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output

                                if @RetFlightOk = 1
                                begin
                                  if exists (select * from #Flight where StepNo = @DefCurStepNo) 
                                    delete from #Flight where StepNo = @DefCurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                                   
                                  if not exists (select * from #Flight where StepNo = @DefCurStepNo)  
                                  begin
                                    insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                        (@RetFlight, @DefRetFlightNo, @RetFlyDate, @Class, @Supplier, @AllotType, @AllotType, @WarnType, @DefCurStepNo, @RetFlight, @RetFlyDate)
                                    if exists (Select * From #FlightUse where FlightNo = @RetFlight and FlyDate = @RetFlyDate)
                                      update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @RetFlight and FlyDate = @RetFlyDate
                                    else
                                      insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@RetFlight, @RetFlyDate, @FlightUnit)
                                  end            
                                end 
                              end
                              else
                        --        if @ChkNext = 2 -- Return Only own Airline
                                begin            
                                  Set @RetFlyDate = @CheckIn + @Night
                                  
                                  Set @AirLine = '%'
                                  if @ChkNext = 2 -- Return Only own Airline
                                    Select @AirLine = AirLine From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate

                                  Select Top 1 @DefRetFlightNo = S.Service, @DefCurStepNo = X.StepNo
                                  From CatService S (NOLOCK)
                                  join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo
                                  join CatPackPlan P (NOLOCK) on P.CatPackID = S.CatPackID and P.StepNo = X.StepNo
                                  Where S.CatPackID=@CatPackID and X.CatPricePID=@PRecNo and S.ServiceType = 'FLIGHT' and P.StepType = 2

                                  Declare FCursor CURSOR LOCAL FAST_FORWARD FOR  
                                  Select Distinct S.ServiceItem
                                  from CatalogPack C (NOLOCK)
                                  join HolPackPlan P (NOLOCK) on P.HolPack = C.HolPack and P.Service = 'FLIGHT' and P.Departure <> C.DepCity --and P.Arrival = C.ArrCity
                                  join HolPackSer S (NOLOCK) on S.HolPack = P.HolPack and S.StepNo = P.StepNo 
                                  join FlightDay F (NOLOCK) on F.FlightNo = S.ServiceItem and F.FlyDate = @RetFlyDate
                                  Where C.RecID=@CatPackID and S.Service = 'FLIGHT' and P.StepType = 2 and F.AirLine LIKE @AirLine
                                  Open FCursor
                                  Fetch Next FROM FCursor INTO @RetFlight
                                  While @@FETCH_STATUS = 0
                                  Begin            
                                    execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output
                                    if @RetFlightOk = 1
                                    begin
                                      if exists (select * from #Flight where StepNo = @DefCurStepNo) 
                                        delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                                       
                                      if not exists (select * from #Flight where StepNo = @DefCurStepNo)  
                                      begin
                                        insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                            (@RetFlight, @DefRetFlightNo, @RetFlyDate, @Class, @Supplier, @AllotType, @AllotType, @WarnType, @DefCurStepNo, @RetFlight, @RetFlyDate)
                                        if exists (Select * From #FlightUse where FlightNo = @RetFlight and FlyDate = @RetFlyDate)
                                          update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @RetFlight and FlyDate = @RetFlyDate
                                        else
                                          insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@RetFlight, @RetFlyDate, @FlightUnit)
                                      end            
                                      break
                                    end               
                                    Fetch Next FROM FCursor INTO @RetFlight
                                  end
                                  Close FCursor
                                  DeAllocate FCursor
                                end         
                              if @RetFlightOk = 0 and @ChkNext > 0 Select @FlightOk = 0, @NextFlightAllow = 0
                            end
                          end

                          if @FlightOk = 1
                          begin
                        --    if exists (select * from #Flight where StepNo = @CurStepNo and isnull(FlightNo,'') = '' ) delete from #Flight where StepNo = @CurStepNo 
                            if @FlightNo <> ''
                              if exists (select * from #Flight where StepNo = @CurStepNo) delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                             
                            if not exists (select * from #Flight where StepNo = @CurStepNo)  
                            begin
                              insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                  (@FlightNo, @DefFlightNo, @FlyDate, @Class, @DepSupplier, @AllotType, @RetAllotType, @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                              if exists (Select * From #FlightUse where FlightNo = @FlightNo and FlyDate = @FlyDate)
                                update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @FlightNo and FlyDate = @FlyDate
                              else
                                insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@FlightNo, @FlyDate, @FlightUnit)
                            end
                        --    break
                          end

                          if @FlightOk = 0
                          begin
                        --    if @NextFlightAllow = 0 Set @WarnType = 100
                            Set @AllotType = @FirstAllotType
                            Set @RetAllotType = @AllotType
                            if @NextFlightAllow = 0 
                              Set @RetAllotType = 'O'
                            else
                              Set @AllotType = 'O'

                            if @FirstFlight <> ''
                            begin
                              if @FlightNo <> ''
                                if exists (select * from #Flight where StepNo = @CurStepNo) delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)

                              if not exists (select * from #Flight where StepNo = @CurStepNo)  
                              begin 
                                insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                    (@FirstFlight, @DefFlightNo, @FirstDate, @Class, @FirstSupplier, @AllotType, @RetAllotType, @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                                if exists (Select * From #FlightUse where FlightNo = @FirstFlight and FlyDate = @FirstDate)
                                  update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @FirstFlight and FlyDate = @FirstDate
                                else
                                  insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@FirstFlight, @FirstDate, @FlightUnit)
                              end
                            end
                            else
                              if not exists (select * from #Flight where StepNo = @CurStepNo)
                                insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                    ('', @DefFlightNo, @FlyDate, '', '', '', '', @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                          end

                          Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                        end
                        Close MyCursor
                        DeAllocate MyCursor
                        SET NOCOUNT OFF

                        Select * From #Flight ";
                #endregion
            else
                #region SQL
                tsql = @"   SET NOCOUNT ON

                        if not OBJECT_ID('TempDB.dbo.#FlightUse') is null drop table #FlightUse
                          CREATE TABLE #FlightUse
                          (
                            FlightNo varchar(10) Collate database_default,
                            FlyDate DateTime,
                            FlightUnit SmallInt
                          )
                        truncate table #FlightUse

                        --Select * From #FlightUse

                        if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
                          CREATE TABLE #Flight
                          (
                            FlightNo varchar(10),
                            PLFlightNo varchar(10),
                            FlyDate DateTime,
                            Class varchar(2),
                            Supplier varchar(10),
                            AllotType varchar(1),
                            RetAllotType varchar(1),
                            WarnType SmallInt,
                            StepNo SmallInt,
                            NextFlight varchar(10),
                            NextFlyDate DateTime
                          )
                        truncate table #Flight

                        Declare @FlightNo varchar(10), @DefFlightNo varchar(10), @DefRetFlightNo varchar(10), @HolPack varchar(10), @Class varchar(2), @CheckIn DateTime, @Operator varchar(10), @Night SmallInt,
                                    @DepCity int, @ArrCity int, @Departure int, @Arrival int, @FlyDate DateTime, @StepType SmallInt, @AirLine varchar(10),
                                    @NextFlight varchar(10), @NextFlyDate Datetime, @RetFlight varchar(10), @RetFlyDate Datetime, @StepNo int,
                                    @CurStepNo int, @DefCurStepNo int, @Supplier varchar(10),@DepSupplier varchar(10), @AllotType varchar(1), @RetAllotType varchar(1), @FlightStat SmallInt, @OldFlightStat SmallInt, @FreeSeat smallint,
                                    @FlightOk TinyInt, @RetFlightOk TinyInt,
                                    @WarnType SmallInt, @FirstFlight varchar(10), @FirstSupplier varchar(10), @FirstAllotType varchar(1), @FirstDate DateTime, 
                                    @PFAllotChk varchar(1), @PFAllotWarnFull varchar(1), @PFAllotDoNotOver varchar(1), @FlightUnit SmallInt, @MaxInfAge dec(5,2),
                                    @FDAllotChk varchar(1), @FDAllotWarnFull varchar(1), @FDAllotDoNotOver varchar(1), @FDAllotUseSysSet varchar(1),
                                    @CatPackID int, @PRecNo int, @Priority SmallInt, @PLMarket varchar(10), @OldUnit int,
                                    @RoomCnt SmallInt, @Adl SmallInt, @Chd1Age dec(5,2), @Chd2Age dec(5,2), @Chd3Age dec(5,2), @Chd4Age dec(5,2), @RoomChd1Age dec(5,2), @RoomChd2Age dec(5,2), @RoomChd3Age dec(5,2), @RoomChd4Age dec(5,2),
                                    @RoomChd1Count int, @RoomChd2Count int, @RoomChd3Count int, @RoomChd4Count int, @ChkNext TinyInt, @NextFlightAllow TinyInt, @BaseFlight varchar(10), @svOperator varchar(10)

                        Set @BaseFlight = @BaseFlight1
                        Set @CatPackID = @CatPackID1
                        Set @PRecNo = @PRecNo1
                        Set @PLMarket = @PLMarket1
                        Set @RoomCnt = @RoomCnt1
                        Set @Adl = @Adl1
                        Set @Chd1Age = @Chd1Age1
                        Set @Chd2Age = @Chd2Age1
                        Set @Chd3Age = @Chd3Age1
                        Set @Chd4Age = @Chd4Age1
                        Set @RoomChd1Age = @RoomChd1Age1
                        Set @RoomChd2Age = @RoomChd2Age1
                        Set @RoomChd3Age = @RoomChd3Age1
                        Set @RoomChd4Age = @RoomChd4Age1
                        Set @RoomChd1Count = @RoomChd1Count1
                        Set @RoomChd2Count = @RoomChd2Count1
                        Set @RoomChd3Count = @RoomChd3Count1
                        Set @RoomChd4Count = @RoomChd4Count1
                        Set @svOperator = @Operator1

                        if isnull(@BaseFlight,'') = '' Set @BaseFlight = '%'

                        Select @FlightUnit = @Adl

                        Select @CurStepNo = -1, @DefCurStepNo = -1, @FlightOk = 0, @WarnType = 0, @FirstFlight = '', @FirstSupplier = '', @FirstAllotType = '', @FirstDate = 0

                        Select @PFAllotChk = pf_AllotChk, @PFAllotWarnFull = pf_AllotWarnFull, @PFAllotDoNotOver = pf_AllotDoNotOver From dbo.ufn_GetMarketParams(@PLMarket,'pf')

                        Declare MyCursor CURSOR LOCAL FAST_FORWARD FOR
                        Select FlightNo, DefFlightNo, HolPack, Class, CheckIn, Operator, DepCity, ArrCity, Departure, Arrival, FlyDate, StepType, NextFlight, StepNo, Night,
                               Priority = isnull((Select Top 1 isnull(FB.SalePriority,0)
                                                  from FlightPrice FP
                                                  join FlightBlock FB on FB.FlightNo = X.FlightNo and FB.RecID = FP.BlockID
                                                  where FP.FlightNo = X.FlightNo and FP.FlyDate = X.FlyDate and FP.Operator = @svOperator),0)
                        From
                        (
                          select S.ServiceItem FlightNo, CS.Service DefFlightNo, P.HolPack, CS.SClass Class,  PP.CheckIn, C.Operator, C.DepCity, C.ArrCity, P.Departure, P.Arrival, X.Night,
                                 FlyDate = case when C.PackType = 'T' then PP.CheckIn + isnull(P.StartDay, 1) - 1
                                                else case when C.DepCity = P.Departure then PP.CheckIn else PP.CheckIn+X.Night end
                                           end,
                                 StepType = isnull(P.StepType,9), S.StepNo,
                                 FlgOrder = Case when CS.Service = S.ServiceItem then 0 else 1 end,
                                 NextFlight = (Select RetCode From Flight where Code = S.ServiceItem)
                          from CatPriceP PP (NOLOCK) 
                          join CatalogPack C (NOLOCK) on C.RecID = PP.CatPackID 
                          join HolPackPlan P (NOLOCK) on P.HolPack = C.HolPack and P.Service = 'FLIGHT' and P.Departure = C.DepCity and P.Arrival = C.ArrCity 
                          join HolPackSer S (NOLOCK) on S.HolPack = P.HolPack and S.StepNo = P.StepNo 
                          join CatService CS (NOLOCK) on CS.CatPackID=PP.CatPackID and CS.ServiceType = 'FLIGHT' 
                          join CatServiceNdx X (NOLOCK) on X.CatPackID=CS.CatPackID and X.CatServiceID=CS.RecNo and X.CatPricePID = PP.RecNo and X.StepNo = S.StepNo
                          where PP.CatPackID = @CatPackID and PP.RecNo = @PRecNo and S.ServiceItem LIKE @BaseFlight
                        ) X
                        order by StepType, FlyDate, Priority, FlgOrder
                        Open MyCursor
                        Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                        While @@FETCH_STATUS = 0
                        Begin
                          Select @FlightUnit = @Adl, @ChkNext = 0, @NextFlightAllow = 1
                          if @CurStepNo = -1 Set @CurStepNo = @StepNo
                          if @FlightOk = 1 
                            while @CurStepNo = @StepNo and @@FETCH_STATUS = 0
                              Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                          if @@FETCH_STATUS <> 0 break
                          if @CurStepNo <> @StepNo Select @CurStepNo = @StepNo, @FlightOk = 0, @FirstFlight = '', @FirstSupplier = '', @FirstAllotType = '', @FirstDate = 0, @FlightOk = 0

                          Declare SupCursor CURSOR LOCAL FAST_FORWARD FOR  
                          Select Distinct P.Supplier, B.SalePriority, D.MaxInfAge
                          from FlightDay D (NOLOCK)
                          Join FlightPrice P (NOLOCK) on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                          Join FlightBlock B (NOLOCK) on B.FlightNo = D.FlightNo and B.RecID = P.BlockID
                          where D.FlightNo = @FlightNo and D.FlyDate = @FlyDate and isnull(P.Supplier,'') <> '' and P.Operator = @svOperator
                          order by B.SalePriority
                          Open SupCursor
                          Fetch Next FROM SupCursor INTO @Supplier, @Priority, @MaxInfAge
                          While @@FETCH_STATUS = 0
                          Begin  
                            select @FlightStat = 0, @FreeSeat = 0, @AllotType = 'H', @WarnType = 0, @FlightOk = 0
                            select @FDAllotChk = AllotChk, @FDAllotWarnFull = AllotWarnFull, @FDAllotDoNotOver = AllotDoNotOver, @FDAllotUseSysSet = AllotUseSysSet From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate

                            if @Chd1Age>0 and @RoomChd1Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd1Count
                            if @Chd2Age>0 and @RoomChd2Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd2Count
                            if @Chd3Age>0 and @RoomChd3Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd3Count
                            if @Chd4Age>0 and @RoomChd4Age > @MaxInfAge Set @FlightUnit = @FlightUnit + @RoomChd4Count

                            Select @FlightUnit = @FlightUnit * @RoomCnt, @OldUnit = 0
                            Select @OldUnit = FlightUnit From #FlightUse where FlightNo = @FlightNo and FlyDate = @FlyDate
                            Set @FlightUnit = @FlightUnit + @OldUnit
                            
                            if (@FDAllotUseSysSet = 'Y' and @PFAllotChk = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotChk = 'Y')
                            begin
                              execute sp_CheckFlightAlllot @FlightNo, @FlyDate, @Class, @svOperator, @Supplier, '', @AllotType, @FlightUnit, @FlightStat output, @FreeSeat output
                              if @FlightStat<> 0
                              begin
                                Select @OldFlightStat = @FlightStat, @AllotType = 'S'
                                execute sp_CheckFlightAlllot @FlightNo, @FlyDate, @Class, @svOperator, @Supplier, '', @AllotType, @FlightUnit, @FlightStat output, @FreeSeat output
                                if @FlightStat = 0
                                  Set @FlightOk = 1
                                else
                                  Set @FlightStat = @OldFlightStat
                              end
                              else
                                Set @FlightOk = 1

                              if @FlightOk = 0
                              begin
                                if (@FDAllotUseSysSet = 'Y' and @PFAllotWarnFull = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotWarnFull = 'Y')
                                begin
                                  if @FlightStat = -1 Set @WarnType = -1 --Allotment not found
                                  if @FlightStat = 1 Set @WarnType = 1   --Allotment not enough
                                end

                                if not ((@FDAllotUseSysSet = 'Y' and @PFAllotDoNotOver = 'Y') or (@FDAllotUseSysSet = 'N' and @FDAllotDoNotOver = 'Y'))
                                  if @FirstFlight = '' Select @FirstFlight = @FlightNo, @FirstSupplier = @Supplier, @FirstAllotType = @AllotType, @FirstDate = @FlyDate
                              end
                              else
                                Select @FlightOk = 1, @FirstFlight = @FlightNo, @FirstSupplier = @Supplier, @FirstAllotType = @AllotTYpe, @FirstDate = @FlyDate
                            end
                            else
                              Set @FlightOk = 1

                            if @FlightOk = 1 break
                            Fetch Next FROM SupCursor INTO @Supplier, @Priority, @MaxInfAge
                          end
                          Close SupCursor
                          DeAllocate SupCursor
                          Set @DepSupplier=@Supplier
                          if @FlightOk = 1
                          begin      
                            if @StepType = 1 
                            begin
                              Set @RetFlightOk = 0
                              Set @ChkNext = 0
                              if exists (Select * From FlightDay where FlightNo = @FlightNo and FlyDate = @FlyDate and isnull(RetFlightUseSysSet, 'N') = 'N')
                                Select @ChkNext = RetFlightOpt From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate
                              else
                                if exists (Select * From ParamFlight where isnull(UseSysParam, 'N') = 'N')
                                  Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = @PLMarket
                                else
                                  Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = ''

                              if @ChkNext = 1 and isnull(@NextFlight,'') <> '' -- Return Only Next Flight
                              begin
                                Select @RetFlight = @NextFlight, @RetFlyDate = @CheckIn + @Night

                                Select Top 1 @DefRetFlightNo = S.Service, @DefCurStepNo = X.StepNo
                                From CatService S (NOLOCK)
                                join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo
                                join CatPackPlan P (NOLOCK) on P.CatPackID = S.CatPackID and P.StepNo = X.StepNo
                                Where S.CatPackID=@CatPackID and X.CatPricePID=@PRecNo and P.StepType = 2

                                execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output

                                if @RetFlightOk = 1
                                begin
                                  if exists (select * from #Flight where StepNo = @DefCurStepNo) 
                                    delete from #Flight where StepNo = @DefCurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                                   
                                  if not exists (select * from #Flight where StepNo = @DefCurStepNo)  
                                  begin
                                    insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                        (@RetFlight, @DefRetFlightNo, @RetFlyDate, @Class, @Supplier, @AllotType, @AllotType, @WarnType, @DefCurStepNo, @RetFlight, @RetFlyDate)
                                    if exists (Select * From #FlightUse where FlightNo = @RetFlight and FlyDate = @RetFlyDate)
                                      update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @RetFlight and FlyDate = @RetFlyDate
                                    else
                                      insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@RetFlight, @RetFlyDate, @FlightUnit)
                                  end            
                                end 
                              end
                              else
                        --        if @ChkNext = 2 -- Return Only own Airline
                                begin            
                                  Set @RetFlyDate = @CheckIn + @Night
                                  
                                  Set @AirLine = '%'
                                  if @ChkNext = 2 -- Return Only own Airline
                                    Select @AirLine = AirLine From FlightDay (NOLOCK) where FlightNo = @FlightNo and FlyDate = @FlyDate

                                  Select Top 1 @DefRetFlightNo = S.Service, @DefCurStepNo = X.StepNo
                                  From CatService S (NOLOCK)
                                  join CatServiceNdx X (NOLOCK) on X.CatPackID=S.CatPackID and X.CatServiceID=S.RecNo
                                  join CatPackPlan P (NOLOCK) on P.CatPackID = S.CatPackID and P.StepNo = X.StepNo
                                  Where S.CatPackID=@CatPackID and X.CatPricePID=@PRecNo and S.ServiceType = 'FLIGHT' and P.StepType = 2

                                  Declare FCursor CURSOR LOCAL FAST_FORWARD FOR  
                                  Select Distinct S.ServiceItem
                                  from CatalogPack C (NOLOCK)
                                  join HolPackPlan P (NOLOCK) on P.HolPack = C.HolPack and P.Service = 'FLIGHT' and P.Departure <> C.DepCity --and P.Arrival = C.ArrCity
                                  join HolPackSer S (NOLOCK) on S.HolPack = P.HolPack and S.StepNo = P.StepNo 
                                  join FlightDay F (NOLOCK) on F.FlightNo = S.ServiceItem and F.FlyDate = @RetFlyDate
                                  Where C.RecID=@CatPackID and S.Service = 'FLIGHT' and P.StepType = 2 and F.AirLine LIKE @AirLine
                                  Open FCursor
                                  Fetch Next FROM FCursor INTO @RetFlight
                                  While @@FETCH_STATUS = 0
                                  Begin            
                                    execute usp_GetNextFlightStat @PLMarket, @svOperator, @RetFlight, @RetFlyDate, @FlightUnit, @Class, @RetFlightOk output, @Supplier output, @AllotType output
                                    if @RetFlightOk = 1
                                    begin
                                      if exists (select * from #Flight where StepNo = @DefCurStepNo) 
                                        delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                                       
                                      if not exists (select * from #Flight where StepNo = @DefCurStepNo)  
                                      begin
                                        insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                            (@RetFlight, @DefRetFlightNo, @RetFlyDate, @Class, @Supplier, @AllotType, @AllotType, @WarnType, @DefCurStepNo, @RetFlight, @RetFlyDate)
                                        if exists (Select * From #FlightUse where FlightNo = @RetFlight and FlyDate = @RetFlyDate)
                                          update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @RetFlight and FlyDate = @RetFlyDate
                                        else
                                          insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@RetFlight, @RetFlyDate, @FlightUnit)
                                      end            
                                      break
                                    end               
                                    Fetch Next FROM FCursor INTO @RetFlight
                                  end
                                  Close FCursor
                                  DeAllocate FCursor
                                end         
                              if @RetFlightOk = 0 and @ChkNext > 0 Select @FlightOk = 0, @NextFlightAllow = 0
                            end
                          end

                          if @FlightOk = 1
                          begin
                        --    if exists (select * from #Flight where StepNo = @CurStepNo and isnull(FlightNo,'') = '' ) delete from #Flight where StepNo = @CurStepNo 
                            if @FlightNo <> ''
                              if exists (select * from #Flight where StepNo = @CurStepNo) delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)
                             
                            if not exists (select * from #Flight where StepNo = @CurStepNo)  
                            begin
                              insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                  (@FlightNo, @DefFlightNo, @FlyDate, @Class, @DepSupplier, @AllotType, @RetAllotType, @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                              if exists (Select * From #FlightUse where FlightNo = @FlightNo and FlyDate = @FlyDate)
                                update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @FlightNo and FlyDate = @FlyDate
                              else
                                insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@FlightNo, @FlyDate, @FlightUnit)
                            end
                        --    break
                          end

                          if @FlightOk = 0
                          begin
                        --    if @NextFlightAllow = 0 Set @WarnType = 100
                            Set @AllotType = @FirstAllotType
                            Set @RetAllotType = @AllotType
                            if @NextFlightAllow = 0 
                              Set @RetAllotType = 'O'
                            else
                              Set @AllotType = 'O'

                            if @FirstFlight <> ''
                            begin
                              if @FlightNo <> ''
                                if exists (select * from #Flight where StepNo = @CurStepNo) delete from #Flight where StepNo = @CurStepNo and (FlightNo = '' or AllotType = 'O' or WarnType > 0)

                              if not exists (select * from #Flight where StepNo = @CurStepNo)  
                              begin 
                                insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                    (@FirstFlight, @DefFlightNo, @FirstDate, @Class, @FirstSupplier, @AllotType, @RetAllotType, @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                                if exists (Select * From #FlightUse where FlightNo = @FirstFlight and FlyDate = @FirstDate)
                                  update #FlightUse Set FlightUnit = FlightUnit + @FlightStat where FlightNo = @FirstFlight and FlyDate = @FirstDate
                                else
                                  insert into #FlightUse (FlightNo, FlyDate, FlightUnit) values (@FirstFlight, @FirstDate, @FlightUnit)
                              end
                            end
                            else
                              if not exists (select * from #Flight where StepNo = @CurStepNo)
                                insert into #Flight (FlightNo, PLFlightNo, FlyDate, Class, Supplier, AllotType, RetAllotType, WarnType, StepNo, NextFlight, NextFlyDate) Values 
                                                    ('', @DefFlightNo, @FlyDate, '', '', '', '', @WarnType, @CurStepNo, @NextFlight, @NextFlyDate)
                          end

                          Fetch Next FROM MyCursor INTO @FlightNo, @DefFlightNo, @HolPack, @Class, @CheckIn, @Operator, @DepCity, @ArrCity, @Departure, @Arrival, @FlyDate, @StepType, @NextFlight, @StepNo, @Night, @Priority
                        end
                        Close MyCursor
                        DeAllocate MyCursor
                        SET NOCOUNT OFF

                        Select * From #Flight ";
                #endregion

            DataRow SelectBook;
            try
            {
                SelectBook = ResData.Tables["SelectBook"].Rows.Find(BookID);
                if (SelectBook == null)
                    return plFlights;
            }
            catch
            {
                return plFlights;
            }

            int roomCount = 1;
            int Adult = Convert.ToInt32(SelectBook["HAdult"].ToString());
            int chdAge1 = Convert.ToInt32(SelectBook["HChdAgeG1"].ToString());
            int chdAge2 = Convert.ToInt32(SelectBook["HChdAgeG2"].ToString());
            int chdAge3 = Convert.ToInt32(SelectBook["HChdAgeG3"].ToString());
            int chdAge4 = Convert.ToInt32(SelectBook["HChdAgeG4"].ToString());
            decimal roomChd1Age1 = Convert.ToDecimal(SelectBook["ChdG1Age2"].ToString());
            decimal roomChd2Age1 = Convert.ToDecimal(SelectBook["ChdG2Age2"].ToString());
            decimal roomChd3Age1 = Convert.ToDecimal(SelectBook["ChdG3Age2"].ToString());
            decimal roomChd4Age1 = SelectBook["ChdG4Age2"].ToString() != "" ? Convert.ToDecimal(SelectBook["ChdG4Age2"].ToString()) : Convert.ToDecimal("0");
            int roomChd1Count1 = chdAge1;
            int roomChd2Count1 = chdAge2;
            int roomChd3Count1 = chdAge3;
            int roomChd4Count1 = chdAge4;

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "BaseFlight1", DbType.String, SelectBook["DepFlight"].ToString());
                db.AddInParameter(dbCommand, "CatPackID1", DbType.String, (int)SelectBook["CatPackID"]);
                db.AddInParameter(dbCommand, "PRecNo1", DbType.String, (int)SelectBook["PRecNo"]);
                db.AddInParameter(dbCommand, "PLMarket1", DbType.String, SelectBook["Market"].ToString());
                db.AddInParameter(dbCommand, "RoomCnt1", DbType.Int32, roomCount);
                db.AddInParameter(dbCommand, "Adl1", DbType.Int32, Adult);
                db.AddInParameter(dbCommand, "Chd1Age1", DbType.Int32, chdAge1);
                db.AddInParameter(dbCommand, "Chd2Age1", DbType.Int32, chdAge2);
                db.AddInParameter(dbCommand, "Chd3Age1", DbType.Int32, chdAge3);
                db.AddInParameter(dbCommand, "Chd4Age1", DbType.Int32, chdAge4);
                db.AddInParameter(dbCommand, "RoomChd1Age1", DbType.Decimal, roomChd1Age1);
                db.AddInParameter(dbCommand, "RoomChd2Age1", DbType.Decimal, roomChd2Age1);
                db.AddInParameter(dbCommand, "RoomChd3Age1", DbType.Decimal, roomChd3Age1);
                db.AddInParameter(dbCommand, "RoomChd4Age1", DbType.Decimal, roomChd4Age1);
                db.AddInParameter(dbCommand, "RoomChd1Count1", DbType.Int32, roomChd1Count1);
                db.AddInParameter(dbCommand, "RoomChd2Count1", DbType.Int32, roomChd2Count1);
                db.AddInParameter(dbCommand, "RoomChd3Count1", DbType.Int32, roomChd3Count1);
                db.AddInParameter(dbCommand, "RoomChd4Count1", DbType.Int32, roomChd4Count1);
                db.AddInParameter(dbCommand, "Operator1", DbType.String, SelectBook["Operator"].ToString());
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200010"))
                    db.AddInParameter(dbCommand, "ResDate", DbType.DateTime, ResData.Tables.Contains("ResMain") && ResData.Tables["ResMain"].Rows[0]["ResDate"].ToString() != "" ? (DateTime)ResData.Tables["ResMain"].Rows[0]["ResDate"] : Convert.DBNull);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        PLFlights row = new PLFlights
                        {
                            AllotType = Conversion.getStrOrNull(oReader["AllotType"]),
                            Class = Conversion.getStrOrNull(oReader["Class"]),
                            FlightNo = Conversion.getStrOrNull(oReader["FlightNo"]),
                            FlyDate = (DateTime)oReader["FlyDate"],
                            NextFlight = Conversion.getStrOrNull(oReader["NextFlight"]),
                            NextFlyDate = Conversion.getDateTimeOrNull(oReader["NextFlyDate"]),
                            PLFlightNo = Conversion.getStrOrNull(oReader["PLFlightNo"]),
                            RetAllotType = Conversion.getStrOrNull(oReader["RetAllotType"]),
                            StepNo = Convert.ToInt16(oReader["StepNo"]),
                            Supplier = Conversion.getStrOrNull(oReader["Supplier"]),
                            WarnType = Convert.ToInt16(oReader["WarnType"])
                        };
                        plFlights.Add(row);
                    }
                }
                return plFlights;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return plFlights;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool getFlightsAndAllotment(User UserData, ResDataRecord ResData, ref List<plResServiceRecord> PLService, Int32? _BookID, bool IncPack, ref string errorMsg)
        {
            StringBuilder errorStr = new StringBuilder();
            int BookID = _BookID.HasValue ? _BookID.Value : 0;

            bool makeRes = true;
            List<PLFlights> flight = getPLFlights(UserData, ResData, BookID, ref errorMsg);

            foreach (var r in flight.OrderBy(o => o.FlyDate))
            {
                if (Convert.ToInt32(r.WarnType) == 100)
                {
                    errorStr.Append(string.Format("Flight allotment is not found for {0},{1} at {2}.", r.FlightNo, r.Class, r.FlyDate.ToShortDateString()));
                    makeRes = false;
                    break;
                }
                List<plResServiceRecord> PLServiceRow = new List<plResServiceRecord>();
                PLFlights r1 = r;
                PLServiceRow = (PLService.Where(q => q.ServiceType == "FLIGHT" && q.Service == r1.PLFlightNo && (q.StepType == 1 || q.StepType == 2))).ToList();

                PLServiceRow[0].Service = r.FlightNo; //
                PLServiceRow[0].Supplier = r.Supplier;
                PLServiceRow[0].AllotType = r.AllotType;
                if (string.IsNullOrEmpty(r.FlightNo))
                    PLServiceRow[0].AllotType = "O";

                if (PLServiceRow[0].AllotType == "O" || r.RetAllotType == "O")
                {
                    if (UserData.TvParams.TvParamFlight.AllotDoNotOver)
                        makeRes = false;
                    else
                    {
                        FlightDayRecord FlightDay = getFlightDay(UserData, r.FlightNo, r.FlyDate, ref errorMsg);
                        if ((FlightDay.AllotUseSysSet && (UserData.TvParams.TvParamFlight.AllotDoNotOver)) ||
                            (!FlightDay.AllotUseSysSet && FlightDay.AllotDoNotOver))
                            makeRes = false;
                    }

                }
                if (!makeRes)
                {
                    errorStr.Append(string.Format("Flight allotment is not found for {0},{1} at {2}.", r.FlightNo, r.Class, r.FlyDate.ToShortDateString()));
                    break;
                }
            }

            if (!makeRes)
                errorMsg = errorStr.ToString();

            return makeRes;
        }

        public bool getFlightsAndAllotment(User UserData, DataSet ResData, ref List<plResServiceRecord> PLService, Int32? _BookID, bool IncPack, ref string errorMsg)
        {
            StringBuilder errorStr = new StringBuilder();
            int BookID = Convert.IsDBNull(_BookID) ? _BookID.Value : 0;

            bool makeRes = true;
            List<PLFlights> flight = getPLFlights(UserData, ResData, BookID, ref errorMsg);

            foreach (var r in flight.OrderBy(o => o.FlyDate))
            {
                if (Convert.ToInt32(r.WarnType) == 100)
                {
                    errorStr.Append(string.Format("Flight allotment is not found for {0},{1} at {2}.", r.FlightNo, r.Class, r.FlyDate.ToShortDateString()));
                    makeRes = false;
                    break;
                }
                List<plResServiceRecord> PLServiceRow = new List<plResServiceRecord>();
                PLFlights r1 = r;
                PLServiceRow = (PLService.Where(q => q.ServiceType == "FLIGHT" && q.Service == r1.PLFlightNo)).ToList();

                PLServiceRow[0].Service = r.FlightNo;
                PLServiceRow[0].Supplier = r.Supplier;
                PLServiceRow[0].AllotType = r.AllotType;
                if (string.IsNullOrEmpty(r.FlightNo))
                    PLServiceRow[0].AllotType = "O";

                if (PLServiceRow[0].AllotType == "O" || r.RetAllotType == "O")
                {
                    if (UserData.TvParams.TvParamFlight.AllotDoNotOver)
                        makeRes = false;
                    else
                    {
                        FlightDayRecord FlightDay = getFlightDay(UserData, r.FlightNo, r.FlyDate, ref errorMsg);
                        if ((FlightDay.AllotUseSysSet && (UserData.TvParams.TvParamFlight.AllotDoNotOver)) ||
                            (!FlightDay.AllotUseSysSet && FlightDay.AllotDoNotOver))
                            makeRes = false;
                    }

                }
                if (!makeRes)
                {
                    errorStr.Append(string.Format("Flight allotment is not found for {0},{1} at {2}.", r.FlightNo, r.Class, r.FlyDate.ToShortDateString()));
                    break;
                }
            }

            if (!makeRes)
                errorMsg = errorStr.ToString();

            return makeRes;
        }

        public AirportRecord getAirport(string Market, string Code, ref string errorMsg)
        {
            AirportRecord record = new AirportRecord();

            string tsql = @"Select A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, @Market), A.Name),
	                            A.NameS, A.Location, LocationName=L.Name, LocationNameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                A.PNLFullName, A.PNLFlightSpace, A.PNLNameSpace, A.PNLChdRemark, 
	                            A.PNLSitatex, A.PNLMaxLineInPart, A.PNLDispExtra, A.PNLDispPNR, A.PNLDotAfterTitle
                            From Airport A (NOLOCK)
                            Join Location L (NOLOCK) ON A.Location=L.RecID
                            Where A.Code=@Code";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Location = (Int32)R["Location"];
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        record.PNLFullName = Equals(R["PNLFullName"], "Y");
                        record.PNLFlightSpace = Equals(R["PNLFlightSpace"], "Y");
                        record.PNLNameSpace = Equals(R["PNLNameSpace"], "Y");
                        record.PNLChdRemark = Equals(R["PNLChdRemark"], "Y");
                        record.PNLSitatex = Equals(R["PNLSitatex"], "Y");
                        record.PNLMaxLineInPart = Conversion.getInt32OrNull(R["PNLMaxLineInPart"]);
                        record.PNLDispExtra = Equals(R["PNLDispExtra"], "Y");
                        record.PNLDotAfterTitle = Equals(R["PNLDotAfterTitle"], "Y");
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AirportRecord> getCountryAirportList(string Market, int? _Location, ref string errorMsg)
        {
            List<AirportRecord> records = new List<AirportRecord>();
            string tsql = @"Select A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, @Market), A.Name),
	                            A.NameS, A.Location, LocationName=L.Name, LocationNameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                A.PNLFullName, A.PNLFlightSpace, A.PNLNameSpace, A.PNLChdRemark, 
	                            A.PNLSitatex, A.PNLMaxLineInPart, A.PNLDispExtra, A.PNLDispPNR, A.PNLDotAfterTitle
                            From Airport A (NOLOCK)
                            Join Location L (NOLOCK) ON A.Location=L.RecID                            
                            Where L.Country=@Country";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, new Locations().getLocationForCountry(_Location.HasValue ? _Location.Value : -1, ref errorMsg));

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AirportRecord record = new AirportRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Location = (Int32)R["Location"];
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        record.PNLFullName = Equals(R["PNLFullName"], "Y");
                        record.PNLFlightSpace = Equals(R["PNLFlightSpace"], "Y");
                        record.PNLNameSpace = Equals(R["PNLNameSpace"], "Y");
                        record.PNLChdRemark = Equals(R["PNLChdRemark"], "Y");
                        record.PNLSitatex = Equals(R["PNLSitatex"], "Y");
                        record.PNLMaxLineInPart = Conversion.getInt32OrNull(R["PNLMaxLineInPart"]);
                        record.PNLDispExtra = Equals(R["PNLDispExtra"], "Y");
                        record.PNLDotAfterTitle = Equals(R["PNLDotAfterTitle"], "Y");
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightRecord getFlight(User UserData, string Code, ref string errorMsg)
        {
            FlightRecord record = new FlightRecord();
            string tsql = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040000007"))
                tsql = @"Select Code, Airline, AirlineName=(Select [Name] From AirLine (NOLOCK) Where Code=Flight.Airline),
                            AirlineLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From AirLine (NOLOCK) Where Code=Flight.Airline), Company, 
                            PlaneType, PlaneTypeName=(Select [Name] From PlaneType (NOLOCK) Where RecID=Flight.PlaneType),
                            FlightType, FlightTypeName=(Select [Name] From FlightType (NOLOCK) Where RecID=Flight.FlightType),
                            DepCity, DepCityName=(Select [Name] From Location (NOLOCK) Where RecID=Flight.DepCity),
                            DepCityLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=Flight.DepCity),
                            DepAir, 
                            DepTime, 	
                            ArrCity, ArrCityName=(Select [Name] From Location (NOLOCK) Where RecID=Flight.ArrCity),
                            ArrCityLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=Flight.ArrCity),
                            ArrAir, 
                            ArrTime, 
                            CountCloseTime, BagWeight,	InfAge, ChdAge, TeenAge, RetCode, Virtual, PNLName, ConfStat, FlyDays, 
                            TaxPer, FlyDur, Operator, PayCom, PayComEB, PayPasEB, isFirstFlight, CanDiscount=isnull(CanDiscount, 0), Direction, AccountNo1, AccountNo2, AccountNo3,
                            OTickSaveAsRq
                         From Flight (NOLOCK)
                         Where Code=@Code";
            else
                tsql = @"Select Code, Airline, AirlineName=(Select [Name] From AirLine (NOLOCK) Where Code=Flight.Airline),
                            AirlineLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From AirLine (NOLOCK) Where Code=Flight.Airline), Company, 
                            PlaneType, PlaneTypeName=(Select [Name] From PlaneType (NOLOCK) Where RecID=Flight.PlaneType),
                            FlightType, FlightTypeName=(Select [Name] From FlightType (NOLOCK) Where RecID=Flight.FlightType),
                            DepCity, DepCityName=(Select [Name] From Location (NOLOCK) Where RecID=Flight.DepCity),
                            DepCityLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=Flight.DepCity),
                            DepAir, 
                            DepTime, 	
                            ArrCity, ArrCityName=(Select [Name] From Location (NOLOCK) Where RecID=Flight.ArrCity),
                            ArrCityLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=Flight.ArrCity),
                            ArrAir, 
                            ArrTime, 
                            CountCloseTime, BagWeight,	InfAge, ChdAge, TeenAge, RetCode, Virtual, PNLName, ConfStat, FlyDays, 
                            TaxPer, FlyDur, Operator, PayCom, PayComEB, PayPasEB, isFirstFlight, CanDiscount=isnull(CanDiscount, 0), Direction, AccountNo1, AccountNo2, AccountNo3
                         From Flight (NOLOCK)
                         Where Code=@Code";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Airline = Conversion.getStrOrNull(R["Airline"]);
                        record.AirlineName = Conversion.getStrOrNull(R["AirlineName"]);
                        record.AirlineLocalName = Conversion.getStrOrNull(R["AirlineLocalName"]);
                        record.Company = Conversion.getStrOrNull(R["Company"]);
                        record.PlaneType = Conversion.getInt32OrNull(R["PlaneType"]);
                        record.PlaneTypeName = Conversion.getStrOrNull(R["PlaneTypeName"]);
                        record.FlightType = Conversion.getInt32OrNull(R["FlightType"]);
                        record.FlightTypeName = Conversion.getStrOrNull(R["FlightTypeName"]);
                        record.DepCity = (Int32)R["DepCity"];
                        record.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        record.DepCityLocalName = Conversion.getStrOrNull(R["DepCityLocalName"]);
                        record.DepAir = Conversion.getStrOrNull(R["DepAir"]);
                        record.DepTime = Conversion.getDateTimeOrNull(R["DepTime"]);
                        record.ArrCity = (Int32)R["ArrCity"];
                        record.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        record.ArrCityLocalName = Conversion.getStrOrNull(R["ArrCityLocalName"]);
                        record.ArrAir = Conversion.getStrOrNull(R["ArrAir"]);
                        record.ArrTime = Conversion.getDateTimeOrNull(R["ArrTime"]);
                        record.CountCloseTime = Conversion.getDateTimeOrNull(R["CountCloseTime"]);
                        record.BagWeight = Conversion.getDecimalOrNull(R["BagWeight"]);
                        record.InfAge = Conversion.getDecimalOrNull(R["InfAge"]);
                        record.ChdAge = Conversion.getDecimalOrNull(R["ChdAge"]);
                        record.TeenAge = Conversion.getDecimalOrNull(R["TeenAge"]);
                        record.RetCode = Conversion.getStrOrNull(R["RetCode"]);
                        record.Virtual = Conversion.getStrOrNull(R["Virtual"]);
                        record.PNLName = Conversion.getStrOrNull(R["PNLName"]);
                        record.ConfStat = Conversion.getInt16OrNull(R["ConfStat"]);
                        record.FlyDays = Conversion.getStrOrNull(R["FlyDays"]);
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.FlyDur = Conversion.getInt32OrNull(R["FlyDur"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = Conversion.getBoolOrNull(R["PayPasEB"]);
                        record.IsFirstFlight = Equals(R["IsFirstFlight"], "Y");
                        record.CanDiscount = Conversion.getBoolOrNull(R["CanDiscount"]);
                        record.Direction = Conversion.getInt16OrNull(R["Direction"]);
                        record.AccountNo1 = Conversion.getStrOrNull(R["AccountNo1"]);
                        record.AccountNo2 = Conversion.getStrOrNull(R["AccountNo2"]);
                        record.AccountNo3 = Conversion.getStrOrNull(R["AccountNo3"]);
                        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040000007"))
                            record.OTickSaveAsRq = Conversion.getBoolOrNull(R["OTickSaveAsRq"]);
                        return record;
                    }
                    else
                        return null;

                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightRecord> getFlights(string Market, ref string errorMsg)
        {
            List<FlightRecord> records = new List<FlightRecord>();
            string tsql = @"Select Code, Airline, AirlineName=(Select [Name] From AirLine (NOLOCK) Where Code=Flight.Airline),
	                            AirlineLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From AirLine (NOLOCK) Where Code=Flight.Airline), Company, 
	                            PlaneType, PlaneTypeName=(Select [Name] From PlaneType (NOLOCK) Where RecID=Flight.PlaneType),
	                            FlightType, FlightTypeName=(Select [Name] From FlightType (NOLOCK) Where RecID=Flight.FlightType),
	                            DepCity, DepCityName=(Select [Name] From Location (NOLOCK) Where RecID=Flight.DepCity),
	                            DepCityLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=Flight.DepCity),
	                            DepAir, DepTime, 	
	                            ArrCity, ArrCityName=(Select [Name] From Location (NOLOCK) Where RecID=Flight.ArrCity),
	                            ArrCityLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=Flight.ArrCity),
	                            ArrAir, ArrTime, CountCloseTime, BagWeight,	InfAge, ChdAge, TeenAge, RetCode, Virtual, PNLName, ConfStat, FlyDays, 
	                            TaxPer, FlyDur, Operator, PayCom, PayComEB, PayPasEB, isFirstFlight, CanDiscount=isnull(CanDiscount, 0), Direction
                            From Flight (NOLOCK) 
                            Where DepCity is not null And ArrCity is not null
                            ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        FlightRecord record = new FlightRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Airline = Conversion.getStrOrNull(R["Airline"]);
                        record.AirlineName = Conversion.getStrOrNull(R["AirlineName"]);
                        record.AirlineLocalName = Conversion.getStrOrNull(R["AirlineLocalName"]);
                        record.Company = Conversion.getStrOrNull(R["Company"]);
                        record.PlaneType = Conversion.getInt32OrNull(R["PlaneType"]);
                        record.PlaneTypeName = Conversion.getStrOrNull(R["PlaneTypeName"]);
                        record.FlightType = Conversion.getInt32OrNull(R["FlightType"]);
                        record.FlightTypeName = Conversion.getStrOrNull(R["FlightTypeName"]);
                        record.DepCity = (Int32)R["DepCity"];
                        record.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        record.DepCityLocalName = Conversion.getStrOrNull(R["DepCityLocalName"]);
                        record.DepAir = Conversion.getStrOrNull(R["DepAir"]);
                        record.DepTime = Conversion.getDateTimeOrNull(R["DepTime"]);
                        record.ArrCity = (Int32)R["ArrCity"];
                        record.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        record.ArrCityLocalName = Conversion.getStrOrNull(R["ArrCityLocalName"]);
                        record.ArrAir = Conversion.getStrOrNull(R["ArrAir"]);
                        record.ArrTime = Conversion.getDateTimeOrNull(R["ArrTime"]);
                        record.CountCloseTime = Conversion.getDateTimeOrNull(R["CountCloseTime"]);
                        record.BagWeight = Conversion.getDecimalOrNull(R["BagWeight"]);
                        record.InfAge = Conversion.getDecimalOrNull(R["InfAge"]);
                        record.ChdAge = Conversion.getDecimalOrNull(R["ChdAge"]);
                        record.TeenAge = Conversion.getDecimalOrNull(R["TeenAge"]);
                        record.RetCode = Conversion.getStrOrNull(R["RetCode"]);
                        record.Virtual = Conversion.getStrOrNull(R["Virtual"]);
                        record.PNLName = Conversion.getStrOrNull(R["PNLName"]);
                        record.ConfStat = Conversion.getInt16OrNull(R["ConfStat"]);
                        record.FlyDays = Conversion.getStrOrNull(R["FlyDays"]);
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.FlyDur = Conversion.getInt32OrNull(R["FlyDur"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = Conversion.getBoolOrNull(R["PayPasEB"]);
                        record.IsFirstFlight = Equals(R["IsFirstFlight"], "Y");
                        record.CanDiscount = Conversion.getBoolOrNull(R["CanDiscount"]);
                        record.Direction = Conversion.getInt16OrNull(R["Direction"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightDetailRecord getFlightDetail(User UserData, string Market, string Code, DateTime FlightDay, ref string errorMsg)
        {
            try
            {
                bool useShortName = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt);
                FlightRecord f = getFlight(UserData, Code, ref errorMsg);
                FlightDayRecord flightDay = getFlightDay(UserData, Code, FlightDay, ref errorMsg);
                if(flightDay.Airline!=f.Airline)
                {
                    var airlineInfo = getAirline(UserData.Market, flightDay.Airline, ref errorMsg);
                    f.Airline = airlineInfo.Code;
                    f.AirlineLocalName = airlineInfo.NameL;
                    f.AirlineName = airlineInfo.Name;
                }
                List<FlightDayRecord> flightDays = new List<FlightDayRecord>();

                AirportRecord depAirport = getAirport(Market, flightDay.DepAirport, ref errorMsg);
                AirportRecord arrAirport = getAirport(Market, flightDay.ArrAirport, ref errorMsg);
                flightDays.Add(flightDay);
                IEnumerable<FlightDetailRecord> query = from q in flightDays.AsEnumerable()
                                                        where q.FlightNo == Code //&& q.FlyDate == FlightDay
                                                        select new FlightDetailRecord
                                                        {
                                                            FlightNo =!string.IsNullOrEmpty(q.PNLName)? q.PNLName:q.FlightNo,
                                                            FlyDate = q.FlyDate,
                                                            DepDate = q.TDepDate,
                                                            DepTime = q.DepTime,
                                                            TDepTime = q.TDepTime,
                                                            DepAirport = /*useShortName ? (string.IsNullOrEmpty(depAirport.NameS) ? q.DepAirport : depAirport.NameS) : */q.DepAirport,
                                                            DepAirportName = depAirport.Name,
                                                            DepAirportNameL = depAirport.LocalName,
                                                            DepCity = q.DepCity,
                                                            DepCityName = f.DepCityName,
                                                            DepCityNameL = f.DepCityLocalName,
                                                            ArrTime = q.ArrTime,
                                                            TArrTime = q.TArrTime,
                                                            ArrAirport = /*useShortName ? (string.IsNullOrEmpty(arrAirport.NameS) ? q.ArrAirport : arrAirport.NameS) : */q.ArrAirport,
                                                            ArrAirportName = arrAirport.Name,
                                                            ArrAirportNameL = arrAirport.LocalName,
                                                            ArrCity = q.ArrCity,
                                                            ArrCityName = f.ArrCityName,
                                                            ArrCityNameL = f.ArrCityLocalName,
                                                            Airline = f.Airline,
                                                            AirlineName = f.AirlineName,
                                                            AirlineNameL = f.AirlineLocalName,
                                                            MaxInfAge = q.MaxInfAge,
                                                            MaxChdAge = q.MaxChdAge,
                                                            MaxTeenAge = q.MaxTeenAge,
                                                            BagWeight = q.BagWeight,
                                                            CanDiscount = (f.CanDiscount.HasValue ? f.CanDiscount.Value : false)
                                                        };

                return query.FirstOrDefault();
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
        }

        public bool CheckFlightTime(User UserData, List<ResServiceRecord> ResService, ref string errorMsg)
        {
            var Fly = from flght in ResService
                      where flght.ServiceType == "FLIGHT"
                      orderby flght.BegDate
                      select new { FlightNo = flght.Service, FlyDate = flght.BegDate };

            if (Fly.Count() < 1)
            {
                return true; // No Flight Service;
            }

            FlightDayRecord _depFlight = new Flights().getFlightDay(UserData, Fly.FirstOrDefault().FlightNo, Fly.FirstOrDefault().FlyDate.Value, ref errorMsg);
            if (_depFlight.DepTime.HasValue && _depFlight.DepTime.Value.Ticks > 0)
            {
                long countClose = _depFlight.CountCloseTime.HasValue ? (_depFlight.CountCloseTime.Value.Hour * 60 + _depFlight.CountCloseTime.Value.Minute) : 0;
                long saleRelease = _depFlight.SaleRelease.HasValue ? (_depFlight.SaleRelease.Value.Hour * 60 + _depFlight.SaleRelease.Value.Minute) : 0;
                long depTime = _depFlight.DepTime.HasValue ? (_depFlight.DepTime.Value.Hour * 60 + _depFlight.DepTime.Value.Minute) : 0;
                long release = depTime - countClose - saleRelease;
                DateTime releaseTime = _depFlight.FlyDate.AddMinutes(release);
                if ((releaseTime.Date >= _depFlight.FlyDate) && (releaseTime < DateTime.Now))
                    return false;
            }
            return true;

            #region 2011-09-09
            /*
            string tsql = @"Select FlyDate, DepTime, CountCloseTime From FlightDay (NOLOCK) 
                            Where FlightNo = @FlightNo And FlyDate = @FlyDate ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommad = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommad, "FlightNo", DbType.String, Fly.FirstOrDefault().FlightNo);
                db.AddInParameter(dbCommad, "FlyDate", DbType.DateTime, Fly.FirstOrDefault().FlyDate);
                using (IDataReader R = db.ExecuteReader(dbCommad))
                {
                    while (R.Read())
                    {
                        DateTime flyDate = (DateTime)R["FlyDate"];
                        DateTime depTime = (DateTime)R["DepTime"];
                        DateTime countCloseTime = (DateTime)R["CountCloseTime"];
                        TimeSpan TimeDifference = flyDate.AddHours(depTime.Hour).AddMinutes(depTime.Minute) - DateTime.Now.AddHours(countCloseTime.Hour).AddMinutes(countCloseTime.Minute);
                        if (TimeDifference.Ticks >= 0)
                            return true;
                        else return false;
                    }
                    return false;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommad.Connection.Close();
                dbCommad.Dispose();
            }
            */
            #endregion
        }

        public void checkFlightSeat(User UserData, string FligthNo, int? RecID, DateTime FlyDate, DateTime ResBegDate, ResDataRecord ResData, ref Int16 flySeat, ref decimal flyInfAge, ref Int16 flyInfSeat, List<SelectCustRecord> SelectCust, bool IncPack, ref string errorMsg)
        {
            errorMsg = string.Empty;
            FlightDayRecord flight = new FlightDayRecord();
            flight = getFlightDay(UserData, FligthNo, FlyDate, ref errorMsg);

            if (flight == null || !string.IsNullOrEmpty(errorMsg))
            {
                errorMsg = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "NoFlight").ToString(), FligthNo, FlyDate.ToShortDateString());
                return;
            }

            if (SelectCust == null || SelectCust.Count < 1)
            {
                errorMsg = "Check flight seat error.";
                return;
            }

            if (!RecID.HasValue)
            {
                errorMsg = "Check flight seat error.";
                return;
            }

            decimal _flyInfAge = Convert.ToDecimal(flyInfAge);
            flyInfSeat = 0;

            _flyInfAge = flight.MaxInfAge.HasValue ? flight.MaxInfAge.Value : Convert.ToDecimal(199 / 100);
            flyInfAge = _flyInfAge;


            SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);

            List<Integer> Cst = (from Rs in ResData.ResService
                                 join rCon in ResData.ResCon on Rs.RecID equals rCon.ServiceID
                                 where Rs.RecID == Convert.ToInt32(RecID)
                                 select new Integer { Value = rCon.CustNo }).ToList<Integer>();

            foreach (SelectCustRecord row1 in SelectCust)
                if (Cst.Find(f => f.Value == row1.CustNo) != null)
                    row1.Selected = true;

            List<ResCustRecord> ResCust = ResData.ResCust;
            ResMainRecord ResMain = ResData.ResMain;
            int Pax = -1;
            int Adult = -1;
            int Child = -1;
            int Infant = -1;
            if (IncPack)
            {
                if (SelectCust != null)
                {
                    Pax = (from Rc in ResCust
                           join Sc in SelectCust on Rc.CustNo equals Sc.CustNo
                           where Sc.Selected
                           select Rc).Count();
                    Adult = (from Rc in ResCust
                             join Sc in SelectCust on Rc.CustNo equals Sc.CustNo
                             where Rc.Title <= 5 && Sc.Selected
                             select Rc).Count();

                    Child = (from Rc in ResCust
                             join Sc in SelectCust on Rc.CustNo equals Sc.CustNo
                             where Rc.Title >= 6 && Rc.Title <= 7 && Sc.Selected
                             select Rc).Count();

                    Infant = (from Rc in ResCust
                              join Sc in SelectCust on Rc.CustNo equals Sc.CustNo
                              where Rc.Title > 7 && Rc.Birtday.Value.AddYears(Convert.ToInt32(_flyInfAge)) >= FlyDate && Sc.Selected
                              select Rc).Count();
                }
            }
            else
            {
                if (SelectCust != null)
                {
                    Pax = (from Rc in ResCust
                           join Sc in SelectCust on Rc.CustNo equals Sc.CustNo
                           where Sc.Selected
                           select Rc).Count();
                    Adult = (from Rc in ResCust
                             join Sc in SelectCust on Rc.CustNo equals Sc.CustNo
                             where Rc.Title <= 5 && Sc.Selected
                             select Rc).Count();

                    Child = (from Rc in ResCust
                             join Sc in SelectCust on Rc.CustNo equals Sc.CustNo
                             where Rc.Title >= 6 && Rc.Title <= 7 && Sc.Selected
                             select Rc).Count();

                    Infant = (from Rc in ResCust
                              join Sc in SelectCust on Rc.CustNo equals Sc.CustNo
                              where Rc.Title > 7 && Rc.Birtday.Value.AddYears(Convert.ToInt32(_flyInfAge)) >= FlyDate && Sc.Selected
                              select Rc).Count();
                }
            }
            if (Adult + Child + Infant != Pax)
            {
                Child = Child + (Pax - (Adult + Child + Infant));
            }

            flyInfSeat = Convert.ToInt16(Infant.ToString());
            flySeat = Convert.ToInt16((Adult + Child).ToString());
        }

        public bool CheckFlightAllot(User UserData, string Operator, DateTime? ResDate, string FlightNo, DateTime FlyDate, string SClass, int ForSeat, ref int Status, ref int FreeSeat, ref string errorMsg, string blockType, string supplier,int pNight=0)
        {
            Status = 1;
            FreeSeat = 0;
            string tsql = string.Empty;

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200010"))
                tsql = @"   Declare @Stat Int
                            Declare @FreeSeat Int
                            Declare @Supplier VarChar(10)

                            if @ServiceSupplier<>'' 
                            Begin
                              Set @Supplier=@ServiceSupplier
                            End
                            Else
                            Begin
                              Select Top 1 @Supplier=Supplier From FlightPrice (NOLOCK)
                              Where BS = 'B'
                               AND FlightNo = @FlightNo
                               And FlyDate = @FlyDate
                               And Operator = @Operator
                            End

                            exec dbo.sp_CheckFlightAlllot @FlightNo,@FlyDate,@SClass,@Operator,@Supplier,@Buyer,@BlockType,@ForSeat,@ResDate,
                                                          @Stat OutPut,@FreeSeat OutPut,@Night
                            Select Stat=@Stat, FreeSeat=isnull(@FreeSeat, 0) ";
            else
                tsql = @"Declare @Stat Int
                            Declare @FreeSeat Int
                            Declare @Supplier VarChar(10)

                            Select Top 1 @Supplier=Supplier From FlightPrice (NOLOCK)
                            Where 
                                BS = 'B'
                            AND FlightNo = @FlightNo
                            And FlyDate = @FlyDate
                            And Operator = @Operator

                            exec dbo.sp_CheckFlightAlllot @FlightNo, @FlyDate, @SClass, @Operator, @Supplier,
	                            @Buyer, @BlockType, @ForSeat, @Stat OutPut, @FreeSeat OutPut
                            Select Stat = @Stat, FreeSeat = @FreeSeat ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "SClass", DbType.String, SClass);
                db.AddInParameter(dbCommand, "Operator", DbType.String, string.IsNullOrEmpty(Operator) ? UserData.Operator : Operator);
                db.AddInParameter(dbCommand, "Buyer", DbType.String, string.Empty);
                db.AddInParameter(dbCommand, "BlockType", DbType.String, string.IsNullOrEmpty(blockType) ? "H" : blockType);
                db.AddInParameter(dbCommand, "ForSeat", DbType.Int32, ForSeat);
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200010"))
                {
                    db.AddInParameter(dbCommand, "ResDate", DbType.DateTime, ResDate);
                    db.AddInParameter(dbCommand, "Night", DbType.Int16, pNight);
                }
                    
                db.AddInParameter(dbCommand, "ServiceSupplier", DbType.AnsiString, supplier);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        Status = (int)R["Stat"];
                        object _freeSeat = R["FreeSeat"];
                        FreeSeat = Conversion.getInt32OrNull(_freeSeat).HasValue ? Conversion.getInt32OrNull(_freeSeat).Value : 0;
                        if (FreeSeat <= 0)
                        {
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                            return false;
                        }
                        if (Status != 0)
                        {
                            switch (Status)
                            {
                                case -1:
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotFound").ToString();
                                    break;
                                case 1:
                                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                                    break;
                            }
                            return false;
                        }
                        else
                            return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Int16? getFlightBlockStatus(string FlightNo, string Operator, string Supplier, ref string errorMsg)
        {
            string tsql = @"Select Top 1 ConfStat From FlightBlock (NOLOCK) 
                            Where FlightNo = @FlightNo And BS = 'B' 
                              And Operator = @Operator and Supplier = @Supplier ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Supplier", DbType.String, Supplier);
                object _retVal = (object)db.ExecuteScalar(dbCommand);
                return _retVal != null ? Conversion.getInt16OrNull(_retVal) : null;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int? getFlightOptVal(string Operator, DateTime AllotDate, string Class, int DepCity, int ArrCity, bool flightCont, string FlightNo, ref string errorMsg)
        {
            string tsql = @"Select OptVal=isnull(FlightSeat, 0)-(isnull(UsedFlightSeat,0)-dbo.ufn_get_Over_Seats(@Operator, @AllotDate, @Class, @DepCity, @ArrCity, @FlightNo, @Night))
	                        From FlightOpt M (NOLOCK) 
                            Where 
                                Operator = @Operator 
                            And AllotDate = @AllotDate 
                            And Class = @Class 
                            And Night = @Night
                            And DepCity = @DepCity 
                            And ArrCity = @ArrCity  ";
            if (flightCont && !string.IsNullOrEmpty(FlightNo))
                tsql += string.Format("   And FlightNo Like '{0}' ", FlightNo);
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "AllotDate", DbType.DateTime, AllotDate);
                db.AddInParameter(dbCommand, "Class", DbType.String, Class);
                db.AddInParameter(dbCommand, "Night", DbType.Int16, Class);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, DepCity);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, ArrCity);
                return Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightOptRecord getFlightOptRecord(string Operator, DateTime AllotDate, string Class, int DepCity, int ArrCity, bool flightCont, string FlightNo, bool allotEndDate, ref string errorMsg)
        {
            FlightOptRecord fOpt = new FlightOptRecord();
            string tsql = @"Select Operator, DepCity, ArrCity, Class, Night, AllotDate, AllotEndDate, FlightSeat, 
	                            FlightSeatTot, UsedFlightSeat, Stolen, [Control], FlightNo 
	                        From FlightOpt M (NOLOCK) 
                            Where  Operator = @Operator                                
                               And Class = @Class 
                               And DepCity = @DepCity 
                               And ArrCity = @ArrCity ";
            if (allotEndDate)
                tsql += " And AllotEndDate = @AllotDate ";
            else
                tsql += " And AllotDate = @AllotDate ";
            if (flightCont && string.IsNullOrEmpty(FlightNo))
                tsql += "   And isnull(FlightNo, '') = '' ";
            else
                if (flightCont && !string.IsNullOrEmpty(FlightNo))
                    tsql += string.Format("   And FlightNo Like '{0}' ", FlightNo);
                else
                    if (!string.IsNullOrEmpty(FlightNo))
                        tsql += string.Format("   And FlightNo = '{0}' ", FlightNo);
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "AllotDate", DbType.DateTime, AllotDate);
                db.AddInParameter(dbCommand, "Class", DbType.String, Class);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, DepCity);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, ArrCity);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        fOpt.Operator = Conversion.getStrOrNull(R["Operator"]);
                        fOpt.DepCity = (Int32)R["DepCity"];
                        fOpt.ArrCity = (Int32)R["ArrCity"];
                        fOpt.Class = Conversion.getStrOrNull(R["Class"]);
                        fOpt.Night = (Int16)R["Night"];
                        fOpt.AllotDate = (DateTime)R["AllotDate"];
                        fOpt.AllotEndDate = Conversion.getDateTimeOrNull(R["AllotEndDate"]);
                        fOpt.FlightSeat = Conversion.getInt32OrNull(R["FlightSeat"]);
                        fOpt.FlightSeatTot = Conversion.getInt32OrNull(R["FlightSeatTot"]);
                        fOpt.UsedFlightSeat = Conversion.getInt32OrNull(R["UsedFlightSeat"]);
                        fOpt.Stolen = Conversion.getInt16OrNull(R["Stolen"]);
                        fOpt.Control = Conversion.getInt16OrNull(R["Control"]);
                        fOpt.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                    }
                    else
                        fOpt.AllotDate = null;

                    return fOpt;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return fOpt;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightAndDaysRecord> getDepartureFlightAndLocation(string Operator, string Market, Int32? routeFrom, ref string errorMsg)
        {
            List<FlightAndDaysRecord> records = new List<FlightAndDaysRecord>();
            string tsql = string.Format(@"if OBJECT_ID('TempDB.dbo.#Airport') is not null Drop Table dbo.#Airport
                                            Select Code,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name)
                                            Into #Airport
                                            From Airport (NOLOCK)

                                            Select F.FlightNo, F.FlyDate, 
	                                            F.DepCity, 
                                                DepCityName=(Select Name From Location (NOLOCK) Where RecID=F.DepCity), 
                                                DepCityNameL=dbo.GetLocationNameL(F.DepCity, @Market),
	                                            F.DepAirport, DepAirportName=DA.Name, DepAirportNameL=DA.NameL, 
	                                            F.DepTime,
	                                            F.ArrCity, 
                                                ArrCityName=(Select Name From Location (NOLOCK) Where RecID=F.ArrCity), 
                                                ArrCityNameL=dbo.GetLocationNameL(F.ArrCity, @Market),
	                                            F.ArrAirport, ArrAirportName=AA.Name, ArrAirportNameL=AA.NameL,
	                                            F.ArrTime
                                            From
                                            (
	                                            Select Distinct FP.FlightNo, FP.FlyDate, F.DepCity, F.DepAirport, F.ArrCity, F.ArrAirport, F.DepTime, F.ArrTime
	                                            From FlightPrice FP (NOLOCK) 
	                                            Join FlightDay F (NOLOCK) On F.FlightNo = FP.FlightNo And F.FlyDate=FP.FlyDate
	                                            Where FP.Operator = @Operator 
	                                              And FP.FlyDate >= (dbo.DateOnly(GetDate())) 	                            

                                            ) F
                                            JOIN #Airport DA ON DA.Code=F.DepAirport
                                            JOIN #Airport AA ON AA.Code=F.ArrAirport 
                                            {0} 
                                            Order By F.FlyDate 
                                            ",
                            routeFrom.HasValue ? string.Format("Where F.DepCity={0} ",
                            routeFrom.Value.ToString()) : "");

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightAndDaysRecord row = new FlightAndDaysRecord
                        {
                            FlightNo = Conversion.getStrOrNull(oReader["FlightNo"]),
                            FlyDate = Conversion.getDateTimeOrNull(oReader["FlyDate"]),
                            DepCity = Conversion.getInt32OrNull(oReader["DepCity"]),
                            DepCityName = Conversion.getStrOrNull(oReader["DepCityName"]),
                            DepCityNameL = Conversion.getStrOrNull(oReader["DepCityNameL"]),
                            DepAirport = Conversion.getStrOrNull(oReader["DepAirport"]),
                            DepAirportName = Conversion.getStrOrNull(oReader["DepAirportName"]),
                            DepAirportNameL = Conversion.getStrOrNull(oReader["DepAirportNameL"]),
                            DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]),
                            ArrCity = Conversion.getInt32OrNull(oReader["ArrCity"]),
                            ArrCityName = Conversion.getStrOrNull(oReader["ArrCityName"]),
                            ArrCityNameL = Conversion.getStrOrNull(oReader["ArrCityNameL"]),
                            ArrAirport = Conversion.getStrOrNull(oReader["ArrAirport"]),
                            ArrAirportName = Conversion.getStrOrNull(oReader["ArrAirportName"]),
                            ArrAirportNameL = Conversion.getStrOrNull(oReader["ArrAirportNameL"]),
                            ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"])
                        };
                        records.Add(row);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightDayRecord> getRouteFlightsForOnlyFlightDays(User UserData, string Market, DateTime? FlyDate, Int32? DepCity, Int32? ArrCity, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"
Select D.FlightNo, D.Airline, D.FlgOwner, D.FlyDate, D.DepCity, D.DepAirport, D.DepTime, D.ArrCity, D.ArrAirport, D.ArrTime, 
   D.StopSale, D.BagWeight, D.MaxInfAge, D.MaxChdAge, D.NextFlight, D.CountCloseTime, D.WebPub, 
   D.AllotUseSysSet, D.AllotChk, D.AllotWarnFull, D.AllotDoNotOver, D.FlyDur, 
   D.MaxTeenAge, D.WarnUsedAmount, D.RetFlightOpt, D.RetFlightUseSysSet, D.SaleRelease, D.BagWeightForInf, 
   D.TVCCSendPNL, D.TVCCSendPNLDate, D.TDepDate, D.TArrDate, D.PNLName
From FlightDay D (NOLOCK)                             
Where  D.FlyDate >= dbo.DateOnly(GetDate())                            
  AND (@ArrCity is null or D.ArrCity=@ArrCity)
  AND (@DepCity is null or D.DepCity=@DepCity)
  AND D.FlyDate=@FlyDate
";

            List<FlightDayRecord> flightDayList = new List<FlightDayRecord>();
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, ArrCity);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, DepCity);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightDayRecord record = new FlightDayRecord();
                        record.FlightNo = (string)oReader["FlightNo"];
                        record.Airline = (string)oReader["Airline"];
                        record.FlgOwner = (string)oReader["FlgOwner"];
                        record.FlyDate = (DateTime)oReader["Flydate"];
                        record.DepCity = (int)oReader["DepCity"];
                        record.DepAirport = (string)oReader["DepAirport"];
                        record.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        record.ArrCity = (int)oReader["ArrCity"];
                        record.ArrAirport = (string)oReader["ArrAirport"];
                        record.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        record.StopSale = Equals(oReader["StopSale"], "Y");
                        record.BagWeight = Conversion.getInt16OrNull(oReader["BagWeight"]);
                        record.MaxInfAge = Conversion.getDecimalOrNull(oReader["MaxInfAge"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        record.MaxTeenAge = Conversion.getDecimalOrNull(oReader["MaxTeenAge"]);
                        record.NextFlight = Conversion.getStrOrNull(oReader["NextFlight"]);
                        record.CountCloseTime = Conversion.getDateTimeOrNull(oReader["CountCloseTime"]);
                        record.WebPub = Equals(oReader["WebPub"], "Y");
                        record.AllotUseSysSet = Equals(oReader["AllotUseSysSet"], "Y");
                        record.AllotChk = Equals(oReader["AllotChk"], "Y");
                        record.AllotWarnFull = Equals(oReader["AllotWarnFull"], "Y");
                        record.AllotDoNotOver = Equals(oReader["AllotDoNotOver"], "Y");
                        record.FlyDur = (int)oReader["FlyDur"];
                        record.WarnUsedAmount = Conversion.getInt16OrNull(oReader["WarnUsedAmount"]);
                        record.RetFlightOpt = Conversion.getByteOrNull(oReader["RetFlightOpt"]);
                        record.RetFlightUseSysSet = Equals(oReader["RetFlightUseSysSet"], "Y");
                        record.SaleRelease = Conversion.getDateTimeOrNull(oReader["SaleRelease"]);
                        record.BagWeightForInf = Conversion.getDecimalOrNull(oReader["BagWeightForInf"]);
                        record.TVCCSendPNL = Conversion.getBoolOrNull(oReader["TVCCSendPNL"]);
                        record.TVCCSendPNLDate = Conversion.getDateTimeOrNull(oReader["TVCCSendPNLDate"]);
                        record.TDepDate = Conversion.getDateTimeOrNull(oReader["TDepDate"]);
                        record.TArrDate = Conversion.getDateTimeOrNull(oReader["TArrDate"]);
                        record.PNLName = Conversion.getStrOrNull(oReader["PNLName"]);
                        flightDayList.Add(record);
                    }
                }
                return flightDayList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return flightDayList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightDayRecord> getRouteFlights(User UserData, string Market, DateTime? FlyDate, Int32? DepCity, Int32? ArrCity, string HolPack, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (string.IsNullOrEmpty(HolPack))
                tsql = @"Select D.FlightNo, D.Airline, D.FlgOwner, D.FlyDate, D.DepCity, D.DepAirport, D.DepTime, D.ArrCity, D.ArrAirport, D.ArrTime, 
                               D.StopSale, D.BagWeight, D.MaxInfAge, D.MaxChdAge, D.NextFlight, D.CountCloseTime, D.WebPub, 
                               D.AllotUseSysSet, D.AllotChk, D.AllotWarnFull, D.AllotDoNotOver, D.FlyDur, 
                               D.MaxTeenAge, D.WarnUsedAmount, D.RetFlightOpt, D.RetFlightUseSysSet, D.SaleRelease, D.BagWeightForInf, 
                               D.TVCCSendPNL, D.TVCCSendPNLDate, D.TDepDate, D.TArrDate, D.PNLName
                         From FlightPrice P (NOLOCK)    
                         Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                         Where  P.Buyer = '' 
                            AND P.FlyDate >= dbo.DateOnly(GetDate())
                            AND Exists(	Select Distinct CP.Operator From CatPackMarket CPM (NOLOCK)
                                        Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                                        Where 		
                                            CP.Ready = 'Y' 
                                            And CP.WebPub = 'Y' 
                                            And CP.WebPubDate <= GetDate()
                                            And CP.SaleEndDate >= dbo.DateOnly(GetDate())
                                            And CPM.Market = @Market
                                            AND CP.Operator = P.Operator
                                       )
                            AND (@ArrCity is null or D.ArrCity=@ArrCity)
                            AND D.DepCity=@DepCity 
                            AND D.FlyDate=@FlyDate
                            ";
            else
                tsql = @"Select D.FlightNo, D.Airline, D.FlgOwner, D.FlyDate, D.DepCity, D.DepAirport, D.DepTime, D.ArrCity, D.ArrAirport, D.ArrTime, 
                               D.StopSale, D.BagWeight, D.MaxInfAge, D.MaxChdAge, D.NextFlight, D.CountCloseTime, D.WebPub, 
                               D.AllotUseSysSet, D.AllotChk, D.AllotWarnFull, D.AllotDoNotOver, D.FlyDur, 
                               D.MaxTeenAge, D.WarnUsedAmount, D.RetFlightOpt, D.RetFlightUseSysSet, D.SaleRelease, D.BagWeightForInf, 
                               D.TVCCSendPNL, D.TVCCSendPNLDate, D.TDepDate, D.TArrDate, D.PNLName
                         From FlightPrice P (NOLOCK)    
                         Join FlightDay D (NOLOCK) ON P.FlightNo=D.FlightNo and P.FlyDate=D.FlyDate
                         Join HolPackSer H (NOLOCK) ON H.HolPack=@HolPack And H.Service='FLIGHT' And H.ServiceItem=D.FlightNo
                         Where  P.Buyer = '' 
                            AND P.FlyDate >= dbo.DateOnly(GetDate())
                            AND Exists(	Select Distinct CP.Operator From CatPackMarket CPM (NOLOCK)
                                        Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
                                        Where 		
                                            CP.Ready = 'Y' 
                                            And CP.WebPub = 'Y' 
                                            And CP.WebPubDate <= GetDate()
                                            And CP.SaleEndDate >= dbo.DateOnly(GetDate())
                                            And CPM.Market = @Market
                                            AND CP.Operator = P.Operator
                                       )
                            AND (@ArrCity is null or D.ArrCity=@ArrCity)
                            AND D.DepCity=@DepCity 
                            AND D.FlyDate=@FlyDate";
            List<FlightDayRecord> flightDayList = new List<FlightDayRecord>();
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, ArrCity);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, DepCity);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "HolPack", DbType.String, HolPack);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightDayRecord record = new FlightDayRecord();
                        record.FlightNo = (string)oReader["FlightNo"];
                        record.Airline = (string)oReader["Airline"];
                        record.FlgOwner = (string)oReader["FlgOwner"];
                        record.FlyDate = (DateTime)oReader["Flydate"];
                        record.DepCity = (int)oReader["DepCity"];
                        record.DepAirport = (string)oReader["DepAirport"];
                        record.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        record.ArrCity = (int)oReader["ArrCity"];
                        record.ArrAirport = (string)oReader["ArrAirport"];
                        record.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        record.StopSale = Equals(oReader["StopSale"], "Y");
                        record.BagWeight = Conversion.getInt16OrNull(oReader["BagWeight"]);
                        record.MaxInfAge = Conversion.getDecimalOrNull(oReader["MaxInfAge"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        record.MaxTeenAge = Conversion.getDecimalOrNull(oReader["MaxTeenAge"]);
                        record.NextFlight = Conversion.getStrOrNull(oReader["NextFlight"]);
                        record.CountCloseTime = Conversion.getDateTimeOrNull(oReader["CountCloseTime"]);
                        record.WebPub = Equals(oReader["WebPub"], "Y");
                        record.AllotUseSysSet = Equals(oReader["AllotUseSysSet"], "Y");
                        record.AllotChk = Equals(oReader["AllotChk"], "Y");
                        record.AllotWarnFull = Equals(oReader["AllotWarnFull"], "Y");
                        record.AllotDoNotOver = Equals(oReader["AllotDoNotOver"], "Y");
                        record.FlyDur = (int)oReader["FlyDur"];
                        record.WarnUsedAmount = Conversion.getInt16OrNull(oReader["WarnUsedAmount"]);
                        record.RetFlightOpt = Conversion.getByteOrNull(oReader["RetFlightOpt"]);
                        record.RetFlightUseSysSet = Equals(oReader["RetFlightUseSysSet"], "Y");
                        record.SaleRelease = Conversion.getDateTimeOrNull(oReader["SaleRelease"]);
                        record.BagWeightForInf = Conversion.getDecimalOrNull(oReader["BagWeightForInf"]);
                        record.TVCCSendPNL = Conversion.getBoolOrNull(oReader["TVCCSendPNL"]);
                        record.TVCCSendPNLDate = Conversion.getDateTimeOrNull(oReader["TVCCSendPNLDate"]);
                        record.TDepDate = Conversion.getDateTimeOrNull(oReader["TDepDate"]);
                        record.TArrDate = Conversion.getDateTimeOrNull(oReader["TArrDate"]);
                        record.PNLName = Conversion.getStrOrNull(oReader["PNLName"]);
                        flightDayList.Add(record);
                    }
                }
                return flightDayList;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return flightDayList;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<String> getFlightClass(string Market, string FlightNo, DateTime FlyDate, ref string errorMsg)
        {
            string tsql = @"Select Distinct P.SClass
                            From FlightPrice P (NOLOCK)    
                            Join FlightDay D (NOLOCK) ON P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                            Where  P.Buyer = '' 
                               AND P.FlyDate >= dbo.DateOnly(GetDate())
                               AND Exists(	Select Distinct CP.Operator From CatPackMarket CPM (NOLOCK)
				                            Join CatalogPack CP (NOLOCK) ON CP.RecID = CPM.CatPackID
				                            Where 		
					                            CP.Ready = 'Y' 
					                            And CP.WebPub = 'Y' 
					                            And CP.WebPubDate <= GetDate()
					                            And CP.SaleEndDate >= dbo.DateOnly(GetDate())
					                            And CPM.Market = @Market
					                            AND CP.Operator = P.Operator
			                               )
	                            AND D.FlightNo=@FlightNo
	                            AND D.FlyDate=@FlyDate ";
            List<String> records = new List<String>();
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        records.Add((string)oReader["SClass"]);
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightClassRecord> getFlightClass(string Market, ref string errorMsg)
        {
            string tsql = @"Select RecID, Class, Name, NameS, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name),
	                            Category, B2B, B2C
                            From FlightClass (NOLOCK) ";
            List<FlightClassRecord> records = new List<FlightClassRecord>();
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightClassRecord record = new FlightClassRecord();
                        record.RecID = (int)oReader["RecID"];
                        record.Class = Conversion.getStrOrNull(oReader["Class"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.Category = Conversion.getStrOrNull(oReader["Category"]);
                        record.B2B = (bool)oReader["B2B"];
                        record.B2C = (bool)oReader["B2C"];
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightOptRecord getFlightOpt(string Operator, string FlightNo, DateTime? Date, Int16? Night, string SClass, int? DepCity, int? ArrCity, ref string errorMsg)
        {
            string tsql = @"Select * From FlightOpt M (NOLOCK) 
                            Where 
                                Operator = @Operator ";

            switch (FlightNo)
            {
                case "":
                    break;
                case " ":
                    tsql += " And isnull(FlightNo,'')='' ";
                    break;
                case "%":
                    tsql += string.Format(" And FlightNo Like '{0}' ", FlightNo);
                    break;
                default:
                    tsql += string.Format(" And FlightNo='{0}' ", FlightNo);
                    break;
            }
            /*
            if (!string.IsNullOrEmpty(FlightNo))
                tsql += string.Format(" And FlightNo='{0}' ", FlightNo);
            else if (Equals(FlightNo, " "))
                tsql += " And isnull(FlightNo, '') = '' ";
            */
            if (Date.HasValue)
                tsql += " And AllotDate=@AllotDate ";

            if (!string.IsNullOrEmpty(SClass))
                tsql += string.Format(" And Class='{0}' ", SClass);

            if (DepCity.HasValue)
                tsql += string.Format(" And DepCity={0}", DepCity.Value);

            if (ArrCity.HasValue)
                tsql += string.Format(" And ArrCity={0}", ArrCity.Value);

            if (Night.HasValue)
                tsql += string.Format(" And Night={0}", Night.Value);

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                if (Date.HasValue)
                    db.AddInParameter(dbCommand, "AllotDate", DbType.DateTime, Date.Value);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        FlightOptRecord record = new FlightOptRecord();
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        record.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        record.Class = Conversion.getStrOrNull(R["Class"]);
                        record.Night = Conversion.getInt16OrNull(R["Night"]);
                        record.AllotDate = Conversion.getDateTimeOrNull(R["AllotDate"]);
                        record.AllotEndDate = Conversion.getDateTimeOrNull(R["AllotEndDate"]);
                        record.FlightSeat = Conversion.getInt32OrNull(R["FlightSeat"]);
                        record.FlightSeatTot = Conversion.getInt32OrNull(R["FlightSeatTot"]);
                        record.UsedFlightSeat = Conversion.getInt32OrNull(R["UsedFlightSeat"]);
                        record.Stolen = Conversion.getInt16OrNull(R["Stolen"]);
                        record.Control = Conversion.getInt16OrNull(R["Control"]);
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public Int16? getFlightOptVal(string Operator, string FlightNo, DateTime? Date, Int16? Night, string SClass, int? DepCity, int? ArrCity, ref string errorMsg)
        {
            string tsql = @"Select OptVal=isnull(FlightSeat, 0)-(isnull(UsedFlightSeat,0)-dbo.ufn_get_Over_Seats(@Operator, @AllotDate, @Class, @DepCity, @ArrCity, @FlightNo, @Night))
                            From FlightOpt M (NOLOCK) 
                            Where 
                                Operator = @Operator 
                                And AllotDate=@AllotDate
                                And Class=@Class
                                And DepCity=@DepCity
                                And ArrCity=@ArrCity
                            ";
            if (Night.HasValue && Night.Value > 0)
                tsql += @"     And Night=@Night 
                         ";
            //And FlightNo LIKE @FlightNo ";

            if (!string.IsNullOrEmpty(FlightNo))
                tsql += string.Format(" And FlightNo LIKE '{0}' ", FlightNo);
            //else
            //    if (Equals(FlightNo, " "))
            //        tsql += " And isnull(FlightNo, '') = '' ";

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "AllotDate", DbType.DateTime, Date.HasValue ? Date.Value : Convert.DBNull);
                db.AddInParameter(dbCommand, "Class", DbType.String, SClass);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, DepCity.HasValue ? DepCity.Value : Convert.DBNull);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, ArrCity.HasValue ? ArrCity.Value : Convert.DBNull);
                db.AddInParameter(dbCommand, "Night", DbType.Int16, Night.HasValue ? Night.Value : Convert.DBNull);
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, string.IsNullOrEmpty(FlightNo) ? "%" : FlightNo);
                object _retVal = db.ExecuteScalar(dbCommand);
                return Conversion.getInt16OrNull(_retVal);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public TicketRestRecord getTicketRest(string Operator, string FlightNo, DateTime FlyDate, string SClass, Int16 Day, ref string errorMsg)
        {
            string tsql = @"SET DATEFIRST 1
                            SELECT Top 1 * FROM TicketRest (NOLOCK) 
                            WHERE   Operator=@Operator 
                                And (FlightNo=@FlightNo OR FlightNo='') 
                                And (Class=@Class OR Class='') 
                                And  @FlyDate between BegDate And EndDate 
                                And (@Day between FromDay and ToDay or FromDay=99) 
                                And (abs(DateDiff(Day,@FlyDate, dbo.DateOnly(GetDate())))<=DayOption ) 
                                And SubString(FlyDays,DATEPART(dw,@FlyDate),1)=1
                            ORDER BY Operator, FlightNo, Class, FromDay, DayOption ";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "Class", DbType.String, SClass);
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "Day", DbType.String, Day);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        TicketRestRecord record = new TicketRestRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        record.Class = Conversion.getStrOrNull(R["Class"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.FromDay = Conversion.getInt16OrNull(R["FromDay"]);
                        record.DayOption = Conversion.getInt16OrNull(R["DayOption"]);
                        record.SalePerc = Conversion.getInt16OrNull(R["SalePerc"]);
                        record.StopSale = Conversion.getStrOrNull(R["StopSale"]);
                        record.TVActive = Conversion.getStrOrNull(R["TVActive"]);
                        record.ToDay = Conversion.getInt16OrNull(R["ToDay"]);
                        record.FlyDays = Conversion.getStrOrNull(R["FlyDays"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HolPackFlightRecord> getHolPackFlight(string HolPack, ref string errorMsg)
        {
            List<HolPackFlightRecord> records = new List<HolPackFlightRecord>();

            string tsql = @"Select Service,Departure,Arrival,StepType,StartDay=isnull(StartDay,1)
                            From CatPackPlan (NOLOCK)
                            Where Service = 'FLIGHT' 
                              And HolPack = @Holpack ";

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "HolPack", DbType.String, HolPack);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        HolPackFlightRecord record = new HolPackFlightRecord();
                        record.Service = Conversion.getStrOrNull(oReader["Service"]);
                        record.Arrival = Conversion.getInt32OrNull(oReader["Arrival"]);
                        record.Departure = Conversion.getInt32OrNull(oReader["Departure"]);
                        record.StepType = Conversion.getInt16OrNull(oReader["StepType"]);
                        record.StartDay = Conversion.getInt16OrNull(oReader["StartDay"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HolPackFlightRecord> getHolPackTransport(string HolPack, ref string errorMsg)
        {
            List<HolPackFlightRecord> records = new List<HolPackFlightRecord>();

            string tsql = @"Select Service,Departure,Arrival,StepType,StartDay=isnull(StartDay,1)
                            From CatPackPlan (NOLOCK)
                            Where Service = 'TRANSPORT' 
                              And HolPack = @Holpack ";

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "HolPack", DbType.String, HolPack);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        HolPackFlightRecord record = new HolPackFlightRecord();
                        record.Service = Conversion.getStrOrNull(oReader["Service"]);
                        record.Arrival = Conversion.getInt32OrNull(oReader["Arrival"]);
                        record.Departure = Conversion.getInt32OrNull(oReader["Departure"]);
                        record.StepType = Conversion.getInt16OrNull(oReader["StepType"]);
                        record.StartDay = Conversion.getInt16OrNull(oReader["StartDay"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<LockedPLFlightRecord> getFlight4AgeLockFlight(ResDataRecord ResData, int RefNo, ref string errorMsg)
        {
            List<LockedPLFlightRecord> records = new List<LockedPLFlightRecord>();
            string tsql = @"if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
                            CREATE TABLE #Flight
                            (
	                            FlightNo varchar(10),	                                
	                            FlyDate DateTime,	
	                            Supplier varchar(10),
	                            AllotType varchar(1),		
	                            StepNo SmallInt	
                            )
                            truncate table #Flight                            

                            Declare @Supplier VarChar(10)

                            Select Top 1 @Supplier=P.Supplier
                            from FlightDay D (NOLOCK)
                            Join FlightPrice P (NOLOCK) on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                            Join FlightBlock B (NOLOCK) on B.FlightNo = D.FlightNo and B.RecID = P.BlockID
                            where D.FlightNo = @DepFlight and D.FlyDate = @DepDate and isnull(P.Supplier,'') <> '' and P.Operator = @PLOperator
                            order by B.SalePriority

                            Insert Into #Flight (FlightNo, FlyDate, Supplier, AllotType, StepNo) Values 
	                            (@DepFlight, @DepDate, @Supplier, 'H', 1)
                            	
                            Select Top 1 @Supplier=P.Supplier
                            from FlightDay D (NOLOCK)
                            Join FlightPrice P (NOLOCK) on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                            Join FlightBlock B (NOLOCK) on B.FlightNo = D.FlightNo and B.RecID = P.BlockID
                            where D.FlightNo = @RetFlight and D.FlyDate = @RetDate and isnull(P.Supplier,'') <> '' and P.Operator = @PLOperator
                            order by B.SalePriority

                            Insert Into #Flight (FlightNo, FlyDate, Supplier, AllotType, StepNo) Values 
	                            (@RetFlight, @RetDate, @Supplier, 'H', 2)
                            	
                            Select * From #Flight";

            var selectBook = from q in ResData.SelectBook
                             where q.RefNo == RefNo
                             select new
                             {
                                 RefNo = q.RefNo,
                                 Market = q.Market,
                                 Operator = q.Operator,
                                 DepFlight = q.DepFlight,
                                 DepDate = q.CheckIn,
                                 RetFlight = q.RetFlight,
                                 RetDate = q.CheckOut
                             };
            if (selectBook.Count() < 1)
                return null;
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PLmarket", DbType.String, selectBook.FirstOrDefault().Market);
                db.AddInParameter(dbCommand, "PLOperator", DbType.String, selectBook.FirstOrDefault().Operator);
                db.AddInParameter(dbCommand, "DepFlight", DbType.String, selectBook.FirstOrDefault().DepFlight);
                db.AddInParameter(dbCommand, "DepDate", DbType.DateTime, selectBook.FirstOrDefault().DepDate);
                db.AddInParameter(dbCommand, "RetFlight", DbType.String, selectBook.FirstOrDefault().RetFlight);
                db.AddInParameter(dbCommand, "RetDate", DbType.DateTime, selectBook.FirstOrDefault().RetDate);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        LockedPLFlightRecord record = new LockedPLFlightRecord();
                        record.AllotType = Conversion.getStrOrNull(R["AllotType"]);
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        record.FlyDate = Conversion.getDateTimeOrNull(R["FlyDate"]);
                        record.SClass = string.Empty;
                        record.StepNo = Conversion.getInt16OrNull(R["StepNo"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<LockedPLFlightRecord> getFlight4AgeLockFlightClass(ResDataRecord ResData, int RefNo, ref string errorMsg)
        {
            List<LockedPLFlightRecord> records = new List<LockedPLFlightRecord>();
            string tsql = @"if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
                            CREATE TABLE #Flight
                            (
                                FlightNo varchar(10),
                                FlyDate DateTime,	
                                Supplier varchar(10),
                                AllotType varchar(1),		
                                SClass varchar(3),
                                StepNo SmallInt	
                            )
                            truncate table #Flight                            

                            Declare @Supplier VarChar(10)

                            Select Top 1 @Supplier=P.Supplier
                            from FlightDay D (NOLOCK)
                            Join FlightPrice P (NOLOCK) on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                            Join FlightBlock B (NOLOCK) on B.FlightNo = D.FlightNo and B.RecID = P.BlockID
                            where D.FlightNo = @DepFlight and D.FlyDate = @DepDate and P.SClass = @DepClass and isnull(P.Supplier,'') <> '' and P.Operator = @PLOperator
                            order by B.SalePriority

                            Insert Into #Flight (FlightNo, FlyDate, Supplier, AllotType, SClass, StepNo) Values 
                                (@DepFlight, @DepDate, @Supplier, 'H', @DepClass, 1)
                            	
                            Select Top 1 @Supplier=P.Supplier
                            from FlightDay D (NOLOCK)
                            Join FlightPrice P (NOLOCK) on P.FlightNo = D.FlightNo and P.FlyDate = D.FlyDate
                            Join FlightBlock B (NOLOCK) on B.FlightNo = D.FlightNo and B.RecID = P.BlockID
                            where D.FlightNo = @RetFlight and D.FlyDate = @RetDate and P.SClass = @RetClass and isnull(P.Supplier,'') <> '' and P.Operator = @PLOperator
                            order by B.SalePriority

                            Insert Into #Flight (FlightNo, FlyDate, Supplier, AllotType, SClass, StepNo) Values 
                                (@RetFlight, @RetDate, @Supplier, 'H', @RetClass, 2)
                            	
                            Select * From #Flight";

            var selectBook = from q in ResData.SelectBook
                             where q.RefNo == RefNo
                             select new
                             {
                                 RefNo = q.RefNo,
                                 Market = q.Market,
                                 Operator = q.Operator,
                                 DepFlight = q.DepFlight,
                                 DepDate = q.CheckIn,
                                 DepClass = q.DepClass,
                                 RetFlight = q.RetFlight,
                                 RetDate = q.CheckOut,
                                 RetClass = q.RetClass
                             };
            if (selectBook.Count() < 1)
                return null;
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "PLmarket", DbType.String, selectBook.FirstOrDefault().Market);
                db.AddInParameter(dbCommand, "PLOperator", DbType.String, selectBook.FirstOrDefault().Operator);
                db.AddInParameter(dbCommand, "DepFlight", DbType.String, selectBook.FirstOrDefault().DepFlight);
                db.AddInParameter(dbCommand, "DepDate", DbType.DateTime, selectBook.FirstOrDefault().DepDate);
                db.AddInParameter(dbCommand, "RetFlight", DbType.String, selectBook.FirstOrDefault().RetFlight);
                db.AddInParameter(dbCommand, "RetDate", DbType.DateTime, selectBook.FirstOrDefault().RetDate);
                db.AddInParameter(dbCommand, "DepClass", DbType.String, selectBook.FirstOrDefault().DepClass);
                db.AddInParameter(dbCommand, "RetClass", DbType.String, selectBook.FirstOrDefault().RetClass);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        LockedPLFlightRecord record = new LockedPLFlightRecord();
                        record.AllotType = Conversion.getStrOrNull(R["AllotType"]);
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        record.FlyDate = Conversion.getDateTimeOrNull(R["FlyDate"]);
                        record.SClass = Conversion.getStrOrNull(R["SClass"]);
                        record.StepNo = Conversion.getInt16OrNull(R["StepNo"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool getFlightsAndAllotment4AgeLockFlight(User UserData, ResDataRecord ResData, ref List<plResServiceRecord> PLService, int? RefNo, bool IncPack, ref string errorMsg)
        {
            StringBuilder errorStr = new StringBuilder();

            ParamFlight plFligthParam = UserData.TvParams.TvParamFlight;

            List<LockedPLFlightRecord> flight = new Flights().getFlight4AgeLockFlight(ResData, RefNo.HasValue ? RefNo.Value : -1, ref errorMsg);

            List<plResServiceRecord> PLServiceRow = PLService.Where(w => w.ServiceType == "FLIGHT" && w.StepType != 0).Select(s => s).ToList<plResServiceRecord>();

            if (flight.Count > 0 && PLServiceRow.Count > 0)
            {
                PLServiceRow[0].Service = flight[0].FlightNo;
                PLServiceRow[0].Supplier = flight[0].Supplier;
                PLServiceRow[0].AllotType = flight[0].AllotType;
            }
            if (flight.Count > 1 && PLServiceRow.Count > 1)
            {
                PLServiceRow[1].Service = flight[1].FlightNo;
                PLServiceRow[1].Supplier = flight[1].Supplier;
                PLServiceRow[1].AllotType = flight[1].AllotType;
            }
            return true;
        }

        public bool getFlightsAndAllotment4AgeLockFlight(User UserData, ResDataRecord ResData, ref List<plResServiceRecord> PLService, int? RefNo, bool IncPack, string DepClass, string RetClass, ref string errorMsg)
        {
            StringBuilder errorStr = new StringBuilder();

            ParamFlight plFligthParam = UserData.TvParams.TvParamFlight;

            List<LockedPLFlightRecord> flight = new Flights().getFlight4AgeLockFlightClass(ResData, RefNo.HasValue ? RefNo.Value : -1, ref errorMsg);

            List<plResServiceRecord> PLServiceRow = PLService.Where(w => w.ServiceType == "FLIGHT" && w.StepType != 0).Select(s => s).ToList<plResServiceRecord>();

            if (flight.Count > 0 && PLServiceRow.Count > 0)
            {
                PLServiceRow[0].Service = flight[0].FlightNo;
                PLServiceRow[0].Supplier = flight[0].Supplier;
                PLServiceRow[0].AllotType = flight[0].AllotType;
                PLServiceRow[0].SClass = flight[0].SClass;
            }
            if (flight.Count > 1 && PLServiceRow.Count > 1)
            {
                PLServiceRow[1].Service = flight[1].FlightNo;
                PLServiceRow[1].Supplier = flight[1].Supplier;
                PLServiceRow[1].AllotType = flight[1].AllotType;
                PLServiceRow[1].SClass = flight[1].SClass;
            }
            return true;
        }

        public List<AirlineRecord> getAirlines(string Market, ref string errorMsg)
        {
            List<AirlineRecord> records = new List<AirlineRecord>();
            string tsql = @"Select RecID, Code, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name),
	                            NameS, Phone1, Phone2, Fax, Web, Email, CarCode, Explanation,
	                            AirlineCode, LicenseNo, [Address], AddrZip, AddrCity, AddrCountry, ETicketExists,
	                            PNLUse, PNLFullName, PNLFlightSpace, PNLNameSpace, PNLChdRemark, PNLSitatex, PNLMaxLineInPart,
	                            PNLDispExtra, PNLDispPNR, PNLDotAfterTitle, PNLAutoSend, PNLMailTo, PNLMailCC, 
	                            PNLResNoDispPNR, PNLDispGrpNo, TicketIssued
                            From AirLine (NOLOCK)";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AirlineRecord record = new AirlineRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Phone1 = Conversion.getStrOrNull(R["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(R["Phone2"]);
                        record.Fax = Conversion.getStrOrNull(R["Fax"]);
                        record.Web = Conversion.getStrOrNull(R["Web"]);
                        record.Email = Conversion.getStrOrNull(R["Email"]);
                        record.CarCode = Conversion.getStrOrNull(R["CarCode"]);
                        record.Explanation = Conversion.getStrOrNull(R["Explanation"]);
                        record.AirlineCode = Conversion.getStrOrNull(R["AirlineCode"]);
                        record.LicenseNo = Conversion.getStrOrNull(R["LicenseNo"]);
                        record.Address = Conversion.getStrOrNull(R["Address"]);
                        record.AddrZip = Conversion.getStrOrNull(R["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(R["AddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(R["AddrCountry"]);
                        record.ETicketExists = Conversion.getBoolOrNull(R["ETicketExists"]);
                        record.PNLUse = Conversion.getBoolOrNull(R["PNLUse"]);
                        record.PNLFullName = Conversion.getStrOrNull(R["PNLFullName"]);
                        record.PNLFlightSpace = Conversion.getStrOrNull(R["PNLFlightSpace"]);
                        record.PNLNameSpace = Conversion.getStrOrNull(R["PNLNameSpace"]);
                        record.PNLChdRemark = Conversion.getStrOrNull(R["PNLChdRemark"]);
                        record.PNLSitatex = Conversion.getStrOrNull(R["PNLSitatex"]);
                        record.PNLMaxLineInPart = Conversion.getInt32OrNull(R["PNLMaxLineInPart"]);
                        record.PNLDispExtra = Conversion.getStrOrNull(R["PNLDispExtra"]);
                        record.PNLDispPNR = Conversion.getStrOrNull(R["PNLDispPNR"]);
                        record.PNLDotAfterTitle = Conversion.getStrOrNull(R["PNLDotAfterTitle"]);
                        record.PNLAutoSend = Conversion.getBoolOrNull(R["PNLAutoSend"]);
                        record.PNLMailTo = Conversion.getStrOrNull(R["PNLMailTo"]);
                        record.PNLMailCC = Conversion.getStrOrNull(R["PNLMailCC"]);
                        record.PNLResNoDispPNR = Conversion.getStrOrNull(R["PNLResNoDispPNR"]);
                        record.PNLDispGrpNo = Conversion.getBoolOrNull(R["PNLDispGrpNo"]);
                        record.TicketIssued = Conversion.getBoolOrNull(R["TicketIssued"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public AirlineRecord getAirline(string Market, string Code, ref string errorMsg)
        {

            string tsql = @"Select RecID, Code, Name, NameL=ISNULL(dbo.FindLocalName(NameLID, @Market), Name),
	                            NameS, Phone1, Phone2, Fax, Web, Email, CarCode, Explanation,
	                            AirlineCode, LicenseNo, [Address], AddrZip, AddrCity, AddrCountry, ETicketExists,
	                            PNLUse, PNLFullName, PNLFlightSpace, PNLNameSpace, PNLChdRemark, PNLSitatex, PNLMaxLineInPart,
	                            PNLDispExtra, PNLDispPNR, PNLDotAfterTitle, PNLAutoSend, PNLMailTo, PNLMailCC, 
	                            PNLResNoDispPNR, PNLDispGrpNo, TicketIssued
                            From AirLine (NOLOCK)
                            Where Code=@Code ";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        AirlineRecord record = new AirlineRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Phone1 = Conversion.getStrOrNull(R["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(R["Phone2"]);
                        record.Fax = Conversion.getStrOrNull(R["Fax"]);
                        record.Web = Conversion.getStrOrNull(R["Web"]);
                        record.Email = Conversion.getStrOrNull(R["Email"]);
                        record.CarCode = Conversion.getStrOrNull(R["CarCode"]);
                        record.Explanation = Conversion.getStrOrNull(R["Explanation"]);
                        record.AirlineCode = Conversion.getStrOrNull(R["AirlineCode"]);
                        record.LicenseNo = Conversion.getStrOrNull(R["LicenseNo"]);
                        record.Address = Conversion.getStrOrNull(R["Address"]);
                        record.AddrZip = Conversion.getStrOrNull(R["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(R["AddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(R["AddrCountry"]);
                        record.ETicketExists = Conversion.getBoolOrNull(R["ETicketExists"]);
                        record.PNLUse = Conversion.getBoolOrNull(R["PNLUse"]);
                        record.PNLFullName = Conversion.getStrOrNull(R["PNLFullName"]);
                        record.PNLFlightSpace = Conversion.getStrOrNull(R["PNLFlightSpace"]);
                        record.PNLNameSpace = Conversion.getStrOrNull(R["PNLNameSpace"]);
                        record.PNLChdRemark = Conversion.getStrOrNull(R["PNLChdRemark"]);
                        record.PNLSitatex = Conversion.getStrOrNull(R["PNLSitatex"]);
                        record.PNLMaxLineInPart = Conversion.getInt32OrNull(R["PNLMaxLineInPart"]);
                        record.PNLDispExtra = Conversion.getStrOrNull(R["PNLDispExtra"]);
                        record.PNLDispPNR = Conversion.getStrOrNull(R["PNLDispPNR"]);
                        record.PNLDotAfterTitle = Conversion.getStrOrNull(R["PNLDotAfterTitle"]);
                        record.PNLAutoSend = Conversion.getBoolOrNull(R["PNLAutoSend"]);
                        record.PNLMailTo = Conversion.getStrOrNull(R["PNLMailTo"]);
                        record.PNLMailCC = Conversion.getStrOrNull(R["PNLMailCC"]);
                        record.PNLResNoDispPNR = Conversion.getStrOrNull(R["PNLResNoDispPNR"]);
                        record.PNLDispGrpNo = Conversion.getBoolOrNull(R["PNLDispGrpNo"]);
                        record.TicketIssued = Conversion.getBoolOrNull(R["TicketIssued"]);

                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public getPLFlightsRecord getReturnFlight(string Market, string DepFlightNo, DateTime? DepFlyDate, string DepClass, Int16? Night, Int16? ForSeat, ref string errorMsg)
        {

            string tsql = @"Select DepAirline=FD.Airline, DepFlgClass=@DepClass, 
	                            DepFlgClassName=(Select ISNULL(dbo.FindLocalName(NameLID, @Market), Name) From FlightClass (NOLOCK) Where Class=@DepClass),
	                            DepFlightNo=FD.FlightNo, DepFlyDate=FD.FlyDate, DepFlyTime=FD.DepTime, DepSeat=@FlySeat, NextFlightNo=FD.NextFlight, RetFlgClass=@DepClass,
	                            RetFlgClassName=(Select ISNULL(dbo.FindLocalName(NameLID, @Market), Name) From FlightClass (NOLOCK) Where Class=@DepClass),
	                            RetFlightNo=FD.NextFlight, RetFlyDate=FDr.FlyDate, RetFlyTime=FDr.DepTime, RetSeat=@FlySeat
                            From FlightDay FD (NOLOCK)
                            Join FlightDay FDr (NOLOCK) ON FDr.FlyDate=FD.FlyDate+@Night And FDr.FlightNo=FD.NextFlight
                            Where FD.FlightNo=@FlightNo
                              And FD.FlyDate=@FlyDate";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, DepFlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.Date, DepFlyDate);
                db.AddInParameter(dbCommand, "Night", DbType.Int16, Night);
                db.AddInParameter(dbCommand, "FlySeat", DbType.Int16, ForSeat);
                db.AddInParameter(dbCommand, "DepClass", DbType.String, DepClass);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        getPLFlightsRecord record = new getPLFlightsRecord();
                        record.DepAirline = Conversion.getStrOrNull(R["DepAirline"]);
                        record.DepFlgClass = Conversion.getStrOrNull(R["DepFlgClass"]);
                        record.DepFlgClassName = Conversion.getStrOrNull(R["DepFlgClassName"]);
                        record.DepFlightNo = Conversion.getStrOrNull(R["DepFlightNo"]);
                        record.DepFlyDate = Conversion.getDateTimeOrNull(R["DepFlyDate"]);
                        record.DepFlyTime = Conversion.getDateTimeOrNull(R["DepFlyTime"]);
                        record.DepSeat = Conversion.getInt16OrNull(R["DepSeat"]);
                        record.NextFlightNo = Conversion.getStrOrNull(R["NextFlightNo"]);
                        record.RetFlgClass = Conversion.getStrOrNull(R["RetFlgClass"]);
                        record.RetFlgClassName = Conversion.getStrOrNull(R["RetFlgClassName"]);
                        record.RetFlightNo = Conversion.getStrOrNull(R["RetFlightNo"]);
                        record.RetFlyDate = Conversion.getDateTimeOrNull(R["RetFlyDate"]);
                        record.RetFlyTime = Conversion.getDateTimeOrNull(R["RetFlyTime"]);
                        record.RetSeat = Conversion.getInt16OrNull(R["RetSeat"]);

                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<getPLFlightsRecord> getPListFlights(User UserData, ResDataRecord ResData, Int16? ForSeat, ref string errorMsg)
        {
            List<getPLFlightsRecord> records = new List<getPLFlightsRecord>();
            string tsql = @"Exec dbo.GetFlightInfosClassBase @Operator, @Market, @FlyDate, @Night, @Holpack, @CatPackID, null, @DepCity, @ArrCity, null, @FlySeat";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, ResData.ResMain.PLOperator);
                db.AddInParameter(dbCommand, "Market", DbType.String, ResData.ResMain.PLMarket);
                db.AddInParameter(dbCommand, "FlyDate", DbType.Date, ResData.ResMain.BegDate);
                db.AddInParameter(dbCommand, "Night", DbType.Int16, ResData.ResMain.Days);
                db.AddInParameter(dbCommand, "Holpack", DbType.String, ResData.ResMain.HolPack);
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, ResData.ResMain.PriceListNo);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, ResData.ResMain.DepCity);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, ResData.ResMain.ArrCity);
                db.AddInParameter(dbCommand, "FlySeat", DbType.Int16, ForSeat);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        getPLFlightsRecord record = new getPLFlightsRecord();
                        record.DepAirline = Conversion.getStrOrNull(R["DepAirline"]);
                        record.DepFlgClass = Conversion.getStrOrNull(R["DepFlgClass"]);
                        record.DepFlgClassName = Conversion.getStrOrNull(R["DepFlgClassName"]);
                        record.DepFlightNo = Conversion.getStrOrNull(R["DepFlightNo"]);
                        record.DepFlyDate = Conversion.getDateTimeOrNull(R["DepFlyDate"]);
                        record.DepFlyTime = Conversion.getDateTimeOrNull(R["DepFlyTime"]);
                        record.DepSeat = Conversion.getInt16OrNull(R["DepSeat"]);
                        record.NextFlightNo = Conversion.getStrOrNull(R["NextFlightNo"]);
                        record.RetFlgClass = Conversion.getStrOrNull(R["RetFlgClass"]);
                        record.RetFlgClassName = Conversion.getStrOrNull(R["RetFlgClassName"]);
                        record.RetFlightNo = Conversion.getStrOrNull(R["RetFlightNo"]);
                        record.RetFlyDate = Conversion.getDateTimeOrNull(R["RetFlyDate"]);
                        record.RetFlyTime = Conversion.getDateTimeOrNull(R["RetFlyTime"]);
                        record.RetSeat = Conversion.getInt16OrNull(R["RetSeat"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AirportRecord> getCityAirportLists(User UserData, int? country, int? city, ref string errorMsg)
        {
            List<AirportRecord> list = new List<AirportRecord>();
            string tsql = string.Empty;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040011035"))
                tsql =
@"
Select A.Code,A.Name,LocalName=isnull(dbo.FindLocalName(A.NameLID,@Market),A.Name),A.NameS,
  A.Location,LocationName=L.Name,LocationNameL=isnull(dbo.FindLocalName(L.NameLID,@Market),L.Name),
  A.TrfLocation,TrfLocationName=Tl.Name,TrfLocationNameL=isnull(dbo.FindLocalName(Tl.NameLID,@Market),Tl.Name),
  A.PNLFullName,A.PNLFlightSpace,A.PNLNameSpace,A.PNLChdRemark, 
  A.PNLSitatex,A.PNLMaxLineInPart,A.PNLDispExtra,A.PNLDispPNR,A.PNLDotAfterTitle
From Airport A (NOLOCK)
Join Location L (NOLOCK) ON A.Location=L.RecID
Join Location Tl (NOLOCK) ON A.TrfLocation=Tl.RecID
--Where L.City=@city
";
            else
                tsql =
@"
Select A.Code,A.Name,LocalName=isnull(dbo.FindLocalName(A.NameLID,@Market),A.Name),A.NameS,
  A.Location,LocationName=L.Name,LocationNameL=isnull(dbo.FindLocalName(L.NameLID,@Market),L.Name),
  TrfLocation=null,TrfLocationName=null,TrfLocationNameL=null,
  A.PNLFullName,A.PNLFlightSpace,A.PNLNameSpace,A.PNLChdRemark, 
  A.PNLSitatex,A.PNLMaxLineInPart,A.PNLDispExtra,A.PNLDispPNR,A.PNLDotAfterTitle
From Airport A (NOLOCK)
Join Location L (NOLOCK) ON A.Location=L.RecID
--Where L.City=@city
";
            string whereStr = string.Empty;
            whereStr += country.HasValue ? (whereStr.Length > 0 ? " and " : "") + string.Format("L.Country={0}", country.Value) : "";
            whereStr += city.HasValue ? (whereStr.Length > 0 ? " and " : "") + string.Format("L.City={0}", city.Value) : "";
            tsql += whereStr.Length > 0 ? " Where " + whereStr : "";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AirportRecord record = new AirportRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Location = (Int32)R["Location"];
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        record.PNLFullName = Equals(R["PNLFullName"], "Y");
                        record.PNLFlightSpace = Equals(R["PNLFlightSpace"], "Y");
                        record.PNLNameSpace = Equals(R["PNLNameSpace"], "Y");
                        record.PNLChdRemark = Equals(R["PNLChdRemark"], "Y");
                        record.PNLSitatex = Equals(R["PNLSitatex"], "Y");
                        record.PNLMaxLineInPart = Conversion.getInt32OrNull(R["PNLMaxLineInPart"]);
                        record.PNLDispExtra = Equals(R["PNLDispExtra"], "Y");
                        record.PNLDotAfterTitle = Equals(R["PNLDotAfterTitle"], "Y");
                        record.TrfLocation = Conversion.getInt32OrNull(R["TrfLocation"]);
                        record.TrfLocationName = Conversion.getStrOrNull(R["TrfLocationName"]);
                        record.TrfLocationNameL = Conversion.getStrOrNull(R["TrfLocationNameL"]);
                        list.Add(record);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AirportRecord> getAirportLists(User UserData, ref string errorMsg)
        {
            List<AirportRecord> list = new List<AirportRecord>();
            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 38, VersionControl.Equality.gt))
                tsql = @"Select A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, @Market), A.Name),
	                        A.NameS, A.Location, LocationName=L.Name, LocationNameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                            A.PNLFullName, A.PNLFlightSpace, A.PNLNameSpace, A.PNLChdRemark, 
	                        A.PNLSitatex, A.PNLMaxLineInPart, A.PNLDispExtra, A.PNLDispPNR, A.PNLDotAfterTitle,
                            A.TrfLocation,TrfLocationName=TrfL.Name,TrfLocationNameL=isnull(dbo.FindLocalName(TrfL.NameLID, @Market), TrfL.Name)
                        From Airport A (NOLOCK)
                        Join Location L (NOLOCK) ON A.Location=L.RecID
                        Left Join Location TrfL (NOLOCK) ON A.TrfLocation=TrfL.RecID
                        ";
            else
                tsql = @"Select A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, @Market), A.Name),
	                        A.NameS, A.Location, LocationName=L.Name, LocationNameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                            A.PNLFullName, A.PNLFlightSpace, A.PNLNameSpace, A.PNLChdRemark, 
	                        A.PNLSitatex, A.PNLMaxLineInPart, A.PNLDispExtra, A.PNLDispPNR, A.PNLDotAfterTitle,
                            TrfLocation=null,TrfLocationName=null,TrfLocationNameL=null
                        From Airport A (NOLOCK)
                        Join Location L (NOLOCK) ON A.Location=L.RecID";


            //TrfLocation
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AirportRecord record = new AirportRecord();
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Location = (Int32)R["Location"];
                        record.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        record.LocationNameL = Conversion.getStrOrNull(R["LocationNameL"]);
                        record.TrfLocation = Conversion.getInt32OrNull(R["TrfLocation"]);
                        record.TrfLocationName = Conversion.getStrOrNull(R["TrfLocationName"]);
                        record.TrfLocationNameL = Conversion.getStrOrNull(R["TrfLocationNameL"]);
                        record.PNLFullName = Equals(R["PNLFullName"], "Y");
                        record.PNLFlightSpace = Equals(R["PNLFlightSpace"], "Y");
                        record.PNLNameSpace = Equals(R["PNLNameSpace"], "Y");
                        record.PNLChdRemark = Equals(R["PNLChdRemark"], "Y");
                        record.PNLSitatex = Equals(R["PNLSitatex"], "Y");
                        record.PNLMaxLineInPart = Conversion.getInt32OrNull(R["PNLMaxLineInPart"]);
                        record.PNLDispExtra = Equals(R["PNLDispExtra"], "Y");
                        record.PNLDotAfterTitle = Equals(R["PNLDotAfterTitle"], "Y");

                        list.Add(record);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightsText getFlightTicketText(string FlightNo, ref string errorMsg)
        {
            string tsql = @"Select FlightNo=Code,RTFText=ETicketText,HTMLText=ETicketHtml
                            From Flight (NOLOCK)
                            Where Code=@FlightNo";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        FlightsText record = new FlightsText();
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        record.RTFText = Conversion.getStrOrNull(R["RTFText"]);
                        record.HTMLText = Conversion.getStrOrNull(R["HTMLText"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightInfosRecord> spDepRetFlightInfo(User UserData, ResDataRecord ResData, ref string errorMsg)
        {
            List<FlightInfosRecord> records = new List<FlightInfosRecord>();
            ResMainRecord resMain = ResData.ResMain;
            List<ResServiceRecord> flightService = ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).ToList<ResServiceRecord>();
            string tsql = @"exec dbo.GetFlightInfos @Operator, @Market, @FlgClass, @FlyDate, @Night, @HolPack, 
                                                    @CatPackID, @DepCity, @ArrCity, @PrmAirLine, @FlySeat";

            //DepFlightNo, DepFlyDate, DepFlyTime, DepAirline, NextFlightNo, RetFlightNo, RetFlyDate, RetFlyTime, DepSeat, RetSeat
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, resMain.PLOperator);
                db.AddInParameter(dbCommand, "Market", DbType.String, resMain.PLMarket);
                db.AddInParameter(dbCommand, "FlgClass", DbType.String, flightService.FirstOrDefault().FlgClass);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, resMain.BegDate);
                db.AddInParameter(dbCommand, "Night", DbType.Int16, resMain.Days);
                db.AddInParameter(dbCommand, "HolPack", DbType.String, resMain.HolPack);
                db.AddInParameter(dbCommand, "CatPackID", DbType.Int32, resMain.PriceListNo);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, resMain.DepCity);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, resMain.ArrCity);
                db.AddInParameter(dbCommand, "PrmAirLine", DbType.String, null);
                db.AddInParameter(dbCommand, "FlySeat", DbType.Int32, flightService.FirstOrDefault().Unit);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        FlightInfosRecord record = new FlightInfosRecord();
                        record.DepFlightNo = Conversion.getStrOrNull(R["DepFlightNo"]);
                        record.DepFlyDate = Conversion.getDateTimeOrNull(R["DepFlyDate"]);
                        record.DepFlyTime = Conversion.getDateTimeOrNull(R["DepFlyTime"]);
                        record.DepAirline = Conversion.getStrOrNull(R["DepAirline"]);
                        record.NextFlightNo = Conversion.getStrOrNull(R["NextFlightNo"]);
                        record.RetFlightNo = Conversion.getStrOrNull(R["RetFlightNo"]);
                        record.RetFlyDate = Conversion.getDateTimeOrNull(R["RetFlyDate"]);
                        record.RetFlyTime = Conversion.getDateTimeOrNull(R["RetFlyTime"]);
                        record.DepSeat = Conversion.getInt32OrNull(R["DepSeat"]);
                        record.RetSeat = Conversion.getInt32OrNull(R["RetSeat"]);
                        record.Priority = Conversion.getInt32OrNull(R["Priority"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightDayClassRecord> getFlightDayClass(User UserData, List<FlightInfosRecord> FlightList, string Operator, string DepFlightClass, string RetFlightClass, ref string errorMsg)
        {
            List<FlightDayClassRecord> records = new List<FlightDayClassRecord>();
            if (FlightList == null)
                return null;
            string tsql = @"Select FAC.*, CompareClass=F.Class From
                            (
	                            Select FP.FlightNo, FP.FlyDate, FP.BlockType, FP.SClass, FC.Name, 
		                            NameL=isnull(dbo.FindLocalName(FC.NameLID, @Market), FC.Name)                                    
	                            From FlightPrice FP (NOLOCK)
	                            JOIN FlightClass FC (NOLOCK) ON FP.SClass=FC.Class
	                            Where FP.Operator=@Operator And FC.B2B=1
                            ) FAC
                            Join #Flights F On F.FlightNo=FAC.FlightNo And F.FlyDate=FAC.FlyDate ";

            string flight = @"  Set DateFormat YMD
                                if not OBJECT_ID('TempDB.dbo.#Flights') is null drop table #Flights
                                  CREATE TABLE #Flights
                                  (
                                    FlightNo varchar(10) Collate database_default,
                                    FlyDate DateTime,
                                    Class varchar(5)
                                  )
                                truncate table #Flights ";
            foreach (FlightInfosRecord row in FlightList)
            {
                flight += string.Format(@" Insert Into #Flights (FlightNo, FlyDate, Class) Values('{0}', '{1}', '{2}')  ",
                                        row.DepFlightNo,
                                        row.DepFlyDate.Value.ToString("yyyy-MM-dd"),
                                        DepFlightClass);
            }
            foreach (FlightInfosRecord row in FlightList)
            {
                flight += string.Format(@" Insert Into #Flights (FlightNo, FlyDate, Class) Values('{0}', '{1}', '{2}')  ",
                                        row.RetFlightNo,
                                        row.RetFlyDate.Value.ToString("yyyy-MM-dd"),
                                        RetFlightClass);
            }

            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(flight + tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        FlightDayClassRecord record = new FlightDayClassRecord();
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        record.FlyDate = Conversion.getDateTimeOrNull(R["FlyDate"]);
                        record.BlockType = Conversion.getStrOrNull(R["BlockType"]);
                        record.SClass = Conversion.getStrOrNull(R["SClass"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.CompareClass = Conversion.getStrOrNull(R["CompareClass"]);
                        records.Add(record);
                    }
                    return records;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool isOptAvailable(User UserData, string Operator, int? depCity, int? arrCity, DateTime? begDate, string flgClass, ref string errorMsg)
        {
            string tsql = @"Select RecID From FlightOpt (NOLOCK) 
                            Where Operator=@Operator 
                              And DepCity=@DepCity 
                              And ArrCity=@ArrCity 
                              And AllotDate=@FromDate 
                              And Class=@Class 
                           ";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, depCity);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, arrCity);
                db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, begDate);
                db.AddInParameter(dbCommand, "Class", DbType.String, flgClass);
                int? recID = Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
                if (recID.HasValue && recID.Value > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightTypeRecord> getFlightTypeList(User UserData, ref string errorMsg)
        {
            List<FlightTypeRecord> records = new List<FlightTypeRecord>();
            string tsql =
@"
Select RecID, Name From FlightType (NOLOCK)
";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        FlightTypeRecord record = new FlightTypeRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        records.Add(record);
                    }
                    return records;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }

    public class OnlyTickets
    {
        public List<Location> getFlightDepartureCity(User UserData, int? City, string ORT, ref string errorMsg)
        {
            List<Location> list = new List<Location>();
            bool saleOtherOperator = false;

            if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.Market, "LTECO"))
            {
                if (string.Equals(UserData.CustomRegID, Common.crID_Go2Holiday) && string.Equals(UserData.Market, "LITHUANIA"))
                    saleOtherOperator = true;
                else
                    saleOtherOperator = false;
            }
            else
            {
                saleOtherOperator = true;
            }

            string tsql = string.Empty;
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && VersionControl.CheckWebVersion(UserData.WebVersion, 43, VersionControl.Equality.gt))
            {
                tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
	                                            Select Distinct FD.DepCity
                                                From Flight F (NOLOCK) 
                                                Join FlightUPrice FP (NOLOCK) on FP.DepFlight=F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.DepFlight=FD.FlightNo And FP.DepDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
                                                Where (FP.Operator=@Operator Or Exists(Select RecID From FlightDayOp (NOLOCK) Where FlightDayID=FD.RecID And Operator=@Operator))
                                                  And FP.ORT='{1}'
		                                          {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.DepCity=L.RecID ", City.HasValue ? (string.Format(" And FD.DepCity={0} ", City.Value)) : string.Empty, ORT);
            }
            else
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                {
                    tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
	                                            Select Distinct FD.DepCity
                                                From Flight F (NOLOCK) 
                                                Join FlightUPrice FP (NOLOCK) on FP.DepFlight=F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.DepFlight=FD.FlightNo And FP.DepDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
                                                Where FP.Operator=@Operator
                                                  And FP.ORT='{1}'
		                                          {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.DepCity=L.RecID ", City.HasValue ? (string.Format(" And FD.DepCity={0} ", City.Value)) : string.Empty,
                                                                                              ORT);
                }
                else
                {
                    if (!saleOtherOperator)
                    {
                        if (UserData.TvParams.TvParamFlight.UseTicPriceForOTic.HasValue && UserData.TvParams.TvParamFlight.UseTicPriceForOTic.Value)
                            tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
	                                            Select Distinct FD.DepCity
	                                            From Flight F (NOLOCK) 
	                                            Join FlightUPrice FP (NOLOCK) on FP.DepFlight = F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.DepFlight=FD.FlightNo And FP.DepDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
                                                Where FP.Operator=@Operator
		                                          {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.DepCity=L.RecID ", City.HasValue ? (string.Format(" And FD.DepCity={0} ", City.Value)) : string.Empty);
                        else
                            tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
	                                            Select Distinct FD.DepCity
	                                            From Flight F (NOLOCK) 
	                                            Join FlightPrice FP (NOLOCK) on FP.FlightNo = F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.FlightNo=FD.FlightNo And FP.FlyDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
                                                Where FP.BS='B'And FP.Operator=@Operator
		                                          {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.DepCity=L.RecID ", City.HasValue ? (string.Format(" And FD.DepCity={0} ", City.Value)) : string.Empty);
                    }
                    else
                    {
                        if (UserData.TvParams.TvParamFlight.UseTicPriceForOTic.HasValue && UserData.TvParams.TvParamFlight.UseTicPriceForOTic.Value)
                            tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
                                                Select Distinct FD.DepCity
                                                From Flight F (NOLOCK) 
                                                Join FlightUPrice FP (NOLOCK) on FP.DepFlight = F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.DepFlight=FD.FlightNo And FP.DepDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
                                                Where 1=1
                                                  {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.DepCity=L.RecID ", City.HasValue ? (string.Format(" And FD.DepCity={0} ", City.Value)) : string.Empty);
                        else
                            tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
                                                Select Distinct FD.DepCity
                                                From Flight F (NOLOCK) 
                                                Join FlightPrice FP (NOLOCK) on FP.FlightNo = F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.FlightNo=FD.FlightNo And FP.FlyDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
                                                Where FP.BS='B'                                                  
                                                  {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.DepCity=L.RecID ", City.HasValue ? (string.Format(" And FD.DepCity={0} ", City.Value)) : string.Empty);
                    }
                }
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                if (!saleOtherOperator)
                    db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        Location lRecord = new Location();
                        lRecord.RecID = (int)oReader["RecID"];
                        lRecord.Name = Conversion.getStrOrNull(oReader["Name"]);
                        lRecord.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        lRecord.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        lRecord.Code = Conversion.getStrOrNull(oReader["Code"]);
                        lRecord.Type = (Int16)oReader["Type"];
                        lRecord.Village = Conversion.getInt32OrNull(oReader["Village"]);
                        lRecord.Town = Conversion.getInt32OrNull(oReader["Town"]);
                        lRecord.City = Conversion.getInt32OrNull(oReader["City"]);
                        lRecord.Country = (int)oReader["Country"];
                        lRecord.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        list.Add(lRecord);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Location> getFlightArrivalCity(User UserData, int? DepCity, string ORT, ref string errorMsg)
        {
            List<Location> list = new List<Location>();

            bool saleOtherOperator = false;

            if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.Market, "LTECO"))
            {
                if (string.Equals(UserData.CustomRegID, Common.crID_Go2Holiday) && string.Equals(UserData.Market, "LITHUANIA"))
                    saleOtherOperator = true;
                else
                    saleOtherOperator = false;
            }
            else
            {
                saleOtherOperator = true;
            }

            string tsql = string.Empty;
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && VersionControl.CheckWebVersion(UserData.WebVersion, 43, VersionControl.Equality.gt))
            {
                tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
                                                Select Distinct FD.ArrCity
                                                From Flight F (NOLOCK) 
                                                Join FlightUPrice FP (NOLOCK) on FP.DepFlight=F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.DepFlight=FD.FlightNo And FP.DepDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
                                                Where (FP.Operator=@Operator Or Exists(Select RecID From FlightDayOp (NOLOCK) Where FlightDayID=FD.RecID And Operator=@Operator))
                                                  And FP.ORT='{1}'
                                                  {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.ArrCity=L.RecID ", DepCity.HasValue ? (string.Format(" And FD.DepCity={0} ", DepCity.Value)) : string.Empty,
                                                                                          ORT);
            }
            else if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
                                                Select Distinct FD.ArrCity
                                                From Flight F (NOLOCK) 
                                                Join FlightUPrice FP (NOLOCK) on FP.DepFlight=F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.DepFlight=FD.FlightNo And FP.DepDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
                                                Where FP.Operator=@Operator
                                                  And FP.ORT='{1}'
                                                  {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.ArrCity=L.RecID ", DepCity.HasValue ? (string.Format(" And FD.DepCity={0} ", DepCity.Value)) : string.Empty,
                                                                                          ORT);
            }
            else if (!saleOtherOperator)
            {
                if (UserData.TvParams.TvParamFlight.UseTicPriceForOTic.HasValue && UserData.TvParams.TvParamFlight.UseTicPriceForOTic.Value)
                    tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
	                                            Select Distinct FD.ArrCity
	                                            From Flight F (NOLOCK) 
	                                            Join FlightUPrice FP (NOLOCK) on FP.DepFlight = F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.DepFlight=FD.FlightNo And FP.DepDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
	                                            Where FP.Operator=@Operator
		                                            {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.ArrCity=L.RecID", DepCity.HasValue ? (string.Format(" And FD.DepCity={0} ", DepCity.Value)) : string.Empty);
                else
                    tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
	                                            Select Distinct FD.ArrCity
	                                            From Flight F (NOLOCK) 
	                                            Join FlightPrice FP (NOLOCK) on FP.FlightNo = F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.FlightNo=FD.FlightNo And FP.FlyDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
	                                            Where FP.BS='B' And FP.Operator=@Operator
		                                            {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.ArrCity=L.RecID", DepCity.HasValue ? (string.Format(" And FD.DepCity={0} ", DepCity.Value)) : string.Empty);
            }
            else
            {
                if (UserData.TvParams.TvParamFlight.UseTicPriceForOTic.HasValue && UserData.TvParams.TvParamFlight.UseTicPriceForOTic.Value)
                    tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
	                                            Select Distinct FD.ArrCity
	                                            From Flight F (NOLOCK) 
	                                            Join FlightUPrice FP (NOLOCK) on FP.DepFlight = F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.DepFlight=FD.FlightNo And FP.DepDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
	                                            Where 1=1
		                                            {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.ArrCity=L.RecID", DepCity.HasValue ? (string.Format(" And FD.DepCity={0} ", DepCity.Value)) : string.Empty);
                else
                    tsql = string.Format(@"Select L.RecID, NameL=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
                                            L.Code, L.Name, L.NameS, L.Type, L.Village, L.Town, L.City, L.Country, L.TaxPer 
                                        From (
	                                            Select Distinct FD.ArrCity
	                                            From Flight F (NOLOCK) 
	                                            Join FlightPrice FP (NOLOCK) on FP.FlightNo = F.Code
                                                Join FlightDay FD (NOLOCK) ON FP.FlightNo=FD.FlightNo And FP.FlyDate=FD.FlyDate And isnull(FD.WebPub, 'N')='Y' And FD.FlyDate >= GETDATE()-1
	                                            Where FP.BS='B' 
		                                            {0}
                                             ) FL
                                        Join Location L (NOLOCK) ON FL.ArrCity=L.RecID", DepCity.HasValue ? (string.Format(" And FD.DepCity={0} ", DepCity.Value)) : string.Empty);
            }
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                if (!saleOtherOperator)
                    db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        Location lRecord = new Location();
                        lRecord.RecID = (int)oReader["RecID"];
                        lRecord.Name = (string)oReader["Name"];
                        lRecord.NameS = (string)oReader["NameS"];
                        lRecord.NameL = (string)oReader["NameL"];
                        lRecord.Code = (string)oReader["Code"];
                        lRecord.Type = (Int16)oReader["Type"];
                        lRecord.Village = Conversion.getInt32OrNull(oReader["Village"]);
                        lRecord.Town = Conversion.getInt32OrNull(oReader["Town"]);
                        lRecord.City = Conversion.getInt32OrNull(oReader["City"]);
                        lRecord.Country = (int)oReader["Country"];
                        lRecord.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        list.Add(lRecord);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return list;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OnlyTicket_Flight> GetFlightPrice(User UserData, DateTime? cIn1, DateTime? cIn2, int? day1, int? day2, string flyClass, int? routeFrom, int? routeTo, bool? isRT, bool? b2bAllClass, bool allFlight, int? flightType, ref string errorMsg)
        {
            List<OnlyTicket_Flight> records = new List<OnlyTicket_Flight>();
            string tsql = string.Empty;

            bool saleOtherOperator = false;

            if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.Market, "LTECO"))
            {
                if (string.Equals(UserData.CustomRegID, Common.crID_Go2Holiday) && string.Equals(UserData.Market, "LITHUANIA"))
                    saleOtherOperator = true;
                else
                    saleOtherOperator = false;

                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                    saleOtherOperator = true;
            }
            else
            {
                saleOtherOperator = true;
            }

            if (isRT.HasValue && isRT.Value)
            {
                if (!saleOtherOperator)
                {
                    if (UserData.TvParams.TvParamFlight.UseTicPriceForOTic.HasValue && UserData.TvParams.TvParamFlight.UseTicPriceForOTic.Value)
                        #region <> Novaturas || LTAIP
                        tsql =
@"
Declare 		
    @chkNext smallint,                                 
    @DepDate DateTime, 
    @DepTime DateTime, 
    @DepFlight varchar(10), 
    @RetFlight varchar(10), 
    @RetTime DateTime, 
    @DepClass varchar(2), 
    @NextFlight varchar(10), 
    @DepCity int, 
    @ArrCity int, 
    @Airline varchar(10), 
    @ArrDate DateTime,
    @DFullName varchar(100), 
    @DFullNameL varchar(100), 
    @RFullName varchar(100), 
    @RFullNameL varchar(100), 
    @DepFlightAirline varchar(50), 
    @DepFlightAirlineL varchar(50), 
    @RetFlightAirline varchar(50),
    @RetFlightAirlineL varchar(50),
    @DepartureDepTime DateTime,
    @DepartureArrTime DateTime,
    @ReturnDepTime DateTime,                                
    @ReturnArrTime DateTime,
    @DepFlightDuration int,
    @RetFlightDuration int,
    @DepCountCloseTime DateTime,
    @RetCountCloseTime DateTime
if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
    CREATE TABLE #Flight
    (
    DepDate DateTime,
    DepTime DateTime,
    DepFlight varchar(10) Collate database_default,
    DFullName varchar(100) Collate database_default,
    DFullNameL varchar(100) Collate database_default,
    DepClass  varchar(2) Collate database_default,
    DepFlightAirline  varchar(50) Collate database_default,
    DepFlightAirlineL  varchar(50) Collate database_default,
    ArrDate DateTime,
    ArrTime DateTime,
    ArrFlight varchar(10) Collate database_default, 
    AFullName varchar(100) Collate database_default, 
    AFullNameL varchar(100) Collate database_default, 
    ArrClass varchar(2) Collate database_default, 
    ArrFlightAirline varchar(50) Collate database_default, 
    ArrFlightAirlineL varchar(50) Collate database_default, 
    Days SmallInt,
    isRT varchar(2) Collate database_default,
    DepCity int,
    ArrCity int,
    isNext SmallInt,
    DepartureDepTime DateTime,
    DepartureArrTime DateTime, 
    ReturnDepTime DateTime,
    ReturnArrTime DateTime,
    DepFlightDuration int,
    RetFlightDuration int,
    DepCountCloseTime DateTime,
    RetCountCloseTime DateTime,
	Operator varChar(10),
	Market varChar(10)
    )
truncate table #Flight

Declare FCursor CURSOR LOCAL FAST_FORWARD FOR
    Select Distinct DepDate=DD.FlyDate,DepFlight=DD.FlightNo,DepClass=DP.DepClass,DepCity=@routeFrom,ArrCity=@routeTo
    From FlightDay DD (NOLOCK)
    Join FlightUPrice DP (NOLOCK) on DD.FlightNo=DP.DepFlight and DD.FlyDate=DP.DepDate and DP.Operator=@Operator
    Where DD.DepCity=@routeFrom and DD.ArrCity=@routeTo and
        DD.FlyDate between @cIn1 and @cIn2 and 
        DP.DepClass=isnull(@flyClass,DP.DepClass) and
        DD.WebPub='Y' and
        DD.StopSale='N' and
		(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=DD.FlightNo and F.FlightType=@FlightType))
    Order by DD.FlyDate,DD.FlightNo
                          
Open FCursor
Fetch Next FROM FCursor INTO @DepDate,@DepFlight,@DepClass,@DepCity,@ArrCity
While @@FETCH_STATUS = 0
begin
    Select @NextFlight=NextFlight,@DepCity=DepCity,@ArrCity=ArrCity,@Airline=Airline 
    From FlightDay (NOLOCK)
    where FlightNo=@DepFlight and FlyDate=@DepDate                      
                            
    if exists (Select RecID From FlightDay (NOLOCK) where FlightNo=@DepFlight and FlyDate=@DepDate and RetFlightUseSysSet<>'Y')
    Select @ChkNext=RetFlightOpt From FlightDay (NOLOCK) where FlightNo=@DepFlight and FlyDate=@DepDate
    else
    if exists (Select * From ParamFlight (NOLOCK) where UseSysParam<>'Y')
        Select @ChkNext=RetFlightOpt From ParamFlight (NOLOCK) where Market=@Market
    else
        Select @ChkNext=RetFlightOpt From ParamFlight (NOLOCK) where Market=''

    if @allFlight=1 Set @ChkNext=3

    if @ChkNext=1 and isnull(@NextFlight,'')<>''
    begin
        Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
        Select Distinct RetFlight=D.FlightNo,D.FlyDate
        from FlightDay D (NOLOCK)
        Join FlightUPrice P (NOLOCK) on D.FlightNo=P.DepFlight and D.FlyDate=P.DepDate and Operator=@Operator
        where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
            DateAdd(day, @day2, @DepDate) and 
            D.DepCity=@ArrCity and 
            D.ArrCity=@DepCity and 
            D.FlightNo=@NextFlight and
            D.WebPub='Y' And
			(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=D.FlightNo and F.FlightType=@FlightType))
        Order by D.FlightNo, D.FlyDate
    end
    else
	begin
        if @ChkNext = 2
        begin
			Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
			Select Distinct RetFlight = D.FlightNo, D.FlyDate
			from FlightDay D (NOLOCK)
			Join FlightUPrice P (NOLOCK) on D.FlightNo = P.ArrFlight and D.FlyDate = P.ArrDate and Operator = @Operator
			where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
				DateAdd(day, @day2, @DepDate) and 
				D.DepCity=@ArrCity and 
				D.ArrCity=@DepCity and 
				D.Airline = @Airline and
				D.WebPub='Y' And
				(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=D.FlightNo and F.FlightType=@FlightType))
			Order by D.FlightNo, D.FlyDate
        end
        else
            if @ChkNext = 3
                begin
                Declare @DepFlightNo varchar(10), @DepFlyDate DateTime, @FlyDate DateTime, 
                    @DepCountry int, @ArrCountry int

                    Select @DepCity=DepCity,@ArrCity=ArrCity From FlightDay (NOLOCK) 
                    Where FlightNo=@DepFlightNo and FlyDate=@DepFlyDate
                                          		
		            Select @DepCountry=Country from Location (NOLOCK) where RecID=@DepCity
		            Select @ArrCountry=Country from Location (NOLOCK) where RecID=@ArrCity 
                                		  
		            Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
		            Select Distinct RetFlight=D.FlightNo,D.FlyDate		  
			        from FlightDay D (NOLOCK) 
			        join FlightUPrice P (NOLOCK) on D.FlightNo=P.ArrFlight and D.FlyDate=P.ArrDate and Operator=@Operator 
			        where D.FlyDate between DateAdd(day, @day1, @DepDate) and DateAdd(day, @day2, @DepDate) and 
				            ArrCity <> @ArrCity and DepCity <> @DepCity and 
				            @ArrCountry = (Select Country from Location (NOLOCK) where RecID = D.DepCity) and 
				            @DepCountry = (Select Country from Location (NOLOCK) where RecID = D.ArrCity)  And
							(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=D.FlightNo and F.FlightType=@FlightType))
			        Order by D.FlightNo 		  
                end      
                else
                begin

					Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
					Select Distinct RetFlight = D.FlightNo, D.FlyDate
					from FlightDay D (NOLOCK)
					Join FlightUPrice P (NOLOCK) on D.FlightNo=P.ArrFlight and D.FlyDate=P.ArrDate and Operator = @Operator
					where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
						DateAdd(day, @day2, @DepDate) and 
						D.DepCity=@ArrCity and 
						D.ArrCity=@DepCity and
						D.WebPub='Y'  And
						(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=D.FlightNo and F.FlightType=@FlightType))
					Order by D.FlightNo, D.FlyDate  
                end
		end
    Open RetFCursor
    Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
    While @@FETCH_STATUS = 0
    begin
    Select @DepTime=DepTime,
        @DFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
        @DFullNameL=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
        @DepFlightAirline=A.Name,
        @DepFlightAirlineL=isnull(dbo.FindLocalName(A.NameLID,@Market),A.Name),
        @DepartureDepTime=deptime,
        @DepartureArrTime=arrtime,
        @DepFlightDuration=datediff(minute,deptime,arrtime),
        @DepCountCloseTime=CountCloseTime
    From FlightDay (NOLOCK) FD
    Join AirLine (NOLOCK) A ON A.Code=FD.Airline
    Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
    Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
    where FlightNo=@DepFlight and 
        FlyDate=@DepDate and
        WebPub='Y'

    Select @RetTime=DepTime, 
        @RFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
        @RFullNameL=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
        @RetFlightAirline=A.Name,
        @RetFlightAirlineL=isnull(dbo.FindLocalName(A.NameLID,@Market),A.Name),
        @ReturnDepTime=deptime,
        @ReturnArrTime=arrtime,
        @RetFlightDuration=datediff(minute,deptime,arrtime),
        @RetCountCloseTime=CountCloseTime  
    From FlightDay (NOLOCK)  FD
    Join AirLine (NOLOCK) A ON A.Code=FD.Airline
    Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
    Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
    where FlightNo=@RetFlight and 
        FlyDate=@ArrDate and
        WebPub='Y'
    if (Exists(Select Class From FlightClass Where Class=@DepClass And (@b2bAllClass = 1 OR B2B=1))) -- 2010.01.06 eklendi
    Begin -- 2010.01.06 eklendi
        insert into #Flight
        (DepDate,DepTime,DepFlight,DFullName,DFullNameL,DepClass,DepFlightAirline,DepFlightAirlineL,ArrDate,ArrTime,ArrFlight,AFullName,AFullNameL,ArrClass,ArrFlightAirline,ArrFlightAirlineL,Days,isRT,DepCity,ArrCity,isNext,DepartureDepTime,DepartureArrTime,DepFlightDuration,DepCountCloseTime,ReturnDepTime,ReturnArrTime,RetFlightDuration,RetCountCloseTime,Operator,Market)
        values
        (@DepDate,@DepTime,@DepFlight,@DFullName,@DFullNameL,@DepClass,@DepFlightAirline,@DepFlightAirlineL,@ArrDate,@RetTime,@RetFlight,@RFullName,@RFullNameL,@DepClass,@DepFlightAirline,@DepFlightAirlineL,DateDiff(day, @DepDate,@ArrDate),'RT',@DepCity,@ArrCity,1,@DepartureDepTime,@DepartureArrTime,@DepFlightDuration,@DepCountCloseTime,@ReturnDepTime,@ReturnArrTime,@RetFlightDuration,@RetCountCloseTime,@Operator,@Market)
    End -- 2010.01.06 eklendi
    Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
    end
    Close RetFCursor
    DeAllocate RetFCursor

    Fetch Next FROM FCursor INTO @DepDate, @DepFlight, @DepClass, @DepCity, @ArrCity
end
Close FCursor
DeAllocate FCursor
Select * From #Flight

";
                        #endregion
                    else
                        #region <> Novaturas || LTAIP
                        tsql =
@"
Declare 		
    @chkNext smallint,                                 
    @DepDate DateTime, 
    @DepTime DateTime, 
    @DepFlight varchar(10), 
    @RetFlight varchar(10), 
    @RetTime DateTime, 
    @DepClass varchar(2), 
    @NextFlight varchar(10), 
    @DepCity int, 
    @ArrCity int, 
    @Airline varchar(10), 
    @ArrDate DateTime,
    @DFullName varchar(100), 
    @DFullNameL varchar(100), 
    @RFullName varchar(100), 
    @RFullNameL varchar(100), 
    @DepFlightAirline varchar(50), 
    @DepFlightAirlineL varchar(50), 
    @RetFlightAirline varchar(50),
    @RetFlightAirlineL varchar(50),
    @DepartureDepTime DateTime,
    @DepartureArrTime DateTime,
    @ReturnDepTime DateTime,                                
    @ReturnArrTime DateTime,
    @DepFlightDuration int,
    @RetFlightDuration int,
    @DepCountCloseTime DateTime,
    @RetCountCloseTime DateTime
if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
    CREATE TABLE #Flight
    (
    DepDate DateTime,
    DepTime DateTime,
    DepFlight varchar(10) Collate database_default,
    DFullName varchar(100) Collate database_default,
    DFullNameL varchar(100) Collate database_default,
    DepClass  varchar(2) Collate database_default,
    DepFlightAirline  varchar(50) Collate database_default,
    DepFlightAirlineL  varchar(50) Collate database_default,
    ArrDate DateTime,
    ArrTime DateTime,
    ArrFlight varchar(10) Collate database_default, 
    AFullName varchar(100) Collate database_default, 
    AFullNameL varchar(100) Collate database_default, 
    ArrClass varchar(2) Collate database_default, 
    ArrFlightAirline varchar(50) Collate database_default, 
    ArrFlightAirlineL varchar(50) Collate database_default, 
    Days SmallInt,
    isRT varchar(2) Collate database_default,
    DepCity int,
    ArrCity int,
    isNext SmallInt,
    DepartureDepTime DateTime,
    DepartureArrTime DateTime, 
    ReturnDepTime DateTime,
    ReturnArrTime DateTime,
    DepFlightDuration int,
    RetFlightDuration int,
    DepCountCloseTime DateTime,
    RetCountCloseTime DateTime,
	Operator varChar(10),
	Market varChar(10)
    )
truncate table #Flight

Declare FCursor CURSOR LOCAL FAST_FORWARD FOR
    Select Distinct DepDate=DD.FlyDate,DepFlight=DD.FlightNo,DepClass=DP.SClass,DepCity=@routeFrom,ArrCity=@routeTo
    From FlightDay DD (NOLOCK)
    Join FlightPrice DP (NOLOCK) on DD.FlightNo=DP.FlightNo and DD.FlyDate=DP.FlyDate and isnull(DP.Buyer,'')='' and DP.Operator=@Operator
    Where DD.DepCity=@routeFrom and DD.ArrCity=@routeTo and
        DD.FlyDate between @cIn1 and @cIn2 and 
        DP.SClass=isnull(@flyClass,DP.SClass) and
        DD.WebPub='Y' and
        DD.StopSale='N' and
		(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=DD.FlightNo and F.FlightType=@FlightType))
    Order by DD.FlyDate,DD.FlightNo
                          
Open FCursor
Fetch Next FROM FCursor INTO @DepDate,@DepFlight,@DepClass,@DepCity,@ArrCity
While @@FETCH_STATUS = 0
begin
    Select @NextFlight=NextFlight,@DepCity=DepCity,@ArrCity=ArrCity,@Airline=Airline 
    From FlightDay (NOLOCK)
    where FlightNo=@DepFlight and FlyDate=@DepDate                      
                            
    if exists (Select RecID From FlightDay (NOLOCK) where FlightNo=@DepFlight and FlyDate=@DepDate and RetFlightUseSysSet<>'Y')
    Select @ChkNext=RetFlightOpt From FlightDay (NOLOCK) where FlightNo=@DepFlight and FlyDate=@DepDate
    else
    if exists (Select * From ParamFlight (NOLOCK) where UseSysParam<>'Y')
        Select @ChkNext=RetFlightOpt From ParamFlight (NOLOCK) where Market=@Market
    else
        Select @ChkNext=RetFlightOpt From ParamFlight (NOLOCK) where Market=''

    if @allFlight=1 Set @ChkNext=3

    if @ChkNext=1 and isnull(@NextFlight,'')<>''
    begin
        Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
        Select Distinct RetFlight=D.FlightNo,D.FlyDate
        from FlightDay D (NOLOCK)
        Join FlightPrice P (NOLOCK) on D.FlightNo=P.FlightNo and D.FlyDate=P.FlyDate and isnull(P.Buyer,'')='' and Operator=@Operator
        where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
            DateAdd(day, @day2, @DepDate) and 
            D.DepCity=@ArrCity and 
            D.ArrCity=@DepCity and 
            D.FlightNo = @NextFlight and
            D.WebPub='Y' And
			(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=D.FlightNo and F.FlightType=@FlightType))
        Order by D.FlightNo, D.FlyDate
    end
    else
        if @ChkNext = 2
        begin
        Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
        Select Distinct RetFlight = D.FlightNo, D.FlyDate
        from FlightDay D (NOLOCK)
        Join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' and Operator = @Operator
        where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
            DateAdd(day, @day2, @DepDate) and 
            D.DepCity=@ArrCity and 
            D.ArrCity=@DepCity and 
            D.Airline = @Airline and
            D.WebPub='Y' And
			(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=D.FlightNo and F.FlightType=@FlightType))
        Order by D.FlightNo, D.FlyDate
        end
        else
            if @ChkNext = 3
                begin
                Declare @DepFlightNo varchar(10), @DepFlyDate DateTime, @FlyDate DateTime, 
                    @DepCountry int, @ArrCountry int

                    Select @DepCity=DepCity,@ArrCity=ArrCity From FlightDay (NOLOCK) 
                    Where FlightNo=@DepFlightNo and FlyDate=@DepFlyDate
                                          		
		            Select @DepCountry=Country from Location (NOLOCK) where RecID=@DepCity
		            Select @ArrCountry=Country from Location (NOLOCK) where RecID=@ArrCity 
                                		  
		            Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
		            Select Distinct RetFlight=D.FlightNo,D.FlyDate		  
			        from FlightDay D (NOLOCK) 
			        join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' and Operator = @Operator 
			        where D.FlyDate between DateAdd(day, @day1, @DepDate) and DateAdd(day, @day2, @DepDate) and 
				            ArrCity <> @ArrCity and DepCity <> @DepCity and 
				            @ArrCountry = (Select Country from Location (NOLOCK) where RecID = D.DepCity) and 
				            @DepCountry = (Select Country from Location (NOLOCK) where RecID = D.ArrCity)  And
							(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=D.FlightNo and F.FlightType=@FlightType))
			        Order by D.FlightNo 		  
                end      
                else
                begin
                Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
                Select Distinct RetFlight = D.FlightNo, D.FlyDate
                from FlightDay D (NOLOCK)
                Join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' and Operator = @Operator
                where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
                    DateAdd(day, @day2, @DepDate) and 
                    D.DepCity=@ArrCity and 
                    D.ArrCity=@DepCity and
                    D.WebPub='Y'  And
					(@FlightType is null or Exists(Select null From Flight F (NOLOCK) Where F.Code=D.FlightNo and F.FlightType=@FlightType))
                Order by D.FlightNo, D.FlyDate  
                end
    Open RetFCursor
    Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
    While @@FETCH_STATUS = 0
    begin
    Select @DepTime=DepTime,
        @DFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
        @DFullNameL=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
        @DepFlightAirline=A.Name,
        @DepFlightAirlineL=isnull(dbo.FindLocalName(A.NameLID,@Market),A.Name),
        @DepartureDepTime=deptime,
        @DepartureArrTime=arrtime,
        @DepFlightDuration=datediff(minute,deptime,arrtime),
        @DepCountCloseTime=CountCloseTime
    From FlightDay (NOLOCK) FD
    Join AirLine (NOLOCK) A ON A.Code=FD.Airline
    Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
    Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
    where FlightNo=@DepFlight and 
        FlyDate=@DepDate and
        WebPub='Y'

    Select @RetTime=DepTime, 
        @RFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
        @RFullNameL=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
        @RetFlightAirline=A.Name,
        @RetFlightAirlineL=isnull(dbo.FindLocalName(A.NameLID,@Market),A.Name),
        @ReturnDepTime=deptime,
        @ReturnArrTime=arrtime,
        @RetFlightDuration=datediff(minute,deptime,arrtime),
        @RetCountCloseTime=CountCloseTime  
    From FlightDay (NOLOCK)  FD
    Join AirLine (NOLOCK) A ON A.Code=FD.Airline
    Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
    Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
    where FlightNo=@RetFlight and 
        FlyDate=@ArrDate and
        WebPub='Y'
    if (Exists(Select Class From FlightClass Where Class=@DepClass And (@b2bAllClass = 1 OR B2B=1))) -- 2010.01.06 eklendi
    Begin -- 2010.01.06 eklendi
        insert into #Flight
        (DepDate,DepTime,DepFlight,DFullName,DFullNameL,DepClass,DepFlightAirline,DepFlightAirlineL,ArrDate,ArrTime,ArrFlight,AFullName,AFullNameL,ArrClass,ArrFlightAirline,ArrFlightAirlineL,Days,isRT,DepCity,ArrCity,isNext,DepartureDepTime,DepartureArrTime,DepFlightDuration,DepCountCloseTime,ReturnDepTime,ReturnArrTime,RetFlightDuration,RetCountCloseTime,Operator,Market)
        values
        (@DepDate,@DepTime,@DepFlight,@DFullName,@DFullNameL,@DepClass,@DepFlightAirline,@DepFlightAirlineL,@ArrDate,@RetTime,@RetFlight,@RFullName,@RFullNameL,@DepClass,@DepFlightAirline,@DepFlightAirlineL,DateDiff(day, @DepDate,@ArrDate),'RT',@DepCity,@ArrCity,1,@DepartureDepTime,@DepartureArrTime,@DepFlightDuration,@DepCountCloseTime,@ReturnDepTime,@ReturnArrTime,@RetFlightDuration,@RetCountCloseTime,@Operator,@Market)
    End -- 2010.01.06 eklendi
    Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
    end
    Close RetFCursor
    DeAllocate RetFCursor

    Fetch Next FROM FCursor INTO @DepDate, @DepFlight, @DepClass, @DepCity, @ArrCity
end
Close FCursor
DeAllocate FCursor
Select * From #Flight
";
                        #endregion
                }
                else
                {
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && VersionControl.CheckWebVersion(UserData.WebVersion, 43, VersionControl.Equality.gt))
                        #region Detur
                        tsql =
    @"
Declare 		
    @chkNext smallint,                                 
    @DepDate DateTime, 
    @DepTime DateTime, 
    @DepFlight varchar(10), 
    @RetFlight varchar(10), 
    @RetTime DateTime, 
    @DepClass varchar(2), 
    @NextFlight varchar(10), 
    @DepCity int, 
    @ArrCity int, 
    @Airline varchar(10), 
    @ArrDate DateTime,
    @DFullName varchar(100), 
    @DFullNameL varchar(100), 
    @RFullName varchar(100), 
    @RFullNameL varchar(100), 
    @DepFlightAirline varchar(50), 
    @DepFlightAirlineL varchar(50), 
    @RetFlightAirline varchar(50),
    @RetFlightAirlineL varchar(50),
    @DepartureDepTime DateTime,
    @DepartureArrTime DateTime,
    @ReturnDepTime DateTime,                                
    @ReturnArrTime DateTime,
    @DepFlightDuration int,
    @RetFlightDuration int,
    @DepCountCloseTime DateTime,
    @RetCountCloseTime DateTime
if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
  CREATE TABLE #Flight
  (
    DepDate DateTime,
    DepTime DateTime,
    DepFlight varchar(10) Collate database_default,
    DFullName varchar(100) Collate database_default,
    DFullNameL varchar(100) Collate database_default,
    DepClass  varchar(2) Collate database_default,
    DepFlightAirline  varchar(50) Collate database_default,
    DepFlightAirlineL  varchar(50) Collate database_default,
    ArrDate DateTime,
    ArrTime DateTime,
    ArrFlight varchar(10) Collate database_default, 
    AFullName varchar(100) Collate database_default, 
    AFullNameL varchar(100) Collate database_default, 
    ArrClass varchar(2) Collate database_default, 
    ArrFlightAirline varchar(50) Collate database_default, 
    ArrFlightAirlineL varchar(50) Collate database_default, 
    Days SmallInt,
    isRT varchar(2) Collate database_default,
    DepCity int,
    ArrCity int,
    isNext SmallInt,
    DepartureDepTime DateTime,
    DepartureArrTime DateTime, 
    ReturnDepTime DateTime,
    ReturnArrTime DateTime,
    DepFlightDuration int,
    RetFlightDuration int,
    DepCountCloseTime DateTime,
    RetCountCloseTime DateTime
  )
truncate table #Flight

Declare FCursor CURSOR LOCAL FAST_FORWARD FOR
  Select Distinct DepDate=DD.FlyDate,DepFlight=DD.FlightNo,DepClass=DP.SClass,DepCity=@routeFrom,ArrCity=@routeTo
  From FlightDay DD (NOLOCK)
  Join FlightPrice DP (NOLOCK) on DD.FlightNo=DP.FlightNo and DD.FlyDate=DP.FlyDate and isnull(DP.Buyer,'')='' 
  --and DP.Operator=@Operator
       and (DP.Operator=@Operator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID=DD.RecID And Operator=@Operator))
  Where DD.DepCity=@routeFrom and DD.ArrCity=@routeTo and
        DD.FlyDate between @cIn1 and @cIn2 and 
        DP.SClass=isnull(@flyClass,DP.SClass) and
        DD.WebPub='Y' and
        DD.StopSale='N'
  Order by DD.FlyDate,DD.FlightNo
  
Open FCursor
Fetch Next FROM FCursor INTO @DepDate,@DepFlight,@DepClass,@DepCity,@ArrCity
While @@FETCH_STATUS = 0
begin
  Select @NextFlight=NextFlight,@DepCity=DepCity,@ArrCity=ArrCity,@Airline=Airline 
  From FlightDay (NOLOCK)
  where FlightNo=@DepFlight and FlyDate=@DepDate                      
    
  if exists (Select RecID From FlightDay (NOLOCK) where FlightNo=@DepFlight and FlyDate=@DepDate and RetFlightUseSysSet<>'Y')
    Select @ChkNext=RetFlightOpt From FlightDay (NOLOCK) where FlightNo=@DepFlight and FlyDate=@DepDate
  else
    if exists (Select * From ParamFlight (NOLOCK) where UseSysParam<>'Y')
      Select @ChkNext=RetFlightOpt From ParamFlight (NOLOCK) where Market=@Market
    else
      Select @ChkNext=RetFlightOpt From ParamFlight (NOLOCK) where Market=''

    if @allFlight=1 Set @ChkNext=3

    if @ChkNext=1 and isnull(@NextFlight,'')<>''
    begin
      Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
      Select Distinct RetFlight=D.FlightNo,D.FlyDate
      from FlightDay D (NOLOCK)
      Join FlightPrice P (NOLOCK) on D.FlightNo=P.FlightNo and D.FlyDate=P.FlyDate and isnull(P.Buyer,'')='' 
           --and Operator=@Operator
           and (P.Operator=@Operator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID=D.RecID And Operator=@Operator))
      where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
         DateAdd(day, @day2, @DepDate) and 
         D.DepCity=@ArrCity and 
         D.ArrCity=@DepCity and 
         D.FlightNo = @NextFlight and
         D.WebPub='Y'
      Order by D.FlightNo, D.FlyDate
    end
    else
      if @ChkNext = 2
      begin
        Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
        Select Distinct RetFlight = D.FlightNo, D.FlyDate
        from FlightDay D (NOLOCK)
        Join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' 
        --and Operator = @Operator
             and (P.Operator=@Operator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID=D.RecID And Operator=@Operator))
        where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
           DateAdd(day, @day2, @DepDate) and 
           D.DepCity=@ArrCity and 
           D.ArrCity=@DepCity and 
           D.Airline = @Airline and
           D.WebPub='Y'
        Order by D.FlightNo, D.FlyDate
      end
        else
         if @ChkNext = 3
              begin
              Declare @DepFlightNo varchar(10), @DepFlyDate DateTime, @FlyDate DateTime, 
                   @DepCountry int, @ArrCountry int

                  Select @DepCity=DepCity,@ArrCity=ArrCity From FlightDay (NOLOCK) 
                  Where FlightNo=@DepFlightNo and FlyDate=@DepFlyDate
                  		
                  Select @DepCountry=Country from Location (NOLOCK) where RecID=@DepCity
                  Select @ArrCountry=Country from Location (NOLOCK) where RecID=@ArrCity 
        		  
                  Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
                  Select Distinct RetFlight=D.FlightNo,D.FlyDate		  
                    from FlightDay D (NOLOCK) 
                    join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' 
                    --and Operator = @Operator 
                         and (P.Operator=@Operator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID=D.RecID And Operator=@Operator))
                    where D.FlyDate between DateAdd(day, @day1, @DepDate) and DateAdd(day, @day2, @DepDate) and 
                          ArrCity <> @ArrCity and DepCity <> @DepCity and 
                          @ArrCountry = (Select Country from Location (NOLOCK) where RecID = D.DepCity) and 
                          @DepCountry = (Select Country from Location (NOLOCK) where RecID = D.ArrCity) 
                    Order by D.FlightNo 		  
              end      
              else
              begin
                Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
                Select Distinct RetFlight = D.FlightNo, D.FlyDate
                from FlightDay D (NOLOCK)
                Join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' 
                --and Operator = @Operator
                     and (P.Operator=@Operator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID=D.RecID And Operator=@Operator))
                where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
                   DateAdd(day, @day2, @DepDate) and 
                   D.DepCity=@ArrCity and 
                   D.ArrCity=@DepCity and
                   D.WebPub='Y'
                Order by D.FlightNo, D.FlyDate  
              end
  Open RetFCursor
  Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
  While @@FETCH_STATUS = 0
  begin
    Select @DepTime=DepTime,
      @DFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
      @DFullNameL=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
      @DepFlightAirline=A.Name,
      @DepFlightAirlineL=isnull(dbo.FindLocalName(A.NameLID,@Market),A.Name),
      @DepartureDepTime=deptime,
      @DepartureArrTime=arrtime,
      @DepFlightDuration=datediff(minute,deptime,arrtime),
      @DepCountCloseTime=CountCloseTime
    From FlightDay (NOLOCK) FD
    Join AirLine (NOLOCK) A ON A.Code=FD.Airline
    Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
    Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
    where FlightNo=@DepFlight and 
       FlyDate=@DepDate and
       WebPub='Y'

    Select @RetTime=DepTime, 
      @RFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
      @RFullNameL=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
      @RetFlightAirline=A.Name,
      @RetFlightAirlineL=isnull(dbo.FindLocalName(A.NameLID,@Market),A.Name),
      @ReturnDepTime=deptime,
      @ReturnArrTime=arrtime,
      @RetFlightDuration=datediff(minute,deptime,arrtime),
      @RetCountCloseTime=CountCloseTime  
    From FlightDay (NOLOCK)  FD
    Join AirLine (NOLOCK) A ON A.Code=FD.Airline
    Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
    Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
    where FlightNo=@RetFlight and 
       FlyDate=@ArrDate and
       WebPub='Y'
    if (Exists(Select Class From FlightClass Where Class=@DepClass And (@b2bAllClass = 1 OR B2B=1))) -- 2010.01.06 eklendi
    Begin -- 2010.01.06 eklendi
        insert into #Flight
        (DepDate,DepTime,DepFlight,DFullName,DFullNameL,DepClass,DepFlightAirline,DepFlightAirlineL,ArrDate,ArrTime,ArrFlight,AFullName,AFullNameL,ArrClass,ArrFlightAirline,ArrFlightAirlineL,Days,isRT,DepCity,ArrCity,isNext,DepartureDepTime,DepartureArrTime,DepFlightDuration,DepCountCloseTime,ReturnDepTime,ReturnArrTime,RetFlightDuration,RetCountCloseTime)
        values
        (@DepDate,@DepTime,@DepFlight,@DFullName,@DFullNameL,@DepClass,@DepFlightAirline,@DepFlightAirlineL,@ArrDate,@RetTime,@RetFlight,@RFullName,@RFullNameL,@DepClass,@DepFlightAirline,@DepFlightAirlineL,DateDiff(day, @DepDate,@ArrDate),'RT',@DepCity,@ArrCity,1,@DepartureDepTime,@DepartureArrTime,@DepFlightDuration,@DepCountCloseTime,@ReturnDepTime,@ReturnArrTime,@RetFlightDuration,@RetCountCloseTime)
    End -- 2010.01.06 eklendi
    Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
  end
  Close RetFCursor
  DeAllocate RetFCursor

  Fetch Next FROM FCursor INTO @DepDate, @DepFlight, @DepClass, @DepCity, @ArrCity
end
Close FCursor
DeAllocate FCursor
Select * From #Flight
";
                        #endregion
                    else
                    {
                        if (UserData.TvParams.TvParamFlight.UseTicPriceForOTic.HasValue && UserData.TvParams.TvParamFlight.UseTicPriceForOTic.Value)
                            #region Novaturas || !LTAIP
                            tsql =
@"
Declare 		
    @chkNext smallint,                                 
    @DepDate DateTime, 
    @DepTime DateTime, 
    @DepFlight varchar(10), 
    @RetFlight varchar(10), 
    @RetTime DateTime, 
    @DepClass varchar(2), 
    @NextFlight varchar(10), 
    @DepCity int, 
    @ArrCity int, 
    @Airline varchar(10), 
    @ArrDate DateTime,
    @DFullName varchar(100), 
    @RFullName varchar(100), 
    @DepFlightAirline varchar(50), 
    @RetFlightAirline varchar(50),
    @DepartureDepTime DateTime,
    @DepartureArrTime DateTime,
    @ReturnDepTime DateTime,                                
    @ReturnArrTime DateTime,
    @DepFlightDuration int,
    @RetFlightDuration int,
    @DepCountCloseTime DateTime,
    @RetCountCloseTime DateTime,
    @FlgOperator VarChar(10),
    @FlgMarket VarChar(10)
if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
    CREATE TABLE #Flight
    (
    DepDate DateTime,
    DepTime DateTime,
    DepFlight varchar(10) Collate database_default,
    DFullName varchar(100) Collate database_default,
    DepClass  varchar(2) Collate database_default,
    DepFlightAirline  varchar(50) Collate database_default,
    ArrDate DateTime,
    ArrTime DateTime,
    ArrFlight varchar(10) Collate database_default, 
    AFullName varchar(100) Collate database_default, 
    ArrClass varchar(2) Collate database_default, 
    ArrFlightAirline varchar(50) Collate database_default, 
    Days SmallInt,
    isRT varchar(2) Collate database_default,
    DepCity int,
    ArrCity int,
    isNext SmallInt,
    DepartureDepTime DateTime,
    DepartureArrTime DateTime, 
    ReturnDepTime DateTime,
    ReturnArrTime DateTime,
    DepFlightDuration int,
    RetFlightDuration int,
    DepCountCloseTime DateTime,
    RetCountCloseTime DateTime,
    Operator VarChar(10),
    Market VarChar(10)
    )
truncate table #Flight

Declare FCursor CURSOR LOCAL FAST_FORWARD FOR
    Select Distinct DD.FlyDate DepDate, DD.FlightNo DepFlight, DP.DepClass DepClass, DepCity = @routeFrom, ArrCity = @routeTo, DP.Operator, O.Market
    From FlightDay DD (NOLOCK)
    Join FlightUPrice DP (NOLOCK) on DD.FlightNo = DP.DepFlight and DD.FlyDate = DP.DepDate and DP.Operator<>'LTAIP'
    Join Operator O (NOLOCK) on DP.Operator=O.Code
    Where DD.DepCity = @routeFrom and DD.ArrCity = @routeTo and
        DD.FlyDate between @cIn1 and @cIn2 and DP.DepClass=isnull(@flyClass,DP.DepClass) and
        DD.WebPub='Y' And
        DD.StopSale='N'
    Order by DD.FlyDate, DD.FlightNo
Open FCursor
Fetch Next FROM FCursor INTO @DepDate, @DepFlight, @DepClass, @DepCity, @ArrCity, @FlgOperator, @FlgMarket
While @@FETCH_STATUS = 0
begin
    Select @NextFlight=NextFlight,@DepCity=DepCity,@ArrCity=ArrCity,@Airline=Airline 
    From FlightDay (NOLOCK)
    where FlightNo=@DepFlight and 
    FlyDate=@DepDate And
    WebPub='Y'

    if exists (Select * From FlightDay (NOLOCK) where FlightNo = @DepFlight and FlyDate = @DepDate and isnull(RetFlightUseSysSet, 'N') = 'N')
    Select @ChkNext = RetFlightOpt From FlightDay (NOLOCK) where FlightNo = @DepFlight and FlyDate = @DepDate
    else
    if exists (Select * From ParamFlight (NOLOCK) where isnull(UseSysParam,'N')='N')
        Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = @Market
    else
        Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = ''
                            
    if @allFlight=1 Set @ChkNext=3

    if @ChkNext = 1 and isnull(@NextFlight,'') <> ''
    begin
        Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
        Select Distinct RetFlight = D.FlightNo, D.FlyDate
        from FlightDay D (NOLOCK)
        Join FlightUPrice P (NOLOCK) on D.FlightNo = P.DepFlight and P.DepDate = D.FlyDate and P.Operator = @FlgOperator
        where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
        DateAdd(day, @day2, @DepDate) and 
        D.DepCity=@ArrCity and 
        D.ArrCity=@DepCity and 
        D.FlightNo = @NextFlight and
        D.WebPub='Y'
        Order by D.FlightNo, D.FlyDate
    end
    else
        if @ChkNext = 2
        begin
        Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
        Select Distinct RetFlight = D.FlightNo, D.FlyDate
        from FlightDay D (NOLOCK)
        Join FlightUPrice P (NOLOCK) on D.FlightNo = P.DepFlight and D.FlyDate = P.DepDate and Operator = @FlgOperator
        where D.FlyDate between DateAdd(day, @day1, @DepDate) and DateAdd(day, @day2, @DepDate) and D.DepCity=@ArrCity and D.ArrCity=@DepCity and D.Airline = @Airline and
            D.WebPub='Y'
        Order by D.FlightNo, D.FlyDate
        end
        else
            if @ChkNext = 3
                begin
                Declare @DepFlightNo varchar(10), @DepFlyDate DateTime, @FlyDate DateTime, 
                    @DepCountry int, @ArrCountry int

                    Select @DepCity=DepCity,@ArrCity=ArrCity From FlightDay (NOLOCK) 
                    Where FlightNo=@DepFlightNo and FlyDate=@DepFlyDate
                                          		
		            Select @DepCountry=Country from Location (NOLOCK) where RecID=@DepCity
		            Select @ArrCountry=Country from Location (NOLOCK) where RecID=@ArrCity 
                                		  
		            Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
		            Select Distinct RetFlight=D.FlightNo,D.FlyDate		  
			        from FlightDay D (NOLOCK) 
			        join FlightUPrice P (NOLOCK) on D.FlightNo = P.DepFlight and P.DepDate = D.FlyDate and Operator = @Operator 
			        where D.FlyDate between DateAdd(day, @day1, @DepDate) and DateAdd(day, @day2, @DepDate) and 
				            ArrCity <> @ArrCity and DepCity <> @DepCity and 
				            @ArrCountry = (Select Country from Location (NOLOCK) where RecID = D.DepCity) and 
				            @DepCountry = (Select Country from Location (NOLOCK) where RecID = D.ArrCity) 
			        Order by D.FlightNo 		  
                end 
                else
                begin
                    Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
                    Select Distinct RetFlight = D.FlightNo, D.FlyDate
                    from FlightDay D (NOLOCK)
                    Join FlightUPrice P (NOLOCK) on D.FlightNo = P.ArrFlight and P.ArrDate = D.FlyDate and Operator = @FlgOperator
                    where D.FlyDate between DateAdd(day, @day1, @DepDate) and DateAdd(day, @day2, @DepDate) and D.DepCity=@ArrCity and D.ArrCity=@DepCity And
                        D.WebPub='Y'
                    Order by D.FlightNo, D.FlyDate  
                end
    Open RetFCursor
    Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
    While @@FETCH_STATUS = 0
    begin
    Select @DepTime=DepTime, 
			@DFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')',
			@DepFlightAirline=A.Name,
            @DepartureDepTime=deptime,@DepartureArrTime=arrtime,@DepFlightDuration=datediff(minute,deptime,arrtime),
            @DepCountCloseTime=CountCloseTime
    From FlightDay (NOLOCK) FD
    Join AirLine A (NOLOCK) ON A.Code=FD.Airline
    Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
    Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
    Where FlightNo=@DepFlight and FlyDate=@DepDate And WebPub='Y'

    Select @RetTime=DepTime, 
			@RFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
			@RetFlightAirline=A.Name,								   
            @ReturnDepTime=deptime,@ReturnArrTime=arrtime,@RetFlightDuration=datediff(minute,deptime,arrtime) ,@RetCountCloseTime=CountCloseTime
    From FlightDay (NOLOCK) FD
    Join AirLine A (NOLOCK) ON A.Code=FD.Airline
    Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
    Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
    where FlightNo=@RetFlight and FlyDate=@ArrDate and WebPub='Y'
                            
    if (Exists(Select Class From FlightClass Where Class=@DepClass And (@b2bAllClass = 1 OR B2B=1))) -- 2010.01.06 eklendi
    Begin -- 2010.01.06 eklendi
        insert into #Flight
        (DepDate, DepTime, DepFlight, DFullName, DepClass, DepFlightAirline, ArrDate, ArrTime, ArrFlight, AFullName, ArrClass, ArrFlightAirline, Days, isRT, DepCity, ArrCity, isNext,DepartureDepTime,DepartureArrTime,DepFlightDuration,DepCountCloseTime,ReturnDepTime,ReturnArrTime,RetFlightDuration,RetCountCloseTime,Operator,Market)
        values
        (@DepDate,@DepTime,@DepFlight,@DFullName,@DepClass,@DepFlightAirline,@ArrDate,@RetTime,@RetFlight,@RFullName,@DepClass,@DepFlightAirline,DateDiff(day, @DepDate, @ArrDate),'RT',@DepCity, @ArrCity,1,@DepartureDepTime,@DepartureArrTime,@DepFlightDuration,@DepCountCloseTime,@ReturnDepTime,@ReturnArrTime,@RetFlightDuration,@RetCountCloseTime,@FlgOperator,@FlgMarket)
    End -- 2010.01.06 eklendi
    Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
    end
    Close RetFCursor
    DeAllocate RetFCursor

    Fetch Next FROM FCursor INTO @DepDate, @DepFlight, @DepClass, @DepCity, @ArrCity, @FlgOperator, @FlgMarket
end
Close FCursor
DeAllocate FCursor
select * From #Flight
";
                            #endregion
                        else
                            #region Novaturas || !LTAIP
                            tsql = @"Declare 		
                            @chkNext smallint,                                 
                            @DepDate DateTime, 
                            @DepTime DateTime, 
                            @DepFlight varchar(10), 
                            @RetFlight varchar(10), 
                            @RetTime DateTime, 
                            @DepClass varchar(2), 
                            @NextFlight varchar(10), 
                            @DepCity int, 
                            @ArrCity int, 
                            @Airline varchar(10), 
                            @ArrDate DateTime,
                            @DFullName varchar(100), 
                            @RFullName varchar(100), 
                            @DepFlightAirline varchar(50), 
                            @RetFlightAirline varchar(50),
                            @DepartureDepTime DateTime,
                            @DepartureArrTime DateTime,
                            @ReturnDepTime DateTime,                                
                            @ReturnArrTime DateTime,
                            @DepFlightDuration int,
                            @RetFlightDuration int,
                            @DepCountCloseTime DateTime,
                            @RetCountCloseTime DateTime,
                            @FlgOperator VarChar(10),
                            @FlgMarket VarChar(10)
                        if not OBJECT_ID('TempDB.dbo.#Flight') is null drop table #Flight
                          CREATE TABLE #Flight
                          (
                            DepDate DateTime,
                            DepTime DateTime,
                            DepFlight varchar(10) Collate database_default,
                            DFullName varchar(100) Collate database_default,
                            DepClass  varchar(2) Collate database_default,
                            DepFlightAirline  varchar(50) Collate database_default,
                            ArrDate DateTime,
                            ArrTime DateTime,
                            ArrFlight varchar(10) Collate database_default, 
                            AFullName varchar(100) Collate database_default, 
                            ArrClass varchar(2) Collate database_default, 
                            ArrFlightAirline varchar(50) Collate database_default, 
                            Days SmallInt,
                            isRT varchar(2) Collate database_default,
                            DepCity int,
                            ArrCity int,
                            isNext SmallInt,
                            DepartureDepTime DateTime,
                            DepartureArrTime DateTime, 
                            ReturnDepTime DateTime,
                            ReturnArrTime DateTime,
                            DepFlightDuration int,
                            RetFlightDuration int,
                            DepCountCloseTime DateTime,
                            RetCountCloseTime DateTime,
                            Operator VarChar(10),
                            Market VarChar(10)
                          )
                        truncate table #Flight

                        Declare FCursor CURSOR LOCAL FAST_FORWARD FOR
                          Select Distinct DD.FlyDate DepDate, DD.FlightNo DepFlight, DP.SClass DepClass, DepCity = @routeFrom, ArrCity = @routeTo, DP.Operator, O.Market
                          From FlightDay DD (NOLOCK)
                          Join FlightPrice DP (NOLOCK) on DD.FlightNo = DP.FlightNo and DD.FlyDate = DP.FlyDate and isnull(DP.Buyer,'') = '' and DP.Operator<>'LTAIP'
                          Join Operator O (NOLOCK) on DP.Operator=O.Code
                          Where DD.DepCity = @routeFrom and DD.ArrCity = @routeTo and
                                DD.FlyDate between @cIn1 and @cIn2 and DP.SClass=isnull(@flyClass,DP.SClass) and
                                DD.WebPub='Y' And
                                DD.StopSale='N'
                          Order by DD.FlyDate, DD.FlightNo
                        Open FCursor
                        Fetch Next FROM FCursor INTO @DepDate, @DepFlight, @DepClass, @DepCity, @ArrCity, @FlgOperator, @FlgMarket
                        While @@FETCH_STATUS = 0
                        begin
                          Select @NextFlight=NextFlight,@DepCity=DepCity,@ArrCity=ArrCity,@Airline=Airline 
                          From FlightDay (NOLOCK)
                          where FlightNo=@DepFlight and 
                            FlyDate=@DepDate And
                            WebPub='Y'

                          if exists (Select * From FlightDay (NOLOCK) where FlightNo = @DepFlight and FlyDate = @DepDate and isnull(RetFlightUseSysSet, 'N') = 'N')
                            Select @ChkNext = RetFlightOpt From FlightDay (NOLOCK) where FlightNo = @DepFlight and FlyDate = @DepDate
                          else
                            if exists (Select * From ParamFlight (NOLOCK) where isnull(UseSysParam,'N')='N')
                              Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = @Market
                            else
                              Select @ChkNext = RetFlightOpt From ParamFlight (NOLOCK) where Market = ''
                            
                            if @allFlight=1 Set @ChkNext=3

                            if @ChkNext = 1 and isnull(@NextFlight,'') <> ''
                            begin
                              Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
                              Select Distinct RetFlight = D.FlightNo, D.FlyDate
                              from FlightDay D (NOLOCK)
                              Join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' and P.Operator = @FlgOperator
                              where D.FlyDate between DateAdd(day, @day1, @DepDate) and 
                                DateAdd(day, @day2, @DepDate) and 
                                D.DepCity=@ArrCity and 
                                D.ArrCity=@DepCity and 
                                D.FlightNo = @NextFlight and
                                D.WebPub='Y'
                              Order by D.FlightNo, D.FlyDate
                            end
                            else
                              if @ChkNext = 2
                              begin
                                Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
                                Select Distinct RetFlight = D.FlightNo, D.FlyDate
                                from FlightDay D (NOLOCK)
                                Join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' and Operator = @FlgOperator
                                where D.FlyDate between DateAdd(day, @day1, @DepDate) and DateAdd(day, @day2, @DepDate) and D.DepCity=@ArrCity and D.ArrCity=@DepCity and D.Airline = @Airline and
                                  D.WebPub='Y'
                                Order by D.FlightNo, D.FlyDate
                              end
                              else
                                 if @ChkNext = 3
                                      begin
                                      Declare @DepFlightNo varchar(10), @DepFlyDate DateTime, @FlyDate DateTime, 
                                           @DepCountry int, @ArrCountry int

                                          Select @DepCity=DepCity,@ArrCity=ArrCity From FlightDay (NOLOCK) 
                                          Where FlightNo=@DepFlightNo and FlyDate=@DepFlyDate
                                          		
		                                  Select @DepCountry=Country from Location (NOLOCK) where RecID=@DepCity
		                                  Select @ArrCountry=Country from Location (NOLOCK) where RecID=@ArrCity 
                                		  
		                                  Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
		                                  Select Distinct RetFlight=D.FlightNo,D.FlyDate		  
			                                from FlightDay D (NOLOCK) 
			                                join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' and Operator = @Operator 
			                                where D.FlyDate between DateAdd(day, @day1, @DepDate) and DateAdd(day, @day2, @DepDate) and 
				                                  ArrCity <> @ArrCity and DepCity <> @DepCity and 
				                                  @ArrCountry = (Select Country from Location (NOLOCK) where RecID = D.DepCity) and 
				                                  @DepCountry = (Select Country from Location (NOLOCK) where RecID = D.ArrCity) 
			                                Order by D.FlightNo 		  
                                      end 
                                      else
                                      begin
                                        Declare RetFCursor CURSOR LOCAL FAST_FORWARD FOR
                                        Select Distinct RetFlight = D.FlightNo, D.FlyDate
                                        from FlightDay D (NOLOCK)
                                        Join FlightPrice P (NOLOCK) on D.FlightNo = P.FlightNo and D.FlyDate = P.FlyDate and isnull(P.Buyer,'') = '' and Operator = @FlgOperator
                                        where D.FlyDate between DateAdd(day, @day1, @DepDate) and DateAdd(day, @day2, @DepDate) and D.DepCity=@ArrCity and D.ArrCity=@DepCity And
                                          D.WebPub='Y'
                                        Order by D.FlightNo, D.FlyDate  
                                      end
                          Open RetFCursor
                          Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
                          While @@FETCH_STATUS = 0
                          begin
                            Select @DepTime=DepTime, 
								   @DFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')',
								   @DepFlightAirline=A.Name,
                                   @DepartureDepTime=deptime,@DepartureArrTime=arrtime,@DepFlightDuration=datediff(minute,deptime,arrtime),
                                   @DepCountCloseTime=CountCloseTime
                            From FlightDay (NOLOCK) FD
                            Join AirLine A (NOLOCK) ON A.Code=FD.Airline
                            Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
                            Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
                            Where FlightNo=@DepFlight and FlyDate=@DepDate And WebPub='Y'

                            Select @RetTime=DepTime, 
								   @RFullName=FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' Then ArrAirport Else AA.NameS End)+')', 
								   @RetFlightAirline=A.Name,								   
                                   @ReturnDepTime=deptime,@ReturnArrTime=arrtime,@RetFlightDuration=datediff(minute,deptime,arrtime) ,@RetCountCloseTime=CountCloseTime
                            From FlightDay (NOLOCK) FD
                            Join AirLine A (NOLOCK) ON A.Code=FD.Airline
                            Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
                            Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
                            where FlightNo=@RetFlight and FlyDate=@ArrDate and WebPub='Y'
                            
                            if (Exists(Select Class From FlightClass Where Class=@DepClass And (@b2bAllClass = 1 OR B2B=1))) -- 2010.01.06 eklendi
                            Begin -- 2010.01.06 eklendi
                                insert into #Flight
                                (DepDate, DepTime, DepFlight, DFullName, DepClass, DepFlightAirline, ArrDate, ArrTime, ArrFlight, AFullName, ArrClass, ArrFlightAirline, Days, isRT, DepCity, ArrCity, isNext,DepartureDepTime,DepartureArrTime,DepFlightDuration,DepCountCloseTime,ReturnDepTime,ReturnArrTime,RetFlightDuration,RetCountCloseTime,Operator,Market)
                                values
                                (@DepDate,@DepTime,@DepFlight,@DFullName,@DepClass,@DepFlightAirline,@ArrDate,@RetTime,@RetFlight,@RFullName,@DepClass,@DepFlightAirline,DateDiff(day, @DepDate, @ArrDate),'RT',@DepCity, @ArrCity,1,@DepartureDepTime,@DepartureArrTime,@DepFlightDuration,@DepCountCloseTime,@ReturnDepTime,@ReturnArrTime,@RetFlightDuration,@RetCountCloseTime,@FlgOperator,@FlgMarket)
                            End -- 2010.01.06 eklendi
                            Fetch Next FROM RetFCursor INTO @RetFlight, @ArrDate
                          end
                          Close RetFCursor
                          DeAllocate RetFCursor

                          Fetch Next FROM FCursor INTO @DepDate, @DepFlight, @DepClass, @DepCity, @ArrCity, @FlgOperator, @FlgMarket
                        end
                        Close FCursor
                        DeAllocate FCursor
                        select * From #Flight";
                            #endregion
                    }
                }
            }
            else
            {
                #region OW
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && VersionControl.CheckWebVersion(UserData.WebVersion, 43, VersionControl.Equality.gt))
                {
                    tsql = @"Select Distinct DD.FlyDate DepDate, DD.DepTime DepTime, DD.FlightNo DepFlight, 
                                DFullName=DD.FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DD.DepAirport Else DA.NameS End)+'-'+
										                   (Case When isnull(AA.NameS,'')='' Then DD.ArrAirport Else AA.NameS End)+')', 
                                DP.SClass DepClass, 
                                DepFlightAirline = (Select Name From Airline (NOLOCK) Where Code = DD.Airline),
                                isRT = 'OW', isNext = 0, DepCity = @routeFrom, ArrCity = @routeTo, dd.deptime as DepartureDepTime,dd.arrtime as DepartureArrTime,datediff(minute,dd.deptime,dd.arrtime) DepFlightDuration, DD.CountCloseTime as DepCountCloseTime,
                                ArrDate=null,ArrTime=null,ArrFlight=null,AFullName=null,ArrClass=null,ArrFlightAirline=null,Days=null,isNext=null,ReturnDepTime=null,
	                            ReturnArrTime=null,RetFlightDuration=null,RetCountCloseTime=null, DP.Operator, O.Market
                            From FlightDay DD (NOLOCK)
                            Join AirPort (NOLOCK) DA ON DA.Code=DD.DepAirport
                            Join AirPort (NOLOCK) AA ON AA.Code=DD.ArrAirport
                            Join FlightPrice DP (NOLOCK) on DD.FlightNo=DP.FlightNo and DD.FlyDate = DP.FlyDate and isnull(DP.Buyer,'') = ''
                            Join FlightClass FC (NOLOCK) on FC.Class=DP.SClass And (@b2bAllClass = 1 OR B2B=1)
                            Join Operator O (NOLOCK) on DP.Operator=O.Code
                            Where DD.DepCity = @routeFrom and DD.ArrCity = @routeTo and
                                  DD.FlyDate between @cIn1 and @cIn2 and DP.SClass=isnull(@flyClass,DP.SClass) And
                                  DD.WebPub='Y' And
                                  DD.StopSale='N' And
                                 (DP.Operator=@Operator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID=DD.RecID And Operator=@Operator))
                            Order by DD.FlyDate, DD.FlightNo";
                }
                else
                {
                    if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.Market, "LTAIP"))
                    {
                        if (UserData.TvParams.TvParamFlight.UseTicPriceForOTic.HasValue && UserData.TvParams.TvParamFlight.UseTicPriceForOTic.Value)
                            tsql =
@"
Select Distinct DD.FlyDate DepDate, DD.DepTime DepTime, DD.FlightNo DepFlight, 
	DFullName=DD.FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DD.DepAirport Else DA.NameS End)+'-'+
								(Case When isnull(AA.NameS,'')='' Then DD.ArrAirport Else AA.NameS End)+')', 
	DP.DepClass DepClass, 
    DepFlightAirline=(Select Name From Airline (NOLOCK) Where Code = DD.Airline),
    isRT = 'OW', isNext = 0, DepCity = @routeFrom, ArrCity = @routeTo, dd.deptime as DepartureDepTime,dd.arrtime as DepartureArrTime,datediff(minute,dd.deptime,dd.arrtime) DepFlightDuration, DD.CountCloseTime as DepCountCloseTime,
    ArrDate=null,ArrTime=null,ArrFlight=null,AFullName=null,ArrClass=null,ArrFlightAirline=null,Days=null,isNext=null,ReturnDepTime=null,
	ReturnArrTime=null,RetFlightDuration=null,RetCountCloseTime=null
From FlightDay DD (NOLOCK)
Join AirPort (NOLOCK) DA ON DA.Code=DD.DepAirport
Join AirPort (NOLOCK) AA ON AA.Code=DD.ArrAirport
Join FlightUPrice DP (NOLOCK) on DD.FlightNo = DP.DepFlight and DD.FlyDate = DP.DepDate and DP.Operator = @Operator
Join FlightClass FC (NOLOCK) on FC.Class=DP.DepClass And (@b2bAllClass = 1 OR B2B=1)
Where DD.DepCity = @routeFrom and DD.ArrCity = @routeTo and
        DD.FlyDate between @cIn1 and @cIn2 and DP.DepClass=isnull(@flyClass,DP.DepClass) And
        DD.WebPub='Y' And
        DD.StopSale='N'
Order by DD.FlyDate, DD.FlightNo
";
                        else
                            tsql = @"Select Distinct DD.FlyDate DepDate, DD.DepTime DepTime, DD.FlightNo DepFlight, 
								        DFullName=DD.FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DD.DepAirport Else DA.NameS End)+'-'+
										                           (Case When isnull(AA.NameS,'')='' Then DD.ArrAirport Else AA.NameS End)+')', 
								        DP.SClass DepClass, 
                                        DepFlightAirline=(Select Name From Airline (NOLOCK) Where Code = DD.Airline),
                                        isRT = 'OW', isNext = 0, DepCity = @routeFrom, ArrCity = @routeTo, dd.deptime as DepartureDepTime,dd.arrtime as DepartureArrTime,datediff(minute,dd.deptime,dd.arrtime) DepFlightDuration, DD.CountCloseTime as DepCountCloseTime,
                                        ArrDate=null,ArrTime=null,ArrFlight=null,AFullName=null,ArrClass=null,ArrFlightAirline=null,Days=null,isNext=null,ReturnDepTime=null,
	                                    ReturnArrTime=null,RetFlightDuration=null,RetCountCloseTime=null
                                    From FlightDay DD (NOLOCK)
                                    Join AirPort (NOLOCK) DA ON DA.Code=DD.DepAirport
                                    Join AirPort (NOLOCK) AA ON AA.Code=DD.ArrAirport
                                    Join FlightPrice DP (NOLOCK) on DD.FlightNo = DP.FlightNo and DD.FlyDate = DP.FlyDate and isnull(DP.Buyer,'') = '' and DP.Operator = @Operator
                                    Join FlightClass FC (NOLOCK) on FC.Class=DP.SClass And (@b2bAllClass = 1 OR B2B=1)
                                    Where DD.DepCity = @routeFrom and DD.ArrCity = @routeTo and
                                          DD.FlyDate between @cIn1 and @cIn2 and DP.SClass=isnull(@flyClass,DP.SClass) And
                                          DD.WebPub='Y' And
                                          DD.StopSale='N'
                                    Order by DD.FlyDate, DD.FlightNo";
                    }
                    else
                    {
                        tsql = @"Select Distinct DD.FlyDate DepDate, DD.DepTime DepTime, DD.FlightNo DepFlight, 
                                DFullName=DD.FlightNo+' ('+(Case When isnull(DA.NameS,'')='' Then DD.DepAirport Else DA.NameS End)+'-'+
										                   (Case When isnull(AA.NameS,'')='' Then DD.ArrAirport Else AA.NameS End)+')', 
                                DP.SClass DepClass, 
                                DepFlightAirline = (Select Name From Airline (NOLOCK) Where Code = DD.Airline),
                                isRT = 'OW', isNext = 0, DepCity = @routeFrom, ArrCity = @routeTo, dd.deptime as DepartureDepTime,dd.arrtime as DepartureArrTime,datediff(minute,dd.deptime,dd.arrtime) DepFlightDuration, DD.CountCloseTime as DepCountCloseTime,
                                ArrDate=null,ArrTime=null,ArrFlight=null,AFullName=null,ArrClass=null,ArrFlightAirline=null,Days=null,isNext=null,ReturnDepTime=null,
	                            ReturnArrTime=null,RetFlightDuration=null,RetCountCloseTime=null, DP.Operator, O.Market
                            From FlightDay DD (NOLOCK)
                            Join AirPort (NOLOCK) DA ON DA.Code=DD.DepAirport
                            Join AirPort (NOLOCK) AA ON AA.Code=DD.ArrAirport
                            Join FlightPrice DP (NOLOCK) on DD.FlightNo=DP.FlightNo and DD.FlyDate = DP.FlyDate and isnull(DP.Buyer,'') = '' and DP.Operator<>'LTAIP'
                            Join FlightClass FC (NOLOCK) on FC.Class=DP.SClass And (@b2bAllClass = 1 OR B2B=1)
                            Join Operator O (NOLOCK) on DP.Operator=O.Code
                            Where DD.DepCity = @routeFrom and DD.ArrCity = @routeTo and
                                  DD.FlyDate between @cIn1 and @cIn2 and DP.SClass=isnull(@flyClass,DP.SClass) And
                                  DD.WebPub='Y' And
                                  DD.StopSale='N'
                            Order by DD.FlyDate, DD.FlightNo";
                    }
                }
                #endregion
            }

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);

                db.AddInParameter(dbCommand, "cIn1", DbType.DateTime, cIn1);
                db.AddInParameter(dbCommand, "cIn2", DbType.DateTime, cIn2);
                db.AddInParameter(dbCommand, "day1", DbType.Int16, day1);
                db.AddInParameter(dbCommand, "day2", DbType.Int16, day2);
                db.AddInParameter(dbCommand, "routeFrom", DbType.Int32, routeFrom);
                db.AddInParameter(dbCommand, "routeTo", DbType.Int32, routeTo);
                db.AddInParameter(dbCommand, "flyClass", DbType.String, string.IsNullOrEmpty(flyClass) ? null : flyClass);
                //db.AddInParameter(dbCommand, "isRT", DbType.String, (isRT.HasValue && isRT.Value) ? "RT" : "OW");
                db.AddInParameter(dbCommand, "b2bAllClass", DbType.Boolean, b2bAllClass);
                if (isRT.HasValue && isRT.Value)
                    db.AddInParameter(dbCommand, "allFlight", DbType.Boolean, allFlight);
                db.AddInParameter(dbCommand, "FlightType", DbType.Int32, flightType);
                Stopwatch sw = new Stopwatch();
                sw.Start();
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        OnlyTicket_Flight record = new OnlyTicket_Flight();
                        if (!(string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || (string.Equals(UserData.CustomRegID, Common.crID_Go2Holiday) && string.Equals(UserData.Market, "FIJIRIX"))))
                        {
                            record.Operator = UserData.Operator;
                            record.Market = UserData.Market;
                        }
                        else
                        {
                            record.Operator = Conversion.getStrOrNull(oReader["Operator"]);
                            record.Market = Conversion.getStrOrNull(oReader["Market"]);
                        }
                        record.DepDate = Conversion.getDateTimeOrNull(oReader["DepDate"]);
                        record.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        record.DepFlight = Conversion.getStrOrNull(oReader["DepFlight"]);
                        record.DFullName = Conversion.getStrOrNull(oReader["DFullName"]);
                        record.DepClass = Conversion.getStrOrNull(oReader["DepClass"]);
                        record.DepFlightAirline = Conversion.getStrOrNull(oReader["DepFlightAirline"]);
                        record.ArrDate = Conversion.getDateTimeOrNull(oReader["ArrDate"]);
                        record.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        record.ArrFlight = Conversion.getStrOrNull(oReader["ArrFlight"]);
                        record.AFullName = Conversion.getStrOrNull(oReader["AFullName"]);
                        record.ArrClass = Conversion.getStrOrNull(oReader["ArrClass"]);
                        record.ArrFlightAirline = Conversion.getStrOrNull(oReader["ArrFlightAirline"]);
                        record.Days = Conversion.getInt16OrNull(oReader["Days"]);
                        record.IsRT = Equals(oReader["isRT"], "RT");
                        record.DepCity = Conversion.getInt32OrNull(oReader["DepCity"]);
                        record.ArrCity = Conversion.getInt32OrNull(oReader["ArrCity"]);
                        record.IsNext = Conversion.getInt16OrNull(oReader["isNext"]);
                        record.DepartureDepTime = Conversion.getDateTimeOrNull(oReader["DepartureDepTime"]);
                        record.DepartureArrTime = Conversion.getDateTimeOrNull(oReader["DepartureArrTime"]);
                        record.ReturnDepTime = Conversion.getDateTimeOrNull(oReader["ReturnDepTime"]);
                        record.ReturnArrTime = Conversion.getDateTimeOrNull(oReader["ReturnArrTime"]);
                        record.DepFlightDuration = Conversion.getInt32OrNull(oReader["DepFlightDuration"]);
                        record.RetFlightDuration = Conversion.getInt32OrNull(oReader["RetFlightDuration"]);
                        record.DepCountCloseTime = Conversion.getDateTimeOrNull(oReader["DepCountCloseTime"]);
                        record.RetCountCloseTime = Conversion.getDateTimeOrNull(oReader["RetCountCloseTime"]);
                        records.Add(record);
                    }
                }
                sw.Stop();
                PriceSearchRequest preq = new PriceSearchRequest();
                preq.Adult = 1;
                preq.ArrCity = routeTo;
                preq.Board = null;
                preq.CatName = null;
                preq.CheckInFrom = cIn1;
                preq.Child = 0;
                preq.Country = null;
                preq.Currency = UserData.SaleCur;
                preq.DepCity = routeFrom;
                preq.Direction = 0;
                preq.FlightClass = flyClass;
                preq.HolPack = null;
                preq.HotelCode = null;
                preq.IsB2CAgency = false;
                preq.NightFrom = day1;
                preq.NightTo = day2;
                preq.Room = null;
                preq.UserData = UserData;
                preq.PriceSearchTime = (int)sw.ElapsedMilliseconds;
                preq.ProductType = TourVisio.WebService.Adapter.Enums.enmProductType.Flight;
                Task.Factory.StartNew(() => SanBI.TrySend(preq, records.Count));
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public OnlyTicket_EB getOnlyFlightEB(string DepFlight, string Operator, string Market, DateTime? ResDate, DateTime? BegDate, Int16? Days, ref string errorMsg)
        {
            string tsql = @"Select Top 1 EBPasPer=IsNull(PasEBPer,0), EBP_AdlVal=AdlVal, EBP_ChdGrp1Val=ChdGrp1Val, EBP_ChdGrp2Val=ChdGrp2Val, EBP_Cur=Cur
                            From PasEB EB (NOLOCK)
                            Join PasEBPer EBP (NOLOCK) on EBP.PasEbID=EB.RecID
                            Join PasEBFlight EBF (NOLOCK) on EBF.PasEbID=EB.RecID and EBF.Flight=@DepFlight
                            Where OwnerOperator=@Operator and OwnerMarket=@Market and Operator=@Operator and Market=@Market and EB.SaleType=1
                              and @ResDate between SaleBegDate and SaleEndDate and @BegDate between ResBegDate and ResEndDate
                              and (@Days between IsNull(MinDay,0) and IsNull(MaxDay,999)) and SubString(DoW,DATEPART(dw,@BegDate),1)=1
                            Order by EBF.Flight Desc,EB.CrtDate Desc,SaleBegDate Desc, ResBegDate Desc ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                db.AddInParameter(dbCommand, "DepFlight", DbType.String, DepFlight);
                db.AddInParameter(dbCommand, "ResDate", DbType.DateTime, ResDate);
                db.AddInParameter(dbCommand, "BegDate", DbType.DateTime, BegDate);
                db.AddInParameter(dbCommand, "Days", DbType.Int16, Days);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        OnlyTicket_EB record = new OnlyTicket_EB();
                        record.FlightNo = DepFlight;
                        record.FlyDate = BegDate;
                        record.EBPasPer = Conversion.getDecimalOrNull(R["EBPasPer"]);
                        record.EBP_AdlVal = Conversion.getDecimalOrNull(R["EBP_AdlVal"]);
                        record.EBP_ChdGrp1Val = Conversion.getDecimalOrNull(R["EBP_ChdGrp1Val"]);
                        record.EBP_ChdGrp2Val = Conversion.getDecimalOrNull(R["EBP_ChdGrp2Val"]);
                        record.EBP_Cur = Conversion.getStrOrNull(R["EBP_Cur"]);

                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightData FillFlights(User UserData, FlightData flightData, OnlyTicketCriteria Criteria, ref string errorMsg)
        {
            bool b2bAllClass = Criteria.B2bAllClass;
            List<OnlyFlight_FlightInfo> dtFlightInfo = flightData.FlightInfo;
            List<OnlyTicket_Flight> dtFlightPrice = GetFlightPrice(UserData, Criteria.FlyDateBeg, Criteria.FlyDateEnd, Criteria.Night1, Criteria.Night2,
                                                                    Criteria.FlyClass, Criteria.DepCity, Criteria.ArrCity, Criteria.IsRT, b2bAllClass, Criteria.allFlight, Criteria.FlightType, ref errorMsg);
            foreach (OnlyTicket_Flight dr in dtFlightPrice)
            {
                dtFlightInfo = new OnlyTickets().WriteToFlightInfoRow(UserData, flightData, dr, Criteria.CurControl.HasValue ? Criteria.CurControl.Value : false, Criteria.Cur, ref errorMsg);
            }
            return flightData;
        }

        public FlightDays getFlightDaysRecords(string FlightNo, DateTime? FlyDate, ref string errorMsg)
        {
            string tsql = @"Select FlyDate, DepCity, ArrCity, DepAirport, ArrAirport 
                            From FlightDay (NOLOCK) 
                            Where FlightNo = @FlightNo 
                              And FlyDate = @FlyDate ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        FlightDays record = new FlightDays();
                        record.FlyDate = (DateTime)oReader["FlyDate"];
                        record.DepCity = (int)oReader["DepCity"];
                        record.DepAirport = Conversion.getStrOrNull(oReader["DepAirport"]);
                        record.ArrCity = (int)oReader["ArrCity"];
                        record.ArrAirport = Conversion.getStrOrNull(oReader["ArrAirport"]);
                        return record;
                    }
                    else
                        return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        /// <summary>
        /// Haluk beyin yazdığı methot
        /// </summary>
        /// <param name="NetPrice"></param>
        /// <param name="ProfMars"></param>
        /// <param name="CalcItem"></param>
        /// <returns></returns>
        public decimal CalcPrice2Old(Decimal NetPrice, Profmars ProfMars, int CalcItem)
        {
            if ((NetPrice == 0) ||
               (
                 (ProfMars.GenPer == 0) && (ProfMars.GenVal == 0) &&
                 (ProfMars.AdlPer == 0) && (ProfMars.AdlVal == 0) &&
                 (ProfMars.ChdG1Per == 0) && (ProfMars.ChdG1Val == 0) &&
                 (ProfMars.ChdG2Per == 0) && (ProfMars.ChdG2Val == 0) &&
                 (ProfMars.ChdG3Per == 0) && (ProfMars.ChdG3Val == 0) &&
                 (ProfMars.AgencyCom == 0)
               ))
                return NetPrice;

            decimal ComPer = 0;
            decimal ComVal = 0;

            if (CalcItem == 1)
            {
                if (ProfMars.AdlPer != 0)
                    ComPer = Convert.ToDecimal(ProfMars.AdlPer);
                else
                    ComVal = Convert.ToDecimal(ProfMars.AdlVal);
            }
            else if (CalcItem == 2)
            {
                if (ProfMars.ChdG1Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG1Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG1Val);
            }
            else if (CalcItem == 3)
            {
                if (ProfMars.ChdG2Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG2Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG2Val);
            }
            else if (CalcItem == 4)
            {
                if (ProfMars.ChdG3Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG3Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG3Val);
            }


            if (ComPer == 0 && ComVal == 0)
            {
                ComPer = Convert.ToDecimal(ProfMars.GenPer);
                ComVal = Convert.ToDecimal(ProfMars.GenVal);
            }

            if (ComPer != 0)
                return NetPrice + (NetPrice * ComPer / 100);
            else
                return NetPrice + ComVal;
        }

        public decimal CalcPrice2(Decimal NetPrice, Profmars ProfMars, int CalcItem, Int16? SaleCalcType)
        {
            if ((NetPrice == 0) ||
               (
                 (ProfMars.GenPer == 0) && (ProfMars.GenVal == 0) &&
                 (ProfMars.AdlPer == 0) && (ProfMars.AdlVal == 0) &&
                 (ProfMars.ChdG1Per == 0) && (ProfMars.ChdG1Val == 0) &&
                 (ProfMars.ChdG2Per == 0) && (ProfMars.ChdG2Val == 0) &&
                 (ProfMars.ChdG3Per == 0) && (ProfMars.ChdG3Val == 0) &&
                 (ProfMars.ChdG4Per == 0) && (ProfMars.ChdG4Val == 0) &&
                 (ProfMars.AgencyCom == 0)
               ))
                return NetPrice;

            decimal ComPer = 0;
            decimal ComVal = 0;

            if (CalcItem == 1)
            {
                if (ProfMars.AdlPer != 0)
                    ComPer = Convert.ToDecimal(ProfMars.AdlPer);
                else
                    ComVal = Convert.ToDecimal(ProfMars.AdlVal);
            }
            else if (CalcItem == 2)
            {
                if (ProfMars.ChdG1Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG1Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG1Val);
            }
            else if (CalcItem == 3)
            {
                if (ProfMars.ChdG2Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG2Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG2Val);
            }
            else if (CalcItem == 4)
            {
                if (ProfMars.ChdG3Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG3Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG3Val);
            }
            else if (CalcItem == 5)
            {
                if (ProfMars.ChdG4Per != 0)
                    ComPer = Convert.ToDecimal(ProfMars.ChdG4Per);
                else
                    ComVal = Convert.ToDecimal(ProfMars.ChdG4Val);
            }


            if (ComPer == 0 && ComVal == 0)
            {
                ComPer = Convert.ToDecimal(ProfMars.GenPer);
                ComVal = Convert.ToDecimal(ProfMars.GenVal);
            }

            if (ComPer != 0)
            {
                if (SaleCalcType.HasValue && SaleCalcType.Value == 1)
                    return NetPrice / (1 - (ComPer / 100)); //Divide method
                return NetPrice + (NetPrice * ComPer / 100); //Multiple method
            }
            else
                return NetPrice + ComVal;
        }

        public Profmars FindFlightProfit(User UserData, string flightNo, DateTime flyDate, int day, string NetCur, ref string errorMsg)
        {
            Profmars pf = new Profmars();
            FlightDays dtFlightDay = new OnlyTickets().getFlightDaysRecords(flightNo, flyDate, ref errorMsg);
            if (dtFlightDay == null)
                return pf;
            int depcity = dtFlightDay.DepCity;
            int arrcity = dtFlightDay.ArrCity;
            string depairport = dtFlightDay.DepAirport;
            string arrairport = dtFlightDay.ArrAirport;
            string tsql = @"Select Top 1 P.RecID, P.Service, P.RuleNoID, R.RuleNo, R.BegDate, R.EndDate, R.HotCat, R.Room, R.Accom, R.Board, 
                                    R.FromCity, R.FromAirport, R.ToCity, R.ToAirport, R.Country, R.Location, R.TrfType, R.InsZone, R.VisaType, 
                                    R.RentCat, R.MinDay, R.MaxDay, R.GenPer, R.GenVal, R.AdlPer, R.AdlVal,
                                    R.ChdG1Per,R.ChdG1Val, R.ChdG2Per, R.ChdG2Val, R.ChdG3Per, R.ChdG3Val, R.Cur
                                From GenProfMar P (NOLOCK)
                                Join ProfRuleDet R on R.RuleNoID=P.RuleNoID and R.Service=P.Service
                                Where ProfType='I' and P.Market=@Market and P.Service='FLIGHT' and @FlyDate between BegDate and EndDate
                                    and (@Day >= MinDay or MinDay is Null) and (@Day <= MaxDay or MaxDay is Null)
                                    and (FromCity=@FromCity or FromCity is Null) and (ToCity=@ToCity or ToCity is Null)
                                    and (FromAirport=@FromAirport or FromAirport ='') and (ToAirport=@ToAirport or ToAirport ='')
                                Order by FromCity Desc,FromAirport Desc,ToCity Desc,ToAirport Desc,EndDate-BegDate,P.RecID";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, flyDate);
                db.AddInParameter(dbCommand, "Day", DbType.Int32, day);
                db.AddInParameter(dbCommand, "FromCity", DbType.Int32, depcity);
                db.AddInParameter(dbCommand, "ToCity", DbType.Int32, arrcity);
                db.AddInParameter(dbCommand, "FromAirport", DbType.String, depairport);
                db.AddInParameter(dbCommand, "ToAirport", DbType.String, arrairport);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        pf.GenPer = Conversion.getDecimalOrNull(R["GenPer"]);
                        pf.GenVal = Conversion.getDecimalOrNull(R["GenVal"]);
                        pf.AdlPer = Conversion.getDecimalOrNull(R["AdlPer"]);
                        pf.AdlVal = Conversion.getDecimalOrNull(R["AdlVal"]);
                        pf.ChdG1Per = Conversion.getDecimalOrNull(R["ChdG1Per"]);
                        pf.ChdG1Val = Conversion.getDecimalOrNull(R["ChdG1Val"]);
                        pf.ChdG2Per = Conversion.getDecimalOrNull(R["ChdG2Per"]);
                        pf.ChdG2Val = Conversion.getDecimalOrNull(R["ChdG2Val"]);
                        pf.ChdG3Per = Conversion.getDecimalOrNull(R["ChdG3Per"]);
                        pf.ChdG3Val = Conversion.getDecimalOrNull(R["ChdG3Val"]);
                        pf.Cur = Conversion.getStrOrNull(R["Cur"]);
                        if (pf.GenVal.HasValue && pf.GenVal.Value != 0)
                            pf.GenVal = Equals(pf.Cur, NetCur) ? pf.GenVal.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.GenVal.Value, true, ref errorMsg);
                        if (pf.AdlVal.HasValue && pf.AdlVal.Value != 0)
                            pf.AdlVal = Equals(pf.Cur, NetCur) ? pf.AdlVal.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.AdlVal.Value, true, ref errorMsg);
                        if (pf.ChdG1Val.HasValue && pf.ChdG1Val.Value != 0)
                            pf.ChdG1Val = Equals(pf.Cur, NetCur) ? pf.ChdG1Val.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.ChdG1Val.Value, true, ref errorMsg);
                        if (pf.ChdG2Val.HasValue && pf.ChdG2Val.Value != 0)
                            pf.ChdG2Val = Equals(pf.Cur, NetCur) ? pf.ChdG2Val.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.ChdG2Val.Value, true, ref errorMsg);
                        if (pf.ChdG3Val.HasValue && pf.ChdG3Val.Value != 0)
                            pf.ChdG3Val = Equals(pf.Cur, NetCur) ? pf.ChdG3Val.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.ChdG3Val.Value, true, ref errorMsg);
                    }
                    return pf;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return pf;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Profmars FindCompServProfit(User UserData, string service, DateTime date, int day, int? depcity, int? arrcity, int? country, string NetCur, ref string errorMsg)
        {
            Profmars pf = new Profmars();

            string tsql = @"Select	Top 1 P.RecID, P.Service, P.RuleNoID, R.RuleNo, R.BegDate, R.EndDate, R.HotCat, R.Room, R.Accom, R.Board, 
                                R.FromCity, R.FromAirport, R.ToCity, R.ToAirport, R.Country, R.Location, R.TrfType, R.InsZone, R.VisaType, 
                                R.RentCat, R.MinDay, R.MaxDay, R.GenPer, R.GenVal, R.AdlPer, R.AdlVal,
                                R.ChdG1Per,R.ChdG1Val, R.ChdG2Per, R.ChdG2Val, R.ChdG3Per, R.ChdG3Val, R.Cur
                            From GenProfMar P (NOLOCK)
                            Join ProfRuleDet R on R.RuleNoID=P.RuleNoID and R.Service=P.Service
                            Where ProfType='I' 
	                            and P.Market=@Market 
	                            and P.Service=@Service 
	                            and @FlyDate between BegDate and EndDate
                                and (@Day >= MinDay or MinDay is Null) 
                                and (@Day <= MaxDay or MaxDay is Null)
                                and (FromCity=@FromCity or FromCity is Null) 
                                and (ToCity=@ToCity or ToCity is Null)
                                and (Country=@Country or Country is Null)
                            Order by FromCity Desc,ToCity Desc,EndDate-BegDate,P.RecID
                            ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Service", DbType.String, service);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, date);
                db.AddInParameter(dbCommand, "Day", DbType.Int32, day);
                db.AddInParameter(dbCommand, "FromCity", DbType.Int32, depcity);
                db.AddInParameter(dbCommand, "ToCity", DbType.Int32, arrcity);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, country);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        pf.GenPer = Conversion.getDecimalOrNull(R["GenPer"]);
                        pf.GenVal = Conversion.getDecimalOrNull(R["GenVal"]);
                        pf.AdlPer = Conversion.getDecimalOrNull(R["AdlPer"]);
                        pf.AdlVal = Conversion.getDecimalOrNull(R["AdlVal"]);
                        pf.ChdG1Per = Conversion.getDecimalOrNull(R["ChdG1Per"]);
                        pf.ChdG1Val = Conversion.getDecimalOrNull(R["ChdG1Val"]);
                        pf.ChdG2Per = Conversion.getDecimalOrNull(R["ChdG2Per"]);
                        pf.ChdG2Val = Conversion.getDecimalOrNull(R["ChdG2Val"]);
                        pf.ChdG3Per = Conversion.getDecimalOrNull(R["ChdG3Per"]);
                        pf.ChdG3Val = Conversion.getDecimalOrNull(R["ChdG3Val"]);
                        pf.Cur = Conversion.getStrOrNull(R["Cur"]);
                        if (pf.GenVal.HasValue && pf.GenVal.Value != 0)
                            pf.GenVal = Equals(pf.Cur, NetCur) ? pf.GenVal.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.GenVal.Value, true, ref errorMsg);
                        if (pf.AdlVal.HasValue && pf.AdlVal.Value != 0)
                            pf.AdlVal = Equals(pf.Cur, NetCur) ? pf.AdlVal.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.AdlVal.Value, true, ref errorMsg);
                        if (pf.ChdG1Val.HasValue && pf.ChdG1Val.Value != 0)
                            pf.ChdG1Val = Equals(pf.Cur, NetCur) ? pf.ChdG1Val.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.ChdG1Val.Value, true, ref errorMsg);
                        if (pf.ChdG2Val.HasValue && pf.ChdG2Val.Value != 0)
                            pf.ChdG2Val = Equals(pf.Cur, NetCur) ? pf.ChdG2Val.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.ChdG2Val.Value, true, ref errorMsg);
                        if (pf.ChdG3Val.HasValue && pf.ChdG3Val.Value != 0)
                            pf.ChdG3Val = Equals(pf.Cur, NetCur) ? pf.ChdG3Val.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, pf.Cur, NetCur, pf.ChdG3Val.Value, true, ref errorMsg);
                    }
                    return pf;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return pf;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Int32> getUPriceDays(User UserData, ref string errorMsg)
        {
            List<int> records = new List<int>();
            string tsql = @"
                            Select Distinct Days=Cast(isnull(ArrDate,DepDate)-DepDate As int) From FlightUPrice
                            Where Operator=@Operator
                              And ORT='RT'
                              And [Stop]='N'
                              And Cast(isnull(ArrDate,DepDate)-DepDate As int)>0
                              And DepDate>=GetDate()-1
                            Order By Cast(isnull(ArrDate,DepDate)-DepDate As int)
                           ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, UserData.Operator);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        records.Add(Convert.ToInt32(oReader["Days"]));
                    }
                }
                return records;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightUPriceRecord getFlightUPrice(User UserData, string plOperator, bool? isRT, DateTime depDate, string depFlight, string depClass, DateTime? arrDate, string arrFlight, string arrClass, ref string errorMsg)
        {

            string tsql = string.Empty;
            if (VersionControl.CheckWebVersion(UserData.WebVersion, 43, VersionControl.Equality.gt))
                tsql =
@"
Declare @LastRecord Bit
Declare @MainAgency VarChar(10), @SaleDate DateTime
Set @SaleDate=GetDate()

Select Top 1 @MainAgency = (Case When MainOffice = '' then Code Else MainOffice End)
From Agency (NOLOCK) 
Where Code = @Agency

Set @LastRecord = 1
if @CustomRegID='1195901' Set @LastRecord = 0

Select Top 1 * from FlightUPrice (NOLOCK) P
Join FlightDay (NOLOCK) D ON P.DepFlight=D.FlightNo and P.DepDate=D.FlyDate
Where (Operator=@Operator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID=D.RecID And Operator=@Operator))
  and (Agency=@Agency or Agency='')
  and (AgencyGrp=(Select Groups From GrpAgencyDet where GrpType='U' and Agency=@Agency) or AgencyGrp='')
  and ORT=@ORT 
  and DepDate=@DepDate 
  and DepFlight=@DepFlight 
  and DepClass=@DepClass
  and (ArrDate=@ArrDate or ArrDate is Null) 
  and isnull(ArrFlight,'')=@ArrFlight 
  and isnull(ArrClass,'')=@ArrClass
  and IsNull(Stop,'N')='N'
  and @SaleDate between IsNull(SaleBegDate,@SaleDate) and (case when SaleEndDate is null or dbo.DateOnly(SaleEndDate)=SaleEndDate then IsNull(SaleEndDate,GetDate())+1 else SaleEndDate end)
Order by case when @LastRecord=1 then RecDate else 0 end Desc,Agency Desc,AgencyGrp Desc,RecDate Desc,P.RecID Desc
";
            else
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030400000"))
                    tsql = @"
                        Declare @LastRecord Bit
                        Declare @MainAgency VarChar(10), @SaleDate DateTime
                        Set @SaleDate=GetDate()
                        Select Top 1 @MainAgency = (Case When MainOffice = '' then Code Else MainOffice End) 
                        From Agency (NOLOCK) Where Code = @Agency
                        Set @LastRecord = 1
                        if @CustomRegID='1195901' Set @LastRecord = 0

                        Select Top 1 * from FlightUPrice (NOLOCK)
                        Where Operator=@Operator 
                          and (Agency=@Agency or Agency='')
                          and (AgencyGrp=(Select Groups From GrpAgencyDet where GrpType='U' and Agency=@Agency) or AgencyGrp='')
                          and ORT=@ORT 
                          and DepDate=@DepDate 
                          and DepFlight=@DepFlight 
                          and DepClass=@DepClass
                          and (ArrDate=@ArrDate or ArrDate is Null) 
                          and isnull(ArrFlight,'')=@ArrFlight 
                          and isnull(ArrClass,'')=@ArrClass
                          and IsNull(Stop,'N')='N'
                          and @SaleDate between IsNull(SaleBegDate,@SaleDate) and (case when SaleEndDate is null or dbo.DateOnly(SaleEndDate)=SaleEndDate then IsNull(SaleEndDate,GetDate())+1 else SaleEndDate end)
                        Order by case when @LastRecord=1 then RecDate else 0 end Desc, Agency Desc, AgencyGrp Desc, RecDate Desc
                         ";
                /* Mr. Fatih 2013-02-28 -> from mail
            tsql = @"Declare @MainAgency VarChar(10), @SaleDate DateTime
                     Set @SaleDate=GetDate()
                        Select Top 1 @MainAgency = (Case When MainOffice = '' then Code Else MainOffice End) 
                        From Agency (NOLOCK) Where Code = @Agency
                        Select * from FlightUPrice With (NOLOCK)
                        Where Operator = @Operator                                     
                            And (Agency = @MainAgency Or Agency='') 
                            and (AgencyGrp=(Select Groups From GrpAgencyDet where GrpType= 'U' and Agency = @MainAgency) or AgencyGrp='')
                            And ORT = @ORT 
                            And DepDate = @DepDate 
                            And DepFlight = @DepFlight 
                            And DepClass = @DepClass
                            And (ArrDate is null or ArrDate = @ArrDate) 
                            And isnull(ArrFlight, '') = @ArrFlight 
                            And isnull(ArrClass, '') = @ArrClass
                            And IsNull(Stop, 'N') = 'N'      
                            And @SaleDate between IsNull(SaleBegDate,@SaleDate) and (case when SaleEndDate is null or dbo.DateOnly(SaleEndDate)=SaleEndDate then IsNull(SaleEndDate,GetDate())+1 else SaleEndDate end)
                        --Order by  RecDate Desc, Agency Desc, AgencyGrp Desc --Fatih beyin gazabı
                        Order by Agency Desc, AgencyGrp Desc, RecDate Desc
                        ";
                   */
                else
                    tsql = @"Declare @MainAgency VarChar(10)
                            Select Top 1 @MainAgency = (Case When MainOffice = '' then Code Else MainOffice End) 
                            From Agency (NOLOCK) Where Code = @Agency
                            Select * from FlightUPrice With (NOLOCK)
                            Where Operator = @Operator                                     
                                And (Agency = @MainAgency Or Agency='') 
                                and (AgencyGrp=(Select Groups From GrpAgencyDet where GrpType= 'U' and Agency = @MainAgency) or AgencyGrp='')
                                And ORT = @ORT 
                                And DepDate = @DepDate 
                                And DepFlight = @DepFlight 
                                And DepClass = @DepClass
                                And (ArrDate is null or ArrDate = @ArrDate) 
                                And isnull(ArrFlight, '') = @ArrFlight 
                                And isnull(ArrClass, '') = @ArrClass
                                And IsNull(Stop, 'N') = 'N'                  
                            Order by Agency Desc, AgencyGrp Desc, RecDate Desc ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, plOperator);
                db.AddInParameter(dbCommand, "ORT", DbType.AnsiString, (isRT.HasValue && isRT.Value) ? "RT" : "OW");
                //db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, (string.IsNullOrEmpty(UserData.MainOffice) || !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Qualitum)) ? UserData.AgencyID : UserData.MainOffice);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, string.IsNullOrEmpty(UserData.MainOffice) ? UserData.AgencyID : UserData.MainOffice);
                db.AddInParameter(dbCommand, "DepDate", DbType.DateTime, depDate);
                db.AddInParameter(dbCommand, "DepFlight", DbType.AnsiString, depFlight);
                db.AddInParameter(dbCommand, "DepClass", DbType.AnsiString, depClass);
                db.AddInParameter(dbCommand, "ArrDate", DbType.DateTime, arrDate);
                db.AddInParameter(dbCommand, "ArrFlight", DbType.AnsiString, arrFlight);
                db.AddInParameter(dbCommand, "ArrClass", DbType.AnsiString, arrClass);
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030400000"))
                    db.AddInParameter(dbCommand, "CustomRegID", DbType.AnsiString, UserData.CustomRegID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        FlightUPriceRecord record = new FlightUPriceRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.Agency = Conversion.getStrOrNull(R["Agency"]);
                        record.ORT = Conversion.getStrOrNull(R["ORT"]);
                        record.DepDate = Conversion.getDateTimeOrNull(R["DepDate"]);
                        record.DepFlight = Conversion.getStrOrNull(R["DepFlight"]);
                        record.DepClass = Conversion.getStrOrNull(R["DepClass"]);
                        record.DepPrice = Conversion.getDecimalOrNull(R["DepPrice"]);
                        record.DepInfPrice = Conversion.getDecimalOrNull(R["DepInfPrice"]);
                        record.DepChdPrice = Conversion.getDecimalOrNull(R["DepChdPrice"]);
                        record.ArrDate = Conversion.getDateTimeOrNull(R["ArrDate"]);
                        record.ArrFlight = Conversion.getStrOrNull(R["ArrFlight"]);
                        record.ArrClass = Conversion.getStrOrNull(R["ArrClass"]);
                        record.ArrPrice = Conversion.getDecimalOrNull(R["ArrPrice"]);
                        record.ArrInfPrice = Conversion.getDecimalOrNull(R["ArrInfPrice"]);
                        record.ArrChdPrice = Conversion.getDecimalOrNull(R["ArrChdPrice"]);
                        record.Cur = Conversion.getStrOrNull(R["Cur"]);
                        record.Stop = Conversion.getStrOrNull(R["Stop"]);
                        record.AgencyGrp = Conversion.getStrOrNull(R["AgencyGrp"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightPriceRecord getFlightPrice(string plOperator, DateTime flyDate, string flightNo, string sclass, string blockType, ref string errorMsg)
        {
            string tsql = @"Select *
                            From FlightPrice P (NOLOCK)
                            Join FlightBlock B (NOLOCK) ON B.RecID = P.BlockID                            
                            Where   P.FlyDate=@FlyDate                             
                                And P.FlightNo=@FlightNo 
                                And P.SClass=@Class 
                                And P.Operator=@Operator 
                                And P.BlockType=@BlockType
                            Order By B.SalePriority,P.SalePrice                            
                            ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, plOperator);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, flyDate);
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, flightNo);
                db.AddInParameter(dbCommand, "Class", DbType.String, sclass);
                db.AddInParameter(dbCommand, "BlockType", DbType.String, blockType);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        FlightPriceRecord record = new FlightPriceRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.BlockID = Conversion.getInt32OrNull(R["BlockID"]);
                        record.ParentBlockID = Conversion.getInt32OrNull(R["ParentBlockID"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        record.Buyer = Conversion.getStrOrNull(R["Buyer"]);
                        record.SalePriority = Conversion.getInt16OrNull(R["SalePriority"]);
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        record.FlyDate = Conversion.getDateTimeOrNull(R["FlyDate"]);
                        record.BS = Conversion.getStrOrNull(R["BS"]);
                        record.Seat = Conversion.getInt32OrNull(R["Seat"]);
                        record.SClass = Conversion.getStrOrNull(R["SClass"]);
                        record.BlockType = Conversion.getStrOrNull(R["BlockType"]);
                        record.NetCur = Conversion.getStrOrNull(R["NetCur"]);
                        record.NetPrice = Conversion.getDecimalOrNull(R["NetPrice"]);
                        record.NetChd = Conversion.getDecimalOrNull(R["NetChd"]);
                        record.NetInf = Conversion.getDecimalOrNull(R["NetInf"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.SaleChd = Conversion.getDecimalOrNull(R["SaleChd"]);
                        record.SaleInf = Conversion.getDecimalOrNull(R["SaleInf"]);
                        record.Comment = Conversion.getStrOrNull(R["Comment"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightPriceRecord> getFlightPriceList(User UserData, string plOperator, DateTime flyDate, string flightNo, string sclass, string blockType, ref string errorMsg)
        {
            List<FlightPriceRecord> records = new List<FlightPriceRecord>();
            string tsql = string.Empty;

            if (VersionControl.CheckWebVersion(UserData.WebVersion, 43, VersionControl.Equality.gt))
                tsql =
@"
Select *
From FlightPrice P (NOLOCK)
Join FlightBlock B (NOLOCK) ON B.RecID=P.BlockID
Join FlightDay D (NOLOCK) ON D.FlightNo=P.FlightNo And D.FlyDate=P.FlyDate
Where   P.FlyDate=@FlyDate                             
    And P.FlightNo=@FlightNo 
    And P.SClass=@Class 
    And (P.Operator=@Operator Or Exists(Select RecID From FlightDayOp (Nolock) Where FlightDayID=D.RecID And Operator=@Operator))
    And P.BlockType=@BlockType
    And (Exists(Select RecID From ParamSystem Where CustomRegID='1195901') Or P.BS='B') --2013.03.27 
Order By B.SalePriority,P.SalePrice,P.NetPrice                               
";
            else
                tsql =
@"
Select *
From FlightPrice P (NOLOCK)
Join FlightBlock B (NOLOCK) ON B.RecID=P.BlockID                            
Where   P.FlyDate=@FlyDate                             
    And P.FlightNo=@FlightNo 
    And P.SClass=@Class 
    And P.Operator=@Operator 
    And P.BlockType=@BlockType
    And (Exists(Select RecID From ParamSystem Where CustomRegID='1195901') Or P.BS='B') --2013.03.27 
Order By B.SalePriority,P.SalePrice,P.NetPrice                            
";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, plOperator);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, flyDate);
                db.AddInParameter(dbCommand, "FlightNo", DbType.AnsiString, flightNo);
                db.AddInParameter(dbCommand, "Class", DbType.AnsiString, sclass);
                db.AddInParameter(dbCommand, "BlockType", DbType.AnsiString, blockType);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        FlightPriceRecord record = new FlightPriceRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.BlockID = Conversion.getInt32OrNull(R["BlockID"]);
                        record.ParentBlockID = Conversion.getInt32OrNull(R["ParentBlockID"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        record.Buyer = Conversion.getStrOrNull(R["Buyer"]);
                        record.SalePriority = Conversion.getInt16OrNull(R["SalePriority"]);
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        record.FlyDate = Conversion.getDateTimeOrNull(R["FlyDate"]);
                        record.BS = Conversion.getStrOrNull(R["BS"]);
                        record.Seat = Conversion.getInt32OrNull(R["Seat"]);
                        record.SClass = Conversion.getStrOrNull(R["SClass"]);
                        record.BlockType = Conversion.getStrOrNull(R["BlockType"]);
                        record.NetCur = Conversion.getStrOrNull(R["NetCur"]);
                        record.NetPrice = Conversion.getDecimalOrNull(R["NetPrice"]);
                        record.NetChd = Conversion.getDecimalOrNull(R["NetChd"]);
                        record.NetInf = Conversion.getDecimalOrNull(R["NetInf"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.SaleChd = Conversion.getDecimalOrNull(R["SaleChd"]);
                        record.SaleInf = Conversion.getDecimalOrNull(R["SaleInf"]);
                        record.Comment = Conversion.getStrOrNull(R["Comment"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OnlyFlight_FlightInfo> WriteToFlightInfoRow(User UserData, FlightData flightData, OnlyTicket_Flight drSearch, bool otherCurr, string currency, ref string errorMsg)
        {
            List<OnlyFlight_FlightInfo> dtFlightInfo = flightData.FlightInfo;
            #region Defination

            bool stopSale = false;
            bool? useOnlyUnionPrice = new OnlyTickets().getUseOnlyUnionPrice(UserData, ref errorMsg);
            Int16? saleCalcType = UserData.TvParams.TvParamPrice.SaleCalcType;
            DateTime? arrDate = null;
            string arrFlight = string.Empty;
            string arrClass = string.Empty;
            DateTime depDate = drSearch.DepDate.Value;
            string depFlight = drSearch.DepFlight;
            string depClass = drSearch.DepClass;
            string depSupp = string.Empty;
            string arrSupp = string.Empty;
            string blockType = "H";
            Int16 night = 0;
            if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
            {
                arrDate = drSearch.ArrDate.Value;
                arrFlight = drSearch.ArrFlight;
                arrClass = drSearch.ArrClass;
                night = Convert.ToInt16((arrDate.Value - depDate).Days.ToString());
            }

            string saleCurr = string.Empty;
            if (!otherCurr)
                saleCurr = currency;
            else
                saleCurr = UserData.SaleCur;
            //drSearch.Market
            saleCurr = currency; // attention
            string currMarket = UserData.Market;
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                currMarket = drSearch.Market;

            decimal? depAdlFlight = 0;
            decimal? depChdFlight = 0;
            decimal? depInfFlight = 0;
            decimal? arrAdlFlight = 0;
            decimal? arrChdFlight = 0;
            decimal? arrInfFlight = 0;
            #endregion

            FlightDayRecord _depFlight = new Flights().getFlightDay(UserData, depFlight, depDate, ref errorMsg);
            if (_depFlight.DepTime.HasValue && _depFlight.DepTime.Value.Ticks > 0)
            {
                long countClose = _depFlight.CountCloseTime.HasValue ? (_depFlight.CountCloseTime.Value.Hour * 60 + _depFlight.CountCloseTime.Value.Minute) : 0;
                long saleRelease = _depFlight.SaleRelease.HasValue ? (_depFlight.SaleRelease.Value.Hour * 60 + _depFlight.SaleRelease.Value.Minute) : 0;
                long depTime = _depFlight.DepTime.HasValue ? (_depFlight.DepTime.Value.Hour * 60 + _depFlight.DepTime.Value.Minute) : 0;
                long release = depTime - countClose - saleRelease;
                DateTime releaseTime = _depFlight.FlyDate.AddMinutes(release);
                if ((releaseTime.Date >= _depFlight.FlyDate) && (releaseTime < DateTime.Now))
                    return dtFlightInfo;
            }

            OnlyFlight_FlightInfo drNew = new OnlyFlight_FlightInfo();

            Int16? dayallot = 0;
            if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
                dayallot = drSearch.Days;
            bool isTicketRest = false;
            bool makeResDep = true;
            bool makeResRet = true;
            int a = 0, b = 1, c = 1; //kullanılmıyor sadece referans göndermek için. res yapılırken kullanılıyor.
            int isopt = 0;

            drNew.AllotType = blockType;
            if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
                drNew.Days = drSearch.Days;

            //FlightPriceRecord dtFlightPrice = new FlightPriceRecord();
            List<FlightPriceRecord> dtFlightPriceList = new List<FlightPriceRecord>();
            FlightUPriceRecord dtFlightUPrice = new FlightUPriceRecord();

            dtFlightUPrice = new OnlyTickets().getFlightUPrice(UserData, drSearch.Operator, drSearch.IsRT, depDate, depFlight, depClass, arrDate, arrFlight, arrClass, ref errorMsg);

            if (dtFlightUPrice != null)
            {

                #region U Price
                FlightPriceRecord dtFlightPrice = new FlightPriceRecord();

                if (string.IsNullOrEmpty(saleCurr))
                    saleCurr = dtFlightUPrice.Cur;
                depAdlFlight = Equals(saleCurr, dtFlightUPrice.Cur) ? dtFlightUPrice.DepPrice.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightUPrice.Cur, saleCurr, dtFlightUPrice.DepPrice.Value, true, ref errorMsg);
                depChdFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightUPrice.Cur, saleCurr, dtFlightUPrice.DepChdPrice.HasValue ? dtFlightUPrice.DepChdPrice.Value : dtFlightUPrice.DepPrice.Value, true, ref errorMsg);
                depInfFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightUPrice.Cur, saleCurr, dtFlightUPrice.DepInfPrice.HasValue ? dtFlightUPrice.DepInfPrice.Value : dtFlightUPrice.DepPrice.Value, true, ref errorMsg);
                drNew.DepPriceIsUnion = true;
                if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
                {
                    arrAdlFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightUPrice.Cur, saleCurr, dtFlightUPrice.ArrPrice.Value, true, ref errorMsg);
                    arrChdFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightUPrice.Cur, saleCurr, dtFlightUPrice.ArrChdPrice.HasValue ? dtFlightUPrice.ArrChdPrice.Value : dtFlightUPrice.ArrPrice.Value, true, ref errorMsg);
                    arrInfFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightUPrice.Cur, saleCurr, dtFlightUPrice.ArrInfPrice.HasValue ? dtFlightUPrice.ArrInfPrice.Value : dtFlightUPrice.ArrPrice.Value, true, ref errorMsg);
                    drNew.RetPriceIsUnion = true;
                }

                blockType = "H";
                dtFlightPriceList = new OnlyTickets().getFlightPriceList(UserData, drSearch.Operator, depDate, depFlight, depClass, blockType, ref errorMsg);
                if (dtFlightPriceList == null || dtFlightPriceList.Count < 1)
                {
                    blockType = "S";
                    dtFlightPriceList = new OnlyTickets().getFlightPriceList(UserData, drSearch.Operator, depDate, depFlight, depClass, blockType, ref errorMsg);
                }
                if (dtFlightPriceList != null && dtFlightPriceList.Count > 0)
                {
                    foreach (FlightPriceRecord dtFlightPriceRow in dtFlightPriceList)
                    {
                        isTicketRest = false;
                        makeResDep = true;

                        depSupp = dtFlightPriceRow.Supplier;
                        Int16? depFreeSeat = new OnlyTickets().GetFreeAllot(UserData, drSearch, drSearch.IsRT.HasValue && drSearch.IsRT.Value, depFlight, depDate, depFlight, arrFlight, depClass, depSupp, depDate, (drSearch.IsRT.HasValue && drSearch.IsRT.Value) ? arrDate : null, depClass, dayallot, ref isTicketRest, ref makeResDep, ref isopt, ref a, ref b, ref c, ref errorMsg, blockType);
                        if (((depFreeSeat.HasValue && depFreeSeat.Value > 0) || dtFlightPriceList.Count == 1) && makeResDep)
                        {
                            drNew.DepFreeSeat = depFreeSeat;
                            drNew.DepSupp = depSupp;
                            break;
                        }
                    }
                }

                if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
                {
                    blockType = "H";
                    dtFlightPriceList = new OnlyTickets().getFlightPriceList(UserData, drSearch.Operator, arrDate.Value, arrFlight, arrClass, blockType, ref errorMsg);
                    if (dtFlightPriceList == null || dtFlightPriceList.Count < 1)
                    {
                        blockType = "S";
                        dtFlightPriceList = new OnlyTickets().getFlightPriceList(UserData, drSearch.Operator, arrDate.Value, arrFlight, arrClass, blockType, ref errorMsg);
                    }
                    if (dtFlightPriceList != null && dtFlightPriceList.Count > 0)
                    {
                        foreach (FlightPriceRecord dtFlightPriceRow in dtFlightPriceList)
                        {
                            isTicketRest = false;
                            makeResRet = true;
                            arrSupp = dtFlightPriceRow.Supplier;
                            Int16? arrFreeSeat = GetFreeAllot(UserData, drSearch, drSearch.IsRT.HasValue && drSearch.IsRT.Value, arrFlight, arrDate.Value, depFlight, arrFlight, arrClass, arrSupp, depDate, arrDate, depClass, dayallot, ref isTicketRest, ref makeResRet, ref isopt, ref a, ref b, ref c, ref errorMsg, blockType);
                            if (((arrFreeSeat.HasValue && arrFreeSeat.Value > 0) || dtFlightPriceList.Count == 1) && makeResRet)
                            {
                                drNew.ArrFreeSeat = arrFreeSeat;
                                drNew.ArrSupp = arrSupp;
                                break;
                            }
                        }
                    }
                }
                if (drNew.DepFreeSeat.HasValue && drNew.ArrFreeSeat.HasValue)
                    drNew.FreeSeat = (drNew.ArrFreeSeat < drNew.DepFreeSeat) ? drNew.ArrFreeSeat : drNew.DepFreeSeat;
                else if (drNew.DepFreeSeat.HasValue)
                    drNew.FreeSeat = drNew.DepFreeSeat.Value;
                #endregion U Price
            }
            else
                //if (!(string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mahbal)))
                if (useOnlyUnionPrice.HasValue == false || (useOnlyUnionPrice.HasValue && useOnlyUnionPrice.Value == false))
                {
                    drNew.DepPriceIsUnion = false;
                    drNew.RetPriceIsUnion = false;

                    #region Price
                    blockType = "H";
                    //dtFlightPrice = new OnlyTickets().getFlightPrice(drSearch.Operator, depDate, depFlight, depClass, blockType, ref errorMsg); // For more than one supplier
                    dtFlightPriceList = new OnlyTickets().getFlightPriceList(UserData, drSearch.Operator, depDate, depFlight, depClass, blockType, ref errorMsg);

                    if (dtFlightPriceList == null || dtFlightPriceList.Count < 1)
                    {
                        blockType = "S";
                        //dtFlightPrice = new OnlyTickets().getFlightPrice(drSearch.Operator, depDate, depFlight, depClass, blockType, ref errorMsg);
                        dtFlightPriceList = new OnlyTickets().getFlightPriceList(UserData, drSearch.Operator, depDate, depFlight, depClass, blockType, ref errorMsg);
                    }

                    if (dtFlightPriceList != null && dtFlightPriceList.Count > 0) // Multiple Supplier
                    {
                        foreach (FlightPriceRecord dtFlightPrice in dtFlightPriceList)
                        {
                            isTicketRest = false;
                            makeResDep = true;
                            if (string.IsNullOrEmpty(saleCurr))
                                saleCurr = dtFlightPrice.NetCur;
                            Int16? depFreeSeat = null;
                            depFreeSeat = new OnlyTickets().GetFreeAllot(UserData, drSearch, drSearch.IsRT.HasValue && drSearch.IsRT.Value, depFlight, depDate, depFlight, arrFlight, depClass, dtFlightPrice.Supplier/* depSupp*/, depDate, (drSearch.IsRT.HasValue && drSearch.IsRT.Value) ? arrDate : null, depClass, dayallot, ref isTicketRest, ref makeResDep, ref isopt, ref a, ref b, ref c, ref errorMsg, blockType);
                            if (((depFreeSeat.HasValue && depFreeSeat.Value > 0) || dtFlightPriceList.Count == 1) && makeResDep)
                            {
                                drNew.DepFreeSeat = depFreeSeat;
                                drNew.FreeSeat = drNew.DepFreeSeat;
                                depSupp = dtFlightPrice.Supplier;
                                drNew.DepSupp = depSupp;
                                Profmars pfr = new Profmars();
                                if (!dtFlightPrice.SalePrice.HasValue)
                                {
                                    int day = 0;
                                    if (arrDate.HasValue)
                                        day = arrDate.Value.Subtract(depDate).Days;

                                    pfr = FindFlightProfit(UserData, depFlight, depDate, day, dtFlightPrice.NetCur, ref errorMsg);
                                    if (!string.IsNullOrEmpty(errorMsg))
                                        return dtFlightInfo;
                                }

                                if (dtFlightPrice.SalePrice.HasValue)
                                    depAdlFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.SaleCur, saleCurr, dtFlightPrice.SalePrice.Value, true, ref errorMsg);
                                else
                                    depAdlFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.NetCur, saleCurr, CalcPrice2(dtFlightPrice.NetPrice.Value, pfr, 1, saleCalcType), true, ref errorMsg);
                                if (!string.IsNullOrEmpty(errorMsg))
                                    return dtFlightInfo;

                                if (dtFlightPrice.SaleChd.HasValue)
                                    depChdFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.SaleCur, saleCurr, dtFlightPrice.SaleChd.Value, true, ref errorMsg);
                                else
                                    depChdFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.NetCur, saleCurr, CalcPrice2(dtFlightPrice.NetChd.Value, pfr, 3, saleCalcType), true, ref errorMsg);
                                if (!string.IsNullOrEmpty(errorMsg))
                                    return dtFlightInfo;

                                if (dtFlightPrice.SaleInf.HasValue)
                                    depInfFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.SaleCur, saleCurr, dtFlightPrice.SaleInf.Value, true, ref errorMsg);
                                else
                                    depInfFlight = new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.NetCur, saleCurr, CalcPrice2(dtFlightPrice.NetInf.Value, pfr, 2, saleCalcType), true, ref errorMsg);
                                //if (!string.IsNullOrEmpty(errorMsg))
                                //    return dtFlightInfo;
                                if (string.IsNullOrEmpty(errorMsg) && depAdlFlight.HasValue && depAdlFlight.Value > 0)
                                    break;
                            }
                        }
                    }

                    if (drSearch.IsRT.HasValue && drSearch.IsRT.Value && depAdlFlight > 0 && drNew.DepFreeSeat.HasValue)
                    {
                        blockType = "H";
                        //dtFlightPrice = new OnlyTickets().getFlightPrice(drSearch.Operator, arrDate.Value, arrFlight, arrClass, blockType, ref errorMsg);
                        dtFlightPriceList = new OnlyTickets().getFlightPriceList(UserData, drSearch.Operator, arrDate.Value, arrFlight, arrClass, blockType, ref errorMsg);

                        if (dtFlightPriceList == null || dtFlightPriceList.Count < 1)
                        {
                            blockType = "S";
                            dtFlightPriceList = new OnlyTickets().getFlightPriceList(UserData, drSearch.Operator, arrDate.Value, arrFlight, arrClass, blockType, ref errorMsg);
                        }
                        if (dtFlightPriceList != null && dtFlightPriceList.Count > 0) // Multiple Supplier
                        {
                            foreach (FlightPriceRecord dtFlightPrice in dtFlightPriceList)
                            {
                                if (string.IsNullOrEmpty(saleCurr))
                                    saleCurr = dtFlightPrice.NetCur;

                                isTicketRest = false;
                                makeResRet = true;

                                Int16? retFreeSeat = null;
                                retFreeSeat = GetFreeAllot(UserData, drSearch, drSearch.IsRT.HasValue && drSearch.IsRT.Value, arrFlight, arrDate.Value, depFlight, arrFlight, arrClass, dtFlightPrice.Supplier/*arrSupp*/, depDate, arrDate, depClass, dayallot, ref isTicketRest, ref makeResRet, ref isopt, ref a, ref b, ref c, ref errorMsg, blockType);
                                if (((retFreeSeat.HasValue && retFreeSeat.Value > 0) || dtFlightPriceList.Count == 1) && makeResRet)
                                {
                                    //if (dtFlightPrice != null)
                                    //{
                                    drNew.ArrFreeSeat = retFreeSeat;
                                    drNew.FreeSeat = (drNew.ArrFreeSeat < drNew.DepFreeSeat) ? drNew.ArrFreeSeat : drNew.DepFreeSeat;
                                    arrSupp = dtFlightPrice.Supplier;
                                    drNew.ArrSupp = arrSupp;
                                    Profmars pfr = new Profmars();
                                    if (!dtFlightPrice.SalePrice.HasValue)
                                    {
                                        int day = 0;
                                        if (arrDate.HasValue)
                                            day = (arrDate.Value).Subtract(depDate).Days;
                                        pfr = FindFlightProfit(UserData, arrFlight, arrDate.Value, day, dtFlightPrice.NetCur, ref errorMsg);
                                        if (!string.IsNullOrEmpty(errorMsg))
                                            return dtFlightInfo;
                                    }

                                    if (dtFlightPrice.SalePrice.HasValue)
                                        arrAdlFlight = Equals(dtFlightPrice.SaleCur, saleCurr) ? dtFlightPrice.SalePrice.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.SaleCur, saleCurr, dtFlightPrice.SalePrice.Value, true, ref errorMsg);
                                    else
                                        arrAdlFlight = Equals(dtFlightPrice.NetCur, saleCurr) ? CalcPrice2(dtFlightPrice.NetPrice.Value, pfr, 1, saleCalcType) : new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.NetCur, saleCurr, CalcPrice2(dtFlightPrice.NetPrice.Value, pfr, 1, saleCalcType), true, ref errorMsg);
                                    if (!string.IsNullOrEmpty(errorMsg))
                                        return dtFlightInfo;

                                    if (dtFlightPrice.SaleChd.HasValue)
                                        arrChdFlight = Equals(dtFlightPrice.SaleCur, saleCurr) ? dtFlightPrice.SaleChd.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.SaleCur, saleCurr, dtFlightPrice.SaleChd.Value, true, ref errorMsg);
                                    else
                                        arrChdFlight = Equals(dtFlightPrice.NetCur, saleCurr) ? CalcPrice2(dtFlightPrice.NetChd.Value, pfr, 3, saleCalcType) : new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.NetCur, saleCurr, CalcPrice2(dtFlightPrice.NetChd.Value, pfr, 3, saleCalcType), true, ref errorMsg);
                                    if (!string.IsNullOrEmpty(errorMsg))
                                        return dtFlightInfo;

                                    if (dtFlightPrice.SaleInf.HasValue)
                                        arrInfFlight = Equals(dtFlightPrice.SaleCur, saleCurr) ? dtFlightPrice.SaleInf.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, dtFlightPrice.SaleCur, saleCurr, dtFlightPrice.SaleInf.Value, true, ref errorMsg);
                                    else
                                        arrInfFlight = Equals(dtFlightPrice.NetCur, saleCurr) ? CalcPrice2(dtFlightPrice.NetInf.Value, pfr, 2, saleCalcType) : new TvBo.Common().Exchange(currency, DateTime.Today, dtFlightPrice.NetCur, saleCurr, CalcPrice2(dtFlightPrice.NetInf.Value, pfr, 2, saleCalcType), true, ref errorMsg);
                                    if (!string.IsNullOrEmpty(errorMsg))
                                        return dtFlightInfo;
                                    //}
                                }

                                if (string.IsNullOrEmpty(errorMsg) && arrAdlFlight.HasValue && arrAdlFlight.Value > 0)
                                    break;
                            }
                        }
                    }
                    if (drNew.DepFreeSeat.HasValue && drNew.ArrFreeSeat.HasValue)
                        drNew.FreeSeat = (drNew.ArrFreeSeat < drNew.DepFreeSeat) ? drNew.ArrFreeSeat : drNew.DepFreeSeat;
                    else if (drNew.DepFreeSeat.HasValue)
                        drNew.FreeSeat = drNew.DepFreeSeat.Value;
                    #endregion Price
                }
                else
                    return dtFlightInfo;


            if (!(depAdlFlight.HasValue && depAdlFlight.Value > 0 && drNew.DepFreeSeat.HasValue))
                return dtFlightInfo;

            drNew.RecID = dtFlightInfo != null && dtFlightInfo.Count > 0 ? (dtFlightInfo.Last().RecID.HasValue ? dtFlightInfo.Last().RecID.Value + 1 : 1) : 1;
            //drNew.AllotType = blockType;

            drNew.IsNext = drSearch.IsNext;

            //if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
            //{
            //    //drNew.Days = drSearch.Days;
            //    drNew.ArrFreeSeat = GetFreeAllot(UserData, drSearch, arrFlight, arrDate.Value, depFlight, arrFlight, arrClass, arrSupp, depDate, arrDate, depClass, dayallot, ref isTicketRest, ref makeResRet, ref isopt, ref a, ref b, ref c, ref errorMsg, blockType);
            //    drNew.FreeSeat = (drNew.ArrFreeSeat < drNew.DepFreeSeat) ? drNew.ArrFreeSeat : drNew.DepFreeSeat;
            //}
            drNew.AllowRes = (makeResDep && makeResRet) ? true : false;

            if (!(makeResDep && makeResRet))
                return dtFlightInfo;

            List<OnlyFlight_ServiceExtra> dtAddExtra = flightData.ServiceExtra;

            if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                dtAddExtra = new OnlyTickets().CalculateExtra(UserData, drSearch.Market, saleCurr, flightData, dtAddExtra, "Departure", depFlight, depDate, depClass, drNew.RecID.Value, night, ref errorMsg);
            if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
                if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    dtAddExtra = new OnlyTickets().CalculateExtra(UserData, drSearch.Market, saleCurr, flightData, dtAddExtra, "Arrival", arrFlight, arrDate.Value, arrClass, drNew.RecID.Value, night, ref errorMsg);


            List<OnlyFlight_AdServices> additionalCompServDep = new OnlyTickets().getAdditionalComplusoryServices(UserData, new Locations().getLocationForCountry(_depFlight.ArrCity, ref errorMsg), _depFlight.ArrCity, depDate, night, 1, ref errorMsg);
            List<OnlyFlight_AdServices> additionalCompServRet = new List<OnlyFlight_AdServices>();
            if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
                additionalCompServRet = new OnlyTickets().getAdditionalComplusoryServices(UserData, new Locations().getLocationForCountry(_depFlight.DepCity, ref errorMsg), _depFlight.DepCity, depDate, night, 1, ref errorMsg);


            #region Defination
            decimal? exDNetAdl = 0;
            decimal? exDNetChd = 0;
            decimal? exDNetInf = 0;
            decimal? exDSaleAdl = 0;
            decimal? exDSaleChd = 0;
            decimal? exDSaleInf = 0;
            decimal? exANetAdl = 0;
            decimal? exANetChd = 0;
            decimal? exANetInf = 0;
            decimal? exASaleAdl = 0;
            decimal? exASaleChd = 0;
            decimal? exASaleInf = 0;
            #endregion

            #region CompResService
            decimal? depAdlComp = 0;
            decimal? depChdComp = 0;
            decimal? depInfComp = 0;
            decimal? retAdlComp = 0;
            decimal? retChdComp = 0;
            decimal? retInfComp = 0;

            List<OnlyFlight_AdditionalService> compServices = flightData.CompService;
            if (additionalCompServDep != null && additionalCompServDep.Count > 0)
            {
                foreach (OnlyFlight_AdServices row in additionalCompServDep)
                {
                    OnlyFlight_AdditionalService compServ = new OnlyTickets().getAdditionalComplusoryService(UserData, row.Service, row.Code, new Locations().getLocationForCountry(_depFlight.ArrCity, ref errorMsg), _depFlight.ArrCity, depDate, night, 1, ref errorMsg);
                    if (compServ != null)
                    {
                        if (string.IsNullOrEmpty(saleCurr))
                            saleCurr = compServ.NetCur;
                        if (!compServ.ExtraSale.HasValue) // profit
                        {
                            Profmars pfr = new Profmars();
                            int day = 0;
                            if (arrDate.HasValue)
                                day = arrDate.Value.Subtract(depDate).Days;
                            pfr = FindCompServProfit(UserData, compServ.MainService, depDate, day, flightData.FlightInfo.FirstOrDefault().RouteFrom, flightData.FlightInfo.FirstOrDefault().RouteTo, new Locations().getLocationForCountry(_depFlight.DepCity, ref errorMsg), compServ.NetCur, ref errorMsg);
                            if (string.IsNullOrEmpty(errorMsg))
                            {
                                depAdlComp += Equals(saleCurr, compServ.NetCur) ? CalcPrice2(compServ.ExtraNet.Value, pfr, 1, saleCalcType) : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.NetCur, saleCurr, CalcPrice2(compServ.ExtraNet.Value, pfr, 1, saleCalcType), true, ref errorMsg);
                                if (compServ.ExtraChdNet.HasValue && compServ.ExtraChdNet.Value != 0)
                                    depChdComp += Equals(saleCurr, compServ.NetCur) ? CalcPrice2(compServ.ExtraChdNet.Value, pfr, 1, saleCalcType) : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.NetCur, saleCurr, CalcPrice2(compServ.ExtraChdNet.Value, pfr, 1, saleCalcType), true, ref errorMsg);
                                if (compServ.ExtraInfNet.HasValue && compServ.ExtraInfNet.Value != 0)
                                    depInfComp += Equals(saleCurr, compServ.NetCur) ? CalcPrice2(compServ.ExtraInfNet.Value, pfr, 1, saleCalcType) : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.NetCur, saleCurr, CalcPrice2(compServ.ExtraInfNet.Value, pfr, 1, saleCalcType), true, ref errorMsg);
                                compServices.Add(compServ);
                            }
                        }
                        else
                        {
                            depAdlComp += Equals(saleCurr, compServ.SaleCur) ? compServ.ExtraSale.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.SaleCur, saleCurr, compServ.ExtraSale.Value, true, ref errorMsg);
                            if (compServ.ExtraChdSale.HasValue && compServ.ExtraChdSale.Value != 0)
                                depChdComp += Equals(saleCurr, compServ.SaleCur) ? compServ.ExtraChdSale.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.SaleCur, saleCurr, compServ.ExtraChdSale.Value, true, ref errorMsg);
                            if (compServ.ExtraInfSale.HasValue && compServ.ExtraInfSale.Value != 0)
                                depInfComp += Equals(saleCurr, compServ.SaleCur) ? compServ.ExtraInfSale.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.SaleCur, saleCurr, compServ.ExtraInfSale.Value, true, ref errorMsg);
                            compServices.Add(compServ);
                        }
                    }
                }
            }
            if (additionalCompServRet != null && additionalCompServRet.Count > 0)
            {
                foreach (OnlyFlight_AdServices row in additionalCompServRet)
                {
                    OnlyFlight_AdditionalService compServ = new OnlyTickets().getAdditionalComplusoryService(UserData, row.Service, row.Code, new Locations().getLocationForCountry(_depFlight.ArrCity, ref errorMsg), _depFlight.ArrCity, arrDate, night, 1, ref errorMsg);
                    if (compServ != null)
                    {
                        if (string.IsNullOrEmpty(saleCurr))
                            saleCurr = compServ.NetCur;
                        if (!compServ.ExtraSale.HasValue) // profit
                        {
                            Profmars pfr = new Profmars();
                            int day = 0;
                            if (arrDate.HasValue)
                                day = arrDate.Value.Subtract(depDate).Days;
                            pfr = FindCompServProfit(UserData, compServ.MainService, depDate.AddDays(day), day, flightData.FlightInfo.FirstOrDefault().RouteFrom, flightData.FlightInfo.FirstOrDefault().RouteTo, new Locations().getLocationForCountry(_depFlight.ArrCity, ref errorMsg), compServ.NetCur, ref errorMsg);
                            if (string.IsNullOrEmpty(errorMsg))
                            {
                                retAdlComp += Equals(saleCurr, compServ.NetCur) ? CalcPrice2(compServ.ExtraNet.Value, pfr, 1, saleCalcType) : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.NetCur, saleCurr, CalcPrice2(compServ.ExtraNet.Value, pfr, 1, saleCalcType), true, ref errorMsg);
                                if (compServ.ExtraChdNet.HasValue && compServ.ExtraChdNet.Value != 0)
                                    retChdComp += Equals(saleCurr, compServ.NetCur) ? CalcPrice2(compServ.ExtraChdNet.Value, pfr, 1, saleCalcType) : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.NetCur, saleCurr, CalcPrice2(compServ.ExtraChdNet.Value, pfr, 1, saleCalcType), true, ref errorMsg);
                                if (compServ.ExtraInfNet.HasValue && compServ.ExtraInfNet.Value != 0)
                                    retInfComp += Equals(saleCurr, compServ.NetCur) ? CalcPrice2(compServ.ExtraInfNet.Value, pfr, 1, saleCalcType) : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.NetCur, saleCurr, CalcPrice2(compServ.ExtraInfNet.Value, pfr, 1, saleCalcType), true, ref errorMsg);
                                compServices.Add(compServ);
                            }
                        }
                        else
                        {
                            retAdlComp += Equals(saleCurr, compServ.SaleCur) ? compServ.ExtraSale.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.SaleCur, saleCurr, compServ.ExtraSale.Value, true, ref errorMsg);
                            if (compServ.ExtraChdSale.HasValue && compServ.ExtraChdSale.Value != 0)
                                retChdComp += Equals(saleCurr, compServ.SaleCur) ? compServ.ExtraChdSale.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.SaleCur, saleCurr, compServ.ExtraChdSale.Value, true, ref errorMsg);
                            if (compServ.ExtraInfSale.HasValue && compServ.ExtraInfSale.Value != 0)
                                retInfComp += Equals(saleCurr, compServ.SaleCur) ? compServ.ExtraInfSale.Value : new TvBo.Common().Exchange(currMarket, DateTime.Today, compServ.SaleCur, saleCurr, compServ.ExtraInfSale.Value, true, ref errorMsg);
                            compServices.Add(compServ);
                        }
                    }
                }
            }


            #endregion CompResService

            #region Extra's
            if (dtAddExtra != null && dtAddExtra.Count > 0)
            {
                List<OnlyFlight_ServiceExtra> drsExt = dtAddExtra.Where(w => w.RecID == drNew.RecID).Select(s => s).ToList<OnlyFlight_ServiceExtra>();
                foreach (OnlyFlight_ServiceExtra dr in drsExt)
                {
                    if (string.IsNullOrEmpty(saleCurr))
                        saleCurr = dr.NetCur;
                    if (Equals(dr.ArrDep, "Departure"))
                    {
                        decimal? _extAdlNet = Equals(dr.NetCur, saleCurr) ? (dr.ExtraNet.HasValue ? dr.ExtraNet.Value : 0) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.NetCur, saleCurr, dr.ExtraNet.Value, false, ref errorMsg);
                        exDNetAdl = exDNetAdl + (_extAdlNet.HasValue ? _extAdlNet.Value : 0);
                        exDNetChd = exDNetChd +
                            (Equals(dr.NetCur, saleCurr) ? (dr.ExtraChdNet.HasValue ? dr.ExtraChdNet.Value : (_extAdlNet.HasValue ? _extAdlNet.Value : 0)) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.NetCur, saleCurr, dr.ExtraChdNet.HasValue ? dr.ExtraChdNet.Value : (_extAdlNet.HasValue ? _extAdlNet.Value : 0), false, ref errorMsg));
                        exDNetInf = exDNetInf +
                            (Equals(dr.NetCur, saleCurr) ? (dr.ExtraInfNet.HasValue ? dr.ExtraInfNet.Value : (_extAdlNet.HasValue ? _extAdlNet.Value : 0)) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.NetCur, saleCurr, dr.ExtraInfNet.HasValue ? dr.ExtraInfNet.Value : (_extAdlNet.HasValue ? _extAdlNet.Value : 0), false, ref errorMsg));


                        decimal? _extDAdlSale = Equals(dr.SaleCur, saleCurr) ? (dr.ExtraSale.HasValue ? dr.ExtraSale.Value : 0) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.SaleCur, saleCurr, dr.ExtraSale.Value, true, ref errorMsg);
                        exDSaleAdl = exDSaleAdl + (_extDAdlSale.HasValue ? _extDAdlSale.Value : 0);
                        exDSaleChd = exDSaleChd +
                            (Equals(dr.SaleCur, saleCurr) ? (dr.ExtraChdSale.HasValue ? dr.ExtraChdSale.Value : (_extDAdlSale.HasValue ? _extDAdlSale.Value : 0)) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.SaleCur, saleCurr, dr.ExtraChdSale.HasValue ? dr.ExtraChdSale.Value : (_extDAdlSale.HasValue ? _extDAdlSale.Value : 0), true, ref errorMsg));
                        exDSaleInf = exDSaleInf +
                            (Equals(dr.SaleCur, saleCurr) ? (dr.ExtraInfSale.HasValue ? dr.ExtraInfSale.Value : (_extDAdlSale.HasValue ? _extDAdlSale.Value : 0)) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.SaleCur, saleCurr, dr.ExtraInfSale.HasValue ? dr.ExtraInfSale.Value : (_extDAdlSale.HasValue ? _extDAdlSale.Value : 0), true, ref errorMsg));
                    }
                    else
                    {
                        decimal? _extAdlNet = Equals(dr.NetCur, saleCurr) ? (dr.ExtraNet.HasValue ? dr.ExtraNet.Value : 0) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.NetCur, saleCurr, dr.ExtraNet.Value, false, ref errorMsg);
                        exANetAdl = exANetAdl + (_extAdlNet.HasValue ? _extAdlNet.Value : 0);
                        //(Equals(dr.NetCur, saleCurr) ? dr.ExtraNet.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.NetCur, saleCurr, dr.ExtraNet.Value, false, ref errorMsg));
                        exANetChd = exANetChd +
                            (Equals(dr.NetCur, saleCurr) ? (dr.ExtraChdNet.HasValue ? dr.ExtraChdNet.Value : (_extAdlNet.HasValue ? _extAdlNet.Value : 0)) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.NetCur, saleCurr, dr.ExtraChdNet.HasValue ? dr.ExtraChdNet.Value : (_extAdlNet.HasValue ? _extAdlNet.Value : 0), false, ref errorMsg));
                        //(Equals(dr.NetCur, saleCurr) ? (dr.ExtraChdNet.HasValue ? dr.ExtraChdNet.Value : dr.ExtraNet.Value) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.NetCur, saleCurr, dr.ExtraChdNet.HasValue ? dr.ExtraChdNet.Value : dr.ExtraNet.Value, false, ref errorMsg));
                        exANetInf = exANetInf +
                            (Equals(dr.NetCur, saleCurr) ? (dr.ExtraInfNet.HasValue ? dr.ExtraInfNet.Value : (_extAdlNet.HasValue ? _extAdlNet.Value : 0)) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.NetCur, saleCurr, dr.ExtraInfNet.HasValue ? dr.ExtraInfNet.Value : (_extAdlNet.HasValue ? _extAdlNet.Value : 0), false, ref errorMsg));
                        //(Equals(dr.NetCur, saleCurr) ? (dr.ExtraInfNet.HasValue ? dr.ExtraInfNet.Value : dr.ExtraNet.Value) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.NetCur, saleCurr, dr.ExtraInfNet.HasValue ? dr.ExtraInfNet.Value : dr.ExtraNet.Value, false, ref errorMsg));


                        decimal? _extDAdlNet = Equals(dr.SaleCur, saleCurr) ? (dr.ExtraSale.HasValue ? dr.ExtraSale.Value : 0) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.SaleCur, saleCurr, dr.ExtraSale.Value, true, ref errorMsg);
                        exASaleAdl = exASaleAdl + (_extDAdlNet.HasValue ? _extDAdlNet.Value : 0);
                        //(Equals(dr.SaleCur, saleCurr) ? dr.ExtraNet.Value : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.SaleCur, saleCurr, dr.ExtraNet.Value, true, ref errorMsg));
                        exASaleChd = exASaleChd +
                            //(Equals(dr.SaleCur, saleCurr) ? (dr.ExtraChdNet.HasValue ? dr.ExtraChdNet.Value : dr.ExtraNet.Value) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.SaleCur, saleCurr, dr.ExtraChdNet.HasValue ? dr.ExtraChdNet.Value : dr.ExtraNet.Value, true, ref errorMsg));
                            (Equals(dr.SaleCur, saleCurr) ? (dr.ExtraChdSale.HasValue ? dr.ExtraChdSale.Value : (_extDAdlNet.HasValue ? _extDAdlNet.Value : 0)) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.SaleCur, saleCurr, dr.ExtraChdSale.HasValue ? dr.ExtraChdSale.Value : (_extDAdlNet.HasValue ? _extDAdlNet.Value : 0), true, ref errorMsg));
                        exASaleInf = exASaleInf +
                            //(Equals(dr.SaleCur, saleCurr) ? (dr.ExtraInfNet.HasValue ? dr.ExtraInfNet.Value : dr.ExtraNet.Value) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.SaleCur, saleCurr, dr.ExtraInfNet.HasValue ? dr.ExtraInfNet.Value : dr.ExtraNet.Value, true, ref errorMsg));
                            (Equals(dr.SaleCur, saleCurr) ? (dr.ExtraInfSale.HasValue ? dr.ExtraInfSale.Value : (_extDAdlNet.HasValue ? _extDAdlNet.Value : 0)) : new TvBo.Common().Exchange(UserData.Market, DateTime.Today, dr.SaleCur, saleCurr, dr.ExtraInfSale.HasValue ? dr.ExtraInfSale.Value : (_extDAdlNet.HasValue ? _extDAdlNet.Value : 0), true, ref errorMsg));
                    }
                }
            }
            #endregion

            #region Defination
            decimal? depAdlTot = 0;
            decimal? depChdTot = 0;
            decimal? depInfTot = 0;
            decimal? arrAdlTot = 0;
            decimal? arrChdTot = 0;
            decimal? arrInfTot = 0;
            depAdlTot = depAdlFlight + exDSaleAdl + depAdlComp;
            depChdTot = depChdFlight + exDSaleChd + depChdComp;
            depInfTot = depInfFlight + exDSaleInf + depInfComp;
            arrAdlTot = arrAdlFlight + exASaleAdl + retAdlComp;
            arrChdTot = arrChdFlight + exASaleChd + retChdComp;
            arrInfTot = arrInfFlight + exASaleInf + retInfComp;
            decimal? adlEB = 0;
            decimal? chdEB = 0;
            decimal? infEB = 0;
            #endregion

            #region Calculation Flight EB
            OnlyTicket_EB flightEB = new OnlyTickets().getOnlyFlightEB(depFlight, UserData.Operator, UserData.Market, DateTime.Today, depDate, night, ref errorMsg);
            if (flightEB != null)
            {
                if (flightEB.EBPasPer.HasValue && flightEB.EBPasPer.Value > 0)
                {
                    adlEB = depAdlTot * (flightEB.EBPasPer.Value / 100);
                    chdEB = depChdTot * (flightEB.EBPasPer.Value / 100);
                    infEB = depInfTot * (flightEB.EBPasPer.Value / 100);
                    drNew.EBPasPer = flightEB.EBPasPer.Value;
                    drNew.EBP_AdlVal = adlEB;
                    drNew.EBP_ChdGrp2Val = chdEB;
                    drNew.EBP_ChdGrp1Val = infEB;
                }
                else
                {
                    adlEB = flightEB.EBP_AdlVal.HasValue && flightEB.EBP_AdlVal.Value > 0 ? flightEB.EBP_AdlVal.Value : 0;
                    if (!Equals(saleCurr, flightEB.EBP_Cur))
                        adlEB = new TvBo.Common().Exchange(UserData.Market, DateTime.Today, flightEB.EBP_Cur, saleCurr, adlEB.Value, true, ref errorMsg);
                    chdEB = flightEB.EBP_ChdGrp2Val.HasValue && flightEB.EBP_ChdGrp2Val.Value > 0 ? flightEB.EBP_ChdGrp2Val.Value : 0;
                    if (!Equals(saleCurr, flightEB.EBP_Cur))
                        chdEB = new TvBo.Common().Exchange(UserData.Market, DateTime.Today, flightEB.EBP_Cur, saleCurr, chdEB.Value, true, ref errorMsg);
                    infEB = flightEB.EBP_ChdGrp1Val.HasValue && flightEB.EBP_ChdGrp1Val.Value > 0 ? flightEB.EBP_ChdGrp1Val.Value : 0;
                    if (!Equals(saleCurr, flightEB.EBP_Cur))
                        infEB = new TvBo.Common().Exchange(UserData.Market, DateTime.Today, flightEB.EBP_Cur, saleCurr, infEB.Value, true, ref errorMsg);

                    drNew.EBPasPer = null;
                    drNew.EBP_AdlVal = adlEB;
                    drNew.EBP_ChdGrp2Val = chdEB;
                    drNew.EBP_ChdGrp1Val = infEB;
                }
            }
            if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
            {
                OnlyTicket_EB flightRetEB = new OnlyTickets().getOnlyFlightEB(arrFlight, UserData.Operator, UserData.Market, DateTime.Today, arrDate, night, ref errorMsg);
                if (flightRetEB != null)
                {
                    if (flightRetEB.EBPasPer.HasValue && flightRetEB.EBPasPer.Value > 0)
                    {
                        adlEB = depAdlTot * (flightRetEB.EBPasPer.Value / 100);
                        chdEB = depChdTot * (flightRetEB.EBPasPer.Value / 100);
                        infEB = depInfTot * (flightRetEB.EBPasPer.Value / 100);
                        drNew.EBPasPer = flightRetEB.EBPasPer.Value;
                        drNew.EBP_AdlVal += adlEB.HasValue ? adlEB.Value : 0;
                        drNew.EBP_ChdGrp2Val += chdEB.HasValue ? chdEB.Value : 0;
                        drNew.EBP_ChdGrp1Val += infEB.HasValue ? infEB.Value : 0;
                    }
                    else
                    {
                        adlEB = flightRetEB.EBP_AdlVal.HasValue && flightRetEB.EBP_AdlVal.Value > 0 ? flightRetEB.EBP_AdlVal.Value : 0;
                        if (!Equals(saleCurr, flightRetEB.EBP_Cur))
                            adlEB = new TvBo.Common().Exchange(UserData.Market, DateTime.Today, flightRetEB.EBP_Cur, saleCurr, adlEB.Value, true, ref errorMsg);
                        chdEB = flightRetEB.EBP_ChdGrp2Val.HasValue && flightRetEB.EBP_ChdGrp2Val.Value > 0 ? flightRetEB.EBP_ChdGrp2Val.Value : 0;
                        if (!Equals(saleCurr, flightRetEB.EBP_Cur))
                            chdEB = new TvBo.Common().Exchange(UserData.Market, DateTime.Today, flightRetEB.EBP_Cur, saleCurr, chdEB.Value, true, ref errorMsg);
                        infEB = flightRetEB.EBP_ChdGrp1Val.HasValue && flightRetEB.EBP_ChdGrp1Val.Value > 0 ? flightRetEB.EBP_ChdGrp1Val.Value : 0;
                        if (!Equals(saleCurr, flightRetEB.EBP_Cur))
                            infEB = new TvBo.Common().Exchange(UserData.Market, DateTime.Today, flightRetEB.EBP_Cur, saleCurr, infEB.Value, true, ref errorMsg);

                        drNew.EBPasPer = null;
                        drNew.EBP_AdlVal += adlEB.HasValue ? adlEB.Value : 0;
                        drNew.EBP_ChdGrp2Val += chdEB.HasValue ? chdEB.Value : 0;
                        drNew.EBP_ChdGrp1Val += infEB.HasValue ? infEB.Value : 0;
                    }
                }
            }
            #endregion

            #region Adding Row
            drNew.Operator = drSearch.Operator;
            drNew.Market = drSearch.Market;
            drNew.DepTime = drSearch.DepTime;
            drNew.AdlTot = depAdlTot + arrAdlTot;
            drNew.ChdTot = depChdTot + arrChdTot;
            drNew.InfTot = depInfTot + arrInfTot;
            drNew.DepFlight = depFlight;
            drNew.DepFlightAirline = drSearch.DepFlightAirline;
            drNew.DFullName = drSearch.DFullName;
            drNew.DepDate = depDate;
            drNew.DepClass = depClass;
            drNew.DepSupp = depSupp;
            drNew.IsArrFlight = false;
            drNew.RouteFrom = drSearch.DepCity;
            drNew.RouteFromName = new Locations().getLocationName(UserData, drSearch.DepCity.HasValue ? drSearch.DepCity.Value : -1, ref errorMsg);
            drNew.RouteTo = drSearch.ArrCity;
            drNew.RouteToName = new Locations().getLocationName(UserData, drSearch.ArrCity.HasValue ? drSearch.ArrCity.Value : -1, ref errorMsg);
            drNew.MarketCurr = saleCurr;
            drNew.DepAdlTot = depAdlTot;
            drNew.DepChdTot = depChdTot;
            drNew.DepInfTot = depInfTot;
            drNew.FlightDepAdlTot = depAdlFlight;
            drNew.FlightDepChdTot = depChdFlight;
            drNew.FlightDepInfTot = depInfFlight;
            drNew.ExtraDepAdlTot = exDSaleAdl;
            drNew.ExtraDepChdTot = exDSaleChd;
            drNew.ExtraDepInfTot = exDSaleInf;
            drNew.RoundDifDepAdlVal = 0;
            drNew.RoundDifDepChdVal = 0;
            drNew.RoundDifDepInfVal = 0;


            if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
            {
                drNew.ArrTime = drSearch.ArrTime;
                drNew.ArrFlight = arrFlight;
                drNew.ArrFlightAirline = drSearch.ArrFlightAirline;
                drNew.AFullName = drSearch.AFullName;
                drNew.ArrDate = arrDate;
                drNew.ArrClass = arrClass;
                drNew.ArrSupp = arrSupp;
                drNew.IsArrFlight = true;
                drNew.ArrAdlTot = arrAdlTot;
                drNew.ArrChdTot = arrChdTot;
                drNew.ArrInfTot = arrInfTot;
                drNew.FlightArrAdlTot = arrAdlFlight;
                drNew.FlightArrChdTot = arrChdFlight;
                drNew.FlightArrInfTot = arrInfFlight;
                drNew.ExtraArrAdlTot = exASaleAdl;
                drNew.ExtraArrChdTot = exASaleChd;
                drNew.ExtraArrInfTot = exASaleInf;
                drNew.RoundDifArrAdlVal = 0;
                drNew.RoundDifArrChdVal = 0;
                drNew.RoundDifArrInfVal = 0;

            }
            #endregion

            if (drNew.AllowRes.HasValue && drNew.AllowRes.Value)
            {

                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                {
                    if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
                    {
                        if (drNew.AllotType != "O" && drNew.DepFreeSeat > 0 && drNew.ArrFreeSeat > 0)
                            dtFlightInfo.Add(drNew);
                    }
                    else
                    {
                        if (drNew.AllotType != "O" && drNew.DepFreeSeat > 0)
                            dtFlightInfo.Add(drNew);
                    }
                }
                else
                    dtFlightInfo.Add(drNew);
            }
            return dtFlightInfo;
        }

        public Int16? getFreeSeat(User UserData, string Operator, bool isRT, DateTime? ResDate, string FlightNo, DateTime? FlyDate, string SClass, string Supplier, string BlockType, Int16? forSeat, int? Night, ref string errorMsg)
        {
            string tsql = string.Empty;
            if ((VersionControl.CheckWebVersion(UserData.WebVersion, 33, VersionControl.Equality.gt) && !isRT))
                tsql = @"Declare @Stat Int 
                            Declare @FreeSeat Int 
                            Exec sp_CheckFlightAlllotForSearch @FlightNo, @FlyDate, @SClass, @Operator, @Supplier, @Buyer, @BlockType, @ForSeat, @ResDate, @Stat Output, @FreeSeat Output, @Night 
                            Select @Stat as Stat, @FreeSeat as FreeSeat ";
            else
                if (VersionControl.CheckWebVersion(UserData.WebVersion, 19, VersionControl.Equality.gt) && Night.HasValue)
                    tsql = @"Declare @Stat Int 
                            Declare @FreeSeat Int 
                            Exec sp_CheckFlightAlllotForSearch @FlightNo, @FlyDate, @SClass, @Operator, @Supplier, @Buyer, @BlockType, @ForSeat, @ResDate, @Stat Output, @FreeSeat Output, @Night 
                            Select @Stat as Stat, @FreeSeat as FreeSeat ";
                else
                    if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200010"))
                        tsql = @"Declare @Stat Int 
                            Declare @FreeSeat Int 
                            Exec sp_CheckFlightAlllot @FlightNo, @FlyDate, @SClass, @Operator, @Supplier, @Buyer, @BlockType, @ForSeat, @ResDate, @Stat Output, @FreeSeat Output 
                            Select @Stat as Stat, @FreeSeat as FreeSeat ";
                    else
                        tsql = @"Declare @Stat Int 
                            Declare @FreeSeat Int 
                            Exec sp_CheckFlightAlllot @FlightNo, @FlyDate, @SClass, @Operator, @Supplier, @Buyer, @BlockType, @ForSeat, @Stat Output, @FreeSeat Output 
                            Select @Stat as Stat, @FreeSeat as FreeSeat ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "SClass", DbType.String, SClass);
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Supplier", DbType.String, Supplier);
                db.AddInParameter(dbCommand, "Buyer", DbType.String, string.Empty);
                db.AddInParameter(dbCommand, "ForSeat", DbType.Int16, forSeat);
                db.AddInParameter(dbCommand, "BlockType", DbType.String, BlockType);
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("030200010"))
                    db.AddInParameter(dbCommand, "ResDate", DbType.DateTime, ResDate);
                if ((VersionControl.CheckWebVersion(UserData.WebVersion, 33, VersionControl.Equality.gt) && !isRT))
                    db.AddInParameter(dbCommand, "Night", DbType.Int32, 0);
                else
                    if (VersionControl.CheckWebVersion(UserData.WebVersion, 19, VersionControl.Equality.gt))
                        db.AddInParameter(dbCommand, "Night", DbType.Int32, Night);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        Int16? Status = Conversion.getInt16OrNull(R["Stat"]);
                        if (Status.HasValue && Status.Value == 0)
                            return Conversion.getInt16OrNull(R["FreeSeat"]);
                        else
                            return 0;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Int16? getBlockSeat(string Operator, string FlightNo, DateTime? FlyDate, string SClass, string Supplier, string BlockType, ref string errorMsg)
        {
            string tsql = @"if @Supplier<>'' 
                                Select Kont=(Seat-BlockSeat) From AllotFlight (NOLOCK)
                                Where FlightNo=@FlightNo and FlyDate=@FlyDate and SClass=@SClass and Operator=@Operator and Supplier=@Supplier and Buyer=@Buyer and BlockType=@BlockType
                            else
                                Select Kont=Sum(Seat-BlockSeat) From AllotFlight (NOLOCK)
                                Where FlightNo=@FlightNo and FlyDate=@FlyDate and SClass=@SClass and Operator=@Operator and  Buyer=@Buyer and BlockType=@BlockType ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "SClass", DbType.String, SClass);
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Supplier", DbType.String, Supplier);
                db.AddInParameter(dbCommand, "Buyer", DbType.String, string.Empty);
                db.AddInParameter(dbCommand, "BlockType", DbType.String, BlockType);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        Int16? kont = Conversion.getInt16OrNull(R["Kont"]);
                        if (kont.HasValue)
                            return kont.Value;
                        else
                            return 0;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int getUseSeat(string Operator, string Market, string FlightNo, DateTime? FlyDate, string SClass, ref string errorMsg)
        {
            string tsql = @"Select UseVal = sum(Unit) From ResService (NOLOCK) S
                            Join ResMain (NOLOCK) R on R.ResNo = S.ResNo
                            Where R.ResStat in (0,1) and R.ConfStat <> 2 and S.StatSer in (0,1) and S.StatConf <> 2 and s.AllotType<>'O' and 
                                S.ServiceType = 'FLIGHT' and S.Service = @FlightNo and S.BegDate = @FlyDate and
                                isnull(R.HolPack, '') = '' and R.PLMarket = @Market and R.PLOperator = @Operator and
                                S.FlgClass = @SClass ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "SClass", DbType.String, SClass);
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        Int32? UseVal = Conversion.getInt16OrNull(R["UseVal"]);
                        if (UseVal.HasValue)
                            return UseVal.Value;
                        else
                            return 0;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return 0;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public Int16? GetFreeAllotSave(User UserData, ResMainRecord resMain, ResServiceRecord resService, bool isRT, string depFlightNo, DateTime? firstDate, string arrFlightNo, string DepClass, DateTime? depDate, int? Day, string blockType, ref bool isTicketRest, ref bool makeRes, ref int isOpt, ref int optFree, ref int normalFree, ref int restFree, ref string errorMsg)
        {
            makeRes = true;
            isOpt = 0;
            Int16? freeSeat = getFreeSeat(UserData, resMain.PLOperator, isRT, resMain.ResDate, resService.Service, resService.BegDate, resService.FlgClass, resService.Supplier, resService.AllotType, Convert.ToInt16(normalFree), Day, ref errorMsg);
            if (!freeSeat.HasValue)
                return 0;

            string optFlight = string.Empty;
            bool useFlightOpt = UserData.TvParams.TvParamFlight.UseOptForAllot;

            bool flightBaseOpt = false;
            bool isFlightOpt = false;

            if (freeSeat.Value <= 0 && (UserData.TvParams.TvParamFlight.AllotWarnFull || UserData.TvParams.TvParamFlight.AllotDoNotOver) && UserData.TvParams.TvParamFlight.AllotChk)
                makeRes = false;

            Int16? kont = getBlockSeat(resMain.PLOperator, resService.Service, resService.BegDate, resService.FlgClass, resService.Supplier, resService.AllotType, ref errorMsg);
            Int32 TicketUse = getUseSeat(resMain.PLOperator, UserData.Market, resService.Service, resService.BegDate, resService.FlgClass, ref errorMsg);
            FlightOptRecord dtFlyOpt = null;
            if (useFlightOpt)
            {
                dtFlyOpt = new Flights().getFlightOpt(resMain.PLOperator, string.Empty, firstDate, null, DepClass, resMain.DepCity, resMain.ArrCity, ref errorMsg);
                isFlightOpt = dtFlyOpt != null ? true : false;
                depFlightNo = "%";
                if (isFlightOpt)
                {
                    dtFlyOpt = new Flights().getFlightOpt(resMain.PLOperator, " ", firstDate, null, DepClass, resMain.DepCity, resMain.ArrCity, ref errorMsg);
                    if (dtFlyOpt == null)
                        flightBaseOpt = true;

                    if (flightBaseOpt)
                    {
                        dtFlyOpt = new Flights().getFlightOpt(resMain.PLOperator, resService.Service, resService.BegDate, null, DepClass, resMain.DepCity, resMain.ArrCity, ref errorMsg);

                        if (dtFlyOpt == null)
                            return freeSeat;
                        if (Equals(depFlightNo, resService.Service))
                        {
                            if (Day.HasValue && Day.Value > 0)
                            {
                                FlightDayRecord flyDay = new Flights().getFlightDay(UserData, optFlight, depDate.HasValue ? depDate.Value : DateTime.MinValue, ref errorMsg);
                                if (flyDay != null && !Equals(arrFlightNo, flyDay.NextFlight))
                                    return 0;
                            }
                        }
                        else
                            return freeSeat;
                    }
                    /*2011-08-17*/
                    Int16? Night = resMain.Days;
                    Int16? OptVal = new Flights().getFlightOptVal(resMain.PLOperator, depFlightNo, firstDate, Night, DepClass, resMain.DepCity, resMain.ArrCity, ref errorMsg);

                    if (!OptVal.HasValue || OptVal.Value < 0)
                        OptVal = 0;

                    if (OptVal == 0 && freeSeat <= 0)
                        isOpt = 1;
                    if (OptVal == 0 && freeSeat > 0)
                        isOpt = 2;
                    if (OptVal < freeSeat)
                        freeSeat = OptVal;

                    /*2011-08-17*/
                }
            }

            TicketRestRecord ticketRest = new Flights().getTicketRest(resMain.PLOperator, resService.Service, resService.BegDate.HasValue ? resService.BegDate.Value : DateTime.MinValue, DepClass, Convert.ToInt16(Day.HasValue ? Day.Value : 0), ref errorMsg);
            if (ticketRest != null)
            {
                isTicketRest = true;
                if (Equals(ticketRest.StopSale, "Y"))
                {
                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "StopSaleAlert").ToString();
                    makeRes = false;
                    return 0;
                }
                else
                {
                    Int16? salePerc = ticketRest.SalePerc;
                    if (salePerc.HasValue && salePerc > 100)
                        salePerc = 100;
                    else if (salePerc.HasValue && salePerc.Value < 0)
                        salePerc = 0;
                    kont = Convert.ToInt16(Math.Truncate(Convert.ToDecimal(kont.Value * salePerc / 100)));
                    restFree = kont.Value - TicketUse;
                    if (restFree < 0)
                        restFree = 0;
                    if (restFree < freeSeat)
                        freeSeat = Convert.ToInt16(restFree);
                    if (restFree == 0)
                        makeRes = false;
                }
            }
            return freeSeat;
        }

        public Int16? GetFreeAllot(User UserData, OnlyTicket_Flight drSearch, bool isRT, string FlightNo, DateTime FlyDate, string depFlightNo, string arrFlightNo, string SClass, string Supplier, DateTime DepDate, DateTime? ArrDate, string DepClass, Int16? Day, ref bool isTicketRest, ref bool makeRes, ref int isOpt, ref int optFree, ref int normalFree, ref int restFree, ref string errorMsg, string blockType)
        {
            isOpt = 0;
            Int16? freeSeat = getFreeSeat(UserData, drSearch.Operator, isRT, null, FlightNo, FlyDate, SClass, Supplier, blockType, Convert.ToInt16(normalFree), isRT ? Day : null, ref errorMsg);
            if (!freeSeat.HasValue)
                return 0;

            TicketRestRecord ticketRest = new TicketRestRecord();

            string optFlight = string.Empty;
            bool useFlightOpt = UserData.TvParams.TvParamFlight.UseOptForAllot;

            bool flightBaseOpt = false;
            bool isFlightOpt = false;

            if (freeSeat.Value <= 0)
                makeRes = false;

            Int16? kont = getBlockSeat(drSearch.Operator, FlightNo, FlyDate, SClass, Supplier, blockType, ref errorMsg);
            Int32 TicketUse = getUseSeat(drSearch.Operator, UserData.Market, FlightNo, FlyDate, SClass, ref errorMsg);
            FlightOptRecord dtFlyOpt = null;
            if (useFlightOpt)
            {
                dtFlyOpt = new Flights().getFlightOpt(drSearch.Operator, FlightNo /*string.Empty*/, DepDate, null, DepClass, drSearch.DepCity, drSearch.ArrCity, ref errorMsg);
                if (dtFlyOpt == null)
                {
                    dtFlyOpt = new Flights().getFlightOpt(drSearch.Operator, string.Empty, DepDate, null, DepClass, drSearch.DepCity, drSearch.ArrCity, ref errorMsg);
                    if (dtFlyOpt != null && !string.IsNullOrEmpty(dtFlyOpt.FlightNo) && dtFlyOpt.FlightNo != FlightNo)
                        dtFlyOpt = null;
                }
                isFlightOpt = dtFlyOpt != null ? true : false;
                if (!isRT && isFlightOpt)
                {
                    ticketRest = new Flights().getTicketRest(drSearch.Operator, FlightNo, FlyDate, DepClass, Convert.ToInt16(Day.HasValue ? Day.Value : 0), ref errorMsg);
                    if (ticketRest != null)
                    {
                        isTicketRest = true;
                        if (Equals(ticketRest.StopSale, "Y"))
                            makeRes = false;
                    }
                    return 0;
                }
                else
                    if (!isFlightOpt && !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                    {
                        dtFlyOpt = new Flights().getFlightOpt(drSearch.Operator, string.Empty, DepDate, null, DepClass, drSearch.ArrCity, drSearch.DepCity, ref errorMsg);
                        isFlightOpt = dtFlyOpt != null ? true : false;
                        if (isFlightOpt)
                        {
                            ticketRest = new Flights().getTicketRest(drSearch.Operator, FlightNo, FlyDate, DepClass, Convert.ToInt16(Day.HasValue ? Day.Value : 0), ref errorMsg);
                            if (ticketRest != null)
                            {
                                isTicketRest = true;
                                if (Equals(ticketRest.StopSale, "Y"))
                                    makeRes = false;
                            }
                            return 0;
                        }
                    }
            }
            Int16? Night = drSearch.IsRT.HasValue && drSearch.IsRT.Value ? Convert.ToInt16(((ArrDate.HasValue ? ArrDate.Value : DepDate) - DepDate).Days) : Convert.ToInt16(0);
            if (isFlightOpt)
            {
                optFlight = depFlightNo;
                dtFlyOpt = new Flights().getFlightOpt(drSearch.Operator, " ", DepDate, null, DepClass, drSearch.DepCity, drSearch.ArrCity, ref errorMsg);
                if (dtFlyOpt == null)
                    flightBaseOpt = true;
                if (flightBaseOpt)
                {
                    dtFlyOpt = new Flights().getFlightOpt(drSearch.Operator, optFlight, DepDate, null, DepClass, drSearch.DepCity, drSearch.ArrCity, ref errorMsg);
                    if (dtFlyOpt == null)
                        return freeSeat;
                    if (Equals(depFlightNo, FlightNo))
                    {
                        if (drSearch.IsRT.HasValue && drSearch.IsRT.Value)
                        {
                            FlightDayRecord flyDay = new Flights().getFlightDay(UserData, optFlight, DepDate, ref errorMsg);
                            if (flyDay != null && !Equals(arrFlightNo, flyDay.NextFlight))
                                return 0;

                            Int16? OptVal = new Flights().getFlightOptVal(drSearch.Operator, optFlight, DepDate, Night, DepClass, drSearch.DepCity, drSearch.ArrCity, ref errorMsg);
                            if (!OptVal.HasValue || OptVal.Value < 0)
                                freeSeat = 0;
                            else
                            {
                                if (OptVal == 0 && freeSeat <= 0)
                                    isOpt = 1;
                                if (OptVal == 0 && freeSeat > 0)
                                    isOpt = 2;
                                if (OptVal < freeSeat)
                                    freeSeat = OptVal;
                            }
                        }
                    }
                }
                else
                {
                    Int16? OptVal = new Flights().getFlightOptVal(drSearch.Operator, "%", DepDate, Night, DepClass, drSearch.DepCity, drSearch.ArrCity, ref errorMsg);
                    if (!OptVal.HasValue || OptVal.Value < 0)
                        freeSeat = 0;
                    else
                    {
                        if (OptVal == 0 && freeSeat <= 0)
                            isOpt = 1;
                        if (OptVal == 0 && freeSeat > 0)
                            isOpt = 2;
                        if (OptVal < freeSeat)
                            freeSeat = OptVal;
                    }
                }
            }

            if (useFlightOpt && isFlightOpt)
            {
                Int16? optFree_ = new Flights().getFlightOptVal(drSearch.Operator, null, DepDate, Night, DepClass, drSearch.DepCity, drSearch.ArrCity, ref errorMsg);
                if (optFree_ != null && (optFree_.HasValue && optFree_.Value > 0))
                {
                    //freeSeat = optFree_.Value;
                    optFree = optFree_.Value;
                }
                else
                    optFree = 0;

                if (optFree <= freeSeat)
                    freeSeat = Convert.ToInt16(optFree.ToString());
            }

            ticketRest = new Flights().getTicketRest(drSearch.Operator, FlightNo, FlyDate, DepClass, Convert.ToInt16(Day.HasValue ? Day.Value : 0), ref errorMsg);
            if (ticketRest != null)
            {
                isTicketRest = true;
                if (Equals(ticketRest.StopSale, "Y"))
                {
                    makeRes = false;
                    return 0;
                }
                else
                {
                    Int16? salePerc = ticketRest.SalePerc;
                    if (salePerc.HasValue && salePerc > 100)
                        salePerc = 100;
                    else if (salePerc.HasValue && salePerc.Value < 0)
                        salePerc = 0;
                    kont = Convert.ToInt16(Math.Truncate(Convert.ToDecimal(kont.Value * salePerc / 100)));
                    restFree = kont.Value - TicketUse;
                    if (restFree < 0)
                        restFree = 0;
                    if (restFree < freeSeat)
                        freeSeat = Convert.ToInt16(restFree);
                    if (restFree == 0)
                        makeRes = false;
                }
            }
            return freeSeat;
        }

        public List<OnlyFlight_ServiceExtra> CalculateExtra(User UserData, string plMarket, string saleCur, FlightData flightData, List<OnlyFlight_ServiceExtra> dtAddExtra, string aType, string aFlight, DateTime aDate, string sClass, int recID, Int16 night, ref string errorMsg)
        {
            FlightDayRecord flight = new Flights().getFlightDay(UserData, aFlight, aDate, ref errorMsg);
            string aAirport = flight.DepAirport;
            List<ExtraServiceListRecord> dtExtService = new Reservation().getExtraServicesForFlight(UserData, plMarket, DateTime.Today, saleCur, sClass, aDate, night, true, aFlight, true, string.Empty, false, ref errorMsg);
            if (dtExtService == null)
                return dtAddExtra;
            foreach (ExtraServiceListRecord dtService in dtExtService)
            {
                ServiceExtRecord extServiceRec = new Reservation().getExtraServisDetail(UserData, "FLIGHT", dtService.Code, ref errorMsg);
                if (extServiceRec != null)
                {

                    ServiceExtPriceRecord row = new Reservation().getServiceExtPrice(UserData, dtService.RecID, true, true, false, ref errorMsg);
                    if (row != null)
                    {
                        OnlyFlight_ServiceExtra dr = new OnlyFlight_ServiceExtra();

                        dr.ArrDep = aType;
                        dr.RecID = recID;
                        dr.Extra = row.Code;
                        dr.ExtraName = dtService.Description;
                        dr.NetCur = row.NetCur;
                        dr.ExtraNet = row.NetAdlPrice.HasValue ? row.NetAdlPrice.Value : 0;
                        dr.ExtraSale = row.SaleAdlPrice.HasValue ? row.SaleAdlPrice.Value : 0;

                        if (row.ChdAge2.HasValue)
                        {
                            dr.ExtraChdNet = (row.NetChdPrice.HasValue) ? row.NetChdPrice.Value : 0;
                            dr.ExtraChdSale = (row.SaleChdPrice.HasValue) ? row.SaleChdPrice.Value : 0;
                        }
                        else
                        {
                            dr.ExtraChdNet = dr.ExtraNet;
                            dr.ExtraChdSale = dr.ExtraSale;
                        }

                        if (row.InfAge2.HasValue)
                        {
                            dr.ExtraInfNet = row.NetInfPrice.HasValue ? row.NetInfPrice.Value : 0;
                            dr.ExtraInfSale = row.SaleInfPrice.HasValue ? row.SaleInfPrice.Value : 0;
                        }
                        else
                        {
                            dr.ExtraInfNet = dr.ExtraNet;
                            dr.ExtraInfSale = dr.ExtraSale;
                        }

                        dr.Supplier = row.Supplier;
                        dr.PriceType = row.PriceType;
                        dr.CalcType = row.CalcType;
                        dr.PayCom = row.PayCom;
                        dr.PayComEB = row.PayComEB;
                        dr.SaleCur = row.SaleCur;
                        dr.StatConf = extServiceRec.ConfStat;
                        dr.MainService = aFlight;
                        dtAddExtra.Add(dr);
                    }
                }
            }
            return dtAddExtra;
        }

        public List<OnlyFlight_AdditionalService> CalculateAdditionalService(User UserData, string plMarket, FlightData flightData, List<OnlyFlight_AdditionalService> dtAddExtra, OnlyFlight_AdServices adsService, ref string errorMsg)
        {


            return dtAddExtra;
        }

        public List<SearchResult> FillSelectBook(User UserData, List<SearchResult> SelectBook, OnlyFlight_FlightInfo FlightData, Int16 Adult, Int16 Child, Int16 Infant, ref string errorMsg)
        {
            FlightDetailRecord DFlight = new Flights().getFlightDetail(UserData, UserData.Market, FlightData.DepFlight, FlightData.DepDate.Value, ref errorMsg);
            if (DFlight == null)
            {
                errorMsg = "Flight not found.";
                return null;
            }
            SearchResult row = new SearchResult();
            #region Age Group
            List<AgeGroup> ageGroups = new List<AgeGroup>();
            int roomCnt = 0;
            int refNo = 0;
            roomCnt++;
            refNo++;
            ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Adult, GroupNo = roomCnt, RefNo = refNo, Unit = Adult, Age = null, DateOfBirth = null });
            if (Infant > 0)
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Infant, GroupNo = roomCnt, RefNo = refNo, Unit = Infant, Age = Convert.ToDecimal(199.0 / 100), DateOfBirth = null });
            if (Child > 0)
                ageGroups.Add(new AgeGroup { GroupType = AgeGroupType.Child, GroupNo = roomCnt, RefNo = refNo, Unit = Child, Age = Convert.ToDecimal(1599.0 / 100), DateOfBirth = null });
            #endregion
            row.CatPackID = 0;
            row.RefNo = 1;
            row.ARecNo = 0;
            row.PRecNo = 0;
            row.CheckIn = FlightData.DepDate.Value;
            if (!string.IsNullOrEmpty(FlightData.ArrFlight))
                row.CheckOut = (FlightData.IsNext.HasValue && FlightData.IsNext.Value == 1 && FlightData.ArrDate.HasValue) ? FlightData.ArrDate.Value : (FlightData.DepDate.Value).AddDays(FlightData.Days.Value);
            else
                row.CheckOut = FlightData.DepDate.Value;
            row.Night = Convert.ToInt16(FlightData.Days.HasValue ? FlightData.Days.Value : 0);
            row.NetCur = string.IsNullOrEmpty(row.NetCur) ? FlightData.MarketCurr : row.NetCur;
            row.ChdG1Age1 = 0;
            row.ChdG1Age2 = DFlight.MaxInfAge.HasValue ? DFlight.MaxInfAge.Value : (Convert.ToDecimal(199) / 100);
            row.ChdG2Age1 = DFlight.MaxInfAge.HasValue ? Convert.ToDecimal(Convert.ToInt32(DFlight.MaxInfAge.Value)) : Convert.ToDecimal(2);
            row.ChdG2Age2 = DFlight.MaxChdAge.HasValue ? DFlight.MaxChdAge.Value : Convert.ToDecimal(16);
            row.ChdG3Age1 = 0;
            row.ChdG3Age2 = 0;
            row.ChdG4Age1 = 0;
            row.ChdG4Age2 = 0;
            row.SaleCur = FlightData.MarketCurr;
            row.HolPack = string.Empty;
            row.Operator = FlightData.Operator;
            row.Market = FlightData.Market;
            row.FlightClass = FlightData.DepClass;
            row.DepCity = FlightData.RouteFrom;
            row.ArrCity = FlightData.RouteTo;
            row.HAdult = Adult;
            row.HChdAgeG1 = Infant;
            row.HChdAgeG2 = Child;
            row.HChdAgeG3 = 0;
            row.HChdAgeG4 = 0;
            if (Infant > 0)
                row.Child1Age = row.ChdG1Age2.HasValue ? Convert.ToInt16(Math.Truncate(row.ChdG1Age2.Value)) : Convert.ToInt16(1);
            if (Child > 0)
                row.Child2Age = row.ChdG2Age2.HasValue ? Convert.ToInt16(Math.Truncate(row.ChdG2Age2.Value)) : Convert.ToInt16(15);
            row.DepCityName = new Locations().getLocationName(UserData, FlightData.RouteFrom.Value, ref errorMsg);
            row.ArrCityName = new Locations().getLocationName(UserData, FlightData.RouteTo.Value, ref errorMsg);
            row.DepSeat = FlightData.DepFreeSeat;
            row.RetSeat = FlightData.ArrFreeSeat.HasValue ? FlightData.ArrFreeSeat.Value : 0;
            row.StopSaleGuar = 0;
            row.StopSaleStd = 0;
            row.CurrentCur = UserData.SaleCur;
            row.AgeGroupList = ageGroups;
            row.PLCur = FlightData.MarketCurr;
            SelectBook.Add(row);

            return SelectBook;
        }

        public ResDataRecord bookFlight(User UserData, ResDataRecord _ResData, OnlyFlight_FlightInfo bookRow, Int16 Adult, Int16 Child, Int16 Infant, bool CreateChildAge, OnlyFlight_FlightInfo FlightRow, ref string errorMsg)
        {

            ResDataRecord ResData = new ResDataRecord();
            ResData = new ResTables().copyData(_ResData);

            int BookID = bookRow.RecID.HasValue ? bookRow.RecID.Value : 1;

            ResData.Title = new TvBo.Common().getTitle(ref errorMsg);

            #region Create ResMain
            ResData = new Reservation().GenerateResMain(UserData, ResData, SearchType.OnlyFlightSearch, string.Empty, ref errorMsg);
            ResMainRecord resMain = ResData.ResMain;
            resMain.SaleCur = bookRow.MarketCurr;
            if (errorMsg != "")
                return null;
            #endregion

            #region Create ResCust
            ResData.ResCust = new List<ResCustRecord>();
            for (int i = 0; i < ResData.SelectBook.Count; i++)
            {
                //if (ResData.SelectBook[i].RefNo != null)
                BookID = ResData.SelectBook[i].RefNo;
                //else
                //    BookID = i + 1;
                ResData = new Reservation().GenerateTourists(UserData, ResData, BookID, ref errorMsg);
                if (errorMsg != "")
                    return null;
            }

            ResData.ResCust[0].Leader = "Y";
            #endregion Create ResCust

            ResData.ResService = new List<ResServiceRecord>();
            ResData.ResCon = new List<ResConRecord>();
            ResData.ResServiceExt = new List<ResServiceExtRecord>();
            ResData.ResConExt = new List<ResConExtRecord>();
            ResData.ResCustPrice = new List<ResCustPriceRecord>();
            ResData.ResCustInfo = new List<ResCustInfoRecord>();
            ResData.ResFlightDetail = new List<ResFlightDetailRecord>();
            ResData.ResSupDis = new List<ResSupDisRecord>();

            List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
            foreach (SelectCustRecord s in SelectCust)
                s.Selected = true;

            ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, bookRow.DepDate.Value, bookRow.DepDate.Value, "FLIGHT", bookRow.DepFlight,
                                string.Empty, string.Empty, string.Empty, bookRow.DepClass, 1, 0, 1, bookRow.RouteFrom, bookRow.RouteTo, string.Empty, string.Empty, 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, bookRow.DepSupp, null, ref errorMsg, null, null, null, null, null, null, null);

            List<ResServiceRecord> resServcies = ResData.ResService;
            ResServiceRecord service = resServcies.FirstOrDefault();
            FlightRecord flight = new Flights().getFlight(UserData, service.Service, ref errorMsg);
            if (flight != null && flight.OTickSaveAsRq.HasValue && flight.OTickSaveAsRq.Value)
                service.StatConf = 0;

            if (!string.IsNullOrEmpty(errorMsg))
            {
                return ResData;
            }

            #region Cancel 2013-02-18
            /* Cancel 2013-02-18            
            if (bookRow.DepPriceIsUnion.HasValue && bookRow.DepPriceIsUnion.Value)
            {
                List<ResServiceRecord> ResServiceList = ResData.ResService;
                if (ResServiceList.Count > 0)
                {
                    ResServiceRecord resService = ResServiceList[ResServiceList.Count - 1];
                    if (resService != null)
                    {
                        resService.PriceSource = 2;

                        resService.SalePrice = (bookRow.DepAdlTot.HasValue ? bookRow.DepAdlTot.Value : 0) * Adult + (bookRow.DepChdTot.HasValue ? bookRow.DepChdTot.Value : 0) * Child + (bookRow.DepInfTot.HasValue ? bookRow.DepInfTot.Value : 0) * Infant;
                    }
                }
            }
            */
            #endregion

            List<ResServiceRecord> flights = ResData.ResService;
            int ServiceID = ResData.ResService.LastOrDefault() != null ? ResData.ResService.LastOrDefault().RecID : -1;
            ResServiceRecord CompResServ = flights.Find(f => f.RecID == ServiceID);

            string AllotType = bookRow.AllotType;
            Int16 flySeat = Convert.ToInt16(Adult);
            decimal flyMaxInfAge = Convert.ToDecimal(199 / 100);
            Int16 flyInfSeat = 0;

            string Supplier = string.Empty;
            int Status = 0;
            int FreeSeat = 0;
            new Flights().checkFlightSeat(UserData, CompResServ.Service, ServiceID, CompResServ.BegDate.Value, ResData.ResMain.BegDate.Value, ResData, ref flySeat, ref flyMaxInfAge, ref flyInfSeat, SelectCust, true, ref errorMsg);
            CompResServ.Unit = flySeat;
            if (!(new Flights().CheckFlightAllot(UserData, bookRow.Operator, null, bookRow.DepFlight, bookRow.DepDate.Value, bookRow.DepClass, flySeat,
                ref Status, ref FreeSeat, ref errorMsg, "H", bookRow.DepSupp,bookRow.Days.HasValue?bookRow.Days.Value:0)))
            {
                return _ResData;
            }

            if (!string.IsNullOrEmpty(bookRow.ArrFlight))
            {
                ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, bookRow.ArrDate.Value, bookRow.ArrDate.Value, "FLIGHT", bookRow.ArrFlight,
                                string.Empty, string.Empty, string.Empty, bookRow.DepClass, Convert.ToInt16(bookRow.Days.Value - 1), Convert.ToInt16(bookRow.Days.Value), 1, bookRow.RouteTo, bookRow.RouteFrom, string.Empty, string.Empty, 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, bookRow.ArrSupp, null, ref errorMsg, null, null, null, null, null, null, null);
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    return ResData;
                }
                resServcies = ResData.ResService;
                service = resServcies.FirstOrDefault();
                flight = new Flights().getFlight(UserData, service.Service, ref errorMsg);
                if (flight != null && flight.OTickSaveAsRq.HasValue && flight.OTickSaveAsRq.Value)
                    service.StatConf = 0;

                #region Cancel 2013-02-18
                /* Cancel 2013-02-18
                if (bookRow.RetPriceIsUnion.HasValue && bookRow.RetPriceIsUnion.Value)
                {
                    List<ResServiceRecord> ResServiceList = ResData.ResService;
                    if (ResServiceList.Count > 1)
                    {
                        ResServiceRecord resService = ResServiceList[ResServiceList.Count - 1];
                        if (resService != null)
                        {
                            resService.PriceSource = 2;

                            resService.SalePrice = (bookRow.ArrAdlTot.HasValue ? bookRow.ArrAdlTot.Value : 0) * Adult + (bookRow.ArrChdTot.HasValue ? bookRow.ArrChdTot.Value : 0) * Child + (bookRow.ArrInfTot.HasValue ? bookRow.ArrInfTot.Value : 0) * Infant;
                        }
                    }
                }
                */
                #endregion

                flights = ResData.ResService;
                ServiceID = ResData.ResService.LastOrDefault() != null ? ResData.ResService.LastOrDefault().RecID : -1;
                CompResServ = flights.Find(f => f.RecID == ServiceID);
                AllotType = bookRow.AllotType;
                flySeat = Convert.ToInt16(Adult);
                flyMaxInfAge = Convert.ToDecimal(199 / 100);
                flyInfSeat = 0;

                Supplier = string.Empty;
                Status = 0;
                FreeSeat = 0;
                new Flights().checkFlightSeat(UserData, CompResServ.Service, ServiceID, CompResServ.BegDate.Value, ResData.ResMain.BegDate.Value, ResData, ref flySeat, ref flyMaxInfAge, ref flyInfSeat, SelectCust, true, ref errorMsg);
                CompResServ.Unit = flySeat;
                if (!(new Flights().CheckFlightAllot(UserData, bookRow.Operator, null, bookRow.ArrFlight, bookRow.ArrDate.Value, bookRow.ArrClass, flySeat,
                        ref Status, ref FreeSeat, ref errorMsg, "H", bookRow.ArrSupp)))
                {
                    return _ResData;
                }
            }

            for (int i = 0; i < ResData.SelectBook.Count; i++)
            {
                BookID = ResData.SelectBook[i].RefNo;
                List<plResServiceRecord> CompService = new List<plResServiceRecord>();
                CompService = new Reservation().GetCompulsoryResService(UserData, ResData, BookID, ref errorMsg);

                if (CompService != null)
                {
                    foreach (plResServiceRecord row in CompService)
                    {
                        #region Create ResService
                        int RowCount = 0;
                        List<ResServiceRecord> compResServ = (from q in ResData.ResService
                                                              where q.Service == row.Service && q.ServiceType == row.ServiceType
                                                              select q).ToList<ResServiceRecord>();
                        List<ResServiceRecord> ResService = ResData.ResService;
                        List<ResConRecord> ResCon = ResData.ResCon;
                        ResServiceRecord resService = ResService.Find(f => f.ServiceType == row.ServiceType && f.Service == row.Service);
                        if (compResServ.Count() > 0)
                            RowCount = compResServ.Count();

                        ServiceID = -1;

                        if (RowCount > 0)
                        {
                            SelectCust = new List<SelectCustRecord>();
                            var Cst = from Rc in ResData.ResCust
                                      where Rc.BookID == BookID && Rc.Status == 0
                                      select Rc;
                            if (SelectCust == null)
                            {
                                SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);

                                for (int selectCustCount = 0; selectCustCount < SelectCust.Count; selectCustCount++)
                                    if (Cst.Where(w => w.CustNo == SelectCust[selectCustCount].CustNo).Count() > 0)
                                        SelectCust[selectCustCount].Selected = true;
                            }
                            else
                                if (SelectCust.Count < 1)
                                {
                                    SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);

                                    for (int selectCustCount = 0; selectCustCount < SelectCust.Count; selectCustCount++)
                                        if (Cst.Where(w => w.CustNo == SelectCust[selectCustCount].CustNo).Count() > 0)
                                            SelectCust[selectCustCount].Selected = true;
                                }
                            int Unit = 1;

                            var resControl = from q1 in ResData.ResService
                                             join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                             join q3 in SelectCust on q2.CustNo equals q3.CustNo
                                             where q3.Selected == true &&
                                                q1.ServiceType == row.ServiceType &&
                                                q1.Service == row.Service
                                             select q1;

                            if (resControl.Count() > 0)
                            {
                                //errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "DuplicateService").ToString();
                            }
                            else
                            {
                                Unit = Convert.ToInt16(Convert.ToInt16(Cst.Where(w => w.Title < 6 && w.BookID == BookID && w.Status == 0).Count()) + Cst.Where(w => w.Title > 5 && w.BookID == BookID && w.Status == 0).Count());
                                resService.Unit += Convert.ToInt16(Unit);
                                resService.Adult += Convert.ToInt16(Cst.Where(w => w.Title < 6 && w.BookID == BookID && w.Status == 0).Count());
                                resService.Child += Convert.ToInt16(Cst.Where(w => w.Title > 5 && w.BookID == BookID && w.Status == 0).Count());
                                ServiceID = resService.RecID;
                                ResCon = new Reservation().AddResCon(UserData, ResData, null, BookID, ServiceID, true, ref errorMsg);
                            }
                            if (errorMsg != "")
                            {
                                //errorMsg = GetGlobalResourceObject(UserData.Rows[0]["Lang"].ToString(), "ReservationMsg31").ToString() + " ( " + errorMsg + " )";
                                return ResData;
                            }
                        }
                        else
                        {
                            ResData.ResService = new Reservation().AddResService(UserData, ResData, row, BookID, SearchType.OnlyFlightSearch, true, false, false, null, ref errorMsg);
                            ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                            if (errorMsg != "")
                                return ResData;
                            ResData.ResCon = new Reservation().AddResCon(UserData, ResData, null, BookID, ServiceID, true, ref errorMsg);
                            if (errorMsg != "")
                                return ResData;
                        }
                        #endregion Create ResService

                        #region Create Compulsory And Include Extra Service

                        List<ResConExtRecord> resConExt = ResData.ResConExt;
                        bool seperate = false;
                        if (row.ResSeparate != null)
                            seperate = row.ResSeparate.Value;
                        ResData.ResServiceExt = new Reservation().AddResServiceExt(UserData, ResData, (seperate || RowCount == 0) ? ResData.ResService[ResData.ResService.Count - 1] : compResServ.FirstOrDefault(), ref resConExt, Convert.ToInt32(ResData.SelectBook[i].RefNo), -1, ServiceID, null, ref errorMsg);
                        //if (errorMsg != "") return ResData;
                        #endregion Create Compulsory And Include Extra Service
                    }
                }
            }

            List<ResServiceRecord> resServiceList = ResData.ResService;
            foreach (ResServiceRecord row in resServiceList)
                row.ExcludeService = false;
            List<ResServiceExtRecord> resServiceExtList = ResData.ResServiceExt;
            foreach (ResServiceExtRecord row in resServiceExtList)
                row.ExcludeService = false;

            #region Calculate ResCustPrice

            string ResNo = ResData.ResMain.ResNo;
            ResNo = ResNo.Length > 10 ? ResNo.Substring(0, 10) : ResNo;

            StringBuilder CalcStrSql = new Reservation().BuildCalcSqlString(ResData);

            CalcStrSql.Append("Declare  @PassEB bit \n");
            CalcStrSql.Append("Declare  @ErrCode SmallInt \n");
            CalcStrSql.Append("Declare @Supplier varchar(10) \n");
            CalcStrSql.Append("exec dbo.usp_Calc_Res_Price '" + ResNo + "', Default, Default, @PassEB OutPut, @ErrCode OutPut, 1 \n");
            CalcStrSql.Append("Select * From #ResMain \n");
            CalcStrSql.Append("Select * From #ResService \n");
            CalcStrSql.Append("Select * From #ResServiceExt \n");
            CalcStrSql.Append("Select * From #ResCust \n");
            CalcStrSql.Append("Select * From #ResCustPrice \n");
            CalcStrSql.Append("Select * From #ResSupDis \n");
            CalcStrSql.Append("Select ErrorCode=@ErrCode \n");

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(CalcStrSql.ToString());
            DataSet ds;
            try
            {
                ds = db.ExecuteDataSet(dbCommand);
                if (ds != null)
                {
                    ds.Tables[0].TableName = "ResMain";
                    ds.Tables[1].TableName = "ResService";
                    ds.Tables[2].TableName = "ResServiceExt";
                    ds.Tables[3].TableName = "ResCust";
                    ds.Tables[4].TableName = "ResCustPrice";
                    ds.Tables[5].TableName = "ResSupDis";
                    ds.Tables[6].TableName = "ErrorTable";
                }
                else
                {
                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationPriceNotCalculated").ToString();
                    ds = null;
                    return ResData;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                ds = null;
                return ResData;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
            #endregion Calculate ResCustPrice

            int ErrorCode = Convert.ToInt32(ds.Tables["ErrorTable"].Rows[0]["ErrorCode"].ToString());
            if (ErrorCode != 0)
            {
                errorMsg = new ResCalcError().calcError(ErrorCode, ResData);
                return ResData;
            }

            ResData = new Reservation().SetCalcDataToResData(UserData, ResData, ds, false, ref errorMsg);

            if (!CreateChildAge)
            {
                foreach (ResCustRecord row in ResData.ResCust)
                {
                    row.Age = null;
                    row.Birtday = null;
                }
            }

            return ResData;
        }

        public ResDataRecord getFlightTicket(User UserData, OnlyTicketCriteria criteria, OnlyFlight_FlightInfo bookRow, Int16? adult, Int16? child, Int16? infant, bool CreateChildAge, ref string errorMsg)
        {
            ResDataRecord ResData = new ResDataRecord();

            ResData.SelectBook = new OnlyTickets().FillSelectBook(UserData, ResData.SelectBook, bookRow, adult.HasValue ? adult.Value : Convert.ToInt16(1), child.HasValue ? child.Value : Convert.ToInt16(0), infant.HasValue ? infant.Value : Convert.ToInt16(0), ref errorMsg);

            ResData = new OnlyTickets().bookFlight(UserData, ResData, bookRow, adult.HasValue ? adult.Value : Convert.ToInt16(1), child.HasValue ? child.Value : Convert.ToInt16(0), infant.HasValue ? infant.Value : Convert.ToInt16(0), CreateChildAge, bookRow, ref errorMsg);

            return ResData;
        }

        public string getTicketHandicapsStr(User UserData, string Market, ResDataRecord ResData, ref string errorMsg)
        {
            StringBuilder sb = new StringBuilder();
            List<HandicapsRecord> handicaps = new List<HandicapsRecord>();
            List<HandicapsRecord> handicapsAirports = new List<HandicapsRecord>();
            foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "FLIGHT").OrderBy(o => o.BegDate))
            {
                handicaps = new TvBo.Common().getHandicaps(UserData, row.BegDate, null, "FLIGHT", row.Service, HandicapTypes.OnlyTicket, ref errorMsg);
                if (handicaps != null && handicaps.Count > 0)
                {
                    foreach (HandicapsRecord r in handicaps)
                    {
                        sb.Append("<b>" + row.ServiceNameL + ":</b>" + "<br />");
                        sb.Append("&nbsp;&nbsp;&nbsp;" + r.NameL);
                        sb.Append("<br />");
                    }
                }
                FlightDayRecord flight = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                handicapsAirports = new TvBo.Common().getHandicaps(UserData, row.BegDate, null, "AIRPORT", flight.DepAirport, HandicapTypes.OnlyTicket, ref errorMsg);
                if (handicapsAirports != null && handicapsAirports.Count > 0)
                {
                    foreach (HandicapsRecord r1 in handicapsAirports)
                    {
                        sb.Append("<b>" + flight.DepAirport + ":</b>" + "<br />");
                        sb.Append("&nbsp;&nbsp;&nbsp;" + r1.NameL);
                        sb.Append("<br />");
                    }
                }
                handicapsAirports = new TvBo.Common().getHandicaps(UserData, row.BegDate, null, "AIRPORT", flight.ArrAirport, HandicapTypes.OnlyTicket, ref errorMsg);
                if (handicapsAirports != null && handicapsAirports.Count > 0)
                {
                    foreach (HandicapsRecord r1 in handicapsAirports)
                    {
                        sb.Append("<b>" + flight.DepAirport + ":</b>" + "<br />");
                        sb.Append("&nbsp;&nbsp;&nbsp;" + r1.NameL);
                        sb.Append("<br />");
                    }
                }
            }
            return sb.ToString();
        }

        public bool? getUseOptForAllot(string Market, ref string errorMsg)
        {
            string tsql = @"if (Exists(Select isnull(UseOptForAllot,'N') UseOptForAllot From ParamFlight (NOLOCK) Where Market=@Market) And 
                                Exists(Select * From ParamFlight (NOLOCK) Where Market=@Market and UseSysParam<>'Y')) 
                                 Select UseOptForAllot From ParamFlight (NOLOCK) Where Market=@Market 
                            Else Select UseOptForAllot From ParamFlight (NOLOCK) Where Market='' ";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                return Equals(db.ExecuteScalar(dbCommand), "Y");
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<OnlyFlight_AdServices> getAdditionalComplusoryServices(User UserData, int? Country, int? Location, DateTime? FlyDate, Int16? Night, Int16? Pax, ref string errorMsg)
        {
            string tsql = @"Select Distinct A.Service,A.Code  
                            From AdService A (NOLOCK) 
                            Join AdServicePrice P (NOLOCK) On P.Service=A.Service and P.AdService=A.Code 
                            Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups = P.AgencyGrp and AG.GrpType='P' 
                            Where Compulsory=1 and Market=@Market and 
                              (Country=@Country or Country is null) and 
                              (Location=@Location or Location is Null) and 
                              @FlyDate between BegDate and EndDate and 
                              dbo.DateOnly(GETDATE()) between SaleBegDate and SaleEndDate and 
                              @Night between IsNull(FromDay,0) and IsNull(ToDay,999) and 
                              @Pax between IsNull(FromPax,0) and IsNull(ToPax,999) and 
                              ( (P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null) ) 
                           ";

            List<OnlyFlight_AdServices> list = new List<OnlyFlight_AdServices>();

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, Country);
                db.AddInParameter(dbCommand, "Location", DbType.Int32, Location);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "Night", DbType.Int16, Night);
                db.AddInParameter(dbCommand, "Pax", DbType.Int16, Pax);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        OnlyFlight_AdServices record = new OnlyFlight_AdServices();
                        record.Code = Conversion.getStrOrNull(oReader["Code"]);
                        record.Service = Conversion.getStrOrNull(oReader["Service"]);
                        list.Add(record);
                    }
                }
                return list;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public OnlyFlight_AdditionalService getAdditionalComplusoryService(User UserData, string Service, string Code, int? Country, int? Location, DateTime? FlyDate, Int16? Night, Int16? Pax, ref string errorMsg)
        {
            string tsql = @"Select Top 1 A.Service,A.Code,Name=A.Name,NameL=isnull(dbo.FindLocalName(A.NameLID,@Market),A.Name),Supplier,NetAdlPrice,NetChdG1Price,NetChdG2Price,NetChdG3Price,ChdG1MaxAge,ChdG2MaxAge,ChdG2MaxAge,NetCur,
                              P.SaleAdlPrice,P.SaleChdG1Price,P.SaleChdG2Price,P.SaleChdG3Price,P.SaleCur,CalcType,PriceType,A.PayCom,A.PayComEB,A.PayPasEB,A.CanDiscount,A.ConfStat,P.RecID 
                            From AdService A (NOLOCK) 
                            Join AdServicePrice P (NOLOCK) On P.Service=A.Service and P.AdService=A.Code 
                            Left Join GrpAgencyDet AG (NOLOCK) on AG.Groups = P.AgencyGrp and AG.GrpType='P' 
                            Where Compulsory=1 and Market=@Market and 
                              A.Service=@Service and
                              A.Code=@Code and
                              (Country=@Country or Country is null) and 
                              (Location=@Location or Location is Null) and 
                              @FlyDate between BegDate and EndDate and 
                              dbo.DateOnly(GETDATE()) between SaleBegDate and SaleEndDate and 
                              @Night between IsNull(FromDay,0) and IsNull(ToDay,999) and 
                              @Pax between IsNull(FromPax,0) and IsNull(ToPax,999) and 
                              ( (P.Agency=@Agency or AG.Agency=@Agency) or (P.Agency='' and AG.Agency is Null) )                               
                           ";

            List<OnlyFlight_AdditionalService> list = new List<OnlyFlight_AdditionalService>();

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Service", DbType.String, Service);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, Country);
                db.AddInParameter(dbCommand, "Location", DbType.Int32, Location);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "Night", DbType.Int16, Night);
                db.AddInParameter(dbCommand, "Pax", DbType.Int16, Pax);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        OnlyFlight_AdditionalService record = new OnlyFlight_AdditionalService();
                        record.ArrDep = string.Empty;
                        record.CalcType = Conversion.getInt16OrNull(oReader["CalcType"]);
                        record.Extra = Conversion.getStrOrNull(oReader["Code"]);
                        if (!string.IsNullOrEmpty(oReader["ChdG1MaxAge"].ToString()))
                        {
                            record.ExtraInfNet = Conversion.getDecimalOrNull(oReader["NetChdG1Price"]);
                            record.ExtraInfSale = Conversion.getDecimalOrNull(oReader["SaleChdG1Price"]);
                        }
                        if (!string.IsNullOrEmpty(oReader["ChdG2MaxAge"].ToString()))
                        {
                            record.ExtraChdNet = Conversion.getDecimalOrNull(oReader["NetChdG2Price"]);
                            record.ExtraChdSale = Conversion.getDecimalOrNull(oReader["SaleChdG2Price"]);
                        }
                        else
                        {
                            record.ExtraChdNet = Conversion.getDecimalOrNull(oReader["NetAdlPrice"]);
                            record.ExtraChdSale = Conversion.getDecimalOrNull(oReader["SaleAdlPrice"]);
                        }
                        record.ExtraName = Conversion.getStrOrNull(oReader["NameL"]);
                        record.ExtraNet = Conversion.getDecimalOrNull(oReader["NetAdlPrice"]);
                        record.ExtraSale = Conversion.getDecimalOrNull(oReader["SaleAdlPrice"]);
                        record.MainService = Conversion.getStrOrNull(oReader["Service"]);
                        record.NetCur = Conversion.getStrOrNull(oReader["NetCur"]);
                        record.PayCom = Conversion.getStrOrNull(oReader["PayCom"]);
                        record.PayComEB = Conversion.getStrOrNull(oReader["PayComEB"]);
                        record.PayPasEB = Conversion.getBoolOrNull(oReader["PayPasEB"]);
                        record.PriceType = Conversion.getInt16OrNull(oReader["PriceType"]);
                        record.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        record.SaleCur = Conversion.getStrOrNull(oReader["SaleCur"]);
                        record.StatConf = Conversion.getInt16OrNull(oReader["ConfStat"]);
                        record.Supplier = Conversion.getStrOrNull(oReader["Supplier"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool? getUseOnlyUnionPrice(User UserData, ref string errorMsg)
        {
            string tsql = @"Declare @UseSysParamOnlyTicket VarChar(1), @UseTicPriceForOTic bit
                            Select Top 1 @UseSysParamOnlyTicket=UseSysParamOnlyTicket, @UseTicPriceForOTic=UseTicPriceForOTic From Agency (NOLOCK) Where Code = @Agency
                            if @UseSysParamOnlyTicket='Y'
                            Begin
                              if Exists(Select * from ParamFlight (NOLOCK) Where Market=@Market) and 
                                 Exists(Select * from ParamFlight (NOLOCK) Where Market=@Market and UseSysParam<>'Y') 
                                 Select top 1 @UseTicPriceForOTic=UseTicPriceForOTic from ParamFlight (NOLOCK) Where Market=@Market 
                               else 
                                 Select top 1 @UseTicPriceForOTic=UseTicPriceForOTic from ParamFlight (NOLOCK) Where Market=''
                            End
                            Select @UseTicPriceForOTic
                            ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                return Conversion.getBoolOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }

    public class FlightSeatCheckIn
    {
        public PlaneTemp getPlaneTemp(User UserData, string ResNo, string FlightNo, DateTime? FlyDate, ref string errorMsg)
        {
            string tsql =
@"
Select PTmp.RecID,PTmp.Code,PTmp.PlaneName,PTmp.Exits,PTmp.SkipList
From ResService S (NOLOCK)
Left Join FlightDay Fd (NOLOCK) ON S.[Service]=Fd.FlightNo And S.BegDate=Fd.FlyDate
Left Join Flight F (NOLOCK) ON F.Code=S.[Service]
Left Join PlaneType Pt (NOLOCK) ON F.PlaneType=Pt.RecID
Join PlaneTemp PTmp (NOLOCK) ON isnull(Fd.PlaneTemp,Pt.PlaneTemp)=PTmp.RecID
Where S.ServiceType='FLIGHT'
  And S.ResNo=@ResNo
  And S.[Service]=@FlightNo
  And S.BegDate=@FlyDate
";
            /*
            string tsql =
@"
Select PTmp.RecID,PTmp.Code,PTmp.PlaneName,PTmp.Exits,PTmp.SkipList
From ResService S (NOLOCK)
Join FlightDay Fd (NOLOCK) ON S.[Service]=Fd.FlightNo And S.BegDate=Fd.FlyDate
Join PlaneTemp PTmp (NOLOCK) ON Fd.PlaneTemp=PTmp.RecID
Where S.ServiceType='FLIGHT'
  And S.ResNo=@ResNo
  And S.[Service]=@FlightNo
  And S.BegDate=@FlyDate
";
            */
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.AnsiString, ResNo);
                db.AddInParameter(dbCommand, "FlightNo", DbType.AnsiString, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        PlaneTemp planeZone = new PlaneTemp();
                        planeZone.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        planeZone.Code = Conversion.getStrOrNull(R["Code"]);
                        planeZone.PlaneName = Conversion.getStrOrNull(R["PlaneName"]);
                        planeZone.Exits = Conversion.getStrOrNull(R["Exits"]);
                        planeZone.SkipList = Conversion.getStrOrNull(R["SkipList"]);
                        return planeZone;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PlaneTempZone> getPlaneTempZone(User UserData, int? PlaneId, ref string errorMsg)
        {
            List<PlaneTempZone> planeTempZoneList = new List<PlaneTempZone>();

            string tsql =
@"
Select RecID,PlaneID,ZoneName,RangeStart,RangeEnd,ItemLength
From PlaneTempZone (NOLOCK)
Where PlaneID=@PlaneID
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "PlaneID", DbType.AnsiString, PlaneId);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        PlaneTempZone planeTempZone = new PlaneTempZone();
                        planeTempZone.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        planeTempZone.PlaneID = Conversion.getInt32OrNull(R["PlaneID"]);
                        planeTempZone.ZoneName = Conversion.getStrOrNull(R["ZoneName"]);
                        planeTempZone.RangeStart = Conversion.getInt16OrNull(R["RangeStart"]);
                        planeTempZone.RangeEnd = Conversion.getInt16OrNull(R["RangeEnd"]);
                        planeTempZone.ItemLength = Conversion.getInt16OrNull(R["ItemLength"]);
                        planeTempZoneList.Add(planeTempZone);
                    }
                    return planeTempZoneList;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<string> getPlaneTempZoneExtraServices(User UserData, int? PlaneId, ref string errorMsg)
        {

            List<string> planeTempZoneItemList = new List<string>();

            string tsql =
@"
Select Distinct ExtraService
From PlaneTempZoneItems (NOLOCK)
Where ExtraService is not null And PlaneID=@PlaneID
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "PlaneID", DbType.AnsiString, PlaneId);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {

                    }
                    return planeTempZoneItemList;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PlaneTempZoneItems> getPlaneTempZoneItems(User UserData, int? PlaneId, ref string errorMsg)
        {

            List<PlaneTempZoneItems> planeTempZoneItemList = new List<PlaneTempZoneItems>();

            string tsql =
@"
Select RecID,PlaneID,ZoneId,SeatLetter,ItemRow,ItemCol,ItemType,ForB2B,ForB2C,IsExit,SeatNo,ForBackOffice,ExtraLegroom,ExtraService
From PlaneTempZoneItems (NOLOCK)
Where PlaneID=@PlaneID
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "PlaneID", DbType.AnsiString, PlaneId);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        PlaneTempZoneItems planeTempZone = new PlaneTempZoneItems();
                        planeTempZone.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        planeTempZone.PlaneID = Conversion.getInt32OrNull(R["PlaneID"]);
                        planeTempZone.ZoneId = Conversion.getInt32OrNull(R["ZoneId"]);
                        planeTempZone.SeatLetter = Conversion.getStrOrNull(R["SeatLetter"]);
                        planeTempZone.ItemRow = Conversion.getInt16OrNull(R["ItemRow"]);
                        planeTempZone.ItemCol = Conversion.getInt16OrNull(R["ItemCol"]);
                        Int16? itemType = Conversion.getInt16OrNull(R["ItemType"]);
                        planeTempZone.ItemType = itemType.HasValue ? (enmItemType)itemType.Value : enmItemType.Seat;
                        planeTempZone.ForB2B = Conversion.getBoolOrNull(R["ForB2B"]);
                        planeTempZone.ForB2C = Conversion.getBoolOrNull(R["ForB2C"]);
                        planeTempZone.IsExit = Conversion.getBoolOrNull(R["IsExit"]);
                        planeTempZone.SeatNo = Conversion.getInt16OrNull(R["SeatNo"]);
                        planeTempZone.ForBackOffice = Conversion.getBoolOrNull(R["ForBackOffice"]);
                        planeTempZone.ExtraLegroom = Conversion.getBoolOrNull(R["ExtraLegroom"]);
                        planeTempZone.ExtraService = Conversion.getStrOrNull(R["ExtraService"]);
                        planeTempZoneItemList.Add(planeTempZone);
                    }
                    return planeTempZoneItemList;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public PlaneTempZoneItems getPlaneTempZoneItemForService(User UserData, int? ServiceID, string seatLetter, Int16? seatNo, ref string errorMsg)
        {
            string tsql =
@"
Select TZI.RecID,TZI.PlaneID,TZI.ZoneId,TZI.SeatLetter,TZI.ItemRow,TZI.ItemCol,TZI.ItemType,TZI.ForB2B,TZI.ForB2C,TZI.IsExit,TZI.SeatNo,
       TZI.ForBackOffice,TZI.ExtraLegroom,TZI.ExtraService
From PlaneTempZoneItems TZI (NOLOCK)
Join PlaneTemp PT (NOLOCK) ON PT.RecID=TZI.PlaneID
Join (
  Select Distinct TempID=isnull(FD.PlaneTemp,PType.PlaneTemp) From ResService S
  Left Join FlightDay FD (NOLOCK) ON FD.FlightNo=S.[Service] And FD.FlyDate=S.BegDate
  Left Join Flight F (NOLOCK) ON F.Code=S.[Service] 
  Left Join PlaneType PType (NOLOCK) ON PType.RecID=F.PlaneType  
  Where S.RecID=@RecID 
) PTmp ON PTmp.TempID=PT.RecID
Where TZI.SeatLetter=@seatLetter
  And TZI.SeatNo=@seatNo
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "RecID", DbType.Int32, ServiceID);
                db.AddInParameter(dbCommand, "seatLetter", DbType.AnsiString, seatLetter);
                db.AddInParameter(dbCommand, "seatNo", DbType.Int16, seatNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        PlaneTempZoneItems planeTempZone = new PlaneTempZoneItems();
                        planeTempZone.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        planeTempZone.PlaneID = Conversion.getInt32OrNull(R["PlaneID"]);
                        planeTempZone.ZoneId = Conversion.getInt32OrNull(R["ZoneId"]);
                        planeTempZone.SeatLetter = Conversion.getStrOrNull(R["SeatLetter"]);
                        planeTempZone.ItemRow = Conversion.getInt16OrNull(R["ItemRow"]);
                        planeTempZone.ItemCol = Conversion.getInt16OrNull(R["ItemCol"]);
                        Int16? itemType = Conversion.getInt16OrNull(R["ItemType"]);
                        planeTempZone.ItemType = itemType.HasValue ? (enmItemType)itemType.Value : enmItemType.Seat;
                        planeTempZone.ForB2B = Conversion.getBoolOrNull(R["ForB2B"]);
                        planeTempZone.ForB2C = Conversion.getBoolOrNull(R["ForB2C"]);
                        planeTempZone.IsExit = Conversion.getBoolOrNull(R["IsExit"]);
                        planeTempZone.SeatNo = Conversion.getInt16OrNull(R["SeatNo"]);
                        planeTempZone.ForBackOffice = Conversion.getBoolOrNull(R["ForBackOffice"]);
                        planeTempZone.ExtraLegroom = Conversion.getBoolOrNull(R["ExtraLegroom"]);
                        planeTempZone.ExtraService = Conversion.getStrOrNull(R["ExtraService"]);

                        return planeTempZone;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PlaneTempZoneItems> getPlaneTempZoneItemsForService(User UserData, int? ServiceID, ref string errorMsg)
        {

            List<PlaneTempZoneItems> planeTempZoneItemList = new List<PlaneTempZoneItems>();
            /*
            Select TZI.RecID,TZI.PlaneID,TZI.ZoneId,TZI.SeatLetter,TZI.ItemRow,TZI.ItemCol,TZI.ItemType,TZI.ForB2B,TZI.ForB2C,TZI.IsExit,TZI.SeatNo,
              TZI.ForBackOffice,TZI.ExtraLegroom,TZI.ExtraService
            From PlaneTempZoneItems TZI (NOLOCK)
            Join PlaneTemp PT (NOLOCK) ON PT.RecID=TZI.PlaneID
            Join FlightDay FD (NOLOCK) ON FD.PlaneTemp=PT.RecID
            Join ResService S (NOLOCK) ON S.BegDate=FD.FlyDate And S.[Service]=FD.FlightNo
            Where S.RecID=@RecID
             */
            string tsql =
@"
Select TZI.RecID,TZI.PlaneID,TZI.ZoneId,TZI.SeatLetter,TZI.ItemRow,TZI.ItemCol,TZI.ItemType,TZI.ForB2B,TZI.ForB2C,TZI.IsExit,TZI.SeatNo,
  TZI.ForBackOffice,TZI.ExtraLegroom,TZI.ExtraService
From PlaneTempZoneItems TZI (NOLOCK)
Join PlaneTemp PT (NOLOCK) ON PT.RecID=TZI.PlaneID
Join (
  Select Distinct TempID=isnull(FD.PlaneTemp,PType.PlaneTemp) From ResService S
  Left Join FlightDay FD (NOLOCK) ON FD.FlightNo=S.[Service] And FD.FlyDate=S.BegDate
  Left Join Flight F (NOLOCK) ON F.Code=S.[Service] 
  Left Join PlaneType PType (NOLOCK) ON PType.RecID=F.PlaneType  
  Where S.RecID=@RecID 
) PTmp ON PTmp.TempID=PT.RecID
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "RecID", DbType.AnsiString, ServiceID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        PlaneTempZoneItems planeTempZone = new PlaneTempZoneItems();
                        planeTempZone.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        planeTempZone.PlaneID = Conversion.getInt32OrNull(R["PlaneID"]);
                        planeTempZone.ZoneId = Conversion.getInt32OrNull(R["ZoneId"]);
                        planeTempZone.SeatLetter = Conversion.getStrOrNull(R["SeatLetter"]);
                        planeTempZone.ItemRow = Conversion.getInt16OrNull(R["ItemRow"]);
                        planeTempZone.ItemCol = Conversion.getInt16OrNull(R["ItemCol"]);
                        Int16? itemType = Conversion.getInt16OrNull(R["ItemType"]);
                        planeTempZone.ItemType = itemType.HasValue ? (enmItemType)itemType.Value : enmItemType.Seat;
                        planeTempZone.ForB2B = Conversion.getBoolOrNull(R["ForB2B"]);
                        planeTempZone.ForB2C = Conversion.getBoolOrNull(R["ForB2C"]);
                        planeTempZone.IsExit = Conversion.getBoolOrNull(R["IsExit"]);
                        planeTempZone.SeatNo = Conversion.getInt16OrNull(R["SeatNo"]);
                        planeTempZone.ForBackOffice = Conversion.getBoolOrNull(R["ForBackOffice"]);
                        planeTempZone.ExtraLegroom = Conversion.getBoolOrNull(R["ExtraLegroom"]);
                        planeTempZone.ExtraService = Conversion.getStrOrNull(R["ExtraService"]);
                        planeTempZoneItemList.Add(planeTempZone);
                    }
                    return planeTempZoneItemList;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<FlightCIn> getFlightSeatCIn(User UserData, string FlightNo, DateTime? FlyDate, ref string errorMsg)
        {
            List<FlightCIn> flightCInList = new List<FlightCIn>();
            string tsql =
@"
Select RecID,Flight,FlightDate,FlightTemp,CustNo,FlightDay,SeatLetter,SeatNo,ServiceID,ServiceExtID
From FlightCIn (NOLOCK)
Where Flight=@FlightNo
  And FlightDate=@FlyDate
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.AnsiString, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        FlightCIn flightCIn = new FlightCIn();
                        flightCIn.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        flightCIn.Flight = Conversion.getStrOrNull(R["Flight"]);
                        flightCIn.FlightDate = Conversion.getDateTimeOrNull(R["FlightDate"]);
                        flightCIn.FlightTemp = Conversion.getInt32OrNull(R["FlightTemp"]);
                        flightCIn.CustNo = Conversion.getInt32OrNull(R["CustNo"]);
                        flightCIn.FlightDay = Conversion.getInt16OrNull(R["FlightDay"]);
                        flightCIn.SeatLetter = Conversion.getStrOrNull(R["SeatLetter"]);
                        flightCIn.SeatNo = Conversion.getInt16OrNull(R["SeatNo"]);
                        flightCIn.ServiceID = Conversion.getInt32OrNull(R["ServiceID"]);
                        flightCIn.ServiceExtID = Conversion.getInt32OrNull(R["ServiceExtID"]);
                        flightCInList.Add(flightCIn);
                    }
                    return flightCInList;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool checkExitsFlightCIn(User UserData, string FlightNo, DateTime? FlyDate, string SeatLetter, Int16? SeatNo, ref string errorMsg)
        {
            string tsql =
@"
Select RecID
From FlightCIn (NOLOCK)
Where Flight=@FlightNo
  And FlightDate=@FlyDate
  And SeatLetter=@SeatLetter
  And SeatNo=@SeatNo
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.AnsiString, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "SeatLetter", DbType.AnsiString, SeatLetter);
                db.AddInParameter(dbCommand, "SeatNo", DbType.Int16, SeatNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return true;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool SaveUsedSeatExtras(User UserData, ref ResDataRecord ResData, ref List<FlightCIn> usedSeats, ResServiceRecord flightService, ref string errorMsg)
        {
            bool onlyTicket = ResData.ResMain.SaleResource.HasValue && (new List<short> { 2, 3, 5 }).Contains(ResData.ResMain.SaleResource.Value);
            List<ExtraServiceListRecord> extPrices = new Reservation().getExtraServicesForFlight(UserData, ResData.ResMain.PLMarket, ResData.ResMain.ResDate, ResData.ResMain.SaleCur, flightService.FlgClass, flightService.BegDate, flightService.Duration, false, flightService.Service, onlyTicket, string.Empty, true, ref errorMsg);
            foreach (FlightCIn usedSeat in usedSeats)
            {
                if (extPrices != null)
                {
                    PlaneTempZoneItems custSeat = new FlightSeatCheckIn().getPlaneTempZoneItemForService(UserData, flightService.RecID, usedSeat.SeatLetter, usedSeat.SeatNo, ref errorMsg);

                    ExtraServiceListRecord extPrice = extPrices.Find(f => (string.IsNullOrEmpty(f.ServiceItem) || f.ServiceItem == usedSeat.Flight) && f.Code == custSeat.ExtraService);
                    if (extPrice != null)
                        usedSeat.ServiceExtID = extPrice.RecID;
                }

                List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
                SelectCust.Find(f => f.CustNo == usedSeat.CustNo).Selected = true;

                if (usedSeat.ServiceExtID.HasValue)
                {
                    ResData = new Reservation().setExtraService(UserData, ResData, SelectCust, usedSeat.ServiceID, usedSeat.ServiceExtID, 1, true, string.Empty, true, ref errorMsg);
                    if (!string.IsNullOrEmpty(errorMsg))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool SaveSeatCheckIn(User UserData, FlightCIn usedSeat, Database db, DbTransaction dbT, ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);

            string tsql =
@"
INSERT INTO FlightCIn (Flight,FlightDate,FlightTemp,CustNo,FlightDay,SeatLetter,SeatNo,ServiceID,ServiceExtID)
               VALUES (@Flight,@FlightDate,@FlightTemp,@CustNo,@FlightDay,@SeatLetter,@SeatNo,@ServiceID,@ServiceExtID)
";
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "Flight", DbType.AnsiString, usedSeat.Flight);
                db.AddInParameter(dbCommand, "FlightDate", DbType.DateTime, usedSeat.FlightDate);
                db.AddInParameter(dbCommand, "FlightTemp", DbType.Int32, usedSeat.FlightTemp);
                db.AddInParameter(dbCommand, "CustNo", DbType.Int32, usedSeat.CustNo);
                db.AddInParameter(dbCommand, "FlightDay", DbType.Int32, usedSeat.FlightDay);
                db.AddInParameter(dbCommand, "SeatLetter", DbType.AnsiString, usedSeat.SeatLetter);
                db.AddInParameter(dbCommand, "SeatNo", DbType.Int16, usedSeat.SeatNo);
                db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, usedSeat.ServiceID);
                db.AddInParameter(dbCommand, "ServiceExtID", DbType.Int32, usedSeat.ServiceExtID);

                db.ExecuteNonQuery(dbCommand, dbT);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
        }
        public bool UpdateSeatCheckIn(User UserData, FlightCIn usedSeat,ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            Database db = DatabaseFactory.CreateDatabase();
            string tsql =
@"
Update FlightCIn SET ServiceExtID=@ServiceExtID
               where ServiceID=@ServiceID and CustNo=@CustNo and Flight=@Flight
";
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "Flight", DbType.AnsiString, usedSeat.Flight);
                db.AddInParameter(dbCommand, "CustNo", DbType.Int32, usedSeat.CustNo);
                db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, usedSeat.ServiceID);
                db.AddInParameter(dbCommand, "ServiceExtID", DbType.Int32, usedSeat.ServiceExtID);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
        }

        public bool SaveSeatCheckInList(User UserData, ref List<FlightCIn> usedSeats, ResServiceRecord flightService, Database db, DbTransaction dbT, ref string errorMsg)
        {
            foreach (FlightCIn row in usedSeats)
            {
                bool seatUse = new FlightSeatCheckIn().checkExitsFlightCIn(UserData, flightService.Service, flightService.BegDate, row.SeatLetter, row.SeatNo, ref errorMsg);
                if (seatUse)
                {
                    if (!SaveSeatCheckIn(UserData, row, db, dbT, ref errorMsg))
                        return false;
                }
                else
                {
                    errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgSeatCheckInNoSeat").ToString();
                    return false;
                }
            }
            return true;
        }

        public List<ReservastionSaveErrorRecord> saveUserSeats(User UserData, ref ResDataRecord ResData, List<FlightCIn> usedSeats, ResServiceRecord flightService, ref string errorMsg)
        {
            string ResNo = ResData.ResMain.ResNo;
            string retVal = string.Empty;
            bool isModify = false;
            bool reCalc = false;

            List<ReservastionSaveErrorRecord> SaveResReport = new List<ReservastionSaveErrorRecord>();
            if (!SaveUsedSeatExtras(UserData, ref ResData, ref usedSeats, flightService, ref errorMsg))
            {
                SaveResReport.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = errorMsg });
                return SaveResReport;
            }
            ResDataRecord tResData = ResData;
            Database db = DatabaseFactory.CreateDatabase();
            using (DbConnection conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction transaction = conn.BeginTransaction(IsolationLevel.ReadUncommitted);
                try
                {
                    #region FlightCheckinExtraServiceId Düzenleme
                    //usedSeats.ForEach(f=>{f.ServiceExtID=ResData.ResServiceExt.Where(w=>w.se)})
                    int _seat = 0;
                    usedSeats.ForEach(f =>
                    {
                        var serviceExt = tResData.ResServiceExt.Where(w => w.BegDate == f.FlightDate && w.PriceID == f.ServiceExtID && w.RecStatus == RecordStatus.New).Skip(_seat).FirstOrDefault();
                        if (serviceExt != null)
                            f.ServiceExtID = serviceExt.RecordID;
                        _seat++;
                    });
                    #endregion
                    if (!SaveSeatCheckInList(UserData, ref usedSeats, flightService, db, transaction, ref errorMsg))
                    {
                        SaveResReport.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = errorMsg });
                    }
                    else
                        if (!new Reservation().SaveResServiceExt(UserData, ref ResData, db, transaction, ref isModify, ref reCalc, ref errorMsg))
                        {
                            SaveResReport.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = errorMsg });
                        }
                        else
                            if (!new Reservation().SaveResConExt(UserData, ref ResData, db, transaction, ref isModify, ref errorMsg))
                            {
                                SaveResReport.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = errorMsg });
                            }
                            else
                                if (!new Reservation().SaveResMain(UserData, ref ResData, db, transaction, ref isModify, ref errorMsg))
                                {
                                    SaveResReport.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = errorMsg });
                                }

                    if (SaveResReport.Count > 0)
                    {
                        SaveResReport.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = errorMsg });
                        transaction.Rollback();
                    }
                    else
                    {
                        retVal = new Reservation().CalcReservationPriceOldReservation(UserData, ref ResData, db, transaction, false, true);
                        if (retVal != "NO")
                        {
                            if (retVal != "")
                            {
                                SaveResReport.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = retVal });
                                transaction.Rollback();
                            }
                            else
                            {
                                SaveResReport.Add(new ReservastionSaveErrorRecord
                                {
                                    ControlOK = true,
                                    Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(), ResNo)
                                });

                                if (new Reservation().statusControl(UserData, ResNo, db, transaction, ref errorMsg))
                                    transaction.Commit();
                                else
                                {
                                    retVal = errorMsg;
                                    SaveResReport.Clear();
                                    SaveResReport.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = retVal });
                                    transaction.Rollback();
                                }
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    transaction.Rollback();
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }
            return SaveResReport;
        }
    }
}

