﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Web;
using TvTools;

namespace TvBo
{
    public class Transports
    {
        public BusRecord getBusRecord(string Market, string BusCode, ref string errorMsg)
        {
            BusRecord record = new BusRecord();
            string tsql = @"Select RecID, Code, Name, Plate, Seat, Producer, Model, ModelYear, Explanation
                            From Bus (NOLOCK)
                            Where Code=@BusCode";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "BusCode", DbType.String, BusCode);
                using (IDataReader rdr = db.ExecuteReader(dbCommand)) {
                    if (rdr.Read()) {
                        record.RecID = (int)rdr["RecID"];
                        record.Code = Conversion.getStrOrNull(rdr["Code"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.Plate = Conversion.getStrOrNull(rdr["Plate"]);
                        record.Seat = Conversion.getInt16OrNull(rdr["Seat"]);
                        record.Producer = Conversion.getStrOrNull(rdr["Producer"]);
                        record.Model = Conversion.getStrOrNull(rdr["Model"]);
                        record.ModelYear = Conversion.getStrOrNull(rdr["ModelYear"]);
                        record.Explanation = Conversion.getStrOrNull(rdr["Explanation"]);
                    }
                }
                return record;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getBus(string Market, string BusCode, ref string errorMsg)
        {
            BusRecord rec = new BusRecord();
            rec = getBusRecord(Market, BusCode, ref errorMsg);
            return rec != null ? rec.Name : string.Empty;
        }

        public string getBus(string Transport, DateTime TrnsDate, ref string errorMsg)
        {
            string tsql = @"Select Top 1 Bus
		                    From TPDayRouteV (NOLOCK)
		                    Where		 
		                     Exists (Select RecID From TransportPrice Where Transport=Transport) And		                     
		                     Transport=@Transport And
		                     TransDate=@Date
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try {
                db.AddInParameter(dbCommand, "Transport", DbType.String, Transport);
                db.AddInParameter(dbCommand, "Date", DbType.DateTime, TrnsDate);

                return (string)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getBusForCatPackSer(int? CatPackId, DateTime TrnsDate, ref string errorMsg)
        {
            string tsql =
@"
Select Top 1 B.Plate
From CatPackPlanSer Cps (NOLOCK)
Join TransportDay Td (NOLOCK) ON Td.Transport=Cps.ServiceItem
Join Bus B (NOLOCK) ON Td.Bus=B.Code
Where Cps.CatPackID=@CatPackId And Cps.[Service]='TRANSPORT' And TransDate=@TransportDay
Order By Cps.StepNo
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try {
                db.AddInParameter(dbCommand, "CatPackId", DbType.Int32, CatPackId);
                db.AddInParameter(dbCommand, "TransportDay", DbType.DateTime, TrnsDate);

                return (string)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransportRecord> getTransportForRoutes(string Market, string plMarket, int? Departure, int? Arrival, DateTime? TransDate, ref string errorMsg)
        {
            List<TransportRecord> records = new List<TransportRecord>();
            string tsql = @"Select T.RecID,T.TpType,T.Code,T.Name,NameL=isnull(dbo.FindLocalName(T.NameLID, @Market),T.Name),
                                T.NameS,T.Operator,T.Departure,
                                DepartureName=dbo.GetLocationNameL(T.Departure, @Market),
                                T.Arrival,ArrivalName=dbo.GetLocationNameL(T.Arrival, @Market),
                                T.DepTime,T.ArrTime,T.Distance,T.Duration,T.Explanation,T.RoundBuyPrice,T.RoundSalePrice,T.RoundPayment,T.PayCom,T.PayComEB,T.ConfStat,T.DepDays,T.TaxPer,
                                T.AccountNo1,T.AccountNo2,T.AccountNo3,T.NextRoute,T.DepRet,T.PayPasEB,T.CanDiscount,T.SerArea
                            From Transport T (NOLOCK) 
                            Join TransportPrice Tp (NOLOCK) ON T.Code=Tp.Transport
                            Where Exists(Select RecID From Operator (NOLOCK) Where Market=@plMarket And Code=T.Operator)
                              And Tp.RouteFrom=@Departure
                              And Tp.RouteTo=@Arrival
                              And Exists(Select RecID From TransportDay Where RecID=Tp.TransportDayID And TransDate>=dbo.DateOnly(GETDATE()) And (@TransportDay is null Or TransDate=@TransportDay))
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Departure", DbType.Int32, Departure);
                db.AddInParameter(dbCommand, "Arrival", DbType.Int32, Arrival);
                db.AddInParameter(dbCommand, "TransportDay", DbType.DateTime, TransDate);
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    while (oReader.Read()) {
                        TransportRecord row = new TransportRecord();
                        row.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        row.TpType = Conversion.getByteOrNull(oReader["TpType"]);
                        row.Code = (string)oReader["Code"];
                        row.Name = (string)oReader["Name"];
                        row.NameL = (string)oReader["NameL"];
                        row.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        row.Operator = (string)oReader["Operator"];
                        row.Departure = (int)oReader["Departure"];
                        row.DepartureName = Conversion.getStrOrNull(oReader["DepartureName"]);
                        row.Arrival = (int)oReader["Arrival"];
                        row.ArrivalName = Conversion.getStrOrNull(oReader["ArrivalName"]);
                        row.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        row.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        row.Distance = Conversion.getInt32OrNull(oReader["Distance"]);
                        row.Duration = Conversion.getInt32OrNull(oReader["Duration"]);
                        row.Explanation = Conversion.getStrOrNull(oReader["Explanation"]);
                        row.RoundBuyPrice = Conversion.getInt16OrNull(oReader["RoundBuyPrice"]);
                        row.RoundSalePrice = Conversion.getInt16OrNull(oReader["RoundSalePrice"]);
                        row.RoundPayment = Conversion.getInt16OrNull(oReader["RoundPayment"]);
                        row.PayCom = Conversion.getStrOrNull(oReader["PayCom"]);
                        row.PayComEB = Conversion.getStrOrNull(oReader["PayComEB"]);
                        row.ConfStat = Conversion.getInt16OrNull(oReader["ConfStat"]);
                        row.DepDays = Conversion.getStrOrNull(oReader["DepDays"]);
                        row.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        row.AccountNo1 = Conversion.getStrOrNull(oReader["AccountNo1"]);
                        row.AccountNo2 = Conversion.getStrOrNull(oReader["AccountNo2"]);
                        row.AccountNo3 = Conversion.getStrOrNull(oReader["AccountNo3"]);
                        row.NextRoute = Conversion.getStrOrNull(oReader["NextRoute"]);
                        row.DepRet = Conversion.getInt16OrNull(oReader["DepRet"]);
                        row.PayPasEB = Conversion.getBoolOrNull(oReader["PayPasEB"]);
                        row.CanDiscount = Conversion.getBoolOrNull(oReader["CanDiscount"]);
                        row.SerArea = Conversion.getInt16OrNull(oReader["SerArea"]);
                        records.Add(row);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransportRecord> getTransport(string Market, string plMarket, string Code, int? Departure, int? Arrival, ref string errorMsg)
        {
            List<TransportRecord> records = new List<TransportRecord>();
            string tsql = string.Empty;
            tsql =
@"Select T.RecID,TpType,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market),[Name]),
    NameS,Operator,Departure,DepartureName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=T.Departure),
    Arrival,ArrivalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=T.Arrival),
    DepTime,ArrTime,Distance,T.Duration,T.Explanation,RoundBuyPrice,RoundSalePrice,RoundPayment,PayCom,PayComEB,ConfStat,DepDays,TaxPer,AccountNo1,AccountNo2,AccountNo3,NextRoute,DepRet,PayPasEB,CanDiscount,SerArea
From Transport T (NOLOCK) 
Join TransportRoute R (NOLOCK) ON T.Code=R.Transport
Where Exists(Select RecID From Operator (NOLOCK) Where Market=@plMarket And Code=T.Operator) ";

            string whereStr = string.Empty;
            if (!string.IsNullOrEmpty(Code))
                whereStr += string.Format(" And Code = '{0}' ", Code);

            if (Departure.HasValue)
                whereStr += string.Format(" And (R.Location={0} Or T.Departure={0}) ", Departure.Value);

            if (Arrival.HasValue)
                whereStr += string.Format(" And (R.Location={0} Or T.Arrival={0}) ", Arrival.Value);

            if (whereStr.Length > 0) tsql += whereStr;

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    while (oReader.Read()) {
                        TransportRecord row = new TransportRecord();
                        row.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        row.TpType = Conversion.getByteOrNull(oReader["TpType"]);
                        row.Code = (string)oReader["Code"];
                        row.Name = (string)oReader["Name"];
                        row.NameL = (string)oReader["NameL"];
                        row.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        row.Operator = (string)oReader["Operator"];
                        row.Departure = (int)oReader["Departure"];
                        row.DepartureName = Conversion.getStrOrNull(oReader["DepartureName"]);
                        row.Arrival = (int)oReader["Arrival"];
                        row.ArrivalName = Conversion.getStrOrNull(oReader["ArrivalName"]);
                        row.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        row.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        row.Distance = Conversion.getInt32OrNull(oReader["Distance"]);
                        row.Duration = Conversion.getInt32OrNull(oReader["Duration"]);
                        row.Explanation = Conversion.getStrOrNull(oReader["Explanation"]);
                        row.RoundBuyPrice = Conversion.getInt16OrNull(oReader["RoundBuyPrice"]);
                        row.RoundSalePrice = Conversion.getInt16OrNull(oReader["RoundSalePrice"]);
                        row.RoundPayment = Conversion.getInt16OrNull(oReader["RoundPayment"]);
                        row.PayCom = Conversion.getStrOrNull(oReader["PayCom"]);
                        row.PayComEB = Conversion.getStrOrNull(oReader["PayComEB"]);
                        row.ConfStat = Conversion.getInt16OrNull(oReader["ConfStat"]);
                        row.DepDays = Conversion.getStrOrNull(oReader["DepDays"]);
                        row.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        row.AccountNo1 = Conversion.getStrOrNull(oReader["AccountNo1"]);
                        row.AccountNo2 = Conversion.getStrOrNull(oReader["AccountNo2"]);
                        row.AccountNo3 = Conversion.getStrOrNull(oReader["AccountNo3"]);
                        row.NextRoute = Conversion.getStrOrNull(oReader["NextRoute"]);
                        row.DepRet = Conversion.getInt16OrNull(oReader["DepRet"]);
                        row.PayPasEB = Conversion.getBoolOrNull(oReader["PayPasEB"]);
                        row.CanDiscount = Conversion.getBoolOrNull(oReader["CanDiscount"]);
                        row.SerArea = Conversion.getInt16OrNull(oReader["SerArea"]);
                        records.Add(row);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public TransportRecord getTransport(string Market, string plMarket, string Code, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql = @"Select RecID,TpType,Code,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market),[Name]),
	                    NameS,Operator,Departure,DepartureName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=T.Departure),
	                    Arrival,ArrivalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=T.Arrival),
	                    DepTime,ArrTime,Distance,Duration,Explanation,RoundBuyPrice,RoundSalePrice,RoundPayment,PayCom,PayComEB,ConfStat,DepDays,TaxPer,AccountNo1,AccountNo2,AccountNo3,NextRoute,DepRet,PayPasEB,CanDiscount,SerArea
                    From Transport T (NOLOCK) 
                    Where Exists(Select RecID From Operator (NOLOCK) Where Market=@plMarket And Code=T.Operator)                       
                      And T.Code=@Code
                    ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    if (oReader.Read()) {
                        TransportRecord row = new TransportRecord();
                        row.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        row.TpType = Conversion.getByteOrNull(oReader["TpType"]);
                        row.Code = (string)oReader["Code"];
                        row.Name = (string)oReader["Name"];
                        row.NameL = (string)oReader["NameL"];
                        row.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        row.Operator = (string)oReader["Operator"];
                        row.Departure = (int)oReader["Departure"];
                        row.DepartureName = Conversion.getStrOrNull(oReader["DepartureName"]);
                        row.Arrival = (int)oReader["Arrival"];
                        row.ArrivalName = Conversion.getStrOrNull(oReader["ArrivalName"]);
                        row.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        row.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        row.Distance = Conversion.getInt32OrNull(oReader["Distance"]);
                        row.Duration = Conversion.getInt32OrNull(oReader["Duration"]);
                        row.Explanation = Conversion.getStrOrNull(oReader["Explanation"]);
                        row.RoundBuyPrice = Conversion.getInt16OrNull(oReader["RoundBuyPrice"]);
                        row.RoundSalePrice = Conversion.getInt16OrNull(oReader["RoundSalePrice"]);
                        row.RoundPayment = Conversion.getInt16OrNull(oReader["RoundPayment"]);
                        row.PayCom = Conversion.getStrOrNull(oReader["PayCom"]);
                        row.PayComEB = Conversion.getStrOrNull(oReader["PayComEB"]);
                        row.ConfStat = Conversion.getInt16OrNull(oReader["ConfStat"]);
                        row.DepDays = Conversion.getStrOrNull(oReader["DepDays"]);
                        row.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        row.AccountNo1 = Conversion.getStrOrNull(oReader["AccountNo1"]);
                        row.AccountNo2 = Conversion.getStrOrNull(oReader["AccountNo2"]);
                        row.AccountNo3 = Conversion.getStrOrNull(oReader["AccountNo3"]);
                        row.NextRoute = Conversion.getStrOrNull(oReader["NextRoute"]);
                        row.DepRet = Conversion.getInt16OrNull(oReader["DepRet"]);
                        row.PayPasEB = Conversion.getBoolOrNull(oReader["PayPasEB"]);
                        row.CanDiscount = Conversion.getBoolOrNull(oReader["CanDiscount"]);
                        row.SerArea = Conversion.getInt16OrNull(oReader["SerArea"]);
                        return row;
                    } else return null;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransportDayRecord> getTransportDays(string Market, string plMarket, DateTime? Date, string TransportCode, int? RouteFrom, int? RouteTo, string Bus, ref string errorMsg)
        {
            List<TransportDayRecord> records = new List<TransportDayRecord>();
            string tsql = string.Empty;
            tsql = @"   Select TD.Transport, 
                            TD.RouteFrom, RouteFromName=(Select Name From Location (NOLOCK) Where RecID=TD.RouteFrom),
                            RouteFromNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=TD.RouteFrom),
                            TD.RouteTo, RouteToName=(Select Name From Location (NOLOCK) Where RecID=TD.RouteTo),
                            RouteToNameL=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=TD.RouteTo),
                            TD.TransDate, TD.Bus, TD.DepTime, TD.Seat, TD.Supplier, TD.NextRoute, TD.AllotUseSysSet, TD.AllotChk, TD.AllotWarnFull, TD.AllotDoNotOver,
                            TD.ChdG1MaxAge, TD.ChdG2MaxAge, TD.ChdG3MaxAge	
                        From TransportDay TD (NOLOCK) 
                        JOIN Transport T (NOLOCK) ON TD.Transport=T.Code
                        Where Exists(Select RecID From Operator (NOLOCK) Where Market=@plMarket And Code=T.Operator) ";
            string whereStr = string.Empty;
            if (!string.IsNullOrEmpty(TransportCode))
                whereStr += string.Format(" And T.Code='{0}' ", TransportCode);
            else {
                if (RouteFrom.HasValue)
                    whereStr += string.Format(" And RouteFrom={0} ", RouteFrom.Value);

                if (RouteTo.HasValue)
                    whereStr += string.Format(" And RouteTo={0} ", RouteTo.Value);

                if (!string.IsNullOrEmpty(Bus))
                    whereStr += string.Format(" And Bus='{0}' ", Bus);
            }
            if (Date.HasValue)
                whereStr += " And TransDate=@Date ";

            if (whereStr.Length > 0) tsql += whereStr;

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                if (Date.HasValue)
                    db.AddInParameter(dbCommand, "Date", DbType.Date, Date.Value);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        TransportDayRecord row = new TransportDayRecord();
                        row.Transport = (string)R["Transport"];
                        row.RouteFrom = (int)R["RouteFrom"];
                        row.RouteFromName = Conversion.getStrOrNull(R["RouteFromName"]);
                        row.RouteFromNameL = Conversion.getStrOrNull(R["RouteFromNameL"]);
                        row.RouteTo = (int)R["RouteTo"];
                        row.RouteToName = Conversion.getStrOrNull(R["RouteToName"]);
                        row.RouteToNameL = Conversion.getStrOrNull(R["RouteToNameL"]);
                        row.TransDate = (DateTime)R["TransDate"];
                        row.Bus = Conversion.getStrOrNull(R["Bus"]);
                        row.DepTime = Conversion.getDateTimeOrNull(R["DepTime"]);
                        row.Seat = Conversion.getInt16OrNull(R["Seat"]);
                        row.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        row.NextRoute = Conversion.getStrOrNull(R["NextRoute"]);
                        row.AllotUseSysSet = Equals(R["AllotUseSysSet"], "Y");
                        row.AllotChk = Equals(R["AllotChk"], "Y");
                        row.AllotWarnFull = Equals(R["AllotWarnFull"], "Y");
                        row.AllotDoNotOver = Equals(R["AllotDoNotOver"], "Y");
                        row.ChdG1MaxAge = Conversion.getDecimalOrNull(R["ChdG1MaxAge"]);
                        row.ChdG2MaxAge = Conversion.getDecimalOrNull(R["ChdG2MaxAge"]);
                        row.ChdG3MaxAge = Conversion.getDecimalOrNull(R["ChdG3MaxAge"]);
                        records.Add(row);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return records;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public TransportDetailRecord getTransportDetail(string Market, string plMarket, string TransportCode, string Bus, DateTime TransDate, ref string errorMsg)
        {
            List<TransportRecord> transport = getTransport(Market, plMarket, TransportCode, null, null, ref errorMsg);
            List<TransportDayRecord> transportDay = getTransportDays(Market, plMarket, TransDate, string.Empty, null, null, Bus, ref errorMsg);
            var tDetail = from q1 in transport.AsEnumerable()
                          join q2 in transportDay.AsEnumerable() on q1.Code equals q2.Transport
                          where q1.Code == TransportCode &&
                             q2.Bus == Bus &&
                             q2.TransDate == TransDate
                          select new { q1.Name, q1.NameL, q2.RouteFrom, q2.RouteFromName, q2.RouteFromNameL, q2.RouteTo, q2.RouteToName, q2.RouteToNameL, q2.ChdG1MaxAge, q1.CanDiscount, q2.TransDate, q2.DepTime };
            List<TransportDetailRecord> result = (from q in tDetail.AsEnumerable()
                                                  group q by new { q.Name, q.NameL, q.RouteFrom, q.RouteFromName, q.RouteFromNameL, q.RouteTo, q.RouteToName, q.RouteToNameL, q.ChdG1MaxAge, q.CanDiscount, q.TransDate, q.DepTime } into k
                                                  select new TransportDetailRecord {
                                                      Name = k.Key.Name,
                                                      NameL = k.Key.NameL,
                                                      RouteFrom = k.Key.RouteFrom,
                                                      RouteFromName = k.Key.RouteFromName,
                                                      RouteFromNameL = k.Key.RouteFromNameL,
                                                      RouteTo = k.Key.RouteTo,
                                                      RouteToName = k.Key.RouteToName,
                                                      RouteToNameL = k.Key.RouteToNameL,
                                                      ChdG1MaxAge = k.Key.ChdG1MaxAge,
                                                      CanDiscount = k.Key.CanDiscount.HasValue ? k.Key.CanDiscount.Value : false,
                                                      TransDate = k.Key.TransDate,
                                                      DepTime = k.Key.DepTime
                                                  }).ToList<TransportDetailRecord>();

            return result.Count() > 0 ? result.First() : null;
        }

        public bool Check_TransportAllot(User UserData, ResDataRecord ResData, int OldUnit, ref string errorMsg)
        {
            Int16 Stat = 0;
            Int16 freeSeat = 0;
            List<ResServiceRecord> SelectRow = ResData.ResService.Where(w => w.ServiceType == "TRANSPORT").ToList<ResServiceRecord>();
            if (SelectRow == null || SelectRow.Count == 0) return true;
            bool allotOK = false;
            foreach (ResServiceRecord row in SelectRow) {
                bool tmpAllotOK = false;

                if (string.IsNullOrEmpty(row.AllotType)) row.AllotType = "S";

                List<TransportDayRecord> _TransportDays = new Transports().getTransportDays(UserData.Market, ResData.ResMain.PLMarket, row.BegDate, row.Service, row.DepLocation.Value, row.ArrLocation.Value, string.Empty, ref errorMsg);
                if (_TransportDays == null || _TransportDays.Count < 1) {
                    errorMsg = string.Format("Allotment not found for Transport {0},{1} at {2}.",
                                                row.Service,
                                                row.Bus,
                                                row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "");
                    return false;
                }

                foreach (TransportDayRecord tRow in _TransportDays) {
                    if (Equals(row.AllotType, "O") || Equals(row.AllotType, "S")) {
                        Int16 Seat = Convert.ToInt16(OldUnit.ToString());
                        row.AllotType = "S";
                        Stat = new Transports().Transport_Allotment_Control(row.Service,
                                                        tRow.Bus, ResData.ResMain.PLOperator,
                                                        row.AllotType, row.BegDate.Value,
                                                        row.DepLocation.Value, row.ArrLocation.Value, Seat, ref freeSeat, ref errorMsg);
                    }

                    if (Stat == 0) {
                        tmpAllotOK = true;
                    } else
                        if (Stat != 0) {
                        if ((tRow.AllotUseSysSet && UserData.TvParams.TvParamTransport.AllotWarnFull) ||
                            (!tRow.AllotUseSysSet && tRow.AllotWarnFull)) {
                            if (Stat == -1) errorMsg = string.Format("Allotment not found for Transport {0},{1} at {2}.",
                                row.Service, row.Bus, row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "");
                            if (Stat == 1) errorMsg = string.Format("Allotment not enough for Transport {0},{1} at {2}.",
                                row.Service, row.Bus, row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "");
                        }

                        row.AllotType = "O";
                        row.StatConf = 0;

                        if ((tRow.AllotUseSysSet && UserData.TvParams.TvParamTransport.AllotDoNotOver) ||
                            (!tRow.AllotUseSysSet && tRow.AllotDoNotOver)) {
                            if (Stat == 1) {
                                tmpAllotOK = false;
                            }
                        } else {
                            tmpAllotOK = true;
                        }
                    }
                    if (tmpAllotOK) {
                        allotOK = true;
                        row.Bus = tRow.Bus;
                        break;
                    }
                }

                allotOK = tmpAllotOK;
            }
            if (allotOK) errorMsg = string.Empty;
            return allotOK;
        }

        public Int16 Transport_Allotment_Control(string Transport, string Bus, string Operator, string AllotType, DateTime TransDate, int RouteFrom, int RouteTo, Int16 ForSeat, ref Int16 freeSeat, ref string errorMsg)
        {
            if (AllotType == "O") return 0;
            string tsql = @"Declare @Stat int, @FreeSeat int
                            exec dbo.sp_CheckTransportAlllot @Transport, @TransportDate, @Bus, @Operator, 
                                    @AllotType, @RouteFrom, @RouteTo, @ForSeat, @Stat Output, @FreeSeat Output 
                            Select Stat=@Stat, FreeSeat=@FreeSeat ";
            Int16 Status = -1;
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Transport", DbType.String, Transport);
                db.AddInParameter(dbCommand, "TransportDate", DbType.DateTime, TransDate);
                db.AddInParameter(dbCommand, "Bus", DbType.String, Bus);
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "RouteFrom", DbType.Int32, RouteFrom);
                db.AddInParameter(dbCommand, "RouteTo", DbType.Int32, RouteTo);
                db.AddInParameter(dbCommand, "AllotType", DbType.String, AllotType);
                db.AddInParameter(dbCommand, "ForSeat", DbType.Int16, ForSeat);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    if (R.Read()) {
                        int? _stat = Conversion.getInt32OrNull(R["Stat"]);
                        int? _freeSeat = Conversion.getInt32OrNull(R["FreeSeat"]);
                        Status = _stat.HasValue ? Convert.ToInt16(_stat.ToString()) : Status;
                        freeSeat = _freeSeat.HasValue ? Convert.ToInt16(_freeSeat.ToString()) : Convert.ToInt16(0);
                    }
                    return Status;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return Status;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransportStatRecord> getPickups(string Market, string Transport, int? location, ref string errorMsg)
        {
            List<TransportStatRecord> records = new List<TransportStatRecord>();
            string tsql =
@"
Select T.Transport, T.RouteID, T.Location, 
	LocationName=(Select Name From Location (NOLOCK) Where RecID=T.Location),
	LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=T.Location),
	T.StepNo, T.DepTime	
From TransportStat T (NOLOCK) 
Join TransportRoute R (NOLOCK) ON T.RouteID=R.RecID
Where R.Location=@location
";

            if (!string.IsNullOrEmpty(Transport))
                tsql += string.Format(" And T.Transport='{0}' ", Transport);
            tsql += "Order By T.StepNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "location", DbType.Int32, location);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        TransportStatRecord rec = new TransportStatRecord();
                        rec.Transport = Conversion.getStrOrNull(R["Transport"]);
                        rec.RouteID = (int)R["RouteID"];
                        rec.Location = (int)R["Location"];
                        rec.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        rec.LocationLocalName = Conversion.getStrOrNull(R["LocationLocalName"]);
                        rec.StepNo = Conversion.getByteOrNull(R["StepNo"]);
                        rec.DepTime = Conversion.getDateTimeOrNull(R["DepTime"]);
                        records.Add(rec);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return records;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransportStatRecord> getPickups(string Market, List<ResServiceRecord> ResService, ref string errorMsg)
        {
            List<TransportStatRecord> records = new List<TransportStatRecord>();

            var query = from q in ResService
                        where q.ServiceType == "TRANSPORT"
                        select new { Service = q.Service };
            if (query.Count() > 0) {
                foreach (var r in query) {
                    int? location = ResService.Where(w => w.Service == r.Service).FirstOrDefault().DepLocation;
                    List<TransportStatRecord> _list = new List<TransportStatRecord>();
                    _list = getPickups(Market, r.Service, location, ref errorMsg);
                    foreach (TransportStatRecord row in _list) {
                        TransportStatRecord rec = new TransportStatRecord();
                        rec = row;
                        records.Add(rec);
                    }
                }
            }
            return records;
        }

        public List<BusRecord> getBusList(string Market, int? Departure, int? Arrival, DateTime? Date, ref string errorMsg)
        {
            List<BusRecord> list = new List<BusRecord>();
            string whereStr = string.Empty;

            if (Departure.HasValue) {
                if (whereStr.Length > 0) whereStr += " And";
                whereStr += string.Format(" (D.RouteFrom={0} Or R.Location={0}) ", Departure.Value);
            }

            if (Arrival.HasValue) {
                if (whereStr.Length > 0) whereStr += " And";
                whereStr += string.Format(" (D.RouteTo={0} Or R.Location={0}) ", Arrival.Value);
            }

            if (Date.HasValue) {
                if (whereStr.Length > 0) whereStr += " And";
                whereStr += " TransDate=@Date ";
            }

            if (whereStr.Length > 0) whereStr = "Where " + whereStr;

            string tsql = string.Format(@"Select RecID, Code, Name, Plate, Seat, Producer, Model, ModelYear, Explanation
                                            From ( 
                                              Select Distinct Bus 
                                              From TransportDay D (NOLOCK)
                                              Join TransportRoute R (NOLOCK) ON D.Transport=R.Transport
                                                {0} 
                                            ) X
                                            Join Bus B (NOLOCK) On B.Code=X.Bus", whereStr.Length > 0 ? whereStr : "");

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader rdr = db.ExecuteReader(dbCommand)) {
                    while (rdr.Read()) {
                        BusRecord record = new BusRecord();
                        record.RecID = (int)rdr["RecID"];
                        record.Code = Conversion.getStrOrNull(rdr["Code"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.Plate = Conversion.getStrOrNull(rdr["Plate"]);
                        record.Seat = Conversion.getInt16OrNull(rdr["Seat"]);
                        record.Producer = Conversion.getStrOrNull(rdr["Producer"]);
                        record.Model = Conversion.getStrOrNull(rdr["Model"]);
                        record.ModelYear = Conversion.getStrOrNull(rdr["ModelYear"]);
                        record.Explanation = Conversion.getStrOrNull(rdr["Explanation"]);
                        list.Add(record);
                    }
                }
                return list;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return list;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BusRecord> getBusList(string Operator, string Market, int? Departure, int? Arrival, ref string errorMsg)
        {
            List<BusRecord> list = new List<BusRecord>();
            string whereStr = string.Empty;

            string tsql = @"Select RecID, Code, Name, Plate, Seat, Producer, Model, ModelYear, Explanation
                            From ( 
	                            Select Distinct Tr.Bus 
	                            From TPDayRouteV Tr (NOLOCK)
	                            Join Transport T (NOLOCK) ON T.Code=Tr.Transport	
	                            Where Tr.TransDate >= dbo.DateOnly(GETDATE())
	                              And Tr.Arrival<>Location
	                              And Tr.Operator=@Operator
	                              And Exists (Select RecID From TransportPrice Where Transport=Tr.Transport)
	                              And Tr.Location=@Departure
	                              And Tr.Arrival=@Arrival
                            ) X
                            Join Bus B (NOLOCK) On B.Code=X.Bus
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Departure", DbType.Int32, Departure);
                db.AddInParameter(dbCommand, "Arrival", DbType.Int32, Arrival);
                using (IDataReader rdr = db.ExecuteReader(dbCommand)) {
                    while (rdr.Read()) {
                        BusRecord record = new BusRecord();
                        record.RecID = (int)rdr["RecID"];
                        record.Code = Conversion.getStrOrNull(rdr["Code"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.Plate = Conversion.getStrOrNull(rdr["Plate"]);
                        record.Seat = Conversion.getInt16OrNull(rdr["Seat"]);
                        record.Producer = Conversion.getStrOrNull(rdr["Producer"]);
                        record.Model = Conversion.getStrOrNull(rdr["Model"]);
                        record.ModelYear = Conversion.getStrOrNull(rdr["ModelYear"]);
                        record.Explanation = Conversion.getStrOrNull(rdr["Explanation"]);
                        list.Add(record);
                    }
                }
                return list;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return list;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransportRouteRecord> getTransportRoute(string Transport, ref string errorMsg)
        {
            List<TransportRouteRecord> list = new List<TransportRouteRecord>();
            string tsql = @"Select RecID, Transport, StepNo, Location, DisFromDep, DisToArr, Duration 
                            From TransportRoute (NOLOCK)
                            Where Transport=@Transport ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try {
                db.AddInParameter(dbCommand, "Transport", DbType.String, Transport);
                using (IDataReader rdr = db.ExecuteReader(dbCommand)) {
                    while (rdr.Read()) {
                        TransportRouteRecord record = new TransportRouteRecord();
                        record.RecID = Conversion.getInt32OrNull(rdr["RecID"]);
                        record.Transport = Conversion.getStrOrNull(rdr["Transport"]);
                        record.StepNo = Conversion.getInt16OrNull(rdr["StepNo"]);
                        record.Location = Conversion.getInt32OrNull(rdr["Location"]);
                        record.DisFromDep = Conversion.getInt16OrNull(rdr["DisFromDep"]);
                        record.DisToArr = Conversion.getInt16OrNull(rdr["DisToArr"]);
                        record.Duration = Conversion.getInt32OrNull(rdr["Duration"]);
                        list.Add(record);
                    }
                }
                return list;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return list;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransportDays> getTransportDays(string Market, string Operator, int? ArrCity, ref string errorMsg)
        {
            List<TransportDays> records = new List<TransportDays>();
            string tsql = string.Empty;
            tsql = string.Format(@" Select TransDate, 
	                                    RouteFrom, RouteFromName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=RouteFrom),
	                                    RouteTo, RouteToName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=RouteTo),
                                        SerArea
                                    From (
	                                    Select Distinct TransDate, 
		                                    tD.RouteFrom, 
		                                    tD.RouteTo,
                                            t.SerArea
	                                    From TransportDay tD (NOLOCK)
	                                    Join Transport t (NOLOCK) ON tD.Transport=t.Code
	                                    Where t.Operator=@Operator
	                                      And t.DepRet=0
	                                      And tD.TransDate>=GetDate()
                                          {0}
                                    ) X", ArrCity.HasValue ? "And tD.RouteTo=" + ArrCity.ToString() : "");
//            tsql = string.Format(@" 
//            Select TransDate, 
//	            RouteFrom, RouteFromName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=RouteFrom),
//	            RouteTo, RouteToName=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID=RouteTo),
//                SerArea From 
//                ( Select Distinct td.TransDate,td.RouteFrom,td.RouteTo, t.SerArea
//                From HolPack H (Nolock)
//                LEFT JOIN HolPackMarket hpm (Nolock) On hpm.HolPack = H.Code 
//                LEFT JOIN HolPackSer hps (Nolock) On hps.HolPack = H.Code
//                LEFT JOIN TransportDay td (Nolock) on td.Transport=hps.ServiceItem
//                LEFT JOIN Transport t (Nolock) on t.Code = td.Transport
//                WHERE 
//                h.B2BActive=1 
//                And (hpm.Operator = @Operator Or IsNull(hpm.Operator,'')= '' ) and t.Operator <> @Operator  
//                And hps.Service = 'TRANSPORT' 
//                And t.DepRet=0
//                And td.TransDate>=GetDate()
//                {0}
//            Union All
//                  Select Distinct td.TransDate, td.RouteFrom, td.RouteTo, t.SerArea
//                  From TransportDay td (NOLOCK)
//                  Join Transport t (NOLOCK) ON td.Transport=t.Code   
//                  Where t.Operator=@Operator 
//                      And t.DepRet=0
//                      And td.TransDate>=GetDate()
//                      {0}
//            ) X Order By X.TransDate", ArrCity.HasValue ? "And td.RouteTo=" + ArrCity.ToString() : "");

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        TransportDays row = new TransportDays();
                        row.TransDate = (DateTime)R["TransDate"];
                        row.RouteFrom = (int)R["RouteFrom"];
                        row.RouteFromName = Conversion.getStrOrNull(R["RouteFromName"]);
                        row.RouteTo = (int)R["RouteTo"];
                        row.RouteToName = Conversion.getStrOrNull(R["RouteToName"]);
                        row.SerArea = Conversion.getInt16OrNull(R["SerArea"]);
                        records.Add(row);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransportDayRouteV> getTransportDayRoute(string Market, string Operator, ref string errorMsg)
        {
            List<TransportDayRouteV> records = new List<TransportDayRouteV>();
            string tsql = @"Select TransDate,Transport,Departure,DepartureName=dbo.GetLocationNameL(Departure, @Market),
	                               Arrival,ArrivalName=dbo.GetLocationNameL(Arrival, @Market),
	                               DepRet,SerArea,Bus
                            From
                            (
	                            Select Distinct TransDate,Transport,Departure=(Case When DepRet=0 Then Departure Else Arrival End), 
		                            Arrival=(Case When DepRet=0 Then Arrival Else Departure End),DepRet,SerArea,Bus
	                            From
	                            (
		                            Select Distinct	TransDate,Transport,Departure=Location,Arrival=(Case When DepRet=0 Then RouteTo Else RouteFrom End),DepRet,SerArea,Bus
		                            From TPDayRouteV (NOLOCK)
		                            Where
		                             Arrival<>Departure and
		                             Exists (Select RecID From TransportPrice Where Transport=Transport) And
		                             Operator=@Operator
	                            ) X
	                            Where Arrival<>Departure
                            ) Y
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        TransportDayRouteV row = new TransportDayRouteV();
                        row.TransDate = Conversion.getDateTimeOrNull(R["TransDate"]);
                        row.Transport = Conversion.getStrOrNull(R["Transport"]);
                        row.Departure = Conversion.getInt32OrNull(R["Departure"]);
                        row.DepartureName = Conversion.getStrOrNull(R["DepartureName"]);
                        row.Arrival = Conversion.getInt32OrNull(R["Arrival"]);
                        row.ArrivalName = Conversion.getStrOrNull(R["ArrivalName"]);
                        row.DepRet = Conversion.getInt16OrNull(R["DepRet"]);
                        row.SerArea = Conversion.getInt16OrNull(R["SerArea"]);
                        row.Bus = Conversion.getStrOrNull(R["Bus"]);
                        records.Add(row);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<BusSeatPlanRecord> getSeatPlan(string Market, string bus, ref string errorMsg)
        {
            List<BusSeatPlanRecord> records = new List<BusSeatPlanRecord>();
            string tsql = @"Select RecID,Bus,CellNo,CellType,SeatNo,SeatLetter,isExit,ChdRestriction,RowNo,ColNo,forB2B,forB2C,forTV
                            From BusSeatPlan (NOLOCK)
                            Where Bus=@Bus
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Bus", DbType.String, bus);
                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        BusSeatPlanRecord row = new BusSeatPlanRecord();
                        row.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        row.Bus = Conversion.getStrOrNull(R["Bus"]);
                        row.CellNo = Conversion.getInt16OrNull(R["CellNo"]);
                        row.CellType = Conversion.getInt16OrNull(R["CellType"]);
                        row.SeatNo = Conversion.getInt16OrNull(R["SeatNo"]);
                        row.SeatLetter = Conversion.getStrOrNull(R["SeatLetter"]);
                        row.IsExit = Conversion.getBoolOrNull(R["isExit"]);
                        row.ChdRestriction = Conversion.getBoolOrNull(R["ChdRestriction"]);
                        row.RowNo = Conversion.getInt16OrNull(R["RowNo"]);
                        row.ColNo = Conversion.getInt16OrNull(R["ColNo"]);
                        row.ForB2B = Conversion.getBoolOrNull(R["forB2B"]);
                        row.ForB2C = Conversion.getBoolOrNull(R["forB2C"]);
                        row.ForTV = Conversion.getBoolOrNull(R["forTV"]);
                        records.Add(row);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TransportCInRecord> getUsedSeat(string Market, string transport, DateTime? transportDate, string bus, ref string errorMsg)
        {
            List<TransportCInRecord> records = new List<TransportCInRecord>();
            string tsql = @"Select RecID,Transport,TransportDate,Bus,CustNo,BusSeatNo,ServiceID,Reserve,SeatLetter,Sex,Comment,ExtSeat
                            From TransportCIn (NOLOCK)
                            Where Transport=@Transport 
                              And TransportDate=@TransportDate
                              And Bus=@Bus
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "Transport", DbType.String, transport);
                db.AddInParameter(dbCommand, "TransportDate", DbType.DateTime, transportDate);
                db.AddInParameter(dbCommand, "Bus", DbType.String, bus);
                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        TransportCInRecord row = new TransportCInRecord();
                        row.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        row.Transport = Conversion.getStrOrNull(R["Transport"]);
                        row.TransportDate = Conversion.getDateTimeOrNull(R["TransportDate"]);
                        row.Bus = Conversion.getStrOrNull(R["Bus"]);
                        row.CustNo = Conversion.getInt32OrNull(R["CustNo"]);
                        row.BusSeatNo = Conversion.getInt16OrNull(R["BusSeatNo"]);
                        row.ServiceID = Conversion.getInt32OrNull(R["ServiceID"]);
                        row.Reserve = Conversion.getBoolOrNull(R["Reserve"]);
                        row.SeatLetter = Conversion.getStrOrNull(R["SeatLetter"]);
                        row.Sex = Conversion.getStrOrNull(R["Sex"]);
                        row.Comment = Conversion.getStrOrNull(R["Comment"]);
                        row.ExtSeat = Conversion.getBoolOrNull(R["ExtSeat"]);
                        records.Add(row);
                    }
                }
                return records;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool setUsedSeat(User UserData, string Transport, DateTime? TransportDate, string Bus, int? CustNo, Int16? BusSeatNo,
                                int? ServiceID, string SeatLetter, string Sex, string Comment, int? ExtSeat, ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string tsql =
@"
if (Not Exists(Select null From TransportCIn (NOLOCK) Where Transport=@Transport and TransportDate=@TransportDate and Bus=@Bus and BusSeatNo=@BusSeatNo))
Begin
  Insert Into TransportCIn (Transport,TransportDate,Bus,CustNo,BusSeatNo,ServiceID,SeatLetter,Sex,Comment,ExtSeat)
  Values(@Transport,@TransportDate,@Bus,@CustNo,@BusSeatNo,@ServiceID,@SeatLetter,@Sex,@Comment,@ExtSeat)
  Select Cast(1 as bit)
End
Else
Begin
  Select Cast(0 as bit)
End
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try {
                db.AddInParameter(dbCommand, "Transport", DbType.AnsiString, Transport);
                db.AddInParameter(dbCommand, "TransportDate", DbType.DateTime, TransportDate);
                db.AddInParameter(dbCommand, "Bus", DbType.AnsiString, Bus);
                db.AddInParameter(dbCommand, "CustNo", DbType.Int32, CustNo);
                db.AddInParameter(dbCommand, "BusSeatNo", DbType.Int16, BusSeatNo);
                db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, ServiceID);
                db.AddInParameter(dbCommand, "SeatLetter", DbType.AnsiString, SeatLetter);
                db.AddInParameter(dbCommand, "Sex", DbType.AnsiString, Sex);
                db.AddInParameter(dbCommand, "Comment", DbType.String, Comment);
                db.AddInParameter(dbCommand, "ExtSeat", DbType.Int16, ExtSeat.HasValue ? ExtSeat.Value : 0);

                return (bool)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return false;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}

