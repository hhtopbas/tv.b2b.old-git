﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class tvReportPDFRecord
    {
        public tvReportPDFRecord()
        {
        }

        Int16? _Status;
        public Int16? Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        object _RptImage;
        public object RptImage
        {
            get { return _RptImage; }
            set { _RptImage = value; }
        }

        Int16? _FileType;
        public Int16? FileType
        {
            get { return _FileType; }
            set { _FileType = value; }
        }
    }    

    public class dbInformationShema
    {
        public dbInformationShema()
        {
        }

        int _SeqNo;
        public int SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _DType;
        public string DType
        {
            get { return _DType; }
            set { _DType = value; }
        }
    }    
}
