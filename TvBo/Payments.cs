﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;

namespace TvBo
{
    public class Payments
    {
        public Int16 getAccountTypeForPayment(string ResNo, ref string errorMsg)
        {
            string tsql = @"Select AccountType=Case When IsNull(PaymentFrom,InvoiceTo)=1 then 3 else 0 end From ResMain (NOLOCK) Where ResNo=@ResNo";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                Int16? accountType = Conversion.getInt16OrNull(db.ExecuteScalar(dbCommand));
                return accountType.HasValue ? accountType.Value : Convert.ToInt16(0);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return Convert.ToInt16(0);
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool checkPaymentUser(User UserData, string resNo, ref string errorMsg)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string UserID = UserData.UserID;
            string tsql = @"
                            Declare @AgencyUser VarChar(10)
                            Select @AgencyUser=isnull(AgencyUser, '') 
                            From ResMain (NOLOCK) 
                            Where ResNo=@ResNo                            

                            if (@AgencyUser = '')
                            Begin
	                            Update ResMain
	                            Set AgencyUser=@UserID
	                            Where ResNo=@ResNo
                            End
                           ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNo);
                db.AddInParameter(dbCommand, "UserID", DbType.String, UserID);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool createCheckPayment(string resNo, string token, ref string errorMsg)
        {
            string tsql = @"Insert Into TV_CheckPayment(ResNo, Token, IsComplete, CreateDate, ChangeDate)
                            Values (@ResNo, @Token, 0, GetDate(), null) ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNo);
                db.AddInParameter(dbCommand, "Token", DbType.String, token);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool closeCheckPayment(string resNo, string token, ref string errorMsg)
        {
            string tsql = @"
                            if (Exists(Select * From TV_CheckPayment Where ResNo=@ResNo And Token=@Token And IsComplete=0))
                            Begin
                                Update TV_CheckPayment
                                Set
                                    IsComplete=1, 
                                    ChangeDate=GetDate()
                                Where 
                                    ResNo=@ResNo And Token=@Token 
                                Select ok='1'    
                            End
                            Else
                            Begin
                                Select ok='0'
                            End
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNo);
                db.AddInParameter(dbCommand, "Token", DbType.String, token);
                return Equals(db.ExecuteScalar(dbCommand), "1") ? true : false;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool checkPayment(string resNo, string token, ref string errorMsg)
        {
            string tsql = @"
                            if (Exists(Select * From TV_CheckPayment Where ResNo=@ResNo And Token=@Token And IsComplete=0))                            
                               Select ok='1'                                
                            Else Select ok='0'                            
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNo);
                db.AddInParameter(dbCommand, "Token", DbType.String, token);
                return Equals(db.ExecuteScalar(dbCommand), "1") ? true : false;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<JournalCDetRecord> getJournalCDetList(string ResNo, int? JournalID, ref string errorMsg)
        {
            List<JournalCDetRecord> list = new List<JournalCDetRecord>();
            string tsql = @"Select JC.RecID,JC.JournalID,JC.PayOrder,JC.ResNo,JC.PaidAmount,JC.PaidCur,JC.CurRate,JC.ResPaidAmount,
	                            JC.ResPaidCur,JC.AllocDate,JC.AllocUser,JC.Status
                            From JournalCDet JC (NOLOCK) 
                            ";
            if (JournalID.HasValue)
                tsql += @"Where JC.JournalID=@JournalID";
            else tsql += @"Where JC.ResNo=@ResNo";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                if (JournalID.HasValue)
                    db.AddInParameter(dbCommand, "JournalID", DbType.Int32, JournalID);
                else db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        JournalCDetRecord record = new JournalCDetRecord();
                        record.AllocDate = Conversion.getDateTimeOrNull(R["AllocDate"]);
                        record.AllocUser = Conversion.getStrOrNull(R["AllocUser"]);
                        record.CurRate = Conversion.getDecimalOrNull(R["CurRate"]);
                        record.JournalID = Convert.ToInt32(R["JournalID"]);
                        record.PaidAmount = Conversion.getDecimalOrNull(R["PaidAmount"]);
                        record.PaidCur = Conversion.getStrOrNull(R["PaidCur"]);
                        record.PayOrder = (Int16)R["PayOrder"];
                        record.RecID = Convert.ToInt32(R["RecID"]);
                        record.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        record.ResPaidAmount = Conversion.getDecimalOrNull(R["ResPaidAmount"]);
                        record.ResPaidCur = Conversion.getStrOrNull(R["ResPaidCur"]);
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public JournalRecord getJournal(User UserData, int JournalID, ref string errorMsg)
        {
            string tsql = @"Select * From Journal (NOLOCK)
                            Where RecID=@RecID
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "RecID", DbType.Int32, JournalID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        JournalRecord record = new JournalRecord();
                        record.Account = Conversion.getStrOrNull(R["Account"]);
                        record.AccountType = Conversion.getInt16OrNull(R["AccountType"]);
                        record.Allocated = Conversion.getDecimalOrNull(R["Allocated"]);
                        record.AllocStat = Conversion.getStrOrNull(R["AllocStat"]);
                        record.Amount = Conversion.getDecimalOrNull(R["Amount"]);
                        record.BankComPerc = Conversion.getDecimalOrNull(R["BankComPerc"]);
                        record.CCBank = Conversion.getStrOrNull(R["CCBank"]);
                        record.CCExpDate = Conversion.getStrOrNull(R["CCExpDate"]);
                        record.CCHolder = Conversion.getStrOrNull(R["CCHolder"]);
                        record.CCNo = Conversion.getStrOrNull(R["CCNo"]);
                        record.CCSecNo = Conversion.getStrOrNull(R["CCSecNo"]);
                        record.CCTypeID = Conversion.getInt32OrNull(R["CCTypeID"]);
                        record.ChgAgency = Conversion.getStrOrNull(R["ChgAgency"]);
                        record.ClientAddress = Conversion.getStrOrNull(R["ClientAddress"]);
                        record.ClientCity = Conversion.getStrOrNull(R["ClientCity"]);
                        record.ClientCountry = Conversion.getStrOrNull(R["ClientCountry"]);
                        record.ClientName = Conversion.getStrOrNull(R["ClientName"]);
                        record.ClientZip = Conversion.getStrOrNull(R["ClientZip"]);
                        record.Comment = Conversion.getStrOrNull(R["Comment"]);
                        record.CP = Conversion.getStrOrNull(R["CP"]);
                        record.CrtAgency = Conversion.getStrOrNull(R["CrtAgency"]);
                        record.Cur = Conversion.getStrOrNull(R["Cur"]);
                        record.FixExCur = Conversion.getStrOrNull(R["FixExCur"]);
                        record.FixExRate = Conversion.getDecimalOrNull(R["FixExRate"]);
                        record.InstalNum = Conversion.getInt16OrNull(R["InstalNum"]);
                        record.IntNote = Conversion.getStrOrNull(R["IntNote"]);
                        record.InvoiceDate = Conversion.getDateTimeOrNull(R["InvoiceDate"]);
                        record.InvoiceNo = Conversion.getInt32OrNull(R["InvoiceNo"]);
                        record.InvoiceSerial = Conversion.getStrOrNull(R["InvoiceSerial"]);
                        record.InvPaidAmount = Conversion.getDecimalOrNull(R["InvPaidAmount"]);
                        record.InvPaidRate = Conversion.getDecimalOrNull(R["InvPaidRate"]);
                        record.Locked = Conversion.getStrOrNull(R["Locked"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Office = Conversion.getStrOrNull(R["Office"]);
                        record.OppAccount = Conversion.getStrOrNull(R["OppAccount"]);
                        record.PayAmount = Conversion.getDecimalOrNull(R["PayAmount"]);
                        record.PayDate = Conversion.getDateTimeOrNull(R["PayDate"]);
                        record.PayNo = Conversion.getInt32OrNull(R["PayNo"]);
                        record.PayTime = Conversion.getDateTimeOrNull(R["PayTime"]);
                        record.PayType = Conversion.getStrOrNull(R["PayType"]);
                        record.PosType = Conversion.getInt16OrNull(R["PosType"]);
                        record.ReceiptDate = Conversion.getDateTimeOrNull(R["ReceiptDate"]);
                        record.ReceiptNo = Conversion.getInt32OrNull(R["ReceiptNo"]);
                        record.ReceiptPrt = Conversion.getStrOrNull(R["ReceiptPrt"]);
                        record.ReceiptSerial = Conversion.getStrOrNull(R["ReceiptSerial"]);
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.RecType = Conversion.getInt16OrNull(R["RecType"]);
                        record.Reference = Conversion.getStrOrNull(R["Reference"]);
                        record.Remain = Conversion.getDecimalOrNull(R["Remain"]);
                        record.Resource = Conversion.getByteOrNull(R["Resource"]);
                        record.SendToAcc = Conversion.getBoolOrNull(R["SendToAcc"]);
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        record.TaxAccNo = Conversion.getStrOrNull(R["TaxAccNo"]);
                        record.TaxOffice = Conversion.getStrOrNull(R["TaxOffice"]);
                        record.TrfNo = Conversion.getInt32OrNull(R["TrfNo"]);
                        record.UseInvRate = Conversion.getStrOrNull(R["UseInvRate"]);

                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<JournalRecord> getJournalList(User UserData, ref string errorMsg)
        {
            List<JournalRecord> list = new List<JournalRecord>();
            string tsql = @"Select * From Journal Where Market='" + UserData.Market + "'";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        JournalRecord record = new JournalRecord();
                        record.Account = Conversion.getStrOrNull(R["Account"]);
                        record.AccountType = Conversion.getInt16OrNull(R["AccountType"]);
                        record.Allocated = Conversion.getDecimalOrNull(R["Allocated"]);
                        record.AllocStat = Conversion.getStrOrNull(R["AllocStat"]);
                        record.Amount = Conversion.getDecimalOrNull(R["Amount"]);
                        record.BankComPerc = Conversion.getDecimalOrNull(R["BankComPerc"]);
                        record.CCBank = Conversion.getStrOrNull(R["CCBank"]);
                        record.CCExpDate = Conversion.getStrOrNull(R["CCExpDate"]);
                        record.CCHolder = Conversion.getStrOrNull(R["CCHolder"]);
                        record.CCNo = Conversion.getStrOrNull(R["CCNo"]);
                        record.CCSecNo = Conversion.getStrOrNull(R["CCSecNo"]);
                        record.CCTypeID = Conversion.getInt32OrNull(R["CCTypeID"]);
                        record.ChgAgency = Conversion.getStrOrNull(R["ChgAgency"]);
                        record.ClientAddress = Conversion.getStrOrNull(R["ClientAddress"]);
                        record.ClientCity = Conversion.getStrOrNull(R["ClientCity"]);
                        record.ClientCountry = Conversion.getStrOrNull(R["ClientCountry"]);
                        record.ClientName = Conversion.getStrOrNull(R["ClientName"]);
                        record.ClientZip = Conversion.getStrOrNull(R["ClientZip"]);
                        record.Comment = Conversion.getStrOrNull(R["Comment"]);
                        record.CP = Conversion.getStrOrNull(R["CP"]);
                        record.CrtAgency = Conversion.getStrOrNull(R["CrtAgency"]);
                        record.Cur = Conversion.getStrOrNull(R["Cur"]);
                        record.FixExCur = Conversion.getStrOrNull(R["FixExCur"]);
                        record.FixExRate = Conversion.getDecimalOrNull(R["FixExRate"]);
                        record.InstalNum = Conversion.getInt16OrNull(R["InstalNum"]);
                        record.IntNote = Conversion.getStrOrNull(R["IntNote"]);
                        record.InvoiceDate = Conversion.getDateTimeOrNull(R["InvoiceDate"]);
                        record.InvoiceNo = Conversion.getInt32OrNull(R["InvoiceNo"]);
                        record.InvoiceSerial = Conversion.getStrOrNull(R["InvoiceSerial"]);
                        record.InvPaidAmount = Conversion.getDecimalOrNull(R["InvPaidAmount"]);
                        record.InvPaidRate = Conversion.getDecimalOrNull(R["InvPaidRate"]);
                        record.Locked = Conversion.getStrOrNull(R["Locked"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Office = Conversion.getStrOrNull(R["Office"]);
                        record.OppAccount = Conversion.getStrOrNull(R["OppAccount"]);
                        record.PayAmount = Conversion.getDecimalOrNull(R["PayAmount"]);
                        record.PayDate = Conversion.getDateTimeOrNull(R["PayDate"]);
                        record.PayNo = Conversion.getInt32OrNull(R["PayNo"]);
                        record.PayTime = Conversion.getDateTimeOrNull(R["PayTime"]);
                        record.PayType = Conversion.getStrOrNull(R["PayType"]);
                        record.PosType = Conversion.getInt16OrNull(R["PosType"]);
                        record.ReceiptDate = Conversion.getDateTimeOrNull(R["ReceiptDate"]);
                        record.ReceiptNo = Conversion.getInt32OrNull(R["ReceiptNo"]);
                        record.ReceiptPrt = Conversion.getStrOrNull(R["ReceiptPrt"]);
                        record.ReceiptSerial = Conversion.getStrOrNull(R["ReceiptSerial"]);
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.RecType = Conversion.getInt16OrNull(R["RecType"]);
                        record.Reference = Conversion.getStrOrNull(R["Reference"]);
                        record.Remain = Conversion.getDecimalOrNull(R["Remain"]);
                        record.Resource = Conversion.getByteOrNull(R["Resource"]);
                        record.SendToAcc = Conversion.getBoolOrNull(R["SendToAcc"]);
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        record.TaxAccNo = Conversion.getStrOrNull(R["TaxAccNo"]);
                        record.TaxOffice = Conversion.getStrOrNull(R["TaxOffice"]);
                        record.TrfNo = Conversion.getInt32OrNull(R["TrfNo"]);
                        record.UseInvRate = Conversion.getStrOrNull(R["UseInvRate"]);

                        list.Add(record);
                    }
                    return list;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }

        }

        public List<InvoiceRecord> getInvoiceRes(User UserData, string ResNo, Int16? InvioceType, ref string errorMsg)
        {
            List<InvoiceRecord> records = new List<InvoiceRecord>();
            string tsql = @"Select I.RecID, I.Operator, I.Office, I.InvSerial, I.InvNo, I.InvDate, I.InvType, I.SumType, 
	                            I.ClientType, I.ClientCode, I.ClientName, I.ClientAddress, I.ClientZip, I.ClientCity, I.ClientCountry,
	                            I.Proforma, I.InvAmount, I.InvCur, I.Tax, I.DueDate, I.Language, I.Explanation, I.InBatch, I.ResAmount, I.ResCur,
	                            I.Rate, I.RateDate, I.Status, I.ReceiptSerial, I.ReceiptNo, I.SendToAcc, I.SendToAccDate, I.SendToAccUser, I.FormID, I.Note,
	                            I.CrtDate, I.CrtUser, I.BankName, I.BankNo, I.BankLocation, I.BankAccNo, I.BankCur, I.BankIBAN,
	                            I.TaxOffice, I.TaxAccNo, I.Phone1, I.Phone2, I.Fax1, I.Fax2, I.email1, I.email2, I.PaidAmount, I.InvImageType, I.CrtAgency
                            From InvoiceRes IR (NOLOCK)
                            Join Invoice I (NOLOCK) ON IR.InvoiceID=I.RecID
                            Where IR.ResNo=@ResNo And I.InvType=@InvType ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "InvType", DbType.Int16, InvioceType);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        InvoiceRecord record = new InvoiceRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.Office = Conversion.getStrOrNull(R["Office"]);
                        record.InvSerial = Conversion.getStrOrNull(R["InvSerial"]);
                        record.InvNo = Conversion.getInt32OrNull(R["InvNo"]);
                        record.InvDate = Conversion.getDateTimeOrNull(R["InvDate"]);
                        record.InvType = Conversion.getInt16OrNull(R["InvType"]);
                        record.SumType = Conversion.getInt16OrNull(R["SumType"]);
                        record.ClientType = Conversion.getByteOrNull(R["ClientType"]);
                        record.ClientCode = Conversion.getStrOrNull(R["ClientCode"]);
                        record.ClientName = Conversion.getStrOrNull(R["ClientName"]);
                        record.ClientAddress = Conversion.getStrOrNull(R["ClientAddress"]);
                        record.ClientZip = Conversion.getStrOrNull(R["ClientZip"]);
                        record.ClientCity = Conversion.getStrOrNull(R["ClientCity"]);
                        record.ClientCountry = Conversion.getStrOrNull(R["ClientCountry"]);
                        record.Proforma = Conversion.getBoolOrNull(R["Proforma"]);
                        record.InvAmount = Conversion.getDecimalOrNull(R["InvAmount"]);
                        record.InvCur = Conversion.getStrOrNull(R["InvCur"]);
                        record.Tax = Conversion.getDecimalOrNull(R["Tax"]);
                        record.DueDate = Conversion.getDateTimeOrNull(R["DueDate"]);
                        record.Language = Conversion.getStrOrNull(R["Language"]);
                        record.Explanation = Conversion.getStrOrNull(R["Explanation"]);
                        record.InBatch = Conversion.getBoolOrNull(R["InBatch"]);
                        record.ResAmount = Conversion.getDecimalOrNull(R["ResAmount"]);
                        record.ResCur = Conversion.getStrOrNull(R["ResCur"]);
                        record.Rate = Conversion.getDecimalOrNull(R["Rate"]);
                        record.RateDate = Conversion.getDateTimeOrNull(R["RateDate"]);
                        record.Status = Conversion.getInt16OrNull(R["Status"]);
                        record.ReceiptSerial = Conversion.getStrOrNull(R["ReceiptSerial"]);
                        record.ReceiptNo = Conversion.getInt32OrNull(R["ReceiptNo"]);
                        record.SendToAcc = Conversion.getStrOrNull(R["SendToAcc"]);
                        record.SendToAccDate = Conversion.getDateTimeOrNull(R["SendToAccDate"]);
                        record.SendToAccUser = Conversion.getStrOrNull(R["SendToAccUser"]);
                        record.FormID = Conversion.getInt32OrNull(R["FormID"]);
                        record.Note = Conversion.getStrOrNull(R["Note"]);
                        record.CrtDate = Conversion.getDateTimeOrNull(R["CrtDate"]);
                        record.CrtUser = Conversion.getStrOrNull(R["CrtUser"]);
                        record.BankName = Conversion.getStrOrNull(R["BankName"]);
                        record.BankNo = Conversion.getStrOrNull(R["BankNo"]);
                        record.BankLocation = Conversion.getStrOrNull(R["BankLocation"]);
                        record.BankAccNo = Conversion.getStrOrNull(R["BankAccNo"]);
                        record.BankCur = Conversion.getStrOrNull(R["BankCur"]);
                        record.BankIBAN = Conversion.getStrOrNull(R["BankIBAN"]);
                        record.TaxOffice = Conversion.getStrOrNull(R["TaxOffice"]);
                        record.TaxAccNo = Conversion.getStrOrNull(R["TaxAccNo"]);
                        record.Phone1 = Conversion.getStrOrNull(R["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(R["Phone2"]);
                        record.Fax1 = Conversion.getStrOrNull(R["Fax1"]);
                        record.Fax2 = Conversion.getStrOrNull(R["Fax2"]);
                        record.email1 = Conversion.getStrOrNull(R["email1"]);
                        record.email2 = Conversion.getStrOrNull(R["email2"]);
                        record.PaidAmount = Conversion.getDecimalOrNull(R["PaidAmount"]);
                        record.InvImageType = Conversion.getInt16OrNull(R["InvImageType"]);
                        record.CrtAgency = Conversion.getStrOrNull(R["CrtAgency"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int? getNewProformaNumber()
        {
            string tsql = @"Select InvNo=Max(InvNo) From Invoice (NOLOCK) Where InvSerial='PRO' and Proforma=1";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                return Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool createInvoice(User UserData, string ResNo, string Operator, string Office, string clientCode, string clientName, string clientAddress, string clientZip,
                                  string clientCity, string clientCountry, decimal? invAmount, string invCur, byte[] invImage, string crtUser, string crtAgency,
                                  string eMail1, string eMail2, int proformaNumber)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string tsql = @"
                            Declare @RecID int
                            Insert Into Invoice (Operator, Office, InvSerial, InvNo, InvDate, InvType, SumType, ClientType, ClientCode, ClientName, ClientAddress,
				                                 ClientZip, ClientCity, ClientCountry, Proforma, InvAmount, InvCur, InvImage, InvImageType, CrtDate, CrtUser, CrtAgency,
                                                 email1, email2)
                            Values (@Operator, @Office, 'PRO', @InvNo, dbo.DateOnly(GetDate()), @InvType, @SumType, @ClientType, @ClientCode, @ClientName, @ClientAddress,
		                            @ClientZip, @ClientCity, @ClientCountry, 1, @InvAmount, @InvCur, @InvImage, 1, GetDate(), @CrtUser, @CrtAgency,
                                    @EMail1, @Email2)
                            Select @RecID=SCOPE_IDENTITY()

                            Insert Into InvoiceRes (InvoiceID, ResNo, ResInvAmount, ResInvAmountPrt)
                            Values (@RecID, @ResNo, @ResInvAmount, @ResInvAmountPrt)
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Office", DbType.String, Office);
                db.AddInParameter(dbCommand, "InvNo", DbType.Int32, proformaNumber);
                db.AddInParameter(dbCommand, "InvDate", DbType.DateTime, DateTime.Today);
                db.AddInParameter(dbCommand, "InvType", DbType.Int16, 0);
                db.AddInParameter(dbCommand, "SumType", DbType.Int16, 1);
                db.AddInParameter(dbCommand, "ClientType", DbType.Byte, 1);
                db.AddInParameter(dbCommand, "ClientCode", DbType.String, clientCode);
                db.AddInParameter(dbCommand, "ClientName", DbType.String, clientName);
                db.AddInParameter(dbCommand, "ClientAddress", DbType.String, clientAddress);
                db.AddInParameter(dbCommand, "ClientZip", DbType.String, clientZip);
                db.AddInParameter(dbCommand, "ClientCity", DbType.String, clientCity);
                db.AddInParameter(dbCommand, "ClientCountry", DbType.String, clientCountry);
                db.AddInParameter(dbCommand, "InvAmount", DbType.Decimal, invAmount);
                db.AddInParameter(dbCommand, "InvCur", DbType.String, invCur);
                db.AddInParameter(dbCommand, "InvImage", DbType.Binary, invImage);
                db.AddInParameter(dbCommand, "CrtUser", DbType.String, crtUser);
                db.AddInParameter(dbCommand, "CrtAgency", DbType.String, crtAgency);
                db.AddInParameter(dbCommand, "Email1", DbType.String, eMail1);
                db.AddInParameter(dbCommand, "Email2", DbType.String, eMail2);

                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "ResInvAmount", DbType.Decimal, invAmount);
                db.AddInParameter(dbCommand, "ResInvAmountPrt", DbType.Decimal, invAmount);

                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool invoiceIssued(User UserData, string ResNo)
        {
            string logUser = new TvBo.Common().logUserCreate(UserData);
            string tsql = @"
                            Update ResMain
                            Set InvoiceIssued='Y', InvoiceDate=GetDate()
                            Where ResNo=@ResNo
                           ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int GetPaxCountForBankComission(string resNumber, ref string errorMsg)
        {
            string tsql = @"Select TOP 1 * From ResCust C (NOLOCK)
                            Where C.ResNo=@ResNo and C.Status=0 and C.ppSalePrice>0 and C.Title<8
                            and Exists(Select * from ResSupDis Where ResNo=@ResNo and JournalID>0 and CustNo=C.CustNo) ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNumber);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                        return 0;
                    else return 1;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return 0;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PaymentPageDefination> getPaymentPageDefination(string Market, ref string errorMsg)
        {
            try
            {
                object _ppd = new TvBo.Common().getFormConfigValue("Payments", "PaymentPageDefination");
                List<PaymentPageDefination> ppd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentPageDefination>>(Conversion.getStrOrNull(_ppd));
                return ppd.Where(w => w.Market == Market).ToList<PaymentPageDefination>();
            }
            catch
            {
                return null;
            }
        }

        public decimal? ufn_GetResAmountForPay(string ResNo, ref string errorMsg)
        {
            string tsql = @"Select dbo.ufn_GetResAmountForPay(@ResNo)";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                return Conversion.getDecimalOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string GetRefNoForNordea(string resNo)
        {
            string strRefNo = "";
            string sql = "Select RefNo = dbo.DetBankPayRef(@ResNo)";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCmd = db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCmd, "ResNo", DbType.String, resNo);

            DataSet ds = db.ExecuteDataSet(dbCmd);

            if (ds.Tables[0].Rows.Count > 0)
                strRefNo = ds.Tables[0].Rows[0]["RefNo"].ToString();
            else
                strRefNo = "";

            dbCmd.Connection.Close();
            return strRefNo;
        }

        public List<PaymentTypeData> getPaymentTypeData(string market, string plMarket, string Operator, string agency, string currency, int? country, int? catPackID, DateTime? payDate, DateTime? resBegDate, int? night, int? preperVal, bool onlyAgencyPayCategory, bool onlyCreditCards, ref string errorMsg)
        {
            List<PaymentTypeData> records = new List<PaymentTypeData>();
            string tsql = string.Empty;
            if (catPackID > 0)
            {
                tsql = @"   Select *
                        From ( Select Distinct P.RecID PayTypeID, P.Code,Name = isnull(dbo.FindLocalName(P.NameLID, @Market), P.Name), 
                                       P.Paycat PayCatID, P.PosType,
                                       PayCatName = isnull(dbo.FindLocalName(PTC.NameLID, @Market), PTC.Name), 
                                       isnull(P.InstalNumFrom, 1) InstalNumFrom, isnull(p.InstalNumTo, 1) InstalNumTo,
                                       B.RecID CreditCardID, P.Bank, P.PayCur, isnull(P.SupDiscode, '''') SupDisCode,
                                       isnull(P.DiscountPer, 0) DiscountPer ,
                                       CreditCardName = isnull(dbo.FindLocalName(B.NameLID,@Market), B.Name),
                                       CreditCardCode = B.Code,
                                       isnull(P.ResPayMinPer, 100) ResPayMinPer,
                                       WPosBank = (Select Bank From BankCard K (NOLOCK) Where isnull(B.RedirectVPos, B.RecID) = K.RecID),
                                       isPicture = (CASE WHEN DATALENGTH(Picture) > 0 THEN Cast(1 AS bit) ELSE Cast(0 AS bit) END)
                                From PayType P (NOLOCK)
                                LEFT JOIN BankCard B (NOLOCK) ON P.CreditCard = B.Code and P.Bank = B.Bank and P.PayCat = 3
                                LEFT JOIN OperatorBank O (NOLOCK) ON P.Bank = O.Bank And O.Operator = @Operator
                                JOIN CatPackPayType CP (NOLOCK) ON CP.PayTypeID = P.RecID 
                                JOIN PayTypeCat PTC (NOLOCK) ON PTC.Code = P.PayCat
                                LEFT JOIN PayTypeDest (NOLOCK) PTD ON PTD.PayTypeID = P.RecID
                                Where	Market=@plMarket 
                                    And P.PayCur=@Currency 
                                    --And (@PrePerVal is null or ABS(P.DiscountPer)>=@PrePerVal)
                                    --And (@PrePerVal is null or isnull(p.InstalNumTo, 0)<=@PrePerVal)
                                    And (@PrePerVal is null or ((@PrePerVal between isnull(p.InstalNumFrom, 0) And isnull(p.InstalNumTo, 0))
							                                    OR (isnull(p.InstalNumTo, 0)<=@PrePerVal)))
                                    And @PayDate between IsNull(p.PayBegDate,@PayDate) 
                                    And IsNull(p.PayEndDate,@PayDate)
                                    And @ResBegDate between IsNull(p.ResBegDate,@ResBegDate) 
                                    And IsNull(p.ResEndDate,@ResBegDate)
                                    And @Night between IsNull(MinNight,@Night) 
                                    And IsNull(MaxNight,@Night)
                                    And (isnull(PTD.Country,0)=0 Or PTD.Country = @Country)
	                                And CP.CatPackID = @CatPackID ";
                if (onlyCreditCards)
                    tsql += " And p.Paycat=3 ";
                if (onlyAgencyPayCategory)
                    tsql += "		And P.PayCat in (Select PayCat FROM AgencyPasPayCat WHERE Agency=@Agency)  ";
                tsql += " ) MultiTbl ";
                tsql += "   Order By PayCatID, CreditCardID, InstalNumFrom";
            }
            else
            {
                tsql = @"  Select *
                        From (
                                Select Distinct P.RecID PayTypeID, P.Code,Name = isnull(dbo.FindLocalName(P.NameLID, @Market), P.Name), 
                                       P.Paycat PayCatID, P.PosType,
                                       PayCatName = isnull(dbo.FindLocalName(PTC.NameLID, @Market), PTC.Name), 
                                       isnull(P.InstalNumFrom, 1) InstalNumFrom, isnull(p.InstalNumTo, 1) InstalNumTo,
                                       B.RecID CreditCardID, P.Bank, P.PayCur, isnull(P.SupDiscode, '') SupDisCode,
                                       isnull(P.DiscountPer, 0) DiscountPer ,
                                       CreditCardName = isnull(dbo.FindLocalName(B.NameLID,@Market), B.Name),
                                       CreditCardCode = B.Code,
                                       isnull(P.ResPayMinPer, 100) ResPayMinPer,
                                       WPosBank = (Select Bank From BankCard K (NOLOCK) Where isnull(B.RedirectVPos, B.RecID) = K.RecID),
                                       isPicture = (CASE WHEN DATALENGTH(Picture) > 0 THEN Cast(1 AS bit) ELSE Cast(0 AS bit) END)
                                From PayType P (NOLOCK) 
                                    LEFT JOIN BankCard B (NOLOCK) ON P.CreditCard = B.Code and P.Bank = B.Bank and P.PayCat = 3
                                    LEFT JOIN OperatorBank O (NOLOCK) ON P.Bank = O.Bank And O.Operator = @Operator
								    LEFT JOIN CatPackPayType CP (NOLOCK) ON CP.PayTypeID = P.RecID
                                    JOIN PayTypeCat PTC (NOLOCK) ON PTC.Code = P.PayCat
                                    Left JOIN PayTypeDest (NOLOCK) PTD ON PTD.PayTypeID = P.RecID
                                Where   Market=@plMarket 
                                    And AllowWIS = 1
                                    And P.PayCur=@Currency                                     
                                    And (@PrePerVal is null or isnull(p.InstalNumTo, 0)<=@PrePerVal)
                                    And @PayDate between IsNull(p.PayBegDate,@PayDate) 
                                    And IsNull(p.PayEndDate,@PayDate)
                                    And @ResBegDate between IsNull(p.ResBegDate,@ResBegDate) 
                                    And IsNull(p.ResEndDate,@ResBegDate)
                                    And @Night between IsNull(MinNight,@Night) 
                                    And IsNull(MaxNight,@Night) 
                                    And (isnull(PTD.Country,0)=0 Or PTD.Country = @Country) ";
                if (onlyCreditCards)
                    tsql += " And p.Paycat=3 ";
                if (onlyAgencyPayCategory)
                    tsql += "		And P.PayCat in (Select PayCat FROM AgencyPasPayCat WHERE Agency=@Agency)  ";
                tsql += " ) MultiTbl ";
                tsql += "   Order By PayCatID, CreditCardID, InstalNumFrom";
            }

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, market);
                db.AddInParameter(dbCommand, "plMarket", DbType.String, plMarket);
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Currency", DbType.String, currency);
                db.AddInParameter(dbCommand, "CatPackID", DbType.String, catPackID);
                db.AddInParameter(dbCommand, "PayDate", DbType.String, payDate);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.String, resBegDate);
                db.AddInParameter(dbCommand, "Night", DbType.String, night);
                db.AddInParameter(dbCommand, "PrePerVal", DbType.String, (preperVal > -1) ? preperVal : Convert.DBNull);
                db.AddInParameter(dbCommand, "Agency", DbType.String, agency);
                db.AddInParameter(dbCommand, "Country", DbType.String, country);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        PaymentTypeData rec = new PaymentTypeData();

                        rec.PayTypeID = Conversion.getInt32OrNull(R["PayTypeID"]);
                        rec.Code = Conversion.getStrOrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.PayCatID = Conversion.getInt32OrNull(R["PayCatID"]);
                        rec.PosType = Conversion.getInt16OrNull(R["PosType"]);
                        rec.PayCatName = Conversion.getStrOrNull(R["PayCatName"]);
                        rec.InstalNumFrom = (Int16)R["InstalNumFrom"];
                        rec.InstalNumTo = (Int16)R["InstalNumTo"];
                        rec.CreditCardID = Conversion.getInt32OrNull(R["CreditCardID"]);
                        rec.Bank = Conversion.getStrOrNull(R["Bank"]);
                        rec.PayCur = Conversion.getStrOrNull(R["PayCur"]);
                        rec.SupDisCode = Conversion.getStrOrNull(R["SupDisCode"]);
                        rec.DiscountPer = Conversion.getDecimalOrNull(R["DiscountPer"]);
                        rec.CreditCardName = Conversion.getStrOrNull(R["CreditCardName"]);
                        rec.CreditCardCode = Conversion.getStrOrNull(R["CreditCardCode"]);
                        rec.ResPayMinPer = Conversion.getDecimalOrNull(R["ResPayMinPer"]);
                        rec.WPosBank = Conversion.getStrOrNull(R["WPosBank"]);
                        rec.isPicture = (bool)R["isPicture"];

                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public PaymentInfo getPaymentInfoForReservation(string resNo, int? custNo, ref string errorMsg)
        {
            string tsql = string.Empty;
            if (custNo.HasValue)
                tsql = @"   SELECT TOP 1
                                R.ResNo, R.PriceListNo, DiscountAmount=isnull(P.PasDiscBase, 0), 
                                ResAmount=P.ResAmount, R.PasAmount, SalePrice=P.ResAmount,
                                R.OptDate OptionTill, ConfStat = (CASE R.confstat 
                                                                        WHEN 0 THEN 'Request' 
                                                                        WHEN 1 THEN 'OK' 
                                                                        WHEN 2 THEN 'Not Confirm' 
                                                                        ELSE 'No Show' END), 
                                RConfStat = R.ConfStat,
                                R.SaleCur, C.CustNo, R.BegDate ResBegDate, R.Days Night, isnull(P.TotPayment, 0) PrePayment,
                                isnull(S.PerVal, 0) PrePerVal, Remain = P.Amount, 
                                J.InstalNum, PasPayRule = isnull(R.PasPayRule, 0)
                            FROM ResMain (NOLOCK) R 
                            JOIN ResCust (NOLOCK) C on R.ResNo = C.ResNo
                            Join dbo.GetPassAmountForPay(@ResNo, @CustNo) P ON P.CustNo = C.CustNo 
                            LEFT JOIN JournalCDet (NOLOCK) JCD on JCD.ResNo = R.ResNo                        
                            LEFT JOIN Journal (NOLOCK) J ON J.RecID = JCD.JournalID And J.Account = @CustNo
                            LEFT JOIN ResSupDis (NOLOCK) S on R.ResNo = S.ResNo and S.JournalID > 0 and S.CustNo = @CustNo
                            WHERE
                                R.ResNo = @ResNo
                            And C.CustNo = @CustNo
                            Order By J.PayDate ";
            else
                tsql = @"   SELECT TOP 1
                                R.ResNo, R.PriceListNo, dbo.ufn_Amount4Discount_Pas(R.ResNo) DiscountAmount, 
                                dbo.ufn_GetResAmount(R.ResNo, 4, 0) ResAmount, R.PasAmount, R.SalePrice,
                                R.OptDate OptionTill, ConfStat = (CASE R.confstat 
                                                                        WHEN 0 THEN 'Request' 
                                                                        WHEN 1 THEN 'OK' 
                                                                        WHEN 2 THEN 'Not Confirm' 
                                                                        ELSE 'No Show' END), 
                                RConfStat=R.ConfStat,
                                R.SaleCur, C.CustNo, R.BegDate ResBegDate, R.Days Night, R.AgencyPayment PrePayment,
                                S.PerVal PrePerVal, Remain = dbo.ufn_GetResAmount(R.ResNo, 3, 1), 
	                            J.InstalNum, PasPayRule = isnull(R.PasPayRule, 0)
                            FROM ResMain (NOLOCK) R 
                            JOIN ResCust (NOLOCK) C on R.ResNo = C.ResNo and C.Leader = 'Y'
                            LEFT JOIN JournalCDet (NOLOCK) JCD on JCD.ResNo = R.ResNo                        
                            LEFT JOIN Journal (NOLOCK) J ON J.RecID = JCD.JournalID
                            LEFT JOIN ResSupDis (NOLOCK) S on R.ResNo = S.ResNo and S.JournalID > 0 
                            WHERE
                                R.ResNo = @ResNo
                            Order By J.PayDate ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNo);
                db.AddInParameter(dbCommand, "CustNo", DbType.String, custNo);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        return new PaymentInfo
                        {
                            ResNo = Conversion.getStrOrNull(R["ResNo"]),
                            PriceListNo = Conversion.getInt32OrNull(R["PriceListNo"]),
                            DiscountAmount = Conversion.getDecimalOrNull(R["DiscountAmount"]),
                            ResAmount = Conversion.getDecimalOrNull(R["ResAmount"]),
                            PasAmount = Conversion.getDecimalOrNull(R["PasAmount"]),
                            SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]),
                            OptionTill = Conversion.getDateTimeOrNull(R["OptionTill"]),
                            ConfStat = Conversion.getStrOrNull(R["ConfStat"]),
                            RConfStat = Conversion.getInt16OrNull(R["RConfStat"]),
                            SaleCur = Conversion.getStrOrNull(R["SaleCur"]),
                            CustNo = Conversion.getInt32OrNull(R["CustNo"]),
                            ResBegDate = Conversion.getDateTimeOrNull(R["ResBegDate"]),
                            Night = Conversion.getInt16OrNull(R["Night"]),
                            PrePayment = Conversion.getDecimalOrNull(R["PrePayment"]),
                            PrePerVal = Conversion.getDecimalOrNull(R["PrePerVal"]),
                            Remain = Conversion.getDecimalOrNull(R["Remain"]),
                            InstalNum = Conversion.getInt16OrNull(R["InstalNum"]),
                            PasPayRule = Conversion.getByteOrNull(R["PasPayRule"])
                        };
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public CreditCardAndDetail prepareCardDetailTable(User UserData, List<PaymentTypeData> paymentData, decimal? remain, decimal? PrePayment, decimal? disCountprice, int minPerVal, string prePerVal)
        {
            CreditCardAndDetail retVal = new CreditCardAndDetail();
            List<CardDetail> cardDetail = new List<CardDetail>();
            List<Cards> cardList = new List<Cards>();
            decimal minpayper = 100;
            decimal disper = 0;
            string cardCode = string.Empty;
            foreach (PaymentTypeData row in paymentData)
            {
                if (row.PayCatID != 3)
                {
                    if (minPerVal < 0)
                    {
                        disper = prePerVal != "" ? (prePerVal == "0" ? row.DiscountPer.Value : 0) : row.DiscountPer.Value;
                        minpayper = prePerVal != "" ? (prePerVal == "0" ? row.ResPayMinPer.Value : 0) : row.ResPayMinPer.Value;
                    }
                    else
                    {
                        disper = 0;
                        minpayper = 0;
                    }
                    decimal Payment = Math.Round(remain.Value + (disCountprice.Value * disper / 100), 2);

                    CardDetail card = new CardDetail();
                    card.PayTypeID = row.PayTypeID;
                    card.Code = row.Code;
                    if (disper < 0)
                        card.FullName = row.PayCatName + " ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " İndirim )";
                    else
                        if (disper > 0)
                            card.FullName = row.PayCatName + " ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " Vade Farkı )";
                        else
                            card.FullName = row.PayCatName;

                    card.DiscountPrice = Payment;
                    card.Currency = row.PayCur;
                    card.Bank = row.Bank;
                    card.SupDisCode = row.SupDisCode;
                    card.InstalNum = 0;
                    card.MinPayPrice = Math.Round(Payment * minpayper / 100, 2);
                    card.PayCatID = row.PayCatID;
                    card.PrePayment = PrePayment;
                    cardDetail.Add(card);
                }
                else
                {
                    if (!string.Equals(cardCode, row.CreditCardID.ToString()))
                    {
                        Cards card = new Cards();
                        card.CreditCardID = row.CreditCardID;
                        card.CreditCardName = row.CreditCardName;
                        cardList.Add(card);
                    }
                    string taksitStr = "";
                    int possource = 1;
                    if (UserData.OwnAgency && row.PosType.HasValue)
                        possource = row.PosType.Value;

                    int forTo = minPerVal > 0 ? minPerVal : row.InstalNumTo;
                    for (int i = row.InstalNumFrom; i <= row.InstalNumTo; i++)
                    {
                        if (i > forTo) continue;
                        if (minPerVal < 0)
                        {
                            disper = prePerVal != "" ? (prePerVal == "0" ? row.DiscountPer.Value : 0) : row.DiscountPer.Value;
                            minpayper = prePerVal != "" ? (prePerVal == "0" ? row.ResPayMinPer.Value : 0) : row.ResPayMinPer.Value;
                        }
                        else
                        {
                            disper = 0;
                            minpayper = 0;
                        }
                        decimal Payment = Math.Round(remain.Value + (disCountprice.Value * disper / 100), 2);
                        if (i == 1)
                        {
                            taksitStr = " Tek Çekim ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " İndirim )";
                            if (disper < 0)
                                taksitStr = " Tek Çekim ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " İndirim )";
                            else
                                if (disper > 0)
                                    taksitStr = " Tek Çekim ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " Vade Farkı )";
                                else
                                    taksitStr = " Tek Çekim ";
                        }
                        else
                        {
                            if (disper < 0)
                                taksitStr = " " + i.ToString() + " Taksit ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " İndirim )";
                            else
                                if (disper > 0)
                                    taksitStr = " " + i.ToString() + " Taksit ( %" + string.Format("{0:0.##}", Math.Abs((decimal)disper)) + " Vade Farkı )";
                                else
                                    taksitStr = " " + i.ToString() + " Taksit ";
                        }
                        CardDetail card = new CardDetail();

                        card.PayTypeID = row.PayTypeID;
                        card.Code = row.Code;
                        card.FullName = row.CreditCardName.ToString() + taksitStr;
                        card.CreditCardID = row.CreditCardID;
                        card.DiscountPrice = Payment;
                        card.MinPayPrice = Math.Round(Payment * minpayper / 100, 2);
                        card.Currency = row.PayCur;
                        card.Bank = row.Bank;
                        card.SupDisCode = row.SupDisCode;
                        card.InstalNum = i;
                        card.PayCatID = row.PayCatID;
                        card.WPosBank = row.WPosBank;
                        card.PosType = possource;
                        card.PrePayment = PrePayment;
                        card.Selected = false;
                        cardDetail.Add(card);
                    }
                    cardCode = row.CreditCardID.ToString();
                }
            }
            retVal.card_Detail = cardDetail;
            retVal.CardList = cardList;
            return retVal;
        }

        public List<ReservationInvoice> getReservationInvoiceListV2(User UserData, string resNo, bool ClientInv, ref string errorMsg)
        {
            List<ReservationInvoice> records = new List<ReservationInvoice>();
            string tsql = string.Empty;

            tsql =
@"
Select I.RecID, I.InvSerial+' '+Cast(I.InvNo as varchar(8)) as InvoiceNo,
  I.InvSerial,I.InvNo,I.InvDate,I.InvType,I.ClientType,I.ClientCode,I.ClientName,
  I.Proforma,I.InvAmount,I.InvCur,Status--,I.InvImage
From Invoice I (NOLOCK)
Join InvoiceRes IR (NOLOCK) on IR.InvoiceID=I.RecID
Where IR.ResNo=@ResNo
";

            if (ClientInv) {
                tsql +=
@"
  And I.ClientType=1
";
            }

            tsql +=
@"
Order by I.RecID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNo);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        ReservationInvoice rec = new ReservationInvoice();

                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.InvoiceNo = Conversion.getStrOrNull(R["InvoiceNo"]);
                        rec.InvSerial = Conversion.getStrOrNull(R["InvSerial"]);
                        rec.InvNo = Conversion.getStrOrNull(R["InvNo"]);
                        rec.InvDate = Conversion.getDateTimeOrNull(R["InvDate"]);
                        rec.InvAmount = Conversion.getDecimalOrNull(R["InvAmount"]);
                        rec.InvCurr = Conversion.getStrOrNull(R["InvCur"]);
                        rec.Status = Conversion.getBoolOrNull(R["Status"]);

                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<ReservationInvoice> getReservationInvoiceList(User UserData, string resNo, ref string errorMsg)
        {
            List<ReservationInvoice> records = new List<ReservationInvoice>();
            string tsql = string.Empty;

            tsql =
@"
Select I.RecID, I.InvSerial+' '+Cast(I.InvNo as varchar(8)) as InvoiceNo,
  I.InvSerial,I.InvNo,I.InvDate,I.InvType,I.ClientType,I.ClientCode,I.ClientName,
  I.Proforma,I.InvAmount,I.InvCur,Status--,I.InvImage
From Invoice I (NOLOCK)
Join InvoiceRes IR (NOLOCK) on IR.InvoiceID=I.RecID
Where IR.ResNo=@ResNo
Order by I.RecID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, resNo);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ReservationInvoice rec = new ReservationInvoice();

                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.InvoiceNo = Conversion.getStrOrNull(R["InvoiceNo"]);
                        rec.InvSerial = Conversion.getStrOrNull(R["InvSerial"]);
                        rec.InvNo = Conversion.getStrOrNull(R["InvNo"]);
                        rec.InvDate = Conversion.getDateTimeOrNull(R["InvDate"]);
                        rec.InvAmount = Conversion.getDecimalOrNull(R["InvAmount"]);
                        rec.InvCurr = Conversion.getStrOrNull(R["InvCur"]);
                        rec.Status = Conversion.getBoolOrNull(R["Status"]);

                        records.Add(rec);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public object getReservationInvoice(User UserData, int invID, ref string errorMsg)
        {

            string tsql = string.Empty;
            tsql =
@"
Select InvImage
From Invoice (NOLOCK)
Where RecID=@invID
";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "invID", DbType.Int32, invID);
                return db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool createPaymentList(User UserData, decimal? payment, string payCurr, string payType, string resList, ref int? journalID, string referans, Int16? accountType)
        {
            XmlDocument paramsXml = new XmlDocument();
            if (resList == "<root></root>")
                paramsXml = null;
            else
                paramsXml.LoadXml(resList);
            string tsql =
@"
Declare @JournalID1 int
Exec dbo.usp_TVPaymentAllocation @Agency1,@Payment1,@PayCur1,@PayType1,@ResList1,@Reference1,@ErrCode1 OutPut,@JournalID1 OutPut
Select ErrCode=@ErrCode1,JournalID=@JournalID1
";
            string logUser = new TvBo.Common().logUserCreate(UserData);
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(logUser + tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency1", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Payment1", DbType.Decimal, payment);
                db.AddInParameter(dbCommand, "PayCur1", DbType.AnsiString, payCurr);
                db.AddInParameter(dbCommand, "PayType1", DbType.AnsiString, payType);
                db.AddInParameter(dbCommand, "ResList1", DbType.String, paramsXml == null ? string.Empty : paramsXml.InnerXml);
                db.AddInParameter(dbCommand, "Reference1", DbType.AnsiString, referans);                
                db.AddInParameter(dbCommand, "ErrCode1", DbType.Int32, accountType);                
                
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        int? errCode = Conversion.getInt32OrNull(R["ErrCode"]);
                        journalID = Conversion.getInt32OrNull(R["JournalID"]);
                        if (errCode.HasValue && errCode.Value == 0)
                        {                      
                            return true;
                        }
                        else
                        {
                            return false;
                        }                        
                    }
                    else
                    {
                        return false;
                    }
                }                
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

    }

    public class NovaPayments
    {
        public int? saveBeginPayment(User UserData, NovaBeforePament PaymentDetail)
        {
            string tsql =
@"
INSERT INTO NovaPaymentGateway (AgencyID,PaymentId,Curr,TotalAmount,PaymentResList,CreateDate,NovaSuccess,TvSuccess,PayTypeID)
                        VALUES(@AgencyID,@PaymentId,@Curr,@TotalAmount,@PaymentResList,GetDate(),0,0,@PayTypeID)
Select SCOPE_IDENTITY()
";
            string xml = string.Empty;
            if (PaymentDetail.PaymentResList != null)
            {
                xml += "<root>";
                foreach (AgencyPayList row in PaymentDetail.PaymentResList)
                {
                    xml += string.Format("<ResList ResNo=\"{0}\" Amount=\"{1}\" />", row.ResNo, row.PayAmount.HasValue ? (row.PayAmount.Value * 100).ToString("#.") : "");
                }
                xml += "</root>";
            }
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "AgencyID", DbType.AnsiString, PaymentDetail.AgencyID);
                db.AddInParameter(dbCommand, "Curr", DbType.AnsiString, PaymentDetail.Curr);
                db.AddInParameter(dbCommand, "TotalAmount", DbType.Decimal, PaymentDetail.TotalAmount);
                db.AddInParameter(dbCommand, "PaymentResList", DbType.AnsiString, xml);
                db.AddInParameter(dbCommand, "PaymentId", DbType.AnsiString, PaymentDetail.PaymentId);
                db.AddInParameter(dbCommand, "PayTypeID", DbType.Int32, PaymentDetail.PayTypeID);
                object obj = db.ExecuteScalar(dbCommand);
                return Conversion.getInt32OrNull(obj);
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public NovaBeforePament getBeginPayment(int? vk_RefId)
        {
            string tsql =
@"
Select RefNo,AgencyID,PaymentId,Curr,TotalAmount,CreateDate,NovaSuccess,NovaSuccessDate,TvSuccess,TvSuccessDate,TvRefNo,PaymentResList,PayTypeID
From NovaPaymentGateway (NOLOCK)
Where RefNo=@refNo
";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "refNo", DbType.Int32, vk_RefId);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        string xml = Conversion.getStrOrNull(R["PaymentResList"]);
                        List<AgencyPayList> PaymentResList = new List<AgencyPayList>();
                        if (!string.IsNullOrEmpty(xml))
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xml);
                            //Newtonsoft.Json.JsonConvert.SerializeXmlNode(xmlDoc);
                            XmlNode DocNode = xmlDoc.DocumentElement;
                            XmlNodeList BookNodeList = DocNode.SelectNodes("./ResList");
                            foreach (XmlNode Book in BookNodeList) {
                                string ResNo = Book.Attributes["ResNo"].Value;
                                string Amount = Book.Attributes["Amount"].Value;                                
                                PaymentResList.Add(new AgencyPayList {
                                    ResNo = ResNo,
                                    PayAmount = Conversion.getDecimalOrNull(Amount).HasValue ? Conversion.getDecimalOrNull(Amount).Value / (decimal)100 : 0,
                                    Currency = Conversion.getStrOrNull(R["Curr"])
                                });
                            }
                        }

                        return new NovaBeforePament
                        {
                            RefNo = Conversion.getInt32OrNull(R["RefNo"]),
                            AgencyID = Conversion.getStrOrNull(R["AgencyID"]),
                            PaymentId = Conversion.getStrOrNull(R["PaymentId"]),
                            Curr = Conversion.getStrOrNull(R["Curr"]),
                            TotalAmount = Conversion.getDecimalOrNull(R["TotalAmount"]),
                            CreateDate = Conversion.getDateTimeOrNull(R["CreateDate"]),
                            NovaSuccess = (bool)(R["NovaSuccess"]),
                            NovaSuccessDate = Conversion.getDateTimeOrNull(R["NovaSuccessDate"]),
                            TvSuccess = (bool)(R["TvSuccess"]),
                            TvSuccessDate = Conversion.getDateTimeOrNull(R["TvSuccessDate"]),
                            PaymentResList = PaymentResList,
                            PayTypeID = Conversion.getInt32OrNull(R["PayTypeID"])
                        };
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool updateNovaSuccess(int? vk_RefId, string transID, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"
if (Exists(Select null From NovaPaymentGateway (NOLOCK) Where RefNo=@RefNo And NovaSuccess=0 and TvSuccess=0))
Begin
    Update NovaPaymentGateway
    Set
        NovaSuccess=1,
        NovaSuccessDate=GetDate(),
        NovaTransId=@transId
    Where RefNo=@RefNo

    Select Cast(1 As Bit)
End
Else
Begin
  Select Cast(0 As Bit)
End
";
            //AgencyID,Curr,TotalAmount,PaymentResList,CreateDate,NovaSuccess,TvSuccess
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "RefNo", DbType.Int32, vk_RefId);
                db.AddInParameter(dbCommand, "transId", DbType.AnsiString, transID);
                bool? result = Conversion.getBoolOrNull(db.ExecuteScalar(dbCommand));
                return result.HasValue ? result.Value : false;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool updateTvSuccess(int? vk_RefId, int? tvRef)
        {
            string tsql = string.Empty;
            tsql =
@"
Update NovaPaymentGateway
Set
    TvSuccess=1,
    TvSuccessDate=GetDate(),
    TvRefNo=@TvRefNo
Where RefNo=@RefNo
";
            //AgencyID,Curr,TotalAmount,PaymentResList,CreateDate,NovaSuccess,TvSuccess
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "RefNo", DbType.Int32, vk_RefId);
                db.AddInParameter(dbCommand, "TvRefNo", DbType.Int32, tvRef);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}

