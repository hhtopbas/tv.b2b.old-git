﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
   public class SmsBo
    {
       public enum SmsType { NewReservation=1,PaymentReceived=2,CancellationOfReservation=3,PaymentReminder=4,RefundPayment=5 }
       public SmsBo()
       {

       }
       string _Market;
       public string Market
       {
           get { return _Market; }
           set { _Market = value; }
       }

       SmsType _Type;
       public SmsType Type
       {
           get { return _Type; }
           set { _Type = value; }
       }
       bool? _ValB2B;
       public bool? ValB2B
       {
           get { return _ValB2B; }
           set { _ValB2B = value; }
       }
       string _Text;
       public string Text
       {
           get { return _Text; }
           set { _Text = value; }
       }
       
    }
}
