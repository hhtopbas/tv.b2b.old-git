﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class BankRecord
    {
        public BankRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _SwiftCode;
        public string SwiftCode
        {
            get { return _SwiftCode; }
            set { _SwiftCode = value; }
        }

        string _BankNo;
        public string BankNo
        {
            get { return _BankNo; }
            set { _BankNo = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

    }

    public class CurrencyRecord
    {
        public CurrencyRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _FracName;
        public string FracName
        {
            get { return _FracName; }
            set { _FracName = value; }
        }

        string _FracNameL;
        public string FracNameL
        {
            get { return _FracNameL; }
            set { _FracNameL = value; }
        }

        string _FracNameS;
        public string FracNameS
        {
            get { return _FracNameS; }
            set { _FracNameS = value; }
        }

        string _IntCode;
        public string IntCode
        {
            get { return _IntCode; }
            set { _IntCode = value; }
        }
    }

    public class BankInfoRecord
    {
        public BankInfoRecord() 
        { 
        }
        
        string _BankName;
        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }

        string _BankIBAN;
        public string BankIBAN
        {
            get { return _BankIBAN; }
            set { _BankIBAN = value; }
        }

        string _BankCurr;
        public string BankCurr
        {
            get { return _BankCurr; }
            set { _BankCurr = value; }
        }
        
        string _BankAccNo;
        public string BankAccNo
        {
            get { return _BankAccNo; }
            set { _BankAccNo = value; }
        }
        
        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }
        
        string _BankBankNo;
        public string BankBankNo
        {
            get { return _BankBankNo; }
            set { _BankBankNo = value; }
        }
    }

    public class BankVPosRecord
    {
        public BankVPosRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        string _Host;
        public string Host
        {
            get { return _Host; }
            set { _Host = value; }
        }

        string _ClientID;
        public string ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }

        string _UserID;
        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        string _Password;
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        string _BankCur;
        public string BankCur
        {
            get { return _BankCur; }
            set { _BankCur = value; }
        }

        string _Interface;
        public string Interface
        {
            get { return _Interface; }
            set { _Interface = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        string _ProvUserID;
        public string ProvUserID
        {
            get { return _ProvUserID; }
            set { _ProvUserID = value; }
        }

        string _ProvUserPass;
        public string ProvUserPass
        {
            get { return _ProvUserPass; }
            set { _ProvUserPass = value; }
        }

        string _MerchantID;
        public string MerchantID
        {
            get { return _MerchantID; }
            set { _MerchantID = value; }
        }

        string _PosVersion;
        public string PosVersion
        {
            get { return _PosVersion; }
            set { _PosVersion = value; }
        }
    }

    public class OperatorBankRecord
    {
        public OperatorBankRecord()
        { }
        int? _RecID; public int? RecID { get { return _RecID; } set { _RecID = value; } }
        string _Operator; public string Operator { get { return _Operator; } set { _Operator = value; } }
        string _Bank; public string Bank { get { return _Bank; } set { _Bank = value; } }
        string _BankNo; public string BankNo { get { return _BankNo; } set { _BankNo = value; } }
        string _BranchNo; public string BranchNo { get { return _BranchNo; } set { _BranchNo = value; } }
        string _AccNo; public string AccNo { get { return _AccNo; } set { _AccNo = value; } }
        string _Cur; public string Cur { get { return _Cur; } set { _Cur = value; } }
        string _IBAN; public string IBAN { get { return _IBAN; } set { _IBAN = value; } }
        string _AccId; public string AccId { get { return _AccId; } set { _AccId = value; } }
        bool? _DefAcc; public bool? DefAcc { get { return _DefAcc; } set { _DefAcc = value; } }
        bool? _PrintDoc; public bool? PrintDoc { get { return _PrintDoc; } set { _PrintDoc = value; } }
    }

}
