﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;
using System.Collections.Generic;

namespace TvBo
{
    public class HolPacks
    {

        public HolPackSerSale getHolpackSaleableService(User UserData, string recID, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql =
@"
Select RecID,HolPack,ServiceType,[Service],CanSale 
From HolPackSerSale (NOLOCK)
Where RecID=@recID
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "holpack", DbType.Int32, recID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        HolPackSerSale record = new HolPackSerSale();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        record.ServiceType = Conversion.getStrOrNull(R["ServiceType"]);
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.CanSale = Conversion.getBoolOrNull(R["CanSale"]);

                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HolPackSerSale> getHolpackSaleableServiceList(User UserData, string holpack, ref string errorMsg)
        {
            string tsql = string.Empty;
            List<HolPackSerSale> records = new List<HolPackSerSale>();
            tsql =
@"
Select RecID,HolPack,ServiceType,[Service],CanSale 
From HolPackSerSale (NOLOCK)
Where HolPack=@holpack
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "holpack", DbType.String, holpack);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HolPackSerSale record = new HolPackSerSale();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        record.ServiceType = Conversion.getStrOrNull(R["ServiceType"]);
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.CanSale = Conversion.getBoolOrNull(R["CanSale"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HolPackFixDate> getCultureTourCheckInDays(User UserData, int? depCity, int? arrCity, string Direction, string HolPackCat, ref string errorMsg)
        {
            List<HolPackFixDate> records = new List<HolPackFixDate>();
            string tsql =
@"
Select CheckIn=Cd.FixDate,HolPackName=H.Name,HolPackNameL=isnull(dbo.FindLocalName(H.NameLID,@market),H.Name)
From HolPackFixDate (NOLOCK) Cd
Join HolPack (NOLOCK) H ON Cd.HolPack=H.Code
";
            string whereStr = string.Empty;
            if (depCity.HasValue)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += string.Format("H.DepCity={0}", depCity.ToString());
            }
            if (arrCity.HasValue)
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += string.Format("H.ArrCity={0}", arrCity.ToString());
            }
            if (!string.IsNullOrEmpty(Direction))
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += string.Format("H.Direction={0}", Direction.ToString());
            }
            if (!string.IsNullOrEmpty(HolPackCat))
            {
                if (whereStr.Length > 0) whereStr += " And ";
                whereStr += string.Format("H.Code='{0}'", HolPackCat.ToString());
            }
            if (whereStr.Length > 0)
            {
                tsql +=
@"
Where 
";
                tsql += whereStr;
            }
            tsql +=
@"
Order by FixDate
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HolPackFixDate record = new HolPackFixDate();
                        record.CheckIn = Conversion.getDateTimeOrNull(R["CheckIn"]);
                        record.HolPackName = Conversion.getStrOrNull(R["HolPackName"]);
                        record.HolPackNameL = Conversion.getStrOrNull(R["HolPackNameL"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<RRFHolPack> GetAllHolPackInfo(User UserData,ref string errorMsg)
        {
            List<RRFHolPack> records = new List<RRFHolPack>();
            string tsql =
                @"
                Select H.Code,H.Name
                From HolPack H (NOLOCK)";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        RRFHolPack record = new RRFHolPack();

                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<RRFHolPack> getRRFHolPack(User UserData, DateTime? BegDate, Int16? Night, Int16? ResType, ref string errorMsg)
        {
            List<RRFHolPack> records = new List<RRFHolPack>();
            string tsql =
@"
Select H.Code,H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@pMarket),H.Name),
	H.DepCity,DepCityName=dbo.ufn_GetLocationName(H.DepCity),DepCityNameL=dbo.GetLocationNameL(H.DepCity,@pMarket),
	H.ArrCity,ArrCityName=dbo.ufn_GetLocationName(H.ArrCity),DepCityNameL=dbo.GetLocationNameL(H.ArrCity,@pMarket),
	BegDate,EndDate,[Description],SaleCur,PNight,TANight,Template,ValHajj,ValUmrah
From HolPack H (NOLOCK)
Where H.Market=@pMarket
  And H.Ready='Y' And H.webpub='Y'
  And isnull(H.ValHajj,0)=@pIsHajj
  And isnull(H.ValUmrah,0)=@pIsUmrah  
  And @pBegDate<=H.EndDate And @pBegDate+@pNight>=H.BegDate
  And @pNight=H.PNight
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pMarket", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "pBegDate", DbType.DateTime, BegDate);
                db.AddInParameter(dbCommand, "pNight", DbType.Int16, Night);
                db.AddInParameter(dbCommand, "pIsUmrah", DbType.Boolean, ResType.HasValue && ResType.Value == 1);
                db.AddInParameter(dbCommand, "pIsHajj", DbType.Boolean, ResType.HasValue && ResType.Value == 2);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        RRFHolPack record = new RRFHolPack();

                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        record.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        record.DepCityNameL = Conversion.getStrOrNull(R["DepCityNameL"]);
                        record.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        record.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        record.DepCityNameL = Conversion.getStrOrNull(R["DepCityNameL"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.Description = Conversion.getStrOrNull(R["Description"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.PNight = Conversion.getInt32OrNull(R["PNight"]);
                        record.TANight = Conversion.getInt32OrNull(R["TANight"]);
                        record.Template = Conversion.getBoolOrNull(R["Template"]);
                        record.Hajj = Conversion.getBoolOrNull(R["ValHajj"]);
                        record.Umrah = Conversion.getBoolOrNull(R["ValUmrah"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HolPackSer> getHolpackServiceList(User UserData, string Holpack, ref string errorMsg)
        {
            List<HolPackSer> records = new List<HolPackSer>();
            string tsql =
@"
Select RecID,HolPack,StepNo,[Service],ServiceItem,IncPack,ValAccomNights,DispNo,Pilot,Board,SClass,Room
From HolPackSer (NOLOCK)
Where HolPack=@pHolpack
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pHolpack", DbType.AnsiString, Holpack);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HolPackSer record = new HolPackSer();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        record.StepNo = Conversion.getInt32OrNull(R["StepNo"]);
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.ServiceItem = Conversion.getStrOrNull(R["ServiceItem"]);
                        record.IncPack = Conversion.getStrOrNull(R["IncPack"]);
                        record.ValAccomNights = Conversion.getStrOrNull(R["ValAccomNights"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.Pilot = Conversion.getBoolOrNull(R["Pilot"]);
                        record.Board = Conversion.getStrOrNull(R["Board"]);
                        record.SClass = Conversion.getStrOrNull(R["SClass"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<HolPackPlan> getHolPackPlan(User UserData, string Holpack, ref string errorMsg)
        {
            List<HolPackPlan> records = new List<HolPackPlan>();
            string tsql =
@"
Select RecID,HolPack,StepNo,[Service],Direction,StartDay,Duration,Departure,Arrival,IncPack,Priced,AccomNights,Explanation,DispOrd,PayCom,PayComEB,PayPasEB,StepType,CanDiscount,
  NPS2SpoOrder,NPS2SpoPerc,CanEdit,CanDelete,DefSupplier
From HolPackPlan (NOLOCK)
Where HolPack=@pHolpack
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "pHolpack", DbType.AnsiString, Holpack);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        HolPackPlan record = new HolPackPlan();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        record.StepNo = Conversion.getInt32OrNull(R["StepNo"]);
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.Direction = Conversion.getStrOrNull(R["Direction"]);
                        record.StartDay = Conversion.getInt16OrNull(R["StartDay"]);
                        record.Duration = Conversion.getInt16OrNull(R["Duration"]);
                        record.Departure = Conversion.getInt32OrNull(R["Departure"]);
                        record.Arrival = Conversion.getInt32OrNull(R["Arrival"]);
                        record.IncPack = Conversion.getStrOrNull(R["IncPack"]);
                        record.Priced = Conversion.getStrOrNull(R["Priced"]);
                        record.AccomNights = Conversion.getStrOrNull(R["AccomNights"]);
                        record.Explanation = Conversion.getStrOrNull(R["Explanation"]);
                        record.DispOrd = Conversion.getInt16OrNull(R["DispOrd"]);
                        record.PayCom = Conversion.getStrOrNull(R["PayCom"]);
                        record.PayComEB = Conversion.getStrOrNull(R["PayComEB"]);
                        record.PayPasEB = Conversion.getBoolOrNull(R["PayPasEB"]);
                        record.StepType = Conversion.getByteOrNull(R["StepType"]);
                        record.CanDiscount = Conversion.getBoolOrNull(R["CanDiscount"]);
                        record.NPS2SpoOrder = Conversion.getByteOrNull(R["NPS2SpoOrder"]);
                        record.NPS2SpoPerc = Conversion.getDecimalOrNull(R["NPS2SpoPerc"]);
                        record.CanEdit = Conversion.getBoolOrNull(R["CanEdit"]);
                        record.CanDelete = Conversion.getBoolOrNull(R["CanDelete"]);
                        record.DefSupplier = Conversion.getStrOrNull(R["DefSupplier"]);                        

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
