﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public class SpecSerRQCodeRecord
    {
        public SpecSerRQCodeRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        byte? _Category;
        public byte? Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        bool? _WebDisp;
        public bool? WebDisp
        {
            get { return _WebDisp; }
            set { _WebDisp = value; }
        }

        bool? _Priced;
        public bool? Priced
        {
            get { return _Priced; }
            set { _Priced = value; }
        }

        int? _ExtServicePriceID;
        public int? ExtServicePriceID
        {
            get { return _ExtServicePriceID; }
            set { _ExtServicePriceID = value; }
        }

        string _ExtService;
        public string ExtService
        {
            get { return _ExtService; }
            set { _ExtService = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _ServiceItem;
        public string ServiceItem
        {
            get { return _ServiceItem; }
            set { _ServiceItem = value; }
        }

        decimal? _SaleAdlPrice;
        public decimal? SaleAdlPrice
        {
            get { return _SaleAdlPrice; }
            set { _SaleAdlPrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        Int16? _PriceType;
        public Int16? PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        Int16? _CalcType;
        public Int16? CalcType
        {
            get { return _CalcType; }
            set { _CalcType = value; }
        }

        decimal? _InfAge1;
        public decimal? InfAge1
        {
            get { return _InfAge1; }
            set { _InfAge1 = value; }
        }

        decimal? _InfAge2;
        public decimal? InfAge2
        {
            get { return _InfAge2; }
            set { _InfAge2 = value; }
        }

        decimal? _ChdAge1;
        public decimal? ChdAge1
        {
            get { return _ChdAge1; }
            set { _ChdAge1 = value; }
        }

        decimal? _ChdAge2;
        public decimal? ChdAge2
        {
            get { return _ChdAge2; }
            set { _ChdAge2 = value; }
        }

        decimal? _SaleInfPrice;
        public decimal? SaleInfPrice
        {
            get { return _SaleInfPrice; }
            set { _SaleInfPrice = value; }
        }

        decimal? _SaleChdPrice;
        public decimal? SaleChdPrice
        {
            get { return _SaleChdPrice; }
            set { _SaleChdPrice = value; }
        }

    }

    public class PricedSSRCode
    {
        public PricedSSRCode()
        {
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _SpecSerRQCode;
        public string SpecSerRQCode
        {
            get { return _SpecSerRQCode; }
            set { _SpecSerRQCode = value; }
        }

    }

    public class SSRQCodeRecord
    {
        public int? RecID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public string ServiceType { get; set; }
        public byte? Category { get; set; }
        public bool? NotAllowPlaneExitSeat { get; set; }
        public string PnlText { get; set; }
        public SSRQCodeRecord()
        { 
        }
    }
}
