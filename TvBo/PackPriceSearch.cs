﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using TvTools;

namespace TvBo
{
    public class PackPriceSearchs
    {
        public List<HotelMarOptRecord> getHotelMarOptList(User UserData, List<PackPriceSearchResult> data, ref string errorMsg)
        {
            List<HotelMarOptRecord> records = new List<HotelMarOptRecord>();
            var query = from q in data
                        group q by new { q.Hotel } into k
                        select new { Hotel = k.Key.Hotel };
            string tmpQueryStr = @"
if OBJECT_ID('TempDB.dbo.#tmpHotelTable') is not null Drop Table dbo.#tmpHotelTable
Create Table #tmpHotelTable (Hotel VarChar(10) COLLATE Latin1_General_CI_AS)";
            foreach (var row in query)
                tmpQueryStr += string.Format(" Insert Into #tmpHotelTable (Hotel) Values ('{0}') ", row.Hotel);

            tmpQueryStr += @"Select H.RecID, Hotel=HT.Hotel, Market=@Market, InfoWeb=isnull(H.InfoWeb, ''), H.MaxChdAge
                                 From #tmpHotelTable HT
                                 Left Join HotelMarOpt (NOLOCK) H ON H.Hotel=HT.Hotel
                                 Where H.Market=@Market";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tmpQueryStr);
            try {
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand)) {
                    while (oReader.Read()) {
                        HotelMarOptRecord record = new HotelMarOptRecord();
                        record.RecID = Conversion.getInt32OrNull(oReader["RecID"]);
                        record.Hotel = Conversion.getStrOrNull(oReader["Hotel"]);
                        record.Market = Conversion.getStrOrNull(oReader["Market"]);
                        record.InfoWeb = Conversion.getStrOrNull(oReader["InfoWeb"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Dispose();
            }
        }
        public SearchPLData addPPHotel(User UserData, List<PackPriceFilterCache> cacheData, List<HotelRecord> hotelList, List<Location> location, SearchPLData baseData, ref string errorMsg)
        {
            List<HolPackRecord> holPackList = new Common().getHolPackList(UserData.Market, ref errorMsg);
            List<plHotelData> data = (from c in cacheData
                                      join h in hotelList on c.HotelID equals h.RecID
                                      join hp in holPackList on c.HolPackID equals hp.RecID
                                      join l in location on h.Location equals l.RecID
                                      select new plHotelData {
                                          Code = h.Code,
                                          Name = h.Name,
                                          NameL = h.LocalName,
                                          Category = h.Category,
                                          Location = l.RecID,
                                          LocationName = location.Find(f => f.RecID == l.RecID).Name,
                                          LocationNameL = location.Find(f => f.RecID == l.RecID).NameL,
                                          ArrCity = l.City.Value,
                                          ArrCityName = location.Find(f => f.RecID == l.City).Name,
                                          ArrCityNameL = location.Find(f => f.RecID == l.City).NameL,
                                          Country = l.Country,
                                          CountryName = location.Find(f => f.RecID == l.Country).Name,
                                          CountryNameL = location.Find(f => f.RecID == l.Country).NameL,
                                          Town = l.Town,
                                          TownName = location.Find(f => f.RecID == l.Town) != null ? location.Find(f => f.RecID == l.Town).Name : "",
                                          TownNameL = location.Find(f => f.RecID == l.Town) != null ? location.Find(f => f.RecID == l.Town).NameL : "",
                                          Holpack = hp.Code,
                                          HolpackName = hp.Name,
                                          HolpackNameL = hp.NameL
                                      }).ToList();
            List<plHotelData> plData = (from d in data
                                        group d by new {
                                            Code = d.Code,
                                            Name = d.Name,
                                            NameL = d.NameL,
                                            Category = d.Category,
                                            Location = d.Location,
                                            LocationName = d.LocationName,
                                            LocationNameL = d.LocationNameL,
                                            ArrCity = d.ArrCity,
                                            ArrCityName = d.ArrCityName,
                                            ArrCityNameL = d.ArrCityNameL,
                                            Country = d.Country,
                                            CountryName = d.CountryName,
                                            CountryNameL = d.CountryNameL,
                                            Town = d.Town,
                                            TownName = d.TownName,
                                            TownNameL = d.TownNameL,
                                            Holpack = d.Holpack,
                                            HolpackName = d.HolpackName,
                                            HolpackNameL = d.HolpackNameL
                                        } into k
                                        select new plHotelData {
                                            Code = k.Key.Code,
                                            Name = k.Key.Name,
                                            NameL = k.Key.NameL,
                                            Category = k.Key.Category,
                                            Location = k.Key.Location,
                                            LocationName = k.Key.LocationName,
                                            LocationNameL = k.Key.LocationNameL,
                                            ArrCity = k.Key.ArrCity,
                                            ArrCityName = k.Key.ArrCityName,
                                            ArrCityNameL = k.Key.ArrCityNameL,
                                            Country = k.Key.Country,
                                            CountryName = k.Key.CountryName,
                                            CountryNameL = k.Key.CountryNameL,
                                            Town = k.Key.Town,
                                            TownName = k.Key.TownName,
                                            TownNameL = k.Key.TownNameL,
                                            Holpack = k.Key.Holpack,
                                            HolpackName = k.Key.HolpackName,
                                            HolpackNameL = k.Key.HolpackNameL
                                        }).ToList();
            baseData.plHotels = plData;
            return baseData;
        }
        public SearchPLData addPPResort(User UserData, List<PackPriceFilterCache> cacheData, List<HotelRecord> hotelList, List<Location> location, SearchPLData baseData, ref string errorMsg)
        {
            List<plResortData> plData = (from c in cacheData
                                         join h in hotelList on c.HotelID equals h.RecID
                                         join l in location on h.Location equals l.RecID
                                         group l by new {
                                             RecID = l.RecID,
                                             Name = l.Name,
                                             NameL = l.NameL,
                                             Country = l.Country,
                                             City = l.City,
                                             Town = l.Town
                                         } into k
                                         select new plResortData {
                                             RecID = k.Key.RecID,
                                             Name = k.Key.Name,
                                             NameL = k.Key.NameL,
                                             Country = k.Key.Country,
                                             City = k.Key.City.Value,
                                             Town = k.Key.Town
                                         }).ToList();
            baseData.plResorts = plData;
            return baseData;
        }
        public SearchPLData addPPRoom(User UserData, List<PackPriceFilterCache> cacheData, List<HotelRecord> hotelList, List<Location> location, SearchPLData baseData, ref string errorMsg)
        {
            List<HotelRoomRecord> hotelRoomList = new Hotels().getHotelRoomList(UserData.Market, ref errorMsg);

            List<plRoomData> data = (from c in cacheData
                                     join h in hotelList on c.HotelID equals h.RecID
                                     join b in hotelRoomList on c.BoardID equals b.RecID
                                     join l in location on h.Location equals l.RecID
                                     select new plRoomData {
                                         Name = b.Name,
                                         NameL = b.NameL,
                                         Hotel_Code = h.Code,
                                         Code = b.Code,
                                         City = l.City.Value,
                                         Town = l.Town,
                                         Village = l.Village,
                                         PLArrCity = l.City.Value,
                                         PLDepCity = c.DepCityID.Value
                                     }).ToList();
            List<plRoomData> plData = (from d in data
                                       group d by new {
                                           Name = d.Name,
                                           NameL = d.NameL,
                                           Hotel_Code = d.Hotel_Code,
                                           Code = d.Code,
                                           City = d.City,
                                           Town = d.Town,
                                           Village = d.Village,
                                           PLArrCity = d.PLArrCity,
                                           PLDepCity = d.PLDepCity
                                       } into k
                                       select new plRoomData {
                                           Name = k.Key.Name,
                                           NameL = k.Key.NameL,
                                           Hotel_Code = k.Key.Hotel_Code,
                                           Code = k.Key.Code,
                                           City = k.Key.City,
                                           Town = k.Key.Town,
                                           Village = k.Key.Village,
                                           PLArrCity = k.Key.PLArrCity,
                                           PLDepCity = k.Key.PLDepCity
                                       }).ToList();

            baseData.plRooms = plData;
            return baseData;
        }
        public SearchPLData addPPBoard(User UserData, List<PackPriceFilterCache> cacheData, List<HotelRecord> hotelList, List<Location> location, SearchPLData baseData, ref string errorMsg)
        {
            List<HotelBoardRecord> hotelBoardList = new Hotels().getHotelBoardList(UserData.Market, ref errorMsg);

            List<plBoardData> data = (from c in cacheData
                                      join h in hotelList on c.HotelID equals h.RecID
                                      join b in hotelBoardList on c.BoardID equals b.RecID
                                      join l in location on h.Location equals l.RecID
                                      select new plBoardData {
                                          Name = b.Name,
                                          NameL = b.NameL,
                                          Hotel_Code = h.Code,
                                          Code = b.Code,
                                          City = l.City.Value,
                                          Town = l.Town,
                                          Village = l.Village,
                                          PLArrCity = l.City.Value,
                                          PLDepCity = c.DepCityID.Value
                                      }).ToList();
            List<plBoardData> plData = (from d in data
                                        group d by new {
                                            Name = d.Name,
                                            NameL = d.NameL,
                                            Hotel_Code = d.Hotel_Code,
                                            Code = d.Code,
                                            City = d.City,
                                            Town = d.Town,
                                            Village = d.Village,
                                            PLArrCity = d.PLArrCity,
                                            PLDepCity = d.PLDepCity
                                        } into k
                                        select new plBoardData {
                                            Name = k.Key.Name,
                                            NameL = k.Key.NameL,
                                            Hotel_Code = k.Key.Hotel_Code,
                                            Code = k.Key.Code,
                                            City = k.Key.City,
                                            Town = k.Key.Town,
                                            Village = k.Key.Village,
                                            PLArrCity = k.Key.PLArrCity,
                                            PLDepCity = k.Key.PLDepCity
                                        }).ToList();
            baseData.plBoards = plData;
            return baseData;
        }
        public SearchPLData addPPHolPack(User UserData, List<PackPriceFilterCache> cacheData, List<HotelRecord> hotelList, List<Location> location, SearchPLData baseData, ref string errorMsg)
        {
            List<HolPackRecord> holPackList = new Common().getHolPackList(UserData.Market, ref errorMsg);
            List<plHolPackData> data = (from c in cacheData
                                        join h in holPackList on c.HolPackID equals h.RecID
                                        select new plHolPackData {
                                            Name = h.Name,
                                            NameL = h.NameL,
                                            HolPack = h.Code,
                                            DepCity = c.DepCityID.Value,
                                            ArrCity = c.ArrCityID.Value,
                                            Country = location.Find(f => f.RecID == c.ArrCityID).RecID,
                                        }).ToList();
            List<plHolPackData> plData = (from d in data
                                          group d by new {
                                              d.Name,
                                              d.NameL,
                                              d.HolPack,
                                              d.DepCity,
                                              d.ArrCity,
                                              d.Country
                                          } into k
                                          select new plHolPackData {
                                              Name = k.Key.Name,
                                              NameL = k.Key.NameL,
                                              HolPack = k.Key.HolPack,
                                              DepCity = k.Key.DepCity,
                                              ArrCity = k.Key.ArrCity,
                                              Country = k.Key.Country
                                          }).ToList();
            baseData.plHolPacks = plData;
            return baseData;
        }
        public SearchPLData addPPCategory(User UserData, List<PackPriceFilterCache> cacheData, List<HotelRecord> hotelList, List<Location> location, SearchPLData baseData, ref string errorMsg)
        {
            List<HotelCatRecord> hotelCatList = new Hotels().getHotelCatList(UserData, ref errorMsg);
            List<plCategoryData> cData = (from c in cacheData
                                          join h in hotelList on c.HotelID equals h.RecID
                                          join ct in hotelCatList on h.Category equals ct.Code
                                          join l in location on h.Location equals l.RecID
                                          select new plCategoryData {
                                              Code = h.Category,
                                              Name = ct.Name,
                                              NameL = ct.NameL,
                                              Country = l.Country,
                                              PLCountry = l.Country,
                                              City = l.City.Value,
                                              PLCity = l.City,
                                              Town = l.Town,
                                              Village = l.Village
                                          }).ToList<plCategoryData>();
            List<plCategoryData> plData = (from d in cData
                                           group d by new {
                                               d.Code,
                                               d.Name,
                                               d.NameL,
                                               d.Country,
                                               d.PLCountry,
                                               d.City,
                                               d.PLCity,
                                               d.Town,
                                               d.Village
                                           } into k
                                           select new plCategoryData {
                                               Code = k.Key.Code,
                                               Name = k.Key.Name,
                                               NameL = k.Key.NameL,
                                               Country = k.Key.Country,
                                               PLCountry = k.Key.PLCountry,
                                               City = k.Key.City,
                                               PLCity = k.Key.PLCity,
                                               Town = k.Key.Town,
                                               Village = k.Key.Village
                                           }).ToList<plCategoryData>();
            baseData.plCategorys = plData;
            return baseData;
        }
        public SearchPLData addPPLocation(User UserData, List<PackPriceFilterCache> cacheData, List<HotelRecord> hotelList, List<Location> location, SearchPLData baseData, ref string errorMsg)
        {
            List<plLocationData> data = (from c in cacheData
                                         join h in hotelList on c.HotelID equals h.RecID
                                         join l in location on h.Location equals l.RecID
                                         //where l.Type == 2
                                         select new plLocationData {
                                             DepCity = c.DepCityID.Value,
                                             DepCityName = location.Find(f => f.RecID == c.DepCityID).Name,
                                             DepCityNameL = location.Find(f => f.RecID == c.DepCityID).NameL,
                                             ArrCountry = l.Country,
                                             ArrCountryName = location.Find(f => f.RecID == l.Country).Name,
                                             ArrCountryNameL = location.Find(f => f.RecID == l.Country).NameL,
                                             ArrCity = c.ArrCityID.Value,
                                             ArrCityName = location.Find(f => f.RecID == c.ArrCityID).Name,
                                             ArrCityNameL = location.Find(f => f.RecID == c.ArrCityID).NameL
                                         }).ToList<plLocationData>();
            List<plLocationData> plData = (from d in data
                                           group d by new {
                                               DepCity = d.DepCity,
                                               DepCityName = d.DepCityName,
                                               DepCityNameL = d.DepCityNameL,
                                               ArrCountry = d.ArrCountry,
                                               ArrCountryName = d.ArrCountryName,
                                               ArrCountryNameL = d.ArrCountryNameL,
                                               ArrCity = d.ArrCity,
                                               ArrCityName = d.ArrCityName,
                                               ArrCityNameL = d.ArrCityNameL
                                           } into k
                                           select new plLocationData {
                                               DepCity = k.Key.DepCity,
                                               DepCityName = k.Key.DepCityName,
                                               DepCityNameL = k.Key.DepCityNameL,
                                               ArrCountry = k.Key.ArrCountry,
                                               ArrCountryName = k.Key.ArrCountryName,
                                               ArrCountryNameL = k.Key.ArrCountryNameL,
                                               ArrCity = k.Key.ArrCity,
                                               ArrCityName = k.Key.ArrCityName,
                                               ArrCityNameL = k.Key.ArrCityNameL
                                           }).ToList();
            baseData.PlLocations = plData;
            return baseData;
        }
        public SearchPLData getPackPriceSearchFilterCacheData(User UserData, List<PackPriceFilterCache> cacheData, ref string errorMsg)
        {
            SearchPLData sPLData = new SearchPLData();
            List<Location> location = new Locations().getLocationList(UserData.Market, LocationType.None, null, null, null, null, ref errorMsg);
            List<HotelRecord> hotelList = new Hotels().getHotelDetail(UserData, null, null, ref errorMsg);
            MarketRecord userMarket = new Parameters().getMarket(UserData.Market, ref errorMsg);

            sPLData = addPPLocation(UserData, cacheData, hotelList, location, sPLData, ref errorMsg);
            sPLData = addPPCategory(UserData, cacheData, hotelList, location, sPLData, ref errorMsg);
            sPLData = addPPHolPack(UserData, cacheData, hotelList, location, sPLData, ref errorMsg);
            sPLData = addPPBoard(UserData, cacheData, hotelList, location, sPLData, ref errorMsg);
            sPLData = addPPRoom(UserData, cacheData, hotelList, location, sPLData, ref errorMsg);
            sPLData = addPPResort(UserData, cacheData, hotelList, location, sPLData, ref errorMsg);
            sPLData = addPPHotel(UserData, cacheData, hotelList, location, sPLData, ref errorMsg);
            sPLData.PlMaxPaxCount = new plMaxPaxCounts() {
                maxAdult = 8,
                maxChd = 4,
                maxChdAgeG1 = 4,
                maxChdAgeG2 = 4,
                maxChdAgeG3 = 4,
                maxChdAgeG4 = 4
            };
            
            return sPLData;
        }
        public SearchPLData getPackPriceSearchFilterCacheData(string Operator, string Market, bool Groups, ref string errorMsg)
        {
            SearchPLData sPLData = new SearchPLData();
            List<Location> location = new Locations().getLocationList(Market, LocationType.None, null, null, null, null, ref errorMsg);

            string tsql =
@"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

Declare @SaleDate Date
Select @SaleDate=GETDATE()

SET NOCOUNT ON

Declare @Location TABLE (RecID int,Name varchar(100),NameL nvarchar(100),Country int,City int,Town int,Village int,Type smallint)

Insert Into @Location (RecID,Name,NameL,Country,City,Town,Village,Type)
Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID,@Market),Name),Country,City,Town,Village,[Type] 
From Location (NOLOCK)

--Locations
Select Distinct DepCity=Ldep.RecID,DepCityName=Ldep.Name,DepCityNameL=Ldep.NameL,
  ArrCountry=L.Country,ArrCountryName=Lu.Name,ArrCountryNameL=Lu.NameL,
  ArrCity=L.City,ArrCityName=L.Name,ArrCityNameL=L.NameL  
From B2BPackPriceFilter P
Join Hotel H (NOLOCK) ON H.RecID=P.HotelID
Join @Location Ldep ON P.DepCityID=Ldep.RecID
Join @Location L ON H.Location=L.RecID
Join @Location Lc ON L.City=Lc.RecID
Join @Location Lu ON L.Country=Lu.RecID
Where L.Type=2
--Locations

--Category 
Select Distinct Code=H.Category,Name=HC.Name,NameL=isnull(dbo.FindLocalName(HC.NameLID,@Market),HC.Name),
    Country=L.Country,PL_Country=L.Country,City=L.City,PL_City=L.City,Town=L.Town,Village=L.Village  
From B2BPackPriceFilter P 
Join Hotel H (NOLOCK) ON P.HotelID=H.RecID
Join HotelCat HC (NOLOCK) ON HC.Code=H.Category     
Join @Location L ON H.Location=L.RecID
--Category

--HolPack 
Select Distinct H.[Name],NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),HolPack=H.Code,
   DepCity=P.DepCityID,ArrCity=p.ArrCityID,Country=L.Country,CatPackID=null 
From B2BPackPriceFilter P 
Join HolPack H (NOLOCK) ON P.HolPackID=H.RecID
Join @Location L ON P.ArrCityID=L.RecID
--HolPack 

--Board
    Select Distinct Name=B.Name,NameL=isnull(dbo.FindLocalName(B.NameLID,@Market),B.Name),
         Hotel_Code=H.Code,B.Code,L.City,L.Town,L.Village,PLArrCity=L.City,PLDepCity=P.DepCityID
    From B2BPackPriceFilter P
    Join Hotel H (NOLOCK) ON H.RecID=P.HotelID        
    Join HotelBoard B (NOLOCK) ON B.RecID=P.BoardID
    Join @Location L ON H.Location=L.RecID    
--Board

--Rooms 
   Select Distinct Name=R.Name,NameL=isnull(dbo.FindLocalName(R.NameLID,@Market),R.Name),
                  Hotel_Code=H.Code,R.Code,L.City,L.Town,L.Village,PLArrCity=L.City,PLDepCity=P.DepCityID,
				  R.RoomViewID        
    From B2BPackPriceFilter P 
    Join Hotel H (NOLOCK) ON P.HotelID=H.RecID  
    Join HotelRoom R (NOLOCK) ON R.RecID=P.RoomID    
    Join @Location L ON H.Location=L.RecID    
--Rooms

--Resort  
Select Distinct RecID=L.RecID,Name=L.Name,NameL, --=isnull(dbo.FindLocalName(L.NameLID,@Market),L.Name),
L.Country,L.City 
From B2BPackPriceFilter P
Join Hotel H (NOLOCK) ON P.HotelID=H.RecID    
Join @Location L ON H.Location=L.RecID
--Resort

--Hotels 
Select Distinct H.Name,NameL=isnull(dbo.FindLocalName(H.NameLID,@Market),H.Name),
  H.Code,H.Category,Location=L.RecID,L.Country,L.City,L.Town,ArrCity=L.City,CatPackID=null,Holpack=Hp.Code
From B2BPackPriceFilter P
Join Hotel H (NOLOCK) ON P.HotelID=H.RecID    
Join HolPack Hp (NOLOCK) ON P.HolPackID=Hp.RecID
Join @Location L ON H.Location=L.RecID
--Hotels 

--Max Pax Count
Select Adult=8,ChdAgeG1=4,ChdAgeG2=4,ChdAgeG3=4,ChdAgeG4=4
--Max Pax Count

-- Room View
Select [Name],Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity 
From (     
  SELECT Hotel_Code=H.Code,Code=RW.RecID,RW.Name,L.City,L.Town,L.Village,         
    PLArrCity=L.City,PLDepCity=P.DepCityID
  From B2BPackPriceFilter P 
  Join Hotel H (NOLOCK) ON P.HotelID=H.RecID    
  JOIN @Location L ON L.RecID=H.Location     
  JOIN HotelRoom HR (NOLOCK) ON HR.Hotel=H.Code     
  JOIN RoomView RW (NOLOCK) ON HR.RoomViewID=RW.RecID  
) Rooms 
Group By [Name],Hotel_Code,Code,City,Town,Village,PLArrCity,PLDepCity 
-- Room View
";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                ds.Tables[0].TableName = "Locations";
                ds.Tables[1].TableName = "Category";
                ds.Tables[2].TableName = "HolPack";
                ds.Tables[3].TableName = "Board";
                ds.Tables[4].TableName = "Room";
                ds.Tables[5].TableName = "Resort";
                ds.Tables[6].TableName = "Hotels";
                ds.Tables[7].TableName = "MaxPaxCount";
                ds.Tables[8].TableName = "RoomView";

                sPLData = new Search().addPLLocationsB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLCategorysB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLHolPacksB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLBoardsB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLRoomsB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLResortsB2BMenuCat(ds, sPLData);
                sPLData = new Search().addPLHotelsB2BMenuCat(ds, sPLData, location);
                sPLData = new Search().addPLMaxPaxB2BMenuCat(ds, sPLData);

                return sPLData;
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<PackPriceSearchResult> searchPackPrice(User UserData, SearchCriteria filter, Guid? logID, ref string errorMsg)
        {
            List<PackPriceSearchResult> records = new List<PackPriceSearchResult>();

            List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

            StringBuilder sq = new StringBuilder();
            string param = string.Empty;

            #region Create xml Parameters
            param += "<ROOT>";
            #region How many rooms searching
            foreach (SearchCriteriaRooms row in filter.RoomsInfo) {
                param += string.Format("<SEARCH RoomNr=\"{0}\" Adult=\"{1}\" Child=\"{2}\" ", row.RoomNr, row.Adult, row.Child);
                if (row.Child > 0) param += string.Format("ChdAge1=\"{0}\" ", row.Chd1Age);
                if (row.Child > 1) param += string.Format("ChdAge2=\"{0}\" ", row.Chd2Age);
                if (row.Child > 2) param += string.Format("ChdAge3=\"{0}\" ", row.Chd3Age);
                if (row.Child > 3) param += string.Format("ChdAge4=\"{0}\" ", row.Chd4Age);
                param += "/>";
            }
            #endregion
            #region How many search location
            if (!string.IsNullOrEmpty(filter.Resort)) {
                if (filter.Resort.Split('|').Length > 0) {
                    if (filter.Resort.Split('|').Length == 1) {
                        param += string.Format("<LOCATIONS Location=\"{0}\" /> ", filter.Resort.Split('|')[0]);
                    } else {
                        foreach (string r in filter.Resort.Split('|')) {
                            Location _location = locations.Find(f => f.RecID == Conversion.getInt32OrNull(r));
                            if (_location != null) {
                                if (_location.Type == 3) {
                                    foreach (Location rL in locations.Where(w => w.Parent == _location.RecID))
                                        param += string.Format("<LOCATIONS Location=\"{0}\" /> ", rL.RecID);
                                } else
                                    param += string.Format("<LOCATIONS Location=\"{0}\" /> ", r);
                            }
                        }
                    }
                }
            }
            #endregion
            #region How many search hotel
            if (!string.IsNullOrEmpty(filter.Hotel)) {
                foreach (string r in filter.Hotel.Split('|'))
                    param += string.Format("<HOTELS Hotel=\"{0}\" />", r);
            }
            #endregion
            #region How many search hotel category
            if (!string.IsNullOrEmpty(filter.Category)) {
                foreach (string r in filter.Category.Split('|'))
                    param += string.Format("<HOTCATS Category=\"{0}\" />", r);
            }
            #endregion
            #region How many search hotel room
            if (!string.IsNullOrEmpty(filter.Room)) {
                foreach (string r in filter.Room.Split('|'))
                    param += string.Format("<ROOMS Room=\"{0}\" />", r);
            }
            #endregion
            #region How many search hotel board
            if (!string.IsNullOrEmpty(filter.Board)) {
                foreach (string r in filter.Board.Split('|'))
                    param += string.Format("<BOARDS Board=\"{0}\" />", r);
            }
            #endregion
            param += "</ROOT>";

            XmlDocument paramsXml = new XmlDocument();
            paramsXml.LoadXml(param);
            #endregion
            sq.AppendLine(
@"
 Exec dbo.usp_PackPriceMultiSearch @Market, @Operator, @Agency, @Country, @DepCity, @ArrCity, @CheckIn, @CheckIn2, @Night, @Night2, @HolPack, @ThemeId, @LangMarket, @SaleResource,
                                   @Price, @Price2, @Cur, @FlightNo, @FlightControl, @HotelControl, @StopSaleControl, @GetServiceDetail, @SaleType, @Params
");
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(sq.ToString());
            dbCommand.CommandTimeout = 120;
            try {
                int i = 0;
                db.AddInParameter(dbCommand, "Market", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "Operator", DbType.AnsiString, UserData.Operator);
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Country", DbType.Int32, null);
                db.AddInParameter(dbCommand, "DepCity", DbType.Int32, filter.DepCity);
                db.AddInParameter(dbCommand, "ArrCity", DbType.Int32, filter.ArrCity);
                db.AddInParameter(dbCommand, "CheckIn", DbType.DateTime, filter.CheckIn);
                db.AddInParameter(dbCommand, "CheckIn2", DbType.DateTime, filter.CheckIn.AddDays(filter.ExpandDate));
                db.AddInParameter(dbCommand, "Night", DbType.Int16, filter.NightFrom);
                db.AddInParameter(dbCommand, "Night2", DbType.Int16, filter.NightTo);
                db.AddInParameter(dbCommand, "HolPack", DbType.AnsiString, filter.Package);
                db.AddInParameter(dbCommand, "ThemeId", DbType.Int32, null);
                db.AddInParameter(dbCommand, "LangMarket", DbType.AnsiString, UserData.Market);
                db.AddInParameter(dbCommand, "SaleResource", DbType.Int16, 2);
                db.AddInParameter(dbCommand, "Price", DbType.Decimal, null);
                db.AddInParameter(dbCommand, "Price2", DbType.Decimal, null);
                db.AddInParameter(dbCommand, "Cur", DbType.AnsiString, !filter.CurControl || string.IsNullOrEmpty(filter.CurrentCur) ? null : filter.CurrentCur);
                db.AddInParameter(dbCommand, "FlightNo", DbType.AnsiString, string.Empty);
                db.AddInParameter(dbCommand, "FlightControl", DbType.Boolean, true /*filter.ShowAvailable*/);
                db.AddInParameter(dbCommand, "HotelControl", DbType.Boolean, true /*filter.ShowAvailable*/);
                db.AddInParameter(dbCommand, "StopSaleControl", DbType.Boolean, !filter.ShowStopSaleHotels);
                db.AddInParameter(dbCommand, "GetServiceDetail", DbType.Boolean, false);
                db.AddInParameter(dbCommand, "SaleType", DbType.Int32, null);
                db.AddInParameter(dbCommand, "Params", DbType.Xml, paramsXml.InnerXml);

                using (IDataReader R = db.ExecuteReader(dbCommand)) {
                    while (R.Read()) {
                        i++;
                        PackPriceSearchResult record = new PackPriceSearchResult();
                        record.SearchNr = Conversion.getInt32OrNull(R["SearchNr"]);
                        record.PriceID = Conversion.getInt32OrNull(R["PriceID"]);
                        record.PartNo = Conversion.getInt32OrNull(R["PartNo"]);
                        record.HSRecID = Conversion.getInt64OrNull(R["HSRecID"]);
                        record.HSPartNo = Conversion.getInt32OrNull(R["HSPartNo"]);
                        record.MarketID = Conversion.getInt32OrNull(R["MarketID"]);
                        record.HolPackID = Conversion.getInt32OrNull(R["HolPackID"]);
                        record.HotelID = Conversion.getInt32OrNull(R["HotelID"]);
                        record.RoomID = Conversion.getInt32OrNull(R["RoomID"]);
                        record.AccomID = Conversion.getInt32OrNull(R["AccomID"]);
                        record.BoardID = Conversion.getInt32OrNull(R["BoardID"]);
                        record.DepCity = Conversion.getInt32OrNull(R["DepCity"]);
                        record.ArrCity = Conversion.getInt32OrNull(R["ArrCity"]);
                        record.CheckIn = Conversion.getDateTimeOrNull(R["CheckIn"]);
                        record.Night = Conversion.getInt16OrNull(R["Night"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.AdlPrice = Conversion.getDecimalOrNull(R["AdlPrice"]);
                        record.EBed1Price = Conversion.getDecimalOrNull(R["EBed1Price"]);
                        record.EBed2Price = Conversion.getDecimalOrNull(R["EBed2Price"]);
                        record.ChdG1Price = Conversion.getDecimalOrNull(R["ChdG1Price"]);
                        record.ChdG2Price = Conversion.getDecimalOrNull(R["ChdG2Price"]);
                        record.ChdG3Price = Conversion.getDecimalOrNull(R["ChdG3Price"]);
                        record.ChdG4Price = Conversion.getDecimalOrNull(R["ChdG4Price"]);
                        record.Chd1Price = Conversion.getDecimalOrNull(R["Chd1Price"]);
                        record.Chd2Price = Conversion.getDecimalOrNull(R["Chd2Price"]);
                        record.Chd3Price = Conversion.getDecimalOrNull(R["Chd3Price"]);
                        record.Chd4Price = Conversion.getDecimalOrNull(R["Chd4Price"]);
                        record.Adl = Conversion.getInt16OrNull(R["Adl"]);
                        record.ExtBed = Conversion.getInt16OrNull(R["ExtBed"]);
                        record.Chd = Conversion.getInt16OrNull(R["Chd"]);
                        record.ChdAgeG1 = Conversion.getInt16OrNull(R["ChdAgeG1"]);
                        record.ChdAgeG2 = Conversion.getInt16OrNull(R["ChdAgeG2"]);
                        record.ChdAgeG3 = Conversion.getInt16OrNull(R["ChdAgeG3"]);
                        record.ChdAgeG4 = Conversion.getInt16OrNull(R["ChdAgeG4"]);
                        record.ChdG1Age1 = Conversion.getDecimalOrNull(R["ChdG1Age1"]);
                        record.ChdG1Age2 = Conversion.getDecimalOrNull(R["ChdG1Age2"]);
                        record.ChdG2Age1 = Conversion.getDecimalOrNull(R["ChdG2Age1"]);
                        record.ChdG2Age2 = Conversion.getDecimalOrNull(R["ChdG2Age2"]);
                        record.ChdG3Age1 = Conversion.getDecimalOrNull(R["ChdG3Age1"]);
                        record.ChdG3Age2 = Conversion.getDecimalOrNull(R["ChdG3Age2"]);
                        record.ChdG4Age1 = Conversion.getDecimalOrNull(R["ChdG4Age1"]);
                        record.ChdG4Age2 = Conversion.getDecimalOrNull(R["ChdG4Age2"]);
                        record.SaleType = Conversion.getInt16OrNull(R["SaleType"]);
                        record.Services = Conversion.getStrOrNull(R["Services"]);
                        record.PackDepFlight = Conversion.getStrOrNull(R["PackDepFlight"]);
                        record.CurID = Conversion.getInt32OrNull(R["CurID"]);
                        record.Market = Conversion.getStrOrNull(R["Market"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        record.DepName = Conversion.getStrOrNull(R["DepName"]);
                        record.DepNameL = Conversion.getStrOrNull(R["DepNameL"]);
                        record.ArrName = Conversion.getStrOrNull(R["ArrName"]);
                        record.ArrNameL = Conversion.getStrOrNull(R["ArrNameL"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        record.HotelNameL = Conversion.getStrOrNull(R["HotelNameL"]);
                        record.GiataCode = Conversion.getStrOrNull(R["GiataCode"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        record.RoomNameL = Conversion.getStrOrNull(R["RoomNameL"]);
                        record.Accom = Conversion.getStrOrNull(R["Accom"]);
                        record.AccomName = Conversion.getStrOrNull(R["AccomName"]);
                        record.AccomNameL = Conversion.getStrOrNull(R["AccomNameL"]);
                        record.Board = Conversion.getStrOrNull(R["Board"]);
                        record.BoardName = Conversion.getStrOrNull(R["BoardName"]);
                        record.BoardNameL = Conversion.getStrOrNull(R["BoardNameL"]);
                        record.HotCategory = Conversion.getStrOrNull(R["HotCategory"]);
                        record.CatName = Conversion.getStrOrNull(R["CatName"]);
                        record.CatNameL = Conversion.getStrOrNull(R["CatNameL"]);
                        record.HotLocation = Conversion.getInt32OrNull(R["HotLocation"]);
                        record.HotLocationName = Conversion.getStrOrNull(R["HotLocationName"]);
                        record.HotLocationNameL = Conversion.getStrOrNull(R["HotLocationNameL"]);
                        record.ChdCalcSale = Conversion.getBoolOrNull(R["ChdCalcSale"]);
                        record.FlightClass = Conversion.getStrOrNull(R["FlightClass"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.CurrentCur = Conversion.getStrOrNull(R["CurrentCur"]);
                        record.StdAdl = Conversion.getInt16OrNull(R["StdAdl"]);
                        record.AccomFullName = Conversion.getStrOrNull(R["AccomFullName"]);
                        record.StopSaleGuar = Conversion.getInt16OrNull(R["StopSaleGuar"]);
                        record.StopSaleStd = Conversion.getInt16OrNull(R["StopSaleStd"]);
                        record.HotelAllot = Conversion.getInt16OrNull(R["HotelAllot"]);
                        record.DepSeat = Conversion.getInt16OrNull(R["DepSeat"]);
                        record.RetSeat = Conversion.getInt16OrNull(R["RetSeat"]);
                        record.DepFlight = Conversion.getStrOrNull(R["DepFlight"]);
                        record.RetFlight = Conversion.getStrOrNull(R["RetFlight"]);
                        record.PackSPOID = Conversion.getInt32OrNull(R["PackSPOID"]);
                        record.PackSPOValID = Conversion.getInt32OrNull(R["PackSPOValID"]);
                        record.PackChildSPOID = Conversion.getInt32OrNull(R["PackChildSPOID"]);
                        record.DiscountType = Conversion.getInt16OrNull(R["DiscountType"]);
                        record.SpoPer = Conversion.getDecimalOrNull(R["SpoPer"]);
                        record.SpoAdlVal = Conversion.getDecimalOrNull(R["SpoAdlVal"]);
                        record.SpoExtB1Val = Conversion.getDecimalOrNull(R["SpoExtB1Val"]);
                        record.SpoExtB2Val = Conversion.getDecimalOrNull(R["SpoExtB2Val"]);
                        record.SpoChdGrp1Val = Conversion.getDecimalOrNull(R["SpoChdGrp1Val"]);
                        record.SpoChdGrp2Val = Conversion.getDecimalOrNull(R["SpoChdGrp2Val"]);
                        record.SpoChdGrp3Val = Conversion.getDecimalOrNull(R["SpoChdGrp3Val"]);
                        record.SpoChdGrp4Val = Conversion.getDecimalOrNull(R["SpoChdGrp4Val"]);
                        record.CHDSpoPer = Conversion.getDecimalOrNull(R["CHDSpoPer"]);
                        record.CHDSpoVal = Conversion.getDecimalOrNull(R["CHDSpoVal"]);
                        record.HotChd1Free = Conversion.getBoolOrNull(R["HotChd1Free"]);
                        record.HotChd2Free = Conversion.getBoolOrNull(R["HotChd2Free"]);
                        record.HotChd3Free = Conversion.getBoolOrNull(R["HotChd3Free"]);
                        record.HotChd4Free = Conversion.getBoolOrNull(R["HotChd4Free"]);
                        record.ChildBasePrice = Conversion.getDecimalOrNull(R["ChildBasePrice"]);
                        record.ChdSpSType = Conversion.getStrOrNull(R["ChdSpSType"]);
                        record.ChdSpPer = Conversion.getDecimalOrNull(R["ChdSpPer"]);
                        record.CHDSpVal = Conversion.getDecimalOrNull(R["CHDSpVal"]);
                        record.Chd1GrpNo = Conversion.getInt32OrNull(R["Chd1GrpNo"]);
                        record.Chd2GrpNo = Conversion.getInt32OrNull(R["Chd2GrpNo"]);
                        record.Chd3GrpNo = Conversion.getInt32OrNull(R["Chd3GrpNo"]);
                        record.Chd4GrpNo = Conversion.getInt32OrNull(R["Chd4GrpNo"]);
                        record.ChildSPOChdNo = Conversion.getInt32OrNull(R["ChildSPOChdNo"]);
                        record.ServiceDetail = Conversion.getStrOrNull(R["ServiceDetail"]);
                        record.CustomFixNote = Conversion.getStrOrNull(R["CustomFixNote"]);
                        record.AutoStop = Conversion.getBoolOrNull(R["AutoStop"]);
                        record.WebPub = Conversion.getStrOrNull(R["WebPub"]);
                        record.B2BActive = Conversion.getBoolOrNull(R["B2BActive"]);
                        record.B2CActive = Conversion.getBoolOrNull(R["B2CActive"]);
                        if (record.Market == UserData.Market)
                            records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex) {
                errorMsg = Ex.Message;
                return null;
            }
            finally {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        public List<PPHotelGroup> getHotelSalePriceGroup(User UserData, SearchCriteria filter, List<PackPriceSearchResult> priceData, bool useLocalName, bool showHotelImage, ref string errorMsg)
        {
            Int16 flightSeat = 10;
            if (UserData.TvParams.TvParamFlight.DispMinFlightAllotW.HasValue)
                flightSeat = UserData.TvParams.TvParamFlight.DispMinFlightAllotW.Value > 0 ? UserData.TvParams.TvParamFlight.DispMinFlightAllotW.Value : (short)10;

            List<HotelMarOptRecord> hotelInfoUrlList = getHotelMarOptList(UserData, priceData, ref errorMsg);
            HotelImageUrl hotelImg = null;
            if (showHotelImage) {
                string hotelImagesUrl = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "HotelImagesUrl"));
                List<HotelImageUrl> hotelImgUrl = string.IsNullOrEmpty(hotelImagesUrl) ? new List<HotelImageUrl>() : Newtonsoft.Json.JsonConvert.DeserializeObject<List<HotelImageUrl>>(hotelImagesUrl);
                hotelImg = hotelImgUrl.Find(f => f.Market == UserData.Market);
            }
            List<PPHotelGroup> result = new List<PPHotelGroup>();
            var query = from q in priceData
                        group q by new { q.CheckIn, q.Night, q.Hotel } into k
                        select new { k.Key.CheckIn, k.Key.Night, k.Key.Hotel };
            int grpCount = 0;
            foreach (var row in query) {
                PPHotelGroup hGrp = new PPHotelGroup();
                List<PackPriceSearchResult> groups = priceData.Where(w => w.CheckIn == row.CheckIn && w.Night == row.Night && w.Hotel == row.Hotel).ToList<PackPriceSearchResult>();
                string imageUrl = hotelImg != null && !string.IsNullOrEmpty(hotelImg.Url) ? hotelImg + "/" + groups.FirstOrDefault().HotelID.ToString() + "/0_T.jpg" : string.Empty;

                HotelMarOptRecord hotelUrl = hotelInfoUrlList.Find(f => f.Hotel == row.Hotel);
                string _hotelUrl = (hotelUrl != null && !string.IsNullOrEmpty(hotelUrl.InfoWeb)) ? hotelUrl.InfoWeb : string.Empty;

                int grpCnt = (from q in groups
                              group q by new { q.SearchNr } into k
                              select new { RoomNr = k.Key.SearchNr }).Count();
                if (grpCnt > 0 && grpCnt == filter.RoomCount) {
                    PackPriceSearchResult first = groups.FirstOrDefault();

                    grpCount++;
                    hGrp.HotelGroupID = grpCount;
                    hGrp.CheckIn = row.CheckIn.Value.ToShortDateString();
                    hGrp.CheckOut = row.CheckIn.Value.AddDays(row.Night.Value).ToShortDateString();
                    hGrp.Night = row.Night.Value.ToString();
                    hGrp.HotelName = useLocalName ? first.HotelNameL : first.HotelName;
                    hGrp.HotelLocationName = useLocalName ? first.HotLocationNameL : first.HotLocationName;
                    hGrp.showHotelImage = showHotelImage;
                    hGrp.hotelImageUrl = showHotelImage && !string.IsNullOrEmpty(imageUrl) ? imageUrl : string.Empty;
                    hGrp.hotelInfoUrl = _hotelUrl;
                    hGrp.Parameter1 = grpCount % 2 != 0;
                    List<PPPriceDetail> pdetail = new List<PPPriceDetail>();
                    var q1 = from q in groups
                             group q by new { q.SearchNr } into k
                             select new { RoomNr = k.Key.SearchNr };
                    decimal selectPrice = (decimal)0;
                    foreach (var r1 in q1) {
                        #region Room Groups
                        SearchCriteriaRooms room = filter.RoomsInfo.Find(f => f.RoomNr == r1.RoomNr);
                        PPPriceDetail pd = new PPPriceDetail();
                        List<PPAccomPrice> ap = new List<PPAccomPrice>();
                        List<PackPriceSearchResult> apList = groups.Where(w => w.SearchNr == r1.RoomNr.Value).ToList<PackPriceSearchResult>();
                        pd.labelDepFlight = string.Format("<img title=\"{0}\" src=\"Images/depFlight.gif\" width=\"16\" height=\"16\" />",
                                                          HttpContext.GetGlobalResourceObject("PackageSearchResult", "DepartureFlightAvaillable"));
                        pd.labelRetFlight = string.Format("<img title=\"{0}\" src=\"Images/arrFlight.gif\" width=\"16\" height=\"16\" />",
                                                          HttpContext.GetGlobalResourceObject("PackageSearchResult", "ReturnFlightAvaillable"));
                        pd.labelAccomName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom").ToString();
                        pd.labelBoardName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblBoardName").ToString();
                        pd.labelFreeRoom = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFreeRoom").ToString();
                        pd.labelPrice = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblPrice").ToString();
                        pd.labelRoomName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoomName").ToString();
                        pd.labelSelected = string.Empty;
                        pd.labelOffer = string.Empty;
                        pd.PriceDetailID = grpCount;
                        pd.AccomInfo += apList.FirstOrDefault().AccomFullName;
                        int pOE = 0;
                        foreach (PackPriceSearchResult accomP in apList) {

                            pOE++;
                            Int16 StopSaleValue = 0;
                            string stopSaleMessage = string.Empty;
                            PPAccomPrice p = new PPAccomPrice();
                            p.RefNo = accomP.PriceID.HasValue ? accomP.PriceID.Value : 0;
                            if (accomP.DepSeat.HasValue && accomP.DepSeat.Value >= 0)
                                p.DepFlightSeat = accomP.DepSeat.Value > flightSeat ? flightSeat.ToString() + "+" : accomP.DepSeat.Value.ToString();
                            else p.DepFlightSeat = "0";
                            if (accomP.RetSeat.HasValue && accomP.RetSeat.Value >= 0)
                                p.RetFlightSeat = accomP.RetSeat.Value > flightSeat ? flightSeat.ToString() + "+" : accomP.RetSeat.Value.ToString();
                            else p.RetFlightSeat = "0";
                            p.DepFlight = accomP.DepFlight;
                            p.RetFlight = accomP.RetFlight;
                            p.RoomNr = accomP.SearchNr.HasValue ? accomP.SearchNr.Value : 1;
                            p.AccomName = useLocalName ? accomP.AccomNameL : accomP.AccomName;
                            p.BoardName = useLocalName ? accomP.BoardNameL : accomP.BoardName;
                            p.FreeRoom = accomP.HotelAllot.HasValue ? (accomP.HotelAllot.Value > 3 ? "3+" : (accomP.HotelAllot.Value < 0 ? "0" : accomP.HotelAllot.Value.ToString())) : "";
                            p.PriceID = accomP.PriceID.HasValue ? accomP.PriceID.Value : -1;
                            p.RoomName = useLocalName ? accomP.RoomNameL : accomP.RoomName;
                            p.SaleCur = accomP.SaleCur;
                            p.SalePrice = accomP.SalePrice.HasValue ? accomP.SalePrice.Value.ToString("#.00") : "";
                            p.PriceNoComma = accomP.SalePrice.HasValue ? (accomP.SalePrice.Value * 100).ToString("#.") : "";
                            p.Price = accomP.SalePrice;
                            if ((accomP.StopSaleGuar == 2 && accomP.StopSaleStd == 2)) {
                                StopSaleValue = 2;
                            } else
                                if (accomP.StopSaleGuar == 1 && accomP.StopSaleStd == 1) {
                                string msg = new Reservation().getStopSaleMessage(UserData.Market, accomP.Hotel, accomP.Room, accomP.Accom, accomP.Board, null, UserData.AgencyID, accomP.CheckIn, accomP.Night, true, ref errorMsg);
                                stopSaleMessage = string.Format(HttpContext.GetGlobalResourceObject("PackageSearchResult", "StopSaleWarningMessage").ToString(),
                                                                    string.IsNullOrEmpty(msg) ? "" : msg + ", ");
                                StopSaleValue = 1;
                            } else
                                p.StopSale = StopSaleValue.ToString();
                            p.StopSaleMessage = stopSaleMessage;
                            p.Discount = false;// accomP.SalePrice < accomP.PriceContractPrice;
                            p.priceOddEven = pOE % 2 != 0;
                            ap.Add(p);
                        }
                        List<PPAccomPrice> pList = ap.OrderBy(o => o.Price).ToList<PPAccomPrice>();
                        pList[0].Selected = true;
                        pd.accomPrice = pList;
                        pdetail.Add(pd);
                        #endregion Room Groups
                    }
                    bool pDetailPriceValid = pdetail.Where(w => (w.accomPrice.Where(w1 => w1.Selected == true && w1.Price.HasValue).Count() > 0)).Count() > 0;
                    selectPrice += pDetailPriceValid ? pdetail.Sum(s => s.accomPrice.Where(w => w.Selected == true && w.Price.HasValue).First().Price.Value) : 0;
                    hGrp.MinPrice = selectPrice;
                    hGrp.SelectedPrice = selectPrice.ToString("#,###.00");
                    hGrp.priceDetail = pdetail;
                    hGrp.SelectedPriceCur = hGrp.priceDetail.First().accomPrice.First().SaleCur;
                    hGrp.SortingID = -1;
                    result.Add(hGrp);
                }
            }
            return result;
        }
    }
}
