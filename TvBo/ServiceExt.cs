﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;

namespace TvBo
{
    public class ServiceExt
    {
        public CategoryExtSer getCategoryExtSer(User UserData, int? recID, ref string errorMsg)
        {
            string tsql = @"Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name) 
                            From CategoryExtSer (NOLOCK)
                            Where RecID=@recID
                            ";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "recID", DbType.Int32, recID);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        return new CategoryExtSer
                        {
                            RecID = Conversion.getInt32OrNull(rdr["RecID"]),
                            Name = Conversion.getStrOrNull(rdr["Name"]),
                            NameL = Conversion.getStrOrNull(rdr["NameL"])
                        };
                    }
                    else return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CategoryExtSer> getCategoryExtSerList(User UserData, ref string errorMsg)
        {
            List<CategoryExtSer> records = new List<CategoryExtSer>();
            string tsql = @"Select RecID,Name,NameL=isnull(dbo.FindLocalName(NameLID, @Market),Name) 
                            From CategoryExtSer (NOLOCK)                            
                            ";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {                
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    while (rdr.Read())
                    {
                        CategoryExtSer record = new CategoryExtSer();
                        record.RecID = Conversion.getInt32OrNull(rdr["RecID"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.NameL = Conversion.getStrOrNull(rdr["NameL"]);
                        records.Add(record);
                    }
                    return records;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
