﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Collections.Generic;
using TvTools;

namespace TvBo
{
    public class Agency
    {
        public List<AgencyPasPaymentCategory> AgencyPasPaymentCategory(User UserData, ref string errorMsg)
        {
            string tsql = string.Empty;
            List<AgencyPasPaymentCategory> records = new List<AgencyPasPaymentCategory>();
            tsql = @"   Select P.PayCat PayCatID, isnull(P.OptTime, isnull(S.PayOptTime, 0)) AS OptTime 
                        From AgencyPasPayCat P (NOLOCK)
				        Join Parampay S on Market=@Market
                        Where Agency=@Agency
                        Order By P.PayCat ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "Market", DbType.String, UserData.Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(
                            new AgencyPasPaymentCategory
                            {
                                PayCatID = Conversion.getInt16OrNull(R["PayCatID"]),
                                OptTime = Conversion.getInt16OrNull(R["OptTime"])
                            });
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool getRole(User UserData, Int16 RoleID)
        {
            bool retval = true;
            if (UserData.AgencyRec.UseRoles.HasValue && UserData.AgencyRec.UseRoles.Value && UserData.AgencyRec.Roles != null)
            {
                AgencyRoles role = UserData.AgencyRec.Roles.Find(f => f.RoleID == RoleID);
                if (role != null)
                    retval = role.Authority;
            }
            return retval;
        }

        public bool getAgencyPasPaymentDefinition(User UserData, ref string errorMsg)
        {
            try
            {
                List<AgencyPasPaymentCategory> list = new List<AgencyPasPaymentCategory>();
                list = AgencyPasPaymentCategory(UserData, ref errorMsg);
                if (list != null)
                {
                    if (list.Count > 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public int getAgencyOpTime(User UserData, ref string errorMsg)
        {
            string tsql = @"Select PayOptTime 
                            From Agency (NOLOCK) 
                            Where Code = @Code";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, UserData.AgencyID);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                        return (int)oReader["PayOpyTime"];
                    else
                        return -1;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return -1;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public AgencyRecord getAgency(string AgencyCode, ref string errorMsg)
        {
            return getAgency(AgencyCode, false, ref errorMsg);
        }

        public AgencyRecord getAgency(string AgencyCode, bool MainAgency, ref string errorMsg)
        {
            string tvVersion = Common.getTvVersion(ref errorMsg);
            string dbOwner = Common.getDBOwner();
            AgencyRecord rec = new AgencyRecord();
            string tsql = string.Empty;
            if (MainAgency)
                tsql += 
@"
Declare @AgencyCode VarChar(10)
Select @AgencyCode=(Case When isnull(MainOffice, '')<>'' then MainOffice else Code end) From Agency (NOLOCK) Where Code=@Code
";
            else
                tsql += 
@"
Declare @AgencyCode VarChar(10)
Set @AgencyCode=@Code 
";
            if (Convert.ToDecimal(tvVersion) > Convert.ToDecimal("050018155"))
                tsql +=
                #region version > 5.0.18.155
@"
Select A.RecID,
    A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, O.Market), A.Name),
    A.NameS, A.FirmName, A.BossName, A.ContName, 
    A.OprOffice, OprOfficeName=O.Name, OprOfficeNameL=isnull(dbo.FindLocalName(O.NameLID,O.Market), O.Name),
    A.MainOffice, A.WorkType, A.VocAddrType, A.InvoiceTo, A.PaymentFrom,
    A.Location, LocationName=(Select Name From Location (NOLOCK) Where RecID=A.Location),
    LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Location (NOLOCK) Where RecID=A.Location),
    A.Address, A.AddrZip, A.AddrCity, A.AddrCountry, A.Phone1, A.Phone2, A.MobPhone, A.Fax1, A.Fax2, A.www, A.email1, A.email2, 
    A.InvAddress, A.InvAddrZip, A.InvAddrCity, A.InvAddrCountry, 
    A.DocAddress, A.DocAddrZip, A.DocAddrCity, A.DocAddrCountry, A.DocPhone, A.DocFax,
    A.VocAddr_Address, A.VocAddr_Zip, A.VocAddr_City, A.VocAddr_Country, A.VocAddr_Phone1, A.VocAddr_Phone2, A.VocAddr_Fax1, A.VocAddr_Fax2, A.VocAddr_EMail1, A.VocAddr_EMail2,	
    A.BlackList, A.AllowMakeRes, A.AgreeCheck, A.OwnAgency, 	
    A.Bank1, A.Bank1BankNo, A.Bank1AccNo, A.Bank1IBAN, A.Bank1Curr, A.Bank1Location,
    A.Bank2, A.Bank2BankNo, A.Bank2AccNo, A.Bank2IBAN, A.Bank2Curr, A.Bank2Location,	
    A.ACContacName, A.CreditLimit, A.UseSysPay, A.Curr, A.PayMode, A.AfBef, A.PayModeDay, A.PayDay, A.PayType,
    A.Quota, A.TAcCode, A.UseOffMarket, A.AgencyType, A.Parent, A.DocPrtNoPay, A.UseMainCont, A.UseMainAcc, A.UseMainPay,
    A.SupComType, A.UseBonus, A.AccountCode, A.RegisterCode, A.SupplierCode,	
    AceExport=isnull(A.AceExport, 'N'), AceSendTo=isnull(A.AceSendTo, 0),
    A.AcePath, A.AceEMail, A.FtpHost, A.FtpDir, A.FtpUser, A.FtpPas, A.AceWSAddress, A.AceWSUserID, A.AceWSPass, 
    A.TaxOffice, A.TaxAccNo, A.EarnBonus, A.UserEarnBonus, A.ResPayRule, A.PayOptTime, A.UseSysParamCredit, A.CreditMode, 
    A.LicenseNo, A.CIF, A.CapSocial, A.CapSocialCur, A.LogoUrl, A.WebDisp, A.CuratorUser, A.MerlinxID, A.Aggregator, A.MaxPaxCnt, A.MaxRoomCnt,
    A.Pxm_Use,A.Pxm_UseLimit,A.CanPayment,A.Pxm_DoNotShowOwnCntry,A.Pxm_DoNotShowOwnCntry_UseMarOpt,
    A.IATA,A.MOHCode,A.VisaReq,A.SaleUmrah,A.SaleHajj,A.SaleIndRes,A.LockSale,A.LockFinance
From Agency A (NOLOCK)
Join Office O (NOLOCK) ON O.Code = A.OprOffice
Where A.Status='Y' And A.Code=@AgencyCode                            
";
                #endregion
            else
                if (VersionControl.getTableField("Agency", "Pxm_DoNotShowOwnCntry"))
                    tsql +=
                    #region Table : Agency, Field : Pxm_DoNotShowOwnCntry
 @"
Select A.RecID,
    A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, O.Market), A.Name),
    A.NameS, A.FirmName, A.BossName, A.ContName, 
    A.OprOffice, OprOfficeName=O.Name, OprOfficeNameL=isnull(dbo.FindLocalName(O.NameLID,O.Market), O.Name),
    A.MainOffice, A.WorkType, A.VocAddrType, A.InvoiceTo, A.PaymentFrom,
    A.Location, LocationName=(Select Name From Location (NOLOCK) Where RecID=A.Location),
    LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Location (NOLOCK) Where RecID=A.Location),
    A.Address, A.AddrZip, A.AddrCity, A.AddrCountry, A.Phone1, A.Phone2, A.MobPhone, A.Fax1, A.Fax2, A.www, A.email1, A.email2, 
    A.InvAddress, A.InvAddrZip, A.InvAddrCity, A.InvAddrCountry, 
    A.DocAddress, A.DocAddrZip, A.DocAddrCity, A.DocAddrCountry, A.DocPhone, A.DocFax,
    A.VocAddr_Address, A.VocAddr_Zip, A.VocAddr_City, A.VocAddr_Country, A.VocAddr_Phone1, A.VocAddr_Phone2, A.VocAddr_Fax1, A.VocAddr_Fax2, A.VocAddr_EMail1, A.VocAddr_EMail2,	
    A.BlackList, A.AllowMakeRes, A.AgreeCheck, A.OwnAgency, 	
    A.Bank1, A.Bank1BankNo, A.Bank1AccNo, A.Bank1IBAN, A.Bank1Curr, A.Bank1Location,
    A.Bank2, A.Bank2BankNo, A.Bank2AccNo, A.Bank2IBAN, A.Bank2Curr, A.Bank2Location,	
    A.ACContacName, A.CreditLimit, A.UseSysPay, A.Curr, A.PayMode, A.AfBef, A.PayModeDay, A.PayDay, A.PayType,
    A.Quota, A.TAcCode, A.UseOffMarket, A.AgencyType, A.Parent, A.DocPrtNoPay, A.UseMainCont, A.UseMainAcc, A.UseMainPay,
    A.SupComType, A.UseBonus, A.AccountCode, A.RegisterCode, A.SupplierCode,	
    AceExport=isnull(A.AceExport, 'N'), AceSendTo=isnull(A.AceSendTo, 0),
    A.AcePath, A.AceEMail, A.FtpHost, A.FtpDir, A.FtpUser, A.FtpPas, A.AceWSAddress, A.AceWSUserID, A.AceWSPass, 
    A.TaxOffice, A.TaxAccNo, A.EarnBonus, A.UserEarnBonus, A.ResPayRule, A.PayOptTime, A.UseSysParamCredit, A.CreditMode, 
    A.LicenseNo, A.CIF, A.CapSocial, A.CapSocialCur, A.LogoUrl, A.WebDisp, A.CuratorUser, A.MerlinxID, A.Aggregator, A.MaxPaxCnt, A.MaxRoomCnt,
    A.Pxm_Use,A.Pxm_UseLimit,A.CanPayment,A.Pxm_DoNotShowOwnCntry,A.Pxm_DoNotShowOwnCntry_UseMarOpt
From Agency A (NOLOCK)
Join Office O (NOLOCK) ON O.Code = A.OprOffice
Where A.Status='Y' And A.Code=@AgencyCode                            
";
                    #endregion
                else
                    if (Convert.ToDecimal(tvVersion) > Convert.ToDecimal("040071120"))
                        tsql +=
                        #region version > 4.0.71.119
 @"
Select A.RecID,
    A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, O.Market), A.Name),
    A.NameS, A.FirmName, A.BossName, A.ContName, 
    A.OprOffice, OprOfficeName=O.Name, OprOfficeNameL=isnull(dbo.FindLocalName(O.NameLID,O.Market), O.Name),
    A.MainOffice, A.WorkType, A.VocAddrType, A.InvoiceTo, A.PaymentFrom,
    A.Location, LocationName=(Select Name From Location (NOLOCK) Where RecID=A.Location),
    LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Location (NOLOCK) Where RecID=A.Location),
    A.Address, A.AddrZip, A.AddrCity, A.AddrCountry, A.Phone1, A.Phone2, A.MobPhone, A.Fax1, A.Fax2, A.www, A.email1, A.email2, 
    A.InvAddress, A.InvAddrZip, A.InvAddrCity, A.InvAddrCountry, 
    A.DocAddress, A.DocAddrZip, A.DocAddrCity, A.DocAddrCountry, A.DocPhone, A.DocFax,
    A.VocAddr_Address, A.VocAddr_Zip, A.VocAddr_City, A.VocAddr_Country, A.VocAddr_Phone1, A.VocAddr_Phone2, A.VocAddr_Fax1, A.VocAddr_Fax2, A.VocAddr_EMail1, A.VocAddr_EMail2,	
    A.BlackList, A.AllowMakeRes, A.AgreeCheck, A.OwnAgency, 	
    A.Bank1, A.Bank1BankNo, A.Bank1AccNo, A.Bank1IBAN, A.Bank1Curr, A.Bank1Location,
    A.Bank2, A.Bank2BankNo, A.Bank2AccNo, A.Bank2IBAN, A.Bank2Curr, A.Bank2Location,	
    A.ACContacName, A.CreditLimit, A.UseSysPay, A.Curr, A.PayMode, A.AfBef, A.PayModeDay, A.PayDay, A.PayType,
    A.Quota, A.TAcCode, A.UseOffMarket, A.AgencyType, A.Parent, A.DocPrtNoPay, A.UseMainCont, A.UseMainAcc, A.UseMainPay,
    A.SupComType, A.UseBonus, A.AccountCode, A.RegisterCode, A.SupplierCode,	
    AceExport=isnull(A.AceExport, 'N'), AceSendTo=isnull(A.AceSendTo, 0),
    A.AcePath, A.AceEMail, A.FtpHost, A.FtpDir, A.FtpUser, A.FtpPas, A.AceWSAddress, A.AceWSUserID, A.AceWSPass, 
    A.TaxOffice, A.TaxAccNo, A.EarnBonus, A.UserEarnBonus, A.ResPayRule, A.PayOptTime, A.UseSysParamCredit, A.CreditMode, 
    A.LicenseNo, A.CIF, A.CapSocial, A.CapSocialCur, A.LogoUrl, A.WebDisp, A.CuratorUser, A.MerlinxID, A.Aggregator, A.MaxPaxCnt, A.MaxRoomCnt,
    A.Pxm_Use,A.Pxm_UseLimit,A.CanPayment,Pxm_DoNotShowOwnCntry=Cast(1 As Bit),Pxm_DoNotShowOwnCntry_UseMarOpt=Cast(1 As Bit)
From Agency A (NOLOCK)
Join Office O (NOLOCK) ON O.Code = A.OprOffice
Where A.Status='Y' And A.Code=@AgencyCode                            
";
                        #endregion
                    else
                        if (Convert.ToDecimal(tvVersion) > Convert.ToDecimal("040070118") || string.Equals(dbOwner, Common.crID_Paximum))
                            tsql +=
                            #region version > 4.0.70.117
 @"
Select A.RecID,
    A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, O.Market), A.Name),
    A.NameS, A.FirmName, A.BossName, A.ContName, 
    A.OprOffice, OprOfficeName=O.Name, OprOfficeNameL=isnull(dbo.FindLocalName(O.NameLID,O.Market), O.Name),
    A.MainOffice, A.WorkType, A.VocAddrType, A.InvoiceTo, A.PaymentFrom,
    A.Location, LocationName=(Select Name From Location (NOLOCK) Where RecID=A.Location),
    LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Location (NOLOCK) Where RecID=A.Location),
    A.Address, A.AddrZip, A.AddrCity, A.AddrCountry, A.Phone1, A.Phone2, A.MobPhone, A.Fax1, A.Fax2, A.www, A.email1, A.email2, 
    A.InvAddress, A.InvAddrZip, A.InvAddrCity, A.InvAddrCountry, 
    A.DocAddress, A.DocAddrZip, A.DocAddrCity, A.DocAddrCountry, A.DocPhone, A.DocFax,
    A.VocAddr_Address, A.VocAddr_Zip, A.VocAddr_City, A.VocAddr_Country, A.VocAddr_Phone1, A.VocAddr_Phone2, A.VocAddr_Fax1, A.VocAddr_Fax2, A.VocAddr_EMail1, A.VocAddr_EMail2,	
    A.BlackList, A.AllowMakeRes, A.AgreeCheck, A.OwnAgency, 	
    A.Bank1, A.Bank1BankNo, A.Bank1AccNo, A.Bank1IBAN, A.Bank1Curr, A.Bank1Location,
    A.Bank2, A.Bank2BankNo, A.Bank2AccNo, A.Bank2IBAN, A.Bank2Curr, A.Bank2Location,	
    A.ACContacName, A.CreditLimit, A.UseSysPay, A.Curr, A.PayMode, A.AfBef, A.PayModeDay, A.PayDay, A.PayType,
    A.Quota, A.TAcCode, A.UseOffMarket, A.AgencyType, A.Parent, A.DocPrtNoPay, A.UseMainCont, A.UseMainAcc, A.UseMainPay,
    A.SupComType, A.UseBonus, A.AccountCode, A.RegisterCode, A.SupplierCode,	
    AceExport=isnull(A.AceExport, 'N'), AceSendTo=isnull(A.AceSendTo, 0),
    A.AcePath, A.AceEMail, A.FtpHost, A.FtpDir, A.FtpUser, A.FtpPas, A.AceWSAddress, A.AceWSUserID, A.AceWSPass, 
    A.TaxOffice, A.TaxAccNo, A.EarnBonus, A.UserEarnBonus, A.ResPayRule, A.PayOptTime, A.UseSysParamCredit, A.CreditMode, 
    A.LicenseNo, A.CIF, A.CapSocial, A.CapSocialCur, A.LogoUrl, A.WebDisp, A.CuratorUser, A.MerlinxID, A.Aggregator, A.MaxPaxCnt, A.MaxRoomCnt,
    A.Pxm_Use,A.Pxm_UseLimit,Pxm_DoNotShowOwnCntry=Cast(1 As Bit),Pxm_DoNotShowOwnCntry_UseMarOpt=Cast(1 As Bit)
From Agency A (NOLOCK)
Join Office O (NOLOCK) ON O.Code = A.OprOffice
Where A.Status='Y' And A.Code=@AgencyCode                            
";
                            #endregion
                        else
                            if (Convert.ToDecimal(tvVersion) > Convert.ToDecimal("030401006"))
                                tsql +=
                                #region version > 3.4.1.5
 @"
Select A.RecID,
    A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, O.Market), A.Name),
    A.NameS, A.FirmName, A.BossName, A.ContName, 
    A.OprOffice, OprOfficeName=O.Name, OprOfficeNameL=isnull(dbo.FindLocalName(O.NameLID,O.Market), O.Name),
    A.MainOffice, A.WorkType, A.VocAddrType, A.InvoiceTo, A.PaymentFrom,
    A.Location, LocationName=(Select Name From Location (NOLOCK) Where RecID=A.Location),
    LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Location (NOLOCK) Where RecID=A.Location),
    A.Address, A.AddrZip, A.AddrCity, A.AddrCountry, A.Phone1, A.Phone2, A.MobPhone, A.Fax1, A.Fax2, A.www, A.email1, A.email2, 
    A.InvAddress, A.InvAddrZip, A.InvAddrCity, A.InvAddrCountry, 
    A.DocAddress, A.DocAddrZip, A.DocAddrCity, A.DocAddrCountry, A.DocPhone, A.DocFax,
    A.VocAddr_Address, A.VocAddr_Zip, A.VocAddr_City, A.VocAddr_Country, A.VocAddr_Phone1, A.VocAddr_Phone2, A.VocAddr_Fax1, A.VocAddr_Fax2, A.VocAddr_EMail1, A.VocAddr_EMail2,	
    A.BlackList, A.AllowMakeRes, A.AgreeCheck, A.OwnAgency, 	
    A.Bank1, A.Bank1BankNo, A.Bank1AccNo, A.Bank1IBAN, A.Bank1Curr, A.Bank1Location,
    A.Bank2, A.Bank2BankNo, A.Bank2AccNo, A.Bank2IBAN, A.Bank2Curr, A.Bank2Location,	
    A.ACContacName, A.CreditLimit, A.UseSysPay, A.Curr, A.PayMode, A.AfBef, A.PayModeDay, A.PayDay, A.PayType,
    A.Quota, A.TAcCode, A.UseOffMarket, A.AgencyType, A.Parent, A.DocPrtNoPay, A.UseMainCont, A.UseMainAcc, A.UseMainPay,
    A.SupComType, A.UseBonus, A.AccountCode, A.RegisterCode, A.SupplierCode,	
    AceExport=isnull(A.AceExport, 'N'), AceSendTo=isnull(A.AceSendTo, 0),
    A.AcePath, A.AceEMail, A.FtpHost, A.FtpDir, A.FtpUser, A.FtpPas, A.AceWSAddress, A.AceWSUserID, A.AceWSPass, 
    A.TaxOffice, A.TaxAccNo, A.EarnBonus, A.UserEarnBonus, A.ResPayRule, A.PayOptTime, A.UseSysParamCredit, A.CreditMode, 
    A.LicenseNo, A.CIF, A.CapSocial, A.CapSocialCur, A.LogoUrl, A.WebDisp, A.CuratorUser, A.MerlinxID, A.Aggregator, A.MaxPaxCnt, A.MaxRoomCnt,
    Pxm_DoNotShowOwnCntry=Cast(1 As Bit),Pxm_DoNotShowOwnCntry_UseMarOpt=Cast(1 As Bit)
From Agency A (NOLOCK)
Join Office O (NOLOCK) ON O.Code = A.OprOffice
Where A.Status='Y' And A.Code=@AgencyCode                            
";
                                #endregion
                            else
                                tsql +=
                                #region First query
 @"
Select A.RecID,
    A.Code, A.Name, LocalName=isnull(dbo.FindLocalName(A.NameLID, O.Market), A.Name),
    A.NameS, A.FirmName, A.BossName, A.ContName, 
    A.OprOffice, OprOfficeName=O.Name, OprOfficeNameL=isnull(dbo.FindLocalName(O.NameLID,O.Market), O.Name),
    A.MainOffice, A.WorkType, A.VocAddrType, A.InvoiceTo, A.PaymentFrom,
    A.Location, LocationName=(Select Name From Location (NOLOCK) Where RecID=A.Location),
    LocationLocalName=(Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Location (NOLOCK) Where RecID=A.Location),
    A.Address, A.AddrZip, A.AddrCity, A.AddrCountry, A.Phone1, A.Phone2, A.MobPhone, A.Fax1, A.Fax2, A.www, A.email1, A.email2, 
    A.InvAddress, A.InvAddrZip, A.InvAddrCity, A.InvAddrCountry, 
    A.DocAddress, A.DocAddrZip, A.DocAddrCity, A.DocAddrCountry, A.DocPhone, A.DocFax,
    A.VocAddr_Address, A.VocAddr_Zip, A.VocAddr_City, A.VocAddr_Country, A.VocAddr_Phone1, A.VocAddr_Phone2, A.VocAddr_Fax1, A.VocAddr_Fax2, A.VocAddr_EMail1, A.VocAddr_EMail2,	
    A.BlackList, A.AllowMakeRes, A.AgreeCheck, A.OwnAgency, 	
    A.Bank1, A.Bank1BankNo, A.Bank1AccNo, A.Bank1IBAN, A.Bank1Curr, A.Bank1Location,
    A.Bank2, A.Bank2BankNo, A.Bank2AccNo, A.Bank2IBAN, A.Bank2Curr, A.Bank2Location,	
    A.ACContacName, A.CreditLimit, A.UseSysPay, A.Curr, A.PayMode, A.AfBef, A.PayModeDay, A.PayDay, A.PayType,
    A.Quota, A.TAcCode, A.UseOffMarket, A.AgencyType, A.Parent, A.DocPrtNoPay, A.UseMainCont, A.UseMainAcc, A.UseMainPay,
    A.SupComType, A.UseBonus, A.AccountCode, A.RegisterCode, A.SupplierCode,	
    AceExport=isnull(A.AceExport, 'N'), AceSendTo=isnull(A.AceSendTo, 0),
    A.AcePath, A.AceEMail, A.FtpHost, A.FtpDir, A.FtpUser, A.FtpPas, A.AceWSAddress, A.AceWSUserID, A.AceWSPass, 
    A.TaxOffice, A.TaxAccNo, A.EarnBonus, A.UserEarnBonus, A.ResPayRule, A.PayOptTime, A.UseSysParamCredit, A.CreditMode, 
    A.LicenseNo, A.CIF, A.CapSocial, A.CapSocialCur, A.LogoUrl, A.WebDisp, A.CuratorUser, A.MerlinxID, A.Aggregator, MaxPaxCnt=null, MaxRoomCnt=null,
    Pxm_DoNotShowOwnCntry=Cast(1 As Bit),Pxm_DoNotShowOwnCntry_UseMarOpt=Cast(1 As Bit)
From Agency A (NOLOCK)
Join Office O (NOLOCK) ON O.Code = A.OprOffice
Where A.Status='Y' And A.Code=@AgencyCode                            
";
                                #endregion

            bool? useB2BRoles = TvTools.Conversion.getBoolOrNull(System.Configuration.ConfigurationManager.AppSettings["UseB2BRole"]);
            bool UseB2BRole = useB2BRoles.HasValue ? useB2BRoles.Value : false;
            List<dbInformationShema> b2bRolesTable = VersionControl.getTableShema("B2BRoles", ref errorMsg);
            UseB2BRole = UseB2BRole && b2bRolesTable != null && b2bRolesTable.Count > 0;
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, AgencyCode);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        rec.RecID = (int)R["RecID"];
                        rec.Code = Conversion.getStrOrNull(R["Code"]);
                        rec.Name = Conversion.getStrOrNull(R["Name"]);
                        rec.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        rec.NameS = Conversion.getStrOrNull(R["NameS"]);
                        rec.FirmName = Conversion.getStrOrNull(R["FirmName"]);
                        rec.BossName = Conversion.getStrOrNull(R["BossName"]);
                        rec.ContName = Conversion.getStrOrNull(R["ContName"]);
                        rec.OprOffice = Conversion.getStrOrNull(R["OprOffice"]);
                        rec.OprOfficeName = Conversion.getStrOrNull(R["OprOfficeName"]);
                        rec.OprOfficeNameL = Conversion.getStrOrNull(R["OprOfficeNameL"]);
                        rec.MainOffice = Conversion.getStrOrNull(R["MainOffice"]);
                        rec.WorkType = (byte)R["WorkType"];
                        rec.VocAddrType = (Int16)R["VocAddrType"];
                        rec.InvoiceTo = (byte)R["InvoiceTo"];
                        rec.PaymentFrom = (byte)R["PaymentFrom"];
                        rec.Location = Conversion.getInt32OrNull(R["Location"]);
                        rec.LocationName = Conversion.getStrOrNull(R["LocationName"]);
                        rec.LocationLocalName = Conversion.getStrOrNull(R["LocationLocalName"]);
                        rec.Address = Conversion.getStrOrNull(R["Address"]);
                        rec.AddrZip = Conversion.getStrOrNull(R["AddrZip"]);
                        rec.AddrCity = Conversion.getStrOrNull(R["AddrCity"]);
                        rec.AddrCountry = Conversion.getStrOrNull(R["AddrCountry"]);
                        rec.Phone1 = Conversion.getStrOrNull(R["Phone1"]);
                        rec.Phone2 = Conversion.getStrOrNull(R["Phone2"]);
                        rec.Fax1 = Conversion.getStrOrNull(R["Fax1"]);
                        rec.Fax2 = Conversion.getStrOrNull(R["Fax2"]);
                        rec.Www = Conversion.getStrOrNull(R["Www"]);
                        rec.Email1 = Conversion.getStrOrNull(R["Email1"]);
                        rec.Email2 = Conversion.getStrOrNull(R["Email2"]);
                        rec.InvAddress = Conversion.getStrOrNull(R["InvAddress"]);
                        rec.InvAddrZip = Conversion.getStrOrNull(R["InvAddrZip"]);
                        rec.InvAddrCity = Conversion.getStrOrNull(R["InvAddrCity"]);
                        rec.InvAddrCountry = Conversion.getStrOrNull(R["InvAddrCountry"]);
                        rec.DocAddress = Conversion.getStrOrNull(R["DocAddress"]);
                        rec.DocAddrZip = Conversion.getStrOrNull(R["DocAddrZip"]);
                        rec.DocAddrCity = Conversion.getStrOrNull(R["DocAddrCity"]);
                        rec.DocAddrCountry = Conversion.getStrOrNull(R["DocAddrCountry"]);
                        rec.DocPhone = Conversion.getStrOrNull(R["DocPhone"]);
                        rec.DocFax = Conversion.getStrOrNull(R["DocFax"]);
                        rec.VocAddr_Address = Conversion.getStrOrNull(R["VocAddr_Address"]);
                        rec.VocAddr_Zip = Conversion.getStrOrNull(R["VocAddr_Zip"]);
                        rec.VocAddr_City = Conversion.getStrOrNull(R["VocAddr_City"]);
                        rec.VocAddr_Country = Conversion.getStrOrNull(R["VocAddr_Country"]);
                        rec.VocAddr_Phone1 = Conversion.getStrOrNull(R["VocAddr_Phone1"]);
                        rec.VocAddr_Phone2 = Conversion.getStrOrNull(R["VocAddr_Phone2"]);
                        rec.VocAddr_Fax1 = Conversion.getStrOrNull(R["VocAddr_Fax1"]);
                        rec.VocAddr_Fax2 = Conversion.getStrOrNull(R["VocAddr_Fax2"]);
                        rec.VocAddr_EMail1 = Conversion.getStrOrNull(R["VocAddr_EMail1"]);
                        rec.VocAddr_EMail2 = Conversion.getStrOrNull(R["VocAddr_EMail2"]);
                        rec.BlackList = Equals(R["BlackList"], "Y");
                        rec.AllowMakeRes = Equals(R["AllowMakeRes"], "Y");
                        rec.AgreeCheck = (bool)R["AgreeCheck"];
                        rec.OwnAgency = (bool)R["OwnAgency"];
                        rec.Bank1 = Conversion.getStrOrNull(R["Bank1"]);
                        rec.Bank1BankNo = Conversion.getStrOrNull(R["Bank1BankNo"]);
                        rec.Bank1AccNo = Conversion.getStrOrNull(R["Bank1AccNo"]);
                        rec.Bank1IBAN = Conversion.getStrOrNull(R["Bank1IBAN"]);
                        rec.Bank1Curr = Conversion.getStrOrNull(R["Bank1Curr"]);
                        rec.Bank1Location = Conversion.getStrOrNull(R["Bank1Location"]);
                        rec.Bank2 = Conversion.getStrOrNull(R["Bank2"]);
                        rec.Bank2BankNo = Conversion.getStrOrNull(R["Bank2BankNo"]);
                        rec.Bank2AccNo = Conversion.getStrOrNull(R["Bank2AccNo"]);
                        rec.Bank2IBAN = Conversion.getStrOrNull(R["Bank2IBAN"]);
                        rec.Bank2Curr = Conversion.getStrOrNull(R["Bank2Curr"]);
                        rec.Bank2Location = Conversion.getStrOrNull(R["Bank2Location"]);
                        rec.ACContacName = Conversion.getStrOrNull(R["ACContacName"]);
                        rec.CreditLimit = Conversion.getDecimalOrNull(R["CreditLimit"]);
                        rec.UseSysPay = Equals(R["UseSysPay"], "Y");
                        rec.Curr = Conversion.getStrOrNull(R["Curr"]);
                        rec.PayMode = Conversion.getInt16OrNull(R["PayMode"]);
                        rec.AfBef = Conversion.getStrOrNull(R["AfBef"]);
                        rec.PayModeDay = Conversion.getInt16OrNull(R["PayModeDay"]);
                        rec.PayDay = Conversion.getInt16OrNull(R["PayDay"]);
                        rec.PayType = Conversion.getStrOrNull(R["PayType"]);
                        rec.Quota = Conversion.getInt32OrNull(R["Quota"]);
                        rec.TAcCode = Conversion.getStrOrNull(R["TAcCode"]);
                        rec.UseOffMarket = Equals(R["UseOffMarket"], "Y");
                        rec.AgencyType = (Int16)R["AgencyType"];
                        rec.DocPrtNoPay = Equals(R["DocPrtNoPay"], "Y");
                        rec.UseMainCont = Equals(R["UseMainCont"], "Y");
                        rec.UseMainAcc = Equals(R["UseMainAcc"], "Y");
                        rec.UseMainPay = Equals(R["UseMainPay"], "Y");
                        rec.SupComType = Conversion.getByteOrNull(R["SupComType"]);
                        rec.UseBonus = Equals(R["UseBonus"], "Y");
                        rec.AccountCode = Conversion.getStrOrNull(R["AccountCode"]);
                        rec.RegisterCode = Conversion.getStrOrNull(R["RegisterCode"]);
                        rec.SupplierCode = Conversion.getStrOrNull(R["SupplierCode"]);
                        rec.AceExport = Equals(R["AceExport"], "Y");
                        rec.AceSendTo = (Int16)R["AceSendTo"];
                        rec.AcePath = Conversion.getStrOrNull(R["AcePath"]);
                        rec.AceEMail = Conversion.getStrOrNull(R["AceEMail"]);
                        rec.FtpHost = Conversion.getStrOrNull(R["FtpHost"]);
                        rec.FtpDir = Conversion.getStrOrNull(R["FtpDir"]);
                        rec.FtpUser = Conversion.getStrOrNull(R["FtpUser"]);
                        rec.FtpPas = Conversion.getStrOrNull(R["FtpPas"]);
                        rec.AceWSAddress = Conversion.getStrOrNull(R["AceWSAddress"]);
                        rec.AceWSUserID = Conversion.getStrOrNull(R["AceWSUserID"]);
                        rec.AceWSPass = Conversion.getStrOrNull(R["AceWSPass"]);
                        rec.TaxOffice = Conversion.getStrOrNull(R["TaxOffice"]);
                        rec.TaxAccNo = Conversion.getStrOrNull(R["TaxAccNo"]);
                        rec.EarnBonus = (bool)R["EarnBonus"];
                        rec.UserEarnBonus = (bool)R["UserEarnBonus"];
                        rec.ResPayRule = (Int16)R["ResPayRule"];
                        rec.PayOptTime = Conversion.getInt16OrNull(R["PayOptTime"]);
                        rec.UseSysParamCredit = Equals(R["UseSysParamCredit"], "Y");
                        rec.CreditMode = Conversion.getInt16OrNull(R["CreditMode"]);
                        rec.LicenseNo = Conversion.getStrOrNull(R["LicenseNo"]);
                        rec.CIF = Conversion.getStrOrNull(R["CIF"]);
                        rec.CapSocial = Conversion.getDecimalOrNull(R["CapSocial"]);
                        rec.CapSocialCur = Conversion.getStrOrNull(R["CapSocialCur"]);
                        rec.LogoUrl = Conversion.getStrOrNull(R["LogoUrl"]);
                        rec.WebDisp = (bool)R["WebDisp"];
                        rec.CuratorUser = Conversion.getStrOrNull(R["CuratorUser"]);
                        rec.MerlinxID = Conversion.getStrOrNull(R["MerlinxID"]);
                        rec.Aggregator = (bool)R["Aggregator"];
                        rec.MaxPaxCnt = Conversion.getByteOrNull(R["MaxPaxCnt"]);
                        rec.MaxRoomCnt = Conversion.getByteOrNull(R["MaxRoomCnt"]);
                        if (Convert.ToDecimal(tvVersion) > Convert.ToDecimal("040070118") || string.Equals(dbOwner, Common.crID_Paximum))
                        {
                            rec.Pxm_Use = Conversion.getBoolOrNull(R["Pxm_Use"]);
                            rec.Pxm_UseLimit = Conversion.getBoolOrNull(R["Pxm_UseLimit"]);
                        }
                        if (Convert.ToDecimal(tvVersion) > Convert.ToDecimal("040071120"))
                            rec.CanPayment = Conversion.getBoolOrNull(R["CanPayment"]);
                        if (UseB2BRole)
                        {
                            rec.UseRoles = UseB2BRole;
                            rec.Roles = new Common().getAgencyRoles(rec.Code, ref errorMsg);
                        }
                        rec.Pxm_DoNotShowOwnCntry = Conversion.getBoolOrNull(R["Pxm_DoNotShowOwnCntry"]);
                        rec.Pxm_DoNotShowOwnCntry_UseMarOpt = Conversion.getBoolOrNull(R["Pxm_DoNotShowOwnCntry_UseMarOpt"]);
                        if (Convert.ToDecimal(tvVersion) > Convert.ToDecimal("050018155"))
                        {
                            rec.IATA = Conversion.getStrOrNull(R["IATA"]);
                            rec.MOHCode = Conversion.getStrOrNull(R["MOHCode"]);
                            rec.VisaReq = Conversion.getBoolOrNull(R["VisaReq"]);
                            rec.SaleUmrah = Conversion.getBoolOrNull(R["SaleUmrah"]);
                            rec.SaleHajj = Conversion.getBoolOrNull(R["SaleHajj"]);
                            rec.SaleIndRes = Conversion.getBoolOrNull(R["SaleIndRes"]);
                            rec.LockSale = Conversion.getBoolOrNull(R["LockSale"]);
                            rec.LockFinance = Conversion.getBoolOrNull(R["LockFinance"]);
                        }
                        return rec;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return rec;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getAgencyAgreement(User UserData, DateTime ResBegDate, ref string errorMsg)
        {
            string tsql = @"Select ContractNo From AgencyAgree (NOLOCK)
                            Where Status = 'Y'
                            And	Agency = @Agency 
                            And @ResBegDate between BegDate And EndDate";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, UserData.AgencyID);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.DateTime, ResBegDate);
                return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AgencyRecord> getAgencyAddress(string Agency, ref string errorMsg)
        {
            List<AgencyRecord> list = new List<AgencyRecord>();
            string sqlstr = string.Empty;
            string tmpAgency = string.Empty;
            sqlstr = @" Select UseMainCont, MainOffice From Agency (NOLOCK) Where Code = @Agency";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sqlstr);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        MainAgencyData m = new MainAgencyData();
                        m.UseMainCont = Conversion.getStrOrNull(R["UseMainCont"]);
                        m.MainOffice = Conversion.getStrOrNull(R["MainOffice"]);
                        if (m.UseMainCont == "Y" && m.MainOffice != "")
                        {
                            tmpAgency = m.MainOffice;
                        }
                        else
                        {
                            tmpAgency = Agency;
                        }
                        #region sqlstring
                        sqlstr = @" Select A.MainOffice, A.UseMainCont, A.VocAddrType, A.Code, A.Name, A.FirmName,	                            
                                DAddress = Case A.VocAddrType 
                                                When 0 Then (Select [Address] From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select [DocAddress] From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select  [DocAddress] From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else ([DocAddress]) 
		                                                    End)
                                                When 9 Then VocAddr_Address
                                            Else ''
                                            End,
                                Address = Case A.VocAddrType 
                                                When 0 Then (Select [Address] From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select [Address] From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select  [Address] From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else (A.Address) 
		                                                    End)
                                                When 9 Then VocAddr_Address
                                            Else ''
                                            End,
                                DAddrZip =  Case A.VocAddrType 
                                                When 0 Then (Select AddrZip From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select DocAddrZip From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select DocAddrZip From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else (DocAddrZip) 
		                                                    End)
                                                When 9 Then VocAddr_Zip
                                            Else ''
                                            End, 
                                AddrZip =  Case A.VocAddrType 
                                                When 0 Then (Select AddrZip From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select AddrZip From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select  AddrZip From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else (A.AddrZip) 
		                                                    End)
                                                When 9 Then VocAddr_Zip
                                            Else ''
                                            End, 
                                AddrCity = Case A.VocAddrType 
                                                    When 0 Then (Select AddrCity From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select AddrCity From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
				                                                    Then (Select AddrCity From Agency (NOLOCK) Where Code = A.MainOffice)
				                                                    Else (A.AddrCity) 
			                                                    End)
                                                    When 9 Then VocAddr_City
                                                Else ''
                                                End, 
                                DAddrCity = Case A.VocAddrType 
                                                    When 0 Then (Select AddrCity From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocAddrCity From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
				                                                    Then (Select DocAddrCity From Agency (NOLOCK) Where Code = A.MainOffice)
				                                                    Else (DocAddrCity) 
			                                                    End)
                                                    When 9 Then VocAddr_City
                                                Else ''
                                                End, 
                                AddrCountry = Case A.VocAddrType 
                                                    When 0 Then (Select AddrCountry From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select AddrCountry From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
				                                                    Then (Select AddrCountry From Agency (NOLOCK) Where Code = A.MainOffice)
				                                                    Else (A.AddrCountry) 
			                                                    End)
                                                    When 9 Then VocAddr_Country
                                                Else ''
                                                End,  
                                DAddrCountry = Case A.VocAddrType 
                                                    When 0 Then (Select AddrCountry From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocAddrCountry From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
				                                                    Then (Select DocAddrCountry From Agency (NOLOCK) Where Code = A.MainOffice)
				                                                    Else (DocAddrCountry) 
			                                                    End)
                                                    When 9 Then VocAddr_Country
                                                Else ''
                                                End, 
                                Phone1 =  Case A.VocAddrType 
                                                When 0 Then (Select Phone1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select Phone1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select Phone1 From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else (A.Phone1) 
		                                                    End)
                                                When 9 Then VocAddr_Phone1
                                            Else ''
                                            End, 
                                DPhone =  Case A.VocAddrType 
                                                When 0 Then (Select Phone1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select DocPhone From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select DocPhone From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else (DocPhone) 
		                                                    End)
                                                When 9 Then VocAddr_Phone1
                                            Else ''
                                            End, 
                                Fax1 =  Case A.VocAddrType 
                                                When 0 Then (Select Fax1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select Fax1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select Fax1 From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else (A.Fax1) 
		                                                    End)
                                                When 9 Then VocAddr_Fax1
                                            Else ''
                                            End, 
                                DFax =  Case A.VocAddrType 
                                                When 0 Then (Select Fax1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select DocFax From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select DocFax From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else (DocFax) 
		                                                    End)
                                                When 9 Then VocAddr_Fax1
                                            Else ''
                                            End, 
                                EMail1 = Case A.VocAddrType 
                                                When 0 Then (Select Email1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select Email1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select Email1 From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else (A.Email1) 
		                                                    End)
                                                When 9 Then VocAddr_Email1
                                            Else ''
                                            End,
                                DocPrtNoPay,
                                LocationName = (Select Name From Location (NOLOCK) Where RecID = A.Location),
                                TaxAccNo, TaxOffice, BossName,
                                InvAddress = Case A.VocAddrType 
                                                When 0 Then (Select Address + ' ' + AddrZip + ' ' + AddrCity From Office (NOLOCK) Where Code = A.OprOffice)
                                                When 1 Then (Select InvAddress From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                When 2 Then (Case When UseMainCont = 'Y' 
			                                                    Then (Select InvAddress  From Agency (NOLOCK) Where Code = A.MainOffice)
			                                                    Else InvAddress
		                                                    End)
                                                When 9 Then VocAddr_Address + ' ' + VocAddr_Zip + ' ' + VocAddr_City
                                              Else ''
                                              End,
                                Bank1, Bank1Name=isnull((Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Bank (NOLOCK) Where Code = A.Bank1), ''),
                                Bank1BankNo, Bank1AccNo, Bank1IBAN, Bank1Curr
                        From Agency A (NOLOCK) 
                        Join Office O (NOLOCK) ON O.Code = A.OprOffice
                        Where A.Code = @Agency";

                        #endregion
                        dbCommand = db.GetSqlStringCommand(sqlstr);
                        try
                        {
                            db.AddInParameter(dbCommand, "Agency", DbType.String, tmpAgency);
                            using (IDataReader reader = db.ExecuteReader(dbCommand))
                            {
                                while (reader.Read())
                                {
                                    AgencyRecord a = new AgencyRecord();
                                    a.Address = Conversion.getStrOrNull(reader["Address"]);
                                    a.FirmName = Conversion.getStrOrNull(reader["FirmName"]);
                                    a.Bank1AccNo = Conversion.getStrOrNull(reader["Bank1AccNo"]);
                                    a.Bank1BankNo = Conversion.getStrOrNull(reader["Bank1BankNo"]);
                                    a.Bank1Curr = Conversion.getStrOrNull(reader["Bank1Curr"]);
                                    a.Bank1IBAN = Conversion.getStrOrNull(reader["Bank1IBAN"]);
                                    a.Bank1 = Conversion.getStrOrNull(reader["Bank1Name"]);
                                    a.BossName = Conversion.getStrOrNull(reader["BossName"]);
                                    a.AddrCity = Conversion.getStrOrNull(reader["AddrCity"]);
                                    a.AddrCountry = Conversion.getStrOrNull(reader["AddrCountry"]);
                                    a.DocAddress = Conversion.getStrOrNull(reader["DAddress"]);
                                    a.DocAddrCity = Conversion.getStrOrNull(reader["DAddrCity"]);
                                    a.DocAddrCountry = Conversion.getStrOrNull(reader["DAddrCountry"]);
                                    a.DocFax = Conversion.getStrOrNull(reader["DFax"]);
                                    a.DocPrtNoPay = Conversion.getStrOrNull(reader["DocPrtNoPay"]) == "Y" ? true : false;
                                    a.DocPhone = Conversion.getStrOrNull(reader["DPhone"]);
                                    a.DocAddrZip = Conversion.getStrOrNull(reader["DAddrZip"]);
                                    a.Email1 = Conversion.getStrOrNull(reader["EMail1"]);
                                    a.Fax1 = Conversion.getStrOrNull(reader["Fax1"]);
                                    a.FirmName = Conversion.getStrOrNull(reader["FirmName"]);
                                    a.InvAddress = Conversion.getStrOrNull(reader["InvAddress"]);
                                    a.LocationName = Conversion.getStrOrNull(reader["LocationName"]);
                                    a.Name = Conversion.getStrOrNull(reader["Name"]);
                                    a.Phone1 = Conversion.getStrOrNull(reader["Phone1"]);
                                    a.TaxAccNo = Conversion.getStrOrNull(reader["TaxAccNo"]);
                                    a.TaxOffice = Conversion.getStrOrNull(reader["TaxOffice"]);
                                    a.AddrZip = Conversion.getStrOrNull(reader["AddrZip"]);

                                    list.Add(a);
                                }
                            }

                        }
                        catch (Exception)
                        {
                            return null;
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public AgencyDocAddressRecord getAgencyDocAdress(string Agency, ref string errorMsg)
        {
            #region Sql string
            string tsql = @"Declare @mainAgency VarChar(10), @UseMainCont VarChar(1)
                            Select @UseMainCont=UseMainCont, @mainAgency=MainOffice From Agency (NOLOCK) Where Code=@Agency
                            if (@UseMainCont='Y' And @mainAgency <>'') Set @Agency=@mainAgency

                            Select A.MainOffice, A.UseMainCont, A.VocAddrType, A.Code, A.Name, A.FirmName,	                            
                                    DAddress = Case A.VocAddrType 
                                                    When 0 Then (Select [Address] From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select [DocAddress] From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select  [DocAddress] From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else ([DocAddress]) 
                                                                End)
                                                    When 9 Then VocAddr_Address
                                                Else ''
                                                End,
                                    Address = Case A.VocAddrType 
                                                    When 0 Then (Select [Address] From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select [Address] From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select  [Address] From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Address) 
                                                                End)
                                                    When 9 Then VocAddr_Address
                                                Else ''
                                                End,
                                    DAddrZip =  Case A.VocAddrType 
                                                    When 0 Then (Select AddrZip From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocAddrZip From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select DocAddrZip From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (DocAddrZip) 
                                                                End)
                                                    When 9 Then VocAddr_Zip
                                                Else ''
                                                End, 
                                    AddrZip =  Case A.VocAddrType 
                                                    When 0 Then (Select AddrZip From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select AddrZip From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select  AddrZip From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.AddrZip) 
                                                                End)
                                                    When 9 Then VocAddr_Zip
                                                Else ''
                                                End, 
                                    AddrCity = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCity From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select AddrCity From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select AddrCity From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (A.AddrCity) 
                                                                    End)
                                                        When 9 Then VocAddr_City
                                                    Else ''
                                                    End, 
                                    DAddrCity = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCity From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select DocAddrCity From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select DocAddrCity From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (DocAddrCity) 
                                                                    End)
                                                        When 9 Then VocAddr_City
                                                    Else ''
                                                    End, 
                                    AddrCountry = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCountry From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select AddrCountry From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select AddrCountry From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (A.AddrCountry) 
                                                                    End)
                                                        When 9 Then VocAddr_Country
                                                    Else ''
                                                    End,  
                                    DAddrCountry = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCountry From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select DocAddrCountry From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select DocAddrCountry From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (DocAddrCountry) 
                                                                    End)
                                                        When 9 Then VocAddr_Country
                                                    Else ''
                                                    End, 
                                    Phone1 =  Case A.VocAddrType 
                                                    When 0 Then (Select Phone1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select Phone1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select Phone1 From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Phone1) 
                                                                End)
                                                    When 9 Then VocAddr_Phone1
                                                Else ''
                                                End, 
                                    DPhone =  Case A.VocAddrType 
                                                    When 0 Then (Select Phone1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocPhone From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select DocPhone From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (DocPhone) 
                                                                End)
                                                    When 9 Then VocAddr_Phone1
                                                Else ''
                                                End, 
                                    Fax1 =  Case A.VocAddrType 
                                                    When 0 Then (Select Fax1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select Fax1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select Fax1 From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Fax1) 
                                                                End)
                                                    When 9 Then VocAddr_Fax1
                                                Else ''
                                                End, 
                                    DFax =  Case A.VocAddrType 
                                                    When 0 Then (Select Fax1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocFax From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select DocFax From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (DocFax) 
                                                                End)
                                                    When 9 Then VocAddr_Fax1
                                                Else ''
                                                End, 
                                    EMail1 = Case A.VocAddrType 
                                                    When 0 Then (Select Email1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select Email1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select Email1 From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Email1) 
                                                                End)
                                                    When 9 Then VocAddr_Email1
                                                Else ''
                                                End,
                                    DocPrtNoPay,
                                    LocationName = (Select Name From Location (NOLOCK) Where RecID = A.Location),
                                    TaxAccNo, TaxOffice, BossName,
                                    A.InvAddress, 
                                    A.InvAddrZip, 
                                    A.InvAddrCity, 
                                    A.InvAddrCountry,
                                    Bank1, Bank1Name = isnull((Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Bank (NOLOCK) Where Code = A.Bank1), ''),
                                    Bank1BankNo, Bank1AccNo, Bank1IBAN, Bank1Curr
                            From Agency A (NOLOCK) 
                            Join Office O (NOLOCK) ON O.Code = A.OprOffice
                            Where A.Code = @Agency ";
            #endregion

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    AgencyDocAddressRecord record = new AgencyDocAddressRecord();
                    if (rdr.Read())
                    {
                        record.MainOffice = Conversion.getStrOrNull(rdr["MainOffice"]);
                        record.UseMainCont = Equals(rdr["UseMainCont"], "Y");
                        record.VocAddrType = Conversion.getStrOrNull(rdr["VocAddrType"]);
                        record.Code = Conversion.getStrOrNull(rdr["Code"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.FirmName = Conversion.getStrOrNull(rdr["FirmName"]);
                        record.DAddress = Conversion.getStrOrNull(rdr["DAddress"]);
                        record.Address = Conversion.getStrOrNull(rdr["Address"]);
                        record.DAddrZip = Conversion.getStrOrNull(rdr["DAddrZip"]);
                        record.AddrZip = Conversion.getStrOrNull(rdr["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(rdr["AddrCity"]);
                        record.DAddrCity = Conversion.getStrOrNull(rdr["DAddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(rdr["AddrCountry"]);
                        record.DAddrCountry = Conversion.getStrOrNull(rdr["DAddrCountry"]);
                        record.Phone1 = Conversion.getStrOrNull(rdr["Phone1"]);
                        record.DPhone = Conversion.getStrOrNull(rdr["DPhone"]);
                        record.Fax1 = Conversion.getStrOrNull(rdr["Fax1"]);
                        record.DFax = Conversion.getStrOrNull(rdr["DFax"]);
                        record.EMail1 = Conversion.getStrOrNull(rdr["EMail1"]);
                        record.DocPrtNoPay = Equals(rdr["DocPrtNoPay"], "Y");
                        record.LocationName = Conversion.getStrOrNull(rdr["LocationName"]);
                        record.TaxAccNo = Conversion.getStrOrNull(rdr["TaxAccNo"]);
                        record.TaxOffice = Conversion.getStrOrNull(rdr["TaxOffice"]);
                        record.BossName = Conversion.getStrOrNull(rdr["BossName"]);
                        record.InvAddress = Conversion.getStrOrNull(rdr["InvAddress"]);
                        record.InvAddrZip = Conversion.getStrOrNull(rdr["InvAddrZip"]);
                        record.InvAddrCity = Conversion.getStrOrNull(rdr["InvAddrCity"]);
                        record.InvAddrCountry = Conversion.getStrOrNull(rdr["InvAddrCountry"]);
                        record.Bank1 = Conversion.getStrOrNull(rdr["Bank1"]);
                        record.Bank1Name = Conversion.getStrOrNull(rdr["Bank1Name"]);
                        record.Bank1BankNo = Conversion.getStrOrNull(rdr["Bank1BankNo"]);
                        record.Bank1AccNo = Conversion.getStrOrNull(rdr["Bank1AccNo"]);
                        record.Bank1IBAN = Conversion.getStrOrNull(rdr["Bank1IBAN"]);
                        record.Bank1Curr = Conversion.getStrOrNull(rdr["Bank1Curr"]);
                        return record;
                    }
                    else
                        return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<AgencyAgreeRecord> getAgencyAgree(string Agency, ref string errorMsg)
        {
            List<AgencyAgreeRecord> records = new List<AgencyAgreeRecord>();
            string tsql = @"Select Aa.RecID,Aa.Agency,Aa.BegDate,Aa.EndDate,Aa.HolPack,Aa.DepositAmount,Aa.DepositCur,
	                            Aa.MinPax,Aa.MinRes,Aa.MinRoom,Aa.MinGiro,Aa.MinGiroCur,Aa.SignDate,Aa.[Status],Aa.ContractNo,
	                            Aa.AgencyAuthority,Aa.OprAuthority,Aa.SignLocation,Aa.Explanation,
	                            Aa.AgreeType,AgreeTypeName=Aat.Name
                            From AgencyAgree Aa (NOLOCK)
                            Join AgencyAgreeType1 Aat (NOLOCK) ON Aa.AgreeType=Aat.Code
                            Where Aa.Agency=@Agency
                           ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AgencyAgreeRecord record = new AgencyAgreeRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.Agency = Conversion.getStrOrNull(R["Agency"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.HolPack = Conversion.getStrOrNull(R["HolPack"]);
                        record.DepositAmount = Conversion.getDecimalOrNull(R["DepositAmount"]);
                        record.DepositCur = Conversion.getStrOrNull(R["DepositCur"]);
                        record.MinPax = Conversion.getInt32OrNull(R["MinPax"]);
                        record.MinRes = Conversion.getInt32OrNull(R["MinRes"]);
                        record.MinRoom = Conversion.getInt32OrNull(R["MinRoom"]);
                        record.MinGiro = Conversion.getDecimalOrNull(R["MinGiro"]);
                        record.MinGiroCur = Conversion.getStrOrNull(R["MinGiroCur"]);
                        record.SignDate = Conversion.getDateTimeOrNull(R["SignDate"]);
                        record.Status = Conversion.getStrOrNull(R["Status"]);
                        record.ContractNo = Conversion.getStrOrNull(R["ContractNo"]);
                        record.AgencyAuthority = Conversion.getStrOrNull(R["AgencyAuthority"]);
                        record.OprAuthority = Conversion.getStrOrNull(R["OprAuthority"]);
                        record.SignLocation = Conversion.getStrOrNull(R["SignLocation"]);
                        record.Explanation = Conversion.getStrOrNull(R["Explanation"]);
                        record.AgreeType = Conversion.getStrOrNull(R["AgreeType"]);
                        record.AgreeTypeName = Conversion.getStrOrNull(R["AgreeTypeName"]);
                        records.Add(record);
                    }
                }
                return records;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
        private string GetNewAgencyCode(ref string errorMsg)
        {
            string code = "AG";
            string tsql = "select max(code) from Agency where Code LIKE 'AG%'";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                int intVal = 0;
                string maxCode = (string)db.ExecuteScalar(dbCommand);
                if (int.TryParse(maxCode.Replace("AG", ""), out intVal))
                {
                    intVal += 1;
                    if (intVal < 1000)
                        code = "AG0";
                    code = string.Concat(code, intVal);
                }
            }
            catch(Exception ex)
            {
                errorMsg = ex.Message;
            }
            return code;
        }

        public string AddAgency(AgencyRecord agency,ref string errorMsg)
        {
            agency.Code = GetNewAgencyCode(ref errorMsg);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                errorMsg = string.Empty;
                AddAgency(agency, ref errorMsg);
            }
                
            string tsql = @"insert into Agency(
                Code,FirmName,Name,Location,MasterPass,Email1,BossName,VATPayer,TypeOwnerShip,RegisterCode,RegisterDate,OprOffice,Phone1,ContName,
                AgencyType,WorkType,Status,BlackList,UseSysPay,UseOffMarket,UseMainCont,UseMainAcc,UseMainPay,
                SupComType,AceExport,AceSendTo,DocPrtNoPay,VocAddrType,
                OwnAgency,ResPayRule,UseSysParamCredit,CreditMode,AllowMakeRes,InvoiceTo,PaymentFrom,AgreeCheck,EarnBonus,UserEarnBonus,WebDisp,
                Aggregator,WDoc_Voucher,WDoc_Ticket,WDoc_Agree,WDoc_InvoiceA,WDoc_Insurance,WDoc_Receipt,WDoc_TransVoucher,WDoc_ExcVoucher,WDoc_Confirmation,
                BonusUseSysParam,BonusAgencySeeOwnW,BonusUserSeeOwnW,BonusUserSeeAgencyW,PayOptType,UseSysParamDocTo,UseSysParamOnlyTicket,DocTicketEmailTo,
                DocVoucherEmailTo,DocInsuranceEmailTo,DocContractEmailTo,CanPayment,Pxm_Use,Pxm_UseLimit_UseMarOpt,Pxm_UseLimit,Pxm_CancelDeadLine_UseMarOpt,
                Pxm_CancelDeadLine,ResConfRule,SaleUmrah,SaleHajj,SaleIndRes,VATIncluded,Pxm_RestrictAccountCredit,Pxm_RestrictAccountCreditCard,Pxm_RestrictCustomerCreditCard,
                Pxm_DoNotShowOwnCntry,Pxm_DoNotShowOwnCntry_UseMarOpt,AccStat,CancelWarn,DontSendOverDueMsg)
                            values(
                @Code,@FirmName,@Name,@Location,@MasterPass,@Email1,@BossName,@VATPayer,@TypeOwnerShip,@RegisterCode,@RegisterDate,@OprOffice,@Phone1,@ContName,
                0,1,'N','N','Y','Y','N','N','N',0,'N',0,'N',2,0,0,'Y',0,'Y',0,0,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,0,0,0,0,1,1,0,1,1,
                0,1,1,1,1,0,0,0,1,1,0,1,0)";
            
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String,agency.Code);
                db.AddInParameter(dbCommand, "FirmName", DbType.String, agency.FirmName);
                db.AddInParameter(dbCommand, "Name", DbType.String, agency.Name);
                db.AddInParameter(dbCommand, "Location", DbType.Int32, agency.Location);
                db.AddInParameter(dbCommand, "MasterPass", DbType.String, agency.MasterPass);
                db.AddInParameter(dbCommand, "Email1", DbType.String, agency.Email1);
                db.AddInParameter(dbCommand, "BossName", DbType.String, agency.BossName);
                db.AddInParameter(dbCommand, "OprOffice", DbType.String, agency.OprOffice);
                db.AddInParameter(dbCommand, "ContName", DbType.String, agency.ContName);
                db.AddInParameter(dbCommand, "Phone1", DbType.String, agency.Phone1);
                db.AddInParameter(dbCommand, "VATPayer", DbType.Boolean, agency.VATPayer);
                db.AddInParameter(dbCommand, "TypeOwnerShip", DbType.Int16, agency.TypeOfOwnerShip);
                db.AddInParameter(dbCommand, "RegisterCode", DbType.String, agency.RegisterCode);
                db.AddInParameter(dbCommand, "RegisterDate", DbType.Date, agency.RegisterDate);
                db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }
            return agency.Code;
        }
        public bool updateAgencyInfo(string Agency, string contactName, string phone1, string phone2, string mobile, string fax1, string fax2, string www,
                                   string email1, string email2, string address, string zip, string city, ref string errorMsg)
        {
            string setValues = string.Empty;
            if (!string.IsNullOrEmpty(contactName))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("ContName='{0}'", contactName);
            }
            if (!string.IsNullOrEmpty(phone1))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("Phone1='{0}'", phone1);
            }
            if (!string.IsNullOrEmpty(phone2))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("Phone2='{0}'", phone2);
            }
            if (!string.IsNullOrEmpty(mobile))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("MobPhone='{0}'", mobile);
            }
            if (!string.IsNullOrEmpty(fax1))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("Fax1='{0}'", fax1);
            }
            if (!string.IsNullOrEmpty(fax2))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("Fax2='{0}'", fax2);
            }
            if (!string.IsNullOrEmpty(www))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("www='{0}'", www);
            }
            if (!string.IsNullOrEmpty(email1))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("email1='{0}'", email1);
            }
            if (!string.IsNullOrEmpty(email2))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("email2='{0}'", email2);
            }
            if (!string.IsNullOrEmpty(address))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("Address='{0}'", address);
            }
            if (!string.IsNullOrEmpty(zip))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("AddrZip='{0}'", zip);
            }
            if (!string.IsNullOrEmpty(city))
            {
                if (setValues.Length > 0)
                    setValues += ", ";
                setValues += string.Format("AddrCity='{0}'", city);
            }
            if (setValues.Length > 0)
            {
                string tsql = string.Empty;
                tsql = string.Format(@"Update Agency
                                    Set {0}
                                    Where Code=@AgencyID ", setValues);
                Database db = (Database)DatabaseFactory.CreateDatabase();
                DbCommand dbCommand = db.GetSqlStringCommand(tsql);
                try
                {
                    db.AddInParameter(dbCommand, "AgencyID", DbType.String, Agency);

                    db.ExecuteNonQuery(dbCommand);

                    return true;
                }
                catch (Exception Ex)
                {
                    errorMsg = Ex.Message;
                    return false;
                }
                finally
                {
                    dbCommand.Connection.Close();
                    dbCommand.Dispose();
                }
            }
            else
                return true;
        }

        public List<AgencySaleSer> getAgencySaleRestriction(string Agency, ref string errorMsg)
        {
            List<AgencySaleSer> list = new List<AgencySaleSer>();
            string tsql = string.Empty;
            tsql +=
@"
Select RecID,Agency,ServiceType,[Service],CanSale 
From AgencySaleSer (NOLOCK)
Where Agency=@Agency
";        
            
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.AnsiString, Agency);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AgencySaleSer rec = new AgencySaleSer();

                        rec.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        rec.Agency = Conversion.getStrOrNull(R["Agency"]);
                        rec.ServiceType = Conversion.getStrOrNull(R["ServiceType"]);
                        rec.Service = Conversion.getStrOrNull(R["Service"]);
                        rec.CanSale = Conversion.getBoolOrNull(R["CanSale"]);

                        list.Add(rec);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}
