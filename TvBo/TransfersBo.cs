﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvBo
{
    public enum TransferSerArea : short { None = 0, Local = 1, Destination = 2 }

    public class TransferRecord
    {
        public TransferRecord()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _TrfType;
        public string TrfType
        {
            get { return _TrfType; }
            set { _TrfType = value; }
        }

        string _TrfTypeName;
        public string TrfTypeName
        {
            get { return _TrfTypeName; }
            set { _TrfTypeName = value; }
        }

        string _TrfTypeNameL;
        public string TrfTypeNameL
        {
            get { return _TrfTypeNameL; }
            set { _TrfTypeNameL = value; }
        }

        bool _PayCom;
        public bool PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        Int16 _ConfStat;
        public Int16 ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool _PayComEB;
        public bool PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        bool _B2CPub;
        public bool B2CPub
        {
            get { return _B2CPub; }
            set { _B2CPub = value; }
        }

        bool _B2BPub;
        public bool B2BPub
        {
            get { return _B2BPub; }
            set { _B2BPub = value; }
        }

        string _Direction;
        public string Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _SerArea;
        public Int16? SerArea
        {
            get { return _SerArea; }
            set { _SerArea = value; }
        }
    }

    public class TransferRecordV2
    {
        public TransferRecordV2()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _TrfType;
        public string TrfType
        {
            get { return _TrfType; }
            set { _TrfType = value; }
        }

        string _TrfTypeName;
        public string TrfTypeName
        {
            get { return _TrfTypeName; }
            set { _TrfTypeName = value; }
        }

        string _TrfTypeNameL;
        public string TrfTypeNameL
        {
            get { return _TrfTypeNameL; }
            set { _TrfTypeNameL = value; }
        }

        bool _PayCom;
        public bool PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        Int16 _ConfStat;
        public Int16 ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool _PayComEB;
        public bool PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        bool _B2CPub;
        public bool B2CPub
        {
            get { return _B2CPub; }
            set { _B2CPub = value; }
        }

        bool _B2BPub;
        public bool B2BPub
        {
            get { return _B2BPub; }
            set { _B2BPub = value; }
        }

        string _Direction;
        public string Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _SerArea;
        public Int16? SerArea
        {
            get { return _SerArea; }
            set { _SerArea = value; }
        }

        int? _Departure;
        public int? Departure
        {
            get { return _Departure; }
            set { _Departure = value; }
        }

        int? _Arrival;
        public int? Arrival
        {
            get { return _Arrival; }
            set { _Arrival = value; }
        }
    }

    public class TransferPriceRecord
    {
        public TransferPriceRecord()
        {
        }

        string _Transfer;
        public string Transfer
        {
            get { return _Transfer; }
            set { _Transfer = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        int? _Country;
        public int? Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; }
        }

        string _CountryNameL;
        public string CountryNameL
        {
            get { return _CountryNameL; }
            set { _CountryNameL = value; }
        }

        DateTime _BegDate;
        public DateTime BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime _EndDate;
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        int? _TrfFrom;
        public int? TrfFrom
        {
            get { return _TrfFrom; }
            set { _TrfFrom = value; }
        }

        string _TrfFromName;
        public string TrfFromName
        {
            get { return _TrfFromName; }
            set { _TrfFromName = value; }
        }

        string _TrfFromNameL;
        public string TrfFromNameL
        {
            get { return _TrfFromNameL; }
            set { _TrfFromNameL = value; }
        }

        int? _TrfTo;
        public int? TrfTo
        {
            get { return _TrfTo; }
            set { _TrfTo = value; }
        }

        string _TrfToName;
        public string TrfToName
        {
            get { return _TrfToName; }
            set { _TrfToName = value; }
        }

        string _TrfToNameL;
        public string TrfToNameL
        {
            get { return _TrfToNameL; }
            set { _TrfToNameL = value; }
        }

        int? _FromPax;
        public int? FromPax
        {
            get { return _FromPax; }
            set { _FromPax = value; }
        }

        int? _ToPax;
        public int? ToPax
        {
            get { return _ToPax; }
            set { _ToPax = value; }
        }

        string _PriceType;
        public string PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _FreeMaxAge;
        public decimal? FreeMaxAge
        {
            get { return _FreeMaxAge; }
            set { _FreeMaxAge = value; }
        }

        decimal? _ChdG1MaxAge;
        public decimal? ChdG1MaxAge
        {
            get { return _ChdG1MaxAge; }
            set { _ChdG1MaxAge = value; }
        }

        decimal? _ChdG2MaxAge;
        public decimal? ChdG2MaxAge
        {
            get { return _ChdG2MaxAge; }
            set { _ChdG2MaxAge = value; }
        }

    }

    public class TransferDetailRecord
    {
        public TransferDetailRecord()
        {
        }

        string _Transfer;
        public string Transfer
        {
            get { return _Transfer; }
            set { _Transfer = value; }
        }

        string _Market;
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        int? _Country;
        public int? Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        DateTime _BegDate;
        public DateTime BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime _EndDate;
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        int? _TrfFrom;
        public int? TrfFrom
        {
            get { return _TrfFrom; }
            set { _TrfFrom = value; }
        }

        int? _TrfTo;
        public int? TrfTo
        {
            get { return _TrfTo; }
            set { _TrfTo = value; }
        }

        int? _FromPax;
        public int? FromPax
        {
            get { return _FromPax; }
            set { _FromPax = value; }
        }

        int? _ToPax;
        public int? ToPax
        {
            get { return _ToPax; }
            set { _ToPax = value; }
        }

        string _PriceType;
        public string PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        decimal? _FreeMaxAge;
        public decimal? FreeMaxAge
        {
            get { return _FreeMaxAge; }
            set { _FreeMaxAge = value; }
        }

        decimal? _ChdG1MaxAge;
        public decimal? ChdG1MaxAge
        {
            get { return _ChdG1MaxAge; }
            set { _ChdG1MaxAge = value; }
        }

        decimal? _ChdG2MaxAge;
        public decimal? ChdG2MaxAge
        {
            get { return _ChdG2MaxAge; }
            set { _ChdG2MaxAge = value; }
        }
    }

    public class TransTypeRecord
    {
        public int? RecID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string NameL { get; set; }
        public string NameS { get; set; }
        public string Description { get; set; }
        public TransTypeRecord()
        {
        }
    }

    [Serializable]
    public class TrfTypeListRecord
    {
        public string Code { get; set; }
        public string Desc { get; set; }
        public TrfTypeListRecord()
        {
        }
    }

    [Serializable()]
    public class TransferListParam
    {
        public string Market { get; set; }
        public string DefaultTrfType { get; set; }
        public List<TrfTypeListRecord> TrfTypeList { get; set; }
        public TransferListParam()
        {
            this.TrfTypeList = new List<TrfTypeListRecord>();
        }
    }

    [Serializable()]
    public class OnlyTransferFilterData
    {
        public string Code { get; set; }
        public int? Country { get; set; }
        public string CountryName { get; set; }
        public string CountryNameL { get; set; }
        public string TrfType { get; set; }
        public string TrfTypeName { get; set; }
        public string TrfTypeNameL { get; set; }
        public int? City { get; set; }
        public string CityName { get; set; }
        public string CityNameL { get; set; }
        public int? TrfFrom { get; set; }
        public string TrfFromName { get; set; }
        public string TrfFromNameL { get; set; }
        public int? TrfTo { get; set; }
        public string TrfToName { get; set; }
        public string TrfToNameL { get; set; }
        public string Direction { get; set; }
        public Int16? SerArea { get; set; }
        public OnlyTransferFilterData()
        {
        }
    }

    [Serializable()]
    public class OnlyTransferFilter
    {
        public OnlyTransferFilter()
        {
            _BegDate = DateTime.Today;
            _EndDate = DateTime.Today;
            _RoomsInfo = new List<SearchCriteriaRooms>();
            _RoomCount = 1;
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        int? _Country;
        public int? Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        int? _TrfFrom;
        public int? TrfFrom
        {
            get { return _TrfFrom; }
            set { _TrfFrom = value; }
        }

        int? _TrfTo;
        public int? TrfTo
        {
            get { return _TrfTo; }
            set { _TrfTo = value; }
        }

        string _Direction;
        public string Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

        Int16? _SerArea;
        public Int16? SerArea
        {
            get { return _SerArea; }
            set { _SerArea = value; }
        }

        Int16 _RoomCount;
        public Int16 RoomCount
        {
            get { return _RoomCount; }
            set { _RoomCount = value; }
        }

        List<SearchCriteriaRooms> _RoomsInfo;
        public List<SearchCriteriaRooms> RoomsInfo
        {
            get { return _RoomsInfo; }
            set { _RoomsInfo = value; }
        }

        bool _CurControl;
        public bool CurControl
        {
            get { return _CurControl; }
            set { _CurControl = value; }
        }

        string _CurrentCur;
        public string CurrentCur
        {
            get { return _CurrentCur; }
            set { _CurrentCur = value; }
        }

        string _TrfType;
        public string TrfType
        {
            get { return _TrfType; }
            set { _TrfType = value; }
        }
    }

    public class OnlyTransfer_Transfers
    {
        public int RefNo { get; set; }
        public string Market { get; set; }
        public DateTime? DepDate { get; set; }
        public string DepTransfer { get; set; }
        public string DepTrfType { get; set; }
        public string DepTrfTypeName { get; set; }
        public string DepTrfTypeNameL { get; set; }
        public TransferRecord depTransferRecord { get; set; }
        public int? DepVehicleCatID { get; set; }
        public string DepVehicleCatName { get; set; }
        public int? DepTrfFrom { get; set; }
        public string DepTrfFromName { get; set; }
        public string DepTrfFromNameL { get; set; }
        public int? DepTrfTo { get; set; }
        public string DepTrfToName { get; set; }
        public string DepTrfToNameL { get; set; }
        public string DepDirection { get; set; }

        public DateTime? RetDate { get; set; }
        public string RetTransfer { get; set; }
        public string RetTrfType { get; set; }
        public string RetTrfTypeName { get; set; }
        public string RetTrfTypeNameL { get; set; }
        public TransferRecord retTransferRecord { get; set; }
        public int? RetVehicleCatID { get; set; }
        public string RetVehicleCatName { get; set; }
        public int? RetTrfFrom { get; set; }
        public string RetTrfFromName { get; set; }
        public string RetTrfFromNameL { get; set; }
        public int? RetTrfTo { get; set; }
        public string RetTrfToName { get; set; }
        public string RetTrfToNameL { get; set; }
        public string RetDirection { get; set; }

        public Int16? Days { get; set; }
        public bool? IsRT { get; set; }

        public decimal? DepTotalPrice { get; set; }
        public decimal? RetTotalPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public string SaleCur { get; set; }

        public string DepPriceType { get; set; }
        public string DepSaleCur { get; set; }
        public decimal? DepSaleAdlPrice { get; set; }
        public decimal? DepSaleChdG1Price { get; set; }
        public decimal? DepSaleChdG2Price { get; set; }
        public decimal? DepFreeMaxAge { get; set; }
        public decimal? DepChdG1MaxAge { get; set; }
        public decimal? DepChdG2MaxAge { get; set; }
        public bool DepPriceCalculated { get; set; }

        public string RetPriceType { get; set; }
        public string RetSaleCur { get; set; }
        public decimal? RetSaleAdlPrice { get; set; }
        public decimal? RetSaleChdG1Price { get; set; }
        public decimal? RetSaleChdG2Price { get; set; }
        public decimal? RetFreeMaxAge { get; set; }
        public decimal? RetChdG1MaxAge { get; set; }
        public decimal? RetChdG2MaxAge { get; set; }
        public bool RetPriceCalculated { get; set; }

        public OnlyTransfer_Transfers()
        {
            this.DepPriceCalculated = false;
            this.RetPriceCalculated = false;
        }
    }
}
