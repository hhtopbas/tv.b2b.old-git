﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Web;
using TvReport.SunReport;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using TvTools;

namespace TvReport.FitReport
{
    public class FitReport
    {        
        public ResCustInfoRecord getResCustInfoRecord(int? CustNo, ref string errorMsg)
        {
            string tsql = @"Select RecID, CustNo, CTitle, CTitleName=(Select Code From Title (NOLOCK) Where TitleNo=ResCustInfo.CTitle), CName, CSurName, 
	                            AddrHome, AddrHomeCity, AddrHomeZip, AddrHomeTel, AddrHomeFax, AddrHomeEmail, AddrHomeCountry, AddrHomeCountryCode, HomeTaxOffice, HomeTaxAccNo,
	                            AddrWork, AddrWorkCity, AddrWorkZip, AddrWorkTel, AddrWorkFax, AddrWorkEmail, AddrWorkCountry, AddrWorkCountryCode, WorkTaxOffice, WorkTaxAccNo,
	                            ContactAddr, MobTel, Jobs, Note,  InvoiceAddr, WorkFirmName, Bank, BankAccNo, BankIBAN
                            From ResCustInfo (NOLOCK)
                            Where CustNo = @CustNo   ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "CustNo", DbType.Int32, CustNo.HasValue ? CustNo.Value : -1);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        ResCustInfoRecord record = new ResCustInfoRecord();
                        record.RecID = (int)rdr["RecID"];
                        record.CustNo = (int)rdr["CustNo"];
                        record.CTitle = Conversion.getInt16OrNull(rdr["CTitle"]);
                        record.CTitleName = Conversion.getStrOrNull(rdr["CTitleName"]);
                        record.CName = Conversion.getStrOrNull(rdr["CName"]);
                        record.CSurName = Conversion.getStrOrNull(rdr["CSurName"]);
                        record.AddrHome = Conversion.getStrOrNull(rdr["AddrHome"]);
                        record.AddrHomeCity = Conversion.getStrOrNull(rdr["AddrHomeCity"]);
                        record.AddrHomeZip = Conversion.getStrOrNull(rdr["AddrHomeZip"]);
                        record.AddrHomeTel = Conversion.getStrOrNull(rdr["AddrHomeTel"]);
                        record.AddrHomeFax = Conversion.getStrOrNull(rdr["AddrHomeFax"]);
                        record.AddrHomeEmail = Conversion.getStrOrNull(rdr["AddrHomeEmail"]);
                        record.AddrHomeCountry = Conversion.getStrOrNull(rdr["AddrHomeCountry"]);
                        record.AddrHomeCountryCode = Conversion.getStrOrNull(rdr["AddrHomeCountryCode"]);
                        record.HomeTaxOffice = Conversion.getStrOrNull(rdr["HomeTaxOffice"]);
                        record.HomeTaxAccNo = Conversion.getStrOrNull(rdr["HomeTaxAccNo"]);
                        record.AddrWork = Conversion.getStrOrNull(rdr["AddrWork"]);
                        record.AddrWorkCity = Conversion.getStrOrNull(rdr["AddrWorkCity"]);
                        record.AddrWorkZip = Conversion.getStrOrNull(rdr["AddrWorkZip"]);
                        record.AddrWorkTel = Conversion.getStrOrNull(rdr["AddrWorkTel"]);
                        record.AddrWorkFax = Conversion.getStrOrNull(rdr["AddrWorkFax"]);
                        record.AddrWorkEMail = Conversion.getStrOrNull(rdr["AddrWorkEmail"]);
                        record.AddrWorkCountry = Conversion.getStrOrNull(rdr["AddrWorkCountry"]);
                        record.AddrWorkCountryCode = Conversion.getStrOrNull(rdr["AddrWorkCountryCode"]);
                        record.WorkTaxOffice = Conversion.getStrOrNull(rdr["WorkTaxOffice"]);
                        record.WorkTaxAccNo = Conversion.getStrOrNull(rdr["WorkTaxAccNo"]);
                        record.ContactAddr = Conversion.getStrOrNull(rdr["ContactAddr"]);
                        record.MobTel = Conversion.getStrOrNull(rdr["MobTel"]);
                        record.Jobs = Conversion.getStrOrNull(rdr["Jobs"]);
                        record.Note = Conversion.getStrOrNull(rdr["Note"]);
                        record.InvoiceAddr = Conversion.getStrOrNull(rdr["InvoiceAddr"]);
                        record.WorkFirmName = Conversion.getStrOrNull(rdr["WorkFirmName"]);
                        record.Bank = Conversion.getStrOrNull(rdr["Bank"]);
                        record.BankAccNo = Conversion.getStrOrNull(rdr["BankAccNo"]);
                        record.BankIBAN = Conversion.getStrOrNull(rdr["BankIBAN"]);
                        return record;
                    }
                    else return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }                

        public string createFlyTicket(string ResNo, string pdfFilePath, string tempFolder, string data, string _fileName, ref string errorMsg)
        {
            string fileName = pdfFilePath + _fileName + ".pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_" + _fileName + ".pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();
            doc.Close();
            reader.Close();
            return ResNo + "_" + _fileName + ".pdf";
        }

        public string createVoucher(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "VoucherA.pdf";
            string fileNameB = pdfFilePath + "VoucherB.pdf";
            if (!File.Exists(fileName)) return "";
            if (!File.Exists(fileNameB)) return "";
            string afileName = tempFolder + ResNo + "_Voucher.pdf";

            PdfReader reader = new PdfReader(fileName);
            PdfReader readerB = new PdfReader(fileNameB);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);
            systemColor blackColor = new systemColor();
            blackColor.R = 0;
            blackColor.G = 0;
            blackColor.B = 0;
            foreach (Coordinate row in _data.Where(w=>w.PageNo==1))
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x * 2.835f, row.y * 2.835f, row.bold ? fontArialBold : fontArial, row.FontSize, row.bold, row.Underline, blackColor, row.Align);

            cb.ClosePath();

            doc.NewPage();
            page = pdfWriter.GetImportedPage(readerB, currentpage);
            cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);            

            foreach (Coordinate row in _data.Where(w => w.PageNo == 2))
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x * 2.835f, row.y * 2.835f, row.bold ? fontArialBold : fontArial, row.FontSize, row.bold, row.Underline, blackColor, row.Align);

            cb.ClosePath();

            doc.Close();
            reader.Close();
            return ResNo + "_Voucher.pdf";
        }        

        public string createInvoice(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "ClientInvoice.pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_Invoice.pdf";

            PdfReader reader = new PdfReader(fileName);
            try
            {
                int currentpage = 1;
                byte[] pdfBytes = reader.GetPageContent(currentpage);
                if (File.Exists(afileName))
                    File.Delete(afileName);
                Document doc = new Document(reader.GetPageSize(1));
                PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
                doc.Open();
                byte[] pageContent = reader.GetPageContent(currentpage);
                doc.NewPage();
                PdfImportedPage page;
                page = pdfWriter.GetImportedPage(reader, currentpage);
                PdfContentByte cb = pdfWriter.DirectContent;

                cb.AddTemplate(page, 0, 0);
                string fontpath = HttpContext.Current.Server.MapPath(".");
                BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);                
                BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);                
                List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

                foreach (Coordinate row in _data)
                    if (row.isShown)
                    {
                        if (row.Type != writeType.Text)
                        {
                            cb.SetLineWidth(row.LineWidth);
                            cb.MoveTo(row.x, row.y);
                            cb.LineTo(row.x + row.W, row.y + row.H);
                            cb.Stroke();
                        }
                        else
                            TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize, row.bold, row.Underline, row.Color, row.Align);
                    }
                cb.ClosePath();
                doc.Close();
                reader.Close();
                return ResNo + "_Invoice.pdf";
            }
            catch
            {
                reader.Close();
                return "";
            }            
        }

        public string mergeFlyTicket(string tempFolder, List<string> ticketList)
        {
            string aFileName = ticketList[0].ToString().Substring(0, ticketList[0].ToString().IndexOf('_')) + "_ETicket.pdf";
            if (File.Exists(tempFolder + aFileName))
                File.Delete(tempFolder + aFileName);
            string fileName = ticketList.FirstOrDefault().ToString().Substring(0, ticketList[0].ToString().IndexOf('_'));
            int currentpage = 1;
            Document doc = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(tempFolder + aFileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            for (int i = 0; i < ticketList.Count; i++)
            {
                PdfReader reader2 = new PdfReader(tempFolder + ticketList[i].ToString());
                byte[] pdfBytes2 = reader2.GetPageContent(currentpage);
                doc.NewPage();
                PdfImportedPage page = pdfWriter.GetImportedPage(reader2, currentpage);

                PdfContentByte cb2 = pdfWriter.DirectContent;
                cb2.AddTemplate(page, 0, 0);
                cb2.ClosePath();
                reader2.Close();
                //File.Delete(tempFolder + ticketList[i].ToString());
            }
            doc.Close();

            return aFileName;
        }
    }
}
