﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Web;
using TvReport.SunReport;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using TvTools;

namespace TvReport.RezedaReport
{
    public class RezedaReport
    {
        public AgencyDocAddressRecord getAgencyDocAdress(string Agency, ref string errorMsg)
        {
            #region Sql string
            string tsql = @"Declare @mainAgency VarChar(10), @UseMainCont VarChar(1), @tmpAgency VarChar(10)
                            Select @UseMainCont=UseMainCont, @mainAgency=isnull(MainOffice,'') From Agency (NOLOCK) Where Code=@Agency
                            if (@UseMainCont='Y' And @mainAgency <>'') 
                                Set @tmpAgency=@mainAgency
                            Else Set @tmpAgency=@Agency

                            Select A.MainOffice, A.UseMainCont, A.VocAddrType, A.Code, A.Name, A.FirmName,	                            
                                    DAddress = Case A.VocAddrType 
                                                    When 0 Then (Select [Address] From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select [DocAddress] From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select  [DocAddress] From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else ([DocAddress]) 
                                                                End)
                                                    When 9 Then VocAddr_Address
                                                Else ''
                                                End,
                                    Address = Case A.VocAddrType 
                                                    When 0 Then (Select [Address] From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select [Address] From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select  [Address] From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Address) 
                                                                End)
                                                    When 9 Then VocAddr_Address
                                                Else ''
                                                End,
                                    DAddrZip =  Case A.VocAddrType 
                                                    When 0 Then (Select AddrZip From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocAddrZip From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select DocAddrZip From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (DocAddrZip) 
                                                                End)
                                                    When 9 Then VocAddr_Zip
                                                Else ''
                                                End, 
                                    AddrZip =  Case A.VocAddrType 
                                                    When 0 Then (Select AddrZip From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select AddrZip From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select  AddrZip From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.AddrZip) 
                                                                End)
                                                    When 9 Then VocAddr_Zip
                                                Else ''
                                                End, 
                                    AddrCity = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCity From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select AddrCity From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select AddrCity From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (A.AddrCity) 
                                                                    End)
                                                        When 9 Then VocAddr_City
                                                    Else ''
                                                    End, 
                                    DAddrCity = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCity From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select DocAddrCity From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select DocAddrCity From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (DocAddrCity) 
                                                                    End)
                                                        When 9 Then VocAddr_City
                                                    Else ''
                                                    End, 
                                    AddrCountry = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCountry From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select AddrCountry From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select AddrCountry From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (A.AddrCountry) 
                                                                    End)
                                                        When 9 Then VocAddr_Country
                                                    Else ''
                                                    End,  
                                    DAddrCountry = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCountry From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select DocAddrCountry From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select DocAddrCountry From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (DocAddrCountry) 
                                                                    End)
                                                        When 9 Then VocAddr_Country
                                                    Else ''
                                                    End, 
                                    Phone1 =  Case A.VocAddrType 
                                                    When 0 Then (Select Phone1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select Phone1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select Phone1 From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Phone1) 
                                                                End)
                                                    When 9 Then VocAddr_Phone1
                                                Else ''
                                                End, 
                                    DPhone =  Case A.VocAddrType 
                                                    When 0 Then (Select Phone1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocPhone From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select DocPhone From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (DocPhone) 
                                                                End)
                                                    When 9 Then VocAddr_Phone1
                                                Else ''
                                                End, 
                                    Fax1 =  Case A.VocAddrType 
                                                    When 0 Then (Select Fax1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select Fax1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select Fax1 From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Fax1) 
                                                                End)
                                                    When 9 Then VocAddr_Fax1
                                                Else ''
                                                End, 
                                    DFax =  Case A.VocAddrType 
                                                    When 0 Then (Select Fax1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocFax From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select DocFax From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (DocFax) 
                                                                End)
                                                    When 9 Then VocAddr_Fax1
                                                Else ''
                                                End, 
                                    EMail1 = Case A.VocAddrType 
                                                    When 0 Then (Select Email1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select Email1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select Email1 From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Email1) 
                                                                End)
                                                    When 9 Then VocAddr_Email1
                                                Else ''
                                                End,
                                    DocPrtNoPay,
                                    LocationName = (Select Name From Location (NOLOCK) Where RecID = A.Location),
                                    TaxAccNo, TaxOffice, BossName,
                                    A.InvAddress, 
                                    A.InvAddrZip, 
                                    A.InvAddrCity, 
                                    A.InvAddrCountry,
                                    Bank1, Bank1Name = isnull((Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Bank (NOLOCK) Where Code = A.Bank1), ''),
                                    Bank1BankNo, Bank1AccNo, Bank1IBAN, Bank1Curr
                            From Agency A (NOLOCK) 
                            Join Office O (NOLOCK) ON O.Code = A.OprOffice
                            Where A.Code = @tmpAgency ";
            #endregion

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    AgencyDocAddressRecord record = new AgencyDocAddressRecord();
                    if (rdr.Read())
                    {
                        record.MainOffice = Conversion.getStrOrNull(rdr["MainOffice"]);
                        record.UseMainCont = Equals(rdr["UseMainCont"], "Y");
                        record.VocAddrType = Conversion.getStrOrNull(rdr["VocAddrType"]);
                        record.Code = Conversion.getStrOrNull(rdr["Code"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.FirmName = Conversion.getStrOrNull(rdr["FirmName"]);
                        record.DAddress = Conversion.getStrOrNull(rdr["DAddress"]);
                        record.Address = Conversion.getStrOrNull(rdr["Address"]);
                        record.DAddrZip = Conversion.getStrOrNull(rdr["DAddrZip"]);
                        record.AddrZip = Conversion.getStrOrNull(rdr["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(rdr["AddrCity"]);
                        record.DAddrCity = Conversion.getStrOrNull(rdr["DAddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(rdr["AddrCountry"]);
                        record.DAddrCountry = Conversion.getStrOrNull(rdr["DAddrCountry"]);
                        record.Phone1 = Conversion.getStrOrNull(rdr["Phone1"]);
                        record.DPhone = Conversion.getStrOrNull(rdr["DPhone"]);
                        record.Fax1 = Conversion.getStrOrNull(rdr["Fax1"]);
                        record.DFax = Conversion.getStrOrNull(rdr["DFax"]);
                        record.EMail1 = Conversion.getStrOrNull(rdr["EMail1"]);
                        record.DocPrtNoPay = Equals(rdr["DocPrtNoPay"], "Y");
                        record.LocationName = Conversion.getStrOrNull(rdr["LocationName"]);
                        record.TaxAccNo = Conversion.getStrOrNull(rdr["TaxAccNo"]);
                        record.TaxOffice = Conversion.getStrOrNull(rdr["TaxOffice"]);
                        record.BossName = Conversion.getStrOrNull(rdr["BossName"]);
                        record.InvAddress = Conversion.getStrOrNull(rdr["InvAddress"]);
                        record.InvAddrZip = Conversion.getStrOrNull(rdr["InvAddrZip"]);
                        record.InvAddrCity = Conversion.getStrOrNull(rdr["InvAddrCity"]);
                        record.InvAddrCountry = Conversion.getStrOrNull(rdr["InvAddrCountry"]);
                        record.Bank1 = Conversion.getStrOrNull(rdr["Bank1"]);
                        record.Bank1Name = Conversion.getStrOrNull(rdr["Bank1Name"]);
                        record.Bank1BankNo = Conversion.getStrOrNull(rdr["Bank1BankNo"]);
                        record.Bank1AccNo = Conversion.getStrOrNull(rdr["Bank1AccNo"]);
                        record.Bank1IBAN = Conversion.getStrOrNull(rdr["Bank1IBAN"]);
                        record.Bank1Curr = Conversion.getStrOrNull(rdr["Bank1Curr"]);
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PasPriceRecord> getPasPriceList(string ResNo, ref string errorMsg)
        {
            List<PasPriceRecord> records = new List<PasPriceRecord>();

            #region SQL string
            string tsql = @"--Azur_Invoce_Agency_Script.sql
                        if OBJECT_ID('TempDB.dbo.#ResCustPrice') is not null Drop Table #ResCustPrice

                        if OBJECT_ID('TempDB.dbo.#Passengers') is not null Drop Table #Passengers
                        CREATE TABLE #Passengers(
                        RecID int IDENTITY(1,1) NOT NULL,
                        CustNo int,
                        Name nvarchar(80) Collate database_default,
                        SurName nvarchar(80) Collate database_default,
                        Birthdate datetime,
                        Age smallint,
                        Title varchar(5) Collate database_default,
                        TitleNo Smallint,
                        PageNo smallint,
                        SeqNo smallint
                        )

                        if OBJECT_ID('TempDB.dbo.#PasPrices') is not null Drop Table #PasPrices
                        CREATE TABLE #PasPrices(
                        RecID int IDENTITY(1,1) NOT NULL,
                        PageNo smallint,
                        IncPack varchar(1) Collate database_default,
                        Service varchar(10) Collate database_default,
                        ServiceDesc nvarchar(200) Collate database_default,
                        Pax1Amount dec(18,2),
                        Pax2Amount dec(18,2),
                        Pax3Amount dec(18,2),
                        Pax4Amount dec(18,2),
                        Pax5Amount dec(18,2),
                        Pax6Amount dec(18,2)
                        )

                        if OBJECT_ID('TempDB.dbo.#ResPayPlan') is not null Drop Table #ResPayPlan
                        CREATE TABLE dbo.#ResPayPlan
                        (
                        RecID int IDENTITY(1,1) NOT NULL,
                        ResNo varchar(10),
                        PayNo smallint,
                        DueDate datetime,
                        Amount dec(18,2),
                        Cur varchar(5),
                        Adult dec(18,2),
                        Child dec(18,2),
                        Infant dec(18,2),
                        PayAmount dec(18,2),
                        RemAmount dec(18,2)
                        )

                        if OBJECT_ID('TempDB.dbo.#ServicePrices') is not null Drop Table #ServicePrices
                        CREATE TABLE #ServicePrices(
                        RecID int IDENTITY(1,1) NOT NULL,
                        RecType smallint,
                        IncPack varchar(1) Collate database_default,
                        Unit smallint,
                        Service varchar(10) Collate database_default,
                        ServiceDesc nvarchar(200) Collate database_default,
                        SalePrice dec(18,2)
                        )

                        if OBJECT_ID('TempDB.dbo.#PayPlan') is not null Drop Table #PayPlan
                        CREATE TABLE #PayPlan(
                        RecID int IDENTITY(1,1) NOT NULL,
                        Cur varchar(5),
                        DueDate_L1 datetime,
                        RemAmount_L1 dec(18,2),

                        DueDate_L2 datetime,
                        RemAmount_L2 dec(18,2),

                        DueDate_L3 datetime,
                        RemAmount_L3 dec(18,2)
                        )

                        ------------------------------------------------------------------------------
                        Declare @Market varchar(10),@PLSpoStr nvarchar(200),@PasEBStr nvarchar(200),@ZeroInv bit,@ResStat smallint,
                                @PasBalance dec(18,2)

                        --Set @ResNo='01000026'
                        --Set @ResNo='01000001'
                          
                        Set @ZeroInv=0
                        Set @PLSpoStr='Discount'
                        Set @PasEBStr='Early Booking'

                        Set NOCOUNT ON
                        --Payment Plan items --
                        Declare @AgencyPayment dec(18,2),@RemAmount dec(18,2),@Amount dec(18,2), @PayNo int, @DueDate datetime, @str nvarchar(250), @HasSupplement bit

                        Select @ResStat=ResStat,@AgencyPayment=AgencyPayment,@Market=Market,
                               @PasBalance=IsNull(PasBalance,0)
                        From ResMain (NOLOCK) Where ResNo=@ResNo

                        if @ResStat=2 and @PasBalance<>0 Set @ResStat=3

                        --Pas.EB Description 27.04.2011 added
                        Set @str=''
                        Select @str=P.Description From ResMain R (NOLOCK)
                        Join PasEB P (NOLOCK) on P.RecID=R.EBPasID
                        Where ResNo=@ResNo
                        if @str<>'' Set @PasEBStr=@str

                        --PL.Spo Description
                        Set @str=''
                        Select @str=Spo.Description from ResCustPrice P (NOLOCK)
                        Join ResService S on S.ResNo=P.ResNo and S.RecID=P.ServiceID and S.ServiceType='FLIGHT' and S.StatSer in (0,1) and PlSpoVal<>0
                        Join CatPriceSPo (NOLOCK) SPo on Spo.RecID=P.PLSpoNo
                        Where P.ResNo=@ResNo
                        if @str<>'' Set @PLSpoStr=@str

                        --18.10.2011 i.c. added
                        Select Top 1 @HasSupplement=Case When C.ppPLSpoVal>0 then 1 else 0 end  
                        From ResCust C (NOLOCK)
                        Where C.ResNo=@ResNo and C.ppPLSpoVal<>0 and C.Status=0
                        Set @HasSupplement=IsNull(@HasSupplement,0)

                        Insert Into #ResPayPlan 
                         (ResNo,PayNo,DueDate,Amount,Cur,Adult,Child,Infant,PayAmount,RemAmount)
                        Select ResNo,PayNo,DueDate,Amount,Cur,Adult,Child,Infant,0,0 From ResPayPlan (NOLOCK) Where ResNo=@ResNo
                        Set @RemAmount=@AgencyPayment

                        Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
                          Select PayNo,Amount From #ResPayPlan Order by PayNo

                        Open Cursor1
                        Fetch Next FROM Cursor1 INTO @PayNo,@Amount
                        While @@FETCH_STATUS = 0
                        Begin

                         if @RemAmount>0
                         if @RemAmount>=@Amount
                         Begin
                           Update #ResPayPlan Set PayAmount=@Amount Where PayNo=@PayNo
                           Set @RemAmount=@RemAmount-@Amount
                         End
                         Else 
                         Begin
                           Update #ResPayPlan Set PayAmount=@RemAmount Where PayNo=@PayNo
                           Set @RemAmount=0
                         end
                         if @RemAmount<=0 Break

                         Fetch Next FROM Cursor1 INTO @PayNo,@Amount
                        End
                        Close Cursor1
                        DEALLOCATE Cursor1

                        Update #ResPayPlan Set RemAmount=Amount-PayAmount

                        Truncate Table #PayPlan
                        Insert Into #PayPlan (DueDate_L1,RemAmount_L1) Values(Null,Null) --Insert Empty line

                        Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
                         Select PayNo=ROW_NUMBER() OVER(ORDER BY Duedate),Duedate,RemAmount=Sum(RemAmount) from #ResPayPlan
                         Group by Duedate
                         Order by Duedate

                        Open Cursor1
                        Fetch Next FROM Cursor1 INTO @PayNo,@DueDate,@RemAmount
                        While @@FETCH_STATUS = 0
                        Begin
                         if @PayNo=1
                           Update top (1) #PayPlan Set DueDate_L1=@DueDate,RemAmount_L1=@RemAmount

                         if @PayNo=2
                           Update top (1) #PayPlan Set DueDate_L2=@DueDate,RemAmount_L2=@RemAmount

                         if @PayNo=3
                           Update top (1) #PayPlan Set DueDate_L3=@DueDate,RemAmount_L3=@RemAmount

                         Fetch Next FROM Cursor1 INTO @PayNo,@DueDate,@RemAmount
                        End
                        Close Cursor1
                        DEALLOCATE Cursor1

                        if @PayNo>0
                         Update #PayPlan Set Cur=(Select Top 1 Cur From #ResPayPlan)

                        Declare @CustNo int, @SalePrice dec(18,2),@SeqNo smallint, @Unit int,@ServiceID int,@ExtServiceID int,
                        @IncPack varchar(1),@Service varchar(10), @ServiceDesc nvarchar(200),@BegDate datetime

                        Insert Into #Passengers (CustNo,Name,Birthdate,Title,Age,TitleNo)
                         Select CustNo,Surname+' '+Name,Birtday,Title=T.Code,C.Age,TitleNo=C.Title
                         From ResCust C (NOLOCK)
                         Left Join Title T (NOLOCK) on T.TitleNo=C.Title
                         Where ResNo=@ResNo and Status=0
                        Update #Passengers Set PageNo= CEILING(RecID * 1.0 / 6)

                        Update #Passengers
                        Set SeqNo=xSeqNo
                        From
                        (Select xCustNo=CustNo,xSeqNo=ROW_NUMBER() OVER (PARTITION BY PageNo ORDER BY PageNo) From #Passengers) x
                        Where CustNo=X.xCustNo

                        Select P.*,IncPack,T.TitleGrp
                        Into #ResCustPrice
                        From ResCustPrice P
                        Outer Apply(Select IncPack From ResService (NOLOCK) Where ResService.ResNo=P.ResNo and ResService.RecID=P.ServiceID) S
                        Outer Apply(Select Title From ResCust (NOLOCK) Where ResCust.ResNo=P.ResNo and CustNo=P.CustNo) C
                        Outer Apply(Select TitleGrp=Case When TitleNo<=5 then 0 When TitleNo between 6 and 7 then 1 When TitleNo>=8 then 2 end From Title (NOLOCK) Where TitleNo=C.Title) T
                        Where ResNo=@ResNo and (P.SalePrice<>0 or @ZeroInv=1)

                        if @HasSupplement=1 --18.10.2011 i.c. added
                         Update #ResCustPrice Set PLSpoVal=0

                        --if @@ROWCOUNT=0 and 
                        if @ResStat=3 --CancelX --09.04.2011'de bad women ile bareber yapıldı!!!
                        Begin
                          Select @Amount=Sum(SalePrice) from ResService Where ResNo=@ResNo and SalePrice>0

                         --CancelX amount
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                          Select 7,'N',Unit=1,'CANX',N'Storno',@Amount
                        End

                        Update #ResCustPrice
                        set IncPack=E.IncPack
                        From ResServiceExt E
                        Where E.ResNo=@ResNo and E.ServiceID=#ResCustPrice.ServiceID and E.RecID=#ResCustPrice.ExtServiceID

                        Update #ResCustPrice Set ExtBed=0 Where ExtBed is null

                        if Exists(Select * from #ResCustPrice Where ExtBed=1)
                        Begin
                         Update #ResCustPrice Set ExtBed=1
                         Where IsNull(ExtBed,0)=0 and CustNo in (Select CustNo from #ResCustPrice Where ExtBed=1)
                        End

                        ----- PL SPO Amount ----------------------
                        if @HasSupplement=0
                        BEGIN
                          Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                           Select 6,'Y',Unit=Count(TitleGrp),'',@PLSpoStr,PLSpoVal=ABS(PLSpoVal)*-1
                           From
                           (Select TitleGrp,PLSpoVal=Sum(IsNull(ppPLSpoVal,0)) From ResCust C (NOLOCK)
                            Outer Apply(Select TitleGrp=Case When TitleNo<=5 then 0 When TitleNo between 6 and 7 then 1 When TitleNo>=8 then 2 end From Title (NOLOCK) Where TitleNo=C.Title) T
                            Where C.ResNo=@ResNo
                            Group by TitleGrp,CustNo
                           ) P
                           Where PLSpoVal<>0
                           Group By TitleGrp,PLSpoVal
                           Order by TitleGrp
                        END

                        ----- Include Package Items ----------------------
                        Select @Unit=0,@SalePrice=0
                         --Adult Base price
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                          Select 0,'Y',Unit=Count(TitleGrp),'',N' Dospelá osoba',SalePrice
                          From 
                          (Select TitleGrp,SalePrice=Sum(P.SalePrice-IsNull(P.PLSpoVal,0)) From #ResCustPrice P (NOLOCK)
                           Where Incpack='Y' and ExtBed=0 and TitleGrp=0
                           Group by TitleGrp,CustNo
                          ) P
                          Group By TitleGrp,SalePrice
                          Order by TitleGrp

                         --Adult Extra Bed price
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                          Select 0,'Y',Unit=Count(TitleGrp),'',N'Dospelá osoba ',SalePrice
                          From 
                          (Select TitleGrp,SalePrice=Sum(P.SalePrice-IsNull(P.PLSpoVal,0)) From #ResCustPrice P (NOLOCK)
                           Where Incpack='Y' and ExtBed=1 and TitleGrp=0
                           Group by TitleGrp,CustNo
                          ) P
                          Group By TitleGrp,SalePrice
                          Order by TitleGrp

                         --Child Base price
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                          Select 0,'Y',Unit=Count(TitleGrp),'',N'díte 2-12 let se 2 a 3 dosp. osobami',SalePrice
                          From
                          (Select TitleGrp,SalePrice=Sum(P.SalePrice-IsNull(P.PLSpoVal,0)) From #ResCustPrice P (NOLOCK)
                           Where Incpack='Y' and ExtBed=0 and TitleGrp=1
                           Group by TitleGrp,CustNo
                          ) P
                          Group By TitleGrp,SalePrice
                          Order by TitleGrp

                         --Infan Base price
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                          Select 0,'Y',Unit=Count(TitleGrp),'',N'díte 0-2 let se 2 a 3 dosp. osobami',SalePrice
                          From
                          (Select TitleGrp,SalePrice=Sum(P.SalePrice-IsNull(P.PLSpoVal,0)) From #ResCustPrice P (NOLOCK)
                           Where Incpack='Y' and ExtBed=0 and TitleGrp=2
                           Group by TitleGrp,CustNo
                          ) P
                          Group By TitleGrp,SalePrice
                          Order by TitleGrp

                        ----- Out of Package Items ----------------------
                           Set @IncPack='N'
                           Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
                             Select Unit=Count(TitleGrp), P.SalePrice,S.Service,S.BegDate,P.ServiceID,P.ExtServiceID
                             From ResService S (NOLOCK)
                             Join #ResCustPrice P on P.ServiceID=S.RecID and P.IncPack=@IncPack
                             Where S.ResNo=@ResNo and S.IncPack=@IncPack and P.ExtServiceID=0
                             and not Exists(Select * from AdService A Where A.Service=S.ServiceType and A.Code=S.Service and A.SerType=1)
                             Group by S.Service,S.BegDate,P.SalePrice,P.ServiceID,P.ExtServiceID

                             Union ALL  
                          
                             Select Unit=Count(TitleGrp), P.SalePrice,Service=S.ExtService,S.BegDate,P.ServiceID,P.ExtServiceID
                             From ResServiceExt S (NOLOCK)
                             Join #ResCustPrice P on P.ServiceID=S.ServiceID and P.ExtServiceID=S.RecID and P.IncPack=@IncPack
                             Where S.ResNo=@ResNo and S.IncPack=@IncPack
                             Group by S.ExtService,S.BegDate,P.SalePrice,P.ServiceID,P.ExtServiceID
                             Order by S.BegDate
                           
                           Open Cursor1
                           Fetch Next FROM Cursor1 INTO @Unit,@SalePrice,@Service,/*@ServiceDesc,*/ @BegDate,@ServiceID,@ExtServiceID
                           While @@FETCH_STATUS = 0
                           Begin
                             if @ExtServiceID>0
                               Select @ServiceDesc= (Select Name From ServiceExt Where Code=S.ExtService)
                               From ResServiceExt S (NOLOCK)
                               Where RecID=@ExtServiceID
                             Else
                               Select @ServiceDesc=
                                       Case S.ServiceType
                                        When 'HOTEL' then (Select Name+' ('+Category+'),'+(Select Name From Location (NOLOCK) Where RecID=Hotel.Location) From Hotel (NOLOCK) Where Code=Service)
                                        When 'FLIGHT' then (Select IsNull(PNLName,FlightNo)+' ('+DepAirport+'->'+ArrAirport+'),'+FlgClass From FlightDay (NOLOCK) Where FlightNo=Service and FlyDate=S.BegDate)
                                        When 'TRANSPORT' then (Select TP.Name+' ('+dbo.ufn_GetLocationName(S.DepLocation)+'-'+dbo.ufn_GetLocationName(S.ArrLocation)+') ' From Transport TP (NOLOCK)
                                                               Join TransportDay TPD (NOLOCK) on TPD.Transport=TP.Code and TPD.TransDate=S.BegDate and TPD.Bus=S.Bus
                                                               Where TP.Code=S.Service
                                                               )
                                        When 'RENTING' then (Select Name+' (Category:'+Category+') / '+(Select Name From Location (NOLOCK) Where RecId=DepLocation) From Renting (NOLOCK) Where Code=Service)
                                        When 'EXCURSION' then (Select Name+' / '+(Select Name From Location (NOLOCK) Where RecId=DepLocation) From Excursion (NOLOCK) Where Code=Service)
                                        When 'INSURANCE' then N'PRIPOJIŠTENÍ'
                                        When 'VISA' then (Select Name From Visa (NOLOCK) Where Code=Service)
                                        When 'TRANSFER' then (Select Name+', '+dbo.ufn_GetLocationName(S.ArrLocation) From Transfer (NOLOCK) Where Code=Service)
                                        When 'HANDFEE' then (Select Name+', '+dbo.ufn_GetLocationName(S.ArrLocation) From HandFee (NOLOCK) Where Code=Service)
                                        else (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name) From AdService (NOLOCK) Where AdService.Service=S.ServiceType and Code=S.Service)+', '+IsNull(dbo.ufn_GetLocationName(S.DepLocation),'')+Case When S.ArrLocation is Null then '' else '/' end+ IsNull(dbo.ufn_GetLocationName(S.ArrLocation),'')
                                       End
                               From ResService S (NOLOCK) Where RecID=@ServiceID

                             Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                                 Values(1,'N',@Unit,@Service,@ServiceDesc,@SalePrice)


                            Fetch Next FROM Cursor1 INTO @Unit,@SalePrice,@Service,/*@ServiceDesc,*/ @BegDate,@ServiceID,@ExtServiceID
                           End
                           
                           Close Cursor1
                           DEALLOCATE Cursor1

                        -- Agency Service Fee (ayrı satırda per service olarak yazılacak)
                         Set @IncPack='N'
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                           Select 5,'N',Unit=S.Unit,'',Name=IsNull(dbo.FindLocalName(A.NameLID,@Market),A.Name),S.SalePrice
                           From ResService S (NOLOCK)
                           Join AdService A (NOLOCK) on A.Service=S.ServiceType and A.Code=S.Service and A.SerType=1
                           Where S.ResNo=@ResNo and S.IncPack=@IncPack and S.SalePrice>0 and S.StatSer in (0,1)

                        -- PEB
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                          Select 4,'N',Unit=Count(TitleGrp),'',@PasEBStr,PasEB=ABS(PasEB)*-1
                          From 
                          (Select TitleGrp,PasEB=Sum(IsNull(ppPasEB,0)) From ResCust C (NOLOCK)
                           Outer Apply(Select TitleGrp=Case When TitleNo<=5 then 0 When TitleNo between 6 and 7 then 1 When TitleNo>=8 then 2 end From Title (NOLOCK) Where TitleNo=C.Title) T
                           Where C.ResNo=@ResNo
                           Group by TitleGrp,CustNo
                          ) P
                          Where PasEB<>0
                          Group By TitleGrp,PasEB
                          Order by TitleGrp

                        -- Supplement / Discount
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                           Select 2,'N',1,RSD.SupDis,Name=IsNull(dbo.FindLocalName(SD.NameLID,@Market),Name),CalcAmount 
                           From ResSupDis RSD
                           Join SupDis SD (NOLOCK) on SD.Code=RSD.SupDis
                           Join ResMain R (NOLOCK) on R.ResNo=RSD.ResNo and R.ResStat in (0,1) --09.04.2011'de eklendi
                           Where RSD.ResNo=@ResNo and RSD.ApplyType='P' and RSD.Ok='Y'

                        -- Promotions
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                           Select 3,'N',1,RP.PromoID,Name=IsNull(dbo.FindLocalName(P.NameLID,@Market),Description),Amount=Abs(Amount)*-1
                           From ResPromo RP
                           Join Promo P (NOLOCK) on P.RecID=RP.PromoID
                           Where ResNo=@ResNo and RP.Confirmed=1

                        --SSR Code (Free) 08.09.2011 added
                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                           Select 8,'N',Unit=Count(*),SpecSerRQCode1,Name,0
                           From
                           (
                           Select C.CustNo,C.SpecSerRQCode1,Name=IsNull(dbo.FindLocalName(S.NameLID,@Market),S.Name)
                           From ResCon C (NOLOCK)
                           Join SpecSerRQCode S (NOLOCK) on S.Code=C.SpecSerRQCode1
                           Where ResNo=@ResNo
                           and IsNull(SpecSerRQCode1,'')<>''
                           and not Exists(Select * from ServiceExt Where SpecSerRQCode=SpecSerRQCode1)
                           ) x
                           Group by SpecSerRQCode1,Name

                         Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
                           Select 8,'N',Unit=Count(*),SpecSerRQCode2,Name,0
                           From
                           (
                           Select C.CustNo,C.SpecSerRQCode2,Name=IsNull(dbo.FindLocalName(S.NameLID,@Market),S.Name)
                           From ResCon C (NOLOCK)
                           Join SpecSerRQCode S (NOLOCK) on S.Code=C.SpecSerRQCode2
                           Where ResNo=@ResNo
                           and IsNull(SpecSerRQCode2,'')<>''
                           and not Exists(Select * from ServiceExt Where SpecSerRQCode=SpecSerRQCode2)
                           ) x
                           Group by SpecSerRQCode2,Name

                        --Zero Invoice Amount (16.03.2011 Added)
                        if @ZeroInv=1 
                        Begin
                         Update #ResMain Set PasPayable=0
                         Update #ServicePrices Set SalePrice=0

                         Update #PayPlan Set RemAmount_L1=0,RemAmount_L2=0,RemAmount_L3=0

                         if not Exists(Select * from #PayPlan)
                           Insert Into #PayPlan (DueDate_L1,RemAmount_L1) Values(Null,Null) --Insert Empty line
                        end

                        if @ResStat=3
                        begin
                         Delete From #ServicePrices Where Service<>'CANX'
                        end

                        if (@ResStat=3) or (@ResStat<=1 and @PasBalance<0)
                        Begin
                         Delete From #ResPayPlan Where RecID>1
                         Update #ResPayPlan Set DueDate=Null, Amount=@PasBalance, PayAmount=Null

                         Update #PayPlan
                         Set DueDate_L1=Null, RemAmount_L1=@PasBalance,
                             DueDate_L2=Null, RemAmount_L2=Null,DueDate_L3=Null, RemAmount_L3=Null
                        End

                        Set NOCOUNT OFF

                        --Results
                        Select * from #ServicePrices
                        ";
            #endregion SQL string

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        PasPriceRecord record = new PasPriceRecord();
                        //RecID, RecType, IncPack, Unit, Service, ServiceDesc, SalePrice
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.RecType = Conversion.getInt16OrNull(R["RecType"]);
                        record.IncPack = Conversion.getStrOrNull(R["IncPack"]);
                        record.Unit = Conversion.getInt16OrNull(R["Unit"]);
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.ServiceDesc = Conversion.getStrOrNull(R["ServiceDesc"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public PaxCountRecord getPaxCount(string ResNo, ref string errorMsg)
        {
            string tsql = @"Select AdlCnt=Sum(Case When TitleType=0 then 1 else 0 end),
                                   ChdCnt=Sum(Case When TitleType=1 then 1 else 0 end),
                                   InfCnt=Sum(Case When TitleType=2 then 1 else 0 end)
                            From 
                            (
                            Select TitleType=Case When Title in (8,9) then 2 When Title in (6,7) then 1 Else 0 end 
                            From ResCust
                            Where ResNo=@ResNo     
                            ) BWF
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        PaxCountRecord record = new PaxCountRecord();
                        return new PaxCountRecord
                        {
                            AdlCnt = Conversion.getInt16OrNull(R["AdlCnt"]),
                            ChdCnt = Conversion.getInt16OrNull(R["ChdCnt"]),
                            InfCnt = Conversion.getInt16OrNull(R["InfCnt"])
                        };
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getAccomStr(string ResNo, ref string errorMsg)
        {
            string tsql = @"Declare @s varchar(8000)
                            Select @s=COALESCE(@s+', ','')+LTrim(Str(Unit))+'x '+Accom from
                            (
                            Select Accom=HR.Name+' '+Accom,Unit=Count(*)
                            from ResService S 
                            Join HotelRoom HR on HR.Hotel=S.Service and HR.Code=S.Room
                            Where ResNo=@ResNo and ServiceType='HOTEL' and StatSer in (0,1)
                            Group by HR.Name+' '+Accom 
                            ) x
                            Select Acooms=@s
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getTransferStr(string ResNo, ref string errorMsg)
        {
            string tsql = @"Declare @s varchar(8000)
                            Select @s=COALESCE(@s+' / ','')+Name From 
                            (
                            Select Distinct T.Name,Direction From ResService (NOLOCK) S 
                            Join Transfer (NOLOCK) T on T.Code=S.Service 
                            Where ResNo=@ResNo and StatSer in (0,1) and ServiceType='TRANSFER'
                            ) X
                            Order by Direction Desc
                            Select TrfNames=@s
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string createInvoice(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Invoice.pdf";
            if (!File.Exists(fileName)) return "";

            string tmpAFileName = ResNo + "_Invoice_" + System.Guid.NewGuid() + ".pdf";
            string afileName = tempFolder + tmpAFileName;            

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                {
                    if (row.RightToLeft)
                        TvReport.TvReportCommon.writeTextRightAlign(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);
                    else TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);
                }
            cb.ClosePath();
            doc.Close();
            reader.Close();
            return tmpAFileName;
        }

        public string createInvoiceClient(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "InvoiceClient.pdf";
            if (!File.Exists(fileName)) return "";

            string tmpAFileName = ResNo + "_InvoiceClient_" + System.Guid.NewGuid() + ".pdf";
            string afileName = tempFolder + tmpAFileName;            

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                {
                    if (row.RightToLeft)
                        TvReport.TvReportCommon.writeTextRightAlign(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);
                    else TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);
                }
            cb.ClosePath();
            doc.Close();
            reader.Close();
            return tmpAFileName;
        }

        public ETicketHotelRecord getETicketHotelDestination(string ResNo, ref string errorMsg)
        {
            string tsql = @"Select Top 1 H.Name,Location=dbo.ufn_GetLocationName(H.Location),S.BegDate,S.EndDate
                            From ResService S
                            Join Hotel H on H.Code=S.Service
                            Where ResNo=@ResNo
                            And ServiceType='HOTEL'
                            And StatSer in (0,1)
                            Order by BegDate ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {                        
                        return new ETicketHotelRecord
                        {
                            Name = Conversion.getStrOrNull(R["Name"]),
                            Location = Conversion.getStrOrNull(R["Location"]),
                            BegDate = Conversion.getDateTimeOrNull(R["BegDate"]),
                            EndDate = Conversion.getDateTimeOrNull(R["EndDate"])
                        };
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string createFlyTicket(string ResNo, string pdfFilePath, string tempFolder, string data, string _fileName, ref string errorMsg)
        {
            string fileName = pdfFilePath + _fileName + ".pdf";
            string fileName2 = pdfFilePath + "ETicketBack" + ".pdf";
            if (!File.Exists(fileName)) return "";
            string tmpAFileName = ResNo + "_" + _fileName + "_" + System.Guid.NewGuid() + ".pdf";
            string afileName = tempFolder + tmpAFileName;            

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.CP1252, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
            {
                if (row.isShown)
                {
                    if (row.Type == writeType.Html)
                        TvReport.TvReportCommon.writeHtml(ref doc, ref cb, row, fontArial);
                    else TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);                    
                }
            }
            cb.ClosePath();
            if (File.Exists(fileName2))
            {
                PdfReader reader2 = new PdfReader(fileName2);
                for (int i = 0; i < reader2.NumberOfPages; i++)
                {
                    byte[] pdfBytes2 = reader2.GetPageContent(i + 1);
                    doc.NewPage();
                    page = pdfWriter.GetImportedPage(reader2, i + 1);
                    PdfContentByte cb2 = pdfWriter.DirectContent;
                    cb2.AddTemplate(page, 0, 0);
                    cb2.ClosePath();
                }
                reader2.Close();
            }
            doc.Close();
            reader.Close();
            return tmpAFileName; 
        }

        public string mergeFlyTicket(string tempFolder, List<string> ticketList)
        {
            string aFileName = ticketList[0].ToString().Substring(0, ticketList[0].ToString().IndexOf('_')) + System.Guid.NewGuid() + "_ETicket.pdf";
            if (File.Exists(tempFolder + aFileName))
                File.Delete(tempFolder + aFileName);
            string fileName = ticketList.FirstOrDefault().ToString().Substring(0, ticketList[0].ToString().IndexOf('_'));
            int currentpage = 1;
            Document doc = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(tempFolder + aFileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            for (int i = 0; i < ticketList.Count; i++)
            {
                PdfReader reader2 = new PdfReader(tempFolder + ticketList[i].ToString());
                byte[] pdfBytes2 = reader2.GetPageContent(currentpage);
                doc.NewPage();
                PdfImportedPage page = pdfWriter.GetImportedPage(reader2, currentpage);

                PdfContentByte cb2 = pdfWriter.DirectContent;
                cb2.AddTemplate(page, 0, 0);
                cb2.ClosePath();
                reader2.Close();
                //File.Delete(tempFolder + ticketList[i].ToString());
            }
            doc.Close();

            return aFileName;
        }

        public string createVoucher(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Voucher.pdf";
            if (!File.Exists(fileName)) return "";
            string tmpAFileName = ResNo + "_" + System.Guid.NewGuid() + "_Voucher.pdf";
            string afileName = tempFolder + tmpAFileName;

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();
            doc.Close();
            reader.Close();
            return tmpAFileName;
        }

        public decimal? getAvanceAmount(string ResNo, ref string errorMsg)
        {
            string tsql = @"
                            Select AvanceAmount=Sum(isnull(PaidAmount,0)) 
                            From JournalCDet (NOLOCK)
                            Where ResNo=@ResNo And Status='Y'
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                return Conversion.getDecimalOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.RezedaReport.Payments> getPayments(string ResNo, ref string errorMsg)
        {
            List<TvReport.RezedaReport.Payments> records = new List<TvReport.RezedaReport.Payments>();
            string tsql = @"
                            Select ReceiptNo=J.ReceiptSerial+' '+Cast(J.ReceiptNo as varchar(8)), J.ReceiptDate,
                              InvoiceNo=J.InvoiceSerial+' '+Cast(J.InvoiceNo as varchar(8)),
                              J.PayDate,J.PayType,J.Locked,JC.PaidAmount,JC.PaidCur,J.Reference,
                              InstalNum,CCNo,PayTypeName=PT.Name
                            From JournalCDet JC (NOLOCK)
                            Join Journal J (NOLOCK) on J.RecID=JC.JournalID      
                            Join PayType PT (NOLOCK) on PT.Market=J.Market and PT.Code=J.PayType
                            Where ResNo=@ResNo And
                              CP='C' And 
                              J.Status='Y' And 
                              JC.Status='Y'
                            Order by J.PayDate
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(new TvReport.RezedaReport.Payments
                        {                            
                            ReceiptNo = Conversion.getStrOrNull(R["ReceiptNo"]),
                            ReceiptDate = Conversion.getDateTimeOrNull(R["ReceiptDate"]),
                            InvoiceNo = Conversion.getStrOrNull(R["InvoiceNo"]),
                            PayDate = Conversion.getDateTimeOrNull(R["PayDate"]),
                            PayType = Conversion.getStrOrNull(R["PayType"]),
                            PayTypeName = Conversion.getStrOrNull(R["PayTypeName"]),
                            Locked = Conversion.getStrOrNull(R["Locked"]),
                            PaidAmount = Conversion.getDecimalOrNull(R["PaidAmount"]),
                            PaidCur = Conversion.getStrOrNull(R["PaidCur"]),
                            Reference = Conversion.getStrOrNull(R["Reference"]),
                            InstalNum = Conversion.getInt32OrNull(R["InstalNum"]),
                            CCNo = Conversion.getStrOrNull(R["CCNo"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.RezedaReport.rezedaResService> getResServices(string Market, string ResNo, ref string errorMsg)
        {
            List<TvReport.RezedaReport.rezedaResService> records = new List<TvReport.RezedaReport.rezedaResService>();
            string tsql = @"
                            Select * From
                            (
                              Select ServiceType, Service, BegDate,
                                  AccomFullName= '',
                                  AirLineName=Case When ServiceType='FLIGHT' then 
                                                     (Select Top 1 A.Name From FlightDay F
                                                     Join Airline A on A.Code=F.AirLine       
                                                     Where F.FlightNo=Service and F.FlyDate=BegDate)
                                              Else '' end,          
                                  ServiceDesc=
                                   Case ServiceType      
                                     When 'FLIGHT' then (Select Top 1 FlightNo+' ('+DepAirport+'->'+
							                               ArrAirport+'),'+FlgClass+', '+Convert(varchar(8),FlyDate,3) From
							                               FlightDay (NOLOCK) Where FlightNo=Service and FlyDate=ResService.BegDate)      
                                     When 'TRANSPORT' then (Select isnull(dbo.FindLocalName(NameLID,@Market),TP.Name)+' ('+dbo.ufn_GetLocationName(TP.Departure)+'-'+dbo.ufn_GetLocationName(TP.Arrival)+') ' 
								                            From Transport TP (NOLOCK)
                                                           Where TP.Code=ResService.Service)            
                                     When 'RENTING' then (Select isnull(dbo.FindLocalName(NameLID,@Market),Name)+' (Category:'+Category+') / '+(Select isnull(dbo.FindLocalName(NameLID,@Market),Name) 
							                                From Location (NOLOCK) 
								                            Where RecId=DepLocation)+' '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3)  From Renting (NOLOCK) Where Code=Service)      
                                     When 'EXCURSION' then (Select isnull(dbo.FindLocalName(NameLID,@Market),Name)+' / '+(Select isnull(dbo.FindLocalName(NameLID,@Market),Name) 
								                            From Location (NOLOCK) 
								                            Where RecId=DepLocation) From Excursion (NOLOCK) Where Code=Service)      
                                     When 'INSURANCE' then (Select isnull(dbo.FindLocalName(NameLID,@Market),Name)+', '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3) 
								                            From Insurance (NOLOCK) 
								                            Where Code=Service)      
                                     When 'VISA' then (Select isnull(dbo.FindLocalName(NameLID,@Market),Name)+ Case when VisaType<>'' then ' ('+VisaType+')' else '' end++', '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3) 
								                            From Visa (NOLOCK) 
								                            Where Code=Service)      
                                     When 'TRANSFER' then (Select isnull(dbo.FindLocalName(NameLID,@Market),Name)+Case when TrfType<>'' then ' ('+TrfType+')' else '' end+', '+Convert(varchar(8),BegDate,3) 
								                            From Transfer (NOLOCK) 
								                            Where Code=Service)      
                                     When 'HANDFEE' then (Select isnull(dbo.FindLocalName(NameLID,@Market),Name)+', '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3) 
								                            From HandFee (NOLOCK) 
								                            Where Code=Service)      
                                     else (Select isnull(dbo.FindLocalName(NameLID,@Market),Name) From AdService (NOLOCK) Where AdService.Service=ResService.ServiceType and Code=ResService.Service)+', '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3)end,
					                            Unit=Sum(Unit),Adult=Sum(Adult),Child=Sum(Child)
	                            From ResService (NOLOCK)
	                            Where ServiceType<>'HOTEL' and ResNo=@ResNo and StatSer<2      
	                            Group by ServiceType, Service, BegDate, EndDate, FlgClass, Supplier, DepLocation
	                            Union All
                                Select ServiceType, Service, BegDate,      
                                    AccomFullName= case when Adult>0 then Str(Adult,1)+' Adl' else '' end +      
                                                   case when Child>0 then '+'+Str(Child,1)+' Chd' else '' end,
                                    AirLineName='',      
                                    ServiceDesc=(Select isnull(dbo.FindLocalName(NameLID,@Market),Name)+' ('+Category+'),'+
						                            (Select isnull(dbo.FindLocalName(NameLID,@Market),Name) From Location (NOLOCK) Where RecID=Hotel.Location)+
						                             ' ('+Room+','+Accom+','+Board+') '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3) 
					                             From Hotel (NOLOCK) 
					                             Where Code=Service),
		                            Unit=Unit,Adult,Child
	                            From ResService (NOLOCK)
                                Where ServiceType='HOTEL' and ResNo=@ResNo and StatSer<2
                            ) X
                            Order by BegDate
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(new TvReport.RezedaReport.rezedaResService
                        {
                            AccomFullName = Conversion.getStrOrNull(R["AccomFullName"]),
                            AirLineName = Conversion.getStrOrNull(R["AirLineName"]),
                            Service = Conversion.getStrOrNull(R["Service"]),
                            ServiceDesc = Conversion.getStrOrNull(R["ServiceDesc"]),
                            ServiceType = Conversion.getStrOrNull(R["ServiceType"]),
                            BegDate = Conversion.getDateTimeOrNull(R["BegDate"]),
                            Unit = Conversion.getInt32OrNull(R["Unit"]),
                            Adult = Conversion.getInt32OrNull(R["Adult"]),                            
                            Child = Conversion.getInt32OrNull(R["Child"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.RezedaReport.rezedaResServiceExt> getResServiceExt(string Market, string ResNo, ref string errorMsg)
        {
            List<TvReport.RezedaReport.rezedaResServiceExt> records = new List<TvReport.RezedaReport.rezedaResServiceExt>();
            string tsql = @"
                            Select ServiceType, ExtService, BegDate, EndDate, Unit = Sum(Unit), Adult = Sum(Adult), Child = Sum(Child),      
                               ServiceDesc=(Select isnull(dbo.FindLocalName(NameLID,@Market),Name)+', '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3) 
				                            From ServiceExt With (NOLOCK) Where Code=ExtService)
                            From ResServiceExt (NOLOCK)
                            Where ResNo=@ResNo 
                            Group By ServiceType, ExtService, BegDate, EndDate
                            Order by BegDate
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        records.Add(new TvReport.RezedaReport.rezedaResServiceExt
                        {                            
                            ExtService = Conversion.getStrOrNull(R["Service"]),
                            ServiceDesc = Conversion.getStrOrNull(R["ServiceDesc"]),
                            ServiceType = Conversion.getStrOrNull(R["ServiceType"]),
                            BegDate = Conversion.getDateTimeOrNull(R["BegDate"]),
                            EndDate = Conversion.getDateTimeOrNull(R["EndDate"]),
                            Unit = Conversion.getInt32OrNull(R["Unit"]),
                            Adult = Conversion.getInt32OrNull(R["Adult"]),
                            Child = Conversion.getInt32OrNull(R["Child"])
                        });
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }    
}
