﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvReport.QuaReport
{    
    public class ResCustInfoRecord
    {
        public ResCustInfoRecord()
        {
            _InvoiceAddr = "H";
            _ContactAddr = "H";
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _CTitle;
        public Int16? CTitle
        {
            get { return _CTitle; }
            set { _CTitle = value; }
        }

        string _CTitleName;
        public string CTitleName
        {
            get { return _CTitleName; }
            set { _CTitleName = value; }
        }

        string _CName;
        public string CName
        {
            get { return _CName; }
            set { _CName = value; }
        }

        string _CSurName;
        public string CSurName
        {
            get { return _CSurName; }
            set { _CSurName = value; }
        }

        string _AddrHome;
        public string AddrHome
        {
            get { return _AddrHome; }
            set { _AddrHome = value; }
        }

        string _AddrHomeCity;
        public string AddrHomeCity
        {
            get { return _AddrHomeCity; }
            set { _AddrHomeCity = value; }
        }

        string _AddrHomeZip;
        public string AddrHomeZip
        {
            get { return _AddrHomeZip; }
            set { _AddrHomeZip = value; }
        }

        string _AddrHomeTel;
        public string AddrHomeTel
        {
            get { return _AddrHomeTel; }
            set { _AddrHomeTel = value; }
        }

        string _AddrHomeFax;
        public string AddrHomeFax
        {
            get { return _AddrHomeFax; }
            set { _AddrHomeFax = value; }
        }

        string _AddrHomeEmail;
        public string AddrHomeEmail
        {
            get { return _AddrHomeEmail; }
            set { _AddrHomeEmail = value; }
        }

        string _AddrHomeCountry;
        public string AddrHomeCountry
        {
            get { return _AddrHomeCountry; }
            set { _AddrHomeCountry = value; }
        }

        string _AddrHomeCountryCode;
        public string AddrHomeCountryCode
        {
            get { return _AddrHomeCountryCode; }
            set { _AddrHomeCountryCode = value; }
        }

        string _HomeTaxOffice;
        public string HomeTaxOffice
        {
            get { return _HomeTaxOffice; }
            set { _HomeTaxOffice = value; }
        }

        string _HomeTaxAccNo;
        public string HomeTaxAccNo
        {
            get { return _HomeTaxAccNo; }
            set { _HomeTaxAccNo = value; }
        }

        string _AddrWork;
        public string AddrWork
        {
            get { return _AddrWork; }
            set { _AddrWork = value; }
        }

        string _AddrWorkCity;
        public string AddrWorkCity
        {
            get { return _AddrWorkCity; }
            set { _AddrWorkCity = value; }
        }

        string _AddrWorkZip;
        public string AddrWorkZip
        {
            get { return _AddrWorkZip; }
            set { _AddrWorkZip = value; }
        }

        string _AddrWorkTel;
        public string AddrWorkTel
        {
            get { return _AddrWorkTel; }
            set { _AddrWorkTel = value; }
        }

        string _AddrWorkFax;
        public string AddrWorkFax
        {
            get { return _AddrWorkFax; }
            set { _AddrWorkFax = value; }
        }

        string _AddrWorkEMail;
        public string AddrWorkEMail
        {
            get { return _AddrWorkEMail; }
            set { _AddrWorkEMail = value; }
        }

        string _AddrWorkCountry;
        public string AddrWorkCountry
        {
            get { return _AddrWorkCountry; }
            set { _AddrWorkCountry = value; }
        }

        string _AddrWorkCountryCode;
        public string AddrWorkCountryCode
        {
            get { return _AddrWorkCountryCode; }
            set { _AddrWorkCountryCode = value; }
        }

        string _WorkTaxOffice;
        public string WorkTaxOffice
        {
            get { return _WorkTaxOffice; }
            set { _WorkTaxOffice = value; }
        }

        string _WorkTaxAccNo;
        public string WorkTaxAccNo
        {
            get { return _WorkTaxAccNo; }
            set { _WorkTaxAccNo = value; }
        }

        string _ContactAddr;
        public string ContactAddr
        {
            get { return _ContactAddr; }
            set { _ContactAddr = value; }
        }

        string _MobTel;
        public string MobTel
        {
            get { return _MobTel; }
            set { _MobTel = value; }
        }

        string _Jobs;
        public string Jobs
        {
            get { return _Jobs; }
            set { _Jobs = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        string _InvoiceAddr;
        public string InvoiceAddr
        {
            get { return _InvoiceAddr; }
            set { _InvoiceAddr = value; }
        }

        string _WorkFirmName;
        public string WorkFirmName
        {
            get { return _WorkFirmName; }
            set { _WorkFirmName = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankAccNo;
        public string BankAccNo
        {
            get { return _BankAccNo; }
            set { _BankAccNo = value; }
        }

        string _BankIBAN;
        public string BankIBAN
        {
            get { return _BankIBAN; }
            set { _BankIBAN = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }
    }
    
}
