﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Web;
using TvReport.SunReport;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using TvTools;

namespace TvReport.AztReport
{
    public class AztReport
    {
        public List<ResCustInsuranceRecord> getInsuranceCustomers(string Market, string ResNo, ref string errorMsg)
        {
            List<ResCustInsuranceRecord> records = new List<ResCustInsuranceRecord>();
            string tsql = @"if OBJECT_ID('TempDB.dbo.#CancelInsBaseAmount') is not null Drop Table dbo.#CancelInsBaseAmount
                            CREATE TABLE dbo.#CancelInsBaseAmount
                            (
                              CustNo int,
                              SaleCur varchar(5),
                              SalePrice dec(18,2)
                            )

                            Declare @CanInsIncStd bit

                            Select @Market = Market From ResMain (NOLOCK) where ResNo = @ResNo

                            Select @CanInsIncStd=pp_CanInsIncStd From dbo.ufn_GetMarketParams(@Market,'pp')

                            Insert Into #CancelInsBaseAmount (CustNo, SaleCur, SalePrice)
                            Select C.CustNo, P.SaleCur,SalePrice=IsNull(P.SalePrice,0)  
                            From ResCon C (NOLOCK)
                            Join ResService S (NOLOCK) ON S.ResNo=C.ResNo And S.RecID=C.ServiceID
                            Join ResCustPrice P (NOLOCK) ON P.ResNo=C.ResNo And P.ServiceID=C.ServiceID And P.CustNo=C.CustNo
                            Where P.ResNo=@ResNo And 
                                 (
	                              (S.ServiceType<>'INSURANCE' And 
	                               (S.ServiceType<>'VISA' Or (S.ServiceType='VISA' And 
								                              isnull((Select IncVisa From Insurance I (NOLOCK)
                                                                      Where Exists(Select SS.Service 
                                                                                   From ResService SS 
                                                                                   Join ResCon CC on CC.ResNo=SS.ResNo And CC.CustNo=P.CustNo
                                                                                   Where SS.ResNo=S.ResNo And 
													 		                             SS.ServiceType='INSURANCE' And 
															                             Exists(Select InsType From Insurance where Code = SS.Service And InsType=1) And
															                             I.Code=SS.Service) And
                                                                            InsType = 1), 0) = 1))) or 
                                    ( S.ServiceType='INSURANCE' And 
                                      (Select InsType From Insurance (NOLOCK) Where Code = S.Service)=0) And 
                                      isnull(@CanInsIncStd, 0) = 1) And
                                    exists(Select Top 1 SS.Service 
                                           From ResService SS 
                                           join ResCon CC on CC.ResNo = SS.ResNo and CC.CustNo = P.CustNo
                                           where SS.ResNo = S.ResNo and SS.ServiceType = 'INSURANCE' 
                                            and (Select InsType From Insurance where Code = SS.Service) = 1)


                            Select R.ResNo, R.CustNo, DocStat=Cast(isnull(R.DocStat, 0) As smallint), ServiceID, I.InsType
                            From ResCon R (NOLOCK)
                            Join ResCust C (NOLOCK) on C.CustNo=R.CustNo
                            Join ResService RS (NOLOCK) on R.ServiceID = RS.RecID
                            Join Insurance I (NOLOCK) on I.Code = RS.Service --And I.InsType = 1
                            Outer Apply
                            (Select SaleCur,SalePrice= IsNull(Sum( IsNull(SalePrice,0)),0) From #CancelInsBaseAmount
                                Where CustNo=R.CustNo
                                Group by CustNo,SaleCur
                            ) P	
                            Where R.ResNo=@ResNo   
                            Order by R.UnitNo, C.SeqNo";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ResCustInsuranceRecord record = new ResCustInsuranceRecord();

                        record.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        record.CustNo = Conversion.getInt32OrNull(R["CustNo"]);
                        record.DocStat = Conversion.getInt16OrNull(R["DocStat"]);
                        record.ServiceID = Conversion.getInt32OrNull(R["ServiceID"]);
                        record.InsType = Conversion.getInt16OrNull(R["InsType"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ResCustInfoRecord getResCustInfoRecord(int? CustNo, ref string errorMsg)
        {
            string tsql = @"Select RecID, CustNo, CTitle, CTitleName=(Select Code From Title (NOLOCK) Where TitleNo=ResCustInfo.CTitle), CName, CSurName, 
	                            AddrHome, AddrHomeCity, AddrHomeZip, AddrHomeTel, AddrHomeFax, AddrHomeEmail, AddrHomeCountry, AddrHomeCountryCode, HomeTaxOffice, HomeTaxAccNo,
	                            AddrWork, AddrWorkCity, AddrWorkZip, AddrWorkTel, AddrWorkFax, AddrWorkEmail, AddrWorkCountry, AddrWorkCountryCode, WorkTaxOffice, WorkTaxAccNo,
	                            ContactAddr, MobTel, Jobs, Note,  InvoiceAddr, WorkFirmName, Bank, BankAccNo, BankIBAN
                            From ResCustInfo (NOLOCK)
                            Where CustNo = @CustNo   ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "CustNo", DbType.Int32, CustNo.HasValue ? CustNo.Value : -1);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        ResCustInfoRecord record = new ResCustInfoRecord();
                        record.RecID = (int)rdr["RecID"];
                        record.CustNo = (int)rdr["CustNo"];
                        record.CTitle = Conversion.getInt16OrNull(rdr["CTitle"]);
                        record.CTitleName = Conversion.getStrOrNull(rdr["CTitleName"]);
                        record.CName = Conversion.getStrOrNull(rdr["CName"]);
                        record.CSurName = Conversion.getStrOrNull(rdr["CSurName"]);
                        record.AddrHome = Conversion.getStrOrNull(rdr["AddrHome"]);
                        record.AddrHomeCity = Conversion.getStrOrNull(rdr["AddrHomeCity"]);
                        record.AddrHomeZip = Conversion.getStrOrNull(rdr["AddrHomeZip"]);
                        record.AddrHomeTel = Conversion.getStrOrNull(rdr["AddrHomeTel"]);
                        record.AddrHomeFax = Conversion.getStrOrNull(rdr["AddrHomeFax"]);
                        record.AddrHomeEmail = Conversion.getStrOrNull(rdr["AddrHomeEmail"]);
                        record.AddrHomeCountry = Conversion.getStrOrNull(rdr["AddrHomeCountry"]);
                        record.AddrHomeCountryCode = Conversion.getStrOrNull(rdr["AddrHomeCountryCode"]);
                        record.HomeTaxOffice = Conversion.getStrOrNull(rdr["HomeTaxOffice"]);
                        record.HomeTaxAccNo = Conversion.getStrOrNull(rdr["HomeTaxAccNo"]);
                        record.AddrWork = Conversion.getStrOrNull(rdr["AddrWork"]);
                        record.AddrWorkCity = Conversion.getStrOrNull(rdr["AddrWorkCity"]);
                        record.AddrWorkZip = Conversion.getStrOrNull(rdr["AddrWorkZip"]);
                        record.AddrWorkTel = Conversion.getStrOrNull(rdr["AddrWorkTel"]);
                        record.AddrWorkFax = Conversion.getStrOrNull(rdr["AddrWorkFax"]);
                        record.AddrWorkEMail = Conversion.getStrOrNull(rdr["AddrWorkEmail"]);
                        record.AddrWorkCountry = Conversion.getStrOrNull(rdr["AddrWorkCountry"]);
                        record.AddrWorkCountryCode = Conversion.getStrOrNull(rdr["AddrWorkCountryCode"]);
                        record.WorkTaxOffice = Conversion.getStrOrNull(rdr["WorkTaxOffice"]);
                        record.WorkTaxAccNo = Conversion.getStrOrNull(rdr["WorkTaxAccNo"]);
                        record.ContactAddr = Conversion.getStrOrNull(rdr["ContactAddr"]);
                        record.MobTel = Conversion.getStrOrNull(rdr["MobTel"]);
                        record.Jobs = Conversion.getStrOrNull(rdr["Jobs"]);
                        record.Note = Conversion.getStrOrNull(rdr["Note"]);
                        record.InvoiceAddr = Conversion.getStrOrNull(rdr["InvoiceAddr"]);
                        record.WorkFirmName = Conversion.getStrOrNull(rdr["WorkFirmName"]);
                        record.Bank = Conversion.getStrOrNull(rdr["Bank"]);
                        record.BankAccNo = Conversion.getStrOrNull(rdr["BankAccNo"]);
                        record.BankIBAN = Conversion.getStrOrNull(rdr["BankIBAN"]);
                        return record;
                    }
                    else return null;
                }

            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CustInsuranceRecord> getcustInsurance(string ResNo, int? ServiceID, string CustNo, ref string errorMsg)
        {
            List<CustInsuranceRecord> records = new List<CustInsuranceRecord>();
            string tsql = @"Select TitleName=(Select Code From Title (NOLOCK) Where TitleNo = RC.Title),
                                SurName, Name, SurnameL, NameL, Birtday, DocInsur, PassSerie,PassNo,NetCur,SalePrice,
                                NationCode=(Select Code From Location (NOLOCK) Where RecID=RC.Nation)
                            From ResCust RC (NOLOCK)
                            Join ResCon RCon (NOLOCK) ON RCon.CustNo = RC.CustNo
                            Join ResService RS (NOLOCK) ON RS.RecID = RCon.ServiceID
                            Where RC.ResNo = @ResNo 
                              And RS.RecID = @ServiceID ";
            tsql += string.Format("  And RC.CustNo in ({0}) ", CustNo);

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, ServiceID);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        CustInsuranceRecord record = new CustInsuranceRecord();
                        record.TitleName = Conversion.getStrOrNull(reader["TitleName"]);
                        record.Surname = Conversion.getStrOrNull(reader["Surname"]);
                        record.Name = Conversion.getStrOrNull(reader["Name"]);
                        record.SurnameL = Conversion.getStrOrNull(reader["SurnameL"]);
                        record.NameL = Conversion.getStrOrNull(reader["NameL"]);
                        record.Birthday = Conversion.getDateTimeOrNull(reader["Birtday"]);
                        record.DocInsur = Conversion.getByteOrNull(reader["DocInsur"]);
                        record.PassSerie = Conversion.getStrOrNull(reader["PassSerie"]);
                        record.PassNo = Conversion.getStrOrNull(reader["PassNo"]);
                        record.NetCur = Conversion.getStrOrNull(reader["NetCur"]);
                        record.SalePrice = Conversion.getDecimalOrNull(reader["SalePrice"]);
                        record.NationCode = Conversion.getStrOrNull(reader["NationCode"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public InsuranceRptRecord getInsurance(string Market, string ResNo, int? ServiceID, ref string errorMsg)
        {
            string tsql = string.Empty;

            tsql = @"   Select RS.Service, RS.Deplocation, LocationName=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
	                        RS.BegDate,  RS.EndDate, RS.Duration,
	                        InsuranceName = isnull(dbo.FindLocalName(I.NameLID, @Market), I.Name),
	                        InsZone, INameS=I.NameS, Coverage, CoverageCur, CoverExplain, IDescription = I.Description, I.Franchise
                        From ResService RS (NOLOCK) 
                        Join Insurance I (NOLOCK) ON I.Code = RS.Service
                        Join Location L (NOLOCK) ON L.RecID = RS.DepLocation
                        Where ResNo = @ResNo And ServiceType='INSURANCE' And RS.RecID = @ServiceID ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, ServiceID);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        InsuranceRptRecord record = new InsuranceRptRecord();
                        record.Service = Conversion.getStrOrNull(reader["Service"]);
                        record.Deplocation = Conversion.getInt32OrNull(reader["Deplocation"]);
                        record.LocationName = Conversion.getStrOrNull(reader["LocationName"]);
                        record.BegDate = Conversion.getDateTimeOrNull(reader["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(reader["EndDate"]);
                        record.Duration = Conversion.getInt16OrNull(reader["Duration"]);
                        record.InsuranceName = Conversion.getStrOrNull(reader["InsuranceName"]);
                        record.InsZone = Conversion.getStrOrNull(reader["InsZone"]);
                        record.INameS = Conversion.getStrOrNull(reader["INameS"]);
                        record.Coverage = Conversion.getDecimalOrNull(reader["Coverage"]);
                        record.CoverageCur = Conversion.getStrOrNull(reader["CoverageCur"]);
                        record.CoverExplain = Conversion.getStrOrNull(reader["CoverExplain"]);
                        record.IDescription = Conversion.getStrOrNull(reader["IDescription"]);
                        record.Franchise = Conversion.getDecimalOrNull(reader["Franchise"]);
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string createContract(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Contract";
            if (!File.Exists(fileName + "A.pdf")) return "";
            if (!File.Exists(fileName + "B.pdf")) return "";            

            string afileName = tempFolder + ResNo + "_Contract.pdf";

            if (File.Exists(afileName))
                File.Delete(afileName);

            PdfReader readerP1 = new PdfReader(fileName + "A.pdf");
            PdfReader readerP2 = new PdfReader(fileName + "B.pdf");            

            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\TIMES.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\TIMESBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            int currentpage = 1;
            byte[] pdfBytes = readerP1.GetPageContent(currentpage);

            Document doc = new Document(readerP1.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = readerP1.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(readerP1, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            foreach (Coordinate row in _data.Where(w => w.PageNo == 1))
                if (row.isShown)
                {
                    if (row.Type == writeType.AreaText)
                        TvReport.TvReportCommon.writeAreaText(ref cb, row.value, row.bold ? fontArialBold : fontArial, row.FontSize, row.x * 2.835f, row.y * 2.835f, row.W * 2.835f, row.H * 2.835f);
                    else
                        if (row.RightToLeft)
                            TvReport.TvReportCommon.writeTextRightAlign(ref cb, row.value, row.x * 2.835f, row.y * 2.835f, row.bold ? fontArialBold : fontArial, row.FontSize);
                        else TvReport.TvReportCommon.writeText(ref cb, row.value, row.x * 2.835f, row.y * 2.835f, row.bold ? fontArialBold : fontArial, row.FontSize);
                }
            cb.ClosePath();
            readerP1.Close();

            pageContent = readerP2.GetPageContent(currentpage);
            doc.NewPage();
            page = pdfWriter.GetImportedPage(readerP2, currentpage);
            cb.AddTemplate(page, 0, 0);
            foreach (Coordinate row in _data.Where(w => w.PageNo == 2))
                if (row.isShown)
                {
                    if (row.Type == writeType.AreaText)
                        TvReport.TvReportCommon.writeAreaText(ref cb, row.value, row.bold ? fontArialBold : fontArial, row.FontSize, row.x * 2.835f, row.y * 2.835f, row.W * 2.835f, row.H * 2.835f);
                    else
                        if (row.RightToLeft)
                            TvReport.TvReportCommon.writeTextRightAlign(ref cb, row.value, row.x * 2.835f, row.y * 2.835f, row.bold ? fontArialBold : fontArial, row.FontSize);
                        else TvReport.TvReportCommon.writeText(ref cb, row.value, row.x * 2.835f, row.y * 2.835f, row.bold ? fontArialBold : fontArial, row.FontSize);
                }
            cb.ClosePath();
            readerP2.Close();
           
            doc.Close();
            return ResNo + "_Contract.pdf";
        }

        public string createFlyTicket(string ResNo, string pdfFilePath, string tempFolder, string data, string _fileName, ref string errorMsg)
        {
            string fileName = pdfFilePath + _fileName + ".pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_" + _fileName + ".pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();
            doc.Close();
            reader.Close();
            return ResNo + "_" + _fileName + ".pdf";
        }

        public string createVoucher(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Voucher.pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_Voucher.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();
            doc.Close();
            reader.Close();
            return ResNo + "_Voucher.pdf";
        }

        public string createInsurance(string ResNo, string pdfFilePath, string tempFolder, string data, string pdfName, ref string errorMsg)
        {
            string fileName = pdfFilePath + pdfName + ".pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_" + pdfName + ".pdf";

            string fileName2 = pdfFilePath + pdfName + "2.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\TIMES.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\TIMESBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();
            if (File.Exists(fileName2))
            {
                PdfReader reader2 = new PdfReader(fileName2);
                for (int i = 0; i < reader2.NumberOfPages; i++)
                {
                    byte[] pdfBytes2 = reader2.GetPageContent(i + 1);
                    doc.NewPage();
                    page = pdfWriter.GetImportedPage(reader2, i + 1);
                    PdfContentByte cb2 = pdfWriter.DirectContent;
                    cb2.AddTemplate(page, 0, 0);
                    cb2.ClosePath();
                }
                reader2.Close();
            }
            doc.Close();
            reader.Close();
            return ResNo + "_" + pdfName + ".pdf";
        }

        public string createInvoice(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "ClientInvoice.pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_Invoice.pdf";

            PdfReader reader = new PdfReader(fileName);
            try
            {
                int currentpage = 1;
                byte[] pdfBytes = reader.GetPageContent(currentpage);
                if (File.Exists(afileName))
                    File.Delete(afileName);
                Document doc = new Document(reader.GetPageSize(1));
                PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
                doc.Open();
                byte[] pageContent = reader.GetPageContent(currentpage);
                doc.NewPage();
                PdfImportedPage page;
                page = pdfWriter.GetImportedPage(reader, currentpage);
                PdfContentByte cb = pdfWriter.DirectContent;

                cb.AddTemplate(page, 0, 0);
                string fontpath = HttpContext.Current.Server.MapPath(".");
                BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);                
                BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);                
                List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

                foreach (Coordinate row in _data)
                    if (row.isShown)
                    {
                        if (row.Type != writeType.Text)
                        {
                            cb.SetLineWidth(row.LineWidth);
                            cb.MoveTo(row.x, row.y);
                            cb.LineTo(row.x + row.W, row.y + row.H);
                            cb.Stroke();
                        }
                        else
                            TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize, row.bold, row.Underline, row.Color, row.Align);
                    }
                cb.ClosePath();
                doc.Close();
                reader.Close();
                return ResNo + "_Invoice.pdf";
            }
            catch
            {
                reader.Close();
                return "";
            }            
        }

        public string mergeFlyTicket(string tempFolder, List<string> ticketList)
        {
            string aFileName = ticketList[0].ToString().Substring(0, ticketList[0].ToString().IndexOf('_')) + "_ETicket.pdf";
            if (File.Exists(tempFolder + aFileName))
                File.Delete(tempFolder + aFileName);
            string fileName = ticketList.FirstOrDefault().ToString().Substring(0, ticketList[0].ToString().IndexOf('_'));
            int currentpage = 1;
            Document doc = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(tempFolder + aFileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            for (int i = 0; i < ticketList.Count; i++)
            {
                PdfReader reader2 = new PdfReader(tempFolder + ticketList[i].ToString());
                byte[] pdfBytes2 = reader2.GetPageContent(currentpage);
                doc.NewPage();
                PdfImportedPage page = pdfWriter.GetImportedPage(reader2, currentpage);

                PdfContentByte cb2 = pdfWriter.DirectContent;
                cb2.AddTemplate(page, 0, 0);
                cb2.ClosePath();
                reader2.Close();
                //File.Delete(tempFolder + ticketList[i].ToString());
            }
            doc.Close();

            return aFileName;
        }
    }
}
