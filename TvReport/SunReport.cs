﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using TvTools;
using System.Web;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Globalization;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace TvReport.SunReport
{
    public class SunReport
    {
        public List<ResCustInsuranceRecord> getInsuranceCustomers(string Market, string ResNo, ref string errorMsg)
        {
            List<ResCustInsuranceRecord> records = new List<ResCustInsuranceRecord>();
            string tsql = @"if OBJECT_ID('TempDB.dbo.#CancelInsBaseAmount') is not null Drop Table dbo.#CancelInsBaseAmount
                            CREATE TABLE dbo.#CancelInsBaseAmount
                            (
                              CustNo int,
                              SaleCur varchar(5),
                              SalePrice dec(18,2)
                            )

                            Declare @CanInsIncStd bit

                            Select @Market = Market From ResMain (NOLOCK) where ResNo = @ResNo

                            Select @CanInsIncStd=pp_CanInsIncStd From dbo.ufn_GetMarketParams(@Market,'pp')

                            Insert Into #CancelInsBaseAmount (CustNo, SaleCur, SalePrice)
                            Select C.CustNo, P.SaleCur,SalePrice=IsNull(P.SalePrice,0)  
                            From ResCon C (NOLOCK)
                            Join ResService S (NOLOCK) ON S.ResNo=C.ResNo And S.RecID=C.ServiceID
                            Join ResCustPrice P (NOLOCK) ON P.ResNo=C.ResNo And P.ServiceID=C.ServiceID And P.CustNo=C.CustNo
                            Where P.ResNo=@ResNo And  
                                                                   
                                 (
	                              (S.ServiceType<>'INSURANCE' And 
	                               (S.ServiceType<>'VISA' Or (S.ServiceType='VISA' And 
								                              isnull((Select IncVisa From Insurance I (NOLOCK)
                                                                      Where Exists(Select SS.Service 
                                                                                   From ResService SS 
                                                                                   Join ResCon CC on CC.ResNo=SS.ResNo And CC.CustNo=P.CustNo
                                                                                   Where SS.ResNo=S.ResNo And 
													 		                             SS.ServiceType='INSURANCE' And 
															                             Exists(Select InsType From Insurance where Code = SS.Service And InsType=1) And
															                             I.Code=SS.Service) And
                                                                            InsType = 1), 0) = 1))) or 
                                    ( S.ServiceType='INSURANCE' And 
                                      (Select InsType From Insurance (NOLOCK) Where Code = S.Service)=0) And 
                                      isnull(@CanInsIncStd, 0) = 1) And
                                    exists(Select Top 1 SS.Service 
                                           From ResService SS 
                                           join ResCon CC on CC.ResNo = SS.ResNo and CC.CustNo = P.CustNo
                                           where SS.ResNo = S.ResNo and SS.ServiceType = 'INSURANCE' 
                                            and (Select InsType From Insurance where Code = SS.Service) = 1)


                            Select R.ResNo, R.CustNo, DocStat=Cast(isnull(R.DocStat, 0) As smallint), ServiceID, I.InsType, P.SalePrice, P.SaleCur
                            From ResCon R (NOLOCK)
                            Join ResCust C (NOLOCK) on C.CustNo=R.CustNo
                            Join ResService RS (NOLOCK) on R.ServiceID = RS.RecID
                            Join Insurance I (NOLOCK) on I.Code = RS.Service --And I.InsType = 1
                            Outer Apply
                            (Select SaleCur,SalePrice= IsNull(Sum( IsNull(SalePrice,0)),0) From #CancelInsBaseAmount
                                Where CustNo=R.CustNo
                                Group by CustNo,SaleCur
                            ) P	
                            Where R.ResNo=@ResNo
                              And RS.StatSer in (0, 1)
                            Order by R.UnitNo, C.SeqNo";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ResCustInsuranceRecord record = new ResCustInsuranceRecord();

                        record.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        record.CustNo = Conversion.getInt32OrNull(R["CustNo"]);
                        record.DocStat = Conversion.getInt16OrNull(R["DocStat"]);
                        record.ServiceID = Conversion.getInt32OrNull(R["ServiceID"]);
                        record.InsType = Conversion.getInt16OrNull(R["InsType"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<CustInsuranceRecord> getcustInsurance(string ResNo, int? ServiceID, string CustNo, ref string errorMsg)
        {
            List<CustInsuranceRecord> records = new List<CustInsuranceRecord>();
            string tsql = @"Select TitleName=(Select Code From Title (NOLOCK) Where TitleNo = RC.Title),
                                SurName, Name, SurnameL, NameL, Birtday, DocInsur, PassSerie, PassNo, NetCur, SalePrice,
                                NationCode=(Select Code From Location (NOLOCK) Where RecID=RC.Nation)
                            From ResCust RC (NOLOCK)
                            Join ResCon RCon (NOLOCK) ON RCon.CustNo = RC.CustNo
                            Join ResService RS (NOLOCK) ON RS.RecID = RCon.ServiceID
                            Where RC.ResNo = @ResNo 
                              And RS.RecID = @ServiceID ";
            tsql += string.Format("  And RC.CustNo in ({0}) ", CustNo);

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, ServiceID);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        CustInsuranceRecord record = new CustInsuranceRecord();
                        record.TitleName = Conversion.getStrOrNull(reader["TitleName"]);
                        record.Surname = Conversion.getStrOrNull(reader["Surname"]);
                        record.Name = Conversion.getStrOrNull(reader["Name"]);
                        record.SurnameL = Conversion.getStrOrNull(reader["SurnameL"]);
                        record.NameL = Conversion.getStrOrNull(reader["NameL"]);
                        record.Birthday = Conversion.getDateTimeOrNull(reader["Birtday"]);
                        record.DocInsur = Conversion.getByteOrNull(reader["DocInsur"]);
                        record.PassSerie = Conversion.getStrOrNull(reader["PassSerie"]);
                        record.PassNo = Conversion.getStrOrNull(reader["PassNo"]);
                        record.NetCur = Conversion.getStrOrNull(reader["NetCur"]);
                        record.SalePrice = Conversion.getDecimalOrNull(reader["SalePrice"]);
                        record.NationCode = Conversion.getStrOrNull(reader["NationCode"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public InsuranceRptRecord getInsurance(string Market, string ResNo, int? ServiceID, ref string errorMsg)
        {
            string tsql = string.Empty;
            //INameS=I.NameS
            tsql = @"   Select RS.Service, RS.Deplocation, LocationName=isnull(dbo.FindLocalName(L.NameLID, @Market), L.Name),
	                        RS.BegDate,  RS.EndDate, Duration=(isnull(RS.Duration, 0)+1),
	                        InsuranceName = isnull(dbo.FindLocalName(I.NameLID, @Market), I.Name),
	                        InsZone, INameS=isnull(dbo.FindLocalName(I.NameLID, @Market), I.Name), Coverage, CoverageCur, CoverExplain, IDescription = I.Description
                        From ResService RS (NOLOCK) 
                        Join Insurance I (NOLOCK) ON I.Code = RS.Service
                        Join Location L (NOLOCK) ON L.RecID = RS.DepLocation
                        Where ResNo = @ResNo And ServiceType='INSURANCE' And RS.RecID = @ServiceID ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "ServiceID", DbType.Int32, ServiceID);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        InsuranceRptRecord record = new InsuranceRptRecord();
                        record.Service = Conversion.getStrOrNull(reader["Service"]);
                        record.Deplocation = Conversion.getInt32OrNull(reader["Deplocation"]);
                        record.LocationName = Conversion.getStrOrNull(reader["LocationName"]);
                        record.BegDate = Conversion.getDateTimeOrNull(reader["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(reader["EndDate"]);
                        record.Duration = Conversion.getInt16OrNull(reader["Duration"]);
                        record.InsuranceName = Conversion.getStrOrNull(reader["InsuranceName"]);
                        record.InsZone = Conversion.getStrOrNull(reader["InsZone"]);
                        record.INameS = Conversion.getStrOrNull(reader["INameS"]);
                        record.Coverage = Conversion.getDecimalOrNull(reader["Coverage"]);
                        record.CoverageCur = Conversion.getStrOrNull(reader["CoverageCur"]);
                        record.CoverExplain = Conversion.getStrOrNull(reader["CoverExplain"]);
                        record.IDescription = Conversion.getStrOrNull(reader["IDescription"]);
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getPDF(string pdfFilePath, string tempFolder, string resNo, int? ServiceID, int? CustNo, string Market, CultureInfo Ci)
        {
            string error = "";
            List<CustInsuranceRecord> custIns = getcustInsurance(resNo, ServiceID,/* CustNo */ "", ref error);
            InsuranceRptRecord ins = getInsurance(Market, resNo, ServiceID, ref error);
            DataTable dt = new DataTable();
            string pdfName = ServiceID.ToString();
            /*
            if (pdfName == "GVA50A_AUS" || pdfName == "GVA50A_IT" || pdfName == "RENGVA15S")
            {
                //grid
                dt.Rows.Add(custIns.Surname + "," + custIns.Name, 16, 654, true);
                dt.Rows.Add(custIns.Birthday, 168, 654, true);
                dt.Rows.Add(custIns.NationCode, 260, 654, true);
                //tablo
                dt.Rows.Add(ins.LocationName, 235, 570, true);
                dt.Rows.Add(ins.Coverage, 235, 556, true);
                dt.Rows.Add(ins.InsZone, 235, 542, true);
                dt.Rows.Add(Convert.ToDateTime(ins.BegDate.Value, Ci).ToShortDateString() + "-" + Convert.ToDateTime(ins.EndDate, Ci).ToShortDateString(), 235, 528, true);
                dt.Rows.Add(ins.Duration, 235, 514, true);
                dt.Rows.Add(Convert.ToDateTime(DateTime.Today, Ci).ToShortDateString(), 235, 472, true);
            }
            else if (pdfName == "RENGVA15")
            {
                //grid
                dt.Rows.Add(custIns.Surname + "," + custIns.Name, 16, 621, true);
                dt.Rows.Add(custIns.Birthday, 168, 621, true);
                dt.Rows.Add(custIns.NationCode, 260, 621, true);
                //tablo
                dt.Rows.Add(ins.LocationName, 235, 534, true);
                dt.Rows.Add(ins.Coverage, 235, 520, true);
                dt.Rows.Add(ins.InsZone, 235, 506, true);
                dt.Rows.Add(Convert.ToDateTime(ins.BegDate.Value, Ci).ToShortDateString() + "-" + Convert.ToDateTime(ins.EndDate, Ci).ToShortDateString(), 235, 494, true);
                dt.Rows.Add(ins.Duration, 235, 480, true);
                dt.Rows.Add(Convert.ToDateTime(DateTime.Today, Ci).ToShortDateString(), 235, 438, true);
            }
            else if (pdfName == "B2B HP" || pdfName == "B2B HP VIP ENG" || pdfName == "RENNVA4")//b2b hp, b2b hp vip eng
            {
                //grid
                dt.Rows.Add(custIns.Surname + "," + custIns.Name, 18, 635, true);
                dt.Rows.Add(custIns.Birthday, 106, 635, true);
                dt.Rows.Add(custIns.NationCode, 172, 635, true);
                dt.Rows.Add(custIns.PassSerie + "-" + custIns.PassNo, 235, 635, true);
                dt.Rows.Add(custIns.SalePrice, 295, 635, true);
                //tablo
                dt.Rows.Add(ins.LocationName, 395, 665, true);
                dt.Rows.Add(Convert.ToDateTime(ins.BegDate.Value, Ci).ToShortDateString() + "-" + Convert.ToDateTime(ins.EndDate, Ci).ToShortDateString(), 395, 632, true);
                dt.Rows.Add(ins.Duration, 395, 608, true);
                dt.Rows.Add(ins.InsuranceName, 395, 585, true);
                dt.Rows.Add(custIns.NetCur, 495, 575, true);
                dt.Rows.Add(Convert.ToDateTime(DateTime.Today, Ci).ToShortDateString(), 504, 502, true);
                dt.Rows.Add(DateTime.Now.TimeOfDay.ToString(), 504, 490, true);
            }
            else if (pdfName == "NA")
            {
                //grid
                dt.Rows.Add(custIns.Surname + "," + custIns.Name, 36, 658, true);
                dt.Rows.Add(custIns.Birthday, 358, 658, true);
                dt.Rows.Add(custIns.NationCode, 460, 658, true);
                //tablo
                dt.Rows.Add(ins.LocationName, 360, 580, true);
                dt.Rows.Add(ins.Duration, 360, 524, true);
                dt.Rows.Add(custIns.NetCur, 360, 510, true);
                dt.Rows.Add(Convert.ToDateTime(DateTime.Today, Ci).ToShortDateString(), 360, 495, true);
            }
            else if (pdfName == "GVA050HC" || pdfName == "VIP GVA100")
            {
                //grid
                dt.Rows.Add(custIns.Surname + "," + custIns.Name, 37, 662, true);
                dt.Rows.Add(custIns.Birthday, 249, 662, true);
                dt.Rows.Add(custIns.NationCode, 319, 662, true);
                //tablo
                dt.Rows.Add(ins.LocationName, 262, 634, true);
                dt.Rows.Add(ins.Coverage, 262, 620, true);
                dt.Rows.Add(ins.InsZone, 262, 606, true);
                dt.Rows.Add(Convert.ToDateTime(ins.BegDate.Value, Ci).ToShortDateString() + "-" + Convert.ToDateTime(ins.EndDate, Ci).ToShortDateString(), 262, 594, true);
                dt.Rows.Add(ins.Duration, 262, 580, true);
                dt.Rows.Add(Convert.ToDateTime(DateTime.Today, Ci).ToShortDateString(), 262, 538, true);
            }
             * */
            return RewirePDF(pdfFilePath, tempFolder, resNo, pdfName, dt);
        }

        public string RewirePDF(string pdfFilePath, string tempFolder, string resNo, string pdfName, DataTable dt)
        {
            string fileName = pdfFilePath + pdfName + ".pdf";
            if (!File.Exists(fileName))
            {
                fileName = pdfFilePath + "RENGVA15.pdf";
            }
            string afileName = tempFolder + resNo + "_" + pdfName + ".pdf";
            string fileName2 = pdfFilePath + pdfName + "2.pdf";
            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);

            BaseFont bfont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            foreach (DataRow row in dt.Rows)
            {
                if ((bool)row["IsShown"] == true)
                {
                    TvReport.TvReportCommon.writeText(ref cb, row["item"].ToString(), (float)row["x"], (float)row["y"], bfont, 8);
                }
            }
            cb.ClosePath();
            if (File.Exists(fileName2))
            {
                PdfReader reader2 = new PdfReader(fileName2);
                for (int i = 0; i < reader2.NumberOfPages; i++)
                {
                    byte[] pdfBytes2 = reader2.GetPageContent(i + 1);
                    doc.NewPage();
                    page = pdfWriter.GetImportedPage(reader2, i + 1);
                    PdfContentByte cb2 = pdfWriter.DirectContent;
                    cb2.AddTemplate(page, 0, 0);
                    cb2.ClosePath();
                }
                reader2.Close();
            }
            doc.Close();
            reader.Close();

            return resNo + "_" + pdfName + ".pdf";
        }

        public string createInsurance(string ResNo, string pdfFilePath, string tempFolder, string data, string pdfName, ref string errorMsg)
        {

            string fileName = pdfFilePath + pdfName + ".pdf";
            if (!File.Exists(fileName)) return "";
            string tmpAFileName = ResNo + "_" + pdfName + System.Guid.NewGuid() + ".pdf";
            string afileName = tempFolder + tmpAFileName;

            string fileName2 = pdfFilePath + pdfName + "2.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath("~\\");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();
            if (File.Exists(fileName2))
            {
                PdfReader reader2 = new PdfReader(fileName2);
                for (int i = 0; i < reader2.NumberOfPages; i++)
                {
                    byte[] pdfBytes2 = reader2.GetPageContent(i + 1);
                    doc.NewPage();
                    page = pdfWriter.GetImportedPage(reader2, i + 1);
                    PdfContentByte cb2 = pdfWriter.DirectContent;
                    cb2.AddTemplate(page, 0, 0);
                    cb2.ClosePath();
                }
                reader2.Close();
            }
            doc.Close();
            reader.Close();
            return tmpAFileName;
        }

        public string createAggreement(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Aggreement.pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_Aggreement.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                    if (row.Type == writeType.QRCode)
                        TvReport.TvReportCommon.writeQRCode(ref doc, row.value, row.x, row.y, row.W, row.H);
                    else TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();
            doc.Close();
            reader.Close();
            return ResNo + "_Aggreement.pdf";
        }

        public string createProforma(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Proforma.pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_Proforma.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                {
                    if (row.RightToLeft)
                        TvReport.TvReportCommon.writeTextRightAlign(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);
                    else TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);
                }
            cb.ClosePath();
            doc.Close();
            reader.Close();
            return ResNo + "_Proforma.pdf";
        }

        public string createInvoice(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Invoice.pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_Invoice.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                {
                    if (row.RightToLeft)
                        TvReport.TvReportCommon.writeTextRightAlign(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);
                    else TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);
                }
            cb.ClosePath();
            doc.Close();
            reader.Close();
            return ResNo + "_Invoice.pdf";
        }

        public string createVoucher(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Voucher.pdf";
            if (!File.Exists(fileName)) return "";
            string tmpAFileName = ResNo + "_" + System.Guid.NewGuid() + "_Voucher.pdf";
            string afileName = tempFolder + tmpAFileName;

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();
            doc.Close();
            reader.Close();
            return tmpAFileName;
        }

        public string createFlyTicket(string ResNo, string pdfFilePath, string tempFolder, string data, string _fileName, ref string errorMsg)
        {
            string fileName = pdfFilePath + _fileName + ".pdf";
            if (!File.Exists(fileName)) return "";
            string tmpAFileName = ResNo + "_" + _fileName + "_" + System.Guid.NewGuid() + ".pdf";
            string afileName = tempFolder + tmpAFileName;

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data)
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x, row.y, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();
            doc.Close();
            reader.Close();
            return tmpAFileName;
        }

        public string mergeFlyTicket(string tempFolder, List<string> ticketList)
        {
            string aFileName = ticketList[0].ToString().Substring(0, ticketList[0].ToString().IndexOf('_')) + System.Guid.NewGuid() + "_ETicket.pdf";
            if (File.Exists(tempFolder + aFileName))
                File.Delete(tempFolder + aFileName);
            string fileName = ticketList.FirstOrDefault().ToString().Substring(0, ticketList[0].ToString().IndexOf('_'));
            int currentpage = 1;
            Document doc = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(tempFolder + aFileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            for (int i = 0; i < ticketList.Count; i++)
            {
                PdfReader reader2 = new PdfReader(tempFolder + ticketList[i].ToString());
                byte[] pdfBytes2 = reader2.GetPageContent(currentpage);
                doc.NewPage();
                PdfImportedPage page = pdfWriter.GetImportedPage(reader2, currentpage);

                PdfContentByte cb2 = pdfWriter.DirectContent;
                cb2.AddTemplate(page, 0, 0);
                cb2.ClosePath();
                reader2.Close();
                //File.Delete(tempFolder + ticketList[i].ToString());
            }
            doc.Close();

            return aFileName;
        }

        public List<AgencyReportTaxSystem> getAgencyReport(int? Year, int? Month, string Agency, bool TaxSystem, ref string errorMsg)
        {
            List<AgencyReportTaxSystem> records = new List<AgencyReportTaxSystem>();
            string tsql = @"Declare @MinDate datetime, @MaxDate datetime, @isGrp bit
                            Set @isGrp=0

                            Set @MinDate = Right('0'+cast(@Month as varchar(2)),2) + '/01/' + Cast(@Year as varchar(4))

                            --if (SELECT DATEPART(month, GETDATE())) = @Month 
                            --begin
                            --  Set @MaxDate = Right('0'+cast(@Month as varchar(2)),2) + '/'+ Right('0'+DATEPART(day, GETDATE()),2) + '/'+ Cast(@Year as varchar(4))  
                            --end
                            --else
                            --begin
                            Set @MaxDate = DateAdd(day, -1, DateAdd(month, 1, @MinDate))
                            --end

                            Select *, FirstDate = @MinDate, LastDate = @MaxDate,
                                   Field14 = case
                                               when LastPayDateReal > LastPayDate then Field4Alternative
                                               when Balance between -1 and 1 then Payment2
                                               else cast((Payment2/Payment) * AgencyPayable as dec(18, 2))
                                             end,
                                   Field12 = case when OwnAgency = 1 then Cast(30.00 as dec(18,2)) else cast((case
                                                                                                                when LastPayDateReal > LastPayDate then Field4Alternative
                                                                                                                when Balance between -1 and 1 then Payment2
                                                                                                                else cast((Payment2/Payment) * AgencyPayable as dec(18, 2))
                                                                                                              end/AgencyPayable) as dec(18, 2)) end,
                                   Field11 = --cast(cast((Payment2/Payment) * AgencyPayable as dec(18, 2)) + 
                                             cast(
                                             case
                                               when LastPayDateReal > LastPayDate then Field4Alternative
                                               when Balance between -1 and 1 then Payment2
                                               else cast((Payment2/Payment) * AgencyPayable as dec(18, 2))
                                             end +
                                             case when OwnAgency = 1 then Cast(30.00 as dec(18,2)) else cast((case
                                                                                                                when LastPayDateReal > LastPayDate then Field4Alternative
                                                                                                                when Balance between -1 and 1 then Payment2
                                                                                                                else cast((Payment2/Payment) * AgencyPayable as dec(18, 2))
                                                                                                              end/AgencyPayable) as dec(18, 2)) end as dec(18, 2)),
                                   Field13 = case 
                                               when @TaxSystem = 0 then -999                 
                                               else Cast(case when OwnAgency = 1 then Cast(30.00 as dec(18,2)) else cast((case
                                                                                                                            when LastPayDateReal > LastPayDate then Field4Alternative
                                                                                                                            when Balance between -1 and 1 then Payment2
                                                                                                                            else cast((Payment2/Payment) * AgencyPayable as dec(18, 2))
                                                                                                                          end/Payment) as dec(18, 2)) end / 118 * 100 * 0.18 as dec(18, 2))
                                             end
                            From
                            (
                              Select R.ResNo, C.SurName, C.Name, Country = (Select Name From Location where RecID = L.Country),
                                     AgencyFirmName = A.FirmName, AgencyBossName = A.BossName, OperatorFirmName = O.FirmName, OperatorBossName = O.BossName, AC.ContractNo, AC.BegDate,
                                     --LastPayDate = PD.PayDate, 
                                     LastPayDate = R.BegDate, LastPayDateReal = PD.PayDate, 
                                     AgencyPayable, A.OwnAgency,
                                     --Field4Alternative = cast(dbo.ufn_Exchange(R.BegDate, R.SaleCur, M.Cur, AgencyPayable, 4, R.Market) as dec(18, 2)), --Bank Sale'den istediler
                                     Field4Alternative = cast(dbo.ufn_ExchangeForRpt(R.BegDate, R.SaleCur, M.Cur, AgencyPayable, 1, R.Market, '', 2) as dec(18, 2)), --Bank Sale'den istediler
                                     R.Balance,
                                     Payment = R.AgencyPayment, --cast(dbo.ufn_Exchange(PD.PayDate, R.SaleCur, M.Cur, R.AgencyPayment, 1, R.Market) as dec(18, 2)),
                                     Payment2 = cast((Select Sum(dbo.ufn_Exchange(PayDate, PaidCur, MM.Cur, isnull(PaidAmount,0), 1, RR.Market)) 
                                                 from JournalCDet JC (NOLOCK) 
                                                 Join Journal J (NOLOCK) on J.RecID=JC.JournalID 
                                                 join ResMain RR (NOLOCK) on RR.ResNo = R.ResNo
                                                 join Market MM (NOLOCK) on MM.Code = RR.Market
                                                 Where JC.ResNo=R.ResNo and CP='C') as dec(18, 2))
                              from ResMain R (NOLOCK)
                              join ResCust C (NOLOCK) on R.ResNo = C.ResNo and C.Leader = 'Y'
                              join Location L (NOLOCK) on L.RecID = R.ArrCity
                              join Agency A (NOLOCK) on A.Code = R.Agency
                              join Operator O (NOLOCK) on O.Code = R.Operator
                              join Market M (NOLOCK) on M.Code = R.Market
                            /*
                              outer apply
                              (
                                Select Top 1 ContractNo, BegDate From AgencyAgree (NOLOCK)
                                where Agency = R.Agency
                                order by CrtDate desc, ContractNo, BegDate
                              ) AC
                            */
                              outer apply
                              (
                                Select Top 1 ContractNo, BegDate, RecID 
                                From AgencyAgree (NOLOCK)
                                where Agency = R.Agency and
                                      ContractNo = (Select Top 1 ContractNo From AgencyAgree (NOLOCK)
                                                    where Agency = R.Agency
                                                    order by CrtDate desc, ContractNo desc)
                                order by RecID
                              ) AC
                              
                              outer apply
                              (
                                Select Top 1 J.PayDate
                                from JournalCDet JC (NOLOCK)
                                Join Journal J (NOLOCK) on J.RecID=JC.JournalID
                                Where ResNo=R.ResNo and CP='C' and RecType = 0
                                Order by J.PayDate Desc
                              ) PD
                              Where R.ResStat in (0, 1) and isnull(R.Balance, 0) < 1 and 
                              --        (SELECT DATEPART(month, PD.PayDate)) = @Month and (SELECT DATEPART(Year, PD.PayDate)) = @Year and
                                (SELECT DATEPART(month, R.BegDate)) = @Month and (SELECT DATEPART(Year, R.BegDate)) = @Year and
                                ((@isGrp = 0 and R.Agency = @Agency) or
                                (@isGrp = 1 and R.Agency in (Select Agency From GrpAgencyDet where Groups = @Agency and GrpType = 'R'))) and        
                                isnull(R.AgencyPayment, 0) <> 0 --and
                              --        not exists(Select *
                              --                   from JournalCDet JC (NOLOCK)
                              --                   Join Journal J (NOLOCK) on J.RecID=JC.JournalID
                              --                   join PayType P (NOLOCK) on P.Code = J.PayType
                              --                   Where ResNo=R.ResNo and CP='C' and P.PayCat = 0)
                            ) X
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Year", DbType.Int32, Year);
                db.AddInParameter(dbCommand, "Month", DbType.Int32, Month);
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);
                //db.AddInParameter(dbCommand, "isGrp", DbType.Boolean, false);
                db.AddInParameter(dbCommand, "TaxSystem", DbType.Boolean, TaxSystem);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        AgencyReportTaxSystem record = new AgencyReportTaxSystem();
                        record.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        record.SurName = Conversion.getStrOrNull(R["SurName"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.Country = Conversion.getStrOrNull(R["Country"]);
                        record.AgencyFirmName = Conversion.getStrOrNull(R["AgencyFirmName"]);
                        record.AgencyBossName = Conversion.getStrOrNull(R["AgencyBossName"]);
                        record.OperatorFirmName = Conversion.getStrOrNull(R["OperatorFirmName"]);
                        record.OperatorBossName = Conversion.getStrOrNull(R["OperatorBossName"]);
                        record.ContractNo = Conversion.getStrOrNull(R["ContractNo"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.LastPayDate = Conversion.getDateTimeOrNull(R["LastPayDate"]);
                        record.AgencyPayable = Conversion.getDecimalOrNull(R["AgencyPayable"]);
                        record.Payment = Conversion.getDecimalOrNull(R["Payment"]);
                        record.Payment2 = Conversion.getDecimalOrNull(R["Payment2"]);
                        record.FirstDate = Conversion.getDateTimeOrNull(R["FirstDate"]);
                        record.LastDate = Conversion.getDateTimeOrNull(R["LastDate"]);
                        record.Field14 = Conversion.getDecimalOrNull(R["Field14"]);
                        record.Field12 = Conversion.getDecimalOrNull(R["Field12"]);
                        record.Field11 = Conversion.getDecimalOrNull(R["Field11"]);
                        record.Field13 = Conversion.getDecimalOrNull(R["Field13"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public TicketRefPriceRecord getTicketRefValues(string Supplier, string FlightNo, DateTime? FlyDate, string SClass, string BlockType, ref string errorMsg)
        {
            string tsql = @"Select Top 1 FlightNo,FlyDate,SClass,BlockType,Supplier,NetPrice,NetChd,NetInf,NetCur 
                            From FlightPrice (NOLOCK)
                            Where FlightNo=@FlightNo and FlyDate=@FlyDate and Supplier=@Supplier and SClass=@SClass and BlockType=BlockType and BS='B' and Buyer=''
                            Order By SalePriority Desc, BlockID Desc
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Supplier", DbType.String, Supplier);
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlyDate", DbType.DateTime, FlyDate);
                db.AddInParameter(dbCommand, "SClass", DbType.String, SClass);
                db.AddInParameter(dbCommand, "BlockType", DbType.String, BlockType);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        TicketRefPriceRecord record = new TicketRefPriceRecord();
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        record.FlyDate = Conversion.getDateTimeOrNull(R["FlyDate"]);
                        record.SClass = Conversion.getStrOrNull(R["SClass"]);
                        record.BlockType = Conversion.getStrOrNull(R["BlockType"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        record.NetPrice = Conversion.getDecimalOrNull(R["NetPrice"]);
                        record.NetChd = Conversion.getDecimalOrNull(R["NetChd"]);
                        record.NetInf = Conversion.getDecimalOrNull(R["NetInf"]);
                        record.NetCur = Conversion.getStrOrNull(R["NetCur"]);
                        return record;
                    }
                    else return null;                    
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }             
        }

        public decimal? getTicketRefRate(string ResNo, string Market, string CurTo, ref string errorMsg)
        {
            string tsql = @"Declare @Rate dec(18,6), @RateDate datetime
                            --Retrieve Max PayDate
                            Select @RateDate=Max(J.PayDate) from JournalCDet JC (NOLOCK)
                            Join Journal J (NOLOCK) on J.RecID=JC.JournalID
                            Where ResNo=@ResNo and CP='C'
                            --                            
                            Select @Rate=dbo.ufn_Exchange(IsNull(@RateDate,R.ResDate),R.SaleCur, @CurTo, 1,1,@Market)
                            from ResMain R (NOLOCK)
                            Where R.ResNo=@ResNo
                            Select Rate=@Rate
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "CurTo", DbType.String, CurTo);
                return Conversion.getDecimalOrNull(db.ExecuteScalar(dbCommand));  
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }            
        }
    }
}