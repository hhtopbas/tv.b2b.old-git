﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;
using iTextSharp;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Web;
using System.Globalization;
using System.Threading;
using PDFBuilder;
using iTextSharp.text.html.simpleparser;

namespace TvReport.DetReport
{
    public class DetReport
    {
        #region Detur_Table_Crt_Qry
        string Detur_Table_Crt_Qry = @"
                    if OBJECT_ID('TempDB.dbo.#Passengers') is not null Drop Table #Passengers
                    CREATE TABLE #Passengers(
                    RecID int IDENTITY(1,1) NOT NULL,
                    CustNo int,
                    Name nvarchar(80) Collate database_default,
                    SurName nvarchar(80) Collate database_default,
                    Birthdate datetime,
                    Age smallint,
                    Title varchar(5) Collate database_default,
                    TitleNo Smallint,
                    PageNo smallint,
                    SeqNo smallint,
                    Leader varchar(1),
                    Status tinyint
                    )

                    if OBJECT_ID('TempDB.dbo.#PasPrices') is not null Drop Table #PasPrices
                    CREATE TABLE #PasPrices(
                    RecID int IDENTITY(1,1) NOT NULL,
                    PageNo smallint,
                    IncPack varchar(1) Collate database_default,
                    Service varchar(10) Collate database_default,
                    ServiceDesc nvarchar(200) Collate database_default,
                    Pax1Amount dec(18,2),
                    Pax2Amount dec(18,2),
                    Pax3Amount dec(18,2),
                    Pax4Amount dec(18,2),
                    Pax5Amount dec(18,2),
                    Pax6Amount dec(18,2),
                    ServiceType varchar(10)
                    )

                    if OBJECT_ID('TempDB.dbo.#ResPayPlan') is not null Drop Table #ResPayPlan
                    CREATE TABLE dbo.#ResPayPlan
                    (
                    RecID int IDENTITY(1,1) NOT NULL,
                    ResNo varchar(10),
                    PayNo smallint,
                    DueDate datetime,
                    Amount dec(18,2),
                    Cur varchar(5),
                    Adult dec(18,2),
                    Child dec(18,2),
                    Infant dec(18,2),
                    PayAmount dec(18,2),
                    RemAmount dec(18,2)
                    )

                    if OBJECT_ID('TempDB.dbo.#ServicePrices') is not null Drop Table #ServicePrices
                    CREATE TABLE #ServicePrices(
                    RecID int IDENTITY(1,1) NOT NULL,
                    RecType smallint,
                    IncPack varchar(1) Collate database_default,
                    Unit smallint,
                    Service varchar(10) Collate database_default,
                    ServiceDesc nvarchar(200) Collate database_default,
                    SalePrice dec(18,2),
                    BegDate datetime,
                    Room varchar(10),
                    Board varchar(10),
                    ServiceType varchar(10)
                    )

                    if OBJECT_ID('TempDB.dbo.#PayPlan') is not null Drop Table #PayPlan
                    CREATE TABLE #PayPlan(
                    RecID int IDENTITY(1,1) NOT NULL,
                    Cur varchar(5),
                    DueDate_L1 datetime,
                    RemAmount_L1 dec(18,2),

                    DueDate_L2 datetime,
                    RemAmount_L2 dec(18,2),

                    DueDate_L3 datetime,
                    RemAmount_L3 dec(18,2)
                    )
                    ";
        #endregion

        #region Detur_InvoiceItems_Build_Qry
        string Detur_InvoiceItems_Build_Qry = @"
Set NOCOUNT ON
--Payment Plan items --
Declare @AgencyPayment dec(18,2),@RemAmount dec(18,2),@Amount dec(18,2), @PayNo int,@Market varchar(10), @str nvarchar(250), @HasSupplement bit, @SaleResource smallint,@ResStat smallint
Select @AgencyPayment=AgencyPayment,@Market=Market,@SaleResource=SaleResource,@ResStat=ResStat From ResMain (NOLOCK) Where ResNo=@ResNo

if @Market<>'SWEMAR' --27.04.2011'de kaldırıldı
Begin

  --Pas.EB Description
  Set @str=''
  Select @str=P.Description From ResMain R (NOLOCK)
  Join PasEB P (NOLOCK) on P.RecID=R.EBPasID
  Where ResNo=@ResNo
  if @str<>'' Set @PasEBStr=@str
 
  --PL.Spo Description
  Set @str=''
  Select @str=Spo.Description from ResCustPrice P (NOLOCK)
  Join ResService S on S.ResNo=P.ResNo and S.RecID=P.ServiceID and S.ServiceType='FLIGHT' and S.StatSer in (0,1) and PlSpoVal<>0
  Join CatPriceSPo (NOLOCK) SPo on Spo.RecID=P.PLSpoNo
  Where P.ResNo=@ResNo
 
  if @str<>'' Set @PLSpoStr=@str
end

Insert Into #ResPayPlan
 (ResNo,PayNo,DueDate,Amount,Cur,Adult,Child,Infant,PayAmount,RemAmount)
Select ResNo,PayNo,DueDate,Amount,Cur,Adult,Child,Infant,0,0 From ResPayPlan (NOLOCK) Where ResNo=@ResNo
Set @RemAmount=@AgencyPayment
Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
  Select PayNo,Amount From #ResPayPlan Order by PayNo

Open Cursor1
Fetch Next FROM Cursor1 INTO @PayNo,@Amount
While @@FETCH_STATUS = 0
Begin

 if @RemAmount>0
 if @RemAmount>=@Amount
 Begin
   Update #ResPayPlan Set PayAmount=@Amount Where PayNo=@PayNo
   Set @RemAmount=@RemAmount-@Amount
 End
 Else 
 Begin
   Update #ResPayPlan Set PayAmount=@RemAmount Where PayNo=@PayNo
   Set @RemAmount=0
 end
 if @RemAmount<=0 Break

 Fetch Next FROM Cursor1 INTO @PayNo,@Amount
End
Close Cursor1
DEALLOCATE Cursor1

Update #ResPayPlan Set RemAmount=Amount-PayAmount

Declare @PageNo smallint, @MaxPageNo smallint, @PaxPerPage smallint,@CustNo int, @SalePrice dec(18,2),@SeqNo smallint,
@IncPack varchar(1),@Service varchar(10), @ServiceDesc nvarchar(200),@BegDate datetime

Insert Into #Passengers (CustNo,Name,Surname,Birthdate,Age,Leader,Title,TitleNo,Status)
Select CustNo,Name,Surname,Birtday,Age,Leader,dbo.GetTitle(Title),Title,Status From ResCust (NOLOCK) Where ResNo=@ResNo and Status=0

Insert Into #Passengers (CustNo,Name,Surname,Birthdate,Age,Leader,Title,TitleNo,Status) --16.03.2011 - Cezalı yolcularda ekleniyor
Select CustNo,Name,Surname,Birtday,Age,Leader,dbo.GetTitle(Title),Title,Status From ResCust (NOLOCK) Where ResNo=@ResNo and Status=1 and ppSupDisAmount<>0

Update #Passengers Set PageNo= CEILING(RecID * 1.0 / 6)

Update #Passengers
Set SeqNo=xSeqNo
From
(Select xCustNo=CustNo,xSeqNo=ROW_NUMBER() OVER (PARTITION BY PageNo ORDER BY PageNo) From #Passengers) x
Where CustNo=X.xCustNo

Select @MaxPageNo=Max(PageNo) From #Passengers

--18.10.2011 i.c. added
Select Top 1 @HasSupplement=Case When C.ppPLSpoVal>0 then 1 else 0 end  
From ResCust C (NOLOCK)
Join #Passengers LP on LP.CustNo=C.CustNo and LP.PageNo=1 and LP.Status=0
Where C.ResNo=@ResNo and C.ppPLSpoVal<>0
Set @HasSupplement=IsNull(@HasSupplement,0)


--PL SPO Amounts
if @HasSupplement=0
BEGIN
  Set @PageNo=1
  While @PageNo<=@MaxPageNo
  Begin
     Set @IncPack='N'
  
     Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
       Select C.CustNo,ppPLSpoVal=C.ppPLSpoVal,LP.SeqNo,Service='.PLSPO.'
       From ResCust C (NOLOCK)
       Join #Passengers LP on LP.CustNo=C.CustNo and LP.PageNo=@PageNo and LP.Status=0
       Where C.ResNo=@ResNo and C.ppPLSpoVal<>0
       Order by C.CustNo,LP.SeqNo
     
     Open Cursor1
     Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service
     While @@FETCH_STATUS = 0
     Begin
       if not Exists(Select * from #PasPrices Where PageNo=@PageNo and Service=@Service and IncPack=@IncPack)
           Insert #PasPrices (PageNo,IncPack,Service,ServiceDesc)
            Values(@PageNo,@IncPack,@Service,@PlSPOStr)
   
       if @SeqNo=1
          Update #PasPrices Set Pax1Amount=IsNull(Pax1Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
       Else
       if @SeqNo=2
          Update #PasPrices Set Pax2Amount=IsNull(Pax2Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
       Else
       if @SeqNo=3
          Update #PasPrices Set Pax3Amount=IsNull(Pax3Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
       Else
       if @SeqNo=4
          Update #PasPrices Set Pax4Amount=IsNull(Pax4Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
       Else
       if @SeqNo=5
          Update #PasPrices Set Pax5Amount=IsNull(Pax5Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
       Else
       if @SeqNo=6
          Update #PasPrices Set Pax6Amount=IsNull(Pax6Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
   
       Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service
     End
     
     Close Cursor1
     DEALLOCATE Cursor1
  
     Set @PageNo=@PageNo+1
  End --while
END

--Include Pack Amounts---
Set @PageNo=1
While @PageNo<=@MaxPageNo
Begin
   Set @IncPack='Y'

   Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
     Select P.CustNo,SalePrice=Sum(P.SalePrice),LP.SeqNo
     From ResCustPrice P (NOLOCK)
     Join #Passengers LP on LP.CustNo=P.CustNo and LP.PageNo=@PageNo --and LP.Status=0
     Join(Select ServiceID=RecID,ExtServiceID=0,ResNo,IncPack From ResService Where IncPack=@IncPack and (SalePrice<>0 or @ZeroInv=1)
          Union All
          Select ServiceID,ExtServiceID=SE.RecID,ResNo,IncPack From ResServiceExt SE
          Join ServiceExt E (NOLOCK) on E.Service=SE.ServiceType and E.Code=ExtService and SepInInv='N'
          Where IncPack=@IncPack and (SalePrice<>0 or @ZeroInv=1)
         ) S on S.ResNo=P.ResNo and S.ServiceID=P.ServiceID and S.ExtServiceID=P.ExtServiceID
  
     Where P.ResNo=@ResNo and S.IncPack=@IncPack --and P.ExtServiceID=0
     Group by P.CustNo,LP.SeqNo
     Order by P.CustNo,LP.SeqNo
   
   Open Cursor1
   Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo
   While @@FETCH_STATUS = 0
   Begin

    Set @ServiceDesc=@BasisPriceStr
    if not Exists(Select * from #PasPrices Where PageNo=@PageNo and Service='' and IncPack=@IncPack)
        Insert #PasPrices (PageNo,IncPack,Service,ServiceDesc)
         Values(@PageNo,@IncPack,'',@ServiceDesc)

    if @SeqNo=1
       Update #PasPrices Set Pax1Amount=@SalePrice Where PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=2
       Update #PasPrices Set Pax2Amount=@SalePrice Where PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=3
       Update #PasPrices Set Pax3Amount=@SalePrice Where PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=4
       Update #PasPrices Set Pax4Amount=@SalePrice Where PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=5
       Update #PasPrices Set Pax5Amount=@SalePrice Where PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=6
       Update #PasPrices Set Pax6Amount=@SalePrice Where PageNo=@PageNo and IncPack=@IncPack


    Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo
   End
   
   Close Cursor1
   DEALLOCATE Cursor1

  Set @PageNo=@PageNo+1
End --while

--PLSPO indirimlerinin Ana Fiyata eklenmesi
if Exists(Select * from #PasPrices Where Service='.PLSPO.')
Begin

 Update #PasPrices
   Set Pax1Amount=Pax1Amount * -1,
       Pax2Amount=Pax2Amount * -1,
       Pax3Amount=Pax3Amount * -1,
       Pax4Amount=Pax4Amount * -1,
       Pax5Amount=Pax5Amount * -1,
       Pax6Amount=Pax6Amount * -1
 Where Service='.PLSPO.'

 Update #PasPrices
 Set Pax1Amount=(Select Sum( Pax1Amount ) From #PasPrices P Where Pax1Amount is not null and PageNo=#PasPrices.PageNo),
     Pax2Amount=(Select Sum( Pax2Amount ) From #PasPrices P Where Pax2Amount is not null and PageNo=#PasPrices.PageNo),
     Pax3Amount=(Select Sum( Pax3Amount ) From #PasPrices P Where Pax3Amount is not null and PageNo=#PasPrices.PageNo),
     Pax4Amount=(Select Sum( Pax4Amount ) From #PasPrices P Where Pax4Amount is not null and PageNo=#PasPrices.PageNo),
     Pax5Amount=(Select Sum( Pax5Amount ) From #PasPrices P Where Pax5Amount is not null and PageNo=#PasPrices.PageNo),
     Pax6Amount=(Select Sum( Pax6Amount ) From #PasPrices P Where Pax6Amount is not null and PageNo=#PasPrices.PageNo)
 Where Service=''

 Update #PasPrices
   Set Pax1Amount=Pax1Amount * -1,
       Pax2Amount=Pax2Amount * -1,
       Pax3Amount=Pax3Amount * -1,
       Pax4Amount=Pax4Amount * -1,
       Pax5Amount=Pax5Amount * -1,
       Pax6Amount=Pax6Amount * -1
 Where Service='.PLSPO.'

End

--Out of Pack Amounts---
Set @PageNo=1
While @PageNo<=@MaxPageNo
Begin
   Set @IncPack='N'

   Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
     Select P.CustNo,P.SalePrice,LP.SeqNo,S.Service,
            ServiceDesc=
              Case S.ServiceType
               When 'HOTEL' then (Select Name+' ('+Category+'),'+(Select Name From Location (NOLOCK) Where RecID=Hotel.Location) From Hotel (NOLOCK) Where Code=Service)
               When 'FLIGHT' then (Select IsNull(PNLName,FlightNo)+' ('+DepAirport+'->'+ArrAirport+'),'+FlgClass From FlightDay (NOLOCK) Where FlightNo=Service and FlyDate=S.BegDate)
               When 'TRANSPORT' then (Select IsNull(dbo.FindLocalName(TP.NameLID,@Market),TP.Name)+' ('+dbo.ufn_GetLocationName(S.DepLocation)+'-'+dbo.ufn_GetLocationName(S.ArrLocation)+') ' From Transport TP (NOLOCK)
                                      Join TransportDay TPD (NOLOCK) on TPD.Transport=TP.Code and TPD.TransDate=S.BegDate and TPD.Bus=S.Bus
                                      Where TP.Code=S.Service
                                      )
               When 'RENTING' then (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name)+' (Category:'+Category+') / '+(Select Name From Location (NOLOCK) Where RecId=DepLocation) From Renting (NOLOCK) Where Code=Service)               
               When 'EXCURSION' then (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name) From Excursion (NOLOCK) Where Code=Service)
               When 'INSURANCE' then (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name) From Insurance (NOLOCK) Where Code=Service)
               When 'VISA' then (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name) From Visa (NOLOCK) Where Code=Service)
               When 'TRANSFER' then (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name) /*+', '+dbo.ufn_GetLocationName(S.ArrLocation)*/ From Transfer (NOLOCK) Where Code=Service)
               When 'HANDFEE' then (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name)+', '+dbo.ufn_GetLocationName(S.ArrLocation) From HandFee (NOLOCK) Where Code=Service)
               else (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name) From AdService (NOLOCK) Where AdService.Service=S.ServiceType and Code=S.Service)
              End +
              Case When StatConf=0 then ' '+@RequestStr else '' end,
            S.BegDate
     From ResCustPrice P (NOLOCK)
     Join #Passengers LP on LP.CustNo=P.CustNo and LP.PageNo=@PageNo --and LP.Status=0
     Join ResService S (NOLOCK) on S.RecID=P.ServiceID
     Where P.ResNo=@ResNo and S.IncPack=@IncPack and P.ExtServiceID=0 and (S.SalePrice<>0 or @ZeroInv=1)

     Union ALL

     Select P.CustNo,P.SalePrice,LP.SeqNo,S.ExtService,
            ServiceDesc= (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name) From ServiceExt Where Code=S.ExtService)+
                         Case When StatConf=0 then ' '+@RequestStr else '' end,
            S.BegDate
     From ResCustPrice P (NOLOCK)
     Join #Passengers LP on LP.CustNo=P.CustNo and LP.PageNo=@PageNo and LP.Status=0
     Join ResServiceExt S (NOLOCK) on S.RecID=P.ExtServiceID and S.ServiceID=P.ServiceID
     Join ServiceExt E (NOLOCK) on E.Service=S.ServiceType and E.Code=ExtService --and SepInInv='N'
     Left Join ServiceExtMarOpt EMO (NOLOCK) on EMO.ServiceID=E.RecID and EMO.Market=@Market
     --Where P.ResNo=@ResNo and (S.IncPack=@IncPack or E.SepInInv='Y') and (S.SalePrice<>0 or @ZeroInv=1 or E.SepInInv='Y') --28.03.2011'de yapıldı
     Where P.ResNo=@ResNo and (S.IncPack=@IncPack or E.SepInInv='Y') and 
          (S.SalePrice<>0 or @ZeroInv=1 or E.SepInInv='Y' or IsNull(EMO.NoShowInvZero,0)=0) --28.03.2011'de yapıldı --01.03.2013 IC.NoShowInvZero added
     Order by P.CustNo,LP.SeqNo,S.BegDate
   
   Open Cursor1
   Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc,@BegDate
   While @@FETCH_STATUS = 0
   Begin

    if not Exists(Select * from #PasPrices Where PageNo=@PageNo and Service=@Service and IncPack=@IncPack)
        Insert #PasPrices (PageNo,IncPack,Service,ServiceDesc)
         Values(@PageNo,@IncPack,@Service,@ServiceDesc)

    if @SeqNo=1
       Update #PasPrices Set Pax1Amount=IsNull(Pax1Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=2
       Update #PasPrices Set Pax2Amount=IsNull(Pax2Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=3
       Update #PasPrices Set Pax3Amount=IsNull(Pax3Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=4
       Update #PasPrices Set Pax4Amount=IsNull(Pax4Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=5
       Update #PasPrices Set Pax5Amount=IsNull(Pax5Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=6
       Update #PasPrices Set Pax6Amount=IsNull(Pax6Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack

    Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc,@BegDate
   End
   
   Close Cursor1
   DEALLOCATE Cursor1

  Set @PageNo=@PageNo+1
End --while

--Pas.EB
Set @PageNo=1
While @PageNo<=@MaxPageNo
Begin
   Set @IncPack='N'

   Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
     Select C.CustNo,ppPasEB=ABS(C.ppPasEB)*-1,LP.SeqNo,Service='.PASEB.'
     From ResCust C (NOLOCK)
     Join #Passengers LP on LP.CustNo=C.CustNo and LP.PageNo=@PageNo and LP.Status=0
     Where C.ResNo=@ResNo and C.ppPasEB<>0
     Order by C.CustNo,LP.SeqNo
   
   Open Cursor1
   Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service
   While @@FETCH_STATUS = 0
   Begin

    if not Exists(Select * from #PasPrices Where PageNo=@PageNo and Service=@Service and IncPack=@IncPack)
        Insert #PasPrices (PageNo,IncPack,Service,ServiceDesc)
         Values(@PageNo,@IncPack,@Service,@PasEBStr)

    if @SeqNo=1
       Update #PasPrices Set Pax1Amount=IsNull(Pax1Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=2
       Update #PasPrices Set Pax2Amount=IsNull(Pax2Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=3
       Update #PasPrices Set Pax3Amount=IsNull(Pax3Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=4
       Update #PasPrices Set Pax4Amount=IsNull(Pax4Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=5
       Update #PasPrices Set Pax5Amount=IsNull(Pax5Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=6
       Update #PasPrices Set Pax6Amount=IsNull(Pax6Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack

    Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service
   End
   
   Close Cursor1
   DEALLOCATE Cursor1

  Set @PageNo=@PageNo+1
End --while

--Supplement
Set @PageNo=1
While @PageNo<=@MaxPageNo
Begin
   Set @IncPack='N'

   Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
     Select C.CustNo,C.CalcAmount,LP.SeqNo,Service=C.SupDis,ServiceDesc=IsNull(dbo.FindLocalName(S.NameLID,@Market),S.Name)
     From ResSupDis C (NOLOCK)
     Join #Passengers LP on LP.CustNo=C.CustNo and LP.PageNo=@PageNo
     Join SupDis S (NOLOCK) on S.Code=C.SupDis
     Where C.ResNo=@ResNo and C.CalcAmount<>0 and C.SD='S'
     Order by C.CustNo,LP.SeqNo
   
   Open Cursor1
   Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc
   While @@FETCH_STATUS = 0
   Begin

    if not Exists(Select * from #PasPrices Where PageNo=@PageNo and Service=@Service and IncPack=@IncPack)
        Insert #PasPrices (PageNo,IncPack,Service,ServiceDesc)
         Values(@PageNo,@IncPack,@Service,@ServiceDesc)

    if @SeqNo=1
       Update #PasPrices Set Pax1Amount=IsNull(Pax1Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=2
       Update #PasPrices Set Pax2Amount=IsNull(Pax2Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=3
       Update #PasPrices Set Pax3Amount=IsNull(Pax3Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=4
       Update #PasPrices Set Pax4Amount=IsNull(Pax4Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=5
       Update #PasPrices Set Pax5Amount=IsNull(Pax5Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=6
       Update #PasPrices Set Pax6Amount=IsNull(Pax6Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack

    Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc
   End
   
   Close Cursor1
   DEALLOCATE Cursor1

  Set @PageNo=@PageNo+1
End --while

--Discount
Set @PageNo=1
While @PageNo<=@MaxPageNo
Begin
   Set @IncPack='N'

   Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
     Select C.CustNo,C.CalcAmount,LP.SeqNo,Service=C.SupDis,ServiceDesc=IsNull(dbo.FindLocalName(S.NameLID,@Market),S.Name)
     From ResSupDis C (NOLOCK)
     Join #Passengers LP on LP.CustNo=C.CustNo and LP.PageNo=@PageNo and LP.Status=0
     Join SupDis S (NOLOCK) on S.Code=C.SupDis
     Where C.ResNo=@ResNo and C.CalcAmount<>0 and C.SD='D'
     Order by C.CustNo,LP.SeqNo
   
   Open Cursor1
   Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc
   While @@FETCH_STATUS = 0
   Begin

    if not Exists(Select * from #PasPrices Where PageNo=@PageNo and Service=@Service and IncPack=@IncPack)
        Insert #PasPrices (PageNo,IncPack,Service,ServiceDesc)
         Values(@PageNo,@IncPack,@Service,@ServiceDesc)

    if @SeqNo=1
       Update #PasPrices Set Pax1Amount=IsNull(Pax1Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=2
       Update #PasPrices Set Pax2Amount=IsNull(Pax2Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=3
       Update #PasPrices Set Pax3Amount=IsNull(Pax3Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=4
       Update #PasPrices Set Pax4Amount=IsNull(Pax4Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=5
       Update #PasPrices Set Pax5Amount=IsNull(Pax5Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=6
       Update #PasPrices Set Pax6Amount=IsNull(Pax6Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack

    Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc
   End
   
   Close Cursor1
   DEALLOCATE Cursor1

  Set @PageNo=@PageNo+1
End --while


--Promotions
Set @PageNo=1
While @PageNo<=@MaxPageNo
Begin
   Set @IncPack='N'

   Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
     Select RP.CustNo,Amount=ABS(RP.Amount)*-1,LP.SeqNo,Service='.PROMO.',ServiceDesc=IsNull(dbo.FindLocalName(P.NameLID,@Market),P.Name) --20.04.2012 ic.added
     From ResPromo RP (NOLOCK)
     Join #Passengers LP on LP.CustNo=RP.CustNo and LP.PageNo=@PageNo and LP.Status=0
     Join Promo P (NOLOCK) on P.RecID=RP.PromoID
     Where RP.ResNo=@ResNo and RP.Amount<>0
     Order by RP.CustNo,LP.SeqNo
   
   Open Cursor1
   Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc
   While @@FETCH_STATUS = 0
   Begin

    if not Exists(Select * from #PasPrices Where PageNo=@PageNo and Service=@Service and IncPack=@IncPack)
        Insert #PasPrices (PageNo,IncPack,Service,ServiceDesc)
         Values(@PageNo,@IncPack,@Service,@ServiceDesc)

    if @SeqNo=1
       Update #PasPrices Set Pax1Amount=IsNull(Pax1Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=2
       Update #PasPrices Set Pax2Amount=IsNull(Pax2Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=3
       Update #PasPrices Set Pax3Amount=IsNull(Pax3Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=4
       Update #PasPrices Set Pax4Amount=IsNull(Pax4Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=5
       Update #PasPrices Set Pax5Amount=IsNull(Pax5Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
    Else
    if @SeqNo=6
       Update #PasPrices Set Pax6Amount=IsNull(Pax6Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack

    Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc
   End
   
   Close Cursor1
   DEALLOCATE Cursor1

  Set @PageNo=@PageNo+1
End --while

--SSR Code (Free) 08.09.2011 added
/* 07.09.2011'de eklenmişti ama Orhan Bey istemedi.

Set @PageNo=1
While @PageNo<=@MaxPageNo
Begin
   Set @IncPack='N'

   Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
     Select C.CustNo,0,LP.SeqNo,Service='.SSRCODE.',Name=IsNull(dbo.FindLocalName(S.NameLID,@Market),S.Name)
     From ResCon C (NOLOCK)
     Join SpecSerRQCode S (NOLOCK) on S.Code=C.SpecSerRQCode1
     Join #Passengers LP on LP.CustNo=C.CustNo and LP.PageNo=@PageNo and LP.Status=0
     Where ResNo=@ResNo and IsNull(C.SpecSerRQCode1,'')<>''
     and not Exists(Select * from ServiceExt Where SpecSerRQCode=C.SpecSerRQCode1)
   
   Open Cursor1
   Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc
   While @@FETCH_STATUS = 0
   Begin
     if not Exists(Select * from #PasPrices Where PageNo=@PageNo and Service=@Service and IncPack=@IncPack)
         Insert #PasPrices (PageNo,IncPack,Service,ServiceDesc)
          Values(@PageNo,@IncPack,@Service,@ServiceDesc)
 
     if @SeqNo=1
        Update #PasPrices Set Pax1Amount=IsNull(Pax1Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
     Else
     if @SeqNo=2
        Update #PasPrices Set Pax2Amount=IsNull(Pax2Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
     Else
     if @SeqNo=3
        Update #PasPrices Set Pax3Amount=IsNull(Pax3Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
     Else
     if @SeqNo=4
        Update #PasPrices Set Pax4Amount=IsNull(Pax4Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
     Else
     if @SeqNo=5
        Update #PasPrices Set Pax5Amount=IsNull(Pax5Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
     Else
     if @SeqNo=6
        Update #PasPrices Set Pax6Amount=IsNull(Pax6Amount,0)+@SalePrice Where Service=@Service and PageNo=@PageNo and IncPack=@IncPack
 
     Fetch Next FROM Cursor1 INTO @CustNo,@SalePrice,@SeqNo,@Service,@ServiceDesc
   End
   
   Close Cursor1
   DEALLOCATE Cursor1

  Set @PageNo=@PageNo+1
End --while
*/

--Zero Invoice Amount (16.03.2011 Added)
if @ZeroInv=1 
Begin
  Update #PasPrices 
  Set Pax1Amount=0,Pax2Amount=0,Pax3Amount=0,Pax4Amount=0,Pax5Amount=0,Pax6Amount=0
 
  Delete #ResPayPlan Where PayNo>1
  Update #ResPayPlan Set RemAmount=0,Amount=0
 
  if not Exists(Select * from #ResPayPlan)
   Insert Into #ResPayPlan 
     (ResNo,PayNo,DueDate,RemAmount)
   Select ResNo,1,ResDate,0 From ResMain Where ResNo=@ResNo
end

--30.05.2013 17:42 ic.added
if (@SaleResource in (2,3,5,7)) and 
   (Select Count(*) from #PasPrices Where ServiceType='FLIGHT')=1
  if @ResStat in (2,3)
  Update Top(1) #PasPrices
  Set ServiceDesc=@BasisPriceStr
  Where ServiceType='FLIGHT'

--02.03.2103 11:30 IC.added
if (@SaleResource in (2,3,5,7)) and 
   (Select Count(*) from #PasPrices Where ServiceType='FLIGHT') > 1
Begin
  Declare @FirstID int, @PageNo_Key smallint

  Declare PageNo_Cursor CURSOR LOCAL FORWARD_ONLY STATIC FOR --08.07.2013 ic.added
    Select Distinct PageNo from #PasPrices Order by PageNo
  
  Open PageNo_Cursor
  Fetch Next FROM PageNo_Cursor INTO @PageNo_Key
  While @@FETCH_STATUS = 0
  Begin

    Select Top 1 @FirstID=RecID from #PasPrices
    Where PageNo=@PageNo_Key and ServiceType='FLIGHT'
    Order by PageNo, RecID
  
    Update #PasPrices
    Set Pax1Amount=(Select Sum( Pax1Amount ) From #PasPrices P Where PageNo=@PageNo_Key and ServiceType='FLIGHT' and Pax1Amount is not null),
        Pax2Amount=(Select Sum( Pax2Amount ) From #PasPrices P Where PageNo=@PageNo_Key and ServiceType='FLIGHT' and Pax2Amount is not null),
        Pax3Amount=(Select Sum( Pax3Amount ) From #PasPrices P Where PageNo=@PageNo_Key and ServiceType='FLIGHT' and Pax3Amount is not null),
        Pax4Amount=(Select Sum( Pax4Amount ) From #PasPrices P Where PageNo=@PageNo_Key and ServiceType='FLIGHT' and Pax4Amount is not null),
        Pax5Amount=(Select Sum( Pax5Amount ) From #PasPrices P Where PageNo=@PageNo_Key and ServiceType='FLIGHT' and Pax5Amount is not null),
        Pax6Amount=(Select Sum( Pax6Amount ) From #PasPrices P Where PageNo=@PageNo_Key and ServiceType='FLIGHT' and Pax6Amount is not null)
    Where PageNo=@PageNo_Key and ServiceType='FLIGHT' and recID=@FirstID
  
    --30.05.2013 17:42 ic.added
    if @ResStat in (2,3)
    Update #PasPrices
    Set ServiceDesc=@BasisPriceStr
    Where PageNo=@PageNo_Key and ServiceType='FLIGHT' and recID=@FirstID
  
    Update #PasPrices
    Set Pax1Amount=Null,
        Pax2Amount=Null,
        Pax3Amount=Null,
        Pax4Amount=Null,
        Pax5Amount=Null,
        Pax6Amount=Null
    Where PageNo=@PageNo_Key and ServiceType='FLIGHT' and recID<>@FirstID

   Fetch Next FROM PageNo_Cursor INTO @PageNo_Key
  End
  
  Close PageNo_Cursor
  DEALLOCATE PageNo_Cursor
end

Set NOCOUNT OFF

--Resultsss
Select * From #Passengers

Select * from #PasPrices
Order by PageNo, IncPack Desc

Select * From #ResPayPlan

Select * From #ServicePrices

Select GiroKID=dbo.OCRGiroKIDCalc(@ResNo)

Select RM.ResDate, RM.Agency, AgencyUserName=(Case When isnull(AU.Name, '') = '' Then U.Name Else AU.Name End), 
	   RM.PasPayable, RM.ReceiveDate, RM.ResNote, RM.SaleResource, AgencyName=isnull(dbo.FindLocalName(A.NameLID, RM.Market), A.Name),
       RM.Holpack, RM.BegDate, RM.EndDate, RM.ArrCity, RM.DepCity, ArrCountry=dbo.GetCountry(RM.ArrCity),RM.ResStat 
From ResMain (NOLOCK) RM
LEFT JOIN AgencyUser (NOLOCK) AU ON AU.Agency=RM.Agency And AU.Code=RM.AgencyUser
LEFT JOIN Users (NOLOCK) U ON U.Code=RM.AuthorUser 
JOIN Agency (NOLOCK) A ON A.Code=RM.Agency
Where ResNo=@ResNo

Select Ssrc=Name+' *('+LTrim(Str(count(*)))+')'
From (
Select CustNo,Name=IsNull(dbo.FindLocalName(NameLID,@Market),SSR.Name)
from ResCon C (NOLOCK)
Join SpecSerRQCode SSR (NOLOCK) on SSR.Code in (C.SpecSerRQCode1,C.SpecSerRQCode2)
Where ResNo=@ResNo) x
Group By Name
 ";
        #endregion

        #region DeturFin_Calc_Items_Qry
        string DeturFin_Calc_Items_Qry = @"                                        
Declare @ResStat smallint
Set NOCOUNT ON

--Payment Plan items --
Declare @AgencyPayment dec(18,2),@RemAmount dec(18,2),@Amount dec(18,2), @PayNo int, @DueDate datetime, @str nvarchar(250), @PasBalance dec(18,2), @HasSupplement bit

Select @ResStat=ResStat,@AgencyPayment=AgencyPayment,@Market=Market,
       @PasBalance=IsNull(PasBalance,0)
From ResMain (NOLOCK) Where ResNo=@ResNo

if @ResStat=2 and @PasBalance<>0 Set @ResStat=3

--Pas.EB Description 27.04.2011 added
Set @str=''
Select @str=P.Description From ResMain R (NOLOCK)
Join PasEB P (NOLOCK) on P.RecID=R.EBPasID
Where ResNo=@ResNo
if @str<>'' Set @PasEBStr=@str

--PL.Spo Description
Set @str=''
Select @str=Spo.Description from ResCustPrice P (NOLOCK)
Join ResService S on S.ResNo=P.ResNo and S.RecID=P.ServiceID and S.ServiceType='FLIGHT' and S.StatSer in (0,1) and PlSpoVal<>0
Join CatPriceSPo (NOLOCK) SPo on Spo.RecID=P.PLSpoNo
Where P.ResNo=@ResNo
if @str<>'' Set @PLSpoStr=@str
--18.10.2011 i.c. added
Select Top 1 @HasSupplement=Case When C.ppPLSpoVal>0 then 1 else 0 end  
From ResCust C (NOLOCK)
Where C.ResNo=@ResNo and C.ppPLSpoVal<>0 and C.Status=0
Set @HasSupplement=IsNull(@HasSupplement,0)

Insert Into #ResPayPlan 
 (ResNo,PayNo,DueDate,Amount,Cur,Adult,Child,Infant,PayAmount,RemAmount)
Select ResNo,PayNo,DueDate,Amount,Cur,Adult,Child,Infant,0,0 From ResPayPlan (NOLOCK) Where ResNo=@ResNo
Set @RemAmount=@AgencyPayment

Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
  Select PayNo,Amount From #ResPayPlan Order by PayNo

Open Cursor1
Fetch Next FROM Cursor1 INTO @PayNo,@Amount
While @@FETCH_STATUS = 0
Begin

 if @RemAmount>0
 if @RemAmount>=@Amount
 Begin
   Update #ResPayPlan Set PayAmount=@Amount Where PayNo=@PayNo
   Set @RemAmount=@RemAmount-@Amount
 End
 Else 
 Begin
   Update #ResPayPlan Set PayAmount=@RemAmount Where PayNo=@PayNo
   Set @RemAmount=0
 end
 if @RemAmount<=0 Break

 Fetch Next FROM Cursor1 INTO @PayNo,@Amount
End
Close Cursor1
DEALLOCATE Cursor1

Update #ResPayPlan Set RemAmount=Amount-PayAmount

Truncate Table #PayPlan
Insert Into #PayPlan (DueDate_L1,RemAmount_L1) Values(Null,Null) --Insert Empty line

Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
 Select PayNo=ROW_NUMBER() OVER(ORDER BY Duedate),Duedate,RemAmount=Sum(RemAmount) from #ResPayPlan
 Group by Duedate
 Order by Duedate

Open Cursor1
Fetch Next FROM Cursor1 INTO @PayNo,@DueDate,@RemAmount
While @@FETCH_STATUS = 0
Begin
 if @PayNo=1
   Update top (1) #PayPlan Set DueDate_L1=@DueDate,RemAmount_L1=@RemAmount

 if @PayNo=2
   Update top (1) #PayPlan Set DueDate_L2=@DueDate,RemAmount_L2=@RemAmount

 if @PayNo=3
   Update top (1) #PayPlan Set DueDate_L3=@DueDate,RemAmount_L3=@RemAmount

 Fetch Next FROM Cursor1 INTO @PayNo,@DueDate,@RemAmount
End
Close Cursor1
DEALLOCATE Cursor1

if @PayNo>0
 Update #PayPlan Set Cur=(Select Top 1 Cur From #ResPayPlan)

Declare @CustNo int, @SalePrice dec(18,2),@SeqNo smallint, @Unit int,@ServiceID int,@ExtServiceID int,
@IncPack varchar(1),@Service varchar(10), @ServiceDesc nvarchar(200),@BegDate datetime

--Insert Into #Passengers (CustNo,Name,Birthdate)
-- Select CustNo,dbo.GetTitle(Title)+'.'+Surname+' '+Name,Birtday From ResCust Where ResNo=@ResNo and Status=0

Insert Into #Passengers (CustNo,Surname, Name,Birthdate,Title,Age,TitleNo,Leader,Status)
 Select CustNo,Surname,Name,Birtday,Title=T.Code,C.Age,TitleNo=C.Title,C.Leader,C.Status
 From ResCust C (NOLOCK)
 Left Join Title T (NOLOCK) on T.TitleNo=C.Title
 Where ResNo=@ResNo and Status=0
Update #Passengers Set PageNo= CEILING(RecID * 1.0 / 6)

Update #Passengers
Set SeqNo=xSeqNo
From
(Select xCustNo=CustNo,xSeqNo=ROW_NUMBER() OVER (PARTITION BY PageNo ORDER BY PageNo) From #Passengers) x
Where CustNo=X.xCustNo

Select P.*,IncPack,T.TitleGrp
Into #ResCustPrice
From ResCustPrice P
Outer Apply(Select IncPack From ResService (NOLOCK) Where ResService.ResNo=P.ResNo and ResService.RecID=P.ServiceID) S
Outer Apply(Select Title From ResCust (NOLOCK) Where ResCust.ResNo=P.ResNo and CustNo=P.CustNo) C
Outer Apply(Select TitleGrp=Case When TitleNo<=5 then 0 When TitleNo between 6 and 7 then 1 When TitleNo>=8 then 2 end From Title (NOLOCK) Where TitleNo=C.Title) T
Where ResNo=@ResNo and (P.SalePrice<>0 or @ZeroInv=1)
if @HasSupplement=1 --18.10.2011 i.c. added
 Update #ResCustPrice Set PLSpoVal=0

--if @@ROWCOUNT=0 and 
if @ResStat=3 --CancelX --09.04.2011'de bad women ile bareber yapıldı!!!
Begin
  Select @Amount=Sum(SalePrice) from ResService Where ResNo=@ResNo and SalePrice>0

 --CancelX amount
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
  Select 7,'N',Unit=1,'CANX','Peruutusmaksu',@Amount
End

Update #ResCustPrice
set IncPack=E.IncPack
From ResServiceExt E
Where E.ResNo=@ResNo and E.ServiceID=#ResCustPrice.ServiceID and E.RecID=#ResCustPrice.ExtServiceID

Update #ResCustPrice Set ExtBed=0 Where ExtBed is null

if Exists(Select * from #ResCustPrice Where ExtBed=1)
Begin
 Update #ResCustPrice Set ExtBed=1
 Where IsNull(ExtBed,0)=0 and CustNo in (Select CustNo from #ResCustPrice Where ExtBed=1)
End

----- PL SPO Amount ----------------------
if @HasSupplement=0
BEGIN
  Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
   Select 6,'Y',Unit=Count(TitleGrp),'',@PLSpoStr,PLSpoVal=ABS(PLSpoVal)*-1
   From
   (Select TitleGrp,PLSpoVal=Sum(IsNull(ppPLSpoVal,0)) From ResCust C (NOLOCK)
    Outer Apply(Select TitleGrp=Case When TitleNo<=5 then 0 When TitleNo between 6 and 7 then 1 When TitleNo>=8 then 2 end From Title (NOLOCK) Where TitleNo=C.Title) T
    Where C.ResNo=@ResNo
    Group by TitleGrp,CustNo
   ) P
   Where PLSpoVal<>0
   Group By TitleGrp,PLSpoVal
   Order by TitleGrp
END

----- Include Package Items ----------------------
Select @Unit=0,@SalePrice=0
 --Adult Base price
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
  Select 0,'Y',Unit=Count(TitleGrp),'','Aikuinen/perushinta',SalePrice
  From 
  (Select TitleGrp,SalePrice=Sum(P.SalePrice-IsNull(P.PLSpoVal,0)) From #ResCustPrice P (NOLOCK)
   Where Incpack='Y' and ExtBed=0 and TitleGrp=0
   Group by TitleGrp,CustNo
  ) P
  Group By TitleGrp,SalePrice
  Order by TitleGrp

 --Adult Extra Bed price
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
  Select 0,'Y',Unit=Count(TitleGrp),'','Aikuinen lisavuoteella',SalePrice
  From 
  (Select TitleGrp,SalePrice=Sum(P.SalePrice-IsNull(P.PLSpoVal,0)) From #ResCustPrice P (NOLOCK)
   Where Incpack='Y' and ExtBed=1 and TitleGrp=0
   Group by TitleGrp,CustNo
  ) P
  Group By TitleGrp,SalePrice
  Order by TitleGrp

 --Child Base price
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
  Select 0,'Y',Unit=Count(TitleGrp),'','Lapsi 2-15',SalePrice
  From
  (Select TitleGrp,SalePrice=Sum(P.SalePrice-IsNull(P.PLSpoVal,0)) From #ResCustPrice P (NOLOCK)
   Where Incpack='Y' and ExtBed=0 and TitleGrp=1
   Group by TitleGrp,CustNo
  ) P
  Group By TitleGrp,SalePrice
  Order by TitleGrp

 --Infan Base price
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
  Select 0,'Y',Unit=Count(TitleGrp),'','Lapsi 0-2',SalePrice
  From
  (Select TitleGrp,SalePrice=Sum(P.SalePrice-IsNull(P.PLSpoVal,0)) From #ResCustPrice P (NOLOCK)
   Where Incpack='Y' and ExtBed=0 and TitleGrp=2
   Group by TitleGrp,CustNo
  ) P
  Group By TitleGrp,SalePrice
  Order by TitleGrp

----- Out of Package Items ----------------------
   Set @IncPack='N'
   Declare Cursor1 CURSOR LOCAL FORWARD_ONLY STATIC FOR
     Select Unit=Count(TitleGrp), P.SalePrice,S.Service,S.BegDate,P.ServiceID,P.ExtServiceID
     From ResService S (NOLOCK)
     Join #ResCustPrice P on P.ServiceID=S.RecID and P.IncPack=@IncPack
     Where S.ResNo=@ResNo and S.IncPack=@IncPack and P.ExtServiceID=0
     and not Exists(Select * from AdService A Where A.Service=S.ServiceType and A.Code=S.Service and A.SerType=1)
     Group by S.Service,S.BegDate,P.SalePrice,P.ServiceID,P.ExtServiceID

     Union ALL  
  
     Select Unit=Count(TitleGrp), P.SalePrice,Service=S.ExtService,S.BegDate,P.ServiceID,P.ExtServiceID
     From ResServiceExt S (NOLOCK)
     Join #ResCustPrice P on P.ServiceID=S.ServiceID and P.ExtServiceID=S.RecID and P.IncPack=@IncPack
     Where S.ResNo=@ResNo and S.IncPack=@IncPack
     Group by S.ExtService,S.BegDate,P.SalePrice,P.ServiceID,P.ExtServiceID
     Order by S.BegDate
   
   Open Cursor1
   Fetch Next FROM Cursor1 INTO @Unit,@SalePrice,@Service,/*@ServiceDesc,*/ @BegDate,@ServiceID,@ExtServiceID
   While @@FETCH_STATUS = 0
   Begin
     if @ExtServiceID>0
       Select @ServiceDesc= (Select Name+
                             Case When StatConf=0 then ' '+@RequestStr else '' end
                             From ServiceExt Where Code=S.ExtService)
       From ResServiceExt S (NOLOCK)
       Where RecID=@ExtServiceID
     Else
       Select @ServiceDesc=
               Case S.ServiceType
                When 'HOTEL' then (Select Name+' ('+Category+'),'+(Select Name From Location (NOLOCK) Where RecID=Hotel.Location) From Hotel (NOLOCK) Where Code=Service)
                When 'FLIGHT' then (Select IsNull(PNLName,FlightNo)+' ('+DepAirport+'->'+ArrAirport+'),'+FlgClass From FlightDay (NOLOCK) Where FlightNo=Service and FlyDate=S.BegDate)
                When 'TRANSPORT' then (Select TP.Name+' ('+dbo.ufn_GetLocationName(S.DepLocation)+'-'+dbo.ufn_GetLocationName(S.ArrLocation)+') ' From Transport TP (NOLOCK)
                                       Join TransportDay TPD (NOLOCK) on TPD.Transport=TP.Code and TPD.TransDate=S.BegDate and TPD.Bus=S.Bus
                                       Where TP.Code=S.Service
                                       )
                When 'RENTING' then (Select Name+' (Category:'+Category+') / '+(Select Name From Location (NOLOCK) Where RecId=DepLocation) From Renting (NOLOCK) Where Code=Service)
                When 'EXCURSION' then (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name) From Excursion (NOLOCK) Where Code=Service)
                When 'INSURANCE' then (Select Name From Insurance (NOLOCK) Where Code=Service)
                When 'VISA' then (Select Name From Visa (NOLOCK) Where Code=Service)
                When 'TRANSFER' then (Select Name+', '+dbo.ufn_GetLocationName(S.ArrLocation) From Transfer (NOLOCK) Where Code=Service)
                When 'HANDFEE' then (Select Name+', '+dbo.ufn_GetLocationName(S.ArrLocation) From HandFee (NOLOCK) Where Code=Service)
                else (Select IsNull(dbo.FindLocalName(NameLID,@Market),Name) From AdService (NOLOCK) Where AdService.Service=S.ServiceType and Code=S.Service)
               End+
               Case When StatConf=0 then ' '+@RequestStr else '' end
       From ResService S (NOLOCK) Where RecID=@ServiceID

     Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
         Values(1,'N',@Unit,@Service,@ServiceDesc,@SalePrice)


    Fetch Next FROM Cursor1 INTO @Unit,@SalePrice,@Service,/*@ServiceDesc,*/ @BegDate,@ServiceID,@ExtServiceID
   End
   
   Close Cursor1
   DEALLOCATE Cursor1

-- Agency Service Fee (ayrı satırda per service olarak yazılacak)
 Set @IncPack='N'
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
   Select 5,'N',Unit=S.Unit,'',Name=IsNull(dbo.FindLocalName(A.NameLID,@Market),A.Name),S.SalePrice
   From ResService S (NOLOCK)
   Join AdService A (NOLOCK) on A.Service=S.ServiceType and A.Code=S.Service and A.SerType=1
   Where S.ResNo=@ResNo and S.IncPack=@IncPack and S.SalePrice>0 and S.StatSer in (0,1)

-- PEB
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
  Select 4,'N',Unit=Count(TitleGrp),'',@PasEBStr,PasEB=ABS(PasEB)*-1
  From 
  (Select TitleGrp,PasEB=Sum(IsNull(ppPasEB,0)) From ResCust C (NOLOCK)
   Outer Apply(Select TitleGrp=Case When TitleNo<=5 then 0 When TitleNo between 6 and 7 then 1 When TitleNo>=8 then 2 end From Title (NOLOCK) Where TitleNo=C.Title) T
   Where C.ResNo=@ResNo
   Group by TitleGrp,CustNo
  ) P
  Where PasEB<>0
  Group By TitleGrp,PasEB
  Order by TitleGrp

-- Supplement / Discount
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
   Select 2,'N',1,RSD.SupDis,Name=IsNull(dbo.FindLocalName(SD.NameLID,@Market),Name),CalcAmount 
   From ResSupDis RSD
   Join SupDis SD (NOLOCK) on SD.Code=RSD.SupDis
   Join ResMain R (NOLOCK) on R.ResNo=RSD.ResNo and R.ResStat in (0,1) --09.04.2011'de eklendi
   Where RSD.ResNo=@ResNo and RSD.ApplyType='P' and RSD.Ok='Y'

-- Promotions
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
   Select 3,'N',1,RP.PromoID,Name=IsNull(dbo.FindLocalName(P.NameLID,@Market),Description),Amount=Abs(Amount)*-1
   From ResPromo RP
   Join Promo P (NOLOCK) on P.RecID=RP.PromoID
   Where ResNo=@ResNo and RP.Confirmed=1

--SSR Code (Free) 08.09.2011 added
 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
   Select 8,'N',Unit=Count(*),SpecSerRQCode1,Name,0
   From
   (
   Select C.CustNo,C.SpecSerRQCode1,Name=IsNull(dbo.FindLocalName(S.NameLID,@Market),S.Name)
   From ResCon C (NOLOCK)
   Join SpecSerRQCode S (NOLOCK) on S.Code=C.SpecSerRQCode1
   Where ResNo=@ResNo
   and IsNull(SpecSerRQCode1,'')<>''
   and not Exists(Select * from ServiceExt Where SpecSerRQCode=SpecSerRQCode1)
   ) x
   Group by SpecSerRQCode1,Name

 Insert Into #ServicePrices (RecType,IncPack,Unit,Service,ServiceDesc,SalePrice)
   Select 8,'N',Unit=Count(*),SpecSerRQCode2,Name,0
   From
   (
   Select C.CustNo,C.SpecSerRQCode2,Name=IsNull(dbo.FindLocalName(S.NameLID,@Market),S.Name)
   From ResCon C (NOLOCK)
   Join SpecSerRQCode S (NOLOCK) on S.Code=C.SpecSerRQCode2
   Where ResNo=@ResNo
   and IsNull(SpecSerRQCode2,'')<>''
   and not Exists(Select * from ServiceExt Where SpecSerRQCode=SpecSerRQCode2)
   ) x
   Group by SpecSerRQCode2,Name



--Zero Invoice Amount (16.03.2011 Added)
if @ZeroInv=1 
Begin
 Update #ResMain Set PasPayable=0
 Update #ServicePrices Set SalePrice=0

 Update #PayPlan Set RemAmount_L1=0,RemAmount_L2=0,RemAmount_L3=0

 if not Exists(Select * from #PayPlan)
   Insert Into #PayPlan (DueDate_L1,RemAmount_L1) Values(Null,Null) --Insert Empty line
end


if @ResStat=3
begin
 Delete From #ServicePrices Where Service<>'CANX'
end

if (@ResStat=3) or (@ResStat<=1 and @PasBalance<0)
Begin
 Delete From #ResPayPlan Where RecID>1
 Update #ResPayPlan Set DueDate=Null, Amount=@PasBalance, PayAmount=Null

 Update #PayPlan
 Set DueDate_L1=Null, RemAmount_L1=@PasBalance,
     DueDate_L2=Null, RemAmount_L2=Null,DueDate_L3=Null, RemAmount_L3=Null
End

Select * From #Passengers

Select * from #PasPrices
Order by PageNo, IncPack Desc

Select * From #ResPayPlan

Select * From #ServicePrices

Select GiroKID=dbo.DetBankPayRef(@ResNo)

Select RM.ResDate, RM.Agency, AgencyUserName=(Case When isnull(AU.Name, '') = '' Then U.Name Else AU.Name End), 
	   RM.PasPayable, RM.ReceiveDate, RM.ResNote, RM.SaleResource, AgencyName=isnull(dbo.FindLocalName(A.NameLID, RM.Market), A.Name),
       RM.Holpack, RM.BegDate, RM.EndDate, RM.ArrCity, RM.DepCity, ArrCountry=dbo.GetCountry(RM.ArrCity),RM.ResStat
From ResMain (NOLOCK) RM
LEFT JOIN AgencyUser (NOLOCK) AU ON AU.Agency=RM.Agency And AU.Code=RM.AgencyUser
LEFT JOIN Users (NOLOCK) U ON U.Code=RM.AuthorUser 
JOIN Agency (NOLOCK) A ON A.Code=RM.Agency
Where ResNo=@ResNo ";

        #endregion

        public List<ResServiceRecord> getResService(string ResNo, ref string errorMsg)
        {
            List<ResServiceRecord> records = new List<ResServiceRecord>();
            string tsql = @"Select ServiceType, Service, DepLocation, ArrLocation, BegDate, EndDate, Room, Board, Accom, FlgClass, SupNote 
                            From ResService (NOLOCK)
                            Where ResNo=@ResNo And StatSer<2 ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ResServiceRecord record = new ResServiceRecord();
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.ServiceType = Conversion.getStrOrNull(R["ServiceType"]);
                        record.DepLocation = Conversion.getInt32OrNull(R["DepLocation"]);
                        record.ArrLocation = Conversion.getInt32OrNull(R["ArrLocation"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.Board = Conversion.getStrOrNull(R["Board"]);
                        record.Accom = Conversion.getStrOrNull(R["Accom"]);
                        record.FlgClass = Conversion.getStrOrNull(R["FlgClass"]);
                        record.SupNote = Conversion.getStrOrNull(R["SupNote"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return records;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<PassengersRecord> getPassengers(DataTable data)
        {
            List<PassengersRecord> records = new List<PassengersRecord>();
            if (data == null || data.Rows.Count < 1) return records;
            foreach (DataRow row in data.Rows)
            {
                PassengersRecord record = new PassengersRecord();
                record.RecID = (int)row["RecID"];
                record.CustNo = Conversion.getInt32OrNull(row["CustNo"]);
                record.Name = Conversion.getStrOrNull(row["Name"]);
                record.Surname = Conversion.getStrOrNull(row["Surname"]);
                record.Birthdate = Conversion.getDateTimeOrNull(row["Birthdate"]);
                record.PageNo = Conversion.getInt16OrNull(row["PageNo"]);
                record.SeqNo = Conversion.getInt16OrNull(row["SeqNo"]);
                record.Leader = Conversion.getStrOrNull(row["Leader"]);
                record.Age = Conversion.getInt16OrNull(row["Age"]);
                record.Title = Conversion.getStrOrNull(row["Title"]);
                record.TitleNo = Conversion.getInt16OrNull(row["TitleNo"]);
                record.Status = Conversion.getByteOrNull(row["Status"]);
                records.Add(record);
            }
            return records;
        }

        public List<PasPricesRecord> getPasPrices(DataTable data)
        {
            List<PasPricesRecord> records = new List<PasPricesRecord>();
            if (data == null || data.Rows.Count < 1) return records;
            foreach (DataRow row in data.Rows)
            {
                PasPricesRecord record = new PasPricesRecord();
                record.RecID = (int)row["RecID"];
                record.Service = Conversion.getStrOrNull(row["Service"]);                
                record.ServiceDesc = Conversion.getStrOrNull(row["ServiceDesc"]);
                record.IncPack = Conversion.getStrOrNull(row["IncPack"]);
                record.PageNo = Conversion.getInt16OrNull(row["PageNo"]);
                record.Pax1Amount = Conversion.getDecimalOrNull(row["Pax1Amount"]);
                record.Pax2Amount = Conversion.getDecimalOrNull(row["Pax2Amount"]);
                record.Pax3Amount = Conversion.getDecimalOrNull(row["Pax3Amount"]);
                record.Pax4Amount = Conversion.getDecimalOrNull(row["Pax4Amount"]);
                record.Pax5Amount = Conversion.getDecimalOrNull(row["Pax5Amount"]);
                record.Pax6Amount = Conversion.getDecimalOrNull(row["Pax6Amount"]);
                records.Add(record);
            }
            return records;
        }

        public List<ResPayPlanRecord> getResPayPlan(DataTable data)
        {
            List<ResPayPlanRecord> records = new List<ResPayPlanRecord>();
            if (data == null || data.Rows.Count < 1) return records;
            foreach (DataRow row in data.Rows)
            {
                ResPayPlanRecord record = new ResPayPlanRecord();

                record.RecID = (int)row["RecID"];
                record.ResNo = Conversion.getStrOrNull(row["ResNo"]);
                record.PayNo = Conversion.getInt16OrNull(row["PayNo"]);
                record.DueDate = Conversion.getDateTimeOrNull(row["DueDate"]);
                record.Amount = Conversion.getDecimalOrNull(row["Amount"]);
                record.Cur = Conversion.getStrOrNull(row["Cur"]);
                record.Adult = Conversion.getDecimalOrNull(row["Adult"]);
                record.Child = Conversion.getDecimalOrNull(row["Child"]);
                record.Infant = Conversion.getDecimalOrNull(row["Infant"]);
                record.PayAmount = Conversion.getDecimalOrNull(row["PayAmount"]);
                record.RemAmount = Conversion.getDecimalOrNull(row["RemAmount"]);
                records.Add(record);
            }
            return records;
        }

        public List<ServicePricesRecord> getServicePrices(DataTable data)
        {
            List<ServicePricesRecord> records = new List<ServicePricesRecord>();
            if (data == null || data.Rows.Count < 1) return records;
            foreach (DataRow row in data.Rows)
            {
                ServicePricesRecord record = new ServicePricesRecord();
                record.RecID = (int)row["RecID"];
                record.RecType = Conversion.getInt16OrNull(row["RecType"]);
                record.IncPack = Conversion.getStrOrNull(row["IncPack"]);
                record.Unit = Conversion.getInt16OrNull(row["Unit"]);
                record.Service = Conversion.getStrOrNull(row["Service"]);
                record.ServiceDesc = Conversion.getStrOrNull(row["ServiceDesc"]);
                record.SalePrice = Conversion.getDecimalOrNull(row["SalePrice"]);
                record.BeginDate = Conversion.getDateTimeOrNull(row["BegDate"]);
                record.Room = Conversion.getStrOrNull(row["Room"]);
                record.Board = Conversion.getStrOrNull(row["Board"]);
                record.ServiceType = Conversion.getStrOrNull(row["ServiceType"]);
                records.Add(record);
            }
            return records;
        }

        public List<string> getSsrc(DataTable data)
        {
            List<string> records = new List<string>();
            if (data == null || data.Rows.Count < 1) return records;
            foreach (DataRow row in data.Rows)
                records.Add(Conversion.getStrOrNull(row["Ssrc"]));
            return records;
        }

        public HotelRoomRecord getHotelRoom(string Market, string Hotel, string Room, ref string errorMsg)
        {
            HotelRoomRecord record = new HotelRoomRecord();
            string tsql = @"Select RecID, Hotel, Code, Name, 
	                            NameL=isnull(dbo.FindLocalName(NameLID, @Market), (Select isnull(dbo.FindLocalName(NameLID, @Market), HotelRoom.Name) From Room (NOLOCK) Where Code=HotelRoom.Code)),
	                            NameS, DispNo=isnull(DispNo, 9999), BaseRoom
                            From HotelRoom (NOLOCK)
                            Where Hotel=@Hotel And Code=@Code
                            Order By DispNo ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Code", DbType.String, Room);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.BaseRoom = Conversion.getStrOrNull(R["BaseRoom"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                    }
                }
                return record;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public HotelBoardRecord getHotelBoard(string Market, string Hotel, string Board, ref string errorMsg)
        {
            HotelBoardRecord record = new HotelBoardRecord();
            string tsql = @"Select RecID, Hotel, Code, Name, 
	                            NameL=isnull(dbo.FindLocalName(NameLID, @Market), (Select isnull(dbo.FindLocalName(NameLID, @Market), HotelBoard.Name) From Board (NOLOCK) Where Code=HotelBoard.Code)),
	                            NameS, DispNo, BegDate, EndDate
                            From HotelBoard (NOLOCK)
                            Where Hotel=@Hotel And Code=@Code ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Code", DbType.String, Board);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.RecID = (int)R["RecID"];
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.NameL = Conversion.getStrOrNull(R["NameL"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                    }
                }
                return record;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public HotelAccomRecord getHotelAccom(string Market, string Hotel, string Room, string Code, ref string errorMsg)
        {
            HotelAccomRecord record = new HotelAccomRecord();
            string tsql = string.Empty;
            tsql = @"   Select Hotel, Room, Code, [Name], 
                            LocalName=isnull(dbo.FindLocalName(NameLID, @Market), (Select ISNULL(dbo.FindLocalName(NameLID, @Market) ,HotelAccom.[Name]) From RoomAccom (NOLOCK) Where Code=HotelAccom.Code)),
                            NameS, StdAdl, MinAdl, MaxAdl, MaxChd, MaxPax, DispNo
                        From HotelAccom (NOLOCK)
                        Where Hotel=@Hotel 
                          And Room=@Room
                          And Code=@Code ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Hotel", DbType.String, Hotel);
                db.AddInParameter(dbCommand, "Room", DbType.String, Room);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.StdAdl = Conversion.getInt16OrNull(R["StdAdl"]);
                        record.MinAdl = Conversion.getInt16OrNull(R["MinAdl"]);
                        record.MaxAdl = Conversion.getInt16OrNull(R["MaxAdl"]);
                        record.MaxChd = Conversion.getInt16OrNull(R["MaxChd"]);
                        record.MaxPax = Conversion.getInt16OrNull(R["MaxPax"]);
                        record.DispNo = Conversion.getInt16OrNull(R["DispNo"]);
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;

            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ReportResMainRec getResMainRec(DataTable data)
        {
            ReportResMainRec record = new ReportResMainRec();
            if (data == null || data.Rows.Count < 1) return record;
            foreach (DataRow row in data.Rows)
            {
                record.AgencyUserName = Conversion.getStrOrNull(row["AgencyUserName"]);
                record.AgencyName = Conversion.getStrOrNull(row["AgencyName"]);
                record.ResDate = Conversion.getDateTimeOrNull(row["ResDate"]);
                record.PasPayable = Conversion.getDecimalOrNull(row["PasPayable"]);
                record.ReceiveDate = Conversion.getDateTimeOrNull(row["ReceiveDate"]);
                record.ResNote = Conversion.getStrOrNull(row["ResNote"]);
                record.SaleResource = Conversion.getInt16OrNull(row["SaleResource"]);
                record.Holpack = Conversion.getStrOrNull(row["Holpack"]);
                record.BegDate = Conversion.getDateTimeOrNull(row["BegDate"]);
                record.DepCity = Conversion.getInt32OrNull(row["DepCity"]);
                record.ArrCity = Conversion.getInt32OrNull(row["ArrCity"]);
                record.ArrCountry = Conversion.getInt32OrNull(row["ArrCountry"]);
                record.ResStat = Conversion.getInt16OrNull(row["ResStat"]);
            }
            return record;
        }

        public ResCustInfoRecord getResCustInfo(string Market, int? CustNo, ref string errorMsg)
        {
            string tsql = @"Select RCI.RecID, RCI.CustNo, RCI.CTitle, CTitleName=(Select Code From Title (NOLOCK) Where TitleNo=RCI.CTitle), RCI.CName, RCI.CSurName, 
                                RCI.AddrHome, RCI.AddrHomeCity, RCI.AddrHomeZip, RCI.AddrHomeTel, RCI.AddrHomeFax, RCI.AddrHomeEmail, RCI.AddrHomeCountry, RCI.HomeTaxOffice, RCI.HomeTaxAccNo,
                                RCI.AddrWork, RCI.AddrWorkCity, RCI.AddrWorkZip, RCI.AddrWorkTel, RCI.AddrWorkFax, RCI.AddrWorkEmail, RCI.AddrWorkCountry, RCI.WorkTaxOffice, RCI.WorkTaxAccNo,
                                RCI.ContactAddr, RCI.MobTel, RCI.Jobs, RCI.Note,  RCI.InvoiceAddr, RCI.WorkFirmName, RCI.Bank, RCI.BankAccNo, RCI.BankIBAN
                            From ResCustInfo RCI (NOLOCK)                            
                            Where RCI.CustNo= @CustNo";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "CustNo", DbType.Int32, CustNo);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        ResCustInfoRecord record = new ResCustInfoRecord();
                        record.RecID = (int)rdr["RecID"];
                        record.CustNo = (int)rdr["CustNo"];
                        record.CTitle = Conversion.getInt16OrNull(rdr["CTitle"]);
                        record.CTitleName = Conversion.getStrOrNull(rdr["CTitleName"]);
                        record.CName = Conversion.getStrOrNull(rdr["CName"]);
                        record.CSurName = Conversion.getStrOrNull(rdr["CSurName"]);
                        record.AddrHome = Conversion.getStrOrNull(rdr["AddrHome"]);
                        record.AddrHomeCity = Conversion.getStrOrNull(rdr["AddrHomeCity"]);
                        record.AddrHomeZip = Conversion.getStrOrNull(rdr["AddrHomeZip"]);
                        record.AddrHomeTel = Conversion.getStrOrNull(rdr["AddrHomeTel"]);
                        record.AddrHomeFax = Conversion.getStrOrNull(rdr["AddrHomeFax"]);
                        record.AddrHomeEmail = Conversion.getStrOrNull(rdr["AddrHomeEmail"]);
                        record.AddrHomeCountry = Conversion.getStrOrNull(rdr["AddrHomeCountry"]);
                        record.HomeTaxOffice = Conversion.getStrOrNull(rdr["HomeTaxOffice"]);
                        record.HomeTaxAccNo = Conversion.getStrOrNull(rdr["HomeTaxAccNo"]);
                        record.AddrWork = Conversion.getStrOrNull(rdr["AddrWork"]);
                        record.AddrWorkCity = Conversion.getStrOrNull(rdr["AddrWorkCity"]);
                        record.AddrWorkZip = Conversion.getStrOrNull(rdr["AddrWorkZip"]);
                        record.AddrWorkTel = Conversion.getStrOrNull(rdr["AddrWorkTel"]);
                        record.AddrWorkFax = Conversion.getStrOrNull(rdr["AddrWorkFax"]);
                        record.AddrWorkEMail = Conversion.getStrOrNull(rdr["AddrWorkEmail"]);
                        record.AddrWorkCountry = Conversion.getStrOrNull(rdr["AddrWorkCountry"]);
                        record.WorkTaxOffice = Conversion.getStrOrNull(rdr["WorkTaxOffice"]);
                        record.WorkTaxAccNo = Conversion.getStrOrNull(rdr["WorkTaxAccNo"]);
                        record.ContactAddr = Conversion.getStrOrNull(rdr["ContactAddr"]);
                        record.MobTel = Conversion.getStrOrNull(rdr["MobTel"]);
                        record.Jobs = Conversion.getStrOrNull(rdr["Jobs"]);
                        record.Note = Conversion.getStrOrNull(rdr["Note"]);
                        record.InvoiceAddr = Conversion.getStrOrNull(rdr["InvoiceAddr"]);
                        record.WorkFirmName = Conversion.getStrOrNull(rdr["WorkFirmName"]);
                        record.Bank = Conversion.getStrOrNull(rdr["Bank"]);
                        record.BankAccNo = Conversion.getStrOrNull(rdr["BankAccNo"]);
                        record.BankIBAN = Conversion.getStrOrNull(rdr["BankIBAN"]);
                        record.MemTable = true;
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public ReportData getReportData(string Market, string ResNo, ref string errorMsg)
        {
            ReportData data = new ReportData();
            string tsql = string.Empty;
            tsql += Detur_Table_Crt_Qry;
            if (Equals(Market, "FINMAR"))
                tsql += DeturFin_Calc_Items_Qry;
            else tsql += Detur_InvoiceItems_Build_Qry;

            /*
             @BasisPriceStr='Grunddpris', --'FULLT BETALENDE PERSON',
	         @PasEBStr='Annons erbjudande',
	         @SupDisStr='Suppleme/Discount',
	         @PromoStr='Promotion'
             */

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            DataSet ds;
            try
            {
                if (Equals(Market, "FINMAR"))
                {
                    db.AddInParameter(dbCommand, "Market", DbType.String, Market);                    
                    db.AddInParameter(dbCommand, "PLSpoStr", DbType.String, "ALENNUS");
                    db.AddInParameter(dbCommand, "PasEBStr", DbType.String, "Ennakkovaraajan etu");
                    db.AddInParameter(dbCommand, "ZeroInv", DbType.Boolean, false);
                    db.AddInParameter(dbCommand, "RequestStr", DbType.String, "Tilaus");
                }
                else if (Equals(Market, "SWEMAR"))
                {
                    db.AddInParameter(dbCommand, "BasisPriceStr", DbType.String, "Grundpris");
                    db.AddInParameter(dbCommand, "PasEBStr", DbType.String, "Annons erbjudande");
                    db.AddInParameter(dbCommand, "SupDisStr", DbType.String, "Suppleme/Discount");
                    db.AddInParameter(dbCommand, "PromoStr", DbType.String, "Promotion");
                    db.AddInParameter(dbCommand, "PLSpoStr", DbType.String, "Annons erbjudande");
                    db.AddInParameter(dbCommand, "ZeroInv", DbType.Boolean, false);
                    db.AddInParameter(dbCommand, "RequestStr", DbType.String, "Begäran");
                }
                else if (Equals(Market, "DENMAR"))
                {
                    db.AddInParameter(dbCommand, "BasisPriceStr", DbType.String, "Grundpris");
                    db.AddInParameter(dbCommand, "PasEBStr", DbType.String, "Kampagnetilbud/Annoncetilbud");
                    db.AddInParameter(dbCommand, "SupDisStr", DbType.String, "Supplement/Rabat");
                    db.AddInParameter(dbCommand, "PromoStr", DbType.String, "Kampagne");
                    db.AddInParameter(dbCommand, "PLSpoStr", DbType.String, "RABAT");
                    db.AddInParameter(dbCommand, "ZeroInv", DbType.Boolean, false);
                    db.AddInParameter(dbCommand, "RequestStr", DbType.String, "Anmodning");
                }
                else if (Equals(Market, "NORMAR"))
                {
                    db.AddInParameter(dbCommand, "BasisPriceStr", DbType.String, "Fullt betalende person"); //Avbestillingsgebyr                    
                    db.AddInParameter(dbCommand, "PasEBStr", DbType.String, "Kampagnetilbud/Annoncetilbud");
                    db.AddInParameter(dbCommand, "SupDisStr", DbType.String, "Supplement/Rabat");
                    db.AddInParameter(dbCommand, "PromoStr", DbType.String, "Kampagne");
                    db.AddInParameter(dbCommand, "PLSpoStr", DbType.String, "RABAT");
                    db.AddInParameter(dbCommand, "ZeroInv", DbType.Boolean, false);
                    db.AddInParameter(dbCommand, "RequestStr", DbType.String, "Forespørsel");
                }

                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                ds = db.ExecuteDataSet(dbCommand);
                if (ds != null)
                {
                    ds.Tables[0].TableName = "Passengers";
                    ds.Tables[1].TableName = "PasPrices";
                    ds.Tables[2].TableName = "ResPayPlan";
                    ds.Tables[3].TableName = "ServicePrices";
                    ds.Tables[4].TableName = "GiroKID";
                    ds.Tables[5].TableName = "ResMain";
                    if (!Equals(Market, "FINMAR"))
                        ds.Tables[6].TableName = "Ssrc";
                    data.PassengerList = new DetReport().getPassengers(ds.Tables["Passengers"]);
                    data.PasPriceList = new DetReport().getPasPrices(ds.Tables["PasPrices"]);
                    data.ResPayPlanList = new DetReport().getResPayPlan(ds.Tables["ResPayPlan"]);
                    data.ServicePriceList = new DetReport().getServicePrices(ds.Tables["ServicePrices"]);
                    if (ds.Tables.Contains("GiroKID") && ds.Tables["GiroKID"].Rows.Count > 0)
                    {
                        data.GiroKID = ds.Tables["GiroKID"].Rows[0][0].ToString();
                    }
                    data.ReportResMain = new DetReport().getResMainRec(ds.Tables["ResMain"]);
                    if (ds.Tables.Contains("Ssrc") && ds.Tables["Ssrc"].Rows.Count > 0)
                    {
                        data.Ssrc = new DetReport().getSsrc(ds.Tables["Ssrc"]);
                    }
                    if (string.Equals(Market, "NORMAR"))
                    {
                        List<PasPricesRecord> pData = data.PasPriceList;
                        foreach (PasPricesRecord row in pData)
                        {
                            if (data.ReportResMain.ResStat == 2 || data.ReportResMain.ResStat == 3)
                                if (Equals(row.ServiceDesc, "Fullt betalende person"))
                                    row.ServiceDesc = "Avbestillingsgebyr";
                        }
                    }
                    return data;
                }
                else
                {
                    errorMsg = HttpContext.GetGlobalResourceObject("DetReport", "ReservationNotFound").ToString();
                    return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public HotelRecord getHotelDetail(string Market, string Code, ref string errorMsg)
        {
            HotelRecord record = new HotelRecord();

            string tsql = @" Select H.Code, H.[Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market), [Name]), 
	                            H.NameS, H.HotelType, H.Category, H.AirportDist, H.CenterDist, H.SeaDist, 
	                            H.CheckInTime, H.CheckOutTime, H.PostAddress, 
	                            H.Location,
	                            LocationName = (Select Name From Location (NOLOCK) Where RecID = H.Location),
                                LocationLocalName = (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = H.Location),
	                            H.TrfLocation, 
	                            TrfLocationName = (Select Name From Location (NOLOCK) Where RecID = H.TrfLocation),
                                TrfLocationLocalName = (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = H.TrfLocation),
	                            H.PostZip, H.InvAddress, H.Phone1, H.Phone2, H.Fax, H.FaxRes, H.email, H.emailres, H.homepage, 
	                            H.ManagerName, H.RoomCount, H.BossName, H.ContactName, H.TaxOffice, H.TaxAccNo, H.AirPort1,
	                            H.ChdAgeGrpCnt, H.PayCom, H.PayComEB, H.PayPasEB, H.ConfStat, H.TaxPer, 
	                            H.UseSysParamAllot, H.HotAllotChk, H.HotAllotWarnFull, H.HotAllotNotAcceptFull, H.HotAllotNotFullGA,
	                            H.DefSupplier, H.CanDiscount, MaxChdAge=isnull(HO.MaxChdAge, H.MaxChdAge), H.Lat, H.Lon, H.OfCategory, H.WithoutLady
                            From Hotel H (NOLOCK)                                 
                            Left Join HotelMarOpt HO on HO.Hotel=H.Code and HO.Market=@Market
                            Where H.Status = 'Y' ";
            if (!string.IsNullOrEmpty(Code)) tsql += string.Format(" And Code = '{0}' ", Code);

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {                        
                        record.Code = (string)oReader["Code"];
                        record.Name = (string)oReader["Name"];
                        record.LocalName = (string)oReader["LocalName"];
                        record.NameS = (string)oReader["NameS"];
                        record.HotelType = (string)oReader["HotelType"];
                        record.Category = (string)oReader["Category"];
                        record.AirportDist = Conversion.getInt32OrNull(oReader["AirportDist"]);
                        record.CenterDist = Conversion.getInt32OrNull(oReader["CenterDist"]);
                        record.SeaDist = Conversion.getInt32OrNull(oReader["SeaDist"]);
                        record.CheckInTime = Conversion.getDateTimeOrNull(oReader["CheckInTime"]);
                        record.CheckOutTime = Conversion.getDateTimeOrNull(oReader["CheckOutTime"]);
                        record.PostAddress = Conversion.getStrOrNull(oReader["PostAddress"]);
                        record.Location = (int)oReader["Location"];
                        record.LocationName = (string)oReader["LocationName"];
                        record.LocationLocalName = (string)oReader["LocationLocalName"];
                        record.TrfLocation = (int)oReader["TrfLocation"];
                        record.TrfLocationName = (string)oReader["TrfLocationName"];
                        record.TrfLocationLocalName = (string)oReader["TrfLocationLocalName"];
                        record.PostZip = Conversion.getStrOrNull(oReader["PostZip"]);
                        record.InvAddress = Conversion.getStrOrNull(oReader["InvAddress"]);
                        record.Phone1 = Conversion.getStrOrNull(oReader["Phone1"]);
                        record.Phone2 = Conversion.getStrOrNull(oReader["Phone2"]);
                        record.Fax = Conversion.getStrOrNull(oReader["Fax"]);
                        record.Email = Conversion.getStrOrNull(oReader["email"]);
                        record.Homepage = Conversion.getStrOrNull(oReader["homepage"]);
                        record.ManagerName = Conversion.getStrOrNull(oReader["ManagerName"]);
                        record.RoomCount = Conversion.getInt16OrNull(oReader["RoomCount"]);
                        record.BossName = Conversion.getStrOrNull(oReader["BossName"]);
                        record.ContactName = Conversion.getStrOrNull(oReader["ContactName"]);
                        record.TaxOffice = Conversion.getStrOrNull(oReader["TaxOffice"]);
                        record.TaxAccNo = Conversion.getStrOrNull(oReader["TaxAccNo"]);
                        record.AirPort1 = Conversion.getStrOrNull(oReader["AirPort1"]);
                        record.ChdAgeGrpCnt = Conversion.getInt16OrNull(oReader["ChdAgeGrpCnt"]);
                        record.ConfStat = Conversion.getInt16OrNull(oReader["ConfStat"]);
                        record.PayCom = Equals(oReader["PayCom"], "Y");
                        record.PayComEB = Equals(oReader["PayComEB"], "Y");
                        record.PayPasEB = Equals(oReader["PayPasEB"], "Y");
                        record.CanDiscount = (bool)oReader["CanDiscount"];
                        record.TaxPer = Conversion.getDecimalOrNull(oReader["TaxPer"]);
                        record.UseSysParamAllot = Equals(oReader["UseSysParamAllot"], "Y");
                        record.HotAllotChk = Equals(oReader["HotAllotChk"], "Y");
                        record.HotAllotWarnFull = Equals(oReader["HotAllotWarnFull"], "Y");
                        record.HotAllotNotAcceptFull = Equals(oReader["HotAllotNotAcceptFull"], "Y");
                        record.HotAllotNotFullGA = Equals(oReader["HotAllotNotFullGA"], "Y");
                        record.DefSupplier = Conversion.getStrOrNull(oReader["DefSupplier"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        record.Lat = Conversion.getDecimalOrNull(oReader["Lat"]);
                        record.Lon = Conversion.getDecimalOrNull(oReader["Lon"]);
                        record.OfCategory = Conversion.getStrOrNull(oReader["OfCategory"]);
                        record.WithoutLady = (bool)oReader["WithoutLady"];
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public AirportRecord getAirport(string Market, string Code, ref string errorMsg)
        {
            AirportRecord record = new AirportRecord();
            string tsql = @"Select Code, [Name], LocalName=isnull(dbo.FindLocalName(NameLID, @Market), [Name]),
	                            NameS, Location, PNLFullName, PNLFlightSpace, PNLNameSpace, PNLChdRemark, 
	                            PNLSitatex, PNLMaxLineInPart, PNLDispExtra, PNLDispPNR, PNLDotAfterTitle
                            From Airport (NOLOCK)
                            Where Code=@Code";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.LocalName = Conversion.getStrOrNull(R["LocalName"]);
                        record.NameS = Conversion.getStrOrNull(R["NameS"]);
                        record.Location = (Int32)R["Location"];
                        record.PNLFullName = Equals(R["PNLFullName"], "Y");
                        record.PNLFlightSpace = Equals(R["PNLFlightSpace"], "Y");
                        record.PNLNameSpace = Equals(R["PNLNameSpace"], "Y");
                        record.PNLChdRemark = Equals(R["PNLChdRemark"], "Y");
                        record.PNLSitatex = Equals(R["PNLSitatex"], "Y");
                        record.PNLMaxLineInPart = Conversion.getInt32OrNull(R["PNLMaxLineInPart"]);
                        record.PNLDispExtra = Equals(R["PNLDispExtra"], "Y");
                        record.PNLDotAfterTitle = Equals(R["PNLDotAfterTitle"], "Y");
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightRecord getFlight(string Market, string Code, ref string errorMsg)
        {
            FlightRecord record = new FlightRecord();
            string tsql = @"Select Code, Airline, AirlineName=(Select [Name] From AirLine (NOLOCK) Where Code=Flight.Airline),
	                            AirlineLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From AirLine (NOLOCK) Where Code=Flight.Airline), Company, 
	                            PlaneType, PlaneTypeName=(Select [Name] From PlaneType (NOLOCK) Where RecID=Flight.PlaneType),
	                            FlightType, FlightTypeName=(Select [Name] From FlightType (NOLOCK) Where RecID=Flight.FlightType),
	                            DepCity, DepCityName=(Select [Name] From Location (NOLOCK) Where RecID=Flight.DepCity),
	                            DepCityLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=Flight.DepCity),
	                            DepAir, DepTime, 	
	                            ArrCity, ArrCityName=(Select [Name] From Location (NOLOCK) Where RecID=Flight.ArrCity),
	                            ArrCityLocalName=(Select isnull(dbo.FindLocalName(NameLID, @Market), [Name]) From Location (NOLOCK) Where RecID=Flight.ArrCity),
	                            ArrAir, ArrTime, CountCloseTime, BagWeight,	InfAge, ChdAge, TeenAge, RetCode, Virtual, PNLName, ConfStat, FlyDays, 
	                            TaxPer, FlyDur, Operator, PayCom, PayComEB, PayPasEB, isFirstFlight, CanDiscount=isnull(CanDiscount, 0), Direction
                            From Flight (NOLOCK)
                            Where Code=@Code";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "Code", DbType.String, Code);

                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        record.Code = Conversion.getStrOrNull(R["Code"]);
                        record.Airline = Conversion.getStrOrNull(R["Airline"]);
                        record.AirlineName = Conversion.getStrOrNull(R["AirlineName"]);
                        record.AirlineLocalName = Conversion.getStrOrNull(R["AirlineLocalName"]);
                        record.Company = Conversion.getStrOrNull(R["Company"]);
                        record.PlaneType = Conversion.getInt32OrNull(R["PlaneType"]);
                        record.PlaneTypeName = Conversion.getStrOrNull(R["PlaneTypeName"]);
                        record.FlightType = Conversion.getInt32OrNull(R["FlightType"]);
                        record.FlightTypeName = Conversion.getStrOrNull(R["FlightTypeName"]);
                        record.DepCity = (Int32)R["DepCity"];
                        record.DepCityName = Conversion.getStrOrNull(R["DepCityName"]);
                        record.DepCityLocalName = Conversion.getStrOrNull(R["DepCityLocalName"]);
                        record.DepAir = Conversion.getStrOrNull(R["DepAir"]);
                        record.DepTime = Conversion.getDateTimeOrNull(R["DepTime"]);
                        record.ArrCity = (Int32)R["ArrCity"];
                        record.ArrCityName = Conversion.getStrOrNull(R["ArrCityName"]);
                        record.ArrCityLocalName = Conversion.getStrOrNull(R["ArrCityLocalName"]);
                        record.ArrAir = Conversion.getStrOrNull(R["ArrAir"]);
                        record.ArrTime = Conversion.getDateTimeOrNull(R["ArrTime"]);
                        record.CountCloseTime = Conversion.getDateTimeOrNull(R["CountCloseTime"]);
                        record.BagWeight = Conversion.getDecimalOrNull(R["BagWeight"]);
                        record.InfAge = Conversion.getDecimalOrNull(R["InfAge"]);
                        record.ChdAge = Conversion.getDecimalOrNull(R["ChdAge"]);
                        record.TeenAge = Conversion.getDecimalOrNull(R["TeenAge"]);
                        record.RetCode = Conversion.getStrOrNull(R["RetCode"]);
                        record.Virtual = Conversion.getStrOrNull(R["Virtual"]);
                        record.PNLName = Conversion.getStrOrNull(R["PNLName"]);
                        record.ConfStat = Conversion.getInt16OrNull(R["ConfStat"]);
                        record.FlyDays = Conversion.getStrOrNull(R["FlyDays"]);
                        record.TaxPer = Conversion.getDecimalOrNull(R["TaxPer"]);
                        record.FlyDur = Conversion.getInt32OrNull(R["FlyDur"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        record.PayComEB = Equals(R["PayComEB"], "Y");
                        record.PayPasEB = Conversion.getBoolOrNull(R["PayPasEB"]);
                        record.IsFirstFlight = Equals(R["IsFirstFlight"], "Y");
                        record.CanDiscount = Conversion.getBoolOrNull(R["CanDiscount"]);
                        record.Direction = Conversion.getInt16OrNull(R["Direction"]);
                        return record;
                    }
                    else return null;

                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightDayRecord getFlightDay(string FlightNo, DateTime FlightDate, ref string errorMsg)
        {
            FlightDayRecord record = new FlightDayRecord();
            string tsql = @"    Select FlightNo, Airline, FlgOwner, FlyDate=isnull(TDepDate,FlyDate), 
                                    DepCity,DepAirport,DepTime=isnull(TDepTime,DepTime),
                                    ArrCity,ArrAirport,ArrTime=isnull(TArrTime,ArrTime), 
                                    StopSale, BagWeight, MaxInfAge, MaxChdAge, NextFlight, PNLName, CountCloseTime, WebPub, 
                                    AllotUseSysSet, AllotChk, AllotWarnFull, AllotDoNotOver, FlyDur, 
                                    MaxTeenAge, WarnUsedAmount, RetFlightOpt, RetFlightUseSysSet, TDepDate, TArrDate,
                                    ArrDate=isnull(TArrDate, (isnull(TDepDate,FlyDate)+Case when IsNull(TDepTime,isnull(DepTime,0)) > IsNull(TArrTime,IsNull(ArrTime,IsNull(DepTime,0))) then 1 else 0 end))
                                From FlightDay (NOLOCK)
                                Where FlightNo=@FlightNo and FlyDate=@FlightDate ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "FlightNo", DbType.String, FlightNo);
                db.AddInParameter(dbCommand, "FlightDate", DbType.DateTime, FlightDate);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())
                    {
                        record.FlightNo = (string)oReader["FlightNo"];
                        record.Airline = (string)oReader["Airline"];
                        record.FlgOwner = (string)oReader["FlgOwner"];
                        record.FlyDate = (DateTime)oReader["Flydate"];
                        record.ArrDate = (DateTime)oReader["ArrDate"];
                        record.DepCity = (int)oReader["DepCity"];
                        record.DepAirport = (string)oReader["DepAirport"];
                        record.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        record.ArrCity = (int)oReader["ArrCity"];
                        record.ArrAirport = (string)oReader["ArrAirport"];
                        record.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        record.StopSale = Equals(oReader["StopSale"], "Y");
                        record.BagWeight = Conversion.getInt16OrNull(oReader["BagWeight"]);
                        record.MaxInfAge = Conversion.getDecimalOrNull(oReader["MaxInfAge"]);
                        record.MaxChdAge = Conversion.getDecimalOrNull(oReader["MaxChdAge"]);
                        record.MaxTeenAge = Conversion.getDecimalOrNull(oReader["MaxTeenAge"]);
                        record.NextFlight = Conversion.getStrOrNull(oReader["NextFlight"]);
                        record.PNRName = Conversion.getStrOrNull(oReader["PNLName"]);
                        record.CountCloseTime = Conversion.getDateTimeOrNull(oReader["CountCloseTime"]);
                        record.WebPub = Equals(oReader["WebPub"], "Y");
                        record.AllotUseSysSet = Equals(oReader["AllotUseSysSet"], "Y");
                        record.AllotChk = Equals(oReader["AllotChk"], "Y");
                        record.AllotWarnFull = Equals(oReader["AllotWarnFull"], "Y");
                        record.AllotDoNotOver = Equals(oReader["AllotDoNotOver"], "Y");
                        record.FlyDur = (int)oReader["FlyDur"];
                        record.WarnUsedAmount = Conversion.getInt16OrNull(oReader["WarnUsedAmount"]);
                        record.RetFlightOpt = Conversion.getByteOrNull(oReader["RetFlightOpt"]);
                        record.RetFlightUseSysSet = Equals(oReader["RetFlightUseSysSet"], "Y");
                        record.TDepDate = Conversion.getDateTimeOrNull(oReader["TDepDate"]);
                        record.TArrDate = Conversion.getDateTimeOrNull(oReader["TArrDate"]);
                    }
                    return record;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return record;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public FlightDetailRecord getFlightDetail(string Market, string Code, DateTime FlightDay, ref string errorMsg)
        {
            try
            {
                FlightRecord f = getFlight(Market, Code, ref errorMsg);
                FlightDayRecord flightDay = getFlightDay(Code, FlightDay, ref errorMsg);
                List<FlightDayRecord> flightDays = new List<FlightDayRecord>();
                AirportRecord depAirport = getAirport(Market, flightDay.DepAirport, ref errorMsg);
                AirportRecord arrAirport = getAirport(Market, flightDay.ArrAirport, ref errorMsg);
                flightDays.Add(flightDay);
                IEnumerable<FlightDetailRecord> query = from q in flightDays.AsEnumerable()
                                                        where q.FlightNo == Code && q.FlyDate == FlightDay
                                                        select new FlightDetailRecord
                                                        {
                                                            FlightNo = q.FlightNo,
                                                            FlyDate = q.TDepDate.HasValue ? q.TDepDate.Value : q.FlyDate,
                                                            ArrDate = q.ArrDate,
                                                            FlyDur = q.FlyDur,
                                                            DepTime = q.DepTime,
                                                            DepAirport = q.DepAirport,
                                                            DepAirportName = depAirport.Name,
                                                            DepAirportNameL = depAirport.LocalName,
                                                            DepCity = q.DepCity,
                                                            DepCityName = f.DepCityName,
                                                            DepCityNameL = f.DepCityLocalName,
                                                            ArrTime = q.ArrTime,
                                                            ArrAirport = q.ArrAirport,
                                                            ArrAirportName = arrAirport.Name,
                                                            ArrAirportNameL = arrAirport.LocalName,
                                                            ArrCity = q.ArrCity,
                                                            ArrCityName = f.ArrCityName,
                                                            ArrCityNameL = f.ArrCityLocalName,
                                                            Airline = f.Airline,
                                                            AirlineName = f.AirlineName,
                                                            AirlineNameL = f.AirlineLocalName,
                                                            MaxInfAge = q.MaxInfAge,
                                                            MaxChdAge = q.MaxChdAge,
                                                            MaxTeenAge = q.MaxTeenAge,
                                                            BagWeight = q.BagWeight,
                                                            CanDiscount = (f.CanDiscount.HasValue ? f.CanDiscount.Value : false),
                                                            PNRName = q.PNRName
                                                        };

                return query.FirstOrDefault();
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
        }

        public List<FlightClassRecord> getFlightClass(string Market, ref string errorMsg)
        {
            string tsql = @"Select RecID, Class, Name, NameS, NameL=isnull(dbo.FindLocalName(NameLID, @Market), Name),
	                            Category, B2B, B2C
                            From FlightClass (NOLOCK) ";
            List<FlightClassRecord> records = new List<FlightClassRecord>();
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        FlightClassRecord record = new FlightClassRecord();
                        record.RecID = (int)oReader["RecID"];
                        record.Class = Conversion.getStrOrNull(oReader["Class"]);
                        record.Name = Conversion.getStrOrNull(oReader["Name"]);
                        record.NameS = Conversion.getStrOrNull(oReader["NameS"]);
                        record.NameL = Conversion.getStrOrNull(oReader["NameL"]);
                        record.Category = Conversion.getStrOrNull(oReader["Category"]);
                        record.B2B = (bool)oReader["B2B"];
                        record.B2C = (bool)oReader["B2C"];
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Det_Fin_FlightDetailRecord> getFlights_Fin(string ResNo, ref string errorMsg)
        {
            List<Det_Fin_FlightDetailRecord> records = new List<Det_Fin_FlightDetailRecord>();
            string tsql = @"Select Distinct F.FlightNo as Service, Date=isnull(F.TDepDate,F.FlyDate), DepTime=isnull(F.TDepTime,F.DepTime),ArrTime=isnull(F.TArrTime,F.ArrTime), F.DepAirport, F.ArrAirport,
	                            DL.Name as Departure, AL.Name as Arrival, 
	                            ArrDate=isnull(F.TArrDate, (isnull(F.TDepDate,F.FlyDate)+Case when IsNull(F.TDepTime,isnull(F.DepTime,0)) > IsNull(F.TArrTime,IsNull(F.ArrTime,IsNull(F.DepTime,0))) then 1 else 0 end)),
	                            S.FlgClass,F.FlyDur,F.PNLName                                      
                            From ResService S (NOLOCK)
                            left Join Flight FC (NOLOCK) on FC.Code = S.Service
                            left Join FlightDay F (NOLOCK) on F.FlightNo = S.Service and F.FlyDate=S.BegDate
                            left Join Location DL (NOLOCK) on DL.RecID=F.DepCity
                            Left Join Location AL (NOLOCK) on AL.RecID=F.ArrCity
                            Where S.ResNo=@ResNo and ServiceType='FLIGHT' And S.StatSer in (0, 1)
                            Order by isnull(F.TDepDate,F.FlyDate)";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        Det_Fin_FlightDetailRecord record = new Det_Fin_FlightDetailRecord();
                        record.Service = Conversion.getStrOrNull(oReader["Service"]);
                        record.Date = Conversion.getDateTimeOrNull(oReader["Date"]);
                        record.DepTime = Conversion.getDateTimeOrNull(oReader["DepTime"]);
                        record.ArrTime = Conversion.getDateTimeOrNull(oReader["ArrTime"]);
                        record.DepAirport = Conversion.getStrOrNull(oReader["DepAirport"]);
                        record.ArrAirport = Conversion.getStrOrNull(oReader["ArrAirport"]);
                        record.Departure = Conversion.getStrOrNull(oReader["Departure"]);
                        record.Arrival = Conversion.getStrOrNull(oReader["Arrival"]);
                        record.ArrDate = Conversion.getDateTimeOrNull(oReader["ArrDate"]);
                        record.FlgClass = Conversion.getStrOrNull(oReader["FlgClass"]);
                        record.FlyDur = Conversion.getInt16OrNull(oReader["FlyDur"]);
                        record.PNLName = Conversion.getStrOrNull(oReader["PNLName"]);

                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<Det_Fin_HotelDetailRecord> getHotels_Fin(string ResNo, ref string errorMsg)
        {
            List<Det_Fin_HotelDetailRecord> records = new List<Det_Fin_HotelDetailRecord>();
            string tsql = @"Select *, Unit=count(*) From
                            (
                            Select ServiceName=H.Name, 
                                Location=isnull(dbo.FindLocalName(L.NameLID, M.Market), L.Name), 
                                RoomName=isnull(dbo.FindLocalName(HR.NameLID, M.Market), HR.Name), 
                                HotelCat=H.Category                                 
                            From ResService S (NOLOCK)
                            Join Hotel H (NOLOCK) on H.Code=S.Service
                            Join Location L (NOLOCK) on L.RecID=H.Location
                            Join Room HR (NOLOCK) on HR.Code=S.Room
                            Join ResMain M (NOLOCK) on M.ResNo=S.ResNo
                            Where S.ResNo=@ResNo and S.ServiceType='HOTEL'
                            ) X
                            Group by X.ServiceName, X.Location, X.RoomName, X.HotelCat";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        Det_Fin_HotelDetailRecord record = new Det_Fin_HotelDetailRecord();
                        record.ServiceName = Conversion.getStrOrNull(oReader["ServiceName"]);
                        record.Location = Conversion.getStrOrNull(oReader["Location"]);
                        record.RoomName = Conversion.getStrOrNull(oReader["RoomName"]);
                        record.HotelCat = Conversion.getStrOrNull(oReader["HotelCat"]);
                        record.Unit = Conversion.getInt16OrNull(oReader["Unit"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<string> getBoards_Fin(string ResNo, ref string errorMsg)
        {
            List<string> records = new List<string>();
            string tsql = @"Select Distinct BoardName=isnull(dbo.FindLocalName(B.NameLID, M.Market), B.Name)
                            From ResService S (NOLOCK)
                            Join Board B (NOLOCK) on B.Code=S.Board
                            Join ResMain M (NOLOCK) on M.ResNo=S.ResNo
                            Where S.ResNo=@ResNo and ServiceType='HOTEL'";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);

                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        records.Add(Conversion.getStrOrNull(oReader["BoardName"]));
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<string> getHandicaps_Fin(string Market, string ResNo, ref string errorMsg)
        {
            List<string> records = new List<string>();
            string tsql = @"Select Distinct Text=dbo.FindLocalNameEx(NameLID,Market)
                            From Handicaps H (NOLOCK)
                            Join ResService S (NOLOCK) on S.ResNo=@ResNo and S.ServiceType=H.Type and S.Service=H.Code and S.StatSer in (0,1)
                            Where Type = 'FLIGHT' and Market=@Market and PrtVoucher=1 and H.BegDate <= S.EndDate and H.EndDate >= S.BegDate
                            UNION
                            Select Distinct Text=dbo.FindLocalNameEx(NameLID,Market)
                            From HotelHandicap H (NOLOCK)
                            Join ResService S (NOLOCK) on S.ResNo=@ResNo and S.ServiceType='HOTEL' and S.Service=H.Hotel and S.StatSer in (0,1)
                            Where H.Market=@Market and PrtVoucher=1 and H.BegDate <= S.EndDate and H.EndDate >= S.BegDate
                            UNION
                            --Hotel Location Handicaps
                            Select Distinct Text=dbo.FindLocalNameEx(H.NameLID,Market) 
                            From Handicaps (NOLOCK) H
                            Join Location L (NOLOCK) on L.Code=H.Code
                            Join ResService S (NOLOCK) on S.ResNo=@ResNo and S.ServiceType='HOTEL' and S.StatSer in (0,1)
                            Outer Apply (Select LocID from dbo.ufn_LocationUp(S.DepLocation,1) ) D 
                            Where H.Market=@Market and H.Type = 'LOCATION' and Market='FINMAR' and PrtVoucher=1 and D.LocID=L.RecID
                            and H.BegDate <= S.EndDate and H.EndDate >= S.BegDate
                            UNION
                            --Airport handicaps                                             
                            Select Distinct Text=dbo.FindLocalNameEx(NameLID,Market)
                            From Handicaps H (NOLOCK)
                            Join ResService S (NOLOCK) on S.ResNo=@ResNo and S.ServiceType='FLIGHT' and S.StatSer in (0,1)
                            Join FlightDay F (NOLOCK) on F.FlightNo=S.Service and FlyDate=S.BegDate and S.StatSer in (0,1)
                            Where Type = 'AIRPORT' and Market=@Market and PrtVoucher=1 and H.Code=F.DepAirport
                            and H.BegDate <= S.EndDate and H.EndDate >= S.BegDate";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    while (oReader.Read())
                    {
                        records.Add(Conversion.getStrOrNull(oReader["Text"]));
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getHandicaps_Den(string Market, string ResNo, ref string errorMsg)
        {
            List<string> records = new List<string>();
            string tsql = @"Declare @s varchar(MAX)
                            Select @s=COALESCE(@s+ char(13),'')+Text From
                            (
                            --Hotel
                            Select Distinct Type='H',Text=dbo.FindLocalNameEx(NameLID,Market)
                            From HotelHandicap C (NOLOCK)
                            Join ResService S (NOLOCK) on S.ResNo=@ResNo and S.ServiceType='HOTEL' and S.StatSer in (0,1)
                            Where C.Hotel=S.Service and C.BegDate <= S.EndDate and C.EndDate >= S.BegDate
                            Union ALL
                            --Airport
                            Select Distinct Type='A',Text=dbo.FindLocalNameEx(NameLID,Market)
                            From Handicaps H (NOLOCK)
                            Join ResService S (NOLOCK) on S.ResNo=@ResNo and S.ServiceType='FLIGHT' and S.StatSer in (0,1)
                            Join FlightDay F (NOLOCK) on F.FlightNo=S.Service and FlyDate=S.BegDate and S.StatSer in (0,1)
                            Where Type = 'AIRPORT' and Market=@Market and PrtVoucher=1 and H.Code=F.DepAirport and H.BegDate <= S.EndDate and H.EndDate >= S.BegDate
                            Union ALL
                            --Flight Handicaps
                            Select Distinct Type='F',Text=dbo.FindLocalNameEx(NameLID,Market)
                            From Handicaps H (NOLOCK)
                            Join ResService S (NOLOCK) on S.ResNo=@ResNo and S.ServiceType=H.Type and S.Service=H.Code and S.StatSer in (0,1)
                            Where Type = 'FLIGHT' and Market=@Market and PrtVoucher=1 and H.BegDate <= S.EndDate and H.EndDate >= S.BegDate
                            Union ALL
                            --Hotel Location Handicaps
                            Select Distinct Type='HL',Text=dbo.FindLocalNameEx(H.NameLID,Market)
                            From Handicaps (NOLOCK) H
                            Join Location L (NOLOCK) on L.Code=H.Code
                            Join ResService S (NOLOCK) on S.ResNo=@ResNo and S.ServiceType='HOTEL' and S.StatSer in (0,1)
                            Outer Apply (Select LocID from dbo.ufn_LocationUp(S.DepLocation,1) ) D
                            Where H.Type = 'LOCATION' and Market=@Market and PrtVoucher=1 and D.LocID=L.RecID
                            ) X

                            Select Text=@s";
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getLocationForCountryName(string Market, int Location, ref string errorMsg)
        {
            string tsql = @"Select [Name]=isnull(dbo.FindLocalName(NameLID, @Market), Name) 
                            From Location (NOLOCK) 
                            Where RecID = (Select Country From Location (NOLOCK) Where RecID = @RecID) ";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);

            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "RecId", DbType.Int32, Location);

                return (string)db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string convertByteToString(byte[] inValue)
        {
            if (inValue == null) return string.Empty;
            string retVal = string.Empty;
            foreach (byte r in inValue) retVal += Convert.ToChar(r);
            return retVal;
        }

        public byte[] convertStringToByte(string inValue)
        {
            if (string.IsNullOrEmpty(inValue)) return null;
            List<byte> retVal = new List<byte>();
            foreach (char r in inValue) retVal.Add(Convert.ToByte(r));
            return retVal.ToArray<byte>();
        }

        public static void writeTextRightAlign(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            cb.SetColorFill(iTextSharp.text.BaseColor.BLACK);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, text, x, y, 0);
            cb.EndText();
        }

        public static void writeTextRightAlign(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize, iTextSharp.text.BaseColor color)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            if (color == null)
                cb.SetColorFill(iTextSharp.text.BaseColor.BLACK);
            else cb.SetColorFill(color);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, text, x, y, 0);
            cb.EndText();
        }

        public static void writeText(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            cb.SetColorFill(iTextSharp.text.BaseColor.BLACK);
            cb.SetTextMatrix(x, y);
            cb.ShowText(text);
            cb.EndText();
        }

        public static void writeText(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize, iTextSharp.text.BaseColor color)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            if (color == null)
                cb.SetColorFill(iTextSharp.text.BaseColor.BLACK);
            else cb.SetColorFill(color);
            cb.SetTextMatrix(x, y);
            cb.ShowText(text);
            cb.EndText();
        }

        public string getPDF_FinMar(string pdfFilePath, string tempFilePath, string ResNo, string Market)
        {
            try
            {
                string errorMsg = string.Empty;

                ReportData data = new TvReport.DetReport.DetReport().getReportData(Market, ResNo, ref errorMsg);
                ReportResMainRec resMain = data.ReportResMain;
                List<ResServiceRecord> resService = new DetReport().getResService(ResNo, ref errorMsg);
                string fileNameF = ResNo + System.Guid.NewGuid() + ".pdf"; ;
                string fileName = tempFilePath + fileNameF;
                PdfReader reader = new PdfReader(pdfFilePath);
                int pageCount = reader.NumberOfPages;
                int currentpage = 1;
                byte[] pdfBytes = reader.GetPageContent(currentpage);

                if (File.Exists(fileName))
                    File.Delete(fileName);

                Document doc = new Document(reader.GetPageSize(1));
                PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Append, FileAccess.Write));
                doc.Open();
                byte[] pageContent = reader.GetPageContent(currentpage);
                var query = from q in data.PassengerList
                            group q by new { q.PageNo } into k
                            select new { k.Key.PageNo };
                foreach (var rr1 in query)
                {
                    doc.NewPage();
                    PdfImportedPage page;
                    page = pdfWriter.GetImportedPage(reader, currentpage);
                    PdfContentByte cb = pdfWriter.DirectContent;

                    cb.AddTemplate(page, 0, 0);
                    string fontpath = HttpContext.Current.Server.MapPath(".");
                    BaseFont fontTimes = BaseFont.CreateFont(fontpath + "\\Document\\TIMES.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                    BaseFont fontTimesBold = BaseFont.CreateFont(fontpath + "\\Document\\TIMESBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                    BaseFont fontTimesItalic = BaseFont.CreateFont(fontpath + "\\Document\\TIMESI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                    writeText(ref cb, "Tämä on lentolippunne", 338, 806, fontTimesBold, 8);
                    writeText(ref cb, "joka on säilytettävä matkan ajan.", 418, 806, fontTimes, 8);
                    writeText(ref cb, "Tällä myös maksetaan matkavaraus. Lentolippu esitetään", 338, 798, fontTimes, 8);
                    writeText(ref cb, "lähtöselvityksessä lentokentällä sekä meno- että paluulennolla.", 338, 790, fontTimes, 8);
                    writeText(ref cb, "Pyydämme ystävällisesti tarkistamaan  varauksenne", 338, 782, fontTimes, 8);
                    writeText(ref cb, "tiedot ja ottamaan yhteyttä matkatoimistoonne,", 338, 774, fontTimes, 8);
                    writeText(ref cb, "mikäli tiedot eivät täsmää.", 338, 766, fontTimes, 8);

                    writeText(ref cb, "Varausnumero :", 338, 744, fontTimesBold, 9);
                    writeText(ref cb, ResNo, 437, 744, fontTimesBold, 10);
                    writeText(ref cb, "Varauspäivä :", 338, 730, fontTimesBold, 9);
                    writeText(ref cb, resMain.ResDate.HasValue ? resMain.ResDate.Value.ToString("dd.MM.yyyy") : "", 437, 730, fontTimesBold, 10);
                    writeText(ref cb, "Teitä palveli :", 338, 716, fontTimesBold, 9);
                    writeText(ref cb, resMain.AgencyUserName, 437, 716, fontTimesBold, 10);
                    PassengersRecord _Leader = data.PassengerList.Find(f => f.Leader == "Y");
                    ResCustInfoRecord resCustInfo = new DetReport().getResCustInfo(Market, _Leader != null ? _Leader.CustNo : -1, ref errorMsg);
                    if (resCustInfo != null)
                    {
                        string Address = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHome : resCustInfo.AddrWork;
                        string Zip = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHomeZip : resCustInfo.AddrWorkZip;
                        string City = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHomeCity : resCustInfo.AddrWorkCity;

                        PassengersRecord _leader = data.PassengerList.Find(f => f.CustNo == _Leader.CustNo);
                        writeText(ref cb, _Leader.Surname + " " + _leader.Name, 79, 714, fontTimesBold, 10);
                        writeText(ref cb, Address, 79, 700, fontTimesBold, 10);
                        writeText(ref cb, Zip, 79, 686, fontTimesBold, 10);
                        writeText(ref cb, City, 130, 686, fontTimesBold, 10);
                    }
                    writeText(ref cb, "Matkustustiedot", 79, 618, fontTimesBold, 9);
                    writeText(ref cb, "Lentoyhtiö", 79, 606, fontTimes, 9);
                    writeText(ref cb, "Lennon Numero", 79, 598, fontTimes, 9);
                    writeText(ref cb, "Päivä", 150, 600, fontTimes, 9);
                    writeText(ref cb, "Lähtöaika", 240, 606, fontTimes, 9);
                    writeText(ref cb, "Mistä", 319, 606, fontTimes, 9);
                    writeText(ref cb, "Mihin", 406, 606, fontTimes, 9);
                    writeText(ref cb, "Tuloaika", 520, 606, fontTimes, 9);

                    List<Det_Fin_FlightDetailRecord> flights = new TvReport.DetReport.DetReport().getFlights_Fin(ResNo, ref errorMsg);
                    if (flights != null && flights.Count > 0)
                    {
                        int rowy = 600;
                        foreach (var row in flights)
                        {
                            rowy = rowy - 10;
                            writeText(ref cb, string.IsNullOrEmpty(row.PNLName) ? row.Service : row.PNLName, 79, rowy, fontTimes, 9);
                            writeText(ref cb, row.Date.Value.ToString("dd.MM.yyyy"), 150, rowy, fontTimes, 9);
                            writeText(ref cb, row.DepTime.HasValue ? row.DepTime.Value.ToString("HH:mm") : "", 240, rowy, fontTimes, 9);
                            writeText(ref cb, row.Departure, 319, rowy, fontTimes, 9);
                            writeText(ref cb, row.Arrival, 406, rowy, fontTimes, 9);
                            writeText(ref cb, row.ArrTime.HasValue ? row.ArrTime.Value.ToString("HH:mm") : "", 520, rowy, fontTimes, 9);
                        }
                    }

                    writeText(ref cb, "Matkustajat", 79, 514, fontTimesBold, 9);
                    int y = 512;
                    int i = 0;
                    foreach (PassengersRecord row in data.PassengerList.Where(w => w.PageNo == rr1.PageNo).Select(s => s).ToList<PassengersRecord>())
                    {
                        i++;
                        y -= 10;
                        if (row.Status.HasValue && row.Status.Value == 0)
                            writeText(ref cb, i.ToString() + " . " + row.Surname + " " + row.Name + " " + row.Title, 79, y, fontTimes, 8);
                        else writeText(ref cb, i.ToString() + " . " + row.Surname + " " + row.Name + " " + row.Title, 79, y, fontTimesItalic, 8, iTextSharp.text.BaseColor.LIGHT_GRAY);
                    }
                    List<Det_Fin_HotelDetailRecord> hotels = new TvReport.DetReport.DetReport().getHotels_Fin(ResNo, ref errorMsg);

                    if (hotels != null && hotels.Count > 0)
                    {
                        y = 512;
                        writeText(ref cb, "Majoitustiedot", 300, 514, fontTimesBold, 9);
                        foreach (var row in hotels)
                        {
                            y -= 10;
                            writeText(ref cb, row.Location + ", " + row.ServiceName + " " + row.RoomName + (row.Unit > 1 ? " *" + row.Unit.ToString() : ""), 300, y, fontTimes, 9);
                        }
                    }
                    List<string> boards = new TvReport.DetReport.DetReport().getBoards_Fin(ResNo, ref errorMsg);
                    if (boards != null && boards.Count > 0)
                    {
                        y = 452;
                        writeText(ref cb, "Ateriat", 300, y, fontTimesBold, 9);
                        foreach (string row in boards)
                        {
                            y -= 10;
                            writeText(ref cb, row, 300, y, fontTimes, 9);
                        }
                    }
                    if (rr1.PageNo == 1)
                    {
                        writeText(ref cb, "Erittely", 79, 383, fontTimesBold, 8);
                        y = 383;
                        foreach (ServicePricesRecord row in data.ServicePriceList)
                        {
                            y -= 10;
                            writeText(ref cb, row.Unit.ToString(), 79, y, fontTimes, 8);
                            writeText(ref cb, row.ServiceDesc, 107, y, fontTimes, 8);
                            writeTextRightAlign(ref cb, row.SalePrice.HasValue ? (row.RecType != 5 ? row.SalePrice.Value.ToString("#,###.00") : "") : "", 429, y, fontTimes, 8);
                            writeTextRightAlign(ref cb, row.SalePrice.HasValue ? (row.SalePrice.Value * row.Unit.Value).ToString("#,###.00") : "", 538, y, fontTimes, 8);
                        }
                        cb.SetLineWidth(1);
                        cb.MoveTo(450, y - 2);
                        cb.LineTo(538, y - 2);
                        cb.Stroke();
                        y -= 10;
                        writeText(ref cb, "Yhteensä", 79, y, fontTimes, 8);
                        writeTextRightAlign(ref cb, data.ReportResMain.PasPayable.HasValue ? data.ReportResMain.PasPayable.Value.ToString("#,###.00") : "", 538, y, fontTimes, 8);

                        y -= 20;
                        List<string> handicaps = new TvReport.DetReport.DetReport().getHandicaps_Fin(Market, ResNo, ref errorMsg);
                        if (handicaps != null && handicaps.Count > 0)
                        {
                            writeText(ref cb, "Huom!", 79, y, fontTimesBold, 8);
                            foreach (string row in handicaps)
                            {
                                y -= 10;
                                writeText(ref cb, row, 79, y, fontTimes, 8);
                            }
                        }
                    }
                    y = 160;
                    writeText(ref cb, "Maksutiedot", 79, y, fontTimesBold, 8);

                    y = 145;
                    bool firstLine = true;
                    foreach (ResPayPlanRecord row in data.ResPayPlanList)
                    {
                        y -= 10;
                        writeText(ref cb, firstLine ? "Erapaiva , varausmaksu" : "Erapaiva , loppumaksu", 79, y, fontTimes, 8);
                        writeText(ref cb, row.DueDate.HasValue ? row.DueDate.Value.ToString("dd.MM.yyyy") : "", 215, y, fontTimes, 8);
                        writeText(ref cb, "Yth.", 349, y, fontTimes, 8);
                        writeTextRightAlign(ref cb, row.RemAmount.HasValue ? row.RemAmount.Value.ToString("#,###.00") : "", 456, y, fontTimes, 8);
                        writeText(ref cb, "€", 460, y, fontTimes, 8);
                        writeText(ref cb, "euroa Yhteensä", 479, y, fontTimes, 8);
                        firstLine = false;
                    }
                    y -= 10;
                    writeText(ref cb, "Viite :" + data.GiroKID, 79, y, fontTimes, 8);
                    //Nordea 174530-132763      IBAN    FI64 1745 3000 1327 63
                    //Sampo 800187-10036091  IBAN     FI37 8001 8710 0360 91
                    writeText(ref cb, "PANKKIYHTEYS:", 79, 60, fontTimes, 9);
                    writeText(ref cb, "Nordea Fredrikintori", 79, 50, fontTimes, 8);
                    writeText(ref cb, "174530-132763", 205, 50, fontTimes, 8);
                    writeText(ref cb, "IBAN FI64 1745 3000 1327 63 NDEAFIHH", 305, 50, fontTimes, 8);
                    writeText(ref cb, "Sampo", 79, 40, fontTimes, 8);
                    writeText(ref cb, "800187-10036091", 205, 40, fontTimes, 8);
                    writeText(ref cb, "IBAN FI37 8001 8710 0360 91 DABAFIHH", 305, 40, fontTimes, 8);
                    writeText(ref cb, "Huom ! Viitettä käytettävä ehdottomasti maksaessanne, jotta maksu ohjautuu oikealle varaukselle.", 79, 30, fontTimes, 8);

                    cb.ClosePath();
                }
                if (!string.IsNullOrEmpty(resMain.Holpack))
                {
                    object secondPages = getHolpackMedia(resMain.Holpack, resMain.BegDate);
                    if (secondPages != null)
                    {
                        try
                        {
                            PdfReader spReader = new PdfReader((byte[])secondPages);
                            for (int i = 0; i < spReader.NumberOfPages; i++)
                            {
                                doc.NewPage();
                                PdfImportedPage page;
                                page = pdfWriter.GetImportedPage(spReader, i + 1);
                                PdfContentByte cb = pdfWriter.DirectContent;
                                cb.AddTemplate(page, 0, 0);
                                cb.ClosePath();
                            }
                        }
                        catch { }
                    }
                }
                doc.Close();
                reader.Close();
                return fileNameF;
            }
            catch
            {
                return "";
            }
        }

        public string getPDF_SweMar(string pdfFilePath, string tempFilePath, string ResNo, string Market)
        {
            try
            {
                CultureInfo ci = new CultureInfo("sv-SE");
                Thread.CurrentThread.CurrentCulture = ci;

                string errorMsg = string.Empty;
                ReportData data = new TvReport.DetReport.DetReport().getReportData(Market, ResNo, ref errorMsg);
                ReportResMainRec resMain = data.ReportResMain;
                List<ResServiceRecord> resService = new DetReport().getResService(ResNo, ref errorMsg);

                List<FlightClassRecord> FlgClass = new DetReport().getFlightClass(Market, ref errorMsg);
                string fileNameF = ResNo + System.Guid.NewGuid() + ".pdf"; ;
                string fileName = tempFilePath + fileNameF;
                PdfReader reader = new PdfReader(pdfFilePath);
                int pageCount = reader.NumberOfPages;
                int currentpage = 1;
                byte[] pdfBytes = reader.GetPageContent(currentpage);

                if (File.Exists(fileName))
                    File.Delete(fileName);

                Document doc = new Document(reader.GetPageSize(1));
                PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Append, FileAccess.Write));
                doc.Open();
                byte[] pageContent = reader.GetPageContent(currentpage);

                var pageCnt = from q in data.PasPriceList
                              group q by new { q.PageNo } into k
                              select new { pageNo = k.Key.PageNo };
                var payPlan = from q1 in data.ResPayPlanList
                              from q2 in pageCnt
                              orderby q1.PayNo, q2.pageNo
                              select new { q1.ResNo, q1.PayNo, q1.DueDate, q1.Amount, q1.RemAmount, q1.Cur, q1.Adult, q1.Child, q1.Infant, q2.pageNo };

                string fontpath = HttpContext.Current.Server.MapPath(".");
                BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                BaseFont fontArialIta = BaseFont.CreateFont(fontpath + "\\Document\\ARIALI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                BaseFont fontTimes = BaseFont.CreateFont(fontpath + "\\Document\\TIMES.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                BaseFont fontTimesBold = BaseFont.CreateFont(fontpath + "\\Document\\TIMESBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                BaseFont fontVerdana = BaseFont.CreateFont(fontpath + "\\Document\\VERDANA.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                BaseFont fontVerdanaBold = BaseFont.CreateFont(fontpath + "\\Document\\VERDANAB.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                BaseFont fontOCR = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                foreach (var pcRow in payPlan)
                {
                    doc.NewPage();
                    PdfImportedPage page;
                    page = pdfWriter.GetImportedPage(reader, currentpage);
                    PdfContentByte cb = pdfWriter.DirectContent;
                    cb.AddTemplate(page, 0, 0);
                    writeText(ref cb, "BOKNINGSNUMBER: SWE-" + ResNo, 60, 821, fontArialBold, 9);
                    writeText(ref cb, "Bokad:  " + (resMain.ReceiveDate.HasValue ? resMain.ReceiveDate.Value.ToString("dd-MMM-yyyy") : ""), 307, 815, fontArial, 6);
                    writeText(ref cb, "av:  " + resMain.AgencyName, 390, 815, fontArial, 6);
                    writeText(ref cb, "Utskriven:  " + DateTime.Today.ToString("dd-MMM-yyyy"), 505, 815, fontArial, 6);
                    cb.SetLineWidth(25 / 100);
                    cb.MoveTo(300, 812);
                    cb.LineTo(584, 812);
                    cb.Stroke();
                    var flights = (from r in resService
                                   where r.ServiceType == "FLIGHT"
                                   group r by new { r.Service, r.BegDate, r.FlgClass } into k
                                   select new { Service = k.Key.Service, BegDate = k.Key.BegDate, FlgClass = k.Key.FlgClass }).OrderBy(o => o.BegDate);
                    if (flights != null && flights.Count() > 0)
                    {
                        writeText(ref cb, "Flvg nr", 395, 801, fontArial, 8);
                        writeText(ref cb, "Klass", 448, 801, fontArial, 8);
                        writeText(ref cb, "Datum", 497, 801, fontArial, 8);
                        writeText(ref cb, "Tid", 551, 801, fontArial, 8);

                        FlightDetailRecord flight = new DetReport().getFlightDetail(Market, flights.FirstOrDefault().Service, flights.FirstOrDefault().BegDate.Value, ref errorMsg);
                        string FlgClassName = FlgClass.Find(f => f.Class == flights.FirstOrDefault().FlgClass).NameL;
                        writeText(ref cb, flight.DepCityNameL, 305, 791, fontTimes, 8);
                        writeText(ref cb, string.IsNullOrEmpty(flight.PNRName) ? flight.FlightNo : flight.PNRName, 395, 791, fontTimes, 8);
                        writeText(ref cb, FlgClassName, 448, 791, fontTimes, 8);
                        writeText(ref cb, flight.FlyDate.ToString("dd-MMM-yyyy"), 497, 791, fontTimes, 8);
                        writeText(ref cb, flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "", 551, 791, fontTimes, 8);
                        writeText(ref cb, flight.ArrCityNameL, 305, 782, fontTimes, 8);
                        writeText(ref cb, flight.ArrDate.ToString("dd-MMM-yyyy"), 497, 782, fontTimes, 8);
                        writeText(ref cb, flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "", 551, 782, fontTimes, 8);

                        cb.SetLineWidth(25 / 100);
                        cb.MoveTo(300, 780);
                        cb.LineTo(584, 780);
                        cb.Stroke();
                        if (flights.Count() > 1)
                        {
                            flight = new DetReport().getFlightDetail(Market, flights.LastOrDefault().Service, flights.LastOrDefault().BegDate.Value, ref errorMsg);
                            FlgClassName = FlgClass.Find(f => f.Class == flights.LastOrDefault().FlgClass).NameL;
                            writeText(ref cb, flight.DepCityNameL, 305, 769, fontTimes, 8);
                            writeText(ref cb, string.IsNullOrEmpty(flight.PNRName) ? flight.FlightNo : flight.PNRName, 395, 769, fontTimes, 8);
                            writeText(ref cb, FlgClassName, 448, 769, fontTimes, 8);
                            writeText(ref cb, flight.FlyDate.ToString("dd-MMM-yyyy"), 497, 769, fontTimes, 8);
                            writeText(ref cb, flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "", 551, 769, fontTimes, 8);
                            writeText(ref cb, flight.ArrCityNameL, 305, 759, fontTimes, 8);
                            writeText(ref cb, flight.ArrDate.ToString("dd-MMM-yyyy"), 497, 759, fontTimes, 8);
                            writeText(ref cb, flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "", 551, 759, fontTimes, 8);
                            cb.SetLineWidth(25 / 100);
                            cb.MoveTo(305, 757);
                            cb.LineTo(584, 757);
                            cb.Stroke();
                        }
                    }
                    //+735

                    PassengersRecord _Leader = data.PassengerList.Find(f => f.Leader == "Y");
                    ResCustInfoRecord resCustInfo = new DetReport().getResCustInfo(Market, _Leader != null ? _Leader.CustNo : -1, ref errorMsg);
                    PassengersRecord _leader = data.PassengerList.Find(f => f.CustNo == _Leader.CustNo); ;
                    if (resCustInfo != null)
                    {
                        string Address = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHome : resCustInfo.AddrWork;
                        string Zip = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHomeZip : resCustInfo.AddrWorkZip;
                        string City = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHomeCity : resCustInfo.AddrWorkCity;

                        writeText(ref cb, _Leader.Surname + " " + _leader.Name, 60, 724, fontArialBold, 8);
                        writeText(ref cb, Address, 60, 711, fontArialBold, 8);
                        writeText(ref cb, Zip + "  " + City, 60, 698, fontArialBold, 8);
                    }

                    int y = 756; //+13
                    writeText(ref cb, "Resande:", 305, y - 13, fontArial, 7);
                    foreach (PassengersRecord row in data.PassengerList.Where(w => w.PageNo == pcRow.pageNo).Select(s => s))
                    {
                        y -= 13;
                        writeText(ref cb, row.SeqNo.Value.ToString(), 370, y, fontArial, 7);
                        if (row.Status.HasValue && row.Status.Value == 0)
                            writeText(ref cb, row.Title + ". " + row.Surname + " " + row.Name + (row.Age.HasValue && (row.TitleNo.HasValue && row.TitleNo.Value > 5) ? " " + row.Age.ToString() : ""), 382, y, fontArial, 7);
                        else writeText(ref cb, row.Title + ". " + row.Surname + " " + row.Name + (row.Age.HasValue && (row.TitleNo.HasValue && row.TitleNo.Value > 5) ? " " + row.Age.ToString() : ""), 382, y, fontArialIta, 7, iTextSharp.text.BaseColor.LIGHT_GRAY);
                    }

                    var hotels = from q in resService
                                 where q.ServiceType == "HOTEL"
                                 group q by new { q.Service, q.Room, q.Board, q.Accom, q.BegDate, q.EndDate } into k
                                 select new
                                 {
                                     k.Key.Service,
                                     k.Key.Room,
                                     k.Key.Board,
                                     k.Key.Accom,
                                     k.Key.BegDate,
                                     k.Key.EndDate,
                                     roomCnt = resService.Where(w => w.Service == k.Key.Service && w.Room == k.Key.Room && w.Board == k.Key.Board && w.Accom == k.Key.Accom && w.BegDate == k.Key.BegDate && w.EndDate == k.Key.EndDate).Count(),

                                 };
                    if (hotels != null && hotels.Count() > 0)
                    {
                        HotelRecord hotel = new DetReport().getHotelDetail(Market, hotels.FirstOrDefault().Service, ref errorMsg);

                        y = 660;
                        foreach (var row in hotels)
                        {
                            y -= 10;
                            HotelBoardRecord board = new DetReport().getHotelBoard(Market, row.Service, row.Board, ref errorMsg);
                            HotelRoomRecord room = new DetReport().getHotelRoom(Market, row.Service, row.Room, ref errorMsg);
                            HotelAccomRecord accom = new DetReport().getHotelAccom(Market, row.Service, row.Room, row.Accom, ref errorMsg);

                            writeText(ref cb, hotel.LocalName + " " + row.roomCnt.ToString() + " " + room.NameL + "  " + /*accom.LocalName + " " +*/ board.NameL + "  " + hotel.LocationName + "/" + new DetReport().getLocationForCountryName(Market, hotel.Location, ref errorMsg) + " " + (row.EndDate.Value - row.BegDate.Value).Days.ToString() + " DAGAR", 51, y, fontTimesBold, 7);
                            var query = from q in resService
                                        where q.Service == row.Service && q.Room == row.Room && q.Board == row.Board && q.BegDate == row.BegDate && q.EndDate == row.EndDate
                                        select q;

                            string suppNote = string.Empty;
                            foreach (var r1 in query)
                                if (!string.IsNullOrEmpty(r1.SupNote))
                                {
                                    if (suppNote.Length > 0) suppNote += ", ";
                                    suppNote += r1.SupNote;
                                }

                            if (!string.IsNullOrEmpty(resMain.ResNote) || !string.IsNullOrEmpty(suppNote))
                            {
                                y -= 10;
                                writeText(ref cb, !string.IsNullOrEmpty(resMain.ResNote) ? "REMARKS: " + resMain.ResNote : (!string.IsNullOrEmpty(suppNote) ? "WISH:" + suppNote : ""), 51, y, fontTimesBold, 7);
                            }
                        }
                    }
                    else
                        if (resMain.SaleResource == 2 || resMain.SaleResource == 3 || resMain.SaleResource == 5)
                        {
                            string onlyFlightNote = "ENDAST FLYGSTOL";
                            if (resMain.ArrCountry.HasValue && resMain.ArrCountry != 4 && resMain.ArrCountry != 72)
                                onlyFlightNote = "ENDAST FLYGSTOL";
                            writeText(ref cb, onlyFlightNote, 51, 650, fontTimesBold, 7);
                            writeText(ref cb, !string.IsNullOrEmpty(resMain.ResNote) ? "REMARKS: " + resMain.ResNote : "", 51, 540, fontTimesBold, 7);
                        }
                    //data.ServicePriceList

                    y = 610;
                    List<PasPricesRecord> pasPrice = (from q in data.PasPriceList
                                                      where q.PageNo == pcRow.pageNo
                                                      select q).ToList<PasPricesRecord>();
                    foreach (PasPricesRecord row in pasPrice)
                    {
                        y -= 11;
                        writeText(ref cb, row.ServiceDesc, 46, y, fontTimes, 8);

                        writeTextRightAlign(ref cb, row.Pax1Amount.HasValue ? row.Pax1Amount.Value.ToString("#,###") : "", 305, y, fontTimes, 8);
                        writeTextRightAlign(ref cb, row.Pax2Amount.HasValue ? row.Pax2Amount.Value.ToString("#,###") : "", 3556 / 10, y, fontTimes, 8);
                        writeTextRightAlign(ref cb, row.Pax3Amount.HasValue ? row.Pax3Amount.Value.ToString("#,###") : "", 4062 / 10, y, fontTimes, 8);
                        writeTextRightAlign(ref cb, row.Pax4Amount.HasValue ? row.Pax4Amount.Value.ToString("#,###") : "", 4568 / 10, y, fontTimes, 8);
                        writeTextRightAlign(ref cb, row.Pax5Amount.HasValue ? row.Pax5Amount.Value.ToString("#,###") : "", 5074 / 10, y, fontTimes, 8);
                        writeTextRightAlign(ref cb, row.Pax6Amount.HasValue ? row.Pax6Amount.Value.ToString("#,###") : "", 558, y, fontTimes, 8);
                    }

                    y = 370;
                    List<String> handcaps = getHandicaps_Fin(Market, ResNo, ref errorMsg);
                    foreach (string r1 in handcaps)
                    {
                        writeText(ref cb, r1.ToString(), 36, y, fontArialBold, 8);
                        y -= 10;
                    }

                    y = 407;
                    writeText(ref cb, "Summa per person:", 45, y, fontTimes, 8);
                    var sumPerson = from q in data.PasPriceList
                                    where q.PageNo == pcRow.pageNo
                                    select q;
                    decimal? totalSum = 0;
                    decimal? sum;
                    sum = sumPerson.Sum(s => s.Pax1Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, sum.HasValue ? sum.Value.ToString("#,###") : "", 305, y, fontTimes, 8);
                    sum = sumPerson.Sum(s => s.Pax2Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, sum.HasValue ? sum.Value.ToString("#,###") : "", 3556 / 10, y, fontTimes, 8);
                    sum = sumPerson.Sum(s => s.Pax3Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, sum.HasValue ? sum.Value.ToString("#,###") : "", 4062 / 10, y, fontTimes, 8);
                    sum = sumPerson.Sum(s => s.Pax4Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, sum.HasValue ? sum.Value.ToString("#,###") : "", 4568 / 10, y, fontTimes, 8);
                    sum = sumPerson.Sum(s => s.Pax5Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, sum.HasValue ? sum.Value.ToString("#,###") : "", 5074 / 10, y, fontTimes, 8);
                    sum = sumPerson.Sum(s => s.Pax6Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, sum.HasValue ? sum.Value.ToString("#,###") : "", 558, y, fontTimes, 8);
                    writeText(ref cb, "Totalt", 474, 390, fontTimesBold, 8);
                    writeTextRightAlign(ref cb, totalSum.HasValue ? totalSum.Value.ToString("#,###") : "", 558, 390, fontTimes, 8);

                    y = 390;
                    if (data.Ssrc != null && data.Ssrc.Count > 0)
                    {
                        foreach (string r1 in data.Ssrc)
                        {
                            y -= 12;
                            writeText(ref cb, r1, 45, y, fontTimes, 6);
                        }
                    }
                    //HANDPENNING pcRow.DueDate == payPlan.FirstOrDefault().DueDate ? "Handpenning":"Slutbetalning"
                    string msg = string.Format("{0} {1} {3} Detur tillhanda senast {2}",
                        pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value.ToString("#,###.00") : "0",
                        pcRow.Cur,
                        pcRow.DueDate.HasValue ? pcRow.DueDate.Value.ToString("dd MMM yyyy") : "",
                        pcRow.DueDate == payPlan.FirstOrDefault().DueDate ? "handpenning" : "slutbetalning");
                    writeText(ref cb, msg, 145, 255, fontArialBold, 9);

                    writeText(ref cb, ResNo, 6808 / 100, 20245 / 100, fontVerdanaBold, 6);

                    writeText(ref cb, _leader.Surname + " " + _leader.Name, 315, 180, fontArialBold, 8);
                    if (resCustInfo != null)
                    {
                        string Address = Equals(resCustInfo.InvoiceAddr, "H") ? resCustInfo.AddrHome : resCustInfo.AddrWork;
                        string Zip = Equals(resCustInfo.InvoiceAddr, "H") ? resCustInfo.AddrHomeZip : resCustInfo.AddrWorkZip;
                        string City = Equals(resCustInfo.InvoiceAddr, "H") ? resCustInfo.AddrHomeCity : resCustInfo.AddrWorkCity;

                        writeText(ref cb, Address, 315, 170, fontArialBold, 8);
                        writeText(ref cb, Zip + "  " + City, 315, 160, fontArialBold, 8);
                    }

                    writeText(ref cb, "720-6295", 310, 113, fontOCR, 8);
                    writeText(ref cb, "Detur Sweden AB", 410, 113, fontArialBold, 8);

                    decimal remAmount = pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value : 0;
                    string ocrStr = string.Empty;
                    ocrStr = "    " + ocrStr;
                    //ocrStr = "#41" + ocrStr;
                    //ocrStr = " 7206295" + ocrStr;

                    //ocrStr = ">                " + ocrStr;
                    //ocrStr = " 0    " + ocrStr;
                    //ocrStr = (remAmount - Math.Floor(remAmount)) > 0 ? Math.Floor((remAmount - Math.Floor(remAmount)) * 100).ToString("##") : "00" + ocrStr;
                    //ocrStr = " " + ocrStr;
                    //ocrStr = fillSpace((pcRow.RemAmount.HasValue ? Math.Floor(pcRow.RemAmount.Value).ToString() : "0"), 10) + ocrStr;
                    //ocrStr = " #" + ocrStr;
                    //ocrStr = fillSpace(data.GiroKID, 24) + ocrStr;
                    //ocrStr = "  # " + ocrStr;



                    //writeTextRightAlign(ref cb, ocrStr, 574, 43, fontOCR, 11);
                    //writeTextRightAlign(ref cb, "12345678901234567890123456789012345678901234567890123456789012345678901234567890123", 574, 30, fontOcr, 11);

                    //writeText(ref cb, "#", 43, 43, fontOCR, 11);
                    writeText(ref cb, data.GiroKID, 85, 43, fontOCR, 11);
                    //writeText(ref cb, "#", 239, 43, fontOCR, 11);
                    writeTextRightAlign(ref cb, pcRow.RemAmount.HasValue ? Math.Floor(pcRow.RemAmount.Value).ToString() : "0", 2674 / 10, 43, fontOCR, 11);
                    writeTextRightAlign(ref cb, (remAmount - Math.Floor(remAmount)) > 0 ? Math.Floor((remAmount - Math.Floor(remAmount)) * 100).ToString("##") : "00", 310, 43, fontOCR, 11);
                    writeText(ref cb, "0", 33565 / 100, 43, fontOCR, 11);

                    cb.ClosePath();
                }
                if (!string.IsNullOrEmpty(resMain.Holpack))
                {
                    object secondPages = getHolpackMedia(resMain.Holpack, resMain.BegDate);
                    if (secondPages != null)
                    {
                        try
                        {
                            PdfReader spReader = new PdfReader((byte[])secondPages);
                            for (int i = 0; i < spReader.NumberOfPages; i++)
                            {
                                doc.NewPage();
                                PdfImportedPage page;
                                page = pdfWriter.GetImportedPage(spReader, i + 1);
                                PdfContentByte cb = pdfWriter.DirectContent;
                                cb.AddTemplate(page, 0, 0);
                                cb.ClosePath();
                            }
                        }
                        catch { }
                    }
                }
                doc.Close();
                reader.Close();
                return fileNameF;
            }
            catch
            {
                return "";
            }
        }

        public string getPDF_DenMar(string pdfFilePath, string tempFilePath, string ResNo, string Market)
        {
            try
            {
                CultureInfo ci = new CultureInfo("da-DK");
                Thread.CurrentThread.CurrentCulture = ci;

                string errorMsg = string.Empty;
                ReportData data = new TvReport.DetReport.DetReport().getReportData(Market, ResNo, ref errorMsg);
                ReportResMainRec resMain = data.ReportResMain;
                List<ResServiceRecord> resService = new DetReport().getResService(ResNo, ref errorMsg);

                List<FlightClassRecord> FlgClass = new DetReport().getFlightClass(Market, ref errorMsg);

                string fileNameF = ResNo + System.Guid.NewGuid() + ".pdf"; ;
                string fileName = tempFilePath + fileNameF;
                PdfReader reader = new PdfReader(pdfFilePath);
                int pageCount = reader.NumberOfPages;
                int currentpage = 1;
                byte[] pdfBytes = reader.GetPageContent(currentpage);

                if (File.Exists(fileName))
                    File.Delete(fileName);

                Document doc = new Document(reader.GetPageSize(1));
                PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Append, FileAccess.Write));
                doc.Open();
                byte[] pageContent = reader.GetPageContent(currentpage);

                var pageCnt = from q in data.PasPriceList
                              group q by new { q.PageNo } into k
                              select new { pageNo = k.Key.PageNo };
                var payPlan = from q1 in data.ResPayPlanList
                              from q2 in pageCnt
                              orderby q1.PayNo, q2.pageNo
                              select new { q1.ResNo, q1.PayNo, q1.DueDate, q1.Amount, q1.RemAmount, q1.Cur, q1.Adult, q1.Child, q1.Infant, q2.pageNo };

                string fontpath = HttpContext.Current.Server.MapPath(".");
                BaseFont bfont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont fontHelv = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont fontHelvItalic = BaseFont.CreateFont(fontpath + "\\Document\\ARIALI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                BaseFont fontTime = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont fontOcr = BaseFont.CreateFont(fontpath + "\\Document\\OCR-B 10 BT.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
                foreach (var pcRow in payPlan)
                {

                    doc.NewPage();
                    PdfImportedPage page;
                    page = pdfWriter.GetImportedPage(reader, currentpage);
                    PdfContentByte cb = pdfWriter.DirectContent;

                    cb.AddTemplate(page, 0, 0);
                    writeText(ref cb, "Reservationsnummer : " + ResNo, 100, 820, bfont, 9);
                    writeText(ref cb, "Reserveret: " + (resMain.ReceiveDate.HasValue ? resMain.ReceiveDate.Value.ToShortDateString() : ""), 311, 830, fontHelv, 6);
                    writeText(ref cb, "af: " + resMain.AgencyName, 430, 830, fontHelv, 6);
                    writeText(ref cb, "Udskrevet: " + DateTime.Today.ToString("dd-MMM-yyyy"), 520, 830, fontHelv, 6);
                    cb.SetLineWidth(1);
                    cb.MoveTo(311, 827);
                    cb.LineTo(608, 827);
                    cb.Stroke();
                    var flights = (from r in resService
                                   where r.ServiceType == "FLIGHT"
                                   group r by new { r.Service, r.BegDate, r.FlgClass } into k
                                   select new { Service = k.Key.Service, BegDate = k.Key.BegDate, FlgClass = k.Key.FlgClass }).OrderBy(o => o.BegDate);
                    if (flights != null && flights.Count() > 0)
                    {
                        writeText(ref cb, "Fly nr", 400, 820, fontHelv, 6);
                        writeText(ref cb, "Klasse", 440, 820, fontHelv, 6);
                        writeText(ref cb, "Dato", 486, 820, fontHelv, 6);
                        writeText(ref cb, "Tid", 542, 820, fontHelv, 6);

                        FlightDetailRecord flight = new DetReport().getFlightDetail(Market, flights.FirstOrDefault().Service, flights.FirstOrDefault().BegDate.Value, ref errorMsg);
                        string FlgClassName = FlgClass.Find(f => f.Class == flights.FirstOrDefault().FlgClass).NameL;
                        writeText(ref cb, flight.DepCityNameL, 311, 810, fontTime, 6);
                        writeText(ref cb, string.IsNullOrEmpty(flight.PNRName) ? flight.FlightNo : flight.PNRName, 400, 810, fontTime, 6);
                        writeText(ref cb, FlgClassName, 440, 810, fontTime, 6);
                        writeText(ref cb, flight.FlyDate.ToString("dd-MMM-yyyy"), 486, 810, fontTime, 6);
                        writeText(ref cb, flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "", 542, 810, fontTime, 6);
                        writeText(ref cb, flight.ArrCityNameL, 311, 803, fontTime, 6);
                        writeText(ref cb, flight.ArrDate.ToString("dd-MMM-yyyy"), 486, 803, fontTime, 6);
                        writeText(ref cb, flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "", 542, 803, fontTime, 6);
                        cb.SetLineWidth(1);
                        cb.MoveTo(311, 800);
                        cb.LineTo(608, 800);
                        cb.Stroke();
                        if (flights.Count() > 1)
                        {
                            flight = new DetReport().getFlightDetail(Market, flights.LastOrDefault().Service, flights.LastOrDefault().BegDate.Value, ref errorMsg);
                            FlgClassName = FlgClass.Find(f => f.Class == flights.LastOrDefault().FlgClass).NameL;
                            writeText(ref cb, flight.DepCityNameL, 311, 790, fontTime, 6);
                            writeText(ref cb, string.IsNullOrEmpty(flight.PNRName) ? flight.FlightNo : flight.PNRName, 400, 790, fontTime, 6);
                            writeText(ref cb, FlgClassName, 440, 790, fontTime, 6);
                            writeText(ref cb, flight.FlyDate.ToString("dd-MMM-yyyy"), 486, 790, fontTime, 6);
                            writeText(ref cb, flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "", 542, 790, fontTime, 6);
                            writeText(ref cb, flight.ArrCityNameL, 311, 783, fontTime, 6);
                            writeText(ref cb, flight.ArrDate.ToString("dd-MMM-yyyy"), 486, 783, fontTime, 6);
                            writeText(ref cb, flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "", 542, 783, fontTime, 6);
                            cb.SetLineWidth(1);
                            cb.MoveTo(311, 781);
                            cb.LineTo(608, 781);
                            cb.Stroke();
                        }
                    }
                    //+735
                    PassengersRecord _Leader = data.PassengerList.Find(f => f.Leader == "Y");
                    ResCustInfoRecord resCustInfo = new DetReport().getResCustInfo(Market, _Leader != null ? _Leader.CustNo : -1, ref errorMsg);
                    PassengersRecord _leader = data.PassengerList.Find(f => f.CustNo == _Leader.CustNo); ;
                    if (resCustInfo != null)
                    {
                        string Address = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHome : resCustInfo.AddrWork;
                        string Zip = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHomeZip : resCustInfo.AddrWorkZip;
                        string City = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHomeCity : resCustInfo.AddrWorkCity;

                        writeText(ref cb, _leader.Name + " " + _leader.Surname, 30, 730, bfont, 8);
                        writeText(ref cb, Address, 30, 709, bfont, 8);
                        writeText(ref cb, Zip + "  " + City, 30, 695, bfont, 8);
                    }

                    int y = 767;
                    writeText(ref cb, "Rejsende:", 316, y, fontHelv, 8);
                    foreach (PassengersRecord row in data.PassengerList.Where(w => w.PageNo == pcRow.pageNo).Select(s => s))
                    {
                        writeText(ref cb, row.SeqNo.Value.ToString(), 384, y, fontHelv, 8);
                        if (row.Status.HasValue && row.Status.Value == 0)
                            writeText(ref cb, row.Title + ". " + row.Surname + " " + row.Name + (row.Age.HasValue && (row.TitleNo.HasValue && row.TitleNo.Value > 5) ? " " + row.Age.ToString() + " år" : ""), 396, y, fontHelv, 8);
                        else writeText(ref cb, row.Title + ". " + row.Surname + " " + row.Name + (row.Age.HasValue && (row.TitleNo.HasValue && row.TitleNo.Value > 5) ? " " + row.Age.ToString() + " år" : ""), 396, y, fontHelvItalic, 8, iTextSharp.text.BaseColor.LIGHT_GRAY);
                        y -= 11;
                    }

                    var hotels = from q in resService
                                 where q.ServiceType == "HOTEL"
                                 group q by new { q.Service, q.Room, q.Board, q.Accom, q.BegDate, q.EndDate } into k
                                 select new
                                 {
                                     k.Key.Service,
                                     k.Key.Room,
                                     k.Key.Board,
                                     k.Key.Accom,
                                     k.Key.BegDate,
                                     k.Key.EndDate,
                                     roomCnt = resService.Where(w => w.Service == k.Key.Service && w.Room == k.Key.Room && w.Board == k.Key.Board && w.Accom == k.Key.Accom && w.BegDate == k.Key.BegDate && w.EndDate == k.Key.EndDate).Count()
                                 };

                    if (hotels != null && hotels.Count() > 0)
                    {
                        HotelRecord hotel = new DetReport().getHotelDetail(Market, hotels.FirstOrDefault().Service, ref errorMsg);

                        y = 672;
                        foreach (var row in hotels)
                        {
                            y -= 10;
                            HotelBoardRecord board = new DetReport().getHotelBoard(Market, row.Service, row.Board, ref errorMsg);
                            HotelRoomRecord room = new DetReport().getHotelRoom(Market, row.Service, row.Room, ref errorMsg);
                            HotelAccomRecord accom = new DetReport().getHotelAccom(Market, row.Service, row.Room, row.Accom, ref errorMsg);
                            writeText(ref cb, hotel.LocalName + " " + row.roomCnt.ToString() + " " + room.NameL + "  " /*+ accom.LocalName + " " */+ board.NameL + "  " + hotel.LocationName + "/" + new DetReport().getLocationForCountryName(Market, hotel.Location, ref errorMsg) + " " + (row.EndDate.Value - row.BegDate.Value).Days.ToString() + " OVERNATNINGER", 25, y, fontTime, 8);
                        }

                        if (!string.IsNullOrEmpty(resMain.ResNote))
                        {
                            y -= 10;
                            writeText(ref cb, "Bemærkninger: " + resMain.ResNote, 25, y, fontTime, 8);
                        }
                    }

                    y = 617;
                    List<PasPricesRecord> pasPrice = (from q in data.PasPriceList
                                                      where q.PageNo == pcRow.pageNo
                                                      select q).ToList<PasPricesRecord>();
                    foreach (PasPricesRecord row in pasPrice)
                    {
                        y -= 11;
                        writeText(ref cb, row.ServiceDesc, 25, y, fontTime, 7);

                        writeTextRightAlign(ref cb, row.Pax1Amount.HasValue ? row.Pax1Amount.Value.ToString("#,###") : "", 278, y, fontTime, 7);
                        writeTextRightAlign(ref cb, row.Pax2Amount.HasValue ? row.Pax2Amount.Value.ToString("#,###") : "", 329, y, fontTime, 7);
                        writeTextRightAlign(ref cb, row.Pax3Amount.HasValue ? row.Pax3Amount.Value.ToString("#,###") : "", 380, y, fontTime, 7);
                        writeTextRightAlign(ref cb, row.Pax4Amount.HasValue ? row.Pax4Amount.Value.ToString("#,###") : "", 430, y, fontTime, 7);
                        writeTextRightAlign(ref cb, row.Pax5Amount.HasValue ? row.Pax5Amount.Value.ToString("#,###") : "", 480, y, fontTime, 7);
                        writeTextRightAlign(ref cb, row.Pax6Amount.HasValue ? row.Pax6Amount.Value.ToString("#,###") : "", 530, y, fontTime, 7);
                    }

                    y = 437;
                    writeText(ref cb, "Pris pr person:", 25, y, fontTime, 7);
                    var sumPerson = from q in data.PasPriceList
                                    where q.PageNo == pcRow.pageNo
                                    select q;
                    decimal? totalSum = 0;
                    decimal? sum;
                    sum = sumPerson.Sum(s => s.Pax1Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 278, y, fontTime, 7);
                    sum = sumPerson.Sum(s => s.Pax2Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 329, y, fontTime, 7);
                    sum = sumPerson.Sum(s => s.Pax3Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 380, y, fontTime, 7);
                    sum = sumPerson.Sum(s => s.Pax4Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 430, y, fontTime, 7);
                    sum = sumPerson.Sum(s => s.Pax5Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 480, y, fontTime, 7);
                    sum = sumPerson.Sum(s => s.Pax6Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 530, y, fontTime, 7);

                    // Handicaps
                    y = 400;
                    List<String> handcaps = getHandicaps_Fin(Market, ResNo, ref errorMsg);
                    foreach (string r1 in handcaps)
                    {
                        writeText(ref cb, r1.ToString(), 30, y, fontTime, 8);
                        y -= 10;
                    }
                    // Handicaps

                    writeText(ref cb, "For din og vores sikkerhed tager vi ikke imod kontanter i vores butikker", 25, 422, fontTime, 7);
                    writeText(ref cb, "Total", 470, 422, fontTime, 7);
                    writeTextRightAlign(ref cb, totalSum.HasValue ? totalSum.Value.ToString("#,###") : "", 530, 422, fontTime, 7);
                    writeText(ref cb, "Læs venligst den VIGTIGE REJSE- OG BETALINGSINFORMATION på næste side", 25, 411, fontTime, 7);
                    y = 360;
                    if (data.Ssrc != null && data.Ssrc.Count > 0)
                    {
                        foreach (string r1 in data.Ssrc)
                        {
                            y -= 12;
                            writeText(ref cb, r1, 25, y, fontTime, 7);
                        }
                    }

                    string msg = string.Format("{0} {1} {2} Indbetales til Detur senest {3}",
                        pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value.ToString("#,###.00") : "0",
                        pcRow.Cur,
                        pcRow.DueDate == payPlan.FirstOrDefault().DueDate ? "Depositum" : "Restbetaling",
                        pcRow.DueDate.HasValue ? pcRow.DueDate.Value.ToString("dd MMM yyyy") : "");
                    writeText(ref cb, msg, 160, 340, bfont, 8);

                    writeText(ref cb, _leader.Name + " " + _leader.Surname, 40, 203, bfont, 8);
                    if (resCustInfo != null)
                    {
                        string Address = Equals(resCustInfo.InvoiceAddr, "H") ? resCustInfo.AddrHome : resCustInfo.AddrWork;
                        string Zip = Equals(resCustInfo.InvoiceAddr, "H") ? resCustInfo.AddrHomeZip : resCustInfo.AddrWorkZip;
                        string City = Equals(resCustInfo.InvoiceAddr, "H") ? resCustInfo.AddrHomeCity : resCustInfo.AddrWorkCity;

                        writeText(ref cb, Address, 40, 190, bfont, 8);
                        writeText(ref cb, Zip + "  " + City, 40, 180, bfont, 8);
                    }

                    writeText(ref cb, pcRow.RemAmount.HasValue ? "Balance: " + pcRow.RemAmount.Value.ToString("#,###. 00") + " " + pcRow.Cur + " til betaling " + pcRow.DueDate.Value.ToShortDateString() : "", 40, 145, fontTime, 7);

                    writeText(ref cb, ResNo, 40, 117, fontOcr, 9);

                    writeText(ref cb, pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value.ToString("#.") : "", 23, 60, fontOcr, 22);
                    string remAmount = pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value.ToString("####.00") : "";
                    int i1 = remAmount.IndexOf('.') > 0 ? remAmount.IndexOf('.') : (remAmount.IndexOf(',') > 0 ? remAmount.IndexOf(',') : 0);
                    writeTextRightAlign(ref cb, remAmount.Length > 0 ? remAmount.Substring(i1 + 1) : "", 181, 60, fontOcr, 22);
                    writeText(ref cb, pcRow.DueDate.HasValue ? pcRow.DueDate.Value.ToString("ddMMyy") : "", 219, 60, fontOcr, 22);
                    //writeText(ref cb, remAmount.Length > 0 ? remAmount.Substring(i1 + 1) : "", 140, 60, fontOcr, 8);

                    writeText(ref cb, pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value.ToString("#.") : "", 424, 60, fontOcr, 22);
                    writeTextRightAlign(ref cb, remAmount.Length > 0 ? remAmount.Substring(i1 + 1) : "", 578, 60, fontOcr, 22);

                    writeText(ref cb, "+71 < 000000" + data.GiroKID + "+80407637", 23, 12, fontOcr, 12);
                    writeText(ref cb, ResNo, 462, 12, fontOcr, 12);

                    cb.ClosePath();
                }

                #region Removed 29.02.2012 Add pdf
                /*
                string lastPage = string.Empty;

                lastPage += "<html><head><title></title></head><body>";
                lastPage += "<table style=\"font-family:Arial;font-size:8px;width:180mm;border: solid 4px #FFA800;\" cellpadding=\"4\" cellspacing=\"0\">";
                lastPage += "<tr>";
                lastPage += "<td style=\"border-style:solid 2px #333333;\">";
                lastPage += "<p>";
                lastPage += "Ekstra information :";
                lastPage += getHandicaps_Den("DENMAR", ResNo, ref errorMsg);
                lastPage += "</p>";
                lastPage += "</td>";
                lastPage += "</tr>";
                lastPage += "<tr>";
                lastPage += "<td style=\"border-style:solid;border-width:1px;border-color:#333333;\">";
                lastPage += "<p><b>VIGTIG REJSE- OG BETALINGSINFORMATION:</b><br />";
                lastPage += "Dette dokument er dit rejsebevis og bør behandles som et værdipapir.</p>";
                lastPage += "<p><b>Rejseaftale: </b>Rejsebetingelserne danner grundlaget for dine rettigheder som rejsende. Derfor er det <br />";
                lastPage += "vigtigt og nødvendigt, at du gør dig bekendt med vores rejsebetingelser i vores kataloget <br />";
                lastPage += "eller på www.detur.dk. </p>";
                lastPage += "<p>Det er den rejsendes ansvar at kontrollere at rejsedokumentet stemmer overens med den bestilling der er <br />";
                lastPage += "foretaget, og med det samme tager kontakt til Detur hvis ikke billetten/rejsebeviset er korrekt ifølge bestillingen.</p>";
                lastPage += "<p><b>Betaling: </b>Husk altid at påføre dit reservationsnummer når du betaler din rejse via bank, netbank eller <br />";
                lastPage += "posthuset.</p>";
                lastPage += "<p> Depositum skal være indbetalt senest 7 dage efter bestilling. </p>";
                lastPage += "<p> Resterende beløb skal indbetales senest 30 dage før afrejse. Hvis der er mindre end 30 dage<br />";
                lastPage += "til afrejsen, skal hele beløbet betales med det samme.</p>";
                lastPage += "<p> NB! Din billet er først gyldigt når totalbeløbet står i kr 0,- på rejsebeviset.</p>";
                lastPage += "<p><b>Afrejsen: </b>Indtjekning i lufthavnen senest 2 timer før afrejse.</p>";
                lastPage += "<p><b>Hjemrejsen: </b>Se venligst på vores informationstavle, -mappe på hotellet, for afhentningstidspunktet på dit hotel. <br />";
                lastPage += "Du er også velkommen til at ringe til vores guider, på vores lokale Detur kontor.</p>";
                lastPage += "<p> Hvis du kun har købt FLYBILLET, beder vi dig kontakte vores lokale kontor, for at bekræfte din <br />";
                lastPage += "hjemrejse 24 timer før. </p>";
                lastPage += "<p><b>Kontakt: </b>Detur – Antalya: +90 242 323 56 49           Detur – Alanya: +90 242 511 98 88.</p>";
                lastPage += "<p><b>Bagage: </b>Personer over 2 år må medbringe følgende:<br />";
                lastPage += "Indchecket bagage: 20kg<br/>";
                lastPage += "Håndbagage: 5kg<br/>";
                lastPage += "Golfbag: 15kg</p>";
                lastPage += "<p>Overvægt:      kr 50 pr kg. (Betales ved indtjekning) </p>";
                lastPage += "<p>Prisen for golfbags er kr 200,- pr vej pr person, og skal bestilles samt betales inden afrejse (Bemærk:  Der er <br />";
                lastPage += "begrænset antal golfbags på hvert fly). </p>";
                lastPage += " <p style=\"color: #FFA800\"> ";
                lastPage += " Detur ønsker dig en god ferie! ";
                lastPage += " </p> ";
                lastPage += " </td> ";
                lastPage += " </tr> ";
                lastPage += " <tr> ";
                lastPage += " <td style=\"font-size:10px;color:#000;text-align:center;\"> ";
                lastPage += " <p>";
                lastPage += " <b>Mere information finder du på www.detur.dk. Der kan du også betale rejsen online.</b>";
                lastPage += " </p> ";
                lastPage += " </td> ";
                lastPage += " </tr> ";
                lastPage += " </table>";
                lastPage += "</body></html>";
                */
                #endregion

                string file = pdfFilePath.ToLower().Replace(".pdf", "Back.pdf");
                PdfReader readLastPage = new PdfReader(file);
                doc.NewPage();
                PdfImportedPage pageLastPdf = pdfWriter.GetImportedPage(readLastPage, 1);
                PdfContentByte cb2 = pdfWriter.DirectContent;
                cb2.AddTemplate(pageLastPdf, 0, 0);
                cb2.ClosePath();
                if (!string.IsNullOrEmpty(resMain.Holpack))
                {
                    object secondPages = getHolpackMedia(resMain.Holpack, resMain.BegDate);
                    if (secondPages != null)
                    {
                        try
                        {
                            PdfReader spReader = new PdfReader((byte[])secondPages);
                            for (int i = 0; i < spReader.NumberOfPages; i++)
                            {
                                doc.NewPage();
                                PdfImportedPage page;
                                page = pdfWriter.GetImportedPage(spReader, i + 1);
                                PdfContentByte cb = pdfWriter.DirectContent;
                                cb.AddTemplate(page, 0, 0);
                                cb.ClosePath();
                            }
                        }
                        catch { }
                    }
                }
                doc.Close();
                reader.Close();
                return fileNameF;
            }
            catch
            {
                return "";
            }
        }

        public string getPDF_NorMar(string pdfFilePath, string tempFilePath, string ResNo, string Market)
        {
            CultureInfo ci = new CultureInfo("nb-NO");
            Thread.CurrentThread.CurrentCulture = ci;

            string errorMsg = string.Empty;
            ReportData data = new TvReport.DetReport.DetReport().getReportData(Market, ResNo, ref errorMsg);
            ReportResMainRec resMain = data.ReportResMain;
            List<ResServiceRecord> resService = new DetReport().getResService(ResNo, ref errorMsg);

            List<FlightClassRecord> FlgClass = new DetReport().getFlightClass(Market, ref errorMsg);

            string fileNameF = ResNo + System.Guid.NewGuid() + ".pdf"; ;
            string fileName = tempFilePath + fileNameF;
            PdfReader reader = new PdfReader(pdfFilePath);
            int pageCount = reader.NumberOfPages;
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            try
            {
                if (File.Exists(fileName))
                    File.Delete(fileName);

                Document doc = new Document(reader.GetPageSize(1));
                PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Append, FileAccess.Write));
                doc.Open();
                byte[] pageContent = reader.GetPageContent(currentpage);

                var pageCnt = from q in data.PasPriceList
                              group q by new { q.PageNo } into k
                              select new { pageNo = k.Key.PageNo };
                var payPlan = from q1 in data.ResPayPlanList
                              from q2 in pageCnt
                              orderby q1.PayNo, q2.pageNo
                              select new { q1.ResNo, q1.PayNo, q1.DueDate, q1.Amount, q1.RemAmount, q1.Cur, q1.Adult, q1.Child, q1.Infant, q2.pageNo };

                string fontpath = HttpContext.Current.Server.MapPath(".");
                BaseFont bfont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont fontHelv = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                BaseFont fontAng = BaseFont.CreateFont(fontpath + "\\Document\\ANGSA.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                BaseFont fontAngBold = BaseFont.CreateFont(fontpath + "\\Document\\ANGSAB.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                BaseFont fontAngIta = BaseFont.CreateFont(fontpath + "\\Document\\ANGSAI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                BaseFont fontOcr = BaseFont.CreateFont(fontpath + "\\Document\\OCR-B 10 BT.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
                foreach (var pcRow in payPlan)
                {

                    doc.NewPage();
                    PdfImportedPage page;
                    page = pdfWriter.GetImportedPage(reader, currentpage);
                    PdfContentByte cb = pdfWriter.DirectContent;

                    cb.AddTemplate(page, 0, 0);
                    writeText(ref cb, "BEKREFTELSE/BILLETT NR. " + ResNo, 40, 820, fontAngBold, 14);
                    writeText(ref cb, "Dato: " + DateTime.Today.ToShortDateString(), 518, 823, fontAng, 8);

                    var flights = (from r in resService
                                   where r.ServiceType == "FLIGHT"
                                   group r by new { r.Service, r.BegDate, r.FlgClass } into k
                                   select new { Service = k.Key.Service, BegDate = k.Key.BegDate, FlgClass = k.Key.FlgClass }).OrderBy(o => o.BegDate);
                    if (flights != null && flights.Count() > 0)
                    {

                        writeText(ref cb, "FLY NR", 427, 815, fontAngBold, 8);
                        writeText(ref cb, "KLASSE", 470, 815, fontAngBold, 8);
                        writeText(ref cb, "DATO", 518, 815, fontAngBold, 8);
                        writeText(ref cb, "KL", 564, 815, fontAngBold, 8);

                        cb.SetLineWidth(1);
                        cb.MoveTo(313, 813);
                        cb.LineTo(590, 813);
                        cb.Stroke();


                        FlightDetailRecord flight = new DetReport().getFlightDetail(Market, flights.FirstOrDefault().Service, flights.FirstOrDefault().BegDate.Value, ref errorMsg);
                        string FlgClassName = FlgClass.Find(f => f.Class == flights.FirstOrDefault().FlgClass).NameL;
                        writeTextRightAlign(ref cb, "Avreise", 345, 805, fontAngBold, 9);
                        writeText(ref cb, flight.DepCityNameL, 359, 805, fontAng, 9);
                        writeText(ref cb, string.IsNullOrEmpty(flight.PNRName) ? flight.FlightNo : flight.PNRName, 427, 805, fontAng, 9);
                        writeText(ref cb, FlgClassName, 470, 805, fontAng, 9);
                        writeText(ref cb, flight.FlyDate.ToString("dd-MMM-yyyy"), 518, 805, fontAng, 9);
                        writeText(ref cb, flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "", 564, 805, fontAng, 9);

                        writeTextRightAlign(ref cb, "Ankomst", 345, 797, fontAngBold, 9);
                        writeText(ref cb, flight.ArrCityNameL, 359, 797, fontAng, 9);
                        writeText(ref cb, flight.ArrDate.ToString("dd-MMM-yyyy"), 518, 797, fontAng, 9);
                        writeText(ref cb, flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "", 564, 797, fontAng, 9);

                        cb.SetLineWidth(1);
                        cb.MoveTo(313, 795);
                        cb.LineTo(590, 795);
                        cb.Stroke();
                        if (flights.Count() > 1)
                        {
                            flight = new DetReport().getFlightDetail(Market, flights.LastOrDefault().Service, flights.LastOrDefault().BegDate.Value, ref errorMsg);
                            FlgClassName = FlgClass.Find(f => f.Class == flights.LastOrDefault().FlgClass).NameL;

                            writeTextRightAlign(ref cb, "Hjemreise", 345, 787, fontAngBold, 9);
                            writeText(ref cb, flight.DepCityNameL, 359, 787, fontAng, 9);
                            writeText(ref cb, string.IsNullOrEmpty(flight.PNRName) ? flight.FlightNo : flight.PNRName, 427, 787, fontAng, 9);
                            writeText(ref cb, FlgClassName, 470, 787, fontAng, 9);
                            writeText(ref cb, flight.FlyDate.ToString("dd-MMM-yyyy"), 518, 787, fontAng, 9);
                            writeText(ref cb, flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "", 564, 787, fontAng, 9);

                            writeTextRightAlign(ref cb, "Hjemkomst", 345, 779, fontAngBold, 9);
                            writeText(ref cb, flight.ArrCityNameL, 359, 779, fontAng, 9);
                            writeText(ref cb, flight.ArrDate.ToString("dd-MMM-yyyy"), 518, 779, fontAng, 9);
                            writeText(ref cb, flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "", 564, 779, fontAng, 9);

                            cb.SetLineWidth(1);
                            cb.MoveTo(311, 777);
                            cb.LineTo(608, 777);
                            cb.Stroke();
                        }
                    }

                    writeTextRightAlign(ref cb, "Deres selger: ", 345, 769, fontAng, 9);
                    writeText(ref cb, resMain.AgencyUserName, 359, 769, fontAng, 9);
                    writeTextRightAlign(ref cb, "Utskriftsdato: " + ResNo, 470, 769, fontAng, 9);

                    PassengersRecord _Leader = data.PassengerList.Find(f => f.Leader == "Y");
                    ResCustInfoRecord resCustInfo = new DetReport().getResCustInfo(Market, _Leader != null ? _Leader.CustNo : -1, ref errorMsg);
                    PassengersRecord _leader = data.PassengerList.Find(f => f.CustNo == _Leader.CustNo); ;
                    if (resCustInfo != null)
                    {
                        string Address = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHome : resCustInfo.AddrWork;
                        string Zip = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHomeZip : resCustInfo.AddrWorkZip;
                        string City = !Equals(resCustInfo.InvoiceAddr, "W") ? resCustInfo.AddrHomeCity : resCustInfo.AddrWorkCity;

                        writeText(ref cb, _leader.Name + " " + _leader.Surname, 40, 730, bfont, 8);
                        writeText(ref cb, Address, 40, 709, bfont, 8);
                        writeText(ref cb, Zip, 40, 695, bfont, 8);
                        writeText(ref cb, City, 100, 695, bfont, 8);
                    }

                    int y = 758; //+15                                    
                    writeText(ref cb, "Navn :", 300, 747, fontAngBold, 10);
                    int iCnt = 0;
                    foreach (PassengersRecord row in data.PassengerList.Where(w => w.PageNo == pcRow.pageNo).Select(s => s))
                    {
                        iCnt++;
                        y -= 11;
                        if (row.Status.HasValue && row.Status.Value == 0)
                            writeText(ref cb, iCnt.ToString() + ". " + row.Name + " " + row.Surname + (row.Age.HasValue && (row.TitleNo.HasValue && row.TitleNo.Value > 5) ? " " + row.Age.ToString() + " år" : ""), 326, y, fontAng, 10);
                        else writeText(ref cb, iCnt.ToString() + ". " + row.Name + " " + row.Surname + (row.Age.HasValue && (row.TitleNo.HasValue && row.TitleNo.Value > 5) ? " " + row.Age.ToString() + " år" : ""), 326, y, fontAngIta, 10, iTextSharp.text.BaseColor.LIGHT_GRAY);
                    }

                    var hotels = from q in resService
                                 where q.ServiceType == "HOTEL"
                                 group q by new { q.Service, q.Room, q.Board, q.Accom, q.BegDate, q.EndDate } into k
                                 select new
                                 {
                                     k.Key.Service,
                                     k.Key.Room,
                                     k.Key.Board,
                                     k.Key.Accom,
                                     k.Key.BegDate,
                                     k.Key.EndDate,
                                     roomCnt = resService.Where(w => w.Service == k.Key.Service && w.Room == k.Key.Room && w.Board == k.Key.Board && w.Accom == k.Key.Accom && w.BegDate == k.Key.BegDate && w.EndDate == k.Key.EndDate).Count()
                                 };
                    if (hotels != null && hotels.Count() > 0)
                    {
                        HotelRecord hotel = new DetReport().getHotelDetail(Market, hotels.FirstOrDefault().Service, ref errorMsg);

                        y = 672;
                        foreach (var row in hotels)
                        {
                            y -= 10;
                            HotelBoardRecord board = new DetReport().getHotelBoard(Market, row.Service, row.Board, ref errorMsg);
                            HotelRoomRecord room = new DetReport().getHotelRoom(Market, row.Service, row.Room, ref errorMsg);
                            HotelAccomRecord accom = new DetReport().getHotelAccom(Market, row.Service, row.Room, row.Accom, ref errorMsg);
                            writeText(ref cb, hotel.LocalName + " " + row.roomCnt.ToString() + " " + room.NameL + "  " + /*accom.LocalName + " " +*/ board.NameL + "  " + hotel.LocationName + "/" + new DetReport().getLocationForCountryName(Market, hotel.Location, ref errorMsg) + " " + (row.EndDate.Value - row.BegDate.Value).Days.ToString() + " OVERNATTINGER", 40, y, fontAng, 10);
                        }

                        if (!string.IsNullOrEmpty(resMain.ResNote))
                        {
                            y -= 10;
                            writeText(ref cb, "Notes: " + resMain.ResNote, 40, y, fontAng, 10);
                        }
                    }
                    else
                        if (resMain.SaleResource == 2 || resMain.SaleResource == 3 || resMain.SaleResource == 5)
                        {
                            writeText(ref cb, "KUN FLY", 40, 662, fontAngBold, 10);
                        }

                    y = 617;
                    List<PasPricesRecord> pasPrice = (from q in data.PasPriceList
                                                      where q.PageNo == pcRow.pageNo
                                                      select q).ToList<PasPricesRecord>();
                    foreach (PasPricesRecord row in pasPrice)
                    {
                        y -= 11;
                        writeText(ref cb, row.ServiceDesc, 36, y, fontAng, 10);

                        writeTextRightAlign(ref cb, row.Pax1Amount.HasValue ? row.Pax1Amount.Value.ToString("#,###") : "", 289, y, fontAng, 10);
                        writeTextRightAlign(ref cb, row.Pax2Amount.HasValue ? row.Pax2Amount.Value.ToString("#,###") : "", 340, y, fontAng, 10);
                        writeTextRightAlign(ref cb, row.Pax3Amount.HasValue ? row.Pax3Amount.Value.ToString("#,###") : "", 391, y, fontAng, 10);
                        writeTextRightAlign(ref cb, row.Pax4Amount.HasValue ? row.Pax4Amount.Value.ToString("#,###") : "", 441, y, fontAng, 10);
                        writeTextRightAlign(ref cb, row.Pax5Amount.HasValue ? row.Pax5Amount.Value.ToString("#,###") : "", 491, y, fontAng, 10);
                        writeTextRightAlign(ref cb, row.Pax6Amount.HasValue ? row.Pax6Amount.Value.ToString("#,###") : "", 541, y, fontAng, 10);
                    }

                    y = 437;
                    //writeText(ref cb, "Summa per person:", 45, y, fontTime, 7);
                    var sumPerson = from q in data.PasPriceList
                                    where q.PageNo == pcRow.pageNo
                                    select q;
                    decimal? totalSum = 0;
                    decimal? sum;
                    sum = sumPerson.Sum(s => s.Pax1Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 289, y, fontAng, 10);
                    sum = sumPerson.Sum(s => s.Pax2Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 340, y, fontAng, 10);
                    sum = sumPerson.Sum(s => s.Pax3Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 391, y, fontAng, 10);
                    sum = sumPerson.Sum(s => s.Pax4Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 441, y, fontAng, 10);
                    sum = sumPerson.Sum(s => s.Pax5Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 491, y, fontAng, 10);
                    sum = sumPerson.Sum(s => s.Pax6Amount);
                    totalSum += sum;
                    writeTextRightAlign(ref cb, (sum.HasValue && sum.Value > 0) ? sum.Value.ToString("#,###") : "", 541, y, fontAng, 10);
                    writeText(ref cb, "Total", 470, 422, fontAng, 10);
                    writeTextRightAlign(ref cb, totalSum.HasValue ? totalSum.Value.ToString("#,###") : "", 541, 422, fontAng, 10);

                    // Handicaps
                    y = 426;
                    List<String> handcaps = getHandicaps_Fin(Market, ResNo, ref errorMsg);
                    foreach (string r1 in handcaps)
                    {
                        writeText(ref cb, r1.ToString(), 30, y, fontAng, 8);
                        y -= 10;
                    }
                    // Handicaps

                    y = 360;
                    if (data.Ssrc != null && data.Ssrc.Count > 0)
                    {
                        foreach (string r1 in data.Ssrc)
                        {
                            y -= 12;
                            writeText(ref cb, r1, 25, y, fontAng, 10);
                        }
                    }

                    string msg = string.Format("{0} {1} {2} Innbetales til Detur Senest {3}",
                        pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value.ToString("#,###.00") : "0",
                        pcRow.Cur,
                        pcRow.DueDate == payPlan.FirstOrDefault().DueDate ? "Depositum" : "Restbetaling",
                        pcRow.DueDate.HasValue ? pcRow.DueDate.Value.ToString("dd MMM yyyy") : "");
                    writeText(ref cb, msg, 126, 350, fontAngBold, 12);

                    writeText(ref cb, pcRow.DueDate.HasValue ? pcRow.DueDate.Value.ToString("dd-MMM-yyyy") : "", 527, 248, fontAngBold, 14);
                    writeText(ref cb, "Bekreftelse/billett nr. " + ResNo, 45, 233, fontAng, 10);

                    writeText(ref cb, _leader.Name + " " + _leader.Surname, 45, 163, bfont, 8);
                    if (resCustInfo != null)
                    {
                        string Address = Equals(resCustInfo.InvoiceAddr, "H") ? resCustInfo.AddrHome : resCustInfo.AddrWork;
                        string Zip = Equals(resCustInfo.InvoiceAddr, "H") ? resCustInfo.AddrHomeZip : resCustInfo.AddrWorkZip;
                        string City = Equals(resCustInfo.InvoiceAddr, "H") ? resCustInfo.AddrHomeCity : resCustInfo.AddrWorkCity;

                        writeText(ref cb, Address, 45, 152, bfont, 8);
                        writeText(ref cb, Zip, 45, 141, bfont, 8);
                        writeText(ref cb, City, 130, 141, bfont, 8);
                    }

                    writeTextRightAlign(ref cb, pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value.ToString("# ###.00") : "", 271, 302, fontOcr, 10);


                    writeText(ref cb, data.GiroKID, 62, 62, fontOcr, 10);

                    writeTextRightAlign(ref cb, pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value.ToString("######") : "", 233, 62, fontOcr, 10);
                    string remAmount = pcRow.RemAmount.HasValue ? pcRow.RemAmount.Value.ToString("#.00") : "";
                    int i1 = remAmount.IndexOf('.') > 0 ? remAmount.IndexOf('.') : (remAmount.IndexOf(',') > 0 ? remAmount.IndexOf(',') : 0);
                    writeText(ref cb, remAmount.Length > 0 ? remAmount.Substring(i1 + 1) : "", 242, 62, fontOcr, 10);
                    string aa = string.Format("< {0} >", data.GiroKID.Length > 0 ? data.GiroKID.Substring(data.GiroKID.Length - 1, 1) : "");
                    writeText(ref cb, aa, 290, 62, fontOcr, 10);

                    writeText(ref cb, "9001 10 02802", 335, 62, fontOcr, 10);

                    cb.ClosePath();
                }

                #region Removed 28.05.2012 Add pdf
                /*
                string lastPage = string.Empty;
                lastPage += "<html><head><title></title></head><body>";
                lastPage += "<table style=\"font-family:Arial;font-size:8px;width:180mm;border:1px;\" cellpadding=\"4\" cellspacing=\"0\">";
                lastPage += "<tr>";
                lastPage += "<td style=\"border-style:solid;border-width:1px;border-color:#333333;\">";
                lastPage += "<p>";
                lastPage += "Ekstra information :";
                lastPage += getHandicaps_Den("NORMAR", ResNo, ref errorMsg);
                lastPage += "</p>";
                lastPage += "</td>";
                lastPage += "</tr>";
                lastPage += "<tr>";
                lastPage += " <td style=\"border-style:solid;border-width:1px;border-color:#333333;\">";
                lastPage += " <p>";
                lastPage += " Dette dokumentet er ditt reisebevis og må behandles som et verdipapir.";
                lastPage += " </p>";                
                lastPage += " <p> ";
                lastPage += " Dette danner grunnlaget for dine rettigheter som reisende. ";
                lastPage += " </p> ";
                lastPage += " <p> ";
                lastPage += " Ansvar for å kontrollere at reisedokumentet stemmer";
                lastPage += " Det er den reisendes ansvar å kontrollere at reisedokumentet stemmer overens med det som er bestilt. Dersom";
                lastPage += " billetten ikke stemmer overens med din bestilling, må du snarest ta kontakt med ditt reisebyrå eller Detur.";
                lastPage += " </p> ";
                lastPage += " <p> ";
                lastPage += " NB! Gyldig kvittering må forevises ved innsjekking på flyplassen, dersom din betaling ikke står i 0,- på vår nettside. ";
                lastPage += " Husk alltid å fakse kvittering, når dere betaler reisen i bank eller på nettbank. Fax: 22 41 72 90. Depositum må være ";
                lastPage += " innbetalt 10 dager etter bestilling. Resterende beløp skal være innbetalt senest 35 dager før avreise. Dersom det er ";
                lastPage += " 35 dager eller mindre til avreise, skal hele reisen betales i sin helhet ved bestilling.";
                lastPage += " </p> ";
                lastPage += " <p> ";
                lastPage += " Ved avreise: Fremmøte på flyplassen 2 timer før avgang. Bagasjen må være innsjekket senest";
                lastPage += " 1 time før angitt avgangstid. Mat på flyet er inkludert på alle våre reiser.";
                lastPage += " </p> ";
                lastPage += " <p>";
                lastPage += " Ved hjemreise: Fremmøte ifølge anvisninger på reisemålet. Sjekk informasjonsperm på hotellet eller kontakt våre guider på";
                lastPage += " destinasjonen. Hjemreisetidspunkt bekreftes på reisemålet.";
                lastPage += " </p> ";
                lastPage += " <p> ";
                lastPage += " Reisende med ”KUN FLY” plikter å kontakte Deturs kontor på reisemålet innen";
                lastPage += " 24 timer før hjemreise. Dette for å orientere seg om eventuelle endringer. Kontakt Deturs representanter på telefon:";
                lastPage += " </p> ";
                lastPage += " <p> ";
                lastPage += " <table>";
                lastPage += " <tr>";
                lastPage += "  <td>•TYRKIA-Antalya:</td><td>+90 242 348 00 68</td>";
                lastPage += " </tr>";
                lastPage += " <tr>";
                lastPage += "  <td>•TYRKIA-Alanya:</td><td>+90 242 511 98 88</td>";
                lastPage += " </tr>";
                lastPage += " <tr>";
                lastPage += "  <td>•TYRKIA-Marmaris:</td><td>+90 252 417 69 27</td>";
                lastPage += " </tr>";
                lastPage += " <tr>";
                lastPage += "  <td>•TYRKIA-Bodrum:</td><td>+90 252 319 25 00</td>";
                lastPage += " </tr>";
                lastPage += " <tr>";
                lastPage += "  <td>•TUNISIA-Sousse:</td><td>+216 73 277 444</td>";
                lastPage += " </tr>";
                lastPage += " <tr>";
                lastPage += "  <td>•EGYPT-Sharm El Sheikh:</td><td>+20 69 366 1617</td>";
                lastPage += " </tr>";
                lastPage += " </table>";
                lastPage += " </p> ";
                lastPage += " <p> ";
                lastPage += " Bagasje: Maks 25 kg er inkludert i billettprisen (inkludert 5 kg håndbagasje) per reisende. Golfbag må bestilles og betales";
                lastPage += " på forhånd og kan veie maks 15 kg (begrenset antall på hvert fly). Eventuell overvekt belastes ved innsjekk. Ved spesielle";
                lastPage += " ønsker formidles disse til reisemålet, men Detur garanterer ikke ønskene innfridd.";
                lastPage += " </p> ";
                lastPage += " <p> ";
                lastPage += "  GOD TUR!";
                lastPage += " </p> ";
                lastPage += " </td> ";
                lastPage += " </tr> ";
                lastPage += " <tr> ";
                lastPage += " <td style=\"font-size:10px;color:#000;text-align:center;\"> ";
                lastPage += " <p>";
                lastPage += " <b>Mer info finner du på www.detur.no. Der kan du også betale reisen direkte (online).</b>";
                lastPage += " </p> ";
                lastPage += " </td> ";
                lastPage += " </tr> ";
                lastPage += " </table>";
                lastPage += "</body></html>";
               
                HtmlToPdfBuilder builder = new HtmlToPdfBuilder(PageSize.A4);
                HtmlPdfPage lastPagePDF = builder.AddPage();

                lastPagePDF.AppendHtml(lastPage);

                byte[] file = builder.RenderPdf();
                PdfReader readLastPage = new PdfReader(file);
                doc.NewPage();
                PdfImportedPage pageLastPdf = pdfWriter.GetImportedPage(readLastPage, 1);
                PdfContentByte cb2 = pdfWriter.DirectContent;
                cb2.AddTemplate(pageLastPdf, 0, 0);
                cb2.ClosePath();
                */
                #endregion

                string file = pdfFilePath.ToLower().Replace(".pdf", "Back.pdf");
                PdfReader readLastPage = new PdfReader(file);
                doc.NewPage();
                PdfImportedPage pageLastPdf = pdfWriter.GetImportedPage(readLastPage, 1);
                PdfContentByte cb2 = pdfWriter.DirectContent;
                cb2.AddTemplate(pageLastPdf, 0, 0);
                cb2.ClosePath();

                if (!string.IsNullOrEmpty(resMain.Holpack))
                {
                    object secondPages = getHolpackMedia(resMain.Holpack, resMain.BegDate);
                    if (secondPages != null)
                    {
                        try
                        {
                            PdfReader spReader = new PdfReader((byte[])secondPages);
                            for (int i = 0; i < spReader.NumberOfPages; i++)
                            {
                                doc.NewPage();
                                PdfImportedPage page;
                                page = pdfWriter.GetImportedPage(spReader, i + 1);
                                PdfContentByte cb = pdfWriter.DirectContent;
                                cb.AddTemplate(page, 0, 0);
                                cb.ClosePath();
                            }
                        }
                        catch { }
                    }
                }
                doc.Close();
                reader.Close();
                return fileNameF;
            }
            catch
            {
                reader.Close();
                return "";
            }
        }

        internal string fillSpace(string val, int lengt)
        {
            for (int i = 0; i < (lengt - val.Length); i++)
            {
                val = " " + val;
            }
            return val;
        }

        public int? getNewProformaNumber()
        {
            string tsql = @"Select InvNo=Max(InvNo) From Invoice (NOLOCK) Where InvSerial='PRO' and Proforma=1";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                return Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool createInvoice(string ResNo, string Operator, string Office, string clientCode, string clientName, string clientAddress, string clientZip,
                                  string clientCity, string clientCountry, decimal? invAmount, string invCur, byte[] invImage, string crtUser, string crtAgency, string eMail1, string eMail2)
        {
            int? _newProformaNumber = getNewProformaNumber();
            int newProformaNumber = _newProformaNumber.HasValue ? _newProformaNumber.Value + 1 : 1;
            string tsql = @"Declare @RecID int
                            Insert Into Invoice (Operator, Office, InvSerial, InvNo, InvDate, InvType, SumType, ClientType, ClientCode, ClientName, ClientAddress,
				                                 ClientZip, ClientCity, ClientCountry, Proforma, InvAmount, InvCur, InvImage, InvImageType, CrtDate, CrtUser, CrtAgency,
                                                 email1, email2)
                            Values (@Operator, @Office, 'PRO', @InvNo, dbo.DateOnly(GetDate()), @InvType, @SumType, @ClientType, @ClientCode, @ClientName, @ClientAddress,
		                            @ClientZip, @ClientCity, @ClientCountry, 1, @InvAmount, @InvCur, @InvImage, 1, GetDate(), @CrtUser, @CrtAgency,
                                    @EMail1, @Email2)
                            Select @RecID=SCOPE_IDENTITY()

                            Insert Into InvoiceRes (InvoiceID, ResNo, ResInvAmount, ResInvAmountPrt)
                            Values (@RecID, @ResNo, @ResInvAmount, @ResInvAmountPrt)
                            ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "Office", DbType.String, Office);
                db.AddInParameter(dbCommand, "InvNo", DbType.Int32, newProformaNumber);
                db.AddInParameter(dbCommand, "InvDate", DbType.DateTime, DateTime.Today);
                db.AddInParameter(dbCommand, "InvType", DbType.Int16, 0);
                db.AddInParameter(dbCommand, "SumType", DbType.Int16, 1);
                db.AddInParameter(dbCommand, "ClientType", DbType.Byte, 1);
                db.AddInParameter(dbCommand, "ClientCode", DbType.String, clientCode);
                db.AddInParameter(dbCommand, "ClientName", DbType.String, clientName);
                db.AddInParameter(dbCommand, "ClientAddress", DbType.String, clientAddress);
                db.AddInParameter(dbCommand, "ClientZip", DbType.String, clientZip);
                db.AddInParameter(dbCommand, "ClientCity", DbType.String, clientCity);
                db.AddInParameter(dbCommand, "ClientCountry", DbType.String, clientCountry);
                db.AddInParameter(dbCommand, "InvAmount", DbType.Decimal, invAmount);
                db.AddInParameter(dbCommand, "InvCur", DbType.String, invCur);
                db.AddInParameter(dbCommand, "InvImage", DbType.Binary, invImage);
                db.AddInParameter(dbCommand, "CrtUser", DbType.String, crtUser);
                db.AddInParameter(dbCommand, "CrtAgency", DbType.String, crtAgency);
                db.AddInParameter(dbCommand, "Email1", DbType.String, eMail1);
                db.AddInParameter(dbCommand, "Email2", DbType.String, eMail2);

                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "ResInvAmount", DbType.Decimal, invAmount);
                db.AddInParameter(dbCommand, "ResInvAmountPrt", DbType.Decimal, invAmount);

                db.ExecuteNonQuery(dbCommand);

                return true;
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool invoiceIssued(string ResNo)
        {
            string tsql = @"Update ResMain
                            Set InvoiceIssued='Y', InvoiceDate=GetDate()
                            Where ResNo=@ResNo";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public object getHolpackMedia(string Holpack, DateTime? ResBegDate)
        {
            string tsql = @"Select FileImage From HolPackBrochure
                            Where HolPack=@Holpack
                              And @ResBegDate between BegDate And EndDate
                              And FileType='PDF'";

            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Holpack", DbType.String, Holpack);
                db.AddInParameter(dbCommand, "ResBegDate", DbType.DateTime, ResBegDate);
                return db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getInvoicePDF(int? RecID, string ResNo, string tempFilePath)
        {
            string tsql = @"Select RptImage From DocReportTmp (NOLOCK) 
                            Where RecID=@RecID";
            Database db = DatabaseFactory.CreateDatabase() as Database;
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "@RecID", DbType.Int32, RecID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    object retval = db.ExecuteScalar(dbCommand);
                    if (retval != null)
                    {
                        byte[] obj = (byte[])retval;
                        if (obj.Length > 0)
                        {
                            string fileNameL = ResNo + System.Guid.NewGuid().ToString() + ".pdf";
                            string fileName = tempFilePath + fileNameL;
                            if (File.Exists(fileName))
                                File.Delete(fileName);
                            FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite);
                            fileStream.Write((byte[])retval, 0, ((byte[])retval).Length);
                            fileStream.Close();
                            return fileNameL;
                        }
                        else
                            return string.Empty;
                    }
                    else
                        return string.Empty;
                }
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return string.Empty;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}

