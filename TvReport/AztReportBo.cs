﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvReport.AztReport
{
    public class CustInsuranceRecord
    {
        public CustInsuranceRecord() { }
        string _TitleName;
        public string TitleName { get { return _TitleName; } set { _TitleName = value; } }
        string _Surname;
        public string Surname { get { return _Surname; } set { _Surname = value; } }
        string _Name;
        public string Name { get { return _Name; } set { _Name = value; } }
        string _SurnameL;
        public string SurnameL { get { return _SurnameL; } set { _SurnameL = value; } }
        string _NameL;
        public string NameL { get { return _NameL; } set { _NameL = value; } }
        DateTime? _Birthday;
        public DateTime? Birthday { get { return _Birthday; } set { _Birthday = value; } }
        byte? _DocInsur;
        public byte? DocInsur { get { return _DocInsur; } set { _DocInsur = value; } }
        string _PassSerie;
        public string PassSerie { get { return _PassSerie; } set { _PassSerie = value; } }
        string _PassNo;
        public string PassNo { get { return _PassNo; } set { _PassNo = value; } }
        string _NetCur;
        public string NetCur { get { return _NetCur; } set { _NetCur = value; } }
        decimal? _SalePrice;
        public decimal? SalePrice { get { return _SalePrice; } set { _SalePrice = value; } }
        string _NationCode;
        public string NationCode { get { return _NationCode; } set { _NationCode = value; } }
    }

    public class InsuranceRptRecord
    {
        public InsuranceRptRecord() { }
        string _Service;
        public string Service { get { return _Service; } set { _Service = value; } }
        int? _Deplocation;
        public int? Deplocation { get { return _Deplocation; } set { _Deplocation = value; } }
        string _LocationName;
        public string LocationName { get { return _LocationName; } set { _LocationName = value; } }
        DateTime? _BegDate;
        public DateTime? BegDate { get { return _BegDate; } set { _BegDate = value; } }
        DateTime? _EndDate;
        public DateTime? EndDate { get { return _EndDate; } set { _EndDate = value; } }
        Int16? _Duration;
        public Int16? Duration { get { return _Duration; } set { _Duration = value; } }
        string _InsuranceName;
        public string InsuranceName { get { return _InsuranceName; } set { _InsuranceName = value; } }
        string _InsZone;
        public string InsZone { get { return _InsZone; } set { _InsZone = value; } }
        string _INameS;
        public string INameS { get { return _INameS; } set { _INameS = value; } }
        decimal? _Coverage;
        public decimal? Coverage { get { return _Coverage; } set { _Coverage = value; } }
        string _CoverageCur;
        public string CoverageCur { get { return _CoverageCur; } set { _CoverageCur = value; } }
        string _CoverExplain;
        public string CoverExplain { get { return _CoverExplain; } set { _CoverExplain = value; } }
        string _IDecription;
        public string IDescription { get { return _IDecription; } set { _IDecription = value; } }
        decimal? _Franchise;
        public decimal? Franchise { get { return _Franchise; } set { _Franchise = value; } }
    }

    public class ResCustInsuranceRecord
    {
        public ResCustInsuranceRecord()
        {
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _DocStat;
        public Int16? DocStat
        {
            get { return _DocStat; }
            set { _DocStat = value; }
        }

        int? _ServiceID;
        public int? ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        Int16? _InsType;
        public Int16? InsType
        {
            get { return _InsType; }
            set { _InsType = value; }
        }

    }

    public class InsurancePolicyRecord
    {
        public InsurancePolicyRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int? _PolNo;
        public int? PolNo
        {
            get { return _PolNo; }
            set { _PolNo = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        string _Insurance;
        public string Insurance
        {
            get { return _Insurance; }
            set { _Insurance = value; }
        }

        string _InsuranceName;
        public string InsuranceName
        {
            get { return _InsuranceName; }
            set { _InsuranceName = value; }
        }

        string _InsuranceLocalName;
        public string InsuranceLocalName
        {
            get { return _InsuranceLocalName; }
            set { _InsuranceLocalName = value; }
        }

        string _InsuranceNameS;
        public string InsuranceNameS
        {
            get { return _InsuranceNameS; }
            set { _InsuranceNameS = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _Days;
        public Int16? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }

        decimal? _Coverage;
        public decimal? Coverage
        {
            get { return _Coverage; }
            set { _Coverage = value; }
        }

        string _CoverageCur;
        public string CoverageCur
        {
            get { return _CoverageCur; }
            set { _CoverageCur = value; }
        }

        int? _Territory;
        public int? Territory
        {
            get { return _Territory; }
            set { _Territory = value; }
        }

        string _InsZone;
        public string InsZone
        {
            get { return _InsZone; }
            set { _InsZone = value; }
        }

        DateTime? _PrintDate;
        public DateTime? PrintDate
        {
            get { return _PrintDate; }
            set { _PrintDate = value; }
        }

        int? _PrintLocation;
        public int? PrintLocation
        {
            get { return _PrintLocation; }
            set { _PrintLocation = value; }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set { _CrtDate = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        string _CrtUser;
        public string CrtUser
        {
            get { return _CrtUser; }
            set { _CrtUser = value; }
        }

        DateTime? _Birtday;
        public DateTime? Birtday
        {
            get { return _Birtday; }
            set { _Birtday = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _SurName;
        public string SurName
        {
            get { return _SurName; }
            set { _SurName = value; }
        }

        Int16? _Title;
        public Int16? Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        string _TitleStr;
        public string TitleStr
        {
            get { return _TitleStr; }
            set { _TitleStr = value; }
        }

        string _Nation;
        public string Nation
        {
            get { return _Nation; }
            set { _Nation = value; }
        }

        string _PassSerie;
        public string PassSerie
        {
            get { return _PassSerie; }
            set { _PassSerie = value; }
        }

        string _PassNo;
        public string PassNo
        {
            get { return _PassNo; }
            set { _PassNo = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        string _PrintStat;
        public string PrintStat
        {
            get { return _PrintStat; }
            set { _PrintStat = value; }
        }

        int? _ServiceID;
        public int? ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _Franchise;
        public decimal? Franchise
        {
            get { return _Franchise; }
            set { _Franchise = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        decimal? _PasResAmount;
        public decimal? PasResAmount
        {
            get { return _PasResAmount; }
            set { _PasResAmount = value; }
        }

        DateTime? _ResDate;
        public DateTime? ResDate
        {
            get { return _ResDate; }
            set { _ResDate = value; }
        }

        string _AgencyCityName;
        public string AgencyCityName
        {
            get { return _AgencyCityName; }
            set { _AgencyCityName = value; }
        }

        DateTime? _ServiceCrtDate;
        public DateTime? ServiceCrtDate
        {
            get { return _ServiceCrtDate; }
            set { _ServiceCrtDate = value; }
        }

    }

    public class InsuranceServiceRecord
    {
        public InsuranceServiceRecord()
        {
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        string _ServiceTypeName;
        public string ServiceTypeName
        {
            get { return _ServiceTypeName; }
            set { _ServiceTypeName = value; }
        }

        string _ServiceTypeNameL;
        public string ServiceTypeNameL
        {
            get { return _ServiceTypeNameL; }
            set { _ServiceTypeNameL = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _ServiceName;
        public string ServiceName
        {
            get { return _ServiceName; }
            set { _ServiceName = value; }
        }

        string _ServiceNameL;
        public string ServiceNameL
        {
            get { return _ServiceNameL; }
            set { _ServiceNameL = value; }
        }

        int? _DepLocation;
        public int? DepLocation
        {
            get { return _DepLocation; }
            set { _DepLocation = value; }
        }

        string _DepLocationName;
        public string DepLocationName
        {
            get { return _DepLocationName; }
            set { _DepLocationName = value; }
        }

        string _DepLocationNameL;
        public string DepLocationNameL
        {
            get { return _DepLocationNameL; }
            set { _DepLocationNameL = value; }
        }

        int? _ArrLocation;
        public int? ArrLocation
        {
            get { return _ArrLocation; }
            set { _ArrLocation = value; }
        }

        string _ArrLocationName;
        public string ArrLocationName
        {
            get { return _ArrLocationName; }
            set { _ArrLocationName = value; }
        }

        string _ArrLocationNameL;
        public string ArrLocationNameL
        {
            get { return _ArrLocationNameL; }
            set { _ArrLocationNameL = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        Int16? _Duration;
        public Int16? Duration
        {
            get { return _Duration; }
            set { _Duration = value; }
        }

        string _Supplier;
        public string Supplier
        {
            get { return _Supplier; }
            set { _Supplier = value; }
        }

        string _SupplierName;
        public string SupplierName
        {
            get { return _SupplierName; }
            set { _SupplierName = value; }
        }

        string _SupplierNameL;
        public string SupplierNameL
        {
            get { return _SupplierNameL; }
            set { _SupplierNameL = value; }
        }

        Int16? _Unit;
        public Int16? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _RoomName;
        public string RoomName
        {
            get { return _RoomName; }
            set { _RoomName = value; }
        }

        string _RoomNameL;
        public string RoomNameL
        {
            get { return _RoomNameL; }
            set { _RoomNameL = value; }
        }

        string _Accom;
        public string Accom
        {
            get { return _Accom; }
            set { _Accom = value; }
        }

        string _AccomName;
        public string AccomName
        {
            get { return _AccomName; }
            set { _AccomName = value; }
        }

        string _AccomNameL;
        public string AccomNameL
        {
            get { return _AccomNameL; }
            set { _AccomNameL = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _BoardName;
        public string BoardName
        {
            get { return _BoardName; }
            set { _BoardName = value; }
        }

        string _BoardNameL;
        public string BoardNameL
        {
            get { return _BoardNameL; }
            set { _BoardNameL = value; }
        }

        Int16? _Adult;
        public Int16? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        Int16? _Child;
        public Int16? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        string _FlgClass;
        public string FlgClass
        {
            get { return _FlgClass; }
            set { _FlgClass = value; }
        }

        Int16? _PriceSource;
        public Int16? PriceSource
        {
            get { return _PriceSource; }
            set { _PriceSource = value; }
        }

        decimal? _NetPrice;
        public decimal? NetPrice
        {
            get { return _NetPrice; }
            set { _NetPrice = value; }
        }

        string _NetCur;
        public string NetCur
        {
            get { return _NetCur; }
            set { _NetCur = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        Int16? _StatConf;
        public Int16? StatConf
        {
            get { return _StatConf; }
            set { _StatConf = value; }
        }

        string _StatConfName;
        public string StatConfName
        {
            get { return _StatConfName; }
            set { _StatConfName = value; }
        }

        string _StatConfNameL;
        public string StatConfNameL
        {
            get { return _StatConfNameL; }
            set { _StatConfNameL = value; }
        }

        DateTime? _StatConfDate;
        public DateTime? StatConfDate
        {
            get { return _StatConfDate; }
            set { _StatConfDate = value; }
        }

        Int16? _StatSer;
        public Int16? StatSer
        {
            get { return _StatSer; }
            set { _StatSer = value; }
        }

        string _StatSerName;
        public string StatSerName
        {
            get { return _StatSerName; }
            set { _StatSerName = value; }
        }

        string _StatSerNameL;
        public string StatSerNameL
        {
            get { return _StatSerNameL; }
            set { _StatSerNameL = value; }
        }

        DateTime? _StatSerDate;
        public DateTime? StatSerDate
        {
            get { return _StatSerDate; }
            set { _StatSerDate = value; }
        }

        Int16? _StepNo;
        public Int16? StepNo
        {
            get { return _StepNo; }
            set { _StepNo = value; }
        }

        string _SupNote;
        public string SupNote
        {
            get { return _SupNote; }
            set { _SupNote = value; }
        }

        decimal? _FixCom;
        public decimal? FixCom
        {
            get { return _FixCom; }
            set { _FixCom = value; }
        }

        string _PayCom;
        public string PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        string _SendSupplier;
        public string SendSupplier
        {
            get { return _SendSupplier; }
            set { _SendSupplier = value; }
        }

        DateTime? _SendSupplierDate;
        public DateTime? SendSupplierDate
        {
            get { return _SendSupplierDate; }
            set { _SendSupplierDate = value; }
        }

        string _SendSupplierStat;
        public string SendSupplierStat
        {
            get { return _SendSupplierStat; }
            set { _SendSupplierStat = value; }
        }

        decimal? _PayToSup;
        public decimal? PayToSup
        {
            get { return _PayToSup; }
            set { _PayToSup = value; }
        }

        DateTime? _PayDueDate;
        public DateTime? PayDueDate
        {
            get { return _PayDueDate; }
            set { _PayDueDate = value; }
        }

        int? _FlyTicketID;
        public int? FlyTicketID
        {
            get { return _FlyTicketID; }
            set { _FlyTicketID = value; }
        }

        string _AllotType;
        public string AllotType
        {
            get { return _AllotType; }
            set { _AllotType = value; }
        }

        int? _CatPRecNo;
        public int? CatPRecNo
        {
            get { return _CatPRecNo; }
            set { _CatPRecNo = value; }
        }

        string _HotSPO;
        public string HotSPO
        {
            get { return _HotSPO; }
            set { _HotSPO = value; }
        }

        byte? _ResMainStat;
        public byte? ResMainStat
        {
            get { return _ResMainStat; }
            set { _ResMainStat = value; }
        }

        string _IncPack;
        public string IncPack
        {
            get { return _IncPack; }
            set { _IncPack = value; }
        }

        string _PayComEB;
        public string PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        string _RoomNo;
        public string RoomNo
        {
            get { return _RoomNo; }
            set { _RoomNo = value; }
        }

        string _Bus;
        public string Bus
        {
            get { return _Bus; }
            set { _Bus = value; }
        }

        string _BusName;
        public string BusName
        {
            get { return _BusName; }
            set { _BusName = value; }
        }

        byte? _AllotFHOpt;
        public byte? AllotFHOpt
        {
            get { return _AllotFHOpt; }
            set { _AllotFHOpt = value; }
        }

        decimal? _Tax;
        public decimal? Tax
        {
            get { return _Tax; }
            set { _Tax = value; }
        }

        decimal? _SupInvAmount;
        public decimal? SupInvAmount
        {
            get { return _SupInvAmount; }
            set { _SupInvAmount = value; }
        }

        string _CallFromResMain;
        public string CallFromResMain
        {
            get { return _CallFromResMain; }
            set { _CallFromResMain = value; }
        }

        string _DailyHotelAllot;
        public string DailyHotelAllot
        {
            get { return _DailyHotelAllot; }
            set { _DailyHotelAllot = value; }
        }

        bool? _PayPasEB;
        public bool? PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool? _Compulsory;
        public bool? Compulsory
        {
            get { return _Compulsory; }
            set { _Compulsory = value; }
        }

        decimal? _NetPriceBEB;
        public decimal? NetPriceBEB
        {
            get { return _NetPriceBEB; }
            set { _NetPriceBEB = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool? _CanDiscount;
        public bool? CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        bool? _SerCheck;
        public bool? SerCheck
        {
            get { return _SerCheck; }
            set { _SerCheck = value; }
        }

        DateTime? _SerCheckDate;
        public DateTime? SerCheckDate
        {
            get { return _SerCheckDate; }
            set { _SerCheckDate = value; }
        }

        string _SerCheckUser;
        public string SerCheckUser
        {
            get { return _SerCheckUser; }
            set { _SerCheckUser = value; }
        }

        int? _Pickup;
        public int? Pickup
        {
            get { return _Pickup; }
            set { _Pickup = value; }
        }

        string _PickupName;
        public string PickupName
        {
            get { return _PickupName; }
            set { _PickupName = value; }
        }

        string _PickupNameL;
        public string PickupNameL
        {
            get { return _PickupNameL; }
            set { _PickupNameL = value; }
        }

        decimal? _PasDisAmount;
        public decimal? PasDisAmount
        {
            get { return _PasDisAmount; }
            set { _PasDisAmount = value; }
        }

        decimal? _AgDisAmount;
        public decimal? AgDisAmount
        {
            get { return _AgDisAmount; }
            set { _AgDisAmount = value; }
        }

        DateTime? _PickupTime;
        public DateTime? PickupTime
        {
            get { return _PickupTime; }
            set { _PickupTime = value; }
        }

        string _PickupNote;
        public string PickupNote
        {
            get { return _PickupNote; }
            set { _PickupNote = value; }
        }

        decimal? _PasEBAmount;
        public decimal? PasEBAmount
        {
            get { return _PasEBAmount; }
            set { _PasEBAmount = value; }
        }

        decimal? _AgComAmount;
        public decimal? AgComAmount
        {
            get { return _AgComAmount; }
            set { _AgComAmount = value; }
        }

        decimal? _AgEBAmount;
        public decimal? AgEBAmount
        {
            get { return _AgEBAmount; }
            set { _AgEBAmount = value; }
        }

        decimal? _PSalePrice;
        public decimal? PSalePrice
        {
            get { return _PSalePrice; }
            set { _PSalePrice = value; }
        }

        decimal? _ASalePrice;
        public decimal? ASalePrice
        {
            get { return _ASalePrice; }
            set { _ASalePrice = value; }
        }

        string _PnrNo;
        public string PnrNo
        {
            get { return _PnrNo; }
            set { _PnrNo = value; }
        }

        Int16? _AllotUnit;
        public Int16? AllotUnit
        {
            get { return _AllotUnit; }
            set { _AllotUnit = value; }
        }

        byte? _TrfDT;
        public byte? TrfDT
        {
            get { return _TrfDT; }
            set { _TrfDT = value; }
        }

        string _TrfDTName;
        public string TrfDTName
        {
            get { return _TrfDTName; }
            set { _TrfDTName = value; }
        }

        byte? _TrfAT;
        public byte? TrfAT
        {
            get { return _TrfAT; }
            set { _TrfAT = value; }
        }

        string _TrfATName;
        public string TrfATName
        {
            get { return _TrfATName; }
            set { _TrfATName = value; }
        }

        string _TrfDep;
        public string TrfDep
        {
            get { return _TrfDep; }
            set { _TrfDep = value; }
        }

        string _TrfDepName;
        public string TrfDepName
        {
            get { return _TrfDepName; }
            set { _TrfDepName = value; }
        }

        string _TrfDepNameL;
        public string TrfDepNameL
        {
            get { return _TrfDepNameL; }
            set { _TrfDepNameL = value; }
        }

        string _TrfArr;
        public string TrfArr
        {
            get { return _TrfArr; }
            set { _TrfArr = value; }
        }

        string _TrfArrName;
        public string TrfArrName
        {
            get { return _TrfArrName; }
            set { _TrfArrName = value; }
        }

        string _TrfArrNameL;
        public string TrfArrNameL
        {
            get { return _TrfArrNameL; }
            set { _TrfArrNameL = value; }
        }

        Int16? _GrpPax;
        public Int16? GrpPax
        {
            get { return _GrpPax; }
            set { _GrpPax = value; }
        }

        decimal? _NetKBAmount;
        public decimal? NetKBAmount
        {
            get { return _NetKBAmount; }
            set { _NetKBAmount = value; }
        }

        decimal? _SaleKBAmount;
        public decimal? SaleKBAmount
        {
            get { return _SaleKBAmount; }
            set { _SaleKBAmount = value; }
        }

        decimal? _Franchise;
        public decimal? Franchise
        {
            get { return _Franchise; }
            set { _Franchise = value; }
        }

        decimal? _CoverAge;
        public decimal? CoverAge
        {
            get { return _CoverAge; }
            set { _CoverAge = value; }
        }

        string _CoverAgeCur;
        public string CoverAgeCur
        {
            get { return _CoverAgeCur; }
            set { _CoverAgeCur = value; }
        }

        string _InsZone;
        public string InsZone
        {
            get { return _InsZone; }
            set { _InsZone = value; }
        }

        string _Country;
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        Int16? _Days;
        public Int16? Days
        {
            get { return _Days; }
            set { _Days = value; }
        }
    }

    public class ResCustInfoRecord
    {
        public ResCustInfoRecord()
        {
            _InvoiceAddr = "H";
            _ContactAddr = "H";
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _CTitle;
        public Int16? CTitle
        {
            get { return _CTitle; }
            set { _CTitle = value; }
        }

        string _CTitleName;
        public string CTitleName
        {
            get { return _CTitleName; }
            set { _CTitleName = value; }
        }

        string _CName;
        public string CName
        {
            get { return _CName; }
            set { _CName = value; }
        }

        string _CSurName;
        public string CSurName
        {
            get { return _CSurName; }
            set { _CSurName = value; }
        }

        string _AddrHome;
        public string AddrHome
        {
            get { return _AddrHome; }
            set { _AddrHome = value; }
        }

        string _AddrHomeCity;
        public string AddrHomeCity
        {
            get { return _AddrHomeCity; }
            set { _AddrHomeCity = value; }
        }

        string _AddrHomeZip;
        public string AddrHomeZip
        {
            get { return _AddrHomeZip; }
            set { _AddrHomeZip = value; }
        }

        string _AddrHomeTel;
        public string AddrHomeTel
        {
            get { return _AddrHomeTel; }
            set { _AddrHomeTel = value; }
        }

        string _AddrHomeFax;
        public string AddrHomeFax
        {
            get { return _AddrHomeFax; }
            set { _AddrHomeFax = value; }
        }

        string _AddrHomeEmail;
        public string AddrHomeEmail
        {
            get { return _AddrHomeEmail; }
            set { _AddrHomeEmail = value; }
        }

        string _AddrHomeCountry;
        public string AddrHomeCountry
        {
            get { return _AddrHomeCountry; }
            set { _AddrHomeCountry = value; }
        }

        string _AddrHomeCountryCode;
        public string AddrHomeCountryCode
        {
            get { return _AddrHomeCountryCode; }
            set { _AddrHomeCountryCode = value; }
        }

        string _HomeTaxOffice;
        public string HomeTaxOffice
        {
            get { return _HomeTaxOffice; }
            set { _HomeTaxOffice = value; }
        }

        string _HomeTaxAccNo;
        public string HomeTaxAccNo
        {
            get { return _HomeTaxAccNo; }
            set { _HomeTaxAccNo = value; }
        }

        string _AddrWork;
        public string AddrWork
        {
            get { return _AddrWork; }
            set { _AddrWork = value; }
        }

        string _AddrWorkCity;
        public string AddrWorkCity
        {
            get { return _AddrWorkCity; }
            set { _AddrWorkCity = value; }
        }

        string _AddrWorkZip;
        public string AddrWorkZip
        {
            get { return _AddrWorkZip; }
            set { _AddrWorkZip = value; }
        }

        string _AddrWorkTel;
        public string AddrWorkTel
        {
            get { return _AddrWorkTel; }
            set { _AddrWorkTel = value; }
        }

        string _AddrWorkFax;
        public string AddrWorkFax
        {
            get { return _AddrWorkFax; }
            set { _AddrWorkFax = value; }
        }

        string _AddrWorkEMail;
        public string AddrWorkEMail
        {
            get { return _AddrWorkEMail; }
            set { _AddrWorkEMail = value; }
        }

        string _AddrWorkCountry;
        public string AddrWorkCountry
        {
            get { return _AddrWorkCountry; }
            set { _AddrWorkCountry = value; }
        }

        string _AddrWorkCountryCode;
        public string AddrWorkCountryCode
        {
            get { return _AddrWorkCountryCode; }
            set { _AddrWorkCountryCode = value; }
        }

        string _WorkTaxOffice;
        public string WorkTaxOffice
        {
            get { return _WorkTaxOffice; }
            set { _WorkTaxOffice = value; }
        }

        string _WorkTaxAccNo;
        public string WorkTaxAccNo
        {
            get { return _WorkTaxAccNo; }
            set { _WorkTaxAccNo = value; }
        }

        string _ContactAddr;
        public string ContactAddr
        {
            get { return _ContactAddr; }
            set { _ContactAddr = value; }
        }

        string _MobTel;
        public string MobTel
        {
            get { return _MobTel; }
            set { _MobTel = value; }
        }

        string _Jobs;
        public string Jobs
        {
            get { return _Jobs; }
            set { _Jobs = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        string _InvoiceAddr;
        public string InvoiceAddr
        {
            get { return _InvoiceAddr; }
            set { _InvoiceAddr = value; }
        }

        string _WorkFirmName;
        public string WorkFirmName
        {
            get { return _WorkFirmName; }
            set { _WorkFirmName = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankAccNo;
        public string BankAccNo
        {
            get { return _BankAccNo; }
            set { _BankAccNo = value; }
        }

        string _BankIBAN;
        public string BankIBAN
        {
            get { return _BankIBAN; }
            set { _BankIBAN = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }
    }
    
}
