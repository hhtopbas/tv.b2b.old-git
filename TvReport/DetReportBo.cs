﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TvReport.DetReport;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using TvTools;

namespace TvReport.DetReport
{    
    public class ReportData
    {
        public ReportData()
        {
            _PassengerList = new List<PassengersRecord>();
            _PasPriceList = new List<PasPricesRecord>();
            _ResPayPlanList = new List<ResPayPlanRecord>();
            _ServicePriceList = new List<ServicePricesRecord>();
        }

        List<PassengersRecord> _PassengerList;
        public List<PassengersRecord> PassengerList
        {
            get { return _PassengerList; }
            set { _PassengerList = value; }
        }

        List<PasPricesRecord> _PasPriceList;
        public List<PasPricesRecord> PasPriceList
        {
            get { return _PasPriceList; }
            set { _PasPriceList = value; }
        }

        List<ResPayPlanRecord> _ResPayPlanList;
        public List<ResPayPlanRecord> ResPayPlanList
        {
            get { return _ResPayPlanList; }
            set { _ResPayPlanList = value; }
        }

        List<ServicePricesRecord> _ServicePriceList;
        public List<ServicePricesRecord> ServicePriceList
        {
            get { return _ServicePriceList; }
            set { _ServicePriceList = value; }
        }

        string _GiroKID;
        public string GiroKID
        {
            get { return _GiroKID; }
            set { _GiroKID = value; }
        }

        ReportResMainRec _ReportResMain;
        public ReportResMainRec ReportResMain
        {
            get { return _ReportResMain; }
            set { _ReportResMain = value; }
        }

        List<string> _Ssrc;
        public List<string> Ssrc
        {
            get { return _Ssrc; }
            set { _Ssrc = value; }
        }
    }

    public class PassengersRecord
    {
        public PassengersRecord()
        { 
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int? _CustNo;
        public int? CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        DateTime? _Birthdate;
        public DateTime? Birthdate
        {
            get { return _Birthdate; }
            set { _Birthdate = value; }
        }

        Int16? _PageNo;
        public Int16? PageNo
        {
            get { return _PageNo; }
            set { _PageNo = value; }
        }

        Int16? _SeqNo;
        public Int16? SeqNo
        {
            get { return _SeqNo; }
            set { _SeqNo = value; }
        }

        string _Leader;
        public string Leader
        {
            get { return _Leader; }
            set { _Leader = value; }
        }

        Int16? _Age;
        public Int16? Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

        string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        Int16? _TitleNo;
        public Int16? TitleNo
        {
            get { return _TitleNo; }
            set { _TitleNo = value; }
        }

        byte? _Status;
        public byte? Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
                  
    }

    public class PasPricesRecord
    {
        public PasPricesRecord()
        { 
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int16? _PageNo;
        public Int16? PageNo
        {
            get { return _PageNo; }
            set { _PageNo = value; }
        }

        string _IncPack;
        public string IncPack
        {
            get { return _IncPack; }
            set { _IncPack = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _ServiceDesc;
        public string ServiceDesc
        {
            get { return _ServiceDesc; }
            set { _ServiceDesc = value; }
        }

        decimal? _Pax1Amount;
        public decimal? Pax1Amount
        {
            get { return _Pax1Amount; }
            set { _Pax1Amount = value; }
        }

        decimal? _Pax2Amount;
        public decimal? Pax2Amount
        {
            get { return _Pax2Amount; }
            set { _Pax2Amount = value; }
        }

        decimal? _Pax3Amount;
        public decimal? Pax3Amount
        {
            get { return _Pax3Amount; }
            set { _Pax3Amount = value; }
        }

        decimal? _Pax4Amount;
        public decimal? Pax4Amount
        {
            get { return _Pax4Amount; }
            set { _Pax4Amount = value; }
        }

        decimal? _Pax5Amount;
        public decimal? Pax5Amount
        {
            get { return _Pax5Amount; }
            set { _Pax5Amount = value; }
        }

        decimal? _Pax6Amount;
        public decimal? Pax6Amount
        {
            get { return _Pax6Amount; }
            set { _Pax6Amount = value; }
        }

    }

    public class ResPayPlanRecord
    {
        public ResPayPlanRecord()
        {         
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ResNo;
        public string ResNo
        {
            get { return _ResNo; }
            set { _ResNo = value; }
        }

        Int16? _PayNo;
        public Int16? PayNo
        {
            get { return _PayNo; }
            set { _PayNo = value; }
        }

        DateTime? _DueDate;
        public DateTime? DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        decimal? _Adult;
        public decimal? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        decimal? _Child;
        public decimal? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

        decimal? _Infant;
        public decimal? Infant
        {
            get { return _Infant; }
            set { _Infant = value; }
        }

        decimal? _PayAmount;
        public decimal? PayAmount
        {
            get { return _PayAmount; }
            set { _PayAmount = value; }
        }

        decimal? _RemAmount;
        public decimal? RemAmount
        {
            get { return _RemAmount; }
            set { _RemAmount = value; }
        }

    }

    public class ServicePricesRecord
    {
        public ServicePricesRecord()
        { 
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int16? _RecType;
        public Int16? RecType
        {
            get { return _RecType; }
            set { _RecType = value; }
        }

        string _IncPack;
        public string IncPack
        {
            get { return _IncPack; }
            set { _IncPack = value; }
        }

        Int16? _Unit;
        public Int16? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _ServiceDesc;
        public string ServiceDesc
        {
            get { return _ServiceDesc; }
            set { _ServiceDesc = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        DateTime? _BeginDate;
        public DateTime? BeginDate
        {
            get { return _BeginDate; }
            set { _BeginDate = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }
    }

    public class ReportResMainRec
    {
        public ReportResMainRec()
        { 
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _ResDate;
        public DateTime? ResDate
        {
            get { return _ResDate; }
            set { _ResDate = value; }
        }        

        string _AgencyUserName;
        public string AgencyUserName
        {
            get { return _AgencyUserName; }
            set { _AgencyUserName = value; }
        }

        string _AgencyName;
        public string AgencyName
        {
            get { return _AgencyName; }
            set { _AgencyName = value; }
        }

        decimal? _PasPayable;
        public decimal? PasPayable
        {
            get { return _PasPayable; }
            set { _PasPayable = value; }
        }

        DateTime? _ReceiveDate;
        public DateTime? ReceiveDate
        {
            get { return _ReceiveDate; }
            set { _ReceiveDate = value; }
        }

        string _ResNote;
        public string ResNote
        {
            get { return _ResNote; }
            set { _ResNote = value; }
        }

        Int16? _SaleResource;
        public Int16? SaleResource
        {
            get { return _SaleResource; }
            set { _SaleResource = value; }
        }

        string _Holpack;
        public string Holpack
        {
            get { return _Holpack; }
            set { _Holpack = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        int? _ArrCountry;
        public int? ArrCountry
        {
            get { return _ArrCountry; }
            set { _ArrCountry = value; }
        }

        Int16? _ResStat;
        public Int16? ResStat
        {
            get { return _ResStat; }
            set { _ResStat = value; }
        }
    }

    public class ResCustInfoRecord
    {
        public ResCustInfoRecord()
        {
            _InvoiceAddr = "H";
            _ContactAddr = "H";
        }

        int _RecordID;
        public int RecordID
        {
            get { return _RecordID; }
            set { _RecordID = value; }
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        Int16? _CTitle;
        public Int16? CTitle
        {
            get { return _CTitle; }
            set { _CTitle = value; }
        }

        string _CTitleName;
        public string CTitleName
        {
            get { return _CTitleName; }
            set { _CTitleName = value; }
        }

        string _CName;
        public string CName
        {
            get { return _CName; }
            set { _CName = value; }
        }

        string _CSurName;
        public string CSurName
        {
            get { return _CSurName; }
            set { _CSurName = value; }
        }

        string _AddrHome;
        public string AddrHome
        {
            get { return _AddrHome; }
            set { _AddrHome = value; }
        }

        string _AddrHomeCity;
        public string AddrHomeCity
        {
            get { return _AddrHomeCity; }
            set { _AddrHomeCity = value; }
        }

        string _AddrHomeZip;
        public string AddrHomeZip
        {
            get { return _AddrHomeZip; }
            set { _AddrHomeZip = value; }
        }

        string _AddrHomeTel;
        public string AddrHomeTel
        {
            get { return _AddrHomeTel; }
            set { _AddrHomeTel = value; }
        }

        string _AddrHomeFax;
        public string AddrHomeFax
        {
            get { return _AddrHomeFax; }
            set { _AddrHomeFax = value; }
        }

        string _AddrHomeEmail;
        public string AddrHomeEmail
        {
            get { return _AddrHomeEmail; }
            set { _AddrHomeEmail = value; }
        }

        string _AddrHomeCountry;
        public string AddrHomeCountry
        {
            get { return _AddrHomeCountry; }
            set { _AddrHomeCountry = value; }
        }

        string _HomeTaxOffice;
        public string HomeTaxOffice
        {
            get { return _HomeTaxOffice; }
            set { _HomeTaxOffice = value; }
        }

        string _HomeTaxAccNo;
        public string HomeTaxAccNo
        {
            get { return _HomeTaxAccNo; }
            set { _HomeTaxAccNo = value; }
        }

        string _AddrWork;
        public string AddrWork
        {
            get { return _AddrWork; }
            set { _AddrWork = value; }
        }

        string _AddrWorkCity;
        public string AddrWorkCity
        {
            get { return _AddrWorkCity; }
            set { _AddrWorkCity = value; }
        }

        string _AddrWorkZip;
        public string AddrWorkZip
        {
            get { return _AddrWorkZip; }
            set { _AddrWorkZip = value; }
        }

        string _AddrWorkTel;
        public string AddrWorkTel
        {
            get { return _AddrWorkTel; }
            set { _AddrWorkTel = value; }
        }

        string _AddrWorkFax;
        public string AddrWorkFax
        {
            get { return _AddrWorkFax; }
            set { _AddrWorkFax = value; }
        }

        string _AddrWorkEMail;
        public string AddrWorkEMail
        {
            get { return _AddrWorkEMail; }
            set { _AddrWorkEMail = value; }
        }

        string _AddrWorkCountry;
        public string AddrWorkCountry
        {
            get { return _AddrWorkCountry; }
            set { _AddrWorkCountry = value; }
        }

        string _WorkTaxOffice;
        public string WorkTaxOffice
        {
            get { return _WorkTaxOffice; }
            set { _WorkTaxOffice = value; }
        }

        string _WorkTaxAccNo;
        public string WorkTaxAccNo
        {
            get { return _WorkTaxAccNo; }
            set { _WorkTaxAccNo = value; }
        }

        string _ContactAddr;
        public string ContactAddr
        {
            get { return _ContactAddr; }
            set { _ContactAddr = value; }
        }

        string _MobTel;
        public string MobTel
        {
            get { return _MobTel; }
            set { _MobTel = value; }
        }

        string _Jobs;
        public string Jobs
        {
            get { return _Jobs; }
            set { _Jobs = value; }
        }

        string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        string _InvoiceAddr;
        public string InvoiceAddr
        {
            get { return _InvoiceAddr; }
            set { _InvoiceAddr = value; }
        }

        string _WorkFirmName;
        public string WorkFirmName
        {
            get { return _WorkFirmName; }
            set { _WorkFirmName = value; }
        }

        string _Bank;
        public string Bank
        {
            get { return _Bank; }
            set { _Bank = value; }
        }

        string _BankAccNo;
        public string BankAccNo
        {
            get { return _BankAccNo; }
            set { _BankAccNo = value; }
        }

        string _BankIBAN;
        public string BankIBAN
        {
            get { return _BankIBAN; }
            set { _BankIBAN = value; }
        }

        bool _MemTable;
        public bool MemTable
        {
            get { return _MemTable; }
            set { _MemTable = value; }
        }
    }

    public class FlightDetailRecord
    {
        public FlightDetailRecord()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        DateTime _FlyDate;
        public DateTime FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        DateTime _ArrDate;
        public DateTime ArrDate
        {
            get { return _ArrDate; }
            set { _ArrDate = value; }
        }

        int _FlyDur;
        public int FlyDur
        {
            get { return _FlyDur; }
            set { _FlyDur = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        string _DepAirport;
        public string DepAirport
        {
            get { return _DepAirport; }
            set { _DepAirport = value; }
        }

        string _DepAirportName;
        public string DepAirportName
        {
            get { return _DepAirportName; }
            set { _DepAirportName = value; }
        }

        string _DepAirportNameL;
        public string DepAirportNameL
        {
            get { return _DepAirportNameL; }
            set { _DepAirportNameL = value; }
        }

        int? _DepCity;
        public int? DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityNameL;
        public string DepCityNameL
        {
            get { return _DepCityNameL; }
            set { _DepCityNameL = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        string _ArrAirport;
        public string ArrAirport
        {
            get { return _ArrAirport; }
            set { _ArrAirport = value; }
        }

        string _ArrAirportName;
        public string ArrAirportName
        {
            get { return _ArrAirportName; }
            set { _ArrAirportName = value; }
        }

        string _ArrAirportNameL;
        public string ArrAirportNameL
        {
            get { return _ArrAirportNameL; }
            set { _ArrAirportNameL = value; }
        }

        int? _ArrCity;
        public int? ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityNameL;
        public string ArrCityNameL
        {
            get { return _ArrCityNameL; }
            set { _ArrCityNameL = value; }
        }

        string _Airline;
        public string Airline
        {
            get { return _Airline; }
            set { _Airline = value; }
        }

        string _AirlineName;
        public string AirlineName
        {
            get { return _AirlineName; }
            set { _AirlineName = value; }
        }

        string _AirlineNameL;
        public string AirlineNameL
        {
            get { return _AirlineNameL; }
            set { _AirlineNameL = value; }
        }

        decimal? _MaxInfAge;
        public decimal? MaxInfAge
        {
            get { return _MaxInfAge; }
            set { _MaxInfAge = value; }
        }

        decimal? _MaxChdAge;
        public decimal? MaxChdAge
        {
            get { return _MaxChdAge; }
            set { _MaxChdAge = value; }
        }

        decimal? _MaxTeenAge;
        public decimal? MaxTeenAge
        {
            get { return _MaxTeenAge; }
            set { _MaxTeenAge = value; }
        }

        int? _BagWeight;
        public int? BagWeight
        {
            get { return _BagWeight; }
            set { _BagWeight = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        string _PNRName;
        public string PNRName
        {
            get { return _PNRName; }
            set { _PNRName = value; }
        }
    }

    public class FlightDayRecord
    {
        public FlightDayRecord()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        string _Airline;
        public string Airline
        {
            get { return _Airline; }
            set { _Airline = value; }
        }

        string _FlgOwner;
        public string FlgOwner
        {
            get { return _FlgOwner; }
            set { _FlgOwner = value; }
        }

        DateTime _FlyDate;
        public DateTime FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        DateTime _ArrDate;
        public DateTime ArrDate
        {
            get { return _ArrDate; }
            set { _ArrDate = value; }
        }

        int _DepCity;
        public int DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepAirport;
        public string DepAirport
        {
            get { return _DepAirport; }
            set { _DepAirport = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        int _ArrCity;
        public int ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrAirport;
        public string ArrAirport
        {
            get { return _ArrAirport; }
            set { _ArrAirport = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        bool _StopSale;
        public bool StopSale
        {
            get { return _StopSale; }
            set { _StopSale = value; }
        }

        Int16? _BagWeight;
        public Int16? BagWeight
        {
            get { return _BagWeight; }
            set { _BagWeight = value; }
        }

        decimal? _MaxInfAge;
        public decimal? MaxInfAge
        {
            get { return _MaxInfAge; }
            set { _MaxInfAge = value; }
        }

        decimal? _MaxChdAge;
        public decimal? MaxChdAge
        {
            get { return _MaxChdAge; }
            set { _MaxChdAge = value; }
        }

        decimal? _MaxTeenAge;
        public decimal? MaxTeenAge
        {
            get { return _MaxTeenAge; }
            set { _MaxTeenAge = value; }
        }

        string _NextFlight;
        public string NextFlight
        {
            get { return _NextFlight; }
            set { _NextFlight = value; }
        }

        string _PNRName;
        public string PNRName
        {
            get { return _PNRName; }
            set { _PNRName = value; }
        }

        DateTime? _CountCloseTime;
        public DateTime? CountCloseTime
        {
            get { return _CountCloseTime; }
            set { _CountCloseTime = value; }
        }

        bool _WebPub;
        public bool WebPub
        {
            get { return _WebPub; }
            set { _WebPub = value; }
        }

        bool _AllotUseSysSet;
        public bool AllotUseSysSet
        {
            get { return _AllotUseSysSet; }
            set { _AllotUseSysSet = value; }
        }

        bool _AllotChk;
        public bool AllotChk
        {
            get { return _AllotChk; }
            set { _AllotChk = value; }
        }

        bool _AllotWarnFull;
        public bool AllotWarnFull
        {
            get { return _AllotWarnFull; }
            set { _AllotWarnFull = value; }
        }

        bool _AllotDoNotOver;
        public bool AllotDoNotOver
        {
            get { return _AllotDoNotOver; }
            set { _AllotDoNotOver = value; }
        }

        int _FlyDur;
        public int FlyDur
        {
            get { return _FlyDur; }
            set { _FlyDur = value; }
        }

        Int16? _WarnUsedAmount;
        public Int16? WarnUsedAmount
        {
            get { return _WarnUsedAmount; }
            set { _WarnUsedAmount = value; }
        }

        Byte? _RetFlightOpt;
        public Byte? RetFlightOpt
        {
            get { return _RetFlightOpt; }
            set { _RetFlightOpt = value; }
        }

        bool _RetFlightUseSysSet;
        public bool RetFlightUseSysSet
        {
            get { return _RetFlightUseSysSet; }
            set { _RetFlightUseSysSet = value; }
        }

        DateTime? _TDepDate;
        public DateTime? TDepDate
        {
            get { return _TDepDate; }
            set { _TDepDate = value; }
        }

        DateTime? _TArrDate;
        public DateTime? TArrDate
        {
            get { return _TArrDate; }
            set { _TArrDate = value; }
        }

    }

    public class FlightRecord
    {
        public FlightRecord()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Airline;
        public string Airline
        {
            get { return _Airline; }
            set { _Airline = value; }
        }

        string _AirlineName;
        public string AirlineName
        {
            get { return _AirlineName; }
            set { _AirlineName = value; }
        }

        string _AirlineLocalName;
        public string AirlineLocalName
        {
            get { return _AirlineLocalName; }
            set { _AirlineLocalName = value; }
        }

        string _Company;
        public string Company
        {
            get { return _Company; }
            set { _Company = value; }
        }

        int? _PlaneType;
        public int? PlaneType
        {
            get { return _PlaneType; }
            set { _PlaneType = value; }
        }

        string _PlaneTypeName;
        public string PlaneTypeName
        {
            get { return _PlaneTypeName; }
            set { _PlaneTypeName = value; }
        }

        int? _FlightType;
        public int? FlightType
        {
            get { return _FlightType; }
            set { _FlightType = value; }
        }

        string _FlightTypeName;
        public string FlightTypeName
        {
            get { return _FlightTypeName; }
            set { _FlightTypeName = value; }
        }

        int _DepCity;
        public int DepCity
        {
            get { return _DepCity; }
            set { _DepCity = value; }
        }

        string _DepCityName;
        public string DepCityName
        {
            get { return _DepCityName; }
            set { _DepCityName = value; }
        }

        string _DepCityLocalName;
        public string DepCityLocalName
        {
            get { return _DepCityLocalName; }
            set { _DepCityLocalName = value; }
        }

        string _DepAir;
        public string DepAir
        {
            get { return _DepAir; }
            set { _DepAir = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        int _ArrCity;
        public int ArrCity
        {
            get { return _ArrCity; }
            set { _ArrCity = value; }
        }

        string _ArrCityName;
        public string ArrCityName
        {
            get { return _ArrCityName; }
            set { _ArrCityName = value; }
        }

        string _ArrCityLocalName;
        public string ArrCityLocalName
        {
            get { return _ArrCityLocalName; }
            set { _ArrCityLocalName = value; }
        }

        string _ArrAir;
        public string ArrAir
        {
            get { return _ArrAir; }
            set { _ArrAir = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        DateTime? _CountCloseTime;
        public DateTime? CountCloseTime
        {
            get { return _CountCloseTime; }
            set { _CountCloseTime = value; }
        }

        decimal? _BagWeight;
        public decimal? BagWeight
        {
            get { return _BagWeight; }
            set { _BagWeight = value; }
        }

        decimal? _InfAge;
        public decimal? InfAge
        {
            get { return _InfAge; }
            set { _InfAge = value; }
        }

        decimal? _ChdAge;
        public decimal? ChdAge
        {
            get { return _ChdAge; }
            set { _ChdAge = value; }
        }

        decimal? _TeenAge;
        public decimal? TeenAge
        {
            get { return _TeenAge; }
            set { _TeenAge = value; }
        }

        string _RetCode;
        public string RetCode
        {
            get { return _RetCode; }
            set { _RetCode = value; }
        }

        string _Virtual;
        public string Virtual
        {
            get { return _Virtual; }
            set { _Virtual = value; }
        }

        string _PNLName;
        public string PNLName
        {
            get { return _PNLName; }
            set { _PNLName = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        string _FlyDays;
        public string FlyDays
        {
            get { return _FlyDays; }
            set { _FlyDays = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        int? _FlyDur;
        public int? FlyDur
        {
            get { return _FlyDur; }
            set { _FlyDur = value; }
        }

        string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        bool? _PayCom;
        public bool? PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        bool? _PayComEB;
        public bool? PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool? _PayPasEB;
        public bool? PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        bool? _isFirstFlight;
        public bool? IsFirstFlight
        {
            get { return _isFirstFlight; }
            set { _isFirstFlight = value; }
        }

        bool? _CanDiscount;
        public bool? CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        Int16? _Direction;
        public Int16? Direction
        {
            get { return _Direction; }
            set { _Direction = value; }
        }

    }

    public class AirportRecord
    {
        public AirportRecord()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        int _Location;
        public int Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        bool _PNLFullName;
        public bool PNLFullName
        {
            get { return _PNLFullName; }
            set { _PNLFullName = value; }
        }

        bool _PNLFlightSpace;
        public bool PNLFlightSpace
        {
            get { return _PNLFlightSpace; }
            set { _PNLFlightSpace = value; }
        }

        bool _PNLNameSpace;
        public bool PNLNameSpace
        {
            get { return _PNLNameSpace; }
            set { _PNLNameSpace = value; }
        }

        bool _PNLChdRemark;
        public bool PNLChdRemark
        {
            get { return _PNLChdRemark; }
            set { _PNLChdRemark = value; }
        }

        bool _PNLSitatex;
        public bool PNLSitatex
        {
            get { return _PNLSitatex; }
            set { _PNLSitatex = value; }
        }

        int? _PNLMaxLineInPart;
        public int? PNLMaxLineInPart
        {
            get { return _PNLMaxLineInPart; }
            set { _PNLMaxLineInPart = value; }
        }

        bool _PNLDispExtra;
        public bool PNLDispExtra
        {
            get { return _PNLDispExtra; }
            set { _PNLDispExtra = value; }
        }

        string _PNLDispPNR;
        public string PNLDispPNR
        {
            get { return _PNLDispPNR; }
            set { _PNLDispPNR = value; }
        }

        bool _PNLDotAfterTitle;
        public bool PNLDotAfterTitle
        {
            get { return _PNLDotAfterTitle; }
            set { _PNLDotAfterTitle = value; }
        }
    }

    public class HotelRecord
    {
        public HotelRecord()
        {
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _HotelType;
        public string HotelType
        {
            get { return _HotelType; }
            set { _HotelType = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        int? _AirportDist;
        public int? AirportDist
        {
            get { return _AirportDist; }
            set { _AirportDist = value; }
        }

        int? _CenterDist;
        public int? CenterDist
        {
            get { return _CenterDist; }
            set { _CenterDist = value; }
        }

        int? _SeaDist;
        public int? SeaDist
        {
            get { return _SeaDist; }
            set { _SeaDist = value; }
        }

        DateTime? _CheckInTime;
        public DateTime? CheckInTime
        {
            get { return _CheckInTime; }
            set { _CheckInTime = value; }
        }

        DateTime? _CheckOutTime;
        public DateTime? CheckOutTime
        {
            get { return _CheckOutTime; }
            set { _CheckOutTime = value; }
        }

        string _PostAddress;
        public string PostAddress
        {
            get { return _PostAddress; }
            set { _PostAddress = value; }
        }

        int _Location;
        public int Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _LocationLocalName;
        public string LocationLocalName
        {
            get { return _LocationLocalName; }
            set { _LocationLocalName = value; }
        }

        int _TrfLocation;
        public int TrfLocation
        {
            get { return _TrfLocation; }
            set { _TrfLocation = value; }
        }

        string _TrfLocationName;
        public string TrfLocationName
        {
            get { return _TrfLocationName; }
            set { _TrfLocationName = value; }
        }

        string _TrfLocationLocalName;
        public string TrfLocationLocalName
        {
            get { return _TrfLocationLocalName; }
            set { _TrfLocationLocalName = value; }
        }

        string _PostZip;
        public string PostZip
        {
            get { return _PostZip; }
            set { _PostZip = value; }
        }

        string _InvAddress;
        public string InvAddress
        {
            get { return _InvAddress; }
            set { _InvAddress = value; }
        }

        string _Phone1;
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        string _Phone2;
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }

        string _Fax;
        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }

        string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        string _homepage;
        public string Homepage
        {
            get { return _homepage; }
            set { _homepage = value; }
        }

        string _ManagerName;
        public string ManagerName
        {
            get { return _ManagerName; }
            set { _ManagerName = value; }
        }

        Int16? _RoomCount;
        public Int16? RoomCount
        {
            get { return _RoomCount; }
            set { _RoomCount = value; }
        }

        string _BossName;
        public string BossName
        {
            get { return _BossName; }
            set { _BossName = value; }
        }

        string _ContactName;
        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }

        string _TaxOffice;
        public string TaxOffice
        {
            get { return _TaxOffice; }
            set { _TaxOffice = value; }
        }

        string _TaxAccNo;
        public string TaxAccNo
        {
            get { return _TaxAccNo; }
            set { _TaxAccNo = value; }
        }

        string _AirPort1;
        public string AirPort1
        {
            get { return _AirPort1; }
            set { _AirPort1 = value; }
        }

        Int16? _ChdAgeGrpCnt;
        public Int16? ChdAgeGrpCnt
        {
            get { return _ChdAgeGrpCnt; }
            set { _ChdAgeGrpCnt = value; }
        }

        Int16? _ConfStat;
        public Int16? ConfStat
        {
            get { return _ConfStat; }
            set { _ConfStat = value; }
        }

        decimal? _TaxPer;
        public decimal? TaxPer
        {
            get { return _TaxPer; }
            set { _TaxPer = value; }
        }

        bool _UseSysParamAllot;
        public bool UseSysParamAllot
        {
            get { return _UseSysParamAllot; }
            set { _UseSysParamAllot = value; }
        }

        bool _HotAllotChk;
        public bool HotAllotChk
        {
            get { return _HotAllotChk; }
            set { _HotAllotChk = value; }
        }

        bool _HotAllotWarnFull;
        public bool HotAllotWarnFull
        {
            get { return _HotAllotWarnFull; }
            set { _HotAllotWarnFull = value; }
        }

        bool _HotAllotNotAcceptFull;
        public bool HotAllotNotAcceptFull
        {
            get { return _HotAllotNotAcceptFull; }
            set { _HotAllotNotAcceptFull = value; }
        }

        bool _HotAllotNotFullGA;
        public bool HotAllotNotFullGA
        {
            get { return _HotAllotNotFullGA; }
            set { _HotAllotNotFullGA = value; }
        }

        bool _PayCom;
        public bool PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }

        bool _PayComEB;
        public bool PayComEB
        {
            get { return _PayComEB; }
            set { _PayComEB = value; }
        }

        bool _PayPasEB;
        public bool PayPasEB
        {
            get { return _PayPasEB; }
            set { _PayPasEB = value; }
        }

        string _DefSupplier;
        public string DefSupplier
        {
            get { return _DefSupplier; }
            set { _DefSupplier = value; }
        }

        bool _CanDiscount;
        public bool CanDiscount
        {
            get { return _CanDiscount; }
            set { _CanDiscount = value; }
        }

        decimal? _MaxChdAge;
        public decimal? MaxChdAge
        {
            get { return _MaxChdAge; }
            set { _MaxChdAge = value; }
        }

        decimal? _Lat;
        public decimal? Lat
        {
            get { return _Lat; }
            set { _Lat = value; }
        }

        decimal? _Lon;
        public decimal? Lon
        {
            get { return _Lon; }
            set { _Lon = value; }
        }

        string _OfCategory;
        public string OfCategory
        {
            get { return _OfCategory; }
            set { _OfCategory = value; }
        }

        bool _WithoutLady;
        public bool WithoutLady
        {
            get { return _WithoutLady; }
            set { _WithoutLady = value; }
        }

    }

    public class HotelRoomRecord
    {
        public HotelRoomRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        int? _DispNo;
        public int? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

        string _BaseRoom;
        public string BaseRoom
        {
            get { return _BaseRoom; }
            set { _BaseRoom = value; }
        }

    }

    public class HotelBoardRecord
    {
        public HotelBoardRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        int? _DispNo;
        public int? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

    }

    public class HotelAccomRecord
    {
        public HotelAccomRecord()
        {
        }

        string _Hotel;
        public string Hotel
        {
            get { return _Hotel; }
            set { _Hotel = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _LocalName;
        public string LocalName
        {
            get { return _LocalName; }
            set { _LocalName = value; }
        }

        string _Name2;
        public string Name2
        {
            get { return _Name2; }
            set { _Name2 = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        Int16? _StdAdl;
        public Int16? StdAdl
        {
            get { return _StdAdl; }
            set { _StdAdl = value; }
        }

        Int16? _MinAdl;
        public Int16? MinAdl
        {
            get { return _MinAdl; }
            set { _MinAdl = value; }
        }

        Int16? _MaxAdl;
        public Int16? MaxAdl
        {
            get { return _MaxAdl; }
            set { _MaxAdl = value; }
        }

        Int16? _MaxChd;
        public Int16? MaxChd
        {
            get { return _MaxChd; }
            set { _MaxChd = value; }
        }

        Int16? _MaxPax;
        public Int16? MaxPax
        {
            get { return _MaxPax; }
            set { _MaxPax = value; }
        }

        Int16? _DispNo;
        public Int16? DispNo
        {
            get { return _DispNo; }
            set { _DispNo = value; }
        }

    }

    public class ResServiceRecord
    {
        public ResServiceRecord()
        { 
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        int? _DepLocation;
        public int? DepLocation
        {
            get { return _DepLocation; }
            set { _DepLocation = value; }
        }

        int? _ArrLocation;
        public int? ArrLocation
        {
            get { return _ArrLocation; }
            set { _ArrLocation = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _Room;
        public string Room
        {
            get { return _Room; }
            set { _Room = value; }
        }

        string _Board;
        public string Board
        {
            get { return _Board; }
            set { _Board = value; }
        }

        string _Accom;
        public string Accom
        {
            get { return _Accom; }
            set { _Accom = value; }
        }

        string _FlgClass;
        public string FlgClass
        {
            get { return _FlgClass; }
            set { _FlgClass = value; }
        }

        string _SupNote;
        public string SupNote
        {
            get { return _SupNote; }
            set { _SupNote = value; }
        }
    }

    public class FlightClassRecord
    {
        public FlightClassRecord()
        {
        }

        int _RecID;
        public int RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _Class;
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _NameS;
        public string NameS
        {
            get { return _NameS; }
            set { _NameS = value; }
        }

        string _NameL;
        public string NameL
        {
            get { return _NameL; }
            set { _NameL = value; }
        }

        string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        bool _B2B;
        public bool B2B
        {
            get { return _B2B; }
            set { _B2B = value; }
        }

        bool _B2C;
        public bool B2C
        {
            get { return _B2C; }
            set { _B2C = value; }
        }

    }

    public class Det_Fin_FlightDetailRecord
    { 
        public Det_Fin_FlightDetailRecord()
        {
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        DateTime? _Date;
        public DateTime? Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        DateTime? _DepTime;
        public DateTime? DepTime
        {
            get { return _DepTime; }
            set { _DepTime = value; }
        }

        DateTime? _ArrTime;
        public DateTime? ArrTime
        {
            get { return _ArrTime; }
            set { _ArrTime = value; }
        }

        string _DepAirport;
        public string DepAirport
        {
            get { return _DepAirport; }
            set { _DepAirport = value; }
        }

        string _ArrAirport;
        public string ArrAirport
        {
            get { return _ArrAirport; }
            set { _ArrAirport = value; }
        }

        string _Departure;
        public string Departure
        {
            get { return _Departure; }
            set { _Departure = value; }
        }

        string _Arrival;
        public string Arrival
        {
            get { return _Arrival; }
            set { _Arrival = value; }
        }

        DateTime? _ArrDate;
        public DateTime? ArrDate
        {
            get { return _ArrDate; }
            set { _ArrDate = value; }
        }

        string _FlgClass;
        public string FlgClass
        {
            get { return _FlgClass; }
            set { _FlgClass = value; }
        }

        Int16? _FlyDur;
        public Int16? FlyDur
        {
            get { return _FlyDur; }
            set { _FlyDur = value; }
        }

        string _PNLName;
        public string PNLName
        {
            get { return _PNLName; }
            set { _PNLName = value; }
        }
    }

    public class Det_Fin_HotelDetailRecord
    { 
        public Det_Fin_HotelDetailRecord()
        {
        }

        string _ServiceName;
        public string ServiceName
        {
            get { return _ServiceName; }
            set { _ServiceName = value; }
        }

        string _Location;
        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        string _RoomName;
        public string RoomName
        {
            get { return _RoomName; }
            set { _RoomName = value; }
        }

        string _HotelCat;
        public string HotelCat
        {
            get { return _HotelCat; }
            set { _HotelCat = value; }
        }

        Int16? _Unit;
        public Int16? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }
    }
}