﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvReport.MahbalReport
{
    public class PasPriceRecord
    {
        public PasPriceRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int16? _RecType;
        public Int16? RecType
        {
            get { return _RecType; }
            set { _RecType = value; }
        }

        string _IncPack;
        public string IncPack
        {
            get { return _IncPack; }
            set { _IncPack = value; }
        }

        Int16? _Unit;
        public Int16? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _ServiceDesc;
        public string ServiceDesc
        {
            get { return _ServiceDesc; }
            set { _ServiceDesc = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }
    }

    public class PaxCountRecord
    {
        public PaxCountRecord()
        {
        }

        Int16? _AdlCnt;
        public Int16? AdlCnt
        {
            get { return _AdlCnt; }
            set { _AdlCnt = value; }
        }

        Int16? _ChdCnt;
        public Int16? ChdCnt
        {
            get { return _ChdCnt; }
            set { _ChdCnt = value; }
        }

        Int16? _InfCnt;
        public Int16? InfCnt
        {
            get { return _InfCnt; }
            set { _InfCnt = value; }
        }
    }

    public class ETicketHotelRecord
    {
        public ETicketHotelRecord()
        { }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Location;
        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
    }

    public class Mahbal_Receipt
    {
        public Mahbal_Receipt() { }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        string _ReceiptSerial;
        public string ReceiptSerial
        {
            get { return _ReceiptSerial; }
            set { _ReceiptSerial = value; }
        }

        int? _ReceiptNo;
        public int? ReceiptNo
        {
            get { return _ReceiptNo; }
            set { _ReceiptNo = value; }
        }

        DateTime? _ReceiptDate;
        public DateTime? ReceiptDate
        {
            get { return _ReceiptDate; }
            set { _ReceiptDate = value; }
        }

        DateTime? _PaymentDate;
        public DateTime? PaymentDate
        {
            get { return _PaymentDate; }
            set { _PaymentDate = value; }
        }

        string _PaymentType;
        public string PaymentType
        {
            get { return _PaymentType; }
            set { _PaymentType = value; }
        }

        string _Reference;
        public string Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        decimal? _Amount;
        public decimal? Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        string _Cur;
        public string Cur
        {
            get { return _Cur; }
            set { _Cur = value; }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        DateTime? _RegisterDate;
        public DateTime? RegisterDate
        {
            get { return _RegisterDate; }
            set { _RegisterDate = value; }
        }

        string _ClientName;
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        string _ClientAddress;
        public string ClientAddress
        {
            get { return _ClientAddress; }
            set { _ClientAddress = value; }
        }

        string _ClientZip;
        public string ClientZip
        {
            get { return _ClientZip; }
            set { _ClientZip = value; }
        }

        string _ClientCity;
        public string ClientCity
        {
            get { return _ClientCity; }
            set { _ClientCity = value; }
        }

        string _ClientCountry;
        public string ClientCountry
        {
            get { return _ClientCountry; }
            set { _ClientCountry = value; }
        }

    }
}
