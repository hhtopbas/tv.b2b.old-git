﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;

namespace TvReport
{
    public enum writeType { Text = 0, Line = 1, Circle = 2, Rectangle = 3, Image = 3, Field = 4, PdfTable = 5, Html = 6, AreaText = 7, QRCode = 8, BarCode = 9 };
    public enum fieldTypes { Text = 0, CheckBox = 1 }
    public enum fontAlign { Left = 0, Right = 1, Center = 2 };

    public class TvReportCommon
    {
        public static void writeTextRightAlign(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            cb.SetColorFill(iTextSharp.text.BaseColor.BLACK);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, text, x, y, 0);
            cb.EndText();
        }

        public static void writeTextRightAlign(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize, iTextSharp.text.BaseColor color)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            cb.SetColorFill(color);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, text, x, y, 0);
            cb.EndText();
        }

        public static void writeTextCenter(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            cb.SetColorFill(iTextSharp.text.BaseColor.BLACK);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, text, x, y, 0);
            cb.EndText();
        }

        public static void writeTextCenter(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize, iTextSharp.text.BaseColor color)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            cb.SetColorFill(color);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, text, x, y, 0);
            cb.EndText();
        }

        public static void writeText(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            cb.SetColorFill(iTextSharp.text.BaseColor.BLACK);
            cb.SetTextMatrix(x, y);
            cb.ShowText(text);
            cb.EndText();
        }

        public static void writeText(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize, bool bold, bool underline, systemColor color, fontAlign align)
        {
            int _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_LEFT;
            switch (align)
            {
                case fontAlign.Left: _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_LEFT; break;
                case fontAlign.Right: _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_RIGHT; break;
                case fontAlign.Center: _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_CENTER; break;
                default: _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_LEFT; break;
            }
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            if (color != null)
                cb.SetColorFill(color == null ? iTextSharp.text.BaseColor.BLACK : new BaseColor(color.R.Value, color.G.Value, color.B.Value));
            cb.ShowTextAligned(_align, text, x, y, 0);
            cb.EndText();
            if (underline)
            {
                float width = cb.GetEffectiveStringWidth(text, false);
                cb.SetLineWidth(Convert.ToSingle(fontSize * (bold ? 0.05 : 0.02)));
                float _width = Convert.ToSingle(0.0);

                if (align == fontAlign.Right) _width = (-1) * width;
                else if (align == fontAlign.Center) _width = (-1) * (width / Convert.ToSingle(2.0));

                cb.MoveTo(x + _width, y - 1);
                cb.LineTo(x + width + _width, y - 1);
                cb.Stroke();
            }
        }

        public static void _addFieldText(PdfWriter writer, BaseFont font, PdfContentByte cb, TvReport.Coordinate record)
        {
            iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(record.x, record.y, record.x + record.W, record.y + record.H);

            // Change the font Widths
            int[] widths = font.Widths;
            for (int k = 0; k < widths.Length; k++) if (widths[k] != 0) widths[k] = 256;

            font.ForceWidthsOutput = true;
            string text = " ";
            string all = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-*/+.,\"'():;,?./!$+=_{[]}&éèàêîâ";
            iTextSharp.text.BaseColor textColor = iTextSharp.text.BaseColor.BLACK;

            // Create the Acroform TextField
            PdfFormField field = PdfFormField.CreateTextField(writer, PdfFormField.SINGLELINE, PdfFormField.PLAINTEXT, 128);
            field.FieldName = record.FieldName;
            field.DefaultValueAsString = text;
            field.SetFieldFlags(PdfAnnotation.FLAGS_PRINT);
            field.SetPage();
            writer.AddAnnotation(field);

            // Position the field on the page
            field.SetWidget(rec, PdfAnnotation.HIGHLIGHT_PUSH);

            // Create apperance so the field Size and font match what we want
            PdfAppearance tp = cb.CreateAppearance(rec.Right - rec.Left, rec.Top - rec.Bottom);
            PdfAppearance da = (PdfAppearance)tp.Duplicate;
            da.SetFontAndSize(font, record.FontSize);
            da.SetColorFill(textColor);
            field.DefaultAppearanceString = da;

            // Fill the default value of the field
            tp.BeginVariableText();
            tp.SaveState();
            tp.Rectangle(rec);
            tp.Clip();
            tp.NewPath();
            tp.BeginText();
            tp.SetFontAndSize(font, record.FontSize);
            tp.SetColorFill(textColor);
            tp.SetTextMatrix(100, 100);
            tp.ShowText(all);
            tp.SetTextMatrix(2, (rec.Top - rec.Bottom) / 2 - (record.FontSize * 0.3f));
            tp.ShowText(text);
            tp.EndText();
            tp.RestoreState();
            tp.EndVariableText();
            field.SetAppearance(PdfAnnotation.APPEARANCE_NORMAL, tp);

            writer.AcroForm.SetTextFieldParams(field, record.value, record.FieldName, record.x, record.y, record.x + record.W, record.y + record.H);
        }

        public static void _addFieldChkBox(PdfWriter writer, BaseFont font, PdfContentByte cb, TvReport.Coordinate record)
        {
            float x;
            float y;
            float w;
            float h;
            if (record.Align == fontAlign.Left)
            {
                x = record.x;
                y = record.y;

                cb.BeginText();
                cb.SetFontAndSize(font, record.FontSize);
                float Width = cb.GetEffectiveStringWidth(record.Caption, false);
                cb.SetColorFill(record.Color == null ? iTextSharp.text.BaseColor.BLACK : new BaseColor(record.Color.R.Value, record.Color.G.Value, record.Color.B.Value));
                cb.ShowTextAligned(iTextSharp.text.pdf.PdfContentByte.ALIGN_LEFT, record.Caption, record.x + (record.FontSize * 1.0716f) + 4.0f, record.y, 0);
                cb.EndText();
                if (record.Underline)
                {
                    cb.SetLineWidth(Convert.ToSingle(record.FontSize * (record.bold ? 0.05 : 0.02)));
                    float _width = Convert.ToSingle(0.0);
                    cb.MoveTo(record.x + _width + (record.FontSize * 1.0716f) + 1.5f, record.y - 1);
                    cb.LineTo(record.x + Width + _width + (record.FontSize * 1.0716f) + 1.5f, record.y - 1);
                    cb.Stroke();
                }
                w = (record.FontSize * 1.0716f);
                h = (record.FontSize * 1.0716f);

            }
            else
            {
                cb.BeginText();
                cb.SetFontAndSize(font, record.FontSize);
                float Width = cb.GetEffectiveStringWidth(record.Caption, false);
                cb.SetColorFill(record.Color == null ? iTextSharp.text.BaseColor.BLACK : new BaseColor(record.Color.R.Value, record.Color.G.Value, record.Color.B.Value));
                cb.ShowTextAligned(iTextSharp.text.pdf.PdfContentByte.ALIGN_LEFT, record.Caption, record.x, record.y, 0);
                cb.EndText();
                if (record.Underline)
                {
                    cb.SetLineWidth(Convert.ToSingle(record.FontSize * (record.bold ? 0.05 : 0.02)));
                    float _width = Convert.ToSingle(0.0);
                    cb.MoveTo(record.x + _width, record.y - 1);
                    cb.LineTo(record.x + Width + _width, record.y - 1);
                    cb.Stroke();
                }
                x = record.x + Width + 2.0f;
                y = record.y;
                w = (record.FontSize * 1.0716f);
                h = (record.FontSize * 1.0716f);
            }

            iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(x - 2.0f, y - 2.0f, x - 2.0f + 10, y - 2.0f + 10);
            PdfFormField field = PdfFormField.CreateCheckBox(writer);
            PdfAppearance tpOff = cb.CreateAppearance(20, 20);
            PdfAppearance tpOn = cb.CreateAppearance(20, 20);
            tpOff.Rectangle(1, 1, 16, 16);
            tpOff.Stroke();

            tpOn.Rectangle(1, 1, 16, 16);
            cb.SetLineWidth(6.0f);
            tpOn.MoveTo(3, 10);
            tpOn.LineTo(8, 4);
            tpOn.MoveTo(8, 4);
            tpOn.LineTo(15, 15);
            tpOn.Stroke();
            field.SetWidget(rec, PdfAnnotation.HIGHLIGHT_TOGGLE);

            field.FieldName = record.FieldName;
            field.ValueAsName = record.value;
            field.AppearanceState = record.value;
            field.SetAppearance(PdfAnnotation.APPEARANCE_NORMAL, "Off", tpOff);
            field.SetAppearance(PdfAnnotation.APPEARANCE_NORMAL, "On", tpOn);
            field.Border = new PdfBorderArray(0f, 0f, 1.5f);
            field.BorderStyle = new PdfBorderDictionary(1f, 0);
            writer.AddAnnotation(field);
            writer.AcroForm.SetCheckBoxParams(field, record.FieldName, record.value, record.value == "On", x - 2.0f, y - 2.0f, x - 2.0f + 10, y - 2.0f + 10);
        }

        public static void writeField(ref PdfContentByte cb, ref PdfWriter writer, BaseFont bfont, TvReport.Coordinate record)
        {
            switch (record.FieldType)
            {
                case fieldTypes.Text:
                    //cb.DrawTextField(record.x, record.y, record.x + record.W, record.y + record.H);
                    _addFieldText(writer, bfont, cb, record);
                    cb.SetLineWidth(0.1f);
                    cb.SetLineDash(2.5f, 2.5f);
                    cb.SetColorStroke(BaseColor.BLUE);
                    cb.MoveTo(record.x, record.y);
                    cb.LineTo(record.x + record.W, record.y);
                    cb.Stroke();
                    break;
                case fieldTypes.CheckBox:
                    _addFieldChkBox(writer, bfont, cb, record);
                    break;
            }
        }

        public static void writeLine(ref PdfContentByte cb, float x, float y, float w, float h, float lineWidth)
        {
            cb.SetLineWidth(lineWidth);
            cb.MoveTo(x, y);
            cb.LineTo(x + w, y + h);
            cb.Stroke();
        }

        public static void writePdfTable(ref PdfContentByte cb, ref PdfWriter writer, ref Document doc, BaseFont bfont, TvReport.Coordinate record)
        {
            List<TvReport.tableRow> pdfTable = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TvReport.tableRow>>(record.value);
            if (pdfTable != null && pdfTable.Count > 0)
            {
                cb.SetLineDash(0.01f);
                cb.SetColorStroke(BaseColor.BLACK);
                int columns = pdfTable.FirstOrDefault().TableRow.Count;
                float? tableTotalWidth = pdfTable.FirstOrDefault().TableRow.Sum(s => s.Width);
                float? tableTotalHeight = pdfTable.Sum(s => s.RowHeight);
                writeLine(ref cb, record.x, record.y, tableTotalWidth.HasValue ? tableTotalWidth.Value : 450f, 0f, 0.25f);
                float tableHeight = 0f;
                foreach (TvReport.tableRow row in pdfTable)
                {
                    List<tableCell> cells = new List<tableCell>();
                    cells = row.TableRow;
                    float width = 0f;
                    writeLine(ref cb, record.x, record.y - tableHeight - row.RowHeight.Value, 0f, row.RowHeight.Value, 0.25f);
                    foreach (tableCell cell in cells)
                    {
                        float x = record.x + width;
                        float y = record.y - row.RowHeight.Value - tableHeight;
                        float alignX = 0f;
                        switch (cell.Align)
                        {
                            case fontAlign.Center: alignX = cell.Width.Value / 2.0f; break;
                            case fontAlign.Right: alignX = cell.Width.Value - 2f; break;
                            default: alignX = cell.Width.Value * 0.05f; break;
                        }
                        writeLine(ref cb, x + cell.Width.Value, y, 0f, row.RowHeight.Value, 0.25f);
                        writeText(ref cb, cell.Text, x + alignX, y + 2f, bfont, record.FontSize, record.bold, record.Underline, record.Color, cell.Align);
                        width += cell.Width.Value;
                    }
                    tableHeight += row.RowHeight.Value;
                    writeLine(ref cb, record.x, record.y - tableHeight, tableTotalWidth.HasValue ? tableTotalWidth.Value : 450f, 0f, 0.25f);
                }
            }
        }

        public static void writeImage(ref Document cb, byte[] image, float x, float y, float? w, float? h)
        {
            if (image == null) return;
            byte[] _image = (byte[])image;
            iTextSharp.text.Image png = iTextSharp.text.Image.GetInstance(_image);
            if (w.HasValue && w.Value > 0 && h.HasValue && h.Value > 0)
                png = ImageResize.FixedSize(png, w.Value, h.Value);
            else
                if (w.HasValue && w.Value > 0)
                    png = ImageResize.ConstrainProportions(png, w.Value, Dimensions.Width);
                else
                    if (h.HasValue && h.Value > 0)
                        png = ImageResize.ConstrainProportions(png, h.Value, Dimensions.Height);
            png.SetAbsolutePosition(x, y);
            cb.Add(png);
        }

        public static void writeAreaText(ref PdfContentByte cb, string text, BaseFont bfont, float? fontSize, float x, float y, float? w, float? h)
        {
            int style = 0;
            iTextSharp.text.Font font = new iTextSharp.text.Font(bfont, fontSize.Value, style);
            ColumnText columnText = new ColumnText(cb);
            columnText.SetSimpleColumn(
                /*Document.Left*/ x,
                /*Document.Bottom*/ y - h.Value,
                /*Document.Right*/ x + w.Value,
                /*position1*/ y + 2f,
                fontSize.Value, Element.ALIGN_TOP);
            columnText.AddText(new Phrase(text, font));
            columnText.Go();

            //Chunk c = new Chunk(text, font);            
            //Paragraph p = new Paragraph(c);                        
            //cb.Add(p);                        
        }

        public static void writeText(ref PdfContentByte cb, string text, byte[] image, float x, float y, float? w, float? h, float? width, BaseFont bfont, float fontSize, bool bold, bool underline, systemColor color, fontAlign align, writeType type)
        {
            switch (type)
            {
                case writeType.Image:
                    #region Image
                    #endregion
                    break;
                case writeType.Line:
                    #region Line
                    cb.SetLineWidth(width.Value);
                    cb.MoveTo(x, y);
                    cb.LineTo(x + w.Value, y + h.Value);
                    cb.Stroke();
                    #endregion
                    break;
                case writeType.Text:
                    #region Text
                    int _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_LEFT;
                    switch (align)
                    {
                        case fontAlign.Left: _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_LEFT; break;
                        case fontAlign.Right: _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_RIGHT; break;
                        case fontAlign.Center: _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_CENTER; break;
                        default: _align = iTextSharp.text.pdf.PdfContentByte.ALIGN_LEFT; break;
                    }
                    cb.BeginText();
                    cb.SetFontAndSize(bfont, fontSize);

                    cb.SetColorFill(color == null ? iTextSharp.text.BaseColor.BLACK : new BaseColor(color.R.Value, color.G.Value, color.B.Value));
                    cb.ShowTextAligned(_align, text, x, y, 0);
                    cb.EndText();
                    if (underline)
                    {
                        float Width = cb.GetEffectiveStringWidth(text, false);
                        cb.SetLineWidth(Convert.ToSingle(fontSize * (bold ? 0.05 : 0.02)));
                        float _width = Convert.ToSingle(0.0);

                        if (align == fontAlign.Right) _width = (-1) * Width;
                        else if (align == fontAlign.Center) _width = (-1) * (Width / Convert.ToSingle(2.0));

                        cb.MoveTo(x + _width, y - 1);
                        cb.LineTo(x + Width + _width, y - 1);
                        cb.Stroke();
                    }
                    #endregion
                    break;
                case writeType.AreaText:
                    break;
            }
        }

        public static void writeText(ref PdfContentByte cb, string text, float x, float y, BaseFont bfont, float fontSize, systemColor color)
        {
            cb.BeginText();
            cb.SetFontAndSize(bfont, fontSize);
            cb.SetColorFill(new BaseColor(color.R.Value, color.G.Value, color.B.Value));
            cb.SetTextMatrix(x, y);
            cb.ShowText(text);
            cb.EndText();
        }

        public static void writeHtml(ref Document doc, ref PdfContentByte cb, Coordinate row, BaseFont bFont)
        {
            using (TextReader sReader = new StringReader(row.value.ToString()))
            {

                iTextSharp.text.html.simpleparser.StyleSheet style = new iTextSharp.text.html.simpleparser.StyleSheet();
                style.LoadTagStyle("p", "font-family", "Sans-Serif");
                List<IElement> list = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(sReader, style);
                foreach (IElement elm in list)
                {
                    doc.Add(elm);
                }
            }
        }

        public static void writeQRCode(ref Document cb, string text, float x, float y, float? w, float? h)
        {

            iTextSharp.text.pdf.BarcodePDF417 pdf417 = new iTextSharp.text.pdf.BarcodePDF417();
            pdf417.SetText(text);
            iTextSharp.text.Image img = pdf417.GetImage();
            iTextSharp.text.pdf.BarcodeQRCode qrcode = new iTextSharp.text.pdf.BarcodeQRCode(text, 1, 1, null);
            img = qrcode.GetImage();


            //iTextSharp.text.pdf.PdfContentByte cb = writer.DirectContent;
            //cb.SaveState();
            //cb.BeginText();
            if (w.HasValue && w.Value > 0 && h.HasValue && h.Value > 0)
                img = ImageResize.FixedSize(img, w.Value, h.Value);
            else
                if (w.HasValue && w.Value > 0)
                    img = ImageResize.ConstrainProportions(img, w.Value, Dimensions.Width);
                else
                    if (h.HasValue && h.Value > 0)
                        img = ImageResize.ConstrainProportions(img, h.Value, Dimensions.Height);
            img.SetAbsolutePosition(x, y);
            cb.Add(img);
            //cb.AddImage(img);
            //cb.EndText();
            //cb.RestoreState();            
        }

        public static void writeBarCode(ref PdfContentByte cb, string text, float x, float y, float? w, float? h)
        {

            iTextSharp.text.pdf.Barcode39 code = new Barcode39();
            code.Code = text;
            iTextSharp.text.Image img = code.CreateImageWithBarcode(cb, BaseColor.BLACK, BaseColor.WHITE);

            if (w.HasValue && w.Value > 0 && h.HasValue && h.Value > 0)
                img = ImageResize.FixedSize(img, Convert.ToInt32(w.Value), Convert.ToInt32(h.Value));
            else
                if (w.HasValue && w.Value > 0)
                    img = ImageResize.ConstrainProportions(img, Convert.ToInt32(w.Value), Dimensions.Width);
                else
                    if (h.HasValue && h.Value > 0)
                        img = ImageResize.ConstrainProportions(img, Convert.ToInt32(h.Value), Dimensions.Height);
            img.SetAbsolutePosition(x, y);
            cb.AddImage(img);
        }
    }

    public class systemColor
    {
        public systemColor()
        {
            _R = 0;
            _G = 0;
            _B = 0;
        }

        int? _R;
        public int? R
        {
            get { return _R; }
            set { _R = value; }
        }

        int? _G;
        public int? G
        {
            get { return _G; }
            set { _G = value; }
        }

        int? _B;
        public int? B
        {
            get { return _B; }
            set { _B = value; }
        }
    }

    [Serializable]
    public class Coordinate
    {
        public Coordinate()
        {
            _type = writeType.Text;
            _fieldType = fieldTypes.Text;
            _isShown = true;
            _bold = false;
            _fontSize = 8;
            _rightToLeft = false;
            _center = false;
            _underline = false;
            _align = fontAlign.Left;
            _pageNo = 1;
            _pic = null;
            _italic = false;
        }

        writeType _type;
        public writeType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        string _fieldName;
        public string FieldName
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }

        fieldTypes _fieldType;
        public fieldTypes FieldType
        {
            get { return _fieldType; }
            set { _fieldType = value; }
        }

        string _value;
        public string value
        {
            get { return _value; }
            set { _value = value; }
        }

        string _Caption;
        public string Caption
        {
            get { return _Caption; }
            set { _Caption = value; }
        }

        byte[] _pic;
        public byte[] Pic
        {
            get { return _pic; }
            set { _pic = value; }
        }

        float _x;
        public float x
        {
            get { return _x; }
            set { _x = value; }
        }

        float _y;
        public float y
        {
            get { return _y; }
            set { _y = value; }
        }

        float _h;
        public float H
        {
            get { return _h; }
            set { _h = value; }
        }

        float _w;
        public float W
        {
            get { return _w; }
            set { _w = value; }
        }

        bool _isShown;
        public bool isShown
        {
            get { return _isShown; }
            set { _isShown = value; }
        }

        float _fontSize;
        public float FontSize
        {
            get { return _fontSize; }
            set { _fontSize = value; }
        }

        bool _bold;
        public bool bold
        {
            get { return _bold; }
            set { _bold = value; }
        }

        bool _underline;
        public bool Underline
        {
            get { return _underline; }
            set { _underline = value; }
        }

        bool _italic;
        public bool Italic
        {
            get { return _italic; }
            set { _italic = value; }
        }

        bool _rightToLeft;
        public bool RightToLeft
        {
            get { return _rightToLeft; }
            set { _rightToLeft = value; }
        }

        bool _center;
        public bool Center
        {
            get { return _center; }
            set { _center = value; }
        }

        systemColor _color;
        public systemColor Color
        {
            get { return _color; }
            set { _color = value; }
        }

        fontAlign _align;
        public fontAlign Align
        {
            get { return _align; }
            set { _align = value; }
        }

        float _lineWidth;
        public float LineWidth
        {
            get { return _lineWidth; }
            set { _lineWidth = value; }
        }

        int _pageNo;
        public int PageNo
        {
            get { return _pageNo; }
            set { _pageNo = value; }
        }
    }

    [Serializable]
    public class tableRow
    {
        public tableRow()
        {
            _tableRow = new List<tableCell>();
        }

        float? _rowHeight;
        public float? RowHeight
        {
            get { return _rowHeight; }
            set { _rowHeight = value; }
        }

        List<tableCell> _tableRow;
        public List<tableCell> TableRow
        {
            get { return _tableRow; }
            set { _tableRow = value; }
        }
    }

    [Serializable]
    public class tableCell
    {
        public tableCell()
        {
            _align = 0;
        }

        float? _width;
        public float? Width
        {
            get { return _width; }
            set { _width = value; }
        }

        fontAlign _align;
        public fontAlign Align
        {
            get { return _align; }
            set { _align = value; }
        }

        string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
    }

    [Serializable]
    public class htmlCodeData
    {
        public htmlCodeData()
        {
            _PdfPage = 1;
            _AfterInsertShow = false;
            _Disabled = false;
        }

        string _IdName;
        public string IdName
        {
            get { return _IdName; }
            set { _IdName = value; }
        }

        string _TagName;
        public string TagName
        {
            get { return _TagName; }
            set { _TagName = value; }
        }

        string _Data;
        public string Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        object _Value;
        public object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        int _PdfPage;
        public int PdfPage
        {
            get { return _PdfPage; }
            set { _PdfPage = value; }
        }

        bool _AfterInsertShow;
        public bool AfterInsertShow
        {
            get { return _AfterInsertShow; }
            set { _AfterInsertShow = value; }
        }

        string _Css;
        public string Css
        {
            get { return _Css; }
            set { _Css = value; }
        }

        bool _Disabled;
        public bool Disabled
        {
            get { return _Disabled; }
            set { _Disabled = value; }
        }

    }
}
