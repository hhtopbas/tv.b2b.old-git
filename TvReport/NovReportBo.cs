﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvReport.NovReport
{
    public class AgencyDocAddressRecord
    {
        public AgencyDocAddressRecord()
        {
        }

        string _MainOffice;
        public string MainOffice
        {
            get { return _MainOffice; }
            set { _MainOffice = value; }
        }

        bool _UseMainCont;
        public bool UseMainCont
        {
            get { return _UseMainCont; }
            set { _UseMainCont = value; }
        }

        string _VocAddrType;
        public string VocAddrType
        {
            get { return _VocAddrType; }
            set { _VocAddrType = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _FirmName;
        public string FirmName
        {
            get { return _FirmName; }
            set { _FirmName = value; }
        }

        string _DAddress;
        public string DAddress
        {
            get { return _DAddress; }
            set { _DAddress = value; }
        }

        string _Address;
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        string _DAddrZip;
        public string DAddrZip
        {
            get { return _DAddrZip; }
            set { _DAddrZip = value; }
        }

        string _AddrZip;
        public string AddrZip
        {
            get { return _AddrZip; }
            set { _AddrZip = value; }
        }

        string _AddrCity;
        public string AddrCity
        {
            get { return _AddrCity; }
            set { _AddrCity = value; }
        }

        string _DAddrCity;
        public string DAddrCity
        {
            get { return _DAddrCity; }
            set { _DAddrCity = value; }
        }

        string _AddrCountry;
        public string AddrCountry
        {
            get { return _AddrCountry; }
            set { _AddrCountry = value; }
        }

        string _DAddrCountry;
        public string DAddrCountry
        {
            get { return _DAddrCountry; }
            set { _DAddrCountry = value; }
        }

        string _Phone1;
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        string _DPhone;
        public string DPhone
        {
            get { return _DPhone; }
            set { _DPhone = value; }
        }

        string _Fax1;
        public string Fax1
        {
            get { return _Fax1; }
            set { _Fax1 = value; }
        }

        string _DFax;
        public string DFax
        {
            get { return _DFax; }
            set { _DFax = value; }
        }

        string _EMail1;
        public string EMail1
        {
            get { return _EMail1; }
            set { _EMail1 = value; }
        }

        bool _DocPrtNoPay;
        public bool DocPrtNoPay
        {
            get { return _DocPrtNoPay; }
            set { _DocPrtNoPay = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _TaxAccNo;
        public string TaxAccNo
        {
            get { return _TaxAccNo; }
            set { _TaxAccNo = value; }
        }

        string _TaxOffice;
        public string TaxOffice
        {
            get { return _TaxOffice; }
            set { _TaxOffice = value; }
        }

        string _BossName;
        public string BossName
        {
            get { return _BossName; }
            set { _BossName = value; }
        }

        string _InvAddress;
        public string InvAddress
        {
            get { return _InvAddress; }
            set { _InvAddress = value; }
        }

        string _InvAddrZip;
        public string InvAddrZip
        {
            get { return _InvAddrZip; }
            set { _InvAddrZip = value; }
        }

        string _InvAddrCity;
        public string InvAddrCity
        {
            get { return _InvAddrCity; }
            set { _InvAddrCity = value; }
        }

        string _InvAddrCountry;
        public string InvAddrCountry
        {
            get { return _InvAddrCountry; }
            set { _InvAddrCountry = value; }
        }

        string _Bank1;
        public string Bank1
        {
            get { return _Bank1; }
            set { _Bank1 = value; }
        }

        string _Bank1Name;
        public string Bank1Name
        {
            get { return _Bank1Name; }
            set { _Bank1Name = value; }
        }

        string _Bank1BankNo;
        public string Bank1BankNo
        {
            get { return _Bank1BankNo; }
            set { _Bank1BankNo = value; }
        }

        string _Bank1AccNo;
        public string Bank1AccNo
        {
            get { return _Bank1AccNo; }
            set { _Bank1AccNo = value; }
        }

        string _Bank1IBAN;
        public string Bank1IBAN
        {
            get { return _Bank1IBAN; }
            set { _Bank1IBAN = value; }
        }

        string _Bank1Curr;
        public string Bank1Curr
        {
            get { return _Bank1Curr; }
            set { _Bank1Curr = value; }
        }
    }

    public class Proforma_CustPrice
    {
        public Proforma_CustPrice()
        {
        }
        int? _CustNo;
        public int? CustNo { get { return _CustNo; } set { _CustNo = value; } }
        string _Surname;
        public string Surname { get { return _Surname; } set { _Surname = value; } }
        string _Name;
        public string Name { get { return _Name; } set { _Name = value; } }
        string _Leader;
        public string Leader { get { return _Leader; } set { _Leader = value; } }
        decimal? _Price;
        public decimal? Price { get { return _Price; } set { _Price = value; } }
    }

    public class Proforma_OutOfPackeageService
    {
        public Proforma_OutOfPackeageService()
        {
        }
        string _ServiceDesc;
        public string ServiceDesc { get { return _ServiceDesc; } set { _ServiceDesc = value; } }
        Int16? _Unit;
        public Int16? Unit { get { return _Unit; } set { _Unit = value; } }
        decimal? _UnitPrice;
        public decimal? UnitPrice { get { return _UnitPrice; } set { _UnitPrice = value; } }
        decimal? _SalePrice;
        public decimal? SalePrice { get { return _SalePrice; } set { _SalePrice = value; } }
        string _SaleCur;
        public string SaleCur { get { return _SaleCur; } set { _SaleCur = value; } }
        string _PayCom;
        public string PayCom { get { return _PayCom; } set { _PayCom = value; } }
    }

    public class Proforma_Flights
    {
        public Proforma_Flights()
        {
        }
        DateTime? _FlyDate;
        public DateTime? FlyDate { get { return _FlyDate; } set { _FlyDate = value; } }
        DateTime? _DepTime;
        public DateTime? DepTime { get { return _DepTime; } set { _DepTime = value; } }
        string _Service;
        public string Service { get { return _Service; } set { _Service = value; } }
        Int16? _StatConf;
        public Int16? StatConf { get { return _StatConf; } set { _StatConf = value; } }
        string _StatConfig;
        public string StatConfig { get { return _StatConfig; } set { _StatConfig = value; } }
        string _AirPorts;
        public string AirPorts { get { return _AirPorts; } set { _AirPorts = value; } }
    }

    public class Proforma_Hotels
    {
        public Proforma_Hotels()
        {
        }

        string _HotelName;
        public string HotelName { get { return _HotelName; } set { _HotelName = value; } }
        string _Board;
        public string Board { get { return _Board; } set { _Board = value; } }
        string _HotelResort;
        public string HotelResort { get { return _HotelResort; } set { _HotelResort = value; } }
        string _Room;
        public string Room { get { return _Room; } set { _Room = value; } }
        DateTime? _BegDate;
        public DateTime? BegDate { get { return _BegDate; } set { _BegDate = value; } }
        Int16? _Unit;
        public Int16? Unit { get { return _Unit; } set { _Unit = value; } }
        Int16? _Duration;
        public Int16? Duration { get { return _Duration; } set { _Duration = value; } }
        Int16? _People;
        public Int16? People { get { return _People; } set { _People = value; } }
        string _StatConfig;
        public string StatConfig { get { return _StatConfig; } set { _StatConfig = value; } }
    }

    public class FlightTicketRecord
    {
        public FlightTicketRecord()
        {
        }
        string _ResNo; public string ResNo { get { return _ResNo; } set { _ResNo = value; } }
        int _CustNo; public int CustNo { get { return _CustNo; } set { _CustNo = value; } }
        string _Title; public string Title { get { return _Title; } set { _Title = value; } }
        string _Surname; public string Surname { get { return _Surname; } set { _Surname = value; } }
        string _Name; public string Name { get { return _Name; } set { _Name = value; } }
        int _RecID; public int RecID { get { return _RecID; } set { _RecID = value; } }
        Int16 _SeqNo; public Int16 SeqNo { get { return _SeqNo; } set { _SeqNo = value; } }
        string _Service; public string Service { get { return _Service; } set { _Service = value; } }
        DateTime _BegDate; public DateTime BegDate { get { return _BegDate; } set { _BegDate = value; } }
        string _FlgClass; public string FlgClass { get { return _FlgClass; } set { _FlgClass = value; } }
        string _FlightNo; public string FlightNo { get { return _FlightNo; } set { _FlightNo = value; } }
        string _Airline; public string Airline { get { return _Airline; } set { _Airline = value; } }
        DateTime? _LastCInTime; public DateTime? LastCInTime { get { return _LastCInTime; } set { _LastCInTime = value; } }
        DateTime? _DepDate; public DateTime? DepDate { get { return _DepDate; } set { _DepDate = value; } }
        DateTime? _DepTime; public DateTime? DepTime { get { return _DepTime; } set { _DepTime = value; } }
        DateTime? _ArrDate; public DateTime? ArrDate { get { return _ArrDate; } set { _ArrDate = value; } }
        DateTime? _ArrTime; public DateTime? ArrTime { get { return _ArrTime; } set { _ArrTime = value; } }
        string _Stat; public string Stat { get { return _Stat; } set { _Stat = value; } }
        decimal? _BagWeight; public decimal? BagWeight { get { return _BagWeight; } set { _BagWeight = value; } }
        string _FlightNoStr; public string FlightNoStr { get { return _FlightNoStr; } set { _FlightNoStr = value; } }
        string _AirPorts; public string AirPorts { get { return _AirPorts; } set { _AirPorts = value; } }
        string _DepAirPort; public string DepAirPort { get { return _DepAirPort; } set { _DepAirPort = value; } }
        string _DepAirPortCode; public string DepAirPortCode { get { return _DepAirPortCode; } set { _DepAirPortCode = value; } }
        string _DepAirPortSName; public string DepAirPortSName { get { return _DepAirPortSName; } set { _DepAirPortSName = value; } }
        string _DepLocation; public string DepLocation { get { return _DepLocation; } set { _DepLocation = value; } }
        string _ArrAirPort; public string ArrAirPort { get { return _ArrAirPort; } set { _ArrAirPort = value; } }
        string _ArrAirPortCode; public string ArrAirPortCode { get { return _ArrAirPortCode; } set { _ArrAirPortCode = value; } }
        string _ArrAirPortSName; public string ArrAirPortSName { get { return _ArrAirPortSName; } set { _ArrAirPortSName = value; } }
        string _ArrLocation; public string ArrLocation { get { return _ArrLocation; } set { _ArrLocation = value; } }
        string _Operator; public string Operator { get { return _Operator; } set { _Operator = value; } }
        DateTime _ResDate; public DateTime ResDate { get { return _ResDate; } set { _ResDate = value; } }
        string _OPName; public string OPName { get { return _OPName; } set { _OPName = value; } }
        string _Address; public string Address { get { return _Address; } set { _Address = value; } }
        string _AddrZip; public string AddrZip { get { return _AddrZip; } set { _AddrZip = value; } }
        string _AddrCity; public string AddrCity { get { return _AddrCity; } set { _AddrCity = value; } }
        string _Carier; public string Carier { get { return _Carier; } set { _Carier = value; } }
        string _PnrNo; public string PnrNo { get { return _PnrNo; } set { _PnrNo = value; } }
        string _FlightClassShortName; public string FlightClassShortName { get { return _FlightClassShortName; } set { _FlightClassShortName = value; } }
        string _FlightType; public string FlightType { get { return _FlightType; } set { _FlightType = value; } }
        string _AirLineCompany; public string AirLineCompany { get { return _AirLineCompany; } set { _AirLineCompany = value; } }
        string _CarrierCode; public string CarrierCode { get { return _CarrierCode; } set { _CarrierCode = value; } }


    }

    public class InvServicePrice
    {
        public InvServicePrice()
        {
        }

        string _ServiceDesc;
        public string ServiceDesc
        {
            get { return _ServiceDesc; }
            set { _ServiceDesc = value; }
        }

        Int16? _Unit;
        public Int16? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        decimal? _UnitPrice;
        public decimal? UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }

        string _SaleCur;
        public string SaleCur
        {
            get { return _SaleCur; }
            set { _SaleCur = value; }
        }

        bool _PayCom;
        public bool PayCom
        {
            get { return _PayCom; }
            set { _PayCom = value; }
        }
    }

    public class ResService
    {
        public ResService()
        {
        }
        string _Name; public string Name { get { return _Name; } set { _Name = value; } }
        Int16 _Adult; public Int16 Adult { get { return _Adult; } set { _Adult = value; } }
        Int16 _Child; public Int16 Child { get { return _Child; } set { _Child = value; } }
        decimal _SalePrice; public decimal SalePrice { get { return _SalePrice; } set { _SalePrice = value; } }
        string _SaleCur; public string SaleCur { get { return _SaleCur; } set { _SaleCur = value; } }
        Int16 _Unit; public Int16 Unit { get { return _Unit; } set { _Unit = value; } }
        string _Description; public string Description { get { return _Description; } set { _Description = value; } }
        Int16 _Step; public Int16 Step { get { return _Step; } set { _Step = value; } }
        Int16 _StatConf; public Int16 StatConf { get { return _StatConf; } set { _StatConf = value; } }
    }

    public class CustomerPrice
    {
        public CustomerPrice()
        {
        }
        int _IDNo;
        public int IDNo { get { return _IDNo; } set { _IDNo = value; } }
        int _CustNo;
        public int CustNo { get { return _CustNo; } set { _CustNo = value; } }
        decimal _Price;
        public decimal Price { get { return _Price; } set { _Price = value; } }
    }

    public class ContractCustomerPrice
    {
        public ContractCustomerPrice()
        {
        }

        int _CustNo;
        public int CustNo
        {
            get { return _CustNo; }
            set { _CustNo = value; }
        }

        int _ServiceID;
        public int ServiceID
        {
            get { return _ServiceID; }
            set { _ServiceID = value; }
        }

        int _ExtServiceID;
        public int ExtServiceID
        {
            get { return _ExtServiceID; }
            set { _ExtServiceID = value; }
        }

        string _ServiceCode;
        public string ServiceCode
        {
            get { return _ServiceCode; }
            set { _ServiceCode = value; }
        }

        decimal _Price;
        public decimal Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        string _ServiceDesc;
        public string ServiceDesc
        {
            get { return _ServiceDesc; }
            set { _ServiceDesc = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

    }

    public class Voucher_Info
    {
        public Voucher_Info()
        {
        }

        string _ResNo; public string ResNo { get { return _ResNo; } set { _ResNo = value; } }
        string _RefNo; public string RefNo { get { return _RefNo; } set { _RefNo = value; } }
        int? _Country; public int? Country { get { return _Country; } set { _Country = value; } }
        string _CountryName; public string CountryName { get { return _CountryName; } set { _CountryName = value; } }
        int? _ArrCity; public int? ArrCity { get { return _ArrCity; } set { _ArrCity = value; } }
        string _ArrCityName; public string ArrCityName { get { return _ArrCityName; } set { _ArrCityName = value; } }
        int? _DepCity; public int? DepCity { get { return _DepCity; } set { _DepCity = value; } }
        string _DepcityName; public string DepcityName { get { return _DepcityName; } set { _DepcityName = value; } }
        int? _HotelLocation; public int? HotelLocation { get { return _HotelLocation; } set { _HotelLocation = value; } }
        string _HotelLoc; public string HotelLoc { get { return _HotelLoc; } set { _HotelLoc = value; } }
        string _HotelName; public string HotelName { get { return _HotelName; } set { _HotelName = value; } }
        string _HotelLocalName; public string HotelLocalName { get { return _HotelLocalName; } set { _HotelLocalName = value; } }
        string _HotelCat; public string HotelCat { get { return _HotelCat; } set { _HotelCat = value; } }
        string _Accom; public string Accom { get { return _Accom; } set { _Accom = value; } }
        DateTime? _BegDate; public DateTime? BegDate { get { return _BegDate; } set { _BegDate = value; } }
        DateTime? _EndDate; public DateTime? EndDate { get { return _EndDate; } set { _EndDate = value; } }
        Int16? _Night; public Int16? Night { get { return _Night; } set { _Night = value; } }
        string _Board; public string Board { get { return _Board; } set { _Board = value; } }
        string _Remarks; public string Remarks { get { return _Remarks; } set { _Remarks = value; } }
        string _AgencyName; public string AgencyName { get { return _AgencyName; } set { _AgencyName = value; } }
        string _AgencyAddress; public string AgencyAddress { get { return _AgencyAddress; } set { _AgencyAddress = value; } }
        string _AgencyPhone; public string AgencyPhone { get { return _AgencyPhone; } set { _AgencyPhone = value; } }
        string _Partners; public string Partners { get { return _Partners; } set { _Partners = value; } }
        string _PartnersAddress; public string PartnersAddress { get { return _PartnersAddress; } set { _PartnersAddress = value; } }
        string _PartnersPhone; public string PartnersPhone { get { return _PartnersPhone; } set { _PartnersPhone = value; } }
        string _PartnersFax; public string PartnersFax { get { return _PartnersFax; } set { _PartnersFax = value; } }
        Int16 _Adult; public Int16 Adult { get { return _Adult; } set { _Adult = value; } }
        Int16 _Child; public Int16 Child { get { return _Child; } set { _Child = value; } }
    }

    public class formDataAgreement
    {
        public formDataAgreement()
        {
        }

        string _retVal;
        public string retVal
        {
            get { return _retVal; }
            set { _retVal = value; }
        }

        string _retType;
        public string retType
        {
            get { return _retType; }
            set { _retType = value; }
        }
    }

}
