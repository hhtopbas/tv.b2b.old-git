﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvReport.StandartReport
{
    public class PasPriceRecord
    {
        public PasPriceRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int16? _RecType;
        public Int16? RecType
        {
            get { return _RecType; }
            set { _RecType = value; }
        }

        string _IncPack;
        public string IncPack
        {
            get { return _IncPack; }
            set { _IncPack = value; }
        }

        Int16? _Unit;
        public Int16? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _ServiceDesc;
        public string ServiceDesc
        {
            get { return _ServiceDesc; }
            set { _ServiceDesc = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }
    }

    public class PaxCountRecord
    {
        public PaxCountRecord()
        { 
        }

        Int16? _AdlCnt;
        public Int16? AdlCnt
        {
            get { return _AdlCnt; }
            set { _AdlCnt = value; }
        }

        Int16? _ChdCnt;
        public Int16? ChdCnt
        {
            get { return _ChdCnt; }
            set { _ChdCnt = value; }
        }

        Int16? _InfCnt;
        public Int16? InfCnt
        {
            get { return _InfCnt; }
            set { _InfCnt = value; }
        }
    }

    public class ETicketHotelRecord
    { 
        public ETicketHotelRecord() 
        {}

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Location;
        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
    }
}
