﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using PDFBuilder;

namespace TvReport
{
    public class AllOperator
    {
        public List<int?> getDocumentLang(string Market, Int16? docTypeID, ref string errorMsg)
        {
            string tsql = @"Exec dbo.usp_GetDocumentLangID @Market,@DocType";
            List<int?> Result = new List<int?>();
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "DocType", DbType.String, docTypeID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        Result.Add(Conversion.getInt32OrNull(R["LangID"]));
                    }
                    return Result;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public bool generateReportPDF(int? RecID, ref string errorMsg)
        {
            string tsql = @"Declare @ResNo VarChar(10), @DocType smallint
                            Select @ResNo=ResNo, @DocType=DocType From DocReportTmp (NOLOCK)
                            Where Status=0 And RecID=@ID
                            if (@@RowCount>0)  
                                Exec dbo.Get_Document_Report @ResNo, @ID, @DocType  
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ID", DbType.String, RecID);
                db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return false;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public int? getTVReportID(string UserCode, string ResNo, Int16 DocType, ref string errorMsg)
        {
            string tsql = @"Insert Into DocReportTmp (ResNo, DocType, Status, CrtDate, CrtUser) 
                            Values(@ResNo, @DocType, 0, GetDate(), @CrtUser)
                            Select SCOPE_IDENTITY() ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "DocType", DbType.Int16, DocType);
                db.AddInParameter(dbCommand, "CrtUser", DbType.String, UserCode);
                return Conversion.getInt32OrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public byte[] getLogo(string fieldName, string tableName, string whereName, string code, int? recID, ref string errorMsg)
        {
            object logoObject = getLogoObject(fieldName, tableName, whereName, code, recID, ref errorMsg);
            byte[] image;
            if (logoObject == null)
            {
                string emptyPic = "255|216|255|224|0|16|74|70|73|70|0|1|1|0|0|1|0|1|0|0|255|219|0|67|0|3|2|2|3|2|2|3|3|3|3|4|3|3|4|5|8|5|5|4|4|5|10|7|7|6|8|12|10|12|12|11|10|11|11|13|14|18|16|13|14|17|14|11|11|16|22|16|17|19|20|21|21|21|12|15|23|24|22|20|24|18|20|21|20|255|219|0|67|1|3|4|4|5|4|5|9|5|5|9|20|13|11|13|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|255|192|0|17|8|0|5|0|5|3|1|34|0|2|17|1|3|17|1|255|196|0|31|0|0|1|5|1|1|1|1|1|1|0|0|0|0|0|0|0|0|1|2|3|4|5|6|7|8|9|10|11|255|196|0|181|16|0|2|1|3|3|2|4|3|5|5|4|4|0|0|1|125|1|2|3|0|4|17|5|18|33|49|65|6|19|81|97|7|34|113|20|50|129|145|161|8|35|66|177|193|21|82|209|240|36|51|98|114|130|9|10|22|23|24|25|26|37|38|39|40|41|42|52|53|54|55|56|57|58|67|68|69|70|71|72|73|74|83|84|85|86|87|88|89|90|99|100|101|102|103|104|105|106|115|116|117|118|119|120|121|122|131|132|133|134|135|136|137|138|146|147|148|149|150|151|152|153|154|162|163|164|165|166|167|168|169|170|178|179|180|181|182|183|184|185|186|194|195|196|197|198|199|200|201|202|210|211|212|213|214|215|216|217|218|225|226|227|228|229|230|231|232|233|234|241|242|243|244|245|246|247|248|249|250|255|196|0|31|1|0|3|1|1|1|1|1|1|1|1|1|0|0|0|0|0|0|1|2|3|4|5|6|7|8|9|10|11|255|196|0|181|17|0|2|1|2|4|4|3|4|7|5|4|4|0|1|2|119|0|1|2|3|17|4|5|33|49|6|18|65|81|7|97|113|19|34|50|129|8|20|66|145|161|177|193|9|35|51|82|240|21|98|114|209|10|22|36|52|225|37|241|23|24|25|26|38|39|40|41|42|53|54|55|56|57|58|67|68|69|70|71|72|73|74|83|84|85|86|87|88|89|90|99|100|101|102|103|104|105|106|115|116|117|118|119|120|121|122|130|131|132|133|134|135|136|137|138|146|147|148|149|150|151|152|153|154|162|163|164|165|166|167|168|169|170|178|179|180|181|182|183|184|185|186|194|195|196|197|198|199|200|201|202|210|211|212|213|214|215|216|217|218|226|227|228|229|230|231|232|233|234|242|243|244|245|246|247|248|249|250|255|218|0|12|3|1|0|2|17|3|17|0|63|0|253|83|162|138|40|3|255|217";
                string[] tmpPic = emptyPic.Split('|');
                byte[] tmpByte = new byte[tmpPic.Length];
                for (int i = 0; i < tmpPic.Length; i++)
                {
                    tmpByte[i] = Convert.ToByte(tmpPic[i]);
                }
                image = tmpByte;
            }
            else
            {
                try
                {
                    image = (byte[])logoObject;
                }
                catch
                {
                    string emptyPic = "255|216|255|224|0|16|74|70|73|70|0|1|1|0|0|1|0|1|0|0|255|219|0|67|0|3|2|2|3|2|2|3|3|3|3|4|3|3|4|5|8|5|5|4|4|5|10|7|7|6|8|12|10|12|12|11|10|11|11|13|14|18|16|13|14|17|14|11|11|16|22|16|17|19|20|21|21|21|12|15|23|24|22|20|24|18|20|21|20|255|219|0|67|1|3|4|4|5|4|5|9|5|5|9|20|13|11|13|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|255|192|0|17|8|0|5|0|5|3|1|34|0|2|17|1|3|17|1|255|196|0|31|0|0|1|5|1|1|1|1|1|1|0|0|0|0|0|0|0|0|1|2|3|4|5|6|7|8|9|10|11|255|196|0|181|16|0|2|1|3|3|2|4|3|5|5|4|4|0|0|1|125|1|2|3|0|4|17|5|18|33|49|65|6|19|81|97|7|34|113|20|50|129|145|161|8|35|66|177|193|21|82|209|240|36|51|98|114|130|9|10|22|23|24|25|26|37|38|39|40|41|42|52|53|54|55|56|57|58|67|68|69|70|71|72|73|74|83|84|85|86|87|88|89|90|99|100|101|102|103|104|105|106|115|116|117|118|119|120|121|122|131|132|133|134|135|136|137|138|146|147|148|149|150|151|152|153|154|162|163|164|165|166|167|168|169|170|178|179|180|181|182|183|184|185|186|194|195|196|197|198|199|200|201|202|210|211|212|213|214|215|216|217|218|225|226|227|228|229|230|231|232|233|234|241|242|243|244|245|246|247|248|249|250|255|196|0|31|1|0|3|1|1|1|1|1|1|1|1|1|0|0|0|0|0|0|1|2|3|4|5|6|7|8|9|10|11|255|196|0|181|17|0|2|1|2|4|4|3|4|7|5|4|4|0|1|2|119|0|1|2|3|17|4|5|33|49|6|18|65|81|7|97|113|19|34|50|129|8|20|66|145|161|177|193|9|35|51|82|240|21|98|114|209|10|22|36|52|225|37|241|23|24|25|26|38|39|40|41|42|53|54|55|56|57|58|67|68|69|70|71|72|73|74|83|84|85|86|87|88|89|90|99|100|101|102|103|104|105|106|115|116|117|118|119|120|121|122|130|131|132|133|134|135|136|137|138|146|147|148|149|150|151|152|153|154|162|163|164|165|166|167|168|169|170|178|179|180|181|182|183|184|185|186|194|195|196|197|198|199|200|201|202|210|211|212|213|214|215|216|217|218|226|227|228|229|230|231|232|233|234|242|243|244|245|246|247|248|249|250|255|218|0|12|3|1|0|2|17|3|17|0|63|0|253|83|162|138|40|3|255|217";
                    string[] tmpPic = emptyPic.Split('|');
                    byte[] tmpByte = new byte[tmpPic.Length];
                    for (int i = 0; i < tmpPic.Length; i++)
                    {
                        tmpByte[i] = Convert.ToByte(tmpPic[i]);
                    }
                    image = tmpByte;
                }
            }
            return image;
        }

        public object getLogoObject(string fieldName, string tableName, string whereName, string code, int? recID, ref string errorMsg)
        {
            string tsql = string.Format("Select {0} From {1} (NOLOCK) Where {2}={3}",
                                    fieldName,
                                    tableName,
                                    whereName,
                                    recID.HasValue ? recID.Value.ToString() : "'" + code + "'");
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                object obj = db.ExecuteScalar(dbCommand);
                return obj;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string getAgencyUserPhone(string Agency, string User, ref string errorMsg)
        {
            string tsql = @"Declare @UserMobile VarChar(15), @OfficeTel VarChar(15), @MainAgencyTel VarChar(15), @MainOffice VarChar(10)
                            Select @UserMobile=isnull(Mobile, '') From AgencyUser (NOLOCK) Where Agency=@AgencyID And Code=@UserID
                            Select @OfficeTel=isnull(Phone1, ''), @MainOffice=MainOffice From Agency (NOLOCK) Where Code=@AgencyID
                            Select @MainAgencyTel=isnull(Phone1, '') From Agency (NOLOCK) Where Code=@MainOffice                            
                            Select Case When @UserMobile <> '' Then @UserMobile Else (Case When @OfficeTel <> '' Then @OfficeTel Else @MainAgencyTel End) End ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "AgencyID", DbType.String, Agency);
                db.AddInParameter(dbCommand, "UserID", DbType.String, User);
                return Conversion.getStrOrNull(db.ExecuteScalar(dbCommand));
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string createOfferHtml(string UserID, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Empty.pdf";
            string cssFileName = pdfFilePath + "offer.css";
            if (!File.Exists(fileName)) return "";
            string bfileName = UserID + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_.pdf";
            string afileName = tempFolder + UserID + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_.pdf";
            if (File.Exists(afileName))
                File.Delete(afileName);

            HtmlToPdfBuilder builder = new HtmlToPdfBuilder(PageSize.A4);
            HtmlPdfPage pdfPage = builder.AddPage();
            pdfPage.AppendHtml(data);
            builder.ImportStylesheet(cssFileName);

            byte[] file = builder.RenderPdf();
            File.WriteAllBytes(afileName, file);

            /*
            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Append, FileAccess.Write));
            HtmlToPdfBuilder builder = new HtmlToPdfBuilder(PageSize.A4);
            builder.ImportStylesheet(cssFileName);
            HtmlPdfPage lastPagePDF = builder.AddPage();            
            lastPagePDF.AppendHtml(data);

            byte[] file = builder.RenderPdf();
            PdfReader readLastPage = new PdfReader(file);
            doc.NewPage();
            PdfImportedPage pageLastPdf = pdfWriter.GetImportedPage(readLastPage, 1);
            PdfContentByte cb2 = pdfWriter.DirectContent;
            cb2.AddTemplate(pageLastPdf, 0, 0);
            cb2.ClosePath();
            doc.Close();
            reader.Close();
            */
            return bfileName;
        }

        public string createOffer(string UserID, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Empty.pdf";
            if (!File.Exists(fileName)) return "";
            string bfileName = UserID + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_.pdf";
            string afileName = tempFolder + UserID + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            string fontpath = System.Web.HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialItalic = BaseFont.CreateFont(fontpath + "\\Document\\ARIALI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialItalicBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            for (int i = 0; i < _data1.GroupBy(g => g.PageNo).Count(); i++)
            {
                List<Coordinate> _data = _data1.Where(w => w.PageNo == (i + 1)).ToList<Coordinate>();
                doc.NewPage();
                PdfImportedPage page;
                page = pdfWriter.GetImportedPage(reader, currentpage);
                PdfContentByte cb = pdfWriter.DirectContent;

                cb.AddTemplate(page, 0, 0);
                foreach (Coordinate row in _data.Where(w => w.Type != writeType.Image && w.Type != writeType.Field))
                    if (row.isShown)
                        TvReport.TvReportCommon.writeText(ref cb, row.value, row.Pic, row.x * 2.835f, row.y * 2.835f, row.W * 2.835f, row.H * 2.835f, row.LineWidth * 2.835f, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row.FontSize, row.bold, row.Underline, row.Color, row.Align, row.Type);
                cb.ClosePath();
                foreach (Coordinate row in _data.Where(w => w.Type == writeType.Image))
                    if (row.isShown)
                        TvReport.TvReportCommon.writeImage(ref doc, row.Pic, row.x * 2.835f, row.y * 2.835f, row.W * 2.835f, row.H * 2.835f);
                foreach (Coordinate row in _data.Where(w => w.Type == writeType.Field))
                    if (row.isShown)
                    {
                        row.x = row.x * 2.835f;
                        row.y = row.y * 2.835f;
                        row.W = row.W * 2.835f;
                        row.H = row.H * 2.835f;
                        row.LineWidth = row.LineWidth * 2.835f;

                        TvReport.TvReportCommon.writeField(ref cb, ref pdfWriter, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row);
                    }
                foreach (Coordinate row in _data.Where(w => w.Type == writeType.PdfTable))
                    if (row.isShown)
                    {
                        row.x = row.x * 2.835f;
                        row.y = row.y * 2.835f;
                        row.W = row.W * 2.835f;
                        row.H = row.H * 2.835f;
                        row.LineWidth = row.LineWidth * 2.835f;

                        TvReport.TvReportCommon.writePdfTable(ref cb, ref pdfWriter, ref doc, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row);
                    }
                foreach (Coordinate row in _data.Where(w => w.Type == writeType.AreaText))
                    if (row.isShown)
                        TvReport.TvReportCommon.writeAreaText(ref cb, row.value, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row.FontSize, row.x * 2.835f, row.y * 2.835f, row.W * 2.835f, row.H * 2.835f);

            }
            doc.Close();
            reader.Close();
            return bfileName;
        }

        public decimal getBonus(string ResNo)
        {
            string tsql = @"Select Sum(BonusAmount)
                            From
                            (
                              Select BonusAmount=isnull(AgencyBonusAmount, 0) + isnull(UserBonusAmount, 0) From ResMain where ResNo = @ResNo
                              Union All
                              Select BonusAmount=isnull(BonusAmount, 0) from ResCust where ResNo = @ResNo
                            ) X ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                object retVal = db.ExecuteScalar(dbCommand);
                return retVal != null ? Convert.ToDecimal(retVal) : Convert.ToDecimal(0);
            }
            catch (Exception Ex)
            {
                string errorMsg = Ex.Message;
                return Convert.ToDecimal(0);
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

    }
}
