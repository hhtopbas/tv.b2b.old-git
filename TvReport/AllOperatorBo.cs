﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvReport
{
    public class basketLangRec
    {
        public basketLangRec()
        {
        }

        string _Lang;
        public string Lang
        {
            get { return _Lang; }
            set { _Lang = value; }
        }

        string _Labels;
        public string Labels
        {
            get { return _Labels; }
            set { _Labels = value; }
        }

        string _Texts;
        public string Texts
        {
            get { return _Texts; }
            set { _Texts = value; }
        }
    }    
}
