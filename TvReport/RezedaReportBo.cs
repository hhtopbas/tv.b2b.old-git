﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvReport.RezedaReport
{
    public class AgencyDocAddressRecord
    {
        public AgencyDocAddressRecord()
        {
        }

        string _MainOffice;
        public string MainOffice
        {
            get { return _MainOffice; }
            set { _MainOffice = value; }
        }

        bool _UseMainCont;
        public bool UseMainCont
        {
            get { return _UseMainCont; }
            set { _UseMainCont = value; }
        }

        string _VocAddrType;
        public string VocAddrType
        {
            get { return _VocAddrType; }
            set { _VocAddrType = value; }
        }

        string _Code;
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _FirmName;
        public string FirmName
        {
            get { return _FirmName; }
            set { _FirmName = value; }
        }

        string _DAddress;
        public string DAddress
        {
            get { return _DAddress; }
            set { _DAddress = value; }
        }

        string _Address;
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        string _DAddrZip;
        public string DAddrZip
        {
            get { return _DAddrZip; }
            set { _DAddrZip = value; }
        }

        string _AddrZip;
        public string AddrZip
        {
            get { return _AddrZip; }
            set { _AddrZip = value; }
        }

        string _AddrCity;
        public string AddrCity
        {
            get { return _AddrCity; }
            set { _AddrCity = value; }
        }

        string _DAddrCity;
        public string DAddrCity
        {
            get { return _DAddrCity; }
            set { _DAddrCity = value; }
        }

        string _AddrCountry;
        public string AddrCountry
        {
            get { return _AddrCountry; }
            set { _AddrCountry = value; }
        }

        string _DAddrCountry;
        public string DAddrCountry
        {
            get { return _DAddrCountry; }
            set { _DAddrCountry = value; }
        }

        string _Phone1;
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        string _DPhone;
        public string DPhone
        {
            get { return _DPhone; }
            set { _DPhone = value; }
        }

        string _Fax1;
        public string Fax1
        {
            get { return _Fax1; }
            set { _Fax1 = value; }
        }

        string _DFax;
        public string DFax
        {
            get { return _DFax; }
            set { _DFax = value; }
        }

        string _EMail1;
        public string EMail1
        {
            get { return _EMail1; }
            set { _EMail1 = value; }
        }

        bool _DocPrtNoPay;
        public bool DocPrtNoPay
        {
            get { return _DocPrtNoPay; }
            set { _DocPrtNoPay = value; }
        }

        string _LocationName;
        public string LocationName
        {
            get { return _LocationName; }
            set { _LocationName = value; }
        }

        string _TaxAccNo;
        public string TaxAccNo
        {
            get { return _TaxAccNo; }
            set { _TaxAccNo = value; }
        }

        string _TaxOffice;
        public string TaxOffice
        {
            get { return _TaxOffice; }
            set { _TaxOffice = value; }
        }

        string _BossName;
        public string BossName
        {
            get { return _BossName; }
            set { _BossName = value; }
        }

        string _InvAddress;
        public string InvAddress
        {
            get { return _InvAddress; }
            set { _InvAddress = value; }
        }

        string _InvAddrZip;
        public string InvAddrZip
        {
            get { return _InvAddrZip; }
            set { _InvAddrZip = value; }
        }

        string _InvAddrCity;
        public string InvAddrCity
        {
            get { return _InvAddrCity; }
            set { _InvAddrCity = value; }
        }

        string _InvAddrCountry;
        public string InvAddrCountry
        {
            get { return _InvAddrCountry; }
            set { _InvAddrCountry = value; }
        }

        string _Bank1;
        public string Bank1
        {
            get { return _Bank1; }
            set { _Bank1 = value; }
        }

        string _Bank1Name;
        public string Bank1Name
        {
            get { return _Bank1Name; }
            set { _Bank1Name = value; }
        }

        string _Bank1BankNo;
        public string Bank1BankNo
        {
            get { return _Bank1BankNo; }
            set { _Bank1BankNo = value; }
        }

        string _Bank1AccNo;
        public string Bank1AccNo
        {
            get { return _Bank1AccNo; }
            set { _Bank1AccNo = value; }
        }

        string _Bank1IBAN;
        public string Bank1IBAN
        {
            get { return _Bank1IBAN; }
            set { _Bank1IBAN = value; }
        }

        string _Bank1Curr;
        public string Bank1Curr
        {
            get { return _Bank1Curr; }
            set { _Bank1Curr = value; }
        }
    }

    public class PasPriceRecord
    {
        public PasPriceRecord()
        {
        }

        int? _RecID;
        public int? RecID
        {
            get { return _RecID; }
            set { _RecID = value; }
        }

        Int16? _RecType;
        public Int16? RecType
        {
            get { return _RecType; }
            set { _RecType = value; }
        }

        string _IncPack;
        public string IncPack
        {
            get { return _IncPack; }
            set { _IncPack = value; }
        }

        Int16? _Unit;
        public Int16? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        string _ServiceDesc;
        public string ServiceDesc
        {
            get { return _ServiceDesc; }
            set { _ServiceDesc = value; }
        }

        decimal? _SalePrice;
        public decimal? SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }
    }

    public class PaxCountRecord
    {
        public PaxCountRecord()
        {
        }

        Int16? _AdlCnt;
        public Int16? AdlCnt
        {
            get { return _AdlCnt; }
            set { _AdlCnt = value; }
        }

        Int16? _ChdCnt;
        public Int16? ChdCnt
        {
            get { return _ChdCnt; }
            set { _ChdCnt = value; }
        }

        Int16? _InfCnt;
        public Int16? InfCnt
        {
            get { return _InfCnt; }
            set { _InfCnt = value; }
        }
    }

    public class ETicketHotelRecord
    {
        public ETicketHotelRecord()
        { }

        string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _Location;
        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
    }

    public class Payments
    {
        public Payments()
        { }

        string _ReceiptNo;
        public string ReceiptNo
        {
            get { return _ReceiptNo; }
            set { _ReceiptNo = value; }
        }

        DateTime? _ReceiptDate;
        public DateTime? ReceiptDate
        {
            get { return _ReceiptDate; }
            set { _ReceiptDate = value; }
        }

        string _InvoiceNo;
        public string InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        DateTime? _PayDate;
        public DateTime? PayDate
        {
            get { return _PayDate; }
            set { _PayDate = value; }
        }

        string _PayType;
        public string PayType
        {
            get { return _PayType; }
            set { _PayType = value; }
        }

        string _PayTypeName;
        public string PayTypeName
        {
            get { return _PayTypeName; }
            set { _PayTypeName = value; }
        }

        string _Locked;
        public string Locked
        {
            get { return _Locked; }
            set { _Locked = value; }
        }

        decimal? _PaidAmount;
        public decimal? PaidAmount
        {
            get { return _PaidAmount; }
            set { _PaidAmount = value; }
        }

        string _PaidCur;
        public string PaidCur
        {
            get { return _PaidCur; }
            set { _PaidCur = value; }
        }

        string _Reference;
        public string Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        int? _InstalNum;
        public int? InstalNum
        {
            get { return _InstalNum; }
            set { _InstalNum = value; }
        }

        string _CCNo;
        public string CCNo
        {
            get { return _CCNo; }
            set { _CCNo = value; }
        }
    }

    public class rezedaResService
    {
        public rezedaResService()
        {
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        string _Service;
        public string Service
        {
            get { return _Service; }
            set { _Service = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        string _AccomFullName;
        public string AccomFullName
        {
            get { return _AccomFullName; }
            set { _AccomFullName = value; }
        }

        string _AirLineName;
        public string AirLineName
        {
            get { return _AirLineName; }
            set { _AirLineName = value; }
        }

        string _ServiceDesc;
        public string ServiceDesc
        {
            get { return _ServiceDesc; }
            set { _ServiceDesc = value; }
        }

        int? _Unit;
        public int? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        int? _Adult;
        public int? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        int? _Child;
        public int? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

    }

    public class rezedaResServiceExt
    {
        public rezedaResServiceExt()
        {
        }

        string _ServiceType;
        public string ServiceType
        {
            get { return _ServiceType; }
            set { _ServiceType = value; }
        }

        string _ExtService;
        public string ExtService
        {
            get { return _ExtService; }
            set { _ExtService = value; }
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }       

        string _ServiceDesc;
        public string ServiceDesc
        {
            get { return _ServiceDesc; }
            set { _ServiceDesc = value; }
        }

        int? _Unit;
        public int? Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }

        int? _Adult;
        public int? Adult
        {
            get { return _Adult; }
            set { _Adult = value; }
        }

        int? _Child;
        public int? Child
        {
            get { return _Child; }
            set { _Child = value; }
        }

    }
}
