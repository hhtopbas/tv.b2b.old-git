﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using TvTools;
using System.Web;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Globalization;
using System.Threading;

namespace TvReport.SafReport
{
    public class SafReport
    {
        public List<ExcursionTextRecord> getExcursionText(string Market, string ResNo, ref string errorMsg)
        {
            List<ExcursionTextRecord> records = new List<ExcursionTextRecord>();
            string tsql = @"Select Rs.BegDate, Rs.EndDate, isnull(dbo.FindLocalName(E.NameLID, @Market), E.Name) EName, Et.TextT, Et.TextR, Et.TextH
                            From ResService Rs (NOLOCK)
                            Join Excursion E (NOLOCK) ON E.Code = Rs.Service
                            Join ExcursionText Et (NOLOCK) ON Et.Excursion = Rs.Service
                            Where ResNo = @ResNo And ServiceType = 'EXCURSION'";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        ExcursionTextRecord record = new ExcursionTextRecord();
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        record.EndDate = Conversion.getDateTimeOrNull(R["EndDate"]);
                        record.EName = Conversion.getStrOrNull(R["EName"]);
                        record.TextT = Conversion.getStrOrNull(R["TextT"]);
                        record.TextR = Conversion.getStrOrNull(R["TextR"]);
                        record.TextH = Conversion.getStrOrNull(R["TextH"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public VoucherRecord getVoucherHotels(string ResNo, int? RecID, ref string errorMsg)
        {
            string tsql = @"Select S.RecID,S.ResNo,S.SeqNo,S.ServiceType,Hotel=S.Service,
	                            HotelName=Case When Supplier='HTLSPRO' then dbo.WordSplit(NoteFromIncome,'HotelName:','Room:') else H.Name end,
	                            HotelName_CatLoc=H.Name+' ('+Category+'),'+L.Name,
	                            Category=Case When Supplier='HTLSPRO' then '' else L.Name end,  
	                            Location=Case When Supplier='HTLSPRO' then '' else L.Name end,
	                            HotelPhone=H.Phone1,HotelFax=H.Fax,HotelEmail=H.email,
	                            Country= Case When Supplier='HTLSPRO' then '' else (Select Name From Location Where Location.RecID=L.Country) end,  
	                            Checkin=S.BegDate,CheckOut=S.EndDate,Night=Duration,Unit,
	                            S.Room,RoomName=Case When Supplier='HTLSPRO' then dbo.WordSplit(NoteFromIncome,'Room:','Board:') else HR.Name end,
	                            Accom=Case When Supplier='HTLSPRO' then '' else ' '+Accom end,        
	                            AccomName=HA.Name,
	                            AccomFullName=	case when S.Adult>0 then Str(S.Adult,1)+' Adl' else '' end + case when S.Child>0 then '+'+Str(S.Child,1)+' Chd' else '' end,
	                            Board,BoardName=Case When Supplier='HTLSPRO' then dbo.WordSplit(NoteFromIncome,'Board:','Preferences:') else HB.Name end,
	                            S.Adult,S.Child,
	                            S.Supplier,SupplierName=Sup.Name, SupAddress=Sup.Address,SupPhone1=Sup.Phone1,SupPhone2=Sup.Phone2,SupFax=Sup.Fax1,Supemail1=Sup.email1,
	                            SupNote as SupplierNote,
	                            ResNote as ReservationNote,
	                            S.StatConf, HotelAddress=H.PostAddress,
	                            TT.AdlCnt,TT.ChdCnt,TT.InfCnt,HasEB=Case When  HotSPO like '%E%' then 1 else 0 end,
	                            HTLSPRONote=Case When Supplier='HTLSPRO' then 'Booking no:'+ SupNote else '' end    
                            From ResService S (NOLOCK)
                            Join ResMain R (NOLOCK) on R.ResNo=S.ResNo
                            Join Hotel H (NOLOCK) on H.Code=S.Service
                            Join Location L (NOLOCK) on L.RecID=H.Location
                            Join HotelRoom HR (NOLOCK) on HR.Hotel=S.Service and HR.Code=S.Room
                            Join HotelAccom HA (NOLOCK) on HA.Hotel=S.Service and HA.Room=S.Room and HA.Code=S.Accom
                            Join HotelBoard HB (NOLOCK) on HB.Hotel=S.Service and HB.Code=S.Board
                            Join Supplier Sup (NOLOCK) on Sup.Code=S.Supplier
                            Outer Apply (
	                            Select AdlCnt=Sum(Case When TitleType=0 then 1 else 0 end),
		                               ChdCnt=Sum(Case When TitleType=1 then 1 else 0 end),
		                               InfCnt=Sum(Case When TitleType=2 then 1 else 0 end)
	                            From 
	                            (
		                            Select TitleType=Case When Title in (8,9) then 2 When Title in (6,7) then 1 Else 0 end 
		                            From ResService xS (NOLOCK)
		                            Join ResCust xP (NOLOCK) on xP.ResNo=xS.ResNo
		                            Join ResCon xC (NOLOCK) on xC.ResNo=xS.ResNo and xC.CustNo=xP.CustNo and xC.ServiceID=xS.RecID
		                            Where xS.ResNo=S.ResNo and xS.RecID=S.RecID
	                            ) BWF
                            ) TT
                            Where S.ResNo=@ResNo and S.ServiceType='HOTEL' and S.StatSer<2 and S.RecId=@RecID
                            Order by S.BegDate,SeqNo
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "RecID", DbType.Int32, RecID);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {
                        VoucherRecord record = new VoucherRecord();
                        record.RecID = Conversion.getInt32OrNull(R["RecID"]);
                        record.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        record.SeqNo = Conversion.getInt16OrNull(R["SeqNo"]);
                        record.ServiceType = Conversion.getStrOrNull(R["ServiceType"]);
                        record.Hotel = Conversion.getStrOrNull(R["Hotel"]);
                        record.HotelName = Conversion.getStrOrNull(R["HotelName"]);
                        record.HotelNameCatLoc = Conversion.getStrOrNull(R["HotelName_CatLoc"]);
                        record.Category = Conversion.getStrOrNull(R["Category"]);
                        record.Location = Conversion.getStrOrNull(R["Location"]);
                        record.HotelPhone = Conversion.getStrOrNull(R["HotelPhone"]);
                        record.HotelFax = Conversion.getStrOrNull(R["HotelFax"]);
                        record.HotelEmail = Conversion.getStrOrNull(R["HotelEmail"]);
                        record.Country = Conversion.getStrOrNull(R["Country"]);
                        record.Checkin = Conversion.getDateTimeOrNull(R["Checkin"]);
                        record.CheckOut = Conversion.getDateTimeOrNull(R["CheckOut"]);
                        record.Night = Conversion.getInt16OrNull(R["Night"]);
                        record.Unit = Conversion.getInt16OrNull(R["Unit"]);
                        record.Room = Conversion.getStrOrNull(R["Room"]);
                        record.RoomName = Conversion.getStrOrNull(R["RoomName"]);
                        record.Accom = Conversion.getStrOrNull(R["Accom"]);
                        record.AccomName = Conversion.getStrOrNull(R["AccomName"]);
                        record.AccomFullName = Conversion.getStrOrNull(R["AccomFullName"]);
                        record.Board = Conversion.getStrOrNull(R["Board"]);
                        record.BoardName = Conversion.getStrOrNull(R["BoardName"]);
                        record.Adult = Conversion.getInt16OrNull(R["Adult"]);
                        record.Child = Conversion.getInt16OrNull(R["Child"]);
                        record.Supplier = Conversion.getStrOrNull(R["Supplier"]);
                        record.SupplierName = Conversion.getStrOrNull(R["SupplierName"]);
                        record.SupAddress = Conversion.getStrOrNull(R["SupAddress"]);
                        record.SupPhone1 = Conversion.getStrOrNull(R["SupPhone1"]);
                        record.SupPhone2 = Conversion.getStrOrNull(R["SupPhone2"]);
                        record.SupFax = Conversion.getStrOrNull(R["SupFax"]);
                        record.Supemail1 = Conversion.getStrOrNull(R["Supemail1"]);
                        record.SupplierNote = Conversion.getStrOrNull(R["SupplierNote"]);
                        record.ReservationNote = Conversion.getStrOrNull(R["ReservationNote"]);
                        record.StatConf = Conversion.getInt16OrNull(R["StatConf"]);
                        record.HotelAddress = Conversion.getStrOrNull(R["HotelAddress"]);
                        record.AdlCnt = Conversion.getInt32OrNull(R["AdlCnt"]);
                        record.ChdCnt = Conversion.getInt32OrNull(R["ChdCnt"]);
                        record.InfCnt = Conversion.getInt32OrNull(R["InfCnt"]);
                        record.HasEB = Conversion.getDecimalOrNull(R["HasEB"]);
                        record.HTLSPRONote = Conversion.getStrOrNull(R["HTLSPRONote"]);
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }

    public class MellatResponseMessage
    {
        public MellatResponseMessage()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string GetMellatResponseMessage(string code)
        {
            switch (code)
            {
                case "0": return "Transaction Approved";
                case "11": return "Invalid Card Number";
                case "12": return "No Sufficient Funds";
                case "13": return "Incorrect Pin";
                case "14": return "Allowable Number Of Pin Tries Exceeded";
                case "15": return "Card Not Effective";
                case "16": return "Exceeds Withdrawal Frequency Limit";
                case "17": return "Customer Cancellation";
                case "18": return "Expired Card";
                case "19": return "Exceeds Withdrawal Amount Limit";
                case "111": return "No Such Issuer";
                case "112": return "Card Switch Internal Error";
                case "113": return "Issuer Or Switch Is Inoperative";
                case "114": return "Transaction Not Permitted To Card Holder";
                case "21": return "Invalid Merchant";
                case "23": return "Security Violation";
                case "24": return "Invalid User Or Password";
                case "25": return "Invalid Amount";
                case "31": return "Invalid Response";
                case "32": return "Format Error";
                case "33": return "No Investment Account";
                case "34": return "System Internal Error";
                case "35": return "Invalid Business Date";
                case "41": return "Duplicate Order Id";
                case "42": return "Sale Transaction Not Found";
                case "43": return "Duplicate Verify";
                case "44": return "Verify Transaction Not Found";
                case "45": return "Transaction Has Been Settled";
                case "46": return "Transaction Has Not Been Settled";
                case "47": return "Settle Transaction Not Found";
                case "48": return "Transaction Has Been Reversed";
                case "49": return "Refund Transaction Not Found";
                case "412": return "Bill Digit Incorrect";
                case "413": return "Payment Digit Incorrect";
                case "414": return "Bill Organization Not Valid";
                case "415": return "Session Timeout";
                case "416": return "Data Access Exception";
                case "417": return "Payer Id Is Invalid";
                case "418": return "Customer Not Found";
                case "419": return "Try Count Exceeded";
                case "421": return "Invalid IP";
                case "51": return "Duplicate Transmission";
                case "54": return "Original Transaction Not Found";
                case "55": return "Invalid Transaction";
                case "61": return "Error In Settle";
                default: return "Unknown error";
            }
        }
    }
}