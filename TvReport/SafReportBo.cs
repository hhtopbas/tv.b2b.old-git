﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TvReport.SafReport
{
    public class ExcursionTextRecord
    { 
        public ExcursionTextRecord()
        {
        }

        DateTime? _BegDate;
        public DateTime? BegDate
        {
            get { return _BegDate; }
            set { _BegDate = value; }
        }

        DateTime? _EndDate;
        public DateTime? EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        string _EName;
        public string EName
        {
            get { return _EName; }
            set { _EName = value; }
        }

        string _TextT;
        public string TextT
        {
            get { return _TextT; }
            set { _TextT = value; }
        }

        string _TextR;
        public string TextR
        {
            get { return _TextR; }
            set { _TextR = value; }
        }

        string _TextH;
        public string TextH
        {
            get { return _TextH; }
            set { _TextH = value; }
        }

    }

    public class VoucherRecord
    { 
        public VoucherRecord()
        {
        }

        int? _RecID; public int? RecID { get { return _RecID; } set { _RecID = value; } }
        string _ResNo; public string ResNo { get { return _ResNo; } set { _ResNo = value; } }
        Int16? _SeqNo; public Int16? SeqNo { get { return _SeqNo; } set { _SeqNo = value; } }
        string _ServiceType; public string ServiceType { get { return _ServiceType; } set { _ServiceType = value; } }
        string _Hotel; public string Hotel { get { return _Hotel; } set { _Hotel = value; } }
        string _HotelName; public string HotelName { get { return _HotelName; } set { _HotelName = value; } }
        string _HotelNameCatLoc; public string HotelNameCatLoc { get { return _HotelNameCatLoc; } set { _HotelNameCatLoc = value; } }
        string _Category; public string Category { get { return _Category; } set { _Category = value; } }
        string _Location; public string Location { get { return _Location; } set { _Location = value; } }
        string _HotelPhone; public string HotelPhone { get { return _HotelPhone; } set { _HotelPhone = value; } }
        string _HotelFax; public string HotelFax { get { return _HotelFax; } set { _HotelFax = value; } }
        string _HotelEmail; public string HotelEmail { get { return _HotelEmail; } set { _HotelEmail = value; } }
        string _Country; public string Country { get { return _Country; } set { _Country = value; } }
        DateTime? _Checkin; public DateTime? Checkin { get { return _Checkin; } set { _Checkin = value; } }
        DateTime? _CheckOut; public DateTime? CheckOut { get { return _CheckOut; } set { _CheckOut = value; } }
        Int16? _Night; public Int16? Night { get { return _Night; } set { _Night = value; } }
        Int16? _Unit; public Int16? Unit { get { return _Unit; } set { _Unit = value; } }
        string _Room; public string Room { get { return _Room; } set { _Room = value; } }
        string _RoomName; public string RoomName { get { return _RoomName; } set { _RoomName = value; } }
        string _Accom; public string Accom { get { return _Accom; } set { _Accom = value; } }
        string _AccomName; public string AccomName { get { return _AccomName; } set { _AccomName = value; } }
        string _AccomFullName; public string AccomFullName { get { return _AccomFullName; } set { _AccomFullName = value; } }
        string _Board; public string Board { get { return _Board; } set { _Board = value; } }
        string _BoardName; public string BoardName { get { return _BoardName; } set { _BoardName = value; } }
        Int16? _Adult; public Int16? Adult { get { return _Adult; } set { _Adult = value; } }
        Int16? _Child; public Int16? Child { get { return _Child; } set { _Child = value; } }
        string _Supplier; public string Supplier { get { return _Supplier; } set { _Supplier = value; } }
        string _SupplierName; public string SupplierName { get { return _SupplierName; } set { _SupplierName = value; } }
        string _SupAddress; public string SupAddress { get { return _SupAddress; } set { _SupAddress = value; } }
        string _SupPhone1; public string SupPhone1 { get { return _SupPhone1; } set { _SupPhone1 = value; } }
        string _SupPhone2; public string SupPhone2 { get { return _SupPhone2; } set { _SupPhone2 = value; } }
        string _SupFax; public string SupFax { get { return _SupFax; } set { _SupFax = value; } }
        string _Supemail1; public string Supemail1 { get { return _Supemail1; } set { _Supemail1 = value; } }
        string _SupplierNote; public string SupplierNote { get { return _SupplierNote; } set { _SupplierNote = value; } }
        string _ReservationNote; public string ReservationNote { get { return _ReservationNote; } set { _ReservationNote = value; } }
        Int16? _StatConf; public Int16? StatConf { get { return _StatConf; } set { _StatConf = value; } }
        string _HotelAddress; public string HotelAddress { get { return _HotelAddress; } set { _HotelAddress = value; } }
        int? _AdlCnt; public int? AdlCnt { get { return _AdlCnt; } set { _AdlCnt = value; } }
        int? _ChdCnt; public int? ChdCnt { get { return _ChdCnt; } set { _ChdCnt = value; } }
        int? _InfCnt; public int? InfCnt { get { return _InfCnt; } set { _InfCnt = value; } }
        decimal? _HasEB; public decimal? HasEB { get { return _HasEB; } set { _HasEB = value; } }
        string _HTLSPRONote; public string HTLSPRONote { get { return _HTLSPRONote; } set { _HTLSPRONote = value; } }
    }
}
