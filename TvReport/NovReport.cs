﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Web;
using TvTools;
using PDFBuilder;
using System.Globalization;

namespace TvReport.NovReport
{
    public class NovReport
    {
        public decimal getBonus(string ResNo, ref string errorMsg)
        {
            string tsql = @"Select Sum(BonusAmount)
                            From
                            (
                              Select BonusAmount=isnull(AgencyBonusAmount, 0) + isnull(UserBonusAmount, 0) From ResMain where ResNo = @ResNo
                              Union All
                              Select BonusAmount=isnull(BonusAmount, 0) from ResCust where ResNo = @ResNo
                            ) X ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                object retVal = db.ExecuteScalar(dbCommand);
                return Conversion.getDecimalOrNull(retVal).HasValue ? Conversion.getDecimalOrNull(retVal).Value : 0;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return 0;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public AgencyDocAddressRecord getAgencyDocAdress(string Agency, ref string errorMsg)
        {
            #region Sql string
            string tsql = @"Declare @mainAgency VarChar(10), @UseMainCont VarChar(1), @tmpAgency VarChar(10)
                            Select @UseMainCont=UseMainCont, @mainAgency=isnull(MainOffice,'') From Agency (NOLOCK) Where Code=@Agency
                            if (@UseMainCont='Y' And @mainAgency <>'') 
                                Set @tmpAgency=@mainAgency
                            Else Set @tmpAgency=@Agency

                            Select A.MainOffice, A.UseMainCont, A.VocAddrType, A.Code, A.Name, A.FirmName,	                            
                                    DAddress = Case A.VocAddrType 
                                                    When 0 Then (Select [Address] From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select [DocAddress] From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select  [DocAddress] From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else ([DocAddress]) 
                                                                End)
                                                    When 9 Then VocAddr_Address
                                                Else ''
                                                End,
                                    Address = Case A.VocAddrType 
                                                    When 0 Then (Select [Address] From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select [Address] From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select  [Address] From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Address) 
                                                                End)
                                                    When 9 Then VocAddr_Address
                                                Else ''
                                                End,
                                    DAddrZip =  Case A.VocAddrType 
                                                    When 0 Then (Select AddrZip From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocAddrZip From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select DocAddrZip From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (DocAddrZip) 
                                                                End)
                                                    When 9 Then VocAddr_Zip
                                                Else ''
                                                End, 
                                    AddrZip =  Case A.VocAddrType 
                                                    When 0 Then (Select AddrZip From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select AddrZip From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select  AddrZip From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.AddrZip) 
                                                                End)
                                                    When 9 Then VocAddr_Zip
                                                Else ''
                                                End, 
                                    AddrCity = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCity From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select AddrCity From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select AddrCity From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (A.AddrCity) 
                                                                    End)
                                                        When 9 Then VocAddr_City
                                                    Else ''
                                                    End, 
                                    DAddrCity = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCity From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select DocAddrCity From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select DocAddrCity From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (DocAddrCity) 
                                                                    End)
                                                        When 9 Then VocAddr_City
                                                    Else ''
                                                    End, 
                                    AddrCountry = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCountry From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select AddrCountry From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select AddrCountry From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (A.AddrCountry) 
                                                                    End)
                                                        When 9 Then VocAddr_Country
                                                    Else ''
                                                    End,  
                                    DAddrCountry = Case A.VocAddrType 
                                                        When 0 Then (Select AddrCountry From Office (NOLOCK) Where Code = A.OprOffice)
                                                        When 1 Then (Select DocAddrCountry From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                        When 2 Then (Case When UseMainCont = 'Y' 
                                                                        Then (Select DocAddrCountry From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                        Else (DocAddrCountry) 
                                                                    End)
                                                        When 9 Then VocAddr_Country
                                                    Else ''
                                                    End, 
                                    Phone1 =  Case A.VocAddrType 
                                                    When 0 Then (Select Phone1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select Phone1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select Phone1 From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Phone1) 
                                                                End)
                                                    When 9 Then VocAddr_Phone1
                                                Else ''
                                                End, 
                                    DPhone =  Case A.VocAddrType 
                                                    When 0 Then (Select Phone1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocPhone From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select DocPhone From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (DocPhone) 
                                                                End)
                                                    When 9 Then VocAddr_Phone1
                                                Else ''
                                                End, 
                                    Fax1 =  Case A.VocAddrType 
                                                    When 0 Then (Select Fax1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select Fax1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select Fax1 From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Fax1) 
                                                                End)
                                                    When 9 Then VocAddr_Fax1
                                                Else ''
                                                End, 
                                    DFax =  Case A.VocAddrType 
                                                    When 0 Then (Select Fax1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select DocFax From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select DocFax From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (DocFax) 
                                                                End)
                                                    When 9 Then VocAddr_Fax1
                                                Else ''
                                                End, 
                                    EMail1 = Case A.VocAddrType 
                                                    When 0 Then (Select Email1 From Office (NOLOCK) Where Code = A.OprOffice)
                                                    When 1 Then (Select Email1 From Operator (NOLOCK) Where Code = (Select Operator From Office (NOLOCK) Where Code = A.OprOffice))
                                                    When 2 Then (Case When UseMainCont = 'Y' 
                                                                    Then (Select Email1 From Agency (NOLOCK) Where Code = A.MainOffice)
                                                                    Else (A.Email1) 
                                                                End)
                                                    When 9 Then VocAddr_Email1
                                                Else ''
                                                End,
                                    DocPrtNoPay,
                                    LocationName = (Select Name From Location (NOLOCK) Where RecID = A.Location),
                                    TaxAccNo, TaxOffice, BossName,
                                    A.InvAddress, 
                                    A.InvAddrZip, 
                                    A.InvAddrCity, 
                                    A.InvAddrCountry,
                                    Bank1, Bank1Name = isnull((Select isnull(dbo.FindLocalName(NameLID, O.Market), Name) From Bank (NOLOCK) Where Code = A.Bank1), ''),
                                    Bank1BankNo, Bank1AccNo, Bank1IBAN, Bank1Curr
                            From Agency A (NOLOCK) 
                            Join Office O (NOLOCK) ON O.Code = A.OprOffice
                            Where A.Code = @tmpAgency ";
            #endregion

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    AgencyDocAddressRecord record = new AgencyDocAddressRecord();
                    if (rdr.Read())
                    {
                        record.MainOffice = Conversion.getStrOrNull(rdr["MainOffice"]);
                        record.UseMainCont = Equals(rdr["UseMainCont"], "Y");
                        record.VocAddrType = Conversion.getStrOrNull(rdr["VocAddrType"]);
                        record.Code = Conversion.getStrOrNull(rdr["Code"]);
                        record.Name = Conversion.getStrOrNull(rdr["Name"]);
                        record.FirmName = Conversion.getStrOrNull(rdr["FirmName"]);
                        record.DAddress = Conversion.getStrOrNull(rdr["DAddress"]);
                        record.Address = Conversion.getStrOrNull(rdr["Address"]);
                        record.DAddrZip = Conversion.getStrOrNull(rdr["DAddrZip"]);
                        record.AddrZip = Conversion.getStrOrNull(rdr["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(rdr["AddrCity"]);
                        record.DAddrCity = Conversion.getStrOrNull(rdr["DAddrCity"]);
                        record.AddrCountry = Conversion.getStrOrNull(rdr["AddrCountry"]);
                        record.DAddrCountry = Conversion.getStrOrNull(rdr["DAddrCountry"]);
                        record.Phone1 = Conversion.getStrOrNull(rdr["Phone1"]);
                        record.DPhone = Conversion.getStrOrNull(rdr["DPhone"]);
                        record.Fax1 = Conversion.getStrOrNull(rdr["Fax1"]);
                        record.DFax = Conversion.getStrOrNull(rdr["DFax"]);
                        record.EMail1 = Conversion.getStrOrNull(rdr["EMail1"]);
                        record.DocPrtNoPay = Equals(rdr["DocPrtNoPay"], "Y");
                        record.LocationName = Conversion.getStrOrNull(rdr["LocationName"]);
                        record.TaxAccNo = Conversion.getStrOrNull(rdr["TaxAccNo"]);
                        record.TaxOffice = Conversion.getStrOrNull(rdr["TaxOffice"]);
                        record.BossName = Conversion.getStrOrNull(rdr["BossName"]);
                        record.InvAddress = Conversion.getStrOrNull(rdr["InvAddress"]);
                        record.InvAddrZip = Conversion.getStrOrNull(rdr["InvAddrZip"]);
                        record.InvAddrCity = Conversion.getStrOrNull(rdr["InvAddrCity"]);
                        record.InvAddrCountry = Conversion.getStrOrNull(rdr["InvAddrCountry"]);
                        record.Bank1 = Conversion.getStrOrNull(rdr["Bank1"]);
                        record.Bank1Name = Conversion.getStrOrNull(rdr["Bank1Name"]);
                        record.Bank1BankNo = Conversion.getStrOrNull(rdr["Bank1BankNo"]);
                        record.Bank1AccNo = Conversion.getStrOrNull(rdr["Bank1AccNo"]);
                        record.Bank1IBAN = Conversion.getStrOrNull(rdr["Bank1IBAN"]);
                        record.Bank1Curr = Conversion.getStrOrNull(rdr["Bank1Curr"]);
                        return record;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public byte[] getContractLogo(string Operator, string Holpack, ref bool imgHolpack, ref string errorMsg)
        {
            object logoObject = getContractLogoObject(Operator, Holpack, ref imgHolpack, ref errorMsg);
            byte[] image;
            if (logoObject == null)
            {
                string emptyPic = "255|216|255|224|0|16|74|70|73|70|0|1|1|0|0|1|0|1|0|0|255|219|0|67|0|3|2|2|3|2|2|3|3|3|3|4|3|3|4|5|8|5|5|4|4|5|10|7|7|6|8|12|10|12|12|11|10|11|11|13|14|18|16|13|14|17|14|11|11|16|22|16|17|19|20|21|21|21|12|15|23|24|22|20|24|18|20|21|20|255|219|0|67|1|3|4|4|5|4|5|9|5|5|9|20|13|11|13|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|255|192|0|17|8|0|5|0|5|3|1|34|0|2|17|1|3|17|1|255|196|0|31|0|0|1|5|1|1|1|1|1|1|0|0|0|0|0|0|0|0|1|2|3|4|5|6|7|8|9|10|11|255|196|0|181|16|0|2|1|3|3|2|4|3|5|5|4|4|0|0|1|125|1|2|3|0|4|17|5|18|33|49|65|6|19|81|97|7|34|113|20|50|129|145|161|8|35|66|177|193|21|82|209|240|36|51|98|114|130|9|10|22|23|24|25|26|37|38|39|40|41|42|52|53|54|55|56|57|58|67|68|69|70|71|72|73|74|83|84|85|86|87|88|89|90|99|100|101|102|103|104|105|106|115|116|117|118|119|120|121|122|131|132|133|134|135|136|137|138|146|147|148|149|150|151|152|153|154|162|163|164|165|166|167|168|169|170|178|179|180|181|182|183|184|185|186|194|195|196|197|198|199|200|201|202|210|211|212|213|214|215|216|217|218|225|226|227|228|229|230|231|232|233|234|241|242|243|244|245|246|247|248|249|250|255|196|0|31|1|0|3|1|1|1|1|1|1|1|1|1|0|0|0|0|0|0|1|2|3|4|5|6|7|8|9|10|11|255|196|0|181|17|0|2|1|2|4|4|3|4|7|5|4|4|0|1|2|119|0|1|2|3|17|4|5|33|49|6|18|65|81|7|97|113|19|34|50|129|8|20|66|145|161|177|193|9|35|51|82|240|21|98|114|209|10|22|36|52|225|37|241|23|24|25|26|38|39|40|41|42|53|54|55|56|57|58|67|68|69|70|71|72|73|74|83|84|85|86|87|88|89|90|99|100|101|102|103|104|105|106|115|116|117|118|119|120|121|122|130|131|132|133|134|135|136|137|138|146|147|148|149|150|151|152|153|154|162|163|164|165|166|167|168|169|170|178|179|180|181|182|183|184|185|186|194|195|196|197|198|199|200|201|202|210|211|212|213|214|215|216|217|218|226|227|228|229|230|231|232|233|234|242|243|244|245|246|247|248|249|250|255|218|0|12|3|1|0|2|17|3|17|0|63|0|253|83|162|138|40|3|255|217";
                string[] tmpPic = emptyPic.Split('|');
                byte[] tmpByte = new byte[tmpPic.Length];
                for (int i = 0; i < tmpPic.Length; i++)
                {
                    tmpByte[i] = Convert.ToByte(tmpPic[i]);
                }
                image = tmpByte;
            }
            else
                image = (byte[])logoObject;

            return image;
        }

        public object getContractLogoObject(string Operator, string Holpack, ref bool imgHolpack, ref string errorMsg)
        {
            string tsql = @"if Exists(Select Code From Holpack Where Code=@HolPack And Logo is not null)
	                            Select Logo, imgHolPack=Cast(1 AS bit) From Holpack (NOLOCK)
	                            Where Code = @HolPack 
	                              And Logo is not null
                            Else
	                            Select Logo, imgHolPack=Cast(0 AS bit) From Operator (NOLOCK)
	                            Where Code = @Operator ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Operator", DbType.String, Operator);
                db.AddInParameter(dbCommand, "HolPack", DbType.String, Holpack);
                using (IDataReader rdr = db.ExecuteReader(dbCommand))
                {
                    if (rdr.Read())
                    {
                        object logo = rdr["Logo"];
                        object imgHlpk = rdr["imgHolPack"];
                        imgHolpack = (bool)imgHlpk;
                        return logo;
                    }
                    else return null;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string createFlyTicket(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "ETicket.pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_ETicket.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));

            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialItalic = BaseFont.CreateFont(fontpath + "\\Document\\ARIALI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialItalicBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _dataAll = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);
            List<Coordinate> _data = _dataAll.Where(w => w.PageNo == 1).ToList<Coordinate>();

            foreach (Coordinate row in _data.Where(w => w.Type != writeType.Image && w.Type != writeType.Field))
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.Pic, row.x * 2.835f, row.y * 2.835f, row.W * 2.835f, row.H * 2.835f, row.LineWidth * 2.835f, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row.FontSize, row.bold, row.Underline, row.Color, row.Align, row.Type);
            cb.ClosePath();
            foreach (Coordinate row in _data.Where(w => w.Type == writeType.Image))
                if (row.isShown)
                    TvReport.TvReportCommon.writeImage(ref doc, row.Pic, row.x * 2.835f, row.y * 2.835f, row.W * 2.835f, row.H * 2.835f);
            foreach (Coordinate row in _data.Where(w => w.Type == writeType.Field))
                if (row.isShown)
                {
                    row.H = row.H * 2.835f;
                    row.LineWidth = row.LineWidth * 2.835f;
                    row.W = row.W * 2.835f;
                    row.x = row.x * 2.835f;
                    row.y = row.y * 2.835f;
                    TvReport.TvReportCommon.writeField(ref cb, ref pdfWriter, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row);
                }
            foreach (Coordinate row in _data.Where(w => w.Type == writeType.PdfTable))
                if (row.isShown)
                {
                    row.H = row.H * 2.835f;
                    row.LineWidth = row.LineWidth * 2.835f;
                    row.W = row.W * 2.835f;
                    row.x = row.x * 2.835f;
                    row.y = row.y * 2.835f;
                    TvReport.TvReportCommon.writePdfTable(ref cb, ref pdfWriter, ref doc, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row);
                }
            foreach (Coordinate row in _data.Where(w => w.Type == writeType.AreaText))
                if (row.isShown)
                    TvReport.TvReportCommon.writeAreaText(ref cb, row.value, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row.FontSize, row.x * 2.835f, row.y * 2.835f, row.W * 2.835f, row.H * 2.835f);

            List<Coordinate> _dataHtml = _dataAll.Where(w => w.PageNo == 2 && w.Type == writeType.Html).ToList<Coordinate>();
            if (_dataHtml.Count > 0) doc.NewPage();
            foreach (Coordinate row in _dataHtml)
            {
                if (row.isShown)
                {
                    row.H = row.H * 2.835f;
                    row.LineWidth = row.LineWidth * 2.835f;
                    row.W = row.W * 2.835f;
                    row.x = row.x * 2.835f;
                    row.y = row.y * 2.835f;
                    TvReport.TvReportCommon.writeHtml(ref doc, ref cb, row, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial));
                }
            }
            doc.Close();
            reader.Close();
            return ResNo + "_ETicket.pdf";
        }

        public object getAirlineLogo(string Airline, bool isEmpty, ref string errorMsg)
        {
            object logoObject = getAirlineLogoObject(Airline, ref errorMsg);
            byte[] image;
            if (logoObject.GetType().Name == "DBNull")
            {
                if (isEmpty) return null;
                string emptyPic = "255|216|255|224|0|16|74|70|73|70|0|1|1|0|0|1|0|1|0|0|255|219|0|67|0|3|2|2|3|2|2|3|3|3|3|4|3|3|4|5|8|5|5|4|4|5|10|7|7|6|8|12|10|12|12|11|10|11|11|13|14|18|16|13|14|17|14|11|11|16|22|16|17|19|20|21|21|21|12|15|23|24|22|20|24|18|20|21|20|255|219|0|67|1|3|4|4|5|4|5|9|5|5|9|20|13|11|13|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|20|255|192|0|17|8|0|5|0|5|3|1|34|0|2|17|1|3|17|1|255|196|0|31|0|0|1|5|1|1|1|1|1|1|0|0|0|0|0|0|0|0|1|2|3|4|5|6|7|8|9|10|11|255|196|0|181|16|0|2|1|3|3|2|4|3|5|5|4|4|0|0|1|125|1|2|3|0|4|17|5|18|33|49|65|6|19|81|97|7|34|113|20|50|129|145|161|8|35|66|177|193|21|82|209|240|36|51|98|114|130|9|10|22|23|24|25|26|37|38|39|40|41|42|52|53|54|55|56|57|58|67|68|69|70|71|72|73|74|83|84|85|86|87|88|89|90|99|100|101|102|103|104|105|106|115|116|117|118|119|120|121|122|131|132|133|134|135|136|137|138|146|147|148|149|150|151|152|153|154|162|163|164|165|166|167|168|169|170|178|179|180|181|182|183|184|185|186|194|195|196|197|198|199|200|201|202|210|211|212|213|214|215|216|217|218|225|226|227|228|229|230|231|232|233|234|241|242|243|244|245|246|247|248|249|250|255|196|0|31|1|0|3|1|1|1|1|1|1|1|1|1|0|0|0|0|0|0|1|2|3|4|5|6|7|8|9|10|11|255|196|0|181|17|0|2|1|2|4|4|3|4|7|5|4|4|0|1|2|119|0|1|2|3|17|4|5|33|49|6|18|65|81|7|97|113|19|34|50|129|8|20|66|145|161|177|193|9|35|51|82|240|21|98|114|209|10|22|36|52|225|37|241|23|24|25|26|38|39|40|41|42|53|54|55|56|57|58|67|68|69|70|71|72|73|74|83|84|85|86|87|88|89|90|99|100|101|102|103|104|105|106|115|116|117|118|119|120|121|122|130|131|132|133|134|135|136|137|138|146|147|148|149|150|151|152|153|154|162|163|164|165|166|167|168|169|170|178|179|180|181|182|183|184|185|186|194|195|196|197|198|199|200|201|202|210|211|212|213|214|215|216|217|218|226|227|228|229|230|231|232|233|234|242|243|244|245|246|247|248|249|250|255|218|0|12|3|1|0|2|17|3|17|0|63|0|253|83|162|138|40|3|255|217";
                string[] tmpPic = emptyPic.Split('|');
                byte[] tmpByte = new byte[tmpPic.Length];
                for (int i = 0; i < tmpPic.Length; i++)
                {
                    tmpByte[i] = Convert.ToByte(tmpPic[i]);
                }
                image = tmpByte;
            }
            else
                image = (byte[])logoObject;

            return image;
        }

        public object getAirlineLogoObject(string Airline, ref string errorMsg)
        {
            string tsql = @"Select Logo From Airline (NOLOCK)
	                        Where Code=@Airline ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Airline", DbType.String, Airline);
                object obj = db.ExecuteScalar(dbCommand);
                return obj;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string createContract(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Empty.pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_Contract.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            pdfWriter.UserProperties = true;
            pdfWriter.CreateXmpMetadata();
            pdfWriter.ClearTextWrap();
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;
            doc.AddCreator("Tourvisio B2B web module");
            doc.AddCreationDate();
            doc.AddTitle("Contract");
            cb.AddTemplate(page, 0, 0);

            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialItalic = BaseFont.CreateFont(fontpath + "\\Document\\ARIALI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialItalicBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data.Where(w => w.Type != writeType.Image && w.Type != writeType.Field))
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.Pic, row.x, row.y, row.W, row.H, row.LineWidth, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row.FontSize, row.bold, row.Underline, row.Color, row.Align, row.Type);
            cb.ClosePath();
            foreach (Coordinate row in _data.Where(w => w.Type == writeType.Image))
                if (row.isShown)
                    TvReport.TvReportCommon.writeImage(ref doc, row.Pic, row.x, row.y, row.W, row.H);
            foreach (Coordinate row in _data.Where(w => w.Type == writeType.Field))
                if (row.isShown)
                    TvReport.TvReportCommon.writeField(ref cb, ref pdfWriter, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row);
            foreach (Coordinate row in _data.Where(w => w.Type == writeType.PdfTable))
                if (row.isShown)
                    TvReport.TvReportCommon.writePdfTable(ref cb, ref pdfWriter, ref doc, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row);
            foreach (Coordinate row in _data.Where(w => w.Type == writeType.AreaText))
                if (row.isShown)
                    TvReport.TvReportCommon.writeAreaText(ref cb, row.value, row.bold ? (row.Italic ? fontArialItalicBold : fontArialBold) : (row.Italic ? fontArialItalic : fontArial), row.FontSize, row.x, row.y, row.W, row.H);

            doc.Close();
            reader.Close();
            return ResNo + "_Contract.pdf";
        }

        public string mergeFlyTicket(string tempFolder, List<string> ticketList)
        {
            string aFileName = ticketList[0].ToString().Substring(0, ticketList[0].ToString().IndexOf('_')) + "_ETicket.pdf";
            if (File.Exists(tempFolder + aFileName))
                File.Delete(tempFolder + aFileName);
            string fileName = ticketList.FirstOrDefault().ToString().Substring(0, ticketList[0].ToString().IndexOf('_'));
            int currentpage = 1;
            Document doc = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(tempFolder + aFileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            for (int i = 0; i < ticketList.Count; i++)
            {
                PdfReader reader2 = new PdfReader(tempFolder + ticketList[i].ToString());
                byte[] pdfBytes2 = reader2.GetPageContent(currentpage);
                doc.NewPage();
                PdfImportedPage page = pdfWriter.GetImportedPage(reader2, currentpage);

                PdfContentByte cb2 = pdfWriter.DirectContent;
                cb2.AddTemplate(page, 0, 0);
                cb2.ClosePath();
                reader2.Close();
                //File.Delete(tempFolder + ticketList[i].ToString());
            }
            doc.Close();

            return aFileName;
        }

        public byte[] getContrat2Page(DataTable UserData, string Holpack, ref string errorMsg)
        {
            string tsql = string.Empty;
            tsql = @"Select ContractPict From HolPack (NOLOCK) Where Code = @Code";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, Holpack);
                object val = db.ExecuteScalar(dbCommand);
                if (val != null)
                    return (byte[])val;
                else return null;
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public string createVoucher(string ResNo, string pdfFilePath, string tempFolder, string data, ref string errorMsg)
        {
            string fileName = pdfFilePath + "Voucher.pdf";
            if (!File.Exists(fileName)) return "";
            string afileName = tempFolder + ResNo + "_Voucher.pdf";

            PdfReader reader = new PdfReader(fileName);
            int currentpage = 1;
            byte[] pdfBytes = reader.GetPageContent(currentpage);
            if (File.Exists(afileName))
                File.Delete(afileName);
            Document doc = new Document(reader.GetPageSize(1));
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, new FileStream(afileName, FileMode.Append, FileAccess.Write));
            doc.Open();
            byte[] pageContent = reader.GetPageContent(currentpage);
            doc.NewPage();
            PdfImportedPage page;
            page = pdfWriter.GetImportedPage(reader, currentpage);
            PdfContentByte cb = pdfWriter.DirectContent;

            cb.AddTemplate(page, 0, 0);
            string fontpath = HttpContext.Current.Server.MapPath(".");
            BaseFont fontArial = BaseFont.CreateFont(fontpath + "\\Document\\ARIAL.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont fontArialBold = BaseFont.CreateFont(fontpath + "\\Document\\ARIALBD.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            List<Coordinate> _data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Coordinate>>(data);

            foreach (Coordinate row in _data.Where(w => w.Type == writeType.Text))
                if (row.isShown)
                    TvReport.TvReportCommon.writeText(ref cb, row.value, row.x * 2.835f, row.y * 2.835f, row.bold ? fontArialBold : fontArial, row.FontSize);

            cb.ClosePath();

            foreach (Coordinate row in _data.Where(w => w.Type == writeType.Image))
                if (row.isShown)
                    TvReport.TvReportCommon.writeImage(ref doc, row.Pic, row.x * 2.835f, row.y * 2.835f, row.W * 2.835f, row.H * 2.835f);

            doc.Close();
            reader.Close();
            return ResNo + "_Voucher.pdf";
        }

        public List<TvReport.NovReport.FlightTicketRecord> getResTicketData(string ResNo, int? CustNo, string Agency, string UserID, string Market, ref string errorMsg)
        {
            List<TvReport.NovReport.FlightTicketRecord> records = new List<FlightTicketRecord>();
            string tsql = @"Select 
                                RCust.ResNo, RCust.CustNo, 
                                Title=(Select Code From Title (NOLOCK) Where TitleNo=RCust.Title), 
                                RCust.Surname,RCust.Name,RS.RecID,RCust.SeqNo,RS.Service,RS.BegDate,RS.FlgClass,FD.FlightNo,FD.Airline,
                                LastCInTime=(isnull(FD.TDepTime,FD.DepTime)-isnull(FD.CountCloseTime,0)), 
                                DepDate=isnull(FD.TDepDate,FD.FlyDate),ArrDate=isnull(FD.TArrDate,FD.FlyDate),
                                DepTime=isnull(FD.TDepTime,FD.DepTime),ArrTime=isnull(FD.TArrTime, FD.ArrTime), 
                                (CASE WHEN RS.StatConf = 0 THEN CASE WHEN RCust.Title < 8 THEN 'Request' ELSE 'NS' END	
                                      WHEN RS.StatConf = 1 THEN CASE WHEN RCust.Title < 8 THEN 'OK'  ELSE 'NS' END	
                                      WHEN RS.StatConf = 2 THEN CASE WHEN RCust.Title < 8 THEN 'Not Confirm' ELSE 'NS' END 
                                      WHEN RS.StatConf = 3 THEN CASE WHEN RCust.Title < 8 THEN 'No Show'  ELSE 'NS' END 
                                 END 
                                ) AS Stat, (CASE WHEN RCust.Title < 8 THEN FD.BagWeight ELSE FD.BagWeightForInf END) AS BagWeight,
                                FlightNoStr=FD.FlightNo + ' / ' + RS.FlgClass,                             
                                AirPorts=(Select AP.Name From AirPort (NOLOCK) AP Where AP.Code = FD.DepAirPort) + ' / '+ (Select AP.Name From AirPort (NOLOCK) AP Where AP.Code = FD.ArrAirPort),                             
                                DepAirPort=(Select AP.Name From AirPort (NOLOCK) AP Where AP.Code = FD.DepAirPort), 
                                DepAirPortCode=FD.DepAirPort,
                                DepAirPortSName=(Select AP.NameS From AirPort (NOLOCK) AP Where AP.Code = FD.DepAirPort), 
                                DepLocation=(Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = RS.DepLocation),
                                ArrAirPort=(Select AP.Name From AirPort (NOLOCK) AP Where AP.Code = FD.ArrAirPort), 
                                ArrAirPortCode=FD.ArrAirPort,
                                ArrAirPortSName=(Select AP.NameS From AirPort (NOLOCK) AP Where AP.Code = FD.ArrAirPort), 
                                ArrLocation = (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Location (NOLOCK) Where RecID = RS.ArrLocation),
                                RM.Operator, RM.ResDate, 
                                OP.Name AS OPName, OP.Address, OP.AddrZip, OP.AddrCity, F.Airline Carier, isnull(PnrNo, '') PnrNo,
                                FlightClassShortName = (Select NameS From FlightClass (NOLOCK) Where Class = RS.FlgClass),
                                FlightType = (Select Name From FlightType (NOLOCK) Where RecID = F.FlightType),
                                AirLineCompany = (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Airline Where Code = FD.Airline),
                                CarrierCode=isnull(A.CarCode, '')
                            From ResCust RCust (NOLOCK) 
                                JOIN ResCon (NOLOCK) RC on RCust.CustNo = RC.CustNo And RC.ResNo = RCust.ResNo 
                                JOIN ResService (NOLOCK) RS on RS.RecID = RC.ServiceID AND RS.ServiceType = 'FLIGHT' And RS.ResNo = RC.ResNo
                                LEFT OUTER JOIN FlightDay (NOLOCK) FD on FD.FlightNo = RS.Service AND FD.FlyDate = RS.BegDate 
                                LEFT JOIN Flight (NOLOCK) F ON F.Code = FD.FlightNo                            
                                LEFT JOIN Airline (NOLOCK) A ON A.Code = F.AirLine
                                LEFT JOIN ResMain (NOLOCK) RM on RM.ResNo = RCust.ResNo 
                                LEFT OUTER JOIN Operator (NOLOCK) OP on OP.Code = RM.Operator 
                            Where 
	                            RCust.ResNo = @ResNo And RCust.CustNo = @CustNo 
                            Order By RS.BegDate";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                //string ResNo, int? CustNo, string Agency, string UserID, string Market
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "CustNo", DbType.Int32, CustNo);
                db.AddInParameter(dbCommand, "Agency", DbType.String, Agency);
                db.AddInParameter(dbCommand, "UserID", DbType.String, UserID);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TvReport.NovReport.FlightTicketRecord record = new TvReport.NovReport.FlightTicketRecord();
                        record.ResNo = Conversion.getStrOrNull(R["ResNo"]);
                        record.CustNo = (int)R["CustNo"];
                        record.Title = Conversion.getStrOrNull(R["Title"]);
                        record.Surname = Conversion.getStrOrNull(R["Surname"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.RecID = (int)R["RecID"];
                        record.SeqNo = (Int16)R["SeqNo"];
                        record.Service = Conversion.getStrOrNull(R["Service"]);
                        record.BegDate = (DateTime)R["BegDate"];
                        record.FlgClass = Conversion.getStrOrNull(R["FlgClass"]);
                        record.FlightNo = Conversion.getStrOrNull(R["FlightNo"]);
                        record.Airline = Conversion.getStrOrNull(R["Airline"]);
                        record.LastCInTime = Conversion.getDateTimeOrNull(R["LastCInTime"]);
                        record.DepDate = Conversion.getDateTimeOrNull(R["DepDate"]);
                        record.DepTime = Conversion.getDateTimeOrNull(R["DepTime"]);
                        record.ArrDate = Conversion.getDateTimeOrNull(R["ArrDate"]);
                        record.ArrTime = Conversion.getDateTimeOrNull(R["ArrTime"]);
                        record.Stat = Conversion.getStrOrNull(R["Stat"]);
                        record.BagWeight = Conversion.getDecimalOrNull(R["BagWeight"]);
                        record.FlightNoStr = Conversion.getStrOrNull(R["FlightNoStr"]);
                        record.AirPorts = Conversion.getStrOrNull(R["AirPorts"]);
                        record.DepAirPort = Conversion.getStrOrNull(R["DepAirPort"]);
                        record.DepAirPortCode = Conversion.getStrOrNull(R["DepAirPortCode"]);
                        record.DepAirPortSName = Conversion.getStrOrNull(R["DepAirPortSName"]);
                        record.DepLocation = Conversion.getStrOrNull(R["DepLocation"]);
                        record.ArrAirPort = Conversion.getStrOrNull(R["ArrAirPort"]);
                        record.ArrAirPortCode = Conversion.getStrOrNull(R["ArrAirPortCode"]);
                        record.ArrAirPortSName = Conversion.getStrOrNull(R["ArrAirPortSName"]);
                        record.ArrLocation = Conversion.getStrOrNull(R["ArrLocation"]);
                        record.Operator = Conversion.getStrOrNull(R["Operator"]);
                        record.ResDate = (DateTime)R["ResDate"];
                        record.OPName = Conversion.getStrOrNull(R["OPName"]);
                        record.Address = Conversion.getStrOrNull(R["Address"]);
                        record.AddrZip = Conversion.getStrOrNull(R["AddrZip"]);
                        record.AddrCity = Conversion.getStrOrNull(R["AddrCity"]);
                        record.Carier = Conversion.getStrOrNull(R["Carier"]);
                        record.PnrNo = Conversion.getStrOrNull(R["PnrNo"]);
                        record.FlightClassShortName = Conversion.getStrOrNull(R["FlightClassShortName"]);
                        record.FlightType = Conversion.getStrOrNull(R["FlightType"]);
                        record.AirLineCompany = Conversion.getStrOrNull(R["AirLineCompany"]);
                        record.CarrierCode = Conversion.getStrOrNull(R["CarrierCode"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.NovReport.Proforma_CustPrice> getInvInPackServicePrice(string ResNo, ref string errorMsg)
        {
            List<TvReport.NovReport.Proforma_CustPrice> records = new List<Proforma_CustPrice>();
            string tsql = @"Select RC.CustNo, RC.Surname, RC.Name, RC.Leader, isnull(Sum(X.SalePrice), 0) As Price 
                            From ResCust RC (NOLOCK)
                            Left Join ( 
                                       Select C1.CustNo, SalePrice=SUM(C1.SalePrice) 
                                       From ResCustPrice C1 (NOLOCK) 
                                       Join ResService S1 On S1.RecID = C1.ServiceID  And C1.ExtserviceID=0 And isnull(IncPack,'N') = 'Y' 
                                       Where 
                                            C1.ResNo = @ResNo And (S1.StatSer in (0,1) Or (S1.StatSer = 3 And isnull(S1.SalePrice, 0) <> 0))
                                       Group By CustNo 
                                       Union All
                                       Select C2.CustNo, SalePrice=SUM(C2.SalePrice) 
                                       From ResCustPrice C2 (NOLOCK) 
                                       Join ResServiceExt S2 On S2.RecID = C2.ExtServiceID And (isnull(IncPack,'N') = 'Y') 
                                       Where 
                                            C2.ResNo = @ResNo And (S2.StatSer in (0,1) Or (S2.StatSer = 3 And isnull(S2.SalePrice, 0) <> 0))
                                       Group By CustNo 
                                      ) X ON RC.CustNo = X.CustNo 
                            Where RC.ResNo = @ResNo
                            Group by RC.CustNo, RC.Surname, RC.Name, RC.Leader";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TvReport.NovReport.Proforma_CustPrice record = new TvReport.NovReport.Proforma_CustPrice();
                        record.CustNo = Conversion.getInt32OrNull(R["CustNo"]);
                        record.Surname = Conversion.getStrOrNull(R["Surname"]);
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.Price = Conversion.getDecimalOrNull(R["Price"]);
                        record.Leader = Conversion.getStrOrNull(R["Leader"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.NovReport.InvServicePrice> getOutPackServicePrice(string ResNo, string Market, ref string errorMsg)
        {
            List<TvReport.NovReport.InvServicePrice> records = new List<InvServicePrice>();
            string tsql = @"Select ServiceDesc, Unit, (SalePrice / Unit) AS UnitPrice, SalePrice, SaleCur, PayCom 
                            From 
                            (
		                      (Select Unit, SalePrice, SaleCur, 
		                          ServiceDesc= 
		                             (Case ServiceType 
				                       WHEN 'HOTEL' THEN (Select isnull(dbo.FindLocalName(Hotel.NameLID, @Market), Name) + 
                                                         ' (' + Category + '),' + 
                                                         (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = Hotel.Location) + 
                                                         ' (' + Room + ',' + Accom + ',' + Board + ') '+ 
                                                         Convert(VarChar(8), BegDate, 3) + ' - ' + Convert(VarChar(8), EndDate, 3) From Hotel (NOLOCK) Where Code = Service)
		                                WHEN 'FLIGHT' THEN  (Select FlightNo + ' (' + DepAirport + '->' + ArrAirport + '),' + FlgClass + ', ' + 
                                                            Convert(VarChar(8), isnull(TDepDate,FlyDate),3) + ' ' + Convert(VarChar(5), isnull(TDepTime,DepTime),108) + '-' + 
                                                            Convert(VarChar(5), isnull(TArrTime,ArrTime),108) From FlightDay (NOLOCK) Where FlightNo = Service And FlyDate = ResService.BegDate) 
		                                WHEN 'TRANSPORT' THEN (Select isnull(dbo.FindLocalName(Transport.NameLID, @Market), Name) From Transport Where Code = ResService.Service) + ' (' +
					                                        (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location Where RecID = ResService.DepLocation) + ' -> ' +
					                                        (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location Where RecID = ResService.ArrLocation) + ') '
		                                WHEN 'RENTING' THEN (Select isnull(dbo.FindLocalName(Renting.NameLID, @Market),Name) + ' (Category:' + Category + ') / ' + 
                                                            (Select isnull(dbo.FindLocalName(Location.NameLID, @Market),Name) From Location (NOLOCK) Where RecId=DepLocation) + ' ' + 
                                                            Convert(VarChar(8), BegDate, 3) + ' - ' + Convert(VarChar(8), EndDate, 3) From Renting (NOLOCK) Where Code = Service) 
		                                WHEN 'EXCURSION' THEN (Select isnull(dbo.FindLocalName(Excursion.NameLID, @Market), Name) + ' / ' + 
                                                            (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecId = DepLocation) From Excursion (NOLOCK) Where Code = Service) 
		                                WHEN 'INSURANCE' then (Select isnull(dbo.FindLocalName(Insurance.NameLID, @Market),Name)+', '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3) From Insurance With (NOLOCK) Where Code=Service) 
		                                WHEN 'VISA' then (Select isnull(dbo.FindLocalName(Visa.NameLID, @Market),Name)+ Case when VisaType<>'' then ' ('+VisaType+')' else '' end++', '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3) From Visa With (NOLOCK) Where Code=Service) 
		                                WHEN 'TRANSFER' then (Select isnull(dbo.FindLocalName(Transfer.NameLID, @Market),Name)+Case when TrfType<>'' then ' ('+TrfType+')' else '' end+', '+Convert(varchar(8),BegDate,3) From Transfer With (NOLOCK) Where Code=Service) 
		                                WHEN 'HANDFEE' then (Select isnull(dbo.FindLocalName(HandFee.NameLID, @Market),Name)+', '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3) From HandFee With (NOLOCK) Where Code=Service) 
		                                ELSE (Select isnull(dbo.FindLocalName(AdService.NameLID, @Market),Name) From AdService With (NOLOCK) Where AdService.Service=ResService.ServiceType and Code=ResService.Service)+', '+Convert(varchar(8),BegDate,3)+' - '+Convert(varchar(8),EndDate,3)end ), PayCom 
		                       From ResService (NOLOCK)	
		                       Where ResNo=@ResNo AND IncPack = 'N' And (StatSer in (0,1) Or (StatSer = 3 And isnull(SalePrice, 0) <> 0))
		                      ) 
                              Union All                
                              ( 
                                Select RsE.Unit, RsE.SalePrice, RsE.SaleCur, 
                                    ServiceDesc=Case RsE.ServiceType
                                    When 'FLIGHT' Then
                                      (Select isnull(dbo.FindLocalName(ServiceExt.NameLID,@Market),ServiceExt.Name)  From ServiceExt With (NOLOCK) Where Code=RsE.ExtService)
                                      +', '+Convert(varchar(8),isnull(FD.TDepDate,RsE.BegDate),3)+' - '+Convert(varchar(8),isnull(FD.TDepDate,RsE.EndDate),3) 
                                    Else
                                      (Select isnull(dbo.FindLocalName(ServiceExt.NameLID,@Market),ServiceExt.Name) 
                                        +', '+Convert(varchar(8),RsE.BegDate,3)+' - '+Convert(varchar(8),RsE.EndDate,3) From ServiceExt With (NOLOCK) Where Code=RsE.ExtService)
                                    End,
                                    RsE.PayCom 
                                From ResServiceExt RsE (NOLOCK) 
                                Join ResService Rs (NOLOCK) ON Rs.RecID=RsE.ServiceID
                                Left Join FlightDay FD (NOLOCK) ON FD.FlyDate=Rs.BegDate And FD.FlightNo=Rs.Service
                                Where RsE.ResNo=@ResNo And RsE.IncPack='N' And (RsE.StatSer in (0,1) Or (RsE.StatSer=3 And isnull(RsE.SalePrice,0)<>0)) 
                              )
                            ) X ";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TvReport.NovReport.InvServicePrice record = new TvReport.NovReport.InvServicePrice();
                        record.ServiceDesc = Conversion.getStrOrNull(R["ServiceDesc"]);
                        record.Unit = Conversion.getInt16OrNull(R["Unit"]);
                        record.UnitPrice = Conversion.getDecimalOrNull(R["UnitPrice"]);
                        record.SalePrice = Conversion.getDecimalOrNull(R["SalePrice"]);
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.PayCom = Equals(R["PayCom"], "Y");
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.NovReport.ResService> getResServices(string Market, string ResNo, CultureInfo Ci, ref string errorMsg)
        {
            List<TvReport.NovReport.ResService> retVal = new List<TvReport.NovReport.ResService>();
            switch (Market)
            {
                case "LTNOV": retVal = new TvReport.NovReport.NovReport().getResServices_LT(Market, ResNo, Ci, ref errorMsg); break;
                case "LVNOV": retVal = new TvReport.NovReport.NovReport().getResServices_LV(Market, ResNo, Ci, ref errorMsg); break;
                case "EENOV": retVal = new TvReport.NovReport.NovReport().getResServices_EE(Market, ResNo, Ci, ref errorMsg); break;
                case "LTAIP": retVal = new TvReport.NovReport.NovReport().getResServices_LT(Market, ResNo, Ci, ref errorMsg); break;
                case "LTECO": retVal = new TvReport.NovReport.NovReport().getResServices_LT(Market, ResNo, Ci, ref errorMsg); break;
            }
            return retVal;
        }

        public List<TvReport.NovReport.ResService> getResServices_EE(string Market, string ResNo, CultureInfo Ci, ref string errorMsg)
        {
            List<TvReport.NovReport.ResService> records = new List<TvReport.NovReport.ResService>();
            string tsql = @"Select Name=isnull(dbo.FindlocalName(S.NameLID, @Market), S.Name), Adult=isnull(Adult,0), Child=isnull(Child,0), SalePrice=isnull(SalePrice,0), SaleCur, Unit=isnull(Unit,0),
	                            Description=(Case ServiceType 							 
		                             When 'HOTEL' Then (Select isnull(dbo.FindLocalName(Hotel.NameLID,@Market), Name) + ' (' + Category +') ' From Hotel (NOLOCK) Where Code = S1.Service) + ',' 
						                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location Where RecID = S1.DepLocation) + '/'
						                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location Where RecID = (Select Top 1 Country From Location (NOLOCK) Where RecID=S1.DepLocation))
						                              + '<br />Check-in : ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '&nbsp;&nbsp;Check-out : ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator) 													
						                              + '<br />Toatüüp :' + (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Room (NOLOCK) Where Code=S1.Room)
						                              + '<br />Toitlustustüüp :' + (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Board (NOLOCK) Where Code=S1.Board)                              
		                             When 'FLIGHT' Then S1.Service+' ('
					                              + (
						                              Select (Case When isnull(DA.NameS,'')='' then FD.DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' then FD.ArrAirport Else AA.NameS End)+'),'+S1.FlgClass+', '+dbo.ConvertDate(isnull(FD.TDepDate,FD.FlyDate),@DateFormat,@Seperator)+' '+CONVERT(VarChar(5),isnull(FD.TDepTime,FD.DepTime),108)+'-'+CONVERT(VarChar(5),isnull(FD.TArrTime,FD.ArrTime),108) 
						                              From FlightDay (NOLOCK) FD
						                              Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
						                              Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
						                              Where FlightNo=S1.Service And FlyDate=S1.BegDate
						                            )
		                             When 'TRANSPORT' Then (Select isnull(dbo.FindLocalName(Transport.NameLID, @Market), Name) From Transport (NOLOCK) Where Code = S1.Service) + ' ('  
						                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + '-' 
						                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.ArrLocation) + ') ' 
						                              + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)
		                             When 'TRANSFER' Then (Select isnull(dbo.FindLocalName(Transfer.NameLID, @Market), Name) From Transfer (NOLOCK) Where Code = S1.Service) + '-' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)							
		                             When 'INSURANCE' Then (Select isnull(dbo.FindLocalName(Insurance.NameLID, @Market), Name) From Insurance (NOLOCK) Where Code = S1.Service) + ' , ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)
						                              + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
		                             When 'VISA' Then (Select isnull(dbo.FindLocalName(Visa.NameLID, @Market), Name) + ' , ' 
						                              + (Select isnull(dbo.FindLocalName(VisaType.NameLID, @Market), Name) From VisaType (NOLOCK) Where Code = Visa.VisaType) From Visa (NOLOCK) Where Code = S1.Service)  
						                              + ' ( ' + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + ' -> '  
						                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.ArrLocation) + ' ) '  					                                      
		                             When 'EXCURSION' Then (Select isnull(dbo.FindLocalName(Excursion.NameLID, @Market), Name) From Excursion (NOLOCK) Where Code = S1.Service) + ' ( ' 
						                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + ' ) ' 
						                              + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
		                             When 'HANDFEE' Then (Select isnull(dbo.FindLocalName(HandFee.NameLID,  @Market), Name) From HandFee (NOLOCK) Where Code = S1.Service) + ' , '  
						                              + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
		                             When 'RENTING' Then (Select isnull(dbo.FindLocalName(Renting.NameLID, @Market), Name) + ' Category: ' + Category From Renting (NOLOCK) Where Code = S1.Service) + ' / '  
						                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation)  
						                              + ' ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
		                             Else 
			                               (Select isnull(dbo.FindLocalName(AdService.NameLID,@Market), Name) From AdService (NOLOCK) Where Code = S1.Service) + ' , ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) 
						                            + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
		                             End ), Step=Cast(1 As smallint), S1.StatConf
	                            From ResService S1 (NOLOCK)
	                            Join Service S (NOLOCK) ON S.Code = S1.ServiceType
	                            Where ResNo = @ResNo 
	                            And S1.StatSer<>2 
                            ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "DateFormat", DbType.String, strFunc.Trim(Ci.DateTimeFormat.ShortDatePattern, ' ').ToString().Replace("dd", "d").Replace("yy", "y").Replace("MM", "m").Replace(Ci.DateTimeFormat.DateSeparator[0].ToString(), ""));
                db.AddInParameter(dbCommand, "Seperator", DbType.String, Ci.DateTimeFormat.DateSeparator[0]);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TvReport.NovReport.ResService record = new TvReport.NovReport.ResService();
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.Adult = (Int16)R["Adult"];
                        record.Child = (Int16)R["Child"];
                        record.SalePrice = (decimal)R["SalePrice"];
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.Unit = (Int16)R["Unit"];
                        record.Description = Conversion.getStrOrNull(R["Description"]); ;
                        record.Step = (Int16)R["Step"];
                        record.StatConf = (Int16)R["StatConf"];
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.NovReport.ResService> getResServices_LV(string Market, string ResNo, CultureInfo Ci, ref string errorMsg)
        {
            List<TvReport.NovReport.ResService> records = new List<TvReport.NovReport.ResService>();
            string tsql = @"Select Name=isnull(dbo.FindlocalName(S.NameLID, @Market), S.Name), Adult=isnull(Adult,0), Child=isnull(Child,0), SalePrice=isnull(SalePrice,0), SaleCur, Unit=isnull(Unit,0),
                                    Description=(Case ServiceType 							 
                                         When 'HOTEL' Then (Select isnull(dbo.FindLocalName(Hotel.NameLID,@Market), Name) + ' (' + Category +') ' From Hotel (NOLOCK) Where Code = S1.Service) + ',' 
							                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location Where RecID = S1.DepLocation) + '/'
	                                                      + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location Where RecID = (Select Top 1 Country From Location (NOLOCK) Where RecID=S1.DepLocation))
							                              + '<br />Iebraukšanas datums : ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '&nbsp;&nbsp;Izbraukšanas datums : ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator) 													
							                              + '<br />Numura tips :' + (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Room (NOLOCK) Where Code=S1.Room)
	                                                      + '<br />Ēdināšana :' + (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Board (NOLOCK) Where Code=S1.Board)                              
                                         When 'FLIGHT' Then S1.Service+' ('
					                              + (
						                              Select (Case When isnull(DA.NameS,'')='' then FD.DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' then FD.ArrAirport Else AA.NameS End)+'),'+S1.FlgClass+', '+dbo.ConvertDate(isnull(FD.TDepDate,FD.FlyDate),@DateFormat,@Seperator)+' '+CONVERT(VarChar(5),isnull(FD.TDepTime,FD.DepTime),108)+'-'+CONVERT(VarChar(5),isnull(FD.TArrTime,FD.ArrTime),108) 
						                              From FlightDay (NOLOCK) FD
						                              Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
						                              Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
						                              Where FlightNo=S1.Service And FlyDate=S1.BegDate
						                            )
                                         When 'TRANSPORT' Then (Select isnull(dbo.FindLocalName(Transport.NameLID, @Market), Name) From Transport (NOLOCK) Where Code = S1.Service) + ' ('  
                                                          + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + '-' 
                                                          + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.ArrLocation) + ') ' 
							                              + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)
                                         When 'TRANSFER' Then (Select isnull(dbo.FindLocalName(Transfer.NameLID, @Market), Name) From Transfer (NOLOCK) Where Code = S1.Service) + '-' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)							
                                         When 'INSURANCE' Then (Select isnull(dbo.FindLocalName(Insurance.NameLID, @Market), Name) From Insurance (NOLOCK) Where Code = S1.Service) + ' , ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)
                                                          + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                         When 'VISA' Then (Select isnull(dbo.FindLocalName(Visa.NameLID, @Market), Name) + ' , ' 
                                                          + (Select isnull(dbo.FindLocalName(VisaType.NameLID, @Market), Name) From VisaType (NOLOCK) Where Code = Visa.VisaType) From Visa (NOLOCK) Where Code = S1.Service)  
                                                          + ' ( ' + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + ' -> '  
                                                          + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.ArrLocation) + ' ) '  					                                      
                                         When 'EXCURSION' Then (Select isnull(dbo.FindLocalName(Excursion.NameLID, @Market), Name) From Excursion (NOLOCK) Where Code = S1.Service) + ' ( ' 
                                                          + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + ' ) ' 
                                                          + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                         When 'HANDFEE' Then (Select isnull(dbo.FindLocalName(HandFee.NameLID,  @Market), Name) From HandFee (NOLOCK) Where Code = S1.Service) + ' , '  
                                                          + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                         When 'RENTING' Then (Select isnull(dbo.FindLocalName(Renting.NameLID, @Market), Name) + ' Category: ' + Category From Renting (NOLOCK) Where Code = S1.Service) + ' / '  
                                                          + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation)  
                                                          + ' ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                         Else 
                                               (Select isnull(dbo.FindLocalName(AdService.NameLID,@Market), Name) From AdService (NOLOCK) Where Code = S1.Service) + ' , ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) 
                                                        + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                         End ), Step=Cast(1 As smallint), S1.StatConf
                            From ResService S1 (NOLOCK)
                            Join Service S (NOLOCK) ON S.Code = S1.ServiceType
                            Where ResNo = @ResNo 
                              And S1.StatSer<>2 ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "DateFormat", DbType.String, strFunc.Trim(Ci.DateTimeFormat.ShortDatePattern, ' ').ToString().Replace("dd", "d").Replace("yy", "y").Replace("MM", "m").Replace(Ci.DateTimeFormat.DateSeparator[0].ToString(), ""));
                db.AddInParameter(dbCommand, "Seperator", DbType.String, Ci.DateTimeFormat.DateSeparator[0]);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TvReport.NovReport.ResService record = new TvReport.NovReport.ResService();
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.Adult = (Int16)R["Adult"];
                        record.Child = (Int16)R["Child"];
                        record.SalePrice = (decimal)R["SalePrice"];
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.Unit = (Int16)R["Unit"];
                        record.Description = Conversion.getStrOrNull(R["Description"]); ;
                        record.Step = (Int16)R["Step"];
                        record.StatConf = (Int16)R["StatConf"];
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.NovReport.ResService> getResServices_LT(string Market, string ResNo, CultureInfo Ci, ref string errorMsg)
        {
            List<TvReport.NovReport.ResService> records = new List<TvReport.NovReport.ResService>();
            string tsql = @"Select Name=isnull(dbo.FindlocalName(S.NameLID, @Market), S.Name), Adult=isnull(Adult,0), Child=isnull(Child,0), SalePrice=isnull(SalePrice,0), SaleCur, Unit=isnull(Unit,0),
                            Description=(Case ServiceType 							 
                                 When 'HOTEL' Then (Select isnull(dbo.FindLocalName(Hotel.NameLID,@Market), Name) + ' (' + Category +') ' From Hotel (NOLOCK) Where Code = S1.Service) + ',' 
							                      + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location Where RecID = S1.DepLocation) + '/'
	                                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location Where RecID = (Select Top 1 Country From Location (NOLOCK) Where RecID=S1.DepLocation))
							                      + '<br />Atvykimas : ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '&nbsp;&nbsp;Išvykimas : ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator) 													
							                      + '<br />Kambarys :' + (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Room (NOLOCK) Where Code=S1.Room)
	                                              + '<br />Maitinimo tipas :' + (Select isnull(dbo.FindLocalName(NameLID, @Market), Name) From Board (NOLOCK) Where Code=S1.Board)                                                               
                                 When 'FLIGHT' Then S1.Service+' ('
					                              + (
						                              Select (Case When isnull(DA.NameS,'')='' then FD.DepAirport Else DA.NameS End)+'-'+(Case When isnull(AA.NameS,'')='' then FD.ArrAirport Else AA.NameS End)+'),'+S1.FlgClass+', '+dbo.ConvertDate(isnull(FD.TDepDate,FD.FlyDate),@DateFormat,@Seperator)+' '+CONVERT(VarChar(5),isnull(FD.TDepTime,FD.DepTime),108)+'-'+CONVERT(VarChar(5),isnull(FD.TArrTime,FD.ArrTime),108) 
						                              From FlightDay (NOLOCK) FD
						                              Join AirPort (NOLOCK) DA ON DA.Code=FD.DepAirport
						                              Join AirPort (NOLOCK) AA ON AA.Code=FD.ArrAirport
						                              Where FlightNo=S1.Service And FlyDate=S1.BegDate
						                            )
                                 When 'TRANSPORT' Then (Select isnull(dbo.FindLocalName(Transport.NameLID, @Market), Name) From Transport (NOLOCK) Where Code = S1.Service) + ' ('  
                                                  + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + '-' 
                                                  + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.ArrLocation) + ') ' 
							                      + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)
                                 When 'TRANSFER' Then (Select isnull(dbo.FindLocalName(Transfer.NameLID, @Market), Name) From Transfer (NOLOCK) Where Code = S1.Service) + '-' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)							
                                 When 'INSURANCE' Then (Select isnull(dbo.FindLocalName(Insurance.NameLID, @Market), Name) From Insurance (NOLOCK) Where Code = S1.Service) + ' , ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)
                                                  + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                 When 'VISA' Then (Select isnull(dbo.FindLocalName(Visa.NameLID, @Market), Name) + ' , ' 
                                                  + (Select isnull(dbo.FindLocalName(VisaType.NameLID, @Market), Name) From VisaType (NOLOCK) Where Code = Visa.VisaType) From Visa (NOLOCK) Where Code = S1.Service)  
                                                  + ' ( ' + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + ' -> '  
                                                  + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.ArrLocation) + ' ) '  					                                      
                                 When 'EXCURSION' Then (Select isnull(dbo.FindLocalName(Excursion.NameLID, @Market), Name) From Excursion (NOLOCK) Where Code = S1.Service) + ' ( ' 
                                                  + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + ' ) ' 
                                                  + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                 When 'HANDFEE' Then (Select isnull(dbo.FindLocalName(HandFee.NameLID,  @Market), Name) From HandFee (NOLOCK) Where Code = S1.Service) + ' , '  
                                                  + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                 When 'RENTING' Then (Select isnull(dbo.FindLocalName(Renting.NameLID, @Market), Name) + ' Category: ' + Category From Renting (NOLOCK) Where Code = S1.Service) + ' / '  
                                                  + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation)  
                                                  + ' ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                 Else 
                                       (Select isnull(dbo.FindLocalName(AdService.NameLID,@Market), Name) From AdService (NOLOCK) Where Code = S1.Service) + ' , ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) 
                                                + '-' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                 End ), Step=Cast(1 As smallint), S1.StatConf
                            From ResService S1 (NOLOCK)
                            Join Service S (NOLOCK) ON S.Code=S1.ServiceType
                            Where ResNo=@ResNo 
                              And S1.StatSer<>2 ";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "DateFormat", DbType.String, strFunc.Trim(Ci.DateTimeFormat.ShortDatePattern, ' ').ToString().Replace("dd", "d").Replace("yy", "y").Replace("MM", "m").Replace(Ci.DateTimeFormat.DateSeparator[0].ToString(), ""));
                db.AddInParameter(dbCommand, "Seperator", DbType.String, Ci.DateTimeFormat.DateSeparator[0]);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        TvReport.NovReport.ResService record = new TvReport.NovReport.ResService();
                        record.Name = Conversion.getStrOrNull(R["Name"]);
                        record.Adult = (Int16)R["Adult"];
                        record.Child = (Int16)R["Child"];
                        record.SalePrice = (decimal)R["SalePrice"];
                        record.SaleCur = Conversion.getStrOrNull(R["SaleCur"]);
                        record.Unit = (Int16)R["Unit"];
                        record.Description = Conversion.getStrOrNull(R["Description"]); ;
                        record.Step = (Int16)R["Step"];
                        record.StatConf = (Int16)R["StatConf"];
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.NovReport.CustomerPrice> getCustomerIncPackPrice(string ResNo, ref string errorMsg)
        {
            List<TvReport.NovReport.CustomerPrice> records = new List<TvReport.NovReport.CustomerPrice>();
            string tsql = @"Declare @PriceListNo int
                            Declare @HolPack VarChar(10)                            
                            Select @PriceListNo=PriceListNo, @HolPack=HolPack From ResMain (NOLOCK) Where ResNo = @ResNo
                            if @PriceListNo > 0 Or @HolPack <> ''
                            Begin
                              SELECT CustNo, Sum(SalePrice) As Price From 
                              (
                                  Select RC1.CustNo, SalePrice=SUM(C1.SalePrice) From ResCust RC1 (NOLOCK)
							      Left Join ResCustPrice C1 (NOLOCK) ON C1.CustNo = RC1.CustNo
                                  Left join ResService S1 (NOLOCK) ON S1.RecID = C1.ServiceID  and C1.ExtserviceID=0 and isnull(IncPack,'N') = 'Y'
                                  where RC1.ResNo = @ResNo And (S1.StatSer in (0,1) Or (S1.StatSer = 3 And isnull(S1.SalePrice, 0) <> 0)) 
                                  group by RC1.CustNo 
                                  union all
                                  Select RC2.CustNo, SalePrice=SUM(C2.SalePrice) From ResCust RC2 (NOLOCK)
							      Left Join ResCustPrice C2 (NOLOCK) ON C2.CustNo = RC2.CustNo
                                  Left join ResServiceExt S2 on S2.RecID = C2.ExtServiceID and (isnull(IncPack,'N') = 'Y')
                                  where RC2.ResNo = @ResNo And (S2.StatSer in (0,1) Or (S2.StatSer = 3 And isnull(S2.SalePrice, 0) <> 0))
                                  group by RC2.CustNo 
                              ) X
                              Group by CustNo
                            End
                            Else
                            Begin
                              SELECT CustNo, Sum(SalePrice1) As Price From 
                              (
                                  Select RC1.CustNo, SalePrice1=0 FROM ResCust RC1 (NOLOCK)
							      Left Join ResCustPrice C1 (NOLOCK) ON C1.CustNo = RC1.CustNo
                                  Left join ResService S1 on S1.RecID = C1.ServiceID  and C1.ExtserviceID=0 
                                  where RC1.ResNo = @ResNo And (S1.StatSer in (0,1) Or (S1.StatSer = 3 And isnull(S1.SalePrice, 0) <> 0)) 
                                  group by RC1.CustNo 
                                  union all
                                  Select RC2.CustNo, SalePrice1=0 FROM ResCust RC2 (NOLOCK)
							      Left Join ResCustPrice C2 (NOLOCK) ON C2.CustNo = RC2.CustNo
                                  Left join ResServiceExt S2 on S2.RecID = C2.ExtServiceID and isnull(Compulsory, 'N') = 'Y' 
                                  where C2.ResNo = @ResNo And (S2.StatSer in (0,1) Or (S2.StatSer = 3 And isnull(S2.SalePrice, 0) <> 0))
                                  group by RC2.CustNo 
                              ) X
                              Group by CustNo
                            End";

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    int i = 0;
                    while (R.Read())
                    {
                        ++i;
                        TvReport.NovReport.CustomerPrice record = new TvReport.NovReport.CustomerPrice();
                        record.IDNo = i;
                        record.CustNo = (int)R["CustNo"];
                        record.Price = (decimal)R["Price"];
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public List<TvReport.NovReport.ContractCustomerPrice> getContractCustomerPrices(string ResNo, string Market, CultureInfo Ci, ref string errorMsg)
        {
            List<TvReport.NovReport.ContractCustomerPrice> records = new List<TvReport.NovReport.ContractCustomerPrice>();
            #region SQL string
            string tsql = @"Declare @PriceListNo int
                            Declare @HolPack VarChar(10)                            
                            Select @PriceListNo=PriceListNo, @HolPack=HolPack From ResMain (NOLOCK) Where ResNo = @ResNo
                            if OBJECT_ID('TempDB.dbo.#TempFile') is not null Drop Table dbo.#TempFile 
                            Create Table #TempFile (CustNo int, ServiceID int, ExtServiceID int, ServiceCode VarChar(10), Price decimal(18, 6), ServiceDesc VarChar(200), BegDate DateTime)

                            if (@PriceListNo > 0 Or @HolPack <> '')
                            Begin
                              Insert Into #TempFile (CustNo, ServiceID, ExtServiceID, ServiceCode, Price, ServiceDesc, BegDate)
                              SELECT CustNo, ServiceID=0, ExtServiceID=0, ServiceCode='', Price, ServiceDesc='',BegDate=CAST(1 AS DateTime) 
                              FROM (
	                              Select CustNo, Sum(SalePrice) As Price From
	                              (
		                              Select RC1.CustNo, SalePrice=SUM(C1.SalePrice) From ResCust RC1 (NOLOCK)
		                              Left Join ResCustPrice C1 (NOLOCK) ON C1.CustNo = RC1.CustNo
		                              Left join ResService S1 (NOLOCK) ON S1.RecID = C1.ServiceID  and C1.ExtserviceID=0 and isnull(IncPack,'N') = 'Y'
		                              where RC1.ResNo = @ResNo And (S1.StatSer in (0,1) Or (S1.StatSer = 3 And isnull(S1.SalePrice, 0) <> 0)) 
		                              group by RC1.CustNo 
		                              union all
		                              Select RC2.CustNo, SalePrice=SUM(C2.SalePrice) From ResCust RC2 (NOLOCK)
		                              Left Join ResCustPrice C2 (NOLOCK) ON C2.CustNo = RC2.CustNo
		                              Left join ResServiceExt S2 on S2.RecID = C2.ExtServiceID and (isnull(IncPack,'N') = 'Y')
		                              where RC2.ResNo = @ResNo And (S2.StatSer in (0,1) Or (S2.StatSer = 3 And isnull(S2.SalePrice, 0) <> 0))
		                              group by RC2.CustNo 
	                              ) X
	                              Group by CustNo
                              ) Y
                            End
                            Else
                            Begin
                              Insert Into #TempFile (CustNo, ServiceID, ExtServiceID, ServiceCode, Price, ServiceDesc, BegDate)
                              SELECT CustNo, ServiceID=0, ExtServiceID=0, ServiceCode='', Price, ServiceDesc='',BegDate=CAST(1 AS DateTime) From 
                              (
	                              Select CustNo, Sum(SalePrice1) As Price From
	                              (
		                              Select RC1.CustNo, SalePrice1=0 FROM ResCust RC1 (NOLOCK)
		                              Left Join ResCustPrice C1 (NOLOCK) ON C1.CustNo = RC1.CustNo
		                              Left join ResService S1 on S1.RecID = C1.ServiceID  and C1.ExtserviceID=0 
		                              where RC1.ResNo = @ResNo And (S1.StatSer in (0,1) Or (S1.StatSer = 3 And isnull(S1.SalePrice, 0) <> 0)) 
		                              group by RC1.CustNo 
		                              union all
		                              Select RC2.CustNo, SalePrice1=0 FROM ResCust RC2 (NOLOCK)
		                              Left Join ResCustPrice C2 (NOLOCK) ON C2.CustNo = RC2.CustNo
		                              Left join ResServiceExt S2 on S2.RecID = C2.ExtServiceID and isnull(Compulsory, 'N') = 'Y' 
		                              where C2.ResNo = @ResNo And (S2.StatSer in (0,1) Or (S2.StatSer = 3 And isnull(S2.SalePrice, 0) <> 0))
		                              group by RC2.CustNo 
	                              ) X
	                              Group by CustNo
                              ) Y
                            End

                            Insert Into #TempFile (CustNo, ServiceID, ExtServiceID, ServiceCode, Price, ServiceDesc, BegDate)
                            SELECT CustNo, ServiceID, ExtServiceID, ServiceCode, SalePrice AS Price, ServiceDesc, BegDate From 
                            ( 
                                    Select C1.CustNo, S1.RecID ServiceID, C1.ExtServiceID, S1.Service ServiceCode, SalePrice=C1.SalePrice,  
                                        ServiceDesc=( Case S1.ServiceType 
                                                             When 'HOTEL' Then (Select isnull(dbo.FindLocalName(Hotel.NameLID,@Market), Name) + ' (' + Category +') ' From Hotel (NOLOCK) Where Code = S1.Service) + ', ' 
                                                                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation)  
                                                                              + ' (' + S1.Room + ',' + S1.Accom + ',' + S1.Board + ') ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + ' - ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator) 
                                                             When 'FLIGHT' Then S1.Service+' ('+(Select DepAir+' -> '+ArrAir From Flight (NOLOCK) Where Code=S1.Service)+') / '+S1.FlgClass+' , '
												                              + (Select dbo.ConvertDate(isnull(FD.TDepDate,FD.FlyDate),@DateFormat,@Seperator) From FlightDay FD (NOLOCK) Where FD.FlyDate=S1.BegDate And FD.FlightNo=S1.Service)
                                                             When 'TRANSPORT' Then (Select isnull(dbo.FindLocalName(Transport.NameLID, @Market), Name) From Transport (NOLOCK) Where Code = S1.Service) + ' ('  
                                                                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + ' -> ' 
                                                                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.ArrLocation) + ') ' 
                                                             When 'TRANSFER' Then (Select isnull(dbo.FindLocalName(Transfer.NameLID, @Market), Name) From Transfer (NOLOCK) Where Code = S1.Service) + ' - ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)
                                                             When 'INSURANCE' Then (Select isnull(dbo.FindLocalName(Insurance.NameLID, @Market), Name) From Insurance (NOLOCK) Where Code = S1.Service) + ' , ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator)
                                                                              + ' - ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                                             When 'VISA' Then (Select isnull(dbo.FindLocalName(Visa.NameLID, @Market), Name) + ' , ' 
                                                                              + isnull((Select isnull(dbo.FindLocalName(VisaType.NameLID, @Market), Name) From VisaType (NOLOCK) Where Code = Visa.VisaType), '') From Visa (NOLOCK) Where Code = S1.Service)  
                                                                              + ' ( ' + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + ' -> '  
                                                                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.ArrLocation) + ' ) '  
                                                                              + ' , ' +dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + ' - ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                                             When 'EXCURSION' Then (Select isnull(dbo.FindLocalName(Excursion.NameLID, @Market), Name) From Excursion (NOLOCK) Where Code = S1.Service) + ' ( ' 
                                                                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation) + ' ) ' 
                                                                              + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + ' - ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                                             When 'HANDFEE' Then (Select isnull(dbo.FindLocalName(HandFee.NameLID,  @Market), Name) From HandFee (NOLOCK) Where Code = S1.Service) + ' , '  
                                                                              + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + ' - ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                                             When 'RENTING' Then (Select isnull(dbo.FindLocalName(Renting.NameLID, @Market), Name) + ' Category: ' + Category From Renting (NOLOCK) Where Code = S1.Service) + ' / '  
                                                                              + (Select isnull(dbo.FindLocalName(Location.NameLID, @Market), Name) From Location (NOLOCK) Where RecID = S1.DepLocation)  
                                                                              + ' ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) + ' - ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                                             Else 
                                                                   (Select isnull(dbo.FindLocalName(AdService.NameLID,@Market), Name) From AdService (NOLOCK) Where Code = S1.Service) + ' , ' + dbo.ConvertDate(S1.BegDate, @DateFormat, @Seperator) 
                                                                            + ' - ' + dbo.ConvertDate(S1.EndDate, @DateFormat, @Seperator)
                                                             End ),
                                            S1.BegDate
                                    from ResCustPrice C1 (NOLOCK) 
                                    join ResService S1 (NOLOCK) on S1.RecID = C1.ServiceID  and C1.ExtserviceID=0 and isnull(IncPack,'N') = 'N' 
                                    where C1.ResNo = @ResNo And (S1.StatSer in (0,1) Or (S1.StatSer = 3 And isnull(S1.SalePrice, 0) <> 0)) 		
                                union all
                                    Select C2.CustNo, S2.ServiceID, C2.ExtServiceID, S2.ExtService ServiceCode, SalePrice=C2.SalePrice, 
                                        --ServiceDesc=(Select isnull(dbo.FindLocalName(SE1.NameLID, @Market), SE1.Name) From ServiceExt SE1 (NOLOCK) WHERE SE1.Code=S2.ExtService)+' '+
                                        --            (Case When S2.BegDate<>S2.EndDate Then (dbo.ConvertDate(S2.BegDate, @DateFormat, @Seperator) + ' - ' + dbo.ConvertDate(S2.EndDate, @DateFormat, @Seperator)) else dbo.ConvertDate(S2.BegDate, @DateFormat, @Seperator) End),
                                        ServiceDesc=Case When S2.ServiceType='FLIGHT' 
						                                          Then
						                                            (Select isnull(dbo.FindLocalName(SE1.NameLID, @Market), SE1.Name) From ServiceExt SE1 (NOLOCK) WHERE SE1.Code=S2.ExtService)+' '+
						                                            (
						                                             Select dbo.ConvertDate(isnull(FD.TDepDate,FD.FlyDate),@DateFormat,@Seperator) From FlightDay FD (NOLOCK) 
						                                             Where FD.FlyDate=S2.BegDate And FD.FlightNo=(Select Service From ResService (NOLOCK) Where RecID=S2.ServiceID)
						                                            )
						                                          Else
						                                            (Select isnull(dbo.FindLocalName(SE1.NameLID, @Market), SE1.Name) From ServiceExt SE1 (NOLOCK) WHERE SE1.Code=S2.ExtService)+' '+
                                                                    (Case When S2.BegDate<>S2.EndDate Then (dbo.ConvertDate(S2.BegDate, @DateFormat, @Seperator) + ' - ' + dbo.ConvertDate(S2.EndDate, @DateFormat, @Seperator)) else dbo.ConvertDate(S2.BegDate, @DateFormat, @Seperator) End)
                                                                  End,
                                        S2.BegDate
                                    from ResCustPrice C2 (NOLOCK) 
                                    join ResServiceExt S2 (NOLOCK) on S2.RecID = C2.ExtServiceID and isnull(IncPack,'N') = 'N' 
                                    where C2.ResNo = @ResNo And (S2.StatSer in (0,1) Or (S2.StatSer = 3 And isnull(S2.SalePrice, 0) <> 0))
                            ) X 

                            Select CustNo, ServiceID, ExtServiceID, ServiceCode, Sum(Price) Price, ServiceDesc, BegDate
                            From #TempFile
                            Group By CustNo, ServiceID, ExtServiceID, ServiceCode, ServiceDesc, BegDate 
                          ";
            #endregion

            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                //@ResNo VarChar(10), @Market VarChar(10), @DateFormat VarChar(15), @Seperator VarChar(1)
                db.AddInParameter(dbCommand, "ResNo", DbType.String, ResNo);
                db.AddInParameter(dbCommand, "Market", DbType.String, Market);
                db.AddInParameter(dbCommand, "DateFormat", DbType.String, strFunc.Trim(Ci.DateTimeFormat.ShortDatePattern, ' ').ToString().Replace("dd", "d").Replace("yy", "y").Replace("MM", "m").Replace(Ci.DateTimeFormat.DateSeparator[0].ToString(), ""));
                db.AddInParameter(dbCommand, "Seperator", DbType.String, Ci.DateTimeFormat.DateSeparator[0]);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    while (R.Read())
                    {
                        //CustNo, ServiceID, ExtServiceID, ServiceCode, Sum(Price) Price, ServiceDesc 
                        TvReport.NovReport.ContractCustomerPrice record = new TvReport.NovReport.ContractCustomerPrice();
                        record.CustNo = (int)R["CustNo"];
                        record.ServiceID = (int)R["ServiceID"];
                        record.ExtServiceID = (int)R["ExtServiceID"];
                        record.ServiceCode = Conversion.getStrOrNull(R["ServiceCode"]);
                        record.Price = (decimal)R["Price"];
                        record.ServiceDesc = Conversion.getStrOrNull(R["ServiceDesc"]);
                        record.BegDate = Conversion.getDateTimeOrNull(R["BegDate"]);
                        records.Add(record);
                    }
                    return records;
                }
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        public object getContrat2Page(string Holpack, ref string errorMsg)
        {
            string tsql = @"Select Code, UseContText, ContractText, ContractPict, ContractHtml From HolPack (NOLOCK) Where Code=@Code";
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                db.AddInParameter(dbCommand, "Code", DbType.String, Holpack);
                using (IDataReader R = db.ExecuteReader(dbCommand))
                {
                    if (R.Read())
                    {

                    }
                }
                return db.ExecuteScalar(dbCommand);
            }
            catch (Exception Ex)
            {
                errorMsg = Ex.Message;
                return null;
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }
    }
}


