﻿using BankIntegration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvTools;

public partial class Payments_General_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["CCData"] = null;
            bool? useSSL = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("Payments", "UseSSL"));
            if (!useSSL.HasValue || (useSSL.HasValue && useSSL == false))
            {
                string port = Request.ServerVariables["SERVER_PORT"].ToString();
                if (port == "80" || port == "8080")
                {
                    string secureURL;
                    secureURL = Request.Url.ToString().Replace("http://", "https://");
                    string HostStr = Request.Url.Host;
                    if (HostStr != "localhost" &&
                        HostStr.Split('.')[0].ToString() != "192" &&
                        HostStr.Split('.')[0].ToString() != "172" &&
                        HostStr.Split('.')[0].ToString() != "10")
                    {
                        try
                        {
                            Response.Redirect(secureURL);
                        }
                        catch { }
                    }
                }
            }
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static BIPaymentDetail getPaymentDetail(string ResNo, string PaymentCategory, string CardCode, string InstallNum)
    {
        string errorMsg = string.Empty;

        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        BIPaymentDetail retVal = new BIPaymentDetail();
        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        BIPaymentFormData paymentFormData = (BIPaymentFormData)HttpContext.Current.Session["CCData"];

        int? payCat = Conversion.getInt32OrNull(PaymentCategory);
        int? installNum = Conversion.getInt32OrNull(InstallNum);

        List<BICardDetail> cardList = paymentFormData.paymentData.CardDetailList.Where(w => w.PayCatID == payCat).ToList<BICardDetail>();
        BICardDetail card = new BICardDetail();
        if (cardList.Count == 1)
            card = cardList.First();
        else card = cardList.Find(f => f.Code == CardCode && f.InstalNum == installNum);
        if (card != null)
        {
            //retVal.PaymentTypeCode = card.;
            retVal.PaymentType = card.FullName;
            retVal.Total = paymentFormData.PaymentTotalStr;
            retVal.Paid = card.PrePayment.HasValue ? (card.PrePayment.Value.ToString("#.00") + " " + card.Currency) : "";
            retVal.Payable = card.DiscountPrice.HasValue ? (card.DiscountPrice.Value.ToString("#.00") + " " + card.Currency) : "";
            retVal.MinPayable = card.MinPayPrice.HasValue ? (card.MinPayPrice.Value.ToString("#.00") + " " + card.Currency) : "";
            retVal.PaidAmount = card.DiscountPrice.ToString();
            retVal.PaidAmountCur = card.Currency;
            return retVal;
        }
        else
            return null;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<BICardDetail> getCreditCardDetail(string ID)
    {
        string errorMsg = string.Empty;
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        BIPaymentFormData paymentFormData = (BIPaymentFormData)HttpContext.Current.Session["CCData"];

        List<BICardDetail> cardDetails = paymentFormData.paymentData.CardDetailList;
        List<BICardDetail> retVal = (from q in cardDetails
                                     where q.CreditCardID == Conversion.getInt32OrNull(ID)
                                     select q).ToList<BICardDetail>();
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<BIKeyValue> getCreditCards()
    {
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        BIPaymentFormData paymentFormData = (BIPaymentFormData)HttpContext.Current.Session["CCData"];
        var retVal = from q in paymentFormData.paymentData.CardList
                     select new BIKeyValue { Key = q.CreditCardID.ToString(), Value = q.CreditCardName };
        return retVal.ToList<BIKeyValue>();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData(string ResNo)
    {
        string errorMsg = string.Empty;
        BIPaymentFormData formData = new BIPaymentFormData();
        BIUser UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        if (UserData == null)
        {
            return new
            {
                isOK = false,
                Error = true,
                ErrMessage = HttpContext.GetGlobalResourceObject("BeginPayment", "glblReservationOrAgencyNotFound")
            };
        }
        BIResMainRecord resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
        if (resMain == null)
        {
            return new
            {
                isOK = false,
                Error = true,
                ErrMessage = HttpContext.GetGlobalResourceObject("BeginPayment", "glblReservationNotFound")
            };
        }
        if (UserData.OwnAgency)
        {
            formData.DisabledPaidAmount = true;
        }
        else
        {
            if (UserData.WorkType == 3)
                formData.DisabledPaidAmount = false;
            else
                formData.DisabledPaidAmount = true;
            formData.PosType = 1;
        }

        if (Conversion.getStrOrNull(HttpContext.Current.Application["UsePaymentRule"]) == "1")
        {
            formData.UsePaymentRule = true;
            if (resMain.PasPayRule == 1)
                formData.PaymentRule = 1;
            List<BIResCustRecord> resCustList = new BILib().getResCustList(resMain.Market, resMain.ResNo, ref errorMsg);
            IEnumerable accData = from q in resCustList
                                  orderby (string.Equals(q.Leader, "Y") ? 0 : 1)
                                  select new BIKeyValue { Key = q.CustNo.ToString(), Value = q.TitleStr + "," + q.Name + " " + q.Surname };
            formData.AccountData = accData.Cast<BIKeyValue>();
        }
        List<BIPayTypeRecord> payTypes = new BILib().getPayType(UserData.Market, ref errorMsg);
        BIPaymentInfo paymentInfoRes = new BILib().getPaymentInfoForReservation(ResNo, null, ref errorMsg);
        int? Country = new BILib().getLocationForCountry(resMain.ArrCity.Value, ref errorMsg);
        string CardCur = paymentInfoRes.SaleCur;
        int? catPackID = paymentInfoRes.PriceListNo;
        int? minPerVal = paymentInfoRes.InstalNum;
        decimal? price = paymentInfoRes.ResAmount;

        List<BIPaymentTypeData> paymentTypeData = new BILib().getPaymentTypeData(UserData.Market, resMain.PLMarket, resMain.PLOperator, UserData.AgencyID, CardCur, Country, catPackID, DateTime.Now, paymentInfoRes.ResBegDate, paymentInfoRes.Night, minPerVal, false, true, ref errorMsg);
        formData.paymentTypeData = paymentTypeData;
        List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(string.Empty, ref errorMsg);
        formData.bankVPos = bankVPosList;
        decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
        if (paymentInfoRes.PrePayment.HasValue && paymentInfoRes.PrePayment.Value > 0)
        {
            minPerVal = paymentInfoRes.InstalNum.HasValue ? Math.Abs(paymentInfoRes.InstalNum.Value) : -1;
            formData.visablePerDesc = true;
        }
        formData.PaymentAmount = price.HasValue ? price.Value : (decimal)0;
        formData.PaymentAmountCurr = paymentInfoRes.SaleCur;
        formData.PaymentTotalStr = price.ToString() + " " + paymentInfoRes.SaleCur;
        BICreditCardAndDetail paymentData = new BILib().createCardDetail(UserData, paymentTypeData, paymentInfoRes);
        formData.paymentData = paymentData;
        formData.ResNo = ResNo;
        HttpContext.Current.Session["CCData"] = formData;
        return new
        {
            isOK = true,
            Error = false,
            ErrMessage = string.Empty,
            ResNo = resMain.ResNo,
            TotalPrice = _amount.HasValue ? _amount.Value.ToString("#,###.00") + " " + resMain.SaleCur : string.Empty,
            success = true,
            UsePaymentRule = formData.UsePaymentRule
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object paymentTypeData(string ResNo)
    {
        string errorMsg = string.Empty;
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        BIPaymentInfo paymentInfoRes = new BILib().getPaymentInfoForReservation(ResNo, null, ref errorMsg);
        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        BIPaymentFormData paymentFormData = (BIPaymentFormData)HttpContext.Current.Session["CCData"];
        List<BIPaymentTypeData> val = (List<BIPaymentTypeData>)paymentFormData.paymentTypeData;

        var retVal = from q in val
                     group q by new { Code = q.PayCatID, Name = q.PayCatName } into k
                     select new BIKeyValue { Key = k.Key.Code.ToString(), Value = k.Key.Name };
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object getGetPayment(string ResNo, string Amount, string CardName, string CardNumber1, string CardNumber2, string CardNumber3, string CardNumber4, string CVC, string ExpDateM, string ExpDateY, string BankCode, string external)
    {
        string errorMsg = string.Empty;
        BIUser UserData = new BIUser();
        UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        BIResMainRecord resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);

        bool okey = false;
        //returnObj retVal = new returnObj();
        List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(string.Empty, ref errorMsg);
        List<BIPayTypeRecord> payTypes = new BILib().getPayType(resMain.PLMarket, ref errorMsg);

        decimal? _amount = Conversion.getDecimalOrNull(Amount.Replace(",", ci.NumberFormat.NumberDecimalSeparator).Replace(".", ci.NumberFormat.NumberDecimalSeparator));

        string posReferans = "";
        string resNo = ResNo + "_" + DateTime.Now.ToString("ddMMhhmm");
        string cardNumber = string.Empty;

        cardNumber = CardNumber1 + CardNumber2 + CardNumber3 + CardNumber4;

        string cardNumberxxx = "";
        if (cardNumber != "")
        {
            try
            {
                cardNumberxxx = CardNumber1 + CardNumber2.Substring(0, 2) + "XXXXXXXX" + CardNumber4.Substring(1, 3);
            }
            catch { }
        }
        List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
        List<BIResCustInfoRecord> resCustInfo = new BILib().getResCustInfoList(UserData.Market, ResNo, ref errorMsg);
        string LeaderName = string.Empty;
        string LeaderPhone = string.Empty;
        int? CustNumber = null;
        try
        {
            var Ln = from RC in resCust
                     join RCI in resCustInfo on RC.CustNo equals RCI.CustNo
                     where string.Equals(RC.Leader, "Y")
                     select new
                     {
                         LeaderCustNo = RC.CustNo,
                         LeaderName = (!string.IsNullOrEmpty(RCI.CName) || !string.IsNullOrEmpty(RCI.CSurName)) ? RCI.CSurName + " " + RCI.CName : RC.Surname + " " + RC.Surname,
                         LeaderPhone = (RCI.ContactAddr == "H" ? RCI.AddrHomeTel : RCI.AddrWorkTel)
                     };

            if (Ln.Count() > 0)
            {
                CustNumber = Ln.FirstOrDefault().LeaderCustNo;
                LeaderName = Ln.FirstOrDefault().LeaderName;
                LeaderPhone = Ln.FirstOrDefault().LeaderPhone;
            }
            else
                LeaderName = resCust.Where(f => f.Leader == "Y").FirstOrDefault().Name + " " + resCust.Where(f => f.Leader == "Y").FirstOrDefault().Surname;
        }
        catch { }

        string retValue = string.Empty;

        BIPayTypeRecord payType = payTypes.Where(w => w.Code == BankCode).FirstOrDefault();
        if (payType == null)
        {
            return new
            {
                Paid = 0,
                Message = HttpContext.GetGlobalResourceObject("BeginPayment", "glblPayTypeNotFound"),
                External = ""
            };
        }
        BIBankVPosRecord vPos = bankVPosList.Find(f => f.Bank == payType.Bank);
        if (vPos == null)
        {
            return new
            {
                Paid = 0,
                Message = HttpContext.GetGlobalResourceObject("BeginPayment", "glblVPosNotFound"),
                External = ""
            };
        }
        if (string.Equals(vPos.Interface, "YKB"))
        {
            string hostUrl = vPos.Host;
            string clientID = vPos.ClientID;//"400247195";
            string userID = vPos.UserID;//"vezirsatis";
            string pass = vPos.Password;//"V3z1rs@t1s";
            string bankcur = vPos.BankCur; //840 Euro
            retValue = VPosUtils.ConnectToYapiKrediPos(hostUrl, clientID, userID, cardNumber, ExpDateM, ExpDateY, CVC, resNo,
                (_amount.Value / 100).ToString(), bankcur, ref errorMsg, ref posReferans, "0", LeaderName, LeaderPhone);
            if (retValue == "1")
            {
                //   ViewState["OdemeOk"] = "true";            
            }
            else
            {
                return new
                {
                    Paid = 0,
                    Message = errorMsg,
                    External = ""
                };
            }
        }
        else
        {
            #region ETS ye bağlan
            if (vPos == null)
            {
                return new
                {
                    Paid = 0,
                    Message = HttpContext.GetGlobalResourceObject("BeginPayment", "glblBankInfoNotFound"),
                    External = ""
                };
            }
            string hostUrl = vPos.Host;
            string clientID = vPos.ClientID;//"400247195";
            string userID = vPos.UserID;//"vezirsatis";
            string pass = vPos.Password;//"V3z1rs@t1s";
            string bankcur = vPos.BankCur; //840 Euro

            retValue = VPosUtils.ConnectToVPos(hostUrl, clientID, userID, pass, bankcur, cardNumber, ExpDateM, ExpDateY, CVC, resNo,
                                        (_amount.Value / 100).ToString(), ref errorMsg, ref posReferans, "0", LeaderName, LeaderPhone, true);

            if (retValue == "1")
            {
                //   ViewState["OdemeOk"] = "true";            
            }
            else
            {
                return new
                {
                    Paid = 0,
                    Message = errorMsg,
                    External = ""
                };
            }
            #endregion
        }

        BIResCustRecord leader = resCust.Where(f => f.Leader == "Y").FirstOrDefault();

        int PayTypeID = -1;

        if (payType != null && payType.RecID.HasValue)
            PayTypeID = payType.RecID.Value;

        okey = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, PayTypeID,
                                                     _amount.Value / 100, "YTL", cardNumberxxx, null, BankCode, CardName,
                                                     null, null, -1, 0, posReferans, ResNo, "", ref errorMsg);
        if (okey)
        {
            string urlString = string.Empty;

            new BILib().SetResOptDate(ResNo, null, -1, ref errorMsg);
            new BILib().setReservationComplate(UserData, ResNo, false, ref errorMsg);

            return new
            {
                Paid = 1,
                Message = string.Format(HttpContext.GetGlobalResourceObject("BeginPayment", "glblPaymentReceived").ToString(), ResNo, posReferans),
                External = urlString
            };
        }
        else
        {
            return new
            {
                Paid = 0,
                Message = string.Format(HttpContext.GetGlobalResourceObject("BeginPayment", "glblPaymentReceived2").ToString(), ResNo, posReferans),
                External = ""
            };
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object getPostNet()
    {
        string errorMsg = string.Empty;
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        BIPaymentFormData paymentFormData = (BIPaymentFormData)HttpContext.Current.Session["CCData"];
        BIPaymentInfo paymentInfoRes = new BILib().getPaymentInfoForReservation(paymentFormData.ResNo, null, ref errorMsg);
        List<BIPaymentTypeData> val = (List<BIPaymentTypeData>)paymentFormData.paymentTypeData;
        List<BIHtmlCodeData> retVal = new List<BIHtmlCodeData>();
        retVal.Add(new BIHtmlCodeData() { IdName = "posnetID", Value = "206" });
        retVal.Add(new BIHtmlCodeData() { IdName = "mid", Value = "6707780182" });
        retVal.Add(new BIHtmlCodeData() { IdName = "xid", Value = "" });
        retVal.Add(new BIHtmlCodeData() { IdName = "tranType", Value = "Auth" });
        retVal.Add(new BIHtmlCodeData() { IdName = "lang", Value = "tr" });
        retVal.Add(new BIHtmlCodeData() { IdName = "currencyCode", Value = "TL" });
        retVal.Add(new BIHtmlCodeData() { IdName = "merchantReturnSuccessURL", Value = "http ://www.google.com.tr/webhp?hl=tr" });
        retVal.Add(new BIHtmlCodeData() { IdName = "merchantReturnFailURL", Value = "http ://www.google.com.tr/webhp?hl=tr" });
        /*
        <form id="posnetForm" name="posnetForm" action="http ://setmpos.ykb.com/3DSWebService/OOS" method="post">
        <input type="hidden" id="posnet_PosnetID" name="posnetID" value="206" />
        <input type="hidden" id="posnet_" name="mid" value="6707780182" />
        <input type="hidden" id="" name="xid" value="" />
        <input type="hidden" id="" name="tranType" value="Auth" />
        <input type="hidden" id="" name="lang" value="tr" />
        <input type="hidden" id="" name="currencyCode" value="TL" />
        <input type="hidden" id="" name="merchantReturnSuccessURL" value="http ://www.google.com.tr/webhp?hl=tr" />
        <input type="hidden" id="" name="merchantReturnFailURL" value="http ://www.google.com.tr/webhp?hl=tr" />
        </form>
         */

        return new
        {
            isOK = true,
            actionUrl = "http://setmpos.ykb.com/3DSWebService/OOS",
            data = retVal
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object getCardFinans(string PaymentType, string CreditCard,
                        string pCardHolder, string pCardNumber, string pCardCvv2, string pMonth, string pYear, string pAmount, int? pInstallNum)
    {
        string errorMsg = string.Empty;
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        BIPaymentFormData paymentFormData = (BIPaymentFormData)HttpContext.Current.Session["CCData"];
        BIPaymentInfo paymentInfoRes = new BILib().getPaymentInfoForReservation(paymentFormData.ResNo, null, ref errorMsg);
        List<BIPaymentTypeData> val = (List<BIPaymentTypeData>)paymentFormData.paymentTypeData;

        BIUser UserData = new BILib().getReservationCreateUserData(paymentFormData.ResNo, ref errorMsg);

        List<BIPayTypeRecord> payTypes = new BILib().getPayType(UserData.Market, ref errorMsg);
        BICardDetail creditCard = paymentFormData.paymentData.CardDetailList.Find(f => f.CreditCardID == (Conversion.getInt32OrNull(CreditCard)));
        BIBankVPosRecord vPos = ((List<BIBankVPosRecord>)paymentFormData.bankVPos).Find(f => f.Bank == creditCard.Bank && f.Cur == creditCard.Currency);
        List<BIHtmlCodeData> retVal = new List<BIHtmlCodeData>();
        BIPayTypeRecord payType = payTypes.Find(f => f.Code == creditCard.Code);
        BIResMainRecord resMain = new BILib().getResMain(UserData, paymentFormData.ResNo, ref errorMsg);

        decimal? amount = Conversion.getDecimalOrNull(pAmount).HasValue ? Conversion.getDecimalOrNull(pAmount).Value : paymentFormData.PaymentAmount;
        
        string payCur = "TRY";

        //if (!string.Equals(payCur, resMain.SaleCur))
        //{
        //    amount = new BILib().Exchange(resMain.Market, DateTime.Today, payCur, paymentFormData.PaymentAmountCurr, amount, true, ref errorMsg);
        //}
        if (!amount.HasValue)
            return null;

        if (payType.Secure3d.HasValue && payType.Secure3d.Value)
        {
            retVal.Add(new BIHtmlCodeData() { IdName = "clientid", Value = vPos.ClientID });
            retVal.Add(new BIHtmlCodeData() { IdName = "amount", Value = amount.Value.ToString("#.00").Replace(",", ".") });
            retVal.Add(new BIHtmlCodeData() { IdName = "oid", Value = paymentFormData.ResNo });
            retVal.Add(new BIHtmlCodeData() { IdName = "okUrl", Value = vPos.SuccessUrlB2B });
            retVal.Add(new BIHtmlCodeData() { IdName = "failUrl", Value = vPos.ErrorUrlB2B });
            retVal.Add(new BIHtmlCodeData() { IdName = "islemtipi", Value = "Auth" });
            retVal.Add(new BIHtmlCodeData() { IdName = "taksit", Value = creditCard.InstalNum.ToString() });
            string rnd = DateTime.Now.ToString();
            retVal.Add(new BIHtmlCodeData() { IdName = "rnd", Value = rnd });
            retVal.Add(new BIHtmlCodeData() { IdName = "storetype", Value = "3d_pay_hosting" });
            retVal.Add(new BIHtmlCodeData() { IdName = "refreshtime", Value = "10" });
            retVal.Add(new BIHtmlCodeData() { IdName = "lang", Value = "tr" });
            retVal.Add(new BIHtmlCodeData() { IdName = "currency", Value = vPos.BankCur });

            String storekey = vPos.StoreKey;  //işyeri anahtarı
            String hashstr = vPos.ClientID + paymentFormData.ResNo + paymentFormData.PaymentTotalStr + vPos.SuccessUrlB2B + vPos.ErrorUrlB2B + "Auth" + "0" + rnd + storekey;
            System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashstr);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            String hash = Convert.ToBase64String(inputbytes); //Güvenlik ve kontrol amaçlı oluşturulan hash
            retVal.Add(new BIHtmlCodeData() { IdName = "hash", Value = hash });

            return new
            {
                secure3d = true,
                isOK = true,
                actionUrl = vPos.Host,
                data = retVal
            };
        }
        else
        {
            PosForm pf = new PosForm();
            pf.CardHolder = pCardHolder;
            pf.CardNumber = Convert.ToInt64(pCardNumber.Replace(" ", ""));
            pf.CardCvv2 = pCardCvv2;
            pf.Month = pMonth;
            pf.Year = pYear;
            pf.vPosInfo = vPos;
            pf.Instalment = pInstallNum.HasValue ? pInstallNum.Value : (int)0; //creditCard.InstalNum.HasValue ? creditCard.InstalNum.Value : (int)0;
            pf.Amount = amount.HasValue ? amount.Value : (decimal)0.0;

            Pos p = new Pos();        
            switch (vPos.Interface)
            {
                case "finans":
                    p.FinansBank(pf);
                    break;
                case "akbank":
                    p.Akbank(pf);
                    break;
                case "isbank":
                    p.IsBankasi(pf);
                    break;
                case "ykb":
                    p.YapiKredi(pf);
                    break;
                case "garanti":
                    p.GarantiBankasi(pf);
                    break;
            }
            if (p.Result)
            {
                List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, paymentFormData.ResNo, ref errorMsg);
                BIResCustRecord leader = resCust.Find(f => f.Leader == "Y");
                bool okey = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, leader.Surname + " " + leader.Name, DateTime.Today, payType.RecID,
                                                     pf.Amount, payCur/*paymentFormData.PaymentAmountCurr*/, pf.CardNumber.ToString().Substring(12), null, payType.Bank, pf.CardHolder,
                                                     null, null, -1, pInstallNum.HasValue ? pInstallNum.Value : (int)0, p.RefNo, paymentFormData.ResNo, "", ref errorMsg);
                if (okey)
                {
                    string urlString = string.Empty;

                    new BILib().SetResOptDate(paymentFormData.ResNo, null, -1, ref errorMsg);
                    new BILib().setReservationComplate(UserData, paymentFormData.ResNo, false, ref errorMsg);

                    return new
                    {
                        secure3d = false,
                        isOK = true,
                        Paid = 1,
                        Message = string.Format(HttpContext.GetGlobalResourceObject("BeginPayment", "glblPaymentReceived").ToString(), paymentFormData.ResNo, p.RefNo),
                        External = urlString
                    };
                }
                else
                {
                    return new
                    {
                        secure3d = false,
                        isOK = false,
                        Paid = 0,
                        Message = string.Format(HttpContext.GetGlobalResourceObject("BeginPayment", "glblPaymentReceived2").ToString(), paymentFormData.ResNo, p.RefNo),
                        External = ""
                    };
                }
            }
            else
            {
                return new {
                    secure3d = false,
                    isOK = false,
                    errorMsg = p.ErrorMessage + " (" + p.ErrorCode + ")",
                    bankResult = p
                };
            }
        }
    }
}