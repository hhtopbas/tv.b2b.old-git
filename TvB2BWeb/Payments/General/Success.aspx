﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Success.aspx.cs" Inherits="Payments_General_Success" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function paymentFinal() {
            //self.parent.getResData(true);
            self.close();
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="500px">
                <tr>
                    <td valign="top" style="width: 105px;">
                        <asp:Image ID="labelImage" runat="server" ImageUrl="error.png" />

                    </td>
                    <td style="white-space: nowrap;" valign="top">
                        <asp:Literal ID="resultValues" runat="server"></asp:Literal>

                        <asp:Label ID="lblResult1" runat="server" />
                        <br />
                        <asp:Label ID="lblResult2" runat="server" />
                        <br />
                        <asp:Label ID="lblResult3" runat="server" Text="_" />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <input type="button" value="Close" onclick="paymentFinal();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" style="font-size: 10pt;" />
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
