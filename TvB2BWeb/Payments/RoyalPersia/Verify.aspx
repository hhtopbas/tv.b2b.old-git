﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Verify.aspx.cs" Inherits="RoyalPersiaVerify" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">        
        function init() {
            window.onunload = onCloseWindow;
            waitClose();
        }

        function onCloseWindow(evt) {            
            window.location = window.location;            
            this.close();
        }

        function closeWindow() {
            self.close();
        }

        function waitClose() {            
            setTimeout("closeWindow()", 10000);
        }
    </script>
</head>
<body onload="init();">
    <form id="SafiranPaymentResultForm" runat="server">
    <div>
        <h2><asp:Label ID="lblMsg" runat="server" /></h2><br />
        <strong>Reservation number :</strong><asp:Label ID="_iN" runat="server" /><br />
        <strong>Referans :</strong><asp:Label ID="_tref" runat="server" /><br />
        <asp:HiddenField ID="hfreturnAtlas" runat="server" Value="0" />
        <input type="button" value="Close" onclick="closeWindow();" />
    </div>
    </form>
</body>
</html>
