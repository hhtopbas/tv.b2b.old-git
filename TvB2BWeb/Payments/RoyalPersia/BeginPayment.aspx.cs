﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using System.Threading;
using TvTools;
using System.Security.Cryptography;
using System.Globalization;
using BankIntegration;

public partial class Payments_BeginPayment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(string ResNo, string Atlas)
    {
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        BIUser UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        BIResMainRecord resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);

        List<BIPayTypeRecord> payTypes = new BILib().getPayType(UserData.Market, ref errorMsg);
        List<BIPaymentPageDefination> pPD = new BILib().getPaymentPageDefination(resMain.Market, ref errorMsg);
        var query = from q1 in payTypes
                    join q2 in pPD on new { q1.Market, q1.Code } equals new { q2.Market, q2.Code }
                    where q1.ValidForB2B.HasValue && q1.ValidForB2B.Value
                    select new { q1.RecID, q2.Code, q2.B2BUrl, q2.Description };
        if (query != null && query.Count() > 0)
        {
            foreach (var row in query)
            {
                sb.AppendFormat("<input id=\"{0}\" name=\"bank\" type=\"radio\" value=\"{0}|{3}\" atlas=\"{4}\"/><label for=\"{0}\">{2}</label><br />",
                                row.Code, row.RecID, row.Description, row.B2BUrl, Atlas);
            }
        }
        return sb.ToString();
    }
}
