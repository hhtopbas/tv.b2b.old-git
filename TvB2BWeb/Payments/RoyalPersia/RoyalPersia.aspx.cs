using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Xml;
using System.Globalization;
using System.Threading;
using BankIntegration;
using System.Web.Services;
using System.Web.Script.Services;


public partial class RoyalPersiaPayment : System.Web.UI.Page
{     
    protected string redirectAddress = "http://localhost"; //?invoiceNumber=" + invoiceNumber&result=;
    protected string BankCode = "";
    protected Int64 amount;
    protected string timeStamp;
    protected string invoiceDate;
    protected string invoiceNumber;
    protected string action = "1003";
    protected string sign;

    protected void Page_Load(object sender, EventArgs e)
    {
        string returnUrl = string.Empty;
        returnUrl = "";
        string errorMsg = string.Empty;
        redirectAddress = returnUrl;
        timeStamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        invoiceNumber = (string)Request.Params["ResNo"];
        invoiceDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        BankCode = Request.Params["Code"].ToString();
        decimal? _amount = new BILib().getResAmount((string)Request.Params["ResNo"], 0, ref errorMsg);
        if (_amount.HasValue)
        {
            BIUser UserData = new BILib().getReservationCreateUserData((string)Request.Params["ResNo"], ref errorMsg);
            BIResMainRecord resMain = new BILib().getResMain(UserData, (string)Request.Params["ResNo"], ref errorMsg);
            if (resMain != null)
            {
                string baseCurr = "IRR";
                string saleCur = resMain.SaleCur;
                if (!string.Equals(baseCurr, saleCur))
                {
                    _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                }
            }
        }
        amount = _amount.HasValue ? Convert.ToInt64(_amount.Value) : 0;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getPayment(string ResNo, string BankCode)
    {
        string errorMsg = string.Empty;
        decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
        if (!_amount.HasValue)
            return null;

        BIUser UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        BIResMainRecord resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
        string baseCurr = UserData.Cur;
        if (resMain != null)
        {            
            string saleCur = resMain.SaleCur;
            if (!string.Equals(baseCurr, saleCur))
            {
                _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
            }
        }
        List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(BankCode, ref errorMsg);
        BIBankVPosRecord bankVPos = bankVPosList.Find(f => f.Cur == baseCurr);
        if (bankVPos == null)
            return null;
        Int64 amount = _amount.HasValue ? Convert.ToInt64(_amount.Value) : 0;
        bankVPos.MerchantID = "55ac9e9f-0980-41bb-8807-0e285bef37d4";
        System.Net.ServicePointManager.Expect100Continue = false;
        zarinpal.PaymentGatewayImplementationServicePortTypeClient zp = new zarinpal.PaymentGatewayImplementationServicePortTypeClient();
        string Authority;

        int Status = zp.PaymentRequest(bankVPos.MerchantID, Convert.ToInt32((amount / 10).ToString()), ResNo, UserData.EMail, "", Global.getBasePageRoot() + "Payments/RoyalPersia/Verify.aspx?ResNo=" + ResNo + "&a=" + Convert.ToInt32((amount / 10).ToString()).ToString(), out Authority);

        if (Status == 100)
        {            
            return new
            {
                Success = true,
                BankUrl = bankVPos.Host + Authority
            };
        }
        else
        {
            HttpContext.Current.Response.Write("error: " + Status);
            return new
            {
                Success = false,
                Message = "Status error: " + Status
            };
        }        
    }
}
