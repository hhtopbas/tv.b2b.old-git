﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RoyalPersia.aspx.cs" Inherits="RoyalPersiaPayment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Payment System Integration </title>

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.url.js" type="text/javascript"></script>

    <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        body, form { width: 100%; position: relative; text-align: center; margin: 0 auto; }
        table { background-color: #EBF1EB; text-align: left; margin-left: auto; margin-right: auto; }
        td { border: 1px dotted #000; background-color: #fff; height: 23px; font-family: Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif; }
        input { border: 1px solid #EBF1EB; height: 23px; width: 200px; }
    </style>
    <script language="javascript" type="text/javascript">

        function getPayment() {
            $.query = $.query.load(location.href);
            var resNo = $.query.get('ResNo');
            var bankCode = $.query.get('Code');
            var params = new Object();
            params.ResNo = resNo;
            params.BankCode = bankCode;
            $.ajax({
                async: false,
                type: "POST",
                url: "RoyalPersia.aspx/getPayment",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null)
                    {
                        if (msg.d.Success) {
                            location.href = msg.d.BankUrl;
                        }
                        else {
                            alert(msg.d.Message);
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }
    </script>
</head>
<body>
    <form id="BeginPaymentForm" runat="server">
        <table cellpadding="5" cellspacing="5" width="600px">
            <tr>
                <td colspan="2" style="background: #f1f2f3; font-size: 1.0em; font-family: Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif; text-align: center;">
                    <span style="font-weight: bold;">Payment System Integration </span>
                </td>
            </tr>
            <tr>
                <td>Invoice Number
                </td>
                <td>
                    <input type="text" name="invoiceNumber" value="<%=invoiceNumber%>" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td>Invoice Date
                </td>
                <td>
                    <input type="text" name="invoiceDate" value="<%=invoiceDate%>" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td>Amount (مبلغ)
                </td>
                <td>
                    <input type="text" name="amount" value="<%=amount%>" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <input type="button" value="Secure Payment" onclick="getPayment()" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
