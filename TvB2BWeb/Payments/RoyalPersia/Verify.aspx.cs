﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Xml;
using System.Globalization;
using System.Threading;
using BankIntegration;

public partial class RoyalPersiaVerify : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string errorMsg = string.Empty;
        if (!IsPostBack)
        {
            if (Request.QueryString["Status"] != "" && Request.QueryString["Status"] != null && Request.QueryString["Authority"] != "" && Request.QueryString["Authority"] != null)
            {
                if (Request.QueryString["Status"].ToString().Equals("OK"))
                {
                    int Amount = Convert.ToInt32(Request.QueryString["a"].ToString());
                    long RefID;
                    System.Net.ServicePointManager.Expect100Continue = false;
                    zarinpal.PaymentGatewayImplementationServicePortTypeClient zp = new zarinpal.PaymentGatewayImplementationServicePortTypeClient();

                    int Status = zp.PaymentVerification("55ac9e9f-0980-41bb-8807-0e285bef37d4", Request.QueryString["Authority"].ToString(), Amount, out RefID);

                    if (Status == 100)
                    {
                        Response.Write("Success!! RefId: " + RefID);
                        if (setPayment(Request.QueryString["ResNo"].ToString(), (Amount * 10).ToString(), RefID.ToString()) != "pay")
                        {
                            Response.Write("Payment success and not created payment record in TourVisio");
                        }
                    }
                    else
                    {
                        Response.Write("Error!! Status: " + Status);
                    }
                }
                else
                {
                    Response.Write("Error! Authority: " + Request.QueryString["Authority"].ToString() + " Status: " + Request.QueryString["Status"].ToString());
                }
            }
            else
            {
                Response.Write("Invalid Input");
            }
        }
    }

    protected string setPayment(string ResNo, string Amount, string referans)
    {
        bool returnAtlas = false;
        bool returnBiletBank = false;
        bool returnHotelsPro = false;
        bool returnAxxaSigorta = false;
        bool returnAxxaVisa = false;
        returnAtlas = ResNo[0] == '9';
        returnBiletBank = ResNo[0] == '7';
        returnHotelsPro = ResNo[0] == '6';
        returnAxxaVisa = ResNo[0] == '3';
        returnAxxaSigorta = ResNo[0] == '5';
        //ResNo = ResNo.Remove(0, 1);
        string errorMsg = string.Empty;
        BIUser UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        decimal? _amount = Convert.ToDecimal(Amount);
        BIResMainRecord resMain = new BIResMainRecord();
        if (_amount.HasValue)
        {
            resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
            //if (resMain != null)
            //{
            //    string baseCurr = "IRR";
            //    string saleCur = resMain.SaleCur;
            //    if (!Equals(baseCurr, saleCur))
            //    {
            //        _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
            //    }
            //}
        }
        List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
        BIResCustRecord leader = resCust.Find(f => f.Leader == "Y");

        string LeaderName = string.Empty;
        if (leader != null)
            LeaderName = leader.Surname + " " + leader.Name;
        decimal odenen = _amount.HasValue ? _amount.Value : 0;
        string posReferans = "Ref: " + referans;

        int PayTypeID = 1;

        bool okey = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, PayTypeID,
                                                     odenen, "IRR", "",
                                                     -1, "", "",
                                                     null, null, -1, 0, posReferans, ResNo, "", ref errorMsg);
        if (okey)
        {
            new BILib().SetResOptDate(ResNo, null, -1, ref errorMsg);
            new BILib().setReservationComplate(UserData, ResNo, false, ref errorMsg);
            return "pay";
        }
        else
        {
            return "nopay";
        }
    }
}

