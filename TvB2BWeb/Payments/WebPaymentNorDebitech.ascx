﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebPaymentNorDebitech.ascx.cs" Inherits="themes_Detur_UserControls_WebPaymentFinLuottokunta" %>

</form>

<form id="PostForm" name="PostForm" action="https://secure.incab.se/verify/bin/DETURAS/index" method="post">
<input type="hidden" name="pageSet" value="<%=pageSet%>">
<input type="hidden" name="data" value="<%=data%>" />
<input type="hidden" name="currency" value="<%=currency%>" />
<input type="hidden" name="method" value="cc.cekab" default="cc.nw" />
<input type="hidden" name="billingFirstName" value="<%=billingFirstName%>" />
<input type="hidden" name="billingLastName" value="<%=billingLastName%>" />
<input type="hidden" name="billingAddress" value="<%=billingAddress%>" />
<input type="hidden" name="billingZipCode" value="<%=billingZipCode%>" />
<input type="hidden" name="billingCity" value="<%=billingCity%>" />
<input type="hidden" name="billingCountry" value="<%=billingCountry%>" />
<input type="hidden" name="eMail" value="<%=eMail%>" />
<input type="hidden" name="MAC" value="<%=mac%>" />
<input type="hidden" name="resNumber" value="<%=ReservationNumber%>" />  
<input type="hidden" name="paymentType" value="<%=PayType%>" /> 
<input type="hidden" name="Reserve" value="<%=Reserve%>" />  
</form>
<script language="javascript" type="text/javascript">
    var vPostForm = document.PostForm;
    vPostForm.submit();
</script>