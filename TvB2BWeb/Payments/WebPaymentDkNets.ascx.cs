﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using TvBo;

public partial class WebPaymentDkNets : System.Web.UI.UserControl
{
    #region Parameters

    string MerchantId
    {
        get { return "494615"; }
    }

    string Token
    {
        get { return "9Rg-t5=H"; }
    }

    string RedirectUrl
    {
        get { return WebRoot.BasePageRoot + "Payments/NetsDkReturn.aspx?PayType={0}&Reserve={1}&resNumber={2}&Amount={3}"; }
    }

    string TerminalUrl
    {
        get { return "https://epayment.bbs.no/epay/default.aspx?merchantId={0}&transactionId={1}"; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(WebRoot.BasePageRoot)) Response.Redirect("~/Default.aspx");

        string resNo = this.Attributes["ReservationNumber"];
        string payType = this.Attributes["PayType"];
        string reserve = this.Attributes["Reserve"];
        string secure3d = this.Attributes["Secure"];
        string amount = "0";
        decimal amountVal = 0;
        if (String.IsNullOrEmpty(this.Attributes["Amount"]) == false)
        {
            amountVal = Decimal.Parse(this.Attributes["Amount"]) * 100;
            amount = amountVal.ToString("#.");
        }
        string cardType = "AmericanExpress,MasterCard,Visa,ResursBank,Dankort";
        if (this.Attributes["CardType"] != null)
        {
            cardType = this.Attributes["CardType"];
            if (cardType == "DANKORT")
                cardType = "Dankort";
        }

        NetsSVC.Netaxept client = new NetsSVC.Netaxept();
        NetsSVC.RegisterResponse response = client.Register(MerchantId, Token,
            new NetsSVC.RegisterRequest
        {
            ServiceType = "B",
            Order = new NetsSVC.Order
            {
                OrderNumber = resNo,
                CurrencyCode = "DKK",
                Amount = amount,
                Force3DSecure = secure3d
            },
            Environment = new NetsSVC.Environment
            {
                WebServicePlatform = "WCF"
            },
            Terminal = new NetsSVC.Terminal
            {
                PaymentMethodList = cardType,
                OrderDescription = "",
                Language = "da_DK",
                RedirectUrl = String.Format(RedirectUrl, payType, reserve, resNo, (amountVal / 100).ToString("#.00"))
            }
        });

        Response.Redirect(String.Format(TerminalUrl, MerchantId, response.TransactionId));
    }
}