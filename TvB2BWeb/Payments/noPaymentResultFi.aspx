﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="noPaymentResultFi.aspx.cs" Inherits="Payments_PaymentResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Result</title>
    <script type="text/javascript" src="../Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.custom.min.js"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
       
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="Page">
            <tv1:Header ID="tvHeader" runat="server" />
            <br />
            <br />
            <div>
                <asp:Label ID="lblResNo" runat="server"></asp:Label>
            </div>
            <br />
            <asp:Button Text="Payment" ID="paymentBtn"  runat="server" OnClick="paymentBtn_Click" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
        </div>
    </form>
</body>
</html>
