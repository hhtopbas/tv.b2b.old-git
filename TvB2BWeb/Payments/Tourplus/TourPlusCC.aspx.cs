﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Threading;
using BankIntegration;
using TvTools;
using System.Web.Script.Services;
using System.Globalization;
using System.Collections;

public partial class TourPlusCC : System.Web.UI.Page
{
    protected decimal amount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static List<BIHtmlCodeData> getFormData(string ResNo)
    {

        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        Dictionary<string, string> currencyList = new Dictionary<string, string>();
        Dictionary<string, decimal?> amountList = new Dictionary<string, decimal?>();

        string errorMsg = string.Empty;
        BIUser UserData = new BIUser();
        decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
        decimal? _payAmount = _amount;
        int? payTypeID = 0;
        string payTypeCode = string.Empty;
        BIResMainRecord resMain = new BIResMainRecord();
        BIPayTypeRecord payTypeRec = null;
        string baseCurr = "EURO";
               
        if (_amount.HasValue)
        {
            UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);            
            resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
            if (resMain != null)
            {
                List<BIPayTypeRecord> payTypes = new BILib().getPayType(resMain.PLMarket, ref errorMsg);
                payTypeRec = payTypes.Find(f => f.Code == "TEBCARD");
                if (payTypes != null)
                {
                    payTypeID = payTypes.Find(f => f.Code == "TEBCARD").RecID;                    
                }

                string saleCur = resMain.SaleCur;             
                
                if (!string.Equals(baseCurr, saleCur))
                {
                    _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);                    
                }
            }
        }

        List<BIHtmlCodeData> htmlData = new List<BIHtmlCodeData>();
        htmlData.Add(new BIHtmlCodeData { IdName = "txtResNo", TagName = "span", Data = resMain.ResNo });
        htmlData.Add(new BIHtmlCodeData { IdName = "txtAmount", TagName = "span", Data = _amount.HasValue ? _amount.Value.ToString("#,###.00") + " EURO " + (_payAmount.HasValue ? "(" + _payAmount.Value.ToString("#,###.00") + " " + resMain.SaleCur + ")" : "") : "" });
        htmlData.Add(new BIHtmlCodeData { IdName = "amountVal", TagName = "", Data = _amount.HasValue ? _amount.Value.ToString("#.") : "" });

        string successURL = String.Format(Global.getBasePageRoot() + "Payments/TourPlus/TourPlusCCResult.aspx?ResNumber={0}&Curr={1}&Amount={2}&PayType={3}",
            ResNo,
            baseCurr,
            _amount.HasValue ? _amount.Value.ToString("#.00").Replace(",", ".") : "",
            payTypeID.ToString());

        string errorURL = String.Format(Global.getBasePageRoot() + "Payments/TourPlus/TourPlusCCResult.aspx?ResNumber={0}&ErrorUrl=true",
            ResNo);
        List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(string.Empty, ref errorMsg);
        BIBankVPosRecord bankVPos = bankVPosList.Find(f => f.Bank == payTypeRec.Bank && f.Cur == baseCurr);
        if (bankVPos == null) return null;
        string strOrderId = String.Format("{0}_{1}{2}", ResNo, DateTime.Now.Minute.ToString(), DateTime.Now.Second.ToString());

        decimal balance = _amount.HasValue ? _amount.Value : (decimal)0;

        GVCHashData gvc = new GVCHashData(strOrderId, bankVPos.ClientID, bankVPos.ProvUserPass, GetPrice(_amount), successURL,
                                errorURL, "0", bankVPos.StoreKey, HttpContext.Current.Request.UserHostAddress, "", bankVPos.MerchantID, bankVPos.UserID,
                                "en", "N", System.DateTime.Now.ToString(), "10");


        String rnd = DateTime.Now.ToString();

        htmlData.Add(new BIHtmlCodeData { IdName = "clientid", TagName = "", Data = bankVPos.ClientID });
        htmlData.Add(new BIHtmlCodeData { IdName = "amount", TagName = "", Data = _amount.HasValue ? _amount.Value.ToString("#.00").Replace(",", ".") : "" });
        htmlData.Add(new BIHtmlCodeData { IdName = "currency", TagName = "", Data = "978" });
        htmlData.Add(new BIHtmlCodeData { IdName = "oid", TagName = "", Data = ResNo });
        htmlData.Add(new BIHtmlCodeData { IdName = "okUrl", TagName = "", Data = successURL });
        htmlData.Add(new BIHtmlCodeData { IdName = "failUrl", TagName = "", Data = errorURL });
        htmlData.Add(new BIHtmlCodeData { IdName = "islemtipi", TagName = "", Data = "Auth" });
        htmlData.Add(new BIHtmlCodeData { IdName = "taksit", TagName = "", Data = "" });
        htmlData.Add(new BIHtmlCodeData { IdName = "rnd", TagName = "", Data = rnd });


        String hashstr = bankVPos.ClientID +
                         ResNo +
                         (_amount.HasValue ? _amount.Value.ToString("#.00").Replace(",", ".") : "") +
                         successURL +
                         errorURL +
                         "Auth" +
                         "" +
                         rnd +
                         bankVPos.StoreKey;
        System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
        byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashstr);
        byte[] inputbytes = sha.ComputeHash(hashbytes);

        String hash = Convert.ToBase64String(inputbytes); //Güvenlik ve kontrol amaçlı oluşturulan hash

        htmlData.Add(new BIHtmlCodeData { IdName = "hash", TagName = "", Data = hash });
        htmlData.Add(new BIHtmlCodeData { IdName = "storetype", TagName = "", Data = "3d_pay_hosting" });
        htmlData.Add(new BIHtmlCodeData { IdName = "refreshtime", TagName = "", Data = "10" });
        htmlData.Add(new BIHtmlCodeData { IdName = "lang", TagName = "", Data = ci.TwoLetterISOLanguageName == "tr" ? "tr" : "en" });
        return htmlData;
    }

    private static string GetPrice(decimal? price)
    {
        if (!price.HasValue) return "00";

        return Math.Floor(price.Value * 100).ToString("#.");
    }
}

public class returnObj
{
    public returnObj()
    {
        _Paid = 0;
        _Message = string.Empty;
        _external = string.Empty;
    }

    Int16? _Paid;
    public Int16? Paid
    {
        get { return _Paid; }
        set { _Paid = value; }
    }

    string _Message;
    public string Message
    {
        get { return _Message; }
        set { _Message = value; }
    }

    string _external;
    public string External
    {
        get { return _external; }
        set { _external = value; }
    }
}