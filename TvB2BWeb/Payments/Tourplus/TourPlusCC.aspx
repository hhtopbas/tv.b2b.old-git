﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TourPlusCC.aspx.cs"
    Inherits="TourPlusCC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Begin Payment</title>

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
       
    </style>

    <script type="text/javascript">
        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showAlert(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                position: 'center',
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        $(this).dialog('close');
                        return true;
                    }
                }
            });
        }

        function getFormData(resNo) {
            $.ajax({
                async: false,
                type: "POST",
                url: "TourPlusCC.aspx/getFormData",
                data: '{"ResNo":"' + resNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = msg.d;
                        $.each(data, function (i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                case 'select':
                                    $("#" + this.IdName).html('');
                                    var listData = $.json.decode(this.data);
                                    $.each(listData.List, function(){
                                        $("#" + this.IdName).append('<option value="' + this.key + '" ' + (listData.value == this.key ? 'selected="selected"' : '') + '>' + this.value + '</option>');
                                    });                                    
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                                    break;
                            }
                        });
                        $("#messageDiv").hide();
                        $("#mainDiv").show();
                    }
                },
                error: function (xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function btnCancelClick() {
            self.window.close();
            self.parent.getPaymentsReturn(false);
        }

        function btnOKClick() {
           $("#secureForm").submit();
        }

        $(document).ready(
            function () {
                $.query = $.query.load(location.href);
                $("#hfResNo").val($.query.get('ResNo'));

                var resNo = $("#hfResNo").val();
                getFormData(resNo);
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <input id="hfResNo" type="hidden" />
        <input id="hfExternal" type="hidden" />
        <input id="hfBankCode" type="hidden" value="GRN" />
        <div id="messageDiv" class="ui-helper-hidden">
            Please Wait
        </div>
        <div id="mainDiv" style="text-align: center;">
            <div style="width: 605px; text-align: center;">
                <div style="width: 600px; font-family: Tahoma; text-align: left; border: solid 1px #666;">
                    <div id="Panel2" style="text-align: center; border: solid 1px #666;">
                        <fieldset>
                            <legend style="line-height: 2;">
                                <label>
                                    <span style="font-size: 12pt;">»</span></label>
                                <strong style="font-size: 12pt;">Payment System Integration</strong> </legend>
                            <br />
                            <br />
                            <table style="font-size: 10pt; text-align: left;">
                                <tr>
                                    <td align="right">Reservation number :
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td align="left">
                                        <strong><span id="txtResNo">ResNo</span></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td align="left">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Amount :
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td align="left">
                                        <strong><span id="txtAmount">Amount</span></strong>
                                        <input id="amountVal" type="hidden" />                                        
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </fieldset>
                    </div>

                    <div id="finalPanel">
                        <table width="100%">
                            <tr>
                                <td style="height: 21px">
                                    <br />
                                </td>
                            </tr>
                            <tr align="center">
                                <td style="height: 26px">
                                    <input id="btnOK" type="button" value="Go to payment" style="height: 30px; width: 150px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                        onclick="btnOKClick()" />
                                    <input id="btnCancel" type="button" value="Cancel - Return Reservation Menu" style="height: 30px;"
                                        class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                        onclick="btnCancelClick()" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
            style="display: none;">
            <div id="messages">
                Message
            </div>
        </div>
    </form>
    <%--<form id="secureForm" action="https ://sanalposprov.garanti.com.tr/servlet/gt3dengine" name="secureForm" method="post">--%>
    <form id="secureForm" action="https://sanalpos.teb.com.tr/servlet/est3Dgate" name="secureForm" method="post" target="_blank">
        <input type="hidden" name="clientid" id="clientid" />
        <input type="hidden" name="amount" id="amount" />
        <input type="hidden" name="currency" id="currency" />
        <input type="hidden" name="oid" id="oid" />
        <input type="hidden" name="okUrl" id="okUrl" />
        <input type="hidden" name="failUrl" id="failUrl" />
        <input type="hidden" name="islemtipi" id="islemtipi" />
        <input type="hidden" name="taksit" id="taksit" />
        <input type="hidden" name="rnd" id="rnd" />
        <input type="hidden" name="hash" id="hash" />
        <input type="hidden" name="storetype" id="storetype" value="3d_pay_hosting" />
        <input type="hidden" name="refreshtime" id="refreshtime" value="10" />
        <input type="hidden" name="lang" id="lang" value="tr" />                
    </form>
</body>
</html>
