﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using TvBo;
using BankIntegration;

public partial class themes_Detur_UserControls_WebPaymentFinSampo : System.Web.UI.UserControl
{
    protected string authentication_Mac;

    protected string merchant_Number;

    private string reservationNumber;
    public string ReservationNumber
    {
        get { return reservationNumber; }
        set { reservationNumber = value; }
    }

    private decimal amount;
    public decimal Amount
    {
        get { return amount; }
        set { amount = value; }
    }

    protected string paymentType;
    protected string Reserve;
    protected string retAddress = "";
    protected string cancelAddress = "";
    protected int language = 3; //1-finnish 2-swedish 3-english    
    protected string dueDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region culture set
        if (((User)Session["UserData"]).Ci.TwoLetterISOLanguageName.ToLower().IndexOf("fi") > -1)
        {
            language = 1;
        }
        else if (((User)Session["UserData"]).Ci.TwoLetterISOLanguageName.ToLower().IndexOf("se") > -1)
        {
            language = 2;
        }
        #endregion

        if (string.IsNullOrEmpty(WebRoot.BasePageRoot)) Response.Redirect("~/Default.aspx");

        string basePage = WebRoot.BasePageRoot;

        merchant_Number = "014497732800";

        //string key = "nnstAu2e8pDFpT6Wc9sFuSfgkKrxjE96YTFCL9jRWDLCzC6E7kEtLePaV78yZB75"; //15-11-2012 de değişti
        string key = "YnjNqaQMEDyLfs2Wj9YfSreHxeF6AVZnd8UBfEzsRj3tSJLZXVhSjD5RgLDgET6X";

        if (this.Attributes["ReservationNumber"] != null)
        {
            reservationNumber = this.Attributes["ReservationNumber"].ToString();
        }
        if (this.Attributes["PayType"] != null)
        {
            paymentType = this.Attributes["PayType"].ToString();
        }
        if (this.Attributes["Amount"] != null)
        {
            amount = decimal.Parse(this.Attributes["Amount"].ToString());
        }
        if (this.Attributes["Reserve"] != null)
        {
            Reserve = this.Attributes["Reserve"].ToString();
        }

        retAddress = string.Format("{0}Payments/PaymentResultfi.aspx?PayType={3}&resNumber={1}&Amount={2}&Reserve={4}",
            basePage,
            reservationNumber,
            amount.ToString("#.00"),
            paymentType,
            Reserve);        

        cancelAddress = string.Format("{0}Payments/noPaymentResultfi.aspx?resNumber={1}",
            basePage,
            reservationNumber);        

       //VERIFICATION CODE = MD5(MAC+SUM+REFERENCE NUMBER+ID+ VERSION+CURRENCY+RETURN ADDRESS+CANCELLATION ADDRESS) 
        dueDate = DateTime.Today.ToString("dd.MM.yyyy");

        reservationNumber = CalculateReferenceNumber(reservationNumber);

//        string data = key + Amount.ToString("#.00") + reservationNumber + merchant_Number + "4" + "EUR" + retAddress + cancelAddress;
        string data = String.Format("{0}&{1}&{2}&{3}&{4}&{5}&{6}&{7}&{8}&", key, Amount.ToString("#.00"),
            reservationNumber, merchant_Number, "4", "EUR", retAddress, cancelAddress,
            dueDate);

        //authentication_Mac = MD5Encode(data, key);
        authentication_Mac = BILib.GetSHA256(data);
    }

    public string CalculateReferenceNumber(string reservationNumber)
    {
        short num = 0;
        short total = 0;
        string referenceNumber = "0";
        for (int i = reservationNumber.Length; i > 0; i--)
        {
            if ((reservationNumber.Length - i) % 3 == 0) num = 7;
            if ((reservationNumber.Length - i) % 3 == 1) num = 3;
            if ((reservationNumber.Length - i) % 3 == 2) num = 1;
            total += (short)(short.Parse(reservationNumber[i - 1].ToString()) * num);
        }

        if (total % 10 > 0) referenceNumber = (10 - (total % 10)).ToString();
        return (reservationNumber + referenceNumber).ToString();
    }

    public string MD5Encode(string originalPassword, string key)
    {
        //Declarations
        Byte[] originalBytes;
        Byte[] encodedBytes;
        MD5 md5;

        //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
        md5 = new MD5CryptoServiceProvider();
        originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
        encodedBytes = md5.ComputeHash(originalBytes);

        //Convert encoded bytes back to a 'readable' string
        return BitConverter.ToString(encodedBytes).Replace("-", "");
    }
}
