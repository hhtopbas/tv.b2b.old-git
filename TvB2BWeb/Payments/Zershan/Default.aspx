﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Payments_Zershan_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Begin Payment</title>

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .card.hidden {
            display: none;
        }
    </style>
    <script type="text/javascript">

        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
        $(document)
            .ajaxStart($.blockUI)
            .ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showAlert(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                position: 'center',
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        return true;
                    }
                }]
            });
        }

        function noNumbers(kontrol, sayi, e) {
            var keynum;
            var keychar;
            var number;
            var unicode = e.charCode ? e.charCode : e.keyCode
            var numcheck; if (window.event) // IE
            {
                keynum = e.keyCode;
                number = 4;
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                keynum = e.which;
                number = 3;
            }
            if (keynum == 8 || e.charcode)
                return true;
            keychar = String.fromCharCode(keynum);
            numcheck = /\d/;
            if (keychar == null)
                return true;

            if (numcheck.test(keychar)) {
                if (kontrol == 'cs1')
                    cs1(sayi, number);
                if (kontrol == 'cs2')
                    cs2(sayi, number);
                if (kontrol == 'cs3')
                    cs3(sayi, number);
                return true;
            }
            else if (keynum < 48 || keynum > 57)
                return false;
            else
                return true;
        }

        function cs1(sayi, number) {
            if (sayi == number) { $("#txtCardNumber2").focus(); }
        }

        function cs2(sayi, number) {
            if (sayi == number) { $("#txtCardNumber3").focus(); }
        }

        function cs3(sayi, number) {
            if (sayi == number) { $("#txtCardNumber4").focus(); }
        }

        function changeCreditCardDetail(value, installNum, seqNo) {
            $("#btnOK").removeData("InstallNum");
            $("#cardDetailTable td").css('background-color', '#FFF')
            $('img[name="visaImg"]').attr("src", "VisaMaster.jpg");
            $("#img_" + seqNo).attr("src", "selectVisaMaster.jpg");
            $("#tr_" + seqNo + " > td").css('background-color', '#999');
            showPaymentDetail(3, value, installNum);
        }

        function changeCreditCard(value) {
            $("#btnOK").removeData("InstallNum");
            $.ajax({
                async: false,
                type: "POST",
                url: "Default.aspx/getCreditCardDetail",
                data: '{"ID":"' + value + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#cardDetail").html('');
                        var cCode = '';
                        var html = '';
                        html += '<table id="cardDetailTable" width="100%">';
                        html += '<tr>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 50px;"><strong>&nbsp;</strong></td>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px;"><strong>Financing options</strong></td>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 90px;"><strong>Amount</strong></td>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 90px;"><strong>Minimum payment</strong></td>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 50px;"><strong>Curr.</strong></td>';
                        html += '</tr>';
                        $.each(data, function (i) {
                            html += '<tr id="tr_' + i + '" onclick=changeCreditCardDetail("' + this.Code + '","' + this.InstalNum + '",' + i + ');>';
                            if (i == 0) cCode = this.Code;
                            html += '<td style="width: 50px;"><img id="img_' + i + '" name="visaImg" alt="" src="VisaMaster.jpg" /></td>';
                            html += '<td>' + this.FullName + '</td>';
                            html += '<td style="width: 90px; text-align: right;">' + this.DiscountPrice + '</td>';
                            html += '<td style="width: 90px; text-align: right;">' + this.MinPayPrice + '</td>';
                            html += '<td style="width: 50px;">&nbsp;&nbsp;' + this.Currency + '</td>';
                            html += '</tr>';
                        });
                        html += '</table>';
                        $("#cardDetail").html(html);
                        changeCreditCardDetail(cCode, null, null);
                    }
                },
                error: function (xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function showPaymentDetail(payCat, cardCode, installNum) {
            var obj = new Object();
            obj.ResNo = $("#hfResNo").val();
            obj.PaymentCategory = payCat;
            obj.CardCode = cardCode;
            obj.InstallNum = installNum;
            $.ajax({
                async: false,
                type: "POST",
                url: "Default.aspx/getPaymentDetail",
                data: $.json.encode(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#txtPaymentType").html(data.PaymentType);
                        $("#txtTotal").html(data.Total);
                        $("#txtPaid").html(data.Paid);
                        $("#txtPayable").html(data.Payable);
                        $("#txtMinPayable").html(data.MinPayable);
                        $("#amountVal").val(data.PaidAmount);
                        $("#txtCur").html(data.PaidAmountCur);

                        $("#btnOK").data("InstallNum", installNum);
                    }
                },
                error: function (xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getCreditCards() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Default.aspx/getCreditCards",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#cardList").html('');
                        var cCode = '';
                        $.each(data, function (i) {
                            if (i == 0) {
                                cCode = this.Key;
                                $("#cardList").append('<input id="rbCreditCard' + i + '" name="rbCreditCard" type="radio" value="' + this.Key + '" onclick="changeCreditCard(this.value);" checked="checked"/><label for="rbCreditCard' + i + '">' + this.Value + '</label><br />');
                            }
                            else {
                                $("#cardList").append('<input id="rbCreditCard' + i + '" name="rbCreditCard" type="radio" value="' + this.Key + '" onclick="changeCreditCard(this.value);" /><label for="rbCreditCard' + i + '">' + this.Value + '</label><br />');
                            }
                        });
                        changeCreditCard(cCode);
                    }
                },
                error: function (xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changePaymentType(value) {
            if (value == undefined || value == '') return;
            if (parseInt(value) != 3) {
                $("#divCreditCard").hide();
                $("#divCardInformation").hide();
                $("#trReferans").show();
                showPaymentDetail(value, null, null);
            }
            else {
                getCreditCards();
                $("#divCreditCard").show();
                $("#divCardInformation").show();
                $("#trReferans").hide();
            }
        }

        function getPaymentType(resNo) {
            var params = new Object();
            params.ResNo = resNo;
            $.ajax({
                async: false,
                type: "POST",
                url: "Default.aspx/paymentTypeData",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#rbPaymentTypeDiv").html('');
                        var payType = '';
                        $.each(data, function (i) {
                            if (i == 0) {
                                payType = this.Key;
                                $("#rbPaymentTypeDiv").append('<input id="rbPaymentTypes' + i + '" name="rbPaymentTypes" type="radio" value="' + this.Key + '" onclick="changePaymentType(this.value);" checked="checked"/><label for="rbPaymentTypes' + i + '">' + this.Value + '</label><br />');
                            }
                            else {
                                $("#rbPaymentTypeDiv").append('<input id="rbPaymentTypes' + i + '" name="rbPaymentTypes" type="radio" value="' + this.Key + '" onclick="changePaymentType(this.value);" /><label for="rbPaymentTypes' + i + '">' + this.Value + '</label><br />');
                            }
                        });
                        changePaymentType(payType);
                    }
                },
                error: function (xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getFormData(resNo) {
            var params = new Object();
            params.ResNo = resNo;
            $.ajax({
                async: false,
                type: "POST",
                url: "Default.aspx/getFormData",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        if (!msg.d.isOK) {
                            showAlert(msg.d.ErrMessage);
                        }
                        else if (msg.d.isOK) {
                            $("#txtResNo").html(msg.d.ResNo);
                            $("#txtTotal").html(msg.d.TotalPrice);
                            if (msg.d.success) {
                                getPaymentType(msg.d.ResNo);
                            }
                            else {
                                showAlert(msg.d.ErrMessage);
                            }
                            $("#mainDiv").show();
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getPosnetForm() {
            var params = new Object();
            $.ajax({
                async: false,
                type: "POST",
                url: "Default.aspx/getPostNet",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        if (!msg.d.isOK) {
                            showAlert(msg.d.errorMsg);
                        }
                        else if (msg.d.isOK) {
                            $("#bankVPos").append('<form id="postnetForm" name="postnetForm" method="POST" target="_blank" action="' + msg.d.actionUrl + '" />');
                            $.each(msg.d.data, function (i) {
                                $("#postnetForm").append($('<input />', { id: this.IdName, name: this.IdName, type: 'hidden', value: this.Value }));
                            });
                            //$("#bankVPos").append(form);
                            $("#postnetForm").submit();
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function cardFinansSuccess(Msg) {
            $("#messages").html(Msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                position: 'center',
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        self.parent.getPaymentsReturn(true);
                        return true;
                    }
                }]
            });
        }

        function getCardFinansForm() {
            var params = new Object();
            var paymentType = $("input[name=rbPaymentTypes]:checked").val();
            var creditCard = $("input[name=rbCreditCard]:checked").val();
            params.PaymentType = paymentType;
            params.CreditCard = creditCard;
            params.pCardHolder = $("#txtCardHolderName").val();
            if ($("#cmbCardType").val() == "Amex")
                params.pCardNumber = $("#txtCardNumberAmex1").val() + $("#txtCardNumberAmex2").val() + $("#txtCardNumberAmex3").val();
            else
                params.pCardNumber = $("#txtCardNumber1").val() + $("#txtCardNumber2").val() + $("#txtCardNumber3").val() + $("#txtCardNumber4").val();
            params.pCardCvv2 = $("#txtSecurityCode").val();
            params.pMonth = $("#cmbExpireMonth").val();
            params.pYear = $("#cmbExpireYear").val();
            params.pAmount = $("#amountVal").val();
            params.pInstallNum = $("#btnOK").data("InstallNum") != undefined && $("#btnOK").data("InstallNum") != null ? parseInt($("#btnOK").data("InstallNum")) : null;
            $.ajax({
                async: false,
                type: "POST",
                url: "Default.aspx/getCardFinans",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        if (!msg.d.isOK) {
                            showAlert(msg.d.errorMsg);
                        }
                        else if (msg.d.isOK) {
                            if (msg.d.secure3d == true) {
                                //var frmPos = $("#<%=form1.ClientID%>");
                                $("#bankVPos").append('<form id="postnetForm" name="postnetForm" method="POST" target="_blank" action="' + msg.d.actionUrl + '" />');

                                $.each(msg.d.data, function (i) {
                                    $("#postnetForm").append($('<input />', { id: this.IdName, name: this.IdName, type: 'hidden', value: this.Value }));
                                    //frmPos.append($('<input />', { id: this.IdName, name: this.IdName, type: 'hidden', value: this.Value }));
                                });
                                $("#postnetForm").append($('<input />', { id: "btnSubmit", name: "btnSubmit", type: 'submit', value: "Submit" }));
                                //$("#bankVPos").append(form);
                                $("#postnetForm").submit();
                                //frmPos.submit();
                            }
                            else {
                                cardFinansSuccess(msg.d.Message);
                            }
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }
        function btnOKClick() {
            getCardFinansForm();
        }

        function btnCancelClick() {
            self.parent.getPaymentsReturn(false);
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            $("#hfResNo").val($.query.get('ResNo'));
            getFormData($("#hfResNo").val());
            $("#cmbCardType").change(function () {
                var type = $(this).val();
                $(".card").removeClass("hidden");
                if (type == "Amex")
                    $(".card.visa").addClass("hidden");
                else
                    $(".card.amex").addClass("hidden");
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" target="_blank" action="https://sanalposprovtest.garanti.com.tr/servlet/gt3dengine">
        <input id="hfResNo" type="hidden" />
        <div id="mainDiv" style="text-align: center; display: block;">
            <div style="width: 605px; text-align: center;">
                <div style="margin-left: 5px;">
                    <fieldset>
                        <legend>
                            <label>
                                <span style="font-size: 11pt;">»</span>
                            </label>
                            <span style="font-weight: bold; font-size: 11pt;"><%= GetGlobalResourceObject("BeginPayment", "glblPaymentType") %></span> </legend>
                        <span id="tdPerDesc" style="font-weight: bold; background-color: #CC0000; color: #FFF; font-size: 7pt; display: none;"><%= GetGlobalResourceObject("BeginPayment", "glblWarning1") %></span>
                        <div id="rbPaymentTypeDiv" style="text-align: left;">
                        </div>
                        <br />
                    </fieldset>
                </div>
            </div>
            <div id="creditCardDiv" style="width: 600px; font-family: Tahoma; text-align: left; border: solid 1px #666;">
                <fieldset>
                    <legend>
                        <label>
                            <span style="font-size: 11pt;">»</span></label>
                        <span style="font-weight: bold; font-size: 11pt;"><%= GetGlobalResourceObject("BeginPayment", "glblCreditCard") %></span>
                    </legend>
                    <div id="rbCreditCardDiv" style="text-align: left;">
                        <div id="cardList">
                        </div>
                        <br />
                        <div id="cardDetail">
                        </div>
                    </div>
                    <br />
                </fieldset>
            </div>
            <div style="width: 600px; font-family: Tahoma; text-align: left; border: solid 1px #666;">
                <div style="text-align: center; border: solid 1px #666;">
                    <fieldset>
                        <legend style="line-height: 2;">
                            <label>
                                <span style="font-size: 11pt;">»</span></label>
                            <strong style="font-size: 11pt;"><%= GetGlobalResourceObject("BeginPayment", "glblSelectedPayment") %></strong>
                        </legend>
                        <br />
                        <div class="ui-helper-clearfix">
                        </div>
                        <table style="font-size: 10pt; text-align: left; width: 99%;">
                            <tr>
                                <td align="right" style="height: 24px; width: 200px;"><%= GetGlobalResourceObject("BeginPayment", "glblReservationNumber") %> :
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <strong><span id="txtResNo">ResNo</span></strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">&nbsp;
                                </td>
                            </tr>
                            <tr id="trPaymentRule" class="ui-helper-hidden">
                                <td align="right" style="height: 24px;"><%= GetGlobalResourceObject("BeginPayment", "glblPaymentRule") %> :
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <select id="ddlPaymentRule">
                                    </select>
                                </td>
                            </tr>
                            <tr id="trPaymentAccount" class="ui-tabs-hide">
                                <td align="right" style="height: 24px;"><%= GetGlobalResourceObject("BeginPayment", "glblPaying") %> :
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <select id="paymentRule" onchange="changedPaymentRule(this.value)">
                                        <option value="0" selected="selected"><%= GetGlobalResourceObject("BeginPayment", "glblGeneralPayment") %></option>
                                        <option value="1"><%= GetGlobalResourceObject("BeginPayment", "glblPaymentPerPerson") %></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;"><%= GetGlobalResourceObject("BeginPayment", "glblPaymentType") %> :
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <span id="txtPaymentType" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="style1"><%= GetGlobalResourceObject("BeginPayment", "glblTotalAmount") %> :
                                </td>
                                <td class="style1">&nbsp;
                                </td>
                                <td align="left" class="style1">
                                    <span id="txtTotal" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;"><%= GetGlobalResourceObject("BeginPayment", "glblTheAmountPaid") %> :
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <span id="txtPaid" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;"><%= GetGlobalResourceObject("BeginPayment", "glblPayable") %> :
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <span id="txtPayable" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;"><%= GetGlobalResourceObject("BeginPayment", "glblTheMinimumToBePaid") %> :
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <span id="txtMinPayable" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;"><%= GetGlobalResourceObject("BeginPayment", "glblAmountToBePAid") %> :
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <input id="amountVal" style="width: 120px;" type="text" disabled="disabled" />
                                    <span id="txtCur" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr id="trReferans">
                                <td align="right" style="height: 24px;"><%= GetGlobalResourceObject("BeginPayment", "glblReference") %> :
                                </td>
                                <td>&nbsp;
                                </td>
                                <td align="left">
                                    <input id="txtReferance" style="width: 90%;" type="text" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                </div>
                <br />
                <div id="divCardInformation" style="text-align: center; border: solid 1px #666;" class="ui-helper-hidden">
                    <fieldset>
                        <legend style="line-height: 2;">
                            <label>
                                <span style="font-size: 11pt;">»</span></label>
                            <strong style="font-size: 11pt;"><%= GetGlobalResourceObject("BeginPayment", "glblCreditCardInformation") %></strong> </legend>
                        <br />
                        <table style="font-size: 10pt; text-align: left; width: 99%;">
                            <tr>
                                <td style="text-align: right">
                                    <span><%= GetGlobalResourceObject("BeginPayment", "glblCardOwner") %> :</span>
                                </td>
                                <td style="text-align: right">&nbsp;
                                </td>
                                <td>
                                    <input id="txtCardHolderName" type="text" style="width: 98%;" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <span><%= GetGlobalResourceObject("BeginPayment", "glblCardNumber") %> :</span>
                                </td>
                                <td style="text-align: right">&nbsp;
                                </td>
                                <td style="width:369px;">
                                    <input id="txtCardNumber1" maxlength="4" class="card visa" onkeypress="return noNumbers('cs1',this.value.length,event);"
                                        size="4" style="width: 47px;" />
                                    <input id="txtCardNumber2" maxlength="4" class="card visa" onkeypress="return noNumbers('cs2',this.value.length,event);"
                                        size="4" style="width: 47px;" />
                                    <input id="txtCardNumber3" maxlength="4" class="card visa" onkeypress="return noNumbers('cs3',this.value.length,event);"
                                        size="4" style="width: 47px;" />
                                    <input id="txtCardNumber4" maxlength="4" class="card visa" onkeypress="return noNumbers('cs4',this.value.length,event);"
                                        size="4" style="width: 47px;" />
                                    <input id="txtCardNumberAmex1" class="card amex hidden" maxlength="4" size="4" style="width: 48px;" />
                                    <input id="txtCardNumberAmex2" class="card amex hidden" maxlength="6" size="6" style="width: 66px;" />
                                    <input id="txtCardNumberAmex3" class="card amex hidden" maxlength="5" size="5" style="width: 56px;" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <span><%= GetGlobalResourceObject("BeginPayment", "glblSecurityCode") %> (CVC) :</span>
                                </td>
                                <td style="text-align: right">&nbsp;
                                </td>
                                <td>
                                    <input id="txtSecurityCode" maxlength="4" style="width: 33px;" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <span><%= GetGlobalResourceObject("BeginPayment", "glblExpiryDate") %> :</span>
                                </td>
                                <td style="text-align: right">&nbsp;
                                </td>
                                <td>
                                    <select id="cmbExpireMonth" style="width: 50px;">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                    <select id="cmbExpireYear" style="width: 50px;">
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                        <option value="32">32</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <span><%= GetGlobalResourceObject("BeginPayment", "glblCardType") %> :</span>
                                </td>
                                <td style="text-align: right">&nbsp;
                                </td>
                                <td>
                                    <select id="cmbCardType" style="width: 152px;">
                                        <option value="Amex">Amex</option>
                                        <option value="MasterCard">Master Card</option>
                                        <option selected="selected" value="Visa">Visa</option>
                                    </select>
                                </td>
                            </tr>
                            <%--<tr>
                                <td colspan="3">
                                    <input id="chkVada" type="checkbox" />
                                    <label for="chkVada">
                                        Vada seçenekleri</label>
                                </td>
                            </tr>--%>
                            <%--<tr>
                                <td colspan="3">
                                    <div id="rblVadaOptionsDiv">
                                    </div>
                                </td>
                            </tr>--%>
                        </table>
                        <br />
                    </fieldset>
                </div>
                <div id="finalPanel">
                    <table width="100%">
                        <%--<tr>
                            <td style="text-align: left;">
                                <div id="txtSozlesme" style="width: 98%; height: 175px;"></div>
                                <br />
                                <input id="AcceptChk" type="checkbox" style="color: #FF0033; font-weight: bold;" />
                                <label for="AcceptChk">
                                    Ödeme şartlarını okudum ve kabul ediyorum.</label>
                            </td>
                        </tr>--%>
                        <tr align="center">
                            <td style="height: 26px">
                                <input id="btnOK" type="button" style="height: 30px; width: 100px;" value="<%= GetGlobalResourceObject("BeginPayment", "glblGetPayment") %>"
                                    class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                    onclick="btnOKClick()" />
                                <input id="btnCancel" type="button" style="height: 30px;" value="<%= GetGlobalResourceObject("BeginPayment", "glblCancelPayment") %>"
                                    class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                    onclick="btnCancelClick()" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <a target="_blank" href="<%=ResolveClientUrl("~/data/zer/GizlilikVeGuvenlik.aspx") %>">Gizlilik Sözleşmesi</a>
                                <a target="_blank" href="<%=ResolveClientUrl("~/data/zer/IptalIadeKosullari.aspx") %>">İptal & İade Sözleşmesi</a>
                                <a target="_blank" href="<%=ResolveClientUrl("~/data/zer/MesafeliSatisSozlesmesi.aspx?resno=" + Request.QueryString["ResNo"].ToString()) %>">Satış Sözleşmesi</a>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
    <div id="bankVPos"></div>
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
        style="display: none;">
        <div id="messages"></div>
    </div>
</body>
</html>
