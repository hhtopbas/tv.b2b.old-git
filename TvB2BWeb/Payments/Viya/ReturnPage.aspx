﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReturnPage.aspx.cs" Inherits="Payments_Viya_ReturnPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="~/Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="~/Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "ResView") %></title>

  <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <%--<script src="Scripts/jquery.bgiframe-2.1.1.js" type="text/javascript"></script>--%>
  <script src="../../Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="../../Scripts/json2.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.simplemodal.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.printPage.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.browser.js" type="text/javascript"></script>
  <script src="/Scripts/jquery.dialogextend.js" type="text/javascript"></script>
  <%--<script src="Scripts/jquery.Base64.js" type="text/javascript"></script>--%>
  <script src="../../Scripts/jquery.url.js" type="text/javascript"></script>

  <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="../../CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="../../CSS/ResView.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
        </style>

  <script language="javascript" type="text/javascript">

      var basePageRoot = '<%= Global.getBasePageRoot() %>';
      var EmailSendedMsg = '<%= GetGlobalResourceObject("DetReport", "EmailSended") %>';
      var lblSessionEnd = '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>';
      var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
      var btnChangeLeader = '<%= GetGlobalResourceObject("LibraryResource", "btnChangeLeader") %>';
      var btnClear = '<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>';
      var btnClose = '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>';
      var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';
      var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
      var btnPDF = '<%= GetGlobalResourceObject("LibraryResource", "btnPDF") %>';
      var btnPrint = '<%= GetGlobalResourceObject("LibraryResource", "btnPrint") %>';
      var btnPrintNext = '<%= GetGlobalResourceObject("LibraryResource", "btnPrintNext") %>';
      var btnRefresh = '<%= GetGlobalResourceObject("LibraryResource", "btnRefresh") %>';
      var btnSave = '<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>';
      var btnSend = '<%= GetGlobalResourceObject("LibraryResource", "btnSend") %>';
      var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
      var cancelService = '<%= GetGlobalResourceObject("LibraryResource", "cancelService") %>';
      var deleteCustomer = '<%= GetGlobalResourceObject("LibraryResource", "deleteCustomer") %>';
      var fillLeaderInfo = '<%= GetGlobalResourceObject("LibraryResource", "fillLeaderInfo") %>';
      var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
      var lblTimeOutMesage = '<%= GetGlobalResourceObject("LibraryResource", "lblTimeOutMesage") %>';
      var lblNotSaved = '<%= GetGlobalResourceObject("LibraryResource","lblNotSaved") %>';
      var lblErrorSysAdmin = '<%= GetGlobalResourceObject("ResView","lblErrorSysAdmin") %>';


      var resViewTimeout = null;
      var resOpenTime = null;
      var resIsOpen = false;

      if (typeof window.event != 'undefined')
          document.onkeydown = function () {
              if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                  return (event.keyCode != 8);
          }
      else
          document.onkeypress = function (e) {
              if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                  return (e.keyCode != 8);
          }

      var NS = document.all;
      var resNo = '';
      var pxmResNo = '';
      var _ChangeLeader = 0;
      var _CustNo = -1;

      function showAlertWithLink(message, link) {
          var alink = "<a href='" + link + "'>" + link + "</a>";
          message = message.replace("[LINK]", alink);
          showAlert(message);
      }
      function logout() {
          $('<div>' + lblSessionEnd + '</div>').dialog({
              autoOpen: true,
              position: {
                  my: 'center',
                  at: 'center'
              },
              modal: true,
              resizable: true,
              autoResize: true,
              bigframe: true,
              buttons: [{
                  text: btnOK,
                  click: function () {
                      $(this).dialog("close");
                      $(this).dialog("destroy");
                      window.location = 'Default.aspx';
                  }
              }]
          });
      }

      $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
      $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

      function showAlert(msg) {
          $(function () {
              $("#messages").html(msg);
              $("#dialog").dialog("destroy");
              $("#dialog-message").dialog({
                  position: 'center',
                  modal: true,
                  buttons: [{
                      text: btnOK,
                      click: function () {
                          $(this).dialog('close');
                          return true;
                      }
                  }]
              });
          });
      }

      function showDialogYesNo(msg) {
          $(function () {
              $("#messages").html(msg);
              $("#dialog").dialog("destroy");
              $("#dialog-message").dialog({
                  position: {
                      my: 'center',
                      at: 'center'
                  },
                  modal: true,
                  buttons: [{
                      text: btnOK,
                      click: function () {
                          $(this).dialog('close');
                          return true;
                      }
                  }, {
                      text: btnCancel,
                      click: function () {
                          $(this).dialog('close');
                          return false;
                      }
                  }]
              });
          });
      }

      function showDialog(msg, transfer, trfUrl) {
          $("#messages").html(msg);
          $("#dialog").dialog("destroy");
          $("#dialog-message").dialog({
              maxWidth: 880,
              maxHeight: 500,
              modal: true,
              buttons: [{
                  text: btnOK,
                  click: function () {
                      $(this).dialog('close');
                      if (transfer == true) {
                          var url = trfUrl;
                          $(location).attr('href', url);
                      }
                  }
              }]
          });
      }

      function maximize() {
          var maxW = screen.availWidth;
          var maxH = screen.availHeight;
          if (location.href.indexOf('pic') == -1) {
              if (window.opera) { } else {
                  top.window.moveTo(0, 0);
                  if (document.all) { top.window.resizeTo(maxW, maxH); }
                  else
                      if (document.layers || document.getElementById) {
                          if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                              top.window.outerHeight = maxH; top.window.outerWidth = maxW;
                          }
                      }
              }
          }
      }

      var selectedSeqNo = null;


      $.extend($.ui.dialog.prototype, {
          'addbutton': function (buttonName, func) {
              var buttons = this.element.dialog('option', 'buttons');
              buttons.push({ text: buttonName, click: func });
              //buttons[buttonName] = func; //2014-09-06 Nova bug
              this.element.dialog('option', 'buttons', buttons);
          }
      });

      $.extend($.ui.dialog.prototype, {
          'removebutton': function (buttonName) {
              var tmpbuttons = this.element.dialog('option', 'buttons');
              var buttons = [];
              for (var i in tmpbuttons) {
                  if (tmpbuttons[i].text != buttonName) {
                      buttons.push({ text: tmpbuttons[i].text, click: tmpbuttons[i].click });
                  }
              }
              //delete buttons[buttonName];
              this.element.dialog('option', 'buttons', buttons);
          }
      });

      function viewReportAddButton(btnName, func) {
          $("#dialog-viewReport").dialog('addbutton', btnName, func);
      }

      function viewReportRemoveButton(btnName) {
          $("#dialog-viewReport").dialog('removebutton', btnName);
      }

      function viewReportPrint(url, width, height) {
          if ($.browser.msie) {
              var windowSizeArray = ["width=" + width + ",height=" + height + ",scrollbars=yes"];
              var windowName = "popUp";
              var windowSize = windowSizeArray[0];
              window.open(url, windowName, windowSize);
          } else {
              if ($.browser.chrome) {
                  $("#viewReport").width(width + 32);
                  $("#viewReport").height(height + 32);
                  id = 'viewReport';
                  var iframe = document.frames ? document.frames[id] : document.getElementById(id);
                  var ifWin = iframe.contentWindow || iframe; iframe.focus(); ifWin.print();
                  return false;
              }
              else {
                  $("#viewReport").width(width + 32);
                  $("#viewReport").height(height + 32);
                  $("#viewReport").contents().find(".mainPage").printPage($("#viewReport").contents().find('style'), width);
                  id = 'viewReport';
                  var iframe = document.frames ? document.frames[id] : document.getElementById(id);
                  var ifWin = iframe.contentWindow || iframe; iframe.focus(); ifWin.print();
                  return false;
              }
          }
      }



      function getLanguageID(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, formID, param1, external, emailSender) {
          var multiLanguageID = null;
          var param = new Object();
          param.DocCode = DocName;
          $.ajax({
              async: false,
              type: "POST",
              data: $.json.encode(param),
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              url: 'ResView.aspx/getMultiLangID',
              success: function (msg) {
                  var html = '';
                  if (msg.hasOwnProperty('d') && msg.d != null && msg.d.length > 0) {
                      $("#langListDiv").html('');
                      $.each(msg.d, function (i) {
                          var clickBtn = '\'' + DocName + '\',\'' + docUrl + '\',\'' + docNameResource + '\',\'' + subFolder + '\',' + resIsBusyControl + ',\'' + this.Id + '\',\'' + param1 + '\',\'' + external + '\',' + (emailSender == true ? true : false);
                          $("#langListDiv").append('<a href="#" onclick="getLanguage(' + clickBtn + ')">' + this.Name + '</a><br />');
                      });
                      $("#dialog").dialog("destroy");
                      $("#dialog-SelectReportLang").dialog({
                          position: 'center',
                          modal: true
                      });
                  } else {
                      multiLanguageID = null;
                      printDocumentFinall(DocName, docUrl, docNameResource, subFolder, resIsBusyControl, null, formID, param1, external, emailSender);
                  }

              },
              error: function (xhr, msg, e) {
                  if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                      showAlert(xhr.responseText);
                  }
              },
              statusCode: {
                  408: function () {
                      logout();
                  }
              }
          });
      }


      $(document).ready(function () {

          $.query = $.query.load(location.href);
          resNo = $.query.get('ResNo');
          pxmResNo = $.query.get('PaxResNo');

          //$("#dialog-cancelReservation").dialog({ autoOpen: false, modal: true, width: 650, height: 300, resizable: true, autoResize: true });
          //$("#dialog-AddTourist").dialog({ autoOpen: false, modal: true, width: 540, height: 490, resizable: true, autoResize: true });
          //$("#dialog-resService").dialog({
          //  autoOpen: false,
          //  modal: true,
          //  width: 675,
          //  height: 400,
          //  resizable: true,
          //  autoResize: true,
          //  buttons: [{
          //    text: btnOK,
          //    click: function () {
          //      $(this).dialog('close'); return true;
          //    }
          //  }]
          //});
          //$("#dialog-resServiceEdit").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
          //$("#dialog-resServiceAdd").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
          //$("#dialog-changeRoomAccom").dialog({ autoOpen: false, modal: true, width: 420, height: 390, resizable: true, autoResize: true });
          //$("#dialog-resPayment").dialog({ autoOpen: false, modal: true, width: 1000, height: 600, resizable: true, autoResize: true });
          //$("#dialog-changeDate").dialog({ autoOpen: false, modal: true, width: 450, height: 400, resizable: true, autoResize: true });
          //$("#dialog-Ssrc").dialog({ autoOpen: false, modal: true, width: 640, height: 550, resizable: true, autoResize: true });
          //maximize();
          //var maxH = screen.availHeight;
          //var height = maxH - 330;
          //var divworkArea = document.getElementById('workArea1');
          //divworkArea.style.minheight = height;
          //ResLockCheck(resNo, pxmResNo);
      });


  </script>

</head>
<body>
  <form id="ResViewForm" runat="server">
    <asp:HiddenField ID="resCheck" runat="server" Value="1" />
    <input id="lastDocName" type="hidden" value="" />
    <input id="resMonitorUrl" type="hidden" value="" />
    <div class="Page">
      <tv1:Header ID="tvHeader" runat="server" />
      <tv1:MainMenu ID="tvMenu" runat="server" />
      <div id="closeReservation" class="ui-helper-hidden ui-helper-clearfix closeReservationDiv">
        <input id="btnCloseRes" type="button" value='<%= GetGlobalResourceObject("ResView", "CloseReservation") %>' onclick="closeReservation()"
          class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
      </div>
      <div id="divSubMenuLiteral" class="ui-helper-clearfix">
        <asp:Literal ID="SubMenuLiteral" runat="server" />
      </div>
      <div id="workArea1" class="Content">
        <div id="divResMain">
          <fieldset>
            <legend>
              <label>
                »</label><strong><%= GetGlobalResourceObject("ResView", "lblPaymentStatus") %></strong>
            </legend>
            <div style="text-align: center;">
              <div id="tableResMain"></div>
              <br />
                <p>
                    <asp:Label runat="server" ID="lblPaymentStatus"></asp:Label>
                </p>
            </div>
          </fieldset>
        </div>
        <div id="divReport">
        </div>
      
        <div id="divResSupDis">
        </div>
        <div id="divResPayment">
        </div>
        <%--<div class="tampon">
            </div>--%>
      </div>
      <div class="Footer">
        <tv1:Footer ID="tvfooter" runat="server" />
      </div>
    </div>
    <div id="dialog-seatSelect" title='<%= GetGlobalResourceObject("ResView", "lblEditService") %>'
      style="display: none;">
      <iframe id="seatSelect" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resCustInfo" title='<%= GetGlobalResourceObject("ResView", "lblCustomerInfo") %>'
      style="display: none;">
      <iframe id="resCustInfo" runat="server" height="580" width="750px" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resService" title='<%= GetGlobalResourceObject("ResView", "lblViewService") %>'
      class="ui-helper-hidden">
      <iframe id="resService" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resServiceEdit" title='<%= GetGlobalResourceObject("ResView", "lblEditService") %>'
      style="display: none;">
      <iframe id="resServiceEdit" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resServiceAdd" title='<%= GetGlobalResourceObject("ResView", "lblAddingService") %>'
      style="display: none;">
      <iframe id="resServiceAdd" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resServiceExtAdd" title='<%= GetGlobalResourceObject("ResView", "lblAddingExtraService") %>'
      style="display: none;">
      <iframe id="resServiceExtAdd" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-cancelReservation" title='<%= GetGlobalResourceObject("ResView", "lblCancelReservation") %>'
      style="display: none;">
      <iframe id="cancelReservation" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-AddTourist" title='<%= GetGlobalResourceObject("ResView", "lblAddingTurist") %>'
      style="display: none;">
      <iframe id="AddTourist" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-paymentPlan" title='<%= GetGlobalResourceObject("ResView", "lblPaymetPlan") %>'
      style="display: none;">
      <span id="divPaymentPlan"></span>
    </div>
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
      style="display: none;">
      <span id="messages">Message</span>
    </div>
    <div id="dialog-changeRoomAccom" title='<%= GetGlobalResourceObject("ResView", "lblChangeRoomOrAccommodation") %>'
      style="display: none;">
      <iframe id="changeRoomAccom" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resPayment" title='<%= GetGlobalResourceObject("ResView", "lblPaymentReservation") %>'
      style="display: none;">
      <iframe id="resPayment" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-changeDate" title='<%= GetGlobalResourceObject("ResView", "lblChangeDate") %>'
      style="display: none;">
      <iframe id="changeDate" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-Ssrc" title='<%= GetGlobalResourceObject("ResView","lblSpecialServiceReqCode") %>'
      style="display: none;">
      <iframe id="Ssrc" runat="server" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
    </div>
    <div id="dialog-changeLeader" title='<%= GetGlobalResourceObject("ResView", "btnChangeLeader")%>'
      style="display: none;">
      <div id="divChangeLeader">
      </div>
    </div>
    <div id="dialog-reportList" title='' style="display: none;">
      <div id="divReportList">
      </div>
    </div>
    <div id="dialog-viewReport" title='' style="display: none;">
      <iframe id="viewReport" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
      <input type="hidden" id="viewReportSrc" />
    </div>
    <div id="dialog-viewPDF" title='' style="display: none;">
      <iframe id="viewPDF" runat="server" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
    </div>
    <div id="dialog-AddingCustsServices" title='' style="display: none;">
      <h1>
        <%= GetGlobalResourceObject("ResView", "msgAdding") %></h1>
      <br />
      <div id="divAddingCustsServices" style="width: 100%;">
      </div>
    </div>
    <div id="dialog-SelectReportLang" title='<%= GetGlobalResourceObject("ResView", "headerLanguageSelect") %>' style="display: none;">
      <div id="langListDiv">
      </div>
    </div>
    <%--Add / Edit Complaint--%>
    <div id="dialog-ViewEditComplaint" title='Add / Edit Complaint'
      style="display: none; text-align: center;">
      <iframe id="ViewEditComplaint" runat="server" height="100%" width="1000px" frameborder="0"
        style="clear: both; text-align: left;"></iframe>
    </div>
    <%--Add / Edit Complaint--%>
    <asp:HiddenField ID="pdfConvSerial" runat="server" Value="RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==" />
  </form>
  <div id="dialog-passportScan" title='Read Passport Data' style="display: none;">
    <iframe id="passportScan" height="100%" width="550" frameborder="0" style="clear: both;"></iframe>
  </div>
</body>
</html>

