﻿using BankIntegration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using TvTools;
public partial class Payments_Viya_ReturnPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs events)
    {
        User user = (User)this.Session["UserData"];
        this.Session["Menu"] = (object)"ResMonitor";
        Thread.CurrentThread.CurrentCulture = user.Ci;
        Thread.CurrentThread.CurrentUICulture = user.Ci;
        if (this.Request.Form.Get("ccid") != null)
            PaymentState(sender, events);
    }

   protected void PaymentState(object sender, System.EventArgs ev)
    {
        string pCreditCard = this.Request.Form.Get("ccid");
        if (HttpContext.Current.Session["CCData"] == null)
        {
            HttpContext.Current.Response.StatusCode = 408;
        }
        else
        {
            BIPaymentFormData biPaymentFormData = (BIPaymentFormData)HttpContext.Current.Session["CCData"];
            BICardDetail creditCard = biPaymentFormData.paymentData.CardDetailList.Find(w => w.CreditCardID == Convert.ToInt32(pCreditCard));
            BIBankVPosRecord biBankVposRecord = ((List<BIBankVPosRecord>)biPaymentFormData.bankVPos).Find((Predicate<BIBankVPosRecord>)(f =>
            {
                if (f.Bank == creditCard.Bank)
                    return f.Cur == creditCard.Currency;
                return false;
            }));

            #region Test Ortamı
            //biBankVposRecord.ClientID = "30691297";
            //biBankVposRecord.ErrorUrlB2B = "http://local.tvb2b.com/payments/viya/returnpage.aspx";
            //biBankVposRecord.Host = "https://sanalposprovtest.garanti.com.tr/vpservlet";
            //biBankVposRecord.MerchantID = "7000679";
            //biBankVposRecord.Password = "123qweASD";
            //biBankVposRecord.ProvUserID = "PROVAUT";
            //biBankVposRecord.ProvUserPass = "123qweASD";
            //biBankVposRecord.StoreKey = "12345678";
            //biBankVposRecord.SuccessUrlB2B = "http://local.tvb2b.com/payments/viya/returnpage.aspx";
            //biBankVposRecord.UserID = "PROVAUT";
            #endregion
            try
            {

                string errorMsg = "";
                BIUser reservationCreateUserData = new BILib().getReservationCreateUserData(biPaymentFormData.ResNo, ref errorMsg);
                new BILib().getPayType(reservationCreateUserData.Market, ref errorMsg);
                BIResMainRecord resMain = new BILib().getResMain(reservationCreateUserData, biPaymentFormData.ResNo, ref errorMsg);
                PosForm pf = new PosForm();
                pf.CardholderPresentCode = "13";
                pf.CardHolder = this.Request.Form.Get("cardholder");
                pf.Amount = Convert.ToDecimal(this.Request.Form.Get("txnamount")) / new Decimal(100);
                int result = 0;
                int.TryParse(this.Request.Form.Get("txninstallmentcount"), out result);
                pf.Instalment = result;
                pf.Secure3D = new Secure3D();
                pf.Secure3D.AuthenticationCode = this.Server.UrlEncode(this.Request.Form.Get("cavv"));
                pf.Secure3D.Md = this.Server.UrlEncode(this.Request.Form.Get("md"));
                pf.Secure3D.SecurityLevel = this.Server.UrlEncode(this.Request.Form.Get("eci"));
                pf.Secure3D.TxnID = this.Server.UrlEncode(this.Request.Form.Get("xid"));
                pf.vPosInfo.ErrorUrlB2B = this.Request.Form.Get("errorurl");
                pf.vPosInfo.SuccessUrlB2B = this.Request.Form.Get("successurl");
                pf.vPosInfo.BankCur = this.Request.Form.Get("txncurrencycode");
                pf.vPosInfo.ClientID = this.Request.Form.Get("clientid");
                pf.vPosInfo.MerchantID = this.Request.Form.Get("terminalmerchantid");
                pf.vPosInfo.ProvUserID = biBankVposRecord.ProvUserID;
                pf.vPosInfo.ProvUserPass = biBankVposRecord.ProvUserPass;
                pf.vPosInfo.UserID = biBankVposRecord.UserID;
                pf.vPosInfo.StoreKey = biBankVposRecord.StoreKey;
                pf.vPosInfo.Host = biBankVposRecord.Host;
                pf.OrderId = this.Request.Form.Get("orderid");
                pf.CustomerEmail = this.Request.Form.Get("customeremailaddress");
                //this.Request.Form.Get("mode");
                //this.Request.Form.Get("apiversion");
                string str1 = this.Request.Form.Get("txntype");
                string str2 = this.Request.Form.Get("txnamount");
                //this.Request.Form.Get("customeremailaddress");
                string str3 = this.Request.Form.Get("mdstatus");
                string error = this.Request.Form.Get("mderrormessage");
                string host = biBankVposRecord.Host;
                string upper = GetSHA1(biBankVposRecord.ProvUserPass + "0" + biBankVposRecord.ClientID).ToUpper();
                GetSHA1(pf.OrderId + biBankVposRecord.ClientID + str2 + upper).ToUpper();
                if (this.Request.Form.Get("secure3dhash") == GetSHA1(biBankVposRecord.ClientID + pf.OrderId + str2 + pf.vPosInfo.SuccessUrlB2B + pf.vPosInfo.ErrorUrlB2B + str1 + (pf.Instalment == 0 ? "" : pf.Instalment.ToString()) + biBankVposRecord.StoreKey + upper).ToUpper())
                {
                    if (str3 == "1" | str3 == "2" | str3 == "3" | str3 == "4")
                    {
                        Pos pos = new Pos();
                        pos.GarantiBankasi(pf);
                        if (!pos.Result)
                        {
                            lblPaymentStatus.Text = pos.ErrorMessage;
                        }
                        else
                        {
                            BIResCustRecord biResCustRecord = new BILib().getResCustList(reservationCreateUserData.Market, biPaymentFormData.ResNo, ref errorMsg).Find((Predicate<BIResCustRecord>)(f => f.Leader == "Y"));
                            if (!new BILib().MakeResPayment(resMain.Agency, (short)0, resMain.Agency, biResCustRecord.Surname + " " + biResCustRecord.Name, new DateTime?(DateTime.Today), biBankVposRecord.RecID, new Decimal?(pf.Amount), biBankVposRecord.Cur, "", new int?(), biBankVposRecord.Bank, pf.CardHolder, new DateTime?(), new short?(), new int?(-1), new int?(result), pos.RefNo, biPaymentFormData.ResNo, "", ref errorMsg))
                                return;
                            new BILib().SetResOptDate(biPaymentFormData.ResNo, new DateTime?(), -1, ref errorMsg);
                            new BILib().setReservationComplate(reservationCreateUserData, biPaymentFormData.ResNo, false, ref errorMsg);
                            lblPaymentStatus.Text = this.GetGlobalResourceObject("ResView", "msgPaymentSuccessFromCard").ToString();
                        }
                    }
                    else
                    {
                        if (!str3.Equals("5") && !str3.Equals("6") && (!str3.Equals("7") && !str3.Equals("8")))
                            return;
                        lblPaymentStatus.Text = this.GetGlobalResourceObject("ResView", "msgPaymentStatus" + str3).ToString();
                    }
                }
                else
                    lblPaymentStatus.Text = this.GetGlobalResourceObject("ResView", "msfPaymentSecurityFromCard").ToString();
            }
            catch (Exception ex)
            {
                lblPaymentStatus.Text = ex.StackTrace;
            }
        }
    }
    private static string GetSHA1(string SHA1Data)
    {
        return GetHexaDecimal(new SHA1CryptoServiceProvider().ComputeHash(System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(SHA1Data)));
    }

    private static string GetHexaDecimal(byte[] bytes)
    {
        StringBuilder stringBuilder = new StringBuilder();
        int length = bytes.Length;
        for (int index = 0; index <= length - 1; ++index)
            stringBuilder.Append(string.Format("{0,2:x}", (object)bytes[index]).Replace(" ", "0"));
        return stringBuilder.ToString();
    }
}