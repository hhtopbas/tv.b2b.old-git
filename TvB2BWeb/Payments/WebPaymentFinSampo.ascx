﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebPaymentFinSampo.ascx.cs" Inherits="themes_Detur_UserControls_WebPaymentFinSampo" %>
</form>
<form id="PostForm" name="PostForm" method="post" action="https://verkkopankki.danskebank.fi/SP/vemaha/VemahaApp"> 
        <input name="KNRO" type="hidden" value="<%=merchant_Number%>"/> 
        <input name="SUMMA" type="hidden" value="<%=Amount.ToString("#.00")%>"/> 
        <input name="VIITE" type="hidden" value="<%=ReservationNumber%>"/> 
        <input name="VALUUTTA" type="hidden" value="EUR"/> 
        <input name="VERSIO" type="hidden" value="4"/> 
        <INPUT NAME="ERAPAIVA" TYPE="HIDDEN" VALUE="<%= dueDate %>">
        <input name="OKURL" type="hidden" value="<%=retAddress%>"/> 
        <input name="VIRHEURL" type="hidden" value="<%=cancelAddress%>"/> 
        <input name="TARKISTE" type="hidden" value="<%=authentication_Mac%>"/> 
        <input name="lng" type="hidden" value="<%=language.ToString()%>"/> 
        <input name="ALG" TYPE="hidden" VALUE="03">
        <input type="image" src="https://www.danskebank.fi/verkko-palvelu/logo.gif" BORDER=0/>
</form> 

<script language="javascript" type="text/javascript">
    var vPostForm = document.PostForm;
    vPostForm.submit();
</script>