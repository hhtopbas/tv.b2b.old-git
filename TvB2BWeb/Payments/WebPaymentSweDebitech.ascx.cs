﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using TvBo;

public partial class Detur_UserControls_WebPaymentSweDebitech : System.Web.UI.UserControl
{
	private string reservationNumber;
	public string ReservationNumber
	{
		get { return reservationNumber; }
		set { reservationNumber = value; }
	}

	private decimal amount;
	public decimal Amount
	{
		get { return amount; }
		set { amount = value; }
	}

	protected string currency = "SEK";
	protected string mac;
	protected string data;
	protected string billingFirstName;
	protected string billingLastName;
	protected string billingAddress;
	protected string billingZipCode;
	protected string billingCity;
	protected string billingCountry;
	protected string eMail;
    protected string paymentType;
    protected string Reserve;

    protected string pageSet = "standardB2B"; //standart is in english, for different cultures, diffrent page sets are created for localization in detur control panel.. aykut..
    //standardB2B-se 
	private string SecretKey = "537959A35007E8C3885EDCFF50CC0EE0008EA30F";

	protected void Page_Load(object sender, EventArgs e)
	{

        #region culture set
        if (((User)Session["UserData"]).Ci.TwoLetterISOLanguageName.ToLower().IndexOf("sv") > -1)
        {
            pageSet += "-se";
        }
        #endregion

		if (this.Attributes["ReservationNumber"] != null)
		{
			reservationNumber = this.Attributes["ReservationNumber"].ToString();
		}

        if (this.Attributes["PayType"] != null)
        {
            paymentType = this.Attributes["PayType"].ToString();
        }
        if (this.Attributes["Reserve"] != null)
        {
          Reserve = this.Attributes["Reserve"].ToString();
        }

		if (this.Attributes["Amount"] != null)
		{
			amount = decimal.Parse(this.Attributes["Amount"].ToString());
		}

        if ((User)Session["UserData"] != null)
		{
			fillUserParams();
		}
        else Response.Redirect("~/../ResView.aspx?ResNo=" + reservationNumber);

		data = reservationNumber + ":" + "Detur Reservation:1:" + amount.ToString("#.00").Replace(",", "").Replace(".", "") + ":";
		mac = MD5Encode(data + "&" + currency + "&" + SecretKey + "&", SecretKey);
		mac = mac.Replace("-", "");
	}

	public string MD5Encode(string originalPassword, string key)
	{
		//Declarations
		Byte[] originalBytes;
		Byte[] encodedBytes;
		MD5 md5;


		//Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
		md5 = new MD5CryptoServiceProvider();
		originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
		encodedBytes = md5.ComputeHash(originalBytes);

		//Convert encoded bytes back to a 'readable' string
		return BitConverter.ToString(encodedBytes);
		//return encodedBytes.ToString();
	}

    private void fillUserParams()
    {
        User UserData = (User)Session["UserData"];
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        if (ResData == null) return;
        ResCustRecord resCust = ResData.ResCust.Find(f => f.Leader == "Y");
        if (resCust == null) return;
        ResCustInfoRecord resCustInfo = ResData.ResCustInfo.Find(f => f.CustNo == resCust.CustNo);
        if (resCustInfo == null) return;
        billingFirstName = resCust.Name;
        billingLastName = resCust.Surname;
        billingAddress = resCustInfo.InvoiceAddr == "H" ? resCustInfo.AddrHome : resCustInfo.AddrWork;
        billingZipCode = resCustInfo.InvoiceAddr == "H" ? resCustInfo.AddrHomeZip : resCustInfo.AddrWorkZip;
        billingCity = resCustInfo.InvoiceAddr == "H" ? resCustInfo.AddrHomeCity : resCustInfo.AddrWorkCity;
        billingCountry = resCustInfo.InvoiceAddr == "H" ? resCustInfo.AddrHomeCountry : resCustInfo.AddrWorkCountry;
        eMail = resCustInfo.InvoiceAddr == "H" ? resCustInfo.AddrHomeEmail : resCustInfo.AddrWorkEMail;
    }
}
