﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Configuration;

public partial class Payments_PaymentResult : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        string isPayLog = ConfigurationManager.AppSettings["PaymentLogIsActive"];
        bool paymentLog = false;
        if (!string.IsNullOrEmpty(isPayLog) && isPayLog.Equals("1"))
            paymentLog = true;

        User UserData = (User)Session["UserData"];
        if (UserData != null)
        {
            CultureInfo ci = UserData.Ci;
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
        }
       
        //lblResNo.Text = !string.IsNullOrEmpty(Request.Params["ResNo"]) ? (string)Request.Params["ResNo"] : "";

        //Status=E&Status_code=3        


        string errorMsg = string.Empty;
        string resNumber = Request.QueryString["resNumber"].ToString();
        if (paymentLog)
            WebLog.SaveWebServiceLog(null, null, "PaymentResultFi-39-ResNo:" + resNumber, "PaymentResultFi", null, null);
        string token = !string.IsNullOrEmpty(Request.Params["Reserve"]) ? Request.Params["Reserve"].ToString() : string.Empty;

        if (Request.QueryString["auriga"] == "true" && Request.QueryString["Status"] == "E")
        {
            Response.Redirect(ResolveClientUrl(String.Format("noPaymentResultFi.aspx?canceled=true&Status_code={0}&ResNumber={1}", Request.QueryString["Status_code"], resNumber)));
        }

        //if (Request["Reserve"] == "true") { ltrBookingResult.Text = resNumber; return; }
        decimal? amount;
        string amountStr = !string.IsNullOrEmpty(Request.QueryString["Amount"]) ? Request.QueryString["Amount"].ToString() : "";
        if (paymentLog)
            WebLog.SaveWebServiceLog(null, null, "PaymentResultFi-51-ResNo:" + resNumber, "PaymentResultFi", null, null);
        if (amountStr.IndexOf('.') > 0)
        {
            amountStr = amountStr.Remove(amountStr.IndexOf('.'), 1);
            amount = TvTools.Conversion.getDecimalOrNull(amountStr) / 100;
        }
        else if (amountStr.IndexOf(',') > 0)
        {
            amountStr = amountStr.Remove(amountStr.IndexOf(','), 1);
            amount = TvTools.Conversion.getDecimalOrNull(amountStr) / 100;
        }
        else amount = TvTools.Conversion.getDecimalOrNull(amountStr);
        Guid logId = Guid.NewGuid();
        string posReferance = !string.IsNullOrEmpty(Request.Params["Reserve"]) ? Request.Params["Reserve"].ToString() : "";
        if (string.IsNullOrEmpty(posReferance))
             Guid.TryParse(posReferance,out logId);
        if (String.IsNullOrEmpty(posReferance))
            posReferance = String.IsNullOrEmpty(Request.QueryString["ARKISTOINTITUNNUS"]) ? "" : Request.QueryString["ARKISTOINTITUNNUS"];
        if (!String.IsNullOrEmpty(Request.QueryString["transactionId"]))
            posReferance = String.IsNullOrEmpty(Request.QueryString["transactionId"]) ? "" : Request.QueryString["transactionId"];
        /*
         paymentTransactionId,requestVals,
         */
        string payTypeIdStr = TvTools.Conversion.getStrOrNull(Request.QueryString["PayType"]);
        TvBo.ReservationPaymentInfo resPaymentInfo = new TvBo.Reservation().getReservationPaymentInfo(resNumber, ref errorMsg);
        if (paymentLog && resPaymentInfo!=null)
            WebLog.SaveWebServiceLog(null, null, "PaymentResultFi-75-ResNo:" + resNumber, "PaymentResultFi", Newtonsoft.Json.JsonConvert.SerializeObject(resPaymentInfo), null);
        string showPaymentMessage = string.Empty;

        if (resPaymentInfo == null)
        {
            if (paymentLog)
                WebLog.SaveWebServiceLog(null, null, "PaymentResultFi-81-ResNo:" + resNumber, "resPaymentInfo is null", null, null);
            showPaymentMessage = HttpContext.GetGlobalResourceObject("LibraryResource", "ReservationPaymentProblemPleaseContactWithOperator").ToString() + ", " + resNumber;
            Response.Redirect(string.Format("ShowPaymentFinal.aspx?ResNo={0}&Message={1}&Amount={2}",
                                                    resNumber,
                                                    showPaymentMessage,
                                                    ""));
        }
        List<TvBo.PayTypeRecord> paymentTypes = new TvBo.Common().getPayType(resPaymentInfo.Market, ref errorMsg);
        if (paymentLog && paymentTypes!=null)
            WebLog.SaveWebServiceLog(null, null, "PaymentResultFi-90-ResNo:" + resNumber, "PaymentResultFi", Newtonsoft.Json.JsonConvert.SerializeObject(paymentTypes), null);
        TvBo.PayTypeRecord paymentType = paymentTypes.Find(f => f.Code == payTypeIdStr && f.Market == resPaymentInfo.Market);
        if (paymentLog && paymentType!=null)
            WebLog.SaveWebServiceLog(null, null, "PaymentResultFi-93-ResNo:" + resNumber, "PaymentResultFi", Newtonsoft.Json.JsonConvert.SerializeObject(paymentType), null);
        TvBo.ResDataRecord ResData = new TvBo.ResTables().getReservationData(null, resNumber, string.Empty, ref errorMsg);
        if (ResData == null)
        {
            if (paymentLog)
                WebLog.SaveWebServiceLog(null, null, "PaymentResultFi-ResNo:" + resNumber, "ResData is null", null, null);
            showPaymentMessage = HttpContext.GetGlobalResourceObject("LibraryResource", "ReservationPaymentProblemPleaseContactWithOperator").ToString() + ", " + resNumber;
            Response.Redirect(string.Format("ShowPaymentFinal.aspx?ResNo={0}&Message={1}&Amount={2}",
                                                    resNumber,
                                                    showPaymentMessage,
                                                    ""));
        }

        bool paymentOk = false;
        if (resPaymentInfo != null && paymentType != null)
        {
            if (!paymentOk)
            {
                TvBo.ResCustRecord leader = ResData.ResCust.Find(f => f.CustNo == resPaymentInfo.CustNo);
                if (leader == null) leader = ResData.ResCust.Find(f => f.Leader == "Y");

                string LeaderName = leader.Surname + " " + leader.Name;
                Int16 accountType = new Payments().getAccountTypeForPayment(ResData.ResMain.ResNo, ref errorMsg);
                if (paymentLog && accountType>-1)
                    WebLog.SaveWebServiceLog(null, null, "PaymentResultFi-117-ResNo:" + resNumber, "PaymentResultFi", Newtonsoft.Json.JsonConvert.SerializeObject(accountType), null);
                //var checkPayment = new TvBo.Reservation().IsExistsPayByReference(posReferance,amount, ref errorMsg);
                //if (!checkPayment)
                paymentOk = new TvBo.Reservation().MakeResPayment(resPaymentInfo.Agency, accountType, accountType == 0 ? resPaymentInfo.Agency : resPaymentInfo.CustNo.ToString(), accountType == 0 ? resPaymentInfo.Agency : LeaderName, DateTime.Today, paymentType.RecID,
                                                   amount, resPaymentInfo.SaleCur, "",
                                                    -1, "", "",
                                                    null, null, null, 0, posReferance, resNumber, "", ref errorMsg);
                //else
                //    paymentOk = true;
                
                if (paymentLog)
                    WebLog.SaveWebServiceLog(null, null, "PaymentResultFi-ResNo:" + resNumber, Newtonsoft.Json.JsonConvert.SerializeObject(Request.Url.Query), Newtonsoft.Json.JsonConvert.SerializeObject(paymentOk), null);

                if (!paymentOk)
                {
                    showPaymentMessage = HttpContext.GetGlobalResourceObject("LibraryResource", "ReservationPaymentProblemPleaseContactWithOperator").ToString() + ", " + resNumber;
                    Response.Redirect(string.Format("ShowPaymentFinal.aspx?ResNo={0}&Message={1}&Amount={2}",
                                                    resNumber,
                                                    showPaymentMessage,
                                                    ""));
                }
                else
                {
                    new Payments().closeCheckPayment(resNumber, token, ref errorMsg);
                    //Payment is accepted for booking number {bookingNR} [paidAmount] NOK is paid.
                    showPaymentMessage = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ReservationPaymentOK").ToString(),
                                        resNumber,
                                        !string.IsNullOrEmpty(Request.Params["Amount"]) ? (string)Request.Params["Amount"] + " " + resPaymentInfo.SaleCur : "");
                    Response.Redirect(string.Format("ShowPaymentFinal.aspx?ResNo={0}&Message={1}&Amount={2}",
                                                     resNumber,
                                                     showPaymentMessage,
                                                     !string.IsNullOrEmpty(Request.Params["Amount"]) ? (string)Request.Params["Amount"] + " " + resPaymentInfo.SaleCur : ""));
                }
            }
        }
    }
}

