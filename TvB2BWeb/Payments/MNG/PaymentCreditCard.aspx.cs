﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Threading;
using BankIntegration;
using TvTools;
using System.Web.Script.Services;
using System.Globalization;
using TvBo;


public partial class MNG_PaymentCreditCard : System.Web.UI.Page
{
    protected decimal amount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["CCData"] = null;
            bool? demo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("Payment", "SSLDemo"));
            if (!demo.HasValue || (demo.HasValue && demo == false))
            {
                string port = Request.ServerVariables["SERVER_PORT"].ToString();
                if (port == "80" || port == "8080")
                {
                    string secureURL;
                    secureURL = Request.Url.ToString().Replace("http://", "https://");
                    string HostStr = Request.Url.Host;
                    if (HostStr != "localhost" &&
                        HostStr.Split('.')[0].ToString() != "192" &&
                        HostStr.Split('.')[0].ToString() != "172" &&
                        HostStr.Split('.')[0].ToString() != "10")
                    {
                        try
                        {
                            if (string.Equals(Common.getDBOwner(), TvBo.Common.crID_Mng_Tr))
                                Response.Redirect(secureURL);
                        }
                        catch { }
                    }
                }
            }
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static MNGPaymentDetail getPaymentDetail(string PaymentCategory, string CardCode, string InstallNum)
    {
        MNGPaymentDetail retVal = new MNGPaymentDetail();

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        MNGPaymentFormData paymentFormData = (MNGPaymentFormData)HttpContext.Current.Session["CCData"];

        int? payCat = Conversion.getInt32OrNull(PaymentCategory);
        int? installNum = Conversion.getInt32OrNull(InstallNum);

        List<CardDetail> cardList = paymentFormData.paymentData.card_Detail.Where(w => w.PayCatID == payCat).ToList<CardDetail>();
        CardDetail card = new CardDetail();
        if (cardList.Count == 1)
            card = cardList.First();
        else card = cardList.Find(f => f.Code == CardCode && f.InstalNum == installNum);
        if (card != null)
        {
            retVal.PaymentType = card.FullName;
            retVal.Total = paymentFormData.txtTotal;
            retVal.Paid = card.PrePayment.HasValue ? (card.PrePayment.Value.ToString("#.00") + " " + card.Currency) : "";
            retVal.Payable = card.DiscountPrice.HasValue ? (card.DiscountPrice.Value.ToString("#.00") + " " + card.Currency) : "";
            retVal.MinPayable = card.MinPayPrice.HasValue ? (card.MinPayPrice.Value.ToString("#.00") + " " + card.Currency) : "";
            retVal.PaidAmount = card.DiscountPrice.ToString();
            retVal.PaidAmountCur = card.Currency;
            return retVal;
        }
        else
            return null;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CardDetail> getCreditCardDetail(string ID)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        MNGPaymentFormData paymentFormData = (MNGPaymentFormData)HttpContext.Current.Session["CCData"];

        List<CardDetail> cardDetails = paymentFormData.paymentData.card_Detail;
        List<CardDetail> retVal = (from q in cardDetails
                                   where q.CreditCardID == Conversion.getInt32OrNull(ID)
                                   select q).ToList<CardDetail>();
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> HowFindUs(string ResNo)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        MNGPaymentFormData paymentFormData = (MNGPaymentFormData)HttpContext.Current.Session["CCData"];
        return paymentFormData.howFindUs;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> paymentTypeData(string ResNo)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return null;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        PaymentInfo paymentInfoRes = new Payments().getPaymentInfoForReservation(ResNo, null, ref errorMsg);

        int? Country = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
        string CardCur = paymentInfoRes.SaleCur;
        int? catPackID = paymentInfoRes.PriceListNo;
        int? minPerVal = paymentInfoRes.InstalNum;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran)) CardCur = "EURO";
        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        MNGPaymentFormData paymentFormData = (MNGPaymentFormData)HttpContext.Current.Session["CCData"];
        List<PaymentTypeData> val = (List<PaymentTypeData>)paymentFormData.paymentTypeData;

        var retVal = from q in val
                     group q by new { Code = q.PayCatID, Name = q.PayCatName } into k
                     select new CodeName { Code = k.Key.Code.ToString(), Name = k.Key.Name };
        return retVal.ToList<CodeName>();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> getCreditCards()
    {
        if (HttpContext.Current.Session["CCData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        MNGPaymentFormData paymentFormData = (MNGPaymentFormData)HttpContext.Current.Session["CCData"];
        var retVal = from q in paymentFormData.paymentData.CardList
                     select new CodeName { Code = q.CreditCardID.ToString(), Name = q.CreditCardName };
        return retVal.ToList<CodeName>();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static MNGGetFormReturn getFormData(string ResNo, string external, int PaymentRule, string AccVal)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return null;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        MNGPaymentFormData retVal = new MNGPaymentFormData();
        int account = Conversion.getInt32OrNull(AccVal).HasValue ? Conversion.getInt32OrNull(AccVal).Value : -1;
        if (ResData.ResMain.ConfStat != 1)
            return new MNGGetFormReturn { Success = false, Message = "Ödeme için rezervasyon onayı beklemelisiniz.", ResNo = ResData.ResMain.ResNo };

        if (UserData.OwnAgency)
            retVal.DisabledPaidAmount = true;
        else
        {
            if (UserData.AgencyRec.WorkType == 3)
                retVal.DisabledPaidAmount = false;
            else retVal.DisabledPaidAmount = true;
            retVal.PosType = 1;
        }

        bool? usePaymentRule = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "UsePaymentRule"));
        if (usePaymentRule.HasValue && usePaymentRule.Value)
        {
            if (ResData.ResMain.PasPayRule == 1)
                retVal.PaymentRule = 1;
            IEnumerable accData = from q in ResData.ResCust
                                  orderby (string.Equals(q.Leader, "Y") ? 0 : 1)
                                  select new CodeName { Code = q.CustNo.ToString(), Name = q.TitleStr + "," + q.Name + " " + q.Surname };
            retVal.AccountData = accData.Cast<CodeName>();
        }

        PaymentInfo paymentInfoRes = new Payments().getPaymentInfoForReservation(ResNo, null, ref errorMsg);

        int? Country = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
        string CardCur = paymentInfoRes.SaleCur;
        int? catPackID = paymentInfoRes.PriceListNo;
        int? minPerVal = paymentInfoRes.InstalNum;
        decimal? price = paymentInfoRes.ResAmount;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran)) CardCur = "EURO";
        List<PaymentTypeData> paymentTypeData = new Payments().getPaymentTypeData(UserData.Market, ResData.ResMain.PLMarket, ResData.ResMain.PLOperator, UserData.AgencyID, CardCur, Country, catPackID, DateTime.Now, paymentInfoRes.ResBegDate, paymentInfoRes.Night, minPerVal, true, false, ref errorMsg);
        retVal.paymentTypeData = paymentTypeData;

        retVal.BankVPos = new BILib().getBankVPos(string.Empty, ref errorMsg);

        if (paymentInfoRes.PrePayment.HasValue && paymentInfoRes.PrePayment.Value > 0)
        {
            minPerVal = paymentInfoRes.InstalNum.HasValue ? Math.Abs(paymentInfoRes.InstalNum.Value) : -1;
            retVal.VisablePerDesc = true;
            retVal.VisableHowFindUs = false;
        }
        else
        {
            retVal.VisableHowFindUs = true;
            List<HowFindUs> howFindUsList = new BankIntegration.MNG().GetHowFindUs(UserData.Market, UserData.Operator, ref errorMsg);            
            retVal.howFindUs = howFindUsList.Select(s => new CodeName { Code = s.RecID.ToString(), Name = s.Name }).ToList<CodeName>();
        }

        retVal.txtTotal = price.ToString() + " " + paymentInfoRes.SaleCur;
        CreditCardAndDetail paymentData = createCardDetailTable(UserData, paymentTypeData, ResData.ResMain.ResNo, PaymentRule > 0 ? account : -1);
        retVal.paymentData = paymentData;
        HttpContext.Current.Session["CCData"] = retVal;

        return new MNGGetFormReturn { Success = true, Message = string.Empty, ResNo = ResData.ResMain.ResNo, UsePaymentRule = usePaymentRule.HasValue ? usePaymentRule.Value : false };
    }

    public static CreditCardAndDetail createCardDetailTable(User UserData, List<PaymentTypeData> paymentData, string ResNo, int? CustNo)
    {
        string errorMsg = string.Empty;
        PaymentInfo paymentInfoRes = new Payments().getPaymentInfoForReservation(ResNo, null, ref errorMsg);
        decimal? price = paymentInfoRes.ResAmount;
        decimal? remain = paymentInfoRes.Remain;
        decimal? PrePayment = paymentInfoRes.PrePayment.HasValue ? paymentInfoRes.PrePayment.Value : 0;
        decimal? disCountprice = paymentInfoRes.DiscountAmount;

        int catPackID = paymentInfoRes.PriceListNo.HasValue ? paymentInfoRes.PriceListNo.Value : -1;
        int minPerVal = paymentInfoRes.InstalNum.HasValue ? paymentInfoRes.InstalNum.Value : -1;
        string prePerVal = paymentInfoRes.PrePerVal.ToString();
        return new Payments().prepareCardDetailTable(UserData, paymentData, remain, PrePayment, disCountprice, minPerVal, prePerVal);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static returnObj getGetPayment(string ResNo, string Amount, string CardName, string CardNumber1, string CardNumber2, string CardNumber3, string CardNumber4, string CVC, string ExpDateM, string ExpDateY, string BankCode, string external)
    {
        string errorMsg = string.Empty;
        BIUser UserData = new BIUser();
        UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        BIResMainRecord resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);

        bool okey = false;
        returnObj retVal = new returnObj();
        List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(string.Empty, ref errorMsg);
        List<BIPayTypeRecord> payTypes = new BILib().getPayType(resMain.PLMarket, ref errorMsg);

        decimal? _amount = Conversion.getDecimalOrNull(Amount.Replace(",", ci.NumberFormat.NumberDecimalSeparator).Replace(".", ci.NumberFormat.NumberDecimalSeparator));

        string posReferans = "";
        string resNo = ResNo + "_" + DateTime.Now.ToString("ddMMhhmm");
        string cardNumber = string.Empty;

        cardNumber = CardNumber1 + CardNumber2 + CardNumber3 + CardNumber4;

        string cardNumberxxx = "";
        if (cardNumber != "")
        {
            try
            {
                cardNumberxxx = CardNumber1 + CardNumber2.Substring(0, 2) + "XXXXXXXX" + CardNumber4.Substring(1, 3);
            }
            catch { }
        }
        List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
        List<BIResCustInfoRecord> resCustInfo = new BILib().getResCustInfoList(UserData.Market, ResNo, ref errorMsg);
        string LeaderName = string.Empty;
        string LeaderPhone = string.Empty;
        int? CustNumber = null;
        try
        {
            var Ln = from RC in resCust
                     join RCI in resCustInfo on RC.CustNo equals RCI.CustNo
                     where string.Equals(RC.Leader, "Y")
                     select new
                     {
                         LeaderCustNo = RC.CustNo,
                         LeaderName = (!string.IsNullOrEmpty(RCI.CName) || !string.IsNullOrEmpty(RCI.CSurName)) ? RCI.CSurName + " " + RCI.CName : RC.Surname + " " + RC.Surname,
                         LeaderPhone = (RCI.ContactAddr == "H" ? RCI.AddrHomeTel : RCI.AddrWorkTel)
                     };

            if (Ln.Count() > 0)
            {
                CustNumber = Ln.FirstOrDefault().LeaderCustNo;
                LeaderName = Ln.FirstOrDefault().LeaderName;
                LeaderPhone = Ln.FirstOrDefault().LeaderPhone;
            }
            else
                LeaderName = resCust.Where(f => f.Leader == "Y").FirstOrDefault().Name + " " + resCust.Where(f => f.Leader == "Y").FirstOrDefault().Surname;
        }
        catch { }

        string retValue = string.Empty;

        BIPayTypeRecord payType = payTypes.Where(w => w.Code == BankCode).FirstOrDefault();
        if (payType == null) return new returnObj { Paid = 0, Message = "Pay type not found.", External = "" };
        BIBankVPosRecord vPos = bankVPosList.Find(f => f.Bank == payType.Bank);
        if (vPos == null) return new returnObj { Paid = 0, Message = "VPos not found.", External = "" };
        if (string.Equals(vPos.Interface, "YKB"))
        {
            string hostUrl = vPos.Host;
            string clientID = vPos.ClientID;//"400247195";
            string userID = vPos.UserID;//"vezirsatis";
            string pass = vPos.Password;//"V3z1rs@t1s";
            string bankcur = vPos.BankCur; //840 Euro
            retValue = VPosUtils.ConnectToYapiKrediPos(hostUrl, clientID, userID, cardNumber, ExpDateM, ExpDateY, CVC, resNo,
                (_amount.Value / 100).ToString(), bankcur, ref errorMsg, ref posReferans, "0", LeaderName, LeaderPhone);
            if (retValue == "1")
            {
                //   ViewState["OdemeOk"] = "true";            
            }
            else
            {
                return new returnObj { Paid = 0, Message = errorMsg, External = "" };
            }
        }
        else
        {
            #region ETS ye bağlan
            if (vPos == null) return new returnObj { Paid = 0, Message = "Bank info not found.", External = "" };
            string hostUrl = vPos.Host;
            string clientID = vPos.ClientID;//"400247195";
            string userID = vPos.UserID;//"vezirsatis";
            string pass = vPos.Password;//"V3z1rs@t1s";
            string bankcur = vPos.BankCur; //840 Euro

            retValue = VPosUtils.ConnectToVPos(hostUrl, clientID, userID, pass, bankcur, cardNumber, ExpDateM, ExpDateY, CVC, resNo,
                                        (_amount.Value / 100).ToString(), ref errorMsg, ref posReferans, "0", LeaderName, LeaderPhone, true);

            if (retValue == "1")
            {
                //   ViewState["OdemeOk"] = "true";            
            }
            else
            {
                return new returnObj { Paid = 0, Message = errorMsg, External = "" };
            }
            #endregion
        }

        BIResCustRecord leader = resCust.Where(f => f.Leader == "Y").FirstOrDefault();

        int PayTypeID = 19;
        switch (resMain.Market)
        {
            case "IRMARKET": PayTypeID = 19; break;
            case "LOCAL": PayTypeID = 20; break;
            case "MIDDLEEAST": PayTypeID = 23; break;
            default: PayTypeID = 19; break;
        }
        if (payType != null && payType.RecID.HasValue)
            PayTypeID = payType.RecID.Value;

        okey = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, PayTypeID,
                                                     _amount.Value / 100, "YTL", cardNumberxxx, null, BankCode, CardName,
                                                     null, null, -1, 0, posReferans, ResNo, "", ref errorMsg);
        if (okey)
        {
            string urlString = string.Empty;
           
            new BILib().SetResOptDate(ResNo, null, -1, ref errorMsg);
            new BILib().setReservationComplate(UserData, ResNo, false, ref errorMsg);

            return new returnObj { Paid = 1, Message = "Payment received. Reservation number " + ResNo + " Referans No " + posReferans, External = urlString };
        }
        else
        {
            return new returnObj { Paid = 0, Message = "Payment received. Payment could not be saved to the system. Please contact your operator. Reservation number " + ResNo + " Referans No " + posReferans, External = "" };
        }
    }
}

[Serializable()]
public class MNGPaymentFormData
{
    public MNGPaymentFormData()
    {
        _Error = false;
        _OwnAgency = false;
        _UsePaymentRule = false;
        _AccountData = new List<CodeName>();
        _howFindUs = new List<CodeName>();
        _paymentTypeData = new List<PaymentTypeData>();
        _paymentData = new CreditCardAndDetail();
        _bankVPos = new List<BIBankVPosRecord>();
        _visablePerDesc = false;
        _visableHowFindUs = true;
    }

    bool _Error;
    public bool Error
    {
        get { return _Error; }
        set { _Error = value; }
    }

    string _ErrMessage;
    public string ErrMessage
    {
        get { return _ErrMessage; }
        set { _ErrMessage = value; }
    }

    bool _OwnAgency;
    public bool OwnAgency
    {
        get { return _OwnAgency; }
        set { _OwnAgency = value; }
    }

    bool _DisabledPaidAmount;
    public bool DisabledPaidAmount
    {
        get { return _DisabledPaidAmount; }
        set { _DisabledPaidAmount = value; }
    }

    bool _VisablePosType;
    public bool VisablePosType
    {
        get { return _VisablePosType; }
        set { _VisablePosType = value; }
    }

    int? _PosType;
    public int? PosType
    {
        get { return _PosType; }
        set { _PosType = value; }
    }

    bool _UsePaymentRule;
    public bool UsePaymentRule
    {
        get { return _UsePaymentRule; }
        set { _UsePaymentRule = value; }
    }

    int? _PaymentRule;
    public int? PaymentRule
    {
        get { return _PaymentRule; }
        set { _PaymentRule = value; }
    }

    IEnumerable _AccountData;
    public IEnumerable AccountData
    {
        get { return _AccountData; }
        set { _AccountData = value; }
    }

    List<CodeName> _howFindUs;
    public List<CodeName> howFindUs
    {
        get { return _howFindUs; }
        set { _howFindUs = value; }
    }

    IEnumerable _paymentTypeData;
    public IEnumerable paymentTypeData
    {
        get { return _paymentTypeData; }
        set { _paymentTypeData = value; }
    }

    CreditCardAndDetail _paymentData;
    public CreditCardAndDetail paymentData
    {
        get { return _paymentData; }
        set { _paymentData = value; }
    }

    IEnumerable _bankVPos;
    public IEnumerable BankVPos
    {
        get { return _bankVPos; }
        set { _bankVPos = value; }
    }

    bool _visablePerDesc;
    public bool VisablePerDesc
    {
        get { return _visablePerDesc; }
        set { _visablePerDesc = value; }
    }

    bool _visableHowFindUs;
    public bool VisableHowFindUs
    {
        get { return _visableHowFindUs; }
        set { _visableHowFindUs = value; }
    }

    string _txtTotal;
    public string txtTotal
    {
        get { return _txtTotal; }
        set { _txtTotal = value; }
    }
}

public class MNGPaymentDetail
{
    public MNGPaymentDetail()
    {
    }

    string _PaymentType;
    public string PaymentType
    {
        get { return _PaymentType; }
        set { _PaymentType = value; }
    }

    string _Total;
    public string Total
    {
        get { return _Total; }
        set { _Total = value; }
    }

    string _Paid;
    public string Paid
    {
        get { return _Paid; }
        set { _Paid = value; }
    }

    string _Payable;
    public string Payable
    {
        get { return _Payable; }
        set { _Payable = value; }
    }

    string _MinPayable;
    public string MinPayable
    {
        get { return _MinPayable; }
        set { _MinPayable = value; }
    }

    string _PaidAmount;
    public string PaidAmount
    {
        get { return _PaidAmount; }
        set { _PaidAmount = value; }
    }

    string _PaidAmountCur;
    public string PaidAmountCur
    {
        get { return _PaidAmountCur; }
        set { _PaidAmountCur = value; }
    }

    string _RefeRans;
    public string RefeRans
    {
        get { return _RefeRans; }
        set { _RefeRans = value; }
    }
}

public class MNGGetFormReturn
{
    public MNGGetFormReturn()
    {
        _Success = false;
    }

    bool _Success;
    public bool Success
    {
        get { return _Success; }
        set { _Success = value; }
    }

    string _Message;
    public string Message
    {
        get { return _Message; }
        set { _Message = value; }
    }

    string _ResNo;
    public string ResNo
    {
        get { return _ResNo; }
        set { _ResNo = value; }
    }

    string _TotalPrice;
    public string TotalPrice
    {
        get { return _TotalPrice; }
        set { _TotalPrice = value; }
    }

    bool _UsePaymentRule;
    public bool UsePaymentRule
    {
        get { return _UsePaymentRule; }
        set { _UsePaymentRule = value; }
    }
}

public class returnObj
{
    public returnObj()
    {
        _Paid = 0;
        _Message = string.Empty;
        _external = string.Empty;
    }

    Int16? _Paid;
    public Int16? Paid
    {
        get { return _Paid; }
        set { _Paid = value; }
    }

    string _Message;
    public string Message
    {
        get { return _Message; }
        set { _Message = value; }
    }

    string _external;
    public string External
    {
        get { return _external; }
        set { _external = value; }
    }
}