﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentCreditCard.aspx.cs"
    Inherits="MNG_PaymentCreditCard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Begin Payment</title>

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showAlert(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                position: 'center',
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                        $(this).dialog('close');
                        return true;
                    }
                }
            });
        }

        function changedPaymentRule(value) {

        }

        function btnCancelClick() {            
            self.window.close();
        }

        function btnOKClick() {            
            $.query = $.query.load(location.href);
            $("#hfResNo").val($.query.get('ResNo'));
            var atlas = '';
            var bank = '';

            var resNo = $("#hfResNo").val();

            $.ajax({
                async: false,
                type: "POST",
                url: "PaymentCreditCard.aspx/getGetPayment",
                data: '{"ResNo":"' + $("#hfResNo").val() + '"' +
                      ',"Amount":"' + $("#amountVal").val() + '"' +
                      ',"CardName":"' + $("#txtCardHolderName").val() + '"' +
                      ',"CardNumber1":"' + $("#txtCardNumber1").val() + '"' +
                      ',"CardNumber2":"' + $("#txtCardNumber2").val() + '"' +
                      ',"CardNumber3":"' + $("#txtCardNumber3").val() + '"' +
                      ',"CardNumber4":"' + $("#txtCardNumber4").val() + '"' +
                      ',"CVC":"' + $("#txtSecurityCode").val() + '"' +
                      ',"ExpDateM":"' + $("#cmbExpireMonth").val() + '"' +
                      ',"ExpDateY":"' + $("#cmbExpireYear").val() + '"' +
                      ',"BankCode":"' + $("#hfBankCode").val() + '"' +
                      ',"external":"' + $("#hfExternal").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d')) {
                        try {
                            var data = msg.d;
                            if (data.Paid == 1) {
                                $("#messages").html(data.Message);
                                $("#dialog").dialog("destroy");
                                $("#dialog-message").dialog({
                                    position: 'center',
                                    modal: true,
                                    buttons: {
                                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                                            $(this).dialog('close');
                                            if (data.External != '')
                                                window.location = data.External.replace('https:', 'http:');
                                            else
                                                self.window.close();
                                            return true;
                                        }
                                    }
                                });
                            }
                            else if (data.Paid == 0) {
                                $("#messages").html(data.Message);
                                $("#dialog").dialog("destroy");
                                $("#dialog-message").dialog({
                                    position: 'center',
                                    modal: true,
                                    buttons: {
                                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                                            $(this).dialog('close');                                            
                                        }
                                    }
                                });
                            }
                            else {
                                self.window.close();
                            }

                        }
                        catch (err) {
                            showAlert(err);
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function noNumbers(kontrol, sayi, e) {
            var keynum;
            var keychar;
            var number;
            var unicode = e.charCode ? e.charCode : e.keyCode
            var numcheck; if (window.event) // IE
            {
                keynum = e.keyCode;
                number = 4;
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                keynum = e.which;
                number = 3;
            }
            if (keynum == 8 || e.charcode)
                return true;
            keychar = String.fromCharCode(keynum);
            numcheck = /\d/;

            if (keychar == null)
                return true;

            if (numcheck.test(keychar)) {
                if (kontrol == 'cs1')
                    cs1(sayi, number);
                if (kontrol == 'cs2')
                    cs2(sayi, number);
                if (kontrol == 'cs3')
                    cs3(sayi, number);
                return true;
            }
            else if (keynum < 48 || keynum > 57)
                return false;
            else
                return true;
        }

        function cs1(sayi, number) {
            if (sayi == number) { $("#txtCardNumber2").focus(); }
        }

        function cs2(sayi, number) {
            if (sayi == number) { $("#txtCardNumber3").focus(); }
        }

        function cs3(sayi, number) {
            if (sayi == number) { $("#txtCardNumber4").focus(); }
        }

        function getHowFindUs(resNo) {
            $.ajax({
                async: false,
                type: "POST",
                url: "PaymentCreditCard.aspx/howFindUs",
                data: '{"ResNo":"' + resNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#rbWhereToHitDiv").html('');
                        $.each(data, function(i) {
                            $("#rbWhereToHitDiv").append('<input id="rbWhereToHit' + i + '" name="rbWhereToHit" type="radio" value="' + this.Code + '" /><label for="rbWhereToHit' + i + '">' + this.Name + '</label><br />');
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function showPaymentDetail(payCat, cardCode, installNum) {
            var obj = new Object();
            obj.PaymentCategory = payCat;
            obj.CardCode = cardCode;
            obj.InstallNum = installNum;
            $.ajax({
                async: false,
                type: "POST",
                url: "PaymentCreditCard.aspx/getPaymentDetail",
                data: $.json.encode(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#txtPaymentType").html(data.PaymentType);
                        $("#txtTotal").html(data.Total);
                        $("#txtPaid").html(data.Paid);
                        $("#txtPayable").html(data.Payable);
                        $("#txtMinPayable").html(data.MinPayable);
                        $("#amountVal").val(data.PaidAmount);
                        $("#txtCur").html(data.PaidAmountCur);                        
                    }
                },
                error: function(xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            }); 
        }

        function changeCreditCardDetail(value, installNum, seqNo) {
            $("#cardDetailTable td").css('background-color', '#FFF')
            $('img[name="visaImg"]').attr("src", "VisaMaster.jpg");
            $("#img_" + seqNo).attr("src", "selectVisaMaster.jpg");
            $("#tr_" + seqNo + " > td").css('background-color', '#999');
            showPaymentDetail(3, value, installNum);
        }

        function changeCreditCard(value) {
            $.ajax({
                async: false,
                type: "POST",
                url: "PaymentCreditCard.aspx/getCreditCardDetail",
                data: '{"ID":"' + value + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#cardDetail").html('');
                        var cCode = '';
                        var html = '';
                        html += '<table id="cardDetailTable" width="100%">';
                        html += '<tr>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 50px;"><strong>Seç</strong></td>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px;"><strong>Taksit seçenekleri</strong></td>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 90px;"><strong>Fiyat</strong></td>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 90px;"><strong>En az ödeme</strong></td>';
                        html += '<td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 50px;"><strong>Kur tipi</strong></td>';
                        html += '</tr>';
                        $.each(data, function(i) {
                            html += '<tr id="tr_' + i + '" onclick=changeCreditCardDetail("' + this.Code + '","' + this.InstalNum + '",' + i + ');>';
                            if (i == 0) cCode = this.Code;
                            html += '<td style="width: 50px;"><img id="img_' + i + '" name="visaImg" alt="" src="VisaMaster.jpg" /></td>';
                            html += '<td>' + this.FullName + '</td>';
                            html += '<td style="width: 90px; text-align: right;">' + this.DiscountPrice + '</td>';
                            html += '<td style="width: 90px; text-align: right;">' + this.MinPayPrice + '</td>';
                            html += '<td style="width: 50px;">&nbsp;&nbsp;' + this.Currency + '</td>';
                            html += '</tr>';
                        });
                        html += '</table>';
                        $("#cardDetail").html(html);
                        changeCreditCardDetail(cCode, null, null);
                    }
                },
                error: function(xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getCreditCards() {
            $.ajax({
                async: false,
                type: "POST",
                url: "PaymentCreditCard.aspx/getCreditCards",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#cardList").html('');
                        var cCode = '';
                        $.each(data, function(i) {
                            if (i == 0) {
                                $("#cardList").append('<input id="rbCreditCard' + i + '" name="rbCreditCard" type="radio" value="' + this.Code + '" onclick="changeCreditCard(this.value);" checked="checked"/><label for="rbCreditCard' + i + '">' + this.Name + '</label><br />');
                                cCode = this.Code;
                            }
                            else $("#cardList").append('<input id="rbCreditCard' + i + '" name="rbCreditCard" type="radio" value="' + this.Code + '" onclick="changeCreditCard(this.value);" /><label for="rbCreditCard' + i + '">' + this.Name + '</label><br />');
                        });
                        changeCreditCard(cCode);
                    }
                },
                error: function(xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function changePaymentType(value) {
            if (value == undefined || value == '') return;
            if (parseInt(value) != 3) {
                $("#divCreditCard").hide();
                $("#divCardInformation").hide();
                $("#trReferans").show();
                showPaymentDetail(value, null, null);
            }
            else {
                getCreditCards();
                $("#divCreditCard").show();
                $("#divCardInformation").show();
                $("#trReferans").hide();
            }
        }

        function getPaymentType(resNo) {
            $.ajax({
                async: false,
                type: "POST",
                url: "PaymentCreditCard.aspx/paymentTypeData",
                data: '{"ResNo":"' + resNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#rbPaymentTypeDiv").html('');
                        var payType = '';
                        $.each(data, function(i) {
                            if (i == 0) {
                                payType = this.Code;
                                $("#rbPaymentTypeDiv").append('<input id="rbPaymentTypes' + i + '" name="rbPaymentTypes" type="radio" value="' + this.Code + '" onclick="changePaymentType(this.value);" checked="checked"/><label for="rbPaymentTypes' + i + '">' + this.Name + '</label><br />');
                            }
                            else $("#rbPaymentTypeDiv").append('<input id="rbPaymentTypes' + i + '" name="rbPaymentTypes" type="radio" value="' + this.Code + '" onclick="changePaymentType(this.value);" /><label for="rbPaymentTypes' + i + '">' + this.Name + '</label><br />');
                        });
                        changePaymentType(payType);
                    }
                },
                error: function(xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getFormData(resNo, external) {
            var params = new Object();
            params.ResNo = resNo;
            params.external = external;
            params.PaymentRule = 0;
            params.AccVal = 0;
            $.ajax({
                async: false,
                type: "POST",
                url: "PaymentCreditCard.aspx/getFormData",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $("#txtResNo").html(data.ResNo);
                        $("#txtTotal").html(data.TotalPrice);
                        if (data.Success) {
                            getHowFindUs(resNo);
                            getPaymentType(resNo);
                        }
                        else {
                            showAlert(data.Message);
                        }
                        if (data.UsePaymentRule != null && data.UsePaymentRule == false) {
                            $("#trPaymentRule").hide();
                            $("#trPaymentAccount").hide();
                        }
                        $("#messageDiv").hide();
                        $("#mainDiv").show();
                    }
                },
                error: function(xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $.query = $.query.load(location.href);
                $("#hfResNo").val($.query.get('ResNo'));
                var resNo = $("#hfResNo").val();
                getFormData(resNo, '');
            });
    </script>

    <style type="text/css">
        .style1 { height: 24px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hfResNo" type="hidden" />
    <div id="mainDiv" style="text-align: center; display: block;">
        <div style="width: 605px; text-align: center;">
            <div id="divWhereToHit" style="margin-left: 5px;">
                <fieldset>
                    <legend>
                        <label>
                            <span style="font-size: 12pt;">»</span></label>
                        <span style="font-weight: bold; font-size: 12pt;">Bizi nereden buldunuz ? </span>
                    </legend>
                    <div id="rbWhereToHitDiv" style="text-align: left;">
                    </div>
                    <br />
                </fieldset>
            </div>
            <div style="margin-left: 5px;">
                <fieldset>
                    <legend>
                        <label>
                            <span style="font-size: 12pt;">»</span></label>
                        <span style="font-weight: bold; font-size: 12pt;">Ödeme Tipi</span> </legend>
                    <span id="tdPerDesc" style="font-weight: bold; background-color: #CC0000; color: #FFF;
                        font-size: 7pt; display: none;">Uyarı:Daha önce bu rezervasyona ait ödeme yapıldı
                        ise, ödeme indirimi oranları sıfır (0) olarak gözükecektir. </span>
                    <div id="rbPaymentTypeDiv" style="text-align: left;">
                    </div>
                    <br />
                </fieldset>
            </div>
            <div id="divCreditCard" style="margin-left: 5px; display: none;">
                <fieldset>
                    <legend>
                        <label>
                            <span style="font-size: 12pt;">»</span></label>
                        <span style="font-weight: bold; font-size: 12pt;">Kredi kartları</span> </legend>
                    <div id="rbCreditCardDiv" style="text-align: left;">
                        <div id="cardList">
                        </div>
                        <br />
                        <div id="cardDetail">
                        </div>
                    </div>
                    <br />
                    <%--<table width="100%">
                        <tr>
                            <td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 50px;">
                                <strong>Seç</strong>
                            </td>
                            <td style="background-color: #1C5E55; color: #FFF; height: 20px;">
                                <strong>Taksit seçenekleri</strong>
                            </td>
                            <td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 90px;">
                                <strong>Fiyat</strong>
                            </td>
                            <td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 90px;">
                                <strong>En az ödeme</strong>
                            </td>
                            <td style="background-color: #1C5E55; color: #FFF; height: 20px; width: 50px;">
                                <strong>Kur tipi</strong>
                            </td>
                        </tr>
                    </table>--%>
                </fieldset>
            </div>
            <div style="width: 600px; font-family: Tahoma; text-align: left; border: solid 1px #666;">
                <div style="text-align: center; border: solid 1px #666;">
                    <fieldset>
                        <legend style="line-height: 2;">
                            <label>
                                <span style="font-size: 12pt;">»</span></label>
                            <strong style="font-size: 12pt;">Seçilen ödeme bilgisi</strong> </legend>
                        <br />
                        <table style="font-size: 10pt; text-align: left; width: 99%;">
                            <tr>
                                <td align="right" style="height: 24px; width: 200px;">
                                    Reservation number :
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <strong><span id="txtResNo">ResNo</span></strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr id="trPaymentRule">
                                <td align="right" style="height: 24px;">
                                    Ödeme kuralı :
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <select id="ddlPaymentRule">
                                    </select>
                                </td>
                            </tr>
                            <tr id="trPaymentAccount">
                                <td align="right" style="height: 24px;">
                                    Ödeyen :
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <select id="paymentRule" onchange="changedPaymentRule(this.value)">
                                        <option value="0" selected="selected">Genel ödeme</option>
                                        <option value="1">Kişli başı ödeme</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;">
                                    Ödeme tipi :
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <span id="txtPaymentType" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="style1">
                                    Toplam tutar :
                                </td>
                                <td class="style1">
                                    &nbsp;
                                </td>
                                <td align="left" class="style1">
                                    <span id="txtTotal" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;">
                                    Ödenen tutar :
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <span id="txtPaid" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;">
                                    Ödenmesi gereken :
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <span id="txtPayable" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;">
                                    En Az Ödemesi Gereken :
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <span id="txtMinPayable" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="height: 24px;">
                                    Ödenecek Tutar :
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <input id="amountVal" style="width: 120px;" type="text" />
                                    <span id="txtCur" style="font-weight: bold;"></span>
                                </td>
                            </tr>
                            <tr id="trReferans">
                                <td align="right" style="height: 24px;">
                                    Reference :
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <input id="txtReferance" style="width: 90%;" type="text" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                </div>
                <br />
                <div id="divCardInformation" style="text-align: center; border: solid 1px #666;">
                    <fieldset>
                        <legend style="line-height: 2;">
                            <label>
                                <span style="font-size: 12pt;">»</span></label>
                            <strong style="font-size: 12pt;">Kredi kartı bilgileri</strong> </legend>
                        <br />
                        <table style="font-size: 10pt; text-align: left; width: 99%;">
                            <tr>
                                <td style="text-align: right; width: 200px;">
                                    <span>Pos tipi :</span>
                                </td>
                                <td style="text-align: right">
                                    &nbsp;
                                </td>
                                <td>
                                    <select id="cmbPosTipi" style="width: 152px;">
                                        <option value="0">Pos</option>
                                        <option value="1">Sanal Pos</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <span>Kart sahibi :</span>
                                </td>
                                <td style="text-align: right">
                                    &nbsp;
                                </td>
                                <td>
                                    <input id="txtCardHolderName" type="text" style="width: 98%;" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <span>Kart numarası :</span>
                                </td>
                                <td style="text-align: right">
                                    &nbsp;
                                </td>
                                <td>
                                    <input id="txtCardNumber1" maxlength="4" onkeypress="return noNumbers('cs1',this.value.length,event);"
                                        size="4" style="width: 47px;" />
                                    <input id="txtCardNumber2" maxlength="4" onkeypress="return noNumbers('cs2',this.value.length,event);"
                                        size="4" style="width: 47px;" />
                                    <input id="txtCardNumber3" maxlength="4" onkeypress="return noNumbers('cs3',this.value.length,event);"
                                        size="4" style="width: 47px;" />
                                    <input id="txtCardNumber4" maxlength="4" onkeypress="return noNumbers('cs4',this.value.length,event);"
                                        size="4" style="width: 47px;" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <span>Güvenlik Numarası (CVC) :</span>
                                </td>
                                <td style="text-align: right">
                                    &nbsp;
                                </td>
                                <td>
                                    <input id="txtSecurityCode" maxlength="3" style="width: 33px;" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <span>Kart tipi :</span>
                                </td>
                                <td style="text-align: right">
                                    &nbsp;
                                </td>
                                <td>
                                    <select id="cmbCardType" style="width: 152px;">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <span>Son kullanma tarihi :</span>
                                </td>
                                <td style="text-align: right">
                                    &nbsp;
                                </td>
                                <td>
                                    <select id="cmbExpireMonth" style="width: 50px;">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                    <select id="cmbExpireYear" style="width: 50px;">
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="19">20</option>
                                        <option value="19">21</option>
                                        <option value="19">22</option>
                                        <option value="19">23</option>
                                        <option value="19">24</option>
                                        <option value="19">25</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <input id="chkVada" type="checkbox" />
                                    <label for="chkVada">
                                        Vada seçenekleri</label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div id="rblVadaOptionsDiv">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </fieldset>
                </div>
                <div id="finalPanel">
                    <table width="100%">
                        <tr>
                            <td style="text-align: left;">
                                <textarea id="txtSozlesme" style="width: 98%; height: 175px;" cols="1"></textarea>
                                <br />
                                <input id="AcceptChk" type="checkbox" style="color: #FF0033; font-weight: bold;" />
                                <label for="AcceptChk">
                                    Ödeme şartlarını okudum ve kabul ediyorum.</label>
                            </td>
                        </tr>
                        <tr align="center">
                            <td style="height: 26px">
                                <input id="btnOK" type="button" value="Tamam" style="height: 30px; width: 100px;"
                                    class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                    onclick="btnOKClick()" />
                                <input id="btnCancel" type="button" value="Vazgeç - Geri dön" style="height: 30px;"
                                    class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                    onclick="btnCancelClick()" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <img src="GlobalSign.gif" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <img src="Visa.gif" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
        style="display: none;">
        <div id="messages">
            Message</div>
    </div>
    </form>
</body>
</html>
