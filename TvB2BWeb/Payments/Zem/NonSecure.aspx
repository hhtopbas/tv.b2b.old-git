﻿<%@ Page Language="C#" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<head id="Head1" runat="server">
    <title>Non Secure Payment</title>
</head>
<body>
    <%
    
        if (Request.Form.Count > 0)
        {

            /*Moto-ecommerce işlem için gerekli alanlar gönderim için listeye ekleniyor.*/
            String format = "{0}={1}&";
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(format, "ShopCode", "12345");
            sb.AppendFormat(format, "PurchAmount", Request.Form["Amount"]);
            sb.AppendFormat(format, "Currency", "949");
            sb.AppendFormat(format, "OrderId", Request.Form["OrderId"]);
            sb.AppendFormat(format, "InstallmentCount", Request.Form["InstallmentCount"]);
            sb.AppendFormat(format, "TxnType", Request.Form["TxnType"]);
            sb.AppendFormat(format, "orgOrderId", Request.Form["orgOrderId"]);
            sb.AppendFormat(format, "UserCode", "vpos1");
            sb.AppendFormat(format, "UserPass", "1");
            sb.AppendFormat(format, "SecureType", "NonSecure");
            sb.AppendFormat(format, "Pan", Request.Form["Pan"]);
            sb.AppendFormat(format, "Expiry", Request.Form["Expiry"]);
            sb.AppendFormat(format, "Cvv2", Request.Form["Cvv2"]);
            sb.AppendFormat(format, "BonusAmount", Request.Form["BonusAmount"]);
            sb.AppendFormat(format, "CardType", Request.Form["CardType"]);
            sb.AppendFormat(format, "Lang", "TR");
            sb.AppendFormat(format, "MOTO", "0");
            sb.AppendFormat(format, "Description", Request.Form["Description"]);
            //Eğer 3D doğrulaması yapılmış ise aşağıdaki alanlar da gönderilmelidir*/							/*
            //sb.AppendFormat(format, "PayerAuthenticationCode", Request.Form["PayerAuthenticationCode"]);
            //sb.AppendFormat(format, "Eci", Request.Form["Eci"]);//Visa - 05,06 MasterCard 01,02 olabilir	
            //sb.AppendFormat(format, "PayerTxnId", Request.Form["PayerTxnId"]);	




            try
            {
                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://93.94.199.122:8085/MPI/Default.aspx");

                byte[] parameters = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(sb.ToString());
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = parameters.Length;
                System.IO.Stream requeststream = request.GetRequestStream();
                requeststream.Write(parameters, 0, parameters.Length);
                requeststream.Close();
                Response.Write(sb.ToString());

                System.Net.HttpWebResponse resp = (System.Net.HttpWebResponse)request.GetResponse();
                System.IO.StreamReader responsereader = new System.IO.StreamReader(resp.GetResponseStream(), System.Text.Encoding.GetEncoding("ISO-8859-9"));

                String responseStr = responsereader.ReadToEnd();

                if (responseStr != null)
                {
                    string[] paramArr = responseStr.Split(';', ';');
    %><table>
        <%
                    foreach (string p in paramArr)
                    {
                        string[] nameValue = p.Split('=');
        %><tr>
            <td>
                <%
                    Response.Write(nameValue[0]);
                %>
            </td>
            <td>
                <% if (nameValue.Length > 1) Response.Write(nameValue[1]);%>
            </td>
        </tr>
        <%}
        %></table>
    <%
           
                }


            }
            catch (Exception ex)
            {
                Response.Write("Hata " + ex.Message + " " + ex.StackTrace + (ex.InnerException != null ? ex.InnerException.Message : ""));
            }
        } 
		
    %>
    <center>
        <form method="post">
        <table>
            <tr>
                <td>
                    Kredi Kart Numarası:
                </td>
                <td>
                    <input type="text" name="Pan" size="20" value="" />
            </tr>
            <tr>
                <td>
                    Güvenlik Kodu:
                </td>
                <td>
                    <input type="text" name="Cvv2" size="4" value="" />
                </td>
            </tr>
            <tr>
                <td>
                    Son Kullanma Tarihi (MMYY):
                </td>
                <td>
                    <input type="text" name="Expiry" value="" />
                </td>
            </tr>
            <tr>
                <td>
                    Tutar:
                </td>
                <td>
                    <input type="text" name="Amount" value="2.50" />
                </td>
            </tr>
            <tr>
                <td>
                    Kur secimi
                </td>
                <td>
                    <select name="Currency">
                        <option value="949">TL</option>
                        <option value="840">USD</option>
                        <option value="978">EUR</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Bonus:
                </td>
                <td>
                    <input type="text" name="BonusAmount" value="" />
                </td>
            </tr>
            <tr>
                <td>
                    Taksit Sayısı:
                </td>
                <td>
                    <input type="text" name="InstallmentCount" value="" />
                </td>
            </tr>
            <tr>
                <td>
                    Visa/MC secimi
                </td>
                <td>
                    <select name="CardType">
                        <option value="0">Visa</option>
                        <option value="1">MasterCard</option>
                        <option value="3">Amex</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Order No
                </td>
                <td>
                    <input type="text" name="OrderId" value="" />
                </td>
            </tr>
            <tr>
                <td>
                    İşlem Tipi
                </td>
                <td>
                    <select name="TxnType">
                        <option value="Auth" selected="selected">Auth</option>
                        <option value="PreAuth">PreAuth</option>
                        <option value="PostAuth">PostAuth</option>
                        <option value="Refund">Refund</option>
                        <option value="Void">Void</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Orijinal İşlem Order No
                </td>
                <td>
                    <input type="text" name="OrgOrderId" value="" />
                </td>
            </tr>
            <tr>
                <td>
                    Description
                </td>
                <td>
                    <input type="text" name="Description" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <input type="submit" value="Ödemeyi Tamamla" />
                </td>
            </tr>
        </table>
        </form>
    </center>
</body>
</html> 