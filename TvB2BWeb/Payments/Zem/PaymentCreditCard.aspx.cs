﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using BankIntegration;
using TvTools;
using System.Web.Script.Services;
using System.Globalization;
using System.Text;

public partial class PaymentCreditCard : System.Web.UI.Page
{
    protected decimal amount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(string ResNo, string external, string PayType)
    {
        string errorMsg = string.Empty;
        BIUser UserData = new BIUser();
        decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
        decimal? _payAmount = _amount;
        int? payTypeID = 0;
        BIResMainRecord resMain = new BIResMainRecord();
        BIPayTypeRecord payTypeRec = null;
        string baseCurr = string.Empty;

        if (_amount.HasValue)
        {
            UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
            CultureInfo ci = BILib.getCultureInfo();
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
            if (resMain != null)
            {
                List<BIPayTypeRecord> payTypes = new BILib().getPayType(resMain.PLMarket, ref errorMsg);
                payTypeRec = payTypes.Find(f => f.Code == PayType);
                if (payTypes != null)
                    payTypeID = payTypes.Find(f => f.Code == PayType).RecID;
                string saleCur = resMain.SaleCur;
                baseCurr = saleCur;
                //if (!string.Equals(baseCurr, saleCur))
                //{
                //    _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                //}
            }
        }

        List<BIHtmlCodeData> htmlData = new List<BIHtmlCodeData>();
        htmlData.Add(new BIHtmlCodeData { IdName = "txtResNo", TagName = "span", Data = resMain.ResNo });
        htmlData.Add(new BIHtmlCodeData { IdName = "txtAmount", TagName = "span", Data = _amount.HasValue ? (_payAmount.HasValue ? _payAmount.Value.ToString("#,###.00") + " " + resMain.SaleCur : "") : "" });
        htmlData.Add(new BIHtmlCodeData { IdName = "amountVal", TagName = "", Data = _amount.HasValue ? _amount.Value.ToString("#.") : "" });

        List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(string.Empty, ref errorMsg);
        BIBankVPosRecord bankVPos = bankVPosList.Find(f => f.Bank == payTypeRec.Bank && f.Cur == baseCurr);

        if (bankVPos == null) return string.Empty;
        string strOrderId = String.Format("{0}", ResNo + "_" + DateTime.Now.ToString("HHmm"));

        decimal balance = _amount.HasValue ? _amount.Value : (decimal)0;

        string successURL = String.Format("{0}?ResNumber={1}&PayType={2}&PayTypeId={3}",
            bankVPos.SuccessUrlB2B,
            ResNo,
            PayType,
            payTypeID.ToString());
        string errorURL = String.Format("{0}?ResNumber={1}&PayType={2}",
            bankVPos.ErrorUrlB2B,
            ResNo,
            PayType);
        //string storeKey = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["UPJ_StoreKey"]);

        String shopCode = bankVPos.ClientID;   //Banka tarafından verilen üye işyeri mağaza numarası     
        String purchaseAmount = GetPrice(_amount); //İşlem tutarı
        String curr = "949";// Kur Bilgisi - 949 TL
        if (baseCurr == "EUR")
            curr = "978";
        else
            if (baseCurr == "USD")
                curr = "840";
        String orderId = strOrderId;  //Sipariş Numarası
        String okUrl = successURL;     //İşlem başarılıysa yönlendirilecek sayfa
        String failUrl = errorURL;   //İşlem başarısızsa yönlendirilecek sayfa
        String rnd = DateTime.Now.ToString();  //Her işlemde değişen bir değer olmalıdır
        String installmentCount = "";      //Taksit sayısı
        String txnType = "Auth"; // İşlem tipi
        String merchantPass = bankVPos.MerchantID;//bankVPos.Password;  //İş yeri anahtarı
        String str = shopCode + orderId + purchaseAmount + okUrl + failUrl + txnType + installmentCount + rnd + merchantPass;
        System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
        byte[] bytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(str);
        byte[] hashingbytes = sha.ComputeHash(bytes);
        String hash = Convert.ToBase64String(hashingbytes);

        htmlData.Add(new BIHtmlCodeData { IdName = "ShopCode", TagName = "", Data = shopCode });
        htmlData.Add(new BIHtmlCodeData { IdName = "PurshAmount", TagName = "", Data = purchaseAmount });
        htmlData.Add(new BIHtmlCodeData { IdName = "Currency", TagName = "", Data = curr }); //TL
        htmlData.Add(new BIHtmlCodeData { IdName = "OrderId", TagName = "", Data = orderId });
        htmlData.Add(new BIHtmlCodeData { IdName = "OkUrl", TagName = "", Data = okUrl });
        htmlData.Add(new BIHtmlCodeData { IdName = "FailUrl", TagName = "", Data = errorURL });
        htmlData.Add(new BIHtmlCodeData { IdName = "Rnd", TagName = "", Data = rnd });
        htmlData.Add(new BIHtmlCodeData { IdName = "Hash", TagName = "", Data = hash });
        htmlData.Add(new BIHtmlCodeData { IdName = "TxnType", TagName = "", Data = txnType });
        htmlData.Add(new BIHtmlCodeData { IdName = "InstallmentCount", TagName = "", Data = installmentCount });
        htmlData.Add(new BIHtmlCodeData { IdName = "SecureType", TagName = "", Data = "3DHost" });
        htmlData.Add(new BIHtmlCodeData { IdName = "Lang", TagName = "", Data = "ru" });
/*

<input type="hidden" id="hfPan" name="Pan" value="" />
<input type="hidden" id="hfCvv2" name="Cvv2" value="" />
<input type="hidden" id="hfExpiry" name="Expiry" value="" />
<input type="hidden" id="hfCardType" name="CardType" value="" />

*/
        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }

    private static string GetPrice(decimal? price)
    {
        if (!price.HasValue) return "00";

        return (price.Value).ToString("#.00");
        //return Math.Floor(price.Value).ToString("#.00");
    }

    [WebMethod(EnableSession = true)]
    public static object noneSecurePayment(string ResNo, string external, string PayType, string cardNumber, string expDate, string cvv2, string cardType, string holderName)
    {
        bool okey = false;
        string errorMsg = string.Empty;
        BIUser UserData = new BIUser();
        decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
        decimal? _payAmount = _amount;
        int? payTypeID = 0;
        BIResMainRecord resMain = new BIResMainRecord();
        BIPayTypeRecord payTypeRec = null;
        string baseCurr = string.Empty;

        if (_amount.HasValue)
        {
            UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
            CultureInfo ci = BILib.getCultureInfo();
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
            if (resMain != null)
            {
                List<BIPayTypeRecord> payTypes = new BILib().getPayType(resMain.PLMarket, ref errorMsg);
                payTypeRec = payTypes.Find(f => f.Code == PayType);
                if (payTypes != null)
                    payTypeID = payTypes.Find(f => f.Code == PayType).RecID;
                string saleCur = resMain.SaleCur;
                baseCurr = saleCur;
            }
        }

        List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(string.Empty, ref errorMsg);
        BIBankVPosRecord bankVPos = bankVPosList.Find(f => f.Bank == payTypeRec.Bank && f.Cur == baseCurr);

        if (bankVPos == null)
            return new { Success = false, Msg = "Payment type not found" };
        string strOrderId = String.Format("{0}", DateTime.Now.Ticks.ToString());

        decimal balance = _amount.HasValue ? _amount.Value : (decimal)0;

        string successURL = String.Format("{0}?ResNumber={1}&PayType={2}&PayTypeId={3}",
            bankVPos.SuccessUrlB2B,
            ResNo,
            PayType,
            payTypeID.ToString());
        string errorURL = String.Format("{0}?ResNumber={1}&PayType={2}",
            bankVPos.ErrorUrlB2B,
            ResNo,
            PayType);

        String shopCode = bankVPos.ClientID;   //Banka tarafından verilen üye işyeri mağaza numarası     
        String purchaseAmount = GetPrice(_amount); //İşlem tutarı
        String curr = "949";// Kur Bilgisi - 949 TL
        if (baseCurr == "EUR")
            curr = "978";
        else
            if (baseCurr == "USD")
                curr = "840";


        String format = "{0}={1}&";
        StringBuilder sb = new StringBuilder();

        sb.AppendFormat(format, "ShopCode", bankVPos.ClientID);
        sb.AppendFormat(format, "PurchAmount", GetPrice(_amount));
        sb.AppendFormat(format, "Currency", curr);
        sb.AppendFormat(format, "OrderId", ResNo);
        sb.AppendFormat(format, "InstallmentCount", "");
        sb.AppendFormat(format, "TxnType", "Auth");
        sb.AppendFormat(format, "OrgOrderId", ""/*ResNo*/);
        sb.AppendFormat(format, "UserCode", bankVPos.UserID);
        sb.AppendFormat(format, "UserPass", bankVPos.Password);
        sb.AppendFormat(format, "SecureType", "NonSecure");
        sb.AppendFormat(format, "Pan", cardNumber);
        sb.AppendFormat(format, "Expiry", expDate);
        sb.AppendFormat(format, "Cvv2", cvv2);
        sb.AppendFormat(format, "BonusAmount", "");
        sb.AppendFormat(format, "CardType", cardType);
        sb.AppendFormat(format, "Lang", "TR");
        sb.AppendFormat(format, "MOTO", "0");        

        //sb.AppendFormat(format, "PayerAuthenticationCode", Request.Form["PayerAuthenticationCode"]);
        //sb.AppendFormat(format, "Eci", Request.Form["Eci"]);
        //sb.AppendFormat(format, "PayerTxnId", Request.Form["PayerTxnId"]);	        
        try
        {
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("https://inter-vpos.com.tr/MPI/Default.aspx");
            byte[] parameters = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(sb.ToString());
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = parameters.Length;
            System.IO.Stream requeststream = request.GetRequestStream();
            requeststream.Write(parameters, 0, parameters.Length);
            requeststream.Close();

            //HttpContext.Current.Response.Write(sb.ToString());
            System.Net.HttpWebResponse resp = (System.Net.HttpWebResponse)request.GetResponse();
            System.IO.StreamReader responsereader = new System.IO.StreamReader(resp.GetResponseStream(), System.Text.Encoding.GetEncoding("ISO-8859-9"));
            String responseStr = responsereader.ReadToEnd();

            StringBuilder result = new StringBuilder();
            if (responseStr != null)
            {
                bool success = true;
                string errors = string.Empty;
                string[] paramArr = responseStr.Split(';');
                result.AppendLine("<table>");
                foreach (string p in paramArr)
                {
                    string[] nameValue = p.Split('=');
                    result.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", nameValue[0], nameValue.Length > 1 ? nameValue[1] : "");
                    if (nameValue[0] == "TxnResult" && nameValue[1] == "Failed")
                        success = false;
                    if (nameValue[0] == "ErrorMessage" && nameValue[1] != "")
                        errors = nameValue.Length > 1 ? nameValue[1] : "";
                }
                result.AppendLine("</table>");
                if (success == false)
                {

                    return new { Success = false, Msg = errors };
                }
                else
                {
                    List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
                    List<BIResCustInfoRecord> resCustInfo = new BILib().getResCustInfoList(UserData.Market, ResNo, ref errorMsg);
                    string LeaderName = string.Empty;
                    string LeaderPhone = string.Empty;
                    int? CustNumber = null;
                    try
                    {
                        var Ln = from RC in resCust
                                 join RCI in resCustInfo on RC.CustNo equals RCI.CustNo
                                 where string.Equals(RC.Leader, "Y")
                                 select new
                                 {
                                     LeaderCustNo = RC.CustNo,
                                     LeaderName = (!string.IsNullOrEmpty(RCI.CName) || !string.IsNullOrEmpty(RCI.CSurName)) ? RCI.CSurName + " " + RCI.CName : RC.Surname + " " + RC.Surname,
                                     LeaderPhone = (RCI.ContactAddr == "H" ? RCI.AddrHomeTel : RCI.AddrWorkTel)
                                 };

                        if (Ln.Count() > 0)
                        {
                            CustNumber = Ln.FirstOrDefault().LeaderCustNo;
                            LeaderName = Ln.FirstOrDefault().LeaderName;
                            LeaderPhone = Ln.FirstOrDefault().LeaderPhone;
                        }
                        else
                            LeaderName = resCust.Where(f => f.Leader == "Y").FirstOrDefault().Name + " " + resCust.Where(f => f.Leader == "Y").FirstOrDefault().Surname;
                    }
                    catch { }

                    okey = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, payTypeID,
                                                         _amount.Value / 100, baseCurr, cardNumber, null, payTypeRec.Bank, holderName,
                                                         null, null, -1, 0, "", ResNo, "", ref errorMsg);
                    if (okey)
                    {
                        new BILib().SetResOptDate(ResNo, null, -1, ref errorMsg);
                        new BILib().setReservationComplate(UserData, ResNo, false, ref errorMsg);
                        return new { Success = true, Msg = "Payment" };
                        //return new returnObj { Paid = 1, Message = "Payment received. Reservation number " + ResNo + " Referans No " + posReferans, External = urlString };
                    }
                    else
                    {
                        return new { Success = false, Msg = errorMsg };
                    }
                }
            }
            else
            {
                return new { Success = false, Msg = "Please contact operator." };
            }
        }
        catch (Exception ex)
        {
            return new { Success = false, Msg = ex.Message };
        }

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object threeDPay(string ResNo, string PayType, string cardNumber, string expDate, string cvv2, string cardType, string holderName)
    {
        List<BIHtmlCodeData> htmlData = new List<BIHtmlCodeData>();
        string errorMsg = string.Empty;
        BIUser UserData = new BIUser();
        decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
        decimal? _payAmount = _amount;
        int? payTypeID = 0;
        BIResMainRecord resMain = new BIResMainRecord();
        BIPayTypeRecord payTypeRec = null;
        string baseCurr = string.Empty;

        if (_amount.HasValue)
        {
            UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
            CultureInfo ci = BILib.getCultureInfo();
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
            if (resMain != null)
            {
                List<BIPayTypeRecord> payTypes = new BILib().getPayType(resMain.PLMarket, ref errorMsg);
                payTypeRec = payTypes.Find(f => f.Code == PayType);
                if (payTypes != null)
                    payTypeID = payTypes.Find(f => f.Code == PayType).RecID;
                string saleCur = resMain.SaleCur;
                baseCurr = saleCur;
                //if (!string.Equals(baseCurr, saleCur))
                //{
                //    _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                //}
            }
        }

        return null;
    }
}

public class returnObj
{
    public returnObj()
    {
        _Paid = 0;
        _Message = string.Empty;
        _external = string.Empty;
    }

    Int16? _Paid;
    public Int16? Paid
    {
        get { return _Paid; }
        set { _Paid = value; }
    }

    string _Message;
    public string Message
    {
        get { return _Message; }
        set { _Message = value; }
    }

    string _external;
    public string External
    {
        get { return _external; }
        set { _external = value; }
    }
}