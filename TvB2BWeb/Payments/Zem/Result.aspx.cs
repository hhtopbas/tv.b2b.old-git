﻿using BankIntegration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.UI;
using TvBo;
using TvTools;

public partial class Result : System.Web.UI.Page
{

    protected void Page_Load(object sender, System.EventArgs events)
    {
        //if (!IsPostBack)
        //{
        StringBuilder log = new StringBuilder();
        try
        {
            List<BIResPayPlanRecord> PaymentPlan = new List<BIResPayPlanRecord>();

            if (!Page.IsPostBack)
                Session["paymentOK"] = false;

            if (Session["paymentOK"] == null || (bool)Session["paymentOK"] == false)
            {
                if (Session["paymentOK"] == null)
                    Session["paymentOK"] = false;

                decimal? amount = null;
                string errorMsg = string.Empty;

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<h3> 3D Dönen Parametreler</h3>");
                sb.AppendLine("<table border=\"1\">");
                sb.AppendLine("<tr>");
                sb.AppendLine("  <td><b>Parametre İsmi:</b></td>");
                sb.AppendLine("  <td><b>Parametre Değeri:</b></td>");
                sb.AppendLine("</tr>");

                String[] odemeparametreleri = new String[] { "AuthCode", "Response", "HostRefNum", "ProcReturnCode", "TransId", "ErrMsg" };
                IEnumerator e = Request.Form.GetEnumerator();

                while (e.MoveNext())
                {
                    String xkey = (String)e.Current;
                    String xval = Request.Form.Get(xkey);

                    bool ok = true;
                    for (int i = 0; i < odemeparametreleri.Length; i++)
                    {
                        if (xkey.Equals(odemeparametreleri[i]))
                        {
                            ok = false;
                            break;
                        }
                    }
                    if (ok)
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", xkey, xval);
                }

                sb.AppendLine("</table>");

                //resultValues.Text = sb.ToString();
                string ResNo = Request["ResNumber"];
                log.AppendLine(ResNo);
                BIUser UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);

                decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
                decimal? _payAmount = _amount;
                int? payTypeID = 0;
                BIResMainRecord resMain = new BIResMainRecord();
                BIPayTypeRecord payTypeRec = new BIPayTypeRecord();
                string baseCurr = string.Empty;
                string PayType = !string.IsNullOrEmpty(Request["PayType"]) ? Request["PayType"].ToString() : "";
                resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
                List<BIPayTypeRecord> payTypes = new BILib().getPayType(resMain.PLMarket, ref errorMsg);
                int? paymentTypeId = Conversion.getInt32OrNull(PayType);

                payTypeRec = payTypes.Find(fi => fi.Code == PayType);
                if (payTypeRec != null && payTypeRec.RecID.HasValue)
                    payTypeID = payTypeRec.RecID;

                string saleCur = resMain.SaleCur;
                log.AppendLine("Find payTypes");
                baseCurr = saleCur;

                List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(string.Empty, ref errorMsg);
                BIBankVPosRecord bankVPos = bankVPosList.Find(fi => fi.Bank == payTypeRec.Bank && fi.Cur == baseCurr);
                String hashparams = Request.Form.Get("HASHPARAMS");
                String hashparamsval = Request.Form.Get("HASHPARAMSVAL");
                String merchantpass = bankVPos.MerchantID;
                String paramsval = "";
                int index1 = 0, index2 = 0;
                do
                {
                    index2 = hashparams.IndexOf(":", index1);
                    String val = Request.Form.Get(hashparams.Substring(index1, index2 - index1)) == null ? "" : Request.Form.Get(hashparams.Substring(index1, index2 - index1));
                    paramsval += val;
                    index1 = index2 + 1;
                }
                while (index1 < hashparams.Length);

                String hashval = paramsval + merchantpass;         //elde edilecek hash değeri için paramsval e store key ekleniyor. (işyeri anahtarı)
                String hashparam = Request.Form.Get("HASH");

                System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashval);
                byte[] inputbytes = sha.ComputeHash(hashbytes);

                String hash = Convert.ToBase64String(inputbytes); //Güvenlik ve kontrol amaçlı oluşturulan hash

                if (!paramsval.Equals(hashparamsval) || !hash.Equals(hashparam)) //oluşturulan hash ile gelen hash ve hash parametreleri değerleri ile ayrıştırılıp edilen edilen aynı olmalı.
                {
                    lblResult1.Text = "<h4>Safety warning. Digital signature is not valid</h4>";
                    return;
                }

                bool paymentSuccess = false;
                String mdStatus = Request.Form.Get("mdStatus"); // 3d işlemin sonucu                    

                if (mdStatus.Equals("1") || mdStatus.Equals("2") || mdStatus.Equals("3") || mdStatus.Equals("4"))
                {
                    sb = new StringBuilder();
                    sb.AppendLine("<h5>3D İşlemi Başarılı</h5><br/>");
                    sb.AppendLine("<h3> Ödeme Sonucu</h3>");
                    sb.AppendLine("<table border=\"1\">");
                    sb.AppendLine("<tr>");
                    sb.AppendLine("    <td><b>Parametre İsmi</b></td>");
                    sb.AppendLine("    <td><b>Parameter Değeri</b></td>");
                    sb.AppendLine("</tr>");
                    log.AppendLine(sb.ToString());

                    for (int i = 0; i < odemeparametreleri.Length; i++)
                    {
                        String paramname = odemeparametreleri[i];
                        String paramval = Request.Form.Get(paramname);
                        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", paramname, paramsval);
                    }
                    sb.AppendLine("</table>");

                    if ("Success".Equals(Request.Form.Get("TxnResult")))
                    {
                        labelImage.ImageUrl = "success.png";
                        //lblResult1.Text = string.Format("<h6>Ödeme İşlemi Başarılı</h6>", "");
                        //lblResult2.Text = string.Format("<h4>Payment successfull</h4> <br />Your payment has been processed. Here are the details of this transaction for your referance -<br /> Invoice number:{0}<br />Amount Paid:{1}", ResNo, amount.ToString() + " " + baseCurr);
                        paymentSuccess = true;
                    }
                    else
                    {
                        labelImage.ImageUrl = "error.png";
                        lblResult2.Text = "<h4>Payment Failed.</h4>";
                        //lblResult1.Text = "<h6>Payment Failed</h6>";
                        paymentSuccess = false;
                    }
                }
                else
                {
                    labelImage.ImageUrl = "error.png";
                    lblResult2.Text = "<h5>3D Unsuccessful</h5>";
                    paymentSuccess = false;
                }


                if (paymentSuccess)
                {
                    string TransId = Conversion.getStrOrNull(Request.Form.Get("TransId"));
                    string resNumber = Request.QueryString["ResNumber"].ToString().Substring(0, 8);
                    try
                    {
                        if (Request["ResNumber"] != null)
                        {
                            string resNo = Request["ResNumber"];
                            PaymentPlan = new BILib().getResPayPlan(resNo, ref errorMsg);
                            amount = new BILib().getResAmountForPay(resNo, ref errorMsg);
                        }

                        if (PaymentPlan.Count == 0)
                            Response.End();
                    }
                    catch { }

                    int payTypeId = Convert.ToInt32(Request.QueryString["PayTypeId"]);
                    if (amount.HasValue && amount <= 0)
                    {
                        Session["PaymentOk"] = true;
                        return;
                    }
                    else if (amount == 0)
                    {
                        return;
                    }

                    CultureInfo ci = BILib.getCultureInfo();
                    Thread.CurrentThread.CurrentCulture = ci;
                    Thread.CurrentThread.CurrentUICulture = ci;

                    List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
                    List<BIResCustInfoRecord> resCustInfo = new BILib().getResCustInfoList(UserData.Market, ResNo, ref errorMsg);

                    string LeaderName = string.Empty;
                    string LeaderPhone = string.Empty;
                    int? CustNumber = null;
                    try
                    {
                        var Ln = from RC in resCust
                                 join RCI in resCustInfo on RC.CustNo equals RCI.CustNo
                                 where string.Equals(RC.Leader, "Y")
                                 select new
                                 {
                                     LeaderCustNo = RC.CustNo,
                                     LeaderName = (!string.IsNullOrEmpty(RCI.CName) || !string.IsNullOrEmpty(RCI.CSurName)) ? RCI.CSurName + " " + RCI.CName : RC.Surname + " " + RC.Surname,
                                     LeaderPhone = (RCI.ContactAddr == "H" ? RCI.AddrHomeTel : RCI.AddrWorkTel)
                                 };

                        if (Ln.Count() > 0)
                        {
                            CustNumber = Ln.FirstOrDefault().LeaderCustNo;
                            LeaderName = Ln.FirstOrDefault().LeaderName;
                            LeaderPhone = Ln.FirstOrDefault().LeaderPhone;
                        }
                        else
                            LeaderName = resCust.Where(f1 => f1.Leader == "Y").FirstOrDefault().Name + " " + resCust.Where(f2 => f2.Leader == "Y").FirstOrDefault().Surname;
                    }
                    catch { }

                    //DataSet ds = TourVisioDB.GetReservationInfoForPayment(resNumber, payTypeId);
                    bool paymentOk = false;
                    if (resMain != null)
                    {
                        if (paymentOk == false)
                        {
                            paymentOk = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, Conversion.getInt32OrNull(payTypeId),
                                amount.Value, resMain.SaleCur, "", null, bankVPos != null ? bankVPos.Bank : "", "",
                                null, null, -1, 0, string.IsNullOrEmpty(TransId) ? ResNo : TransId, ResNo, "", ref errorMsg);

                            if (paymentOk)
                            {
                                paymentOk = true;
                                Session["PaymentOk"] = true;
                            }
                        }
                        else
                            paymentOk = true;

                        labelImage.ImageUrl = "success.png";
                        lblResult2.Text = string.Format("<h4>Payment successfull</h4> <br />Your payment has been processed. Here are the details of this transaction for your referance -<br /> Invoice number:{0}<br />Amount Paid:{1}", ResNo, amount.ToString() + " " + baseCurr);
                    }
                }
                else
                {
                    labelImage.ImageUrl = "error.png";
                    lblResult1.Text = "<h4>Payment Failed.</h4>;" + mdStatus;
                }

                //lblResult3.Text += log.ToString();
            }
        }
        catch (Exception Ex)
        {
            labelImage.ImageUrl = "error.png";
            lblResult2.Text = "<h4>Payment Failed.</h4> " + Ex.Message;

            string error = Common.FlattenException(Ex);
        }
        //   }
    }
}