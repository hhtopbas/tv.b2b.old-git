﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Payments_PaymentResult : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string msg = !string.IsNullOrEmpty(Request.Params["replytext"]) ? Request.Params["replytext"].ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "ReservationPaymentProblemPleaseContactWithOperator").ToString();
            
            string aurigaCanceled = Request.QueryString["canceled"];
            if (!String.IsNullOrEmpty(aurigaCanceled))
            {
                if (String.IsNullOrEmpty(Request.QueryString["Status_code"]))
                    msg = HttpContext.GetGlobalResourceObject("LibraryResource", "ReservationPaymentProblemPleaseContactWithOperator").ToString();
                else
                {
                    int statusCode = -1;
                    string statusText = "";
                    if (Int32.TryParse(Request.QueryString["Status_code"], out statusCode))
                        statusText = GetStatusTextForAuriga(statusCode);
                    msg = statusText;
                }
            }
            lblResNo.Text = !string.IsNullOrEmpty(Request.Params["resNumber"]) ? (string)Request.Params["resNumber"] + " " + msg : "";
        }        
    }

    public static string GetStatusTextForAuriga(int StatusCode)
    {
        string[] err = new string[127];
        err[0] = "The payment/order placement has been carried out: Paid";
        err[1] = "";
        err[2] = "";
        err[3] = "An obligatory field is lacking or is incorrectly formatted.";
        err[4] = "Incorrect Merchant ID (Merchant_id].";
        err[5] = "";
        err[6] = "Incorrect amount (Amount or VAT, good’s value or VAT].";
        err[7] = "Unable to process or Authorize";
        err[8] = "";
        err[9] = "";
        err[10] = "";
        err[11] = "Was not possible to contact bank.";
        err[12] = "Error in conjunction with validation of received MAC.";
        err[13] = "Amount too large";
        err[14] = "Wrong date/time format (Purchase_date, must be exactly 12 digits the format yyyymmddhhmm]";
        err[15] = "Incorrect purchase date (Purchase_date]";
        err[16] = "";
        err[17] = "Invalid payment method (Payment_method]. Payment method not configured for the Shop. Or payment method not supported by Version=1";
        err[18] = "No contact with the bank.";
        err[19] = "Card Payment: Purchase denied at the bank (the card's validity period has expired], contact the bank.";
        err[20] = "Card Payment: Purchase rejected at the bank, contact the bank.";
        err[21] = "Country for card-issuing bank is not permitted.";
        err[22] = "The risk assessment value for the transaction exceeds the permissible value.";
        err[23] = "Card inactive";
        err[24] = "Error Request_type in Order Administration order.";
        err[25] = "Insufficient funds";
        err[26] = "Suspected fraud";
        err[27] = "Purchase amount (Amount] must be greater than 0.";
        err[28] = "Too many attempts";
        err[29] = "";
        err[30] = "Timeout. No answer from bank";
        err[31] = "Purchase terminated (by the buyer]";
        err[32] = "Error in order, transaction already registered and paid. This Customer_refno is already registered on another transaction.";
        err[33] = "eKöp: Technical error in communication with Postgirot/Nordea. Please try again later";
        err[34] = "Provided recipient account (To_accnt] incorrect";
        err[35] = "Provided sending account (From_accnt] incorrect.";
        err[36] = "eKöp: Provided sending account (From_accnt] temporarily frozen";
        err[37] = "";
        err[38] = "";
        err[39] = "eKöp: Provided certificate (Cert] incorrect.";
        err[40] = "eKöp: Account not connected to the service.";
        err[41] = "Payment Selection Page/Card Payment Page: Merchant not connected to the service.";
        err[42] = "Bad Auth_null - Should be YES or NO";
        err[43] = "Bad Capture_now - Should be YES or NO";
        err[44] = "";
        err[45] = "For Order Administration: The status of the transaction does not permit the operation";
        err[46] = "";
        err[47] = "";
        err[48] = "For Order Administration: The transaction does not exist.";
        err[49] = "";
        err[50] = "Card Payment: Merchant not configured for currency/card type combination (Currency/Card_type]";
        err[51] = "Card Payment: Invalid Exp_date (must be exactly 4 digits in the format mmyy].";
        err[52] = "Card Payment: Invalid Card_num (card number].";
        err[53] = "Card Payment: Incorrect format for the card’s control number Cvx2 (3 or 4 digits]";
        err[54] = "Order Administration: The status of the transaction does not permit the operation";
        err[55] = "Bad MOTO method. Should be MAIL or PHONE";
        err[56] = "The purchase has been discontinued by the purchaser or denied by the bank";
        err[57] = "Incorrect Customer_refno";
        err[58] = "Incorrect Version, incorrect format for version parameter.";
        err[59] = "Bad card fee";
        err[60] = "Bad Prepaid_card_num";
        err[61] = "Bad Prepaid_load_amount";
        err[62] = "Bad Prepaid_activation_code";
        err[63] = "";
        err[64] = "";
        err[65] = "Transaction already registered and awaiting a response from the bank";
        err[66] = "";
        err[67] = "Crediting could not be carried out.";
        err[68] = "";
        err[69] = "Technical error, Posten Betalväxel";
        err[70] = "For Order Administration: The function is not supported";
        err[71] = "Wrong format or size on the Response URL (Response_URL]";
        err[72] = "Incorrect Currency Code (Currency]";
        err[73] = "Incorrect Language Code (Language]";
        err[74] = "";
        err[75] = "Incorrect Comments (Comment]";
        err[76] = "Incorrect goods description (Goods_description]";
        err[77] = "Customer_refno does not match the Transaction_id";
        err[78] = "Card Payment: Authorisation Reversal could not be carried out (Request_type= AuthRev]";
        err[79] = "Card Payment: Acquiring could not be carried out (capture]";
        err[80] = "";
        err[81] = "Failed 3DSecure identification";
        err[82] = "Timed out 3DSecure identification";
        err[83] = "";
        err[84] = "";
        err[85] = "";
        err[86] = "";
        err[87] = "";
        err[88] = "";
        err[89] = "";
        err[90] = "Subscription (Transaction_id] has ended";
        err[91] = "subscription (Transaction_id] not found";
        err[92] = "Subscription (Transaction_id] not captured";
        err[93] = "Payment Selection Page: Wrong format or size for Cancel URL (Cancel_URL]";
        err[94] = "";
        err[95] = "Invoice purchase: Incorrect OCR number, e.g. incorrect control digit.";
        err[96] = "Incorrect length for invoice parameters.";
        err[97] = "Incorrect guarantee parameter.";
        err[98] = "Bad postal code";
        err[99] = "";
        err[100] = "";
        err[101] = "";
        err[102] = "";
        err[103] = "";
        err[104] = "";
        err[105] = "";
        err[106] = "";
        err[107] = "";
        err[108] = "";
        err[109] = "";
        err[110] = "Invoice purchase: Invalid personal number or organisation number.";
        err[111] = "UC denied";
        err[112] = "UC ok";
        err[113] = "";
        err[114] = "";
        err[115] = "Incorrect price per unit";
        err[116] = "Incorrect quantity per product";
        err[117] = "Incorrect VAT percentage";
        err[118] = "Incorrect discount percentage";
        err[119] = "";
        err[120] = "Prepaid transaction does not exist";
        err[121] = "Incorrect parameter value for VAT_Code";
        err[122] = "Incorrect parameter value for Fee_type_id";
        err[123] = "Incorrect parameter value for Invoice_carrier";
        err[124] = "Incorrect parameter value for Fee_amount";
        err[125] = "Incorrect parameter value for Discount";
        err[126] = "Incorrect parameter value for Round";

        return err[StatusCode];
    }

    protected void paymentBtn_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.Params["resNumber"]))
        {
            Response.Redirect("BeginPayment.aspx?ResNo=" + Request.Params["resNumber"].ToString());
        }
    }
}
