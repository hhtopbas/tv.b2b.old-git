﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Result.aspx.cs" Inherits="Result" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Begin Payment Final</title>

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function paymentFinal() {
            //self.parent.getResData(true);
            self.close();
        }
                           
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblResult1" runat="server" />
        <br />
        <asp:Label ID="lblResult2" runat="server" />
        <br />
        <asp:Label ID="lblResult3" runat="server" />
        <br />
        <br />
        <input type="button" value="Kapat ve geri dön." onclick="paymentFinal();" />
    </div>
    </form>
</body>
</html>
