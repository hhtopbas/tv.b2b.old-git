﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebPaymentAkbank.aspx.cs"
    Inherits="Payments_Karnak_WebPaymentAkbank" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body { font-family: Arial; font-size: 10pt; }
        .UserFormTitle { width: 200px; float: left; color: #051e34; font-weight: bold; }
        .PaymentTable { margin: 0 auto 0 auto; }
        .PaymentTable tbody { border: 1px solid #c0fff9; }
        .PaymentTable td { line-height: 25px; padding: 0 0 0 8px; }
        .PaymentTable td a { float: none; width: 150px; }
        .PaymentTable .UserFormElement { width: auto; padding: 10px; float: none; }
        .PaymentTable .UserFormElement .GenelInput { width: 200px; }
    </style>
</head>
<body>
    <form method="post" runat="server" id="frmPayment" action="https://www.sanalakpos.com/servlet/est3dgate">
    <table class="PaymentTable">
        <tr>
            <td class="UserFormTitle">
                Card Number
            </td>
            <td class="UserFormElement">
                <asp:TextBox ID="pan" runat="server" CssClass="GenelInput" />
            </td>
        </tr>
        <tr>
            <td class="UserFormTitle">
                Expire Date (mm)
            </td>
            <td class="UserFormElement">
                <asp:TextBox ID="Ecom_Payment_Card_ExpDate_Month" runat="server" CssClass="GenelInput" />
            </td>
        </tr>
        <tr>
            <td class="UserFormTitle">
                Expire Date (yy)
            </td>
            <td class="UserFormElement">
                <asp:TextBox ID="Ecom_Payment_Card_ExpDate_Year" runat="server" CssClass="GenelInput" />
            </td>
        </tr>
        <tr>
            <td class="UserFormTitle">
                CVV2
            </td>
            <td class="UserFormElement">
                <asp:TextBox ID="cv2" runat="server" CssClass="GenelInput" />
            </td>
        </tr>
        <tr>
            <td class="UserFormTitle">
                Card type
            </td>
            <td class="UserFormElement">
                <select name="cardType">
                    <option value="1">Visa</option>
                    <option value="2">MasterCard</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center>                    
                    <input type="submit" value="Send" />
                </center>
            </td>
        </tr>
    </table>
    <input type="hidden" id="clientid" name="clientid" value="<%= _merchantId %>" />
    <input type="hidden" id="amount" name="amount" value="<%= _amount %>" />
    <input type="hidden" id="oid" name="oid" value="<%= _reference %>" />
    <input type="hidden" id="okUrl" name="okUrl" value="<%= _redirectUrl %>" />
    <input type="hidden" id="failUrl" name="failUrl" value="<%= _errorUrl %>" />
    <input type="hidden" id="rnd" name="rnd" value="<%= _random %>" />
    <input type="hidden" id="hash" name="hash" value="<%= _hashData %>" />
    <input type="hidden" id="storetype" name="storetype" value="3d_pay" />
    <input type="hidden" id="lang" name="lang" value="tr" />
    </form>
</body>
</html>
