﻿using BankIntegration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.UI;
using TvTools;

public partial class Result : System.Web.UI.Page
{
    List<BIResPayPlanRecord> PaymentPlan
    {
        get { return (List<BIResPayPlanRecord>)ViewState["ResPayPlan"]; }
        set { ViewState["ResPayPlan"] = value; }
    }

    protected void Page_Load(object sender, System.EventArgs e)
    {
        GVCHashData gvc;

        if (!Page.IsPostBack)
        {
            gvc = new GVCHashData();

            string strMode = Request.Form.Get("mode");
            string strApiVersion = Request.Form.Get("apiversion");
            string strTerminalProvUserID = Request.Form.Get("terminalprovuserid");
            string strType = Request.Form.Get("txntype");
            string strAmount = Request.Form.Get("txnamount");
            string strCurrencyCode = Request.Form.Get("txncurrencycode");
            string strInstallmentCount = Request.Form.Get("txninstallmentcount");
            string strTerminalUserID = Request.Form.Get("terminaluserid");
            string strOrderID = Request.Form.Get("oid");
            string strCustomeripaddress = Request.Form.Get("customeripaddress");
            string strcustomeremailaddress = Request.Form.Get("customeremailaddress");
            string strTerminalID = Request.Form.Get("clientid");
            string _strTerminalID = gvc.PrepareTerminalId(strTerminalID);
            string strTerminalMerchantID = Request.Form.Get("terminalmerchantid");
            string strStoreKey = "42656b6972417379615475725461734163656e7465416e74";
            //HASH doğrulaması için 3D Secure şifreniz
            string strProvisionPassword = "AcenteAsyaTur123";
            //HASH doğrulaması için TerminalProvUserID şifresini tekrar yazıyoruz
            string strSuccessURL = Request.Form.Get("successurl");
            string strErrorURL = Request.Form.Get("errorurl");
            string strCardholderPresentCode = "13";
            //3D Model işlemde bu değer 13 olmalı
            string strMotoInd = "N";
            string strNumber = "";
            //Kart bilgilerinin boş gitmesi gerekiyor
            string strExpireDate = "";
            //Kart bilgilerinin boş gitmesi gerekiyor
            string strCVV2 = "";
            //Kart bilgilerinin boş gitmesi gerekiyor
            string strAuthenticationCode = Server.UrlEncode(Request.Form.Get("cavv"));
            string strSecurityLevel = Server.UrlEncode(Request.Form.Get("eci"));
            string strTxnID = Server.UrlEncode(Request.Form.Get("xid"));
            string strMD = Server.UrlEncode(Request.Form.Get("md"));
            string strMDStatus = Request.Form.Get("mdstatus");
            string strMDStatusText = Request.Form.Get("mderrormessage");
            string strHostAddress = "https://sanalposprov.garanti.com.tr/VPServlet";
            //Provizyon için xml'in post edileceği adres
            string SecurityData = gvc.GetSHA1(strProvisionPassword + _strTerminalID).ToUpper();
            string HashData = gvc.GetSHA1(strOrderID + strTerminalID + strAmount + SecurityData).ToUpper();
            //Daha kısıtlı bilgileri HASH ediyoruz.

            //Hashdata kontrolü için bankadan dönen secure3dhash değeri alınıyor.
            string strHashData = Request.Form.Get("secure3dhash");
            string ValidateHashData = gvc.GetSHA1(strTerminalID + strOrderID + strAmount + strSuccessURL + strErrorURL + strType + strInstallmentCount + strStoreKey + SecurityData).ToUpper();

            //İlk gönderilen ve bankadan dönen HASH değeri yeni üretilenle eşleşiyorsa;
            string errorMsg = string.Empty;
            if (strHashData == ValidateHashData)
            {
                lblResult1.Text = "Sayısal İmza Geçerli";

                //Tam Doğrulama, Kart Sahibi veya bankası sisteme kayıtlı değil, Kartın bankası sisteme kayıtlı değil
                //Doğrulama denemesi, kart sahibi sisteme daha sonra kayıt olmayı seçmiş responselarını alan
                //işlemler için Provizyon almaya çalışıyoruz

                if (strMDStatus == "1" | strMDStatus == "2" | strMDStatus == "3" | strMDStatus == "4")
                {
                    //Provizyona Post edilecek XML Şablonu
                    string strXML = null;
                    strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<GVPSRequest>"
                        + "<Mode>" + strMode + "</Mode>"
                        + "<Version>" + strApiVersion + "</Version>"
                        + "<ChannelCode></ChannelCode>"
                        + "<Terminal><ProvUserID>" + strTerminalProvUserID
                        + "</ProvUserID><HashData>" + HashData
                        + "</HashData><UserID>" + strTerminalUserID
                        + "</UserID><ID>" + strTerminalID
                        + "</ID><MerchantID>" + strTerminalMerchantID
                        + "</MerchantID></Terminal>" + "<Customer><IPAddress>" + strCustomeripaddress
                        + "</IPAddress><EmailAddress>" + strcustomeremailaddress
                        + "</EmailAddress></Customer>"
                        + "<Card><Number></Number><ExpireDate></ExpireDate><CVV2></CVV2></Card>"
                        + "<Order><OrderID>" + strOrderID
                        + "</OrderID><GroupID></GroupID><AddressList><Address><Type>B</Type><Name></Name><LastName></LastName><Company></Company><Text></Text><District></District><City></City><PostalCode></PostalCode><Country></Country><PhoneNumber></PhoneNumber></Address></AddressList></Order>"
                        + "<Transaction>" + "<Type>" + strType
                        + "</Type><InstallmentCnt>" + strInstallmentCount
                        + "</InstallmentCnt><Amount>" + strAmount
                        + "</Amount><CurrencyCode>" + strCurrencyCode
                        + "</CurrencyCode><CardholderPresentCode>" + strCardholderPresentCode
                        + "</CardholderPresentCode><MotoInd>" + strMotoInd
                        + "</MotoInd>" + "<Secure3D><AuthenticationCode>" + strAuthenticationCode
                        + "</AuthenticationCode><SecurityLevel>" + strSecurityLevel
                        + "</SecurityLevel><TxnID>" + strTxnID
                        + "</TxnID><Md>" + strMD
                        + "</Md></Secure3D>" + "</Transaction>" + "</GVPSRequest>";

                    try
                    {
                        string data = "data=" + strXML;

                        WebRequest _WebRequest = WebRequest.Create(strHostAddress);
                        _WebRequest.Method = "POST";

                        byte[] byteArray = Encoding.UTF8.GetBytes(data);
                        _WebRequest.ContentType = "application/x-www-form-urlencoded";
                        _WebRequest.ContentLength = byteArray.Length;

                        Stream dataStream = _WebRequest.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();

                        WebResponse _WebResponse = _WebRequest.GetResponse();
                        Console.WriteLine(((HttpWebResponse)_WebResponse).StatusDescription);
                        dataStream = _WebResponse.GetResponseStream();

                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();

                        Console.WriteLine(responseFromServer);
                        decimal? amount = null;
                        //00 ReasonCode döndüğünde işlem başarılıdır. Müşteriye başarılı veya başarısız şeklinde göstermeniz tavsiye edilir. (Fraud riski)
                        if (responseFromServer.Contains("<ReasonCode>00</ReasonCode>"))
                        {
                            lblResult2.Text = strMDStatusText;
                            lblResult3.Text = "İşlem Başarılı";
                            Session["PaymentOk"] = false;
                            string resNumber = Request.QueryString["ResNumber"].ToString().Substring(0, 8);
                            try
                            {
                                if (Request["ResNumber"] != null)
                                {
                                    string resNo = Request["ResNumber"];
                                    PaymentPlan = new BILib().getResPayPlan(resNo, ref errorMsg);
                                    amount = new BILib().getResAmountForPay(resNo, ref errorMsg);
                                }

                                if (PaymentPlan.Count == 0)
                                    Response.End();
                            }
                            catch { }

                            int payTypeId = Convert.ToInt32(Request.QueryString["PayTypeId"]);
                            if (amount.HasValue && amount <= 0)
                            {
                                Session["PaymentOk"] = true;
                                lblResult2.Text = strMDStatusText;
                                lblResult3.Text = "İşlem Başarılı";
                                lblResult2.Text += "  " + responseFromServer;
                                //Response.Redirect(String.Format("BookingResult.aspx?resNumber={0}&PayType={1}", resNumber, payTypeId));
                                return;
                            }
                            else if (amount == 0)
                            {
                                lblResult2.Text = strMDStatusText;
                                lblResult3.Text = "İşlem Başarılı";
                                lblResult2.Text += "  " + responseFromServer;
                                //errorMsg = string.Concat("Payment Error:Token={0},ResNo={1}", Request["Reserve"], resNumber);
                                //new Sf.ExceptionHandling.AppDatabaseLoggingHandler().HandleException(new Exception(errorMsg));
                                //Response.Redirect("ErrorPage.aspx?ErrorType=1&ResNumber=" + resNumber);
                                return;
                            }

                            if (Request.QueryString["PayTypeId"] != null && Request.QueryString["PayTypeId"].ToString() == "0")
                            {
                                lblResult2.Text = strMDStatusText;
                                lblResult3.Text = "İşlem Başarılı";
                                lblResult2.Text += "  " + responseFromServer;
                                //Response.Redirect("BookingResult.aspx?resNumber=" + resNumber + "&PayType=" + payTypeId.ToString());
                            }
                            string ResNo = resNumber;
                            BIUser UserData = new BIUser();
                            UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
                            CultureInfo ci = BILib.getCultureInfo();
                            Thread.CurrentThread.CurrentCulture = ci;
                            Thread.CurrentThread.CurrentUICulture = ci;
                            BIResMainRecord resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
                            List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
                            List<BIResCustInfoRecord> resCustInfo = new BILib().getResCustInfoList(UserData.Market, ResNo, ref errorMsg);
                            List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(string.Empty, ref errorMsg);
                            BIBankVPosRecord bankVPos = bankVPosList.Find(f => f.RecID == payTypeId);
                            string LeaderName = string.Empty;
                            string LeaderPhone = string.Empty;
                            int? CustNumber = null;
                            try
                            {
                                var Ln = from RC in resCust
                                         join RCI in resCustInfo on RC.CustNo equals RCI.CustNo
                                         where string.Equals(RC.Leader, "Y")
                                         select new
                                         {
                                             LeaderCustNo = RC.CustNo,
                                             LeaderName = (!string.IsNullOrEmpty(RCI.CName) || !string.IsNullOrEmpty(RCI.CSurName)) ? RCI.CSurName + " " + RCI.CName : RC.Surname + " " + RC.Surname,
                                             LeaderPhone = (RCI.ContactAddr == "H" ? RCI.AddrHomeTel : RCI.AddrWorkTel)
                                         };

                                if (Ln.Count() > 0)
                                {
                                    CustNumber = Ln.FirstOrDefault().LeaderCustNo;
                                    LeaderName = Ln.FirstOrDefault().LeaderName;
                                    LeaderPhone = Ln.FirstOrDefault().LeaderPhone;
                                }
                                else
                                    LeaderName = resCust.Where(f1 => f1.Leader == "Y").FirstOrDefault().Name + " " + resCust.Where(f2 => f2.Leader == "Y").FirstOrDefault().Surname;
                            }
                            catch { }

                            //DataSet ds = TourVisioDB.GetReservationInfoForPayment(resNumber, payTypeId);
                            bool paymentOk = false;
                            if (resMain != null)
                            {
                                if (paymentOk == false)
                                {
                                    paymentOk = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, Conversion.getInt32OrNull(payTypeId),
                                        amount.Value, resMain.SaleCur, "", null, bankVPos != null ? bankVPos.Bank : "", "",
                                                     null, null, -1, 0, strOrderID, ResNo, "", ref errorMsg);

                                    //paymentOk = WebServices.MakeResPayment(resNumber, dr["CustNo"].ToString(),
                                    //                                                     payTypeId, amount, dr["SaleCur"].ToString(), "", 1,
                                    //                                                     "", "", "", "", 2, 1, "", ref errorMsg);
                                    if (paymentOk)
                                    {
                                        //TourVisioDB.UpdateTokenStatus(Request["Reserve"], resNumber);
                                        paymentOk = true;
                                        Session["PaymentOk"] = true;
                                    }
                                    //else
                                    //{
                                    //    lblResult2.Text = strMDStatusText;
                                    //    lblResult3.Text = "İşlem Başarısız";
                                    //    lblResult2.Text += "  " + responseFromServer;
                                    //    //string errorMsg22 = string.Concat(errorMsg, "-Payment Error:Token={0},ResNo={1}", Request["Reserve"], resNumber);
                                    //    //new Sf.ExceptionHandling.AppDatabaseLoggingHandler().HandleException(new Exception(errorMsg22));
                                    //    //UICommon.Alert(Sf.Localization.B2CLocalizationCaching.GetLexiconByCode("ReservationPaymentProblemPleaseContactWithOperator"), this.Page);
                                    //}
                                }
                                else
                                    paymentOk = true;

                            }
                            if (paymentOk)
                            {
                                lblResult2.Text = strMDStatusText;
                                lblResult3.Text = "İşlem Başarılı";
                                lblResult2.Text += "  " + responseFromServer;
                                //Response.Redirect(String.Format("~/themes/Quheilan/BookingResult.aspx?resNumber={0}&PayType={1}", resNumber, payTypeId));
                            }
                            else
                            {
                                lblResult2.Text = strMDStatusText;
                                lblResult3.Text = "İşlem Başarısız";
                                lblResult2.Text += "  " + responseFromServer;
                                //string errorMsg33 = string.Concat("Payment Error:Token={0},ResNo={1}", Request["Reserve"], resNumber);
                                //new Sf.ExceptionHandling.AppDatabaseLoggingHandler().HandleException(new Exception(errorMsg33));
                                //Response.Redirect("ErrorPage.aspx?ErrorType=1&ResNumber=" + resNumber);
                            }
                        }
                        else
                        {
                            lblResult2.Text = strMDStatusText;
                            lblResult3.Text = "İşlem Başarısız";
                            lblResult2.Text += "  " + responseFromServer;
                        }
                    }
                    catch (Exception ex)
                    {
                        lblResult2.Text = strMDStatusText;
                        lblResult3.Text = ex.Message;
                    }
                }
                else
                {
                    lblResult2.Text = strMDStatusText;
                    lblResult3.Text = "İşlem Başarısız";
                }
            }
            else
            {
                lblResult1.Text = "Güvenlik Uyarısı. Sayısal Imza Geçerli Degil";
                lblResult2.Text = strMDStatusText;
                lblResult3.Text = " İşlem Başarısız";
            }

            //Dönen değerlerin detayını almak isterseniz alttaki kod bloğunu kullanabilirsiniz.
            System.Collections.IEnumerator fa = Request.Form.GetEnumerator();
            while (fa.MoveNext())
            {
                string xkey = (string)fa.Current;
                string xval = Request.Form.Get(xkey);
                lblResult2.Text = lblResult2.Text + (xkey + " : " + xval);
            }
        }
    }
}