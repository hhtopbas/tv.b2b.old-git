﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BankIntegration;

public partial class Payments_Karnak_WebPaymentAkbank : System.Web.UI.Page
{
    #region Const
    protected string _merchantId { get { return "100919017"; } }        // Shop ID
    protected string _companyName { get { return "KARNAK"; } }
    protected string _workType { get { return "Auth"; } }
    protected string _storeKey { get { return "PrimaryPyramid32"; } }   // Store key
    public string _redirectUrl = "";
    public string _errorUrl = "";

    protected string _amount = string.Empty;
    protected string _reference = string.Empty;
    protected string _random = string.Empty;
    protected string _hashData = string.Empty;
    protected string _rnd = string.Empty;                               //Random value Ex: DateTime.Now primary Key
    protected string _installment = string.Empty;                       //Taksit sayısı
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        #region setAttributes
        if (!IsPostBack)
        {
            _redirectUrl = Request.Url.Scheme + "://" + Request.Url.Host + "/" + Request.Url.LocalPath.Substring(0, Request.Url.LocalPath.LastIndexOf('/')) + "/3dResult.aspx?ResNo=";
            _errorUrl = Request.Url.Scheme + "://" + Request.Url.Host + "/" + Request.Url.LocalPath.Substring(0, Request.Url.LocalPath.LastIndexOf('/')) + "/BeginPayment.aspx";
            
            string errorMsg = string.Empty;
            string resNo = Request.Params["ResNo"].ToString();
            string bankCode = Request.Params["Bank"].ToString();
            BIUser UserData = new BILib().getReservationCreateUserData(resNo, ref errorMsg);
            List<BIPayTypeRecord> payTypes = new BILib().getPayType(UserData.Market, ref errorMsg);

            _redirectUrl += resNo;
            BIResMainRecord resMain = new BIResMainRecord();
            decimal? amount = new BILib().getResAmount(resNo, 0, ref errorMsg);

            resMain = new BILib().getResMain(UserData, resNo, ref errorMsg);
            if (resMain != null)
            {
                string baseCurr = "TL";
                string saleCur = resMain.SaleCur;
                if (!Equals(baseCurr, saleCur))
                {
                    amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, amount, false, ref errorMsg);
                }
            }

            _amount = new BILib().GetPrice(amount);

            _random = _reference = String.Format("{0}_{1}", resNo, DateTime.Now.ToShortTimeString());            
            string data = String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}", _merchantId, _reference, _amount, _redirectUrl, _errorUrl, _workType, _installment, _random, _storeKey);            

            _hashData = new BILib().GetSHA1X64(data);
        }
        #endregion
    }
}
