﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="3dResult.aspx.cs" Inherits="Akbank_3dResult" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        function init() {
            window.onunload = onCloseWindow;
            waitClose();
        }

        function onCloseWindow(evt) {
            window.location = window.location;
            this.close();
        }

        function closeWindow() {
            self.close();
        }

        function waitClose() {
            setTimeout("closeWindow()", 10000);
        }
    </script>

</head>
<body onload="init();">
    <form id="form1" runat="server">
    <div>
        <h2>
            <asp:Label ID="lblMsg" runat="server" /></h2>
        <br />
        <strong>Reservation number :</strong><asp:Label ID="resNo" runat="server" /><br />        
        <input type="button" value="Close" onclick="closeWindow();" />
    </div>
    </form>
</body>
</html>
