﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using TvBo;

public partial class themes_Detur_UserControls_WebPaymentFinLuottokunta : System.Web.UI.UserControl
{

    private string reservationNumber;
    public string ReservationNumber
    {
        get { return reservationNumber; }
        set { reservationNumber = value; }
    }

    private decimal amount;
    public decimal Amount
    {
        get { return amount; }
        set { amount = value; }
    }

    protected string merchant_Number = "8553778";
    protected string authentication_Mac;
    protected string Success_Url = "";
    protected string Failure_Url = "";
    protected string Cancel_Url = "";
    protected string language = "EN";
    protected string paymentType;
    protected string Reserve;

    protected void Page_Load(object sender, EventArgs e)
    {

        #region culture set
        if (((User)Session["UserData"]).Ci.TwoLetterISOLanguageName.ToLower().IndexOf("fi") > -1)
        {
            language = "FI";
        }
        else if (((User)Session["UserData"]).Ci.TwoLetterISOLanguageName.ToLower().IndexOf("se") > -1)
        {
            language = "SE";
        }

        #endregion

        if (string.IsNullOrEmpty(WebRoot.BasePageRoot)) Response.Redirect(WebRoot.BasePage);

        if (this.Attributes["ReservationNumber"] != null)
        {
            reservationNumber = this.Attributes["ReservationNumber"].ToString();
        }

        if (this.Attributes["Amount"] != null)
        {
            amount = decimal.Parse(this.Attributes["Amount"].ToString());
        }

        if (this.Attributes["PayType"] != null)
        {
            paymentType = this.Attributes["PayType"].ToString();
        }

        if (this.Attributes["Reserve"] != null)
        {
            Reserve = this.Attributes["Reserve"].ToString();
        }

        string basePage = TvBo.WebRoot.BasePageRoot;        

        Success_Url = string.Format("{2}Payments/PaymentResultFi.aspx?PayType={4}&resNumber={0}&Amount={1}&Reserve={3}",
                                reservationNumber,
                                amount.ToString("#.00"),
                                basePage,
                                Reserve,
                                paymentType);
        Failure_Url = string.Format("{1}Payments/noPaymentResultFi.aspx?resNumber={0}&B=LUO",
                                reservationNumber,
                                basePage);
        Cancel_Url = string.Format("{1}Payments/noPaymentResultFi.aspx?resNumber={0}",
                                reservationNumber,
                                basePage);

        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        string key = "xo68H1o2d8";
        string data = merchant_Number + reservationNumber + Amount.ToString("#.00").Replace(",", "").Replace(".", "") + "1" + key;
        authentication_Mac = MD5Encode(data, key);
    }

    public string MD5Encode(string originalPassword, string key)
    {
        //Declarations
        Byte[] originalBytes;
        Byte[] encodedBytes;
        MD5 md5;

        //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
        md5 = new MD5CryptoServiceProvider();
        originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
        encodedBytes = md5.ComputeHash(originalBytes);

        //Convert encoded bytes back to a 'readable' string
        return BitConverter.ToString(encodedBytes).Replace("-", "");
    }


}
