﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebPaymentFinLuottokunta.ascx.cs"
    Inherits="themes_Detur_UserControls_WebPaymentFinLuottokunta" %>
</form>
<form id="PostForm" name="PostForm" action="https://dmp2.luottokunta.fi/dmp/html_payments"
method="post">
<input type="hidden" name="Merchant_Number" value="<%=merchant_Number%>" />
<input type="hidden" name="Card_Details_Transmit" value="0" />
<input type="hidden" name="Transaction_Type" value="1" />
<input type="hidden" name="Language" value="<%=language%>"/> 
<input type="hidden" name="Device_Category" value="1" />
<input type="hidden" name="Order_ID" value="<%=ReservationNumber%>" />
<input type="hidden" name="Amount" value="<%=Amount.ToString("#.00").Replace(",","").Replace(".","")%>" />
<input type="hidden" name="Currency_Code" value="978" />
<input type="hidden" name="Success_Url" value="<%=Success_Url%>" />
<input type="hidden" name="Failure_Url" value="<%=Failure_Url %>" />
<input type="hidden" name="Cancel_Url" value="<%=Cancel_Url %>" />
<input type="hidden" name="Authentication_Mac" value="<%=authentication_Mac%>" />
</form>

<script language="javascript" type="text/javascript">
    var vPostForm = document.PostForm;
    vPostForm.submit();
</script>

