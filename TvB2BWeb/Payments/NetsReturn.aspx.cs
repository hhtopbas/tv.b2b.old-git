﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class NetsReturn : System.Web.UI.Page
{
	string MerchantId
	{
		get { return "398194"; }
	}

	string Token
	{
        get { return "Jn2_q?M3"; }
	}

	string ResponseCode
	{
		get { return Request.QueryString["responseCode"]; }
	}

	string ResNumber
	{
		get { return Request.QueryString["resNumber"]; }
	}

	string PayType
	{
		get { return Request.QueryString["PayType"]; }
	}

	string Reserve
	{
		get { return Request.QueryString["Reserve"]; }
	}

    string Amount
    {
        get { return Request.QueryString["Amount"]; }
    }

	string TransactionId
	{
		get { return Request.QueryString["transactionId"]; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Page.IsPostBack)
			return;

        if (string.IsNullOrEmpty(WebRoot.BasePageRoot)) Response.Redirect("~/Default.aspx");

		if (ResponseCode == "Cancel")
		{
            Response.Redirect(String.Concat(WebRoot.BasePageRoot + "Payments/BeginPayment.aspx?ResNo=", ResNumber));
		}
		else if (ResponseCode == "OK")
		{
			var response = new NetsSVC.Netaxept().Process(MerchantId, Token, new NetsSVC.ProcessRequest
			{
				Operation = "SALE",
				TransactionId = TransactionId
			});

			if (response.ResponseCode == "OK")
			{
                Response.Redirect(String.Format(WebRoot.BasePageRoot + "Payments/PaymentResultFi.aspx?PayType={0}&Reserve={1}&resNumber={2}&Amount={3}", PayType, Reserve, ResNumber, Amount));
			}
			else
			{
				Response.Write(String.Format("An error occured:<br /> Source: {0}<br /> Text: {1}", response.ResponseSource, response.ResponseText));
			}
		}
	}
}