﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class NetsSweReturn : System.Web.UI.Page
{
    string _MerchantId
    {
        get { return "767095"; }

    }
    string MerchantId { get; set; }
    string _Token
    {
        get { return "f?5L_Mx"; }
    }
    string Token { get; set; }
    string _EndPoint
    {
        get { return "https://epayment.bbs.no/Netaxept.svc"; }
    }
    string EndPoint { get; set; }
    string ResponseCode
    {
        get { return Request.QueryString["responseCode"]; }
    }

    string ResNumber
    {
        get { return Request.QueryString["resNumber"]; }
    }

    string PayType
    {
        get { return Request.QueryString["PayType"]; }
    }

    string Reserve
    {
        get { return Request.QueryString["Reserve"]; }
    }

    string Amount
    {
        get { return Request.QueryString["Amount"]; }
    }

    string TransactionId
    {
        get { return Request.QueryString["transactionId"]; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
            return;

        if (string.IsNullOrEmpty(WebRoot.BasePageRoot)) Response.Redirect("~/Default.aspx");
        MerchantId = _MerchantId;
        Token = _Token;
        EndPoint = _EndPoint;
        string isTest = ConfigurationManager.AppSettings["Test"];
        string isPayLog = ConfigurationManager.AppSettings["PaymentLogIsActive"];
        if (!string.IsNullOrEmpty(isTest) && isTest.Equals("1"))
        {
            MerchantId = "12003195";
            Token = "bP-3S)8";
            EndPoint = "https://test.epayment.nets.eu/Netaxept.svc";
        }
        Guid gRec = Guid.Empty;
        bool paymentLog = false;
        if (!string.IsNullOrEmpty(isPayLog) && isPayLog.Equals("1"))
            paymentLog = true;
        if(paymentLog)
            WebLog.SaveWebServiceLog(null, null, "NetsSweReturn-ResNo:"+ResNumber, Newtonsoft.Json.JsonConvert.SerializeObject(Request.Url.Query), null, null);

        if (ResponseCode == "Cancel")
        {
            Response.Redirect(String.Concat(WebRoot.BasePageRoot + "Payments/BeginPayment.aspx?ResNo=", ResNumber), false);
        }
        else if (ResponseCode == "OK")
        {

            NetsSVC_Last.ProcessResponse response = null;
            var isSaled = false;
            try
            {
                string errMsg = string.Empty;
                

                var request = new NetsSVC_Last.ProcessRequest
                {
                    Operation = "SALE",
                    TransactionId = TransactionId
                };

                response = new NetsSVC_Last.NetaxeptClient("Endpoint", EndPoint).Process(MerchantId, Token, request);

                if (response.ResponseCode == "OK")
                    isSaled = true;

                if (paymentLog)
                    gRec = WebLog.SaveWebServiceLog(null, null, "NetsSweReturn-ResponseOk-ResNo:" + ResNumber, Newtonsoft.Json.JsonConvert.SerializeObject(request), Newtonsoft.Json.JsonConvert.SerializeObject(response), null);
                if (isSaled)
                    PaymentFinal.Finalize(TransactionId, ResNumber, PayType, Amount, ref errMsg);
                    
               
            }
            catch (Exception ex)
            {
                if (paymentLog)
                    WebLog.SaveWebServiceLog(gRec, null, null, null, null, ex.Message);
                string errorMsg = String.Format("-Payment Response Error: Card information is incorrect. Please try again.", ResNumber);
                string errorLogMsg = string.Concat(ex.Message, "-Payment Response Error: ResNo={0} Error Detail={1}", ResNumber, ex);
                //Business.Services.CmsService.LogService.Save(new Business.BusinessObjects.Cms.Log { ModuleCode = "Nets Payment", Operation = "Error2", Description = "Error : " + ResNumber + " : " + errorLogMsg });
                Response.Redirect(String.Concat(WebRoot.BasePageRoot + "Payments/BeginPayment.aspx?ResNo=", ResNumber));
            }
            if (isSaled)
            {
                string showPaymentMessage = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ReservationPaymentOK").ToString(),
                                        ResNumber,
                                        !string.IsNullOrEmpty(Request.Params["Amount"]) ? (string)Request.Params["Amount"] + " SEK" : "");
                Response.Redirect(WebRoot.BasePageRoot + "Payments/" + string.Format("ShowPaymentFinal.aspx?ResNo={0}&Message={1}&Amount={2}", ResNumber, showPaymentMessage, Amount), true);
            }
            else
            {
                //Business.Services.CmsService.LogService.Save(new Business.BusinessObjects.Cms.Log { ModuleCode = "Nets Payment", Operation = "Error1", Description = "Error : " + ResNumber + " : " + response.ResponseText });
                Response.Redirect(String.Concat(WebRoot.BasePageRoot + "Payments/BeginPayment.aspx?ResNo=", ResNumber), false);
                //return Redirect(defaultUrl + String.Format("webui/paymenterror?token={0}&resNumber={1}&errormessage={2}", "", ResNumber, response.ResponseText));
            }

        }
        else
        {
            Response.Redirect(String.Concat(WebRoot.BasePageRoot + "Payments/BeginPayment.aspx?ResNo=", ResNumber));
        }

    }
}