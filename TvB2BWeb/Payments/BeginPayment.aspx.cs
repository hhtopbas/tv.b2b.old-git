﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Text;
using System.Web.Services;
using System.Threading;
using TvTools;
using System.Security.Cryptography;
using System.Globalization;
using System.Web.Script.Services;
using System.Configuration;

public partial class Payments_BeginPayment : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        string isPayLog = ConfigurationManager.AppSettings["PaymentLogIsActive"];
        bool paymentLog = false;
        if (!string.IsNullOrEmpty(isPayLog) && isPayLog.Equals("1"))
            paymentLog = true;
        if (paymentLog && !string.IsNullOrEmpty(Request.QueryString["ResNo"]))
            WebLog.SaveWebServiceLog(null, null, "BeginPayment-ResNo:" + Request.QueryString["ResNo"], "BeginPayment", null, null);

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getResPayPlan(string ResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<BeginPayment_ResPayPlan> retValPaymentType = new List<BeginPayment_ResPayPlan>();

        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        if (ResData.ResMain.ResNo != ResNo)
            ResNo = ResData.ResMain.ResNo;
        string errorMsg = string.Empty;
        List<resPayPlanRecord> paymentPlan = new ReservationMonitor().getPaymentPlan(ResNo, ref errorMsg);
        if (paymentPlan == null || paymentPlan.Count < 1)
            return "";

        decimal balance = paymentPlan.FirstOrDefault().Balance.HasValue ? paymentPlan.FirstOrDefault().Balance.Value : Convert.ToDecimal(0);
        decimal totalAmount = paymentPlan.Sum(s => s.Amount).HasValue ? paymentPlan.Sum(s => s.Amount).Value : Convert.ToDecimal(0);

        DateTime maxDueDate = paymentPlan.Max(p => p.DueDate);
        DateTime dueDate = paymentPlan.FirstOrDefault().DueDate;

        string currency = paymentPlan.FirstOrDefault().Cur;

        int paxCountForBankComission = new Payments().GetPaxCountForBankComission(ResData.ResMain.ResNo, ref errorMsg);

        string paymentListRBTemp = "<input id=\"paymentList{0}\" type=\"radio\" name=\"paymentList\" value=\"{1}\" {2} onclick=\"changePaymentList('', {0}, {5});\"  /><label for=\"paymentList{3}\">{4}</label><br />";
        int cnt = 1;
        StringBuilder payPlan = new StringBuilder();

        int remaningDay = ResData.ResMain.BegDate.Value.Subtract(DateTime.Today).Days;
        List<PayTypeRecord> payTypeList = new TvBo.Common().getPayType(UserData, UserData.Market, ref errorMsg);
        List<PayTypeRecord> pTypeL = (from q in payTypeList
                                      where (q.ValidForB2B.HasValue && q.ValidForB2B.Value) &&
                                          q.Market == UserData.Market &&
                                          (!q.ValidRemDaytoCin.HasValue || (q.ValidRemDaytoCin.Value > remaningDay))
                                      select q).ToList<PayTypeRecord>();

        payPlan.Append("<div id=\"divPaymentType\">");
        cnt = 0;
        foreach (var row in pTypeL)
        {
            if (!Equals(row.Code, "BANKGIRO"))
            {
                decimal discountVal = row.DiscountVal.HasValue ? row.DiscountVal.Value : (decimal)0;
                decimal perVal = (decimal)0;
                // Kayako : 13503 istinaden kaldırıldı.
                //if (ResData.ResMain.Market == "DENMAR" || ResData.ResMain.Market == "FINMAR")
                //{
                SupDisRecord supDis = new SupDiss().getSupDisDetail(UserData, row.SupDisCode, true, ref errorMsg);
                if (supDis != null)
                {
                    perVal = supDis.PerVal.HasValue ? supDis.PerVal.Value : (decimal)0;
                }
                //}
                payPlan.Append("<p>");
                cnt++;
                payPlan.AppendFormat("<input id=\"paymentType{0}\" type=\"radio\" name=\"paymentType\" value=\"{1}\" bankComm=\"{4}\" onclick=\"changePaymentType({5})\" perVal=\"{7}\"/><label for=\"paymentType{3}\"><img title='' src='LOGO_{2}.gif' width=\"120px\" height=\"45px\" />&nbsp;{6}</label>",
                        cnt,
                        row.Code,
                        row.Code.ToUpper(),
                        cnt,
                        discountVal * paxCountForBankComission,
                        cnt,
                        discountVal * paxCountForBankComission > 0 ? " ( Commission=" + (discountVal * paxCountForBankComission).ToString("#,###.00") + " " + ResData.ResMain.SaleCur + " )" : "",
                        (perVal * 100).ToString("#."));
                payPlan.Append("</p>");

                decimal? bankComm = discountVal * paxCountForBankComission;
                string logoDesc = discountVal * paxCountForBankComission > 0 ? " ( Commission=" + (discountVal * paxCountForBankComission).ToString("#,###.00") + " " + ResData.ResMain.SaleCur + " )" : "";
                retValPaymentType.Add(
                    new BeginPayment_ResPayPlan
                    {
                        payID = cnt.ToString(),
                        paymentCode = row.Code,
                        paymentName = "",
                        bankComm = bankComm > 0 ? bankComm.Value.ToString() : "0",
                        logoCode = row.Market + "_" + row.Code.ToUpper(),
                        logoDesc = row.Name + " " + logoDesc,
                        newAmount = (perVal * 100).ToString("#.")
                    });
            }
        }

        ++cnt;
        if (maxDueDate > DateTime.Today && balance >= totalAmount && UserData.Market != "SWEMAR")
        {
            payPlan.Append("<p>");
            cnt++;
            payPlan.AppendFormat("<input id=\"paymentType{0}\" type=\"radio\" name=\"paymentType\" value=\"{1}\" bankComm=\"{2}\" onclick=\"changePaymentType({3})\"/><label for=\"paymentType{4}\">{5}</label>",
                    cnt,
                    "_RESERVE_",
                    0,
                    cnt,
                    cnt,
                    HttpContext.GetGlobalResourceObject("BeginPayment", "lblReserve"));
            payPlan.Append("</p>");

            retValPaymentType.Add(
                    new BeginPayment_ResPayPlan
                    {
                        payID = cnt.ToString(),
                        paymentCode = "_RESERVE_",
                        paymentName = "Pay later",
                        bankComm = "0",
                        logoCode = "",
                        logoDesc = "Pay later.  " + HttpContext.GetGlobalResourceObject("BeginPayment", "lblReserve").ToString(),
                        newAmount = ((decimal)0).ToString("#.")
                    });
        }
        payPlan.Append("</div>");

        payPlan.Append("<br />");

        List<BeginPayment_PaymentTypeList> retValPaymentTypeList = new List<BeginPayment_PaymentTypeList>();
        //"<input id=\"paymentList{0}\" type=\"radio\" name=\"paymentList\" value=\"{1}\" {2} onclick=\"changePaymentList('', {0}, {5});\" /><label for=\"paymentList{3}\">{4}</label><br />";
        payPlan.Append("<div id=\"divPaymentPlanList\" style=\"display: none;\">");

        #region For Denmark


        #endregion

        #region deposite
        //if (maxDueDate > DateTime.Today)
        if (maxDueDate > DateTime.Today && balance >= totalAmount)
        {
            decimal? amount = paymentPlan.Where(p => p.DueDate == dueDate).Sum(p => p.Amount);
            string value = string.Concat(amount.Value.ToString("#."), ";", currency);

            string depositeStr = string.Format("<span id=\"depositeStr\" val=\"{1}\">{0}</span>",
                                        amount.HasValue ? (amount.Value.ToString("#,###.00") + " " + currency) : "",
                                        string.Concat((amount.Value * 100).ToString("#."), ";", currency));
            string text = string.Format(HttpContext.GetGlobalResourceObject("BeginPayment", "lblPaymentStr").ToString(),
                                        depositeStr,
                                        dueDate.ToString("dd MMMM yyyy"));
            payPlan.AppendFormat(paymentListRBTemp, cnt, value, "", cnt, text, "false", amount.HasValue ? amount.Value.ToString("#,###.00") + ";" + currency : "");

            retValPaymentTypeList.Add(
                    new BeginPayment_PaymentTypeList
                    {
                        payID = cnt.ToString(),
                        value = string.Concat(amount.Value.ToString("#."), ";", currency),
                        visible = true,
                        description = text,
                        changeListParam = "false"
                    });
            ++cnt;
        }
        #endregion

        #region partial payment
        /*
        string partialPayment = "<input id=\"paymentList{0}\" type=\"radio\" name=\"paymentList\" value=\"{1}\" {2} onclick=\"changePaymentList('partial', {0}, false);\" />" +
                                "<label for=\"paymentList{3}\">{4} :<input id=\"partialPayment{0}\" type=\"text\" style=\"width: 90px;\" minAmount=\"{5}\" value=\"{6}\" currency=\"{7}\" onchange=\"desiredChange({0});\" />&nbsp;{8}</label><br />";        
        if (!(balance <= 0 || maxDueDate <= DateTime.Today))
        {
            if (maxDueDate > DateTime.Today && balance >= totalAmount)
            {
                decimal? amount = paymentPlan.Where(p => p.DueDate == dueDate).Sum(p => p.Amount);
                Int16? adultCount = Conversion.getInt16OrNull(ResData.ResMain.Adult);
                //decimal? desiredAmount = Math.Round((amount.HasValue ? amount.Value : 0) / (adultCount.HasValue ? adultCount.Value : 1), 2); 2011-10-10 Hacı istedi
                decimal? desiredAmount = Math.Round((amount.HasValue ? amount.Value : 0), 2);
                string value = string.Concat(desiredAmount.ToString(), ";", currency);
                payPlan.AppendFormat(partialPayment,
                                            cnt,
                                            value,
                                            "",
                                            cnt,
                                            HttpContext.GetGlobalResourceObject("BeginPayment", "lblPayDesiredAmount"),
                                            desiredAmount.HasValue ? desiredAmount.Value.ToString("#.00") : "",
                                            desiredAmount.HasValue ? desiredAmount.Value.ToString("#.00") : "",
                                            currency,
                                            (maxDueDate > DateTime.Today && balance >= totalAmount) ? HttpContext.GetGlobalResourceObject("BeginPayment", "lblMinDediredPrice").ToString() + " (" + desiredAmount.Value.ToString("#,###.00") + ")" : "");
            }
            else
            {
                decimal? amount = paymentPlan.Where(p => p.DueDate == dueDate).Sum(p => p.Amount);
                Int16? adultCount = Conversion.getInt16OrNull(ResData.ResMain.Adult);
                decimal? desiredAmount = Math.Round(Convert.ToDecimal("1"), 2);
                string value = string.Concat(desiredAmount.ToString(), ";", currency);
                payPlan.AppendFormat(partialPayment,
                                            cnt,
                                            value,
                                            "",
                                            cnt,
                                            HttpContext.GetGlobalResourceObject("BeginPayment", "lblPayDesiredAmount"),
                                            desiredAmount.HasValue ? desiredAmount.Value.ToString("#.00") : "",
                                            "",
                                            currency,
                                            "");
            }
        }
        */
        #endregion

        #region total

        string valurStr = string.Format("<span id=\"maxValueStr\" val=\"{3}\">{0}</span> {1} <input id=\"hfMaxValue\" type=\"hidden\" value=\"{2}\" sel=\"\" />",
                                            balance.ToString("#.00"),
                                            currency,
                                            (balance * 100).ToString("#."),
                                            string.Concat((balance * 100).ToString("#."), ";", currency));
        string maxtext = string.Format(HttpContext.GetGlobalResourceObject("BeginPayment", "lblPaymentStrTotal").ToString(),
                                        valurStr,
                                        paymentPlan.LastOrDefault().DueDate.ToString("dd MMMM yyyy"));
        // string depositeStr = string.Format("<span id=\"depositeStr\">{0}</span>", amount.HasValue ? (amount.Value.ToString("#,###.00") + " " + currency) : "");
        string maxvalue = string.Concat(balance.ToString(), ";", currency);
        payPlan.AppendFormat(paymentListRBTemp, cnt, maxvalue, "", cnt, maxtext, "true", balance.ToString("#,###.00") + ";" + currency);

        retValPaymentTypeList.Add(
                    new BeginPayment_PaymentTypeList
                    {
                        payID = cnt.ToString(),
                        value = string.Concat(balance.ToString(), ";", currency),
                        visible = true,
                        description = maxtext,
                        changeListParam = "true"
                    });
        #endregion
        payPlan.Append("</div>");
        //return payPlan.ToString();
        return new
        {
            paymentType = retValPaymentType,
            paymentPlanList = retValPaymentTypeList,
            cultureNumber = Newtonsoft.Json.JsonConvert.SerializeObject(UserData.Ci.NumberFormat)
        };
    }

    [WebMethod(EnableSession = true)]
    public static string getPayment(string payList, string payType, string ResNo, string bankComm, string isLastRow, string perVal)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        decimal _bankCom = Conversion.getDecimalOrNull(bankComm).HasValue ? Conversion.getDecimalOrNull(bankComm).Value : Convert.ToDecimal(0);
        if (string.IsNullOrEmpty(isLastRow) || !string.Equals(isLastRow, "1"))
            _bankCom = Convert.ToDecimal(0);
        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        if (ResData.ResMain.ResNo != ResNo)
            ResNo = ResData.ResMain.ResNo;
        decimal? Amount = Conversion.getDecimalOrNull(payList.Split(';')[0]);
        string Currency = Conversion.getStrOrNull(payList.Split(';')[1]);
        if (!Amount.HasValue)
            return "{\"retVal\":\"-1\",\"data\":\"\"}";
        decimal? _suppVal = Conversion.getDecimalOrNull(perVal);
        if (_suppVal.HasValue && _suppVal.Value > 0)
            Amount = Amount.Value + ((Amount.Value / 100) * (_suppVal.Value / 100));
        List<resPayPlanRecord> paymentPlan = new ReservationMonitor().getPaymentPlan(ResNo, ref errorMsg);
        if (new Payments().checkPaymentUser(UserData, ResNo, ref errorMsg))
        {
            if (payList == "0;0") //Reserve ise depozite tarihine kadar option süresini değiştir
            {
                bool ok = new Reservation().SetResOptDate(UserData, ResNo, paymentPlan.LastOrDefault().DueDate, 0, ref errorMsg);
                if (ok)
                    return "{\"retVal\":\"_RESERVE_\",\"data\":\"\"}";
                else
                    return "{\"retVal\":\"-1\",\"data\":\"\"}";
            }
            else
            {
                decimal? amount = Conversion.getDecimalOrNull(payList.Split(';')[0]);
                if (_suppVal.HasValue && _suppVal.Value > 0)
                    amount = amount.Value + ((amount.Value / 100) * (_suppVal.Value / 100));
                string currency = Conversion.getStrOrNull(payList.Split(';')[1]);
                payList = amount + ";" + currency;

                if (UserData.Market == "FINMAR")
                {
                    string retVal = string.Empty;
                    string paymentId = Conversion.getStrOrNull(payType);
                    switch (paymentId)
                    {
                        case "LK":
                            retVal = "{\"retVal\":\"LK\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "NOR":
                            retVal = "{\"retVal\":\"NOR\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "SMP":
                            retVal = "{\"retVal\":\"SMP\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "TAP":
                            retVal = "{\"retVal\":\"TAP\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "OP-E":
                            retVal = "{\"retVal\":\"OP-E\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "AMEX":
                            retVal = "{\"retVal\":\"AMEX\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "AKT-E":
                            retVal = "{\"retVal\":\"AKT-E\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "S-P_E":
                            retVal = "{\"retVal\":\"S-P_E\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "POP-E":
                            retVal = "{\"retVal\":\"POP-E\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "HB-E":
                            retVal = "{\"retVal\":\"HB-E\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                    }
                    return retVal;
                }
                else
                    if (UserData.Market == "SWEMAR")
                {
                    string retVal = string.Empty;
                    string paymentId = Conversion.getStrOrNull(payType);
                    switch (paymentId)
                    {
                        //case "BNKNESTSWE":
                        //    retVal = "{\"retVal\":\"BNKNESTSWE\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                        //    break;
                        case "DEBITECH":
                            retVal = "{\"retVal\":\"DEBITECH\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        //case "BANKGIRO":
                        //    bool ok = new Reservation().SetResOptDate(UserData, ResNo, paymentPlan.LastOrDefault().DueDate, 0, ref errorMsg);
                        //    if (ok)
                        //        retVal = "{\"retVal\":\"BANKGIRO\",\"data\":\"\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                        //    else
                        //        retVal = "{\"retVal\":\"-1\",\"data\":\"\"}";
                        //    break;
                    }
                    return retVal;
                }
                else
                        if (UserData.Market == "NORMAR")
                {
                    string retVal = string.Empty;
                    string paymentId = Conversion.getStrOrNull(payType);
                    switch (paymentId)
                    {
                        case "NDBI":
                            retVal = "{\"retVal\":\"NDBI\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "WEBCCVIMA":
                            retVal = "{\"retVal\":\"WEBCCVIMA\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "WEBBANKCA":
                            retVal = "{\"retVal\":\"WEBBANKCA\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "AMEX":
                            retVal = "{\"retVal\":\"AMEX\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                    }
                    return retVal;
                }
                else
                            if (UserData.Market == "DENMAR")
                {
                    string retVal = string.Empty;
                    string paymentId = Conversion.getStrOrNull(payType);
                    switch (paymentId)
                    {
                        case "KKDK":
                            retVal = "{\"retVal\":\"KKDK\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "DKNO":
                            retVal = "{\"retVal\":\"DKNO\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                        case "DANKORT":
                            retVal = "{\"retVal\":\"DANKORT\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
                            break;
                    }
                    return retVal;
                }
                else
                    return "{\"retVal\":\"0\",\"data\":\"" + payList + "\",\"bankCom\":\"" + _bankCom.ToString("#.00") + "\"}";
            }
        }
        else
            return "{\"retVal\":\"-1\",\"data\":\"\"}";
    }

    public static string MD5Encode(string originalPassword, string key)
    {
        //Declarations
        Byte[] originalBytes;
        Byte[] encodedBytes;
        MD5 md5;

        //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
        md5 = new MD5CryptoServiceProvider();
        originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
        encodedBytes = md5.ComputeHash(originalBytes);

        //Convert encoded bytes back to a 'readable' string
        return BitConverter.ToString(encodedBytes).Replace("-", "");
    }

    internal int getDecimalLengt(int len)
    {
        string decimalLen = "";
        for (int i = 0; i < len; i++)
        {
            decimalLen += "0";
        }
        decimalLen = "1" + decimalLen;
        return Convert.ToInt32(decimalLen);
    }

    protected void nextBtn_Click(object sender, EventArgs e)
    {
        string controlName = Request.Params.Get("__EVENTTARGET");
        string passedArgument = Request.Params.Get("__EVENTARGUMENT");
        //WEBCCVIMA;1800.00;NOK
        string BankID = passedArgument.Split(';')[0];
        string amountStr = passedArgument.Split(';')[1];
        string _bankComm = passedArgument.Split(';')[3];
        decimal bankComm = Convert.ToDecimal(0);
        if (_bankComm.IndexOf('.') > -1)
        {
            _bankComm = _bankComm.Remove(_bankComm.IndexOf('.'), 1);
            bankComm = Conversion.getDecimalOrNull(_bankComm).HasValue ? Conversion.getDecimalOrNull(_bankComm).Value / 100 : Convert.ToDecimal(0);
        }
        else if (_bankComm.IndexOf(',') > -1)
        {
            _bankComm = _bankComm.Remove(_bankComm.IndexOf(','), 1);
            bankComm = Conversion.getDecimalOrNull(_bankComm).HasValue ? Conversion.getDecimalOrNull(_bankComm).Value / 100 : Convert.ToDecimal(0);
        }
        else
            bankComm = Conversion.getDecimalOrNull(_bankComm).HasValue ? Conversion.getDecimalOrNull(_bankComm).Value : Convert.ToDecimal(0);
        decimal? amount = null;
        if (amountStr.IndexOf('.') > -1)
        {
            int len = amountStr.Length - amountStr.IndexOf('.');
            amountStr = amountStr.Remove(amountStr.IndexOf('.'), 1);
            amount = TvTools.Conversion.getDecimalOrNull(amountStr) / (getDecimalLengt(len > 0 ? len - 1 : 0));
        }
        else if (amountStr.IndexOf(',') > -1)
        {
            int len = amountStr.Length - amountStr.IndexOf(',');
            amountStr = amountStr.Remove(amountStr.IndexOf(','), 1);
            amount = TvTools.Conversion.getDecimalOrNull(amountStr) / (getDecimalLengt(len > 0 ? len - 1 : 0));
        }
        else
            amount = TvTools.Conversion.getDecimalOrNull(amountStr);
        if (!amount.HasValue)
            return;
        amount = amount + bankComm;
        //amount = Conversion.getDecimalOrNull(passedArgument.Split(';')[1]);
        string errorMsg = string.Empty;
        Control userControl = new Control();
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        string resNo = ResData.ResMain.ResNo;

        //string amount = ResData.ResMain.PasPayable.ToString();

        string Reserve = Guid.NewGuid().ToString();
        bool insertOk = new Payments().createCheckPayment(resNo, Reserve, ref errorMsg);
        if (insertOk)
        {
            switch (BankID)
            {
                case "LK":
                    userControl = this.LoadControl("~/Payments/WebPaymentFinLuottokunta.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    pnlPayment.Controls.Add(userControl);
                    break;
                case "NOR":
                    userControl = this.LoadControl("~/Payments/WebPaymentFinNordea.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    pnlPayment.Controls.Add(userControl);
                    break;
                case "SMP":
                    userControl = this.LoadControl("~/Payments/WebPaymentFinSampo.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    pnlPayment.Controls.Add(userControl);
                    break;
                //case "DEBITECH":
                //    userControl = this.LoadControl("~/Payments/WebPaymentSweDebitech.ascx") as UserControl;
                //    pnlPayment.Controls.Clear();
                //    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                //    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                //    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                //    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                //    pnlPayment.Controls.Add(userControl);
                //    break;
                case "KKDK":
                    userControl = this.LoadControl("~/Payments/WebPaymentDkNets.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    pnlPayment.Controls.Add(userControl);
                    break;
                case "DANKORT":
                    userControl = this.LoadControl("~/Payments/WebPaymentDkNets.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);

                    ///B2C 15-04-2014 de durumu yeni bildirdi. Ticket 4168. 
                    if (ResData.ResMain.Market == "DENMAR")
                        ((UserControl)userControl).Attributes.Add("CardType", "DANKORT");

                    pnlPayment.Controls.Add(userControl);
                    break;
                case "DKNO":
                    userControl = this.LoadControl("~/Payments/WebPaymentDenNordea.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    pnlPayment.Controls.Add(userControl);
                    break;
                case "WEBCCVIMA":
                    userControl = this.LoadControl("~/Payments/WebPaymentNorNets.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    ((UserControl)userControl).Attributes.Add("CardType", "MasterCard,Visa");
                    pnlPayment.Controls.Add(userControl);
                    break;
                case "WEBBANKCA":
                    userControl = this.LoadControl("~/Payments/WebPaymentNorNets.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    ((UserControl)userControl).Attributes.Add("CardType", "BankAxess");
                    pnlPayment.Controls.Add(userControl);
                    break;
                case "TAP":
                    userControl = this.LoadControl("~/Payments/WebPaymentFinTapiola.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    pnlPayment.Controls.Add(userControl);
                    break;
                case "OP-E":
                    userControl = this.LoadControl("~/Payments/WebPaymentFinOp.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    pnlPayment.Controls.Add(userControl);
                    break;
                case "AMEX":
                    userControl = this.LoadControl("~/Payments/WebPaymentNorNets.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    ((UserControl)userControl).Attributes.Add("CardType", "AmericanExpress");
                    pnlPayment.Controls.Add(userControl);
                    break;
                case "BNKNESTSWE":
                case "DEBITECH":
                    userControl = this.LoadControl("~/Payments/WebPaymentSweNets.ascx") as UserControl;
                    pnlPayment.Controls.Clear();
                    ((UserControl)userControl).Attributes.Add("ReservationNumber", resNo);
                    ((UserControl)userControl).Attributes.Add("Amount", amount.ToString());
                    ((UserControl)userControl).Attributes.Add("PayType", BankID);
                    ((UserControl)userControl).Attributes.Add("Reserve", Reserve);
                    ((UserControl)userControl).Attributes.Add("CardType", "AmericanExpress");
                    pnlPayment.Controls.Add(userControl);
                    break;
            }
        }
    }
}

public class FinNordeaRecord
{
    public FinNordeaRecord()
    {
        _Version = "0003";
        _Stamp = "1998052212254471";
        _Rcv_Id = "12345678";
        _Rcv_Account = "29501800000014";
        _Ref = "55";
        _Language = "3";
        _Return = "www.detur.fi/payment?sts=ret";
        _Cancel = "www.detur.fi/payment?sts=cancel";
        _Reject = "www.detur.fi/payment?sts=reject";
        _Keyvers = "0001";
        _ALG = "0001";
        _PmtType = "M";
        _MAC = "60FC3A5668E0AFF3CB27CF32C610CB70";
    }

    string _Version;
    public string Version
    {
        get { return _Version; }
        set { _Version = value; }
    }

    string _Stamp;
    public string Stamp
    {
        get { return _Stamp; }
        set { _Stamp = value; }
    }

    string _Rcv_Id;
    public string Rcv_Id
    {
        get { return _Rcv_Id; }
        set { _Rcv_Id = value; }
    }

    string _Rcv_Account;
    public string Rcv_Account
    {
        get { return _Rcv_Account; }
        set { _Rcv_Account = value; }
    }

    string _Ref;
    public string Ref
    {
        get { return _Ref; }
        set { _Ref = value; }
    }

    string _Language;
    public string Language
    {
        get { return _Language; }
        set { _Language = value; }
    }

    string _Return;
    public string Return
    {
        get { return _Return; }
        set { _Return = value; }
    }

    string _Cancel;
    public string Cancel
    {
        get { return _Cancel; }
        set { _Cancel = value; }
    }

    string _Reject;
    public string Reject
    {
        get { return _Reject; }
        set { _Reject = value; }
    }

    string _Keyvers;
    public string Keyvers
    {
        get { return _Keyvers; }
        set { _Keyvers = value; }
    }

    string _ALG;
    public string ALG
    {
        get { return _ALG; }
        set { _ALG = value; }
    }

    string _PmtType;
    public string PmtType
    {
        get { return _PmtType; }
        set { _PmtType = value; }
    }

    string _MAC;
    public string MAC
    {
        get { return _MAC; }
        set { _MAC = value; }
    }




}

public class FinLuottokuntaRecord
{
    public FinLuottokuntaRecord()
    {
        _Card_Details_Transmit = "0";
        _Transaction_Type = "1";
        _Language = "EN";
        _Device_Category = "1";
        _Currency_Code = "978";
    }

    string _Merchant_Number;
    public string Merchant_Number
    {
        get { return _Merchant_Number; }
        set { _Merchant_Number = value; }
    }

    string _Card_Details_Transmit;
    public string Card_Details_Transmit
    {
        get { return _Card_Details_Transmit; }
        set { _Card_Details_Transmit = value; }
    }

    string _Transaction_Type;
    public string Transaction_Type
    {
        get { return _Transaction_Type; }
        set { _Transaction_Type = value; }
    }

    string _Language;
    public string Language
    {
        get { return _Language; }
        set { _Language = value; }
    }

    string _Device_Category;
    public string Device_Category
    {
        get { return _Device_Category; }
        set { _Device_Category = value; }
    }

    string _Order_ID;
    public string Order_ID
    {
        get { return _Order_ID; }
        set { _Order_ID = value; }
    }

    string _Amount;
    public string Amount
    {
        get { return _Amount; }
        set { _Amount = value; }
    }

    string _Currency_Code;
    public string Currency_Code
    {
        get { return _Currency_Code; }
        set { _Currency_Code = value; }
    }

    string _Success_Url;
    public string Success_Url
    {
        get { return _Success_Url; }
        set { _Success_Url = value; }
    }

    string _Failure_Url;
    public string Failure_Url
    {
        get { return _Failure_Url; }
        set { _Failure_Url = value; }
    }

    string _Cancel_Url;
    public string Cancel_Url
    {
        get { return _Cancel_Url; }
        set { _Cancel_Url = value; }
    }

    string _Authentication_Mac;
    public string Authentication_Mac
    {
        get { return _Authentication_Mac; }
        set { _Authentication_Mac = value; }
    }

}

public class BeginPayment_ResPayPlan
{
    public string payID { get; set; }
    public string paymentCode { get; set; }
    public string paymentName { get; set; }
    public string bankComm { get; set; }
    public string logoCode { get; set; }
    public string logoDesc { get; set; }
    public string newAmount { get; set; }
    public bool visible { get; set; }
    public BeginPayment_ResPayPlan()
    {
        this.visible = true;
    }
}

//"<input id=\"paymentList{0}\" type=\"radio\" name=\"paymentList\" value=\"{1}\" {2} onclick=\"changePaymentList('', {0}, {5});\" />
//<label for=\"paymentList{3}\">{4}</label><br />";

public class BeginPayment_PaymentTypeList
{
    public string payID { get; set; }
    public string value { get; set; }
    public string newValue { get; set; }
    public bool visible { get; set; }
    public string description { get; set; }
    public string changeListParam { get; set; }

    public BeginPayment_PaymentTypeList()
    {
        this.visible = true;
    }
}