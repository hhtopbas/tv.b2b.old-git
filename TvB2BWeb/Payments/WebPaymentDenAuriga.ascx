﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebPaymentDenAuriga.ascx.cs" Inherits="WebPaymentDenAuriga" %>

</form>

<form id="PostForm" name="PostForm" action="https://epayment.auriganet.eu/paypagegw" method="post">
<input type="hidden" name="Merchant_id" value="<%=merchantId%>">
<input type="hidden" name="Version" value="<%=Version%>">
<input type="hidden" name="Customer_refno" value="<%=customerRefNo%>">
<input type="hidden" name="Currency" value="<%=currency%>">
<input type="hidden" name="Amount" value="<%=amount%>">
<input type="hidden" name="VAT" value="<%=vat%>">
<input type="hidden" name="Payment_method" value="<%=paymentMethod%>">
<input type="hidden" name="Purchase_date" value="<%=purchaseDate%>">
<input type="hidden" name="Response_URL" value="<%=responseUrl%>">
<input type="hidden" name="Goods_description" value="<%=goodsDesc%>">
<input type="hidden" name="Language" value="<%=lang%>">
<input type="hidden" name="Comment" value="<%=comment%>">
<input type="hidden" name="Country" value="<%=country%>">
<input type="hidden" name="Cancel_URL" value="<%=cancelUrl%>">
<input type="hidden" name="MAC" value="<%=MAC%>"> 
</form>
<script language="javascript" type="text/javascript">
	var vPostForm = document.PostForm;
	vPostForm.submit();
</script>