﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BeginPayment.aspx.cs" Inherits="Payments_BeginPayment"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Begin Payment</title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <script src="../Scripts/NumberFormat154.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        var cultureNumber;

        $.blockUI.defaults.message = '<h1>' + '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>' + '<\/h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function tryNumberFormat(obj) {
            var nf = new NumberFormat(obj);
            if (cultureNumber != null) {
                nf.PERIOD = cultureNumber.CurrencyGroupSeparator;
                nf.COMMA = cultureNumber.CurrencyDecimalSeparator;
            }
            var rsdoS = nf.PERIOD;
            var rsdoD = nf.COMMA;
            nf.setPlaces(2);
            nf.setSeparators(true, rsdoS, rsdoD);
            obj = nf.toFormatted();
            return obj;
        }

        function logout() {
            //            self.parent.logout();
        }

        function changePaymentType(rowID) {
            $("#isLastRow").val('');
            var selectedPaymentType = $("#paymentType" + rowID);
            var bankComm = selectedPaymentType.attr('bankComm');
            var _perVal = selectedPaymentType.attr('perVal');
            if (_perVal == '') _perVal = '0';
            if (selectedPaymentType.val() == '_RESERVE_') {
                $("#divPaymentPlanList").hide();
                $("#divPaymentDesc").html('');
                $("#divPaymentDesc").hide();
            }
            else {
                var depositeStr = $("#depositeStr");
                var maxValueStr = $("#maxValueStr");
                var hfMaxValue = $("#hfMaxValue");
                var hfMaxVal = parseFloat(hfMaxValue.val()) / 100;
                var perVal = parseFloat(_perVal) / 10000;

                var bankC = parseFloat(bankComm);
                if (bankComm == '' || bankComm == undefined) bankC = 0;
                maxValueStr.html((hfMaxVal + bankC).toString());
                var paymentList = $('input[name=paymentList]');
                if (paymentList.length > 0) {
                    if (depositeStr != undefined && depositeStr.length > 0) {
                        var depositValStr = depositeStr.attr('val').split(';');
                        var depVal = parseFloat(depositValStr[0]) / 100;

                        depositeStr.html((tryNumberFormat(depVal) + (perVal != 0 ? ' + ' + tryNumberFormat(depVal * perVal) + ' = ' + tryNumberFormat(depVal + (depVal * perVal)) : '')) + ' ' + depositValStr[1]);
                    }
                    if (maxValueStr != undefined && maxValueStr.length > 0) {
                        var maximumValStr = maxValueStr.attr('val').split(';');
                        var maxVal = parseFloat(maximumValStr[0]) / 100;
                        maxValueStr.html((tryNumberFormat(maxVal) + (perVal != 0 ? ' + ' + tryNumberFormat(maxVal * perVal) + ' = ' + tryNumberFormat(maxVal + (maxVal * perVal)) : '')));// + ' ' + maximumValStr[1]);
                    }
                }
                $("#divPaymentPlanList").show();

                if (perVal != 0) {
                    var desc = 'Det er ikke muligt at betalte sin rejse med Mastercard Debit, Mastercard Cash eller Visa Electron. ' +
                               'Bemærk venligst, at hvis "betaling med kreditkort" vælges tillægges der et gebyr på ' + tryNumberFormat(perVal * 100) + '% af beløbet. ' +
                               'Betaling med Dankort er gratis.';
                    $("#divPaymentDesc").html(desc);
                    $("#divPaymentDesc").show();
                }
                else {
                    $("#divPaymentDesc").html('');
                    $("#divPaymentDesc").hide();
                }

            }
        }

        function desiredChange(cnt) {
            var partialPayment = $("#partialPayment" + cnt);
            var minAmount = partialPayment.attr('minAmount');
            var currency = partialPayment.attr('currency');
            if (partialPayment.val() != '') {
                if (parseFloat(partialPayment.val()) < parseFloat(minAmount))
                    partialPayment.val(minAmount);
            }
            $("#paymentList" + cnt).val(partialPayment.val() + ';' + currency);
        }

        function changePaymentList(opt, cnt, lastRow) {
            $("#isLastRow").val('');
            if (opt == 'partial') {
                var paymentList = $('input[name=paymentList]:checked');
                var partialPayment = $("#partialPayment" + cnt);
                var minAmount = partialPayment.attr('minAmount');
                var currency = partialPayment.attr('currency');
                if (partialPayment.val() != '') {
                    if (parseFloat(partialPayment.val()) < parseFloat(minAmount))
                        partialPayment.val(minAmount);
                }
                paymentList.val(partialPayment.val() + ';' + currency);
            }
            else {
                var paymentList = $('input[name=paymentList]:checked').val();
                if (lastRow) $("#isLastRow").val('1');
            }
        }

        function getResPayPlan() {
            $.ajax({
                type: "POST",
                url: "../Payments/BeginPayment.aspx/getResPayPlan",
                data: '{"ResNo":"' + $("#hfResNo").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg != null && msg.d != undefined && msg.d != null) {
                        var paymentType = msg.d.paymentType;
                        var paymentPlanList = msg.d.paymentPlanList;
                        $("#divPaymentType > tbody").html('');
                        var rows = '';
                        $.each(paymentType, function (i) {
                            var row = '<td valign="middle"  width="25px">';
                            row += '<input id="paymentType' + this.payID + '" type="radio" name="paymentType" value="' + this.paymentCode + '" bankComm="' + this.bankComm + '" onclick="changePaymentType(' + this.payID + ')" perVal="' + this.newAmount + '"/>';
                            row += '</td>';
                            row += '<td valign="middle" width="215px">';
                            if (this.logoCode != null && this.logoCode != '') {
                                row += '<label for="paymentType' + this.payID + '"><img title="" src="LOGO_' + this.logoCode + '.gif" width="205px" height="45px" /></label>';
                            } else {
                                row += '<label for="paymentType' + this.payID + '">' + this.logoDesc + '</label>';
                            }
                            row += '</td>';
                            row += '<td style="width: 10px;">';
                            row += '</td>';
                            row += '<td valign="middle">';
                            if (this.logoCode != null && this.logoCode != '') {
                                row += this.logoDesc;
                            }
                            else {
                                row += '&nbsp;';
                            }
                            row += '</td>';

                            rows += '<tr>' + row + '</tr>';
                        });
                        $("#divPaymentType").html(rows);

                        $("#divPaymentPlanList").html('');
                        $.each(paymentPlanList, function (i) {
                            $("#divPaymentPlanList").append('<input id="paymentList' + this.payID + '" type="radio" name="paymentList" value="' + this.value + '" ' + (this.visible == false ? 'checked="checked"' : '') + ' onclick="changePaymentList(\'\', ' + this.payID + ', ' + this.changeListParam + ');"  />');
                            $("#divPaymentPlanList").append('<label for="paymentList' + this.payID + '">' + this.description + '</label><br />');
                        });

                        cultureNumber = $.json.decode(msg.d.cultureNumber);
                        changePaymentList();
                    }
                },
                error: function (xhr, msg, e) {
                    alert(xhr.responseText);
                }
            });
        }

        function getPayment() {
            var paymentList = $('input[name=paymentList]:checked').val();
            document.getElementById("<%= hfpaymentList.ClientID %>").value = paymentList;
            var paymentTypeObj = $('input[name=paymentType]:checked');
            var perVal = paymentTypeObj.attr('perVal');
            var paymentType = paymentTypeObj.val();
            document.getElementById("<%= hfpaymentType.ClientID %>").value = paymentType;
            var bankComm = paymentTypeObj.attr("bankComm");
            if (paymentType != '_RESERVE_') {
                if (paymentType == undefined || paymentList == undefined) {
                    alert("Please select criteria.");
                    return;
                }
                else {
                }
            }
            else {
                paymentList = '0;0';
            }

            var params = new Object();
            params.payList = paymentList;
            params.payType = paymentType;
            params.ResNo = $("#hfResNo").val();
            params.bankComm = bankComm;
            params.isLastRow = $("#isLastRow").val();
            params.perVal = perVal;
            $.ajax({
                type: "POST",
                url: "../Payments/BeginPayment.aspx/getPayment",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var _data = $.json.decode(msg.d);
                    var retVal = _data.retVal;
                    if (retVal != '_RESERVE_') {
                        var bankCom = _data.bankCom;
                        if (retVal == 'LK') {
                            __doPostBack('nextBtn', 'LK;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'NOR') {
                            __doPostBack('nextBtn', 'NOR;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'SMP') {
                            __doPostBack('nextBtn', 'SMP;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'DEBITECH') {
                            __doPostBack('nextBtn', 'DEBITECH;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'NDBI') {
                            __doPostBack('nextBtn', 'NDBI;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == "BANKGIRO") {
                            self.window.close;
                        } else if (retVal == 'KKDK') {
                            __doPostBack('nextBtn', 'KKDK;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'DKNO') {
                            __doPostBack('nextBtn', 'DKNO;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'DANKORT') {
                            __doPostBack('nextBtn', 'DANKORT;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'WEBCCVIMA') {
                            __doPostBack('nextBtn', 'WEBCCVIMA;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'WEBBANKCA') {
                            __doPostBack('nextBtn', 'WEBBANKCA;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'TAP') {
                            __doPostBack('nextBtn', 'TAP;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'OP-E') {
                            __doPostBack('nextBtn', 'OP-E;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'AMEX') {
                            __doPostBack('nextBtn', 'AMEX;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'AKT-E') {
                            __doPostBack('nextBtn', 'AKT-E;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'S-P_E') {
                            __doPostBack('nextBtn', 'S-P_E;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'POP-E') {
                            __doPostBack('nextBtn', 'POP-E;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'HB-E') {
                            __doPostBack('nextBtn', 'HB-E;' + _data.data + ";" + bankCom, '');
                        } else if (retVal == 'BNKNESTSWE') {
                            __doPostBack('nextBtn', 'BNKNESTSWE;' + _data.data + ";" + bankCom, '');
                        }


                    }
                    else if (retVal == '_RESERVE_') {
                        self.window.close();
                        //self.parent.paymentReturn();
                    }
                },
                error: function (xhr, msg, e) {
                    alert(xhr.responseText);
                }
            });
        }

        function cancelPayment() {
            window.close();
            //self.parent.paymentReturn();
        }

        function onLoad() {
            $.query = $.query.load(location.href);
            $("#hfResNo").val($.query.get('ResNo'));
            getResPayPlan();
        }
    </script>

</head>
<body onload="javascript:onLoad();">
    <form id="BeginPaymentForm" runat="server">
        <div class="Page">
            <tv1:Header ID="tvHeader" runat="server" />
            <input type="hidden" id="hfResNo" value="" />
            <div style="width: 100%; text-align: center; border: solid 1px #e1d8d8;">
                <div style="text-align: left; line-height: normal; height: 30px;">
                    <span style="text-align: left; font-size: 18pt;">Payment
                    </span>
                </div>
                <input id="isLastRow" type="hidden" value="" />
                <div id="divPayment" style="width: 98%; text-align: left; border: solid 1px #808080;">
                    <table id="divPaymentType" style="width: 90%; margin-left: 30px;">
                    </table>
                </div>
                <br />
                <div id="divPaymentPlanList" style="display: none; text-align: left; padding-left: 40px;">
                </div>
                <br />
                <div id="divPaymentDesc" style="display: none; text-align: left; padding-left: 40px;">
                </div>
            </div>
            <br />
            <div style="width: 80%; text-align: left; padding-left: 100px;">
                <input id="btnNext" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "sNext")%>'
                    onclick="getPayment();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                    style="width: 125px;" />
                <asp:Button ID="nextBtn" runat="server" Text="Button" OnClick="nextBtn_Click" Visible="False" />
                &nbsp;&nbsp;<input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel")%>'
                    onclick="cancelPayment();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                    style="width: 125px;" />
            </div>
            <asp:Panel runat="server" ID="pnlPayment">
            </asp:Panel>
            <asp:HiddenField ID="hfpaymentList" runat="server" />
            <asp:HiddenField ID="hfpaymentType" runat="server" />
        </div>
    </form>
</body>
</html>
