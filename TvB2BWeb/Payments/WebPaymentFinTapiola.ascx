﻿1<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebPaymentFinTapiola.ascx.cs" Inherits="themes_Detur_UserControls_WebPaymentFinTapiola" %>
</form>
<form  id="PostForm" name="PostForm"  action="https://pankki.tapiola.fi/service/paybutton" method="post">
    <input type="hidden" name="AAB_VERSION" value="0002"/> 
    <input type="hidden" name="AAB_STAMP" value="<%=ReservationNumber%>"/> 
    <input type="hidden" name="AAB_RCV_ID" value="<%=merchant_Number%>"/>  
     <input type="hidden" name="AAB_RCV_ACCOUNT" value="<%=merchant_Account%>"/>  
    <input type="hidden" name="AAB_RCV_NAME" value="DETUR"/>  
    <input type="hidden" name="AAB_LANGUAGE" value="<%=language.ToString()%>"/>
    <input type="hidden" name="AAB_AMOUNT" value="<%=Amount.ToString("#.00").Replace(".",",")%>"/>
    <input type="hidden" name="AAB_REF" value="<%=ref_Number%>"/>
    <input type="hidden" name="AAB_DATE" value="EXPRESS"/>
    <input type="hidden" name="AAB_RETURN" value="<%=Success_Url%>"/>
    <input type="hidden" name="AAB_CANCEL" value="<%=Cancel_Url%>"/>
    <input type="hidden" name="AAB_REJECT" value="<%=Failure_Url%>"/>
    <input type="hidden" name="AAB_KEYVERS" value="0001"/>
    <input type="hidden" name="AAB_CUR" value="EUR"/> 
    <input type="hidden" name="AAB_CONFIRM" value="YES"/> 
    <input type="hidden" name="AAB_MAC" value="<%=authentication_Mac%>"/> 
</form>

<script language="javascript" type="text/javascript">
var vPostForm = document.PostForm;
vPostForm.submit();
</script>