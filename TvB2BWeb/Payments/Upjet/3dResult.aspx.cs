﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using BankIntegration;
using System.Globalization;
using System.Threading;

public partial class Akbank_3dResult : System.Web.UI.Page
{
    string storeKey { get { return "PrimaryPyramid32"; } }// Store key
    protected void Page_Load(object sender, EventArgs e)
    {
        String[] paymentParams = new String[] { "AuthCode", "Response", "HostRefNum", "ProcReturnCode", "TransId", "ErrMsg" };

        IEnumerator eResponse = Request.Form.GetEnumerator();
        while (eResponse.MoveNext())
        {
            String xkey = (String)eResponse.Current;
            String xval = Request.Form.Get(xkey);
            bool ok = true;
            for (int i = 0; i < paymentParams.Length; i++)
            {
                if (xkey.Equals(paymentParams[i]))
                {
                    ok = false;
                    break;
                }
            }
            if (ok)
                Response.Write("<tr><td>" + xkey + "</td><td>" + xval + "</td></tr>");
        }

        String hashparams = Request.Form.Get("HASHPARAMS");
        String hashparamsval = Request.Form.Get("HASHPARAMSVAL");
        String paramsval = "";
        int index1 = 0, index2 = 0;
        // hash hesaplamada kullanılacak değerler ayrıştırılıp değerleri birleştiriliyor.
        do
        {
            index2 = hashparams.IndexOf(":", index1);
            String val = Request.Form.Get(hashparams.Substring(index1, index2 - index1)) == null ? "" : Request.Form.Get(hashparams.Substring(index1, index2 - index1));
            paramsval += val;
            index1 = index2 + 1;
        }
        while (index1 < hashparams.Length);

        String hashval = paramsval + storeKey;         //elde edilecek hash değeri için paramsval e store key ekleniyor. (işyeri anahtarı)
        String hashparam = Request.Form.Get("HASH");

        String hash = new BILib().GetSHA1X64(hashval);

        if (!paramsval.Equals(hashparamsval) || !hash.Equals(hashparam)) //oluşturulan hash ile gelen hash ve hash parametreleri değerleri ile ayrıştırılıp edilen edilen aynı olmalı.
        {
            Response.Write("<h4>Güvenlik Uyarısı. Sayısal İmza Geçerli Değil</h4>");
        }
        String mdStatus = Request.Form.Get("mdStatus"); // 3d işlemin sonucu
        if (mdStatus.Equals("1") || mdStatus.Equals("2") || mdStatus.Equals("3") || mdStatus.Equals("4"))
        {
            for (int i = 0; i < paymentParams.Length; i++)
            {
                String paramname = paymentParams[i];
                String paramval = Request.Form.Get(paramname);
                Response.Write("<tr><td>" + paramname + "</td><td>" + paramval + "</td></tr>");
            }

            if ("Approved".Equals(Request.Form.Get("Response")))
            {
                //Response.Redirect("~/PaymentResult.aspx?ResNumber=");
                string ResNo = Request.Params["ResNo"].ToString();
                string errorMsg = string.Empty;
                BIUser UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
                CultureInfo ci = BILib.getCultureInfo();
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;

                decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
                BIResMainRecord resMain = new BIResMainRecord();
                if (_amount.HasValue)
                {
                    resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
                    if (resMain != null)
                    {
                        string baseCurr = "TL";
                        string saleCur = resMain.SaleCur;
                        if (!Equals(baseCurr, saleCur))
                        {
                            _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                        }
                    }
                }
                List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
                BIResCustRecord leader = resCust.Find(f => f.Leader == "Y");

                string LeaderName = string.Empty;
                if (leader != null)
                    LeaderName = leader.Surname + " " + leader.Name;
                decimal odenen = _amount.HasValue ? _amount.Value : 0;
                string posReferans = "Ref: " + ResNo;

                int PayTypeID = 17;

                bool okey = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, PayTypeID,
                                                             odenen, "TL", "",
                                                             -1, "", "",
                                                             null, null, -1, 0, posReferans, ResNo, "", ref errorMsg);
                if (okey)
                {
                    new BILib().SetResOptDate(ResNo, null, -1, ref errorMsg);
                    new BILib().setReservationComplate(UserData, ResNo, false, ref errorMsg);
                    lblMsg.Text = "Payment successful";
                    resNo.Text = ResNo;
                }
                else
                {
                    lblMsg.Text = "Payment has been received but did not entered into backoffice records.";
                    resNo.Text = ResNo;
                }
            }
            else
            {
                lblMsg.Text = "Payment failed";                
            }
        }
        else
        {
            lblMsg.Text = "Payment failed";            
        }
    }
}