﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentCreditCard.aspx.cs"
    Inherits="PaymentCreditCard" EnableViewState="false" EnableViewStateMac="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Begin Payment</title>

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showAlert(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                position: 'center',
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        $(this).dialog('close');
                        return true;
                    }
                }
            });
        }

        function getFormData(resNo, external, bank) {
            $.ajax({
                async: false,
                type: "POST",
                url: "PaymentCreditCard.aspx/getFormData",
                data: '{"ResNo":"' + resNo + '","external":"' + external + '","PayType":"' + bank + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function (i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                                    break;
                            }
                        });
                        $("#messageDiv").hide();
                        $("#mainDiv").show();
                    }
                },
                error: function (xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function btnCancelClick() {
            self.window.close();
        }

        function secure3DPayment() {
            $.query = $.query.load(location.href);
            $("#hfResNo").val($.query.get('ResNo'));
            var atlas = '';
            var bank = '';
            if ($.query.get('Atlas')) {
                atlas = $.query.get('Atlas');
                if (atlas == false) atlas = '';
                $("#hfExternal").val(atlas);
            }
            if ($.query.get('Bank')) {
                bank = $.query.get('Bank');
                if (bank == true) bank = '';
                $("#hfBankCode").val(bank);
            }
            var resNo = $("#hfResNo").val();
            $("#Pan").val($('#txtCardNumber1').val() + $('#txtCardNumber2').val() + $('#txtCardNumber3').val() + $('#txtCardNumber4').val());
            $("#Expiry").val($('#cmbExpireMonth').val() + $('#cmbExpireYear').val());
            $("#Cvv2").val($('#txtSecurityCode').val());
            //$("#hfBonusAmount").val($("#PurchAmount").val());
            $("#hfCardType").val($("#CCardType").val());

            $("#secureForm").submit();
        }

        function noneSecurePayment() {
            //string ResNo, string external, string PayType, string cardNumber, string expDate, string cvv2, string cardType, string holderName
            var params = new Object();
            params.ResNo = $("#hfResNo").val();
            params.external = $("#hfExternal").val();
            params.PayType = $("#hfBankCode").val();
            params.cardNumber = $("#txtCardNumber1").val() + $("#txtCardNumber2").val() + $("#txtCardNumber3").val() + $("#txtCardNumber4").val();
            params.expDate = $('#cmbExpireMonth').val() + $('#cmbExpireYear').val();
            params.cvv2 = $('#txtSecurityCode').val();
            params.cardType = $("#CCardType").val();
            params.holderName = $("#txtCardHolderName").val();

            $.ajax({
                async: false,
                type: "POST",
                url: "PaymentCreditCard.aspx/noneSecurePayment",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",                
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        if (data.Success == false) {
                            showAlert(data.Msg);
                        }
                        else {
                            showAlert('Payment is ok.');
                            $("#btnOK").hide();
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function threeDPay() {
            //string ResNo, string external, string PayType, string cardNumber, string expDate, string cvv2, string cardType, string holderName
            var params = new Object();
            params.ResNo = $("#hfResNo").val();            
            params.PayType = $("#hfBankCode").val();
            params.cardNumber = $("#txtCardNumber1").val() + $("#txtCardNumber2").val() + $("#txtCardNumber3").val() + $("#txtCardNumber4").val();
            params.expDate = $('#cmbExpireMonth').val() + $('#cmbExpireYear').val();
            params.cvv2 = $('#txtSecurityCode').val();
            params.cardType = $("#CCardType").val();
            params.holderName = $("#txtCardHolderName").val();
            
            $("#Pan").val(params.cardNumber);
            $("#Cvv2").val(params.cvv2);
            $("#Expiry").val(params.expDate);
            $("#CardType").val(params.cardType);

            $("#secureForm").submit();

            //$.ajax({
            //    async: false,
            //    type: "POST",
            //    url: "PaymentCreditCard.aspx/threeDPay",
            //    data: $.json.encode(params),
            //    contentType: "application/json; charset=utf-8",
            //    //contentType: "application/x-www-form-urlencoded",
            //    dataType: "json",
            //    success: function (msg) {
            //        if (msg.hasOwnProperty('d') && msg.d != null) {
            //            var data = msg.d;
            //            if (data.Success == false) {
            //                showAlert(data.Msg);
            //            }
            //            else {
            //                showAlert('Payment is ok.');
            //                $("#btnOK").hide();
            //            }
            //        }
            //    },
            //    error: function (xhr, msg, e) {
            //        showAlert(xhr.responseText);
            //    },
            //    statusCode: {
            //        408: function () {
            //            logout();
            //        }
            //    }
            //});
        }

        function btnOKClick() {
            //if ($("#3dSelect").is(':checked')) {
            //    secure3DPayment();
            //}
            //else {
            threeDPay();
            //    noneSecurePayment();
            //}
        }

        function noNumbers(kontrol, sayi, e) {
            var keynum;
            var keychar;
            var number;
            var unicode = e.charCode ? e.charCode : e.keyCode
            var numcheck; if (window.event) // IE
            {
                keynum = e.keyCode;
                number = 4;
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                keynum = e.which;
                number = 3;
            }
            if (keynum == 8 || e.charcode)
                return true;
            keychar = String.fromCharCode(keynum);
            numcheck = /\d/;

            if (keychar == null)
                return true;

            if (numcheck.test(keychar)) {
                if (kontrol == 'cs1')
                    cs1(sayi, number);
                if (kontrol == 'cs2')
                    cs2(sayi, number);
                if (kontrol == 'cs3')
                    cs3(sayi, number);
                return true;
            }
            else if (keynum < 48 || keynum > 57)
                return false;
            else
                return true;
        }

        function cs1(sayi, number) {
            if (sayi == number) { $("#txtCardNumber2").focus(); }
        }

        function cs2(sayi, number) {
            if (sayi == number) { $("#txtCardNumber3").focus(); }
        }

        function cs3(sayi, number) {
            if (sayi == number) { $("#txtCardNumber4").focus(); }
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            $("#hfResNo").val($.query.get('ResNo'));
            var atlas = '';
            var bank = '';
            if ($.query.get('Atlas')) {
                atlas = $.query.get('Atlas');
                if (atlas == false) atlas = '';
                $("#hfExternal").val(atlas);
            }
            if ($.query.get('Bank')) {
                bank = $.query.get('Bank');
                if (bank == true) bank = '';
                $("#hfBankCode").val(bank);
            }
            var resNo = $("#hfResNo").val();
            $("#3dSelect").on("click", function () {
                var secure = $("#3dSelect").attr("checked") == "checked";
                if (secure) {
                    $("#Panel2").hide();
                }
                else {
                    $("#Panel2").show();
                }
            });            
            getFormData(resNo, atlas, bank);
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <input id="hfResNo" type="hidden" />
        <input id="hfExternal" type="hidden" />
        <input id="hfBankCode" type="hidden" value="DENIZ" />
        <div id="messageDiv">
        </div>
        <div id="mainDiv" style="text-align: center; display: block;">
            <div style="width: 605px; text-align: center;">
                <div style="width: 600px; font-family: Tahoma; text-align: left; border: solid 1px #666;">
                    <div id="Panel2" style="text-align: center; border: solid 1px #666; display: block;">
                        <fieldset>
                            <legend style="line-height: 2;">
                                <label>
                                    <span style="font-size: 12pt;">»</span></label>
                                <strong style="font-size: 12pt;">Payment System Integration</strong> </legend>
                            <br />
                            <br />
                            <table style="font-size: 10pt; text-align: left;">
                                <tr>
                                    <td align="right">Reservation number :
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td align="left">
                                        <strong><span id="txtResNo">ResNo</span></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td align="left">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Amount :
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td align="left">
                                        <strong><span id="txtAmount">Amount</span></strong>
                                        <input id="amountVal" type="hidden" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td align="left">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <span>Card Owner Name :</span>
                                    </td>
                                    <td style="text-align: right">&nbsp;
                                    </td>
                                    <td>
                                        <input id="txtCardHolderName" type="text" style="width: 100%;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <span>Card Number :</span>
                                    </td>
                                    <td style="text-align: right">&nbsp;
                                    </td>
                                    <td>
                                        <input id="txtCardNumber1" maxlength="4" onkeypress="return noNumbers('cs1',this.value.length,event);"
                                            size="4" style="width: 47px;" />
                                        <input id="txtCardNumber2" maxlength="4" onkeypress="return noNumbers('cs2',this.value.length,event);"
                                            size="4" style="width: 47px;" />
                                        <input id="txtCardNumber3" maxlength="4" onkeypress="return noNumbers('cs3',this.value.length,event);"
                                            size="4" style="width: 47px;" />
                                        <input id="txtCardNumber4" maxlength="4" onkeypress="return noNumbers('cs4',this.value.length,event);"
                                            size="4" style="width: 47px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <span>Security number (CVC) :</span>
                                    </td>
                                    <td style="text-align: right">&nbsp;
                                    </td>
                                    <td>
                                        <input id="txtSecurityCode" maxlength="3" style="width: 33px;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <span>Exp. Date :</span>
                                    </td>
                                    <td style="text-align: right">&nbsp;
                                    </td>
                                    <td>
                                        <select id="cmbExpireMonth" style="width: 50px;">
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                        <select id="cmbExpireYear" style="width: 50px;">
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <span>Card type :</span>
                                    </td>
                                    <td style="text-align: right">&nbsp;
                                    </td>
                                    <td>
                                        <select id="CCardType" name="CCardType">
                                            <option value="0">Visa</option>
                                            <option value="1">MasterCard</option>
                                            <option value="3">Amex</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                            </table>
                            <br />
                        </fieldset>
                    </div>                   
                    <div id="finalPanel">
                        <table width="100%">
                            <tr>
                                <td style="height: 21px">
                                    <input id="3dSelect" type="checkbox" checked="checked" class="ui-helper-hidden" /><label for="3dSelect" class="ui-helper-hidden">I want to make payment via 3D Secure.</label>
                                </td>
                            </tr>
                            <tr align="center">
                                <td style="height: 26px">
                                    <input id="btnOK" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnOK")%>' style="height: 30px; width: 100px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                        onclick="btnOKClick()" />
                                    <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel")%>' style="height: 30px;"
                                        class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                        onclick="btnCancelClick()" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 26px" align="center">
                                    <img alt="" src="Visa.gif" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </form>
    <form id="secureForm" action="https://inter-vpos.com.tr/MPI/Default.aspx" name="secureForm" method="post">
        <input type="hidden" id="Lang" name="Lang" value="tr" />
        <input type="hidden" id="SecureType" name="SecureType" value="3DPay" />
        <input type="hidden" id="InstallmentCount" name="InstallmentCount" value="" />
        <input type="hidden" id="TxnType" name="TxnType" value="Auth" />
        <input type="hidden" id="Hash" name="Hash" value="" />
        <input type="hidden" id="Rnd" name="Rnd" value="" />
        <input type="hidden" id="FailUrl" name="FailUrl" value="" />
        
        <input type="hidden" id="Pan" name="Pan" value="" />
        <input type="hidden" id="Cvv2" name="Cvv2" value="" />
        <input type="hidden" id="Expiry" name="Expiry" value="" />
        <input type="hidden" id="CardType" name="CardType" value="" />

        <input type="hidden" id="BonusAmount" name="BonusAmount" value="" />        
        <input type="hidden" id="ShopCode" name="ShopCode" value="" />
        <input type="hidden" id="PurshAmount" name="PurchAmount" value="" />
        <input type="hidden" id="Currency" name="Currency" value="" />
        <input type="hidden" id="OrderId" name="OrderId" value="" />
        <input type="hidden" id="OkUrl" name="OkUrl" value="" />        
        
    </form>

    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
        </p>
    </div>
</body>
</html>
