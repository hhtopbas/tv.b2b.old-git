﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using TvBo;
using System.Threading;

public partial class Payments_ShowPaymentFinal : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;        
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        

        //lblResNo.Text = !string.IsNullOrEmpty(Request.Params["ResNo"]) ? (string)Request.Params["ResNo"] : "";
        lblMessage.Text = !string.IsNullOrEmpty(Request.Params["Message"]) ? (string)Request.Params["Message"] : "";
        //lblAmount.Text = !string.IsNullOrEmpty(Request.Params["Amount"]) ? (string)Request.Params["Amount"] : "";        
    }
}
