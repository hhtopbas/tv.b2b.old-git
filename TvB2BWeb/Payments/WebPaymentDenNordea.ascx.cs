﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using TvBo;

public partial class themes_Detur_UserControls_WebPaymentDenNordea : System.Web.UI.UserControl
{
    protected string paymentType;
    protected string Reserve;

    protected string version = "0002";
    public string ReservationNumber { get; set; }
    protected string merchant_Number = "3339663512";
    protected int language = 3;
    public decimal Amount { get; set; }
    protected string ref_Number = "121857";
    protected string date = "EXPRESS";
    protected string authentication_Mac;
    protected string confirm = "YES";
    protected string keyVers = "0001";
    protected string cur = "DKK";
    protected string returnUrl = "";
    protected string cancelUrl = "";
    protected string rejectUrl = "";

    private string key = "4pUATzQkM95kug4EvNaE6BbuvaKWmA8I";

    protected void Page_Load(object sender, EventArgs e)
    {
        string errorMsg = string.Empty;

        #region culture set
        if (((User)Session["UserData"]).Ci.TwoLetterISOLanguageName.ToLower().IndexOf("fi") > -1)
            language = 1;
        else if (((User)Session["UserData"]).Ci.TwoLetterISOLanguageName.ToLower().IndexOf("se") > -1)
            language = 2;
        else if (((User)Session["UserData"]).Ci.TwoLetterISOLanguageName.ToLower().IndexOf("dk") > -1)
            language = 5;

        #endregion
        

        if (this.Attributes["ReservationNumber"] != null)
        {
            ReservationNumber = this.Attributes["ReservationNumber"].ToString();
            ref_Number = new TvSystem().GetRefNoForNordea(ReservationNumber, ref errorMsg);
        }

        if (this.Attributes["PayType"] != null)
        {
            paymentType = this.Attributes["PayType"].ToString();
        }

        if (this.Attributes["Reserve"] != null)
        {
            Reserve = this.Attributes["Reserve"].ToString();
        }

        if (this.Attributes["Amount"] != null)
        {
            Amount = decimal.Parse(this.Attributes["Amount"].ToString());
        }

        string basePage = TvBo.WebRoot.BasePageRoot;

        returnUrl = string.Format("{0}Payments/PaymentResultFi.aspx?PayType={1}&Reserve={2}&resNumber={3}&Amount={4}",
                                basePage,    
                                paymentType,            
                                Reserve,
                                ReservationNumber,
                                Amount.ToString("#.00"));
        cancelUrl = string.Format("{0}Payments/noPaymentResultFi.aspx?resNumber={1}", 
                                basePage,
                                ReservationNumber);
        rejectUrl = string.Format("{1}Payments/noPaymentResultFi.aspx?resNumber={0}&B=NOR",
                                ReservationNumber,
                                basePage);            

        string data = String.Format("{0}&{1}&{2}&{3}&{4}&{5}&{6}&{7}&",
            version, ReservationNumber, merchant_Number, Amount.ToString("#.00").Replace(".", ","), ref_Number, date, cur, key);

        authentication_Mac = MD5Encode(data, "").ToUpper();
        //Response.Write(data);
    }

    public string MD5Encode(string originalPassword, string key)
    {
        //Declarations
        Byte[] originalBytes;
        Byte[] encodedBytes;
        MD5 md5;

        //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
        md5 = new MD5CryptoServiceProvider();
        originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
        encodedBytes = md5.ComputeHash(originalBytes);

        //Convert encoded bytes back to a 'readable' string
        return BitConverter.ToString(encodedBytes).Replace("-", "");
    }
}
