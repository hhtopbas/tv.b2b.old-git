﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowPaymentFinal.aspx.cs"
    Inherits="Payments_ShowPaymentFinal" %>

<%@ Register Src="~/Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.custom.min.js"></script>

    <script type="text/javascript">

        function init() {
            window.onunload = onCloseWindow;
        }

        function onCloseWindow(evt) {
            window.parent.paymentCloseAndRefresh();
            //window.location = window.parent.location;
        }

    </script>
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/jquery-ui.css" rel="stylesheet" />
</head>
<body onload="init()">
    <form id="form1" runat="server">
        <div class="Page">
            <tv1:Header ID="tvHeader" runat="server" />
            <br />
            <br />
            <div>
                <asp:Label ID="lblMessage" runat="server"></asp:Label><br />
                <asp:Label ID="lblResNo" runat="server"></asp:Label><br />
                <asp:Label ID="lblAmount" runat="server"></asp:Label>
                <br />
                <br />
                <br />
                <input type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnClose") %>' onclick="onCloseWindow()" 
                    class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" style="width: 150px;" />
            </div>
        </div>
    </form>
</body>
</html>
