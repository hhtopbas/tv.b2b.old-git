﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using BankIntegration;


public partial class MellatBegin : System.Web.UI.Page
{
    public static readonly string PgwSite = ConfigurationManager.AppSettings["PgwSite"];
    protected void Page_Load(object sender, EventArgs e)
    {
        string invoiceNumber = string.Empty; 
        if (!Page.IsPostBack)
        {
            string errorMsg = string.Empty;
            Int64 amount;            
            if (string.IsNullOrEmpty(Request.Params["Atlas"]))
                invoiceNumber = "8" + (string)Request.Params["ResNo"];
            else
            {
                if ((string)Request.Params["Atlas"] == "1")
                    invoiceNumber = "9" + (string)Request.Params["ResNo"];
                else
                    if ((string)Request.Params["Atlas"] == "2")
                        invoiceNumber = "7" + (string)Request.Params["ResNo"];
                    else
                        if ((string)Request.Params["Atlas"] == "3")
                            invoiceNumber = "6" + (string)Request.Params["ResNo"];
                        else
                            if ((string)Request.Params["Atlas"] == "4")
                                invoiceNumber = "3" + (string)Request.Params["ResNo"];
                            else
                                if ((string)Request.Params["Atlas"] == "5")
                                    invoiceNumber = "5" + (string)Request.Params["ResNo"];
            }

            decimal? _amount = new BILib().getResAmount((string)Request.Params["ResNo"], 0, ref errorMsg);
            if (_amount.HasValue)
            {
                BIUser UserData = new BILib().getReservationCreateUserData((string)Request.Params["ResNo"], ref errorMsg);
                BIResMainRecord resMain = new BILib().getResMain(UserData, (string)Request.Params["ResNo"], ref errorMsg);
                if (resMain != null)
                {
                    string baseCurr = "IRR";
                    string saleCur = resMain.SaleCur;
                    if (!string.Equals(baseCurr, saleCur))
                    {
                        _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                    }
                }
            }
            amount = _amount.HasValue ? Convert.ToInt64(_amount.Value) : 0;

            try
            {
                string result;
                string TerminalId = ConfigurationManager.AppSettings["MellatTerminalId"];
                string UserName = ConfigurationManager.AppSettings["MellatUserName"];
                string UserPassword = ConfigurationManager.AppSettings["MellatUserPassword"];
                long resNumber = long.Parse(Request["ResNumber"]+DateTime.Now.ToString("ddMMyyHHmmss"));
                string MellatCallBackUrl = ConfigurationManager.AppSettings["MellatCallBackUrl"] + "?ResNumber=" + invoiceNumber;                
                BypassCertificateError();

                BPService.PaymentGatewayImplService bpService = new BPService.PaymentGatewayImplService();
                result = bpService.bpPayRequest(Int64.Parse(TerminalId),
                    UserName,
                    UserPassword,
                    resNumber,
                    amount,
                    DateTime.Now.ToString("yyyyMMdd"),
                    DateTime.Now.ToString("HHmmss"),
                    "",
                    MellatCallBackUrl,
                    0);
                string message = new TvReport.SafReport.MellatResponseMessage().GetMellatResponseMessage(result);
                PayOutputLabel.Text = result;

                String[] resultArray = result.Split(',');
                if (resultArray[0] == "0")
                    ClientScript.RegisterStartupScript(typeof(Page), "ClientScript", "<script language='javascript' type='text/javascript'> postRefId('" + resultArray[1] + "');</script> ", false);
            }
            catch (Exception exp)
            {
                PayOutputLabel.Text = "Error: " + exp.Message;
            }

        }
    }

    void BypassCertificateError()
    {
        ServicePointManager.ServerCertificateValidationCallback +=
            delegate(
                Object sender1,
                X509Certificate certificate,
                X509Chain chain,
                SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };
    }
}