<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SafiranPayment.aspx.cs" Inherits="SafiranPayment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Safiran - Pasargan Bank / Payment System Integration </title>
    <style type="text/css">
        body, form { width: 100%; position: relative; text-align: center; margin:0 auto;}
        table { background-color: #EBF1EB; text-align: left; margin-left: auto; margin-right: auto; }
        td { border: 1px dotted #000; background-color: #fff; height: 23px; font-family: Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif; }
        input { border: 1px solid #EBF1EB; height: 23px; width: 200px; }
    </style>
</head>
<body>
    <form method="post" action="https://pep.shaparak.ir/gateway.aspx">
        <input type="hidden" name="TerminalCode" value="<%=terminalCode%>" />
        <input type="hidden" name="MerchantCode" value="<%=merchantCode%>" />
        <input type="hidden" name="Action" value="1003" />
        <input type="hidden" name="RedirectAddress" value="<%=redirectAddress%>" />
        <input type="hidden" name="TimeStamp" value="<%=timeStamp %>" />
        <input type="hidden" name="Sign" value="<%=sign %>" readonly="readonly" />
        <table cellpadding="5" cellspacing="5" width="600px">
            <tr>
                <td colspan="2" style="background: #f1f2f3; font-size: 1.0em; font-family: Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif; text-align: center;">
                    <span style="font-weight: bold;">PEP EPayment / Payment System Integration </span>
                </td>
            </tr>
            <tr>
                <td>Invoice Number
                </td>
                <td>
                    <input type="text" name="invoiceNumber" value="<%=invoiceNumber%>" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td>Invoice Date
                </td>
                <td>
                    <input type="text" name="invoiceDate" value="<%=invoiceDate%>" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td>Amount
                </td>
                <td>
                    <input type="text" name="amount" value="<%=amount%>" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <input type="submit" value="Secure Payment" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
