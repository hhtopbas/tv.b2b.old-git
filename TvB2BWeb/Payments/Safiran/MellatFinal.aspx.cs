﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using BankIntegration;
using System.Threading;
using System.Collections.Generic;

public partial class MellatFinal : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RefIdLabel.Text = Request.Params["RefId"];
        ResCodeLabel.Text = Request.Params["ResCode"];
        SaleOrderIdLabel.Text = Request.Params["SaleOrderId"];
        SaleReferenceIdLabel.Text = Request.Params["SaleReferenceId"];
        string resNo = Request.QueryString["ResNumber"];
        if (Request.Params["SaleReferenceId"]!=null && Request.Params["SaleReferenceId"]!="")
        {
            long orderId=long.Parse(Request.Params["RefId"]);
            long saleorderId=long.Parse(Request.Params["SaleOrderId"]);
            long salereferenceId=long.Parse(Request.Params["SaleReferenceId"]);
            string TerminalId = ConfigurationManager.AppSettings["MellatTerminalId"];
            string UserName = ConfigurationManager.AppSettings["MellatUserName"];
            string UserPassword = ConfigurationManager.AppSettings["MellatUserPassword"];
            BPService.PaymentGatewayImplService bpService = new BPService.PaymentGatewayImplService();
            string result = bpService.bpVerifyRequest(Int64.Parse(TerminalId),
                    UserName,
                    UserPassword,
                    orderId,
                    saleorderId,
                    salereferenceId);
            String[] resultArray = result.Split(',');
            if (resultArray[0] == "0")
            {
                returnMsg.Text = "Payment successful";
                setPayment(resNo, salereferenceId.ToString());                
            }                
            else
            {
                string message = new TvReport.SafReport.MellatResponseMessage().GetMellatResponseMessage(result);
                returnMsg.Text = message;
                //error
            }

        }               
    }

    protected void setPayment(string ResNo, string referans)
    {
        bool returnAtlas = false;
        bool returnBiletBank = false;
        bool returnHotelsPro = false;
        bool returnAxxaSigorta = false;
        bool returnAxxaVisa = false;
        returnAtlas = ResNo[0] == '9';
        returnBiletBank = ResNo[0] == '7';
        returnHotelsPro = ResNo[0] == '6';
        returnAxxaVisa = ResNo[0] == '3';
        returnAxxaSigorta = ResNo[0] == '5';
        ResNo = ResNo.Remove(0, 1);
        string errorMsg = string.Empty;
        BIUser UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        //ResDataRecord ResData = new ResTables().getReservationData(null, ResNo, ref errorMsg);
        decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
        BIResMainRecord resMain = new BIResMainRecord();
        if (_amount.HasValue)
        {
            resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
            if (resMain != null)
            {
                string baseCurr = "IRR";
                string saleCur = resMain.SaleCur;
                if (!Equals(baseCurr, saleCur))
                {
                    _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                }
            }
        }
        List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
        BIResCustRecord leader = resCust.Find(f => f.Leader == "Y");

        string LeaderName = string.Empty;
        if (leader != null)
            LeaderName = leader.Surname + " " + leader.Name;
        decimal odenen = _amount.HasValue ? _amount.Value : 0;
        string posReferans = "Ref: " + referans;

        int PayTypeID = 17;

        bool okey = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, PayTypeID,
                                                     odenen, "IRR", "",
                                                     -1, "", "",
                                                     null, null, -1, 0, posReferans, ResNo, "", ref errorMsg);
        if (okey)
        {
            new BILib().SetResOptDate(ResNo, null, -1, ref errorMsg);
            if (!(returnAtlas || returnBiletBank || returnHotelsPro))
                new BILib().setReservationComplate(UserData, ResNo, false, ref errorMsg);
        }

        if (returnAtlas || returnBiletBank || returnHotelsPro || returnAxxaSigorta || returnAxxaVisa)
        {
            //if (returnAtlas)
            //    hfreturnAtlas.Value = "1";
            //else if (returnBiletBank)
            //    hfreturnAtlas.Value = "2";
            //else if (returnHotelsPro)
            //    hfreturnAtlas.Value = "3";
            //else if (returnAxxaVisa)
            //    hfreturnAtlas.Value = "4";
            //else if (returnAxxaSigorta)
            //    hfreturnAtlas.Value = "5";

            string urlString = string.Empty;
            try
            {
                if (returnAtlas)
                {
                    string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "ATLAS"]);
                    urlString = extLink;
                }
                else
                    if (returnBiletBank)
                    {
                        string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "BILETBANK"]);
                        urlString = extLink;
                    }
                    else
                        if (returnHotelsPro)
                        {
                            string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "WORLDHOTELS"]);
                            urlString = extLink;
                        }
                        else
                            if (returnAxxaSigorta)
                            {
                                string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "CREDITCARD"]);
                                urlString = extLink;
                            }
                            else
                                if (returnAxxaVisa)
                                {
                                    string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "AXXA"]);
                                    urlString = extLink;
                                }
            }
            catch { }

            if (returnAtlas)
                Response.Redirect(urlString.ToLower().Replace("default.aspx", "completeatlas.aspx?ResNo=" + ResNo));
            else
                if (returnBiletBank)
                    Response.Redirect(urlString.ToLower().Replace("default.aspx", "completeextticket.aspx?ResNo=" + ResNo));
                else
                    if (returnHotelsPro)
                        Response.Redirect(urlString.ToLower().Replace("default.aspx", "bookingComplete.aspx?ResNo=" + ResNo));
                    else
                        if (returnAxxaSigorta)
                            Response.Redirect(urlString.ToLower().Replace("default.aspx", "policefinal.aspx?resNo=" + ResNo));
                        else
                            if (returnAxxaVisa)
                                Response.Redirect(urlString.ToLower().Replace("default.aspx", "visafinal.aspx?resNo=" + ResNo + "&user=" + (UserData.AgencyID + "/" + UserData.UserID)));
        }
    }

}