﻿<%@ page language="C#" autoeventwireup="true" CodeFile="noPayment.aspx.cs" inherits="noPayment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="SafiranPaymentResultForm" runat="server">
    <div>
        <h2><asp:Label ID="lblMsg" runat="server" /></h2><br />
        <strong>Reservation number :</strong><asp:Label ID="_iN" runat="server" /><br />
        <strong>Payment could not be received.</strong>
    </div>
    </form>
</body>
</html>
