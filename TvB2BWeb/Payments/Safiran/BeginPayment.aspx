﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BeginPayment.aspx.cs" Inherits="Payments_BeginPayment"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Begin Payment</title>

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../Scripts/jquery.url.js" type="text/javascript"></script>

    <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function dialogviewReportClose() {
            window.close();
            self.parent.dialogviewReportClose();
        }

        function cancelPayment() {
            window.close();
            self.parent.dialogviewReportClose();
        }

        function nextClick() {
            var currentUrl = $.url(window.location.href);
            var checkBank = $('input[name=bank]:checked').val();
            var atlas = $('input[name=bank]:checked').attr("atlas");
            var code = checkBank.split('|');
            if (code[0] == 'PASARGD225' || code[0] == 'TEBCCEUR' || code[0] == 'YKBCC' || code[0] == 'MELAT') {
                if (code[0] == 'PASARGARD') {
                    window.open(currentUrl.data.attr.protocol + '://' +
                                  currentUrl.data.attr.host + ':' +
                                  currentUrl.data.attr.port +
                                  currentUrl.data.attr.directory +
                                  code[1] + '?ResNo=' + $("#hfResNo").val() + (atlas != undefined && atlas != '' ? '&Atlas=' + atlas : '&Atlas='), '_blank');
                    window.close();
                    self.parent.dialogviewReportClose();
                }
                else {
                    window.open('http' + '://' +
                                      currentUrl.data.attr.host + ':' +
                                      currentUrl.data.attr.port +
                                      currentUrl.data.attr.directory +
                                      code[1] + '?ResNo=' + $("#hfResNo").val() + (atlas != undefined && atlas != '' ? '&Atlas=' + atlas : '&Atlas=') + '&Bank=' + code[0], '_blank');
                    window.close();
                    self.parent.dialogviewReportClose();
                }
            }
            else alert('Please select Payment type');
        }

        function getFormData(resNo, external) {
            $.ajax({
                async: false,
                type: "POST",
                url: "BeginPayment.aspx/getFormData",
                data: '{"ResNo":"' + resNo + '","Atlas":"' + external + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#paymentListDiv").html('');
                    $("#paymentListDiv").html(msg.d);
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function onLoad() {
            $.query = $.query.load(location.href);
            $("#hfResNo").val($.query.get('ResNo'));
            if ($.query.get('Atlas'))
                $("#hfExternal").val($.query.get('Atlas'));
            getFormData($("#hfResNo").val(), $("#hfExternal").val());
        }                        
    </script>

</head>
<body onload="javascript:onLoad();">
    <form id="BeginPaymentForm" runat="server">
    <div>
        <input id="hfResNo" type="hidden" value="" />
        <input id="hfExternal" type="hidden" value="" />
        <div style="width: 100%; text-align: center; height: 100%; margin: 10px 10px 10px 10px;
            border: solid 1px #666;">
            <div id="paymentListDiv">
            </div>
            <%--<span style="display: block;">
                <input id="pasargan" name="bank" type="radio" value="Pasargan" checked="checked" /><label
                    for="pasargan">Pasargad Bank / Payment System Integration</label>
            </span>
            <br />
            <input id="teb" name="bank" type="radio" value="TEB" /><label for="pasargan">TEB / Payment
                System Integration</label>--%>
            <div style="text-align: center;">
                <input id="nextBtn" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "sNext")%>'
                    style="width: 100px;" onclick="nextClick();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                &nbsp;&nbsp;<input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel")%>'
                    onclick="cancelPayment();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                    style="width: 125px;" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
