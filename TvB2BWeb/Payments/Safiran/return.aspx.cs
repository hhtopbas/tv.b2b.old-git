﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Xml;
using System.Globalization;
using System.Threading;
using BankIntegration;
using System.Configuration;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using Org.BouncyCastle.X509;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public partial class SafiranPaymentResult : System.Web.UI.Page
{
    protected String privateKey = "<RSAKeyValue><Modulus>xyxyff0bqWuSOnHk2D33lqCVBBFxdkxKhhMgN4ScEcF+pE4ETulgATMfcNB9DSGlUBEg3D0M99Rdr7THUbeqF/x/12rnKen7YGlc5KJ0FeOCXxR7qa0nIhxb5Jr763LQElPLRdflwcZqDfyYj/6J0p8rOYGCwjz+F/mTVsAzjz8=</Modulus><Exponent>AQAB</Exponent><P>5bqxBL7kiTAZqzCzO6j5KoCFmRXgIDdyN7uOdEDj9xxDHuyBOs3PKimxsEv4+MUuJYeqyWJ6V/gt8FmAs4wnKw==</P><Q>3fM9zpOKnsJUmcA2Xbj3OBpS1Hub5r4rboAoeERh//ImSwrq/Oq1MmPP7frVxvxkZ9rcaSeF0Ac0ytEyKA2uPQ==</Q><DP>iaBkuHEcPMviNNrCJbW/QCUq5GFkmihdduIYH7FYYQw/ceFCMlZmC95ao0GAcJjAYp96Q5eJr/Xwn5MfNXF7bQ==</DP><DQ>l4Ey75BHT5fXqBxNAC0ClIljgSffg+LMQuG4vl/vhYcGHLOXNI4CoiMADLLsuqgO4dXEHwOblCVXJBso4a51AQ==</DQ><InverseQ>Yi8JlJs8DirtAHIn6pfdYUXvkDEuiDMCQe5OGHQ6hO0LxGxV9MyMKBygz0ovPPUaUKrhQyXafg+nAoXQsSVoDQ==</InverseQ><D>Yi2drk8CwKue5CJaTOGW1vAHJH84r7iBj7+DxPogOHxp4bH3W7KcOVsq52BOFBf+tg4LZaTObKeJGuCA0942AEg36HA3xyt4X2vmslLRWB1Hg09p5eCjTgkwaXQzVfVAC/RfEkpronc5WRPxNtGswjBB/usAoiLZPxwpVoiL4lk=</D></RSAKeyValue>";
    protected String publicKey = "<RSAKeyValue><Modulus>xyxyff0bqWuSOnHk2D33lqCVBBFxdkxKhhMgN4ScEcF+pE4ETulgATMfcNB9DSGlUBEg3D0M99Rdr7THUbeqF/x/12rnKen7YGlc5KJ0FeOCXxR7qa0nIhxb5Jr763LQElPLRdflwcZqDfyYj/6J0p8rOYGCwjz+F/mTVsAzjz8=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
    protected string terminalCode = "226156";
    protected string merchantCode = "225146";
    protected string redirectAddress = "http://agency.safirantravel.com/Payments/Safiran/return.aspx"; //?invoiceNumber=" + invoiceNumber&result=;
    protected Int64 amount;
    protected string timeStamp;
    protected string invoiceDate;
    protected string invoiceNumber;
    protected string action = "1003";
    protected string sign;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            string errorMsg = string.Empty;
            if (!string.IsNullOrEmpty(Request.Params["iN"]) && !string.IsNullOrEmpty(Request.Params["iD"]) && !string.IsNullOrEmpty(Request.Params["tref"])) {
                string inTest = Request.Params["iN"].ToString();

                safBankReturnRecord getTransactionResult = GetTransactionResult(Request.Params["tref"].ToString());
                if (getTransactionResult.Result.HasValue && getTransactionResult.Result.Value) {
                    if (VerifyPayment(Request.Params["iN"].ToString(), ref errorMsg)) {
                        setPayment(Request.Params["iN"].ToString(), Request.Params["tref"].ToString());
                        if ((Request.Params["iN"].ToString()).Length > 0)
                            _iN.Text = (Request.Params["iN"].ToString()).Remove(0, 1);
                        _tref.Text = Request.Params["tref"].ToString();
                        lblMsg.Text = "Payment successful";
                    } else {
                        if ((Request.Params["iN"].ToString()).Length > 0)
                            _iN.Text = (Request.Params["iN"].ToString()).Remove(0, 1);
                        _tref.Text = Request.Params["tref"].ToString();
                        lblMsg.Text = "Payment could not be received.<br />" + errorMsg;
                    }
                } else {
                    if ((Request.Params["iN"].ToString()).Length > 0)
                        _iN.Text = (Request.Params["iN"].ToString()).Remove(0, 1);
                    _tref.Text = Request.Params["tref"].ToString();
                    lblMsg.Text = "Payment could not be received.";
                }
            } else {
                _iN.Text = "";
                _tref.Text = "";
                lblMsg.Text = "Payment could not be received.";
            }
        }
    }

    protected void setPayment(string ResNo, string referans)
    {
        bool returnAtlas = false;
        bool returnBiletBank = false;
        bool returnHotelsPro = false;
        bool returnAxxaSigorta = false;
        bool returnAxxaVisa = false;
        returnAtlas = ResNo[0] == '9';
        returnBiletBank = ResNo[0] == '7';
        returnHotelsPro = ResNo[0] == '6';
        returnAxxaVisa = ResNo[0] == '3';
        returnAxxaSigorta = ResNo[0] == '5';
        ResNo = ResNo.Remove(0, 1);
        string errorMsg = string.Empty;
        BIUser UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        //ResDataRecord ResData = new ResTables().getReservationData(null, ResNo, ref errorMsg);
        decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
        BIResMainRecord resMain = new BIResMainRecord();
        if (_amount.HasValue) {
            resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
            if (resMain != null) {
                string baseCurr = "IRR";
                string saleCur = resMain.SaleCur;
                if (!Equals(baseCurr, saleCur)) {
                    _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                }
            }
        }
        List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
        BIResCustRecord leader = resCust.Find(f => f.Leader == "Y");

        string LeaderName = string.Empty;
        if (leader != null)
            LeaderName = leader.Surname + " " + leader.Name;
        decimal odenen = _amount.HasValue ? _amount.Value : 0;
        string posReferans = "Ref: " + referans;

        int PayTypeID = 4;

        bool okey = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, PayTypeID,
                                                     odenen, "IRR", "",
                                                     -1, "", "",
                                                     null, null, -1, 0, posReferans, ResNo, "", ref errorMsg);
        if (okey) {
            new BILib().SetResOptDate(ResNo, null, -1, ref errorMsg);
            if (!(returnAtlas || returnBiletBank || returnHotelsPro))
                new BILib().setReservationComplate(UserData, ResNo, false, ref errorMsg);
        }

        if (returnAtlas || returnBiletBank || returnHotelsPro || returnAxxaSigorta || returnAxxaVisa) {
            if (returnAtlas)
                hfreturnAtlas.Value = "1";
            else if (returnBiletBank)
                hfreturnAtlas.Value = "2";
            else if (returnHotelsPro)
                hfreturnAtlas.Value = "3";
            else if (returnAxxaVisa)
                hfreturnAtlas.Value = "4";
            else if (returnAxxaSigorta)
                hfreturnAtlas.Value = "5";

            string urlString = string.Empty;
            try {
                if (returnAtlas) {
                    string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "ATLAS"]);
                    urlString = extLink;
                } else
                    if (returnBiletBank) {
                    string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "BILETBANK"]);
                    urlString = extLink;
                } else
                        if (returnHotelsPro) {
                    string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "WORLDHOTELS"]);
                    urlString = extLink;
                } else
                            if (returnAxxaSigorta) {
                    string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "CREDITCARD"]);
                    urlString = extLink;
                } else
                                if (returnAxxaVisa) {
                    string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "AXXA"]);
                    urlString = extLink;
                }
            }
            catch { }
            if (returnAtlas)
                Response.Redirect(urlString.ToLower().Replace("default.aspx", "completeatlas.aspx?ResNo=" + ResNo));
            else
                if (returnBiletBank)
                Response.Redirect(urlString.ToLower().Replace("default.aspx", "completeextticket.aspx?ResNo=" + ResNo));
            else
                    if (returnHotelsPro)
                Response.Redirect(urlString.ToLower().Replace("default.aspx", "bookingComplete.aspx?ResNo=" + ResNo));
            else
                        if (returnAxxaSigorta)
                Response.Redirect(urlString.ToLower().Replace("default.aspx", "policefinal.aspx?resNo=" + ResNo));
            else
                            if (returnAxxaVisa)
                Response.Redirect(urlString.ToLower().Replace("default.aspx", "visafinal.aspx?resNo=" + ResNo + "&user=" + (UserData.AgencyID + "/" + UserData.UserID)));
        }
    }

    private safBankReturnRecord GetTransactionResult(string tref)
    {
        ASCIIEncoding encoding = new ASCIIEncoding();
        string postData = "invoiceUID=" + tref;//"invoiceUID=" + tref;
        byte[] data = Encoding.UTF8.GetBytes(postData);

        // Prepare web request...
        System.Net.HttpWebRequest myRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("https://pep.shaparak.ir/CheckTransactionResult.aspx");
        myRequest.Method = "POST";
        myRequest.ContentType = "application/x-www-form-urlencoded";
        myRequest.ContentLength = data.Length;

        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteSertificateValidation);

        System.IO.Stream newStream = myRequest.GetRequestStream();
        // Send the data.
        newStream.Write(data, 0, data.Length);
        newStream.Close();

        string strResult = "";
        System.Net.WebResponse response = myRequest.GetResponse();
        using (System.IO.StreamReader srValue = new System.IO.StreamReader(response.GetResponseStream())) {
            strResult = srValue.ReadToEnd();
            srValue.Close();
        }

        if (!string.IsNullOrEmpty(strResult)) {
            var logdir = Server.MapPath(VirtualPathUtility.ToAbsolute("~/Log"));
            System.IO.File.WriteAllText(System.IO.Path.Combine(logdir, tref) + ".txt", strResult);
        }

        safBankReturnRecord retVal = new safBankReturnRecord();
        if (strResult.Length > 0) {
            retVal.Result = Convert.ToBoolean(getFieldXmlStr(strResult.ToString(), "result", "false").ToLower());
            retVal.Action = getFieldXmlStr(strResult.ToString(), "action", "");
            retVal.TransactionReferenceID = getFieldXmlStr(strResult.ToString(), "transactionReferenceID", "");
            retVal.InvoiceNumber = getFieldXmlStr(strResult.ToString(), "invoiceNumber", "");
            retVal.InvoiceDate = getFieldXmlStr(strResult.ToString(), "invoiceDate", "");
            retVal.MerchantCode = getFieldXmlStr(strResult.ToString(), "merchantCode", "");
            retVal.TerminalCode = getFieldXmlStr(strResult.ToString(), "terminalCode", "");
            retVal.TraceNumber = getFieldXmlStr(strResult.ToString(), "traceNumber", "");
            retVal.ReferenceNumber = getFieldXmlStr(strResult.ToString(), "referenceNumber", "");
            retVal.TransactionDate = getFieldXmlStr(strResult.ToString(), "transactionDate", "");

            return retVal;
        } else {
            retVal.Result = false;
            return retVal;
        }
        //return strResult.ToString().Substring(first, end - first);
    }

    private static bool RemoteSertificateValidation(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        if (sslPolicyErrors == SslPolicyErrors.None)
            return true;
        return false;
    }

    private bool VerifyPayment(string ResNo, ref string errorMsg)
    {
        string returnUrl = string.Empty;
        string _ResNo = ResNo.Substring(1);
        returnUrl = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["PaymentReturnUrl"]);
        redirectAddress = returnUrl;
        timeStamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        //if (string.IsNullOrEmpty(Request.Params["Atlas"]))
        //    invoiceNumber = "8" + (string)Request.Params["ResNo"];
        //else {
        //    if ((string)Request.Params["Atlas"] == "1")
        //        invoiceNumber = "9" + (string)Request.Params["ResNo"];
        //    else
        //        if ((string)Request.Params["Atlas"] == "2")
        //        invoiceNumber = "7" + (string)Request.Params["ResNo"];
        //    else
        //            if ((string)Request.Params["Atlas"] == "3")
        //        invoiceNumber = "6" + (string)Request.Params["ResNo"];
        //    else
        //                if ((string)Request.Params["Atlas"] == "4")
        //        invoiceNumber = "3" + (string)Request.Params["ResNo"];
        //    else
        //                    if ((string)Request.Params["Atlas"] == "5")
        //        invoiceNumber = "5" + (string)Request.Params["ResNo"];
        //    else
        //                        if ((string)Request.Params["Atlas"] == "6")
        //        invoiceNumber = "4" + (string)Request.Params["ResNo"];
        //}
        invoiceDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        decimal? _amount = new BILib().getResAmount(_ResNo, 0, ref errorMsg);
        if (_amount.HasValue) {
            BIUser UserData = new BILib().getReservationCreateUserData(_ResNo, ref errorMsg);
            BIResMainRecord resMain = new BILib().getResMain(UserData, _ResNo, ref errorMsg);
            if (resMain != null) {
                string baseCurr = "IRR";
                string saleCur = resMain.SaleCur;
                if (!string.Equals(baseCurr, saleCur)) {
                    _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                }
            }
        }
        if (ConfigurationManager.AppSettings["Test"] == "1") {
            //terminalCode = "12";
            //merchantCode = "115";
            //redirectAddress = "176.41.2.15:7000/Payments/Safiran/return.aspx";
        }

        amount = _amount.HasValue ? Convert.ToInt64(_amount.Value) : 0;
        CspParameters _cpsParameter;
        RSACryptoServiceProvider rsa;
        _cpsParameter = new CspParameters();
        _cpsParameter.Flags = CspProviderFlags.UseMachineKeyStore;
        rsa = new RSACryptoServiceProvider(1024, _cpsParameter);
        rsa.FromXmlString(privateKey);
        string data = "#" + merchantCode + "#" + terminalCode + "#" + ResNo + "#" + invoiceDate + "#" + amount + "#" + timeStamp + "#";
        byte[] signMain = rsa.SignData(Encoding.UTF8.GetBytes(data), new SHA1CryptoServiceProvider());
        sign = Convert.ToBase64String(signMain);
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://pep.shaparak.ir/VerifyPayment.aspx");
        string text = "InvoiceNumber=" + ResNo + "&InvoiceDate=" + invoiceDate + "&MerchantCode=" + merchantCode + "&TerminalCode=" + terminalCode + "&Amount=" + amount + "&TimeStamp=" + timeStamp + "&Sign=" + sign;
        byte[] textArray = Encoding.UTF8.GetBytes(text);
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = textArray.Length;
        request.GetRequestStream().Write(textArray, 0, textArray.Length);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string strResult = reader.ReadToEnd();
        if (strResult.Length > 0) {
            bool Result = Convert.ToBoolean(getFieldXmlStr(strResult.ToString(), "result", "false").ToLower());
            string ResultMsg = Convert.ToString(getFieldXmlStr(strResult.ToString(), "resultMessage",""));
            return Result;
        } else {
            return false;
        }
    }

    private string getFieldXmlStr(string xml, string field, string defaultvalue)
    {
        int first = xml.IndexOf(string.Format("<{0}>", field));
        int end = xml.IndexOf(string.Format("</{0}>", field));
        if (first == -1 || end == -1)
            return defaultvalue;
        return xml.Substring(first, end - first).Replace("<" + field + ">", "");
    }

}

public class safBankReturnRecord
{
    public safBankReturnRecord()
    {
    }

    bool? _result;
    public bool? Result
    {
        get { return _result; }
        set { _result = value; }
    }

    string _action;
    public string Action
    {
        get { return _action; }
        set { _action = value; }
    }

    string _transactionReferenceID;
    public string TransactionReferenceID
    {
        get { return _transactionReferenceID; }
        set { _transactionReferenceID = value; }
    }

    string _invoiceNumber;
    public string InvoiceNumber
    {
        get { return _invoiceNumber; }
        set { _invoiceNumber = value; }
    }

    string _invoiceDate;
    public string InvoiceDate
    {
        get { return _invoiceDate; }
        set { _invoiceDate = value; }
    }

    string _merchantCode;
    public string MerchantCode
    {
        get { return _merchantCode; }
        set { _merchantCode = value; }
    }

    string _terminalCode;
    public string TerminalCode
    {
        get { return _terminalCode; }
        set { _terminalCode = value; }
    }

    string _traceNumber;
    public string TraceNumber
    {
        get { return _traceNumber; }
        set { _traceNumber = value; }
    }

    string _referenceNumber;
    public string ReferenceNumber
    {
        get { return _referenceNumber; }
        set { _referenceNumber = value; }
    }

    string _transactionDate;
    public string TransactionDate
    {
        get { return _transactionDate; }
        set { _transactionDate = value; }
    }

}
