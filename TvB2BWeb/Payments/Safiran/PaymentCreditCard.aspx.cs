﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Threading;
using BankIntegration;
using TvTools;
using System.Web.Script.Services;
using System.Globalization;

public partial class Safiran_PaymentCreditCard : System.Web.UI.Page
{
    protected decimal amount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(string ResNo, string external)
    {
        string errorMsg = string.Empty;
        BIUser UserData = new BIUser();        
        decimal? _amount = new BILib().getResAmount(ResNo, 0, ref errorMsg);
        decimal? _payAmount = _amount;
        BIResMainRecord resMain = new BIResMainRecord();
        if (_amount.HasValue)
        {
            UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
            CultureInfo ci = BILib.getCultureInfo();
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);
            if (resMain != null)
            {
                string baseCurr = "YTL";
                string saleCur = resMain.SaleCur;
                if (!string.Equals(baseCurr, saleCur))
                {
                    _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                }
            }
        }

        List<BIHtmlCodeData> htmlData = new List<BIHtmlCodeData>();
        htmlData.Add(new BIHtmlCodeData { IdName = "txtResNo", TagName = "span", Data = resMain.ResNo });
        htmlData.Add(new BIHtmlCodeData { IdName = "txtAmount", TagName = "span", Data = _amount.HasValue ? _amount.Value.ToString("#,###.00") + " YTL " + (_payAmount.HasValue ? "(" + _payAmount.Value.ToString("#,###.00") + " " + resMain.SaleCur + ")" : "") : "" });
        htmlData.Add(new BIHtmlCodeData { IdName = "amountVal", TagName = "", Data = _amount.HasValue ? (_amount.Value * 100).ToString("#.") : "" });

        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static returnObj getGetPayment(string ResNo, string Amount, string CardName, string CardNumber1, string CardNumber2, string CardNumber3, string CardNumber4, string CVC, string ExpDateM, string ExpDateY, string BankCode, string external)
    {
        string errorMsg = string.Empty;
        BIUser UserData = new BIUser();        
        UserData = new BILib().getReservationCreateUserData(ResNo, ref errorMsg);
        CultureInfo ci = BILib.getCultureInfo();
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        BIResMainRecord resMain = new BILib().getResMain(UserData, ResNo, ref errorMsg);

        bool okey = false;
        returnObj retVal = new returnObj();
        List<BIBankVPosRecord> bankVPosList = new BILib().getBankVPos(string.Empty, ref errorMsg);
        List<BIPayTypeRecord> payTypes = new BILib().getPayType(resMain.PLMarket, ref errorMsg);

        decimal? _amount = Conversion.getDecimalOrNull(Amount.Replace(",", ci.NumberFormat.NumberDecimalSeparator).Replace(".", ci.NumberFormat.NumberDecimalSeparator));

        string posReferans = "";
        string resNo = ResNo + "_" + DateTime.Now.ToString("ddMMhhmm");
        string cardNumber = string.Empty;

        cardNumber = CardNumber1 + CardNumber2 + CardNumber3 + CardNumber4;

        string cardNumberxxx = "";
        if (cardNumber != "")
        {
            try
            {
                cardNumberxxx = CardNumber1 + CardNumber2.Substring(0, 2) + "XXXXXXXX" + CardNumber4.Substring(1, 3);
            }
            catch { }
        }
        List<BIResCustRecord> resCust = new BILib().getResCustList(UserData.Market, ResNo, ref errorMsg);
        List<BIResCustInfoRecord> resCustInfo = new BILib().getResCustInfoList(UserData.Market, ResNo, ref errorMsg);
        string LeaderName = string.Empty;
        string LeaderPhone = string.Empty;
        int? CustNumber = null;
        try
        {
            var Ln = from RC in resCust
                     join RCI in resCustInfo on RC.CustNo equals RCI.CustNo
                     where string.Equals(RC.Leader, "Y")
                     select new
                     {
                         LeaderCustNo = RC.CustNo,
                         LeaderName = (!string.IsNullOrEmpty(RCI.CName) || !string.IsNullOrEmpty(RCI.CSurName)) ? RCI.CSurName + " " + RCI.CName : RC.Surname + " " + RC.Surname,
                         LeaderPhone = (RCI.ContactAddr == "H" ? RCI.AddrHomeTel : RCI.AddrWorkTel)
                     };

            if (Ln.Count() > 0)
            {
                CustNumber = Ln.FirstOrDefault().LeaderCustNo;
                LeaderName = Ln.FirstOrDefault().LeaderName;
                LeaderPhone = Ln.FirstOrDefault().LeaderPhone;
            }
            else
                LeaderName = resCust.Where(f => f.Leader == "Y").FirstOrDefault().Name + " " + resCust.Where(f => f.Leader == "Y").FirstOrDefault().Surname;
        }
        catch { }

        string retValue = string.Empty;

        BIPayTypeRecord payType = payTypes.Where(w => w.Code == BankCode).FirstOrDefault();
        if (payType == null) return new returnObj { Paid=0, Message="Pay type not found.", External="" }; 
        BIBankVPosRecord vPos = bankVPosList.Find(f => f.Bank == payType.Bank);
        if (vPos == null) return new returnObj { Paid = 0, Message = "VPos not found.", External = "" };
        if (string.Equals(vPos.Interface, "YKB"))
        {
            string hostUrl = vPos.Host;
            string clientID = vPos.ClientID;//"400247195";
            string userID = vPos.UserID;//"vezirsatis";
            string pass = vPos.Password;//"V3z1rs@t1s";
            string bankcur = vPos.BankCur; //840 Euro
            retValue = VPosUtils.ConnectToYapiKrediPos(hostUrl, clientID, userID, cardNumber, ExpDateM, ExpDateY, CVC, resNo,
                (_amount.Value / 100).ToString(), bankcur, ref errorMsg, ref posReferans, "0", LeaderName, LeaderPhone);
            if (retValue == "1")
            {
                //   ViewState["OdemeOk"] = "true";            
            }
            else
            {
                return new returnObj { Paid = 0, Message = errorMsg, External = "" };                    
            }
        }
        else
        {
            #region ETS ye bağlan
            if (vPos == null) return new returnObj { Paid = 0, Message = "Bank info not found.", External = "" };                
            string hostUrl = vPos.Host;
            string clientID = vPos.ClientID;//"400247195";
            string userID = vPos.UserID;//"vezirsatis";
            string pass = vPos.Password;//"V3z1rs@t1s";
            string bankcur = vPos.BankCur; //840 Euro

            retValue = VPosUtils.ConnectToVPos(hostUrl, clientID, userID, pass, bankcur, cardNumber, ExpDateM, ExpDateY, CVC, resNo,
                                        (_amount.Value / 100).ToString(), ref errorMsg, ref posReferans, "0", LeaderName, LeaderPhone, true);

            if (retValue == "1")
            {
                //   ViewState["OdemeOk"] = "true";            
            }
            else
            {
                return new returnObj { Paid = 0, Message = errorMsg, External = "" };                    
            }
            #endregion
        }

        BIResCustRecord leader = resCust.Where(f => f.Leader == "Y").FirstOrDefault();

        int PayTypeID = 19;
        switch (resMain.Market)
        {
            case "IRMARKET": PayTypeID = 19; break;
            case "LOCAL": PayTypeID = 20; break;
            case "MIDDLEEAST": PayTypeID = 23; break;
            default: PayTypeID = 19; break;
        }
        if (payType != null && payType.RecID.HasValue)
            PayTypeID = payType.RecID.Value;

        okey = new BILib().MakeResPayment(resMain.Agency, 0, resMain.Agency, LeaderName, DateTime.Today, PayTypeID,
                                                     _amount.Value / 100, "YTL", cardNumberxxx, null, BankCode, CardName,
                                                     null, null, -1, 0, posReferans, ResNo, "", ref errorMsg);
        if (okey)
        {
            bool returnAtlas = false;
            bool returnBiletBank = false;
            bool returnHotelsPro = false;
            bool returnAxxaSigorta = false;
            bool returnAxxaVisa = false;
            returnAtlas = string.Equals(external, "1");
            returnBiletBank = string.Equals(external, "2");
            returnHotelsPro = string.Equals(external, "3");
            returnAxxaSigorta = string.Equals(external, "5");
            returnAxxaVisa = string.Equals(external, "4");
            string urlString = string.Empty;
            try
            {
                if (returnAtlas)
                {
                    string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "ATLAS"]);
                    urlString = extLink.ToLower().Replace("default.aspx", "") + "completeatlas.aspx?ResNo=" + ResNo;
                }
                else
                    if (returnBiletBank)
                    {
                        string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "BILETBANK"]);
                        urlString = extLink.ToLower().Replace("default.aspx", "") + "completeextticket.aspx?ResNo=" + ResNo;
                    }
                    else
                        if (returnHotelsPro)
                        {
                            string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "WORLDHOTELS"]);
                            urlString = extLink.ToLower().Replace("default.aspx", "") + "bookingComplete.aspx?ResNo=" + ResNo;
                        }
                        else
                            if (returnAxxaSigorta)
                            {
                                string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "CREDITCARD"]);
                                urlString = extLink.ToLower().Replace("default.aspx", "") + "policefinal.aspx?resNo=" + ResNo;
                            }
                            else
                                if (returnAxxaVisa)
                                {
                                    string extLink = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalPage_" + "AXXA"]);
                                    urlString = extLink.ToLower().Replace("default.aspx", "") + "visafinal.aspx?resNo=" + ResNo + "&user=" + (UserData.AgencyID + "/" + UserData.UserID);
                                }
            }
            catch { }
            new BILib().SetResOptDate(ResNo, null, -1, ref errorMsg);
            if (!(returnAtlas || returnBiletBank || returnHotelsPro))
                new BILib().setReservationComplate(UserData, ResNo, false, ref errorMsg);

            return new returnObj { Paid = 1, Message = "Payment received. Reservation number " + ResNo + " Referans No " + posReferans, External = urlString };
        }
        else
        {
            return new returnObj { Paid = 0, Message = "Payment received. Payment could not be saved to the system. Please contact your operator. Reservation number " + ResNo + " Referans No " + posReferans, External = "" };                
        }
    }
}

public class returnObj
{
    public returnObj()
    {
        _Paid = 0;
        _Message = string.Empty;
        _external = string.Empty;
    }

    Int16? _Paid;
    public Int16? Paid
    {
        get { return _Paid; }
        set { _Paid = value; }
    }

    string _Message;
    public string Message
    {
        get { return _Message; }
        set { _Message = value; }
    }

    string _external;
    public string External
    {
        get { return _external; }
        set { _external = value; }
    }
}