using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Xml;
using System.Globalization;
using System.Threading;
using BankIntegration;
using System.Configuration;

public partial class SafiranPayment : System.Web.UI.Page
{
    protected String privateKey = "<RSAKeyValue><Modulus>xyxyff0bqWuSOnHk2D33lqCVBBFxdkxKhhMgN4ScEcF+pE4ETulgATMfcNB9DSGlUBEg3D0M99Rdr7THUbeqF/x/12rnKen7YGlc5KJ0FeOCXxR7qa0nIhxb5Jr763LQElPLRdflwcZqDfyYj/6J0p8rOYGCwjz+F/mTVsAzjz8=</Modulus><Exponent>AQAB</Exponent><P>5bqxBL7kiTAZqzCzO6j5KoCFmRXgIDdyN7uOdEDj9xxDHuyBOs3PKimxsEv4+MUuJYeqyWJ6V/gt8FmAs4wnKw==</P><Q>3fM9zpOKnsJUmcA2Xbj3OBpS1Hub5r4rboAoeERh//ImSwrq/Oq1MmPP7frVxvxkZ9rcaSeF0Ac0ytEyKA2uPQ==</Q><DP>iaBkuHEcPMviNNrCJbW/QCUq5GFkmihdduIYH7FYYQw/ceFCMlZmC95ao0GAcJjAYp96Q5eJr/Xwn5MfNXF7bQ==</DP><DQ>l4Ey75BHT5fXqBxNAC0ClIljgSffg+LMQuG4vl/vhYcGHLOXNI4CoiMADLLsuqgO4dXEHwOblCVXJBso4a51AQ==</DQ><InverseQ>Yi8JlJs8DirtAHIn6pfdYUXvkDEuiDMCQe5OGHQ6hO0LxGxV9MyMKBygz0ovPPUaUKrhQyXafg+nAoXQsSVoDQ==</InverseQ><D>Yi2drk8CwKue5CJaTOGW1vAHJH84r7iBj7+DxPogOHxp4bH3W7KcOVsq52BOFBf+tg4LZaTObKeJGuCA0942AEg36HA3xyt4X2vmslLRWB1Hg09p5eCjTgkwaXQzVfVAC/RfEkpronc5WRPxNtGswjBB/usAoiLZPxwpVoiL4lk=</D></RSAKeyValue>";
    protected String publicKey = "<RSAKeyValue><Modulus>xyxyff0bqWuSOnHk2D33lqCVBBFxdkxKhhMgN4ScEcF+pE4ETulgATMfcNB9DSGlUBEg3D0M99Rdr7THUbeqF/x/12rnKen7YGlc5KJ0FeOCXxR7qa0nIhxb5Jr763LQElPLRdflwcZqDfyYj/6J0p8rOYGCwjz+F/mTVsAzjz8=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
    protected string terminalCode = "226156";
    protected string merchantCode = "225146";
    protected string redirectAddress = "http://agency.safirantravel.com/Payments/Safiran/return.aspx"; //?invoiceNumber=" + invoiceNumber&result=;
    protected Int64 amount;
    protected string timeStamp;
    protected string invoiceDate;
    protected string invoiceNumber;
    protected string action = "1003";
    protected string sign;

    protected void Page_Load(object sender, EventArgs e)
    {
        string returnUrl = string.Empty;
        returnUrl = TvTools.Conversion.getStrOrNull(System.Web.Configuration.WebConfigurationManager.AppSettings["PaymentReturnUrl"]);
        string errorMsg = string.Empty;
        redirectAddress = returnUrl;
        timeStamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        if (string.IsNullOrEmpty(Request.Params["Atlas"]))
            invoiceNumber = "8" + (string)Request.Params["ResNo"];
        else
        {
            if ((string)Request.Params["Atlas"] == "1")
                invoiceNumber = "9" + (string)Request.Params["ResNo"];
            else
                if ((string)Request.Params["Atlas"] == "2")
                    invoiceNumber = "7" + (string)Request.Params["ResNo"];
                else
                    if ((string)Request.Params["Atlas"] == "3")
                        invoiceNumber = "6" + (string)Request.Params["ResNo"];
                    else
                        if ((string)Request.Params["Atlas"] == "4")
                            invoiceNumber = "3" + (string)Request.Params["ResNo"];
                        else
                            if ((string)Request.Params["Atlas"] == "5")
                                invoiceNumber = "5" + (string)Request.Params["ResNo"];
                            else
                                if ((string)Request.Params["Atlas"] == "6")
                                    invoiceNumber = "4" + (string)Request.Params["ResNo"];
        }
        invoiceDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        decimal? _amount = new BILib().getResAmount((string)Request.Params["ResNo"], 0, ref errorMsg);
        if (_amount.HasValue)
        {
            BIUser UserData = new BILib().getReservationCreateUserData((string)Request.Params["ResNo"], ref errorMsg);
            BIResMainRecord resMain = new BILib().getResMain(UserData, (string)Request.Params["ResNo"], ref errorMsg);
            if (resMain != null)
            {
                string baseCurr = "IRR";
                string saleCur = resMain.SaleCur;
                if (!string.Equals(baseCurr, saleCur))
                {
                    _amount = new BILib().Exchange(resMain.PLMarket, DateTime.Today, saleCur, baseCurr, _amount.Value, false, ref errorMsg);
                }
            }
        }
        if (ConfigurationManager.AppSettings["Test"] == "1") {
            terminalCode = "12";
            merchantCode = "115";
            redirectAddress = "176.41.2.15:7000/Payments/Safiran/return.aspx";
        }

        amount = _amount.HasValue ? Convert.ToInt64(_amount.Value) : 0;
        CspParameters _cpsParameter;
        RSACryptoServiceProvider rsa;
        _cpsParameter = new CspParameters();
        _cpsParameter.Flags = CspProviderFlags.UseMachineKeyStore;
        rsa = new RSACryptoServiceProvider(1024, _cpsParameter);
        rsa.FromXmlString(privateKey);
        string data = "#" + merchantCode + "#" + terminalCode + "#" + invoiceNumber + "#" + invoiceDate + "#" + amount + "#" + redirectAddress + "#" + action + "#" + timeStamp + "#";
        byte[] signMain = rsa.SignData(Encoding.UTF8.GetBytes(data), new SHA1CryptoServiceProvider());
        sign = Convert.ToBase64String(signMain);
    }

}
