﻿1<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebPaymentFinNordea.ascx.cs"
    Inherits="themes_Detur_UserControls_WebPaymentFinNordea" %>
</form>
<form id="PostForm" name="PostForm" action="https://solo3.nordea.fi/cgi-bin/SOLOPM01"
method="post">
<input type="hidden" name="SOLOPMT_VERSION" value="0003" />
<input type="hidden" name="SOLOPMT_STAMP" value="<%=ReservationNumber%>" />
<input type="hidden" name="SOLOPMT_RCV_ID" value="<%=merchant_Number%>" />
<input type="hidden" name="SOLOPMT_LANGUAGE" value="<%=language.ToString()%>"/>
<input type="hidden" name="SOLOPMT_AMOUNT" value="<%=Amount.ToString("#.00").Replace(".",",")%>" />
<input type="hidden" name="SOLOPMT_REF" value="<%=ref_Number%>" />
<input type="hidden" name="SOLOPMT_DATE" value="EXPRESS" />
<input type="hidden" name="SOLOPMT_RETURN" value="<%=Success_Url%>" />
<input type="hidden" name="SOLOPMT_CANCEL" value="<%=Cancel_Url %>" />
<input type="hidden" name="SOLOPMT_REJECT" value="<%=Failure_Url %>" />
<input type="hidden" name="SOLOPMT_KEYVERS" value="0001" />
<input type="hidden" name="SOLOPMT_CUR" value="EUR" />
<input type="hidden" name="SOLOPMT_PMTTYPE" value="" />
<input type="hidden" name="SOLOPMT_MAC" value="<%=authentication_Mac%>" />
</form>

<script language="javascript" type="text/javascript">
    var vPostForm = document.PostForm;
    vPostForm.submit();
</script>

