﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using TvBo;

public partial class WebPaymentNorNets : System.Web.UI.UserControl
{
    #region Parameters

    string MerchantId
    {
        //get { return "398194"; }
        get { return "696453"; }
        set { }
    }

    string Token
    {
        //get { return "Jn2_q?M3"; }
        get { return "n-4B6K)"; }
        set { }
    }

    string RedirectUrl
    {
        get { return WebRoot.BasePageRoot + "Payments/NetsReturn.aspx?PayType={0}&Reserve={1}&resNumber={2}&Amount={3}"; }
        set { }
    }

    string TerminalUrl
    {
        get { return "https://epayment.bbs.no/epay/default.aspx?merchantId={0}&transactionId={1}"; }
        set { }
    }

    string EndPoint { get { return "https://epayment.bbs.no/Netaxept.svc"; } set { } }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(WebRoot.BasePageRoot)) Response.Redirect("~/Default.aspx");
        string isTest = ConfigurationManager.AppSettings["Test"];
        if (!string.IsNullOrEmpty(isTest) && isTest.Equals("1"))
        {
            MerchantId = "12003195";
            Token = "bP-3S)8";
            EndPoint = "https://test.epayment.nets.no/Netaxept.svc";
            TerminalUrl = "https://test.epayment.nets.no/epay/default.aspx?merchantId={0}&transactionId={1}";
        }
        string resNo = this.Attributes["ReservationNumber"];
        string payType = this.Attributes["PayType"];
        string reserve = this.Attributes["Reserve"];
        string amount = "0";
        decimal amountVal = 0;
        if (String.IsNullOrEmpty(this.Attributes["Amount"]) == false)
        {
            amountVal = Decimal.Parse(this.Attributes["Amount"]) * 100;
            amount = amountVal.ToString("#.");
        }
        string cardType = "AmericanExpress";
        List<NetsSVC_Last.PaymentMethodAction> paymentActionList = new List<NetsSVC_Last.PaymentMethodAction>();
        if (this.Attributes["CardType"] != null)
        {
            cardType = this.Attributes["CardType"];
            foreach (var pt in cardType.Split(','))
                paymentActionList.Add(new NetsSVC_Last.PaymentMethodAction { PaymentMethod=pt});
        }
           // cardType = this.Attributes["CardType"].ToString();

        

        NetsSVC_Last.NetaxeptClient client = new NetsSVC_Last.NetaxeptClient("Endpoint", EndPoint);
        NetsSVC_Last.RegisterResponse response = client.Register(MerchantId, Token, new NetsSVC_Last.RegisterRequest
        {
            ServiceType = "B",
            Order = new NetsSVC_Last.Order
            {
                OrderNumber = resNo,
                CurrencyCode = "NOK",
                Amount = amount
            },
            Environment = new NetsSVC_Last.Environment
            {
                WebServicePlatform = "WCF"
            },
            Terminal = new NetsSVC_Last.Terminal
            {
                //PaymentMethodActionList=paymentActionList.ToArray(),
                //PaymentData=cardType,
                //PaymentMethodActionList= paymentActionList.ToArray(),
                PaymentMethodList = cardType,
                OrderDescription = "",
                Language = "no_NO",
                RedirectUrl = String.Format(RedirectUrl, payType, reserve, resNo, (amountVal / 100).ToString("#.00"))
            }
        }) ;
        Response.BufferOutput = true;
        //NetsSVC.Netaxept client = new NetsSVC.Netaxept();
        //NetsSVC.RegisterResponse response = client.Register(MerchantId, Token,
        //    new NetsSVC.RegisterRequest
        //{
        //    ServiceType = "B",
        //    Order = new NetsSVC.Order
        //    {
        //        OrderNumber = resNo,
        //        CurrencyCode = "NOK",
        //        Amount = amount,
        //    },
        //    Environment = new NetsSVC.Environment
        //    {
        //        WebServicePlatform = "WCF"
        //    },
        //    Terminal = new NetsSVC.Terminal
        //    {
        //        PaymentMethodList = cardType,
        //        OrderDescription = "",
        //        Language = "no_NO",
        //        RedirectUrl = String.Format(RedirectUrl, payType, reserve, resNo, (amountVal / 100).ToString("#.00"))
        //    }
        //});

        Response.Redirect(String.Format(TerminalUrl, MerchantId, response.TransactionId));
    }
}