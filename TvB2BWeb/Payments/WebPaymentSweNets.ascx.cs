﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using TvBo;
using System.Net;

public partial class WebPaymentSweNets : System.Web.UI.UserControl
{
    #region Parameters
    string _MerchantId
    {
        get { return "767095"; }

    }
    string MerchantId { get; set; }
    string _Token
    {
        get { return "f?5L_Mx"; }
    }
    string Token { get; set; }
    string _EndPoint
    {
        get { return "https://epayment.bbs.no/Netaxept.svc"; }
    }
    string EndPoint { get; set; }
    string _TerminalUrl
    {
        get { return "https://epayment.nets.eu/terminal/default.aspx?merchantId={0}&transactionId={1}"; }
        
    }
    public string TerminalUrl { get; set; }

    string RedirectUrl
    {
        get { return WebRoot.BasePageRoot + "Payments/NetsSweReturn.aspx?PayType={0}&Reserve={1}&resNumber={2}&Amount={3}"; }
    }

   

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(WebRoot.BasePageRoot)) Response.Redirect("~/Default.aspx");
        MerchantId = _MerchantId;
        Token = _Token;
        EndPoint = _EndPoint;
        TerminalUrl = _TerminalUrl;
        string isPayLog = ConfigurationManager.AppSettings["PaymentLogIsActive"];
        bool paymentLog = false;
        if (!string.IsNullOrEmpty(isPayLog) && isPayLog.Equals("1"))
            paymentLog = true;
        string isTest = ConfigurationManager.AppSettings["Test"];
        if (!string.IsNullOrEmpty(isTest) && isTest.Equals("1"))
        {
            MerchantId = "12003195";
            Token = "bP-3S)8";
            EndPoint = "https://test.epayment.nets.eu/Netaxept.svc";
            TerminalUrl = "https://test.epayment.nets.eu/epay/default.aspx?merchantId={0}&transactionId={1}";
        }
        System.Net.ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
        string resNo = this.Attributes["ReservationNumber"];
        string payType = this.Attributes["PayType"];
        string reserve = this.Attributes["Reserve"];
        string amount = "0";
        decimal amountVal = 0;
        if (String.IsNullOrEmpty(this.Attributes["Amount"]) == false)
        {
            amountVal = Decimal.Parse(this.Attributes["Amount"]) * 100;
            amount = amountVal.ToString("#.");
        }
        string cardType = "AmericanExpress,MasterCard,Visa";
        //if (this.Attributes["CardType"] != null)
        //    cardType = this.Attributes["CardType"];

        NetsSVC_Last.NetaxeptClient client = new NetsSVC_Last.NetaxeptClient("Endpoint",EndPoint );
        var request = new NetsSVC_Last.RegisterRequest
        {
            ServiceType = "B",
            Order = new NetsSVC_Last.Order
            {
                OrderNumber = resNo,
                CurrencyCode = "SEK",
                Amount = amount
            },
            Environment = new NetsSVC_Last.Environment
            {
                WebServicePlatform = "WCF"
            },
            Terminal = new NetsSVC_Last.Terminal
            {
                PaymentMethodList = cardType,
                OrderDescription = "",
                Language = "sv_SE",
                RedirectUrl = String.Format(RedirectUrl, payType, reserve, resNo, (amountVal / 100).ToString("#.00"))
            }
        };
        NetsSVC_Last.RegisterResponse response = client.Register(MerchantId, Token,request );
        if(paymentLog)
        WebLog.SaveWebServiceLog(null, null, "NetsSwe-Register-ResNo:" + resNo, Newtonsoft.Json.JsonConvert.SerializeObject(request), Newtonsoft.Json.JsonConvert.SerializeObject(response), null);

        //Response.BufferOutput = true;
        Response.Redirect(String.Format(TerminalUrl, MerchantId, response.TransactionId));
    }
}