﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;

public partial class WebPaymentDenAuriga : System.Web.UI.UserControl
{
    protected string merchantId = "4934";
    protected int Version = 3;
    protected string customerRefNo;
    protected string currency = "DKK";
    protected string amount;
    protected string vat;
    protected string paymentMethod = "KORTINDK";
    protected string purchaseDate;
    protected string responseUrl;
    protected string goodsDesc;
    protected string lang = "DAN";
    protected string comment;
    protected string country = "DK";
    protected string cancelUrl;
    protected string MAC;
    private string secretWord = "nlgj65c26of73jr1es1x4j9utgp04l86";

    protected void Page_Load(object sender, EventArgs e)
    {
        string paymentType = "";
        string Reserve = "";
        string resNumber = "";
        if (String.IsNullOrEmpty(this.Attributes["ReservationNumber"]) == false)
        {
            customerRefNo = this.Attributes["ReservationNumber"];
            resNumber = customerRefNo;
            customerRefNo = resNumber.Substring(0, 8) + "_" + DateTime.Now.ToString("dd.MM-HH:mm");
        }
        decimal myAmount = 0;
        if (String.IsNullOrEmpty(this.Attributes["Amount"]) == false)
        {
            myAmount = Decimal.Parse(this.Attributes["Amount"]);
            decimal amountVal = Decimal.Parse(this.Attributes["Amount"]) * 100;
            amount = amountVal.ToString("#");
        }

        if (this.Attributes["PayType"] != null)
        {
            paymentType = this.Attributes["PayType"].ToString();
        }

        if (this.Attributes["Reserve"] != null)
        {
            Reserve = this.Attributes["Reserve"].ToString();
        }

        string basePage = TvBo.WebRoot.BasePageRoot;

        vat = "0";
        purchaseDate = ConvertDateTimeToString(DateTime.Now);
        string returnUrl = string.Format("{0}Payments/PaymentResultFi.aspx?auriga=true&Reserve={2}&PayType={1}&ResNumber={3}&Amount={4}",
                                            basePage,
                                            paymentType,
                                            Reserve,
                                            resNumber,
                                            myAmount.ToString("#.00"));

        responseUrl = returnUrl;

        goodsDesc = resNumber;
        comment = "";
        string cancelUrl = string.Format("{0}Payments/noPaymentResultFi.aspx?canceled=true&resNumber={1}",
                                            basePage,
                                            resNumber);

        string macInput = String.Concat(merchantId, Version, customerRefNo, currency, amount, vat, paymentMethod, purchaseDate,
            responseUrl, goodsDesc, lang, comment, country, cancelUrl);

        MAC = md5(macInput + secretWord);
    }

    private string md5(string str)
    {
        MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
        Encoding encoder = Encoding.GetEncoding("ISO-8859-1");
        byte[] hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(str));

        return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
    }

    private string ConvertDateTimeToString(DateTime date)
    {
        string result = date.ToString("yyyyMMddhhmm");
        return result;
    }
}