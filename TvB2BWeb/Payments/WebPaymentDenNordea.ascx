﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebPaymentDenNordea.ascx.cs" Inherits="themes_Detur_UserControls_WebPaymentDenNordea" %>
</form>
<form  id="PostForm" name="PostForm"  action="https://solo3.nordea.fi/cgi-bin/SOLOPM01" method="post">
    <input type="hidden" name="SOLOPMT_VERSION" value="<%=version%>"/> 
    <input type="hidden" name="SOLOPMT_STAMP" value="<%=ReservationNumber%>"/> 
    <input type="hidden" name="SOLOPMT_RCV_ID" value="<%=merchant_Number%>"/>  
    <input type="hidden" name="SOLOPMT_LANGUAGE" value="<%=language.ToString()%>"/>
    <input type="hidden" name="SOLOPMT_AMOUNT" value="<%=Amount.ToString("#.00").Replace(".",",")%>"/>
    <input type="hidden" name="SOLOPMT_REF" value="<%=ref_Number%>"/>
    <input type="hidden" name="SOLOPMT_DATE" value="<%= date %>"/>
    <input type="hidden" name="SOLOPMT_RETURN" value="<%=returnUrl %>" />
    <input type="hidden" name="SOLOPMT_CANCEL" value="<%=cancelUrl %>"/> 
    <input type="hidden" name="SOLOPMT_REJECT" value="<%=rejectUrl %>"/> 
    <input type="hidden" name="SOLOPMT_MAC" value="<%=authentication_Mac%>"/> 
    <input type="hidden" name="SOLOPMT_CONFIRM" value="<%= confirm %>"/>
	<input type="hidden" name="SOLOPMT_KEYVERS" value="<%= keyVers %>"/>
    <input type="hidden" name="SOLOPMT_CUR" value="<%= cur %>"/> 
</form>

<script language="javascript" type="text/javascript">
var vPostForm = document.PostForm;
vPostForm.submit();
</script>