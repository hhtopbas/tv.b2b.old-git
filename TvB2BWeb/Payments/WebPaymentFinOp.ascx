﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebPaymentFinOp.ascx.cs"
    Inherits="themes_Detur_UserControls_WebPaymentFinOp" %>
</form>
<form id="PostForm" name="PostForm" action="https://kultaraha.op.fi/cgi-bin/krcgi"
method="post">
<input type="hidden" name="action_id" value="701" />
<input type="hidden" name="VERSIO" value="1" />
<input type="hidden" name="MAKSUTUNNUS" value="<%=ReservationNumber%>" />
<input type="hidden" name="MYYJA" value="<%=merchant_Number%>" />
<input type="hidden" name="SUMMA" value="<%=Amount.ToString("#.00").Replace(".",",")%>" />
<input type="hidden" name="VIITE" value="<%=ref_Number%>" />
<input type="hidden" name="TARKISTE-VERSIO" value="1" />
<input type="hidden" name="VAHVISTUS" value="K" />
<input type="hidden" name="TARKISTE" value="<%=authentication_Mac%>" />
<input type="hidden" name="PALUU-LINKKI" value="<%=Success_Url%>" />
<input type="hidden" name="PERUUTUS-LINKKI" value="<%=Cancel_Url%>" />
<input type="hidden" name="VALUUTTALAJI" value="EUR" />
</form>

<script language="javascript" type="text/javascript">
    var vPostForm = document.PostForm;
    vPostForm.submit();
</script>

