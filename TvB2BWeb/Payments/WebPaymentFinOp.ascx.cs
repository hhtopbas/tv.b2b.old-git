﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public partial class themes_Detur_UserControls_WebPaymentFinOp : System.Web.UI.UserControl
{
    private string reservationNumber;
    public string ReservationNumber
    {
        get { return reservationNumber; }
        set { reservationNumber = value; }
    }

    private decimal amount;
    public decimal Amount
    {
        get { return amount; }
        set { amount = value; }
    }

    protected string paymentType;
    protected string Reserve;

    protected string merchant_Number = "DETURFINLANDOY";
    //protected string merchant_Number = "Esittelymyyja";

    protected string ref_Number = "";

    private string key = "KH2NPWJZTM9BUSGELV83NFURBKAYCJE7VKST2GNP";
    //private string key = "Esittelykauppiaansalainentunnus";
    protected string authentication_Mac;
    protected int language = 1; //1-finnish 2-swedish 3-english

    protected string Success_Url = "";    
    protected string Cancel_Url = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        //<input type="hidden" name="PALUU-LINKKI" value="http://www.detur.fi/themes/Detur/PaymentResult.aspx?PayType=<%=paymentType%>&Reserve=<%=Reserve%>&resNumber=<%=ReservationNumber%>&Amount=<%=Amount.ToString("#.00")%>"/>
        //<input type="hidden" name="PERUUTUS-LINKKI" value="http://www.detur.fi/themes/Detur/BookPayment.aspx?resNumber=<%=ReservationNumber%>"/>     
        string basePage = TvBo.WebRoot.BasePageRoot;

        Success_Url = string.Format("{2}Payments/PaymentResultFi.aspx?PayType=OP-E&resNumber={0}&Amount={1}&Reserve={3}",
                                reservationNumber,
                                amount.ToString("#.00"),
                                basePage,
                                Reserve);
        
        Cancel_Url = string.Format("{1}Payments/noPaymentResultFi.aspx?resNumber={0}",
                                reservationNumber,
                                basePage);

        if (this.Attributes["ReservationNumber"] != null)
        {
            reservationNumber = this.Attributes["ReservationNumber"].ToString();
            ref_Number = new TvBo.Payments().GetRefNoForNordea(reservationNumber).Replace(" ", "");
        }

        if (this.Attributes["PayType"] != null)
        {
            paymentType = this.Attributes["PayType"].ToString();
        }

        if (this.Attributes["Reserve"] != null)
        {
            Reserve = this.Attributes["Reserve"].ToString();
        }

        if (this.Attributes["Amount"] != null)
        {
            amount = decimal.Parse(this.Attributes["Amount"].ToString());
        }        

        string data = "1" + reservationNumber + merchant_Number + Amount.ToString("#.00").Replace(".", ",") + ref_Number + "EUR1" + key;
        authentication_Mac = MD5Encode(data.Replace(" ", ""), "").ToUpper();        
    }


    public string MD5Encode(string originalPassword, string key)
    {
        //Declarations
        Byte[] originalBytes;
        Byte[] encodedBytes;
        MD5 md5;

        //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
        md5 = new MD5CryptoServiceProvider();
        originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
        encodedBytes = md5.ComputeHash(originalBytes);

        //Convert encoded bytes back to a 'readable' string
        return BitConverter.ToString(encodedBytes).Replace("-", "");
    }


}
