﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;

public partial class Default_Page : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    private static List<AgencyPayList> getSelectedList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<AgencyPayList> list = new List<AgencyPayList>();
        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\APMSelectList." + HttpContext.Current.Session.SessionID;
        if (File.Exists(path)) {
            StreamReader reader = new StreamReader(path);
            try {
                string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgencyPayList>>(uncompressed);
            }
            catch (Exception) {
                throw;
            }
            finally {
                reader.Close();
            }
        }
        return list;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object getFormData(bool Auto, decimal? AutoValue)
    {
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        //List<AgencyPayList> selectedResList = getSelectedList();

        decimal Amount = 0;
        if (Auto)
            Amount = AutoValue.HasValue ? AutoValue.Value / 100 : (decimal)0;

        string configValue = Conversion.getStrOrNull(new Common().getFormConfigValue("Payment", "GatewayApi"));
        List<NovaGatewayApi> gatewayParamList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NovaGatewayApi>>(configValue);
        NovaGatewayApi gateWayApi = gatewayParamList.Find(f => f.Market == UserData.Market);
        string responseFromServer = string.Empty;
        if (gateWayApi != null) {
            try {
                string requestUrl = (string.IsNullOrEmpty(gateWayApi.BankLink) ? "http://payments.novatours.eu/api/paymentlist/" : gateWayApi.BankLink + "api/paymentlist/") + gateWayApi.ApiKey + '/' + UserData.AgencyID;
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                request.ContentType = "utf-8";
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse) {
                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception(String.Format(
                        "Server error (HTTP {0}: {1}).",
                        response.StatusCode,
                        response.StatusDescription));
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream, System.Text.Encoding.UTF8);
                    responseFromServer = reader.ReadToEnd();
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            NovaGatewayPaymentResult novaGatewayPayment = Newtonsoft.Json.JsonConvert.DeserializeObject<NovaGatewayPaymentResult>(responseFromServer);
            List<NovaGatewayPaymentDetail> payment = novaGatewayPayment.payments.payment;

            bool specCode = VersionControl.getTableField("PayType", "SpecCode");

            string bankMapsStr = Conversion.getStrOrNull(new Common().getFormConfigValue("Payment", "BankMaps"));
            List<NovaBankMaping> bankMappings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NovaBankMaping>>(bankMapsStr);
            List<PayTypeRecord> payTypeList = new Common().getPayType(UserData, UserData.Market, ref errorMsg);
            List<PayTypeRecord> payTypeListQ = new List<PayTypeRecord>();
            if (specCode)
                payTypeListQ = payTypeList.Where(w => w.AllowWIS.HasValue && w.AllowWIS.Value == true && /*w.PayCur == UserData.SaleCur &&*/ w.Market == UserData.Market).ToList<PayTypeRecord>();
            else
                payTypeListQ = payTypeList.Where(w => w.AllowWIS.HasValue && w.AllowWIS.Value == true && w.PayCur == UserData.SaleCur && w.Market == UserData.Market).ToList<PayTypeRecord>();
            List<NovaGatewayPaymentDetail> query = new List<NovaGatewayPaymentDetail>();
            if (specCode)
                query = (from q1 in payment
                         join q3 in payTypeListQ on q1.paymentId equals q3.SpecCode
                         select q1).ToList<NovaGatewayPaymentDetail>();
            else
                query = (from q1 in payment
                         join q2 in bankMappings.Where(w => w.Market == UserData.Market) on q1.paymentId equals q2.NovaCode
                         join q3 in payTypeListQ on q2.TvCode equals q3.Bank
                         select q1).ToList<NovaGatewayPaymentDetail>();
            novaGatewayPayment.payments.payment = query;

            List<AgencyPayList> resList = getSelectedList();
            string retVal = string.Empty;
            var query1 = from q in resList
                         group q by new { SaleCur = q.Currency } into key
                         select new { key.Key.SaleCur };
            decimal saleCurrencyTotal = (decimal)0;

            foreach (var row in query1) {
                decimal CurrTotal = (decimal)0;
                CurrTotal += resList.Where(w => w.Currency == row.SaleCur).Sum(s => (s.PayAmount.HasValue ? s.PayAmount.Value : (decimal)0));
                if (row.SaleCur != UserData.SaleCur) {
                    decimal? tempTotal = new Common().Exchange(UserData.Market, DateTime.Today, row.SaleCur, UserData.SaleCur, CurrTotal, true, ref errorMsg);
                    if (tempTotal.HasValue) {
                        saleCurrencyTotal += tempTotal.Value;
                    } else
                        if (!tempTotal.HasValue || !string.IsNullOrEmpty(errorMsg)) {
                        return new {
                            Ok = false,
                            GatewayApi = gateWayApi,
                            AgencyID = UserData.AgencyID,
                            NovaApiValue = novaGatewayPayment,
                            PaymentValueInfo = "Error: Exchange problem.",
                            SaleCurrTotal = saleCurrencyTotal,
                            SaleCurr = UserData.SaleCur
                        };
                    }
                } else {
                    saleCurrencyTotal += CurrTotal;
                }
                retVal += string.Format("<div style=\"width: 150px; text-align: right;\">{0}</div>", CurrTotal.ToString("#,###.00") + " " + row.SaleCur);
            }

            retVal = string.Format("<div><b>{0}</b></div>", "Payment details:") + retVal;

            if (Amount > 0 && saleCurrencyTotal == 0) {
                retVal = "";
                saleCurrencyTotal = Amount;
            }
            retVal += string.Format("<div><b>{0}</b></div>", "Will be paid sum:");
            string showTotal = saleCurrencyTotal.ToString("#.00");
            if (showTotal.IndexOf(",") == 0 || showTotal.IndexOf(".") == 0)
                showTotal = "0" + showTotal;
            retVal += string.Format("<div style=\"width: 150px; text-align: right;\">{0}</div>", showTotal + " " + UserData.SaleCur);

            string fileName = "ExportFile_" + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") + ".xls";
            if (ExportToExcel.CreateExcelFile.CreateExcelDocument<AgencyPayList>(resList, HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute("~/ACE/")) + fileName)) {                
            }


            return new {
                Ok = true,
                GatewayApi = gateWayApi,
                AgencyID = UserData.AgencyID,
                NovaApiValue = novaGatewayPayment,
                PaymentValueInfo = retVal,
                SaleCurrTotal = saleCurrencyTotal,
                SaleCurr = UserData.SaleCur,
                SelectedList = resList,
                ExportFilePath = Global.getBasePageRoot() + "ACE/" + fileName
            };
        } else {
            return null;
        }
    }

    public static List<AgencyPayList> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\AgencyPaymentMonitor." + HttpContext.Current.Session.SessionID;
        System.IO.StreamReader reader = new System.IO.StreamReader(path);
        List<AgencyPayList> list = new List<AgencyPayList>();
        try {
            string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AgencyPayList>>(uncompressed);
        }
        catch (Exception) {
            throw;
        }
        finally {
            reader.Close();
        }
        return list;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object getPaymentData(NovaGatewayPaymentResult paymentData, string PaymentId, decimal? Amount, string Curr)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string sign = string.Empty;
        string errorMsg = string.Empty;
        string gatewayApiJson = Conversion.getStrOrNull(new Common().getFormConfigValue("Payment", "GatewayApi"));

        bool specCode = VersionControl.getTableField("PayType", "SpecCode");

        List<NovaGatewayApi> gatewayApiList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NovaGatewayApi>>(gatewayApiJson);
        NovaGatewayApi gatewayApi = gatewayApiList.Find(f => f.Market == UserData.Market);
        NovaGatewayPaymentDetail selectedPayment = paymentData.payments.payment.Find(f => f.paymentId == PaymentId);

        string bankMapsStr = Conversion.getStrOrNull(new Common().getFormConfigValue("Payment", "BankMaps"));
        List<NovaBankMaping> bankMappings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NovaBankMaping>>(bankMapsStr);
        NovaBankMaping bankMaps = bankMappings.Find(f => f.Market == UserData.Market && f.NovaCode == selectedPayment.paymentId);

        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });
        if (Curr == "")
            Curr = UserData.SaleCur;

        string curreny = Curr;
        List<AgencyPayList> resList = getSelectedList();
        NovaBeforePament pymntDetail = new NovaBeforePament();
        pymntDetail.AgencyID = UserData.AgencyID;
        pymntDetail.CreateDate = DateTime.Now;
        pymntDetail.Curr = curreny;
        pymntDetail.PaymentId = Conversion.getStrOrNull(selectedPayment.paymentId);
        pymntDetail.PaymentResList = resList;

        string retVal = string.Empty;
        var query1 = from q in resList
                     group q by new { SaleCur = q.Currency } into key
                     select new { key.Key.SaleCur };
        decimal saleCurrencyTotal = (decimal)0;

        foreach (var row in query1) {
            decimal CurrTotal = (decimal)0;
            CurrTotal += resList.Where(w => w.Currency == row.SaleCur).Sum(s => (s.PayAmount.HasValue ? s.PayAmount.Value : (decimal)0));
            if (row.SaleCur != UserData.SaleCur) {
                decimal? tempTotal = new Common().Exchange(UserData.Market, DateTime.Today, row.SaleCur, UserData.SaleCur, CurrTotal, true, ref errorMsg);
                if (tempTotal.HasValue) {
                    saleCurrencyTotal += tempTotal.Value;
                }
            } else {
                saleCurrencyTotal += CurrTotal;
            }
        }

        if (!Amount.HasValue)
            Amount = saleCurrencyTotal;

        pymntDetail.TotalAmount = Conversion.getDecimalOrNull(Amount.HasValue ? Amount.Value : 0);

        List<PayTypeRecord> payTypeList = new Common().getPayType(UserData, UserData.Market, ref errorMsg);

        List<PayTypeRecord> payTypeListQ = new List<PayTypeRecord>();
        if (specCode)
            payTypeListQ = payTypeList.Where(w => w.AllowWIS.HasValue && w.AllowWIS.Value == true && /*w.PayCur == UserData.SaleCur &&*/ w.Market == UserData.Market).ToList<PayTypeRecord>();
        else
            payTypeListQ = payTypeList.Where(w => w.AllowWIS.HasValue && w.AllowWIS.Value == true && w.PayCur == UserData.SaleCur && w.Market == UserData.Market).ToList<PayTypeRecord>();

        PayTypeRecord payType = null;
        if (specCode)
            payType = payTypeList.Find(f => f.SpecCode == PaymentId);
        else
            payType = payTypeList.Find(f => f.Bank == bankMaps.TvCode && f.PayCur == curreny && f.AllowWIS.HasValue && f.AllowWIS.Value && f.Market == UserData.Market);
        pymntDetail.PayTypeID = payType.RecID;

        int? paymentRefId = new TvBo.NovaPayments().saveBeginPayment(UserData, pymntDetail);
        if (!paymentRefId.HasValue)
            return null;
        string vkRef = paymentRefId.ToString();

        string paymentVal = Amount.HasValue ? Amount.Value.ToString("#.00").Replace(",", ".") : "0";
        if (paymentVal.IndexOf(".") == 0)
            paymentVal = "0" + paymentVal;

        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_PROJECT_ID", TagName = "input", Data = gatewayApi.ProjectId });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_AGENCY_ID", TagName = "input", Data = UserData.AgencyID });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_PAYMENT_ID", TagName = "input", Data = bankMaps != null ? bankMaps.NovaCode : (specCode ? payType.SpecCode : "") });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_AMOUNT", TagName = "input", Data = paymentVal });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_FULL_ORDER_AMOUNT", TagName = "input", Data = paymentVal });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_CURR", TagName = "input", Data = curreny });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_REF", TagName = "input", Data = vkRef });

        MD5 _hash = MD5CryptoServiceProvider.Create();
        byte[] _plainTextBytes = Encoding.ASCII.GetBytes(gatewayApi.Market + vkRef);
        byte[] _hashBytes = _hash.ComputeHash(_plainTextBytes);
        string vk_Ref_Unique = BitConverter.ToString(_hashBytes).Replace("-", "").ToLowerInvariant();
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_REF_UNIQUE", TagName = "input", Data = vk_Ref_Unique });

        string vk_Msg = string.Format("Order No.{0}", vkRef);
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_MSG", TagName = "input", Data = vk_Msg });

        string data = gatewayApi.ProjectId +
                      UserData.AgencyID +
                      selectedPayment.paymentId +
                      paymentVal +
                      paymentVal +
                      curreny +
                      paymentRefId.ToString() +
                      vk_Ref_Unique +
                      vk_Msg +
                      gatewayApi.Secret;

        SHA1 hash = SHA1CryptoServiceProvider.Create();
        byte[] plainTextBytes = Encoding.ASCII.GetBytes(data);
        byte[] hashBytes = hash.ComputeHash(plainTextBytes);
        sign = BitConverter.ToString(hashBytes).Replace("-", "").ToLowerInvariant();

        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_MAC", TagName = "input", Data = sign });
        string urlHeader = HttpContext.Current.Session["BasePageRoot"].ToString(); //"http: //devb2b.dev.b2b.tourvisio.com:8181/";
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_URL_SUCCESS", TagName = "input", Data = urlHeader + "Payments/Novaturas/ReturnSuccess.aspx?market=" + UserData.Market });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_URL_FAIL", TagName = "input", Data = urlHeader + "Payments/Novaturas/ReturnFail.aspx?market=" + UserData.Market });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_URL_WAIT", TagName = "input", Data = urlHeader + "Payments/Novaturas/ReturnWait.aspx?market=" + UserData.Market });


        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_LANG", TagName = "input", Data = "LT" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "VK_RETURN_LANG", TagName = "input", Data = "LT" });

        return htmlData;
    }

    internal static string getStringMD5(string data)
    {
        MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
        byte[] bs = System.Text.Encoding.UTF8.GetBytes(data);
        return BitConverter.ToString(MD5.ComputeHash(bs)).Replace("-", "");
    }

    public static string MD5Of(string text)
    {
        return MD5Of(text, Encoding.Default);
    }
    public static string MD5Of(string text, Encoding enc)
    {
        return HashOf<MD5CryptoServiceProvider>(text, enc);
    }
    public static string SHA1Of(string text)
    {
        return SHA1Of(text, Encoding.Default);
    }
    public static string SHA1Of(string text, Encoding enc)
    {
        return HashOf<SHA1CryptoServiceProvider>(text, enc);
    }

    public static string SHA384Of(string text)
    {
        return SHA384Of(text, Encoding.Default);
    }
    public static string SHA384Of(string text, Encoding enc)
    {
        return HashOf<SHA384CryptoServiceProvider>(text, enc);
    }

    public static string SHA512Of(string text)
    {
        return SHA512Of(text, Encoding.Default);
    }
    public static string SHA512Of(string text, Encoding enc)
    {
        return HashOf<SHA512CryptoServiceProvider>(text, enc);
    }

    public static string SHA256Of(string text)
    {
        return SHA256Of(text, Encoding.Default);
    }
    public static string SHA256Of(string text, Encoding enc)
    {
        return HashOf<SHA256CryptoServiceProvider>(text, enc);
    }

    public static string HashOf<TP>(string text, Encoding enc)
        where TP : HashAlgorithm, new()
    {
        var buffer = enc.GetBytes(text);
        var provider = new TP();
        return BitConverter.ToString(provider.ComputeHash(buffer)).Replace("-", "");
    }
}
