﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReturnWait.aspx.cs" Inherits="Payments_Novaturas_ReturnWait" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script src="../../Scripts/jquery.min.js"></script>

    <script type="text/javascript">        
        function closeWindow() {                        
            this.close();
            window.close();            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>
                <asp:Label ID="lblMsg" runat="server" /></h2>
            <br />
            <strong>Referans :</strong><asp:Label ID="_refNo" runat="server" /><br />
            <input type="button" value="Close" onclick="closeWindow();" />
            <asp:Literal ID="hfSuccess" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
