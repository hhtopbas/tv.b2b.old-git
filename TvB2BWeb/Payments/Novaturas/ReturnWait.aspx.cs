﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using TvTools;

public partial class Payments_Novaturas_ReturnWait: System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.Equals(Request.Params["VK_SERVICE"], "1201"))
                return;
            
            lblMsg.Text = "Payment could not be received. Waiting...";            
        }
        catch (Exception Ex)
        {
            lblMsg.Text = Common.FlattenException(Ex);
        }
    }

    public static void WriteLog(string logValue)
    {
        string filenName = AppDomain.CurrentDomain.BaseDirectory + "Log\\ErrLog.txt";
        FileStream f = new FileStream(filenName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
        try
        {
            StreamWriter writer = new StreamWriter(f);
            writer.WriteLine(logValue);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            f.Close();
        }
    }
}