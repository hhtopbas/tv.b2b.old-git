﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default_Page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Default_Page</title>

  <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="../../Scripts/NumberFormat154.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.url.js" type="text/javascript"></script>
  <script src="../../Scripts/jquery.json.js" type="text/javascript"></script>  

  <link href="../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />

  <style type="text/css">
    .slResNo { width: 100px; }

    .slPayAmount { width: 100px; text-align: right; }

    .slCurrency { width: 50px; }
  </style>

  <script language="javascript" type="text/javascript">

    var lblSessionEnd = '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var ComboSelect = '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';

    function logout() {
      $('<div>' + lblSessionEnd + '</div>').dialog({
        autoOpen: true,
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        resizable: true,
        autoResize: true,
        bigframe: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog("close");
            $(this).dialog("destroy");
            window.location = '../../Default.aspx';
          }
        }]
      });
    }

    function showAlert(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        position: 'center',
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog("close");
            return true;
          }
        }]
      });
    }

    function closeWindow() {
      this.close();
      window.close();
    }

    function closePaymentDiv() {
      this.close();
      window.close();
    }

    function payment() {
      if ($("#paymentType").val() == undefined || $("#paymentType").val() == '') {
        showAlert("Please choose a bank");
        return;
      }
      var novaApiValue = $("#form1").data("formData").NovaApiValue;

      var param = new Object();
      param.paymentData = novaApiValue;
      param.PaymentId = $("#paymentType").val();
      param.Amount = $("#totalpayment").data("paidTotal");
      param.Curr = $("#totalpayment").data("paidTotalCurr");
      $.ajax({
        async: false,
        type: "POST",
        url: "Default.aspx/getPaymentData",
        data: $.json.encode(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {

            var data = msg.d;
            $.each(data, function (i) {
              switch (this.TagName) {
                case 'span':
                  $("#" + this.IdName).text('');
                  $("#" + this.IdName).text(this.Data);
                  break;
                case 'div':
                  $("#" + this.IdName).html('');
                  $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                  break;
                case 'img':
                  $("#" + this.IdName).removeAttr("src");
                  $("#" + this.IdName).attr("src", this.Data);
                  break;
                default: $("#" + this.IdName).val('');
                  $("#" + this.IdName).val(this.Data);
                  break;
              }
            });

          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
      $("#bloxidsebvbForm").submit();
      closePaymentDiv();
    }

    function getPaymentType(data, orjData) {
      $("#totalpayment").html(orjData.PaymentValueInfo);
      $("#totalpayment").data("paidTotal", orjData.SaleCurrTotal);
      $("#totalpayment").data("paidTotalCurr", orjData.SaleCurr);
      $("#paymentType").html('');
      var comboData = '';
      comboData += '<option value="">' + ComboSelect + '<\/option>';
      $.each(data.payments.payment, function (i) {
        comboData += '<option value="' + this.paymentId + '">' + this.name + '<\/option>';
      });
      $("#paymentType").html(comboData);
    }

    function exportExcel() {
      var data = $("#excelData").html().toString();
      var xls = $(data);
      
      xls.find('tr').each(function (i) {
        var amountStr = $(this).find(".slPayAmount").html().toString();
        var amount = (parseFloat(amountStr) * 100).toFixed();
        $(this).find(".slPayAmount").html("=" + amount.toString() + "/100");
      });
      
      var ua = window.navigator.userAgent;
      var msie = ua.indexOf("MSIE ");
      if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(xls[0].outerHTML);
        txtArea1.document.close();
        txtArea1.focus();
        txtArea1.document.execCommand("SaveAs", true, "download.xls");
      } else if (!!ua.match(/Edge/)) {
        alert('Microsoft Edge not supported.');
      } else {
        var a = document.createElement('a');
        var data_type = 'data:application/vnd.ms-excel';        
        a.href = data_type + ', ' + encodeURIComponent(xls[0].outerHTML);
        a.download = 'payment.xls';
        a.click();        
      }      
    }   

    function getFormData(automaticAllocation, manuelAmount) {
      $("#selectedListDiv").html('');
      var params = new Object();
      params.Auto = automaticAllocation;
      params.AutoValue = manuelAmount;
      $.ajax({
        type: "POST",
        url: "Default.aspx/getFormData",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            if (msg.d.Ok == true) {
              $("#form1").data("formData", msg.d);
              var novaApiValue = msg.d.NovaApiValue;
              getPaymentType(novaApiValue, msg.d);
            }
            else {
              $("#totalpayment").html(mdg.d);
            }

            if (msg.d.hasOwnProperty('SelectedList') && msg.d.SelectedList != null) {
              var selectedListDiv = '';
              selectedListDiv += '<span onclick="window.open(\'' + msg.d.ExportFilePath + '\',\'_blank\');" style=\"cursor:pointer; text-decoration:underline;\">--> Excel --></span><br />';
              selectedListDiv += '<div id="excelData">';
              selectedListDiv += '<table id="xlsData">';
              $.each(msg.d.SelectedList, function (i) {
                selectedListDiv += '<tr>';
                selectedListDiv += '<td class="slResNo">' + this.ResNo + '</td><td class="slPayAmount">' + this.PayAmount + '</td><td class="slCurrency">' + this.Currency + '</td>';
                selectedListDiv += '</tr>';
              });
              selectedListDiv += '</table>';
              selectedListDiv += '</div>';
              $("#selectedListDiv").html(selectedListDiv);

            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(function () {
      $.query = $.query.load(location.href);
      automaticAllocation = $.query.get('Auto');
      var manuel = $.query.get('Manuel');
      getFormData(automaticAllocation == '1', manuel == '' || manuel == 'null' ? null : manuel);
    });
  </script>

</head>
<body class="ui-helper-reset ui-widget ui-widget-content" style="font-size: 80%;">
  <form id="form1" runat="server">
    <div class="ui-helper-clearfix">
      <select id="paymentType">
      </select>
    </div>
    <div class="ui-helper-clearfix">
      <span id="totalpayment"></span>
    </div>
    <div id="paymentButtons" class="ui-helper-clearfix">
      <input type="button" id="btnOK" value='<%= GetGlobalResourceObject("LibraryResource", "btnOK")%>'
        style="width: 140px;" onclick="payment();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
    </div>
    <div id="selectedListDiv" class="ui-helper-clearfix">
    </div>
  </form>
  <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
    style="display: none;">
    <span id="messages"></span>
  </div>
  <iframe id="txtArea1" style="display: none"></iframe>
  <form method="post" name="bloxidsebvbForm" id="bloxidsebvbForm" action="http://test-payments.novatours.eu/gateway" target="_blank">
    <input type="hidden" value="" name="VK_PROJECT_ID" id="VK_PROJECT_ID" />
    <input type="hidden" value="" name="VK_AGENCY_ID" id="VK_AGENCY_ID" />
    <input type="hidden" value="" name="VK_PAYMENT_ID" id="VK_PAYMENT_ID" />
    <input type="hidden" value="" name="VK_AMOUNT" id="VK_AMOUNT" />
    <input type="hidden" value="" name="VK_FULL_ORDER_AMOUNT" id="VK_FULL_ORDER_AMOUNT" />
    <input type="hidden" value="" name="VK_CURR" id="VK_CURR" />
    <input type="hidden" value="" name="VK_REF" id="VK_REF" />
    <input type="hidden" value="" name="VK_REF_UNIQUE" id="VK_REF_UNIQUE" />
    <input type="hidden" value="" name="VK_MSG" id="VK_MSG" />
    <input type="hidden" value="" name="VK_MAC" id="VK_MAC" />
    <input type="hidden" value="" name="VK_URL_SUCCESS" id="VK_URL_SUCCESS" />
    <input type="hidden" value="" name="VK_URL_FAIL" id="VK_URL_FAIL" />
    <input type="hidden" value="" name="VK_URL_WAIT" id="VK_URL_WAIT" />
    <input type="hidden" value="" name="VK_LANG" id="VK_LANG" />
    <input type="hidden" value="" name="VK_RETURN_LANG" id="VK_RETURN_LANG" />
  </form>
</body>
</html>
