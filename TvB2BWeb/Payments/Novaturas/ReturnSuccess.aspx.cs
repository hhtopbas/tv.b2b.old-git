﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using TvTools;

public partial class Payments_Novaturas_ReturnSuccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.Equals(Request.Params["VK_SERVICE"], "1101"))
                return;

            hfSuccess.Text = "<div></div>";
            if (!IsPostBack)
            {
                NovaResponceService novaResponce = new NovaResponceService();

                novaResponce.VK_SERVICE = Request.Params["VK_SERVICE"];
                novaResponce.VK_TRANSACTION_ID = Request.Params["VK_TRANSACTION_ID"];
                novaResponce.VK_REF = Request.Params["VK_REF"];
                novaResponce.VK_REF_UNIQUE = Request.Params["VK_REF_UNIQUE"];
                novaResponce.VK_PAYMENT_ID = Request.Params["VK_PAYMENT_ID"];
                novaResponce.VK_PAYMENT_NAME = Request.Params["VK_PAYMENT_NAME"];
                novaResponce.VK_CURR = Request.Params["VK_CURR"];
                novaResponce.VK_AMOUNT = Request.Params["VK_AMOUNT"];
                novaResponce.VK_SND_ACC = Request.Params["VK_SND_ACC"];
                novaResponce.VK_SND_NAME = Request.Params["VK_SND_NAME"];
                novaResponce.VK_PARTNER_ID = Request.Params["VK_PARTNER_ID"];
                novaResponce.VK_MAC = Request.Params["VK_MAC"];
                novaResponce.VK_LANG = Request.Params["VK_LANG"];
                novaResponce.VK_AUTO = Request.Params["VK_AUTO"];

                string errorMsg = string.Empty;

                NovaBeforePament paymentRec = new NovaPayments().getBeginPayment(Conversion.getInt32OrNull(novaResponce.VK_REF));
                int? vk_Ref = Conversion.getInt32OrNull(novaResponce.VK_REF);
                if (!vk_Ref.HasValue)
                {
                    lblMsg.Text = "Payment could not be received.";
                    //this.Parent.ResolveUrl
                    return;
                }

                if (paymentRec.RefNo.ToString() != novaResponce.VK_REF)
                {
                    lblMsg.Text = "Referans number problem payment not be received.";
                    return;
                }

                if (paymentRec.RefNo.ToString() == novaResponce.VK_REF && paymentRec.TvSuccess == true)
                {
                    if (novaResponce.VK_AUTO == "Y")
                    {
                        HttpResponse r = System.Web.HttpContext.Current.Response;
                        r.Clear();
                        r.ContentEncoding = Encoding.UTF8;
                        r.ContentType = "text/html";
                        r.Write("--Novaturas response OK--");
                        r.StatusCode = 200;
                        r.End();
                    }
                    else
                    {
                        lblMsg.Text = "Payment successful.";
                        _refNo.Text = novaResponce.VK_TRANSACTION_ID.ToString();
                        HttpContext.Current.Response.StatusCode = 200;
                        return;
                    }
                }

                User UserData = new Users().CreateUserData();
                UserData.AgencyID = paymentRec.AgencyID;

                if (!new Users().getAdminUser(ref UserData, IpAddress(), ref errorMsg, HttpContext.Current.Session.SessionID.ToString()))
                {
                    WriteLog(DateTime.Now.ToString("yyyy-MM-dd HH:mm") + " \t" +
                                    "Http 500: " + " \t" +
                                    Request.Params["VK_SERVICE"] + " \t" +
                                    Request.Params["VK_TRANSACTION_ID"] + " \t" +
                                    Request.Params["VK_REF"] + " \t" +
                                    Request.Params["VK_REF_UNIQUE"] + " \t" +
                                    Request.Params["VK_PAYMENT_ID"] + " \t" +
                                    Request.Params["VK_PAYMENT_NAME"] + " \t" +
                                    Request.Params["VK_CURR"] + " \t" +
                                    Request.Params["VK_AMOUNT"] + " \t" +
                                    Request.Params["VK_SND_ACC"] + " \t" +
                                    Request.Params["VK_SND_NAME"] + " \t" +
                                    Request.Params["VK_PARTNER_ID"] + " \t" +
                                    Request.Params["VK_MAC"] + " \t" +
                                    Request.Params["VK_LANG"] + " \t" +
                                    Request.Params["VK_AUTO"] + " \t" +
                                    IpAddress() + "\t" +
                                    errorMsg + "\t" +
                                    HttpContext.Current.Session.SessionID.ToString()
                    );
                    HttpContext.Current.Response.StatusCode = 500;
                    return;
                }

                string gatewayApiJson = Conversion.getStrOrNull(new Common().getFormConfigValue("Payment", "GatewayApi"));
                List<NovaGatewayApi> gatewayApiList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NovaGatewayApi>>(gatewayApiJson);
                NovaGatewayApi gatewayApi = gatewayApiList.Find(f => f.Market == UserData.Market);

                //string bankMapsStr = Conversion.getStrOrNull(new Common().getFormConfigValue("Payment", "BankMaps"));
                //List<NovaBankMaping> bankMappings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NovaBankMaping>>(bankMapsStr);
                //NovaBankMaping bankMaps = bankMappings.Find(f => f.Market == UserData.Market && f.NovaCode == novaResponce.VK_PAYMENT_ID);

                List<PayTypeRecord> payTypeList = new Common().getPayType(UserData, UserData.Market, ref errorMsg);
                PayTypeRecord payType = payTypeList.Find(f => f.RecID == paymentRec.PayTypeID);

                bool success = false;
                if (string.Equals(novaResponce.VK_SERVICE, "1101"))
                {
                    string data = novaResponce.VK_SERVICE +
                                  novaResponce.VK_TRANSACTION_ID +
                                  novaResponce.VK_REF +
                                  novaResponce.VK_REF_UNIQUE +
                                  novaResponce.VK_PAYMENT_ID +
                                  novaResponce.VK_PAYMENT_NAME +
                                  novaResponce.VK_CURR +
                                  novaResponce.VK_AMOUNT +
                                  novaResponce.VK_SND_ACC +
                                  novaResponce.VK_SND_NAME +
                                  novaResponce.VK_PARTNER_ID +
                                  gatewayApi.Secret;

                    SHA1 hash = SHA1CryptoServiceProvider.Create();
                    byte[] plainTextBytes = Encoding.UTF8.GetBytes(data);
                    byte[] hashBytes = hash.ComputeHash(plainTextBytes);
                    string sign = BitConverter.ToString(hashBytes).Replace("-", "").ToLowerInvariant();
                    if (sign == novaResponce.VK_MAC && payType != null)
                    {
                        //if (paymentRec.NovaSuccess == true && paymentRec.TvSuccess == true && string.Equals(Request.Params["VK_AUTO"], "N"))
                        //{
                        //    lblMsg.Text = "Payment successful.";
                        //    _refNo.Text = novaResponce.VK_TRANSACTION_ID.ToString();
                        //    HttpContext.Current.Response.StatusCode = 200;
                        //    return;
                        //}

                        paymentRec.NovaSuccess = true;
                        paymentRec.NovaSuccessDate = DateTime.Now;
                        if (paymentRec.TvSuccess == false)
                        {
                            errorMsg = string.Empty;
                            bool successNova = new NovaPayments().updateNovaSuccess(paymentRec.RefNo, novaResponce.VK_TRANSACTION_ID, ref errorMsg);
                            if (successNova && string.IsNullOrEmpty(errorMsg))
                            {
                                int? journalID = null;
                                string xml = string.Empty;
                                xml += "<root>";
                                foreach (AgencyPayList row in paymentRec.PaymentResList)
                                {
                                    xml += string.Format("<ResPayment ResNo=\"{0}\" Payment=\"{1}\" />", row.ResNo, row.PayAmount.HasValue ? (row.PayAmount.Value * 100).ToString("#.") : "");
                                }
                                xml += "</root>";
                                string referances = "Order No. " + paymentRec.RefNo.ToString();
                                if (new Payments().createPaymentList(UserData, paymentRec.TotalAmount, novaResponce.VK_CURR, payType.Code, xml, ref journalID, referances, null))
                                {
                                    success = true;
                                    if (new NovaPayments().updateTvSuccess(paymentRec.RefNo, journalID))
                                    {
                                        success = true;
                                        if (novaResponce.VK_AUTO == "Y")
                                        {
                                            HttpResponse r = System.Web.HttpContext.Current.Response;
                                            r.Clear();
                                            r.ContentEncoding = Encoding.UTF8;
                                            r.ContentType = "text/html";
                                            r.Write("--Novaturas response OK--");
                                            r.StatusCode = 200;
                                            r.End();
                                        }
                                    }
                                }
                                if (success && novaResponce.VK_AUTO == "N")
                                {
                                    string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\APMSelectList." + HttpContext.Current.Session.SessionID;
                                    if (File.Exists(path))
                                        File.Delete(path);

                                    lblMsg.Text = "Payment successful.";
                                    _refNo.Text = novaResponce.VK_TRANSACTION_ID.ToString();
                                    HttpContext.Current.Response.StatusCode = 200;
                                    return;
                                }
                                else
                                    if (novaResponce.VK_AUTO == "N")
                                    {
                                        lblMsg.Text = "Payment could not be received.";
                                    }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(errorMsg))
                                    lblMsg.Text = "Payment already received.";
                                else
                                    lblMsg.Text = "Payment could not be received.";
                            }
                        }
                    }
                }
            }
            else
            {
                lblMsg.Text = "Please don't refresh the page";

                return;
            }
        }
        catch (Exception Ex)
        {
            lblMsg.Text = Common.FlattenException(Ex);
        }
    }

    public static void WriteLog(string logValue)
    {
        string filenName = AppDomain.CurrentDomain.BaseDirectory + "Log\\ErrLog.txt";
        FileStream f = new FileStream(filenName, FileMode.OpenOrCreate, FileAccess.Write);
        try
        {
            StreamWriter writer = new StreamWriter(f);
            writer.WriteLine(logValue);
            writer.Close();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            f.Close();
        }
    }

    public static string IpAddress()
    {
        string strIpAddress;
        strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (strIpAddress == null)
        {
            strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        return strIpAddress;
    }
}