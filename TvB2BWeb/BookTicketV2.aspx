﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookTicketV2.aspx.cs" Inherits="BookTicketV2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "BookTicket")%></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
  <script src="Scripts/checkDate.js" type="text/javascript"></script>
  <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/BookTicket.css?v=1" rel="stylesheet" type="text/css" />
  <style type="text/css">
    body { width: 950px; font-family: Tahoma; font-size: 10px; text-align: left; }
    #gridCust { width: 950px; border: solid 1px #666; font-family: Tahoma; font-size: 10px; }
  </style>

  <script type="text/javascript">

    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
    var btnGotoResCard = '<%= GetGlobalResourceObject("LibraryResource", "btnGotoResCard") %>';
    var btnPaymentPage = '<%= GetGlobalResourceObject("LibraryResource", "btnPaymentPage") %>';
    var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
    var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';

    var msgAdultCountSmallChhildCount = '<%= GetGlobalResourceObject("BookTicket", "AdultCountSmallChhildCount") %>';
    var msgWhereInvoiceTo = '<%= GetGlobalResourceObject("LibraryResource", "WhereInvoiceTo") %>';
    var msglblAlreadyRes = '<%= GetGlobalResourceObject("BookTicket", "lblAlreadyRes") %>';
    var msgEnterChildOrInfantBirtDay = '<%= GetGlobalResourceObject("LibraryResource", "EnterChildOrInfantBirtDay") %>';
    var msglblNoResult = '<%= GetGlobalResourceObject("BookTicket", "lblNoResult") %>';
    var msgviewOldResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewOldResSalePrice") %>';
    var msgviewNewResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewNewResSalePrice") %>';
    var msgcancelService = '<%= GetGlobalResourceObject("LibraryResource", "cancelService") %>';
    var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
    var ForAgency = '<%=GetGlobalResourceObject("LibraryResource", "ForAgency") %>';
    var ForPassenger = '<%=GetGlobalResourceObject("LibraryResource", "ForPassenger") %>';
    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var msgInvalidBirthDate = '<%= GetGlobalResourceObject("LibraryResource","InvalidBirthDate") %>';
    var msgMaxPaxReservation = '<%= GetGlobalResourceObject("LibraryResource", "MaxPaxReservation") %>';

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    var reservationSaved = false;

    $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    function logout() {
      self.parent.logout();
    }

    $(document).ready(function () {
      $("#dialog-resCustOtherInfo").dialog(
      {
        autoOpen: false,
        modal: true,
        width: 440,
        height: 440,
        resizable: true,
        autoResize: true
      });

      $("#dialog-resCustomerAddress").dialog(
      {
        autoOpen: false,
        modal: true,
        width: 595,
        height: 480,
        resizable: false,
        autoResize: true
      });
    });


    function showDialog(msg, transfer, trfUrl) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
              if (transfer == true) {
                var url = trfUrl;
                $(location).attr('href', url);
              }
            }
          }]
        });
      });
    }

    function showDialogEndGotoPaymentPage(msg, ResNo, PaymentPageUrl, DirectPaymentPage) {
      if (DirectPaymentPage == true) {
        $(this).dialog('close');
        window.close;
        self.parent.paymentPage(ResNo, PaymentPageUrl);
      }
      else {
        $(function () {
          $("#messages").html(msg);
          $("#dialog").dialog("destroy");
          $("#dialog-message").dialog({
            maxWidth: 880,
            maxHeight: 500,
            modal: true,
            resizable: true,
            autoResize: true,
            buttons: [{
              text: btnGotoResCard,
              click: function () {
                $(this).dialog('close');
                window.close;
                self.parent.gotoResViewPage(ResNo);
              }
            }, {
              text: btnPaymentPage,
              click: function () {
                $(this).dialog('close');
                window.close;
                self.parent.paymentPage(ResNo, PaymentPageUrl);
              }
            }]
          });
        });
      }
    }

    function showDialogEndExit(msg, ResNo) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
              window.close;
              if (ResNo != '')
                self.parent.gotoResViewPage(ResNo);
              else self.parent.cancelMakeRes();
            }
          }]
        });
      });
    }

    function SetAge(Id, cinDate, _formatDate) {
      try {
        var birthDate = dateValue(_formatDate, $("#iBirthDay" + Id).val());
        if (birthDate == null) {
          $("#iBirthDay" + Id).val('');
          $("#iAge" + Id).val('');
          return;
        }
        var checkIN = dateValue(_formatDate, cinDate);
        if (checkIN == null) return;
        var _minBirthDate = new Date(1, 1, 1910);
        var _birthDate = birthDate;
        var _cinDate = checkIN;
        if (_birthDate < _minBirthDate) {
          $("#iAge" + Id).val('');
          birthDate = '';
          showDialog(InvalidBirthDate, false, '');
          return;
        }
        var age = Age(_birthDate, _cinDate);  //parseInt((_cinDate - _birthDate) / (1000 * 24 * 60 * 60 * 365.25))
        $("#iAge" + Id).val(age);
      }
      catch (err) {
        $("#iAge" + Id).val('');
      }
    }

    function leaderChange(idNo) {
      var leader = $("#iLeader" + idNo);
      var hfCustCount = parseInt($("#hfCustCount").val());
      var custCount = parseInt(hfCustCount);
      var custList = [];
      for (var i = 1; i <= custCount; i++) {
        var addressInfo = $("#resCustInfo" + i);
        if (i == idNo)
          addressInfo.show();
        else addressInfo.hide();
      }
    }

    function homeCountryChanged(idNo) {
      var country = $("#AddrHomeCountry" + idNo).val();
      var countryList = $("#countryList").val();

      var queryResult = $.Enumerable.From($.json.decode(countryList))
                         .Where(function (x) { return x.CountryName == country })
                         .Select("$.CountryPhoneCode")
                         .First();
      if (queryResult != '') {
        $("#MobTelCountryCode" + idNo).text('+' + queryResult);
      }
    }

    function getCustomers() {
      var cust = '';
      var hfCustCount = parseInt($("#hfCustCount").val());
      var custCount = parseInt(hfCustCount);
      var custList = [];
      for (var i = 1; i <= custCount; i++) {
        var cust = new Object();
        cust.SeqNo = $("#iSeqNo" + i).text();
        cust.Title = $("#iTitle" + i).length > 0 ? $("#iTitle" + i).val() : '';
        cust.Surname = $("#iSurname" + i).length > 0 ? $("#iSurname" + i).val() : '';
        cust.SurnameL = $("#iSurnameL" + i).length > 0 ? $("#iSurnameL" + i).val() : '';
        cust.Name = $("#iName" + i).length > 0 ? $("#iName" + i).val() : '';
        cust.NameL = $("#iNameL" + i).length > 0 ? $("#iNameL" + i).val() : '';
        cust.BirtDay = $("#iBirthDay" + i).length > 0 ? $("#iBirthDay" + i).val() : '';
        cust.Age = $("#iAge" + i).length > 0 ? $("#iAge" + i).val() : '';
        cust.IDNo = $("#iIDNo" + i).length > 0 ? $("#iIDNo" + i).val() : '';
        cust.PassSerie = $("#iPassSerie" + i).length > 0 ? $("#iPassSerie" + i).val() : '';
        cust.PassNo = $("#iPassNo" + i).length > 0 ? $("#iPassNo" + i).val() : '';
        cust.PassIssueDate = $("#iPassIssueDate" + i).length > 0 ? $("#iPassIssueDate" + i).val() : '';
        cust.PassExpDate = $("#iPassExpDate" + i).length > 0 ? $("#iPassExpDate" + i).val() : '';
          if ($("#iPassGiven" + i).length > 0) {
              if ($("#iPassGiven" + i).children("option").length > 0 && $("#iPassGiven" + i).val() == "") {
                  var passindex = $("#iPassGiven" + i).children("option:selected").index();
                  cust.PassGiven = passindex > 0 ? $("#iPassGiven" + i).children("option:selected").text() : "";
              } else
                  cust.PassGiven = $("#iPassGiven" + i).val();
          } else
              cust.PassGiven = '';
        cust.Phone = $("#iPhone" + i).length > 0 ? $("#iPhone" + i).val() : '';
        cust.Nation = $("#iNation" + i).length > 0 ? $("#iNation" + i).val() : null;
        cust.Nationality = $("#iNationality" + i).length > 0 ? $("#iNationality" + i).val() : '';
        cust.Passport = $("#iHasPassport" + i).length > 0 ? $("#iHasPassport" + i)[0].checked : true;
        cust.Leader = $('input[name=Leader]:checked').val() == cust.SeqNo;
        cust.ResCustInfo = null;
        var addressInfo = $("#resCustInfo" + i)
        if (cust.Leader) {
          var custInfo = new Object();
          custInfo.CTitle = $("#CTitle" + i).length > 0 ? $("#CTitle" + i).val() : '';
          custInfo.CName = $("#CName" + i).length > 0 ? $("#CName" + i).val() : '';
          custInfo.CSurName = $("#CSurName" + i).length > 0 ? $("#CSurName" + i).val() : '';
          custInfo.MobTel = ($("#MobTelCountryCode" + i).length > 0 ? $("#MobTelCountryCode" + i).text() : '') + ($("#MobTel" + i).length > 0 ? $("#MobTel" + i).val() : '');
          custInfo.ContactAddr = 'H';
          custInfo.InvoiceAddr = 'H';
          custInfo.AddrHome = $("#AddrHome" + i).length > 0 ? $("#AddrHome" + i).val() : '';
          custInfo.AddrHomeCity = $("#AddrHomeCity" + i).length > 0 ? $("#AddrHomeCity" + i).val() : '';
          custInfo.AddrHomeCountry = $("#AddrHomeCountry" + i).length > 0 ? $("#AddrHomeCountry" + i).val() : '';
          custInfo.AddrHomeZip = $("#AddrHomeZip" + i).length > 0 ? $("#AddrHomeZip" + i).val() : '';
          custInfo.AddrHomeTel = ($("#AddrHomeTelCountryCode" + i).length > 0 ? $("#AddrHomeTelCountryCode" + i).text() : '') + ($("#AddrHomeTel" + i).length > 0 ? $("#AddrHomeTel" + i).val() : '');
          custInfo.AddrHomeFax = ($("#AddrHomeFaxCountryCode" + i).length > 0 ? $("#AddrHomeFaxCountryCode" + i).text() : '') + ($("#AddrHomeFax" + i).length > 0 ? $("#AddrHomeFax" + i).val() : '');
          custInfo.AddrHomeEmail = $("#AddrHomeEmail" + i).length > 0 ? $("#AddrHomeEmail" + i).val() : '';
          custInfo.HomeTaxOffice = $("#HomeTaxOffice" + i).length > 0 ? $("#HomeTaxOffice" + i).val() : '';
          custInfo.HomeTaxAccNo = $("#HomeTaxAccNo" + i).length > 0 ? $("#HomeTaxAccNo" + i).val() : '';
          custInfo.WorkFirmName = $("#WorkFirmName" + i).length > 0 ? $("#WorkFirmName" + i).val() : '';
          custInfo.AddrWork = $("#AddrWork" + i).length > 0 ? $("#AddrWork" + i).val() : '';
          custInfo.AddrWorkCity = $("#AddrWorkCity" + i).length > 0 ? $("#AddrWorkCity" + i).val() : '';
          custInfo.AddrWorkCountry = $("#AddrWorkCountry" + i).length > 0 ? $("#AddrWorkCountry" + i).val() : '';
          custInfo.AddrWorkZip = $("#AddrWorkZip" + i).length > 0 ? $("#AddrWorkZip" + i).val() : '';
          custInfo.AddrWorkTel = ($("#AddrWorkTelCountryCode" + i).length > 0 ? $("#AddrWorkTelCountryCode" + i).text() : '') + ($("#AddrWorkTel" + i).length > 0 ? $("#AddrWorkTel" + i).val() : '');
          custInfo.AddrWorkFax = ($("#AddrWorkFaxCountryCode" + i).length > 0 ? $("#AddrWorkFaxCountryCode" + i).text() : '') + ($("#AddrWorkFax" + i).length > 0 ? $("#AddrWorkFax" + i).val() : '');
          custInfo.AddrWorkEMail = $("#AddrWorkEMail" + i).length > 0 ? $("#AddrWorkEMail" + i).val() : '';
          custInfo.WorkTaxOffice = $("#WorkTaxOffice" + i).length > 0 ? $("#WorkTaxOffice" + i).val() : '';
          custInfo.WorkTaxAccNo = $("#WorkTaxAccNo" + i).length > 0 ? $("#WorkTaxAccNo" + i).val() : '';
          custInfo.Bank = $("#Bank" + i).length > 0 ? $("#Bank" + i).val() : '';
          custInfo.BankAccNo = $("#BankAccNo" + i).length > 0 ? $("#BankAccNo" + i).val() : '';
          custInfo.BankIBAN = $("#BankIBAN" + i).length > 0 ? $("#BankIBAN" + i).val() : '';

          cust.ResCustInfo = custInfo;
        }
        custList.push(cust);
      }
      return $.json.encode(custList);
    }

    function setCustomers() {
      var data = new Object();
      data.data = $.json.decode(getCustomers());
      $.ajax({
        type: "POST",
        url: "BookTicketV2.aspx/setCustomers",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          return true;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function returnCustInfoEdit(data, source) {
      if (source == "save") {
        $.ajax({
          type: "POST",
          url: "BookTicketV2.aspx/setResCustInfo",
          data: '{"data":"' + data + '"}',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (source == "save") {
              getResCustDiv();

            }
            $("#dialog-resCustomerAddress").dialog("close");
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
              showDialogEndExit(xhr.responseText, '');
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      }
      else {
        $("#dialog-resCustomerAddress").dialog("close");
      }
    }

    function showCustAddress(CustNo) {
      var data = new Object();
      data.data = $.json.decode(getCustomers());
      $.ajax({
        type: "POST",
        url: "BookTicketV2.aspx/setCustomers", //setResCustomers",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var custInfoUrl = 'CustomerAddress.aspx?CustNo=';
          $("#dialog-resCustomerAddress").dialog("open");
          $("#resCustomerAddress").attr("src", custInfoUrl + CustNo);
          return false;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getResCustDiv() {
      $.ajax({
        type: "POST",
        url: "BookTicketV2.aspx/createTicketCustStr",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#resCustGrid").html('');
          $("#resCustGrid").html(msg.d);
          $(".formatDate").mask($("#dateMask").val());
          if ($(".mobPhone").length > 0 && $("#mobPhoneMask").val() != '') {
            $(".mobPhone").mask($("#mobPhoneMask").val());
          }
          if ($(".phone").length > 0 && $("#phoneMask").val() != '') {
            $(".phone").mask($("#phoneMask").val());
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function returnCustDetailEdit(dataS, source) {
      if (source == "save") {
        var data = new Object();
        data.data = dataS;
        $.ajax({
          type: "POST",
          url: "BookTicketV2.aspx/setResCustomer",
          data: $.json.encode(data),
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (source == "save") {
              getResCustDiv();
            }
            $("#dialog-resCustOtherInfo").dialog("close");
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
              showDialogEndExit(xhr.responseText, '');
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      }
      else {
        $("#dialog-resCustOtherInfo").dialog("close");
      }
    }

    function showResCustInfo(CustNo) {
      var customers = getCustomers();
      $.ajax({
        type: "POST",
        url: "BookTicketV2.aspx/setResCustomers",
        data: '{"data":"' + customers + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var custInfoUrl = 'CustomerDetail.aspx?CustNo=';

          $("#dialog-resCustOtherInfo").dialog("open");
          $("#resCustOtherInfo").attr("src", custInfoUrl + CustNo);
          return false;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function returnSsrc(cancel, refresh) {
      if (cancel == true) {
        $("#dialog-Ssrc").dialog("close");
      }
      else {
        $("#dialog-Ssrc").dialog("close");
        if (refresh) {
          $.query = $.query.load(location.href);
          var bookNr = $.query.get('BookNr');
          getBookFlightInfo(bookNr);
        }
      }
    }

    function showSSRC(CustNo) {
      setCustomers();
      var serviceUrl = 'Controls/Ssrc.aspx?CustNo=' + CustNo + '&Save=false';
      $("#dialog-Ssrc").dialog("open");
      $("#Ssrc").attr("src", serviceUrl);
    }

    function searchSelectCust(CustNo) {
      $("#dialog-searchCust").dialog("close");
      var Custs = $.json.decode(getCustomers());  //getCustomers();
      var data = new Object();
      data.CustNo = CustNo;
      data.Custs = Custs;
      $.ajax({
        type: "POST",
        url: "BookTicketV2.aspx/copyCustomer",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == 'OK') {
            getResCustDiv();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function searchCustListShow(_html) {
      $(function () {
        $("#searchCust").html('');
        $("#searchCust").html(_html);
        $("#dialog").dialog("destroy");
        $("#dialog-searchCust").dialog({
          modal: true,
          width: 650,
          height: 450,
          resizable: true,
          autoResize: true
        });
      });
    }

    function searchCust(i) {
      $("#dialog").dialog("destroy");
      $("#searchCust").attr("src", "CustomerSearch.aspx?refNo=" + i + "&SeqNo=" + $("#iSeqNo" + i).text());
      $("#dialog-searchCust").dialog({
        modal: true,
        width: 750,
        height: 500
      });
    }

    function searchCustGetList(i) {
      var Surname = $("#iSurname" + i).val();
      var Name = $("#iName" + i).val();
      var BirthDate = $("#iBirthDay" + i).val();
      var SeqNo = $("#iSeqNo" + i).text();
      $.ajax({
        type: "POST",
        url: "BookTicketV2.aspx/searchCustomer",
        data: '{"refNo":"' + i + '","SeqNo":"' + SeqNo + '","Surname":"' + Surname + '","Name":"' + Name + '","BirthDate":"' + BirthDate + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != '')
            searchCustListShow(msg.d);
          else
            showDialogEndExit(msglblNoResult, '');
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function onSurnameExit(elmID) {
      var surnameList = $(".nameSurnameCss");
      var elmValue = $("#" + elmID).val();
      var custNo = elmID.replace('iSurname', '');
      surnameList.each(function (index) {
        $("#iTitle" + custNo + " option:selected").text();
        var custNoIdx = $(this).attr('custNo');
        if (custNoIdx == custNo) {
          var newValue = $("#iTitle" + $(this).attr('custNo') + " option:selected").text() + ". ";
          newValue += $("#" + elmID).val() + " ";
          newValue += $("#iName" + custNo).val();
          $(this).html(newValue);
        }
      });
    }

    function onNameExit(elmID) {
      var surnameList = $(".nameSurnameCss");
      var elmValue = $(elmID).val();
      var custNo = elmID.replace('iName', '');
      surnameList.each(function (index) {
        $("#iTitle" + custNo + " option:selected").text();
        var custNoIdx = $(this).attr('custNo');
        if (custNoIdx == custNo) {
          var newValue = $("#iTitle" + $(this).attr('custNo') + " option:selected").text() + ". ";
          newValue += $("#iSurname" + custNo).val() + " ";
          newValue += $("#" + elmID).val();
          $(this).html(newValue);
        }
      });
    }


    // Nova 15867

    function getResMainDiv() {
      $.ajax({
        async: false,
        type: "POST",
        url: "BookTicketV2.aspx/getResServiceDiv",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != null && msg.d.ServiceAdding == true) {
            $("#resServiceDiv").html('');
            $("#resServiceDiv").html(msg.d.html);
            if (msg.d.TotalSalePrice != '') {
              $("#totalSalePrice").html('');
              $("#totalSalePrice").html(msg.d.TotalSalePrice);
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showMsg(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function checkUsers() {
      var retVal = true;
      var CustList = $.json.decode(getCustomers());
      try {
        var i = 0;
        for (i = 0; i < CustList.length; i++) {
          if (CustList[i].Title > 5 && CustList[i].BirtDay == '') {
            retVal = false;
          }
        }
      }
      catch (e) {
        retVal = false;
      }
      return retVal;
    }

    function returnAddResServices(reLoad, msg) {
      $("#dialog-resServiceAdd").dialog("close");
      if (reLoad == true)
        getResMainDiv(false, false);
    }

    function showAddResService(serviceUrl) {
      var data = new Object();
      data.data = $.json.decode(getCustomers());
      $.ajax({
        async: false,
        type: "POST",
        url: "BookTicketV2.aspx/setCustomers",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var chkUsers = checkUsers();
          if (chkUsers || msg.d == 'OK' || msg.d == 'NO') {
            $("#dialog-resServiceAdd").dialog("open");
            if (serviceUrl.toString().indexOf('?') > -1) {
              $("#resServiceAdd").attr("src", serviceUrl + '&recordType=temp');
            }
            else {
              $("#resServiceAdd").attr("src", serviceUrl + '?recordType=temp');
            }
          }
          else {
            showMsg(msgEnterChildOrInfantBirtDay);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showMsg(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function returnAddResServicesExt(save) {
      if (save == true) {
        $("#dialog-resServiceExtAdd").dialog("close");
        getResMainDiv(false, false);
      }
      else {
        $("#dialog-resServiceExtAdd").dialog("close");
      }
    }

    function showAddResServiceExt(serviceUrl) {
      setCustomers();
      var data = new Object();
      data.data = $.json.decode(getCustomers());
      $.ajax({
        async: false,
        type: "POST",
        url: "BookTicketV2.aspx/setCustomers",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var chkUsers = checkUsers();
          if (chkUsers) {
            $("#dialog-resServiceExtAdd").dialog("open");
            $("#resServiceExtAdd").attr("src", serviceUrl + '?recordType=temp');
          } else {
            showMsg(msgEnterChildOrInfantBirtDay);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
          return false;
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    // Nova 15867

    function serviceDateChanged(elm) {
      var startDay = $("#" + elm.id).val();
      var id = elm.id.split("_");
      var recID = id[1];
      var begStartDay = null;
      var endStartDay = null;
      if (id[0] == 'serviceBegDate')
        begStartDay = parseInt($("#" + elm.id).val());
      if (id[0] == 'serviceEndDate')
        endStartDay = parseInt($("#" + elm.id).val());
      $.ajax({
        type: "POST",
        url: "BookTicketV2.aspx/updateDate",
        data: '{"ServiceID":' + recID + ',"ServiceExtID":null,"begStartDay":' + begStartDay + ',"endStartDay":' + endStartDay + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d == '') {
            //getResMainDiv(true, false);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function firstAddRemoveServiceExt() {
      $("input:checkbox[name='serviceExtSelect']").each(function () {
        if ($(this).attr('Comp') == 'true') {
          $(this).attr('checked', 'checked');
          addremoveServiceExt($(this).val());
        }
      });
    }

    function addremoveServiceExt(recID) {
      setCustomers();
      var recID2 = null;
      var changeService = $("#serviceExtSelect_" + recID.toString());

      if (changeService != undefined && changeService != null) {
        var chkSel = changeService.attr('checked') ? true : false;
        var Code = changeService.attr('Code');
        var RTSale = changeService.attr('RTSale');
        var FlightDir = changeService.attr('FlightDir');
        var Comp = changeService.attr('Comp');
        var ServCat = changeService.attr('ServCat');
        if (chkSel) {
          $("input:checkbox[name='custExtPrice_" + recID + "']").each(function () {
            $(this).attr('checked', 'checked');
          });
          $("#custPriceExtUL_" + recID.toString()).show();
          if (RTSale == 'true' && FlightDir == 'true') {
            $("input:[name='serviceExtSelect']").each(function () {
              if ($(this).attr('Code') == Code) {
                $(this).attr('checked', 'checked');
                var RecID = $(this).attr('RecID');
                var disabled = $(this).attr('disabled') == 'disabled' ? true : false;
                recID2 = RecID != undefined && RecID != '' ? RecID : null;
                $("input:checkbox[name='custExtPrice_" + RecID + "']").each(function () {
                  $(this).attr('checked', 'checked');
                  if (disabled) $(this).attr('disabled', 'disabled');
                });
                $("#custPriceExtUL_" + RecID.toString()).show();
              }
            });
            if (Comp == 'true') {
              $("input:[ServCat='" + ServCat + "']").each(function () {
                if ($(this).attr('Code') != Code) {
                  $(this).removeAttr('checked');
                  $("#custPriceExtUL_" + $(this).val()).hide();
                }
              });
            }
            else {
              $("input:[ServCat='" + ServCat + "']").each(function () {
                if ($(this).attr('Comp') == 'true') {
                  $(this).removeAttr('checked');
                  $("#custPriceExtUL_" + $(this).val()).hide();
                }
              });
            }
          }
        }
        else {
          $("input:checkbox[name='custExtPrice_" + recID + "']").each(function () {
            $(this).removeAttr('checked');
          });
          $("#custPriceExtUL_" + recID.toString()).hide();
          if (RTSale == 'true' && FlightDir == 'true') {
            $("input:[name='serviceExtSelect']").each(function () {
              if ($(this).attr('Code') == Code) {
                $(this).removeAttr('checked');
                var RecID = $(this).attr('RecID');
                recID2 = RecID != undefined && RecID != '' ? RecID : null;
                $("input:checkbox[name='custExtPrice_" + RecID + "']").each(function () {
                  $(this).removeAttr('checked');
                });
                $("#custPriceExtUL_" + RecID.toString()).hide();
              }
            });
            if (ServCat == '1' && $("input:[ServCat='" + ServCat + "'][checked='checked']").length == 0) {
              $("input:[ServCat='" + ServCat + "']").each(function () {
                if ($(this).attr('Comp') == 'true') {
                  $(this).attr('checked', 'checked');
                  $("#custPriceExtUL_" + $(this).val()).show();
                }
              });
            }
          }
        }
        var obj = new Object();
        obj.ServiceID = null;
        obj.ServiceExtID = recID;
        obj.ServiceExtID2 = recID2;
        obj.ChkSel = chkSel;

        $.ajax({
          async: false,
          type: "POST",
          url: "BookTicketV2.aspx/reCalcPrice",
          data: $.json.encode(obj), //'{"ServiceID":null,"ServiceExtID":' + recID + ',"ChkSel":' + chkSel + '}',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (msg.d != null && msg.d.ReCalcOk == true) {
              var salePrice = msg.d.SalePrice;
              $("#salePrice_" + recID).html(salePrice);
              if (msg.d.TotalSalePrice != '') {
                $("#totalSalePrice").html('');
                $("#totalSalePrice").html(msg.d.TotalSalePrice);
              }
            }
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
              showMsg(xhr.responseText);
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      }
    }

    function addremoveService(recID) {
      setCustomers();
      var changeService = $("#serviceSelect_" + recID.toString());
      if (changeService != undefined && changeService != null) {
        var chkSel = changeService.attr('checked') ? true : false;
        if (chkSel) {
          $("input:checkbox[name='custPrice_" + recID + "']").each(function () {
            $(this).attr('checked', 'checked');
          });
          $("#custPriceUL_" + recID.toString()).show();
          $("#mainServiceExtras_" + recID.toString()).show();
          $("#mainServiceExtrasCustPrice_" + recID.toString()).show();
        }
        else {
          $("input:checkbox[name='custPrice_" + recID + "']").each(function () {
            $(this).removeAttr('checked');
          });
          $("#custPriceUL_" + recID.toString()).hide();
          $("#mainServiceExtras_" + recID.toString()).hide();
          $("#mainServiceExtrasCustPrice_" + recID.toString()).hide();
        }
        var obj = new Object();
        obj.ServiceID = recID;
        obj.ServiceExtID = null;
        obj.ServiceExtID2 = null;
        obj.ChkSel = chkSel;
        $.ajax({
          async: false,
          type: "POST",
          url: "BookTicketV2.aspx/reCalcPrice",
          data: $.json.encode(obj), //'{"ServiceID":' + recID + ',"ServiceExtID":null,"ChkSel":' + chkSel + '}',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (msg) {
            if (msg.d != null && msg.d.ReCalcOk == true) {
              var salePrice = msg.d.SalePrice;
              $("#salePrice_" + recID).html(salePrice);
              if (msg.d.TotalSalePrice != '') {
                $("#totalSalePrice").html('');
                $("#totalSalePrice").html(msg.d.TotalSalePrice);
              }
            }
          },
          error: function (xhr, msg, e) {
            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
              showMsg(xhr.responseText);
          },
          statusCode: {
            408: function () {
              logout();
            }
          }
        });
      }
    }

    function changeCustExtPrice(elm) {
      var id = elm.id.split("_");
      var serviceID = id[1];
      var custID = id[2];
      var checked = $('#' + elm.id).attr('checked') == 'checked';

      $.ajax({
        async: false,
        type: "POST",
        url: "BookTicketV2.aspx/getResCustExtPrice",
        data: '{"serviceID":' + serviceID + ',"custID":' + custID + ',"selected":' + checked + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != null && msg.d.ReCalcOk == true) {
            $("#saleExtPrice_" + serviceID).html(msg.d);
            //getResMainDiv(true, false);
            if (msg.d.TotalSalePrice != '') {
              $("#totalSalePrice").html('');
              $("#totalSalePrice").html(msg.d.TotalSalePrice);
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeCustPrice(elm) {
      var id = elm.id.split("_");
      var serviceID = id[1];
      var custID = id[2];
      var checked = $('#' + elm.id).attr('checked') == 'checked';

      $.ajax({
        async: false,
        type: "POST",
        url: "BookTicketV2.aspx/getResCustPrice",
        data: '{"serviceID":' + serviceID + ',"custID":' + custID + ',"selected":' + checked + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != null && msg.d.ReCalcOk == true) {
            $("#salePrice_" + serviceID).html(msg.d.SalePrice);
            //getResMainDiv(true, false);
            if (msg.d.TotalSalePrice != '') {
              $("#totalSalePrice").html('');
              $("#totalSalePrice").html(msg.d.TotalSalePrice);
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getExtrasDiv() {
      $.ajax({
        async: false,
        type: "POST",
        url: "BookTicketV2.aspx/getExtrasDiv",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#divExtras").html('');
          if (msg.d != '') {
            $("#divExtras").html(msg.d);
            firstAddRemoveServiceExt();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function makeTicket(bookNr) {
      var adult = $("#iAdult").val();
      if (adult == '' || adult == '0') {
        adult = '1';
        $("#iAdult").val('1');
      }
      var child = $("#iChild").val();
      var infant = $("#iInfant").val();
      if (parseInt(adult) < parseInt(infant)) {
        showDialog(msgAdultCountSmallChhildCount, false, '');
        return;
      }

      var maxPax = parseInt($("#maxPax").val());
      if (maxPax < (parseInt(adult) + parseInt(child))) {
        var msg = msgMaxPaxReservation;
        showDialog(msg.toString().replace('{0}', maxPax.toString()), false, '');
        return;
      }
      var _data = '';
      _data += '{"BookNr":"' + bookNr + '"';
      _data += ',"Adult":"' + adult + '"';
      _data += ',"Child":"' + child + '"';
      _data += ',"Infant":"' + infant + '"';
      _data += ',"_dateFormat":"' + $("#dateFormat").val() + '"}';
      $.ajax({
        async: false,
        type: "POST",
        url: "BookTicketV2.aspx/makeTicket",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#makeTicket").html('');
          if (msg.d != '') {
            $("#makeTicket").html(msg.d);
            $(".formatDate").mask($("#dateMask").val());

            if ($(".mobPhone").length > 0 && $("#mobPhoneMask").val() != '') {
              $(".mobPhone").mask($("#mobPhoneMask").val());
            }
            if ($(".phone").length > 0 && $("#phoneMask").val() != '') {
              $(".phone").mask($("#phoneMask").val());
            }

            cbHandicapChanged();
            getExtrasDiv();
            promotionDiv();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getBookFlightInfo(bookNr) {
      $.ajax({
        type: "POST",
        url: "BookTicketV2.aspx/getBookFlightInfo",
        data: '{"BookNr":"' + bookNr + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#bookFlight").html('');
          $("#makeTicket").html('');
          if (msg.d != '')
            $("#bookFlight").html(msg.d);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function cbHandicapChanged() {
      if ($('#cbHandicap').attr('checked'))
        $("#btnSave").removeAttr("disabled");
      else $("#btnSave").attr("disabled", "true");
    }

    function selectPromomotion() {
      setCustomers();
      var url = 'Promotion.aspx';
      $("#dialog-promoSelect").dialog("open");
      $("#promoSelect").attr("src", url);
    }

    function returnSelectedPromotion(data) {
      if (data == '') {
        $("#dialog-promoSelect").dialog("close");
        btnSaveClick(data, 0);
      }
      else {
        $("#dialog-promoSelect").dialog("close");
        btnSaveClick(data, 0);
      }
    }

    function saveMakeRes(pax) {

      var _buttons =
          [{
            text: btnYes,
            click: function () {
              $(this).dialog('close');
              window.close();
            }
          }, {
            text: btnNo,
            click: function () {
              $(this).dialog('close');
              window.close;
              if (retVal.Promotion) {
                selectPromomotion();
              } else {
                saveReservation('');
              }
            }
          }];

      setCustomers();
      var data = new Object();
      var customerCode = $("#iCustomerCode").length > 0 ? $("#iCustomerCode").val() : '';
      var dosier = $("#iDosier").length > 0 ? $("#iDosier").val() : '';
      data.Honeymoon = $("#cbHoneymoon").length > 0 ? $("#cbHoneymoon")[0].checked : false;
      data.PromoCode = $("#iPromoCode").length > 0 ? $("#iPromoCode").val() : '';
      data.Customers = getCustomers();
      data.Discount = $("#iAgencyDiscount").length > 0 ? $("#iAgencyDiscount").val() : '';
      if ($("#divFixNote").length > 0 && $("#divFixNote").css("display").toLowerCase() == "block") {
        var _fixResNote = $("#iFixNote").length > 0 && $("#iFixNote").val() != '' ? $("#iFixNote").val() : '';
        data.ResNote = _fixResNote != null && _fixResNote != '' ? _fixResNote.toString().replace(/\"/g, '|') : '';
      }
      else {
        data.ResNote = $("#iNote").length > 0 ? $("#iNote").val().replace(/\"/g, "|") : '';
      }
      data.aceCustomerCode = customerCode;
      data.aceDosier = dosier;
      data.Code1 = $("#iCode1").length > 0 ? $("#iCode1").val() : '';
      data.Code2 = $("#iCode2").length > 0 ? $("#iCode2").val() : '';
      data.Code3 = $("#iCode3").length > 0 ? $("#iCode3").val() : '';
      data.Code4 = $("#iCode4").length > 0 ? $("#iCode4").val() : '';
      $.ajax({
        //async: false,
        type: "POST",
        url: "BookTicketV2.aspx/getPromoList",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var retVal = msg.d;
          if (retVal.Err) {
            showMsg(retVal.ErrorMsg);
          }
          else if (retVal.SaleableService) {
            $('<div>' + retVal.SaleableServiceMsg + '</div>').dialog({
              buttons: _buttons
            });
          }
          else if (retVal.Promotion) {
            selectPromomotion();
          }
          else {
            btnSaveClick($("#iPromoCode").length > 0 ? $("#iPromoCode").val() : '', pax);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMsg(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function btnSaveClick(promo, pax) {
      if ($("#divWhereInvoice").css('display') == 'block') {
        if ($("#iInvoiceTo").val() == '' || $("#iInvoiceTo").val() == undefined) {
          showDialog(msgWhereInvoiceTo, false, '');
          return;
        }
      }

      if (reservationSaved) {
        showDialog(lblAlreadyRes, false, "");
        return;
      }
      else
        reservationSaved = true;
      //, string dosier, string agencyDiscount
      var customerCode = $("#iCustomerCode").length > 0 ? $("#iCustomerCode").val() : '';
      var dosier = $("#iDosier").length > 0 ? $("#iDosier").val() : '';
      var agencyDiscount = $("#iAgencyDiscount").length > 0 ? $("#iAgencyDiscount").val() : '';

      var data = new Object();
      data.data = $.json.decode(getCustomers());
      data.Note = ($("#iNote").length > 0 ? $("#iNote").val().replace(/\"/g, "|") : '');
      data.customerCode = customerCode;
      data.dosier = dosier;
      data.agencyDiscount = agencyDiscount;
      data.invoiceTo = $("#iInvoiceTo").val();
      data.saveOption = ($("#iSaveOption").attr('checked') != undefined) ? $("#iSaveOption").attr('checked') : false;
      data.optionTime = $("#withOption").length > 0 && $("#withOption").attr("checked") == 'checked' ? '1' : '0';
      data.logID = $("#logID").length > 0 ? $("#logID").val() : null;
      data.Code1 = $("#iCode1").length > 0 ? $("#iCode1").val() : '';
      data.Code2 = $("#iCode2").length > 0 ? $("#iCode2").val() : '';
      data.Code3 = $("#iCode3").length > 0 ? $("#iCode3").val() : '';
      data.Code4 = $("#iCode4").length > 0 ? $("#iCode4").val() : '';
      data.SelectedPromo = promo;

      $.ajax({
        type: "POST",
        url: "BookTicketV2.aspx/saveReservation",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var retVal = $.json.decode(msg.d)[0];
          if (retVal.ControlOK == true) {
            if (retVal.GotoPaymentPage == true)
              showDialogEndGotoPaymentPage(retVal.Message, retVal.ResNo, retVal.PaymentPageUrl, retVal.DirectPaymentPage);
            else showDialogEndExit(retVal.Message, retVal.ResNo);
          }
          else {
            showDialog(retVal.Message, false, '');
            reservationSaved = false;
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialogEndExit(xhr.responseText, '');
          reservationSaved = false;
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function btnCancelClick() {
      window.close;
      self.parent.cancelMakeRes();
    }

    function showExtServiceDetail(idNo) {
      $("#extraservice" + idNo).hide();
      $("#extraserviceDetail" + idNo).show();
    }

    function hideExtServiceDetail(idNo) {
      $("#extraserviceDetail" + idNo).hide();
      $("#extraservice" + idNo).show();
    }

    function onClickOptionTime() {
      if ($("#withOption").length > 0) {
        if ($("#withOption").attr("checked") == "checked") {
          $("#withOptionMsg").show();
        }
        else {
          $("#withOptionMsg").hide();
        }
      }
    }
    function promotionDiv() {
        $.ajax({
            async: false,
            type: "POST",
            url: "BookTicketV2.aspx/getPromotionDiv",
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d.split(';')[0] == "0") {
                    $("#divPromotion").hide();
                }
                else {
                    $("#divPromotion").show();
                    if (msg.d.split(';')[1] == "1")
                        $("#divPromotionHoneymoon").show();
                    else $("#divPromotionHoneymoon").hide();
                    if (msg.d.split(';')[2] == "1")
                        $("#divPromotionPromotionCode").show();
                    else $("#divPromotionPromotionCode").hide();
                }

            },
            error: function (xhr, msg, e) {
                if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                    showMsg(xhr.responseText);
            },
            statusCode: {
                408: function () {
                    logout();
                }
            }
        });
    }
    $(document).ready(function () {
      $("#dialog-Ssrc").dialog({ autoOpen: false, modal: true, width: 640, height: 550, resizable: true, autoResize: true });
      $("#dialog-resServiceAdd").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
      $("#dialog-resServiceExtAdd").dialog({ autoOpen: false, modal: true, width: 870, height: 525, resizable: true, autoResize: true });
      $("#dialog-promoSelect").dialog({ autoOpen: false, modal: true, width: 820, height: 440, resizable: true, autoResize: true });
    });

    function pageLoad() {
      $.query = $.query.load(location.href);
      var bookNr = $.query.get('BookNr');
      $.ajax({
        type: "POST",
        url: "BookTicketv2.aspx/getFormData",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            $("#dateMask").val(data.dateMask);
            $("#dateFormat").val(data.dateFormat);
            $("#maxPax").val(data.maxPax);
            if (data.mobPhoneMask != '') {
              $("#mobPhoneMask").val(data.mobPhoneMask);
            }
            if (data.phoneMask != '') {
              $("#phoneMask").val(data.phoneMask);
            }
          }
          getBookFlightInfo(bookNr);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showDialog(xhr.responseText, false, '');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

  </script>

</head>
<body onload="javascript:pageLoad();">
  <form id="form1" runat="server">
    <input id="dateMask" type="hidden" value="99/99/9999" />
    <input id="dateFormat" type="hidden" value="dd/MM/yyyy" />
    <input id="maxPax" type="hidden" value="99" />
    <input id="phoneMask" type="hidden" />
    <input id="mobPhoneMask" type="hidden" />
    <div id="divbookTicket">
      <div id="bookFlight">
      </div>
      <br />
      <div id="makeTicket">
      </div>
    </div>
    <div id="dialog-Ssrc" title='<%= GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode") %>'
      style="display: none;">
      <iframe id="Ssrc" runat="server" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
    </div>
    <div id="dialog-searchCust" title='<%= GetGlobalResourceObject("MakeReservation", "searchCustLabel") %>'
      style="display: none;">
      <iframe id="searchCust" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
      <%--<div id="searchCust">
            </div--%>>
    </div>
    <div id="dialog-resServiceAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblService") %>'
      style="display: none;">
      <iframe id="resServiceAdd" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resServiceExtAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblExtService") %>'
      style="display: none;">
      <iframe id="resServiceExtAdd" runat="server" height="100%" width="830px" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-promoSelect" title='<%= GetGlobalResourceObject("MakeReservation", "lblSelectPromotion") %>'
      style="display: none;">
      <iframe id="promoSelect" runat="server" height="400px" width="795px" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resCustomerAddress" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerAddress") %>'
      style="display: none;">
      <iframe id="resCustomerAddress" runat="server" height="490px" width="570px" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-resCustOtherInfo" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo") %>'
      style="display: none;">
      <iframe id="resCustOtherInfo" runat="server" height="400px" width="410px" frameborder="0"
        style="clear: both;"></iframe>
    </div>
    <div id="dialog-message" title="" style="display: none;">
      <p>
        <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
      </p>
    </div>
  </form>
</body>
</html>
