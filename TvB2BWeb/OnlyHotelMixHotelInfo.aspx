﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyHotelMixHotelInfo.aspx.cs" Inherits="OnlyHotelMixHotelInfo"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle","HotelInfo") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/simpletabs_1.3.packed.js" type="text/javascript"></script>

    <link href="CSS/HotelInfo.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function showAlert(msg) {

            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        }

        function showFormData() {
            $.ajax({
                type: "POST",
                url: "OnlyHotelMixHotelInfo.aspx/getFormData",
                data: '{"Hotel":"' + $("#hfHotel").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $("#Raiting").html(msg.d.Raiting);
                        $("#HotelImageUrl").attr("src", msg.d.HotelImageUrl);
                        $("#HotelName").html(msg.d.HotelName);
                        $("#HotelStars").attr("src", msg.d.StarStr);
                        $("#HotelLocation").html(msg.d.HotelLocation);
                        $("#Description").html(msg.d.Description);
                        //Facilities
                        $("#Facilities").html('');
                        var facHtml = '';
                        $.each(msg.d.Facilities, function (i) {
                            facHtml += '<div class="well"><i><img alt="" src="Images/well.png" width="10" height="10"></i>' + this.Id + '</div>';
                        });
                        $("#Facilities").html(facHtml);
                        //Hotel Description
                        $("#HotelDescription").html('');
                        $.each(msg.d.HotelDescription, function (i) {
                            $("#HotelDescription").append('<h5>' + this.Title + '</h5><p>' + this.Description + '</p>');
                        });
                    }
                    return true;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        window.close();
                    }
                }
            });
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            $("#hfHotel").val($.query.get('Hotel'));
            showFormData();
        });

    </script>

</head>
<body class="ui-helper-reset ui-widget">
    <form id="HotelInfoForm" runat="server">
        <input id="hfHotel" type="hidden" />
        <div class="wrapper">
            <div class="header">
                <div class="score-con">
                    <div class="score-box">
                        <span id="Raiting"></span>
                        <span class="rew"><%= GetGlobalResourceObject("OnlyHotelMix", "HotelScore") %></span>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="hotel-photo">
                    <img id="HotelImageUrl" alt="hotel Photo" src="Images/noimage.png" />
                </div>
                <div class="hotelNameLocation">
                    <div id="HotelName" class="hotel-name ml-10">
                    </div>
                    <div id="HotelLocation" class="hotel-loc">
                    </div>
                </div>
                <div class="stars">
                    <img id="HotelStars" src="Images/HotelStars/0Stars.png" alt="" />
                </div>
            </div>
            <div id="Description" class="hotel-desc">
            </div>
            <div class="simpleTabs">
                <ul class="simpleTabsNavigation">
                    <li><a href="#"><%= GetGlobalResourceObject("OnlyHotelMix", "Facilities") %></a></li>
                    <li><a href="#"><%= GetGlobalResourceObject("OnlyHotelMix", "HotelDescription") %></a></li>
                </ul>
                <div class="simpleTabsContent">
                    <div id="Facilities">
                    </div>
                </div>
                <div class="simpleTabsContent">
                    <div id="HotelDescription">
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="dialog-message" title="" style="display: none;">
        <span id="messages" style="font-size: 80%; font-weight: normal;">Message</span>
    </div>
</body>
</html>
