﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyVisa.aspx.cs" Inherits="OnlyVisa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="cache-control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <title>Only Visa</title>

  <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
  <script src="Scripts/json2.js" type="text/javascript"></script>
  <script src="Scripts/mustache.js" type="text/javascript"></script>
  <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>
  <script src="Scripts/checkDate.js" type="text/javascript"></script>
  <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/OnlyVisa.css?v=1" rel="Stylesheet" type="text/css" />

  <style type="text/css">
        </style>

  <script language="javascript" type="text/javascript">

    var reservationSaved = false;

    var culture = '<%= twoLetterISOLanguageName %>';

    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
    var btnGotoResCard = '<%= GetGlobalResourceObject("LibraryResource", "btnGotoResCard") %>';
    var btnPaymentPage = '<%= GetGlobalResourceObject("LibraryResource", "btnPaymentPage") %>';
    var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
    var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';

    var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';

    var NS = document.all;

    function maximize() {
      var maxW = screen.availWidth;
      var maxH = screen.availHeight;
      if (location.href.indexOf('pic') == -1) {
        if (window.opera) { } else {
          top.window.moveTo(0, 0);
          if (document.all) { top.window.resizeTo(maxW, maxH); }
          else
            if (document.layers || document.getElementById) {
              if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                top.window.outerHeight = maxH; top.window.outerWidth = maxW;
              }
            }
        }
      }
    }

    function showDialogEndExit(msg, ResNo) {
      $("#messages").html('');
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        resizable: true,
        autoResize: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
            window.close;
            if (ResNo != '') {
              window.location = 'ResView.aspx?ResNo=' + ResNo;
            }
            else {
              window.location.reload();
            }
          }
        }],
        open: function () {
          //$(this).parent().children().children("a.ui-dialog-titlebar-close").remove();
        },
        closeOnEscape: false
      });
    }

    function showDialogEndGotoPaymentPage(msg, ResNo) {
      $('<div>' + msg + '</div>').dialog({
        buttons: [{
          text: btnGotoResCard,
          click: function () {
            $(this).dialog('close');
            window.close;
            window.location = 'ResView.aspx?ResNo=' + ResNo;
          }
        }]
      });
    }

    function showMessage(msg) {
      $("#messages").html(msg);
      $("#dialog").dialog("destroy");
      $("#dialog-message").dialog({
        modal: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog('close');
          }
        }, {
          text: btnCancel,
          click: function () {
            $(this).dialog('close');
          }
        }]
      });
    }

    function setage(Id, cinDate, _formatDate) {
      try {
        var birthDate = dateValue(_formatDate, $("#iBirthDay" + Id).val());
        if (birthDate == null) {
          $("#iBirthDay" + Id).val('');
          $("#iAge" + Id).val('');
          return;
        }
        var checkIN = dateValue(_formatDate, cinDate);
        if (checkIN == null) return;
        var _minBirthDate = new Date(1, 1, 1910);
        var _birthDate = birthDate;
        var _cinDate = checkIN;
        if (_birthDate < _minBirthDate) {
          $("#iAge" + Id).val('');
          birthDate = '';
          showMsg(InvalidBirthDate);
          return;
        }
        var age = Age(_birthDate, _cinDate);
        $("#iAge" + Id).val(age);
      }
      catch (err) {
        $("#iAge" + Id).val('');
      }
    }

    function getCustomers() {
      var adult = $("#iAdult").val() != '' ? parseInt($("#iAdult").val()) : 0;
      var child = $("#iChild").val() != '' ? parseInt($("#iChild").val()) : 0;
      var infant = $("#iInfant").val() != '' ? parseInt($("#iInfant").val()) : 0;
      var custCount = adult + child + infant;
      var custList = [];
      for (var i = 1; i <= custCount; i++) {
        var cust = new Object();
        cust.SeqNo = $("#iSeqNo" + i).text();
        cust.Title = $("#iTitle" + i).length > 0 ? $("#iTitle" + i).val() : '';
        cust.Surname = $("#iSurname" + i).length > 0 ? $("#iSurname" + i).val() : '';
        cust.SurnameL = $("#iSurnameL" + i).length > 0 ? $("#iSurnameL" + i).val() : '';
        cust.Name = $("#iName" + i).length > 0 ? $("#iName" + i).val() : '';
        cust.NameL = $("#iNameL" + i).length > 0 ? $("#iNameL" + i).val() : '';
        cust.BirtDay = $("#iBirthDay" + i).length > 0 ? $("#iBirthDay" + i).val() : '';
        cust.Age = $("#iAge" + i).length > 0 ? $("#iAge" + i).val() : '';
        cust.IDNo = $("#iIDNo" + i).length > 0 ? $("#iIDNo" + i).val() : '';
        cust.PassSerie = $("#iPassSerie" + i).length > 0 ? $("#iPassSerie" + i).val() : '';
        cust.PassNo = $("#iPassNo" + i).length > 0 ? $("#iPassNo" + i).val() : '';
        cust.PassIssueDate = $("#iPassIssueDate" + i).length > 0 ? $("#iPassIssueDate" + i).val() : '';
        cust.PassExpDate = $("#iPassExpDate" + i).length > 0 ? $("#iPassExpDate" + i).val() : '';
        cust.PassGiven = $("#iPassGiven" + i).length > 0 ? $("#iPassGiven" + i).val() : '';
        cust.Phone = $("#iPhone" + i).length > 0 ? $("#iPhone" + i).val() : '';
        cust.Nation = $("#iNation" + i).length > 0 ? parseInt($("#iNation" + i).val()) : null;
        cust.Nationality = $("#iNationality" + i).length > 0 ? $("#iNationality" + i).val() : null;
        cust.Passport = $("#iHasPassport" + i).length > 0 ? $("#iHasPassport" + i)[0].checked : true;
        cust.Leader = $('input[name=Leader]:checked').val() == cust.SeqNo;
        custList.push(cust);
      }
      return custList;
    }

    function saveReservation() {
      var params = new Object();
      params.Customers = getCustomers();
      params.goAhead = $("#goAhead").val() == '1' ? true : false;
      if (reservationSaved) {
        showMessage(lblAlreadyRes);
        return;
      } else {
        reservationSaved = true;
      }

      $.ajax({
        type: "POST",
        url: "OnlyVisa.aspx/saveReservation",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var retVal = JSON.parse(msg.d)[0];
            if (retVal.ControlOK == true) {
              if (retVal.GotoPaymentPage == true)
                showDialogEndGotoPaymentPage(retVal.Message, retVal.ResNo);
              else if (retVal.GotoReservation)
                showDialogEndExit(retVal.Message, retVal.ResNo);
              else showDialogEndExit(retVal.Message, '');
            }
            else {
              if (retVal.OtherReturnValue == '32') {
                showMessage(retVal.Message);
                $("#goAhead").val('1');
                reservationSaved = false;
              }
              else {
                showMessage(retVal.Message);
                reservationSaved = false;
                $("#goAhead").val('0');
              }
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        complette: function (e, xhr, settings) {
          if (e.status === 200) {

          } else if (e.status === 304) {

          } else if (e.status === 408) {
            logout();
          } else {

          }
        }
      });
    }

    function getVisaService(tmpHtml) {
      $.ajax({
        type: "POST",
        url: "OnlyVisa.aspx/getVisaService",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data);
            $("#services").html(html);
            $("#services").show();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showVisaServices() {
      $.get($("#tmplPath").val() + 'OVVisaServices.html?v=201511131430', function (html) {
        getVisaService(html);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function getCustomerList(tmpHtml) {
      $.ajax({
        type: "POST",
        url: "OnlyVisa.aspx/getCustomers",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;
            var html = Mustache.render(tmpHtml, data);
            $("#customers").html(html);
            $("#customers").show();
            var _dateMask = $("#dateMask").val();
            if ($(".formatDate").length > 0 && $("#dateMask").length > 0) {
              $(".formatDate").mask(_dateMask);
            }
            if ($("#scanPass").val() == '1') {
              //$(".passportScan").show();
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showCustomers() {
      $.get($("#tmplPath").val() + 'OVCustomers.html?v=201511241130', function (html) {
        getCustomerList(html);
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        showAlert("Request Failed: " + err);
      });
    }

    function preReservation() {
      var params = new Object();
      params.Country = parseInt($("#iCountry").val());
      params.Service = $("#iService").val();
      params.CheckIn = $("#iCheckIn").datepicker('getDate') != null ? $("#iCheckIn").datepicker('getDate').getTime() : null;
      params.CheckOut = $("#iCheckOut").datepicker('getDate') != null ? $("#iCheckOut").datepicker('getDate').getTime() : null;
      params.Adult = parseInt($("#iAdult").val());
      params.Child = parseInt($("#iChild").val());
      params.Infant = parseInt($("#iInfant").val());
      $.ajax({
        type: "POST",
        url: "OnlyVisa.aspx/preReservation",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            if (msg.d.ResOK) {
              showCustomers();
              showVisaServices();
              $(".saveReservationBtnDiv").show();
            } else {
              showMessage(msg.d.ErrorMsg);
            }
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function onChangeCountry() {
      var params = new Object();
      params.Country = $("#iCountry").length > 0 && $("#iCountry").val() != '' ? parseInt($("#iCountry").val()) : null;
      $.ajax({
        type: "POST",
        url: "OnlyVisa.aspx/getServiceList",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            $("#iService").html('');
            $("#iService").append('<option value="">' + ComboSelect + '</option>');
            $.each(msg.d.ServiceList, function (i) {
              $("#iService").append('<option value="' + this.Code + '">' + this.Name + '</option>');
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getFormData() {
      $.ajax({
        type: "POST",
        url: "OnlyVisa.aspx/getFormData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            $("#iCountry").html('');
            $("#iCountry").append('<option value="">' + ComboSelect + '</option>');
            if (msg.d.VisaCountry.length > 0) {
              $.each(msg.d.VisaCountry, function (i) {
                $("#iCountry").append('<option value="' + this.RecID + '">' + this.Name + '</option>');
              });
            }
            $("#iCheckIn").datepicker("setDate", new Date(msg.d.CheckIn));
            $("#iCheckOut").datepicker("setDate", new Date(msg.d.CheckOut));
            $("#iAdult").val(msg.d.Adult);
            $("#iChild").val(msg.d.Child);
            $("#iInfant").val(msg.d.Infant);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(function () {
      $.datepicker.setDefaults($.datepicker.regional[culture != 'en' ? culture : '']);

      $("#iCheckIn").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {

          }
        },
        minDate: new Date()
      });

      $("#iCheckOut").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {

          }
        },
        minDate: new Date()
      });

      getFormData();

    });
  </script>

</head>
<body>
  <form id="OnlyVisaForm" runat="server">
    <div class="Page">
      <div>
        <tv1:Header ID="tvHeader" runat="server" />
        <tv1:MainMenu ID="tvMenu" runat="server" />
      </div>
      <div class="Content">
        <div class="ui-helper-clearfix OnlyVisaHeader">
          <div class="ui-helper-clearfix country">
            <div class="label">
              <span><%= GetGlobalResourceObject("Controls", "viewCountry") %> : </span>
            </div>
            <div class="input">
              <select id="iCountry" class="combo" onchange="onChangeCountry()">
              </select>
            </div>
          </div>
          <div class="ui-helper-clearfix services">
            <div class="label">
              <span><%= GetGlobalResourceObject("Controls", "viewVisa") %> : </span>
            </div>
            <div class="input">
              <select id="iService" class="combo">
              </select>
            </div>
          </div>
          <div class="ui-helper-clearfix dates">
            <div class="label">
              <span><%= GetGlobalResourceObject("Controls", "viewBeginEndDate") %> : </span>
            </div>
            <div class="input">
              <div class="checkIn">
                <input id="iCheckIn" type="text" />
              </div>
              <div class="sperator">&nbsp;-&nbsp;</div>
              <div class="checkOut">
                <input id="iCheckOut" type="text" />
              </div>
            </div>
          </div>
          <div class="ui-helper-clearfix customers">
            <div class="adultLabel">
              <span><%= GetGlobalResourceObject("Controls", "viewAdult") %> : </span>
            </div>
            <div class="adultInput">
              <input id="iAdult" type="text" />
            </div>
            <div class="childLabel">
              <span><%= GetGlobalResourceObject("Controls", "viewChild") %> (1.99->) : </span>
            </div>
            <div class="childInput">
              <input id="iChild" type="text" />
            </div>
            <div class="infantLabel">
              <span><%= GetGlobalResourceObject("Controls", "viewChild") %> (0.00-1.99) : </span>
            </div>
            <div class="childInput">
              <input id="iInfant" type="text" />
            </div>
          </div>
          <div class="makeReservation">
            <input type="button" value="Create Reservation" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" onclick="preReservation()" />
          </div>
        </div>
        <div class="ui-helper-clearfix OVForm">
          <div id="services" class="ui-helper-clearfix VisaService">
          </div>
          <div id="customers" class="ui-helper-clearfix Customers">
          </div>
        </div>
        <div class="ui-helper-hidden saveReservationBtnDiv">
          <input type="button" value="Save reservation" onclick="saveReservation()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
        </div>
      </div>
    </div>
    <div class="ui-helper-hidden">
      <asp:HiddenField ID="tmplPath" runat="server" />
      <input id="goAhead" type="hidden" value="0" />
    </div>
  </form>
  <div id="dialog-message" title="" class="ui-helper-hidden">
    <span id="messages"></span>
  </div>
</body>
</html>
