﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Services;
using System.Threading;
using System.Globalization;
using TvBo;
using TvTools;
using System.Collections;

public partial class PackageSearchResultV2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        if (!Page.IsPostBack)
            HttpContext.Current.Session["SearchResultOH"] = null;
        string _tmpPath = WebRoot.BasePageRoot + "Data/" + new UICommon().getWebID() + "/" + UserData.Market + "/";
        tmplPath.Value = _tmpPath;
    }

    [WebMethod(EnableSession = true)]
    public static string setResultFilter(string FieldName, string Value)
    {
        /*
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["Criteria"] == null) return null;
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];

        if (HttpContext.Current.Session["SearchResult"] == null) return null;

        TvBo.MultiRoomResult searchResult = (TvBo.MultiRoomResult)HttpContext.Current.Session["SearchResult"];
        searchResult.SearchType = criteria.SType;
        ResultFilter filter = new ResultFilter();
        switch (FieldName)
        {
            case "ArrCity": filter.ArrCity = Conversion.getInt32OrNull(Value); break;
            case "HotelLocation": filter.HotelLocation = Conversion.getInt32OrNull(Value); break;
            case "HotCat": filter.Category = Conversion.getStrOrNull(Value); break;
            case "Hotel": filter.Hotel = Conversion.getStrOrNull(Value); break;
            case "CheckIn": filter.CheckIn = Conversion.ConvertFromJulian(Conversion.getInt64OrNull(Value)); break;
            case "Night": filter.Night = Conversion.getInt16OrNull(Value); break;
            case "Room": filter.Room = Conversion.getStrOrNull(Value); break;
            case "Board": filter.Board = Conversion.getStrOrNull(Value); break;
        }
        filter.filtered = filter.ArrCity.HasValue ||
                          (!string.IsNullOrEmpty(filter.Board)) ||
                          (!string.IsNullOrEmpty(filter.Category)) ||
                          filter.CheckIn.HasValue ||
                          filter.DepCity.HasValue ||
                          (!string.IsNullOrEmpty(filter.Hotel)) ||
                          filter.HotelLocation.HasValue ||
                          filter.Night.HasValue ||
                          (!string.IsNullOrEmpty(filter.Room));
        searchResult.FilterResult = filter;
        searchResult.FilteredResultSearch = new List<SearchResult>();
        searchResult.PageRowCount = 20;
        HttpContext.Current.Session["SearchResult"] = searchResult;

        return getResultGrid(0, 20, true, false);
        */
        return null;
    }

    public static OnlyHotelResultFilter createResultFilter(User UserData, List<HotelGroup> result)
    {
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        OnlyHotelResultFilter filter = new OnlyHotelResultFilter();        
        filter.showDeparture = false;
        filter.Arrival = new List<FilterItem>();
        //var Arrival = from q in result
        //              group q by new {  } into k
        //              select new
        //              {
        //                  Code = k.Key.ArrCity,
        //                  Name = result.ResultSearch.Where(w => w.ArrCity == k.Key.ArrCity).FirstOrDefault().ArrCityName,
        //                  NameL = result.ResultSearch.Where(w => w.ArrCity == k.Key.ArrCity).FirstOrDefault().ArrCityNameL
        //              };
        /*
        List<resultFilterCoor> filterList = new List<resultFilterCoor>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        ResultFilter filter = result.FilterResult;
        List<SearchResult> filterdata = new List<SearchResult>();
        if (result.FilterResult.filtered)
            filterdata = result.FilteredResultSearch;
        else filterdata = result.ResultSearch;
        var Departure = from q in ((result.FilterResult.filtered || !result.FilterResult.DepCity.HasValue) ? filterdata : result.FilteredResultSearch)
                        group q by new { q.DepCity } into k
                        select new
                        {
                            Code = k.Key.DepCity,
                            Name = result.ResultSearch.Where(w => w.DepCity == k.Key.DepCity).FirstOrDefault().DepCityName,
                            NameL = result.ResultSearch.Where(w => w.DepCity == k.Key.DepCity).FirstOrDefault().DepCityNameL
                        };
        if ((Departure != null && Departure.Count() > 1) || result.FilterResult.DepCity.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Departure.OrderBy(o => (useLocalName ? o.NameL : o.Name)))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.DepCity.HasValue && filter.DepCity.Value == row.Code ? "selected=\"selected\"" : "");

            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfDeparture\" style=\"width:95%;\" onchange=\"filterResult('DepCity', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblDepCity"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfDeparture", Type = "string", FieldName = "DepCity", Value = filterValue, Width = 205 });
        }

        var Arrival = from q in ((result.FilterResult.filtered || !result.FilterResult.ArrCity.HasValue) ? filterdata : result.FilteredResultSearch)
                      group q by new { q.ArrCity } into k
                      select new
                      {
                          Code = k.Key.ArrCity,
                          Name = result.ResultSearch.Where(w => w.ArrCity == k.Key.ArrCity).FirstOrDefault().ArrCityName,
                          NameL = result.ResultSearch.Where(w => w.ArrCity == k.Key.ArrCity).FirstOrDefault().ArrCityNameL
                      };
        if ((Arrival != null && Arrival.Count() > 1) || result.FilterResult.ArrCity.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Arrival.OrderBy(o => (useLocalName ? o.NameL : o.Name)))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.ArrCity.HasValue && filter.ArrCity.Value == row.Code ? "selected=\"selected\"" : "");

            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfDeparture\" style=\"width:95%;\" onchange=\"filterResult('ArrCity', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblArrCity"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfArrival", Type = "string", FieldName = "ArrCity", Value = filterValue, Width = 205 });
        }

        var HotelLocattion = from q in (result.FilterResult.filtered || !result.FilterResult.HotelLocation.HasValue ? filterdata : result.FilteredResultSearch)
                             group q by new
                             {
                                 HotelLocation = q.HotelLocation,
                                 HolPackName = useLocalName ? q.HolPackNameL : q.HotLocationName
                             } into k
                             select new
                             {
                                 Code = k.Key.HotelLocation,
                                 Name = result.ResultSearch.Where(w => w.HotelLocation == k.Key.HotelLocation).FirstOrDefault().HotLocationName,
                                 NameL = result.ResultSearch.Where(w => w.HotelLocation == k.Key.HotelLocation).FirstOrDefault().HotLocationNameL
                             };
        if ((HotelLocattion != null && HotelLocattion.Count() > 1) || result.FilterResult.HotelLocation.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in HotelLocattion.OrderBy(o => useLocalName ? o.NameL : o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.HotelLocation.HasValue && filter.HotelLocation.Value == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfHotelLocation\" style=\"width:97%;\" onchange=\"filterResult('HotelLocation', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblResort"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfHotelLocation", Type = "int", FieldName = "HotelLocation", Value = filterValue, Width = 205 });
        }

        var Category = from q in ((result.FilterResult.filtered || string.IsNullOrEmpty(result.FilterResult.Category)) ? filterdata : result.FilteredResultSearch)
                       group q by new { q.HotCat } into k
                       select new { k.Key.HotCat };
        if ((Category != null && Category.Count() > 1) || !string.IsNullOrEmpty(result.FilterResult.Category))
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Category.OrderBy(o => o.HotCat))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.HotCat, row.HotCat, filter.Category == row.HotCat ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfCategory\" style=\"width:97%;\" onchange=\"filterResult('HotCat', this.value);\">{1}</select></div>",
                                        HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblCategory"),
                                        selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfCategory", Type = "string", FieldName = "HotCat", Value = filterValue, Width = 205 });
        }

        var Hotel = from q in ((result.FilterResult.filtered || string.IsNullOrEmpty(result.FilterResult.Hotel)) ? filterdata : result.FilteredResultSearch)
                    group q by new { q.Hotel } into k
                    select new
                    {
                        Code = k.Key.Hotel,
                        Name = result.ResultSearch.Where(w => w.Hotel == k.Key.Hotel).FirstOrDefault().HotelName,
                        NameL = result.ResultSearch.Where(w => w.Hotel == k.Key.Hotel).FirstOrDefault().HotelNameL
                    };
        if ((Hotel != null && Hotel.Count() > 1) || !string.IsNullOrEmpty(result.FilterResult.Hotel))
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Hotel.OrderBy(o => useLocalName ? o.NameL : o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.Hotel == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:305px; float: left;\"><strong>{0}</strong><br /><select id=\"rfHotel\" style=\"width:97%;\" onchange=\"filterResult('Hotel', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblHotel"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfHotel", Type = "string", FieldName = "Hotel", Value = filterValue, Width = 305 });
        }

        var CheckIn = from q in ((result.FilterResult.filtered || !result.FilterResult.CheckIn.HasValue) ? filterdata : result.FilteredResultSearch)
                      group q by new { q.CheckIn } into k
                      select new { CheckIn = k.Key.CheckIn, CheckInJul = Conversion.ConvertToJulian(k.Key.CheckIn) };
        if ((CheckIn != null && CheckIn.Count() > 1) || result.FilterResult.CheckIn.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in CheckIn.OrderBy(o => o.CheckIn))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>",
                                    row.CheckInJul,
                                    row.CheckIn.Value.ToShortDateString(),
                                    filter.CheckIn.HasValue && filter.CheckIn.Value == row.CheckIn ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:145px; float: left;\"><strong>{0}</strong><br /><select id=\"rfCheckIn\" style=\"width:97%;\" onchange=\"filterResult('CheckIn', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblCheckIn"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfCheckIn", Type = "DateTime", FieldName = "CheckIn", Value = filterValue, Width = 145 });
        }

        var Night = from q in ((result.FilterResult.filtered || !result.FilterResult.Night.HasValue) ? filterdata : result.FilteredResultSearch)
                    group q by new { q.Night } into k
                    select new { k.Key.Night };
        if ((Night != null && Night.Count() > 1) || result.FilterResult.Night.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Night.OrderBy(o => o.Night))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Night, row.Night, filter.Night.HasValue && filter.Night.Value == row.Night ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:65px; float: left;\"><strong>{0}</strong><br /><select id=\"rfNight\" style=\"width:97%;\" onchange=\"filterResult('Night', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblNights"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfNight", Type = "int", FieldName = "Night", Value = filterValue, Width = 65 });
        }

        var Room = from q in ((result.FilterResult.filtered || string.IsNullOrEmpty(result.FilterResult.Room)) ? filterdata : result.FilteredResultSearch)
                   group q by new { q.Room } into k
                   select new
                   {
                       Code = k.Key.Room,
                       Name = result.ResultSearch.Where(w => w.Room == k.Key.Room).FirstOrDefault().RoomName,
                       NameL = result.ResultSearch.Where(w => w.Room == k.Key.Room).FirstOrDefault().RoomNameL
                   };
        if ((Room != null && Room.Count() > 1) || !string.IsNullOrEmpty(result.FilterResult.Room))
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Room.OrderBy(o => useLocalName ? o.NameL : o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.Room == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfRoom\" style=\"width:97%;\" onchange=\"filterResult('Room', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblRoom"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfRoom", Type = "string", FieldName = "Room", Value = filterValue, Width = 205 });
        }

        var Board = from q in ((result.FilterResult.filtered || string.IsNullOrEmpty(result.FilterResult.Board)) ? filterdata : result.FilteredResultSearch)
                    group q by new { q.Board } into k
                    select new
                    {
                        Code = k.Key.Board,
                        Name = result.ResultSearch.Where(w => w.Board == k.Key.Board).FirstOrDefault().BoardName,
                        NameL = result.ResultSearch.Where(w => w.Board == k.Key.Board).FirstOrDefault().BoardNameL
                    };
        if ((Board != null && Board.Count() > 1) || !string.IsNullOrEmpty(result.FilterResult.Board))
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Board.OrderBy(o => useLocalName ? o.NameL : o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.Board == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfBoard\" style=\"width:97%;\" onchange=\"filterResult('Board', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblBoard"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfBoard", Type = "string", FieldName = "Board", Value = filterValue, Width = 205 });
        }

        StringBuilder sb = new StringBuilder();
        int len = 0;
        string divRow = string.Empty;
        string divRowOuther = "<div class=\"clearfix\">";
        foreach (resultFilterCoor row in filterList)
        {
            len += row.Width.Value;
            if (len > 728)
            {
                len = 0;
                len += row.Width.Value;
                sb.Append(divRowOuther);
                sb.Append(divRow);
                sb.Append("</div>");
                divRow = string.Empty;
            }
            divRow += row.Value;
        }

        sb.Append(divRowOuther);
        sb.Append(divRow);
        sb.Append("</div>");

        sb.Insert(0, "<div style=\"width:730px;\">");
        sb.Append("</div>");
        return sb.ToString();
       */
        return null;
    }

    [WebMethod(EnableSession = true)]
    public static object getResultGrid(int page, int? pageItemCount, bool? filtered, bool? paged)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (HttpContext.Current.Session["Criteria"] == null) return null;
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];

        List<SearchResultOH> priceList = new OnlyHotelSearch().getOnlyHotelPriceSearch(UserData, criteria, null, ref errorMsg);
        List<HotelGroup> result = new OnlyHotelSearch().getOnlyHotelPriceGroup(UserData, criteria, priceList, useLocalName, false, ref errorMsg);

        if (result.Count > 0)
            HttpContext.Current.Session["SearchResultOH"] = priceList;

        #region Search Note
        string searchTextHtml = string.Empty;
        string searchTextRTF = new Search().getSearchText(UserData, ref errorMsg);
        if (!string.IsNullOrEmpty(searchTextRTF))
        {
            SautinSoft.RtfToHtml rtfHtml = new SautinSoft.RtfToHtml();
            rtfHtml.Encoding = SautinSoft.eEncoding.UTF_8;
            rtfHtml.OutputFormat = SautinSoft.eOutputFormat.HTML_401;
            rtfHtml.HtmlParts = SautinSoft.eHtmlParts.Html_body;
            searchTextHtml = rtfHtml.ConvertString(searchTextRTF);
        }
        #endregion

        OnlyHotelResultFilter resultFilter = new OnlyHotelResultFilter();
        resultFilter = createResultFilter(UserData, result);

        return new
        {
            ResultNote = !string.IsNullOrEmpty(searchTextHtml),
            SearchResultNote = searchTextHtml,
            ResultFilter = true,
            SearchResultFilter = /*resultFilter*/"",
            ResultOffer = true,
            btnCreateOffer = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "btnCreateOffer").ToString(),
            btnClearOffer = HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "btnClearOffer").ToString(),
            HotelGroup = result,
            NumberFormat = UserData.Ci.NumberFormat
        };
    }

    [WebMethod(EnableSession = true)]
    public static string getAllotmentControl(string RefNoList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<SearchResultOH> searchResult = (List<SearchResultOH>)HttpContext.Current.Session["SearchResultOH"];
        List<SearchResultOH> _bookList = new List<SearchResultOH>();
        SearchCriteria _criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];

        for (int i = 0; i < RefNoList.Split(',').Length; i++)
        {
            int refNo = Convert.ToInt32(RefNoList.Split(',')[i]);
            SearchResultOH bookRow = searchResult.Find(f => f.RefNo == refNo);
            _bookList.Add(bookRow);
        }

        string errorMsg = string.Empty;

        if (new Reservation().priceSearchOHAllotControl(UserData, _bookList, ref errorMsg))
            return string.Empty;
        else return errorMsg;
    }

    [WebMethod(EnableSession = true)]
    public static string addOffer(string BookList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<SearchResultOH> searchResult = (List<SearchResultOH>)HttpContext.Current.Session["SearchResultOH"];
        List<SearchResultOH> _bookList = new List<SearchResultOH>();
        for (int i = 0; i < BookList.Split(',').Length; i++)
        {
            int refNo = Convert.ToInt32(BookList.Split(',')[i]);
            SearchResultOH bookRow = searchResult.Find(f => f.PriceID == refNo);

            _bookList.Add(bookRow);
        }
        List<SearchResultOH> offerList = new List<SearchResultOH>();
        if (HttpContext.Current.Session["OfferList"] != null)
            offerList = (List<SearchResultOH>)HttpContext.Current.Session["OfferList"];
        foreach (SearchResultOH row in _bookList)
            offerList.Add(row);
        HttpContext.Current.Session["OfferList"] = offerList;
        return "";
    }
}

