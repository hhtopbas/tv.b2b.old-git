﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;
using System.Configuration;

public partial class OnlyHotelMixSearchFilter : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
        //ppcCheckIn.Culture = ci.Name + " " + ci.EnglishName;
        //ppcCheckIn.Format = ci.DateTimeFormat.ShortDatePattern.Replace(ci.DateTimeFormat.DateSeparator[0], ' ');
        //ppcCheckIn.From.Date = DateTime.Today;
        //if (!IsPostBack)
        //    ppcCheckIn.DateValue = DateTime.Today.Date;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static bool CreateCriteria()
    {
        if (HttpContext.Current.Session["UserData"] == null)
        {
            HttpContext.Current.Response.StatusCode = 408; return false;
        }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;

        bool CurControl = false;
        object curControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur");
        if (curControl != null) CurControl = (bool)curControl;

        bool ExtAllotCont = false;
        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        if (extAllotControl != null) ExtAllotCont = (bool)extAllotControl;

        bool ShowExpandDay = true;
        object showExpandDay = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowExpandDay");
        if (showExpandDay != null) ShowExpandDay = (bool)showExpandDay;

        bool ShowSecondDay = true;
        object showSecondDay = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowSecondDay");
        if (showSecondDay != null) ShowSecondDay = (bool)showSecondDay;

        MixSearchCriteria criteria = new MixSearchCriteria
        {
            SType = SearchType.OnlyHotelMixSearch,
            CurrentCurrency = UserData.SaleCur,
            ExpandDay = 0,
            CheckIn = DateTime.Today,
            UseBoardGroup = GroupSearch,
            UseRoomGroup = GroupSearch,
            CurrencyControl = CurControl,
            UseExternalAllotControl = ExtAllotCont,
            ShowExpandDay = ShowExpandDay,
            ShowSecondDay = ShowSecondDay
        };
        HttpContext.Current.Session["MixCriteria"] = criteria;
        return true;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static MixSearchFilterData getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["SearchMixData"] == null)
            getSearchMixFilterData();
        if (HttpContext.Current.Session["SearchMixData"] == null)
            return null;

        CreateCriteria();
        MixSearchFilterCacheData cacheData = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];
        MixSearchCriteria criteria = (MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];

        MixSearchFilterData data = new MixSearchFilterData();

        Int16? defaultNightLength = Conversion.getInt16OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DefaultNightLength"));
        Int16 DefaultNightLength = defaultNightLength.HasValue ? defaultNightLength.Value : Convert.ToInt16(21);

        Int16? defaultNight = Conversion.getInt16OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DefaultDays"));
        Int16 DefaultNight = defaultNight.HasValue ? defaultNight.Value : Convert.ToInt16(7);

        Int16? defaultNight2 = Conversion.getInt16OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DefaultDays2"));
        Int16 DefaultNight2 = defaultNight2.HasValue ? defaultNight2.Value : Convert.ToInt16(7);

        List<Int32> fltCheckInDay = new List<Int32>();
        string lblDay = Conversion.getStrOrNull(HttpContext.GetGlobalResourceObject("LibraryResource", "lblDay"));
        //if (string.Equals(UserData.CustomRegID, Common.ctID_BTBTour))
        //    criteria.MaxExpandDayForPriceSearch = 30;
        for (int i = 0; i <= criteria.MaxExpandDayForPriceSearch.Value; i++) fltCheckInDay.Add(i);

        data.fltCheckInDay = new fltComboDayRecord
        {
            maxDayList = fltCheckInDay,
            selectedDay = 0
        };

        List<Int32> fltNight = new List<Int32>();
        List<Int32> fltNight2 = new List<Int32>();
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ||
            Equals(UserData.CustomRegID, Common.crID_Rezeda) ||
            //Equals(UserData.CustomRegID, Common.crID_Calypso) ||
            Equals(UserData.CustomRegID, Common.crID_Dallas))
        {
            List<Int16> nights = new TvBo.Common().getPLNights(UserData.Market, SearchType.OnlyHotelSearch, ref errorMsg);
            if (nights == null || nights.Count < 1)
            {
                fltNight.Add((Int32)7);
                fltNight2.Add((Int32)7);
            }
            else
            {
                foreach (Int16 row in nights) fltNight.Add((Int32)row);
                foreach (Int16 row in nights) fltNight2.Add((Int32)row);
            }
        }
        else
        {
            for (int i = 1; i < DefaultNightLength; i++) fltNight.Add((Int32)i);
            for (int i = 1; i < DefaultNightLength; i++) fltNight2.Add((Int32)i);
        }

        data.fltNight = new fltComboDayRecord
        {
            maxDayList = fltNight,
            selectedDay = defaultNight.HasValue ? defaultNight.Value : (Int16)7
        };
        data.fltNight2 = new fltComboDayRecord
        {
            maxDayList = fltNight2,
            selectedDay = defaultNight2.HasValue ? defaultNight2.Value : (Int16)7
        };
        List<Int32> fltRoomCount = new List<Int32>();
        for (int i = 1; i <= (UserData.AgencyRec.MaxRoomCnt.HasValue ? UserData.AgencyRec.MaxRoomCnt.Value : UserData.MaxRoomCount); i++)
            fltRoomCount.Add(i);
        data.fltRoomCount = new fltComboDayRecord
        {
            maxDayList = fltRoomCount,
            selectedDay = 1
        };

        fltRoomCount = new List<Int32>();
        for (int i = 1; i <= 4; i++)
            fltRoomCount.Add(i);
        data.fltMixRoomCount = new fltComboDayRecord
        {
            maxDayList = fltRoomCount,
            selectedDay = 1
        };

        bool? currencyConvert = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur"));

        data.CurrencyConvert = currencyConvert.HasValue ? currencyConvert.Value : true;
        List<ReadyCurrencyRecord> getCurrencyList = new TvBo.Common().getReadyCurrency(UserData.Market, UserData.SaleCur, ref errorMsg);
        string curList = string.Empty;
        curList += "<select id=\"currentCur\">";
        foreach (ReadyCurrencyRecord row in getCurrencyList)
        {
            curList += string.Format("<option value=\"{0}\" {2} >{1}</option>",
                                        row.Code,
                                        row.NameL,
                                        Equals(UserData.SaleCur, row.Code) ? "selected=\"selected\"" : "");
        }
        curList += "</select>";
        data.MarketCur = string.Format("<input id=\"CurrencyConvert\" type=\"checkbox\" {0} /><label for=\"CurrencyConvert\">{1}</label>&nbsp;{2}",
                data.CurrencyConvert ? "checked='checked'" : "",
                HttpContext.GetGlobalResourceObject("LibraryResource", "lblShowInPrice2"),
                curList);
        string paxNationality = string.Empty;
        GetUserInformationResponse paxUserInfo = new MixSearchPaximum().getUserInformation(UserData, ref errorMsg);
        if (paxUserInfo != null)
        {
            curList = string.Empty;
            curList += "<select id=\"paxCurrentCur\">";
            foreach (var row in paxUserInfo.WorkingCurrencies)
            {
                curList += string.Format("<option value=\"{0}\">{1}</option>",
                                            row,
                                            row);
            }
            curList += "</select>";
            paxNationality += "<select id=\"fltPaxNationality\">";
            if (!(paxUserInfo.AccountNationalities.Count() == 1 && paxUserInfo.AccountNationalities[0].IsDefault))
            {
                paxNationality += string.Format("<option value=\"\">{0}</option>", HttpContext.GetGlobalResourceObject("LibraryResource", "ComboSelect"));
            }

            foreach (var row in paxUserInfo.AccountNationalities.Where(w => !string.IsNullOrEmpty(w.Name)).OrderBy(o => o.Name))
            {
                paxNationality += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Id, row.Name, row.IsDefault ? "selected=\"selected\"" : "");
            }

            paxNationality += "</select>";
        }
        else
        {
            curList = string.Empty;
            curList += "<select id=\"paxCurrentCur\">";
            curList += string.Format("<option value=\"{0}\">{1}</option>",
                                        "EUR",
                                        "EUR");

            curList += "</select>";
        }
        data.PaxCurrency = string.Format("<input id=\"CurrencyConvert\" type=\"checkbox\" {0} disabled=\"disabled\"/><label for=\"CurrencyConvert\">{1}</label>&nbsp;{2}",
                    "checked='checked'",
                    HttpContext.GetGlobalResourceObject("LibraryResource", "lblShowInPrice2"),
                    curList);

        data.PaxNationality = string.Format("<strong>{0}</strong><br />{1}", HttpContext.GetGlobalResourceObject("MakeReservation", "resCustCitizenship"), paxNationality);
        bool? showFilterPackage = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowFilterPackageOnlyHotel"));
        if (!showFilterPackage.HasValue)
            showFilterPackage = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowFilterPackage"));
        data.ShowFilterPackage = !showFilterPackage.HasValue || (showFilterPackage.HasValue && showFilterPackage.Value == true);

        DateTime ppcCheckIn = DateTime.Today.Date;
        Int64 days = (ppcCheckIn - (new DateTime(1970, 1, 1))).Days - 1;

        data.CheckIn = ppcCheckIn;
        data.ShowStopSaleHotels = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda); // true;
        data.CustomRegID = UserData.CustomRegID;
        data.ShowExpandDay = criteria.ShowExpandDay;
        data.ShowSecondDay = criteria.ShowSecondDay;
        data.CustomRegID = UserData.CustomRegID;
        data.ShowOfferBtn = string.Equals(Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2");
        bool? disableRoomFilter = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DisableRoomFilter"));
        data.DisableRoomFilter = disableRoomFilter.HasValue ? disableRoomFilter.Value : false;
        var countryQ = from q in cacheData.CountryList
                       select new { value = q.Name, item = q.Code + "|" + q.CountryType.ToString() };
        data.fltCountry = countryQ;

        bool? availableHotel = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "PaximumAvailableHotel"));
        data.AvailableHotel = availableHotel.HasValue ? availableHotel.Value : false;

        bool? showAvailableHotel = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowPaximumAvailableHotel"));
        data.ShowAvailableHotel = showAvailableHotel.HasValue ? showAvailableHotel.Value : true;

        return data;
    }

    public static bool getSearchMixFilterData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return false; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["SearchMixData"] == null)
        {
            bool GroupSearch = false;
            object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
            if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
            MixSearchFilterCacheData filterData = new MixSearchFilterCacheData();
            filterData = MixSearch.getMixPackageSearchData(UserData, GroupSearch, SearchType.OnlyHotelMixSearch);
            HttpContext.Current.Session["SearchMixData"] = filterData;
        }
        return true;
    }

    [WebMethod(EnableSession = true)]
    public static string getHolPackCat()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CodeName> holPackCat = new MixSearch().getMixSearchWithOutCTHolPackCat(useLocalName, data, ref errorMsg);
        if (holPackCat.Count > 0)
            return Newtonsoft.Json.JsonConvert.SerializeObject(holPackCat);
        else return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getCountry()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];

        return data.CountryList.OrderBy(o => o.Name);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getPaxLocation(string CountryId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        GetDestinationListResponse paxLocationList = new GetDestinationListResponse();

        if (!CacheHelper.Get("PaxLocation_" + CountryId, out paxLocationList))
            paxLocationList = new MixSearchPaximum().getDestinationList(UserData, CountryId, ref errorMsg);

        //StringBuilder sb = new StringBuilder();
        //StringBuilder sbTopRegion = new StringBuilder();
        //StringBuilder sbRegion = new StringBuilder();

        //sb.AppendLine("<input type=\"text\" id=\"arrCityAutoComplette\"  onkeyup=\"autoCompletteChange(this.value);\" ><br />");

        var returnValue = from q in paxLocationList.DestinationList
                              //group q by new { q.Id, q.Name } into k
                          orderby q.Name
                          select new
                          {
                              Code = q.Id,
                              Name = q.Name,
                              TopRegion = string.IsNullOrEmpty(q.ParentName) ? "1" : "0",
                              Region = q.ParentName

                          };
        /*
        int index = 0;
        foreach (Destination row in paxLocationList.DestinationList.OrderBy(o => o.Name))
        {
            index++;
            string rowStr = string.Format("<div class=\"divListPopupRow\" name=\"listPopupRow\" id=\"divListPopupRow_{0}\" ><input name=\"fltPaxLocations\" id=\"paxlocation_{0}\" type=\"radio\" value=\"{1}\" /><label for=\"paxlocation_{0}\" class=\"arrCityAutoComp\" ><b>{2}</b></label></div>", index, row.Id, row.Name);

            if (string.IsNullOrEmpty(row.ParentName))
            {
                sbTopRegion.AppendLine(rowStr);
            }
            else
            {
                sbRegion.AppendLine(rowStr);
            }
        }

        sb.AppendLine("<div class=\"divListPopupRow mainCountries\"><h4>Top Region</h4></div>");
        sb.AppendLine(sbTopRegion.ToString());
        sb.AppendLine("<div class=\"divListPopupRow otherCountries\"><h4>City</h4></div>");
        sb.AppendLine(sbRegion.ToString());

        return sb.ToString();
        */

        return returnValue;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getPaxLocationCache(string CountryId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        if (!CacheHelper.Exists("CountryId.Split('|').Length > 1 ? CountryId.Split('|')[0] : CountryId"))
        {
            GetDestinationListResponse paxLocationList = new MixSearchPaximum().getDestinationList(UserData, CountryId.Split('|').Length > 1 ? CountryId.Split('|')[0] : CountryId, ref errorMsg);
            if (paxLocationList != null && paxLocationList.DestinationList.Count() > 0)
            {
                object paxLocForCount = CacheObjects.getPaxLocationForCountry(UserData, CountryId.Split('|').Length > 1 ? CountryId.Split('|')[0] : CountryId, paxLocationList);
                return Equals(paxLocationList, paxLocForCount);
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getArrival(string Country)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32? depCity = null;//Conversion.getInt32OrNull(DepCity);                
        List<Location> countryList = new Locations().getLocationList(UserData, LocationType.Country, null, null, null, null, ref errorMsg);
        List<MixCountry> countryMixList = new MixSearch().getCountryList(UserData, ref errorMsg);

        List<Location> cL = (from q1 in countryList
                             join q2 in countryMixList on q1.CountryCode equals q2.ISO3
                             where q2.ISO2 == Country
                             select q1).ToList<Location>();

        Location countryRec = cL != null && cL.Count == 1 ? cL.FirstOrDefault() : null;
        Int32? country = countryRec != null ? (int?)countryRec.RecID : null;
        MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<MixPlLocation> arrCityData = new MixSearch().getMixSearchArrCitys(useLocalName, data, depCity, ref errorMsg);
        var retVal = from q in arrCityData
                     where !q.HolPackCategory.HasValue &&
                           (!country.HasValue || q.ArrCountry == country)
                     orderby q.ArrCountryName, q.ArrCityName
                     group q by new { country = q.ArrCountryName, RecID = q.ArrCity, Name = q.ArrCityName } into k
                     select new { Country = k.Key.country, RecID = k.Key.RecID, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    [WebMethod(EnableSession = true)]
    public static string getHolpack(string ArrCity, string CountryId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        string errorMsg = string.Empty;
        List<Location> countryList = new Locations().getLocationList(UserData, LocationType.Country, null, null, null, null, ref errorMsg);
        List<MixCountry> countryMixList = new MixSearch().getCountryList(UserData, ref errorMsg);

        List<Location> cL = (from q1 in countryList
                             join q2 in countryMixList on q1.CountryCode equals q2.ISO3
                             where q2.ISO2 == CountryId
                             select q1).ToList<Location>();

        Location countryRec = cL != null && cL.Count == 1 ? cL.FirstOrDefault() : null;
        Int32? country = countryRec != null ? (int?)countryRec.RecID : null;


        Int32? depCity = null;
        Int32? arrCity = Conversion.getInt32OrNull(ArrCity);
        if (HttpContext.Current.Session["SearchMixData"] == null) return "";
        MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];

        List<CodeName> holpackData = new MixSearch().getMixSearchHolPack(useLocalName, data, depCity, country, arrCity, null, ref errorMsg);
        var retVal = from q in holpackData
                     orderby q.Name, q.Code
                     group q by new
                     {
                         Name = q.Name,
                         Code = q.Code + ';' + data.HolPackList.Find(f => f.HolPack == q.Code).DepCity.ToString() + ';' + data.HolPackList.Find(f => f.HolPack == q.Code).ArrCity.ToString()
                     } into k
                     select new { Code = k.Key.Code, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    [WebMethod(EnableSession = true)]
    public static string getResourtData(string ArrCity, string CountryId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        List<Location> countryList = new Locations().getLocationList(UserData, LocationType.Country, null, null, null, null, ref errorMsg);
        List<MixCountry> countryMixList = new MixSearch().getCountryList(UserData, ref errorMsg);

        List<Location> cL = (from q1 in countryList
                             join q2 in countryMixList on q1.CountryCode equals q2.ISO3
                             where q2.ISO2 == CountryId
                             select q1).ToList<Location>();

        Location countryRec = cL != null && cL.Count == 1 ? cL.FirstOrDefault() : null;
        Int32? country = countryRec != null ? (int?)countryRec.RecID : null;

        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        if (HttpContext.Current.Session["SearchMixData"] == null) return "";
        MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.LocationIDName> resortData = new MixSearch().getMixSearchResorts(useLocalName, data, country, Conversion.getInt32OrNull(ArrCity), null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(resortData);
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomInfo(string RoomCount, bool IsPaximum)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        Int16 maxChildAge = UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.HasValue ? Convert.ToInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.Value)) : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? Convert.ToInt16(18) : Convert.ToInt16(12));
        StringBuilder html = new StringBuilder();
        MixPlMaxPaxCounts maxPaxData = new MixPlMaxPaxCounts();
        object _paxData = new TvBo.Common().getFormConfigValue("SearchPanel", "PaxData");
        if (_paxData != null)
        {
            MixPlMaxPaxCounts paxData = Newtonsoft.Json.JsonConvert.DeserializeObject<MixPlMaxPaxCounts>(_paxData.ToString());
            maxPaxData = paxData;
        }
        if (HttpContext.Current.Session["SearchMixData"] != null && (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal)))
            if (((MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"]).MixPlMaxPaxCount != null)
                maxPaxData = ((MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"]).MixPlMaxPaxCount;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel))
            maxPaxData.maxAdult = 10;
        //TVB2B-4008
        if (string.Equals(UserData.CustomRegID, Common.crID_Rezeda))
            maxPaxData.maxAdult = 12;
        /* TICKET ID : 14489
        if (string.Equals(UserData.CustomRegID, Common.crID_Dallas))
        {
            maxPaxData.maxAdult = 3;
            maxPaxData.maxChd = 2;
        }
        */
        Int32 roomCount = Conversion.getInt32OrNull(RoomCount).HasValue ? Conversion.getInt32OrNull(RoomCount).Value : 1;
        if (IsPaximum)
        {
            maxPaxData.maxAdult = 4;
            maxPaxData.maxChd = 4;
            maxChildAge = 12;
        }
        Int32 j = 0;
        for (int i = 1; i < roomCount + 1; i++)
        {
            html.AppendFormat("<div style=\"width:250px; clear:both;\"><b>{0} {1}</b></div>",
                    HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblRoom"),
                    i.ToString());
            html.AppendFormat("<div id=\"divRoomInfo{0}\" style=\"width:250px; clear:both; border-top: solid 1px #000000; \">", i.ToString());
            html.AppendFormat("  <div id=\"divAdult{0}\" style=\"float: left; width:100px;\">", i.ToString());
            html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblAdult").ToString());
            html.AppendFormat("  <select id=\"fltAdult{0}\" style=\"width: 50px;\">", i.ToString(), i.ToString());
            for (j = 1; j <= maxPaxData.maxAdult; j++)
            {
                if (j == 2)
                    html.AppendFormat("     <option value='{0}' selected=\"selected\">{1}</option>", j.ToString(), j.ToString());
                else html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            }
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat("  <div id=\"divChild{0}\" style=\"float: left; width:50px;\">", i.ToString());
            html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChild").ToString());
            html.AppendFormat("  <select id=\"fltChild{0}\" onchange=\"onChildChange('{1}')\" style=\"width: 50px;\">", i.ToString(), i.ToString());
            for (j = 0; j < (maxPaxData.maxChd.HasValue ? maxPaxData.maxChd.Value + 1 : 5); j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");
            html.AppendFormat("</div>");

            html.AppendFormat("<div id=\"divRoomInfoChd{0}\" style=\"width:250px; clear:both; display:none;\">", i.ToString());
            html.AppendFormat(" <div id=\"divRoomInfoChd1{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            string childAgeStr = string.Empty;
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "1");
            else childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 1";

            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd1{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");
            html.AppendFormat(" <div id=\"divRoomInfoChd2{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "2");
            else childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 2";
            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd2{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat(" <div id=\"divRoomInfoChd3{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "3");
            else childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 3";
            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd3{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat(" <div id=\"divRoomInfoChd4{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
            if (HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString().IndexOf('{') > 0)
                childAgeStr = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString(), "4");
            else childAgeStr = HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 4";
            html.AppendFormat("   <span>{0}</span><br />", childAgeStr);
            html.AppendFormat("   <select id=\"fltRoomInfoChd4{0}\" style=\"width: 100%;\">", i.ToString());
            for (j = 0; j <= maxChildAge; j++)
                html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
            html.AppendFormat("  </select>");
            html.AppendFormat(" </div>");

            html.AppendFormat("</div>");
        }
        return html.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getCategoryData(string CountryId, string ArrCity, string Resort)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<Location> countryList = new Locations().getLocationList(UserData, LocationType.Country, null, null, null, null, ref errorMsg);
        List<MixCountry> countryMixList = new MixSearch().getCountryList(UserData, ref errorMsg);

        List<Location> cL = (from q1 in countryList
                             join q2 in countryMixList on q1.CountryCode equals q2.ISO3
                             where q2.ISO2 == CountryId
                             select q1).ToList<Location>();

        Location countryRec = cL != null && cL.Count == 1 ? cL.FirstOrDefault() : null;
        Int32? country = countryRec != null ? (int?)countryRec.RecID : null;

        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        TvBo.MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.CodeName> categoryData = new MixSearch().getMixSearchCategorys(useLocalName, data, country, Conversion.getInt32OrNull(ArrCity), Resort, null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(categoryData);
    }

    [WebMethod(EnableSession = true)]
    public static string getMixHotelData(string CountryId, string Location)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        TvBo.MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.CodeHotelName> hotelData = new MixSearch().getMixSearchExtHotels(UserData, Conversion.getInt32OrNull(Location), ref errorMsg);

        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string getHotelData(string ArrCity, string Resort, string Category, string Holpack, string CountryId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        List<Location> countryList = new Locations().getLocationList(UserData, LocationType.Country, null, null, null, null, ref errorMsg);
        List<MixCountry> countryMixList = new MixSearch().getCountryList(UserData, ref errorMsg);

        List<Location> cL = (from q1 in countryList
                             join q2 in countryMixList on q1.CountryCode equals q2.ISO3
                             where q2.ISO2 == CountryId
                             select q1).ToList<Location>();

        Location countryRec = cL != null && cL.Count == 1 ? cL.FirstOrDefault() : null;
        Int32? country = countryRec != null ? (int?)countryRec.RecID : null;

        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        TvBo.MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.CodeHotelName> hotelData = new MixSearch().getMixSearchHotels(useLocalName, data, null, country, Conversion.getInt32OrNull(ArrCity), Resort, Category, Holpack, null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomData(string DepCity, string ArrCity, string Resort, string Hotel, string CountryId, string RoomViewID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        List<Location> countryList = new Locations().getLocationList(UserData, LocationType.Country, null, null, null, null, ref errorMsg);
        List<MixCountry> countryMixList = new MixSearch().getCountryList(UserData, ref errorMsg);

        List<Location> cL = (from q1 in countryList
                             join q2 in countryMixList on q1.CountryCode equals q2.ISO3
                             where q2.ISO2 == CountryId
                             select q1).ToList<Location>();

        Location countryRec = cL != null && cL.Count == 1 ? cL.FirstOrDefault() : null;
        Int32? country = countryRec != null ? (int?)countryRec.RecID : null;

        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        TvBo.MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.CodeName> hotelData = new MixSearch().getMixSearchRooms(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, null, RoomViewID, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string getBoardData(string DepCity, string ArrCity, string Resort, string Hotel, string CountryId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        List<Location> countryList = new Locations().getLocationList(UserData, LocationType.Country, null, null, null, null, ref errorMsg);
        List<MixCountry> countryMixList = new MixSearch().getCountryList(UserData, ref errorMsg);

        List<Location> cL = (from q1 in countryList
                             join q2 in countryMixList on q1.CountryCode equals q2.ISO3
                             where q2.ISO2 == CountryId
                             select q1).ToList<Location>();

        Location countryRec = cL != null && cL.Count == 1 ? cL.FirstOrDefault() : null;
        Int32? country = countryRec != null ? (int?)countryRec.RecID : null;

        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];
        TvBo.MixSearchFilterCacheData data = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<TvBo.CodeName> hotelData = new MixSearch().getMixSearchBoards(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData);
    }

    [WebMethod(EnableSession = true)]
    public static string SetCriterias(long? CheckIn, string ExpandDate, string NightFrom, string NightTo,
            string RoomCount, List<SearchCriteriaRooms> roomsInfo, string Board, string Room, string DepCity, string ArrCity,
            string Resort, string Category, string Hotel, string CurControl, string CurrentCur,
            string ShowStopSaleHotels, string HolPack, string holPackCat, string B2BMenuCatStr, string CountryId)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["MixCriteria"] == null) CreateCriteria();
        MixSearchCriteria criteria = (MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];

        List<Location> countryList = new Locations().getLocationList(UserData, LocationType.Country, null, null, null, null, ref errorMsg);
        List<MixCountry> countryMixList = new MixSearch().getCountryList(UserData, ref errorMsg);

        List<Location> cL = (from q1 in countryList
                             join q2 in countryMixList on q1.CountryCode equals q2.ISO3
                             where q2.ISO2 == CountryId
                             select q1).ToList<Location>();

        Location countryRec = cL != null && cL.Count == 1 ? cL.FirstOrDefault() : null;
        Int32? country = countryRec != null ? (int?)countryRec.RecID : null;

        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(CheckIn) / 86400000) + 1);
        criteria.CheckIn = checkIn;
        criteria.NightFrom = Convert.ToInt16(NightFrom);
        criteria.NightTo = Conversion.getInt16OrNull(NightTo).HasValue ? Conversion.getInt16OrNull(NightTo).Value : criteria.NightFrom;
        criteria.SearchRoomCount = Convert.ToInt16(RoomCount);
        criteria.ExpandDay = Conversion.getInt16OrNull(ExpandDate).HasValue ? Conversion.getInt16OrNull(ExpandDate).Value : (Int16)0;
        List<SearchCriteriaRooms> rooms = roomsInfo;
        criteria.RoomInfoList.Clear();
        short i = 0;
        criteria.RoomInfoList.Clear();
        foreach (SearchCriteriaRooms row in rooms)
        {
            i++;
            MixSearchCriteriaRoom room = new MixSearchCriteriaRoom();
            room.RoomNr = i;
            room.AdultCount = row.Adult;
            room.ChildCount = row.Child;
            List<short> childList = new List<short>();
            if (row.Chd1Age.HasValue && row.Chd1Age > -1)
                childList.Add(row.Chd1Age.Value);
            if (row.Chd2Age.HasValue && row.Chd2Age > -1)
                childList.Add(row.Chd2Age.Value);
            if (row.Chd3Age.HasValue && row.Chd3Age > -1)
                childList.Add(row.Chd3Age.Value);
            if (row.Chd3Age.HasValue && row.Chd4Age > -1)
                childList.Add(row.Chd4Age.Value);
            room.ChildAge = childList;
            criteria.RoomInfoList.Add(room);
        }
        criteria.Board = Conversion.getStrOrNull(Board);
        criteria.Room = Conversion.getStrOrNull(Room);
        criteria.DepCity = Conversion.getInt32OrNull(string.Empty);
        criteria.ArrCity = Conversion.getInt32OrNull(ArrCity).HasValue ? Conversion.getInt32OrNull(ArrCity).Value : country;
        criteria.Resort = Conversion.getStrOrNull(Resort);
        criteria.Category = Conversion.getStrOrNull(Category);
        criteria.Hotel = Conversion.getStrOrNull(Hotel);
        criteria.Package = Conversion.getStrOrNull(HolPack);
        criteria.FromRecord = 1;
        criteria.ToRecord = 25;
        criteria.CurControl = Equals(CurControl, "Y");
        criteria.CurrentCur = CurrentCur;
        criteria.SType = SearchType.OnlyHotelSearch;
        //criteria.Country = country;
        criteria.ShowStopSaleHotel = Equals(ShowStopSaleHotels, "Y");
        //criteria.HolPackCat = Conversion.getInt16OrNull(holPackCat);
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        //criteria.UseGroup = groupSeacrh != null ? (bool)groupSeacrh : false;
        HttpContext.Current.Session["MixCriteria"] = criteria;
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string SetMixCriterias(long? CheckIn, int? ExpandDate, int? NightFrom, int? NightTo, string Location,
            int? RoomCount, List<SearchCriteriaRooms> roomsInfo, string Hotel, string CurControl, string CurrentCur,
            string DefaultNationality, bool Available)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["MixCriteria"] == null) CreateCriteria();
        MixSearchCriteria criteria = (MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];

        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(CheckIn) / 86400000) + 1);
        criteria.CheckIn = checkIn;
        criteria.ExpandDay = Convert.ToInt16(ExpandDate);
        criteria.NightFrom = Convert.ToInt16(NightFrom);
        criteria.NightTo = Conversion.getInt16OrNull(NightTo).HasValue ? Conversion.getInt16OrNull(NightTo).Value : criteria.NightFrom;
        criteria.PaxLocation = new List<string>() { Location };
        criteria.SearchRoomCount = Convert.ToInt16(RoomCount);
        criteria.CurrentCurrency = string.IsNullOrEmpty(CurrentCur) ? "EUR" : CurrentCur;
        List<SearchCriteriaRooms> rooms = roomsInfo;

        criteria.RoomInfoList.Clear();
        foreach (SearchCriteriaRooms row in rooms)
        {
            MixSearchCriteriaRoom room = new MixSearchCriteriaRoom();
            room.AdultCount = row.Adult;
            room.ChildCount = row.Child;
            List<short> childList = new List<short>();
            if (row.Chd1Age.HasValue && row.Chd1Age > -1)
                childList.Add(row.Chd1Age.Value);
            if (row.Chd2Age.HasValue && row.Chd2Age > -1)
                childList.Add(row.Chd2Age.Value);
            if (row.Chd3Age.HasValue && row.Chd3Age > -1)
                childList.Add(row.Chd3Age.Value);
            if (row.Chd3Age.HasValue && row.Chd4Age > -1)
                childList.Add(row.Chd4Age.Value);
            room.ChildAge = childList;
            criteria.RoomInfoList.Add(room);
        }
        criteria.Hotel = Conversion.getStrOrNull(Hotel);
        criteria.SType = SearchType.OnlyHotelMixSearch;
        criteria.PaxNationality = DefaultNationality;
        criteria.PaxAvailable = Available;
        HttpContext.Current.Session["MixCriteria"] = criteria;
        string version = Conversion.getStrOrNull(ConfigurationManager.AppSettings["PaximumResultVersion"]);
        //PaximumResultVersion
        return version;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static FirstFlightRecord reSearch(long? date)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["Criteria"] == null) return null;
        if (!date.HasValue) return null;
        MixSearchCriteria criteria = (MixSearchCriteria)HttpContext.Current.Session["Criteria"];
        criteria.CheckIn = new DateTime(0).AddTicks(date.Value);
        HttpContext.Current.Session["Criteria"] = criteria;
        FirstFlightRecord firstFlight = new FirstFlightRecord();

        Int64 days = (criteria.CheckIn - (new DateTime(1970, 1, 1))).Days - 1;
        firstFlight.CheckIn = (days * 86400000);
        firstFlight.DateFormat = new TvBo.Common().getDateFormat(UserData.Ci).Replace('/', '-');
        firstFlight.FlyDate = criteria.CheckIn.AddDays(1);

        return firstFlight;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<calendarColor> getCheckIn(string ArrCity, string HolPack)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<calendarColor> retVal = new List<calendarColor>();

        if (!(string.Equals(UserData.CustomRegID, Common.crID_HouseTravel) || string.Equals(UserData.CustomRegID, Common.crID_FilipTravel)))
            return retVal;

        string errorMsg = string.Empty;
        int? arrCity = Conversion.getInt32OrNull(ArrCity);
        List<FlightDays> flightDays = new Search().getCheckIn(UserData.Market, UserData.Operator, arrCity, "O", string.Empty, HolPack, ref errorMsg);
        List<Location> location = CacheObjects.getLocationList(UserData.Market);

        foreach (var row in flightDays)
        {
            Int16 colorType = -1;
            retVal.Add(new calendarColor
            {
                Year = row.FlyDate.Year,
                Month = row.FlyDate.Month,
                Day = row.FlyDate.Day,
                Text = " ",
                ColorType = colorType
            });
        }
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static IEnumerable getPaximumLocation(string pTerm, string pCountry)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        MixSearchFilterCacheData cacheData = (MixSearchFilterCacheData)HttpContext.Current.Session["SearchMixData"];

        string errorMsg = string.Empty;

        if (UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode))
        {
            string paximumApiUrl = UserData.PaxSetting.Pxm_APIAddr + (UserData.PaxSetting.Pxm_APIAddr.Substring(UserData.PaxSetting.Pxm_APIAddr.Length - 1, 1) == "/" ? "" : "/");
            string paximumAccess_Token = UserData.PaxSetting.Pxm_AutMainCode;
            string baseUrl = string.Format("{0}AutoComplete/GetCitiesByQuery?access_token={1}",
                paximumApiUrl,
                paximumAccess_Token);

            WebClient client = new WebClient();
            client.BaseAddress = baseUrl;
            client.Headers.Add("content-type", "application/json");
            var rq = new
            {
                term = pTerm,
                serviceProviders = new string[] { "PAX" }
            };
            string response = client.UploadString(baseUrl, Newtonsoft.Json.JsonConvert.SerializeObject(rq)); //.DownloadString(baseUrl);
            GetHotelsByQueryResponse locationData = Newtonsoft.Json.JsonConvert.DeserializeObject<GetHotelsByQueryResponse>(response,
                new JsonSerializerSettings
                {
                    ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                    NullValueHandling = NullValueHandling.Ignore,
                    DefaultValueHandling = DefaultValueHandling.Ignore,
                    ObjectCreationHandling = ObjectCreationHandling.Auto
                });
            string country = pCountry.Split('|').Length > 0 ? pCountry.Split('|')[0] : string.Empty;
            var countryQ = from q in locationData.Suggestions
                           join q1 in cacheData.CountryList on q.Country.id equals q1.Code
                           where q.Type == "city" && q.Country.id == country && q1.CountryType == mixCountryType.Paximum
                           select new { value = q.City.name, item = q.City.id };
            return countryQ;
        }
        else
        {
            return null;
        }
    }
}