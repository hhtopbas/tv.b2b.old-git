﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Web.Services;
using System.Text;
using TvTools;

public partial class CustomerDetail : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(int? _CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == _CustNo.Value);

        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string[] dateMaskB = dateMaskA.Split(UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d": _dateMask += "/99"; _dateFormat += "/dd"; break;
                    case "m": _dateMask += "/99"; _dateFormat += "/MM"; break;
                    case "y": _dateMask += "/9999"; _dateFormat += "/yyyy"; break;
                    default: break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateFormat = _dateFormat.Remove(0, 1);
        List<DDLData> nations = CacheObjects.getNations(UserData.Market);
        List<Nationality> nationalityList = CacheObjects.getNationality(UserData);
        List<TitleAgeRecord> title = ResData.TitleCust;
        sb.AppendFormat("<input id=\"dateMask\" type=\"hidden\" value=\"{0}\" />", _dateMask);
        sb.AppendFormat("<input id=\"iSeqNo\" type=\"hidden\" value=\"{0}\" />", resCust.SeqNo);
        sb.AppendFormat("<input id=\"iPhone\" type=\"hidden\" value=\"{0}\" />", resCust.Phone);
        sb.AppendFormat("<input id=\"iPassport\" type=\"hidden\" value=\"{0}\" />", resCust.HasPassport.HasValue ? resCust.HasPassport.Value : true);
        sb.AppendFormat("<input id=\"iLeader\" type=\"hidden\" value=\"{0}\" />", Equals(resCust.Leader, "Y"));

        sb.Append("<br /><br /><table cellpadding=\"2\" cellspacing=\"2\">");
        string titleList = string.Empty;
        if (resCust.Title < 6)
        {
            foreach (TitleAgeRecord tRow in title.Where(w => w.TitleNo < 6).Select(s => s).ToList<TitleAgeRecord>())
                titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", tRow.TitleNo, Equals(tRow.TitleNo.ToString(), resCust.Title.ToString()) ? "selected=\"selected\"" : "", tRow.Code);
        }
        else
        {
            foreach (TitleAgeRecord tRow in title.Where(w => w.TitleNo > 5).Select(s => s).ToList<TitleAgeRecord>())
                titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", tRow.TitleNo, Equals(tRow.TitleNo.ToString(), resCust.Title.ToString()) ? "selected=\"selected\"" : "", tRow.Code);
        }
        sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><select id=\"iTitle\" class=\"Title\">{1}</select></td></tr>",
                        HttpContext.GetGlobalResourceObject("CustomerDetail", "lblTitle"),
                        titleList);
        sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iSurname\" type=\"text\" value=\"{1}\" class=\"Surname\" {2} onkeypress=\"return isNonUniCodeChar(event);\" /></td></tr>",
                        HttpContext.GetGlobalResourceObject("CustomerDetail", "lblSurname"),
                        resCust.Surname,
                        "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules) + "\"");
        sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iSurnameL\" type=\"text\" value=\"{1}\" class=\"Surname\" {2} /></td></tr>",
                        HttpContext.GetGlobalResourceObject("CustomerDetail", "lblSurnameLocal"),
                        resCust.SurnameL,
                        "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules) + "\"");
        sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iName\" type=\"text\" value=\"{1}\" class=\"Name\" {2} onkeypress=\"return isNonUniCodeChar(event);\" /></td></tr>",
                        HttpContext.GetGlobalResourceObject("CustomerDetail", "lblName"),
                        resCust.Name,
                        "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules) + "\"");
        sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iNameL\" type=\"text\" value=\"{1}\" class=\"Name\" {2} /></td></tr>",
                        HttpContext.GetGlobalResourceObject("CustomerDetail", "lblNameLocal"),
                        resCust.NameL,
                        "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules) + "\"");
        string birthDate = resCust.Birtday.HasValue ? resCust.Birtday.Value.ToString(_dateFormat) : "";
        string _dateFormatRegion = new TvBo.Common().getDateFormatRegion(UserData.Ci);
        sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iBirthDay\" type=\"text\" value=\"{1}\" onblur=\"return SetAge('{2}', '{3}', '{5}')\" class=\"BirthDay formatDate\" /><span class=\"dateFormatAlert\">({4})</span></td></tr>",
                        HttpContext.GetGlobalResourceObject("CustomerDetail", "lblBirthDate"),
                        birthDate,
                        _CustNo,
                        ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(_dateFormat).Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '/').Replace(" ","") : "",                                               
                        _dateFormatRegion,
                        _dateFormat);
        sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iAge\" type=\"text\" value=\"{1}\" class=\"Age\" disabled=\"disabled\" /></td></tr>",
                        HttpContext.GetGlobalResourceObject("CustomerDetail", "lblAge"),
                        resCust.Age);

        bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
        if (UserData.TvParams.TvParamReser.NeedPIN.HasValue && UserData.TvParams.TvParamReser.NeedPIN.Value)
            showPIN = true;
        if (!showPIN.HasValue || showPIN.Value)
            sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iIDNo\" type=\"text\" value=\"{1}\" class=\"IDNo\" /></td></tr>",
                            HttpContext.GetGlobalResourceObject("CustomerDetail", "lblPIN"),
                            resCust.IDNo);
        else sb.AppendFormat("<input id=\"iIDNo\" type=\"hidden\" value=\"{0}\" />", resCust.IDNo);

        bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));

        if (!showPassportInfo.HasValue || showPassportInfo.Value)
        {
            #region Show passport information
            sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iPassSerie\" type=\"text\" value=\"{1}\" class=\"PassSerie\" /></td></tr>",
                            HttpContext.GetGlobalResourceObject("CustomerDetail", "lblPassSerie"),
                            resCust.PassSerie);
            sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iPassNo\" type=\"text\" value=\"{1}\" class=\"PassNo\" /></td></tr>",
                            HttpContext.GetGlobalResourceObject("CustomerDetail", "lblPassNo"),
                            resCust.PassNo);
            sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iPassIssueDate\" type=\"text\" value=\"{1}\" class=\"PassIssueDate formatDate\" /><span class=\"dateFormatAlert\">({2})</span></td></tr>",
                            HttpContext.GetGlobalResourceObject("CustomerDetail", "lblPassIssueDate"),
                            resCust.PassIssueDate.HasValue ? resCust.PassIssueDate.Value.ToString(_dateFormat) : "",
                            _dateFormatRegion);
            sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iPassExpDate\" type=\"text\" value=\"{1}\" class=\"PassExpDate formatDate\" /><span class=\"dateFormatAlert\">({2})</span></td></tr>",
                            HttpContext.GetGlobalResourceObject("CustomerDetail", "lblPassExpDate"),
                            resCust.PassExpDate.HasValue ? resCust.PassExpDate.Value.ToString(_dateFormat) : "",
                            _dateFormatRegion);
            sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><input id=\"iPassGiven\" type=\"text\" value=\"{1}\" class=\"PassGiven\" /></td></tr>",
                            HttpContext.GetGlobalResourceObject("CustomerDetail", "lblPassGiven"),
                            resCust.PassGiven);
            #endregion
        }
        else
        {
            #region Hide passport information
            sb.AppendFormat("<input id=\"iPassSerie\" type=\"hidden\" value=\"{0}\" />", resCust.PassSerie);
            sb.AppendFormat("<input id=\"iPassIssueDate\" type=\"hidden\" value=\"{0}\" />", resCust.PassIssueDate.HasValue ? resCust.PassIssueDate.Value.ToString(_dateFormat) : "");
            sb.AppendFormat("<input id=\"iPassExpDate\" type=\"hidden\" value=\"{0}\" />", resCust.PassExpDate.HasValue ? resCust.PassExpDate.Value.ToString(_dateFormat) : "");
            sb.AppendFormat("<input id=\"iPassGiven\" type=\"hidden\" value=\"{0}\" />", resCust.PassGiven);
            #endregion
        }
        string nationList = string.Empty;


        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
        {
            foreach (Nationality nRow in nationalityList)
                nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.Code3, Equals(nRow.Code3, resCust.Nationality) ? "selected=\"selected\"" : "", nRow.Name);
            sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><select id=\"iNationality\" class=\"Nation\">{1}</select></td></tr>",
                        HttpContext.GetGlobalResourceObject("CustomerDetail", "lblCitizenship"),
                        nationList);
        }
        else
        {
            foreach (DDLData nRow in nations)
                nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.FieldData, Equals(nRow.FieldData, resCust.Nation.ToString()) ? "selected=\"selected\"" : "", nRow.TextData);
            sb.AppendFormat("<tr><td class=\"Field\"><span>{0}</span></td><td><select id=\"iNation\" class=\"Nation\">{1}</select></td></tr>",
                        HttpContext.GetGlobalResourceObject("CustomerDetail", "lblCitizenship"),
                        nationList);
        }

        
        sb.Append("</table>");

        return sb.ToString();
    }
}

