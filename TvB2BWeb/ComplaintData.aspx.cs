﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using TvTools;

public partial class ComplaintData : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        int iDisplayLength = Convert.ToInt32(HttpContext.Current.Request["iDisplayLength"]);
        int iDisplayStart = Convert.ToInt32(HttpContext.Current.Request["iDisplayStart"]);
        int iEcho = Convert.ToInt32(HttpContext.Current.Request["sEcho"]);

        IQueryable<ComplaintList> allRecords = ReadFile().AsQueryable();

        List<requestFormData> requestForm = new List<requestFormData>();
        for (int i = 0; i < Request.Form.Count; i++) requestForm.Add(new requestFormData { Name = Request.Form.GetKey(i), Value = Request[Request.Form.GetKey(i)] });
        string sortingStr = string.Empty;
        var iSortingColsQ = from q in requestForm
                            where q.Name == "iSortingCols"
                            select q.Value;
        Int16? iSortingCols = Conversion.getInt16OrNull(iSortingColsQ.FirstOrDefault());
        string sort = string.Empty;
        if (iSortingCols.HasValue) {
            for (int i = 0; i < iSortingCols.Value; i++) {
                var iSortCol = (from q in requestForm
                                where q.Name == "iSortCol_" + i
                                select q.Value).FirstOrDefault();
                string sSortDir = (from q in requestForm
                                   where q.Name == "sSortDir_" + i
                                   select q.Value).FirstOrDefault();
                string bSortable = (from q in requestForm
                                    where q.Name == "bSortable_" + iSortCol
                                    select q.Value).FirstOrDefault();
                if (string.Equals(bSortable, "true")) {
                    switch (iSortCol) {
                        case "0":
                            sortingStr = "Department";
                            sort = sSortDir;
                            break;
                        case "1":
                            sortingStr = "Hotel";
                            sort = sSortDir;
                            break;
                        case "2":
                            sortingStr = "Category";
                            sort = sSortDir;
                            break;
                        case "3":
                            sortingStr = "ReceivedDate";
                            sort = sSortDir;
                            break;
                    }
                }
            }
        }

        if (sortingStr.Length > 0) {
            if (sort.ToLower() == "asc")
                allRecords = allRecords.OrderBy(sortingStr);
            else
                allRecords = allRecords.OrderByDescending(sortingStr);
        }

        IQueryable<ComplaintList> filteredRecords = allRecords;
        filteredRecords = filteredRecords.Skip(iDisplayStart).Take(iDisplayLength);

        var retval = from q in filteredRecords
                     select new {
                         ViewEdit = "<input type=\"button\" value=\"View / Edit\" onclick=\"onclickViewEdit(" + q.RecID + ")\">",
                         Attach = q.HaveAttach ? "Yes" : "No",
                         Department = useLocalName ? q.DepartmentNameL : q.DepartmentName,
                         Hotel = useLocalName ? q.HotelNameL : q.HotelName,
                         Source = useLocalName ? q.ComplaintSourceNameL : q.ComplaintSourceName,
                         Status = useLocalName ? q.StatusNameL : q.StatusName,
                         Category = useLocalName ? q.CategoryNameL : q.CategoryName,
                         ReceivedDate = q.ReceivedDate.HasValue ? q.ReceivedDate.Value.ToShortDateString() : string.Empty,
                         Active = q.Active.HasValue ? (q.Active.Value ? "Pending" : "Closed") : "",
                         ComplaintType = complaintType(q.ComplaintType)
                     };

        lightTable returnTable = new lightTable();
        returnTable.sEcho = iEcho;
        returnTable.iTotalRecords = allRecords.Count();
        returnTable.iTotalDisplayRecords = allRecords.Count();
        //string selectLinq = string.Format("new({5}ResNo, DepCityName, ArrCityName, BegDate, EndDate, Days, statusStr, ResDate, LeaderName, Adult, Child, SaleCur, SalePrice{3}, Discount{6}, AgencySupDis, AgencyComSup{4}{2}, AgencyPayable, AgencyPayable2, AgencyPayment, Balance{1}{0})",
        //    ((UserData.Bonus.BonusAgencySeeOwnW && UserData.Bonus.BonusUserSeeAgencyW) || (UserData.Bonus.BonusAgencySeeOwnW && UserData.MasterAgency)) ? ", AgencyBonus" : "",
        //    UserData.Bonus.BonusUserSeeOwnW ? ", UserBonus" : "",
        //    showPassengerEB ? ", EBPas" : "",
        //    showAgencyCom ? ", AgencyCom, DiscountFromCom" : "",
        //    showAgencyEB ? ", EBAgency" : "",
        //    showPayment ? "SelectRes, " : string.Empty,
        //    UserData.MainAgency ? ", BrokerCom" : "");

        returnTable.aaData = retval; //.Select(selectLinq);

        Response.Clear();

        Response.ContentType = ("application/json");
        Response.BufferOutput = true;
        Response.Write(ser.Serialize(returnTable));
        Response.End();
    }

    internal string complaintType(Int16? pType)
    {
        string retVal = string.Empty;
        switch (pType) {
            case 0:
                retVal = "Complaint";
                break;
            case 1:
                retVal = "Request";
                break;
            case 2:
                retVal = "Other";
                break;
            default:
                retVal = string.Empty;
                break;
        }
        return retVal;
    }

    protected List<ComplaintList> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\ComplaintMonitor." + HttpContext.Current.Session.SessionID;
        System.IO.StreamReader reader = new System.IO.StreamReader(path);
        List<ComplaintList> list = new List<ComplaintList>();
        try {
            string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ComplaintList>>(uncompressed);
        }
        catch (Exception) {
            throw;
        }
        finally {
            reader.Close();
        }
        return list;
    }
}