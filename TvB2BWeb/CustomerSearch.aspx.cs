﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Globalization;
using TvBo;
using System.Text;
using System.Collections.Generic;
using TvTools;
using System.Web.Services;
using System.Threading;

public partial class Default : BasePage
{
    public static string twoLetterISOLanguageName = "en";
    public static string dateFormatCurrentCulture = "dd/MM/yyyy";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";

        dateFormatCurrentCulture = new Common().getDateFormat(UserData.Ci);
    }

    [WebMethod(EnableSession = true)]
    public static string searchCustomer(string refNo, string SeqNo, string Surname, string Name, long? BirthDate, string PIN, string PassNo, string MobilPhone)
    {       
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;  
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        DateTime birthDate = new DateTime(1970, 1, 1);
        birthDate = birthDate.AddDays((Convert.ToInt64(BirthDate) / 86400000) + 1);

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci);

        SearchCustomerOption searchOptions = SearchCustomerOption.AllData;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            searchOptions = SearchCustomerOption.OnlyAgency;
        else
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                searchOptions = SearchCustomerOption.OnlyMainAgency;
            else
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                    searchOptions = SearchCustomerOption.OnlyMarket;
                else
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Dallas))
                        searchOptions = SearchCustomerOption.AllData;
                    else
                        searchOptions = SearchCustomerOption.OnlyAgency;

        List<ResCustRecord> list = new Reservation().searchCustomers(UserData, searchOptions, Surname, Name, BirthDate.HasValue ? birthDate : (DateTime?)null, PIN, PassNo, MobilPhone, ref errorMsg);

        if (list != null && list.Count > 0 && string.IsNullOrEmpty(errorMsg))
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div id=\"divSearcCustList\">");
            sb.Append("<table>");
            sb.Append("<tr class=\"sercahCustListHeader\">");
            sb.Append("<td class=\"sselectH\">&nbsp;</td>");
            sb.AppendFormat("<td class=\"sSurnameH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustSurname"));
            sb.AppendFormat("<td class=\"sNameH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustName"));
            sb.AppendFormat("<td class=\"sBirthDateH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustBirthDate"));
            sb.AppendFormat("<td class=\"sPassSerieH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustPassSerie"));
            sb.AppendFormat("<td class=\"sPassNoH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustPassNo"));
            sb.Append("</tr>");

            foreach (ResCustRecord row in list)
            {
                sb.Append("<tr class=\"sercahCustListRow\">");
                sb.AppendFormat("<td class=\"sselect\"><input type=\"button\" value=\"{0}\" onclick=\"searchSelectCust('{1}')\" /></td>",
                        HttpContext.GetGlobalResourceObject("LibraryResource", "btnSelect"),
                        row.CustNo.ToString() + ";" + SeqNo + ";" + refNo);
                sb.AppendFormat("<td class=\"sSurname\">{0}</td>", row.Surname);
                sb.AppendFormat("<td class=\"sName\">{0}</td>", row.Name);
                sb.AppendFormat("<td class=\"sBirthDate\">{0}</td>", row.Birtday.HasValue ? row.Birtday.Value.ToString(dateFormat) : "&nbsp;");
                sb.AppendFormat("<td class=\"sPassSerie\">{0}</td>", row.PassSerie);
                sb.AppendFormat("<td class=\"sPassNo\">{0}</td>", row.PassNo);
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }
        else return "Err:" + errorMsg;                
    }
}
