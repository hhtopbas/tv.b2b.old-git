﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="onlyHotelHolPackCatFilter.aspx.cs"
    Inherits="onlyHotelHolPackCatFilter" EnableEventValidation="false" %>

<%--<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "onlyHotelHolPackCatFilter")%></title>
    <style type="text/css">
        .special_day a { background: #ABABAB url('Images/FlightDay.gif') no-repeat !important; color: #222222 !important; }
    </style>
    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
     
    <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/PriceSearchFilter.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var ComboSelect = '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
        var btnClear = '<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>';
        var addPleaseSelectDeparture = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectDeparture") %>';
        var dayColor = [];
        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            self.parent.logout();
        }

        var myWidth = 0, myHeight = 0;

        $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function reSizeFrame() {
            self.parent.reSizeFilterFrame($('body').height());
        }

        function showDialogYesNo(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        return true;
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        $(this).dialog('close');
                        return false;
                    }
                }]
            });
        }

        function showDialog(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        }

        function showMessage(msg, transfer, trfUrl) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        }

        function dateChange(_TextBox, _PopCal) {
            var iDate = $("#hfDate");
            if ((!_TextBox) || (!_PopCal)) return
            var _format = _TextBox.getAttribute("Format");
            var _Date = _PopCal.getDate(_TextBox.value, _format);
            if (_Date) {
                iDate.val(Date.parse(_Date));
            }
            else {
                iDate.val(Date.parse(new Date()));
            }
        }


        function getInf(adult, whois) {
            var Adult = parseInt(adult);
            $("#fltInfant" + whois).html("");
            for (i = 0; i < Adult + 1; i++) {
                $("#fltInfant" + whois).append("<option value='" + i.toString() + "'>" + i.toString() + "</option>");
            }
        }


        function SetChild(childCount, whois) {
            if (childCount > 0) $("#divRoomInfoChd1" + whois).show(); else $("#divRoomInfoChd1" + whois).hide();
            if (childCount > 1) $("#divRoomInfoChd2" + whois).show(); else $("#divRoomInfoChd2" + whois).hide();
            if (childCount > 2) $("#divRoomInfoChd3" + whois).show(); else $("#divRoomInfoChd3" + whois).hide();
            if (childCount > 3) $("#divRoomInfoChd4" + whois).show(); else $("#divRoomInfoChd4" + whois).hide();
        }

        function getChild(child, whois) {
            var _divChild = $("#divRoomInfoChd" + whois);
            if (parseInt(child) == 0) {
                SetChild(parseInt(child), whois);
                _divChild.hide();
            }
            else {
                _divChild.show();
                SetChild(parseInt(child), whois);
            }
        }

        function onChildChange(whois) {
            getChild($("#fltChild" + whois).val(), whois);
            reSizeFrame();
        }


        function getRoomPaxs() {
            var roomCount = parseInt($("#fltRoomCount").val());
            var result = '';
            for (i = 1; i < roomCount + 1; i++) {
                var adult = parseInt($("#fltAdult" + i.toString()).val());
                var child = parseInt($("#fltChild" + i.toString()).val());
                var chd1Age = parseInt($("#fltRoomInfoChd1" + i.toString()).val());
                var chd2Age = parseInt($("#fltRoomInfoChd2" + i.toString()).val());
                var chd3Age = parseInt($("#fltRoomInfoChd3" + i.toString()).val());
                var chd4Age = parseInt($("#fltRoomInfoChd4" + i.toString()).val());
                if (i > 1)
                    result += ',(';
                else result += '(';
                result += '|Adult|:|' + adult.toString() + '|';
                result += ',|Child|:|' + child.toString() + '|';
                result += ',|Chd1Age|:|' + (child > 0 ? chd1Age : -1) + '|';
                result += ',|Chd2Age|:|' + (child > 1 ? chd2Age : -1) + '|';
                result += ',|Chd3Age|:|' + (child > 2 ? chd3Age : -1) + '|';
                result += ',|Chd4Age|:|' + (child > 3 ? chd4Age : -1) + '|';
                result += ')';
            }
            var hfRoomInfo = $("#hfRoomInfo");
            hfRoomInfo.val(result);
            reSizeFrame();
        }

        function getHolPackCat() {
            $.ajax({
                type: "POST",
                url: "onlyHotelHolPackCatFilter.aspx/getHolPackCat",
                data: '{"HolPackCat":"' + $("#fltHolPackCat").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltHolPackCat").html("");
                    $("#fltHolPackCat").append("<option value='' >" + ComboAll + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#fltHolPackCat").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                    else {
                        $("#holPackCatTr").hide();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeHolPackCat() {

        }

        function getArrival() {
            $.ajax({
                type: "POST",
                url: "onlyHotelHolPackCatFilter.aspx/getArrival",
                data: '{"HolPackCat":"' + $("#fltHolPackCat").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltArrCity").html("");
                    $("#fltArrCity").append("<option value='' >" + ComboSelect + "</option>");
                    var country = '';
                    var first = true;
                    $.each($.json.decode(msg.d), function (i) {
                        if (this.Country != country) {
                            if (!first) {
                                $("#fltArrCity").append("</optgroup>");
                            }
                            $("#fltArrCity").append("<optgroup label='" + this.Country + "'>");
                            first = false;
                            country = this.Country;
                        }
                        $("#fltArrCity").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                    });
                    if ($("#fltArrCity").html() != '') {
                        $("#fltArrCity").append("</optgroup>");
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeArrCity() {
            $("#txtResort").html(''); $("#hfResort").val('');
            $("#txtCategory").html(''); $("#hfCategory").val('');
            $("#txtHotel").html(''); $("#hfHotel").val('');
            $("#txtRoom").html(''); $("#hfRoom").val('');
            $("#txtBoard").html(''); $("#hfBoard").val('');

            $("#divListPopup").html('');
            setTransportDays();
        }

        function getHolpack() {
            $.ajax({
                type: "POST",
                url: "onlyHotelHolPackCatFilter.aspx/getHolpack",
                data: '{"DepCity":"","ArrCity":"' + $("#fltArrCity").val() + '","HolPackCat":"' + $("#fltHolPackCat").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltHolpack").html("");
                    $("#fltHolpack").append("<option value=';;' >" + ComboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#fltHolpack").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeHolpack() {
            var holpackCode = $("#fltHolpack").val();
            $("#fltArrCity").val(holpackCode.split(';')[2]);

            $("#txtHotel").html(''); $("#hfHotel").val('');
            $("#txtRoom").html(''); $("#hfRoom").val('');
            $("#txtBoard").html(''); $("#hfBoard").val('');
            $("#hfHolPack").val(holpackCode.split(';')[0]);
        }

        function listPopupFillData(sourceBtn) {
            var depCity = '';
            var arrCity = $("#fltArrCity").val();
            var resort = $("#hfResort").val();
            var category = $("#hfCategory").val();
            var hotel = $("#hfHotel").val();
            var room = $("#hfRoom").val();
            var board = $("#hfBoard").val();
            var holpack = $("#fltHolpack").val().split(';')[0];
            var paraList = '';
            var serviceUrl = '';
            switch (sourceBtn) {
                case 'resort':
                    paraList = '{"ArrCity":"' + arrCity + '","HolPackCat":"' + $("#fltHolPackCat").val() + '"}';
                    serviceUrl = 'onlyHotelHolPackCatFilter.aspx/getResourtData';
                    $.ajax({
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            $("#txtCategory").html(''); $("#hfCategory").val('');
                            $("#txtHotel").html(''); $("#hfHotel").val('');
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            var inputDataArray = msg.d;
                            var listChk = $("#hfResort");
                            var _input = '';
                            var mydata = $.json.decode(inputDataArray)
                            $.each(mydata, function (index, inputData) {
                                var checked = '';
                                if (listChk.val().length > 0) {
                                    var chkListStr = listChk.val();
                                    var chkList = chkListStr.split('|');
                                    if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.RecID.toString()) > 0)
                                        checked = 'checked=\'checked\'';
                                    else checked = '';
                                } else checked = '';
                                _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.RecID + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                            });
                            $("#divListPopup").html('');
                            $("#divListPopup").html(_input);
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                case 'category':
                    paraList = '{"ArrCity":"' + arrCity + '","Resort":"' + resort + '","HolPackCat":"' + $("#fltHolPackCat").val() + '"}';
                    serviceUrl = 'onlyHotelHolPackCatFilter.aspx/getCategoryData';
                    $.ajax({
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            $("#txtCategory").html(''); $("#hfCategory").val('');
                            $("#txtHotel").html(''); $("#hfHotel").val('');
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            var inputDataArray = msg.d;
                            var listChk = $("#hfCategory");
                            var _input = '';
                            var mydata = $.json.decode(inputDataArray)
                            $.each(mydata, function (index, inputData) {
                                var checked = '';
                                if (listChk.val().length > 0) {
                                    var chkListStr = listChk.val();
                                    var chkList = chkListStr.split('|');
                                    if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                        checked = 'checked=\'checked\'';
                                    else checked = '';
                                } else checked = '';
                                _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                            });
                            $("#divListPopup").html('');
                            $("#divListPopup").html(_input);
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                case 'hotel':
                    paraList = '{"ArrCity":"' + arrCity + '","Resort":"' + resort + '","Category":"' + category + '","Holpack":"' + holpack + '","HolPackCat":"' + $("#fltHolPackCat").val() + '"}';
                    serviceUrl = 'onlyHotelHolPackCatFilter.aspx/getHotelData';
                    $.ajax({
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            $("#txtHotel").html(''); $("#hfHotel").val('');
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            var inputDataArray = msg.d;
                            var listChk = $("#hfHotel");
                            var _input = '';
                            var mydata = $.json.decode(inputDataArray)
                            $.each(mydata, function (index, inputData) {
                                var checked = '';
                                if (listChk.val().length > 0) {
                                    var chkListStr = listChk.val();
                                    var chkList = chkListStr.split('|');
                                    if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                        checked = 'checked=\'checked\'';
                                    else checked = '';
                                } else checked = '';

                                _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                            });
                            $("#divListPopup").html('');
                            $("#divListPopup").html(_input);
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                case 'room':
                    paraList = '{"DepCity":"' + depCity + '","ArrCity":"' + arrCity + '","Resort":"' + resort + '","Hotel":"' + hotel + '","HolPackCat":"' + $("#fltHolPackCat").val() + '"}';
                    serviceUrl = 'onlyHotelHolPackCatFilter.aspx/getRoomData';
                    $.ajax({
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            $("#txtRoom").html(''); $("#hfRoom").val('');
                            $("#txtBoard").html(''); $("#hfBoard").val('');
                            var inputDataArray = msg.d;
                            var listChk = $("#hfRoom");
                            var _input = '';
                            var mydata = $.json.decode(inputDataArray)
                            $.each(mydata, function (index, inputData) {
                                var checked = '';
                                if (listChk.val().length > 0) {
                                    var chkListStr = listChk.val();
                                    var chkList = chkListStr.split('|');
                                    if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                        checked = 'checked=\'checked\'';
                                    else checked = '';
                                } else checked = '';

                                _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                            });
                            $("#divListPopup").html('');
                            $("#divListPopup").html(_input);
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                case 'board':
                    paraList = '{"DepCity":"' + depCity + '","ArrCity":"' + arrCity + '","Resort":"' + resort + '","Hotel":"' + hotel + '","HolPackCat":"' + $("#fltHolPackCat").val() + '"}';
                    serviceUrl = 'onlyHotelHolPackCatFilter.aspx/getBoardData';
                    $.ajax({
                        type: "POST",
                        data: paraList,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: serviceUrl,
                        success: function (msg) {
                            $("#txtBoard").text(''); $("#hfBoard").val('');
                            var inputDataArray = msg.d;
                            var listChk = $("#hfBoard");
                            var _input = '';
                            var mydata = $.json.decode(inputDataArray)
                            $.each(mydata, function (index, inputData) {
                                var checked = '';
                                if (listChk.val().length > 0) {
                                    var chkListStr = listChk.val();
                                    var chkList = chkListStr.split('|');
                                    if (chkList.length > 0 && $.json.encode(chkList).indexOf(inputData.Code.toString()) > 0)
                                        checked = 'checked=\'checked\'';
                                    else checked = '';
                                } else checked = '';

                                _input += "<input id='" + sourceBtn + "_" + index.toString() + "' type='checkbox' value='" + inputData.Code + "' " + checked + " /><label for='" + sourceBtn + "_" + index.toString() + "'>" + inputData.Name + "</label><br />";
                            });
                            $("#divListPopup").html('');
                            $("#divListPopup").html(_input);
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                    break;
                default: break;
            }
            reSizeFrame();
        }

        function listPopupGetSelected(sourceBtn) {
            switch (sourceBtn) {
                case 'resort':
                    var hfResort = $("#hfResort");
                    var iResort = $("#txtResort");
                    var listChk = '';
                    var listTxt = '';
                    iResort.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });
                    hfResort.val(listChk);
                    iResort.html(listTxt);
                    break;
                case 'category':
                    var hfCategory = $("#hfCategory");
                    var iCategory = $("#txtCategory");
                    var listChk = '';
                    var listTxt = '';
                    iCategory.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });

                    hfCategory.val(listChk);
                    iCategory.html(listTxt);
                    break;
                case 'hotel':
                    var hfHotel = $("#hfHotel");
                    var iHotel = $("#txtHotel");
                    var listChk = '';
                    var listTxt = '';
                    iHotel.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });

                    hfHotel.val(listChk);
                    iHotel.html(listTxt);
                    break;
                case 'room':
                    var hfRoom = $("#hfRoom");
                    var iRoom = $("#txtRoom");
                    var listChk = '';
                    var listTxt = '';
                    iRoom.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });

                    hfRoom.val(listChk);
                    iRoom.html(listTxt);
                    break;
                case 'board':
                    var hfBoard = $("#hfBoard");
                    var iBoard = $("#txtBoard");
                    var listChk = '';
                    var listTxt = '';
                    iBoard.text('');
                    $(":checked", "#divListPopup").each(function () {
                        if (listChk.length > 0) {
                            listChk += '|';
                            listTxt += '<br />';
                        }
                        listChk += $(this).val();
                        listTxt += $(this).next().text();
                    });
                    hfBoard.val(listChk);
                    iBoard.html(listTxt);
                    break;
                default: break;
            }
            reSizeFrame();
        }

        $.extend($.ui.dialog.prototype, {
            'addbutton': function (buttonName, func) {
                var buttons = this.element.dialog('option', 'buttons');
                buttons.push({ text: buttonName, click: func });
                this.element.dialog('option', 'buttons', buttons);
            }
        });

        function showListPopupAddButton(btnName, func) {
            $("#dialog-listPopup").dialog('addbutton', btnName, func);
        }

        function showListPopup(_title, sourceBtn) {
            $("#divListPopup").html('');
            listPopupFillData(sourceBtn);
            $("#dialog").dialog("destroy");
            $("#dialog-listPopup").dialog({
                title: _title,
                modal: true,
                height: 400,
                width: 240,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        listPopupGetSelected(sourceBtn);
                        $(this).dialog('close');
                        return true;
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        $(this).dialog('close');
                        return false;
                    }
                }]
            }).css('font-size', '7px');

            if ($("#CustomRegID").val() != '0970301') {
                showListPopupAddButton(btnClear, function () {
                    $("#divListPopup").html('');
                    listPopupFillData(sourceBtn, true);
                    return false;
                });
            }

            reSizeFrame();
        }

        function getRoomInfo(setCrt) {
            var roomCount = $("#fltRoomCount").val();
            $.ajax({
                type: "POST",
                url: "onlyHotelHolPackCatFilter.aspx/getRoomInfo",
                data: '{"RoomCount":"' + roomCount + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#fltRoomInfo").html("");
                    $("#fltRoomInfo").html(msg.d);
                    reSizeFrame();
                    if (setCrt == true)
                        setCriteria();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function filterclear() {
            self.parent.pageLoad();
        }


        function setCriteria() {
            getRoomPaxs();
            var hfRoomInfo = $("#hfRoomInfo").val();
            var hfDate = $("#hfDate").val();
            var depCity = $("#fltDepCity").val();
            var arrCity = $("#fltArrCity").val();
            var resort = $("#hfResort").val();
            var category = $("#hfCategory").val();
            var hotel = $("#hfHotel").val();
            var room = $("#hfRoom").val();
            var board = $("#hfBoard").val();
            var holpack = $("#hfHolPack").val();
            var holPackCat = $("#fltHolPackCat").val();
            var dataList = '"CheckIn":"' + hfDate + '"' +
                           ',"ExpandDate":"' + $("#fltCheckInDay").val() + '"' +
                           ',"NightFrom":"' + $("#fltNight1").val() + '"' +
                           ',"NightTo":"' + $("#fltNight2").val() + '"' +
                           ',"RoomCount":"' + $("#fltRoomCount").val() + '"' +
                           ',"roomsInfo":"' + hfRoomInfo + '"' +
                           ',"Board":"' + board + '"' +
                           ',"Room":"' + room + '"' +
                           ',"DepCity":"' + depCity + '"' +
                           ',"ArrCity":"' + arrCity + '"' +
                           ',"Resort":"' + resort + '"' +
                           ',"Category":"' + category + '"' +
                           ',"Hotel":"' + hotel + '"' +
                           ',"CurControl":"' + ($("#CurrencyConvert")[0].checked ? 'Y' : 'N') + '"' +
                           ',"CurrentCur":"' + $("#currentCur").val() + '"' +
                           ',"ShowStopSaleHotels":"' + ($("#showStopSale")[0].checked ? 'Y' : 'N') + '"' +
                           ',"HolPack":"' + holpack + '"' +
                           ',"holPackCat":"' + holPackCat + '"';
            $.ajax({
                type: "POST",
                url: "onlyHotelHolPackCatFilter.aspx/SetCriterias",
                data: "{" + dataList + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function search() {
            getRoomPaxs();
            var hfRoomInfo = $("#hfRoomInfo").val();
            var hfDate = $("#hfDate").val();
            var depCity = '';
            var arrCity = $("#fltArrCity").val();
            var resort = $("#hfResort").val();
            var category = $("#hfCategory").val();
            var hotel = $("#hfHotel").val();
            var room = $("#hfRoom").val();
            var board = $("#hfBoard").val();
            var holpack = $("#hfHolPack").val();
            var holPackCat = $("#fltHolPackCat").val();
            var dataList = '"CheckIn":"' + hfDate + '"' +
                           ',"ExpandDate":"' + $("#fltCheckInDay").val() + '"' +
                           ',"NightFrom":"' + $("#fltNight1").val() + '"' +
                           ',"NightTo":"' + $("#fltNight2").val() + '"' +
                           ',"RoomCount":"' + $("#fltRoomCount").val() + '"' +
                           ',"roomsInfo":"' + hfRoomInfo + '"' +
                           ',"Board":"' + board + '"' +
                           ',"Room":"' + room + '"' +
                           ',"DepCity":"' + depCity + '"' +
                           ',"ArrCity":"' + arrCity + '"' +
                           ',"Resort":"' + resort + '"' +
                           ',"Category":"' + category + '"' +
                           ',"Hotel":"' + hotel + '"' +
                           ',"CurControl":"' + ($("#CurrencyConvert")[0].checked ? 'Y' : 'N') + '"' +
                           ',"CurrentCur":"' + $("#currentCur").val() + '"' +
                           ',"ShowStopSaleHotels":"' + ($("#showStopSale")[0].checked ? 'Y' : 'N') + '"' +
                           ',"HolPack":"' + holpack + '"' +
                           ',"holPackCat":"' + holPackCat + '"';
            $.ajax({
                type: "POST",
                url: "onlyHotelHolPackCatFilter.aspx/SetCriterias",
                data: "{" + dataList + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    self.parent.searchPrice();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            reSizeFrame();
        }

        function getStopSaleHotels(showStopSale) {
            if (showStopSale == true)
                $("#showStopSale").attr("checked", "checked");
        }

        function setTransportDays() {
            $("#ppcCheckIn").attr('disabled', 'disabled');
            $.ajax({
                type: "POST",
                url: "onlyHotelHolPackCatFilter.aspx/getTransportDays",
                data: '{"ArrCity":"' + $("#fltArrCity").val() + '","HolPackCat":"' + $("#fltHolPackCat").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    //console.log(msg);
                    //var objPopCalId = PopCalGetCalendarIndex('ppcCheckIn');
                    //objPopCal = objPopCalList[objPopCalId];
                    //objPopCal.Holidays = [];
                    //objPopCal.HolidaysCounter = 0;
                    //if (msg.hasOwnProperty('d') && msg.d != '') {
                    //    var data = $.json.decode(msg.d);
                    //    if (data != null)
                    //        $.each(data, function (i) {
                    //            PopCalAddSpecialDay(this.Day, this.Month, this.Year, this.Text, 0);
                    //        });
                    //}
                    //$("#ppcCheckIn").removeAttr('disabled');
                    if(msg.d!='')
                        dayColor = JSON.parse(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                    $("#ppcCheckIn").removeAttr('disabled');
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getFormData() {
            $.ajax({
                type: "POST",
                url: "onlyHotelHolPackCatFilter.aspx/getFormData",
                data: '{"HolPackCat":' + $("#fltHolPackCat").val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var data = $.json.decode(msg.d);
                    if (data == null || data == undefined) {
                        self.location = self.location;
                        return false;
                    }
                    if ((data.CustomRegID == '1195901') || data.ShowFilterPackage == false)
                        $("#holpackDiv").hide();
                    else $("#holpackDiv").show();
                    if (data.CustomRegID == '0970301' && data.Market == 'SWEMAR') {
                        $("#divNight2").hide();
                    }
                    $("#fltCheckInDay").html(''); $("#fltCheckInDay").html(data.fltCheckInDay);
                    $("#fltNight1").html(''); $("#fltNight1").html(data.fltNight);
                    $("#fltNight2").html(''); $("#fltNight2").html(data.fltNight2);
                    $("#fltRoomCount").html(''); $("#fltRoomCount").html(data.fltRoomCount);
                    $("#divCurrency").html('');
                    $("#divCurrency").html(data.MarketCur);
                    $("#hfDate").val('');
                    $("#hfDate").val(data.CheckIn);

                    var _date1 = new Date(parseInt(data.CheckIn) + 86400000);
                    $("#iCheckin").val($.format.date(_date1, data.DateFormat));

                    getArrival();
                    getHolpack();
                    getRoomInfo(true);
                    if (data.CustomRegID == '0970301' || data.CustomRegID == '1076601') {
                        $("#divStopSaleHotels").hide();
                    }
                    else {
                        getStopSaleHotels(data.ShowStopSaleHotels);
                    }
                    if (data.ShowOfferBtn) {
                        $("#divOffers").show();
                    }
                    setTransportDays();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changedNight1() {
            $("#fltNight2").val($("#fltNight1").val());
        }

        function changedNight2() {
            var night1 = parseInt($("#fltNight1").val());
            var night2 = parseInt($("#fltNight2").val());
            if (night1 > night2)
                $("#fltNight2").val($("#fltNight1").val());
        }

        function createOffer() {
            self.parent.createOfferV2();
        }
        function transportDays(date) {
            var noWeekend = $.datepicker.noWeekends(date);
            if (dayColor != null && dayColor.length > 0) {
                for (i = 0; i < dayColor.length; i++) {
                    var year = date.getFullYear();
                    var month = date.getMonth();
                    var day = date.getDate();
                    if (month == (parseInt(dayColor[i].Month) - 1) && day == parseInt(dayColor[i].Day) && year == parseInt(dayColor[i].Year)) {
                        return [true, 'special_day', dayColor[i].Text];
                    }
                }
                return [true, ''];
            }
            return [true, ''];
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            var HolPackCat = $.query.get('HolPackCat');
            var PackType = $.query.get('PackType');
            if (HolPackCat == undefined && HolPackCat.length == 0) HolPackCat == '';
            if (PackType == undefined && PackType.length == 0) PackType == '';
            $("#fltHolPackCat").val(HolPackCat);
            getFormData();
            $.datepicker.setDefaults($.datepicker.regional['<%= twoLetterISOLanguageName %>' != 'en' ? '<%= twoLetterISOLanguageName %>' : '']);
            $("#iCheckin").datepicker({
                showOn: "button",
                buttonImage: "Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (dateText, inst) {
                    if (dateText != '') {
                        var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                        var iDate = $("#hfDate");
                        if (date2) {
                            iDate.val(Date.parse(date2));
                        }
                        else {
                            iDate.val(Date.parse(new Date()));
                        }
                    }
                },
                minDate: new Date(),
                beforeShowDay: transportDays
            });
        });
    </script>

</head>
<body>
    <form id="SearchFilterForm" runat="server">
        <input id="fltHolPackCat" type="hidden" value="" />
        <div style="width: 250px;">
            <table cellpadding="2" cellspacing="0" style="text-align: left; width: 250px;">
                <tr>
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblArrCity") %></strong>
                        <br />
                        <select id="fltArrCity" onchange="changeArrCity();">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divCheckIn" style="width: 148px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblCheckIn") %></strong>
                            <br />
                            <%--<asp:TextBox ID="iCheckIn" runat="server" Width="100px" />--%>
                            <input type="text" id="iCheckin" style="width:100px;" />
                            <%--<rjs:PopCalendar ID="ppcCheckIn" runat="server" Control="iCheckIn" ClientScriptOnDateChanged="dateChange" />--%>
                        </div>
                        <div id="divExpandDate" style="width: 95px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblExpandDate") %></strong>
                            <br />
                            <select id="fltCheckInDay">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divNight1" style="width: 122px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblNight1") %></strong>
                            <br />
                            <select id="fltNight1" onchange="changedNight1();">
                            </select>
                        </div>
                        <div id="divNight2" style="width: 122px; float: left;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblNight2") %></strong>
                            <br />
                            <select id="fltNight2" onchange="changedNight2();">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoomCount") %></strong>
                        <br />
                        <select id="fltRoomCount" onchange="getRoomInfo(false);">
                        </select>
                        <div id="divRoomInfo" style="width: 100%;">
                            <div id="fltRoomInfo">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblResort") %></b></span>
                            <img alt="" title="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('Resort List', 'resort');" />
                        </div>
                        <div id="txtResort" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblCategory") %></b> </span>
                            <img alt="" title="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('Category List', 'category');" />
                        </div>
                        <div id="txtCategory" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblHotel") %></b></span>
                            <img alt="" title="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('Hotel List', 'hotel');" />
                        </div>
                        <div id="txtHotel" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblRoom") %></b></span>
                            <img alt="" title="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('Room List', 'room');" />
                        </div>
                        <div id="txtRoom" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%; height: 22px; position: relative; vertical-align: bottom;">
                            <span style="position: absolute; bottom: 2px;"><b>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblBoard") %></b></span>
                            <img alt="" title="" src="Images/search.png" height="22px" style="position: absolute; right: 2px;"
                                onclick="showListPopup('Board List', 'board');" />
                        </div>
                        <div id="txtBoard" class="multiField">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="holpackDiv" style="display: none;">
                            <strong>
                                <%= GetGlobalResourceObject("PackageSearchFilter", "lblHolpack")%></strong>
                            <br />
                            <select id="fltHolpack" onchange="changeHolpack();">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divCurrency" style="clear: both; width: 100%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divStopSaleHotels" style="clear: both; width: 100%;">
                            <br />
                            <input id="showStopSale" type="checkbox" /><label for="showStopSale"><%= GetGlobalResourceObject("PackageSearchFilter", "lblShowStopSaleHotels") %></label>
                            <br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="z-index: 1; text-align: center;">
                        <input type="button" name="btnSearch" value='<%= GetGlobalResourceObject("LibraryResource", "btnSearch") %>'
                            style="width: 100px;" onclick="search()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;
                    <input type="button" name="btnClear" value='<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>'
                        style="width: 100px;" onclick="filterclear()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 3px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td id="divOffers" style="z-index: 1; text-align: center; display: none;">
                        <input type="button" name="btnCreateOffer" value='<%= GetGlobalResourceObject("PackageSearchResultV2","btnCreateOffer") %>'
                            style="width: 80%;" onclick="createOffer()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;                    
                    </td>
                </tr>
            </table>
        </div>
        <div id="hiddenControls" style="display: none; visibility: hidden;">
            <input id="hfDate" type="hidden" />
            <input id="hfResort" type="hidden" />
            <input id="hfCategory" type="hidden" />
            <input id="hfHotel" type="hidden" />
            <input id="hfRoomInfo" type="hidden" />
            <input id="hfRoom" type="hidden" />
            <input id="hfBoard" type="hidden" />
            <input id="hfHolPack" type="hidden" />
        </div>
        <div id="dialog-listPopup" style="display: none;">
            <div id="divListPopup">
            </div>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
    </form>
</body>
</html>
