﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Threading;

public partial class OnlyHotelSearchBlank : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {
            Session["SearchPLData"] = null;
            Session["Criteria"] = null;            
            Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url); //"OnlyHotelSearchBlank.aspx";
            Response.Redirect("~/OnlyHotelSearch.aspx" + Request.Url.Query);
        }
    }
}
