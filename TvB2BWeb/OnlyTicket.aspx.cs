﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;

public partial class OnlyTicket : BasePage
{
    public static string twoLetterISOLanguageName = "en";
    protected string saleCur;
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;

        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";

        if (!IsPostBack)
        {
            //hfFlyDate.Value = "";
            Session["FlightData"] = null;
        }
        saleCur = UserData.SaleCur;
    }

    protected void CreateCriteria(User UserData)
    {
        OnlyTicketCriteria criteria = new OnlyTicketCriteria();

        HttpContext.Current.Session["OnlyTicketCriteria"] = criteria;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string setCriteria(long? Date, string OWRT, string CurrCheck, string Departure, string Arrival, string Days, string ReturnDays1, string ReturnDays2, string FlgClass, string CurOption, string cur, bool? AllFlight, int? FlightType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        DateTime flyDate = new DateTime(1970, 1, 1);
        flyDate = flyDate.AddDays((Convert.ToInt64(Date) / 86400000) + 1);
        int? departure = Conversion.getInt32OrNull(Departure);
        int? arrival = Conversion.getInt32OrNull(Arrival);
        Int16? days = Conversion.getInt16OrNull(Days);
        Int16? RetDay1 = Conversion.getInt16OrNull(ReturnDays1);
        Int16? RetDay2 = Conversion.getInt16OrNull(ReturnDays2);
        bool? currCheck = Conversion.getBoolOrNull(CurrCheck);
        Int16? curOption = Conversion.getInt16OrNull(CurOption);
        if (curOption.HasValue && curOption.Value == 1)
        {
            currCheck = false;
            cur = string.Empty;
        }
        new OnlyTicket().SetCriterias(flyDate, Equals(OWRT, "RT"), currCheck, departure, arrival, days, RetDay1, RetDay2, FlgClass, cur, AllFlight.HasValue && AllFlight.Value ? "1" : "0", FlightType);

        return "";
    }

    protected void SetCriterias(DateTime? FlyDate, bool? isRT, bool? currCheck, int? DepCity, int? ArrCity, Int16? days, Int16? retDay1, Int16? retDay2, string flyClass, string cur, string AllFlight, int? FlightType)
    {
        User UserData = (User)HttpContext.Current.Session["UserData"];
        OnlyTicketCriteria criteria = new OnlyTicketCriteria();
        if (HttpContext.Current.Session["OnlyTicketCriteria"] == null)
            new OnlyTicket().CreateCriteria(UserData);
        criteria = (OnlyTicketCriteria)HttpContext.Current.Session["OnlyTicketCriteria"];

        criteria.Adult = 1;
        criteria.Child = 0;
        criteria.ArrCity = ArrCity;
        criteria.B2bAllClass = true;
        criteria.B2bClass = string.Empty;
        criteria.allFlight = string.Equals(AllFlight, "1");
        string currency = string.Empty;
        currency = string.IsNullOrEmpty(cur) ? UserData.SaleCur : cur;

        Int16? saleCurOpt = new Reservation().getIndSaleCurOption(UserData);
        if (!saleCurOpt.HasValue)
            criteria.Cur = currency;
        else
        {
            if (saleCurOpt.Value == 1)
                criteria.Cur = string.Empty;
            else if (saleCurOpt.Value == 2)
                criteria.Cur = cur;
            else criteria.Cur = UserData.SaleCur;
        }
        if (currCheck.HasValue && (currCheck.Value == false))
            criteria.Cur = string.Empty;
        criteria.CurControl = currCheck;
        criteria.DepCity = DepCity;
        criteria.FlyClass = flyClass;
        criteria.FlyDateBeg = FlyDate;
        criteria.FlyDateEnd = (FlyDate.HasValue ? FlyDate.Value.AddDays(days.HasValue ? days.Value : 0) : FlyDate);
        criteria.Infant = 0;
        criteria.IsRT = isRT.HasValue ? isRT.Value : false;
        criteria.Night1 = retDay1;
        criteria.Night2 = retDay2;
        criteria.FlightType = FlightType;
        HttpContext.Current.Session["OnlyTicketCriteria"] = criteria;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static currencyOptions getTicketTypeLock()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string saleCur = string.Empty;
        currencyOptions curOpt = new currencyOptions();
        List<string> currencyList = curOpt.currencyList;
        currencyList = new List<string>();
        currencyList.Add(UserData.SaleCur);
        if (UserData.TvParams.TvParamPrice.IndSaleCurOpt.HasValue && UserData.TvParams.TvParamPrice.IndSaleCurOpt.Value == 2)
        {
            List<CurrencyRecord> currList = new Banks().getCurrencys(UserData.Market, ref errorMsg);
            if (currList != null && currList.Count() > 0)
            {
                foreach (CurrencyRecord row in currList.Where(w => w.Code != UserData.SaleCur))
                    currencyList.Add(row.Code);
            }
        }
        Int16? saleCurOpt = new Reservation().getIndSaleCurOption(UserData);
        curOpt.showCurrencyDiv = false;
        curOpt.currencyCheck = true;
        curOpt.curOption = saleCurOpt.HasValue ? saleCurOpt.Value : Convert.ToInt16(0);
        if (string.Equals(Common.crID_Jazz, UserData.CustomRegID))
            curOpt.curOption = 1;

        if (!saleCurOpt.HasValue)
        {
            curOpt.currency = UserData.SaleCur;
            curOpt.showCurrencyDiv = false;
        }
        else
        {
            if (saleCurOpt.Value == 1)
            {
                curOpt.currency = string.Empty;
                curOpt.showCurrencyDiv = true;
            }
            else
                if (saleCurOpt.Value == 2)
                {
                    curOpt.currency = UserData.SaleCur;
                    curOpt.showCurrencyDiv = true;
                }
                else
                {
                    curOpt.showCurrencyDiv = false;
                    curOpt.currency = UserData.SaleCur;
                }
        }
        curOpt.typeLock = false;
        curOpt.currencyList = currencyList;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            curOpt.days = new List<int>();
            curOpt.days = new OnlyTickets().getUPriceDays(UserData, ref errorMsg);
        }
        else
        {
            int? returnDayRange = Conversion.getInt32OrNull(new TvBo.Common().getFormConfigValue("OnlyTicket", "ReturnDayRange"));
            if (returnDayRange.HasValue)
            {
                List<int> returnDayRangeList = new List<int>();
                for (int i = 0; i <= returnDayRange; i++)
                    returnDayRangeList.Add(i);
                curOpt.days = new List<int>();
                curOpt.days = returnDayRangeList;
            }
        }

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Go2Holiday))
        {

            List<Int16> expandDays = new List<Int16>();
            for (Int16 i = 0; i <= 60; i++)
                expandDays.Add(i);
            curOpt.expandDay = new List<Int16>();
            curOpt.expandDay = expandDays;
        }

        bool? searchAllFlight = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "SearchAllFlight"));
        bool? searchAllFlightDefaultValue = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "SearchAllFlightDefaultValue"));
        curOpt.SearchAllFlight = !searchAllFlight.HasValue || (searchAllFlight.HasValue && searchAllFlight.Value);
        curOpt.SearchAllFlightDefaultValue = curOpt.SearchAllFlight && searchAllFlightDefaultValue.HasValue && searchAllFlightDefaultValue.Value ? true : false;

        curOpt.showFlyType = string.Equals(UserData.CustomRegID, Common.crID_Kusadasi_Ro);
        if (curOpt.showFlyType)
        {
            List<FlightTypeRecord> flightTypes = new Flights().getFlightTypeList(UserData, ref errorMsg);
            curOpt.flyTypeList = flightTypes;
        }

        return curOpt;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static IEnumerable getDepartureCitys(string ORT)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<Location> locations = new OnlyTickets().getFlightDepartureCity(UserData, null, ORT, ref errorMsg);
        var query = from q in locations
                    orderby (UserData.EqMarketLang ? q.NameL : q.Name)
                    select new { RecID = q.RecID, Name = (UserData.EqMarketLang ? q.NameL : q.Name) };
        return query;
        //}
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static IEnumerable getArrivalCitys(string DepCity, string ORT)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        int? depCity = Conversion.getInt32OrNull(DepCity);
        List<Location> locations = new OnlyTickets().getFlightArrivalCity(UserData, depCity, ORT, ref errorMsg);
        var query = from q in locations
                    orderby (UserData.EqMarketLang ? q.NameL : q.Name)
                    select new { RecID = q.RecID, Name = (UserData.EqMarketLang ? q.NameL : q.Name) };
        return query;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static IEnumerable getFlightClass()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<FlightClassRecord> flgClass = new Flights().getFlightClass(UserData.Market, ref errorMsg);
        var query = from q in flgClass
                    where q.B2B
                    orderby q.Class
                    select new { Class = q.Class, Name = "(" + q.Class + ") " + (UserData.EqMarketLang ? q.NameL : q.Name) };
        return query;
    }

    public static string createSearchResultHtml(FlightData flightData)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        StringBuilder sb = new StringBuilder();

        string handicaps = getFlightHandicaps(UserData, flightData);

        if (handicaps.Length > 0)
        {
            sb.Append("<div style=\"font-size: 7pt; color: red; font-family: Arial;\">");

            sb.Append(handicaps);

            sb.Append("</div>");
        }

        sb.Append("<table id=\"resultTable\"  cellpadding=\"2\" cellspacing=\"2\">");
        sb.Append("<tr class=\"headerTR\">");
        sb.Append("<td class=\"tdh1\">&nbsp;</td>");
        sb.AppendFormat("<td class=\"tdh2\">{0}</td>",
            HttpContext.GetGlobalResourceObject("OnlyTicket", "lblDeparture"));
        sb.AppendFormat("<td class=\"tdh3\">{0}</td>",
            HttpContext.GetGlobalResourceObject("OnlyTicket", "lblDepartureFlight"));
        if (flightData.FlightInfo.FirstOrDefault().ArrDate.HasValue)
        {
            sb.AppendFormat("<td class=\"tdh4\">{0}</td>",
                HttpContext.GetGlobalResourceObject("OnlyTicket", "lblReturn"));
            sb.AppendFormat("<td class=\"tdh5\">{0}</td>",
                HttpContext.GetGlobalResourceObject("OnlyTicket", "lblReturnFlight"));
        }
        sb.AppendFormat("<td class=\"tdh6\">{0}</td>",
            HttpContext.GetGlobalResourceObject("OnlyTicket", "lblClass"));
        sb.AppendFormat("<td class=\"tdh7\">{0}</td>",
            HttpContext.GetGlobalResourceObject("OnlyTicket", "lblFreeSeat"));
        sb.AppendFormat("<td class=\"tdh8\">{0}</td>",
            HttpContext.GetGlobalResourceObject("OnlyTicket", "lblAdultPrice"));
        sb.AppendFormat("<td class=\"tdh9\">{0}</td>",
            HttpContext.GetGlobalResourceObject("OnlyTicket", "lblChildPrice"));
        sb.AppendFormat("<td class=\"tdh10\">{0}</td>",
            HttpContext.GetGlobalResourceObject("OnlyTicket", "lblInfantPrice"));
        sb.AppendFormat("<td class=\"tdh11\">{0}</td>",
            HttpContext.GetGlobalResourceObject("OnlyTicket", "lblCurrency"));
        sb.Append("</tr>");
        int ii = 0;
        Int16 flightSeat = 10;
        if (UserData.TvParams.TvParamFlight.DispMinFlightAllotW.HasValue)
            flightSeat = UserData.TvParams.TvParamFlight.DispMinFlightAllotW.Value;
        string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "Version"));

        //if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt))
        //    version = string.Empty;

        foreach (OnlyFlight_FlightInfo row in flightData.FlightInfo)
        {
            ++ii;
            if ((ii / 2.0) == Convert.ToInt32(ii / 2.0))
                sb.Append("<tr class=\"alterTR\">");
            else sb.Append("<tr>");
            string bookBtn = string.Format("<input type=\"button\" value=\"{0}\" onclick=\"bookFlight({1},'{3}');\" style=\"width: 99%;\" class=\"{2}\"/>",
                                        HttpContext.GetGlobalResourceObject("OnlyTicket", "btnBook"),
                                        row.RecID,
                                        "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover",
                                        version);
            sb.AppendFormat("<td class=\"centerTD\">{0}</td>", (row.AllowRes.HasValue && row.AllowRes.Value) ? bookBtn : "&nbsp;");
            sb.AppendFormat("<td class=\"leftTD\">{0} - <b>{1}</b></td>", row.DepDate.HasValue ? row.DepDate.Value.ToShortDateString() : "&nbsp;", row.DepTime.HasValue ? row.DepTime.Value.ToShortTimeString() : "&nbsp;");
            sb.AppendFormat("<td class=\"leftTD\">{0}</td>", row.DFullName);
            if (flightData.FlightInfo.FirstOrDefault().ArrDate.HasValue)
            {
                sb.AppendFormat("<td  class=\"leftTD\">{0} - <b>{1}</td>", row.ArrDate.HasValue ? row.ArrDate.Value.ToShortDateString() : "&nbsp;", row.ArrTime.HasValue ? row.ArrTime.Value.ToShortTimeString() : "&nbsp;");
                sb.AppendFormat("<td class=\"leftTD\">{0}</td>", row.AFullName);
            }
            sb.AppendFormat("<td class=\"centerTD\">{0}</td>", row.DepClass);
            sb.AppendFormat("<td class=\"centerTD\">{0}</td>", row.FreeSeat.HasValue ? (row.FreeSeat.Value > flightSeat ? flightSeat.ToString() + "+" : (row.FreeSeat.Value < 0 ? "0" : row.FreeSeat.Value.ToString())) : "&nbsp;");
            sb.AppendFormat("<td class=\"rightTD\">{0}</td>", row.AdlTot.HasValue ? row.AdlTot.Value.ToString("#,###.00") : "&nbsp;");
            sb.AppendFormat("<td class=\"rightTD\">{0}</td>", row.ChdTot.HasValue ? row.ChdTot.Value.ToString("#,###.00") : "&nbsp;");
            sb.AppendFormat("<td class=\"rightTD\">{0}</td>", row.InfTot.HasValue ? row.InfTot.Value.ToString("#,###.00") : "&nbsp;");
            sb.AppendFormat("<td class=\"centerTD\">{0}</td>", row.MarketCurr);
            sb.Append("</tr>");
            if (row.EBPasPer.HasValue || (row.EBP_AdlVal.HasValue && row.EBP_ChdGrp1Val.HasValue && row.EBP_ChdGrp2Val.HasValue))
            {
                if ((ii / 2.0) == Convert.ToInt32(ii / 2.0))
                    sb.Append("<tr class=\"alterTR discountRow\">");
                else sb.Append("<tr class=\"discountRow\">");


                sb.AppendFormat("<td class=\"centerTD\">{0}</td>", "&nbsp;");
                sb.AppendFormat("<td class=\"leftTD\">{0}</td>", "&nbsp;");
                if (flightData.FlightInfo.FirstOrDefault().ArrDate.HasValue)
                    sb.AppendFormat("<td class=\"leftTD\">{0}</td>", "&nbsp;");
                if (flightData.FlightInfo.FirstOrDefault().ArrDate.HasValue)
                {
                    sb.AppendFormat("<td class=\"leftTD\">{0}</td>", "&nbsp;");
                }
                sb.AppendFormat("<td class=\"rightTD\" colspan=\"3\"><strong>{0}</strong> :</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "SDName_D"));
                //sb.AppendFormat("<td class=\"centerTD\">{0}</td>", "&nbsp;");
                sb.AppendFormat("<td class=\"rightTD\">{0}</td>", row.EBP_AdlVal.HasValue ? row.EBP_AdlVal.Value.ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"rightTD\">{0}</td>", row.EBP_ChdGrp2Val.HasValue ? row.EBP_ChdGrp2Val.Value.ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"rightTD\">{0}</td>", row.EBP_ChdGrp1Val.HasValue ? row.EBP_ChdGrp1Val.Value.ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"centerTD\">{0}</td>", "&nbsp;");
                sb.Append("</tr>");

                //For Total Row
                if ((ii / 2.0) == Convert.ToInt32(ii / 2.0))
                    sb.Append("<tr class=\"alterTR totalRow\">");
                else sb.Append("<tr class=\"totalRow\">");


                sb.AppendFormat("<td class=\"centerTD\">{0}</td>", "&nbsp;");
                sb.AppendFormat("<td class=\"leftTD\">{0}</td>", "&nbsp;");
                if (flightData.FlightInfo.FirstOrDefault().ArrDate.HasValue)
                    sb.AppendFormat("<td class=\"leftTD\">{0}</td>", "&nbsp;");
                if (flightData.FlightInfo.FirstOrDefault().ArrDate.HasValue)
                {
                    sb.AppendFormat("<td class=\"leftTD\">{0}</td>", "&nbsp;");
                }
                sb.AppendFormat("<td class=\"rightTD\" colspan=\"3\"><strong>{0}</strong> :</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblTotal"));
                //sb.AppendFormat("<td class=\"centerTD\">{0}</td>", "&nbsp;");
                sb.AppendFormat("<td class=\"rightTD\">{0}</td>", row.EBP_AdlVal.HasValue & row.AdlTot.HasValue ? (row.AdlTot.Value - row.EBP_AdlVal.Value).ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"rightTD\">{0}</td>", row.EBP_ChdGrp2Val.HasValue & row.ChdTot.HasValue ? (row.ChdTot.Value - row.EBP_ChdGrp2Val.Value).ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"rightTD\">{0}</td>", row.EBP_ChdGrp1Val.HasValue & row.InfTot.HasValue ? (row.InfTot.Value - row.EBP_ChdGrp1Val.Value).ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"centerTD\">{0}</td>", "&nbsp;");
                sb.Append("</tr>");
            }
        }
        sb.Append("</table>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getSearchResult(long? Date, string OWRT, string CurrCheck, string Departure, string Arrival, string Days, string ReturnDays1, string ReturnDays2, string FlgClass, string CurOption, string cur, bool? AllFlight, int? FlightType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        //DateTime? flyDate = Conversion.convertDateTime(Date, DateFormat);
        DateTime flyDate = new DateTime(1970, 1, 1);
        flyDate = flyDate.AddDays((Convert.ToInt64(Date) / 86400000) + 1);
        int? departure = Conversion.getInt32OrNull(Departure);
        int? arrival = Conversion.getInt32OrNull(Arrival);
        Int16? days = Conversion.getInt16OrNull(Days);
        Int16? RetDay1 = Conversion.getInt16OrNull(ReturnDays1);
        Int16? RetDay2 = Conversion.getInt16OrNull(ReturnDays2);
        bool? currCheck = Conversion.getBoolOrNull(CurrCheck);
        Int16? curOption = Conversion.getInt16OrNull(CurOption);
        if (curOption.HasValue && curOption.Value == 1)
        {
            currCheck = false;
            cur = string.Empty;
        }
        new OnlyTicket().SetCriterias(flyDate, Equals(OWRT, "RT"), currCheck, departure, arrival, days, RetDay1, RetDay2, FlgClass, cur, AllFlight.HasValue && AllFlight.Value ? "1" : "0", FlightType);
        Guid? recID = null;
        String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
        if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
        {
            WEBSearchLog log = new WEBSearchLog
            {
                AGENCYID = UserData.AgencyID,
                ARRCITY = arrival,
                CHECKIN = flyDate,
                CREATEDATE = DateTime.Now,
                DEPCITY = departure,
                EXPENDDATE = days,
                NIGHTFROM = RetDay1,
                NIGHTTO = RetDay2,
                PACKAGE = string.Empty,
                SALERESOURCE = (short)SaleResource.B2BTicket,
                SESSIONID = UserData.SID,
                USERID = UserData.UserID,
                ONLYTICKETOW = !Equals(OWRT, "RT")
            };

            recID = new WEBLog().saveWEBSearchLog(log, ref errorMsg);
        }
        FlightData flightData = new FlightData();
        OnlyTicketCriteria criteria = (OnlyTicketCriteria)HttpContext.Current.Session["OnlyTicketCriteria"];
        flightData = new OnlyTickets().FillFlights(UserData, flightData, criteria, ref errorMsg);
        flightData.LogID = recID;
        if (flightData.FlightInfo.Count > 0)
        {
            HttpContext.Current.Session["FlightData"] = flightData;
            return createSearchResultHtml(flightData);
        }
        else
        {
            HttpContext.Current.Session["FlightData"] = null;
            string retVal = "<div style=\"width: 950px; text-align: center; font-family:Tahoma; font-weight:bold;\">";

            List<calendarColor> flightDayS = new List<calendarColor>();
            flightDayS = criteria.FlightDays;
            DateTime? prevDate = null;
            DateTime? nextDate = null;
            var prevQ = from q in flightDayS
                        where new DateTime(q.Year.Value, q.Month.Value, q.Day.Value) < criteria.FlyDateBeg
                        select new { date = new DateTime(q.Year.Value, q.Month.Value, q.Day.Value) };
            var nextQ = from q in flightDayS
                        where new DateTime(q.Year.Value, q.Month.Value, q.Day.Value) > criteria.FlyDateBeg
                        select new { date = new DateTime(q.Year.Value, q.Month.Value, q.Day.Value) };
            if (prevQ != null && prevQ.Count() > 0)
                prevDate = prevQ.OrderBy(o => o.date).Last().date;
            if (nextQ != null && nextQ.Count() > 0)
                nextDate = nextQ.OrderBy(o => o.date).First().date;

            retVal += "<br /><br /><br /><br />";
            retVal += string.Format("<div style=\"float: left; width: 250px;\">{0}</div>",
                prevDate.HasValue ? "<img src=\"Images/Back_Arrow.png\" onclick=\"prevDate(" + prevDate.Value.Ticks + ");\" /><br />" + prevDate.Value.ToShortDateString() : "&nbsp;");
            retVal += string.Format("<div style=\"float: right; width: 250px;\">{0}</div>",
                nextDate.HasValue ? "<img src=\"Images/Next_Arrow.png\" onclick=\"nextDate(" + nextDate.Value.Ticks + ");\"/><br />" + nextDate.Value.ToShortDateString() : "&nbsp;");
            retVal += "<br /><br /><br /><br />";

            retVal += string.Format("<span style=\"font-size: 14pt; line-height: 30pt;\">{0}</span>",
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "NoRecordFound"));
            retVal += "</div";
            return retVal;
        }

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static FirstFlightRecord reSearch(long? date)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["OnlyTicketCriteria"] == null) return null;
        if (!date.HasValue) return null;
        OnlyTicketCriteria criteria = (OnlyTicketCriteria)HttpContext.Current.Session["OnlyTicketCriteria"];
        int day = criteria.FlyDateEnd.HasValue && criteria.FlyDateBeg.HasValue ? (criteria.FlyDateEnd.Value - criteria.FlyDateBeg.Value).Days : 0;
        criteria.FlyDateBeg = new DateTime(0).AddTicks(date.Value);
        criteria.FlyDateEnd = (criteria.FlyDateBeg.HasValue ? criteria.FlyDateBeg.Value.AddDays(day) : criteria.FlyDateBeg);
        HttpContext.Current.Session["OnlyTicketCriteria"] = criteria;
        FirstFlightRecord firstFlight = new FirstFlightRecord();

        Int64 days = (criteria.FlyDateBeg.Value - (new DateTime(1970, 1, 1))).Days - 1;
        firstFlight.CheckIn = (days * 86400000);
        firstFlight.DateFormat = new TvBo.Common().getDateFormat(UserData.Ci).Replace('/', '-');
        firstFlight.FlyDate = criteria.FlyDateBeg.Value.AddDays(1);

        return firstFlight;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<calendarColor> getFlightDays(string DepCity, string ArrCity, string ORT)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        int? depCity = Conversion.getInt32OrNull(DepCity);
        int? arrCity = Conversion.getInt32OrNull(ArrCity);
        List<FlightDays> flightDays = new Flights().getFlightDays(UserData, depCity, arrCity, false, "", "", "", ref errorMsg);
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            if (string.Equals(ORT, "OW"))
                flightDays = flightDays.Where(w => string.Equals(w.ORT, "OW")).ToList<FlightDays>();
            else flightDays = flightDays.Where(w => string.Equals(w.ORT, "RT")).ToList<FlightDays>();
        }
        List<Location> location = CacheObjects.getLocationList(UserData.Market);
        var flights = from q in flightDays
                      group q by new { q.FlyDate } into k
                      select new { k.Key.FlyDate };
        List<calendarColor> retVal = new List<calendarColor>();
        foreach (var row in flights)
        {
            var query1 = from q in flightDays
                         where q.FlyDate == row.FlyDate
                         select q;
            string text = string.Empty;
            foreach (var row2 in query1)
            {
                text += string.Format("{0} - ({1} -> {2}) \\n",
                                      row2.Flights,
                                      "[" + row2.DepAirport + "] " + location.Find(f => f.RecID == row2.DepCity).NameL,
                                      "[" + row2.ArrAirport + "] " + location.Find(f => f.RecID == row2.ArrCity).NameL);
            }
            retVal.Add(new calendarColor
            {
                Day = row.FlyDate.Day,
                Month = row.FlyDate.Month,
                Year = row.FlyDate.Year,
                Text = text
            });
        }
        OnlyTicketCriteria criteria = new OnlyTicketCriteria();
        if (HttpContext.Current.Session["OnlyTicketCriteria"] != null)
        {
            criteria.FlightDays = retVal;
            HttpContext.Current.Session["OnlyTicketCriteria"] = criteria;
        }
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string getFirstFlight(string DepCity, string ArrCity)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Detur)) return string.Empty;

        string errorMsg = string.Empty;
        int? depCity = Conversion.getInt32OrNull(DepCity);
        int? arrCity = Conversion.getInt32OrNull(ArrCity);
        List<FlightDays> flightDays = new Flights().getFlightDays(UserData, depCity, arrCity, false, "", "", "", ref errorMsg);
        if (flightDays != null && flightDays.Count > 0)
        {
            FirstFlightRecord firstFlight = new FirstFlightRecord();

            DateTime ppcCheckIn = flightDays.OrderBy(o => o.FlyDate).FirstOrDefault().FlyDate;
            Int64 days = (ppcCheckIn - (new DateTime(1970, 1, 1))).Days - 1;
            firstFlight.CheckIn = (days * 86400000);
            firstFlight.DateFormat = new TvBo.Common().getDateFormat(UserData.Ci).Replace('/', '-');
            firstFlight.FlyDate = ppcCheckIn;
            //Aşağısı neden yapıldığı anlaşılamadı olası bir geri dönüşe karşın korundu
            //firstFlight.FlyDate = ppcCheckIn.AddDays(1);
            return Newtonsoft.Json.JsonConvert.SerializeObject(firstFlight);
        }
        else return string.Empty;
    }

    public static string getFlightHandicaps(User UserData, FlightData data)
    {
        var depQ = from q in data.FlightInfo
                   group q by new { FlightNo = q.DepFlight, FlyDate = q.DepDate } into k
                   select k;

        var retQ = from q in data.FlightInfo
                   group q by new { FlightNo = q.ArrFlight, FlyDate = q.ArrDate } into k
                   select k;

        string errorMsg = string.Empty;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string flightHandicapStr = string.Empty;

        if ((depQ != null && depQ.Count() > 0) || (retQ != null && retQ.Count() > 0))
        {
            string handicapFlightServiceLabel = string.Format("<span class=\"handicapServiceLabel\">{0}</span><br />", HttpContext.GetGlobalResourceObject("MakeReservation", "flightHandicapLabel"));
            var flights = retQ != null ? depQ.Union(retQ) : depQ;
            foreach (var row in flights.Where(w => w.Key.FlightNo != null && w.Key.FlyDate != null))
            {
                List<HandicapsRecord> flightHandicapList = new TvBo.Common().getHandicaps(UserData, row.Key.FlyDate, null, "FLIGHT", row.Key.FlightNo, HandicapTypes.OnlyTicket, ref errorMsg);
                if (flightHandicapList != null && flightHandicapList.Count() > 0)
                {
                    flightHandicapStr += string.Format("<b>{0}</b><br />", row.Key.FlightNo);
                    foreach (HandicapsRecord r1 in flightHandicapList)
                        flightHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);
                }

                FlightDayRecord flight = new Flights().getFlightDay(UserData, row.Key.FlightNo, row.Key.FlyDate.Value, ref errorMsg);
                List<HandicapsRecord> depAiprPortHandicapList = new TvBo.Common().getHandicaps(UserData, row.Key.FlyDate, null, "AIRPORT", flight.DepAirport, HandicapTypes.OnlyTicket, ref errorMsg);
                if (depAiprPortHandicapList != null && depAiprPortHandicapList.Count() > 0)
                {
                    AirportRecord airport = new Flights().getAirport(UserData.Market, flight.DepAirport, ref errorMsg);
                    flightHandicapStr += string.Format("<b>{0}</b><br />", airport.Code + " (" + (useLocalName ? airport.LocalName : airport.Name) + ")");
                    foreach (HandicapsRecord r1 in depAiprPortHandicapList)
                        flightHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);
                }
            }
        }
        return flightHandicapStr;
    }
}