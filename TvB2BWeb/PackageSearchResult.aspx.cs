﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Services;
using System.Threading;
using System.Globalization;
using TvBo;
using TvTools;
using System.Configuration;

public partial class PackageSearchResult : BasePage
{
    protected string customJsObj = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        else if (UserData.Ci.Name.ToLower() == "lv-lv")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        string customJsObject = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "CustomJsObject"));
        if (!string.IsNullOrEmpty(customJsObject))
            customJsObj = customJsObject;
    }

    [WebMethod(EnableSession = true)]
    public static string setResultFilter(string FieldName, string Value)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["Criteria"] == null)
            return null;
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];

        if (HttpContext.Current.Session["SearchResult"] == null)
            return null;

        TvBo.MultiRoomResult searchResult = (TvBo.MultiRoomResult)HttpContext.Current.Session["SearchResult"];
        searchResult.SearchType = criteria.SType;
        ResultFilter filter = new ResultFilter();
        switch (FieldName)
        {
            case "DepCity":
                filter.DepCity = Conversion.getInt32OrNull(Value);
                break;
            case "ArrCity":
                filter.ArrCity = Conversion.getInt32OrNull(Value);
                break;
            case "HotelLocation":
                filter.HotelLocation = Conversion.getInt32OrNull(Value);
                break;
            case "HotCat":
                filter.Category = Conversion.getStrOrNull(Value);
                break;
            case "Hotel":
                filter.Hotel = Conversion.getStrOrNull(Value);
                break;
            case "CheckIn":
                filter.CheckIn = Conversion.ConvertFromJulian(Conversion.getInt64OrNull(Value));
                break;
            case "Night":
                filter.Night = Conversion.getInt16OrNull(Value);
                break;
            case "Room":
                filter.Room = Conversion.getStrOrNull(Value);
                break;
            case "Board":
                filter.Board = Conversion.getStrOrNull(Value);
                break;
        }
        filter.filtered = filter.ArrCity.HasValue ||
                          (!string.IsNullOrEmpty(filter.Board)) ||
                          (!string.IsNullOrEmpty(filter.Category)) ||
                          filter.CheckIn.HasValue ||
                          filter.DepCity.HasValue ||
                          (!string.IsNullOrEmpty(filter.Hotel)) ||
                          filter.HotelLocation.HasValue ||
                          filter.Night.HasValue ||
                          (!string.IsNullOrEmpty(filter.Room));
        searchResult.FilterResult = filter;
        searchResult.FilteredResultSearch = new List<SearchResult>();
        searchResult.PageRowCount = 20;
        HttpContext.Current.Session["SearchResult"] = searchResult;

        return getResultGrid(0, 20, true, false);
    }

    public static string createResultFilter(User UserData, TvBo.MultiRoomResult result)
    {
        List<resultFilterCoor> filterList = new List<resultFilterCoor>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        ResultFilter filter = result.FilterResult;
        List<SearchResult> filterdata = new List<SearchResult>();
        if (result.FilterResult.filtered)
            filterdata = result.FilteredResultSearch;
        else
            filterdata = result.ResultSearch;
        var Departure = from q in ((result.FilterResult.filtered || !result.FilterResult.DepCity.HasValue) ? filterdata : result.FilteredResultSearch)
                        group q by new { q.DepCity } into k
                        select new
                        {
                            Code = k.Key.DepCity,
                            Name = result.ResultSearch.Where(w => w.DepCity == k.Key.DepCity).FirstOrDefault().DepCityName,
                            NameL = result.ResultSearch.Where(w => w.DepCity == k.Key.DepCity).FirstOrDefault().DepCityNameL
                        };
        if ((Departure != null && Departure.Count() > 1) || result.FilterResult.DepCity.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Departure.OrderBy(o => (useLocalName ? o.NameL : o.Name)))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.DepCity.HasValue && filter.DepCity.Value == row.Code ? "selected=\"selected\"" : "");

            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfDeparture\" style=\"width:95%;\" onchange=\"filterResult('DepCity', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblDepCity"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfDeparture", Type = "string", FieldName = "DepCity", Value = filterValue, Width = 205 });
        }

        var Arrival = from q in ((result.FilterResult.filtered || !result.FilterResult.ArrCity.HasValue) ? filterdata : result.FilteredResultSearch)
                      group q by new { q.ArrCity } into k
                      select new
                      {
                          Code = k.Key.ArrCity,
                          Name = result.ResultSearch.Where(w => w.ArrCity == k.Key.ArrCity).FirstOrDefault().ArrCityName,
                          NameL = result.ResultSearch.Where(w => w.ArrCity == k.Key.ArrCity).FirstOrDefault().ArrCityNameL
                      };
        if ((Arrival != null && Arrival.Count() > 1) || result.FilterResult.ArrCity.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Arrival.OrderBy(o => (useLocalName ? o.NameL : o.Name)))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.ArrCity.HasValue && filter.ArrCity.Value == row.Code ? "selected=\"selected\"" : "");

            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfDeparture\" style=\"width:95%;\" onchange=\"filterResult('ArrCity', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblArrCity"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfArrival", Type = "string", FieldName = "ArrCity", Value = filterValue, Width = 205 });
        }

        var HotelLocattion = from q in (result.FilterResult.filtered || !result.FilterResult.HotelLocation.HasValue ? filterdata : result.FilteredResultSearch)
                             group q by new
                             {
                                 HotelLocation = q.HotelLocation,
                                 HolPackName = useLocalName ? q.HolPackNameL : q.HotLocationName
                             } into k
                             select new
                             {
                                 Code = k.Key.HotelLocation,
                                 Name = result.ResultSearch.Where(w => w.HotelLocation == k.Key.HotelLocation).FirstOrDefault().HotLocationName,
                                 NameL = result.ResultSearch.Where(w => w.HotelLocation == k.Key.HotelLocation).FirstOrDefault().HotLocationNameL
                             };
        if ((HotelLocattion != null && HotelLocattion.Count() > 1) || result.FilterResult.HotelLocation.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in HotelLocattion.OrderBy(o => useLocalName ? o.NameL : o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.HotelLocation.HasValue && filter.HotelLocation.Value == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfHotelLocation\" style=\"width:97%;\" onchange=\"filterResult('HotelLocation', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblResort"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfHotelLocation", Type = "int", FieldName = "HotelLocation", Value = filterValue, Width = 205 });
        }

        var Category = from q in ((result.FilterResult.filtered || string.IsNullOrEmpty(result.FilterResult.Category)) ? filterdata : result.FilteredResultSearch)
                       group q by new { q.HotCat } into k
                       select new { k.Key.HotCat };
        if ((Category != null && Category.Count() > 1) || !string.IsNullOrEmpty(result.FilterResult.Category))
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Category.OrderBy(o => o.HotCat))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.HotCat, row.HotCat, filter.Category == row.HotCat ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfCategory\" style=\"width:97%;\" onchange=\"filterResult('HotCat', this.value);\">{1}</select></div>",
                                        HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblCategory"),
                                        selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfCategory", Type = "string", FieldName = "HotCat", Value = filterValue, Width = 205 });
        }

        var Hotel = from q in ((result.FilterResult.filtered || string.IsNullOrEmpty(result.FilterResult.Hotel)) ? filterdata : result.FilteredResultSearch)
                    group q by new { q.Hotel } into k
                    select new
                    {
                        Code = k.Key.Hotel,
                        Name = result.ResultSearch.Where(w => w.Hotel == k.Key.Hotel).FirstOrDefault().HotelName,
                        NameL = result.ResultSearch.Where(w => w.Hotel == k.Key.Hotel).FirstOrDefault().HotelNameL
                    };
        if ((Hotel != null && Hotel.Count() > 1) || !string.IsNullOrEmpty(result.FilterResult.Hotel))
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Hotel.OrderBy(o => useLocalName ? o.NameL : o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.Hotel == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:305px; float: left;\"><strong>{0}</strong><br /><select id=\"rfHotel\" style=\"width:97%;\" onchange=\"filterResult('Hotel', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblHotel"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfHotel", Type = "string", FieldName = "Hotel", Value = filterValue, Width = 305 });
        }

        var CheckIn = from q in ((result.FilterResult.filtered || !result.FilterResult.CheckIn.HasValue) ? filterdata : result.FilteredResultSearch)
                      group q by new { q.CheckIn } into k
                      select new { CheckIn = k.Key.CheckIn, CheckInJul = Conversion.ConvertToJulian(k.Key.CheckIn) };
        if ((CheckIn != null && CheckIn.Count() > 1) || result.FilterResult.CheckIn.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in CheckIn.OrderBy(o => o.CheckIn))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>",
                                    row.CheckInJul,
                                    row.CheckIn.Value.ToShortDateString(),
                                    filter.CheckIn.HasValue && filter.CheckIn.Value == row.CheckIn ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:145px; float: left;\"><strong>{0}</strong><br /><select id=\"rfCheckIn\" style=\"width:97%;\" onchange=\"filterResult('CheckIn', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblCheckIn"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfCheckIn", Type = "DateTime", FieldName = "CheckIn", Value = filterValue, Width = 145 });
        }

        var Night = from q in ((result.FilterResult.filtered || !result.FilterResult.Night.HasValue) ? filterdata : result.FilteredResultSearch)
                    group q by new { q.Night } into k
                    select new { k.Key.Night };
        if ((Night != null && Night.Count() > 1) || result.FilterResult.Night.HasValue)
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Night.OrderBy(o => o.Night))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Night, row.Night, filter.Night.HasValue && filter.Night.Value == row.Night ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:65px; float: left;\"><strong>{0}</strong><br /><select id=\"rfNight\" style=\"width:97%;\" onchange=\"filterResult('Night', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblNights"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfNight", Type = "int", FieldName = "Night", Value = filterValue, Width = 65 });
        }

        var Room = from q in ((result.FilterResult.filtered || string.IsNullOrEmpty(result.FilterResult.Room)) ? filterdata : result.FilteredResultSearch)
                   group q by new { q.Room } into k
                   select new
                   {
                       Code = k.Key.Room,
                       Name = result.ResultSearch.Where(w => w.Room == k.Key.Room).FirstOrDefault().RoomName,
                       NameL = result.ResultSearch.Where(w => w.Room == k.Key.Room).FirstOrDefault().RoomNameL
                   };
        if ((Room != null && Room.Count() > 1) || !string.IsNullOrEmpty(result.FilterResult.Room))
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Room.OrderBy(o => useLocalName ? o.NameL : o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.Room == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfRoom\" style=\"width:97%;\" onchange=\"filterResult('Room', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblRoom"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfRoom", Type = "string", FieldName = "Room", Value = filterValue, Width = 205 });
        }

        var Board = from q in ((result.FilterResult.filtered || string.IsNullOrEmpty(result.FilterResult.Board)) ? filterdata : result.FilteredResultSearch)
                    group q by new { q.Board } into k
                    select new
                    {
                        Code = k.Key.Board,
                        Name = result.ResultSearch.Where(w => w.Board == k.Key.Board).FirstOrDefault().BoardName,
                        NameL = result.ResultSearch.Where(w => w.Board == k.Key.Board).FirstOrDefault().BoardNameL
                    };
        if ((Board != null && Board.Count() > 1) || !string.IsNullOrEmpty(result.FilterResult.Board))
        {
            string filterValue = string.Empty;
            string selectValue = string.Format("<option value=\"{0}\">{1}</option>", string.Empty, HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll"));
            foreach (var row in Board.OrderBy(o => useLocalName ? o.NameL : o.Name))
                selectValue += string.Format("<option value=\"{0}\" {2}>{1}</option>", row.Code, useLocalName ? row.NameL : row.Name, filter.Board == row.Code ? "selected=\"selected\"" : "");
            filterValue = string.Format("<div style=\"width:205px; float: left;\"><strong>{0}</strong><br /><select id=\"rfBoard\" style=\"width:97%;\" onchange=\"filterResult('Board', this.value);\">{1}</select></div>",
                            HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblBoard"),
                            selectValue);
            filterList.Add(new resultFilterCoor { ID = "rfBoard", Type = "string", FieldName = "Board", Value = filterValue, Width = 205 });
        }

        StringBuilder sb = new StringBuilder();
        int len = 0;
        string divRow = string.Empty;
        string divRowOuther = "<div class=\"clearfix\">";
        foreach (resultFilterCoor row in filterList)
        {
            len += row.Width.Value;
            if (len > 728)
            {
                len = 0;
                len += row.Width.Value;
                sb.Append(divRowOuther);
                sb.Append(divRow);
                sb.Append("</div>");
                divRow = string.Empty;
            }
            divRow += row.Value;
        }

        sb.Append(divRowOuther);
        sb.Append(divRow);
        sb.Append("</div>");

        sb.Insert(0, "<div style=\"width:730px;\">");
        sb.Append("</div>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getResultGrid(int page, int? pageItemCount, bool? filtered, bool? paged)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["Criteria"] == null)
            return null;
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        bool? showOffers = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "showOffers"));
        bool ExtAllotCont = false;
        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        if (extAllotControl != null)
            ExtAllotCont = (bool)extAllotControl;

        TvBo.MultiRoomResult hotelGroupRows = new TvBo.MultiRoomResult();

        if ((filtered.HasValue && filtered.Value || paged.HasValue && paged.Value) && HttpContext.Current.Session["SearchResult"] != null)
            hotelGroupRows = (TvBo.MultiRoomResult)HttpContext.Current.Session["SearchResult"];
        hotelGroupRows.SearchType = criteria.SType;
        hotelGroupRows.PageRowCount = pageItemCount.HasValue ? pageItemCount.Value : 20;
        if (page == 0)
            hotelGroupRows = getHotelGroupRowsFirst(hotelGroupRows);
        else
            hotelGroupRows = getHotelGroupRows(page);
        string currentCultureNumber = Newtonsoft.Json.JsonConvert.SerializeObject(UserData.Ci.NumberFormat);
        string currentCultureDateTime = Newtonsoft.Json.JsonConvert.SerializeObject(UserData.Ci.DateTimeFormat);
        sb.AppendFormat("<input id=\"cultureNumber\" type=\"hidden\" value='{0}' />", currentCultureNumber);
        sb.AppendFormat("<input id=\"cultureDate\" type=\"hidden\" value='{0}' />", currentCultureDateTime);

        if (hotelGroupRows == null)
        {
            List<calendarColor> flightDayS = new List<calendarColor>();
            flightDayS = criteria.FlightDays;
            DateTime? prevDate = null;
            DateTime? nextDate = null;
            if (flightDayS != null && flightDayS.Count > 0)
            {
                var prevQ = from q in flightDayS
                            where new DateTime(q.Year.Value, q.Month.Value, q.Day.Value) < criteria.CheckIn
                            select new { date = new DateTime(q.Year.Value, q.Month.Value, q.Day.Value), q.ColorType };
                var nextQ = from q in flightDayS
                            where new DateTime(q.Year.Value, q.Month.Value, q.Day.Value) > criteria.CheckIn
                            select new { date = new DateTime(q.Year.Value, q.Month.Value, q.Day.Value), q.ColorType };
                if (prevQ != null && prevQ.Count() > 0)
                    prevDate = prevQ.OrderBy(o => o.date).Last().date;
                if (nextQ != null && nextQ.Count() > 0)
                    nextDate = nextQ.OrderBy(o => o.date).First().date;
            }
            sb.Append("<div style=\"text-align: center; font-family:Tahoma; font-weight:bold;\">");
            sb.Append("<br /><br /><br /><br />");
            sb.AppendFormat("<div style=\"float: left; width: 250px;\">{0}</div>",
                prevDate.HasValue ? "<img src=\"Images/Back_Arrow.png\" onclick=\"prevDate(" + prevDate.Value.Ticks + ");\" /><br />" + prevDate.Value.ToShortDateString() : "&nbsp;");
            sb.AppendFormat("<div style=\"float: right; width: 250px;\">{0}</div>",
                nextDate.HasValue ? "<img src=\"Images/Next_Arrow.png\" onclick=\"nextDate(" + nextDate.Value.Ticks + ");\"/><br />" + nextDate.Value.ToShortDateString() : "&nbsp;");
            sb.Append("<br /><br /><br /><br />");
            sb.AppendFormat("<div style=\"clear:both;\"><span style=\"font-size: 14pt;\">{0}</span><div>",
                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "NoRecordFound"));
            sb.Append("</div>");
            return sb.ToString();
        }

        StringBuilder selectPageStr = new StringBuilder();

        #region Search Note

        string searchTextRTF = new Search().getSearchText(UserData, ref errorMsg);
        if (!string.IsNullOrEmpty(searchTextRTF))
        {
            TvTools.RTFtoHTML rtf = new RTFtoHTML();
            rtf.rtf = searchTextRTF;
            //SautinSoft.RtfToHtml rtfHtml = new SautinSoft.RtfToHtml();
            //rtfHtml.Encoding = SautinSoft.eEncoding.UTF_8;
            //rtfHtml.OutputFormat = SautinSoft.eOutputFormat.XHTML_10;
            //rtfHtml.HtmlParts = SautinSoft.eHtmlParts.Html_body;
            //rtfHtml.UseHtmlSymbols = true;

            string searchTextHtml = rtf.html();//rtfHtml.ConvertString(searchTextRTF);

            sb.Append("<div class=\"divResultNote\">");
            sb.Append(searchTextHtml);
            sb.Append("</div>");
        }
        #endregion

        #region Result filter
        if (HttpContext.Current.Session["SearchResult"] == null)
        {
            sb.Append("<div style=\"text-align: center; font-family:Tahoma; font-weight:bold;\">");
            sb.Append("<br /><br /><br /><br />");
            sb.AppendFormat("<span style=\"font-size: 14pt;\">{0}</span>",
                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "NoRecordFound"));
            sb.Append("</div");
            return sb.ToString();
        }
        TvBo.MultiRoomResult searchResult = (TvBo.MultiRoomResult)HttpContext.Current.Session["SearchResult"];
        List<SearchResult> resultSearch = searchResult.ResultSearch;
        StringBuilder rF = new StringBuilder();
        //if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        rF.Append("<div class=\"resultFilter\">");
        //else rF.Append("<div class=\"resultFilter\" style=\"display:none;\">");

        rF.Append(createResultFilter(UserData, searchResult));

        rF.Append("</div>");

        sb.Append(rF.ToString());

        #endregion

        sb.Append("<div class=\"divResultPageHeader\">");
        #region Offers
        if ((showOffers.HasValue && showOffers.Value == true) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Go2HolidayBg) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_RoyalPersia) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kartaca) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Doris) ||
            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Dallas))
            sb.Append("<div class=\"offer\">");
        else
            sb.Append("<div class=\"offer\" style=\"display:none;\">");

        bool newOffer = string.Equals(Conversion.getStrOrNull(ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2");

        if (newOffer)
        {
            sb.AppendLine("&nbsp;&nbsp");
            sb.AppendFormat("<input id=\"btnCreateOffer\" type=\"button\" value=\"{0}\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" onclick=\"createOfferV2()\" />",
                    HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "btnCreateOffer"));
            sb.AppendLine("&nbsp;&nbsp");
            //sb.AppendFormat("<input id=\"btnClearOffer\" type=\"button\" value=\"{0}\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" onclick=\"ClearOffer()\" />",
            //        HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "btnClearOffer"));
        }
        else
        {
            sb.AppendFormat("<input id=\"btnCreateOffer\" type=\"button\" value=\"{0}\" class=\"{2}\" onclick=\"createOffer();\" />&nbsp;&nbsp;<input id=\"btnClearOffer\" type=\"button\" value=\"{1}\" class=\"{2}\" onclick=\"clearOffer();\" />",
                HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "btnCreateOffer").ToString(),
                HttpContext.GetGlobalResourceObject("PackageSearchResultV2", "btnClearOffer").ToString(),
                "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover");
        }
        sb.Append("</div>");

        #endregion Offers 'For Novatouras

        sb.Append("</div>");

        if (hotelGroupRows.TotalPageCount > 1)
        {
            selectPageStr.Append("<div class=\"divSelectPage\">");
            selectPageStr.Append("<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">");
            selectPageStr.Append("<tr><td valign=\"middle\" style=\"width: 100px;\" align=\"right\" nowrap=\"nowrap\">");
            selectPageStr.AppendFormat("<b><span id=\"MessageLabel\">{0}</span></b></td>", HttpContext.GetGlobalResourceObject("PackageSearchResult", "SelectAPage"));
            selectPageStr.Append("<td>");
            selectPageStr.Append("<select id=\"pageDropDownListTop\" onchange=\"pageChange('');\">");
            for (int jj = 0; jj < hotelGroupRows.TotalPageCount; jj++)
            {
                if ((jj + 1 == page) && (page != 0))
                    selectPageStr.AppendFormat("<option value=\"{0}\" selected=\"selected\">{1}</option>", (jj + 1).ToString(), (jj + 1).ToString());
                else
                    selectPageStr.AppendFormat("<option value=\"{0}\">{1}</option>", (jj + 1).ToString(), (jj + 1).ToString());
            }
            selectPageStr.Append("</select>");
            selectPageStr.Append("</td>");
            selectPageStr.Append("<td style=\"text-align: right\" valign=\"middle\" nowrap=\"nowrap\">");
            selectPageStr.Append("<span id=\"CurrentPageLabel\"></span></td>");
            selectPageStr.Append("<td nowrap=\"nowrap\"><span id=\"lblPageInfo\"></span></td>");

            selectPageStr.AppendFormat("<td nowrap=\"nowrap\" align=\"right\"><span onclick=\"pageChange('-1');\" style=\"cursor:pointer; text-decoration:underline;\">{0}</span>&nbsp;&nbsp;<span onclick=\"pageChange('1');\" style=\"cursor:pointer; text-decoration:underline;\">{1}</span></td>",
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "PrevPage"),
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "NextPage"));
            selectPageStr.Append("<td style=\"width:10px;\">&nbsp;</td>");
            selectPageStr.Append("</tr></table></div>");

            sb.Append(selectPageStr.ToString());
        }
        List<flyHandicaps> flyHandicaps = new List<flyHandicaps>();
        string onlyHotelVersion = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "OnlyHotelVersion"));
        if (criteria.SType != SearchType.OnlyHotelSearch && !string.Equals(onlyHotelVersion, "V2"))
            flyHandicaps = getFlightHandicaps(UserData, hotelGroupRows.ResultSearch);
        List<HotelHandicapRecord> hotelHandicaps = CacheObjects.GetAllHotelHandikaps(UserData.Market);

        //List<HandicapsRecord> holpackHandicaps = new TvBo.Common().getHandicaps(UserData, hotelGroupRows.ResultSearch[0].CheckIn, hotelGroupRows.ResultSearch[0].CheckOut, "HOLPACK", hotelGroupRows.ResultSearch[0].HolPack, HandicapTypes.Package, ref errorMsg);
        List<HandicapsRecord> holpackHandicaps = new TvBo.Common().getHandicaps(UserData, hotelGroupRows.ResultSearch[0].CheckIn, hotelGroupRows.ResultSearch[0].CheckOut, "HOLPACK", null, HandicapTypes.Package, ref errorMsg);

        StringBuilder handicaps = new StringBuilder();

        if (flyHandicaps.Count > 0)
        {
            sb.Append("<div style=\"font-size: 7pt; background-color: #FFFFFF; color: #FF0000; font-family: Arial;\">");
            foreach (var row in flyHandicaps)
            {
                sb.AppendFormat("<span style=\"background-color: #FFFFFF;color: #FF0000;\">{0}</span><br />", row.Handicaps);
            }
            sb.Append("</div>");
        }
        sb.Append("<table class=\"resultGrid\">");

        bool? showHotelImage = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowHotelImages"));
        bool? _showTransportPlate = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowTransportPlate"));
        bool showTransportPlate = _showTransportPlate.HasValue ? _showTransportPlate.Value : false;
        string hotelImagesUrl = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "HotelImagesUrl"));
        List<HotelImageUrl> hotelImgUrl = string.IsNullOrEmpty(hotelImagesUrl) ? new List<HotelImageUrl>() : Newtonsoft.Json.JsonConvert.DeserializeObject<List<HotelImageUrl>>(hotelImagesUrl);

        HotelImageUrl hotelImg = hotelImgUrl.Find(f => f.Market == UserData.Market);
        int i = 0;
        string priceReturnStr = string.Empty;
        bool onlyBus = false;
        if (hotelGroupRows.FlightInfo != null && hotelGroupRows.FlightInfo.Count == 0)
            onlyBus = true;
        foreach (TvBo.MultiRoomSelection row in hotelGroupRows.FirstGroup.OrderBy(o => o.MinPrice).ThenBy(o => o.OrderDisplay))
        {
            if (row.RoomPriceList != null)
            {
                string detailRow = getHotelCheckInNightRows(row.RoomPriceList, i, ExtAllotCont, hotelHandicaps, showHotelImage.HasValue && showHotelImage.Value, hotelImg != null ? hotelImg.Url : string.Empty, holpackHandicaps, onlyBus,showTransportPlate).ToString();
                if (!Equals(detailRow, "<table class=\"PriceDetail\"></table>") && !string.IsNullOrEmpty(detailRow))
                {
                    ++i;
                    priceReturnStr += string.Format("<tr><td>{0}</td></tr>", detailRow);
                }
            }
        }

        if (priceReturnStr.Length > 0)
        {
            sb.Append(priceReturnStr);
        }

        sb.Append("</table>");
        if (selectPageStr.Length > 0)
            sb.Append(selectPageStr.ToString().Replace("<select id=\"pageDropDownListTop\" onchange=\"pageChange('');\">", "<select id=\"pageDropDownListButtom\" onchange=\"pageChangeButtom('');\">"));
        return sb.ToString();

    }

    public static StringBuilder getHotelCheckInNightRows(List<int> hotelRoomPriceList, int resultGridRow, bool extAllotControl, List<HotelHandicapRecord> hotelHandicaps, bool showHotelImage, string hotelImageUrl, List<HandicapsRecord> holpackHandicaps, bool onlyBus, bool showTransportPlate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        StringBuilder sb = new StringBuilder();
        if (HttpContext.Current.Session["SearchResult"] == null)
            return sb;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        bool? showOffers = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "showOffers"));

        sb.AppendFormat("<table id=\"tablePrice_{0}\" class=\"PriceTable {1}\" >",
            resultGridRow.ToString(),
            (int)(resultGridRow / 2.0) == (resultGridRow / 2.0) ? "even" : "odd");
        TvBo.MultiRoomResult searchResult = (MultiRoomResult)HttpContext.Current.Session["SearchResult"];
        List<TvBo.SearchResult> priceList = searchResult.ResultSearch;
        List<TvBo.SearchResult> hotelPriceList = (from q in priceList
                                                  join q1 in hotelRoomPriceList on q.RefNo equals q1
                                                  where q.Calculated == true
                                                  select q).ToList<TvBo.SearchResult>();

        //List<BrochureCheckRecord> brochureCheck = searchResult.brochureCheck;

        decimal TotalPrice = 0;
        string TotalPriceCur = string.Empty;
        bool differentPriceList = false;
        bool StopSale = false;
        bool StopFlight = false;

        
        StringBuilder hotelCheckInNightRoomRows = getHotelCheckInNightRoomRows(hotelPriceList, resultGridRow, ref TotalPrice, ref TotalPriceCur, ref differentPriceList, ref StopSale, ref StopFlight, extAllotControl, hotelHandicaps, holpackHandicaps);

        if (Equals(hotelCheckInNightRoomRows.ToString(), "<table class=\"PriceDetail\" cellpadding=\"0\" cellspacing=\"0\"></table>") ||
            Equals(hotelCheckInNightRoomRows.ToString(), "<table class=\"PriceDetail\"></table>"))
            return hotelCheckInNightRoomRows;

        sb.Append("<tr><td class=\"td\">");
        sb.Append("<table class=\"PriceHeader\" cellpadding=\"0\" cellspacing=\"0\">");
        sb.Append("<tr>");
        sb.Append("<td class=\"totalInfo\">");

        #region show Hotel image
        if (showHotelImage && !string.IsNullOrEmpty(hotelImageUrl))
        {
            sb.Append("<div style=\"width: 99px;float: left;\">");
            sb.AppendFormat("<img src=\"{0}\" style=\"width: 98px;\"/>", hotelImageUrl + "/" + hotelPriceList.FirstOrDefault().HotelRecID.ToString() + (string.Equals(UserData.CustomRegID, Common.crID_Detur) ? "" : "/0_T.jpg"));
            sb.Append("</div>");
        }
        #endregion

        sb.Append("<div style=\"float: left; margin-left: 5px;\">");
        string hotelName = (useLocalName ? hotelPriceList[0].HotelNameL : hotelPriceList[0].HotelName) + " (" + hotelPriceList[0].HotCat + ")";
        string hotelNameUrl = getHotelUrl(searchResult, hotelPriceList[0].Hotel);
        string hotelUrl = string.IsNullOrEmpty(hotelNameUrl) ? hotelName : ("<span onclick=\"showHotelInfo('" + hotelNameUrl + "');\" style=\"cursor: pointer;\">" + hotelName + "</span>");

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && !string.IsNullOrEmpty(UserData.AgencyRec.CIF))
        {
            hotelUrl = string.IsNullOrEmpty(hotelNameUrl) ? hotelName : ("<span onclick=\"showHotelInfo('" + hotelNameUrl + UserData.AgencyRec.CIF + "');\" style=\"cursor: pointer;\">" + hotelName + "</span>");
        }

        if (searchResult.SearchType == SearchType.TourPackageSearch && string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet))
        {
            hotelName = hotelPriceList[0].Description;
            hotelNameUrl = string.Empty;
            hotelUrl = hotelName;
        }
        sb.AppendFormat("<span class=\"headerHotelName\"><b>{0}</b></span><br />", hotelUrl);
        if (searchResult.SearchType == SearchType.TourPackageSearch && string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet))
            sb.AppendFormat("<span class=\"headerHotelLocation\">{0}</span><br />", useLocalName ? hotelPriceList[0].HotelNameL : hotelPriceList[0].HotelName);
        else
            sb.AppendFormat("<span class=\"headerHotelLocation\">{0}</span><br />", useLocalName ? hotelPriceList[0].HotLocationNameL : hotelPriceList[0].HotLocationName);

        TvBo.SearchResult firstPrice = hotelPriceList.FirstOrDefault();

        sb.AppendFormat("<span class=\"headerCheckInOut\">{0}</span>&nbsp;-&nbsp;<span class=\"headerCheckInOut\">{1}</span>&nbsp;&nbsp;<span class=\"headerNight\">{2} {3}</span><br />",
            hotelPriceList[0].CheckIn.Value.ToShortDateString(),
            hotelPriceList[0].CheckOut.Value.ToShortDateString(),
            hotelPriceList[0].HotNight.HasValue ? hotelPriceList[0].HotNight.ToString() : hotelPriceList[0].Night.ToString(),
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblNights"));

        if (searchResult.SearchType == SearchType.PackageSearch || searchResult.SearchType == SearchType.TourPackageSearch)
            sb.AppendFormat("<span class=\"headerDepArr\">{0}</span>&nbsp;-&nbsp;<span class=\"headerDepArr\">{1}</span><br />",
                useLocalName ? hotelPriceList[0].DepCityNameL : hotelPriceList[0].DepCityName,
                useLocalName ? hotelPriceList[0].ArrCityNameL : hotelPriceList[0].ArrCityName);

        sb.Append("</div>");
        sb.Append("</td>");

        sb.Append("<td class=\"availtd\">");
        Int16 flightSeat = 10;
        if (UserData.TvParams.TvParamFlight.DispMinFlightAllotW.HasValue)
            flightSeat = UserData.TvParams.TvParamFlight.DispMinFlightAllotW.Value > 0 ? UserData.TvParams.TvParamFlight.DispMinFlightAllotW.Value : (short)10;

        string tmpDepTransports = string.Empty;
        if (searchResult.SearchType == SearchType.PackageSearch || string.Equals(UserData.CustomRegID, Common.crID_FilipTravel) || (firstPrice.TransportType.HasValue && firstPrice.TransportType != 0))
        {
            if (!UserData.ShowFlight && !extAllotControl)
            {

                string errorMsg = string.Empty;
                string dRedTransImg = "DFlightRed.gif";
                string rRedTransImg = "RFlightRed.gif";
                string dGreenTransImg = "DFlightGreen.gif";
                string rGreenTransImg = "RFlightGreen.gif";

                if (firstPrice.DepServiceType != "FLIGHT")
                {
                    dRedTransImg = "BusRed.png?v=201312161108";
                    dGreenTransImg = "BusGreen.png?v=201312161108";
                }
                if (firstPrice.RetServiceType != "FLIGHT")
                {
                    rRedTransImg = "BusRed.png?v=201312161108";
                    rGreenTransImg = "BusGreen.png?v=201312161108";
                }
                //Burada NOVA'dan dolayı düzenlemeye gidildi.16864 HHTOPBAS
                //if (hotelPriceList.Where(w => w.TransportType.HasValue && w.TransportType.Value == 2).Count() > 0)
                //{
                //    dRedTransImg = "BusRed.png?v=201312161108";
                //    dGreenTransImg = "BusGreen.png?v=201312161108";
                //    rRedTransImg = "BusRed.png?v=201312161108";
                //    rGreenTransImg = "BusGreen.png?v=201312161108";
                //}
                sb.Append("<div class=\"ui-helper-clearfix\" style=\"height:40px;\">");
                string tmpDepFlights = getFlightsInfo(hotelPriceList, resultGridRow).ToString();
                //burada daha önce pricelist üzeirnden gelen ilk index alınıyordu
                //değişiklik yaparak depseat ve repseat değerlerinin 0dan büyük olması 
                //ama bu şekilde list nullable duruum hatası alındı
                //TODO: Departure Seat Problem
                //int useableIndex = hotelPriceList.FindIndex(w => w.DepSeat.Value > 0 && w.RetSeat.Value > 0);

                tmpDepTransports = hotelPriceList[0].TransportType == 2 ? new Transports().getBusForCatPackSer(hotelPriceList[0].CatPackID, hotelPriceList[0].CheckIn.Value, ref errorMsg) : "";
                int depSeat = hotelPriceList[0].DepSeat.HasValue ? (hotelPriceList[0].DepSeat.Value > 0 ? hotelPriceList[0].DepSeat.Value : 0) : 0;
                sb.AppendFormat("<div class=\"flightAllot {3}\"><img src=\"Images/{2}\" title=\"{0}\"><br><span>{1}</span></div>",
                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "DepartureFlightAvaillable"),
                    depSeat > flightSeat ? flightSeat.ToString() + "+" : depSeat.ToString(),
                    depSeat > 0 ? dGreenTransImg : dRedTransImg,
                    (int)(resultGridRow / 2.0) == (resultGridRow / 2.0) ? "even" : "odd");

                int retSeat = hotelPriceList[0].RetSeat.HasValue ? (hotelPriceList[0].RetSeat.Value > 0 ? hotelPriceList[0].RetSeat.Value : 0) : 0;
                if (!hotelPriceList[0].RetSeat.HasValue && firstPrice.TransportType == 2)
                    sb.Append("<div>&nbsp;</div>");
                else
                    sb.AppendFormat("<div class=\"flightAllot {3}\"><img src=\"Images/{2}\" title=\"{0}\"><br><span>{1}</span></div>",
                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "ReturnFlightAvaillable"),
                        retSeat > flightSeat ? flightSeat.ToString() + "+" : retSeat.ToString(),
                        retSeat > 0 ? rGreenTransImg : rRedTransImg,
                        (int)(resultGridRow / 2.0) == (resultGridRow / 2.0) ? "even" : "odd");
                sb.Append("</div>");
                if (!string.IsNullOrEmpty(tmpDepTransports) && showTransportPlate)
                {
                    sb.Append("<div class=\"ui-helper-clearfix\">");
                    sb.AppendFormat("<span class=\"plate\">{0}</span>", tmpDepTransports);
                    sb.Append("</div>");
                }
            }
            else
                sb.Append("&nbsp;");
        }
        else
            sb.Append("&nbsp;");
        sb.Append("</td>");

        sb.Append("<td  class=\"bookCell\">");
        ////////////////////////////////////////////////////////////////
        string clientOfferVersion = Conversion.getStrOrNull(ConfigurationManager.AppSettings["ClientOfferVersion"]);

        string addOfferStr = string.Format("<div style=\"float:left;\"><img alt=\"{0}\" title= \"{1}\" src=\"Images/addofferA.png\" height=\"30px\" width=\"37px\" onclick=\"addOffer({2});\" /></div>",
                                           HttpContext.GetGlobalResourceObject("PackageSearchResult", "btnAddOfferBasket"),
                                           HttpContext.GetGlobalResourceObject("PackageSearchResult", "btnAddOfferBasket"),
                                           resultGridRow.ToString());
        if (string.Equals(clientOfferVersion, "V3"))
        {

            addOfferStr = string.Format("<div style=\"float:left;\"><img alt=\"{5}\" title= \"{6}\" src=\"Images/addofferA.png\" height=\"30px\" width=\"37px\" onclick=\"addOffer({7});\" /><br /><img alt=\"{0}\" title= \"{1}\" src=\"Images/info6.png\" height=\"30px\" width=\"37px\" onclick=\"addHotelOffer('{2}',{3},{4});\" /></div>",
                                           HttpContext.GetGlobalResourceObject("PackageSearchResult", "btnOffer"),
                                           HttpContext.GetGlobalResourceObject("PackageSearchResult", "btnOffer"),
                                           hotelPriceList[0].Hotel,
                                           hotelPriceList[0].CheckIn.Value.Ticks,
                                           hotelPriceList[0].Night,
                                           HttpContext.GetGlobalResourceObject("PackageSearchResult", "btnAddOfferBasket"),
                                           HttpContext.GetGlobalResourceObject("PackageSearchResult", "btnAddOfferBasket"),
                                           resultGridRow.ToString());
        }
        sb.AppendFormat("{3}<div style=\"float:right;\"><span id=\"{0}\" class=\"price\">{1}&nbsp;{2}</span>",
                    "HotelCheckInNightPrice_" + resultGridRow.ToString(),
                    TotalPrice>0? TotalPrice.ToString("#,###.00"):"",
                    TotalPriceCur,
                    (showOffers.HasValue && showOffers.Value == true) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kartaca) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Go2HolidayBg) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Dallas) ? addOfferStr : "");

        if (differentPriceList)
        {
            sb.AppendFormat("<br /><a class=\"divNote\" href=\"#\" title=\"{0}|{1}\">{2}</a>",
                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "BookNote"),
                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "PriceListNotEquals"),
                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "Note"));
        }
        else
        {
            if (StopSale)
            {
                sb.AppendFormat("<br /><span class=\"bookButton\"><input id=\"btnBook_{0}\" name=\"btnBook_{1}\" type=\"button\" value=\"{2}\" onclick=\"book({3})\" class=\"{4}\" disabled=\"disabled\" /></span>",
                            resultGridRow.ToString(),
                            resultGridRow.ToString(),
                            HttpContext.GetGlobalResourceObject("PackageSearchResult", "stopSale"),
                            resultGridRow.ToString(),
                            "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover");
            }
            else
            {
                if (StopFlight)
                    sb.AppendFormat("<br /><span class=\"bookButton\"><input id=\"btnBook_{0}\" name=\"btnBook_{1}\" type=\"button\" value=\"{2}\" onclick=\"book({3})\" class=\"{4}\" disabled=\"disabled\" /></span>",
                            resultGridRow.ToString(),
                            resultGridRow.ToString(),
                            HttpContext.GetGlobalResourceObject("PackageSearchResult", "book"),
                            resultGridRow.ToString(),
                            "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover ui-state-disabled");
                else
                    sb.AppendFormat("<br /><span class=\"bookButton\"><input id=\"btnBook_{0}\" name=\"btnBook_{1}\" type=\"button\" value=\"{2}\" onclick=\"book({3})\" class=\"{4}\" /></span>",
                            resultGridRow.ToString(),
                            resultGridRow.ToString(),
                            HttpContext.GetGlobalResourceObject("PackageSearchResult", "book"),
                            resultGridRow.ToString(),
                            "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover");
            }
        }
        sb.Append("</div></td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("</td></tr>");

        sb.AppendFormat("<tr><td>{0}</td></tr>", hotelCheckInNightRoomRows.ToString());

        sb.Append("</table>");
        ;
        return sb;
    }

    public static StringBuilder getFlightsInfo(List<SearchResult> hotelPriceList, int resultGridRow)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        StringBuilder sb = new StringBuilder();
        List<ReservationFlightInfoRecord> flihgts = new List<ReservationFlightInfoRecord>();
        var roomGroups = from q in hotelPriceList
                         group q by new { RoomNr = q.RoomNr } into k
                         select new { RoomNr = k.Key.RoomNr };
        foreach (var r in roomGroups.OrderBy(o => o.RoomNr))
        {
            List<SearchResult> roomsPrice = (from q in hotelPriceList
                                             where q.RoomNr == r.RoomNr
                                             orderby q.LastPrice
                                             select q).ToList<SearchResult>();

            foreach (TvBo.SearchResult row in roomsPrice)
            {
                flihgts.Add(new ReservationFlightInfoRecord
                {
                    DepFlight = row.UseDynamicFlights ? row.DynamicFlights.DFlightNo : row.DepFlight,
                    DepDate = row.CheckIn,
                    DepSeat = row.UseDynamicFlights ? row.DynamicFlights.DFUnit1 : row.DepSeat,
                    RetFlight = row.UseDynamicFlights ? row.DynamicFlights.RFlightNo : row.RetFlight,
                    RetDate = row.CheckOut,
                    RetSeat = row.UseDynamicFlights ? row.DynamicFlights.RFUnit1 : row.RetSeat
                });
            }
        }

        var query = from q in flihgts
                    group q by new { q.DepFlight, q.DepDate, q.DepSeat, q.RetFlight, q.RetDate, q.RetSeat } into k
                    select new { k.Key.DepFlight, k.Key.DepDate, k.Key.DepSeat, k.Key.RetFlight, k.Key.RetDate, k.Key.RetSeat };


        return sb;
    }

    public static StringBuilder getHotelCheckInNightRoomRows(List<SearchResult> hotelPriceList, int resultGridRow,
                                            ref decimal TotalPrice, ref string TotalPriceCur, ref bool differentPriceList,
                                            ref bool StopSale, ref bool StopFlight, bool extAllotControl, List<HotelHandicapRecord> hotelHandicaps, List<HandicapsRecord> packHandicaps)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];
        TvBo.MultiRoomResult searchResult = (MultiRoomResult)HttpContext.Current.Session["SearchResult"];

        bool? showOldPrice = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowOldPrice"));
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        Int16 flightSeat = 10;
        if (UserData.TvParams.TvParamFlight.DispMinFlightAllotW.HasValue)
            flightSeat = UserData.TvParams.TvParamFlight.DispMinFlightAllotW.Value;

        StringBuilder sb = new StringBuilder();

        var roomGroups = from q in hotelPriceList
                         where q.Calculated
                         group q by new { RoomNr = q.RoomNr } into k
                         select new { RoomNr = k.Key.RoomNr };
        TotalPrice = 0;
        int? priceList = 0;
        bool firstpriceListNo = true;

        var depFlightDetail = from q in hotelPriceList
                              group q by new { FlightNo = q.UseDynamicFlights ? q.DynamicFlights.DFlightNo : q.DepFlight, FlyDate = q.CheckIn } into k
                              select new { k.Key.FlightNo, k.Key.FlyDate };
        var retFlightDetail = from q in hotelPriceList
                              group q by new { FlightNo = q.UseDynamicFlights ? q.DynamicFlights.RFlightNo : q.RetFlight, FlyDate = q.CheckOut } into k
                              select new { k.Key.FlightNo, k.Key.FlyDate };
        List<flyInfo> depFly = new List<flyInfo>();
        List<flyInfo> retFly = new List<flyInfo>();
        if (UserData.ShowFlight)
        {
            var _depFly = from q in depFlightDetail
                          select new flyInfo
                          {
                              FlightNo = q.FlightNo,
                              FlyDate = q.FlyDate,
                              Html = getFlightInfoA(searchResult, q.FlightNo, q.FlyDate)
                          };
            if (_depFly != null && _depFly.Count() > 0)
                depFly = _depFly.ToList<flyInfo>();
            var _retFly = from q in retFlightDetail
                          select new flyInfo
                          {
                              FlightNo = q.FlightNo,
                              FlyDate = q.FlyDate,
                              Html = getFlightInfoA(searchResult, q.FlightNo, q.FlyDate)
                          };
            if (_retFly != null && _retFly.Count() > 0)
                retFly = _retFly.ToList<flyInfo>();
        }

        sb.Append("<table class=\"PriceDetail\">");
        List<BrochureCheckRecord> brochureCheck = searchResult.brochureCheck;
        foreach (var r in roomGroups.OrderBy(o => o.RoomNr))
        {

            List<SearchResult> roomsPrice = (from q in hotelPriceList
                                             where q.RoomNr == r.RoomNr && q.Calculated
                                             orderby q.UseDynamicFlights ? q.DynamicFlights.Price : q.LastPrice
                                             select q).ToList<SearchResult>();

            SearchResult firstRoom = roomsPrice.FirstOrDefault();

            string AccomFullStr = string.Empty;
            var query = from q in hotelPriceList
                        where q.RoomNr == r.RoomNr && q.Calculated
                        group q by new { AccomFullName = q.AccomFullName } into k
                        select new { AccomFullName = k.Key.AccomFullName };
            foreach (var rr1 in query.AsEnumerable())
                AccomFullStr += rr1.AccomFullName + "<br />";

            sb.Append("<tr>");
            sb.Append("<td valign=\"top\">");
            sb.AppendFormat("<br /><strong>{0} {1} : {2}</strong><br />",
                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoom"),
                    r.RoomNr.ToString(),
                    AccomFullStr);

            StringBuilder handicaps = new StringBuilder();

            if (packHandicaps != null && packHandicaps.Count > 0 && !string.Equals(UserData.CustomRegID, Common.crID_UpJet))
            {
                var holHandikap = packHandicaps.Where(w => w.Code == firstRoom.HolPack);
                if(holHandikap!=null && holHandikap.Any())
                {
                    string holpackHandicapStr =!string.IsNullOrEmpty(firstRoom.HolPackName) || !string.IsNullOrEmpty(firstRoom.HolPackNameL)? string.Format("<b>{0}</b><br />", useLocalName ? firstRoom.HolPackNameL : firstRoom.HolPackName):"";
                    //foreach (HandicapsRecord r1 in packHandicaps)
                    //    holpackHandicapStr += string.Format("{0}<br />", useLocalName ? r1.Name : r1.Name);
                    foreach (HandicapsRecord r1 in holHandikap)
                        holpackHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);

                    if (holpackHandicapStr.Length > 0)
                    {
                        sb.Append("<div style=\"font-size: 7pt; color: #FF0000; background-color: #FFFFFF; font-family: Arial; border: solid 1px #FF0000;\">");
                        sb.Append(holpackHandicapStr);
                        sb.Append("</div>");
                    }
                }
                
            }
            if (hotelHandicaps.Count > 0)
            {
                string hotelHandicapStr = string.Empty;
                foreach (var row in hotelHandicaps.Where(w => string.Equals(w.Hotel, hotelPriceList[0].Hotel) && ((w.BegDate <= hotelPriceList[0].CheckIn && w.EndDate >= hotelPriceList[0].CheckIn) ||(w.BegDate <= hotelPriceList[0].CheckOut && w.EndDate >= hotelPriceList[0].CheckOut))))
                {
                    hotelHandicapStr += string.Format("{0}<br />", row.NameL.ToString());
                }
                if (hotelHandicapStr.Length > 0)
                {
                    sb.Append("<div style=\"font-size: 7pt; color: #FF0000; background-color: #FFFFFF; font-family: Arial; border: solid 1px #FF0000;\">");
                    sb.Append(hotelHandicapStr);
                    sb.Append("</div>");
                }
            }

            sb.Append("<table class=\"prices\">");
            // HttpContext.GetGlobalResourceObject("PackageSearchResult", "NoRecordFound")
            int columnCount = 0;
            if (criteria.SType == SearchType.PackageSearch || criteria.SType == SearchType.TourPackageSearch || (firstRoom.TransportType.HasValue && firstRoom.TransportType != 0))
            {
                #region Package Search
                if (!UserData.ShowFlight) // Total Columns 7
                {
                    #region Show Flight Allot Details
                    columnCount = 7;
                    sb.Append("<tr class=\"header\">");
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    {
                        sb.Append("<td>&nbsp;</td>");
                        columnCount = 8;
                    }
                    sb.AppendFormat("<td>&nbsp;</td><td><b>{0}</b></td><td><b>{1}</b></td><td><b>{2}</b></td><td><b>{3}</b></td><td align=\"center\"><b>{4}</b></td><td>&nbsp;</td>",
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFreeRoom"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblBoardName"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoomName"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblPrice"));
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    {
                        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB"));
                        columnCount = 9;
                    }
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Apollo) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro))
                    {
                        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB"));
                        columnCount = 9;
                    }
                    sb.Append("</tr>");
                    #endregion
                }
                else
                    if (!extAllotControl)
                    {
                        #region Total allot control
                        string freeRoom = string.Format("<img title=\"{0}\" src=\"Images/freeRoom.gif\" width=\"16\" height=\"16\" />",
                                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFreeRoom"));
                        string depSeat = string.Format("<img title=\"{0}\" src=\"Images/depFlight.gif\" width=\"16\" height=\"16\" />",
                                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblDepFreeSeat"));
                        string retSeat = string.Format("<img title=\"{0}\" src=\"Images/arrFlight.gif\" width=\"16\" height=\"16\" />",
                                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRetFreeSeat"));
                        // Total Columns 10
                        columnCount = 10;
                        sb.Append("<tr class=\"header\">");
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                        {
                            sb.Append("<td>&nbsp;</td>");
                            columnCount = 11;
                        }
                        sb.AppendFormat("<td>&nbsp;</td><td><b>{0}</b></td><td><b>{1}</b></td><td><b>{2}</b></td><td><b>{3}</b></td><td><b>{4}</b></td><td><b>{5}</b></td><td><b>{6}</b></td><td><b>{7}</b></td><td align=\"center\"><b>{8}</b></td><td>&nbsp;</td>",
                                        freeRoom,
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblDepFlight"),
                                        depSeat,
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblReturnFlight"),
                                        retSeat,
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblBoardName"),
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoomName"),
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom"),
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblPrice"));
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                        {
                            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB"));
                            columnCount = 12;
                        }
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Apollo) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro))
                        {
                            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB"));
                            columnCount = 12;
                        }
                        sb.Append("</tr>");
                        #endregion
                    }
                    else
                    {
                        #region Not allotment control
                        // Total Columns 6
                        columnCount = 6;
                        sb.Append("<tr class=\"header\">");
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                        {
                            sb.Append("<td>&nbsp;</td>");
                            columnCount = 7;
                        }
                        sb.AppendFormat("<td>&nbsp;</td><td><b>{0}{1}</b></td><td><b>{2}</b></td><td><b>{3}</b></td><td align=\"center\"><b>{4}</b></td><td>&nbsp;</td>",
                                    "",
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblBoardName"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoomName"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblPrice"));
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                        {
                            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB"));
                            columnCount = 8;
                        }
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Apollo) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro))
                        {
                            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB"));
                            columnCount = 8;
                        }

                        sb.Append("</tr>");
                        #endregion
                    }
                #endregion
            }
            else
                #region Other Search
                if (!extAllotControl) // Total Columns 7
                {
                    #region Allotment control
                    columnCount = 7;
                    sb.Append("<tr class=\"header\">");
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    {
                        sb.Append("<td>&nbsp;</td>");
                        columnCount = 8;
                    }
                    sb.AppendFormat("<td>&nbsp;</td><td><b>{0}</b></td><td><b>{1}</b></td><td><b>{2}</b></td><td><b>{3}</b></td><td align=\"center\"><b>{4}</b></td><td>&nbsp;</td>",
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFreeRoom"),
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblBoardName"),
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoomName"),
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom"),
                                        HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblPrice"));
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    {
                        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB"));
                        columnCount = 9;
                    }
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Apollo) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro))
                    {
                        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB"));
                        columnCount = 9;
                    }
                    sb.Append("</tr>");
                    #endregion
                }
                else
                {
                    #region Not allotment control
                    columnCount = 6;
                    // Total Columns 6
                    sb.Append("<tr class=\"header\">");
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    {
                        sb.Append("<td>&nbsp;</td>");
                        columnCount = 7;
                    }
                    sb.AppendFormat("<td>&nbsp;</td><td><b>{0}{1}</b></td><td><b>{2}</b></td><td><b>{3}</b></td><td align=\"center\"><b>{4}</b></td><td>&nbsp;</td>",
                                    "",
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblBoardName"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoomName"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom"),
                                    HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblPrice"));
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    {
                        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyEB"));
                        columnCount = 8;
                    }
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Apollo) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro))
                    {
                        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResMonitor", "lblPassangerEB"));
                        columnCount = 8;
                    }
                    sb.Append("</tr>");
                    #endregion
                }
                #endregion

            int j = 0;

            string requestInfo = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "RequestInfo"));
            StopSale = false;
            StopFlight = false;
            string errorMsg = string.Empty;
            bool usedFirstAvailable = false;
            foreach (TvBo.SearchResult row in roomsPrice)
            {
                bool notSaleable = false;
                StringBuilder sbTemp = new StringBuilder();
                Int16 StopSaleValue = 0;
                if (firstpriceListNo)
                    priceList = row.CatPackID;
                else
                {
                    firstpriceListNo = false;
                    if (row.CatPackID != priceList)
                        differentPriceList = true;
                }
                ++j;
                string stopSaleMessage = string.Empty;
                string offerDesc = string.Empty;
                bool offer = false;
                bool lastPriceLtOldPrice = false;

                lastPriceLtOldPrice = (row.LastPrice.HasValue ? row.CalcSalePrice.Value : 0) > (row.LastPrice.HasValue ? row.LastPrice.Value : 0);
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Calsedon))
                {
                    lastPriceLtOldPrice = (row.UseDynamicFlights ? (row.DynamicFlights.Price.HasValue ? row.DynamicFlights.Price.Value : (row.LastPrice.HasValue ? row.CalcSalePrice.Value : 0)) : (row.LastPrice.HasValue ? row.CalcSalePrice.Value : 0))
                                           > (row.LastPrice.HasValue ? row.LastPrice.Value : 0);
                }

                if ((row.SaleSPONo.HasValue && row.SaleSPONo.Value > 0) || (row.plSpoExists.HasValue && row.plSpoExists.Value) /*&& lastPriceLtOldPrice*/)
                {
                    offer = true;
                    if (offerDesc.Length > 0)
                        offerDesc += "+";
                    offerDesc += "SPO";
                }
                if (row.EBPerc.HasValue && row.EBPerc.Value > 0)
                {
                    offer = true;
                    if (offerDesc.Length > 0)
                        offerDesc += "+";
                    offerDesc += "EB";
                }
                if (row.PasEBPer.HasValue && row.PasEBPer.Value > 0)
                {
                    offer = true;
                    if (offerDesc.Length > 0)
                        offerDesc += "+";
                    offerDesc += "EB";
                }
                if (row.PEB_AdlVal.HasValue && row.PEB_AdlVal.Value > 0)
                {
                    offer = true;
                    if (offerDesc.Length > 0)
                        offerDesc += "+";
                    offerDesc += "EB";
                }
                if (row.LastPrice > row.CalcSalePrice)
                    offer = false;
                 bool stopFlight = false;
                 if ((!string.IsNullOrEmpty(row.DepFlight) && row.DepSeat.HasValue && row.DepSeat.Value < 1) || (!string.IsNullOrEmpty(row.RetFlight) && row.RetSeat.HasValue && row.RetSeat.Value < 1))
                    stopFlight = true;
                if ((row.StopSaleGuar == 2 && row.StopSaleStd == 2) && (row.FreeRoom > 0 || (row.AutoStop.HasValue && !row.AutoStop.Value)))
                {
                    StopSaleValue = 2;
                    sbTemp.Append("<tr class=\"StopSaleColor\">");
                }
                else
                    if (row.StopSaleGuar == 1 && row.StopSaleStd == 1)
                    {
                        string msg = new Reservation().getStopSaleMessage(UserData.Market, row.Hotel, row.Room, row.Accom, row.Board, row.CatPackID, UserData.AgencyID, row.CheckIn, row.Night, true, ref errorMsg);
                        stopSaleMessage = string.Format(HttpContext.GetGlobalResourceObject("PackageSearchResult", "StopSaleWarningMessage").ToString(),
                                                            string.IsNullOrEmpty(msg) ? "" : msg + ", ");
                        StopSaleValue = 1;
                        sbTemp.Append("<tr class=\"StopSaleColorOK\">");
                    }
                    else if ((row.StopSaleGuar == 1 && row.StopSaleStd == 1) || (row.StopSaleGuar == 2 && row.StopSaleStd == 2) || (row.FreeRoom == 0) || stopFlight)
                    {
                        sb.Append("<tr class='NotSaleable'>");
                        notSaleable = true;
                        StopFlight = true;
                    }
                    else
                        sb.Append("<tr>");
               
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    sbTemp.AppendFormat("<td style=\"width:25px;\">{0}</td>", string.IsNullOrEmpty(row.OprText) ? "" : row.OprText.Substring(0, row.OprText.Length > 4 ? 4 : row.OprText.Length));

                string depFlight = UserData.CustomRegID.Equals(Common.crID_Detur) ? row.DepFlightPNLName : row.DepFlight;
                string retFlight = UserData.CustomRegID.Equals(Common.crID_Detur) ? row.RetFlightPNLName : row.RetFlight;
                if (!StopSale && !stopFlight && row.FreeRoom > 0 && !usedFirstAvailable)
                {
                    usedFirstAvailable = true;
                    if (((row.StopSaleGuar == 2 && row.StopSaleStd == 2) || (row.FreeRoom <= 0 && row.AutoStop.HasValue && row.AutoStop.Value)) && string.Equals(UserData.CustomRegID, Common.crID_SunFun))
                        StopSale = true;
                    if (stopFlight)
                        StopFlight = true;
                    sbTemp.AppendFormat("<td class=\"select\"><input type=\"radio\" name=\"{0}\" value=\"{1}\" checked=\"checked\" onclick=\"changeRoomPrice({2}, {3});\" price=\"{4}\" salecur=\"{5}\" depFlight=\"{6}\" retFlight=\"{7}\" stopSale=\"{8}\" stopSaleMsg=\"{9}\" stopFlight=\"{10}\" /></td>",
                        resultGridRow.ToString() + "_" + r.RoomNr.ToString(),
                        row.RefNo,
                        resultGridRow.ToString(),
                        row.RefNo.ToString(),
                        row.UseDynamicFlights ? (row.DynamicFlights.Price.HasValue ? (row.DynamicFlights.Price.Value * 100).ToString("#.") : "0") : (row.LastPrice.HasValue ? (row.LastPrice.Value * 100).ToString("#.") : "0"),
                        row.SaleCur,
                        row.UseDynamicFlights ? row.DynamicFlights.DFlightNo : depFlight,
                        row.UseDynamicFlights ? row.DynamicFlights.RFlightNo : retFlight,
                        StopSaleValue,
                        stopSaleMessage,
                        stopFlight ? "1" : "0");

                    TotalPrice += row.UseDynamicFlights ? (row.DynamicFlights.Price.HasValue ? row.DynamicFlights.Price.Value : 0) : (row.LastPrice.HasValue ? row.LastPrice.Value : 0);
                    TotalPriceCur = row.SaleCur;
                }
                else
                    sbTemp.AppendFormat("<td class=\"select\"><input type=\"radio\" {11} name=\"{0}\" value=\"{1}\" onclick=\"changeRoomPrice({2}, {3});\" price=\"{4}\", salecur=\"{5}\" depFlight=\"{6}\" retFlight=\"{7}\" stopSale=\"{8}\" stopSaleMsg=\"{9}\" stopFlight=\"{10}\" /></td>",
                                  resultGridRow.ToString() + "_" + r.RoomNr.ToString(),
                                  row.RefNo,
                                  resultGridRow.ToString(),
                                  row.RefNo.ToString(),
                                  row.UseDynamicFlights ? (row.DynamicFlights.Price.HasValue ? (row.DynamicFlights.Price.Value * 100).ToString("#.") : "0") : (row.LastPrice.HasValue ? (row.LastPrice.Value * 100).ToString("#.") : "0"),
                                  row.SaleCur,
                                  row.UseDynamicFlights ? row.DynamicFlights.DFlightNo : row.DepFlight,
                                  row.UseDynamicFlights ? row.DynamicFlights.RFlightNo : row.RetFlight,
                                  StopSaleValue,
                                  stopSaleMessage,
                                  stopFlight ? "1" : "0", notSaleable ? "disabled=\"disabled\"" : "");
                int freeRoom = row.FreeRoom.HasValue ? row.FreeRoom.Value : 0;
                string javascriptstr = string.Empty;
                int? AvailableFreeRoomCount = Conversion.getInt32OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "AvailableFreeRoomCount"));
                if (!AvailableFreeRoomCount.HasValue)
                    AvailableFreeRoomCount = 3;
                else if (AvailableFreeRoomCount.HasValue && AvailableFreeRoomCount.Value == 0)
                    AvailableFreeRoomCount = freeRoom;
                if (!extAllotControl)
                {
                    if (freeRoom >= 999)
                        sbTemp.AppendFormat("<td class=\"free\"><span>{0}</span></td>", requestInfo); //Allotment not kontrol RQ
                    else
                        sbTemp.AppendFormat("<td class=\"free\" {1}><span>{0}</span></td>",
                                        freeRoom > AvailableFreeRoomCount.Value ? AvailableFreeRoomCount + "+" : (freeRoom < 0 ? "0" : freeRoom.ToString()),
                                        freeRoom >= 0 ? "" : "style=\"background-image: url('" + WebRoot.BasePageRoot + "/Images/HotelAlloRed.jpg');\""); //Free Room
                }
                if (UserData.ShowFlight && (criteria.SType == SearchType.PackageSearch || (criteria.SType == SearchType.TourPackageSearch && string.Equals(UserData.CustomRegID, Common.crID_FilipTravel))) && !extAllotControl)
                {
                    sbTemp.AppendFormat("<td class=\"depFlight\"><a class=\"depFly\" href=\"Controls/FlyInfo.aspx?FlightNo={1}&FlyDate={2}\" rel=\"Controls/FlyInfo.aspx?FlightNo={1}&FlyDate={2}\" title=\"{3}\">{0}</a></td>",
                        row.UseDynamicFlights ? row.DynamicFlights.DFlightNo : depFlight + (row.DEPFlightTime.HasValue && row.DEPFlightTime >= DateTime.Today ? "<br /><span style=\"font-size: 7pt;\">" + row.DEPFlightTime.Value.ToString("dd MMM") + " (" + row.DEPFlightTime.Value.ToString("HH:mm") + ")</span>" : "&nbsp;"),
                        row.UseDynamicFlights ? row.DynamicFlights.DFlightNo : row.DepFlight,
                        row.CheckIn.Value.Ticks,
                        row.UseDynamicFlights ? row.DynamicFlights.DFlightNo : depFlight); //Departure Flight
                    string depSeatStr = string.Empty;
                    if (row.UseDynamicFlights)
                    {
                        if (row.DynamicFlights.DFUnit1.HasValue && row.DynamicFlights.DFUnit1.Value >= 0)
                            depSeatStr = row.DynamicFlights.DFUnit1.Value > flightSeat ? flightSeat.ToString() + "+" : row.DynamicFlights.DFUnit1.Value.ToString();
                        else
                            depSeatStr = "0";
                    }
                    else
                    {
                        if (row.DepSeat.HasValue && row.DepSeat.Value >= 0)
                            depSeatStr = row.DepSeat.Value > flightSeat ? flightSeat.ToString() + "+" : row.DepSeat.Value.ToString();
                        else
                            depSeatStr = "0";
                    }
                    sbTemp.AppendFormat("<td class=\"depFlightSet\"><span>{0}</span></td>", depSeatStr); //Departure Seat
                    sbTemp.AppendFormat("<td class=\"retFlight\" ><a class=\"retFly\" href=\"Controls/FlyInfo.aspx?FlightNo={1}&FlyDate={2}\" rel=\"Controls/FlyInfo.aspx?FlightNo={1}&FlyDate={2}\" title=\"{3}\">{0}</a></td>",
                        row.UseDynamicFlights ? row.DynamicFlights.RFlightNo : retFlight + (row.RETFlightTime.HasValue && row.RETFlightTime >= DateTime.Today ? "<br /><span style=\"font-size: 7pt;\">" + row.RETFlightTime.Value.ToString("dd MMM") + " (" + row.RETFlightTime.Value.ToString("HH:mm") + ")</span>" : "&nbsp;"),
                        row.UseDynamicFlights ? row.DynamicFlights.RFlightNo : row.RetFlight,
                        row.CheckOut.Value.Ticks,
                        row.UseDynamicFlights ? row.DynamicFlights.RFlightNo : retFlight); //Return Flight
                    string retSeatStr = string.Empty;

                    if (row.UseDynamicFlights)
                    {
                        if (row.DynamicFlights.RFUnit1.HasValue && row.DynamicFlights.RFUnit1.Value >= 0)
                            retSeatStr = row.DynamicFlights.RFUnit1.Value > flightSeat ? flightSeat.ToString() + "+" : row.DynamicFlights.RFUnit1.Value.ToString();
                        else
                            retSeatStr = "0";
                    }
                    else
                    {
                        if (row.RetSeat.HasValue && row.RetSeat.Value >= 0)
                            retSeatStr = row.RetSeat.Value > flightSeat ? flightSeat.ToString() + "+" : row.RetSeat.Value.ToString();
                        else
                            retSeatStr = "0";
                    }
                    sbTemp.AppendFormat("<td class=\"retFlightSet\"><span>{0}</span></td>", retSeatStr); //Return Seat                    
                }

                sbTemp.AppendFormat("<td class=\"board\"><a class=\"boardConsept\" href=\"Controls/BoardConsept.aspx?Hotel={1}&Board={2}&Date={3}\" rel=\"Controls/BoardConsept.aspx?Hotel={1}&Board={2}&Date={3}\" title=\"{4}\">{0}</a></td>",
                    useLocalName ? row.BoardNameL : row.BoardName,
                    row.Hotel,
                    row.Board,
                    row.CheckIn.Value.Ticks.ToString(),
                    useLocalName ? row.BoardNameL : row.BoardName); //Board Name                
                sbTemp.AppendFormat("<td class=\"room\"><a class=\"roomConsept\" href=\"Controls/RoomConsept.aspx?Hotel={1}&Room={2}&Date={3}\" rel=\"Controls/RoomConsept.aspx?Hotel={1}&Room={2}&Date={3}\" title=\"{4}\">{0}</a>{5}</td>",
                    useLocalName ? row.RoomNameL : row.RoomName,
                    row.Hotel,
                    row.Room,
                    row.CheckIn.Value.Ticks.ToString(),
                    useLocalName ? row.RoomNameL : row.RoomName,
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Karnak) ? "<br /><span style=\"font-size: 8pt;\">(" + row.AccomFullName + ")</span>" : ""); //Room Name
                sbTemp.AppendFormat("<td class=\"accom\"><span class=\"accomConsept\">{0}</span></td>", useLocalName ? row.AccomNameL : row.AccomName); //Accom
                string oldPriceStr = string.Empty;
                if (row.UseDynamicFlights)
                {
                    if (showOldPrice.HasValue && showOldPrice.Value)
                    {
                        if (row.DynamicFlights.Price < row.DynamicFlights.OldPrice)
                            oldPriceStr = row.DynamicFlights.OldPrice.HasValue ? "<span style=\"text-decoration:line-through;\">" + row.DynamicFlights.OldPrice.Value.ToString("#,###.00") + " " + row.SaleCur + "</span>" + "<br />" : "";
                    }
                }
                else
                {
                    if (showOldPrice.HasValue && showOldPrice.Value)
                    {
                        if (row.LastPrice < row.CalcSalePrice)
                            oldPriceStr = row.CalcSalePrice.HasValue ? "<span style=\"text-decoration:line-through;\">" + row.CalcSalePrice.Value.ToString("#,###.00") + " " + row.SaleCur + "</span>" + "<br />" : "";
                    }
                }
                string percent = string.Empty;
                DateTime? ValidDate = DateTime.MaxValue;
                if (row.ValidDate.HasValue)
                {
                    ValidDate = row.ValidDate.Value;
                    percent = string.Empty;
                }
                if (row.EBValidDate.HasValue && row.EBValidDate.Value < ValidDate.Value && row.EBPerc.HasValue && row.EBPerc.Value > 0)
                {
                    ValidDate = row.EBValidDate.Value;
                    percent = row.EBPerc.HasValue ? (row.EBPerc.Value / 100).ToString("P") : string.Empty;
                }
                if (row.SaleEndDate.HasValue && row.SaleEndDate.Value < ValidDate.Value)
                {
                    ValidDate = row.SaleEndDate.Value;
                    percent = string.Empty;
                }
                if (row.FlightValidDate.HasValue && row.FlightValidDate.Value < ValidDate)
                {
                    ValidDate = row.FlightValidDate.Value;
                    percent = string.Empty;
                }
                if (row.HotelValidDate.HasValue && row.HotelValidDate.Value < ValidDate)
                {
                    ValidDate = row.HotelValidDate.Value;
                    percent = string.Empty;
                }
                if (row.PEB_SaleEndDate.HasValue && row.PEB_SaleEndDate.Value < ValidDate && row.PasEBPer.HasValue && row.PasEBPer.Value > 0)
                {
                    ValidDate = row.PEB_SaleEndDate.Value;
                    percent = row.PasEBPer.HasValue ? (row.PasEBPer.Value / 100).ToString("P") : string.Empty;
                }
                string priceDesc = row.LastPrice.HasValue ? row.LastPrice.Value.ToString("#,###.00") + " " + row.SaleCur : "&nbsp;";
                if (row.UseDynamicFlights)
                    priceDesc = row.DynamicFlights.Price.HasValue ? row.DynamicFlights.Price.Value.ToString("#,###.00") + " " + row.SaleCur : "&nbsp;";
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour)||
                    string.Equals(UserData.CustomRegID, TvBo.Common.ctID_NeedTour))
                {
                    sbTemp.AppendFormat("<td class=\"price\" {1}>{2}<span style=\"font-size:110%;\">{0}{3}</span></td>",
                        priceDesc,
                        offer ? "style=\"color: #FF0000;\"" : "",
                        oldPriceStr,
                        ValidDate.HasValue ? "<br />" + "<span style=\"font-size:80%;\">" + (HttpContext.GetGlobalResourceObject("PackageSearchResult", "DateOfValidity") + " -> " + ValidDate.Value.ToShortDateString()) + "</span>" : "");
                }
                else
                {
                    sbTemp.AppendFormat("<td class=\"price\" {1}>{2}<span style=\"font-size:110%;\">{0}</span></td>",
                            priceDesc,
                            offer ? "style=\"color: #FF0000;\"" : "",
                            oldPriceStr); //Price
                }
                string offerStr = string.Empty;

                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour))
                {
                    offerStr = string.Format("<img alt=\"{0}\" title=\"{0}\" src=\"Images/salehuge.png\" width=\"20px\">", "");

                    if (offerDesc.Length > 0)
                        offerStr = "<span>" + offerDesc + "</span><br />" + offerStr;
                }
                else if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.ctID_Zershan) ||
                    string.Equals(UserData.CustomRegID, TvBo.Common.ctID_NeedTour))
                {
                    offerStr = string.Format("<img alt=\"{0}\" title=\"{1}\" src=\"Images/salehuge.png\" width=\"20px\">", "", "");
                }
                else if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
                {
                    offerStr = string.Format("<img alt=\"{0}\" title=\"{1}\" src=\"Images/salehuge.png\" width=\"20px\">", percent, percent);
                }
                else
                {
                    offerStr = string.Format("<img alt=\"{0}\" title=\"{1}\" src=\"Images/salehuge.png\" width=\"20px\">",
                                                                     ValidDate.HasValue ? HttpContext.GetGlobalResourceObject("PackageSearchResult", "DateOfValidity") + " -> " + ValidDate.Value.ToShortDateString() : "",
                                                                     ValidDate.HasValue ? HttpContext.GetGlobalResourceObject("PackageSearchResult", "DateOfValidity") + " -> " + ValidDate.Value.ToShortDateString() : "");
                }


                sbTemp.AppendFormat("<td style=\"width:22px; text-align: center;\">{0}</td>", offer ? offerStr : "&nbsp;");

                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                {
                    string agencyEBStr = "&nbsp;";
                    if (row.AgencyEB.HasValue && row.AgencyEB.Value > 0)
                        agencyEBStr = (row.AgencyEB.Value / 100).ToString("#,###.00 %");
                    sbTemp.AppendFormat("<td style=\"width: 40px; white-space: nowrap;\">{0}</td>", agencyEBStr);
                }
                else
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Apollo) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka) ||
                        string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro))
                    {
                        string passengerEBStr = "&nbsp;";
                        if (row.PasEBPer.HasValue && row.PasEBPer.Value > 0)
                            passengerEBStr = (row.PasEBPer.Value / 100).ToString("#,###.00%");
                        sbTemp.AppendFormat("<td style=\"min-width: 20px; white-space: nowrap;\">{0}</td>", passengerEBStr);
                    }
                sbTemp.AppendFormat("</tr>");
                if ((!string.IsNullOrEmpty(row.Description) || !string.IsNullOrEmpty(row.SpoDesc)) && (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
                                                                                                       string.Equals(UserData.CustomRegID, TvBo.Common.crID_PliusTravel) ||
                                                                                                       string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) ||
                                                                                                       string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda) ||
                                                                                                       string.Equals(UserData.CustomRegID, TvBo.Common.crID_AliBabaTour) ||
                                                                                                       string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro)
                                                                                                       ))
                    sbTemp.AppendFormat("<tr><td colspan=\"{0}\" align=\"right\"><span style=\"font-style:italic; font-weight:bold;\">{1}</span>&nbsp;&nbsp;&nbsp;&nbsp;{2}</td></tr>",
                        columnCount,
                        row.Description,
                        (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Fibula_Ro) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_PliusTravel)) && !string.IsNullOrEmpty(row.SpoDesc) ? (!string.IsNullOrEmpty(row.Description) ? "<br />" : "") + row.SpoDesc + "&nbsp;&nbsp;&nbsp;&nbsp;" : "");

                BrochureCheckRecord brochure = brochureCheck.Find(f => f.Code == row.HolPack);
                if (brochure != null && !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                {
                    string brochureStr = string.Format("<span style=\"font-size: 10pt; cursor: pointer; color: #0000FF; \" onclick=\"showBrochure('{1}',{2},{3},'{4}')\"><b>{0}</b></span><br />",
                            useLocalName ? brochure.NameL : brochure.Name,
                            brochure.Code,
                            hotelPriceList[0].CheckIn.HasValue ? hotelPriceList[0].CheckIn.Value.Ticks.ToString() : DateTime.Today.Ticks.ToString(),
                            hotelPriceList[0].CheckOut.HasValue ? hotelPriceList[0].CheckOut.Value.Ticks.ToString() : DateTime.Today.Ticks.ToString(),
                            hotelPriceList[0].Market);

                    sbTemp.AppendFormat("<tr><td colspan=\"{0}\" align=\"left\"><span style=\"font-style:italic; font-weight:bold;\">{1}</span>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>",
                            columnCount,
                            brochureStr);
                }
                if(1==1)
                sb.Append(sbTemp.ToString());
            }
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        return sb;
    }

    public static MultiRoomResult getHotelGroupRows(int page)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["Criteria"] == null)
            return null;
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];
        TvBo.MultiRoomResult searchResult = (TvBo.MultiRoomResult)HttpContext.Current.Session["SearchResult"];

        string errorMsg = string.Empty;

        object _checkAvailableFlightSeat = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableFlightSeat");
        bool checkAvailableFlightSeat = Conversion.getBoolOrNull(_checkAvailableFlightSeat).HasValue ? Conversion.getBoolOrNull(_checkAvailableFlightSeat).Value : false;
        object _checkAvailableRoom = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableRoom");
        bool checkAvailableRoom = Conversion.getBoolOrNull(_checkAvailableRoom).HasValue ? Conversion.getBoolOrNull(_checkAvailableRoom).Value : false;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Calsedon))
            checkAvailableFlightSeat = false;

        List<TvBo.SearchResult> resultPrices = searchResult.ResultSearch;
        List<TvBo.MultiRoomSelection> resultTmp = searchResult.Group;

        List<TvBo.MultiRoomSelection> result = new List<TvBo.MultiRoomSelection>();

        int first = (page - 1) * searchResult.PageRowCount;
        int last = page * searchResult.PageRowCount;

        result = resultTmp.Where(w => w.RoomGroupRefNo > first && w.RoomGroupRefNo <= last).ToList<TvBo.MultiRoomSelection>();

        if (searchResult.FilterResult.filtered)
        {
            List<SearchResult> rfTmp = new Search().setEmptySearchDataMultiRoom(UserData, searchResult.FilteredResultSearch, result, checkAvailableRoom, checkAvailableFlightSeat, criteria.ExtAllotControl, criteria.SType, criteria.CurControl ? criteria.CurrentCur : "", ref errorMsg);
            foreach (SearchResult row in rfTmp)
            {
                SearchResult chgRecord = resultPrices.Find(f => f.RefNo == row.RefNo);
                chgRecord = (SearchResult)TvBo.Common.DeepClone(row);
            }
            searchResult.FilteredResultSearch = rfTmp;
        }
        else
            resultPrices = new Search().setEmptySearchDataMultiRoom(UserData, resultPrices, result, checkAvailableRoom, checkAvailableFlightSeat, criteria.ExtAllotControl, criteria.SType, criteria.CurControl ? criteria.CurrentCur : "", ref errorMsg);
        //resultPrices = new Search().setEmptySearchDataMultiRoom(UserData, resultPrices, result, checkAvailableRoom, checkAvailableFlightSeat, criteria.ExtAllotControl, criteria.SType, ref errorMsg);


        searchResult.CurrentPage = page;
        searchResult.FirstGroup = result;
        searchResult.ResultSearch = resultPrices;

        HttpContext.Current.Session["SearchResult"] = searchResult;
        return searchResult;
    }

    public static MultiRoomResult getHotelGroupRowsFirst(TvBo.MultiRoomResult searchResult)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["Criteria"] == null)
            return null;
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];
        string errorMsg = string.Empty;

        object _checkAvailableFlightSeat = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableFlightSeat");
        bool checkAvailableFlightSeat = Conversion.getBoolOrNull(_checkAvailableFlightSeat).HasValue ? Conversion.getBoolOrNull(_checkAvailableFlightSeat).Value : false;
        object _checkAvailableRoom = new TvBo.Common().getFormConfigValue("SearchPanel", "CheckAvailableRoom");
        bool checkAvailableRoom = Conversion.getBoolOrNull(_checkAvailableRoom).HasValue ? Conversion.getBoolOrNull(_checkAvailableRoom).Value : false;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Calsedon))
            checkAvailableFlightSeat = false;

        string onlyHotelVersion = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "OnlyHotelVersion"));

        List<TvBo.SearchResult> resultPrices = new List<SearchResult>();

        List<BrochureCheckRecord> brochureCheckList = new List<BrochureCheckRecord>();
        if (searchResult.FilterResult.filtered)
        {

            resultPrices = searchResult.ResultSearch;
            searchResult.FilteredResultSearch = searchResult.ResultSearch
                                                    .Where(w => !searchResult.FilterResult.DepCity.HasValue || searchResult.FilterResult.DepCity.Value == w.DepCity)
                                                    .Where(w => !searchResult.FilterResult.ArrCity.HasValue || searchResult.FilterResult.ArrCity.Value == w.ArrCity)
                                                    .Where(w => !searchResult.FilterResult.HotelLocation.HasValue || searchResult.FilterResult.HotelLocation.Value == w.HotelLocation)
                                                    .Where(w => string.IsNullOrEmpty(searchResult.FilterResult.Category) || string.Equals(searchResult.FilterResult.Category, w.HotCat))
                                                    .Where(w => string.IsNullOrEmpty(searchResult.FilterResult.Hotel) || string.Equals(searchResult.FilterResult.Hotel, w.Hotel))
                                                    .Where(w => !searchResult.FilterResult.CheckIn.HasValue || searchResult.FilterResult.CheckIn.Value == w.CheckIn)
                                                    .Where(w => !searchResult.FilterResult.Night.HasValue || searchResult.FilterResult.Night.Value == w.Night)
                                                    .Where(w => string.IsNullOrEmpty(searchResult.FilterResult.Room) || string.Equals(searchResult.FilterResult.Room, w.Room))
                                                    .Where(w => string.IsNullOrEmpty(searchResult.FilterResult.Board) || string.Equals(searchResult.FilterResult.Board, w.Board))
                                                .ToList<SearchResult>();
        }
        else
            if (searchResult.ResultSearch.Count < 1)
            {
                Guid? recID = null;
                String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
                if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
                {
                    string packType = string.Empty;
                    switch (criteria.SType)
                    {
                        case SearchType.TourPackageSearch:
                            packType = "T";
                            break;
                        case SearchType.PackageSearch:
                            packType = "H";
                            break;
                        case SearchType.OnlyHotelSearch:
                            packType = "O";
                            break;
                    }

                    recID = new WEBLog().saveWEBSearchLog(UserData.SID, UserData.UserID, UserData.AgencyID, SaleResource.B2B,
                                    criteria.CheckIn, criteria.ExpandDate, criteria.NightFrom, criteria.NightTo, criteria.DepCity,
                                    criteria.ArrCity, null, null, criteria.Package, packType, null, ref errorMsg);
                    if (recID.HasValue)
                    {
                        foreach (var row in criteria.RoomsInfo)
                            new WEBLog().saveWEBRoomInfoLog(recID.Value, row.Adult, row.Child, ref errorMsg);

                        foreach (var row in criteria.Resort.Split('|'))
                            new WEBLog().saveWEBLocationLog(recID.Value, Conversion.getInt32OrNull(row.ToString()), ref errorMsg);

                        foreach (var row in criteria.Hotel.Split('|'))
                            if (!string.IsNullOrEmpty(row))
                                new WEBLog().saveWEBHotelLog(recID.Value, row.ToString(), ref errorMsg);

                        foreach (var row in criteria.Room.Split('|'))
                            if (!string.IsNullOrEmpty(row))
                                new WEBLog().saveWEBRoomLog(recID.Value, row.ToString(), ref errorMsg);

                        foreach (var row in criteria.Board.Split('|'))
                            if (!string.IsNullOrEmpty(row))
                                new WEBLog().saveWEBBoardLog(recID.Value, row.ToString(), ref errorMsg);

                        foreach (var row in criteria.Category.Split('|'))
                            if (!string.IsNullOrEmpty(row))
                                new WEBLog().saveWEBCategoryLog(recID.Value, row.ToString(), ref errorMsg);
                    }
                }

                if (searchResult.SearchType != SearchType.TourPackageSearch)
                {
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Calsedon) && searchResult.SearchType == SearchType.PackageSearch)
                        resultPrices = new Search().getDynamicFlightSearch(UserData, criteria, checkAvailableRoom, searchResult.SearchType, checkAvailableFlightSeat, recID, ref errorMsg);
                    else
                        resultPrices = new Search().getEmptySearch(UserData, criteria, checkAvailableRoom, searchResult.SearchType, checkAvailableFlightSeat, recID, ref errorMsg);
                }
                else
                {
                    resultPrices = new Search().getEmptySearchT(UserData, criteria, true, searchResult.SearchType, false, recID, ref errorMsg);
                }

                if (resultPrices != null && resultPrices.Count > 0)
                {
                    if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ||
                        Equals(UserData.CustomRegID, TvBo.Common.crID_PliusTravel) ||
                        Equals(UserData.CustomRegID, TvBo.Common.crID_FamilyTour) ||
                        Equals(UserData.CustomRegID, TvBo.Common.crID_FilipTravel) ||
                        Equals(UserData.CustomRegID, TvBo.Common.crID_KidyTour) ||
                        //Equals(UserData.CustomRegID, TvBo.Common.crID_Calypso) ||
                        Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun))
                    {
                        resultPrices = new Search().setEmptySearchDataMultiRoomFullData(UserData, resultPrices, checkAvailableRoom, checkAvailableFlightSeat, criteria.ExtAllotControl, criteria.SType, criteria.CurControl ? criteria.CurrentCur : "", ref errorMsg);
                    }
                    var query = from q in resultPrices
                                group q by new { q.Market, q.HolPack, q.CheckIn, q.CheckOut } into k
                                select new { k.Key.Market, k.Key.HolPack, k.Key.CheckIn, k.Key.CheckOut };
                    foreach (var row in query)
                    {
                        List<holPackBrochureReady> getholPackBrochureReadyList = new TvBo.Common().getholPackBrochureReady(row.Market, row.CheckIn, row.CheckOut, ref errorMsg);
                        if (getholPackBrochureReadyList != null)
                            foreach (holPackBrochureReady r1 in getholPackBrochureReadyList)
                            {
                                brochureCheckList.Add(new BrochureCheckRecord
                                {
                                    Code = r1.HolPack,
                                    Name = r1.HolPackName,
                                    NameL = r1.HolPackNameL,
                                    Brochure = r1.brochureReady
                                });
                            }
                    }
                }
            }
            else
                resultPrices = searchResult.ResultSearch;

        if (resultPrices == null || resultPrices.Count < 1)
            return null;

        HttpContext.Current.Session["SearchResult"] = resultPrices;

        List<TvBo.MultiRoomSelection> resultTmp = new List<MultiRoomSelection>();

        List<SearchResult> data = searchResult.FilterResult.filtered ? searchResult.FilteredResultSearch : resultPrices;

        resultTmp = new Search().getEmptySearchGroups(UserData, data, criteria, ref errorMsg);

        if (resultTmp == null || resultTmp.Count < 1)
            return null;

        searchResult.CurrentPage = 1;
        searchResult.Group = resultTmp;

        List<TvBo.MultiRoomSelection> filterData = new List<TvBo.MultiRoomSelection>();
        int first = 0;
        int last = searchResult.PageRowCount;
        filterData = resultTmp.Where(w => w.RoomGroupRefNo > first && w.RoomGroupRefNo <= last).ToList<TvBo.MultiRoomSelection>();

        if (searchResult.FilterResult.filtered)
        {

            List<SearchResult> rfTmp = new Search().setEmptySearchDataMultiRoom(UserData, searchResult.FilteredResultSearch, filterData, checkAvailableRoom, checkAvailableFlightSeat, criteria.ExtAllotControl, criteria.SType, criteria.CurControl ? criteria.CurrentCur : "", ref errorMsg);
            foreach (SearchResult row in rfTmp)
            {
                SearchResult chgRecord = resultPrices.Find(f => f.RefNo == row.RefNo);
                chgRecord = (SearchResult)TvBo.Common.DeepClone(row);
            }
            searchResult.FilteredResultSearch = rfTmp;
        }
        else
        {
            resultPrices = new Search().setEmptySearchDataMultiRoom(UserData, resultPrices, filterData, checkAvailableRoom, checkAvailableFlightSeat, criteria.ExtAllotControl, criteria.SType, criteria.CurControl ? criteria.CurrentCur : "", ref errorMsg);
        }

        filterData = filterData.Where(w => w.RoomGroupRefNo > first && w.RoomGroupRefNo <= last).ToList<TvBo.MultiRoomSelection>();

        searchResult.brochureCheck = brochureCheckList;
        searchResult.FirstGroup = filterData;
        searchResult.ResultSearch = resultPrices.OrderBy(o => o.LastPrice).ToList<SearchResult>();
        searchResult.TotalPageCount = Convert.ToInt32(Math.Ceiling(resultTmp.Count() / (1.0 * searchResult.PageRowCount)));
        searchResult.SearchType = criteria.SType;
        searchResult.HotelMarOpt = new Search().getHotelMarOptList(UserData, searchResult.FilterResult.filtered ? searchResult.FilteredResultSearch : resultPrices, ref errorMsg);
        searchResult.FlightInfo = new Search().getFlightDetail(UserData, searchResult.FilterResult.filtered ? searchResult.FilteredResultSearch : resultPrices, ref errorMsg);

        HttpContext.Current.Session["SearchResult"] = searchResult;
        return searchResult;
    }

    [WebMethod(EnableSession = true)]
    public static string getFlightInfo(string FlightNo, string FlyDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        DateTime _flyDate = DateTime.MinValue.AddTicks(Conversion.getInt64OrNull(FlyDate).Value);
        FlightDetailRecord flight = new Flights().getFlightDetail(UserData, UserData.Market, FlightNo, _flyDate, ref errorMsg);
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat("<table><tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFlightNo"),
            flight.FlightNo);
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFlyDate"),
            flight.DepDate.HasValue ? flight.DepDate.Value.ToShortDateString() : flight.FlyDate.Value.ToShortDateString());
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblDepartureTime"),
            flight.TDepTime.HasValue ? flight.TDepTime.Value.ToString("HH:mm") : (flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "&nbsp;"));
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblDepartureAirport"),
            "(" + flight.DepAirport + ")-" + (useLocalName ? flight.DepAirportNameL : flight.DepAirportName));
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblArrivalTime"),
            flight.TArrTime.HasValue ? flight.TArrTime.Value.ToString("HH:mm") : (flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "&nbsp;"));
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblArrivalAirport"),
            "(" + flight.ArrAirport + ")-" + (useLocalName ? flight.ArrAirportNameL : flight.ArrAirportName));
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr></table>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAirline"),
            useLocalName ? flight.AirlineNameL : flight.AirlineName);
        return sb.ToString();
    }

    public static string getFlightInfoA(TvBo.MultiRoomResult searchResult, string FlightNo, DateTime? FlyDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        if (string.IsNullOrEmpty(FlightNo) || !FlyDate.HasValue)
            return string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        FlightDetailRecord flight = searchResult.FlightInfo.Find(f => f.FlightNo == FlightNo && f.FlyDate == FlyDate);
        if (flight == null)
            return string.Empty;
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat("{0} : {1} <br />",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFlightNo"),
            flight.FlightNo);
        sb.AppendFormat("{0} : {1} <br />",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFlyDate"),
            flight.FlyDate.Value.ToShortDateString());
        sb.AppendFormat("{0} : {1} <br />",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblDepartureTime"),
            flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "&nbsp;");
        sb.AppendFormat("{0} : {1} <br />",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblDepartureAirport"),
            "(" + flight.DepAirport + ")-" + (useLocalName ? flight.DepAirportNameL : flight.DepAirportName));
        sb.AppendFormat("{0} : {1} <br />",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblArrivalTime"),
            flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "&nbsp;");
        sb.AppendFormat("{0} : {1} <br />",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblArrivalAirport"),
            "(" + flight.ArrAirport + ")-" + (useLocalName ? flight.ArrAirportNameL : flight.ArrAirportName));
        sb.AppendFormat("{0} : {1} <br />",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAirline"),
            useLocalName ? flight.AirlineNameL : flight.AirlineName);
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getHotelUrl(TvBo.MultiRoomResult searchResult, string Hotel)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        
        object _AddQueryB2CHotelLink = new TvBo.Common().getFormConfigValue("SearchPanel", "AddQueryB2CHotelLink");
        string AddQueryB2CHotelLink = _AddQueryB2CHotelLink != null ? Convert.ToString(_AddQueryB2CHotelLink) : string.Empty;
        HotelMarOptRecord hotelUrl = searchResult.HotelMarOpt.Find(f => f.Hotel == Hotel);
        string _hotelUrl = (hotelUrl != null && !string.IsNullOrEmpty(hotelUrl.InfoWeb)) ? hotelUrl.InfoWeb + 
            (!string.IsNullOrEmpty(AddQueryB2CHotelLink) ? "?" + AddQueryB2CHotelLink : "") : "";
        return _hotelUrl;
    }

    [WebMethod(EnableSession = true)]
    public static string addOffer(string BookList)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.MultiRoomResult searchResult = (TvBo.MultiRoomResult)HttpContext.Current.Session["SearchResult"];
        List<SearchResult> _bookList = new List<SearchResult>();
        for (int i = 0; i < BookList.Split(',').Length; i++)
        {
            int refNo = Convert.ToInt32(BookList.Split(',')[i]);
            SearchResult bookRow = searchResult.ResultSearch.Find(f => f.RefNo == refNo);

            _bookList.Add(bookRow);
        }
        List<SearchResult> offerList = new List<SearchResult>();
        if (string.Equals(Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2"))
        {
            offerList = new ClientOffer().readClientOfferList(UserData);
        }
        else
        {
            if (HttpContext.Current.Session["OfferList"] != null)
                offerList = (List<SearchResult>)HttpContext.Current.Session["OfferList"];
        }
        foreach (SearchResult row in _bookList)
            offerList.Add(row);

        if (string.Equals(Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2"))
        {
            new ClientOffer().saveClientOfferList(UserData, offerList);
        }
        else
        {
            HttpContext.Current.Session["OfferList"] = offerList;
        }
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string clearOffer()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (string.Equals(Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2"))
        {
            new ClientOffer().removeClientOfferList(UserData);
        }
        else
        {
            HttpContext.Current.Session["OfferList"] = null;
        }
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string getAllotmentControl(string RefNoList)
    {
        

        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.MultiRoomResult searchResult = (TvBo.MultiRoomResult)HttpContext.Current.Session["SearchResult"];
        List<SearchResult> _bookList = new List<SearchResult>();
        SearchCriteria _criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];
        if (_criteria.SType == SearchType.TourPackageSearch)
            return string.Empty;
        for (int i = 0; i < RefNoList.Split(',').Length; i++)
        {
            int refNo = Convert.ToInt32(RefNoList.Split(',')[i]);
            SearchResult bookRow = searchResult.ResultSearch.Find(f => f.RefNo == refNo);
            _bookList.Add(bookRow);
        }

        string errorMsg = string.Empty;
        #region Emerald için test amaçlı yapıldı kontrolü sağlanacak
        if (UserData.CustomRegID == Common.crID_Emerald)
        {
            bool notAllotment = false;
            for (int i = 0; i < RefNoList.Split(',').Length; i++)
            {
                int refNo = Convert.ToInt32(RefNoList.Split(',')[i]);
                var flightInfo = _bookList.Where(w => w.RefNo == refNo).FirstOrDefault();
                if (flightInfo != null)
                {
                    if (flightInfo.DepSeat > 0 && flightInfo.RetSeat > 0)
                        notAllotment = !notAllotment ? false : true;
                    else
                        notAllotment = false;
                }
                else
                    notAllotment = false;

            }
            if (notAllotment)
                return HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
            else
                return string.Empty;

        }
        #endregion
        if (new Reservation().priceSearchAllotControl(UserData, _bookList, _criteria.SType == SearchType.TourPackageSearch, ref errorMsg))
            return string.Empty;
        else
            return errorMsg;
    }

    public static List<flyHandicaps> getFlightHandicaps(User UserData, List<SearchResult> priceList)
    {
        List<flyHandicaps> handicaps = new List<flyHandicaps>();

        var depQ = from q in priceList
                   group q by new { FlightNo = q.DepFlight, FlyDate = q.CheckIn } into k
                   select k;

        var retQ = from q in priceList
                   group q by new { FlightNo = q.RetFlight, FlyDate = q.CheckOut } into k
                   select k;

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        if ((depQ != null && depQ.Count() > 0) || (retQ != null && retQ.Count() > 0))
        {
            string handicapFlightServiceLabel = string.Format("<span class=\"handicapServiceLabel\">{0}</span><br />", HttpContext.GetGlobalResourceObject("MakeReservation", "flightHandicapLabel"));
            var flights = depQ.Union(retQ);
            foreach (var row in flights)
            {
                string flightHandicapStr = string.Empty;
                List<HandicapsRecord> flightHandicapList = new TvBo.Common().getHandicaps(UserData, row.Key.FlyDate, null, "FLIGHT", row.Key.FlightNo, HandicapTypes.Package, ref errorMsg);
                if (flightHandicapList != null && flightHandicapList.Count() > 0)
                {
                    flightHandicapStr += string.Format("<b>{0}</b><br />", row.Key.FlightNo);
                    foreach (HandicapsRecord r1 in flightHandicapList)
                        flightHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);
                    handicaps.Add(new flyHandicaps { FlightNo = row.Key.FlightNo, FlyDate = row.Key.FlyDate, Handicaps = flightHandicapStr });
                }

                FlightDayRecord flight = new Flights().getFlightDay(UserData, row.Key.FlightNo, row.Key.FlyDate.Value, ref errorMsg);
                List<HandicapsRecord> depAiprPortHandicapList = new TvBo.Common().getHandicaps(UserData, row.Key.FlyDate, null, "AIRPORT", flight.DepAirport, HandicapTypes.Package, ref errorMsg);
                if (depAiprPortHandicapList != null && depAiprPortHandicapList.Count() > 0)
                {
                    AirportRecord airport = new Flights().getAirport(UserData.Market, flight.DepAirport, ref errorMsg);
                    flightHandicapStr += string.Format("<b>{0}</b><br />", airport.Code + " (" + (useLocalName ? airport.LocalName : airport.Name) + ")");
                    foreach (HandicapsRecord r1 in depAiprPortHandicapList)
                        flightHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);
                }
            }
        }
        return handicaps;
    }

    public static List<hotelHandicaps> getHotelHandicaps(User UserData, List<SearchResult> priceList)
    {
        List<hotelHandicaps> handicaps = new List<hotelHandicaps>();
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        var hotels = from q in priceList
                     group q by new
                     {
                         Hotel = q.Hotel,
                         ServiceName = useLocalName ? q.HotelNameL : q.HotelName,
                         BegDate = q.CheckIn,
                         EndDate = q.CheckOut,
                         plMarket = q.Market
                     } into k
                     select new { Hotel = k.Key.Hotel, ServiceName = k.Key.ServiceName, BegDate = k.Key.BegDate, EndDate = k.Key.EndDate, plMarket = k.Key.plMarket };
        if (hotels != null && hotels.Count() > 0)
        {
            string handicapServiceLabel = string.Format("<span class=\"handicapServiceLabel\">{0}</span><br />", HttpContext.GetGlobalResourceObject("MakeReservation", "hotelHandicapLabel"));
            foreach (var row in hotels)
            {
                string hotelHandicapStr = string.Empty;
                List<HotelHandicapRecord> hotelHandicapList = new Hotels().getHotelHandicaps(UserData.Market, row.plMarket, row.Hotel, row.BegDate, row.EndDate, ref errorMsg);
                if (hotelHandicapList != null && hotelHandicapList.Count() > 0)
                {
                    //hotelHandicapStr += string.Format("<b>{0}</b><br />", row.ServiceName);
                    foreach (HotelHandicapRecord r1 in hotelHandicapList)
                        hotelHandicapStr += string.Format("{0}<br />", useLocalName ? r1.NameL : r1.Name);
                    
                    handicaps.Add(new hotelHandicaps { Hotel = row.Hotel, CheckIn = row.BegDate, CheckOut = row.EndDate, Handicaps = hotelHandicapStr });
                }
            }
        }
        return handicaps;
    }

    [Serializable]
    public class flyInfo
    {
        public flyInfo()
        {
        }

        string _FlightNo;
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }

        DateTime? _FlyDate;
        public DateTime? FlyDate
        {
            get { return _FlyDate; }
            set { _FlyDate = value; }
        }

        string _html;
        public string Html
        {
            get { return _html; }
            set { _html = value; }
        }

        string _handicap;
        public string Handicap
        {
            get { return _handicap; }
            set { _handicap = value; }
        }

    }

    [Serializable]
    public class resultFilterCoor
    {
        public resultFilterCoor()
        {
        }

        string _ID;
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        string _FieldName;
        public string FieldName
        {
            get { return _FieldName; }
            set { _FieldName = value; }
        }

        int? _Width;
        public int? Width
        {
            get { return _Width; }
            set { _Width = value; }
        }

        string _Value;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        string _Type;
        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
    }

    [Serializable]
    public class flyHandicaps
    {
        public flyHandicaps()
        {
        }

        public string FlightNo { get; set; }
        public DateTime? FlyDate { get; set; }
        public string Handicaps { get; set; }
    }

    [Serializable]
    public class hotelHandicaps
    {
        public hotelHandicaps()
        {
        }

        public string Hotel { get; set; }
        public DateTime? CheckIn { get; set; }
        public DateTime? CheckOut { get; set; }
        public string Handicaps { get; set; }
    }
}