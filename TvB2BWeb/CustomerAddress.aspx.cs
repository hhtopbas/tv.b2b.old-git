﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Web.Services;
using System.Text;
using TvTools;

public partial class CustomerAddress : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
    }

    [WebMethod(EnableSession = true)]
    public static string getFormDataContactInfo(int? _CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = new TvBo.ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == _CustNo.Value);

        string CustomerName = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ContactInfo").ToString(), resCust.Name + " " + resCust.Surname);
        string divCustomers = string.Format("<span style=\"font-size: 11pt;\"><b>{0}</b></span>", CustomerName);
        return divCustomers;
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(int? _CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new TvBo.ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResCustInfoRecord> _ResCustInfo = ResData.ResCustInfo;

        List<IntCountryListRecord> countryList = new TvBo.Common().getCountryList(UserData, ref errorMsg);
        List<Location> LocationList = CacheObjects.getLocationList(UserData.Market);
        Location country = LocationList.Find(f => f.RecID == UserData.Country);

        List<TitleRecord> adultTypes = new TvBo.Common().getAdultTypes();

        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == _CustNo.Value);
        ResCustInfoRecord resCustInfo = new ResCustInfoRecord();
        if (ResData.ResCustInfo.Count > 0 && _ResCustInfo.Find(f => f.CustNo == resCust.CustNo) != null)
        {
            resCustInfo = _ResCustInfo.Find(f => f.CustNo == resCust.CustNo);
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                resCustInfo.AddrHomeTel = !string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : strFunc.Trim(resCustInfo.AddrHomeTel, ' ');
            else resCustInfo.MobTel = !string.IsNullOrEmpty(resCust.Phone) ? strFunc.Trim(resCust.Phone, ' ') : strFunc.Trim(resCustInfo.MobTel, ' ');
        }
        else
        {
            resCustInfo.CustNo = resCust.CustNo;
            resCustInfo.CTitle = resCust.Title;
            resCustInfo.RecID = _ResCustInfo.Count > 0 ? _ResCustInfo.LastOrDefault().RecID + 1 : 1;
            resCustInfo.RecordID = resCustInfo.RecID;
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                resCustInfo.AddrHomeTel = strFunc.Trim(resCust.Phone, ' ');
            else resCustInfo.MobTel = strFunc.Trim(resCust.Phone, ' ');
            IntCountryListRecord _country = new TvBo.Common().getCountryIntCode(UserData.Market, UserData.Country, ref errorMsg);
            resCustInfo.AddrHomeCountryCode = _country != null ? _country.IntCode : string.Empty;            
            resCustInfo.MemTable = false;

            _ResCustInfo.Add(resCustInfo);
        }

        StringBuilder divContact = new StringBuilder();
        StringBuilder divHomeAddr = new StringBuilder();
        StringBuilder divWorkAddr = new StringBuilder();
        StringBuilder divBankInfo = new StringBuilder();

        string _phoneMask = string.Empty;
        string _mobphoneMask = string.Empty;
        if (UserData.PhoneMask != null)
        {
            _phoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.PhoneMask.Replace('#', '9') : "";
            _mobphoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.MobilPhoneMask.Replace('#', '9') : "";
        }

        divContact.AppendFormat("<input id=\"phone_Mask\" type=\"hidden\" value=\"{0}\" />", _phoneMask);
        divContact.AppendFormat("<input id=\"mob_phone_Mask\" type=\"hidden\" value=\"{0}\" />", _mobphoneMask);
        divContact.AppendFormat("<input id=\"countryList\" type=\"hidden\" value='{0}' />", Newtonsoft.Json.JsonConvert.SerializeObject(countryList).Replace('#', '9'));

        #region Contact
        divContact.AppendFormat("<input type=\"hidden\" id=\"CustNo\" value=\"{0}\" /><input type=\"hidden\" id=\"hfLCID\" value=\"{1}\" />", resCust.CustNo, UserData.Ci.Name);
        divContact.Append("<table><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactTitle"));
        string _adultTypes = string.Empty;
        foreach (TitleRecord row in adultTypes)
            _adultTypes += string.Format("<option value=\"{0}\" {1}>{2}</option>", row.TitleNo, row.TitleNo == resCustInfo.CTitle ? "selected=\"selected\"" : "", row.Code);
        divContact.AppendFormat("<td><select id=\"CTitle\" class=\"combo\">{0}</select></td>", _adultTypes);
        divContact.Append("</tr><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactName"));
        divContact.AppendFormat("<td><input id=\"CName\" type=\"text\" maxlength=\"30\" class=\"pix250\" value=\"{0}\" {1} onkeypress=\"return isNonUniCodeChar(event);\" /></td>",
            resCustInfo.CName,
            "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules) + "\"");
        divContact.Append("</tr><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactSurname"));
        divContact.AppendFormat("<td><input id=\"CSurName\" type=\"text\" maxlength=\"30\" class=\"pix250\" value=\"{0}\" {1} onkeypress=\"return isNonUniCodeChar(event);\" /></td>",
            resCustInfo.CSurName,
            "style=\"" + new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules) + "\"");
        //divContact.Append("</tr><tr>");
        //divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
        //        HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactMobile"));
        //divContact.AppendFormat("<td><input id=\"MobTel\" type=\"text\" maxlength=\"30\" class=\"pix250\" value=\"{0}\" /></td>", resCustInfo.MobTel);
        divContact.Append("</tr><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactContactAddress"));
        string _contactAddr = string.Empty;
        _contactAddr += string.Format("<option value=\"{0}\" {1}>{2}</option>", "H", Equals(resCustInfo.ContactAddr, "H") ? "selected=\"selected\"" : "", HttpContext.GetGlobalResourceObject("LibraryResource", "cbContHome").ToString());
        _contactAddr += string.Format("<option value=\"{0}\" {1}>{2}</option>", "W", Equals(resCustInfo.ContactAddr, "W") ? "selected=\"selected\"" : "", HttpContext.GetGlobalResourceObject("LibraryResource", "cbContWork").ToString());
        divContact.AppendFormat("<td><select id=\"ContactAddr\" class=\"combo\">{0}</select></td>", _contactAddr);
        divContact.Append("</tr><tr>");
        divContact.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactInvoiceAddress"));
        string _invoiceAddr = string.Empty;
        _invoiceAddr += string.Format("<option value=\"{0}\" {1}>{2}</option>", "H", Equals(resCustInfo.InvoiceAddr, "H") ? "selected=\"selected\"" : "", HttpContext.GetGlobalResourceObject("LibraryResource", "cbInvHome").ToString());
        _invoiceAddr += string.Format("<option value=\"{0}\" {1}>{2}</option>", "W", Equals(resCustInfo.InvoiceAddr, "W") ? "selected=\"selected\"" : "", HttpContext.GetGlobalResourceObject("LibraryResource", "cbInvWork").ToString());
        divContact.AppendFormat("<td><select id=\"InvoiceAddr\" class=\"combo\">{0}</select></td>", _invoiceAddr);
        divContact.Append("</tr></table>");
        #endregion

        #region Home Address
        divHomeAddr.Append("<table><tr>");
        divHomeAddr.AppendFormat("<td colspan=\"2\" align=\"center\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divHomeAddressLabel"));
        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblAddress"));
        divHomeAddr.AppendFormat("<td><textarea id=\"AddrHome\" cols=\"33\" name=\"AddrHome\" rows=\"3\" >{0}</textarea></td>", resCustInfo.AddrHome);

        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblZipCode"));
        divHomeAddr.AppendFormat("<td><input id=\"AddrHomeZip\" type=\"text\" maxlength=\"10\" style=\"width: 80px;\" value=\"{0}\" /></td>", resCustInfo.AddrHomeZip);

        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblCityCountry"));
        divHomeAddr.AppendFormat("<td><input id=\"AddrHomeCity\" type=\"text\" maxlength=\"30\" style=\"width: 130px;\" value=\"{0}\" />&nbsp;/&nbsp;", resCustInfo.AddrHomeCity);

        string countryListStr = string.Empty;
        string countryWorkListStr = string.Empty;
        string selectedCountryCode = string.Empty;
        string selectedWorkCountryCode = string.Empty;
        foreach (IntCountryListRecord row in countryList)
        {
            string selectStr = string.Empty;
            string selectWorkStr = string.Empty;
            if (Equals(resCustInfo.AddrHomeCountry, row.CountryName) || Equals(resCustInfo.AddrHomeCountryCode, row.IntCode) || Equals(resCustInfo.AddrHomeCountryCode, row.Nationality))
            {
                selectStr = "selected=\"selected\"";
                selectedCountryCode = row.CountryPhoneCode;
            }
            else if (Equals(row.IntCode, country != null ? country.CountryCode.ToString() : ""))
            {
                selectStr = "selected=\"selected\"";
                selectedCountryCode = row.CountryPhoneCode;
            }

            if (Equals(resCustInfo.AddrWorkCountry, row.CountryName) || Equals(resCustInfo.AddrWorkCountryCode, row.IntCode) || Equals(resCustInfo.AddrWorkCountryCode, row.Nationality))
            {
                selectWorkStr = "selected=\"selected\"";
                selectedWorkCountryCode = row.CountryPhoneCode;
            }
            else if (Equals(row.IntCode, country != null ? country.CountryCode.ToString() : ""))
            {
                selectWorkStr = "selected=\"selected\"";
                selectedWorkCountryCode = row.CountryPhoneCode;
            }

            countryListStr += string.Format("<option value='{0}' {2}>{1}</option>",
                                    row.CountryName,
                                    row.CountryName,
                                    selectStr);

            countryWorkListStr += string.Format("<option value='{0}' {2}>{1}</option>",
                                    row.CountryName,
                                    row.CountryName,
                                    selectWorkStr);

        }
        divHomeAddr.AppendFormat("<select id=\"AddrHomeCountry\" onchange=\"homeCountryChanged();\">{0}</select></td>", countryListStr);        
        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divContactMobile"));
        divHomeAddr.AppendFormat("<td>(<span id=\"MobTelCountryCode\">{1}</span> )<input id=\"MobTel\" type=\"text\" maxlength=\"15\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" /></td>",
                !string.IsNullOrEmpty(resCustInfo.MobTel) ? resCustInfo.MobTel.Replace("+" + selectedCountryCode, "") : resCustInfo.MobTel,
                "+" + selectedCountryCode);
        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td><td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTelFax"));
        divHomeAddr.AppendFormat("(<span id=\"AddrHomeTelCountryCode\">{1}</span> )<input id=\"AddrHomeTel\" type=\"text\" maxlength=\"15\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" />&nbsp;/&nbsp;",
                !string.IsNullOrEmpty(resCustInfo.AddrHomeTel) ? resCustInfo.AddrHomeTel.Replace("+" + selectedCountryCode, "") : resCustInfo.AddrHomeTel,
                "+" + selectedCountryCode);
        divHomeAddr.AppendFormat("(<span id=\"AddrHomeFaxCountryCode\">{1}</span> )<input id=\"AddrHomeFax\" type=\"text\" maxlength=\"15\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" /></td>",
                !string.IsNullOrEmpty(resCustInfo.AddrHomeFax) ? resCustInfo.AddrHomeFax.Replace("+" + selectedCountryCode, "") : resCustInfo.AddrHomeFax,
                "+" + selectedCountryCode);

        divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblEMail"));
        divHomeAddr.AppendFormat("<td><input id=\"AddrHomeEmail\" type=\"text\" maxlength=\"100\" style=\"width: 250px;\" value=\"{0}\" /></td>", resCustInfo.AddrHomeEmail);
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            divHomeAddr.Append("</tr><tr style=\"display: none;\">");
        else divHomeAddr.Append("</tr><tr>");
        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTaxOffice"));
        divHomeAddr.AppendFormat("<td><input id=\"HomeTaxOffice\" type=\"text\" maxlength=\"100\" style=\"width: 200px;\" value=\"{0}\" /></td>", resCustInfo.HomeTaxOffice);

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            divHomeAddr.Append("</tr><tr style=\"display: none;\">");
        else divHomeAddr.Append("</tr><tr>");

        divHomeAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTaxAccountNo"));
        divHomeAddr.AppendFormat("<td><input id=\"HomeTaxAccNo\" type=\"text\" maxlength=\"20\" style=\"width: 150px;\" value=\"{0}\" /></td>", resCustInfo.HomeTaxAccNo);
        divHomeAddr.Append("</tr></table>");
        #endregion

        #region Work Address
        divWorkAddr.AppendFormat("<table><tr><td colspan=\"2\" align=\"center\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divWorkAddressLabel"));
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblFirmName"));
        divWorkAddr.AppendFormat("<td><input id=\"WorkFirmName\" type=\"text\" maxlength=\"100\" style=\"width: 300px;\" value=\"{0}\" /></td>", resCustInfo.WorkFirmName);
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblAddress"));
        divWorkAddr.AppendFormat("<td><textarea id=\"AddrWork\" cols=\"33\" name=\"AddrHome\" rows=\"3\" >{0}</textarea></td>", resCustInfo.AddrWork);
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblZipCode"));
        divWorkAddr.AppendFormat("<td><input id=\"AddrWorkZip\" type=\"text\" maxlength=\"10\" style=\"width: 80px;\" value=\"{0}\" /></td>", resCustInfo.AddrWorkZip);
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblCityCountry"));
        divWorkAddr.AppendFormat("<td><input id=\"AddrWorkCity\" type=\"text\" maxlength=\"30\" style=\"width: 130px;\" value=\"{0}\" />&nbsp;/&nbsp;", resCustInfo.AddrWorkCity);

        //divWorkAddr.AppendFormat("<input id=\"AddrWorkCountry\" type=\"text\" maxlength=\"30\" style=\"width: 130px;\" value=\"{0}\" /></td>", resCustInfo.AddrWorkCountry);
        divWorkAddr.AppendFormat("<select id=\"AddrWorkCountry\" onchange=\"workCountryChanged();\">{0}</select></td>", countryListStr);        
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTelFax"));
        //divWorkAddr.AppendFormat("<td><input id=\"AddrWorkTel\" type=\"text\" maxlength=\"30\" style=\"width: 130px;\" value=\"{0}\" />&nbsp;/&nbsp;", resCustInfo.AddrWorkTel);
        //divWorkAddr.AppendFormat("<input id=\"AddrWorkFax\" type=\"text\" maxlength=\"30\" style=\"width: 130px;\" value=\"{0}\" /></td>", resCustInfo.AddrWorkFax);
        divWorkAddr.AppendFormat("<td>(<span id=\"AddrWorkTelCountryCode\">{1}</span> )<input id=\"AddrWorkTel\" type=\"text\" maxlength=\"30\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" />&nbsp;/&nbsp;",
                !string.IsNullOrEmpty(resCustInfo.AddrWorkTel) ? resCustInfo.AddrWorkTel.Replace("+" + selectedCountryCode, "") : resCustInfo.AddrWorkTel,
                "+" + selectedCountryCode);
        divWorkAddr.AppendFormat("(<span id=\"AddrWorkFaxCountryCode\">{1}</span> )<input id=\"AddrWorkFax\" type=\"text\" maxlength=\"30\" style=\"width: 100px;\" value=\"{0}\" onkeypress=\"return isPhoneChar(event);\" /></td>",
                !string.IsNullOrEmpty(resCustInfo.AddrWorkFax) ? resCustInfo.AddrWorkFax.Replace("+" + selectedCountryCode, "") : resCustInfo.AddrWorkFax,
                "+" + selectedCountryCode);

        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblEMail"));
        divWorkAddr.AppendFormat("<td><input id=\"AddrWorkEMail\" type=\"text\" maxlength=\"100\" style=\"width: 250px;\" value=\"{0}\" /></td>", resCustInfo.AddrWorkEMail);
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTaxOffice"));
        divWorkAddr.AppendFormat("<td><input id=\"WorkTaxOffice\" type=\"text\" maxlength=\"30\" style=\"width: 200px;\" value=\"{0}\" /></td>", resCustInfo.WorkTaxOffice);
        divWorkAddr.Append("</tr><tr>");
        divWorkAddr.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblTaxAccountNo"));
        divWorkAddr.AppendFormat("<td><input id=\"WorkTaxAccNo\" type=\"text\" maxlength=\"20\" style=\"width: 150px;\" value=\"{0}\" /></td>", resCustInfo.WorkTaxAccNo);
        divWorkAddr.Append("</tr></table>");
        #endregion

        #region Bank
        // HttpContext.GetGlobalResourceObject("CustomerAddress", )
        divBankInfo.AppendFormat("<table><tr><td colspan=\"2\" align=\"center\"><strong>{0}</strong></td></tr><tr>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "divBankInfo"));
        divBankInfo.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblBank"));
        divBankInfo.AppendFormat("<td><input id=\"Bank\" type=\"text\" maxlength=\"50\" style=\"width: 250px;\" value=\"{0}\" /></td>", resCustInfo.Bank);
        divBankInfo.Append("</tr><tr>");
        divBankInfo.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblAccount"));
        divBankInfo.AppendFormat("<td><input id=\"BankAccNo\" type=\"text\" maxlength=\"20\" style=\"width: 150px;\" value=\"{0}\" /></td>", resCustInfo.BankAccNo);
        divBankInfo.Append("</tr><tr>");
        divBankInfo.AppendFormat("<td class=\"Label\"><strong>{0} :</strong></td>",
                HttpContext.GetGlobalResourceObject("CustomerAddress", "lblIBAN"));
        divBankInfo.AppendFormat("<td><input id=\"BankIBAN\" type=\"text\" maxlength=\"50\" style=\"width: 350px;\" value=\"{0}\" /></td>", resCustInfo.BankIBAN);
        divBankInfo.Append("</tr></table>");
        #endregion

        string retVal = string.Empty;

        retVal += "{";
        retVal += "\"Contact\":\"" + divContact.ToString().Replace('"', '#').Replace('\n', ' ') + "\"";
        retVal += ",\"HomeAddr\":\"" + divHomeAddr.ToString().Replace('"', '#').Replace('\n', ' ') + "\"";
        retVal += ",\"WorkAddr\":\"" + divWorkAddr.ToString().Replace('"', '#').Replace('\n', ' ') + "\"";
        retVal += ",\"Bank\":\"" + (!Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ? divBankInfo.ToString().Replace('"', '#') : "") + "\"";
        retVal += "}";

        return retVal;
    }

}

