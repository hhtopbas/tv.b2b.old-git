﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class onlyHotelWeekendsBlank : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["SearchPLData"] = null;
        Session["Criteria"] = null;
        if (!IsPostBack)
        {
            User UserData = (User)Session["UserData"];
            Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url);  //"onlyHotelBusTripsBlank.aspx";
            Response.Redirect("~/onlyHotelHolPackCat.aspx" + Request.Url.Query);
        }
    }
}
