﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyHotelSearchResultV2.aspx.cs"
    Inherits="PackageSearchResultV2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "PackageSearchResult")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>

    <script src="Scripts/NumberFormat154.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cluetip.js" type="text/javascript"></script>

    <script src="Scripts/mustache.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/PackageSearchResult.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <link href="CSS/OnlyHotelSearchResultV2.css" rel="stylesheet" />

    <style type="text/css">
        #divNote { height: 30px; color: #CC0000; font-weight: bold; text-decoration: underline; cursor: help; }
    </style>

    <script language="javascript" type="text/javascript">

        var cultureNumber;
        var cultureDate;

        //$.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart(function () {
            $.blockUI({
                message: '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'
            });
        }).ajaxStop(function () {
            $.unblockUI();
        });

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }


        function logout() {
            self.parent.logout();
        }

        function showHotelInfo(_hotelUrl) {
            window.open(_hotelUrl);
        }

        function tryNumberFormat(obj) {
            var nf = new NumberFormat(obj);
            if (cultureNumber != null) {
                nf.PERIOD = cultureNumber.CurrencyGroupSeparator;
                nf.COMMA = cultureNumber.CurrencyDecimalSeparator;
            }
            var rsdoS = nf.PERIOD;
            var rsdoD = nf.COMMA;
            nf.setPlaces(2);
            nf.setSeparators(true, rsdoS, rsdoD);
            obj = nf.toFormatted();
            return obj;
        }

        function showDialog(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function showAlert(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function changeRoomPrice(rowNumber, refNo) {
            var tablePrice = $('#tablePrice_' + rowNumber.toString());
            var hotelCheckInNightPrice = tablePrice.find("input");

            var hotelPrice = $("#HotelCheckInNightPrice_" + rowNumber);
            hotelPrice.html('');
            var priceRefNoList = '';
            var price = 0.0;
            var salecur = '';
            var first = true;
            $.each(hotelCheckInNightPrice, function (i) {
                if (this.checked) {
                    var _price = $(this).attr("price");
                    _price = _price.replace(',', '').replace('.', '');
                    var _salecur = $(this).attr("salecur");
                    var _stopSale = $(this).attr("stopSale");
                    if (priceRefNoList.length > 0) priceRefNoList += ',';
                    priceRefNoList += this.value;
                    price += parseFloat(_price) / 100;

                    if (salecur != '' && salecur != _salecur) {
                        hotelPrice.html('<%= GetGlobalResourceObject("PackageSearchResult", "CurrencyTypeNotTheSame") %>');
                        $("#btnBook_" + rowNumber.toString()).attr("disabled", true);
                        return;
                    }
                    else {
                        $("#btnBook_" + rowNumber.toString()).removeAttr("disabled");
                        salecur = _salecur;
                    }

                    hotelPrice.html(tryNumberFormat(price) + ' ' + salecur);

                    if (_stopSale == '2') {
                        $("#btnBook_" + rowNumber.toString()).val('<%= GetGlobalResourceObject("PackageSearchResult", "stopSale") %>');
                        $("#btnBook_" + rowNumber.toString()).attr("disabled", true);
                    }
                    else {
                        $("#btnBook_" + rowNumber.toString()).val('<%= GetGlobalResourceObject("PackageSearchResult", "book") %>');
                        $("#btnBook_" + rowNumber.toString()).removeAttr("disabled");
                    }
                }
            });
        }

        function clearOffer() {
            $.ajax({
                type: "POST",
                url: "OnlyHotelSearchResultV2.aspx/clearOffer",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function offerReportShow(param1, param2) {
            if (param1 == '' && (param2 != null || param2 != ''))
                showAlert(param2);
            else window.open(param1);
        }

        function createOffer() {
            self.parent.createOffer();
        }

        function addOffer(rowNumber) {
            var tablePrice = $('#tablePrice_' + rowNumber.toString());
            var hotelCheckInNightPrice = tablePrice.find("input");
            var priceRefNoList = '';
            $.each(hotelCheckInNightPrice, function (i) {
                if (this.checked) {
                    if (priceRefNoList.length > 0) priceRefNoList += ',';
                    priceRefNoList += this.value;
                }
            });

            $.ajax({
                type: "POST",
                url: "OnlyHotelSearchResultV2.aspx/addOffer",
                data: '{"BookList":"' + priceRefNoList + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function book(rowNumber) {
            var tablePrice = $('#tablePrice_' + rowNumber.toString());
            var hotelCheckInNightPrice = tablePrice.find("input");
            var priceRefNoList = '';
            var price = 0.0;
            var stopSale = false;
            var allotment = false;
            var allotmentMsg = '';
            var stopSaleMsg = '';
            $.each(hotelCheckInNightPrice, function (i) {
                if (this.checked) {
                    if (priceRefNoList.length > 0) priceRefNoList += ',';
                    priceRefNoList += this.value;
                    price += parseFloat($(this).attr("price"));
                    var _stopSale = $(this).attr("stopSale");
                    var _stopSaleMsg = $(this).attr("stopSaleMsg");
                    if (_stopSale == '1') {
                        stopSale = true;
                        stopSaleMsg = _stopSaleMsg;
                    }
                }
            });

            $.ajax({
                async: false,
                type: "POST",
                url: "OnlyHotelSearchResultV2.aspx/getAllotmentControl",
                data: '{"RefNoList":"' + priceRefNoList + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        allotment = true;
                        allotmentMsg = msg.d;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            if (stopSale == true) {
                $(function () {
                    $("#messages").html(stopSaleMsg);
                    $("#dialog").dialog("destroy");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                                $(this).dialog('close');
                                self.parent.bookRooms(priceRefNoList);
                            },
                            '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                });
            }
            else if (allotment == true) {
                $(function () {
                    $("#messages").html(allotmentMsg);
                    $("#dialog").dialog("destroy");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                                $(this).dialog('close');
                            }
                        }
                    });
                });
            }
            else {
                self.parent.bookRooms(priceRefNoList);
            }
    }

    function textTrancateRun() {

        $(".resultGrid .PriceTable .PriceDetail .prices .boardConsept").textTruncate($(".resultGrid .PriceTable .PriceDetail .prices .board").width() - 5);
        $(".resultGrid .PriceTable .PriceDetail .prices .roomConsept").textTruncate($(".resultGrid .PriceTable .PriceDetail .prices .room").width() - 5);
        $(".resultGrid .PriceTable .PriceDetail .prices .accom span").textTruncate($(".resultGrid .PriceTable .PriceDetail .prices .accom").width() - 5);

        $('.roomConsept').cluetip({
            activation: 'click',
            width: 400,
            arrows: true,
            dropShadow: false,
            hoverIntent: false,
            sticky: true,
            mouseOutClose: false,
            closePosition: 'title',
            closeText: '<img src="Images/cancel.png" title="close" />'
        });
        $('.boardConsept').cluetip({
            activation: 'click',
            width: 400,
            arrows: true,
            dropShadow: false,
            hoverIntent: false,
            sticky: true,
            mouseOutClose: false,
            closePosition: 'title',
            closeText: '<img src="Images/cancel.png" title="close" />'
        });
    }

    function filterResult(field, value) {
        $.ajax({
            type: "POST",
            url: "OnlyHotelSearchResultV2.aspx/setResultFilter",
            data: '{"FieldName":"' + field + '","Value":"' + value + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d != null && msg.d != '') {
                    $("#gridResultSpan").html('');
                    $("#gridResultSpan").html(msg.d);
                    textTrancateRun();
                    reSizeFrame($("#gridResultSpan")[0].scrollHeight);
                    var cultureNumberStr = $("#cultureNumber").val();
                    if (cultureNumberStr != '') {
                        cultureNumber = $.json.decode(cultureNumberStr);
                    }
                    else cultureNumber = null;
                }
            },
            error: function (xhr, msg, e) {
                if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                    showDialog(xhr.responseText);
            },
            statusCode: {
                408: function () {
                    logout();
                }
            }
        });
    }

    function setPage(pageNo) {
        var pageItemCount = getPageCount();
        $.ajax({
            type: "POST",
            url: "OnlyHotelSearchResultV2.aspx/getResultGrid",
            data: '{"page":' + pageNo + ',"pageItemCount":' + pageItemCount + ',"filtered":' + false + ',"paged":' + true + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                $("#gridResultSpan").html('');
                $("#gridResultSpan").html(msg.d);
                textTrancateRun();
                reSizeFrame($("#gridResultSpan")[0].scrollHeight);
            },
            error: function (xhr, msg, e) {
                if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                    showDialog(xhr.responseText);
            },
            statusCode: {
                408: function () {
                    logout();
                }
            }
        });
    }

    function getPageCount() {
        return parseInt('20');
    }


    function getResultDataAndCreate(tmpHtml) {
        var pageItemCount = getPageCount();
        var data = new Object();
        data.page = 0;
        data.pageItemCount = parseInt(pageItemCount);
        data.filtered = false;
        data.paged = false;
        var tmpl = '';
        $.ajax({
            async: true,
            type: "POST",
            url: "OnlyHotelSearchResultV2.aspx/getResultGrid",
            data: $.json.encode(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                var data = msg.d;
                var html = Mustache.render(tmpHtml, data);
                $("#searchResult").html(html);
                if (data.ResultNote == true) {
                    $("#resultNote").html(data.SearchResultNote);
                }
                reSizeFrame();
            },
            error: function (xhr, msg, e) {
                if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                    showDialog(xhr.responseText);
            },
            statusCode: {
                408: function () {
                    logout();
                }
            }
        });
    }
    function getResultGrid() {
        $.get($("#tmplPath").val() + 'OnlyHotelSearchTmpl.html?v=201408271108', function (html) {
            getResultDataAndCreate(html);
        }).fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ', ' + error;
            showAlert("Request Failed: " + err);
        });
    }

    function getPage() {
        getResultGrid();
    }

    function pageChangeButtom(changeVal) {
        var pageNo = $("#pageDropDownListButtom").val();
        var _PageNo = parseInt(pageNo);
        if (changeVal != undefined && changeVal != '') {
            _PageNo = _PageNo + parseInt(changeVal);
        }
        setPage(_PageNo.toString());
    }

    function pageChange(changeVal) {
        var pageNo = $("#pageDropDownListTop").val();
        var _PageNo = parseInt(pageNo);
        if (changeVal != undefined && changeVal != '') {
            _PageNo = _PageNo + parseInt(changeVal);
        }
        setPage(_PageNo.toString());
    }

    function showBrochure(holPack, CheckIn, CheckOut, Market) {
        self.parent.showBrochure(holPack, CheckIn, CheckOut, Market);
    }

    function reSizeFrame() {
        self.parent.reSizeResultFrame(document.body.offsetHeight);
    }

    function prevDate(pdate) {
        self.parent.reSearch(pdate);
    }

    function nextDate(ndate) {
        self.parent.reSearch(ndate);
    }

    $(document).ready(function () {
        getPage();
    });
    </script>

</head>
<body class="ui-helper-reset">
    <form id="formPackageSearchResult" runat="server" class="ui-helper-clearfix">
        <asp:HiddenField ID="tmplPath" runat="server" />
        <div id="searchResult" class="ui-helper-clearfix resultBody">
        </div>
        <div id="dialog-message" title="" class="ui-helper-hidden">
            <span id="messages">Message</span>
        </div>
    </form>
</body>
</html>
