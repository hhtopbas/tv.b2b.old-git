﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PackageSearchV2.aspx.cs"
    Inherits="TvSearch.PackageSearch" EnableEventValidation="false" %>

<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="cache-control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "PackageSearch")%></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />
    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
    <script src="Scripts/jquery.printPage.js" type="text/javascript"></script>
    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/PriceSearchV2.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        var myWidth = 0, myHeight = 0;

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        //$.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart(function () {
            $.blockUI({
                message: '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'
            });
        }).ajaxStop(function () {
            $.unblockUI();
            // return false;
        });

        function startWait() {
            $.blockUI({
                message: '<h3><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h3>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .9,
                    color: '#fff',
                    'font-size': '15px',
                    'font-weight': 'bold'
                }
            });
        }


        function stopWait() {
            $.unblockUI();
        }

        function logout() {
            $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }
            });
            window.setTimeout(function () { window.location = "<%=VirtualPathUtility.ToAbsolute("~/Default.aspx")%>"; }, 10000);
        }

        function reSizeFilterFrame(h) {
            $('#filterFrame').height((h + 60) + 'px');
        }

        function reSizeResultFrame(h) {
            var height = parseInt(h);
            if (height > 0) {
                $('#resultFrame').height(height + 'px');
            } else {
                $('#resultFrame').height(600 + 'px');
            }
        }

        function showDialog(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    autoOpen: true,
                    position: {
                        my: 'center',
                        at: 'center'
                    },
                    modal: true,
                    resizable: true,
                    autoResize: false,
                    bigframe: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        var NS = document.all;

        function maximize() {
            var myWidth = screen.availWidth;
            var myHeight = screen.availHeight;
            if (location.href.indexOf('pic') == -1) {
                if (window.opera) { } else {
                    top.window.moveTo(0, 0);
                    if (document.all) { top.window.resizeTo(myWidth, myHeight); }
                    else
                        if (document.layers || document.getElementById) {
                            if ((top.window.outerHeight < myHeight) || (top.window.outerWidth < myWidth)) {
                                top.window.outerHeight = myWidth + 'px';
                                top.window.outerWidth = myHeight + 'px';
                            }
                        }
                }
            }
        }

        function searchPrice() {
            window.scrollTo(0, 0);
            $("#resultFrame").attr("src", "PackageSearchResultV2.aspx");
        }

        function viewReportAddButton(btnName, func) {
            $.extend($.ui.dialog.prototype, {
                'addbutton': function (buttonName, func) {
                    var buttons = this.element.dialog('option', 'buttons');
                    buttons[buttonName] = func;
                    this.element.dialog('option', 'buttons', buttons);
                }
            });
            $("#dialog-viewReport").dialog('addbutton', btnName, func);
        }

        function viewReportRemoveButton(btnName) {
            $.extend($.ui.dialog.prototype, {
                'removebutton': function (buttonName) {
                    var buttons = this.element.dialog('option', 'buttons');
                    delete buttons[buttonName];
                    this.element.dialog('option', 'buttons', buttons);
                }
            });
            $("#dialog-viewReport").dialog('removebutton', btnName);
        }

        function printOffer(url, width, height) {
            if ($.browser.msie) {
                var windowSizeArray = ["width=" + width + ",height=" + height + ",scrollbars=yes"];
                var windowName = "popUp";
                var windowSize = windowSizeArray[0];
                window.open(url, windowName, windowSize);
            }
            else if ($.browser.chrome || $.browser.safari) {
                $("#viewReport").width(width + '32px');
                $("#viewReport").height(height + '32px');
                id = 'viewReport';
                var iframe = document.frames ? document.frames[id] : document.getElementById(id);
                var ifWin = iframe.contentWindow || iframe;
                iframe.focus();
                ifWin.print();
                return false;
            }
            else {
                $("#viewReport").width(width + 32);
                $("#viewReport").height(height + 32);
                document.getElementById('viewReport').focus();
                document.getElementById('viewReport').contentWindow.print();
            }
        }

        function viewPDFReport() {
            var htmlUrl = 'CreateOffer.aspx';
            if ($("#clientOfferVersion").val() == 'V2')
                htmlUrl = 'CreateClientOffer.aspx';
            var DocName = 'Offer';

            var width = $("#viewReport").contents().find(".mainPage").width();
            var viewReport = $("#viewReport").contents();
            var params = new Array();
            viewReport.find('input').each(function () {
                var o = new Object();
                o.Code = $(this).attr('name');
                o.Name = $(this).val();
                params.push(o);
            });

            var _html = viewReport.find('#htmlCreateOffer').html();

            var noPrint = viewReport.find("#noPrintDiv").html();
            if (noPrint != null)
                _html = _html.replace(noPrint, '&nbsp;');

            _html = _html.replace(/noPrint/g, 'noPDF');
            _html = _html.replace(/"/g, '|');

            var obj = new Object();
            obj.reportType = DocName;
            obj._html = _html;
            obj.pageWidth = width;
            obj.urlBase = htmlUrl;
            obj.param = params;
            $.ajax({
                type: "POST",
                url: "PackageSearchV2.aspx/viewPDFReport",
                data: $.json.encode(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        $("#viewReport").removeAttr("src");
                        var _pdfViewPage = "ViewPDF.aspx"
                        $("#viewReport").attr("src", _pdfViewPage + "?url=" + msg.d);
                        viewReportRemoveButton('<%= GetGlobalResourceObject("LibraryResource", "btnPrint") %>');
                        viewReportRemoveButton('<%= GetGlobalResourceObject("LibraryResource", "btnPDF") %>');
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function createOffer() {
            window.scrollTo(0, 0);
            var htmlUrl = 'CreateOffer.aspx';

            if ($("#clientOfferVersion").val() == 'V2')
                htmlUrl = '/ClientOffers/CreateClientOffer.aspx';

            window.scrollTo(0, 0);
            $('html').css('overflow', 'hidden');

            var DocName = 'Offer';
            $("#viewReport").removeAttr("src");
            var url = window.location.href;
            url = url.toString().substring(0, url.toString().indexOf('PackageSearchV2.aspx'));
            $("#viewReport").attr("src", url + htmlUrl);
            $("#dialog-viewReport").dialog(
            {
                autoOpen: true,
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnPrint") %>': function () {
                        var width = $("#viewReport").contents().find(".mainPage").width();
                        var height = $("#viewReport").contents().find(".mainPage").height();
                        var param1 = $("#viewReport").contents().find("#param1").val();
                        printOffer(htmlUrl + "?docName=" + DocName + "&print=1" + (param1 != undefined && param1 != '' ? "&param1=" + param1 : "") + "&" + $("#viewReport").contents().find('input').serialize(), width, height);
                    },
                    '<%= GetGlobalResourceObject("LibraryResource", "btnPDF") %>': function () {
                        viewPDFReport();
                        return false;
                    },
                    '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                        $(this).dialog('close');
                        return true;
                    }
                },
                close: function (event, ui) { $('html').css('overflow', 'auto'); }
            }).dialogExtend({
                "maximize": true,
                "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                }
            });
            $("#dialog-viewReport").dialogExtend("maximize");
        }

        function createOfferV2() {
            window.scrollTo(0, 0);
            var htmlUrl = '/ClientOffers/CreateClientOffer.aspx';
            $('html').css('overflow', 'hidden');
            var DocName = 'ClientOffer';
            $("#ClientOffer").removeAttr("src");
            var url = window.location.href;
            url = url.toString().substring(0, url.toString().indexOf('PackageSearchV2.aspx'));
            $("#ClientOffer").attr("src", url + htmlUrl);
            $("#dialog-ClientOffer").dialog(
            {
                autoOpen: true,
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                        $(this).dialog('close');
                        return true;
                    }
                },
                close: function (event, ui) { $('html').css('overflow', 'auto'); }
            }).dialogExtend({
                "maximize": true,
                "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                }
            });
            $("#dialog-ClientOffer").dialogExtend("maximize");
        }

        function showBrochure(holPack, CheckIn, CheckOut, Market) {
            var obj = new Object();
            obj.holPack = holPack;
            obj.CheckIn = CheckIn;
            obj.CheckOut = CheckOut;
            obj.Market = Market;
            $.ajax({
                type: "POST",
                data: $.json.encode(obj),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: "PackageSearch.aspx/getBrochure",
                success: function (msg) {

                    if (msg.d != "") {

                        window.scrollTo(0, 0);
                        $('html').css('overflow', 'hidden');
                        $("#viewReport").removeAttr("src");
                        var _pdfViewPage = "ViewPDF.aspx"
                        $("#viewReport").attr("src", _pdfViewPage + "?url=" + msg.d);
                        var width = $('body').width() - 50;
                        $("#viewReport").width(width);

                        $("#dialog-viewReport").dialog({
                            autoOpen: true,
                            modal: true,
                            resizable: true,
                            autoResize: true,
                            bigframe: true,
                            buttons: { '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () { $(this).dialog('close'); return true; } },
                            close: function (event, ui) { $('html').css('overflow', 'auto'); }
                        }).dialogExtend({
                            "maximize": true,
                            "icons": {
                                "maximize": "ui-icon-circle-plus",
                                "restore": "ui-icon-pause"
                            }
                        });
                        $("#dialog-viewReport").dialogExtend("maximize");
                        return true;

                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        alert(xhr.responseText);
                        return false;
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function bookReservation(refNo) {
            $.ajax({
                type: "POST",
                data: '{"refNo":' + refNo + '}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: "PackageSearchV2.aspx/getBookReservation",
                success: function (msg) {
                    if (msg.d != null && msg.d.resOK) {
                        window.scrollTo(0, 0);
                        $('html').css('overflow', 'hidden');
                        $("#MakeReservation").attr("src", $("#basePageUrl").val() + 'MakeReservation' + msg.d.version + '.aspx');
                        $("#dialog-MakeReservation").dialog(
                            {
                                autoOpen: true,
                                modal: true,
                                width: 990,
                                height: 700,
                                resizable: true,
                                autoResize: true,
                                bigframe: true,
                                close: function (event, ui) { $('html').css('overflow', 'auto'); }
                            }).dialogExtend({
                                "maximize": true,
                                "icons": {
                                    "maximize": "ui-icon-circle-plus",
                                    "restore": "ui-icon-pause"
                                }
                            });
                        $("#dialog-MakeReservation").dialogExtend("maximize");
                        return true;
                    }
                    else {
                        showDialog(msg.d != null ? msg.d.errMsg : 'unknown error.');
                        return false;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function bookRooms(bookRoomList) {
            bookReservation(bookRoomList);
        }

        function cancelMakeRes() {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            $('html').css('overflow', 'auto');
        }

        function gotoResViewPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            $('html').css('overflow', 'auto');
            window.location = 'ResView.aspx?ResNo=' + ResNo;
        }

        function paymentPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            window.open('Payments/BeginPayment.aspx?ResNo=' + ResNo, '_blank');
        }

        function filterDivShowHide() {
            var filterDiv = $("#filterFrameDiv");
            var resultDiv = $("#divResultDiv");
            var btnHideShow = $("#btnHideShow");
            var filterFrame = $("#filterFrame");
            var showHideImg = $(".showHideImg");
            if (filterDiv.width() > 50) {
                resultDiv.width(975);
                filterDiv.width(1);
                filterFrame.width(1);
                showHideImg.attr('src', 'Images/showFilter.png');
                showHideImg.attr('alt', '<%= GetGlobalResourceObject("PackageSearchResultV2", "ShowFilter") %>');
                showHideImg.attr('title', '<%= GetGlobalResourceObject("PackageSearchResultV2", "ShowFilter") %>');
            }
            else {
                resultDiv.width(730);
                filterDiv.width(250);
                filterFrame.width(250);
                showHideImg.attr('src', 'Images/hideFilter.png');
                showHideImg.attr('alt', '<%= GetGlobalResourceObject("PackageSearchResultV2", "HideFilter") %>');
                    showHideImg.attr('title', '<%= GetGlobalResourceObject("PackageSearchResultV2", "HideFilter") %>');
            }
        }

        $(document).ready(
            function () {
                maximize();
                $("#resultFrame").removeAttr("src");
                $("#filterFrame").removeAttr("src");
                $("#filterFrame").attr("src", "PackageSearchFilterV2.aspx");
            }
        );

        function pageReLoad() {
            window.location = window.location.href;
        }

    </script>

</head>
<body>
    <form id="PackageSearchV2Form" runat="server">
        <div class="Page">
            <div>
                <tv1:Header ID="tvHeader" runat="server" />
                <tv1:MainMenu ID="tvMenu" runat="server" />
            </div>
            <div class="Content">
                <div style="width: 100%;" class="ui-helper-clearfix ui-helper-reset">
                    <div id="filterFrameDiv" style="border: solid 1px #DDD;">
                        <iframe id="filterFrame" width="100%" style="min-height: 550px; height: 750px;" frameborder="0"></iframe>
                    </div>
                    <div id="btnHideShow" style="float: left; min-height: 550px; height: 750px; width: 16px; cursor: pointer;"
                        class="ui-corner-all" onclick="filterDivShowHide();">
                        <img class="showHideImg" alt='<%= GetGlobalResourceObject("PackageSearchResultV2", "HideFilter") %>'
                            title='<%= GetGlobalResourceObject("PackageSearchResultV2", "HideFilter") %>'
                            src="Images/hideFilter.png" />
                    </div>
                    <div id="divResultDiv" style="float: left; min-height: 550px;">
                        <iframe id="resultFrame" width="100%" frameborder="0" style="min-height: 400px;"></iframe>
                    </div>
                </div>
            </div>
            <div class="Footer">
                <tv1:Footer ID="tvfooter" runat="server" />
            </div>
        </div>
        <div id="dialog-MakeReservation" title='<%= GetGlobalResourceObject("MakeReservation", "lblMakeReservation") %>'
            style="display: none; text-align: center;">
            <iframe id="MakeReservation" runat="server" height="100%" width="960px" frameborder="0"
                style="clear: both; text-align: left;"></iframe>
        </div>
        <%--Message, Confirm Area--%>
        <div id="dialog-message" title="" style="display: none;">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
            </p>
        </div>
        <%--Message, Confirm Area--%>
        <div id="dialog-viewReport" title='' style="display: none; text-align: center;">
            <iframe id="viewReport" runat="server" frameborder="0" style="clear: both; width: 100%; height: 100%; text-align: left;"></iframe>
        </div>

        <%--Offer Area--%>
        <asp:HiddenField ID="clientOfferVersion" runat="server" Value="" />
        <div id="dialog-ClientOffer" title='' class="ui-helper-hidden">
            <iframe id="ClientOffer" class="ui-helper-clearfix" style="width: 100%; height: 100%; text-align: left;" frameborder="0"></iframe>
        </div>
        <%--Offer Area--%>
    </form>
</body>
</html>
