﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;

using TvBo;

public partial class OnlyHotelMixPaxSearchResultV2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) {
            HttpContext.Current.Response.StatusCode = 408;
            return false;
        }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["MixCriteria"] == null)
            return null;

        if (UserData.Pxm_Use && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value && UserData.PaxSetting.Pxm_WorkType != PxmWorkType.UsePaximumButton && !string.IsNullOrEmpty(UserData.PaxSetting.Pxm_AutMainCode)) {
            MixSearchCriteria criteria = (MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];
            string baseUrl = string.Format("{0}search", UserData.PaxToken.paximumApiUrl);
            var _paxList = new {
                token = UserData.PaxToken.token,
                apiAddress = baseUrl,
                mediaAddress = "http://media.dev.paximum.com/hotelimages_125x125/",
                rowCountPerPage = 10
            };
            string pLocation = criteria.PaxLocation.FirstOrDefault();
            int pNight = criteria.NightFrom;
            List<DestinationSearchCriteria> destination = new List<DestinationSearchCriteria>();
            destination.Add(new DestinationSearchCriteria { Id = pLocation, Type = "city" });
            if (!string.IsNullOrEmpty(criteria.Hotel)) {
                foreach (string row in criteria.Hotel.Split('|')) {
                    destination.Add(new DestinationSearchCriteria { Id = row, Type = "hotel" });
                }
            }
            SearchHotelRequest request = new SearchHotelRequest();

            request.Destinations = destination.Where(w => w.Type == "city").ToArray();

            List<RoomSearchCriteria> roomList = new List<RoomSearchCriteria>();
            foreach (MixSearchCriteriaRoom row in criteria.RoomInfoList) {
                RoomSearchCriteria room = new RoomSearchCriteria();
                room.Adults = row.AdultCount;
                List<int> childAgeList = new List<int>();
                foreach (var cRow in row.ChildAge)
                    childAgeList.Add(cRow);
                room.ChildrenAges = childAgeList.ToArray();
                roomList.Add(room);
            }
            //new RoomSearchCriteria { Adults = pAdl, ChildrenAges = pChildAges.ToArray() };
            Location defaultCountry = new Locations().getLocation(UserData.Market, new Locations().getLocationForCountry(UserData.AgencyRec.Location.HasValue ? UserData.AgencyRec.Location.Value : 0, ref errorMsg), ref errorMsg);
            List<MixCountry> paxCountries = new MixSearch().getCountryList(UserData, ref errorMsg);
            MixCountry paxCountry = paxCountries.Find(f => f.ISO3 == (defaultCountry != null ? defaultCountry.CountryCode : ""));
            request.Rooms = roomList.ToArray();
            request.CheckinDate = new DateTime(criteria.CheckIn.Ticks, DateTimeKind.Utc);
            request.CheckoutDate = new DateTime(criteria.CheckIn.AddDays(pNight).Ticks, DateTimeKind.Utc);
            request.Currency = string.IsNullOrEmpty(criteria.CurrentCur) ? "EUR" : "USD";// criteria.CurrentCur;
            request.CustomerNationality = string.IsNullOrEmpty(criteria.PaxNationality) ? (paxCountry != null ? paxCountry.ISO2 : "TR") : criteria.PaxNationality;
            request.Language = "en";
            request.FilterUnavailable = criteria.PaxAvailable;

            var _request = new {
                Destinations = destination.Where(w => w.Type == "city").ToArray(),
                CheckinDate = new DateTime(criteria.CheckIn.Ticks, DateTimeKind.Utc).ToString("yyyy-MM-dd"),
                CheckoutDate = new DateTime(criteria.CheckIn.AddDays(pNight).Ticks, DateTimeKind.Utc).ToString("yyyy-MM-dd"),
                Rooms = roomList.ToArray(),
                CustomerNationality = string.IsNullOrEmpty(criteria.PaxNationality) ? (paxCountry != null ? paxCountry.ISO2 : "TR") : criteria.PaxNationality,
                Currency = string.IsNullOrEmpty(criteria.CurrentCur) ? "EUR" :"USD",// criteria.CurrentCur,
                FilterUnavailable = criteria.PaxAvailable,
                WithPromotion = false,
                Language = "en"
            };

            var _localisation = new {
                room = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblRoomType"),
                board = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblBoard"),
                status = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblStatus"),
                price = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblPrice"),
                reservation = HttpContext.GetGlobalResourceObject("PackageSearchResult", "book"),
                offers = "Offers",
                available = "Available",
                notAvailable = "Not Available",
                first = HttpContext.GetGlobalResourceObject("LibraryResource", "sFirst"),
                previous = HttpContext.GetGlobalResourceObject("LibraryResource", "sPrevious"),
                next = HttpContext.GetGlobalResourceObject("LibraryResource", "sNext"),
                last = HttpContext.GetGlobalResourceObject("LibraryResource", "sLast"),
                specialOffer = "Special Offer",
                conditions = "Conditions",
                conditionTitle = "{0} {1} have to be paid if booking is cancelled after and on {2}",
                hotelNotFoundMessage = "Hotel Not Found for your criterias!",
                checkStatus = "Check"
            };

            return new {
                paxList = _paxList,
                request = _request,
                localisation = _localisation
            };
        } else {
            return null;
        }
    }
}