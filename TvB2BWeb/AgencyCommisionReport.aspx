﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgencyCommisionReport.aspx.cs"
    Inherits="AgencyCommisionReport" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "AgencyCommisionReport")%></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="Scripts/NumberFormat154.js" type="text/javascript"></script>

    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>

    <script src="Scripts/datatable/jquery.dataTables.min.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/AgencyPayment.css" rel="Stylesheet" type="text/css" />
    <link href="CSS/jquery.table.jui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function() {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function(e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function logout() {
            $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                        $(this).dialog("close");
                        $(this).dialog("destroy");
                        window.location = 'Default.aspx';
                    }
                }
            });
        }

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function showAlert(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function DateChanged(_TextDay1, _PopCal) {
            if (_TextDay1.value == '') return
            var _TextDay2 = document.getElementById('<%= fltBegDate.ClientID %>');
            if ((!_TextDay1) || (!_PopCal)) return
            if (_TextDay2.value != '') {
                var _format = _TextDay1.getAttribute("Format");
                var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                if (_Day1 > _Day2) {
                    showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                    _TextDay2.value = _TextDay1.value;
                }
            }
        }

        function DateChangedTo(_TextDay2, _PopCal) {
            if (_TextDay2.value == '') return
            var _TextDay1 = document.getElementById('<%= fltEndDate.ClientID %>');
            if ((!_TextDay2) || (!_PopCal)) return
            if (_TextDay1.value != '') {
                var _format = _TextDay2.getAttribute("Format");
                var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                if (_Day1 > _Day2) {
                    showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                    _TextDay2.value = _TextDay1.value;
                }
            }
        }

        function showDialog(msg, transfer, trfUrl) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                            if (transfer == true) {
                                var url = trfUrl;
                                $(location).attr('href', url);
                            }
                        }
                    }
                });
            });
        }

        function showMessage(msg, transfer, trfUrl) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                        ,
                        '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        var NS = document.all;

        function maximize() {
            var maxW = screen.availWidth;
            var maxH = screen.availHeight;
            if (location.href.indexOf('pic') == -1) {
                if (window.opera) { } else {
                    top.window.moveTo(0, 0);
                    if (document.all) { top.window.resizeTo(maxW, maxH); }
                    else
                        if (document.layers || document.getElementById) {
                        if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                            top.window.outerHeight = maxH; top.window.outerWidth = maxW;
                        }
                    }
                }
            }
        }

        function btnUnFilter_onclick() {
            window.location = "AgencyCommisionReport.aspx";
        }

        function tryNumberFormat(obj) {
            var nf = new NumberFormat(obj);
            var rsdoS = nf.PERIOD;
            var rsdoD = nf.COMMA;
            nf.setPlaces(2);
            nf.setSeparators(true, rsdoS, rsdoD);
            obj = nf.toFormatted();
            return obj;
        }

        function GetResult() {
            var oLanguage = {
                "sProcessing": '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>',
                "sLengthMenu": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sZeroRecords": '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>',
                "sEmptyTable": '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>',
                "sInfo": '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>',
                "sInfoFiltered": '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>',
                "sInfoPostFix": '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>',
                "sSearch": '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>',
                "sUrl": '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>',
                "oPaginate": {
                    "sFirst": '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>',
                    "sPrevious": '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>',
                    "sNext": '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>',
                    "sLast": '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>'
                },
                "fnInfoCallback": null
            };
            var dateformat = $("#fltBegDate").attr("Format");
            var dataString = '{"begDate":"' + $("#fltBegDate").val() + '",';
            dataString += '"endDate":"' + $("#fltEndDate").val() + '",';
            dataString += '"agency":"' + $("#fltAgency").val() + '",';
            dataString += '"RegisterDate":"' + $("#fltRegisterDate").val() + '",';
            dataString += '"dateFormat":"' + dateformat + '"}';

            //resBegDateFrom, resBegDateTo
            $.ajax({
                type: "POST",
                data: dataString,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'AgencyCommisionReport.aspx/GetResult',
                success: function(msg) {
                    $("#resMonitorGrid").html('');
                    $("#resMonitorGrid").html(msg.d);
                    $('#resTable').dataTable({
                        "bSort": false,
                        "bJQueryUI": false,
                        "sScrollX": "1000px",
                        "sScrollXInner": "100%",
                        "bScrollCollapse": true,
                        "sPaginationType": "full_numbers",
                        "sScrollY": "600",
                        "bFilter": false,
                        "bJQueryUI": true,
                        "oLanguage": oLanguage,
                        "aaSorting": [],
                        "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
                            /*
                            * Calculate the total market share for all browsers in this table (ie inc. outside
                            * the pagination)
                            */
                            var sumDebit = 0.0;
                            var sumCommision = 0.0;
                            var sumBrokerCommision = 0.0;
                            var sumPayment = 0.0;
                            var sumBalance = 0.0;
                            var sumAdult = 0;
                            var sumChild = 0;
                            var sumPax = 0;
                            for (var i = 0; i < aaData.length; i++) {
                                sumDebit += parseFloat(aaData[i][3]) * 1;
                                sumCommision += parseFloat(aaData[i][4].replace(",",".")) * 1;
                                sumBrokerCommision += parseFloat(aaData[i][5].replace(",",".")) * 1;
                                sumPayment += parseFloat(aaData[i][6].replace(",", ".")) * 1;
                                sumBalance += parseFloat(aaData[i][7].replace(",", ".")) * 1;
                                sumAdult += parseInt(aaData[i][8]) * 1;
                                sumChild += parseInt(aaData[i][9]) * 1;
                                sumPax += parseInt(aaData[i][10]) * 1;
                            }

                            /* Modify the footer row to match what we want */
                            var nCells = nRow.getElementsByTagName('th');
                            nCells[3].innerHTML = tryNumberFormat(sumDebit);
                            nCells[4].innerHTML = tryNumberFormat(sumCommision);
                            nCells[5].innerHTML = tryNumberFormat(sumBrokerCommision);
                            nCells[6].innerHTML = tryNumberFormat(sumPayment);
                            nCells[7].innerHTML = tryNumberFormat(sumBalance);
                            nCells[8].innerHTML = sumAdult.toString();
                            nCells[9].innerHTML = sumChild.toString();
                            nCells[10].innerHTML = sumPax.toString();
                        }
                    });
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function page_load() {
            maximize();
            $.ajax({
                type: "POST",
                data: '{}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'AgencyCommisionReport.aspx/GetAgencyList',
                success: function(msg) {
                    var data = $.json.decode(msg.d);
                    setfields(data);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }
        function setfields(data) {
            $("#fltAgency").html('');
            $("#fltAgency").append("<option value='' >" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
            $.each(data, function(i) {
                $("#fltAgency").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
            GetResult();
        }
    </script>

</head>
<body onload="page_load();">
    <form id="form1" runat="server">
    <div class="Page">
        <tv1:Header ID="tvHeader" runat="server" />
        <tv1:MainMenu ID="tvMenu" runat="server" />
        <div style="text-align: center;">
            <div id="divFilter" style="width: 602px; text-align: left;">
                <div id="divdateFilter" style="clear: both; width: 600px; height: 25px;">
                    <div style="width: 200px; float: left; margin-top: 3px; text-align: right;">
                        <%= GetGlobalResourceObject("AgencyCommisionReport", "lblFilterDateOption")%>
                        :
                    </div>
                    <div style="float: left; width: 400px; text-align: left;">
                        <select id="fltRegisterDate">
                            <option value="Y">
                                <%= GetGlobalResourceObject("AgencyCommisionReport", "lblForResSaleDate")%></option>
                            <option value="N" selected="selected">
                                <%= GetGlobalResourceObject("AgencyCommisionReport", "lblForResBegDate")%></option>
                        </select>
                    </div>
                </div>
                <div id="divBegDate" style="clear: both; width: 600px; height: 25px;">
                    <div style="width: 200px; float: left; margin-top: 3px; text-align: right;">
                        <%= GetGlobalResourceObject("StopSale", "lblFirstDate")%>
                        :
                    </div>
                    <div style="float: left; width: 400px; text-align: left;">
                        <asp:TextBox ID="fltBegDate" runat="server" Width="110px" Text="" ReadOnly="True" />
                        <rjs:PopCalendar ID="ppcBegDate" runat="server" Control="fltBegDate" ClientScriptOnDateChanged="DateChanged" />
                    </div>
                </div>
                <div id="divEndDate" style="clear: both; width: 600px; height: 25px;">
                    <div style="width: 200px; float: left; margin-top: 3px; text-align: right;">
                        <%= GetGlobalResourceObject("StopSale", "lblLastDate")%>
                        :
                    </div>
                    <div style="float: left; width: 400px; text-align: left;">
                        <asp:TextBox ID="fltEndDate" runat="server" Width="110px" ReadOnly="True" />
                        <rjs:PopCalendar ID="ppcEndDate" runat="server" Control="fltEndDate" ClientScriptOnDateChanged="DateChangedTo" />
                    </div>
                </div>
                <div id="divHotel" style="clear: both; width: 600px; height: 25px;">
                    <div style="float: left; margin-top: 3px; text-align: right; width: 200px;">
                        <%= GetGlobalResourceObject("Login", "lblAgency")%>
                    </div>
                    <div style="float: left; width: 400px; text-align: left; height: 25px;">
                        <select id="fltAgency" name="D5" style="width: 95%;">
                        </select></div>
                </div>
                <br />
                <div style="clear: both; width: 600px; height: 35px;">
                    <div style="width: 200px; float: left; text-align: right;">
                        <input visible="true" type="button" id="btnFilter" value='<%= GetGlobalResourceObject("StopSale", "btnFilter")%>'
                            style="min-width: 120px; height: 29px;" onclick="GetResult();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    </div>
                    <div style="float: left;">
                        <input type="button" visible="true" id="btnUnFilter" value='<%= GetGlobalResourceObject("StopSale", "btnClearFilter")%>'
                            style="min-width: 120px; height: 29px;" onclick="return btnUnFilter_onclick()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    </div>
                </div>
            </div>
        </div>
        <div class="Content">
            <div class="dataTables_scroll">
                <div id="resMonitorGrid" class="dataTables_scrollBody" style="width: 100%;">
                </div>
            </div>
            <br />
            <br />
        </div>
        <div class="Footer">
            <tv1:Footer ID="tvfooter" runat="server" />
        </div>
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
        </span><span id="messages">Message</span>
    </div>
    </form>
</body>
</html>
