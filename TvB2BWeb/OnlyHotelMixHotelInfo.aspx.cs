﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;


public partial class OnlyHotelMixHotelInfo : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["UserData"] != null)
        {
            TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData(string Hotel)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        HotelDetailsResponse hotelDetails = new MixSearchPaximum().getHotelDetais(UserData, Hotel, ref errorMsg);
        if (hotelDetails == null)
        {
            return null;
        }
        HotelDescription hotelDescRec = hotelDetails.Content.Where(w => w.Title == "Location").FirstOrDefault();
        string hotelDesc = hotelDescRec != null ? hotelDescRec.Description : string.Empty;
        var facilities = from q in hotelDetails.Facilities
                         where !string.IsNullOrEmpty(q.Id)
                         select new { q.Id };
        var hotelDescription = from q in hotelDetails.Content
                               where !string.IsNullOrEmpty(q.Description)
                               select new { q.Title, q.Description };
        return new
        {
            Raiting = hotelDetails.Rating.ToString("#.00"),
            HotelImageUrl = "http://media.dev.paximum.com/hotelimages_125x125/" + hotelDetails.Photos.FirstOrDefault(),
            HotelName = hotelDetails.Name,
            StarStr = string.Format("Images/HotelStars/{0}Stars.png", hotelDetails.Stars.ToString().Replace(",", "").Replace(".", "")),
            HotelLocation = hotelDetails.City.name + " / " + hotelDetails.Country.name,
            Description = hotelDesc,
            Facilities = facilities.ToArray(),
            HotelDescription = hotelDescription,
            Photos = hotelDetails.Photos
        };
    }

}
