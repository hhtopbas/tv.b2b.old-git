﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Text;
using System.Web.Services;
using TvTools;
using System.Web.Script.Services;
using System.Collections;

public partial class PackageSearchFilterV2 : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        else if (UserData.Ci.Name.ToLower() == "lv-lv")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
    }

    [WebMethod(EnableSession = true)]
    public static string CreateCriteria()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;

        bool CurControl = false;
        object curControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur");
        if (curControl != null) CurControl = (bool)curControl;

        bool ExtAllotCont = false;
        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        if (extAllotControl != null) ExtAllotCont = (bool)extAllotControl;

        int? maxExpendDayForPriceSearch = Conversion.getInt32OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "MaxExpendDayForPriceSearch"));

        bool ShowExpandDay = true;
        object showExpandDay = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowExpandDay");
        if (showExpandDay != null) ShowExpandDay = (bool)showExpandDay;

        bool ShowSecondDay = true;
        object showSecondDay = new TvBo.Common().getFormConfigValue("SearchPanel", "ShowSecondDay");
        if (showSecondDay != null) ShowSecondDay = (bool)showSecondDay;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && string.Equals(UserData.Market, "SWEMAR"))
            ShowSecondDay = false;
        SearchCriteria criteria = new SearchCriteria
        {
            SType = SearchType.PackageSearch,
            FromRecord = 1,
            ToRecord = 25,
            ShowAvailable = true,
            CurrentCur = UserData.SaleCur,
            ExpandDate = 0,
            NightFrom = 7,
            NightTo = 7,
            RoomCount = 1,
            RoomsInfo = new List<SearchCriteriaRooms>(),
            CheckIn = DateTime.Today,
            UseGroup = GroupSearch,
            CurControl = CurControl,
            ExtAllotControl = ExtAllotCont,
            ShowExpandDay = ShowExpandDay,
            MaxExpendDayForPriceSearch = maxExpendDayForPriceSearch.HasValue ? maxExpendDayForPriceSearch.Value : 7,
            ShowSecondDay = ShowSecondDay
        };
        criteria.ShowAgencyEB = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt);
        HttpContext.Current.Session["Criteria"] = criteria;
        return "";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static packageSearchFilterFormDataV2 getFormData(string B2BMenuCatStr)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        CreateCriteria();

        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];

        Int16? defaultNightLength = Conversion.getInt16OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DefaultNightLength"));
        Int16 DefaultNightLength = defaultNightLength.HasValue ? defaultNightLength.Value : Convert.ToInt16(21);

        Int16? defaultNight = Conversion.getInt16OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DefaultDays"));
        Int16 DefaultNight = defaultNight.HasValue ? defaultNight.Value : Convert.ToInt16(7);

        Int16? defaultNight2 = Conversion.getInt16OrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "DefaultDays2"));
        Int16 DefaultNight2 = defaultNight2.HasValue ? defaultNight2.Value : Convert.ToInt16(7);

        List<Int32> fltCheckInDay = new List<Int32>();
        string lblDay = Conversion.getStrOrNull(HttpContext.GetGlobalResourceObject("LibraryResource", "lblDay"));
        for (int i = 0; i <= criteria.MaxExpendDayForPriceSearch; i++) fltCheckInDay.Add(i);

        List<Int32> fltNight = new List<Int32>();
        List<Int32> fltNight2 = new List<Int32>();
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            List<Int16> nights = new TvBo.Common().getPLNights(UserData.Market, SearchType.PackageSearch, ref errorMsg);
            if (nights == null || nights.Count < 1)
                fltNight.Add((Int32)7);
            else foreach (Int16 row in nights) fltNight.Add((Int32)row);
        }
        else
        {
            for (int i = 1; i < DefaultNightLength; i++) fltNight.Add((Int32)i);
            for (int i = 1; i < DefaultNightLength; i++) fltNight2.Add((Int32)i);
        }
        List<Int32> fltRoomCount = new List<Int32>();
        for (int i = 1; i <= (UserData.AgencyRec.MaxRoomCnt.HasValue ? UserData.AgencyRec.MaxRoomCnt.Value : UserData.MaxRoomCount); i++)
            fltRoomCount.Add(i);
        bool? currencyConvert = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur"));
        packageSearchFilterFormDataV2 data = new packageSearchFilterFormDataV2();
        data.Market = UserData.Market;
        data.fltCheckInDay = new fltComboDayRecord
        {
            maxDayList = fltCheckInDay,
            selectedDay = 0
        };
        data.fltNight = new fltComboDayRecord
        {
            maxDayList = fltNight,
            selectedDay = defaultNight.HasValue ? defaultNight.Value : (Int16)7
        };
        data.fltNight2 = new fltComboDayRecord
        {
            maxDayList = fltNight2,
            selectedDay = defaultNight2.HasValue ? defaultNight2.Value : (Int16)7
        };
        data.fltRoomCount = new fltComboDayRecord
        {
            maxDayList = fltRoomCount,
            selectedDay = 1
        };

        data.CurrencyConvert = currencyConvert.HasValue ? currencyConvert.Value : true;

        List<ReadyCurrencyRecord> getCurrencyList = new TvBo.Common().getReadyCurrency(UserData.Market, UserData.SaleCur, ref errorMsg);

        string curList = string.Empty;
        curList += "<select id=\"currentCur\">";
        foreach (ReadyCurrencyRecord row in getCurrencyList)
        {
            curList += string.Format("<option value=\"{0}\" {2} >{1}</option>",
                                        row.Code,
                                        Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "SWEMAR") ? row.Code : row.NameL,
                                        Equals(UserData.SaleCur, row.Code) ? "selected=\"selected\"" : "");
        }
        curList += "</select>";
        data.MarketCur = string.Format("<div {3}><input id=\"CurrencyConvert\" type=\"checkbox\" {0} /><label for=\"CurrencyConvert\">{1}</label>&nbsp;{2}</div>",
                data.CurrencyConvert ? "checked='checked'" : "",
                HttpContext.GetGlobalResourceObject("LibraryResource", "lblShowInPrice2"),
                curList,
                Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "FINMAR") ? "style=\"display: none;\"" : "");

        DateTime ppcCheckIn = DateTime.Today.Date;
        Int64 days = (ppcCheckIn - (new DateTime(1970, 1, 1))).Days - 1;
        bool? showFilterPackage = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowFilterPackage"));
        data.ShowFilterPackage = !showFilterPackage.HasValue || (showFilterPackage.HasValue && showFilterPackage.Value == true);
        data.CheckIn = ppcCheckIn;
        data.ShowStopSaleHotels = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda); // true;
        data.CustomRegID = UserData.CustomRegID;
        data.ShowExpandDay = criteria.ShowExpandDay;
        data.ShowSecondDay = criteria.ShowSecondDay;
        data.DateFormat = UserData.Ci.TwoLetterISOLanguageName;
        data.ShowOfferBtn = string.Equals(Conversion.getStrOrNull(System.Configuration.ConfigurationManager.AppSettings["ClientOfferVersion"]), "V2");
        return data;
    }

    public static string getSearchFilterData(string B2BMenuCatStr)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["SearchPLData"] == null)
        {
            bool GroupSearch = false;
            object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
            if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
            SearchPLData filterData = CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.PackageSearch, null, B2BMenuCatStr);
            HttpContext.Current.Session["SearchPLData"] = filterData;
        }
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<LocationIDName> getDeparture()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["SearchPLData"] == null) getSearchFilterData("");
        if (HttpContext.Current.Session["SearchPLData"] == null) return null;
        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLData"];
        List<LocationIDName> depCityData = new Search().getSearchDepCitys(useLocalName, data, null, ref errorMsg);
        return depCityData;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static IEnumerable getArrival(int? DepCity)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;
        Int32? depCity = Conversion.getInt32OrNull(DepCity);
        if (HttpContext.Current.Session["SearchPLData"] == null) return "";
        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLData"];
        List<plLocationData> arrCityData = new Search().getSearchArrCitys(useLocalName, data, depCity, ref errorMsg);
        var retVal = from q in arrCityData
                     where !q.HolPackCat.HasValue
                     orderby (useLocalName ? q.ArrCountryNameL : q.ArrCountryName), (useLocalName ? q.ArrCityNameL : q.ArrCityName)
                     group q by new
                     {
                         countryCode = q.ArrCountry,
                         country = useLocalName ? q.ArrCountryNameL : q.ArrCountryName,
                         RecID = q.ArrCity,
                         Name = useLocalName ? q.ArrCityNameL : q.ArrCityName
                     } into k
                     select new
                     {
                         CountryCode = k.Key.countryCode,
                         Country = k.Key.country,
                         RecID = k.Key.RecID,
                         Name = k.Key.Name
                     };
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string getHolpack(string DepCity, string ArrCity)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;
        Int32? depCity = Conversion.getInt32OrNull(DepCity);
        Int32? arrCity = Conversion.getInt32OrNull(ArrCity);
        if (HttpContext.Current.Session["SearchPLData"] == null) return "";
        SearchPLData data = (SearchPLData)HttpContext.Current.Session["SearchPLData"];
        List<CodeName> holpackData = new Search().getSearchHolPack(useLocalName, data, depCity, null, arrCity, null, ref errorMsg);
        var retVal = from q in holpackData
                     orderby q.Name, q.Code
                     group q by new
                     {
                         Name = q.Name,
                         Code = q.Code + ';' + data.plHolPacks.Find(f => f.HolPack == q.Code).DepCity.ToString() + ';' + data.plHolPacks.Find(f => f.HolPack == q.Code).ArrCity.ToString()
                     } into k
                     select new { Code = k.Key.Code, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    [WebMethod(EnableSession = true)]
    public static string getResortData(string ArrCity)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int? arrival = Conversion.getInt32OrNull(ArrCity);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;

        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);
        Location loc = locations.Find(f => f.RecID == arrival);

        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.PackageSearch, null, "");
        List<TvBo.LocationIDName> resortData = new TvBo.Search().getSearchResorts(useLocalName, data, loc != null && loc.Parent == loc.RecID ? arrival : null, loc != null && loc.Parent == loc.RecID ? null : arrival, null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(resortData.OrderBy(o => o.Name));
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomInfo(string RoomCount)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        //Int16 maxChildAge = UserData.TvParams.TvParamReser.MaxChdAge.HasValue ? Convert.ToInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAge.Value)) : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? Convert.ToInt16(18) : Convert.ToInt16(12));
        Int16 maxChildAge = UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.HasValue ? Convert.ToInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.Value)) : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? Convert.ToInt16(18) : Convert.ToInt16(12));
        StringBuilder html = new StringBuilder();
        plMaxPaxCounts maxPaxData = new plMaxPaxCounts();
        object _paxData = new TvBo.Common().getFormConfigValue("SearchPanel", "PaxData");
        if(_paxData!=null)
        {
            plMaxPaxCounts paxData = Newtonsoft.Json.JsonConvert.DeserializeObject<plMaxPaxCounts>(_paxData.ToString());
            maxPaxData = paxData;
        }
        if (HttpContext.Current.Session["SearchPLData"] != null && (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal)))
            if (((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount != null)
                maxPaxData = ((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount;
        /* TICKET ID : 14489
        if (string.Equals(UserData.CustomRegID, Common.crID_Dallas))
        {
            maxPaxData.maxAdult = 3;
            maxPaxData.maxChd = 2;
        }
        */
        Int32 j = 0;
        int i = 1;

        html.AppendFormat("<div id=\"divRoomInfo{0}\" style=\"width:250px; clear:both;\">", i.ToString());

        html.AppendFormat("  <div id=\"divAdult{0}\" style=\"float: left; width:100px;\">", i.ToString());
        html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblAdult").ToString());
        html.AppendFormat("  <select id=\"fltAdult{0}\" style=\"width: 50px;\">", i.ToString(), i.ToString());
        for (j = 1; j <= maxPaxData.maxAdult; j++)
        {
            if (j == 2)
                html.AppendFormat("     <option value='{0}' selected=\"selected\">{1}</option>", j.ToString(), j.ToString());
            else html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        }
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");

        html.AppendFormat("  <div id=\"divChild{0}\" style=\"float: left; width:50px;\">", i.ToString());
        html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChild").ToString());
        html.AppendFormat("  <select id=\"fltChild{0}\" onchange=\"onChildChange('{1}')\" style=\"width: 50px;\">", i.ToString(), i.ToString());
        for (j = 0; j < (maxPaxData.maxChd.HasValue ? maxPaxData.maxChd.Value + 1 : 5); j++)        
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat("</div>");

        html.AppendFormat("<div id=\"divRoomInfoChd{0}\" style=\"width:250px; clear:both; display:none;\">", i.ToString());
        html.AppendFormat(" <div id=\"divRoomInfoChd1{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 1");
        html.AppendFormat("   <select id=\"fltRoomInfoChd1{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat(" <div id=\"divRoomInfoChd2{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 2");
        html.AppendFormat("   <select id=\"fltRoomInfoChd2{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat(" <div id=\"divRoomInfoChd3{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 3");
        html.AppendFormat("   <select id=\"fltRoomInfoChd3{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat(" <div id=\"divRoomInfoChd4{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 4");
        html.AppendFormat("   <select id=\"fltRoomInfoChd4{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");

        html.AppendFormat("</div>");

        return html.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getCategoryData(string ArrCity, string Resort)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int? _resort = null;
        int? arrival = Conversion.getInt32OrNull(ArrCity);
        if (Resort.IndexOf('|') < 0)
            _resort = Conversion.getInt32OrNull(Resort);
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        Location filterResort = null;
        if (_resort.HasValue)
            filterResort = locations.Find(f => f.RecID == _resort);

        List<TvBo.CodeName> categoryData = new List<CodeName>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        Location loc = locations.Find(f => f.RecID == arrival);
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.PackageSearch, null, "");
        if (filterResort != null && _resort.HasValue)
        {
            if (filterResort.Type == 4)
                categoryData = new TvBo.Search().getSearchCategorys(useLocalName, data, loc != null && loc.Parent == loc.RecID ? arrival : null, loc != null && loc.Parent == loc.RecID ? null : arrival, null, _resort, ref errorMsg);
            else categoryData = new TvBo.Search().getSearchCategorys(useLocalName, data, loc != null && loc.Parent == loc.RecID ? arrival : null, loc != null && loc.Parent == loc.RecID ? null : arrival, _resort, null, ref errorMsg);
        }
        else
            categoryData = new TvBo.Search().getSearchCategorys(useLocalName, data, loc != null && loc.Parent == loc.RecID ? arrival : null, loc != null && loc.Parent == loc.RecID ? null : arrival, Resort, null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(categoryData.OrderBy(o => o.Name));
    }

    [WebMethod(EnableSession = true)]
    public static string getHotelData(string ArrCity, string Resort, string Category, string Holpack)
    {
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int? _resort = null;
        int? arrival = Conversion.getInt32OrNull(ArrCity);

        if (Resort.IndexOf('|') < 0)
            _resort = Conversion.getInt32OrNull(Resort);
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.PackageSearch, null, "");
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);
        Location filterResort = locations.Find(f => f.RecID == _resort);
        List<TvBo.CodeHotelName> hotelData = new List<CodeHotelName>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        Location loc = locations.Find(f => f.RecID == arrival);

        if (filterResort != null && _resort.HasValue)
        {
            if (filterResort.Type == 4)
                hotelData = new TvBo.Search().getSearchHotels(useLocalName, data, _resort, loc != null && loc.Parent == loc.RecID ? arrival : null, loc != null && loc.Parent == loc.RecID ? null : arrival, Resort, Category, Holpack, null, ref errorMsg);
            else hotelData = new TvBo.Search().getSearchHotels(useLocalName, data, null, loc != null && loc.Parent == loc.RecID ? arrival : null, loc != null && loc.Parent == loc.RecID ? null : arrival, Resort, Category, Holpack, null, ref errorMsg);
        }
        else
            hotelData = new TvBo.Search().getSearchHotels(useLocalName, data, null, loc != null && loc.Parent == loc.RecID ? arrival : null, loc != null && loc.Parent == loc.RecID ? null : arrival, Resort, Category, Holpack, null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData.OrderBy(o => o.Name));
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomData(string DepCity, string ArrCity, string Resort, string Hotel)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? _resort = null;
        if (Resort.IndexOf('|') < 0)
            _resort = Conversion.getInt32OrNull(Resort);
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        Location filterResort = locations.Find(f => f.RecID == _resort);
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.PackageSearch, null, "");
        List<TvBo.CodeName> hotelData = new List<CodeName>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (filterResort != null && _resort.HasValue)
        {
            if (filterResort.Type == 4)
                hotelData = new TvBo.Search().getSearchRooms(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity == "null" ? "" : ArrCity), Resort, _resort, Hotel, null, ref errorMsg);
            else hotelData = new TvBo.Search().getSearchRooms(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity == "null" ? "" : ArrCity), Resort, null, Hotel, null, ref errorMsg);
        }
        else hotelData = new TvBo.Search().getSearchRooms(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity == "null" ? "" : ArrCity), Resort, null, Hotel, null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData.OrderBy(o => o.Name));
    }

    [WebMethod(EnableSession = true)]
    public static string getBoardData(string DepCity, string ArrCity, string Resort, string Hotel)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? _resort = null;
        if (Resort.IndexOf('|') < 0)
            _resort = Conversion.getInt32OrNull(Resort);
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        Location filterResort = locations.Find(f => f.RecID == _resort);
        List<TvBo.CodeName> hotelData = new List<CodeName>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.PackageSearch, null, "");
        if (filterResort != null && _resort.HasValue)
        {
            if (filterResort.Type == 4)
                hotelData = new TvBo.Search().getSearchBoards(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, _resort, Hotel, null, ref errorMsg);
            else hotelData = new TvBo.Search().getSearchBoards(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, null, ref errorMsg);
        }
        else hotelData = new TvBo.Search().getSearchBoards(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData.OrderBy(o => o.Name));
    }

    [WebMethod(EnableSession = true)]
    public static string getFacilityData(string DepCity, string ArrCity, string Resort, string Hotel)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? _resort = null;
        if (Resort.IndexOf('|') < 0)
            _resort = Conversion.getInt32OrNull(Resort);
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

        string errorMsg = string.Empty;
        bool GroupSearch = false;
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        if (groupSeacrh != null) GroupSearch = (bool)groupSeacrh;
        Location filterResort = locations.Find(f => f.RecID == _resort);
        List<TvBo.CodeName> hotelData = new List<CodeName>();
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        TvBo.SearchPLData data = TvBo.CacheObjects.getPackageSearchData(UserData.Operator, UserData.Market, GroupSearch, SearchType.PackageSearch, null, "");
        if (filterResort != null && _resort.HasValue)
        {
            if (filterResort.Type == 4)
                hotelData = new TvBo.Search().getSearchFacilitys(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, _resort, Hotel, null, ref errorMsg);
            else hotelData = new TvBo.Search().getSearchFacilitys(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, null, ref errorMsg);
        }
        else hotelData = new TvBo.Search().getSearchFacilitys(useLocalName, data, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), Resort, null, Hotel, null, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(hotelData.OrderBy(o => o.Name));
    }

    [WebMethod(EnableSession = true)]
    public static string SetCriterias(long? CheckIn, string ExpandDate, string NightFrom, string NightTo,
            string RoomCount, string roomsInfo, string Board, string Room, string DepCity, string ArrCity,
            string Resort, string Category, string Hotel, string CurControl, string CurrentCur,
            string ShowStopSaleHotels, string HolPack, string Facility)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; 
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["Criteria"] == null) CreateCriteria();
        SearchCriteria criteria = (SearchCriteria)HttpContext.Current.Session["Criteria"];
        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(CheckIn) / 86400000) + 1);
        criteria.CheckIn = checkIn;
        criteria.ExpandDate = Convert.ToInt16(ExpandDate);
        criteria.NightFrom = Convert.ToInt16(NightFrom);
        criteria.NightTo = Convert.ToInt16(NightTo);
        criteria.RoomCount = Convert.ToInt16(RoomCount);
        string _room = "[" + roomsInfo.Replace("|", "\"").Replace(')', '}').Replace('(', '{') + "]";
        List<SearchCriteriaRooms> rooms = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchCriteriaRooms>>(_room);
        criteria.RoomsInfo.Clear();
        int i = 0;
        foreach (SearchCriteriaRooms row in rooms)
        {
            ++i;
            criteria.RoomsInfo.Add(new SearchCriteriaRooms
            {
                RoomNr = i,
                Adult = row.Adult,
                Child = row.Child,
                Chd1Age = row.Chd1Age >= 0 ? row.Chd1Age : null,
                Chd2Age = row.Chd2Age >= 0 ? row.Chd2Age : null,
                Chd3Age = row.Chd3Age >= 0 ? row.Chd3Age : null,
                Chd4Age = row.Chd4Age >= 0 ? row.Chd4Age : null
            });
        }
        criteria.Board = Conversion.getStrOrNull(Board);
        criteria.Room = Conversion.getStrOrNull(Room);
        criteria.DepCity = Conversion.getInt32OrNull(DepCity);
        criteria.ArrCity = Conversion.getInt32OrNull(!Equals(ArrCity, "null") ? ArrCity : "");
        criteria.Resort = Conversion.getStrOrNull(Resort);
        criteria.Category = Conversion.getStrOrNull(Category);
        criteria.Hotel = Conversion.getStrOrNull(Hotel);
        criteria.Package = Conversion.getStrOrNull(HolPack);
        criteria.Facility = Conversion.getStrOrNull(Facility);
        criteria.FromRecord = 1;
        criteria.ToRecord = 25;
        criteria.CurControl = Equals(CurControl, "Y");
        criteria.CurrentCur = CurrentCur;
        criteria.SType = SearchType.PackageSearch;
        criteria.ShowStopSaleHotels = Equals(ShowStopSaleHotels, "Y");
        object groupSeacrh = new TvBo.Common().getFormConfigValue("SearchPanel", "GroupSearch");
        criteria.UseGroup = groupSeacrh != null ? (bool)groupSeacrh : false;
        HttpContext.Current.Session["Criteria"] = criteria;
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<calendarColor> getFlightDays(int? DepCity, int? ArrCity, string Holpack, string packType, string b2bMenuCat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        int? depCity = Conversion.getInt32OrNull(DepCity);
        int? arrCity = Conversion.getInt32OrNull(ArrCity);
        List<Location> location = CacheObjects.getLocationList(UserData.Market);
        List<FlightDays> flightDays = new Flights().getFlightDays(UserData, depCity, arrCity, true, packType, b2bMenuCat, Holpack, ref errorMsg);
        List<calendarColor> retVal = new List<calendarColor>();

        var flights = from q in flightDays
                      group q by new { q.FlyDate } into k
                      select new { k.Key.FlyDate };

        foreach (var row in flights)
        {
            var query1 = from q in flightDays
                         where q.FlyDate == row.FlyDate
                         select q;
            string text = string.Empty;
            foreach (var row2 in query1)
            {
                text += string.Format("{0} [{3}-{4}] - ({1} -> {2}) \n",
                                        row2.Flights,
                                        location.Find(f => f.RecID == row2.DepCity).NameL,
                                        location.Find(f => f.RecID == row2.ArrCity).NameL,
                                        row2.DepTime.HasValue ? row2.DepTime.Value.ToString("HH:mm") : "00:00",
                                        row2.ArrTime.HasValue ? row2.ArrTime.Value.ToString("HH:mm") : "00:00");
            }
            retVal.Add(new calendarColor
            {
                Year = row.FlyDate.Year,
                Month = row.FlyDate.Month,
                Day = row.FlyDate.Day,
                Text = text,
                ColorType = 0
            });
        }
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string getFirstFlight(string DepCity, string ArrCity)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Detur)) return string.Empty;

        string errorMsg = string.Empty;
        FirstFlightRecord firstFlight = new Flights().getFirstFlight(UserData, UserData.Market, UserData.AgencyID, Conversion.getInt32OrNull(DepCity), Conversion.getInt32OrNull(ArrCity), true, ref errorMsg);
        if (firstFlight != null && firstFlight.FlyDate.HasValue)
        {
            DateTime ppcCheckIn = firstFlight.FlyDate.Value;
            Int64 days = (ppcCheckIn - (new DateTime(1970, 1, 1))).Days - 1;
            firstFlight.CheckIn = (days * 86400000);
            firstFlight.DateFormat = new TvBo.Common().getDateFormat(UserData.Ci).Replace('/', '-');
            firstFlight.FlyDate = firstFlight.FlyDate.Value.AddDays(1);
            return Newtonsoft.Json.JsonConvert.SerializeObject(firstFlight);
        }
        else return string.Empty;
    }
}

