﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlyTransferResult.aspx.cs"  Inherits="OnlyTransferResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Result</title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>
    <script src="Scripts/NumberFormat154.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cluetip.js" type="text/javascript"></script>
    <script src="Scripts/datatable/jquery.dataTables.js" type="text/javascript"></script>
    <script src="Scripts/datatable/jquery.dataTables.Pagination.ListBox.js" type="text/javascript"></script>
    <script src="Scripts/datatable/ColVis.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />    
    <link href="CSS/OnlyExcursionSearchResult.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery.table.jui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/datatable/ColVis.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #divNote {
            height: 30px;
            color: #CC0000;
            font-weight: bold;
            text-decoration: underline;
            cursor: help;
        }
    </style>

    <script language="javascript" type="text/javascript">

        var cultureNumber;
        var cultureDate;

        //$.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart(function () {
            $.blockUI({
                message: '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'
            });
        }).ajaxStop(function () {
            $.unblockUI();
        });

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }


        function logout() {
            self.parent.logout();
        }

        function tryNumberFormat(obj) {
            var nf = new NumberFormat(obj);
            if (cultureNumber != null) {
                nf.PERIOD = cultureNumber.CurrencyGroupSeparator;
                nf.COMMA = cultureNumber.CurrencyDecimalSeparator;
            }
            var rsdoS = nf.PERIOD;
            var rsdoD = nf.COMMA;
            nf.setPlaces(2);
            nf.setSeparators(true, rsdoS, rsdoD);
            obj = nf.toFormatted();
            return obj;
        }

        function showDialog(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function reSizeFrame() {
            self.parent.reSizeResultFrame(document.body.offsetHeight);
        }

        var oTable = null;

        function showResult() {
            var _aoColumns = [];
            $.ajax({
                async: false,
                type: "POST",
                data: '{}',
                timeout: (120 * 1000),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'OnlyTransferResult.aspx/getGridHeaders',
                success: function (msg) {
                    if (msg.d != null && msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            obj = new Object();
                            if (this.sTitle != undefined && this.sTitle != null) obj.sTitle = this.sTitle;
                            if (this.sWidth != undefined && this.sWidth != null) obj.sWidth = this.sWidth;
                            if (this.bSortable != undefined && this.bSortable != null) obj.bSortable = this.bSortable;
                            if (this.sType != undefined && this.sType != null) obj.sType = this.sType;
                            if (this.sClass != undefined && this.sClass != null) obj.sClass = this.sClass;
                            if (this.bSearchable != undefined && this.bSearchable != null) obj.bSearchable = this.bSearchable;
                            _aoColumns.push(obj);
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showDialog(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            var oLanguage = {
                "sProcessing": '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>',
                "sLengthMenu": '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>',
                "sZeroRecords": '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>',
                "sEmptyTable": '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>',
                "sInfo": '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>',
                "sInfoEmpty": '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>',
                "sInfoFiltered": '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>',
                "sInfoPostFix": '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>',
                "sSearch": '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>',
                "sUrl": '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>',
                "oPaginate": {
                    "sFirst": '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>',
                    "sPrevious": '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>',
                    "sNext": '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>',
                    "sLast": '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>',
                    "sPage": '<%= GetGlobalResourceObject("LibraryResource", "sPage") %>'
                },
                "fnInfoCallback": null
            };
            var first = true;

            oTable = $('#resTable').dataTable({
                "bDestory": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aaSorting": [],
                "fnStateLoadParams": function (oSettings, oData) {
                    if (first) {
                        oData.iStart = 0;
                        oSettings._iDisplayStart = 0;
                    }
                    first = false;
                },
                "sPaginationType": "listbox",
                "bFilter": false,
                "bJQueryUI": true,
                "oLanguage": oLanguage,
                "aoColumns": _aoColumns,
                "bProcessing": false,
                "bServerSide": true,
                "sServerMethod": "POST",
                "sAjaxSource": "OnlyTransferSearchData.aspx",
                "fnDrawCallback": function (oSettings) {

                },
                "fnServerData": function (sSource, aoData, fnCallback) {
                    var obj = new Object();

                    $.ajax({
                        async: false,
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (data, textStatus, xmlHttpRequest) {
                            var jData = $(data);
                            var json = { "sEcho": aoData[0].value, "aaData": [] };
                            json.iTotalRecords = data.iTotalRecords;
                            json.iTotalDisplayRecords = data.iTotalDisplayRecords;
                            $.each(data.aaData, function (i) {
                                var obj = [];
                                if (this.Book != undefined) obj.push(this.Book);
                                if (this.Name != undefined) obj.push(this.Name);                                
                                if (this.CheckIn != undefined) obj.push(this.CheckIn);
                                if (this.Price != undefined) obj.push(this.Price);
                                json.aaData.push(obj);
                            });

                            if (data.aaData.length > 0)
                              fnCallback(json);

                            reSizeFrame();
                            $('.cssExcursionName').cluetip({
                                activation: 'click',
                                width: 400,
                                arrows: true,
                                dropShadow: false,
                                hoverIntent: false,
                                sticky: true,
                                mouseOutClose: false,
                                closePosition: 'title',
                                closeText: '<img src="Images/cancel.png" title="close" />'
                            });
                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                alert(xhr.responseText);
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                }
            });
        }        

        function makeRes(refNo) {
            self.parent.bookReservation(refNo);
        }

        $(document).ready(function () {
            $("#gridResult").remove("#resTable");
            var tableElm = $(document.createElement('table'));
            tableElm.attr({ 'id': 'resTable', 'class': 'display' }).css({ 'width': '745px' });
            $("#gridResult").append(tableElm);

            showResult();
        });
    </script>

</head>
<body>
    <form id="formPackageSearchResult" runat="server">
        <div id="divSearchResult">
            <div id="gridResult">
            </div>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
    </form>
</body>
</html>
