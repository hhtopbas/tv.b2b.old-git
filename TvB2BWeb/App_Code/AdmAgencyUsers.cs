﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for AdmAgencyUsers
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class AdmAgencyUsers : System.Web.Services.WebService
{

    public AdmAgencyUsers()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public string editAgencyUser(string agency, string code, string name, string status, string pin, string phone, string mobile, string eMail,
                                 string hAddress, string showAllRes, string password, string expDate, string expedintID)
    {
        string errorMsg = string.Empty;
        //if (!Conversion.getInt32OrNull(user).HasValue) return "0";
        bool _retVal = new TvBo.Users().editAgencyUser(agency, code, name, status, pin, phone, mobile, eMail, hAddress, showAllRes, password, expDate, expedintID, ref errorMsg);
        return _retVal ? "1" : "0";
    }

    [WebMethod]
    public string createAgencyUser(string agency, string code, string name, string status, string pin, string phone, string mobile, string eMail,
                                   string hAddress, string showAllRes, string password, string expDate, string expedintID)
    {
        string errorMsg = string.Empty;
        if (string.IsNullOrEmpty(agency) || string.IsNullOrEmpty(code) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(password))
        {
            errorMsg = "Code, Name and Password fields is required.";
            return "0";
        }
        bool _retVal = new TvBo.Users().createAgencyUser(agency, code, name, status, pin, phone, mobile, eMail, hAddress, showAllRes, password, expDate, expedintID, ref errorMsg);
        return _retVal ? "1" : "0";
    }

    [WebMethod]
    public string editAgency(string agency, string contactName, string phone1, string phone2, string mobile, string fax1, string fax2, string www,
                                   string email1, string email2, string address, string zip, string city)
    {
        string errorMsg = string.Empty;
        bool _retVal = new TvBo.Agency().updateAgencyInfo(agency, contactName, phone1, phone2, mobile, fax1, fax2, www, email1, email2, address, zip, city, ref errorMsg);
        return _retVal ? "1" : "0";
    }
}

