﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using TvReport.AnexReport;
using System.Text;
using TvTools;
using System.IO;
using System.Threading;

/// <summary>
/// Summary description for AnexReport
/// </summary>
public class AnexReport
{
    public string showInsuranceListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResCustInsuranceRecord> customerInsurances = new TvReport.AnexReport.AnexReport().getInsuranceCustomers(UserData.Market, ResData.ResMain.ResNo, ref errorMsg);
        if (customerInsurances == null || customerInsurances.Count < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResCustInsuranceRecord row in customerInsurances)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q in ResData.ResCon
                         where q.CustNo == row.CustNo && q.ServiceID == row.ServiceID
                         select q.DocStat;

            Int16? DocStat = Conversion.getInt16OrNull(ResData.ResCon.Find(f => f.CustNo == row.CustNo && f.ServiceID == row.ServiceID).DocStat);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == row.ServiceID);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td style=\"font-family: Tahoma; font-size: 14px;\">");
            sb.Append(i.ToString() + ". " + Name);
            sb.Append("     </td>");
            sb.Append("     <td>&nbsp;");

            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Insurance0969801('{0}', {1}, {2});\" style=\"cursor: pointer; text-decoration: underline;\">{3} ",
                                resService.ResNo,
                                resService.RecID,
                                row.CustNo,
                                resService.ServiceNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         Бронирование оплаты еще не завершено.");
                else
                {
                    sb.AppendFormat("<span onclick=\"Insurance0969801('{0}', {1}, {2});\" style=\"cursor: pointer; text-decoration: underline;\">{3} ",
                                resService.ResNo,
                                resService.RecID,
                                row.CustNo,
                                resService.ServiceNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showInsurance(User UserData, ref ResDataRecord ResData, int? ServiceID, int? CustNo)
    {
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder.Replace('/', '\\') + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("~\\");

        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        if (!Equals(resService.ServiceType, "INSURANCE")) return string.Empty;

        InsuranceRecord insurance = new Insurances().getInsurance(UserData.Market, resService.Service, ref errorMsg);
        Int16 maxTourist = insurance.MaxPaxPolicy.HasValue ? insurance.MaxPaxPolicy.Value : Convert.ToInt16(6);

        List<ResCustInsuranceRecord> customerInsurances = new TvReport.AnexReport.AnexReport().getInsuranceCustomers(UserData.Market, ResData.ResMain.ResNo, ref errorMsg);

        string custListString = CustNo.ToString();
        var insuranceCust = from q1 in ResData.ResService
                            join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                            where q1.RecID == ServiceID
                            select new { CustNo = q2.CustNo, recID = Convert.ToInt32(0), polNo = q2.DocNo };
        if (insuranceCust.Count() > 0)
        {
            Int16 cnt = 1;
            foreach (var row in insuranceCust.Where(w => w.CustNo != CustNo))
            {
                cnt++;
                if (cnt <= maxTourist)
                    custListString += "," + row.CustNo.ToString();
            }
        }

        List<TvReport.AnexReport.CustInsuranceRecord> custInsList = new TvReport.AnexReport.AnexReport().getcustInsurance(ResData.ResMain.ResNo, ServiceID, custListString, ref errorMsg);

        TvReport.AnexReport.InsuranceRptRecord ins = new TvReport.AnexReport.AnexReport().getInsurance(UserData.Market, ResData.ResMain.ResNo, ServiceID, ref errorMsg);
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        string polNo = string.Empty;

        string pdfName = ResData.ResService.Find(f => f.RecID == ServiceID).Service;
        if (pdfName == "INS")
        {
            #region INS
            if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
            else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;
            float deltaY = 0f;
            for (int i = 0; i < 3; i++)
            {
                switch (i)
                {
                    case 0: deltaY = 0f; break;
                    case 1: deltaY = 256f; break;
                    case 2: deltaY = 512f; break;
                }

                //grid
                float _Y = 726f;
                foreach (CustInsuranceRecord custIns in custInsList)
                {
                    writeList.Add(new TvReport.Coordinate { value = custIns.Surname + " " + custIns.Name, x = 40f, y = _Y - deltaY, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 182f, y = _Y - deltaY, FontSize = 8 });
                    _Y -= 12;
                }
                //tablo
                writeList.Add(new TvReport.Coordinate { value = polNo, x = 114f, y = 799f - deltaY, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custInsList.FirstOrDefault().Surname + " " + custInsList.FirstOrDefault().Name, x = 440f, y = 799f - deltaY, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.Duration.ToString(), x = 335f, y = 714f - deltaY, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.BegDate.HasValue ? ins.BegDate.Value.ToShortDateString() : "", x = 402f, y = 714f - deltaY, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.EndDate.HasValue ? ins.EndDate.Value.ToShortDateString() : "", x = 477f, y = 714f - deltaY, FontSize = 8 });

                int? country = new Locations().getLocationForCountry(resService.DepLocation.Value, ref errorMsg);
                string territoryName = new Locations().getLocationName(UserData.Market, country.HasValue ? country.Value : -1, NameType.NormalName, ref errorMsg) + " / " +
                                       new Locations().getLocationName(UserData.Market, country.HasValue ? country.Value : -1, NameType.LocalName, ref errorMsg);
                writeList.Add(new TvReport.Coordinate { value = territoryName, x = 320f, y = 676f - deltaY, FontSize = 8 });
                string agencyCityName = UserData.Location.HasValue ? new Locations().getLocationName(UserData.Market, UserData.Location.Value, NameType.NormalName, ref errorMsg) : "";
                int? agencyCountry = new Locations().getLocationForCountry(UserData.Location.HasValue ? UserData.Location.Value : -1, ref errorMsg);
                string agencyContryName = agencyCountry.HasValue ? new Locations().getLocationName(UserData.Market, agencyCountry.Value, NameType.NormalName, ref errorMsg) : "";
                writeList.Add(new TvReport.Coordinate { value = agencyCityName + "/" + agencyContryName, x = 114f, y = 781f - deltaY, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = agencyCityName, x = 182f, y = 656f - deltaY, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 182f, y = 642f - deltaY, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resService.Service, x = 182f, y = 628f - deltaY, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.Coverage.HasValue ? ins.Coverage.Value.ToString("#,###.00") + " " + ins.CoverageCur : "", x = 316f, y = 625f - deltaY, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur : "", x = 363f, y = 601f - deltaY, FontSize = 8 });
            }
            #endregion
        }
        else
            return string.Empty;

        string retVal = string.Empty;
        retVal = new TvReport.AnexReport.AnexReport().createInsurance(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), pdfName, ref errorMsg);

        if (!string.IsNullOrEmpty(retVal))
        {
            var query = from q1 in insuranceCust
                        join q2 in custListString.Split(',') on q1.CustNo equals Convert.ToInt32(q2)
                        select q1;
            foreach (var row in query)
            {
                if (new Reservation().setInsuranceDoc(ResData.ResMain.ResNo, ServiceID, row.CustNo, polNo, ref errorMsg))
                {
                    new Insurances().setInsurancePolicy(UserData, resService.Service, Conversion.getInt32OrNull(polNo), row.CustNo, resService.Supplier, resService.ResNo, resService.BegDate, resService.EndDate,
                                                        resService.Duration, ins.Coverage, ins.CoverageCur, resService.DepLocation, ins.InsZone, UserData.AgencyRec.Location, UserData.UserID, "Y", ref errorMsg);
                }
                ResConRecord resCon = ResData.ResCon.Find(f => f.CustNo == row.CustNo && f.ServiceID == ServiceID);
                resCon.DocNo = polNo;
                resCon.DocStat = 1;
                resCon.DocIsDate = DateTime.Today;
            }
            return retVal;
        }
        else return string.Empty;
    }

    public string showAggrement(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        string statusStr = string.Empty;
        switch (ResData.ResMain.ConfStat)
        {
            case 0: statusStr = "Ожидание"; break;
            case 1: statusStr = "ОК"; break;
            case 2: statusStr = "Не подтверждено"; break;
            case 3: statusStr = "Но шоу"; break;
        }
        OfficeRecord oprOffice = new TvBo.Common().getOffice(UserData.Market, agency.OprOffice, ref errorMsg);
        writeList.Add(new TvReport.Coordinate { value = "SUNRISE TOUR / " + (oprOffice != null ? oprOffice.Name : ""), x = 232, y = 750, FontSize = 11 });
        writeList.Add(new TvReport.Coordinate { value = statusStr, x = 340, y = 739, FontSize = 11 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 400, y = 700, W = 45, H = 45, Type = TvReport.writeType.QRCode });
        writeList.Add(new TvReport.Coordinate { value = agency.Name, x = 154, y = 711, FontSize = 9 });
        writeList.Add(new TvReport.Coordinate { value = agency.Fax1, x = 154, y = 694, FontSize = 9 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Now.ToString(), x = 154, y = 676, FontSize = 9 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 154, y = 657, FontSize = 9 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNote, x = 154, y = 640, FontSize = 9 });

        writeList.Add(new TvReport.Coordinate { value = "Туристы", x = 54, y = 615, FontSize = 9, bold = true });
        float _y = 615;
        foreach (ResCustRecord row in ResData.ResCust)
        {
            ResCustInfoRecord resCustInfo = ResData.ResCustInfo.Find(f => f.CustNo == row.CustNo);
            _y -= 11;
            writeList.Add(new TvReport.Coordinate { value = row.TitleStr + ". " + row.Surname + " " + row.Name, x = 54, y = _y, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 250, y = _y, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = resCustInfo != null ? (string.IsNullOrEmpty(resCustInfo.AddrHomeTel) ? resCustInfo.MobTel : resCustInfo.AddrHomeTel) : "", x = 300, y = _y, FontSize = 9 });
        }
        _y -= 20;
        writeList.Add(new TvReport.Coordinate { value = "Услуга", x = 54, y = _y, FontSize = 9, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "Кол-во", x = 105, y = _y, FontSize = 9, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "Описание", x = 140, y = _y, FontSize = 9, bold = true });
        var services = from srv in ResData.ResService
                       where srv.StatSer < 2
                       group srv by new { Service = srv.ServiceType, LService = srv.ServiceTypeNameL } into k
                       select new { Service = k.Key.Service, LServiceType = k.Key.LService, Count = ResData.ResService.Where(w => w.ServiceType == k.Key.Service).Count() };
        foreach (var q in services)
        {
            List<ResServiceRecord> resService = (from Srv in ResData.ResService
                                                 where Srv.ServiceType == q.Service && Srv.StatSer < 2
                                                 select Srv).ToList<ResServiceRecord>();
            bool first = true;
            foreach (ResServiceRecord row in resService)
            {
                _y -= 10;
                if (first)
                {
                    writeList.Add(new TvReport.Coordinate { value = string.IsNullOrEmpty(q.LServiceType) ? q.Service : q.LServiceType, x = 54, y = _y, FontSize = 9 });
                    first = false;
                }
                string ServiceDesc = string.Empty;
                switch (row.ServiceType)
                {
                    case "HOTEL": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "FLIGHT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "TRANSPORT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "TRANSFER": ServiceDesc += ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "RENTING": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "EXCURSION": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "INSURANCE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "VISA": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "HANDFEE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    default: ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                }
                if (!Equals(row.ServiceType, "HOTEL"))
                    ServiceDesc = row.ServiceNameL + " (" + ServiceDesc + ")";
                else ServiceDesc = row.ServiceNameL;
                writeList.Add(new TvReport.Coordinate { value = row.Unit.ToString(), x = 120, y = _y, FontSize = 9 });
                writeList.Add(new TvReport.Coordinate { value = ServiceDesc, x = 140, y = _y, FontSize = 9 });
                if (Equals(row.ServiceType, "HOTEL"))
                {
                    _y -= 10;
                    writeList.Add(new TvReport.Coordinate
                    {
                        value = "Заезд : " + (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") +
                            "   Выезд : " + (row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : ""),
                        x = 140,
                        y = _y,
                        FontSize = 9
                    });
                    _y -= 10;
                    writeList.Add(new TvReport.Coordinate
                    {
                        value = "Тип номера : " + row.RoomNameL,
                        x = 140,
                        y = _y,
                        FontSize = 9
                    });
                    _y -= 10;
                    writeList.Add(new TvReport.Coordinate
                    {
                        value = "Тип питания : " + row.BoardNameL,
                        x = 140,
                        y = _y,
                        FontSize = 9
                    });

                }
            }
        }
        if (ResData.ResServiceExt.Where(w => w.StatSer < 2).Count() > 0)
        {
            _y -= 25;
            writeList.Add(new TvReport.Coordinate { value = "Доп. услуги", x = 54, y = _y, FontSize = 9, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Кол-во", x = 165, y = _y, FontSize = 9, bold = true });
            foreach (ResServiceExtRecord row in ResData.ResServiceExt.Where(w => w.StatSer < 2).Select(s => s).ToList<ResServiceExtRecord>())
            {
                _y -= 10;
                writeList.Add(new TvReport.Coordinate { value = row.ExtServiceNameL, x = 54, y = _y, FontSize = 9 });
                writeList.Add(new TvReport.Coordinate { value = row.Unit.ToString(), x = 180, y = _y, FontSize = 9 });
            }
        }
        _y -= 25;
        writeList.Add(new TvReport.Coordinate
        {
            value = "Итого к оплате : " + (ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : ""),
            x = 54,
            y = _y,
            FontSize = 9,
            bold = true
        });
        _y -= 30;

        return new TvReport.SunReport.SunReport().createAggreement(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         Бронирование оплаты еще не завершено.");
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;
        float _deltaY = 258f;
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 42f, y = 704f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 42f, y = 704f - _deltaY, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 465f, y = 770f, FontSize = 8, Type = TvReport.writeType.BarCode });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 465f, y = 770f - _deltaY, FontSize = 8, Type = TvReport.writeType.BarCode });

        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 100f, y = 767f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 100f, y = 767f - _deltaY, FontSize = 8 });

        string accomFullName = string.Empty;
        if (resService.Adult.HasValue && resService.Adult.Value > 0)
            accomFullName += resService.Adult.Value.ToString() + " Adl";
        if (resService.Child.HasValue && resService.Child.Value > 0)
        {
            if (accomFullName.Length > 0) accomFullName += " ";
            accomFullName += resService.Child.Value.ToString() + " Chd";
        }

        writeList.Add(new TvReport.Coordinate { value = accomFullName, x = 115f, y = 643f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = accomFullName, x = 115f, y = 643f - _deltaY, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = resService.Board.ToString(), x = 115f, y = 631f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Board.ToString(), x = 115f, y = 631f - _deltaY, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = (resService.BegDate.HasValue ? resService.BegDate.Value.ToShortDateString() : "") + " - " + (resService.EndDate.HasValue ? resService.EndDate.Value.ToShortDateString() : ""), x = 115f, y = 619f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = (resService.BegDate.HasValue ? resService.BegDate.Value.ToShortDateString() : "") + " - " + (resService.EndDate.HasValue ? resService.EndDate.Value.ToShortDateString() : ""), x = 115f, y = 619f - _deltaY, FontSize = 8 });

        string transfers = string.Empty;
        var trfQuery = from q in ResData.ResService
                       where string.Equals(q.ServiceType, "TRANSFER")
                       select q;
        foreach (var row in trfQuery)
        {
            if (transfers.Length > 0) transfers += " / ";
            transfers += row.ServiceName;
        }
        writeList.Add(new TvReport.Coordinate { value = transfers, x = 115f, y = 606f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = transfers, x = 115f, y = 606f - _deltaY, FontSize = 8 });

        var flightQuery = from q in ResData.ResService
                          where string.Equals(q.ServiceType, "FLIGHT")
                          orderby q.BegDate
                          group q by new { q.DepLocationName, q.ArrLocationName, q.Service, q.BegDate } into k
                          select new { k.Key.DepLocationName, k.Key.ArrLocationName, k.Key.Service, k.Key.BegDate };
        if (flightQuery != null && flightQuery.Count() > 0)
        {
            writeList.Add(new TvReport.Coordinate { value = flightQuery.FirstOrDefault().DepLocationName.ToString() + " - " + flightQuery.FirstOrDefault().ArrLocationName.ToString() + " / " + flightQuery.FirstOrDefault().Service, x = 32f, y = 595f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = flightQuery.FirstOrDefault().DepLocationName.ToString() + " - " + flightQuery.FirstOrDefault().ArrLocationName.ToString() + " / " + flightQuery.FirstOrDefault().Service, x = 32f, y = 595f - _deltaY, FontSize = 8 });
            if (flightQuery.Count() > 1)
            {
                writeList.Add(new TvReport.Coordinate { value = flightQuery.LastOrDefault().DepLocationName.ToString() + " - " + flightQuery.LastOrDefault().ArrLocationName.ToString() + " / " + flightQuery.LastOrDefault().Service, x = 215f, y = 595f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightQuery.LastOrDefault().DepLocationName.ToString() + " - " + flightQuery.LastOrDefault().ArrLocationName.ToString() + " / " + flightQuery.LastOrDefault().Service, x = 215f, y = 595f - _deltaY, FontSize = 8 });
            }
        }
        SupplierRecord supp = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
        if (supp != null)
        {
            writeList.Add(new TvReport.Coordinate { value = supp.Name + " " + supp.Phone1.ToString(), x = 115f, y = 562f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = supp.Name + " " + supp.Phone1.ToString(), x = 115f, y = 562f - _deltaY, FontSize = 8 });
        }
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 564f, y = 631f, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 564f, y = 631f - _deltaY, FontSize = 8, RightToLeft = true });

        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyName + (UserData.Location.HasValue ? "(" + new Locations().getLocationName(UserData, UserData.Location.Value, ref errorMsg) + ")" : ""), x = 564f, y = 619f, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyName + (UserData.Location.HasValue ? "(" + new Locations().getLocationName(UserData, UserData.Location.Value, ref errorMsg) + ")" : ""), x = 564f, y = 619f - _deltaY, FontSize = 8, RightToLeft = true });

        writeList.Add(new TvReport.Coordinate { value = UserData.UserName.ToString(), x = 564f, y = 606f, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = UserData.UserName.ToString(), x = 564f, y = 606f - _deltaY, FontSize = 8, RightToLeft = true });

        float _Y = 732f;
        foreach (var row in custs)
        {
            writeList.Add(new TvReport.Coordinate { value = row.Surname + " " + row.Name, x = 118f, y = _Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.Surname + " " + row.Name, x = 118f, y = _Y - _deltaY, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = row.TitleStr, x = 318f, y = _Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.TitleStr, x = 318f, y = _Y - _deltaY, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 350f, y = _Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 350f, y = _Y - _deltaY, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = row.PassSerie.ToString() + (string.IsNullOrEmpty(row.PassNo) ? "№" + row.PassNo.ToString() : ""), x = 470f, y = _Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.PassSerie.ToString() + (string.IsNullOrEmpty(row.PassNo) ? "№" + row.PassNo.ToString() : ""), x = 470f, y = _Y - _deltaY, FontSize = 8 });
            _Y -= 11;
        }

        return new TvReport.AnexReport.AnexReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var customersTicket = from H in ResData.ResService
                              join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                              join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                              join T in ResData.Title on C.Title equals T.TitleNo
                              join F in flights on H.Service equals F.Code
                              join A in airLines on F.Airline equals A.Code
                              where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                                && H.StatSer < 2
                              group Rc by new
                              {
                                  CustNo = Rc.CustNo,
                                  Name = C.Name,
                                  SurName = C.Surname,
                                  TitleName = T.Code
                              } into g
                              select new
                              {
                                  CustNo = g.Key.CustNo,
                                  Name = g.Key.Name,
                                  SurName = g.Key.SurName,
                                  TitleName = g.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;

            Int16? DocStat = Conversion.getInt16OrNull(ResData.ResCon.Find(f => f.CustNo == row.CustNo).DocStat);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
            }
            else
            {

                //if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                //    sb.Append("         Бронирование оплаты еще не завершено.");
                //else
                //{
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                            ResData.ResMain.ResNo,
                            row.CustNo,
                            i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
                //}
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicketRefListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var customersTicket = from H in ResData.ResService
                              join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                              join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                              join T in ResData.Title on C.Title equals T.TitleNo
                              join F in flights on H.Service equals F.Code
                              join A in airLines on F.Airline equals A.Code
                              where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                              group Rc by new
                              {
                                  CustNo = Rc.CustNo,
                                  Name = C.Name,
                                  SurName = C.Surname,
                                  TitleName = T.Code
                              } into g
                              select new
                              {
                                  CustNo = g.Key.CustNo,
                                  Name = g.Key.Name,
                                  SurName = g.Key.SurName,
                                  TitleName = g.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;

            Int16? DocStat = Conversion.getInt16OrNull(ResData.ResCon.Find(f => f.CustNo == row.CustNo).DocStat);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span>{0}</span>&nbsp;&nbsp;",
                                i.ToString() + ". " + Name);
                sb.AppendFormat("<span onclick=\"TicketRef('{0}', '{1}', '{2}', 'HTMLPDF');\" style=\"cursor: pointer; text-decoration: underline;\">{3} ",
                                filename + "TicketRef.aspx",
                                "TicketRef" + row.CustNo,
                                row.CustNo,
                                "Справка по а/б");
                sb.Append("</span>");
            }
            else
            {

                //if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                //    sb.Append("         Бронирование оплаты еще не завершено.");
                //else
                //{
                sb.AppendFormat("<span>{0}</span>&nbsp;&nbsp;",
                            i.ToString() + ". " + Name);
                sb.AppendFormat("&nbsp;&nbsp;<span onclick=\"TicketRef('{0}', '{1}', '{2}');\" style=\"cursor: pointer; text-decoration: underline;\">{3} ",
                            filename + "TicketRef.aspx",
                            "TicketRef" + row.CustNo,
                            row.CustNo,
                            "Справка по а/б");
                sb.Append("</span>");
                //}
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            writeList.Clear();
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                if (string.Equals(row1.airline, "UT"))
                {
                    #region UTAIR
                    ++cnt;
                    List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                                  join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                                  join q3 in flyList on q1.Service equals q3.FlightNo
                                                                  where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                                  select q1).ToList<ResServiceRecord>();

                    string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());
                    writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToString("dd.MMM.yyyy"), x = 511f, y = 616f, FontSize = 8 });

                    AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);
                    string agency_airLineName = UserData.AgencyName + " / " + (airLine != null ? airLine.Name : "");
                    writeList.Add(new TvReport.Coordinate { value = agency_airLineName, x = 164f, y = 616f, FontSize = 8 });

                    writeList.Add(new TvReport.Coordinate { value = resCust.Surname + " " + resCust.Name + " " + resCust.TitleStr, x = 72f, y = 565f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = resCust.Birtday.HasValue ? resCust.Birtday.Value.ToShortDateString() : "", x = 294f, y = 565f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = resCust.PassSerie.ToString() + " " + resCust.PassNo.ToString(), x = 388f, y = 565f, FontSize = 8 });
                    Location national = new Locations().getLocation(UserData.Market, resCust.Nation, ref errorMsg);
                    writeList.Add(new TvReport.Coordinate { value = national != null ? national.Code.ToString() : "", x = 500f, y = 565f, FontSize = 8 });                                                            
                    
                    float _Y = 510f;
                    foreach (ResServiceRecord row in custAirleineFlights)
                    {
                        FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                        writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToString("dd.MMM"), x = 34f, y = _Y, FontSize = 8 });
                        writeList.Add(new TvReport.Coordinate { value = row.Service, x = 99f, y = _Y, FontSize = 8 });
                        AirportRecord depAir = new Flights().getAirport(UserData.Market, flightDay.DepAirport, ref errorMsg);
                        writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport + (depAir != null ? "-" + depAir.Name : ""), x = 160f, y = _Y, FontSize = 8 });
                        AirportRecord arrAir = new Flights().getAirport(UserData.Market, flightDay.ArrAirport, ref errorMsg);
                        writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport + (arrAir != null ? "-" + arrAir.Name : ""), x = 297f, y = _Y, FontSize = 8 });
                        writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToString("HH:mm") : "", x = 437f, y = _Y, FontSize = 8 });
                        writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 478f, y = _Y, FontSize = 8 });                                                                                                                                                
                        writeList.Add(new TvReport.Coordinate { value = flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString("#,###") : "", x = 527f, y = _Y, FontSize = 8 });
                        new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg);
                        _Y -= 11f;
                    }


                    string ServiceCode = "ETicket_" + row1.airline;
                    eTicketList.Add(new TvReport.AnexReport.AnexReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
                    #endregion
                }
                else
                    if (string.Equals(row1.airline, "U6"))
                    {
                        #region URAL AIRLINES
                        ++cnt;
                        List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                                      join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                                      join q3 in flyList on q1.Service equals q3.FlightNo
                                                                      where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                                      select q1).ToList<ResServiceRecord>();
                        string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());

                        writeList.Add(new TvReport.Coordinate { value = resCust.Surname + " " + resCust.Name, x = 41f, y = 620f, FontSize = 8 });
                        writeList.Add(new TvReport.Coordinate { value = custFlights.FirstOrDefault().ResNo, x = 186f, y = 620f, FontSize = 8 });
                        writeList.Add(new TvReport.Coordinate { value = eTicketNo, x = 320f, y = 620f, FontSize = 8 });
                        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToString("dd.MM.yyyy") + " / " + agency.LocationName, x = 445f, y = 620f, FontSize = 8 });
                        
                        float _Y = 547f;
                        foreach (ResServiceRecord row in custAirleineFlights)
                        {
                            FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                            AirportRecord depAir = new Flights().getAirport(UserData.Market, flightDay.DepAirport, ref errorMsg);
                            AirportRecord arrAir = new Flights().getAirport(UserData.Market, flightDay.ArrAirport, ref errorMsg);
                            
                            writeList.Add(new TvReport.Coordinate { value = row.Service, x = 41, y = _Y, FontSize = 8 });
                            writeList.Add(new TvReport.Coordinate { value = row.DepLocationName + (depAir != null ? "-(" + depAir.Name + ")" : ""), x = 101f, y = _Y, FontSize = 8 });
                            writeList.Add(new TvReport.Coordinate { value = row.ArrLocationName + (arrAir != null ? "-(" + arrAir.Name + ")" : ""), x = 101f, y = _Y - 11, FontSize = 8 });
                            writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToString("dd.MMM"), x = 263f, y = _Y, FontSize = 8 });
                            writeList.Add(new TvReport.Coordinate { value = row.EndDate.HasValue ? row.EndDate.Value.ToString("dd.MMM") : "", x = 346f, y = _Y, FontSize = 8 });                            
                            writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 431f, y = _Y, FontSize = 8 });
                            writeList.Add(new TvReport.Coordinate { value = flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString("#,###") + "K" : "", x = 523f, y = _Y, FontSize = 8 });
                            _Y -= 25;
                            new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg);
                        }


                        string ServiceCode = "ETicket_" + row1.airline;
                        eTicketList.Add(new TvReport.AnexReport.AnexReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
                        #endregion
                    }

            }
            string retVal = string.Empty;
            if (eTicketList.Count() == 1)
                retVal = eTicketList.FirstOrDefault();
            else retVal = new TvReport.AnexReport.AnexReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);

            return retVal;
        }
        else return "";

    }

    public string showProforma(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        BankRecord oprBank = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);

        writeList.Add(new TvReport.Coordinate { value = "Proforma Nr." + ResData.ResMain.ResNo, x = 252, y = 827, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = oprBank.NameL + ", " + opr.AddrCity, x = 40, y = 743, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.Bank1IBAN, x = 370, y = 744, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.Bank1AccNo, x = 370, y = 718, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.Bank2AccNo, x = 370, y = 679, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.TaxAccNo, x = 70, y = 695, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.TaxOffice, x = 210, y = 695, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.FirmName, x = 40, y = 680, FontSize = 10 });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo + " от " + DateTime.Today.ToShortDateString() + " г.", x = 265, y = 626, FontSize = 12, bold = true });
        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("ИНН {0}, КПП {1}, {2}, {3}",
                            opr.TaxAccNo,
                            opr.TaxOffice,
                            opr.FirmName,
                            opr.AddrZip),
            x = 110,
            y = 590,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("{0}, {1}",
                            opr.InvAddrCity,
                            opr.InvAddress),
            x = 110,
            y = 578,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("ИНН {0}, КПП {1}, {2}, {3}",
                            agency.TaxAccNo,
                            agency.TaxOffice,
                            agency.FirmName,
                            agency.AddrZip),
            x = 110,
            y = 533,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("{0}, {1}",
                            agency.InvAddrCity,
                            agency.InvAddress),
            x = 110,
            y = 521,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 155, y = 462, FontSize = 10, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "", x = 470, y = 462, FontSize = 10, bold = true, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "", x = 575, y = 462, FontSize = 10, bold = true, RightToLeft = true });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "", x = 575, y = 380, FontSize = 10, bold = true, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? (ResData.ResMain.AgencyPayable.Value + 1).ToString("#,###.00") : "", x = 575, y = 418, FontSize = 10, bold = true, RightToLeft = true });
        TvTools.NumberToWordRU numToWordC = new NumberToWordRU();
        string numToWord = numToWordC.CurrencyToText(ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value : 0, ResData.ResMain.SaleCur, "");
        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("Всего наименований 2, на сумму {0} {1}",
                            ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "",
                            " ( " + numToWord + " )"),
            x = 40,
            y = 333,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate { value = opr.BossName, x = 532, y = 186, FontSize = 10, RightToLeft = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = opr.ContName, x = 532, y = 158, FontSize = 10, RightToLeft = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = "По доверенности №021110/1 от 02.11.10.", x = 420, y = 143, FontSize = 10, RightToLeft = true, Align = TvReport.fontAlign.Right });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 335, y = 77, FontSize = 10 });

        return new TvReport.SunReport.SunReport().createProforma(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showInvoice(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        AgencyRecord agency = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        BankRecord oprBank = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);
        BankRecord agencyBank = new Banks().getBank(UserData.Market, agency.Bank1, ref errorMsg);


        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo + " от " + DateTime.Today.ToShortDateString(), x = 274f, y = 581f, FontSize = 10, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "ООО " + (string.IsNullOrEmpty(agency.FirmName) ? agency.Name : agency.FirmName.ToString()) + " Лидер", x = 140f, y = 568f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "ООО " + (string.IsNullOrEmpty(agency.FirmName) ? agency.Name : agency.FirmName.ToString()) + " Лидер", x = 160f, y = 554f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 168f, y = 485f, FontSize = 8 });
        decimal agencyPayable_agencyCom = (ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value : 0) + (ResData.ResMain.AgencyCom.HasValue ? ResData.ResMain.AgencyCom.Value : 0);
        writeList.Add(new TvReport.Coordinate { value = agencyPayable_agencyCom > 0 ? agencyPayable_agencyCom.ToString("#.00") : "", x = 488f, y = 487f, FontSize = 7, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = agencyPayable_agencyCom > 0 ? agencyPayable_agencyCom.ToString("#.00") : "", x = 532f, y = 487f, FontSize = 7, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyCom.HasValue ? ResData.ResMain.AgencyCom.Value.ToString("#.00") : "", x = 488f, y = 468f, FontSize = 7, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyCom.HasValue ? ResData.ResMain.AgencyCom.Value.ToString("#.00") : "", x = 532f, y = 468f, FontSize = 7, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#.00") : "", x = 532f, y = 432, FontSize = 7, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#.00") : "", x = 225f, y = 391f, FontSize = 7 });

        CurrencyRecord currencyInfo = new Banks().getCurrency(UserData.Market, ResData.ResMain.SaleCur, ref errorMsg);
        string amountToWord = new TvTools.NumberToWordRU().CurrencyToText(ResData.ResMain.AgencyPayable.Value, currencyInfo.NameL, currencyInfo.FracName);
        writeList.Add(new TvReport.Coordinate { value = amountToWord.Length > 70 ? amountToWord.Substring(0, 70) : amountToWord, x = 140f, y = 377f, FontSize = 8 });
        if (amountToWord.Length > 70)
            writeList.Add(new TvReport.Coordinate { value = amountToWord.Substring(70), x = 140f, y = 377f, FontSize = 8 });

        decimal? amountToBaseCur = new TvBo.Common().Exchange(ResData.ResMain.PLMarket, DateTime.Today, ResData.ResMain.SaleCur, UserData.TvParams.TvParamSystem.BaseCur.ToString(), ResData.ResMain.AgencyPayable.Value, true, ref errorMsg);
        writeList.Add(new TvReport.Coordinate { value = amountToBaseCur.HasValue ? amountToBaseCur.Value.ToString("#.00") : "", x = 149f, y = 346f, FontSize = 8 });

        return new TvReport.AnexReport.AnexReport().createInvoice(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

}


