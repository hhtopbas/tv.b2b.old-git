﻿using System;


public class dbShemaRecord
{
    public dbShemaRecord()
    {
        _tablesShema = new System.Collections.Generic.List<dbTablesInformationShema>();
        _spsShema = new System.Collections.Generic.List<dbSpsInformationShema>();
    }

    System.Collections.Generic.List<dbTablesInformationShema> _tablesShema;
    public System.Collections.Generic.List<dbTablesInformationShema> tablesShema
    {
        get { return _tablesShema; }
        set { _tablesShema = value; }
    }

    System.Collections.Generic.List<dbSpsInformationShema> _spsShema;
    public System.Collections.Generic.List<dbSpsInformationShema> spsShema
    {
        get { return _spsShema; }
        set { _spsShema = value; }
    }

}

public class ScheduleRecord
{
    public ScheduleRecord()
    {
    }

    string _ScheduleName;
    public string ScheduleName
    {
        get { return _ScheduleName; }
        set { _ScheduleName = value; }
    }

    DateTime _LastRuning;
    public DateTime LastRuning
    {
        get { return _LastRuning; }
        set { _LastRuning = value; }
    }

    Int32 _RunningCount;
    public Int32 RunningCount
    {
        get { return _RunningCount; }
        set { _RunningCount = value; }
    }

    Int32 _Interval;
    public Int32 Interval
    {
        get { return _Interval; }
        set { _Interval = value; }
    }
}

public class GlobalRecord
{
    public System.Collections.Generic.List<OperatorLogoRecord> OprLogoUrl { get; set; }
    public System.Collections.Generic.List<SessionRecord> SessionRec { get; set; }
    public System.Collections.Generic.List<TvBo.MarketRecord> MarketList { get; set; }
    public System.Collections.Generic.List<dbTablesInformationShema> tablesInformationShemaList { get; set; }
    public System.Collections.Generic.List<dbSpsInformationShema> spsInformationShemaList { get; set; }
    public System.Collections.Generic.List<TvBo.PackPriceFilterCache> PackPriceFltCache { get; set; }
    public System.Collections.Generic.List<TvBo.PackPriceFlightDayCache> PackPriceFlightDayCache { get; set; }
    public GlobalRecord()
    {
        this.SessionRec = new System.Collections.Generic.List<SessionRecord>();
        this.MarketList = new System.Collections.Generic.List<TvBo.MarketRecord>();
        this.tablesInformationShemaList = new System.Collections.Generic.List<dbTablesInformationShema>();
        this.spsInformationShemaList = new System.Collections.Generic.List<dbSpsInformationShema>();
        this.PackPriceFltCache = new System.Collections.Generic.List<TvBo.PackPriceFilterCache>();
        this.PackPriceFlightDayCache = new System.Collections.Generic.List<TvBo.PackPriceFlightDayCache>();
    }
}

public class OperatorLogoRecord
{
    public OperatorLogoRecord()
    {
    }

    string _Code;
    public string Code
    {
        get { return _Code; }
        set { _Code = value; }
    }

    string _LogoPath;
    public string LogoPath
    {
        get { return _LogoPath; }
        set { _LogoPath = value; }
    }
}

public class SessionRecord
{
    public SessionRecord()
    {
    }

    string _SessionID;
    public string SessionID
    {
        get { return _SessionID; }
        set { _SessionID = value; }
    }

    DateTime? _SessionStart;
    public DateTime? SessionStart
    {
        get { return _SessionStart; }
        set { _SessionStart = value; }
    }

    string _SessionStartIP;
    public string SessionStartIP
    {
        get { return _SessionStartIP; }
        set { _SessionStartIP = value; }
    }

    string _ClientInfo;
    public string ClientInfo
    {
        get { return _ClientInfo; }
        set { _ClientInfo = value; }
    }

}

public class dbTablesInformationShema
{
    public dbTablesInformationShema()
    {
    }

    int _SeqNo;
    public int SeqNo
    {
        get { return _SeqNo; }
        set { _SeqNo = value; }
    }

    string _Name;
    public string Name
    {
        get { return _Name; }
        set { _Name = value; }
    }

    string _DType;
    public string DType
    {
        get { return _DType; }
        set { _DType = value; }
    }

    string _TableName;
    public string TableName
    {
        get { return _TableName; }
        set { _TableName = value; }
    }
}

public class dbSpsInformationShema
{
    public dbSpsInformationShema()
    {
    }

    int _SeqNo;
    public int SeqNo
    {
        get { return _SeqNo; }
        set { _SeqNo = value; }
    }

    string _Name;
    public string Name
    {
        get { return _Name; }
        set { _Name = value; }
    }

    string _DType;
    public string DType
    {
        get { return _DType; }
        set { _DType = value; }
    }

    string _SpName;
    public string SpName
    {
        get { return _SpName; }
        set { _SpName = value; }
    }
}

