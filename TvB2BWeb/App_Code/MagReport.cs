﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using System.Text;
using TvTools;
using System.IO;

public class MagReport
{
    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;
        int voucherNo = 1;
        int i = 0;
        if (ResData.ResService.Where(w => w.ServiceType == "HOTEL").Count() > 0)
        {
            var qHotel = from q in ResData.ResService
                         orderby q.RecID
                         select q;

            foreach (var row1 in qHotel)
            {
                ++i;
                if (row1.RecID == ServiceID) voucherNo = i;
            }
        }
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo + " - " + voucherNo.ToString(), x = 170.1f, y = 490f, FontSize = 12, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo + " - " + voucherNo.ToString(), x = 170.1f + 273.58f, y = 490f, FontSize = 12, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo + " - " + voucherNo.ToString(), x = 170.1f + 547.155f, y = 490f, FontSize = 12, bold = true });
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, resService.Service, ref errorMsg);
        writeList.Add(new TvReport.Coordinate { value = hotel.PostAddress + " " + hotel.PostZip, x = 68f, y = 474f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = hotel.PostAddress + " " + hotel.PostZip, x = 68f + 273.58f, y = 474f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = hotel.PostAddress + " " + hotel.PostZip, x = 68f + 547.155f, y = 474f, FontSize = 8 });


        var serviceCust = from q1 in ResData.ResCon
                          join q2 in ResData.ResCust on q1.CustNo equals q2.CustNo
                          where q1.ServiceID == ServiceID
                          select q2;
        float _y = 440f;
        foreach (var row in serviceCust)
        {
            _y -= 10;
            writeList.Add(new TvReport.Coordinate { value = row.Surname + " " + row.Name, x = 85.05f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.Surname + " " + row.Name, x = 85.05f + 273.58f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.Surname + " " + row.Name, x = 85.05f + 547.155f, y = _y, FontSize = 8 });
        }

        writeList.Add(new TvReport.Coordinate { value = resService.Adult.HasValue ? resService.Adult.Value.ToString() : "", x = 65f, y = 375.55f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Adult.HasValue ? resService.Adult.Value.ToString() : "", x = 65f + 273.58f, y = 375.55f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Adult.HasValue ? resService.Adult.Value.ToString() : "", x = 65f + 547.155f, y = 375.55f, FontSize = 8 });
        int infCount = serviceCust.Where(w => w.Title >= 8).Count();
        writeList.Add(new TvReport.Coordinate { value = infCount.ToString(), x = 165.8475f, y = 375.55f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = infCount.ToString(), x = 165.8475f + 273.58f, y = 375.55f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = infCount.ToString(), x = 165.8475f + 547.155f, y = 375.55f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Child.HasValue ? (resService.Child.Value - infCount).ToString() : "", x = 268f, y = 375.55f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Child.HasValue ? (resService.Child.Value - infCount).ToString() : "", x = 268f + 273.58f, y = 375.55f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Child.HasValue ? (resService.Child.Value - infCount).ToString() : "", x = 268f + 547.155f, y = 375.55f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationName, x = 102f, y = 360.5788f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationName, x = 102f + 273.58f, y = 360.5788f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationName, x = 102f + 547.155f, y = 360.5788f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString() + " - " + resService.EndDate.Value.ToShortDateString(), x = 102f, y = 329.19f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString() + " - " + resService.EndDate.Value.ToShortDateString(), x = 102f + 273.58f, y = 329.19f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString() + " - " + resService.EndDate.Value.ToShortDateString(), x = 102f + 547.155f, y = 329.19f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Duration.ToString(), x = 266.54f, y = 329.19f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Duration.ToString(), x = 266.54f + 273.58f, y = 329.19f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Duration.ToString(), x = 266.54f + 547.155f, y = 329.19f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 102f, y = 306.51f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 102f + 273.58f, y = 306.51f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 102f + 547.155f, y = 306.51f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "1 " + resService.RoomName + " " + resService.AccomName, x = 102f, y = 282.2475f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "1 " + resService.RoomName + " " + resService.AccomName, x = 102f + 273.58f, y = 282.2475f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "1 " + resService.RoomName + " " + resService.AccomName, x = 102f + 547.155f, y = 282.2475f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BoardName, x = 102f, y = 267.1113f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BoardName, x = 102f + 273.58f, y = 267.1113f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BoardName, x = 102f + 547.155f, y = 267.1113f, FontSize = 8 });

        var trfQuery = from q in ResData.ResService
                       where q.ServiceType == "TRANSFER"
                       orderby q.BegDate
                       group q by new { BegDate = q.BegDate } into k
                       select new { k.Key.BegDate };
        if (trfQuery.Count() > 0)
        {
            if (trfQuery.Count() == 1)
            {
                writeList.Add(new TvReport.Coordinate { value = "X", x = 104.895f, y = 245.975f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = "X", x = 104.895f + 273.58f, y = 245.975f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = "X", x = 104.895f + 547.155f, y = 245.975f, FontSize = 8 });
            }
            if (trfQuery.Count() > 1)
            {
                writeList.Add(new TvReport.Coordinate { value = "X", x = 104.895f, y = 245.975f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = "X", x = 104.895f + 273.58f, y = 245.975f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = "X", x = 104.895f + 547.155f, y = 245.975f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = "X", x = 249.48f, y = 245.975f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = "X", x = 249.48f + 273.58f, y = 245.975f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = "X", x = 249.48f + 547.155f, y = 245.975f, FontSize = 8 });
            }
        }
        writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString(), x = 23.2725f, y = 202.45f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString(), x = 23.2725f + 273.58f, y = 202.45f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString(), x = 23.2725f + 547.155f, y = 202.45f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.EndDate.Value.ToShortDateString(), x = 164.43f, y = 202.45f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.EndDate.Value.ToShortDateString(), x = 164.43f + 273.58f, y = 202.45f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.EndDate.Value.ToShortDateString(), x = 164.43f + 547.155f, y = 202.45f, FontSize = 8 });
        _y = 192.85f;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "EXCURSION"))
        {
            _y -= 10f;
            writeList.Add(new TvReport.Coordinate { value = row.ServiceName, x = 104.9f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.ServiceName, x = 104.9f + 273.58f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.ServiceName, x = 104.9f + 547.155f, y = _y, FontSize = 8 });
        }

        _y = 167.34f;
        foreach (ResServiceExtRecord row in ResData.ResServiceExt.Where(w => w.ServiceType == "HOTEL" && w.ServiceID == ServiceID))
        {
            _y -= 10f;
            writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName, x = 104.9f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName, x = 104.9f + 273.58f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName, x = 104.9f + 547.155f, y = _y, FontSize = 8 });
        }

        List<InvoiceRecord> invoice = new Payments().getInvoiceRes(UserData, ResData.ResMain.ResNo, Convert.ToInt16(0), ref errorMsg);
        if (invoice != null && invoice.Count > 0)
        {
            writeList.Add(new TvReport.Coordinate { value = invoice.FirstOrDefault().InvSerial + invoice.FirstOrDefault().InvNo.ToString(), x = 82.215f, y = 127.91f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = invoice.FirstOrDefault().InvSerial + invoice.FirstOrDefault().InvNo.ToString(), x = 82.215f + 273.58f, y = 127.91f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = invoice.FirstOrDefault().InvSerial + invoice.FirstOrDefault().InvNo.ToString(), x = 82.215f + 547.155f, y = 127.91f, FontSize = 8 });
        }
        List<JournalCDetRecord> payments = new Payments().getJournalCDetList(ResData.ResMain.ResNo, null, ref errorMsg);
        if (payments != null && payments.Count > 0)
        {
            writeList.Add(new TvReport.Coordinate { value = payments.FirstOrDefault().AllocDate.HasValue ? payments.FirstOrDefault().AllocDate.Value.ToShortDateString() : "", x = 172.5f, y = 127.91f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = payments.FirstOrDefault().AllocDate.HasValue ? payments.FirstOrDefault().AllocDate.Value.ToShortDateString() : "", x = 172.5f + 273.58f, y = 127.91f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = payments.FirstOrDefault().AllocDate.HasValue ? payments.FirstOrDefault().AllocDate.Value.ToShortDateString() : "", x = 172.5f + 547.155f, y = 127.91f, FontSize = 8 });
        }
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "", x = 66.6225f, y = 88.6325f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "", x = 66.6225f + 273.58f, y = 88.6325f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "", x = 66.6225f + 547.155f, y = 88.6325f, FontSize = 8 });

        return new TvReport.MagReport.MagReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showInvoice(User UserData, ResDataRecord ResData, ref string errorMsg)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        BankRecord oprBank = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);
        BankRecord agencyBank = new Banks().getBank(UserData.Market, agency.Bank1, ref errorMsg);
        ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
        if (leader == null)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError").ToString();
            return "";
        }
        ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
        if (leaderInfo == null)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError").ToString();
            return "";
        }

        Int32? proformaNumber = Conversion.getInt32OrNull(new Payments().getNewProformaNumber());
        if (!proformaNumber.HasValue) proformaNumber += 1;
        else if (proformaNumber.HasValue && proformaNumber.Value > 0) proformaNumber += 1;
        else proformaNumber = 1;
        writeList.Add(new TvReport.Coordinate { value = "PRO" + proformaNumber.Value.ToString(), x = 170f, y = 259f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 170f, y = 253.5f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 43f, y = 248f, FontSize = 8 });

        AgencyRecord agencyRec = new Agency().getAgency(ResData.ResMain.Agency, false, ref errorMsg);
        TvReport.MagReport.AgencyDocAddressRecord agencyDocAddr = new TvReport.MagReport.MagReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        writeList.Add(new TvReport.Coordinate { value = agencyDocAddr.Name, x = 35f, y = 241f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyDocAddr.TaxAccNo, x = 35f, y = 235.75f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyDocAddr.TaxAccNo, x = 35f, y = 230.5f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyRec.LocationLocalName, x = 35f, y = 225.25f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyDocAddr.InvAddress + " " + agencyDocAddr.InvAddrZip + " " + agencyDocAddr.InvAddrCity, x = 35f, y = 220f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyRec.BossName, x = 35f, y = 214.75f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyRec.Phone1, x = 35f, y = 209.5f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = opr.FirmName, x = 129f, y = 241f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = opr.TaxAccNo, x = 129f, y = 235.75f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "BG" + opr.TaxAccNo, x = 129f, y = 230.5f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = opr.LocationNameL, x = 129f, y = 225.25f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = opr.InvAddress + " " + opr.InvAddrZip + " " + opr.InvAddrCity, x = 129f, y = 220f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = opr.BossName, x = 129f, y = 214.75f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = opr.Phone1, x = 129f, y = 209.5f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "№", x = 15.0f, y = 201.5f, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        writeList.Add(new TvReport.Coordinate { value = "Description", x = 94.75f, y = 201.5f, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        writeList.Add(new TvReport.Coordinate { value = "Amount (" + ResData.ResMain.SaleCur + ")", x = 184.5f, y = 201.5f, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        float _y = 199.0f;
        int line = 0;
        if (ResData.ResService.Count > 0)
        {
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 11f, y = _y, H = 0, W = 188f, LineWidth = 0.25f });
            foreach (ResServiceRecord row in ResData.ResService)
            {
                _y -= 5f;
                ++line;
                writeList.Add(new TvReport.Coordinate { value = line.ToString(), x = 18f, y = _y + 1f, FontSize = 8, Align = TvReport.fontAlign.Right });
                string Desc = row.ServiceNameL;
                switch (row.ServiceType)
                {
                    case "HOTEL":
                        Desc = row.DepLocationNameL + " (" + row.Room + "," + row.Accom + "," + row.Board + ") " + row.BegDate.Value.ToShortDateString() + " - " + row.EndDate.Value.ToShortDateString();
                        break;
                    case "FLIGHT":
                        Desc = row.BegDate.Value.ToShortDateString();
                        break;
                    case "TRANSPORT":
                        Desc = row.DepLocationNameL + " -> " + row.ArrLocationNameL + " " + row.BegDate.Value.ToShortDateString();
                        break;
                    case "RENTING":
                        Desc = row.DepLocationNameL;
                        break;
                    case "EXCURSION":
                        Desc = row.DepLocationNameL;
                        break;
                    case "INSURANCE":
                        Desc = row.BegDate.Value.ToShortDateString() + (row.EndDate.HasValue ? " - " + row.EndDate.Value.ToShortDateString() : "");
                        break;
                    case "VISA":
                        Desc = row.BegDate.Value.ToShortDateString() + (row.EndDate.HasValue ? " - " + row.EndDate.Value.ToShortDateString() : "");
                        break;
                    case "TRANSFER":
                        Desc = row.BegDate.Value.ToShortDateString();
                        break;
                    default:
                        Desc = row.BegDate.Value.ToShortDateString() + (row.EndDate.HasValue ? " - " + row.EndDate.Value.ToShortDateString() : "");
                        break;
                }
                writeList.Add(new TvReport.Coordinate { value = row.ServiceNameL + " " + Desc, x = 22f, y = _y + 1f, FontSize = 8 });

                writeList.Add(new TvReport.Coordinate { value = row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "", x = 198f, y = _y + 1f, FontSize = 8, Align = TvReport.fontAlign.Right });
                writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 11f, y = _y, H = 5f, W = 0f, LineWidth = 0.25f });
                writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20f, y = _y, H = 5f, W = 0f, LineWidth = 0.25f });
                writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 170f, y = _y, H = 5f, W = 0f, LineWidth = 0.25f });
                writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 199f, y = _y, H = 5f, W = 0f, LineWidth = 0.25f });

                writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 11f, y = _y, H = 0, W = 188f, LineWidth = 0.25f });
            }
        }
        foreach (ResServiceExtRecord row in ResData.ResServiceExt)
        {
            _y -= 5f;
            ++line;
            writeList.Add(new TvReport.Coordinate { value = line.ToString(), x = 18f, y = _y + 1f, FontSize = 8, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = row.ExtServiceNameL + " " + row.BegDate.Value.ToShortDateString() + (row.BegDate != row.EndDate && row.EndDate.HasValue ? " - " + row.EndDate.Value.ToShortDateString() : ""), x = 22f, y = _y + 1f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "", x = 198f, y = _y + 1f, FontSize = 8, Align = TvReport.fontAlign.Right });

            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 11f, y = _y, H = 5f, W = 0f, LineWidth = 0.25f });
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20f, y = _y, H = 5f, W = 0f, LineWidth = 0.25f });
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 170f, y = _y, H = 5f, W = 0f, LineWidth = 0.25f });
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 199f, y = _y, H = 5f, W = 0f, LineWidth = 0.25f });

            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 11f, y = _y, H = 0, W = 188f, LineWidth = 0.25f });
        }

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 198f, y = 86, FontSize = 8, Align = TvReport.fontAlign.Right });
        decimal TotalCommision = (ResData.ResMain.AgencyCom.HasValue ? ResData.ResMain.AgencyCom.Value : 0) + (ResData.ResMain.AgencyComSup.HasValue ? ResData.ResMain.AgencyComSup.Value : 0);
        writeList.Add(new TvReport.Coordinate { value = TotalCommision > 0 ? TotalCommision.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 198f, y = 81, FontSize = 8, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 198f, y = 76, FontSize = 8, Align = TvReport.fontAlign.Right });

        writeList.Add(new TvReport.Coordinate { value = opr.LocationNameL, x = 27f, y = 57f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 47f, y = 52f, FontSize = 8 });

        BankRecord bank1 = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);
        writeList.Add(new TvReport.Coordinate { value = opr.Bank1IBAN, x = 137.5f, y = 62f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = bank1 != null ? bank1.NameL : "", x = 137.5f, y = 57f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = opr.Bank1AccNo, x = 137.5f, y = 52f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = agency.BossName, x = 32f, y = 42f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = UserData.UserName, x = 134f, y = 42f, FontSize = 8 });

        string passangerList = string.Empty;
        foreach (ResCustRecord row in ResData.ResCust)
            passangerList += passangerList.Length > 0 ? " / " + row.Surname + " " + row.Name : row.Surname + " " + row.Name;
        writeList.Add(new TvReport.Coordinate { value = passangerList, x = 41f, y = 15f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyUserName, x = 30f, y = 12f, FontSize = 8 });
        string retVal = new TvReport.MagReport.MagReport().createInvoice(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);

        if (!string.IsNullOrEmpty(retVal))
        {
            //if (!Equals(ResData.ResMain.InvoiceIssued, "Y"))
            //{
            FileStream st = new FileStream(siteFolderISS + "ACE\\" + retVal, FileMode.Open);
            if (st.Length > 0)
            {
                byte[] buffer = new byte[st.Length];
                st.Read(buffer, 0, (int)st.Length);
                st.Close();
                bool? invRetVal = new Payments().createInvoice(UserData, ResData.ResMain.ResNo, ResData.ResMain.PLOperator, UserData.OprOffice, leader.CustNo.ToString(),
                                                    leader.Surname + " " + leader.Name,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHome : leaderInfo.AddrWork,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeZip : leaderInfo.AddrWorkZip,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCity : leaderInfo.AddrWorkCity,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry,
                                                    ResData.ResMain.PasAmount, ResData.ResMain.SaleCur, buffer, UserData.UserID, UserData.AgencyID,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeEmail : leaderInfo.AddrWorkEMail,
                                                    "",
                                                    proformaNumber.HasValue ? proformaNumber.Value : 1);
                if (invRetVal.HasValue && invRetVal.Value)
                    if (new TvReport.DetReport.DetReport().invoiceIssued(ResData.ResMain.ResNo))
                    {
                        ResData.ResMain.InvoiceIssued = "Y";
                        HttpContext.Current.Session["ResData"] = ResData;
                    }
            }
            else st.Close();


            //if (new TvReport.DetReport.DetReport().invoiceIssued(ResData.ResMain.ResNo))
            //{
            //    ResData.ResMain.InvoiceIssued = "Y";
            //    HttpContext.Current.Session["ResData"] = ResData;
            //}
        }
        return retVal;
    }

}


