﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Web;
using TvBo;


/// <summary>
/// Summary description for Global.
/// </summary>
public class Global : System.Web.HttpApplication
{
    /// <summary>
    /// Required designer variable.
    /// </summary>    

    public static bool UseB2BRole = false;
    public static bool usePaxBtnTV = false;
    public static bool useMixSearch = false;

    public static string themesPath = "themes/Default/";
    public static GlobalRecord globalRec;
    public static Thread[] ThSchedule;
    public static System.Collections.Generic.List<ScheduleRecord> ScheduleList;
    public DateTime timer;

    private System.ComponentModel.IContainer components = null;

    public Global()
    {
        InitializeComponent();
    }

    public static string getBasePageRoot()
    {
        string absolutePath = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port != 80 ? ":" + HttpContext.Current.Request.Url.Port : "") + HttpContext.Current.Request.Url.AbsolutePath;
        string relativePath = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
        relativePath = relativePath.Remove(0, 1);
        absolutePath = absolutePath.Substring(0, absolutePath.IndexOf(relativePath)) + "/";
        return absolutePath;
    }

    public static string getBasePage()
    {
        string absolutePath = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port != 80 ? ":" + HttpContext.Current.Request.Url.Port : "") + HttpContext.Current.Request.Url.AbsolutePath;
        string relativePath = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
        relativePath = relativePath.Remove(0, 1);
        absolutePath = absolutePath.Substring(0, absolutePath.IndexOf(relativePath)) + "/";
        return absolutePath + "Default.aspx";
    }

    void cachedScheduledTask(ScheduleRecord pParams)
    {
        // Execute some task        
        string errorMsg = string.Empty;
        while (true)
        {
            switch (pParams.ScheduleName)
            {
                case "PackPriceFilterCache":
                    List<PackPriceFilterCache> packPriceFilterCache = B2BSystems.PackPriceFilterCreateCacheData(ref errorMsg);
                    Application.Lock();
                    globalRec = (GlobalRecord)Application["globalRec"];
                    globalRec.PackPriceFltCache = packPriceFilterCache;
                    Application["globalRec"] = globalRec;
                    Application.UnLock();
                    break;
                case "PackPriceFlightDayCache":
                    List<PackPriceFlightDayCache> packPriceFlightDayCache = new Flights().getFlightDaysPackPrice(ref errorMsg);
                    Application.Lock();
                    globalRec = (GlobalRecord)Application["globalRec"];
                    globalRec.PackPriceFlightDayCache = packPriceFlightDayCache;
                    Application["globalRec"] = globalRec;
                    Application.UnLock();
                    break;
            }

            Thread.Sleep(pParams.Interval * 1000);
        }
    }

    protected void Application_Start(Object sender, EventArgs e)
    {
        string errorMsg = string.Empty;

        if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["themesName"]))
            themesPath = "themes/" + System.Configuration.ConfigurationManager.AppSettings["themesName"].ToString() + "/";

        String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
        if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
        {
            new WEBLog().checkAndCreateLogTables(ref errorMsg);
        }

        try
        {
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Server.MapPath("Cache"));
            System.IO.FileInfo[] files = dir.GetFiles("*.*");
            foreach (System.IO.FileInfo file in files) file.Delete();

            dir = new System.IO.DirectoryInfo(Server.MapPath("ACE"));
            files = dir.GetFiles("*.*");
            foreach (System.IO.FileInfo file in files) file.Delete();

            dir = new System.IO.DirectoryInfo(Server.MapPath("LogoCache"));
            files = dir.GetFiles("*.*");
            foreach (System.IO.FileInfo file in files) file.Delete();
        }
        catch
        {
        }

        String cachingMethod = System.Configuration.ConfigurationManager.AppSettings["CachingMethod"];

        if (string.Equals(cachingMethod, "V2"))
        {
            globalRec = new GlobalRecord();
            timer = DateTime.Now;
            try
            {
                List<PackPriceFilterCache> packPriceFilterCache = B2BSystems.PackPriceFilterCreateCacheData(ref errorMsg);
                List<PackPriceFlightDayCache> packPriceFlightDayCache = new Flights().getFlightDaysPackPrice(ref errorMsg);
                Application.Lock();
                globalRec.PackPriceFltCache = packPriceFilterCache;
                globalRec.PackPriceFlightDayCache = packPriceFlightDayCache;
                Application["globalRec"] = globalRec;
                Application.UnLock();
            }
            catch
            {
                Application.UnLock();
            }
        }

        usePaxBtnTV = GlobalLib.getPaxButtonImage();

        try
        {
            if (!System.IO.Directory.Exists(Server.MapPath("Log")))
                System.IO.Directory.CreateDirectory(Server.MapPath("Log"));
        }
        catch { }

        try
        {
            if (!System.IO.Directory.Exists(Server.MapPath("LogoCache")))
                System.IO.Directory.CreateDirectory(Server.MapPath("LogoCache"));
        }
        catch { }
        try
        {
            if (!System.IO.Directory.Exists(Server.MapPath("ACE")))
                System.IO.Directory.CreateDirectory(Server.MapPath("ACE"));
        }
        catch { }

        try
        {
            if (!System.IO.Directory.Exists(Server.MapPath("Cache")))
                System.IO.Directory.CreateDirectory(Server.MapPath("Cache"));
        }
        catch { }

        if (string.Equals(cachingMethod, "V2"))
        {
            Int32? packPriceFilterSchedule = TvTools.Conversion.getInt32OrNull(System.Configuration.ConfigurationManager.AppSettings["PackPriceFilterScheduleInterval"]);
            ScheduleList = new System.Collections.Generic.List<ScheduleRecord>();
            ScheduleList.Add(new ScheduleRecord { ScheduleName = "PackPriceFilterCache", LastRuning = DateTime.Now, RunningCount = 0, Interval = packPriceFilterSchedule.HasValue ? packPriceFilterSchedule.Value : 3600 });
            ScheduleList.Add(new ScheduleRecord { ScheduleName = "PackPriceFlightDayCache", LastRuning = DateTime.Now, RunningCount = 0, Interval = packPriceFilterSchedule.HasValue ? packPriceFilterSchedule.Value : 3600 });

            foreach (ScheduleRecord row in ScheduleList)
            {
                Thread scheduleThread = new Thread(() => cachedScheduledTask(row));
                scheduleThread.IsBackground = true;
                scheduleThread.Start();
            }
        }

        bool? useB2BRoles = TvTools.Conversion.getBoolOrNull(System.Configuration.ConfigurationManager.AppSettings["UseB2BRole"]);
        UseB2BRole = useB2BRoles.HasValue ? useB2BRoles.Value : false;
        if (UseB2BRole)
            new TvBo.Common().updateRoles();

        bool? mixSearch = TvTools.Conversion.getBoolOrNull(System.Configuration.ConfigurationManager.AppSettings["UseMixSearch"]);
        useMixSearch = mixSearch.HasValue ? mixSearch.Value : false;
    }

    protected void Session_Start(Object sender, EventArgs e)
    {
        try
        {
            string strIpAddress;
            strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (strIpAddress == null)
            {
                strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            Application.Lock();
            if (globalRec.SessionRec.Find(f => f.SessionID == Session.SessionID.ToString()) == null)
            {
                globalRec.SessionRec.Add(new SessionRecord
                {
                    SessionID = Session.SessionID.ToString(),
                    SessionStart = DateTime.Now,
                    ClientInfo = HttpContext.Current.Request.UserAgent,
                    SessionStartIP = strIpAddress
                });
            }
            Application.UnLock();
        }
        catch
        {
            Application.UnLock();
        }



        Session["BasePageRoot"] = getBasePageRoot();
        Session["BasePage"] = getBasePage();
    }

    protected void Application_BeginRequest(Object sender, EventArgs e)
    {
        string errorMsg = string.Empty;
        if (Request.Url.ToString().ToLower().Contains("configuration.cacheclear"))
        {
            List<MarketRecord> markets = new Parameters().getMarkets(ref errorMsg);
            List<OperatorRecord> operators = new Parameters().getOperators("", ref errorMsg);

            CacheHelper.Clear("WebConfig"); //1
            CacheHelper.Clear("OprLogo");//2
            CacheHelper.Clear("AgencyLogo");//2            
            foreach (DictionaryEntry elem in HttpContext.Current.Cache)
            {
                string s = elem.Key.ToString();
                if (s.StartsWith("Package_") ||
                    s.StartsWith("OnlyHotel_") ||
                    s.StartsWith("Other_") ||
                    s.StartsWith("CultureTour_") ||
                    s.StartsWith("None_") ||
                    s.IndexOf("OnlyHotel_") > 0)
                {
                    CacheHelper.Clear(elem.ToString());
                }

                if (s.StartsWith("MainMenu_"))
                {
                    CacheHelper.Clear(elem.ToString());
                }
            }

            foreach (TvBo.MarketRecord rowAll in markets)
            {
                CacheHelper.Clear(string.Format("FlightDays_{0}", rowAll.Code));//4
                CacheHelper.Clear(string.Format("Location_{0}", rowAll.Code));//8
                CacheHelper.Clear(string.Format("Market_{0}", rowAll.Code));//9
                CacheHelper.Clear(string.Format("Operator_{0}", rowAll.Code));//10
                CacheHelper.Clear(string.Format("Nation_{0}", rowAll.Code));//11
            }

            CacheHelper.Clear("ResViewMenu");//6
            CacheHelper.Clear("MakeResMenu");//7            

            Response.StatusCode = 200;
            Response.Clear();
            Response.ClearContent();
            Response.Write("Cache cleared.");
            Response.ClearHeaders();
            Response.End();
            Response.Close();
        }
        else
        {
            var host = Request.Url.Host;
            var serverIP = Request.ServerVariables["LOCAL_ADDR"];
            object _sslControl = new TvBo.Common().getFormConfigValue("General", "UseSSL");
            bool useSSL = TvTools.Conversion.getBoolOrNull(_sslControl).HasValue ? TvTools.Conversion.getBoolOrNull(_sslControl).Value : false;
            //string absolutePath = VirtualPathUtility.ToAbsolute("~/");
            if (useSSL && HttpContext.Current.Request.IsSecureConnection.Equals(false) && HttpContext.Current.Request.IsLocal.Equals(false) && host != serverIP)
            {
                var path = string.Format("https://{0}{1}", Request.ServerVariables["HTTP_HOST"], HttpContext.Current.Request.RawUrl);
                RedirectToUrl(path);
            }
        }
    }
    protected void RedirectToUrl(string redirectUrl)
    {
        Response.Status = "301 Moved Permanently";
        Response.StatusCode = 301;
        Response.AddHeader("Location", "");
        Response.Redirect(redirectUrl);
    }

    protected void Application_EndRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_Error(Object sender, EventArgs e)
    {

    }

    protected void Session_End(Object sender, EventArgs e)
    {
        try
        {
            Application.Lock();
            GlobalRecord globalRec = (GlobalRecord)Application["globalRec"];

            if (globalRec.SessionRec.Find(f => f.SessionID == Session.SessionID.ToString()) != null)
            {
                SessionRecord session = globalRec.SessionRec.Find(f => f.SessionID == Session.SessionID.ToString());
                globalRec.SessionRec.Remove(session);
                Application["globalRec"] = globalRec;
            }
            Application.UnLock();
        }
        catch
        {
            Application.UnLock();
        }

        try
        {
            string errorMsg = string.Empty;
            new TvBo.Users().setLogoutUser(Session.SessionID, false, ref errorMsg);
            string sessionID = Session.SessionID;
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Server.MapPath("Cache"));
            System.IO.FileInfo[] files = dir.GetFiles("*." + sessionID);
            foreach (System.IO.FileInfo file in files) file.Delete();
        }
        catch { }
    }

    protected void Application_End(Object sender, EventArgs e)
    {
        try
        {
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Server.MapPath("Cache"));
            System.IO.FileInfo[] files = dir.GetFiles("*.*");
            foreach (System.IO.FileInfo file in files) file.Delete();

            dir = new System.IO.DirectoryInfo(Server.MapPath("ACE"));
            files = dir.GetFiles("*.*");
            foreach (System.IO.FileInfo file in files) file.Delete();

            dir = new System.IO.DirectoryInfo(Server.MapPath("LogoCache"));
            files = dir.GetFiles("*.*");
            foreach (System.IO.FileInfo file in files) file.Delete();
        }
        catch { }
    }

    #region Web Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
    }
    #endregion
}
