﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using TvBo;

/// <summary>
/// Summary description for PaymentFinal
/// </summary>
public class PaymentFinal
{
    public PaymentFinal()
    {
        //
        // TODO: Add constructor logic here
        //

    }
    public static bool Finalize(string pTransactionId,string pResNo,string pPaymentType,string pAmount, ref string errMsg)
    {
        string isPayLog = ConfigurationManager.AppSettings["PaymentLogIsActive"];
        bool paymentLog = false;
        if (!string.IsNullOrEmpty(isPayLog) && isPayLog.Equals("1"))
            paymentLog = true;
        if (paymentLog)
            WebLog.SaveWebServiceLog(null, null, "PaymentFinal-27-ResNo:" + pResNo, "PaymentFinal", null, null);
        decimal? amount;
        string amountStr = !string.IsNullOrEmpty(pAmount) ? pAmount : "";
        if (amountStr.IndexOf('.') > 0)
        {
            amountStr = amountStr.Remove(amountStr.IndexOf('.'), 1);
            amount = TvTools.Conversion.getDecimalOrNull(amountStr) / 100;
        }
        else if (amountStr.IndexOf(',') > 0)
        {
            amountStr = amountStr.Remove(amountStr.IndexOf(','), 1);
            amount = TvTools.Conversion.getDecimalOrNull(amountStr) / 100;
        }
        else amount = TvTools.Conversion.getDecimalOrNull(amountStr);
        if (paymentLog)
            WebLog.SaveWebServiceLog(null, null, "PaymentFinal-42-ResNo:" + pResNo, "PaymentFinal", null, null);
        TvBo.ReservationPaymentInfo resPaymentInfo = new TvBo.Reservation().getReservationPaymentInfo(pResNo, ref errMsg);
        if (paymentLog && resPaymentInfo != null)
            WebLog.SaveWebServiceLog(null, null, "PaymentFinal-45-ResNo:" + pResNo, "PaymentFinal", Newtonsoft.Json.JsonConvert.SerializeObject(resPaymentInfo), null);

        List<TvBo.PayTypeRecord> paymentTypes = new TvBo.Common().getPayType(resPaymentInfo.Market, ref errMsg);
        if (paymentLog && paymentTypes != null)
            WebLog.SaveWebServiceLog(null, null, "PaymentFinal-45-ResNo:" + pResNo, "PaymentFinal", Newtonsoft.Json.JsonConvert.SerializeObject(paymentTypes), null);
        TvBo.PayTypeRecord paymentType = paymentTypes.Find(f => f.Code == pPaymentType && f.Market == resPaymentInfo.Market);
        if (paymentLog && paymentType != null)
            WebLog.SaveWebServiceLog(null, null, "PaymentFinal-52-ResNo:" + pResNo, "PaymentFinal", Newtonsoft.Json.JsonConvert.SerializeObject(paymentType), null);
        TvBo.ResDataRecord ResData = new TvBo.ResTables().getReservationData(null, pResNo, string.Empty, ref errMsg);
        if (paymentLog && ResData != null)
            WebLog.SaveWebServiceLog(null, null, "PaymentFinal-55-ResNo:" + pResNo, "PaymentFinal", null, null);

        bool paymentOk = false;
        if (resPaymentInfo != null && paymentType != null && ResData!=null)
        {
                TvBo.ResCustRecord leader = ResData.ResCust.Find(f => f.CustNo == resPaymentInfo.CustNo);
                if (leader == null) leader = ResData.ResCust.Find(f => f.Leader == "Y");

                string LeaderName = leader.Surname + " " + leader.Name;
                Int16 accountType = new Payments().getAccountTypeForPayment(ResData.ResMain.ResNo, ref errMsg);
            if (paymentLog)
                WebLog.SaveWebServiceLog(null, null, "PaymentFinal-66-ResNo:" + pResNo, "PaymentFinal", accountType.ToString(), errMsg);
                    
                //var checkPayment = new TvBo.Reservation().IsExistsPayByReference(posReferance,amount, ref errorMsg);
                //if (!checkPayment)
                paymentOk = new TvBo.Reservation().MakeResPayment(resPaymentInfo.Agency, 
                    accountType, 
                    accountType == 0 ? resPaymentInfo.Agency : resPaymentInfo.CustNo.ToString(), 
                    accountType == 0 ? resPaymentInfo.Agency : LeaderName, DateTime.Today, 
                    paymentType.RecID,
                    amount, 
                    resPaymentInfo.SaleCur, 
                    "",
                    -1, "", "",
                    null, null, null, 0, pTransactionId, pResNo, "", ref errMsg);
                if (paymentOk)
                    new Payments().closeCheckPayment(pResNo, pTransactionId, ref errMsg);
            if (paymentLog)
                WebLog.SaveWebServiceLog(null, null, "PaymentFinal-83-ResNo:" + pResNo, "PaymentFinal", paymentOk.ToString(), errMsg);
        }
        return paymentOk;
    }
}