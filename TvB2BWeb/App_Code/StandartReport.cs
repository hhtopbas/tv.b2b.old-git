﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using TvReport.SunReport;
using System.Text;
using TvTools;
using System.IO;
using System.Threading;

public class StandartReport
{   
    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var _customersTicket = from H in ResData.ResService
                               join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                               join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                               join T in ResData.Title on C.Title equals T.TitleNo
                               join F in flights on H.Service equals F.Code
                               join A in airLines on F.Airline equals A.Code
                               where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                               select new
                               {
                                   CustNo = C.CustNo,
                                   Name = C.Name,
                                   SurName = C.Surname,
                                   TitleName = T.Code,
                                   ServiceID = H.RecID,
                                   DocStat = Rc.DocStat,
                                   DocNo = Rc.DocNo
                               };
        var customersTicket = from q in _customersTicket
                              group q by new
                              {
                                  CustNo = q.CustNo,
                                  Name = q.Name,
                                  SurName = q.SurName,
                                  TitleName = q.TitleName
                              } into k
                              select new
                              {
                                  k.Key.CustNo,
                                  k.Key.Name,
                                  k.Key.SurName,
                                  k.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;
            var custTicket = _customersTicket.Where(w => w.CustNo == row.CustNo).GroupBy(g => g.DocNo);

            Int16? DocStat = custTicket.Count() > 1 ?
                Convert.ToInt16(_customersTicket.Where(w => w.CustNo == row.CustNo && w.DocStat.HasValue && w.DocStat.Value == 1).GroupBy(g => g.DocNo).Count() > 0 ? 1 : 0) :
                _customersTicket.Where(w => w.CustNo == row.CustNo).FirstOrDefault().DocStat;
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        List<ResConRecord> resConList = ResData.ResCon;
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                ++cnt;
                writeList.Clear();
                List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                              join q3 in flyList on q1.Service equals q3.FlightNo
                                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                              select q1).ToList<ResServiceRecord>();


                AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);
                string agencyName_airlineName = UserData.AgencyName + " / " + (airLine != null ? airLine.Name : "");
                writeList.Add(new TvReport.Coordinate { value = agencyName_airlineName, x = 30, y = 700, FontSize = 8 });

                string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());

                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToString("dd.MM.yyyy"), x = 490, y = 736, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custFlights.FirstOrDefault().ResNo, x = 490, y = 725, FontSize = 8 });
                if (!string.Equals(row1.airline, "AZAL"))
                    writeList.Add(new TvReport.Coordinate { value = eTicketNo, x = 490, y = 714, FontSize = 8 });

                writeList.Add(new TvReport.Coordinate { value = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name, x = 35, y = 652, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resCust.Birtday.HasValue ? resCust.Birtday.Value.ToShortDateString() : "", x = 308, y = 652, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resCust.PassSerie + " " + resCust.PassNo, x = 460, y = 652, FontSize = 8 });

                float _Y = 600;
                foreach (ResServiceRecord row in custAirleineFlights.OrderBy(o => o.BegDate))
                {
                    FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);

                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToString("dd.MM.yyyy"), x = 25, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.Service, x = 98, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 168, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 300, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToString("HH:mm") : "", x = 432, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 476, y = _Y, FontSize = 8 });
                    string confStat = string.Empty;
                    switch (row.StatConf)
                    {
                        case 0: confStat = resCust.Title < 8 ? "Request" : "NS"; break;
                        case 1: confStat = resCust.Title < 8 ? "OK" : "NS"; break;
                        case 2: confStat = resCust.Title < 8 ? "Not Confirm" : "NS"; break;
                        case 3: confStat = resCust.Title < 8 ? "No Show" : "NS"; break;
                    }
                    writeList.Add(new TvReport.Coordinate { value = confStat, x = 504, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString("#,###") + "K" : "", x = 540, y = _Y, FontSize = 8 });

                    _Y -= 19;
                    if (new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg))
                    {
                        ResConRecord resCon = resConList.Find(f => f.ResNo == row.ResNo && f.ServiceID == row.RecID && f.CustNo == CustNo);
                        resCon.DocNo = eTicketNo;
                        resCon.DocStat = 1;
                        resCon.DocIsDate = DateTime.Now;
                    }
                }

                string ServiceCode = "ETicket";
                eTicketList.Add(new TvReport.MahbalReport.MahbalReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
            }
            HttpContext.Current.Session["ResData"] = ResData;
            if (eTicketList.Count() == 1)
                return eTicketList.FirstOrDefault();
            else return new TvReport.MahbalReport.MahbalReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);
        }
        else return "";
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, resService.Service, ref errorMsg);

        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;
        float _y = 0f;
        int cnt = 0;

        switch (cnt)
        {
            case 0: _y = 0f; break;
            case 1: _y = 271f; break;
            case 2: _y = 540f; break;
        }
        if (opr != null)
        {
            writeList.Add(new TvReport.Coordinate { value = opr.Name.ToString(), x = 20, y = 814 - _y, FontSize = 12, bold = true });
            writeList.Add(new TvReport.Coordinate { value = opr.Address.ToString(), x = 20, y = 798 - _y, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = opr.AddrZip.ToString() + " " + opr.AddrCity.ToString() + " " + opr.AddrCountry.ToString(), x = 20, y = 787 - _y, FontSize = 7 });
        }
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 474, y = 800 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 479, y = 786 - _y, FontSize = 8 });

        float _Y = 767;
        float _X = 17;
        int i = 0;
        foreach (var row in custs)
        {
            _Y -= 11; i++;
            writeList.Add(new TvReport.Coordinate { value = row.TitleStr + ". " + row.Surname + " " + row.Name + " " + (row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : ""), x = _X, y = _Y - _y, FontSize = 8 });
        }

        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 237, y = 765 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, resService.DepLocation.Value, ref errorMsg).ToString(), x = 245, y = 742 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = hotel.LocationName, x = 448, y = 742 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString(), x = 248, y = 719 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.EndDate.Value.ToShortDateString(), x = 410, y = 719 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Duration.Value.ToString(), x = 532, y = 719 - _y, FontSize = 8 });

        string accomFullName = resService.Adult.HasValue && resService.Adult.Value > 0 ? resService.Adult.ToString() + " Adl" : "";
        accomFullName += resService.Child.HasValue && resService.Child.Value > 0 ? resService.Child.ToString() + " Chd" : "";
        writeList.Add(new TvReport.Coordinate { value = resService.Unit.ToString() + " " + resService.AccomName + "( " + accomFullName + ") - " + resService.RoomName, x = 245, y = 697 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Board, x = 529, y = 697 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = new TvReport.AzurReport.AzurReport().getTransferStr(ResData.ResMain.ResNo, ref errorMsg), x = 246, y = 676 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNote.ToString(), x = 233, y = 658 - _y, FontSize = 8 });
        List<HotelHandicapRecord> hotelHandicaps = new Hotels().getHotelHandicaps(UserData.Market, ResData.ResMain.PLMarket, resService.Service, resService.BegDate, resService.EndDate, ref errorMsg);
        if (hotelHandicaps != null && hotelHandicaps.Count > 0)
            writeList.Add(new TvReport.Coordinate { value = string.IsNullOrEmpty(hotelHandicaps.FirstOrDefault().NameL) ? hotelHandicaps.FirstOrDefault().NameL : hotelHandicaps.FirstOrDefault().Name, x = 313, y = 636 - _y, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = resService.Adult.ToString(), x = 40, y = 600 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Child.ToString(), x = 126, y = 600 - _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = UserData.UserName, x = 248, y = 613 - _y, FontSize = 8 });

        return new TvReport.MahbalReport.MahbalReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

}




