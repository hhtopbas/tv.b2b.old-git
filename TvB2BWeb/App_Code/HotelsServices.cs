﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;

/// <summary>
/// Summary description for Hotels
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class HotelsServices : System.Web.Services.WebService
{
    public HotelsServices()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod(EnableSession = true)]
    public string getRoomConsept(string Market, string Hotel, string Room, long Date)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retval = string.Empty;
        retval = new TvBo.Hotels().getRoomConseptInfo(Market, Hotel, Room, DateTime.MinValue.AddTicks(Date), ref errorMsg);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public string getBoardConsept(string Market, string Hotel, string Board, long Date)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retval = string.Empty;
        retval = new TvBo.Hotels().getBoardConseptInfo(Market, Hotel, Board, DateTime.MinValue.AddTicks(Date), ref errorMsg);
        return retval;
    }
              
}

