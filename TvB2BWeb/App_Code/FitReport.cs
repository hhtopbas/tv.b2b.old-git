﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using System.Text;
using TvTools;

public class FitReport
{
    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var customersTicket = from H in ResData.ResService
                              join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                              join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                              join T in ResData.Title on C.Title equals T.TitleNo
                              join F in flights on H.Service equals F.Code
                              join A in airLines on F.Airline equals A.Code
                              where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                              group Rc by new
                              {
                                  CustNo = Rc.CustNo,
                                  Name = C.Name,
                                  SurName = C.Surname,
                                  TitleName = T.Code
                              } into g
                              select new
                              {
                                  CustNo = g.Key.CustNo,
                                  Name = g.Key.Name,
                                  SurName = g.Key.SurName,
                                  TitleName = g.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;

            Int16? DocStat = Conversion.getInt16OrNull(ResData.ResCon.Find(f => f.CustNo == row.CustNo).DocStat);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                ++cnt;
                writeList.Clear();
                List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                              join q3 in flyList on q1.Service equals q3.FlightNo
                                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                              select q1).ToList<ResServiceRecord>();


                AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);
                string agencyName_airlineName = UserData.AgencyName + " / " + (airLine != null ? airLine.Name : "");
                writeList.Add(new TvReport.Coordinate { value = agencyName_airlineName, x = 30, y = 700, FontSize = 8 });

                string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());

                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToString("dd.MM.yyyy"), x = 490, y = 736, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custFlights.FirstOrDefault().ResNo, x = 490, y = 725, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = eTicketNo, x = 490, y = 714, FontSize = 8 });

                writeList.Add(new TvReport.Coordinate { value = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name, x = 35, y = 652, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resCust.Birtday.HasValue ? resCust.Birtday.Value.ToShortDateString() : "", x = 308, y = 652, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resCust.PassSerie + " " + resCust.PassNo, x = 460, y = 652, FontSize = 8 });

                float _Y = 600;
                foreach (ResServiceRecord row in custAirleineFlights)
                {
                    FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);

                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToString("dd.MM.yyyy"), x = 25, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.Service, x = 98, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 168, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 300, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToString("HH:mm") : "", x = 432, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 476, y = _Y, FontSize = 8 });
                    string confStat = string.Empty;
                    switch (row.StatConf)
                    {
                        case 0: confStat = resCust.Title < 8 ? "Request" : "NS"; break;
                        case 1: confStat = resCust.Title < 8 ? "OK" : "NS"; break;
                        case 2: confStat = resCust.Title < 8 ? "Not Confirm" : "NS"; break;
                        case 3: confStat = resCust.Title < 8 ? "No Show" : "NS"; break;
                    }
                    writeList.Add(new TvReport.Coordinate { value = confStat, x = 504, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString("#,###") : "", x = 540, y = _Y, FontSize = 8 });

                    _Y -= 19;
                    new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg);
                }

                string ServiceCode = "ETicket_" + row1.airline;
                eTicketList.Add(new TvReport.FitReport.FitReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
            }
            if (eTicketList.Count() == 1)
                return eTicketList.FirstOrDefault();
            else return new TvReport.FitReport.FitReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);
        }
        else return "";
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 200f, y = 275, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 200f, y = 275, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 200f, y = 275 - 138.2f, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 200f, y = 271f, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 200f, y = 271f, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 200f, y = 271f - 138.2f, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = 1 });
        float _Y = 261.25f;
        float _X = 10;
        int i = 0;
        foreach (var row in custs)
        {
            _Y -= 3.25f; i++;
            writeList.Add(new TvReport.Coordinate { value = i.ToString() + ". " + row.TitleStr + ". " + row.Surname + " " + row.Name, x = _X, y = _Y, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = i.ToString() + ". " + row.TitleStr + ". " + row.Surname + " " + row.Name, x = _X, y = _Y, FontSize = 8, PageNo = 2 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 130f, y = _Y, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 130f, y = _Y, FontSize = 8, PageNo = 2 });
            writeList.Add(new TvReport.Coordinate { value = i.ToString() + ". " + row.TitleStr + ". " + row.Surname + " " + row.Name, x = _X, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 130f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
        }
        _Y = 236f;
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.BegDate.Value.ToShortDateString(), x = 32f, y = _Y, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.BegDate.Value.ToShortDateString(), x = 32f, y = _Y, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.EndDate.Value.ToShortDateString(), x = 106f, y = _Y, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.EndDate.Value.ToShortDateString(), x = 106f, y = _Y, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Days.Value.ToString(), x = 168f, y = _Y, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Days.Value.ToString(), x = 168f, y = _Y, FontSize = 8, PageNo = 2 });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.BegDate.Value.ToShortDateString(), x = 32f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.EndDate.Value.ToShortDateString(), x = 106f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Days.Value.ToString(), x = 168f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });

        _Y = 224.5f;
        writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg), x = 26f, y = _Y, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg), x = 26f, y = _Y, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityNameL, x = 100f, y = _Y, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityNameL, x = 100f, y = _Y, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationNameL, x = 154f, y = _Y, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationNameL, x = 154f, y = _Y, FontSize = 8, PageNo = 2 });

        writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg), x = 26f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityNameL, x = 100f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationNameL, x = 154f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });

        _Y = 220f;
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceNameL, x = 26f, y = _Y, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceNameL, x = 26f, y = _Y, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceNameL, x = 26f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });

        writeList.Add(new TvReport.Coordinate { value = resService.AccomNameL + " " + resService.RoomNameL, x = 154f, y = _Y, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.AccomNameL + " " + resService.RoomNameL, x = 154f, y = _Y, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = resService.AccomNameL + " " + resService.RoomNameL, x = 154f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });

        _Y = 215.5f;
        writeList.Add(new TvReport.Coordinate { value = resService.BoardNameL, x = 26f, y = _Y, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.BoardNameL, x = 26f, y = _Y, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = resService.BoardNameL, x = 26f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });

        _Y = 203.75f;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "TRANSFER").Select(s => s).ToList<ResServiceRecord>())
        {
            writeList.Add(new TvReport.Coordinate { value = row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "", x = 10f, y = _Y, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "", x = 10f, y = _Y, FontSize = 8, PageNo = 2 });
            writeList.Add(new TvReport.Coordinate { value = row.Adult.ToString() + " Adult", x = 31f, y = _Y, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.Adult.ToString() + " Adult", x = 31f, y = _Y, FontSize = 8, PageNo = 2 });
            writeList.Add(new TvReport.Coordinate { value = row.Adult.HasValue && row.Adult.Value > 0 ? (row.Child.HasValue && row.Child.Value > 0 ? row.Child.Value + " Child" : "") : "", x = 46f, y = _Y, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.Adult.HasValue && row.Adult.Value > 0 ? (row.Child.HasValue && row.Child.Value > 0 ? row.Child.Value + " Child" : "") : "", x = 46f, y = _Y, FontSize = 8, PageNo = 2 });
            writeList.Add(new TvReport.Coordinate { value = row.ServiceName + " (" + row.DepLocationNameL + " - " + row.ArrLocationNameL + ")", x = 60f, y = _Y, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.ServiceName + " (" + row.DepLocationNameL + " - " + row.ArrLocationNameL + ")", x = 60f, y = _Y, FontSize = 8, PageNo = 2 });

            writeList.Add(new TvReport.Coordinate { value = row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "", x = 10f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.Adult.ToString() + " Adult", x = 31f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.Adult.HasValue && row.Adult.Value > 0 ? (row.Child.HasValue && row.Child.Value > 0 ? row.Child.Value + " Child" : "") : "", x = 46f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.ServiceName + " (" + row.DepLocationNameL + " - " + row.ArrLocationNameL + ")", x = 60f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
            _Y -= 3.25f;
        }

        _Y = 188f; _X = 10f;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "EXCURSION").Select(s => s).ToList<ResServiceRecord>())
        {
            string lineStr = row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "";
            lineStr += " " + row.Adult.ToString() + " Adult";
            lineStr += " " + (row.Adult.HasValue && row.Adult.Value > 0 ? (row.Child.HasValue && row.Child.Value > 0 ? row.Child.Value + " Child" : "") : "");
            lineStr += " " + row.ServiceName;
            writeList.Add(new TvReport.Coordinate { value = lineStr, x = 10f, y = _Y, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = lineStr, x = 10f, y = _Y, FontSize = 8, PageNo = 2 });
            writeList.Add(new TvReport.Coordinate { value = lineStr, x = 10f, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
            _Y -= 3.25f;
        }
        _Y = 188f; _X = 87f;
        foreach (ResServiceExtRecord row in ResData.ResServiceExt.Where(w => w.ServiceID == ServiceID))
        {
            writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString() + " " + row.ExtServiceName, x = _X, y = _Y, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString() + " " + row.ExtServiceName, x = _X, y = _Y, FontSize = 8, PageNo = 2 });
            writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString() + " " + row.ExtServiceName, x = _X, y = _Y - 138.2f, FontSize = 8, PageNo = 1 });
            _Y -= 3.25f;
        }

        writeList.Add(new TvReport.Coordinate { value = resService.SupNote, x = 19f, y = 171f, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.SupNote, x = 19f, y = 171f, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = resService.SupNote, x = 19f, y = 171f - 138.2f, FontSize = 8, PageNo = 1 });

        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyName, x = 26f, y = 164.8f, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyName, x = 26f, y = 164.8f, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyRec.Phone1, x = 19f, y = 160.25f, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyRec.Phone1, x = 19f, y = 160.25f, FontSize = 8, PageNo = 2 });

        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyName, x = 26f, y = 164.8f - 138.2f, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyRec.Phone1, x = 19f, y = 160.25f - 138.2f, FontSize = 8, PageNo = 1 });

        writeList.Add(new TvReport.Coordinate { value = resService.SupplierNameL, x = 127f, y = 169.25f, FontSize = 8, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.SupplierNameL, x = 127f, y = 169.25f, FontSize = 8, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = resService.SupplierNameL, x = 127f, y = 169.25f - 138.2f, FontSize = 8, PageNo = 1 });
        SupplierRecord supplier = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
        if (supplier != null)
        {
            writeList.Add(new TvReport.Coordinate { value = supplier.Phone1, x = 113.75f, y = 164.75f, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = supplier.Phone1, x = 113.75f, y = 164.75f, FontSize = 8, PageNo = 2 });
            writeList.Add(new TvReport.Coordinate { value = supplier.Fax1, x = 108.75f, y = 159.68f, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = supplier.Fax1, x = 108.75f, y = 159.68f, FontSize = 8, PageNo = 2 });

            writeList.Add(new TvReport.Coordinate { value = supplier.Phone1, x = 113.75f, y = 164.75f - 138.2f, FontSize = 8, PageNo = 1 });
            writeList.Add(new TvReport.Coordinate { value = supplier.Fax1, x = 108.75f, y = 159.68f - 138.2f, FontSize = 8, PageNo = 1 });
        }

        return new TvReport.FitReport.FitReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showInvoice(User UserData, ResDataRecord ResData, ref string errorMsg)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        BankRecord oprBank = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);
        BankRecord agencyBank = new Banks().getBank(UserData.Market, agency.Bank1, ref errorMsg);
        ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
        if (leader == null)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError").ToString();
            return "";
        }
        ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
        if (leaderInfo == null)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError").ToString();
            return "";
        }

        writeList.Add(new TvReport.Coordinate { value = leader.TitleStr + ". " + leader.Surname + " " + leader.Name, x = 46, y = 685, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = !Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHome : leaderInfo.AddrWork, x = 46, y = 665, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = !Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHomeZip + " " + leaderInfo.AddrHomeCity : leaderInfo.AddrWorkZip + " " + leaderInfo.AddrWorkCountry, x = 46, y = 645, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = !Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry, x = 46, y = 625, FontSize = 10 });

        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 530, y = 685, FontSize = 10, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 530, y = 665, FontSize = 10, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = !string.IsNullOrEmpty(leaderInfo.MobTel) ? leaderInfo.MobTel : (!Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry), x = 530, y = 645, FontSize = 10, Align = TvReport.fontAlign.Right });

        writeList.Add(new TvReport.Coordinate { value = "Reservation Book of Number : " + ResData.ResMain.ResNo, x = 100, y = 600, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20, y = 595, H = 0, W = 570, LineWidth = Convert.ToSingle(25 / 100.0) });

        Single Y = 585;

        writeList.Add(new TvReport.Coordinate { value = "From", x = 302, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        writeList.Add(new TvReport.Coordinate { value = "To", x = 381, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        writeList.Add(new TvReport.Coordinate { value = "Night", x = 447, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        writeList.Add(new TvReport.Coordinate { value = "Board", x = 519, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        //Y = 545;
        var query = from q in ResData.ResService
                    where q.ServiceType == "HOTEL"
                    group q by new
                    {
                        HotelName = q.ServiceNameL,
                        RoomName = q.RoomNameL,
                        BoardName = q.BoardNameL,
                        FromDate = q.BegDate,
                        ToDate = q.EndDate,
                        Night = q.Duration,
                        LocationName = q.DepLocationNameL,
                        Location = q.DepLocation
                    } into k
                    select new { k.Key.BoardName, k.Key.FromDate, k.Key.HotelName, k.Key.Location, k.Key.LocationName, k.Key.Night, k.Key.RoomName, k.Key.ToDate };
        //+", "+new Locations().getLocationForCountryName(UserData.Market, q.DepLocation, ref errorMsg).ToString()
        foreach (var row in query)
        {
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = "Hotel:", x = 46, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.HotelName, x = 96, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.FromDate.HasValue ? row.FromDate.Value.ToShortDateString() : "", x = 302, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
            writeList.Add(new TvReport.Coordinate { value = row.ToDate.HasValue ? row.ToDate.Value.ToShortDateString() : "", x = 381, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
            writeList.Add(new TvReport.Coordinate { value = row.Night.ToString(), x = 447, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
            writeList.Add(new TvReport.Coordinate { value = row.BoardName, x = 519, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = "Room type:", x = 46, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.RoomName, x = 96, y = Y, FontSize = 8 });
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = "Location:", x = 46, y = Y, FontSize = 8 });
            string locationCountry = new Locations().getLocationForCountryName(UserData, row.Location.Value, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = row.LocationName + " " + locationCountry, x = 96, y = Y, FontSize = 8 });
        }
        Y -= 3;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20, y = Y, H = 0, W = 570, LineWidth = Convert.ToSingle(25 / 100.0) });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "Passenger(s)", x = 67, y = Y, FontSize = 8, bold = true, Underline = true });
        writeList.Add(new TvReport.Coordinate { value = "Birth Date", x = 194, y = Y, FontSize = 8, bold = true, Underline = true });
        foreach (ResCustRecord row in ResData.ResCust.Where(w => !(w.Status.HasValue && w.Status.Value == 1)))
        {
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = row.TitleStr + " " + row.Surname + " " + row.Name, x = 46, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 194, y = Y, FontSize = 8 });
        }
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "Adult", x = 67, y = Y, FontSize = 8, Underline = true });
        writeList.Add(new TvReport.Coordinate { value = "Child", x = 106, y = Y, FontSize = 8, Underline = true });
        var query1 = from q in ResData.ResService
                     where q.IncPack == "Y"
                     select new { SalePrice = q.SalePrice, SaleCur = q.SaleCur, Desc = "Package Total" };

        var query2 = from q in ResData.ResService
                     where q.IncPack != "Y"
                     select new { Adult = q.Adult, Child = q.Child, SalePrice = q.SalePrice, SaleCur = q.SaleCur, Desc = q.ServiceNameL };
        var query3 = from q in ResData.ResServiceExt
                     where q.IncPack != "Y"
                     select new { Adult = q.Adult, Child = q.Child, SalePrice = q.SalePrice, SaleCur = q.SaleCur, Desc = q.ExtServiceNameL };
        if (query1 != null && query1.Count() > 0)
        {
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Adult.ToString(), x = 78, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Child.ToString(), x = 117, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = query1.Sum(s => s.SalePrice).HasValue ? query1.Sum(s => s.SalePrice).Value.ToString("#,###.00") : "", x = 208, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = query1.FirstOrDefault().SaleCur, x = 217, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = query1.FirstOrDefault().Desc, x = 251, y = Y, FontSize = 8 });
        }
        if (query2 != null && query2.Count() > 0)
        {
            Y -= 2;
            foreach (var row in query2)
            {
                Y -= 12;
                writeList.Add(new TvReport.Coordinate { value = row.Adult.ToString(), x = 78, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Child.ToString(), x = 117, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "", x = 208, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
                writeList.Add(new TvReport.Coordinate { value = row.SaleCur, x = 217, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Desc, x = 251, y = Y, FontSize = 8 });
            }
            Y -= 12;
        }
        if (query3 != null && query3.Count() > 0)
        {
            Y -= 2;
            foreach (var row in query3)
            {
                Y -= 12;
                writeList.Add(new TvReport.Coordinate { value = row.Adult.ToString(), x = 78, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Child.ToString(), x = 117, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "", x = 208, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
                writeList.Add(new TvReport.Coordinate { value = row.SaleCur, x = 217, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Desc, x = 251, y = Y, FontSize = 8 });
            }
        }
        Y -= 20;
        writeList.Add(new TvReport.Coordinate { value = "Payment must be done until :", x = 46, y = Y, FontSize = 7, bold = true, Italic = true, Underline = true });
        List<resPayPlanRecord> resPayPlan = new ReservationMonitor().getPaymentPlan(ResData.ResMain.ResNo, ref errorMsg);
        if (resPayPlan != null)
        {
            foreach (resPayPlanRecord row in resPayPlan)
            {
                Y -= 12;
                writeList.Add(new TvReport.Coordinate { value = row.Amount.HasValue ? row.Amount.Value.ToString("#,###.00") : "", x = 208, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
                writeList.Add(new TvReport.Coordinate { value = row.Cur, x = 217, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.DueDate.ToShortDateString(), x = 251, y = Y, FontSize = 8 });
            }
        }

        ResMainRecord resMain = ResData.ResMain;

        decimal totSupDisEB = (resMain.SalePrice.HasValue ? resMain.SalePrice.Value : 0) - (resMain.InvoicedAmount.HasValue ? resMain.InvoicedAmount.Value : 0) - (resMain.AgencyPayable.HasValue ? resMain.AgencyPayable.Value : 0) - (resMain.AgencyCom.HasValue ? resMain.AgencyCom.Value : 0);
        if (totSupDisEB > 0)
        {
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = totSupDisEB.ToString("#,###.00"), x = 208, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = resMain.SaleCur, x = 217, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Supplement / Discount", x = 251, y = Y, FontSize = 8 });
        }
        Y = 157;
        writeList.Add(new TvReport.Coordinate { value = "Total :", x = 467, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.PasAmount.HasValue ? ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 555, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        Y -= 11;
        decimal agencyCom = ResData.ResMain.AgencyCom.HasValue ? ResData.ResMain.AgencyCom.Value : Convert.ToDecimal(0.0);
        if (agencyCom > 0)
        {
            writeList.Add(new TvReport.Coordinate { value = "Commission :", x = 467, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = agencyCom.ToString("#,###.00") + " " + ResData.ResMain.SaleCur, x = 555, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        }
        Y -= 11;
        writeList.Add(new TvReport.Coordinate { value = "Amount To Pay :", x = 467, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 555, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        Y = 100;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20, y = Y, H = 0, W = 570, LineWidth = Convert.ToSingle(25 / 100.0) });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = opr.FirmName, x = 314, y = Y, FontSize = 10, bold = true, Align = TvReport.fontAlign.Center });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "Web: " + opr.Www + "  Email: " + opr.Email1, x = 314, y = Y, FontSize = 10, Align = TvReport.fontAlign.Center });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = opr.Address + " " + opr.AddrZip + " " + opr.AddrCity + " " + opr.AddrCountry, x = 314, y = Y, FontSize = 10, Align = TvReport.fontAlign.Center });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "Phone: " + opr.Phone1 + " , Fax: " + opr.Fax1, x = 314, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "VAT " + opr.TaxOffice + " | Trade Register " + opr.TaxAccNo, x = 314, y = Y, FontSize = 10, Align = TvReport.fontAlign.Center });

        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20, y = 20, H = 0, W = 570, LineWidth = Convert.ToSingle(25 / 100.0) });

        return new TvReport.FitReport.FitReport().createInvoice(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }
}

