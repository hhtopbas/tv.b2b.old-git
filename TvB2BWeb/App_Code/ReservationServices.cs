﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using TvBo;

/// <summary>
/// Summary description for ReservationServices
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ReservationServices : System.Web.Services.WebService
{

    public ReservationServices()
    {
    }

    [WebMethod]
    public Int16 CreditControl(string AgencyID, string Market)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return 0; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int16 retval = 0;
        retval = new TvBo.Reservation().CreditControl(AgencyID, Market, ref errorMsg);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public string getResService(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)TvBo.Common.DeepClone(Session["ResData"]);
        TvBo.ResServiceRecord ResService = ResData.ResService.Find(f => f.RecID == Convert.ToInt32(RecID));
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
        {
            ResService.StatConfName = string.Empty;
            ResService.StatConfNameL = string.Empty;
        }

        TvBo.onlyText_ResServiceRecord resService = new onlyText_ResServiceRecord();
        resService.Accom = ResService.Accom;
        resService.AccomName = ResService.AccomName;
        resService.AccomNameL = useLocalName ? ResService.AccomNameL : ResService.AccomName;
        resService.Adult = ResService.Adult.ToString();
        resService.ArrLocationName = ResService.ArrLocationName;
        resService.ArrLocationNameL = useLocalName ? ResService.ArrLocationNameL : ResService.ArrLocationName;
        resService.BegDate = ResService.BegDate.HasValue ? ResService.BegDate.Value.ToShortDateString() : "";
        resService.EndDate = ResService.EndDate.HasValue ? ResService.EndDate.Value.ToShortDateString() : "";
        resService.BoardName = ResService.BoardName;
        resService.BoardNameL = useLocalName ? ResService.BoardNameL : ResService.BoardName;
        resService.Bus = ResService.Bus;
        resService.BusName = ResService.BusName;
        resService.Child = ResService.Child.ToString();
        resService.DepLocationName = ResService.DepLocationName;
        resService.DepLocationNameL = useLocalName ? ResService.DepLocationNameL : ResService.DepLocationName;
        resService.Duration = ResService.Duration.ToString();
        resService.FlgClass = ResService.FlgClass;
        resService.IncPack = ResService.IncPack;
        resService.PickupName = ResService.PickupName;
        resService.PickupNameL = useLocalName ? ResService.PickupNameL : ResService.PickupName;
        resService.PickupNote = ResService.PickupNote;
        resService.RPickupNote = ResService.RPickupNote;
        resService.PickupTime = ResService.PickupTime.HasValue ? ResService.PickupTime.Value.ToString("HH:mm") : "";
        resService.RPickupTime = ResService.RPickupTime.HasValue ? ResService.RPickupTime.Value.ToString("HH:mm") : "";
        resService.PnrNo = ResService.PnrNo;
        resService.Room = ResService.Room;
        resService.RoomName = ResService.RoomName;
        resService.RoomNameL = useLocalName ? ResService.RoomNameL : ResService.RoomName;
        resService.SaleCur = ResService.SaleCur;
        resService.SalePrice = ResService.SalePrice.HasValue ? ResService.SalePrice.Value.ToString("#,###.00") : "";
        resService.Service = ResService.Service;
        resService.ServiceName = ResService.ServiceName;
        resService.ServiceNameL = useLocalName ? ResService.ServiceNameL : ResService.ServiceName;
        resService.ServiceType = ResService.ServiceType;
        resService.ServiceTypeName = ResService.ServiceTypeName;
        resService.ServiceTypeNameL = useLocalName ? ResService.ServiceTypeNameL : ResService.ServiceTypeName;
        resService.StatConfName = ResService.StatConfName;
        resService.StatConfNameL = useLocalName ? ResService.StatConfNameL : ResService.StatConfName;
        resService.StatSerName = ResService.StatSerName;
        resService.StatSerNameL = useLocalName ? ResService.StatSerNameL : ResService.StatSerName;
        resService.SupNote = ResService.SupNote;
        resService.Supplier = ResService.Supplier;
        resService.SupplierName = ResService.SupplierName;
        resService.SupplierNameL = useLocalName ? ResService.SupplierNameL : ResService.SupplierName;

        resService.TrfArrName = ResService.TrfArrName;
        resService.TrfArrNameL = useLocalName ? ResService.TrfArrNameL : ResService.TrfArrName;
        resService.TrfATName = ResService.TrfATName;
        resService.TrfDepName = ResService.TrfDepName;
        resService.TrfDepNameL = useLocalName ? ResService.TrfDepNameL : ResService.TrfDepName;
        resService.TrfDTName = ResService.TrfDTName;
        
        resService.RTrfArrName = ResService.RTrfArrName;
        resService.RTrfArrNameL = useLocalName ? ResService.RTrfArrNameL : ResService.RTrfArrName;
        resService.RTrfATName = ResService.RTrfATName;
        resService.RTrfDepName = ResService.RTrfDepName;
        resService.RTrfDepNameL = useLocalName ? ResService.RTrfDepNameL : ResService.RTrfDepName;
        resService.RTrfDTName = ResService.RTrfDTName;

        resService.Unit = ResService.Unit.ToString();

        string jSonResService = Newtonsoft.Json.JsonConvert.SerializeObject(resService);
        return jSonResService;
    }
}

