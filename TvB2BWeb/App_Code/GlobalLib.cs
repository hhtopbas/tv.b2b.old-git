﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GlobalLib
/// </summary>
public class GlobalLib
{
    public static string WriteBinaryImage(object _image, string Code, string H, string W)
    {
        if (_image != null && _image != Convert.DBNull)
        {
            byte[] image = (byte[])_image;
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(image))
            {
                System.Drawing.Bitmap bmp;
                bmp = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms);
                try
                {
                    if (H != "" && W != "")
                    {
                        bmp = ImageResize.FixedSize(bmp, Convert.ToInt32(W), Convert.ToInt32(H));
                    }
                    else
                        if (W == "" && H != "")
                        {
                            bmp = ImageResize.ConstrainProportions(bmp, Convert.ToInt32(H), Dimensions.Height);
                        }
                        else
                            if (H == "" && W != "")
                            {
                                bmp = ImageResize.ConstrainProportions(bmp, Convert.ToInt32(W), Dimensions.Width);
                            }

                }
                catch
                {
                }

                try
                {
                    if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "LogoCache\\" + Code + ".jpg"))
                        System.IO.File.Delete(AppDomain.CurrentDomain.BaseDirectory + "LogoCache\\" + Code + ".jpg");

                    bmp.Save(AppDomain.CurrentDomain.BaseDirectory + "LogoCache\\" + Code + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    return "LogoCache/" + Code + ".jpg";
                }
                catch (Exception Ex)
                {
                    string errorMsg = Ex.Message;
                    return "";
                }
            }
        }
        else
        {
            return "";
        }

    }

    public static List<OperatorLogoRecord> getOperatorLogo()
    {
        List<OperatorLogoRecord> oLogo = new List<OperatorLogoRecord>();
        string tsql = string.Empty;
        tsql = @"Select Code, Logo From Operator (NOLOCK) ";
        Microsoft.Practices.EnterpriseLibrary.Data.Database db = Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase() as Microsoft.Practices.EnterpriseLibrary.Data.Database;
        System.Data.Common.DbCommand dbCommand = db.GetSqlStringCommand(tsql);
        try
        {
            using (System.Data.IDataReader oReader = db.ExecuteReader(dbCommand))
            {
                while (oReader.Read())
                {
                    OperatorLogoRecord record = new OperatorLogoRecord();
                    record.Code = TvTools.Conversion.getStrOrNull(oReader["Code"]);
                    object logo = oReader["Logo"];
                    if (logo != null)
                        record.LogoPath = GlobalLib.WriteBinaryImage(logo, record.Code, "", "");
                    else record.LogoPath = string.Empty;
                    oLogo.Add(record);
                }
            }
            return oLogo;
        }
        catch
        {
            return oLogo;
        }
        finally
        {
            dbCommand.Connection.Close();
            dbCommand.Dispose();
        }
    }

    public static bool getPaxButtonImage()
    {
        object paxBtnImage = null;

        if (TvBo.VersionControl.getTableField("ParamSystem", "Pxm_Logo"))
        {
            string tsql = string.Empty;
            tsql = @"Select Pxm_Logo From ParamSystem (NOLOCK) Where Market=''";
            Microsoft.Practices.EnterpriseLibrary.Data.Database db = Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase() as Microsoft.Practices.EnterpriseLibrary.Data.Database;
            System.Data.Common.DbCommand dbCommand = db.GetSqlStringCommand(tsql);
            try
            {
                using (System.Data.IDataReader oReader = db.ExecuteReader(dbCommand))
                {
                    if (oReader.Read())                    
                        paxBtnImage = oReader["Pxm_Logo"];                    
                    else                    
                        paxBtnImage = null;                    
                }
            }
            catch
            {
                paxBtnImage = null; 
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Dispose();
            }
        }

        try
        {
            if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "LogoCache\\paximumBtn.gif"))
                System.IO.File.Delete(AppDomain.CurrentDomain.BaseDirectory + "LogoCache\\paximumBtn.gif");

            if (paxBtnImage != null)
            {
                byte[] btnImage = (byte[])paxBtnImage;
                System.IO.FileStream fs = System.IO.File.Create(AppDomain.CurrentDomain.BaseDirectory + "LogoCache\\paximumBtn.gif", 2048, System.IO.FileOptions.None);
                System.IO.BinaryWriter bw = new System.IO.BinaryWriter(fs);
                bw.Write(btnImage);
                bw.Close();
                fs.Close();
                return true;
            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }
    }

    public static string getOprLogoUrl(string Code)
    {
        OperatorLogoRecord oprLogoList = Global.globalRec.OprLogoUrl.Find(f => string.Equals(f.Code, Code));
        return oprLogoList != null ? oprLogoList.LogoPath : string.Empty;
    }

    public static List<dbTablesInformationShema> getTablesShema(ref string errorMsg)
    {
        List<dbTablesInformationShema> records = new List<dbTablesInformationShema>();
        string tsql = @"Select SeqNo=ORDINAL_POSITION,Name=COLUMN_NAME,DType=DATA_TYPE,TableName=TABLE_NAME
                        From information_schema.COLUMNS
                        ";
        Microsoft.Practices.EnterpriseLibrary.Data.Database db = (Microsoft.Practices.EnterpriseLibrary.Data.Database)Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
        System.Data.Common.DbCommand dbCommand = db.GetSqlStringCommand(tsql);
        try
        {
            using (System.Data.IDataReader rdr = db.ExecuteReader(dbCommand))
            {
                while (rdr.Read())
                {
                    records.Add(new dbTablesInformationShema
                    {
                        SeqNo = (int)rdr["SeqNo"],
                        Name = TvTools.Conversion.getStrOrNull(rdr["Name"]),
                        DType = TvTools.Conversion.getStrOrNull(rdr["DType"]),
                        TableName = TvTools.Conversion.getStrOrNull(rdr["TableName"])
                    });
                }
                return records;
            }
        }
        catch (Exception Ex)
        {
            errorMsg = Ex.Message;
            return null;
        }
        finally
        {
            dbCommand.Connection.Close();
            dbCommand.Dispose();
        }
    }

    public static List<dbSpsInformationShema> getSpsShema(ref string errorMsg)
    {
        List<dbSpsInformationShema> records = new List<dbSpsInformationShema>();
        string tsql = @"Select SeqNo=ORDINAL_POSITION,Name=PARAMETER_NAME,DType=DATA_TYPE,SpName=specific_name
                            From information_schema.PARAMETERS
                            ";
        Microsoft.Practices.EnterpriseLibrary.Data.Database db = (Microsoft.Practices.EnterpriseLibrary.Data.Database)Microsoft.Practices.EnterpriseLibrary.Data.DatabaseFactory.CreateDatabase();
        System.Data.Common.DbCommand dbCommand = db.GetSqlStringCommand(tsql);
        try
        {
            using (System.Data.IDataReader rdr = db.ExecuteReader(dbCommand))
            {
                while (rdr.Read())
                {
                    records.Add(new dbSpsInformationShema
                    {
                        SeqNo = (int)rdr["SeqNo"],
                        Name = TvTools.Conversion.getStrOrNull(rdr["Name"]),
                        DType = TvTools.Conversion.getStrOrNull(rdr["DType"]),
                        SpName = TvTools.Conversion.getStrOrNull(rdr["SpName"])
                    });
                }
                return records;
            }
        }
        catch (Exception Ex)
        {
            errorMsg = Ex.Message;
            return null;
        }
        finally
        {
            dbCommand.Connection.Close();
            dbCommand.Dispose();
        }
    }

    public bool getTableField(string tableName, string fieldName)
    {
        try
        {
            return Global.globalRec.tablesInformationShemaList.Where(w => string.Equals(w.TableName.ToLower(), tableName.ToLower()) && string.Equals(w.Name.ToLower(), fieldName.ToLower())).Count() > 0;
        }
        catch (Exception Ex)
        {
            string errorMsg = Ex.Message;
            return false;
        }
    }

    public bool getSpParam(string spName, string paramName)
    {
        try
        {
            return Global.globalRec.spsInformationShemaList.Where(w => string.Equals(w.SpName.ToLower(), spName.ToLower()) && string.Equals(w.Name.ToLower(), paramName.ToLower())).Count() > 0;
        }
        catch (Exception Ex)
        {
            string errorMsg = Ex.Message;
            return false;
        }
    }
}
