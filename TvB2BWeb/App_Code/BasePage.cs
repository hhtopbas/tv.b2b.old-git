using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Globalization;
using System.Threading;
using System.IO.Compression;

namespace TvBo
{
    [Serializable]
    public static class CompressDeCompress
    {

        public static byte[] Compress(byte[] data)
        {
            MemoryStream output = new MemoryStream();
            GZipStream gzip = new GZipStream(output, CompressionMode.Compress, true);
            gzip.Write(data, 0, data.Length);
            gzip.Close();
            return output.ToArray();
        }

        public static byte[] Decompress(byte[] data)
        {
            MemoryStream input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;
            GZipStream gzip = new GZipStream(input, CompressionMode.Decompress, true);
            MemoryStream output = new MemoryStream();
            byte[] buff = new byte[64];
            int read = -1;
            read = gzip.Read(buff, 0, buff.Length);
            while (read > 0)
            {
                output.Write(buff, 0, read);
                read = gzip.Read(buff, 0, buff.Length);
            }
            gzip.Close();
            return output.ToArray();
        }
    }

    public class BasePage : System.Web.UI.Page
    {
        //Setup the name of the hidden field on the client for storing the viewstate key
        public const string VIEW_STATE_FIELD_NAME = "__VIEWSTATE";

        //Setup a formatter for the viewstate information
        private LosFormatter _formatter = null;

        public BasePage()
        {
        }        

        override protected void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (Context.Session != null)
            {
                if (Session.IsNewSession)
                {
                    string szCookieHeader = Request.Headers["Cookie"];
                    if ((null != szCookieHeader) && (szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0))
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                }
            }
        }

        protected override void InitializeCulture()
        {
            base.InitializeCulture();

            if (Session["Culture"] != null)
            {
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo(((System.Globalization.CultureInfo)Session["Culture"]).Name, false);
                System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Equals(Request.Url.Segments[Request.Url.Segments.Length - 1], "UserLogin.aspx") && Session["UserData"] == null)
            {
                User UserData = (User)Session["UserData"];
                //if (Equals(Request.Url, ""))
                if (UICommon.IFramePage != null && Request.Url.Segments.Length > 0)
                {
                    if (UICommon.IFramePage.Contains(Request.Url.Segments[Request.Url.Segments.Length - 1]))
                    {
                        Response.Redirect("~/Default.aspx?EndSession=true&CloseIFrame=true");
                    }
                    else
                        Response.Redirect("~/Default.aspx?EndSession=true&CloseIFrame=false");
                }
                else Response.Redirect("~/Default.aspx?EndSession=true&CloseIFrame=false");
            }
            else
                if (Session["UserData"] != null)
                {
                    User UserData = (User)Session["UserData"];
                    if (!UserData.ExtUser)
                        if (Request.Url.ToString().IndexOf("ResViewExt.aspx") > 0)
                            if ((UICommon.IFramePage.Contains(Request.Url.Segments[Request.Url.Segments.Length - 1])))
                            {
                                Session["UserData"] = null;
                                Response.Redirect("~/Default.aspx");
                            }
                    #region GoogleAnalisticsScript
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && !string.IsNullOrEmpty(Request.Url.ToString()) &&
                        !(Request.Url.ToString().ToLower().Contains("BeginPayment.aspx") || 
                          Request.Url.ToString().ToLower().Contains("PaymentResultFi.aspx") ||
                          Request.Url.ToString().ToLower().Contains("noPaymentResultFi.aspx") ||
                          Request.Url.ToString().ToLower().Contains("ShowPaymentFinal.aspx") ||
                          Request.Url.ToString().ToLower().Contains("NetsReturn.aspx") ||
                          Request.Url.ToString().ToLower().Contains("NetsDkReturn.aspx")))
                    {
                        string _setAccount = string.Empty;
                        string _setDomainName = string.Empty;

                        if (Request.Url.ToString().ToLower().Contains("detur.fi"))
                        {
                            _setAccount = "UA-20994235-1";
                            _setDomainName = "agency.detur.fi";
                        }
                        else if (Request.Url.ToString().ToLower().Contains("detur.se"))
                        {
                            _setAccount = "UA-20994235-2";
                            _setDomainName = "agency.detur.se";
                        }
                        else if (Request.Url.ToString().ToLower().Contains("detur.no"))
                        {
                            _setAccount = "UA-20994235-3";
                            _setDomainName = "agency.detur.no";
                        }
                        else if (Request.Url.ToString().ToLower().Contains("detur.dk"))
                        {
                            _setAccount = "UA-20994235-4";
                            _setDomainName = "agency.detur.dk";
                        }
                  
                        if (!string.IsNullOrEmpty(_setAccount) && !string.IsNullOrEmpty(_setDomainName))
                        {
                            StringBuilder script = new StringBuilder();
                            script.AppendLine("<script type=\"text/javascript\">var _gaq = _gaq || [];");
                            script.AppendFormat("_gaq.push(['_setAccount', '{0}']);", _setAccount);
                            script.AppendFormat("_gaq.push(['_setDomainName', '{0}']);", _setDomainName);
                            script.AppendLine("_gaq.push(['_setAllowLinker', true]);");
                            script.AppendLine("_gaq.push(['_trackPageview']);");
                            script.AppendLine("(function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;");
                            script.AppendLine(" ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';");
                            script.AppendLine("var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);");
                            script.AppendLine("})();");
                            script.AppendLine("</script>");
                            this.ClientScript.RegisterStartupScript(this.GetType(), "GoogleAnalisticsScript", script.ToString());
                        }
                    }
                    #endregion
                }
            if (HttpContext.Current.Response.Cookies["BeginTime"] == null)
                HttpContext.Current.Response.Cookies.Add(new HttpCookie("BeginTime", DateTime.Now.ToString()));
            if (Session["BeginTime"] == null)
                Session["BeginTime"] = DateTime.Now;
        }

        //overriding method of Page class

        protected override object LoadPageStateFromPersistenceMedium()
        {
            //If server side enabled use it, otherwise use original base class implementation   

            if (true == viewStateSvrMgr.GetViewStateSvrMgr().ServerSideEnabled)
            {
                return LoadViewState();
            }
            else
            {
                return base.LoadPageStateFromPersistenceMedium();
            }
        }

        //overriding method of Page class        
        protected override void SavePageStateToPersistenceMedium(object viewState)
        {
            if (true == viewStateSvrMgr.GetViewStateSvrMgr().ServerSideEnabled)
            {
                SaveViewState(viewState);
            }
            else
            {
                base.SavePageStateToPersistenceMedium(viewState);
            }
        }

        //implementation of method
        private object LoadViewState()
        {

            if (_formatter == null)
            {
                _formatter = new LosFormatter();
            }

            //Check if the client has form field that stores request key
            if (null == Request.Form[VIEW_STATE_FIELD_NAME])
            {
                //Did not see form field for viewstate, return null to try to continue (could log event...)
                return null;
            }

            //Make sure it can be converted to request number (in case of corruption)
            long lRequestNumber = 0;
            try
            {
                lRequestNumber = Convert.ToInt64(Request.Form[VIEW_STATE_FIELD_NAME]);
            }
            catch
            {
                //Could not covert to request number, return null (could log event...)
                return null;
            }

            //Get the viewstate for this page
            string _viewState = viewStateSvrMgr.GetViewStateSvrMgr().GetViewState(lRequestNumber);

            //If find the viewstate on the server, convert it so ASP.Net can use it
            if (_viewState == null)
                return null;
            else
                return _formatter.Deserialize(_viewState);

        }

        //implementation of method
        private void SaveViewState(object viewState)
        {

            if (_formatter == null)
            {
                _formatter = new LosFormatter();
            }

            //Save the viewstate information
            StringBuilder _viewState = new StringBuilder();
            StringWriter _writer = new StringWriter(_viewState);
            _formatter.Serialize(_writer, viewState);
            long lRequestNumber = viewStateSvrMgr.GetViewStateSvrMgr().SaveViewState(_viewState.ToString());

            //Need to register the viewstate hidden field (must be present or postback things don't 
            // work, we use in our case to store the request number)
            //RegisterHiddenField(VIEW_STATE_FIELD_NAME, lRequestNumber.ToString());

            ClientScript.RegisterHiddenField(VIEW_STATE_FIELD_NAME, lRequestNumber.ToString());
        }
    }
}
