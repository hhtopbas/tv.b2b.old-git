﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using TvReport.SunReport;
using System.Text;
using TvTools;
using System.IO;
using System.Threading;

public class QuaReport
{
    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var _customersTicket = from H in ResData.ResService
                               join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                               join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                               join T in ResData.Title on C.Title equals T.TitleNo
                               join F in flights on H.Service equals F.Code
                               join A in airLines on F.Airline equals A.Code
                               where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                               select new
                               {
                                   CustNo = C.CustNo,
                                   Name = C.Name,
                                   SurName = C.Surname,
                                   TitleName = T.Code,
                                   ServiceID = H.RecID,
                                   DocStat = Rc.DocStat,
                                   DocNo = Rc.DocNo
                               };
        var customersTicket = from q in _customersTicket
                              group q by new
                              {
                                  CustNo = q.CustNo,
                                  Name = q.Name,
                                  SurName = q.SurName,
                                  TitleName = q.TitleName
                              } into k
                              select new
                              {
                                  k.Key.CustNo,
                                  k.Key.Name,
                                  k.Key.SurName,
                                  k.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;
            var custTicket = _customersTicket.Where(w => w.CustNo == row.CustNo).GroupBy(g => g.DocNo);

            Int16? DocStat = custTicket.Count() > 1 ?
                Convert.ToInt16(_customersTicket.Where(w => w.CustNo == row.CustNo && w.DocStat.HasValue && w.DocStat.Value == 1).GroupBy(g => g.DocNo).Count() > 0 ? 1 : 0) :
                _customersTicket.Where(w => w.CustNo == row.CustNo).FirstOrDefault().DocStat;
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        List<ResConRecord> resConList = ResData.ResCon;
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                ++cnt;
                writeList.Clear();
                List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                              join q3 in flyList on q1.Service equals q3.FlightNo
                                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                              select q1).ToList<ResServiceRecord>();
                AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);

                writeList.Add(new TvReport.Coordinate { value = agency.Name, x = 370, y = 798, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = agency.InvAddress, x = 370, y = 787, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = agency.Phone1, x = 370, y = 776, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 370, y = 7965, FontSize = 8 });

                string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());

                writeList.Add(new TvReport.Coordinate { value = airLine.Name, x = 222, y = 636, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = eTicketNo, x = 222, y = 619, FontSize = 8 });                
                writeList.Add(new TvReport.Coordinate { value = custFlights.FirstOrDefault().ResNo, x = 222, y = 600, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resCust.Surname + " / " + resCust.Name + " " + resCust.TitleStr, x = 222, y = 581, FontSize = 8 });                

                float _Y = 540;
                foreach (ResServiceRecord row in custAirleineFlights.OrderBy(o => o.BegDate))
                {
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 13.3f, y = _Y, H = 0, W = 562.45f, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 13.3f, y = _Y - 24f, H = 0, W = 562.45f, LineWidth = 0.25f });

                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 13.3f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 128f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 180f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 215f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 250f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 276f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 322f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 368f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 406f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 486f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 518f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 575.75f, y = _Y, H = -24f, W = 0, LineWidth = 0.25f });

                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 128f, y = _Y - 12f, H = 0, W = 52f, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 322f, y = _Y - 12f, H = 0, W = 46f, LineWidth = 0.25f });
                    writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 406f, y = _Y - 12f, H = 0, W = 80f, LineWidth = 0.25f });

                    FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    float firstLine = _Y - 9f;
                    float secondLine = _Y - 20f;
                    float centerLine = _Y - 15f;
                    writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationName(UserData.Market, flightDay.DepCity, NameType.NormalName, ref errorMsg), x = 18, y = firstLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationName(UserData.Market, flightDay.ArrCity, NameType.NormalName, ref errorMsg), x = 18, y = secondLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 133, y = firstLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 133, y = secondLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.Airline, x = 183, y = centerLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.Service, x = 217, y = centerLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 253, y = centerLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString(), x = 279, y = centerLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToString("HH:mm") : "", x = 325, y = firstLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrTime.HasValue ? flightDay.ArrTime.Value.ToString("HH:mm") : "", x = 325, y = secondLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString(), x = 410, y = firstLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString(), x = 410, y = secondLine, FontSize = 8 });                    
                    string confStat = string.Empty;
                    switch (row.StatConf)
                    {
                        case 0: confStat = resCust.Title < 8 ? "Request" : "NS"; break;
                        case 1: confStat = resCust.Title < 8 ? "OK" : "NS"; break;
                        case 2: confStat = resCust.Title < 8 ? "Not Confirm" : "NS"; break;
                        case 3: confStat = resCust.Title < 8 ? "No Show" : "NS"; break;
                    }
                    writeList.Add(new TvReport.Coordinate { value = confStat, x = 535, y = centerLine, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString("#,###") : "", x = 489, y = centerLine, FontSize = 8 });                    
                    _Y -= 30;
                    if (new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg))
                    {
                        ResConRecord resCon = resConList.Find(f => f.ResNo == row.ResNo && f.ServiceID == row.RecID && f.CustNo == CustNo);
                        resCon.DocNo = eTicketNo;
                        resCon.DocStat = 1;
                        resCon.DocIsDate = DateTime.Now;
                    }
                }
                writeList.Add(new TvReport.Coordinate { value = "VALID ON " + row1.airline + " / NONREF", x = 205, y = 343, FontSize = 8 });
                string ServiceCode = "ETicket_" + row1.airline;
                eTicketList.Add(new TvReport.QuaReport.QuaReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
            }
            HttpContext.Current.Session["ResData"] = ResData;
            if (eTicketList.Count() == 1)
                return eTicketList.FirstOrDefault();
            else return new TvReport.QuaReport.QuaReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);
        }
        else return "";
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, resService.Service, ref errorMsg);

        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, false, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        TvReport.RezedaReport.AgencyDocAddressRecord agencyAddr = new TvReport.RezedaReport.RezedaReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.RezedaReport.RezedaReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);

        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;
        float _yy = 0f;
        for (int cnt = 0; cnt < 3; cnt++)
        {
            switch (cnt)
            {
                case 0: _yy = 0f; break;
                case 1: _yy = 273.25f; break;
                case 2: _yy = 547.2f; break;
                default: _yy = 0f; break;
            }
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 456, y = 786 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.Name, x = 424, y = 744 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 424, y = 711 - _yy, FontSize = 7 });
            OfficeRecord office = new TvBo.Common().getOffice(ResData.ResMain.PLMarket, agent.OprOffice, ref errorMsg);
            if (office != null)
                writeList.Add(new TvReport.Coordinate { value = office.NameL, x = 424, y = 701 - _yy, FontSize = 7 });

            float _Y = 754;
            float _X = 18;
            int i = 0;
            foreach (var row in custs)
            {
                _Y -= 10; i++;
                string birtday = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "";
                writeList.Add(new TvReport.Coordinate { value = i.ToString() + ". " + row.TitleStr + ". " + row.Surname + " " + row.Name + " , " + birtday, x = _X, y = _Y - _yy, FontSize = 7 });
            }

            writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, hotel.Location, ref errorMsg), x = 264, y = 761 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = hotel.Name + " (" + hotel.Category + "), " + new Locations().getLocationForCityName(UserData, hotel.Location, ref errorMsg), x = 180, y = 744 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = resService.RoomName, x = 180, y = 711 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = resService.AccomName, x = 180, y = 701 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = resService.Board, x = 236, y = 683 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = resService.Adult.ToString(), x = 236, y = 663 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = resService.Child.ToString(), x = 355, y = 663 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = resService.BegDate.HasValue ? resService.BegDate.Value.ToShortDateString() : "", x = 236, y = 643 - _yy, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = resService.EndDate.HasValue ? resService.EndDate.Value.ToShortDateString() : "", x = 236, y = 623 - _yy, FontSize = 7 });

            string transfers = string.Empty;
            foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "TRANSFER" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>())
            {
                if (transfers.Length > 0) transfers += " / ";
                transfers += row.ServiceName;
            }
            writeList.Add(new TvReport.Coordinate { value = transfers, x = 133, y = 603 - _yy, FontSize = 7 });

            _Y = 669; _X = 424;
            foreach (ResServiceExtRecord row in ResData.ResServiceExt.Where(w => w.ServiceID == ServiceID && w.StatSer < 2))
            {
                writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName + " ," + row.BegDate.Value.ToShortDateString(), x = _X, y = _Y - _yy, FontSize = 7 });
                _Y -= 10;
            }

        }
        return new TvReport.QuaReport.QuaReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }    

    public string showInvoice(User UserData, ResDataRecord ResData, ref string errorMsg)
    {        
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        BankRecord oprBank = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);
        BankRecord agencyBank = new Banks().getBank(UserData.Market, agency.Bank1, ref errorMsg);
        ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
        if (leader == null)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError").ToString();
            return "";
        }
        ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
        if (leaderInfo == null)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError").ToString();
            return "";
        }
        if (leader != null)
            writeList.Add(new TvReport.Coordinate { value = leader.TitleStr.ToString() + ". " + leader.Surname.ToString() + " " + leader.Name.ToString(), x = 36, y = 692, FontSize = 8 });
        if (leaderInfo != null)
        {
            writeList.Add(new TvReport.Coordinate { value = !Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHome.ToString() : leaderInfo.AddrWork.ToString(), x = 36, y = 676, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = !Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHomeZip + " " + leaderInfo.AddrHomeCity.ToString() : leaderInfo.AddrWorkZip.ToString() + " " + leaderInfo.AddrWorkCountry.ToString(), x = 36, y = 660, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = !Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry.ToString(), x = 36, y = 644, FontSize = 8 });
        }
        writeList.Add(new TvReport.Coordinate { value = "Reservation nummber :" + ResData.ResMain.ResNo, x = 545, y = 676, FontSize = 8, Align = TvReport.fontAlign.Right, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "", x = 545, y = 660, FontSize = 8, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = "Date: " + DateTime.Today.ToShortDateString(), x = 545, y = 644, FontSize = 8, Align = TvReport.fontAlign.Right });

        writeList.Add(new TvReport.Coordinate { value = "Passanger's", x = 36, y = 583, FontSize = 8, bold = true });
        float Y = 583;
        foreach (ResCustRecord row in ResData.ResCust.Where(w => !(w.Status.HasValue && w.Status.Value == 1)))
        {
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = row.TitleStr + " " + row.Surname + " " + row.Name, x = 36, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 284, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.PassSerie.ToString() + row.PassNo.ToString(), x = 360, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.IDNo.ToString(), x = 445, y = Y, FontSize = 8 });
        }

        //writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20, y = 595, H = 0, W = 570, LineWidth = 0.25f });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "Service's", x = 36, y = Y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "Price's", x = 545, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        Y -= 3;
        float tempY = Y;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 32, y = Y, H = 0, W = 518, LineWidth = 0.25f });
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.StatSer < 2))
        {
            string ServiceDesc = string.Empty;
            switch (row.ServiceType)
            {
                case "HOTEL":
                    ServiceDesc += row.ServiceName + " " + row.DepLocationNameL + " (" + row.Room + "," + row.Accom + "," + row.Board + ") " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "FLIGHT":
                    FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    ServiceDesc += row.Service + " (" + flightDay.DepAirport + "->" + flightDay.ArrAirport + ")," + row.FlgClass.ToString() + ", " + row.BegDate.Value.ToShortDateString();
                    break;
                case "TRANSPORT":
                    ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString();
                    break;
                case "TRANSFER": TransferRecord trf = new Transfers().getTransfer(UserData.Market, row.Service, ref errorMsg);
                    if (trf != null && string.Equals(trf.Direction, "R"))
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + ", " + row.EndDate.Value.ToShortDateString();
                    else ServiceDesc += row.ServiceName + " " + ", " + row.BegDate.Value.ToShortDateString();
                    break;
                case "RENTING":
                    ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "EXCURSION":
                    ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString();
                    break;
                case "INSURANCE":
                    ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "VISA":
                    ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "HANDFEE":
                    ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                default:
                    ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
            }

            Y -= 10;
            writeList.Add(new TvReport.Coordinate { value = ServiceDesc, x = 36, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.SalePrice.HasValue ? (row.SalePrice.Value.ToString("#,###.00") + " " + row.SaleCur) : "", x = 545, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
            Y -= 3;
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 32, y = Y, H = 0, W = 518, LineWidth = 0.25f });
        }
        foreach (ResServiceExtRecord row in ResData.ResServiceExt.Where(w => w.StatSer < 2))
        {
            Y -= 10;
            writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName + " , " + row.BegDate.Value.ToShortDateString() + " - " + row.EndDate.Value.ToShortDateString(), x = 36, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.SalePrice.HasValue ? (row.SalePrice.Value.ToString("#,###.00") + " " + row.SaleCur) : "", x = 545, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
            Y -= 3;
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 32, y = Y, H = 0, W = 518, LineWidth = 0.25f });
        }
        if (ResData.ResMain.Discount.HasValue)
        {
            Y -= 10;
            writeList.Add(new TvReport.Coordinate { value = "Suplement / Discount", x = 36, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Discount.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur, x = 545, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
            Y -= 3;
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 32, y = Y, H = 0, W = 518, LineWidth = 0.25f });
        }
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 32, y = tempY, H = Y - tempY, W = 0, LineWidth = 0.25f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 432, y = tempY, H = Y - tempY, W = 0, LineWidth = 0.25f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 550, y = tempY, H = Y - tempY, W = 0, LineWidth = 0.25f });        
        ResMainRecord resMain =ResData.ResMain;        
        writeList.Add(new TvReport.Coordinate { value = resMain.SalePrice.HasValue ? (resMain.SalePrice.Value.ToString("#,###.00") + " " + resMain.SaleCur) : "", x = 545, y = 176, FontSize = 8, Align = TvReport.fontAlign.Right });        
        decimal totalCommision = (resMain.AgencyCom.HasValue ? resMain.AgencyCom.Value : 0) + (resMain.AgencyComSup.HasValue ? resMain.AgencyComSup.Value : 0);
        writeList.Add(new TvReport.Coordinate { value = totalCommision>0 ? (totalCommision.ToString("#,###.00") + " " + resMain.SaleCur) : "", x = 545, y = 161, FontSize = 8, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = resMain.AgencyPayable.HasValue ? (resMain.AgencyPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "", x = 545, y = 145, FontSize = 10, Align = TvReport.fontAlign.Right, bold = true });

        writeList.Add(new TvReport.Coordinate { value = resMain.ResNo, x = 238, y = 95, FontSize = 8 });
        List<resPayPlanRecord> payPlanList = new ReservationMonitor().getPaymentPlan(resMain.ResNo, ref errorMsg);
        Y = 58;
        if (payPlanList != null && payPlanList.Count > 0)
        {
            foreach (resPayPlanRecord row in payPlanList)
            {
                writeList.Add(new TvReport.Coordinate { value = row.DueDate.ToShortDateString(), x = 36, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Amount.HasValue ? row.Amount.Value.ToString("#,###.00") + " " + row.Cur : "", x = 190, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
                Y -= 11;
            }
        }
        return new TvReport.QuaReport.QuaReport().createInvoice(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }
}





