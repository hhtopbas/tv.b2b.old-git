﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using TvBo;
using TvReport.SunReport;
using TvTools;

public class AzrReport
{
    public string showContract(User UserData, ResDataRecord ResData)
    {

        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        ResMainRecord resMain = ResData.ResMain;
        ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
        List<ResServiceRecord> resService = ResData.ResService;
        AgencyDocAddressRecord agencyAddr = new Agency().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 150f, y = 282f, FontSize = 10, bold = true, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 155f, y = 270.5f, FontSize = 10, PageNo = 1 });

        string firstParagraf = string.Format("Bir tərəfdən gələcəkdə “İCRAÇI” adlanacaq, Azərbaycan Respublikasının qanununu və öz Nizamnaməsi əsasında fəaliyyət göstərən {0} turizm firması (Lisenziya № {1} tarixindən), {2} şəxsindən və digər tərəfdən gələcəkdə “SİFARİŞÇİ” adlanacaq {3} şəxsində, aşağıdakı şərtlər əsasında müqaviləni imzaladılar.",
                                    agency.FirmName,
                                    agency.LicenseNo,
                                    agency.BossName,
                                    leader.Name + " " + leader.Surname);
        writeList.Add(new TvReport.Coordinate { value = firstParagraf, x = 10f, y = 266.25f, W = 195.9f, H = 18.5f, FontSize = 10, PageNo = 1, Type = TvReport.writeType.AreaText });

        writeList.Add(new TvReport.Coordinate { value = resMain.Adult.ToString(), x = 93f, y = 209f + 16.225f, FontSize = 10, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resMain.Child.ToString(), x = 112f, y = 209f + 16.225f, FontSize = 10, PageNo = 1 });

        writeList.Add(new TvReport.Coordinate { value = resMain.DepCityNameL + " - " + resMain.ArrCityNameL, x = 80f, y = 204.5f + 16.225f, FontSize = 10, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resMain.BegDate.Value.ToShortDateString() + " - " + resMain.EndDate.Value.ToShortDateString(), x = 80f, y = 200f + 16.225f, FontSize = 10, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.Where(w => w.ServiceType == "FLIGHT").Count() > 0 ? "Təyyarə" : "", x = 80f, y = 195.5f + 16.225f, FontSize = 10, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.Where(w => w.ServiceType == "TRANSFER").Count() > 0 ? "VAR" : "", x = 80f, y = 191f + 16.225f, FontSize = 10, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.Where(w => w.ServiceType == "HOTEL").Count() > 0 ? resService.Where(w => w.ServiceType == "HOTEL").First().ServiceNameL : "", x = 80f, y = 186.5f + 16.225f, FontSize = 10, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.Where(w => w.ServiceType == "HOTEL").Count() > 0 ? resService.Where(w => w.ServiceType == "HOTEL").First().RoomNameL : "", x = 80f, y = 182f + 16.225f, FontSize = 10, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.Where(w => w.ServiceType == "HOTEL").Count() > 0 ? resService.Where(w => w.ServiceType == "HOTEL").First().BoardNameL : "", x = 80f, y = 177.5f + 16.225f, FontSize = 10, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.Where(w => w.ServiceType == "EXCURSION").Count() > 0 ? "VAR" : "", x = 80f, y = 173f + 16.225f, FontSize = 10, PageNo = 1 });
        writeList.Add(new TvReport.Coordinate { value = resService.Where(w => w.ServiceType == "HOTEL").Count() > 0 ? resService.Where(w => w.ServiceType == "INSURANCE").First().ServiceNameL : "", x = 80f, y = 168.5f + 16.225f, FontSize = 10, PageNo = 1 });

        string lineStr = string.Format("{0} N-li sifariş üzrə turizm xidmətlərinin tam dəyəri: {1} Azərbaycan manatıdır.",
                                ResData.ResMain.ResNo,
                                ResData.ResMain.PasPayable.HasValue ? ResData.ResMain.PasPayable.Value.ToString("#,###.00") : "");

        writeList.Add(new TvReport.Coordinate { value = lineStr, x = 11f, y = 94.3f, FontSize = 10, PageNo = 1 });


        writeList.Add(new TvReport.Coordinate { value = agency.FirmName, x = 14f, y = 81.5f, FontSize = 10, bold = true, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Phone1, x = 28.5f, y = 77.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Fax1, x = 28.5f, y = 73.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DAddrZip + " " + agencyAddr.DAddrCity, x = 37.5f, y = 69.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.InvAddress, x = 14f, y = 65.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Bank1Name, x = 28.5f, y = 61.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Bank1IBAN, x = 47.5f, y = 57.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = agency.TaxAccNo, x = 28.5f, y = 53.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = agency.Email1, x = 28.5f, y = 49.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = agency.Www, x = 28.5f, y = 45.5f, FontSize = 10, PageNo = 2 });


        writeList.Add(new TvReport.Coordinate { value = leader.Name + " " + leader.Surname, x = 113f, y = 77.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = leader.PassNo + leader.PassNo, x = 130.5f, y = 65.5f, FontSize = 10, PageNo = 2 });

        writeList.Add(new TvReport.Coordinate { value = agency.BossName, x = 22.5f, y = 17.5f, FontSize = 10, PageNo = 2 });
        writeList.Add(new TvReport.Coordinate { value = leader.Name + " " + leader.Surname, x = 110f, y = 17.5f, FontSize = 10, PageNo = 2 });

        return new TvReport.AztReport.AztReport().createContract(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);

    }

    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var _customersTicket = from H in ResData.ResService
                               join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                               join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                               join T in ResData.Title on C.Title equals T.TitleNo
                               join F in flights on H.Service equals F.Code
                               join A in airLines on F.Airline equals A.Code
                               where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                               select new
                               {
                                   CustNo = C.CustNo,
                                   Name = C.Name,
                                   SurName = C.Surname,
                                   TitleName = T.Code,
                                   ServiceID = H.RecID,
                                   DocStat = Rc.DocStat,
                                   DocNo = Rc.DocNo
                               };
        var customersTicket = from q in _customersTicket
                              group q by new
                              {
                                  CustNo = q.CustNo,
                                  Name = q.Name,
                                  SurName = q.SurName,
                                  TitleName = q.TitleName
                              } into k
                              select new
                              {
                                  k.Key.CustNo,
                                  k.Key.Name,
                                  k.Key.SurName,
                                  k.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;
            var custTicket = _customersTicket.Where(w => w.CustNo == row.CustNo).GroupBy(g => g.DocNo);

            Int16? DocStat = custTicket.Count() > 1 ?
                Convert.ToInt16(_customersTicket.Where(w => w.CustNo == row.CustNo && w.DocStat.HasValue && w.DocStat.Value == 1).GroupBy(g => g.DocNo).Count() > 0 ? 1 : 0) :
                _customersTicket.Where(w => w.CustNo == row.CustNo).FirstOrDefault().DocStat;
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        List<ResConRecord> resConList = ResData.ResCon;
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                ++cnt;
                writeList.Clear();
                List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                              join q3 in flyList on q1.Service equals q3.FlightNo
                                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                              select q1).ToList<ResServiceRecord>();


                AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);
                string agencyName_airlineName = UserData.AgencyName + " / " + (airLine != null ? airLine.Name : "");
                writeList.Add(new TvReport.Coordinate { value = agencyName_airlineName, x = 30, y = 700, FontSize = 8 });

                string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());

                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToString("dd.MM.yyyy"), x = 490, y = 736, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custFlights.FirstOrDefault().ResNo, x = 490, y = 725, FontSize = 8 });
                if (!string.Equals(row1.airline, "AZAL"))
                    writeList.Add(new TvReport.Coordinate { value = eTicketNo, x = 490, y = 714, FontSize = 8 });

                writeList.Add(new TvReport.Coordinate { value = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name, x = 35, y = 652, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resCust.Birtday.HasValue ? resCust.Birtday.Value.ToShortDateString() : "", x = 308, y = 652, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resCust.PassSerie + " " + resCust.PassNo, x = 460, y = 652, FontSize = 8 });

                float _Y = 600;
                foreach (ResServiceRecord row in custAirleineFlights.OrderBy(o => o.BegDate))
                {
                    FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);

                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToString("dd.MM.yyyy"), x = 25, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.Service, x = 98, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 168, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 300, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToString("HH:mm") : "", x = 432, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 476, y = _Y, FontSize = 8 });
                    string confStat = string.Empty;
                    switch (row.StatConf)
                    {
                        case 0: confStat = resCust.Title < 8 ? "Request" : "NS"; break;
                        case 1: confStat = resCust.Title < 8 ? "OK" : "NS"; break;
                        case 2: confStat = resCust.Title < 8 ? "Not Confirm" : "NS"; break;
                        case 3: confStat = resCust.Title < 8 ? "No Show" : "NS"; break;
                    }
                    writeList.Add(new TvReport.Coordinate { value = confStat, x = 504, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString("#,###") + "K" : "", x = 540, y = _Y, FontSize = 8 });

                    _Y -= 19;
                    if (new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg))
                    {
                        ResConRecord resCon = resConList.Find(f => f.ResNo == row.ResNo && f.ServiceID == row.RecID && f.CustNo == CustNo);
                        resCon.DocNo = eTicketNo;
                        resCon.DocStat = 1;
                        resCon.DocIsDate = DateTime.Now;
                    }
                }

                string ServiceCode = "ETicket_" + row1.airline;
                eTicketList.Add(new TvReport.AztReport.AztReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
            }
            HttpContext.Current.Session["ResData"] = ResData;
            if (eTicketList.Count() == 1)
                return eTicketList.FirstOrDefault();
            else return new TvReport.AztReport.AztReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);
        }
        else return "";
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 100, y = 788, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 100, y = 788 - 375, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 100, y = 771, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 100, y = 771 - 375, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 486, y = 771, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 486, y = 771 - 375, FontSize = 8 });

        float _Y = 749;
        float _X = 20;
        int i = 0;
        foreach (var row in custs)
        {
            _Y -= 11; i++;
            writeList.Add(new TvReport.Coordinate { value = i.ToString() + ". " + row.TitleStr + ". " + row.Surname + " " + row.Name, x = _X, y = _Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 370, y = _Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = i.ToString() + ". " + row.TitleStr + ". " + row.Surname + " " + row.Name, x = _X, y = _Y - 375, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 370, y = _Y - 375, FontSize = 8 });
        }
        _Y = 668;
        writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString(), x = 101, y = _Y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.EndDate.Value.ToShortDateString(), x = 291, y = _Y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Duration.Value.ToString(), x = 476, y = _Y, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString(), x = 101, y = _Y - 375, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.EndDate.Value.ToShortDateString(), x = 291, y = _Y - 375, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Duration.Value.ToString(), x = 476, y = _Y - 375, FontSize = 8 });

        _Y = 635;
        writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg), x = 76, y = _Y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityNameL, x = 291, y = _Y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationNameL, x = 468, y = _Y, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg), x = 76, y = _Y - 375, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityNameL, x = 291, y = _Y - 375, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationNameL, x = 468, y = _Y - 375, FontSize = 8 });
        _Y -= 14;
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceNameL, x = 76, y = _Y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceNameL, x = 76, y = _Y - 375, FontSize = 8 });
        _Y -= 14;
        writeList.Add(new TvReport.Coordinate { value = resService.AccomNameL + " " + resService.RoomNameL, x = 87, y = _Y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.AccomNameL + " " + resService.RoomNameL, x = 87, y = _Y - 375, FontSize = 8 });
        _Y -= 14;
        writeList.Add(new TvReport.Coordinate { value = resService.BoardNameL, x = 87, y = _Y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BoardNameL, x = 87, y = _Y - 375, FontSize = 8 });

        _Y = 563; _X = 31;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>())
        {
            FlightDayRecord flight = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
            if (flight != null)
            {
                writeList.Add(new TvReport.Coordinate { value = row.DepLocationNameL, x = _X, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.ArrLocationNameL, x = _X + 86, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Service, x = _X + 185, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "", x = _X + 257, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "", x = _X + 353, y = _Y, FontSize = 8 });

                writeList.Add(new TvReport.Coordinate { value = row.DepLocationNameL, x = _X, y = _Y - 375, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.ArrLocationNameL, x = _X + 86, y = _Y - 375, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Service, x = _X + 185, y = _Y - 375, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "", x = _X + 257, y = _Y - 375, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "", x = _X + 353, y = _Y - 375, FontSize = 8 });

                _Y -= 11;
            }
        }

        _Y = 521; _X = 31;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "TRANSFER" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>())
        {
            writeList.Add(new TvReport.Coordinate { value = row.ServiceName + " (" + row.DepLocationNameL + " - " + row.ArrLocationNameL + ")", x = _X, y = _Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.ServiceName + " (" + row.DepLocationNameL + " - " + row.ArrLocationNameL + ")", x = _X, y = _Y - 375, FontSize = 8 });
            _Y -= 11;
        }

        _Y = 521; _X = 252;
        foreach (ResServiceExtRecord row in ResData.ResServiceExt.Where(w => w.ServiceID == ServiceID && w.StatSer < 2))
        {
            writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString() + " " + row.ExtServiceName, x = _X, y = _Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString() + " " + row.ExtServiceName, x = _X, y = _Y - 375, FontSize = 8 });
            _Y -= 11;
        }

        writeList.Add(new TvReport.Coordinate { value = resService.SupNote, x = 56, y = 505, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.SupNote, x = 56, y = 505 - 375, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyName, x = 80, y = 468, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyRec.Phone1, x = 56, y = 455, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyName, x = 80, y = 468 - 375, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = UserData.AgencyRec.Phone1, x = 56, y = 455 - 375, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = resService.SupplierNameL, x = 370, y = 479, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.SupplierNameL, x = 370, y = 479 - 375, FontSize = 8 });
        SupplierRecord supplier = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
        if (supplier != null)
        {
            writeList.Add(new TvReport.Coordinate { value = supplier.Phone1, x = 325, y = 466, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = supplier.Fax1, x = 314, y = 453, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = supplier.Phone1, x = 325, y = 466 - 375, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = supplier.Fax1, x = 314, y = 453 - 375, FontSize = 8 });
        }

        return new TvReport.AztReport.AztReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showInsuranceListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<TvReport.AztReport.ResCustInsuranceRecord> customerInsurances = new TvReport.AztReport.AztReport().getInsuranceCustomers(UserData.Market, ResData.ResMain.ResNo, ref errorMsg);
        if (customerInsurances == null || customerInsurances.Count < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (TvReport.AztReport.ResCustInsuranceRecord row in customerInsurances)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q in ResData.ResCon
                         where q.CustNo == row.CustNo && q.ServiceID == row.ServiceID
                         select q.DocStat;

            Int16? DocStat = Conversion.getInt16OrNull(ResData.ResCon.Find(f => f.CustNo == row.CustNo && f.ServiceID == row.ServiceID).DocStat);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == row.ServiceID);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td style=\"font-family: Tahoma; font-size: 14px;\">");
            sb.Append(i.ToString() + ". " + Name);
            sb.Append("     </td>");
            sb.Append("     <td>&nbsp;");

            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Insurance0969801('{0}', {1}, {2});\" style=\"cursor: pointer; text-decoration: underline;\">{3} (",
                                resService.ResNo,
                                resService.RecID,
                                row.CustNo,
                                resService.ServiceNameL);
                sb.Append(((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued")) + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Insurance0969801('{0}', {1}, {2});\" style=\"cursor: pointer; text-decoration: underline;\">{3} (",
                                resService.ResNo,
                                resService.RecID,
                                row.CustNo,
                                resService.ServiceNameL);
                    sb.Append(((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued")) + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showInsurance(User UserData, ref ResDataRecord ResData, int? ServiceID, int? CustNo)
    {
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        if (!Equals(resService.ServiceType, "INSURANCE")) return string.Empty;

        InsuranceRecord insurance = new Insurances().getInsurance(UserData.Market, resService.Service, ref errorMsg);
        Int16 maxTourist = insurance.MaxPaxPolicy.HasValue ? insurance.MaxPaxPolicy.Value : Convert.ToInt16(6);

        string custListString = CustNo.ToString();
        var insuranceCust = from q1 in ResData.ResService
                            join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                            where q1.RecID == ServiceID
                            select new { CustNo = q2.CustNo, recID = Convert.ToInt32(0), polNo = q2.DocNo };
        if (insuranceCust.Count() > 0)
        {
            Int16 cnt = 1;
            foreach (var row in insuranceCust.Where(w => w.CustNo != CustNo))
            {
                cnt++;
                if (cnt <= maxTourist)
                    custListString += "," + row.CustNo.ToString();
            }
        }

        List<TvReport.AztReport.CustInsuranceRecord> custInsList = new TvReport.AztReport.AztReport().getcustInsurance(ResData.ResMain.ResNo, ServiceID, custListString, ref errorMsg);
        TvReport.AztReport.CustInsuranceRecord custIns = custInsList.First();

        TvReport.AztReport.InsuranceRptRecord ins = new TvReport.AztReport.AztReport().getInsurance(UserData.Market, ResData.ResMain.ResNo, ServiceID, ref errorMsg);
        TvReport.AztReport.ResCustInfoRecord custInfo = new TvReport.AztReport.AztReport().getResCustInfoRecord(CustNo, ref errorMsg);

        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();

        string pdfName = ResData.ResService.Find(f => f.RecID == ServiceID).Service;
        string polNo = string.Empty;
        if (pdfName == "GAR1")
        {
            #region GAR1
            if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
            else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;

            writeList.Add(new TvReport.Coordinate { value = "NO TI-OG " + polNo, x = 270, y = 795, FontSize = 10, bold = true });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 483, y = 794, FontSize = 10, bold = true });

            writeList.Add(new TvReport.Coordinate { value = custIns.Surname + " " + custIns.Name, x = 131, y = 730, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = custIns.PassSerie + " " + custIns.PassNo, x = 445, y = 684, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 214, y = 684, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = custIns.Surname + " " + custIns.Name, x = 131, y = 698, FontSize = 9 });
            if (custInfo != null)
            {
                writeList.Add(new TvReport.Coordinate { value = !Equals(custInfo.ContactAddr, "W") ? custInfo.AddrHome + " " + custInfo.AddrHomeZip + " " + custInfo.AddrHomeCity : custInfo.AddrWork + " " + custInfo.AddrWorkZip + " " + custInfo.AddrWorkCity, x = 191, y = 715, FontSize = 9 });
                writeList.Add(new TvReport.Coordinate { value = !Equals(custInfo.ContactAddr, "W") ? custInfo.AddrHomeTel : custInfo.AddrWorkTel, x = 445, y = 698, FontSize = 9 });
            }
            string ser_BegDate = resService.BegDate.HasValue ? resService.BegDate.Value.ToShortDateString() : "";
            string ser_EndDate = resService.EndDate.HasValue ? resService.EndDate.Value.ToShortDateString() : "";
            writeList.Add(new TvReport.Coordinate { value = ser_BegDate, x = 185, y = 660, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = ser_EndDate, x = 340, y = 660, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = (ins.Duration + 1).ToString(), x = 518, y = 660, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = ins.Coverage.HasValue ? ins.Coverage.Value.ToString("#,###") + " USD" : "", x = 110, y = 635, FontSize = 14, bold = true });
            //writeList.Add(new TvReport.Coordinate { value = resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur : "", x = 263, y = 635, FontSize = 14, bold = true });
            writeList.Add(new TvReport.Coordinate { value = ins.Franchise.HasValue ? ins.Franchise.Value.ToString("#,###") + " USD" : "", x = 110, y = 612, FontSize = 14, bold = true });
            writeList.Add(new TvReport.Coordinate { value = ins.InsZone, x = 439, y = 635, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur : "", x = 439, y = 612, FontSize = 8 });
            #endregion
        }
        string retVal = new TvReport.AztReport.AztReport().createInsurance(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), pdfName, ref errorMsg);
        if (!string.IsNullOrEmpty(retVal))
        {
            var query = from q1 in insuranceCust
                        join q2 in custListString.Split(',') on q1.CustNo equals Convert.ToInt32(q2)
                        select q1;
            foreach (var row in query)
            {
                if (new Reservation().setInsuranceDoc(ResData.ResMain.ResNo, ServiceID, row.CustNo, polNo, ref errorMsg))
                {
                    new Insurances().setInsurancePolicy(UserData, resService.Service, Conversion.getInt32OrNull(polNo), row.CustNo, resService.Supplier, resService.ResNo, resService.BegDate, resService.EndDate,
                                                        resService.Duration, ins.Coverage, ins.CoverageCur, resService.DepLocation, ins.InsZone, UserData.AgencyRec.Location, UserData.UserID, "Y", ref errorMsg);
                }
                ResConRecord resCon = ResData.ResCon.Find(f => f.CustNo == row.CustNo && f.ServiceID == ServiceID);
                resCon.DocNo = polNo;
                resCon.DocStat = 1;
                resCon.DocIsDate = DateTime.Today;
            }
            return retVal;
        }
        else return "";
    }

    public string showInvoice(User UserData, ResDataRecord ResData, ref string errorMsg)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        BankRecord oprBank = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);
        BankRecord agencyBank = new Banks().getBank(UserData.Market, agency.Bank1, ref errorMsg);
        ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
        if (leader == null)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError").ToString();
            return "";
        }
        ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
        if (leaderInfo == null)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError").ToString();
            return "";
        }

        writeList.Add(new TvReport.Coordinate { value = leader.TitleStr + ". " + leader.Surname + " " + leader.Name, x = 46, y = 685, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = !Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHome : leaderInfo.AddrWork, x = 46, y = 665, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = !Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHomeZip + " " + leaderInfo.AddrHomeCity : leaderInfo.AddrWorkZip + " " + leaderInfo.AddrWorkCountry, x = 46, y = 645, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = !Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry, x = 46, y = 625, FontSize = 10 });

        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 530, y = 685, FontSize = 10, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 530, y = 665, FontSize = 10, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = !string.IsNullOrEmpty(leaderInfo.MobTel) ? leaderInfo.MobTel : (!Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry), x = 530, y = 645, FontSize = 10, Align = TvReport.fontAlign.Right });

        writeList.Add(new TvReport.Coordinate { value = "Reservation Book of Number : " + ResData.ResMain.ResNo, x = 100, y = 600, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20, y = 595, H = 0, W = 570, LineWidth = Convert.ToSingle(25 / 100.0) });

        Single Y = 585;

        writeList.Add(new TvReport.Coordinate { value = "From", x = 302, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        writeList.Add(new TvReport.Coordinate { value = "To", x = 381, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        writeList.Add(new TvReport.Coordinate { value = "Night", x = 447, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        writeList.Add(new TvReport.Coordinate { value = "Board", x = 519, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Center });
        //Y = 545;
        var query = from q in ResData.ResService
                    where q.ServiceType == "HOTEL"
                    group q by new
                    {
                        HotelName = q.ServiceNameL,
                        RoomName = q.RoomNameL,
                        BoardName = q.BoardNameL,
                        FromDate = q.BegDate,
                        ToDate = q.EndDate,
                        Night = q.Duration,
                        LocationName = q.DepLocationNameL,
                        Location = q.DepLocation
                    } into k
                    select new { k.Key.BoardName, k.Key.FromDate, k.Key.HotelName, k.Key.Location, k.Key.LocationName, k.Key.Night, k.Key.RoomName, k.Key.ToDate };
        //+", "+new Locations().getLocationForCountryName(UserData.Market, q.DepLocation, ref errorMsg).ToString()
        foreach (var row in query)
        {
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = "Hotel:", x = 46, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.HotelName, x = 96, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.FromDate.HasValue ? row.FromDate.Value.ToShortDateString() : "", x = 302, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
            writeList.Add(new TvReport.Coordinate { value = row.ToDate.HasValue ? row.ToDate.Value.ToShortDateString() : "", x = 381, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
            writeList.Add(new TvReport.Coordinate { value = row.Night.ToString(), x = 447, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
            writeList.Add(new TvReport.Coordinate { value = row.BoardName, x = 519, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = "Room type:", x = 46, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.RoomName, x = 96, y = Y, FontSize = 8 });
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = "Location:", x = 46, y = Y, FontSize = 8 });
            string locationCountry = new Locations().getLocationForCountryName(UserData, row.Location.Value, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = row.LocationName + " " + locationCountry, x = 96, y = Y, FontSize = 8 });
        }
        Y -= 3;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20, y = Y, H = 0, W = 570, LineWidth = Convert.ToSingle(25 / 100.0) });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "Passenger(s)", x = 67, y = Y, FontSize = 8, bold = true, Underline = true });
        writeList.Add(new TvReport.Coordinate { value = "Birth Date", x = 194, y = Y, FontSize = 8, bold = true, Underline = true });
        foreach (ResCustRecord row in ResData.ResCust.Where(w => !(w.Status.HasValue && w.Status.Value == 1)))
        {
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = row.TitleStr + " " + row.Surname + " " + row.Name, x = 46, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 194, y = Y, FontSize = 8 });
        }
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "Adult", x = 67, y = Y, FontSize = 8, Underline = true });
        writeList.Add(new TvReport.Coordinate { value = "Child", x = 106, y = Y, FontSize = 8, Underline = true });
        var query1 = from q in ResData.ResService
                     where q.IncPack == "Y"
                     select new { SalePrice = q.SalePrice, SaleCur = q.SaleCur, Desc = "Package Total" };

        var query2 = from q in ResData.ResService
                     where q.IncPack != "Y"
                     select new { Adult = q.Adult, Child = q.Child, SalePrice = q.SalePrice, SaleCur = q.SaleCur, Desc = q.ServiceNameL };
        var query3 = from q in ResData.ResServiceExt
                     where q.IncPack != "Y"
                     select new { Adult = q.Adult, Child = q.Child, SalePrice = q.SalePrice, SaleCur = q.SaleCur, Desc = q.ExtServiceNameL };
        if (query1 != null && query1.Count() > 0)
        {
            Y -= 12;
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Adult.ToString(), x = 78, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Child.ToString(), x = 117, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = query1.Sum(s => s.SalePrice).HasValue ? query1.Sum(s => s.SalePrice).Value.ToString("#,###.00") : "", x = 208, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = query1.FirstOrDefault().SaleCur, x = 217, y = Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = query1.FirstOrDefault().Desc, x = 251, y = Y, FontSize = 8 });
        }
        if (query2 != null && query2.Count() > 0)
        {
            Y -= 2;
            foreach (var row in query2)
            {
                Y -= 12;
                writeList.Add(new TvReport.Coordinate { value = row.Adult.ToString(), x = 78, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Child.ToString(), x = 117, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "", x = 208, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
                writeList.Add(new TvReport.Coordinate { value = row.SaleCur, x = 217, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Desc, x = 251, y = Y, FontSize = 8 });
            }
            Y -= 12;
        }
        if (query3 != null && query3.Count() > 0)
        {
            Y -= 2;
            foreach (var row in query3)
            {
                Y -= 12;
                writeList.Add(new TvReport.Coordinate { value = row.Adult.ToString(), x = 78, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Child.ToString(), x = 117, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "", x = 208, y = Y, FontSize = 8, Align = TvReport.fontAlign.Right });
                writeList.Add(new TvReport.Coordinate { value = row.SaleCur, x = 217, y = Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.Desc, x = 251, y = Y, FontSize = 8 });
            }
        }
        Y = 157;
        writeList.Add(new TvReport.Coordinate { value = "Total :", x = 467, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.PasAmount.HasValue ? ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 555, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        Y -= 11;
        writeList.Add(new TvReport.Coordinate { value = "Suppl./Disc. :", x = 467, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        decimal SuppDis = (ResData.ResMain.PasSupDis.HasValue ? ResData.ResMain.PasSupDis.Value : Convert.ToDecimal(0.0)) - (ResData.ResMain.EBPas.HasValue ? ResData.ResMain.EBPas.Value : Convert.ToDecimal(0.0));
        writeList.Add(new TvReport.Coordinate { value = SuppDis.ToString("#,###.00") + " " + ResData.ResMain.SaleCur, x = 555, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        Y -= 11;
        writeList.Add(new TvReport.Coordinate { value = "Amount To Pay :", x = 467, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.PasPayable.HasValue ? ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 555, y = Y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        Y = 100;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20, y = Y, H = 0, W = 570, LineWidth = Convert.ToSingle(25 / 100.0) });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = opr.FirmName, x = 314, y = Y, FontSize = 10, bold = true, Align = TvReport.fontAlign.Center });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "Web: " + opr.Www + "  Email: " + opr.Email1, x = 314, y = Y, FontSize = 10, Align = TvReport.fontAlign.Center });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = opr.Address + " " + opr.AddrZip + " " + opr.AddrCity + " " + opr.AddrCountry, x = 314, y = Y, FontSize = 10, Align = TvReport.fontAlign.Center });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "Phone: " + opr.Phone1 + " , Fax: " + opr.Fax1, x = 314, y = Y, FontSize = 8, Align = TvReport.fontAlign.Center });
        Y -= 15;
        writeList.Add(new TvReport.Coordinate { value = "VAT " + opr.TaxOffice + " | Trade Register " + opr.TaxAccNo, x = 314, y = Y, FontSize = 10, Align = TvReport.fontAlign.Center });

        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20, y = 20, H = 0, W = 570, LineWidth = Convert.ToSingle(25 / 100.0) });

        return new TvReport.AztReport.AztReport().createInvoice(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }
}



