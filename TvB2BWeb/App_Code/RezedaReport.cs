﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using TvReport.SunReport;
using System.Text;
using TvTools;
using System.IO;
using System.Threading;

public class RezedaReport
{
    public string showInvoice(User UserData, ResDataRecord ResData, ref string errorMsg)
    {
        try
        {
            List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
            AgencyRecord agency = UserData.AgencyRec;
            object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
            string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
            string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
            string basePageUrl = WebRoot.BasePageRoot;
            string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
            OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.Operator, ref errorMsg);
            AgencyRecord currentAgency = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
            AgencyRecord agent = new Agency().getAgency(currentAgency.UseMainCont ? (string.IsNullOrEmpty(currentAgency.MainOffice) ? currentAgency.Code : currentAgency.MainOffice) : currentAgency.Code, false, ref errorMsg);
            AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, false, ref errorMsg);
            if (agent == null) agent = new Agency().getAgency(currentAgency.Code, false, ref errorMsg);
            TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(currentAgency.Code, ref errorMsg);
            ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
            ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));
            //writeList.Add(new TvReport.Coordinate { value = "", x = 14f, y = 81.5f, FontSize = 10, bold = false });        
            writeList.Add(new TvReport.Coordinate { value = "(" + ResData.ResMain.ResNo + ")", x = 275f, y = 758f, FontSize = 12, bold = false });
            writeList.Add(new TvReport.Coordinate { value = string.IsNullOrEmpty(agencyAddr.FirmName) ? agencyAddr.Name : agencyAddr.FirmName, x = 405f, y = 807f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.TaxOffice, x = 423f, y = 794f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.TaxAccNo, x = 395f, y = 780f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.InvAddress, x = 413f, y = 765f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.InvAddrZip + " " + agencyAddr.InvAddrCity, x = 376f, y = 755f, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = agency.LocationName + " " + new Locations().getLocationForCountryName(UserData, UserData.Location.HasValue ? UserData.Location.Value : -1, ref errorMsg), x = 406f, y = 743f, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 452f, y = 654f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 502f, y = 639f, FontSize = 8 });
            decimal? _Vat = new TvBo.Common().getTax(UserData, ResData.ResMain.ResDate, ResData.ResMain.ArrCity, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = _Vat.HasValue ? _Vat.Value.ToString("###.00") + "% SDD" : "", x = 471f, y = 625f, FontSize = 8 });
            string begDate = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "";
            string endDate = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "";
            writeList.Add(new TvReport.Coordinate { value = "Perioada : " + begDate + " - " + endDate, x = 70f, y = 505f, FontSize = 8 });
            string passengerNames = string.Empty;
            foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0))
            {
                if (passengerNames.Length > 0) passengerNames += " / ";
                passengerNames += row.Surname + " " + row.Name;
            }
            writeList.Add(new TvReport.Coordinate { value = "Participanti : " + passengerNames, x = 70f, y = 490f, FontSize = 8 });
            float _y = 480f;
            foreach (ResServiceRecord row in ResData.ResService.Where(w => w.StatSer < 2))
            {
                string ServiceDesc = string.Empty;
                switch (row.ServiceType)
                {
                    case "HOTEL":
                        ServiceDesc += row.ServiceName + " " + row.DepLocationNameL + " (" + row.Room + "," + row.Accom + "," + row.Board + ") " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "FLIGHT":
                        FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                        ServiceDesc += row.Service + " (" + flightDay.DepAirport + "->" + flightDay.ArrAirport + ")," + row.FlgClass.ToString() + ", " + row.BegDate.Value.ToShortDateString();
                        break;
                    case "TRANSPORT":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString();
                        break;
                    case "TRANSFER": TransferRecord trf = new Transfers().getTransfer(UserData.Market, row.Service, ref errorMsg);
                        if (trf != null && string.Equals(trf.Direction, "R"))
                            ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + ", " + row.EndDate.Value.ToShortDateString();
                        else ServiceDesc += row.ServiceName + " " + ", " + row.BegDate.Value.ToShortDateString();
                        break;
                    case "RENTING":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "EXCURSION":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString();
                        break;
                    case "INSURANCE":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "VISA":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "HANDFEE":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    default:
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                }
                _y -= 14f;
                writeList.Add(new TvReport.Coordinate { value = ServiceDesc, x = 70f, y = _y, FontSize = 8 });

            }
            _y -= 15f;
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value.ToString("#,###.00") : "", x = 500f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = "Supus regulilor", x = 514f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "speciale conf.", x = 514f, y = _y - 12f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "art.1521 CF", x = 514f, y = _y - 24f, FontSize = 8 });

            decimal? _parity = new TvBo.Common().Exchange(ResData.ResMain.PLMarket, ResData.ResMain.ResDate.Value, "EUR", "RON", 1, true, ref errorMsg);
            decimal? totSupDisEB = null;
            decimal salePrice = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value : 0;
            decimal invoicedAmount = ResData.ResMain.InvoicedAmount.HasValue ? ResData.ResMain.InvoicedAmount.Value : 0;
            decimal agencyPayable = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value : 0;
            decimal agencyCom = ResData.ResMain.AgencyCom.HasValue ? ResData.ResMain.AgencyCom.Value : 0;
            totSupDisEB = (salePrice - invoicedAmount) - agencyPayable - agencyCom;
            _y -= 14f;
            writeList.Add(new TvReport.Coordinate { value = totSupDisEB.HasValue && totSupDisEB.Value != 0 ? "Supplement / Discount" : "", x = 100f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = totSupDisEB.HasValue && totSupDisEB.Value != 0 ? totSupDisEB.Value.ToString("#,###.00") : "-", x = 500f, y = _y, FontSize = 8, RightToLeft = true });
            _y -= 14f;
            writeList.Add(new TvReport.Coordinate { value = agencyCom > 0 ? "Comision Cedat." : "", x = 100f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = agencyCom > 0 ? agencyCom.ToString("#,###.00") : "", x = 500f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = _parity.HasValue ? _parity.Value.ToString("#,###.0000") + " RON" : "", x = 66f, y = 156f, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = UserData.UserName.ToString(), x = 175f, y = 119f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 133f, y = 45f, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 505f, y = 110f, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = "0 " + ResData.ResMain.SaleCur, x = 570f, y = 110f, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 570f, y = 79f, FontSize = 8, RightToLeft = true });

            return new TvReport.RezedaReport.RezedaReport().createInvoice(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
        }
        catch (Exception Ex)
        {
            errorMsg = Ex.Message;
            return string.Empty;
        }
    }

    public string showInvoiceClient(User UserData, ResDataRecord ResData, ref string errorMsg)
    {
        try
        {
            List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
            AgencyRecord agency = UserData.AgencyRec;
            object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
            string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
            string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
            string basePageUrl = WebRoot.BasePageRoot;
            string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
            OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.Operator, ref errorMsg);
            AgencyRecord currentAgency = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
            AgencyRecord agent = new Agency().getAgency(currentAgency.UseMainCont ? (string.IsNullOrEmpty(currentAgency.MainOffice) ? currentAgency.Code : currentAgency.MainOffice) : currentAgency.Code, false, ref errorMsg);
            AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, false, ref errorMsg);
            if (agent == null) agent = new Agency().getAgency(currentAgency.Code, false, ref errorMsg);
            TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(currentAgency.Code, ref errorMsg);
            ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
            ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));
            //writeList.Add(new TvReport.Coordinate { value = "", x = 14f, y = 81.5f, FontSize = 10, bold = false });        
            writeList.Add(new TvReport.Coordinate { value = "(" + ResData.ResMain.ResNo + ")", x = 275f, y = 758f, FontSize = 12, bold = false });
            writeList.Add(new TvReport.Coordinate { value = leader.Surname + " " + leader.Name, x = 405f, y = 807f, FontSize = 8 });
            if (leaderInfo != null)
            {
                writeList.Add(new TvReport.Coordinate { value = leaderInfo.WorkTaxOffice.ToString(), x = 423f, y = 794f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = leaderInfo.WorkTaxAccNo.ToString(), x = 395f, y = 780f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWork.ToString() : leaderInfo.AddrHome.ToString(), x = 413f, y = 765f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkZip.ToString() + " " + leaderInfo.AddrWorkCity.ToString() : leaderInfo.AddrHomeZip.ToString() + " " + leaderInfo.AddrHomeCity.ToString(), x = 376f, y = 755f, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = (string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkCity.ToString() : leaderInfo.AddrHomeCity.ToString()) + " " + (string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkCountry : leaderInfo.AddrHomeCountry.ToString()), x = 406f, y = 743f, FontSize = 8 });
            }
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 452f, y = 654f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 502f, y = 639f, FontSize = 8 });
            decimal? _Vat = new TvBo.Common().getTax(UserData, ResData.ResMain.ResDate, ResData.ResMain.ArrCity, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = _Vat.HasValue ? _Vat.Value.ToString("###.00") + "% SDD" : "", x = 471f, y = 625f, FontSize = 8 });
            string begDate = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "";
            string endDate = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "";
            writeList.Add(new TvReport.Coordinate { value = "Perioada : " + begDate + " - " + endDate, x = 70f, y = 505f, FontSize = 8 });
            string passengerNames = string.Empty;
            foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0))
            {
                if (passengerNames.Length > 0) passengerNames += " / ";
                passengerNames += row.Surname + " " + row.Name;
            }
            writeList.Add(new TvReport.Coordinate { value = "Participanti : " + passengerNames, x = 70f, y = 490f, FontSize = 8 });
            float _y = 480f;
            foreach (ResServiceRecord row in ResData.ResService.Where(w => w.StatSer < 2))
            {
                string ServiceDesc = string.Empty;
                switch (row.ServiceType)
                {
                    case "HOTEL":
                        ServiceDesc += row.ServiceName + " " + row.DepLocationNameL + " (" + row.Room + "," + row.Accom + "," + row.Board + ") " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "FLIGHT":
                        FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                        ServiceDesc += row.Service + " (" + flightDay.DepAirport + "->" + flightDay.ArrAirport + ")," + row.FlgClass.ToString() + ", " + row.BegDate.Value.ToShortDateString();
                        break;
                    case "TRANSPORT":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString();
                        break;
                    case "TRANSFER": TransferRecord trf = new Transfers().getTransfer(UserData.Market, row.Service, ref errorMsg);
                        if (trf != null && string.Equals(trf.Direction, "R"))
                            ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + ", " + row.EndDate.Value.ToShortDateString();
                        else ServiceDesc += row.ServiceName + " " + ", " + row.BegDate.Value.ToShortDateString();
                        break;
                    case "RENTING":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "EXCURSION":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString();
                        break;
                    case "INSURANCE":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "VISA":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "HANDFEE":
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    default:
                        ServiceDesc += row.ServiceName + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                }
                _y -= 14f;
                writeList.Add(new TvReport.Coordinate { value = ServiceDesc, x = 70f, y = _y, FontSize = 8 });

            }
            _y -= 15f;
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value.ToString("#,###.00") : "", x = 500f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = "Supus regulilor", x = 514f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "speciale conf.", x = 514f, y = _y - 12f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "art.1521 CF", x = 514f, y = _y - 24f, FontSize = 8 });

            decimal? _parity = new TvBo.Common().Exchange(ResData.ResMain.PLMarket, ResData.ResMain.ResDate.Value, "EUR", "RON", 1, true, ref errorMsg);
            decimal? totSupDisEB = null;
            decimal salePrice = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value : 0;
            decimal invoicedAmount = ResData.ResMain.InvoicedAmount.HasValue ? ResData.ResMain.InvoicedAmount.Value : 0;
            decimal agencyPayable = ResData.ResMain.PasPayable.HasValue ? ResData.ResMain.PasPayable.Value : 0;
            decimal agencyCom = ResData.ResMain.EBPas.HasValue ? ResData.ResMain.EBPas.Value : 0;
            totSupDisEB = ResData.ResMain.PasSupDis.HasValue ? ResData.ResMain.PasSupDis.Value : 0;
            _y -= 14f;
            writeList.Add(new TvReport.Coordinate { value = totSupDisEB.HasValue && totSupDisEB.Value != 0 ? "Supplement / Discount" : "", x = 100f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = totSupDisEB.HasValue && totSupDisEB.Value != 0 ? totSupDisEB.Value.ToString("#,###.00") : "-", x = 500f, y = _y, FontSize = 8, RightToLeft = true });
            _y -= 14f;
            writeList.Add(new TvReport.Coordinate { value = agencyCom > 0 ? "Early Booking Discount" : "", x = 100f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = agencyCom > 0 ? agencyCom.ToString("#,###.00") : "", x = 500f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = _parity.HasValue ? _parity.Value.ToString("#,###.0000") + " RON" : "", x = 66f, y = 156f, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = UserData.UserName.ToString(), x = 175f, y = 119f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 133f, y = 45f, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.PasPayable.HasValue ? ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 505f, y = 110f, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = "0 " + ResData.ResMain.SaleCur, x = 570f, y = 110f, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "", x = 570f, y = 79f, FontSize = 8, RightToLeft = true });

            return new TvReport.RezedaReport.RezedaReport().createInvoiceClient(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
        }
        catch (Exception Ex)
        {
            errorMsg = Ex.Message;
            return string.Empty;
        }
    }

    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var _customersTicket = from H in ResData.ResService
                               join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                               join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                               join T in ResData.Title on C.Title equals T.TitleNo
                               join F in flights on H.Service equals F.Code
                               join A in airLines on F.Airline equals A.Code
                               where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                               select new
                               {
                                   CustNo = C.CustNo,
                                   Name = C.Name,
                                   SurName = C.Surname,
                                   TitleName = T.Code,
                                   ServiceID = H.RecID,
                                   DocStat = Rc.DocStat,
                                   DocNo = Rc.DocNo
                               };
        var customersTicket = from q in _customersTicket
                              group q by new
                              {
                                  CustNo = q.CustNo,
                                  Name = q.Name,
                                  SurName = q.SurName,
                                  TitleName = q.TitleName
                              } into k
                              select new
                              {
                                  k.Key.CustNo,
                                  k.Key.Name,
                                  k.Key.SurName,
                                  k.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;
            var custTicket = _customersTicket.Where(w => w.CustNo == row.CustNo).GroupBy(g => g.DocNo);

            Int16? DocStat = custTicket.Count() > 1 ?
                Convert.ToInt16(_customersTicket.Where(w => w.CustNo == row.CustNo && w.DocStat.HasValue && w.DocStat.Value == 1).GroupBy(g => g.DocNo).Count() > 0 ? 1 : 0) :
                _customersTicket.Where(w => w.CustNo == row.CustNo).FirstOrDefault().DocStat;
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        List<ResConRecord> resConList = ResData.ResCon;
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                ++cnt;
                writeList.Clear();
                List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                              join q3 in flyList on q1.Service equals q3.FlightNo
                                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                              select q1).ToList<ResServiceRecord>();


                AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);
                string agencyName_airlineName = UserData.AgencyName + " / " + (airLine != null ? airLine.Name : "");
                //writeList.Add(new TvReport.Coordinate { value = agencyName_airlineName, x = 30, y = 700, FontSize = 8 });

                string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());

                float _y = 0f;
                int cnt1 = 0;
                for (int i1 = 0; i1 < 3; i1++)
                {
                    switch (cnt1)
                    {
                        case 0: _y = 0f; break;
                        case 1: _y = 251f; break;
                        case 2: _y = 502f; break;
                    }


                    writeList.Add(new TvReport.Coordinate { value = eTicketNo, x = 173, y = 795 - _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = opr.FirmName.ToString(), x = 385, y = 810 - _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = opr.Address.ToString(), x = 385, y = 801 - _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = opr.Phone1.ToString() + " / " + opr.Fax1.ToString(), x = 385, y = 792 - _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = opr.Email1.ToString(), x = 385, y = 781 - _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = opr.Www.ToString(), x = 385, y = 772 - _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = "CHARTER", x = 26, y = 769 - _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = custFlights.FirstOrDefault().DepLocationName.ToString(), x = 26, y = 758 - _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 173, y = 763 - _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = resCust.Surname + " " + resCust.Name + "/ " + resCust.TitleStr, x = 28, y = 726 - _y, FontSize = 10 });

                    float _Y = 706;
                    foreach (ResServiceRecord row in custAirleineFlights.OrderBy(o => o.BegDate))
                    {
                        _Y -= 26;
                        FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                        writeList.Add(new TvReport.Coordinate { value = row.DepLocationName, x = 25, y = _Y - _y, FontSize = 8 });
                        writeList.Add(new TvReport.Coordinate { value = row.ArrLocationName, x = 25, y = _Y - _y - 10, FontSize = 8 });

                        writeList.Add(new TvReport.Coordinate { value = airLine.Name, x = 179, y = _Y - _y, FontSize = 8 });
                        writeList.Add(new TvReport.Coordinate { value = row.Service, x = 179, y = _Y - _y - 10, FontSize = 8 });

                        writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 252, y = _Y - _y, FontSize = 8 });

                        writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString(), x = 277, y = _Y - _y, FontSize = 8 });

                        writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToString("HH:mm") : "", x = 327, y = _Y - _y, FontSize = 8 });
                        writeList.Add(new TvReport.Coordinate { value = flightDay.ArrTime.HasValue ? flightDay.ArrTime.Value.ToString("HH:mm") : "", x = 413, y = _Y - _y, FontSize = 8 });
                        string confStat = string.Empty;
                        switch (row.StatConf)
                        {
                            case 0: confStat = resCust.Title < 8 ? "Request" : "NS"; break;
                            case 1: confStat = resCust.Title < 8 ? "OK" : "NS"; break;
                            case 2: confStat = resCust.Title < 8 ? "Not Confirm" : "NS"; break;
                            case 3: confStat = resCust.Title < 8 ? "No Show" : "NS"; break;
                        }
                        writeList.Add(new TvReport.Coordinate { value = confStat, x = 491, y = _Y - _y, FontSize = 8 });
                        decimal? bagWeight = null;
                        if (resCust.Title > 7)
                            bagWeight = flightDay.BagWeightForInf.HasValue ? flightDay.BagWeightForInf : flightDay.BagWeight;
                        else bagWeight = flightDay.BagWeight;
                        writeList.Add(new TvReport.Coordinate { value = bagWeight.HasValue ? bagWeight.Value.ToString("#,###.") : "", x = 526, y = _Y - _y, FontSize = 8 });


                        if (new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg))
                        {
                            ResConRecord resCon = resConList.Find(f => f.ResNo == row.ResNo && f.ServiceID == row.RecID && f.CustNo == CustNo);
                            resCon.DocNo = eTicketNo;
                            resCon.DocStat = 1;
                            resCon.DocIsDate = DateTime.Now;
                        }
                    }
                    ++cnt1;
                }

                string ServiceCode = "ETicket_" + row1.airline;
                eTicketList.Add(new TvReport.RezedaReport.RezedaReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
            }
            HttpContext.Current.Session["ResData"] = ResData;
            if (eTicketList.Count() == 1)
                return eTicketList.FirstOrDefault();
            else return new TvReport.RezedaReport.RezedaReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);
        }
        else return "";
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, resService.Service, ref errorMsg);

        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, false, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        TvReport.RezedaReport.AgencyDocAddressRecord agencyAddr = new TvReport.RezedaReport.RezedaReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.RezedaReport.RezedaReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);

        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;
        float _y = 0f;
        int cnt = 0;
        for (int i1 = 0; i1 < 3; i1++)
        {
            switch (cnt)
            {
                case 0: _y = 0f; break;
                case 1: _y = 271f; break;
                case 2: _y = 540f; break;
            }

            writeList.Add(new TvReport.Coordinate { value = agencyAddr.Name, x = 300, y = 818 - _y, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.Address + " " + agencyAddr.AddrZip + " " + agencyAddr.AddrCity, x = 208, y = 808 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.Phone1.ToString(), x = 220, y = 797 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.Fax1.ToString(), x = 352, y = 797 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.EMail1.ToString(), x = 208, y = 786 - _y, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 487, y = 800 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 502, y = 786 - _y, FontSize = 8 });

            float _Y = 763;
            float _X = 17;
            int i = 0;
            foreach (var row in custs)
            {
                _Y -= 11; i++;
                writeList.Add(new TvReport.Coordinate { value = row.TitleStr + ". " + row.Surname + " " + row.Name + " " + (row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : ""), x = _X, y = _Y - _y, FontSize = 8 });
            }

            writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 232, y = 768 - _y, FontSize = 8 });

            SupplierRecord supplier = new TvSystem().getSupplier(ResData.ResMain.PLMarket, resService.Supplier, ref errorMsg);
            if (supplier != null)
            {
                writeList.Add(new TvReport.Coordinate { value = supplier.Name, x = 324, y = 745 - _y, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = supplier.Phone1 + " " + supplier.Phone2 + " " + supplier.Fax1, x = 206, y = 734 - _y, FontSize = 7 });
            }
            writeList.Add(new TvReport.Coordinate { value = resService.SupNote.ToString(), x = 204, y = 756 - _y, FontSize = 7 });
            writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString(), x = 282, y = 718 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.EndDate.Value.ToShortDateString(), x = 432, y = 718 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.Duration.Value.ToString(), x = 564, y = 718 - _y, FontSize = 8 });

            string accomFullName = resService.Adult.HasValue && resService.Adult.Value > 0 ? resService.Adult.ToString() + " Adl" : "";
            accomFullName += resService.Child.HasValue && resService.Child.Value > 0 ? resService.Child.ToString() + " Chd" : "";
            writeList.Add(new TvReport.Coordinate { value = resService.Unit.ToString() + " " + resService.AccomName + "( " + accomFullName + ") - " + resService.RoomName, x = 204, y = 688 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.Board.ToString(), x = 547, y = 696 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = new TvReport.RezedaReport.RezedaReport().getTransferStr(ResData.ResMain.ResNo, ref errorMsg), x = 243, y = 674 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNote.ToString(), x = 258, y = 659 - _y, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = resService.Adult.ToString(), x = 72, y = 597 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.Child.ToString(), x = 157, y = 597 - _y, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = UserData.UserName.ToString(), x = 205, y = 604 - _y, FontSize = 8 });

            ++cnt;
        }
        return new TvReport.RezedaReport.RezedaReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

}




