﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Threading;

/// <summary>
/// Summary description for ResMonitorServices
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ResMonitorServices : System.Web.Services.WebService
{

    public ResMonitorServices()
    {
    }
    
    [WebMethod]
    public string getCommentData(string ResNo, string UserID, int lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        return new TvBo.ReservationMonitor().getCommentDataString(ResNo, UserID, lcID);
    }

    [WebMethod]
    public string setCommentData(string ResNo, string Comment, string UserID, int lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;
        retVal = new TvBo.ReservationMonitor().setComment(UserData, ResNo, Comment, lcID);
        return retVal;
    }
    
    [WebMethod]
    public string setReadCommentData(string ResNo, string UserID, int RecID, int lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;
        retVal = new TvBo.ReservationMonitor().setReadComment(UserID, ResNo, RecID, lcID);
        return retVal;
    }

    [WebMethod]
    public string getPaymentPlanData(string ResNo, int lcID)
    {
        //if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        //TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        //Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;
        retVal = new TvBo.ReservationMonitor().getPaymentPlanHTML(ResNo, lcID);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public string getAnnounce()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; 
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;        
        string errorMsg = string.Empty;
        //StringBuilder sb = new StringBuilder();
        List<TvBo.AnnounceForUser> allAnnounce = null;
        
        string cacheKey=string.Format("GetAnnounce_{0}-{1}-{2}",UserData.Market,UserData.AgencyID,UserData.UserID);
        CacheHelper.Get<List<TvBo.AnnounceForUser>>(cacheKey ,out allAnnounce);
        if (allAnnounce == null)
        {
            allAnnounce = new TvBo.Announces().getAnnounce(UserData, ref errorMsg);
            CacheHelper.Add<List<TvBo.AnnounceForUser>>(allAnnounce, cacheKey,0,15);
        }

        //List<TvBo.AnnounceForUser> allAnnounce = new TvBo.Announces().getAnnounce(UserData, ref errorMsg);
        
        if (allAnnounce != null && allAnnounce.Count > 0)
        {
            var query = allAnnounce.Where(w => w.Read == false).Select(s => s).ToList<TvBo.AnnounceForUser>();
            if (query.Count() > 0)
            {
                if (Equals(UserData.CustomRegID, "0970301"))
                    return "YES";
                else return "OK";
            }
            else return "NO";
        }
        else return "NO";
    }

    [WebMethod(EnableSession = true)]
    public string getAllAnnounce()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<TvBo.AnnounceRecord> allAnnounce = null;
        string errorMsg = string.Empty;
        string cacheKey = string.Format("GetAllAnnounce_{0}-{1}-{2}", UserData.Market, UserData.AgencyID, UserData.UserID);
        CacheHelper.Get<List<TvBo.AnnounceRecord>>(cacheKey, out allAnnounce);
        if (allAnnounce == null)
        {
            allAnnounce = new TvBo.Announces().getAllAnnounce(UserData, ref errorMsg);
            CacheHelper.Add<List<TvBo.AnnounceRecord>>(allAnnounce, cacheKey, 0, 15);
        }
        //StringBuilder sb = new StringBuilder();
        //List<TvBo.AnnounceRecord> allAnnounce = new TvBo.Announces().getAllAnnounce(UserData, ref errorMsg);
        if (allAnnounce != null && allAnnounce.Count > 0)
        {
            if (allAnnounce.Count() > 0)
            {
                string retVal = string.Empty;
                foreach (TvBo.AnnounceRecord row in allAnnounce)
                {
                    if (retVal.Length > 0) retVal += "<br /><br />";
                    retVal += row.Message;
                }
                return retVal;
            }
            else return "";
        }
        else return "";
    }

}

