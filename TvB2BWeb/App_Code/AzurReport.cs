﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using TvReport.SunReport;
using System.Text;
using TvTools;
using System.IO;
using System.Threading;

public class AzurReport
{
    public string showInvoice(User UserData, ResDataRecord ResData, ref string errorMsg)
    {
        try
        {
            List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
            AgencyRecord agency = UserData.AgencyRec;

            object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
            string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
            string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
            string basePageUrl = WebRoot.BasePageRoot;
            string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
            OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.Operator, ref errorMsg);
            AgencyRecord currentAgency = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
            AgencyRecord agent = new Agency().getAgency(currentAgency.UseMainCont ? (string.IsNullOrEmpty(currentAgency.MainOffice) ? currentAgency.Code : currentAgency.MainOffice) : currentAgency.Code, false, ref errorMsg);
            AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, false, ref errorMsg);
            if (agent == null) agent = new Agency().getAgency(currentAgency.Code, false, ref errorMsg);
            TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(currentAgency.Code, ref errorMsg);
            ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
            ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));
            //writeList.Add(new TvReport.Coordinate { value = "", x = 14f, y = 81.5f, FontSize = 10, bold = false });        
            writeList.Add(new TvReport.Coordinate { value = agency.RegisterCode, x = 415f, y = 731f, FontSize = 10 });
            writeList.Add(new TvReport.Coordinate { value = agency.TaxAccNo, x = 380f, y = 714f, FontSize = 10 });
            writeList.Add(new TvReport.Coordinate { value = agency.AccountCode, x = 466f, y = 714f, FontSize = 10 });
            writeList.Add(new TvReport.Coordinate { value = string.IsNullOrEmpty(agency.FirmName) ? agency.Name : agency.FirmName, x = 356f, y = 692f, FontSize = 12 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.InvAddress, x = 356f, y = 682f, FontSize = 10 });
            writeList.Add(new TvReport.Coordinate { value = agencyAddr.InvAddrZip + " " + agencyAddr.InvAddrCity, x = 356f, y = 638f, FontSize = 10 });
            var query = from q in ResData.ResService
                        where string.Equals(q.ServiceType, "FLIGHT")
                        select new { q.BegDate, q.Service };
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 160f, y = 701f, FontSize = 12, bold = true });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 176f, y = 677f, FontSize = 10 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 110f, y = 659f, FontSize = 8 });
            if (query != null && query.Count() > 0)
            {
                FlightDayRecord flightDay = new Flights().getFlightDay(UserData, query.FirstOrDefault().Service, query.FirstOrDefault().BegDate.Value, ref errorMsg);
                writeList.Add(new TvReport.Coordinate { value = flightDay.FlyDate.ToShortDateString(), x = 110f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.FlightNo, x = 165f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 215f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToShortTimeString() : "", x = 245f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 275f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.ArrTime.HasValue ? flightDay.ArrTime.Value.ToShortTimeString() : "", x = 305f, y = 642f, FontSize = 8 });
                if (query.Count() > 1)
                {
                    flightDay = new Flights().getFlightDay(UserData, query.LastOrDefault().Service, query.LastOrDefault().BegDate.Value, ref errorMsg);
                    writeList.Add(new TvReport.Coordinate { value = flightDay.FlyDate.ToShortDateString(), x = 110f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.FlightNo, x = 165f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 215f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToShortTimeString() : "", x = 245f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 275f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrTime.HasValue ? flightDay.ArrTime.Value.ToShortTimeString() : "", x = 305f, y = 625f, FontSize = 8 });
                }
            }
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Days.ToString() + "  nocí", x = 110f, y = 608f, FontSize = 8 });
            var hotel = from q in ResData.ResService
                        where string.Equals(q.ServiceType, "HOTEL")
                        select q;
            if (hotel != null && hotel.Count() > 0)
            {
                HotelRecord _hotel = new Hotels().getHotelDetail(UserData, hotel.FirstOrDefault().Service, ref errorMsg);
                writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, _hotel.Location, ref errorMsg) + " / " + _hotel.LocationName, x = 110f, y = 591f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = _hotel.Name + " (" + _hotel.Category + ")", x = 110f, y = 574f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = hotel.FirstOrDefault().BoardName, x = 110f, y = 560f, FontSize = 8 });
                string accomStr = new TvReport.AzurReport.AzurReport().getAccomStr(ResData.ResMain.ResNo, ref errorMsg);
                writeList.Add(new TvReport.Coordinate { value = accomStr, x = 110f, y = 545f, FontSize = 8 });
            }

            TvReport.AzurReport.PaxCountRecord paxCounts = new TvReport.AzurReport.AzurReport().getPaxCount(ResData.ResMain.ResNo, ref errorMsg);
            if (paxCounts != null)
            {
                writeList.Add(new TvReport.Coordinate { value = paxCounts.AdlCnt.ToString(), x = 160f, y = 528f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = paxCounts.ChdCnt.ToString(), x = 190f, y = 528f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = paxCounts.InfCnt.ToString(), x = 229f, y = 528f, FontSize = 8 });
            }
            writeList.Add(new TvReport.Coordinate { value = leader != null ? (leader.TitleStr + ". " + leader.Name + " " + leader.Surname) : "", x = 129f, y = 485f, FontSize = 8 });
            if (leaderInfo != null)
            {
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWork + " " + leaderInfo.AddrWorkZip + " " + leaderInfo.AddrWorkCity : leaderInfo.AddrHome + " " + leaderInfo.AddrHomeZip + " " + leaderInfo.AddrHomeCity, x = 129f, y = 468f, FontSize = 10 });
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkEMail : leaderInfo.AddrHomeEmail, x = 129f, y = 451f, FontSize = 10 });
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkTel : leaderInfo.AddrHomeTel, x = 129f, y = 434f, FontSize = 10 });
                writeList.Add(new TvReport.Coordinate { value = leaderInfo.MobTel, x = 276f, y = 434f, FontSize = 10 });
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkFax : leaderInfo.AddrHomeFax, x = 432f, y = 434f, FontSize = 10 });
            }
            int i = 0;
            float _x = 33f;
            float _y = 400f;
            foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0).ToList<ResCustRecord>())
            {
                if (i == 4)
                {
                    i = 0;
                    _x += 312f;
                }
                _y = 400f - (i * 13.5f);
                writeList.Add(new TvReport.Coordinate { value = row.TitleStr + ". " + row.Name + " " + row.Surname, x = _x, y = _y, FontSize = 8 });
                ++i;
            }
            List<TvReport.AzurReport.PasPriceRecord> price = new TvReport.AzurReport.AzurReport().getPasPriceList(ResData.ResMain.ResNo, ref errorMsg);
            _y = 316f;
            foreach (TvReport.AzurReport.PasPriceRecord row in price)
            {
                writeList.Add(new TvReport.Coordinate { value = row.ServiceDesc, x = 33f, y = _y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.RecType != 5 ? (row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "") : "", x = 312f, y = _y, FontSize = 8, RightToLeft = true });
                writeList.Add(new TvReport.Coordinate { value = row.Unit.ToString(), x = 345f, y = _y, FontSize = 8 });
                decimal tSalePrice = (row.SalePrice.HasValue ? row.SalePrice.Value : 0) * (row.Unit.HasValue ? row.Unit.Value : 0);
                writeList.Add(new TvReport.Coordinate { value = tSalePrice.ToString("#,###.00") + " " + ResData.ResMain.SaleCur, x = 552f, y = _y, FontSize = 8, RightToLeft = true });
                _y -= 13.5f;
            }
            _y -= 13.5f;
            writeList.Add(new TvReport.Coordinate { value = "Celkem včetně provize:", x = 460f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.SalePrice.HasValue ? (ResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "", x = 552f, y = _y, FontSize = 8, RightToLeft = true });
            _y -= 13.5f;
            writeList.Add(new TvReport.Coordinate { value = "Provize:", x = 460f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyCom.HasValue ? (ResData.ResMain.AgencyCom.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "", x = 552f, y = _y, FontSize = 8, RightToLeft = true });
            _y -= 13.5f;
            writeList.Add(new TvReport.Coordinate { value = "K úhradě celkem:", x = 460f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? (ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "", x = 552f, y = _y, FontSize = 8, RightToLeft = true });

            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 120f, y = 199f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "", x = 120f, y = 185f, FontSize = 8 });

            List<resPayPlanRecord> resPayPlan = new ReservationMonitor().getPaymentPlan(ResData.ResMain.ResNo, ref errorMsg);
            if (resPayPlan != null && resPayPlan.Count > 0)
            {
                _x = 33f;
                _y = 150f;
                foreach (resPayPlanRecord row in resPayPlan.OrderBy(o => o.PayNo))
                {
                    writeList.Add(new TvReport.Coordinate { value = (row.PayNo == 1 ? "Záloha" : (row.PayNo > 1 ? "Doplatek" : "")), x = _x, y = _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.DueDate.ToShortDateString(), x = 96f, y = _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.Amount.HasValue ? row.Amount.Value.ToString("#,###.00") + " " + row.Cur : "", x = 228f, y = _y, FontSize = 8, RightToLeft = true });
                    _y -= 13.5f;
                }
            }
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString(), x = 80f, y = 47f, FontSize = 8 });

            return new TvReport.AzurReport.AzurReport().createInvoice(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
        }
        catch (Exception Ex)
        {
            errorMsg = Ex.Message;
            return string.Empty;
        }
    }

    public string showInvoiceClient(User UserData, ResDataRecord ResData, ref string errorMsg)
    {
        try
        {
            List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
            AgencyRecord agency = UserData.AgencyRec;
            object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
            string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
            string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
            string basePageUrl = WebRoot.BasePageRoot;
            string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
            OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.Operator, ref errorMsg);
            AgencyRecord currentAgency = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
            AgencyRecord agent = new Agency().getAgency(currentAgency.UseMainCont ? (string.IsNullOrEmpty(currentAgency.MainOffice) ? currentAgency.Code : currentAgency.MainOffice) : currentAgency.Code, false, ref errorMsg);
            AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, false, ref errorMsg);
            if (agent == null) agent = new Agency().getAgency(currentAgency.Code, false, ref errorMsg);
            TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(currentAgency.Code, ref errorMsg);
            ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
            ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));
            //writeList.Add(new TvReport.Coordinate { value = "", x = 14f, y = 81.5f, FontSize = 10, bold = false });        
            writeList.Add(new TvReport.Coordinate { value = agency.RegisterCode, x = 415f, y = 731f, FontSize = 10 });
            writeList.Add(new TvReport.Coordinate { value = agency.TaxAccNo, x = 380f, y = 714f, FontSize = 10 });
            writeList.Add(new TvReport.Coordinate { value = agency.AccountCode, x = 466f, y = 714f, FontSize = 10 });
            writeList.Add(new TvReport.Coordinate { value = leader.TitleStr + ". " + leader.Name + " " + leader.Surname, x = 356f, y = 692f, FontSize = 12 });
            if (leaderInfo != null)
            {
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWork : leaderInfo.AddrHome, x = 356f, y = 682f, FontSize = 10 });
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkZip + " " + leaderInfo.AddrWorkCity : leaderInfo.AddrHomeZip + " " + leaderInfo.AddrHomeCity, x = 356f, y = 638f, FontSize = 10 });
            }
            var query = from q in ResData.ResService
                        where string.Equals(q.ServiceType, "FLIGHT")
                        select new { q.BegDate, q.Service };
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 160f, y = 701f, FontSize = 12, bold = true });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 176f, y = 677f, FontSize = 10 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 110f, y = 659f, FontSize = 8 });
            if (query != null && query.Count() > 0)
            {
                FlightDayRecord flightDay = new Flights().getFlightDay(UserData, query.FirstOrDefault().Service, query.FirstOrDefault().BegDate.Value, ref errorMsg);
                writeList.Add(new TvReport.Coordinate { value = flightDay.FlyDate.ToShortDateString(), x = 110f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.FlightNo, x = 165f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 215f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToShortTimeString() : "", x = 245f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 275f, y = 642f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = flightDay.ArrTime.HasValue ? flightDay.ArrTime.Value.ToShortTimeString() : "", x = 305f, y = 642f, FontSize = 8 });
                if (query.Count() > 1)
                {
                    flightDay = new Flights().getFlightDay(UserData, query.LastOrDefault().Service, query.LastOrDefault().BegDate.Value, ref errorMsg);
                    writeList.Add(new TvReport.Coordinate { value = flightDay.FlyDate.ToShortDateString(), x = 110f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.FlightNo, x = 165f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 215f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToShortTimeString() : "", x = 245f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 275f, y = 625f, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrTime.HasValue ? flightDay.ArrTime.Value.ToShortTimeString() : "", x = 305f, y = 625f, FontSize = 8 });
                }
            }
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.Days.ToString() + "  nocí", x = 110f, y = 608f, FontSize = 8 });
            var hotel = from q in ResData.ResService
                        where string.Equals(q.ServiceType, "HOTEL")
                        select q;
            if (hotel != null && hotel.Count() > 0)
            {
                HotelRecord _hotel = new Hotels().getHotelDetail(UserData, hotel.FirstOrDefault().Service, ref errorMsg);
                writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, _hotel.Location, ref errorMsg) + " / " + _hotel.LocationName, x = 110f, y = 591f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = _hotel.Name + " (" + _hotel.Category + ")", x = 110f, y = 574f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = hotel.FirstOrDefault().BoardName, x = 110f, y = 560f, FontSize = 8 });
                string accomStr = new TvReport.AzurReport.AzurReport().getAccomStr(ResData.ResMain.ResNo, ref errorMsg);
                writeList.Add(new TvReport.Coordinate { value = accomStr, x = 110f, y = 545f, FontSize = 8 });
            }

            TvReport.AzurReport.PaxCountRecord paxCounts = new TvReport.AzurReport.AzurReport().getPaxCount(ResData.ResMain.ResNo, ref errorMsg);
            if (paxCounts != null)
            {
                writeList.Add(new TvReport.Coordinate { value = paxCounts.AdlCnt.ToString(), x = 160f, y = 528f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = paxCounts.ChdCnt.ToString(), x = 190f, y = 528f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = paxCounts.InfCnt.ToString(), x = 229f, y = 528f, FontSize = 8 });
            }
            writeList.Add(new TvReport.Coordinate { value = leader != null ? (leader.TitleStr + ". " + leader.Name + " " + leader.Surname) : "", x = 129f, y = 485f, FontSize = 8 });
            if (leaderInfo != null)
            {
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWork + " " + leaderInfo.AddrWorkZip + " " + leaderInfo.AddrWorkCity : leaderInfo.AddrHome + " " + leaderInfo.AddrHomeZip + " " + leaderInfo.AddrHomeCity, x = 129f, y = 468f, FontSize = 10 });
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkEMail : leaderInfo.AddrHomeEmail, x = 129f, y = 451f, FontSize = 10 });
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkTel : leaderInfo.AddrHomeTel, x = 129f, y = 434f, FontSize = 10 });
                writeList.Add(new TvReport.Coordinate { value = leaderInfo.MobTel, x = 276f, y = 434f, FontSize = 10 });
                writeList.Add(new TvReport.Coordinate { value = string.Equals(leaderInfo.InvoiceAddr, "W") ? leaderInfo.AddrWorkFax : leaderInfo.AddrHomeFax, x = 432f, y = 434f, FontSize = 10 });
            }
            int i = 0;
            float _x = 33f;
            float _y = 400f;
            foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0).ToList<ResCustRecord>())
            {
                if (i == 4)
                {
                    i = 0;
                    _x += 312f;
                }
                _y = 400f - (i * 13.5f);
                writeList.Add(new TvReport.Coordinate { value = row.TitleStr + ". " + row.Name + " " + row.Surname, x = _x, y = _y, FontSize = 8 });
                ++i;
            }
            List<TvReport.AzurReport.PasPriceRecord> price = new TvReport.AzurReport.AzurReport().getPasPriceList(ResData.ResMain.ResNo, ref errorMsg);
            _y = 316f;
            foreach (TvReport.AzurReport.PasPriceRecord row in price)
            {
                writeList.Add(new TvReport.Coordinate { value = row.ServiceDesc, x = 33f, y = _y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = row.RecType != 5 ? (row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "") : "", x = 312f, y = _y, FontSize = 8, RightToLeft = true });
                writeList.Add(new TvReport.Coordinate { value = row.Unit.ToString(), x = 345f, y = _y, FontSize = 8 });
                decimal tSalePrice = (row.SalePrice.HasValue ? row.SalePrice.Value : 0) * (row.Unit.HasValue ? row.Unit.Value : 0);
                writeList.Add(new TvReport.Coordinate { value = tSalePrice.ToString("#,###.00") + " " + ResData.ResMain.SaleCur, x = 552f, y = _y, FontSize = 8, RightToLeft = true });
                _y -= 13.5f;
            }
            _y -= 13.5f;
            writeList.Add(new TvReport.Coordinate { value = "Celkem včetně provize :", x = 460f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.SalePrice.HasValue ? (ResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "", x = 552f, y = _y, FontSize = 8, RightToLeft = true });
            _y -= 13.5f;
            writeList.Add(new TvReport.Coordinate { value = "K úhradě celkem:", x = 460f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.PasPayable.HasValue ? (ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "", x = 552f, y = _y, FontSize = 8, RightToLeft = true });

            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 120f, y = 199f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "", x = 120f, y = 185f, FontSize = 8 });

            List<resPayPlanRecord> resPayPlan = new ReservationMonitor().getPaymentPlan(ResData.ResMain.ResNo, ref errorMsg);
            if (resPayPlan != null && resPayPlan.Count > 0)
            {
                _x = 33f;
                _y = 150f;
                foreach (resPayPlanRecord row in resPayPlan.OrderBy(o => o.PayNo))
                {
                    writeList.Add(new TvReport.Coordinate { value = (row.PayNo == 1 ? "Záloha" : (row.PayNo > 1 ? "Doplatek" : "")), x = _x, y = _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.DueDate.ToShortDateString(), x = 96f, y = _y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.Amount.HasValue ? row.Amount.Value.ToString("#,###.00") + " " + row.Cur : "", x = 228f, y = _y, FontSize = 8, RightToLeft = true });
                    _y -= 13.5f;
                }
            }
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString(), x = 80f, y = 47f, FontSize = 8 });

            return new TvReport.AzurReport.AzurReport().createInvoice(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
        }
        catch (Exception Ex)
        {
            errorMsg = Ex.Message;
            return string.Empty;
        }
    }

    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var _customersTicket = from H in ResData.ResService
                               join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                               join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                               join T in ResData.Title on C.Title equals T.TitleNo
                               join F in flights on H.Service equals F.Code
                               join A in airLines on F.Airline equals A.Code
                               where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                               select new
                               {
                                   CustNo = C.CustNo,
                                   Name = C.Name,
                                   SurName = C.Surname,
                                   TitleName = T.Code,
                                   ServiceID = H.RecID,
                                   DocStat = Rc.DocStat,
                                   DocNo = Rc.DocNo
                               };
        var customersTicket = from q in _customersTicket
                              group q by new
                              {
                                  CustNo = q.CustNo,
                                  Name = q.Name,
                                  SurName = q.SurName,
                                  TitleName = q.TitleName
                              } into k
                              select new
                              {
                                  k.Key.CustNo,
                                  k.Key.Name,
                                  k.Key.SurName,
                                  k.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;
            var custTicket = _customersTicket.Where(w => w.CustNo == row.CustNo).GroupBy(g => g.DocNo);

            Int16? DocStat = custTicket.Count() > 1 ?
                Convert.ToInt16(_customersTicket.Where(w => w.CustNo == row.CustNo && w.DocStat.HasValue && w.DocStat.Value == 1).GroupBy(g => g.DocNo).Count() > 0 ? 1 : 0) :
                _customersTicket.Where(w => w.CustNo == row.CustNo).FirstOrDefault().DocStat;
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        List<ResConRecord> resConList = ResData.ResCon;
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                ++cnt;
                writeList.Clear();
                List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                              join q3 in flyList on q1.Service equals q3.FlightNo
                                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                              select q1).ToList<ResServiceRecord>();


                AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);
                string agencyName_airlineName = UserData.AgencyName + " / " + (airLine != null ? airLine.Name : "");
                //writeList.Add(new TvReport.Coordinate { value = agencyName_airlineName, x = 30, y = 700, FontSize = 8 });

                string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());

                writeList.Add(new TvReport.Coordinate { value = resCust.Surname + " / " + resCust.Name + " " + resCust.TitleStr, x = 168, y = 698, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = agency.Name, x = 168, y = 678, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = agency.Fax1, x = 168, y = 658, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = agency.Phone1, x = 320, y = 658, FontSize = 8 });
                string arrCountry = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg);
                string destination = string.Empty;
                TvReport.AzurReport.ETicketHotelRecord eTicketHotel = new TvReport.AzurReport.AzurReport().getETicketHotelDestination(ResData.ResMain.ResNo, ref errorMsg);
                if (eTicketHotel != null)
                {
                    destination += " - " + eTicketHotel.Location.ToString() + " - " + eTicketHotel.Name.ToString();
                    writeList.Add(new TvReport.Coordinate { value = "Od " + eTicketHotel.BegDate.Value.ToShortDateString() + " do " + eTicketHotel.EndDate.Value.ToShortDateString(), x = 168, y = 618, FontSize = 8 });
                }
                writeList.Add(new TvReport.Coordinate { value = arrCountry + destination, x = 168, y = 638, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 168, y = 598, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = eTicketNo, x = 168, y = 581, FontSize = 8 });

                float _Y = 533;
                foreach (ResServiceRecord row in custAirleineFlights.OrderBy(o => o.BegDate))
                {
                    FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    writeList.Add(new TvReport.Coordinate { value = row.DepLocationName + " / " + row.ArrLocationName, x = 40, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.Service, x = 217, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 290, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString(), x = 320, y = _Y, FontSize = 8 });
                    DateTime? lastCheckIn = null;
                    try { lastCheckIn = Convert.ToDateTime(flightDay.DepTime.Value.Ticks - flightDay.CountCloseTime.Value.Ticks); }
                    catch { }
                    writeList.Add(new TvReport.Coordinate { value = lastCheckIn.HasValue ? Convert.ToDateTime(lastCheckIn.Value).ToShortTimeString() : "", x = 382, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToString("HH:mm") : "", x = 422, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrTime.HasValue ? flightDay.ArrTime.Value.ToString("HH:mm") : "", x = 465, y = _Y, FontSize = 8 });
                    string confStat = string.Empty;
                    switch (row.StatConf)
                    {
                        case 0: confStat = resCust.Title < 8 ? "Request" : "NS"; break;
                        case 1: confStat = resCust.Title < 8 ? "OK" : "NS"; break;
                        case 2: confStat = resCust.Title < 8 ? "Not Confirm" : "NS"; break;
                        case 3: confStat = resCust.Title < 8 ? "No Show" : "NS"; break;
                    }
                    writeList.Add(new TvReport.Coordinate { value = confStat, x = 512, y = _Y, FontSize = 8 });
                    decimal? bagWeight = null;
                    if (resCust.Title > 7)
                        bagWeight = flightDay.BagWeightForInf.HasValue ? flightDay.BagWeightForInf : flightDay.BagWeight;
                    else bagWeight = flightDay.BagWeight;
                    writeList.Add(new TvReport.Coordinate { value = bagWeight.HasValue ? bagWeight.Value.ToString("#,###.") : "", x = 543, y = _Y, FontSize = 8 });

                    _Y -= 12;
                    if (new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg))
                    {
                        ResConRecord resCon = resConList.Find(f => f.ResNo == row.ResNo && f.ServiceID == row.RecID && f.CustNo == CustNo);
                        resCon.DocNo = eTicketNo;
                        resCon.DocStat = 1;
                        resCon.DocIsDate = DateTime.Now;
                    }
                }

                string eTest =
@"<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br />
<p>Vážení přátelé,
věnujte prosím zvýšenou pozornost následujícím informacím:
</p>
<p>
Místo srazu: ";
                eTest += "PRO ODLET Z PRAHY: na pevném stánku CK Azur Reizen, který je v terminálu Sever 1, u letištního vchodu \"F\" následně doprava - 3. stánek. Letiště Praha - Ruzyně.";
                eTest +=
@"</p>
<p>
PRO ODLET Z BRNA: u přepážky označené logem CK Azur Reizen, odletová hala letiště Brno - Tuřany.
</p>
<p>
ČAS SRAZU:  2 hodiny před odletem.
Check- in přepážky odbavení se uzavírají 45 min. před plánovaným časem odletu letadla, následně již není bohužel 
možné se odbavit a vycestovat. Berte,prosím tuto informaci v potaz při plánování Vaší dopravy na letiště.
</p>
<p>
POVOLENÁ VÁHA ZAVAZADEL NA OSOBU: 20 kg - platí pro letecké společnosti: ČSA - zkratka letu OKxxxx, Travel 
Service - zkratka letu QSxxx. nOUVEL air - zkratka letu BJ ....Sdružování váhy zavazadel je možné, avšak maximální 
váha 1 zavazadla nesmí překročit 32 kg. Na palubě je povoleno pouze 1 příruční zavazadlo o váze max. 5 kg a o 
maximálních rozměrech 55x40x20cm. V případě překročení povolené váhy je letecká společnost oprávněna účtovat 
poplatek za každý kg váhy navíc.
</p>
<p>
Zájezdy odbavuje na letišti u přepážek či ve stánku pro cestovní kanceláře zástupce CK Azur Reizen, který Vám předá 
letenky, voucher na ubytování a doklady o pojištění. Pokud dojde ke škodě na Vašem zavazadle či k jeho ztrátě během 
přepravy, nechte si ještě před opuštěním odbavovacího prostoru vystavit protokol o škodě (P. I. R.) u přepážky reklamací. 
Na palubě letadla dostanete vstupní a výstupní kartu (platí pro Tabarku, Tunisko a Egypt), kterou si vyplníte a poté 
odevzdáte po příletu u pasové kontroly.
</p>
<p>
JAK SE DOSTANETE NA LETIŠTĚ PRAHA - RUZYNĚ:
Na letiště jezdí autobusy MHD (č.119 a 254 ze stanice metra A- Dejvická, č.100 ze stanice metra B- Zličín, č.179 a 225 ";
                eTest += "ze stanice metra B- Nové Butovice nebo autobus \"Airport express\" ze stanice metra C- Hlavní nádraží, zastávka se ";
                eTest +=
 @"nalézá u magistrály, před historickou budovou nádraží). Podrobnější informace získáte na www.dp-praha.cz nebo na tel. 
296 191111.
</p>
<p>
BEZPEČNOSTNÍ PRAVIDLA NA LETIŠTI PŘI CESTOVÁNÍ ZE STÁTŮ EVROPSKÉ UNIE ( Z ČESKÉ REPUBLIKY, PLATÍ I ZPĚT PŘI CESTĚ Z ŘECKA):
Mějte na paměti, že s sebou do kabiny letadla smíte vzít pouze malá množství tekutin, a to jak v příručním zavazadle, 
které berete s sebou na palubu, tak u sebe. Tekutiny musí být v malém balení do max. objemu 100 mililitrů. Všechna 
balení musí být umístěna v jednom průhledném, uzavíratelném, igelitovém sáčku o maximálním obsahu 1 litr na osobu. 
Tekutinami se rozumí: voda a další nápoje, polévky, sirupy, krémy, pleťová mléka a oleje, parfémy, spreje, gely vč. 
šamponů a sprchových gelů, nádoby pod tlakem včetně pěny na holení, pěnových tužidel, dalších pěnových přípravků 
a deodorantů, pasty vč. zubní pasty, směsi tuhých a tekutých materiálů, všechny další materiály podobné konzistence.
</p>
";

                //writeList.Add(new TvReport.Coordinate { value = "<div style=\"font-size:7pt;\">" + eTest + "</div>", x = 36, y = 480, FontSize = 8, Type = TvReport.writeType.Html });

                string ServiceCode = "ETicket_" + row1.airline;
                eTicketList.Add(new TvReport.AzurReport.AzurReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
            }
            HttpContext.Current.Session["ResData"] = ResData;
            if (eTicketList.Count() == 1)
                return eTicketList.FirstOrDefault();
            else return new TvReport.AzurReport.AzurReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);
        }
        else return "";
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, resService.Service, ref errorMsg);
        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;
        float _y = 0f;
        int cnt = 0;
        for (int i1 = 0; i1 < 3; i1++)
        {
            switch (cnt)
            {
                case 0: _y = 0f; break;
                case 1: _y = 271f; break;
                case 2: _y = 540f; break;
            }
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 474, y = 800 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 479, y = 786 - _y, FontSize = 8 });

            float _Y = 767;
            float _X = 17;
            int i = 0;
            foreach (var row in custs)
            {
                _Y -= 11; i++;
                writeList.Add(new TvReport.Coordinate { value = row.TitleStr + ". " + row.Surname + " " + row.Name + " " + (row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : ""), x = _X, y = _Y - _y, FontSize = 8 });
            }

            writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 237, y = 765 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, resService.DepLocation.Value, ref errorMsg).ToString(), x = 245, y = 742 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = hotel.LocationName, x = 448, y = 742 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString(), x = 248, y = 719 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.EndDate.Value.ToShortDateString(), x = 410, y = 719 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.Duration.Value.ToString(), x = 532, y = 719 - _y, FontSize = 8 });

            string accomFullName = resService.Adult.HasValue && resService.Adult.Value > 0 ? resService.Adult.ToString() + " Adl" : "";
            accomFullName += resService.Child.HasValue && resService.Child.Value > 0 ? resService.Child.ToString() + " Chd" : "";
            writeList.Add(new TvReport.Coordinate { value = resService.Unit.ToString() + " " + resService.AccomName + "( " + accomFullName + ") - " + resService.RoomName, x = 245, y = 697 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.Board, x = 529, y = 697 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = new TvReport.AzurReport.AzurReport().getTransferStr(ResData.ResMain.ResNo, ref errorMsg), x = 246, y = 676 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNote.ToString(), x = 233, y = 658 - _y, FontSize = 8 });
            List<HotelHandicapRecord> hotelHandicaps = new Hotels().getHotelHandicaps(UserData.Market, ResData.ResMain.PLMarket, resService.Service, resService.BegDate, resService.EndDate, ref errorMsg);
            if (hotelHandicaps != null && hotelHandicaps.Count > 0)
                writeList.Add(new TvReport.Coordinate { value = string.IsNullOrEmpty(hotelHandicaps.FirstOrDefault().NameL) ? hotelHandicaps.FirstOrDefault().NameL : hotelHandicaps.FirstOrDefault().Name, x = 313, y = 636 - _y, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = resService.Adult.ToString(), x = 40, y = 600 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.Child.ToString(), x = 126, y = 600 - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.Child.ToString(), x = 248, y = 613 - _y, FontSize = 8 });

            ++cnt;
        }
        return new TvReport.AzurReport.AzurReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showContract(User UserData, ResDataRecord ResData, ref string errorMsg)
    {
        try
        {
            List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
            AgencyRecord agency = UserData.AgencyRec;

            object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
            string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
            string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
            string basePageUrl = WebRoot.BasePageRoot;
            string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
            OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.Operator, ref errorMsg);
            AgencyRecord currentAgency = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
            AgencyRecord agent = new Agency().getAgency(currentAgency.UseMainCont ? (string.IsNullOrEmpty(currentAgency.MainOffice) ? currentAgency.Code : currentAgency.MainOffice) : currentAgency.Code, false, ref errorMsg);
            AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, false, ref errorMsg);
            if (agent == null) agent = new Agency().getAgency(currentAgency.Code, false, ref errorMsg);
            TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(currentAgency.Code, ref errorMsg);
            ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
            ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));
            //writeList.Add(new TvReport.Coordinate { value = "", x = 14f, y = 81.5f, FontSize = 10, bold = false });        

            writeList.Add(new TvReport.Coordinate { value = agent.Name, x = 21f, y = 825f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = agent.Name, x = 322f, y = 767f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = agent.Address + " " + agent.AddrZip + " " + agent.AddrCity, x = 322f, y = 749f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = agent.Email1, x = 322f, y = 738f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = "Tel :" + agent.Phone1 + "  Fax :" + agent.Fax1, x = 322f, y = 726f, FontSize = 10, bold = false });

            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 138f, y = 677f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityName, x = 138f, y = 662f, FontSize = 10, bold = false });

            writeList.Add(new TvReport.Coordinate { value = "", x = 0f, y = 0f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = "", x = 0f, y = 0f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = "", x = 0f, y = 0f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = "", x = 0f, y = 0f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = "", x = 0f, y = 0f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = "", x = 0f, y = 0f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = "", x = 0f, y = 0f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = "", x = 0f, y = 0f, FontSize = 10, bold = false });
            writeList.Add(new TvReport.Coordinate { value = "", x = 0f, y = 0f, FontSize = 10, bold = false });

            /*
Prohlašuji,ze jsem prevzal katalog porádající cestovní kanceláre ( nebo dodatecnou písemnou nabídku zájezdu ) s uvedením 
hlavních charakteristik ubytovacího místa, polohy, kategorie, vybavenosti ubytování a zpusobem dopravy. Dále stvrzuji, 
ze je mi znám obsah všeobecnych podmínek porádající cestovní kanceláre a obsah pojistnych podmínek pojišt'ovny a ze s nimi 
souhlasím a podrizuji se jim. Prohlašuji tímto, ze povinnostem mnou prihlášenych cestujících vuci porádající cestovní 
kancelári dostojím jako svym vlastním. Souhlasím, aby osobní údaje vcetne rodného císla uvedené na této smlouve byly 
zahrnuté do elektronické databáze zastupující cestovní kanceláre a v souladu se zákonem c.101/2000 Sb. dále zpracovány 
pouze pro potreby porádající cestovní kanceláre.Nedílnou soucástí sluzeb je nabídka cestovního pojištení. Beru na vedomí, 
ze nesjednáním pripojištení prebírám plnou odpovednost za dusledky škod vzniklych v souvislosti se zájezdem. Potvrzuji, 
ze pred uzavrením pojistné smlouvy jsem byl/a seznámen/a s pojistnymi podmínkami, které jsem prevzal/a, a ze mi byly 
poskytnuty informace o pojistiteli a o pojistném vztahu dle §66 zák. c. 37/2004 Sb. Na základ zmocnení udeluji souhlas 
rovnez jménem spolucestujících osob.Prohlašuji, ze veškeré mnou uvedené údaje jsou pravdivé.Souhlasím s tím, ze veškerá 
korespondence v elektronické podobe související s objednáním zájezdu me zavazuje ke splnení stejnych podmínek jako originální 
korespondence a tiskopisy.Beru na vedomí, ze kalkulace zájezdu je závislá na údajích, které systému poskytnu a tudíz muze 
dojít ze strany cestovní kanceláre k její korekci.             
             */
            return new TvReport.AzurReport.AzurReport().createContract(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
        }
        catch (Exception Ex)
        {
            errorMsg = Ex.Message;
            return string.Empty;
        }
    }
}




