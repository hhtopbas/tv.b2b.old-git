﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using TvReport.SunReport;
using System.Text;
using TvTools;
using System.IO;
using System.Threading;

public class MahbalReport
{
    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var _customersTicket = from H in ResData.ResService
                               join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                               join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                               join T in ResData.Title on C.Title equals T.TitleNo
                               join F in flights on H.Service equals F.Code
                               join A in airLines on F.Airline equals A.Code
                               where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                               select new
                               {
                                   CustNo = C.CustNo,
                                   Name = C.Name,
                                   SurName = C.Surname,
                                   TitleName = T.Code,
                                   ServiceID = H.RecID,
                                   DocStat = Rc.DocStat,
                                   DocNo = Rc.DocNo
                               };
        var customersTicket = from q in _customersTicket
                              group q by new
                              {
                                  CustNo = q.CustNo,
                                  Name = q.Name,
                                  SurName = q.SurName,
                                  TitleName = q.TitleName
                              } into k
                              select new
                              {
                                  k.Key.CustNo,
                                  k.Key.Name,
                                  k.Key.SurName,
                                  k.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;
            var custTicket = _customersTicket.Where(w => w.CustNo == row.CustNo).GroupBy(g => g.DocNo);

            Int16? DocStat = custTicket.Count() > 1 ?
                Convert.ToInt16(_customersTicket.Where(w => w.CustNo == row.CustNo && w.DocStat.HasValue && w.DocStat.Value == 1).GroupBy(g => g.DocNo).Count() > 0 ? 1 : 0) :
                _customersTicket.Where(w => w.CustNo == row.CustNo).FirstOrDefault().DocStat;
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") + ")</span>" : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        List<ResConRecord> resConList = ResData.ResCon;
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                ++cnt;
                writeList.Clear();
                writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 92f, y = 650f, FontSize = 8 });
                List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                              join q3 in flyList on q1.Service equals q3.FlightNo
                                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                              select q1).ToList<ResServiceRecord>();


                AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);
                string agencyName_airlineName = UserData.AgencyName + " / " + (airLine != null ? airLine.Name : "");
                string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());
                writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 92f, y = 650f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = string.IsNullOrEmpty(custFlights.FirstOrDefault().PnrNo) ? eTicketNo : custFlights.FirstOrDefault().PnrNo.ToString(), x = 247f, y = 652f, FontSize = 12 });
                writeList.Add(new TvReport.Coordinate { value = resCust.Surname + "/" + resCust.Name + " " + resCust.TitleStr, x = 96f, y = 632f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 285f, y = 632f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = airLine.Name, x = 285f, y = 616f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = UserData.AgencyName, x = 450f, y = 632f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString() + " / " + new Locations().getLocationName(UserData, UserData.Location.HasValue ? UserData.Location.Value : Convert.ToInt32(-1), ref errorMsg).ToString(), x = 473f, y = 616f, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = string.IsNullOrEmpty(custFlights.FirstOrDefault().PnrNo) ? eTicketNo : custFlights.FirstOrDefault().PnrNo.ToString(), x = 294f, y = 186f, FontSize = 12 });
                float _Y = 558f;
                foreach (ResServiceRecord row in custAirleineFlights.OrderBy(o => o.BegDate))
                {
                    FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    writeList.Add(new TvReport.Coordinate { value = row.DepLocationName + "/" + row.ArrLocationName, x = 12f, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.Service, x = 124f, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 171f, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 198f, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.FlyDate.ToShortDateString(), x = 237f, y = _Y, FontSize = 8 });
                    if (flightDay.CountCloseTime.HasValue)
                    {
                        string lastCheckInTime = "00:00";
                        if (flightDay.DepTime.HasValue)
                        {
                            TimeSpan timeS = flightDay.DepTime.Value - flightDay.CountCloseTime.Value;

                            lastCheckInTime = timeS.ToString().Substring(0, 5);
                        }
                        writeList.Add(new TvReport.Coordinate { value = lastCheckInTime, x = 285f, y = _Y, FontSize = 8 });
                    }
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToString("HH:mm") : "", x = 322f, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 361f, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrTime.HasValue ? flightDay.ArrTime.Value.ToString("HH:mm") : "", x = 389f, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString(), x = 422f, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString(), x = 473f, y = _Y, FontSize = 8 });
                    string confStat = string.Empty;
                    switch (row.StatConf)
                    {
                        case 0: confStat = resCust.Title < 8 ? "Request" : "NS"; break;
                        case 1: confStat = resCust.Title < 8 ? "OK" : "NS"; break;
                        case 2: confStat = resCust.Title < 8 ? "Not Confirm" : "NS"; break;
                        case 3: confStat = resCust.Title < 8 ? "No Show" : "NS"; break;
                    }
                    writeList.Add(new TvReport.Coordinate { value = confStat, x = 521f, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString("#,###") + "K" : "", x = 552f, y = _Y, FontSize = 8 });

                    _Y -= 12;
                    if (new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg))
                    {
                        ResConRecord resCon = resConList.Find(f => f.ResNo == row.ResNo && f.ServiceID == row.RecID && f.CustNo == CustNo);
                        resCon.DocNo = eTicketNo;
                        resCon.DocStat = 1;
                        resCon.DocIsDate = DateTime.Now;
                    }
                }

                string ServiceCode = "ETicket_" + row1.airline;
                eTicketList.Add(new TvReport.MahbalReport.MahbalReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
            }
            HttpContext.Current.Session["ResData"] = ResData;
            if (eTicketList.Count() == 1)
                return eTicketList.FirstOrDefault();
            else return new TvReport.MahbalReport.MahbalReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);
        }
        else return "";
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued") + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, resService.Service, ref errorMsg);
        AgencyRecord _agency = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        OfficeRecord _office = new TvBo.Common().getOffice(ResData.ResMain.Market, _agency != null ? _agency.OprOffice : "", ref errorMsg);
        //OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;
        float _y = 0f;

        for (int cnt = 0; cnt < 3; cnt++)
        {
            switch (cnt)
            {
                case 0: _y = 0f; break;
                case 1: _y = 281f; break;
                case 2: _y = 562f; break;
            }            
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 400f, y = 790f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyName, x = 400f, y = 752f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 400f, y = 720f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = _office != null ? _office.Name : _agency.OprOffice, x = 400f, y = 703f - _y, FontSize = 8 });

            float _Y = 721;
            float _X = 17;
            foreach (var row in custs)
            {                
                writeList.Add(new TvReport.Coordinate { value = row.TitleStr + ". " + row.Surname + " " + row.Name + " " + (row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : ""), x = _X, y = _Y - _y, FontSize = 8 });
                _Y -= 11; 
            }

            writeList.Add(new TvReport.Coordinate { value = resService.ServiceName + ", " + hotel.LocationName, x = 18f, y = 753f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationForCountryName(UserData, resService.DepLocation.Value, ref errorMsg).ToString(), x = 70f, y = 767f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.BegDate.Value.ToShortDateString(), x = 298f, y = 650f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.EndDate.Value.ToShortDateString(), x = 298f, y = 635f - _y, FontSize = 8 });            
            
            writeList.Add(new TvReport.Coordinate { value = resService.RoomName, x = 206f, y = 722f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.AccomName, x = 206f, y = 707f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.Board, x = 258, y = 681f - _y, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = new TvReport.MahbalReport.MahbalReport().getTransferStr(ResData.ResMain.ResNo, ref errorMsg), x = 14f, y = 615f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.SupNote.ToString(), x = 14f, y = 593f - _y, FontSize = 8 });            

            writeList.Add(new TvReport.Coordinate { value = resService.Adult.ToString(), x = 258f, y = 665f - _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = resService.Child.ToString(), x = 355f, y = 665f - _y, FontSize = 8 });
            _Y = 665;
            _X = 400f;
            foreach (ResServiceExtRecord row in ResData.ResServiceExt.Where(w => w.StatSer != 2))
            {
                writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName + ", " + row.BegDate.Value.ToShortDateString(), x = _X, y = _Y - _y, FontSize = 8 });
                _Y -= 11f;
            }
        }

        return new TvReport.MahbalReport.MahbalReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showReceiptListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<TvReport.MahbalReport.Mahbal_Receipt> receiptList = new TvReport.MahbalReport.MahbalReport().getReceiptList(ResData.ResMain.ResNo, ref errorMsg);
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (receiptList.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in receiptList)
        {
            sb.Append(" <tr>");
            i++;
            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/";
            string Name = row.PaymentDate.Value.ToShortDateString() + " - " + row.PaymentType + " , " + row.Reference + " , " + row.Amount.Value.ToString("#,###.00") + " " + row.Cur;
            sb.AppendFormat("<span onclick=\"Receipt('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2}</span>",
                            ResData.ResMain.ResNo,
                            row.RecID,
                            i.ToString() + ". " + Name);
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }

    public string showReceipt(User UserData, ref ResDataRecord ResData, int? ReceiptID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("~") + "\\";
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        JournalRecord receipt = new Payments().getJournal(UserData, ReceiptID.HasValue ? ReceiptID.Value : -1, ref errorMsg);
        List<JournalCDetRecord> jList = new Payments().getJournalCDetList(ResData.ResMain.ResNo, receipt.RecID, ref errorMsg);
        ResMainRecord rM = ResData.ResMain;
        if (receipt == null) return "";
        DocumentNoRecord newDocNo = new DocumentNoRecord();
        if (string.IsNullOrEmpty(receipt.ReceiptSerial))
        {
            newDocNo = new TvBo.Common().getNewDocumentNo(UserData, 1, ref errorMsg);
            if (newDocNo == null) return "";

            new TvBo.Common().setReceiptSerialNo(UserData, ReceiptID.Value, newDocNo.Serial.ToString(), newDocNo.No.Value, ref errorMsg);
        }
        else
        {
            newDocNo.Serial = receipt.ReceiptSerial;
            newDocNo.No = receipt.ReceiptNo;
        }

        writeList.Add(new TvReport.Coordinate { value = newDocNo.Serial + "/" + newDocNo.No.ToString(), x = 103f, y = 816f, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = receipt.ReceiptDate.HasValue ? receipt.ReceiptDate.Value.ToShortDateString() : "", x = 528f, y = 813f, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = receipt.ClientName.ToString(), x = 420f, y = 730f, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = receipt.Comment.ToString(), x = 521f, y = 694f, FontSize = 8, RightToLeft = true });
        float _y = 615f;
        foreach(JournalCDetRecord row in jList)
        {
            writeList.Add(new TvReport.Coordinate { value = rM.Balance.HasValue ? rM.Balance.Value.ToString("#,###.00") + " " + rM.SaleCur : "", x = 143f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = row.ResPaidAmount.HasValue ? row.ResPaidAmount.Value.ToString("#,###.00") + " " + row.ResPaidCur : "", x = 265f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = rM.AgencyPayable.HasValue ? rM.AgencyPayable.Value.ToString("#,###.00") + " " + rM.SaleCur : "", x = 366f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = rM.Days.ToString(), x = 416f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = rM.BegDate.HasValue ? rM.BegDate.Value.ToShortDateString() : "", x = 478f, y = _y, FontSize = 8, RightToLeft = true });
            writeList.Add(new TvReport.Coordinate { value = rM.ResNo, x = 566f, y = _y, FontSize = 8, RightToLeft = true });
            _y -= 12;
        }
        decimal? amount = jList.Sum(s => s.ResPaidAmount);
        writeList.Add(new TvReport.Coordinate { value = UserData.UserName, x = 160f, y = 512f, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = amount.HasValue ? amount.Value.ToString("#,###.00") + " " + rM.SaleCur : "", x = 496f, y = 512f, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = amount.HasValue ? amount.Value.ToString("#,###.00") + " " + rM.SaleCur : "", x = 374f, y = 306f, FontSize = 8 });        
        string _numberToWord = new LibVB.VBUtils.NumberToWordEN().SumaZodEng(amount.HasValue ? amount.Value : Convert.ToDecimal(0), rM.SaleCur, "");
        writeList.Add(new TvReport.Coordinate { value = _numberToWord, x = 103f, y = 306f, FontSize = 8 });        

        return new TvReport.MahbalReport.MahbalReport().createReceipt(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }
}




