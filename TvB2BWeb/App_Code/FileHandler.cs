using System;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Drawing.Imaging;

public class FileHandler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //Debugger.Break(); 
        
        fileHandlerReturn result = new fileHandlerReturn { success = true, message = "", fileName = "" };
        string path = HttpContext.Current.Server.MapPath("~/CustPicCache/");
        string saveLocation = string.Empty;
        string fileName = string.Empty;
        string custNo = string.Empty;
        string strMIMEType = string.Empty;
        string strExtension = string.Empty;
        string urlStr = string.Empty;
        try
        {
            int length = 4096;
            int bytesRead = 0;
            Byte[] buffer = new Byte[length];

            //This works with Chrome/FF/Safari 
            // get the name from qqfile url parameter here 
            //System.Diagnostics.Debugger.Break();
            fileName = context.Request["qqfile"];
            custNo = !string.IsNullOrEmpty(context.Request["custNo"]) ? (string)context.Request["custNo"] : string.Empty;
            urlStr = !string.IsNullOrEmpty(context.Request["url"]) ? (string)context.Request["url"] : string.Empty;
            //System.Diagnostics.Debug.Write(fileName);
            strExtension = Path.GetExtension(fileName).ToLower();
            switch (strExtension)
            {
                case ".gif":
                    strMIMEType = "image/gif";
                    break;

                case ".jpg":
                    strMIMEType = "image/jpeg";
                    break;

                case ".png":
                    strMIMEType = "image/png";
                    break;

                default:
                    strMIMEType = string.Empty;
                    return;

            }
            saveLocation = path + "CustNo_" + custNo + strExtension;

            try
            {
                if (File.Exists(saveLocation))
                    File.Delete(saveLocation);

                using (FileStream fileStream = new FileStream(saveLocation, FileMode.Create))
                {
                    do
                    {
                        bytesRead = context.Request.InputStream.Read(buffer, 0, length);
                        fileStream.Write(buffer, 0, bytesRead);
                    }
                    while (bytesRead > 0);
                }

                result.message = "";
                result.fileName = urlStr + "/CustNo_" + custNo + strExtension;
            }
            catch (UnauthorizedAccessException ex)
            {
                // log error hinting to set the write permission of ASPNET or the identity accessing the code                 
                result.fileName = "";
                result.success = false;
                result.message = ex.Message + " " + ex.InnerException + " " + ex.StackTrace.ToString();
            }
        }
        catch
        {
            try
            {
                if (File.Exists(saveLocation))
                    File.Delete(saveLocation);
                //This works with IE 
                fileName = Path.GetFileName(context.Request.Files[0].FileName);
                strExtension = Path.GetExtension(fileName).ToLower();
                switch (strExtension)
                {
                    case ".gif":
                        strMIMEType = "image/gif";
                        break;

                    case ".jpg":
                        strMIMEType = "image/jpeg";
                        break;

                    case ".png":
                        strMIMEType = "image/png";
                        break;

                    default:
                        strMIMEType = string.Empty;
                        return;
                }
                saveLocation = path + "CustNo_" + custNo + strExtension;
                context.Request.Files[0].SaveAs(saveLocation);
                result.message = "";                
                result.fileName = urlStr + "/CustNo_" + custNo + strExtension;
            }
            catch (Exception ex)
            {
                result.fileName = "";
                result.success = false;
                result.message = ex.Message + " " + ex.InnerException + " " + ex.StackTrace.ToString();
            }
        }
        
        //context.Response.ContentType = "text/plain";
        context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
    } 

    //public void ProcessRequest(HttpContext context)
    //{
    //    string strFileName = string.Empty;
    //    string strMode = string.Empty;
    //    string strParams1 = string.Empty;
    //    string strResponse = "error";
    //    string strMIMEType = string.Empty;
    //    //context.Items
    //    try
    //    {
    //        strParams1 = !string.IsNullOrEmpty(context.Request.Form["param1"]) ? (string)context.Request.Form["param1"] : string.Empty;
    //        strFileName = Path.GetFileName(context.Request.Files[0].FileName);
    //        string strExtension = Path.GetExtension(context.Request.Files[0].FileName).ToLower();
    //        switch (strExtension)
    //        {
    //            case ".gif":
    //                strMIMEType = "image/gif";
    //                break;

    //            case ".jpg":
    //                strMIMEType = "image/jpeg";
    //                break;

    //            case ".png":
    //                strMIMEType = "image/png";
    //                break;

    //            default:
    //                strMIMEType = string.Empty;
    //                return;
    //        }
    //        byte[] docBytes = new byte[context.Request.Files[0].InputStream.Length];
    //        context.Request.Files[0].InputStream.Read(docBytes, 0, docBytes.Length);
    //        UploadFile(docBytes, strMIMEType, string.IsNullOrEmpty(strParams1) ? strFileName : "CustNo_" + strParams1 + strExtension);
    //        strResponse = "success";
    //    }
    //    catch (Exception)
    //    {

    //    }

    //    context.Response.ContentType = "text/plain";
    //    context.Response.Write(strResponse);

    //}

    public void UploadFile(byte[] bFile, string strMIMEType, string strFileName)
    {
        try
        {
            if (File.Exists(HttpContext.Current.Server.MapPath("CustPicCache") + "\\" + strFileName))
                File.Delete(HttpContext.Current.Server.MapPath("CustPicCache") + "\\" + strFileName);
            File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + "CustPicCache\\" + strFileName, bFile);
        }
        catch (Exception Ex)
        {
            string errorMsg = Ex.Message;
        }        
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}

public class fileHandlerReturn
{
    public bool success { get; set; }
    public string message { get; set; }
    public string fileName { get; set; }
}