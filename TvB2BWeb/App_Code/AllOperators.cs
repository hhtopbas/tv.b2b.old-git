﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using System.IO;
using TvTools;
using System.Text;
using System.Web.UI;
using System.Configuration;
using System.Text.RegularExpressions;
using Winnovative.WnvHtmlConvert;

public class AllOperators
{
    List<TvReport.basketLangRec> list = new List<TvReport.basketLangRec>();

    public AllOperators()
    {

    }

    public List<TvReport.basketLangRec> getMarketLangList(User UserData)
    {
        string Market = UserData.Market;

        string fileName = HttpContext.Current.Server.MapPath("Data\\" + new UICommon().getWebID().ToString() + "\\BasketMarketLang.xml");
        if (!File.Exists(fileName)) return null;
        List<TvReport.basketLangRec> list = new List<TvReport.basketLangRec>();
        System.Data.DataTable dt = new System.Data.DataTable();
        dt.ReadXml(fileName);
        foreach (System.Data.DataRow row in dt.Rows)
            list.Add(new TvReport.basketLangRec { Lang = row["Market"].ToString(), Labels = row["Labels"].ToString(), Texts = row["Texts"].ToString() });
        return list;
    }

    public string getHotelUrl(User UserData, TvBo.MultiRoomResult searchResult, string Hotel)
    {
        string errorMsg = string.Empty;
        HotelMarOptRecord hotelUrl = searchResult.HotelMarOpt.Find(f => f.Hotel == Hotel);
        string _hotelUrl = (hotelUrl != null && !string.IsNullOrEmpty(hotelUrl.InfoWeb)) ? hotelUrl.InfoWeb : "";
        return _hotelUrl;
    }

    public string createOffers(User UserData, List<SearchResult> offerList, TvBo.MultiRoomResult searchResult, ref string errorMsg)
    {
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        list = getMarketLangList(UserData);

        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        byte[] oprLogo = new TvReport.AllOperator().getLogo("Logo", "Operator", "Code", UserData.Operator, null, ref errorMsg);
        string oprLogoUrl = CacheObjects.getOperatorLogoUrl(UserData.Operator);
        string oprLogoStr = Convert.ToBase64String(oprLogo);
        byte[] agencyLogo = new TvReport.AllOperator().getLogo("LogoSmall", "Agency", "Code", UserData.MainAgency ? UserData.MainOffice : UserData.AgencyID, null, ref errorMsg);
        string agencyLogoStr = Convert.ToBase64String(agencyLogo);
        string agencyLogoUrl = CacheObjects.getAgencyLogoUrl(UserData.AgencyID);
        string userPhone = new TvReport.AllOperator().getAgencyUserPhone(UserData.AgencyID, UserData.UserID, ref errorMsg);
        FixNotesRecord fixNote = new TvBo.Common().getFixNotes(UserData.Market, "PriceList", ref errorMsg);
        //string fixNoteText = fixNote != null ? new LibVB.VBUtils.RTFtoHTML3().rtf2html3(fixNote.Note, "+H+G+I+CR+F=8") : "";
        RTFtoHTML rtfHtml = new RTFtoHTML();
        string fixNoteText = string.Empty;
        if (fixNote != null)
        {
            //fixNote.Note = Regex.Replace(fixNote.Note, @"\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?", "");
            rtfHtml.rtf = fixNote.Note;
            fixNoteText = rtfHtml.html();
        }
        string createdBy = list != null && list.Find(f => f.Labels == "Header1") != null ? list.Find(f => f.Labels == "Header1").Texts : "Created ";
        string lblHotel = list != null && list.Find(f => f.Labels == "lblHotel") != null ? list.Find(f => f.Labels == "lblHotel").Texts : "Hotel";
        string lblCheckIn = list != null && list.Find(f => f.Labels == "lblCheckIn") != null ? list.Find(f => f.Labels == "lblCheckIn").Texts : "Check/in";
        string lblDuration = list != null && list.Find(f => f.Labels == "lblDuration") != null ? list.Find(f => f.Labels == "lblDuration").Texts : "Night";

        string lblRoomType = list != null && list.Find(f => f.Labels == "lblRoomType") != null ? list.Find(f => f.Labels == "lblRoomType").Texts : "Room";
        string lblBoardType = list != null && list.Find(f => f.Labels == "lblBoardType") != null ? list.Find(f => f.Labels == "lblBoardType").Texts : "Board";
        string lblAccom = list != null && list.Find(f => f.Labels == "lblAccom") != null ? list.Find(f => f.Labels == "lblAccom").Texts : "Accommodation";
        string lblPrice = list != null && list.Find(f => f.Labels == "lblPrice") != null ? list.Find(f => f.Labels == "lblPrice").Texts : "Price";
        string lblComment = list != null && list.Find(f => f.Labels == "lblComment") != null ? list.Find(f => f.Labels == "lblComment").Texts : "Agency comment";
        StringBuilder html = new StringBuilder();
        html.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        html.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
        html.Append("<head>");
        html.Append("<title>Selected Prices</title>");
        html.Append("<meta content=\"IE=EmulateIE7\" http-equiv=\"X-UA-Compatible\" />");
        html.Append("<meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" />");
        html.Append("<style type=\"text/css\">");
        html.Append("  .body { width: 650px; font-family: Tahoma; font-size: 10pt; }");
        html.Append("  .mainDiv { width: 100%; }");
        html.Append("  .logoDiv { width: 100%; height: 138px; position: relative; border-bottom: solid 1px #000; }");
        html.Append("  .oprLogoDiv { width: 150px; height: 135px; float: left; }");
        html.Append("  .oprLogoDiv img { bottom: 25px; position: absolute; }");
        html.Append("  .agencyLogoDiv { width: 500px; height: 135px; float: right; }");
        html.Append("  .agencyImgDiv { width: 245px; height: 135px; float: left; position: relative; text-align: right; font-family: Tahoma; font-size: 9pt; }");
        html.Append("  .agencyImgDiv img { bottom: 25px; position: absolute; right: 5px; }");
        html.Append("  .agencyImgDiv span { bottom: 3px; position: absolute; right: 5px;}");
        html.Append("  .agencyInfoDiv { width: 245px; height: 100%; float: right; position: relative; font-family: Tahoma; font-size: 9pt; }");
        html.Append("  .agencyInfoDiv span { bottom: 3px; left: 5px; position: absolute; }");
        html.Append("  .dataDiv { width: 100%; border: solid 1px #000; font-family: Tahoma; }");
        html.Append("  .font8pt { font-size: 8pt; }");
        html.Append("  .font8ptBold { font-size: 8pt; font-weight: bold; }");
        html.Append("  .font9pt { font-size: 9pt; }");
        html.Append("  .font9ptBold { font-size: 9pt; font-weight: bold; }");
        html.Append("  .font10pt { font-size: 10pt; }");
        html.Append("  .font10ptBold { font-size: 10pt; font-weight: bold; }");
        html.Append("  .font10ptItalic { font-size: 10pt; font-style: italic; }");
        html.Append("  .font10ptBoldItalic { font-size: 10pt; font-weight: bold; font-style: italic; }");
        html.Append("  .font11ptFont { font-size: 11pt; }");
        html.Append("  .font11ptBold { font-size: 11pt; font-weight: bold; }");
        html.Append("  .font11ptItalic { font-size: 11pt; font-style: italic; }");
        html.Append("  .font11ptBoldItalic { font-size: 11pt; font-weight: bold; font-style: italic; }");
        html.Append("  .hotelInfo { width: 100%; }");
        html.Append("  .noteInput { border: solid 1px #c8c8c8; width: 90%; }");
        html.Append("  .clearDiv { clear: both; border-bottom: solid 1pt #000; }");
        html.Append("</style>");
        html.Append("</head>");
        html.Append("<body class=\"bodyDiv\">");
        html.Append("<div class=\"mainDiv\">");
        html.Append(" <div class=\"logoDiv\">");
        html.Append("  <div class=\"oprLogoDiv\">");
        html.AppendFormat("    <img alt='' width=\"120px\" src=\"{0}\" />", oprLogoUrl);
        html.Append("  </div>"); //oprLogoDiv
        html.Append("  <div class='agencyLogoDiv'>");
        html.Append("    <div class='agencyImgDiv'>");
        if (!string.IsNullOrEmpty(agencyLogoUrl))
            html.AppendFormat("      <img alt='' width=\"120px\" src=\"{0}\" />", agencyLogoUrl);
        html.Append("      <br />");
        html.AppendFormat("      <span>{0}</span>", createdBy);
        html.Append("    </div>"); //agencyImgDiv
        html.Append("    <div class=\"agencyInfoDiv\">");
        html.AppendFormat("     <span>{0}<br />{1}<br />{2}<br />{3}<br />{4}</span>",
                            agency.FirmName,
                            UserData.UserName,
                            userPhone,
                            UserData.EMail,
                            DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
        html.Append("     </span>");
        html.Append("    </div>"); //agencyInfoDiv
        html.Append("  </div>"); //agencyLogoDiv
        html.Append(" </div>"); //Logo Div
        html.Append(" <div class=\"font8pt\">");
        html.Append(fixNoteText);
        html.Append(" </div>");
        html.Append(" <div class=\"clearDiv\"></div>");
        html.Append(" <div class=\"dataDiv\">");
        int i = 0;
        foreach (SearchResult row in offerList)
        {
            i++;
            html.Append(" <div class=\"clearDiv\"></div>");
            html.Append("   <div class=\"hotelInfo\">");
            html.AppendFormat("     <span class=\"font11ptBold\">{0}</span>", row.DepCityName + " - " + row.ArrCityName + " - " + row.DepCityName);
            html.Append("     <br />");
            html.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptBoldItalic\">{1}</span>&nbsp; <span class=\"font10ptItalic\">( {2} )</span>",
                        lblHotel,
                        row.HotelName + " ( " + row.HotCat + " ) ",
                        row.HotLocationName);
            html.Append("     <br />");
            string hotelWWW = string.Empty;
            hotelWWW = getHotelUrl(UserData, searchResult, row.Hotel);
            if (!string.IsNullOrEmpty(hotelWWW))
            {
                html.AppendFormat("     <span class=\"font10ptItalic\">{0}</span>", hotelWWW);
                html.Append("     <br />");
            }
            html.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>&nbsp;",
                        lblCheckIn,
                        row.CheckIn.HasValue ? row.CheckIn.Value.ToShortDateString() : "");
            html.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>",
                        lblDuration,
                        row.Night.HasValue ? row.Night.Value.ToString() : "");
            html.Append("     <br />");
            html.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>&nbsp;&nbsp;",
                        lblRoomType,
                        row.RoomName);
            html.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>",
                        lblBoardType,
                        row.BoardName);
            html.Append("     <br />");
            html.AppendFormat("     <span class=\"font9pt\">{0} : </span><span class=\"font10ptItalic\">{1}</span>",
                        lblAccom,
                        row.AccomFullName);
            html.Append("     <br />");
            html.AppendFormat("     <span class=\"font11ptFont\">{0} : </span><span class=\"font11ptBoldItalic\">{1}</span>",
                        lblPrice,
                        row.LastPrice.HasValue ? row.LastPrice.Value.ToString("#,###.00") + " " + row.SaleCur : "");
            html.Append("     <br />");
            html.AppendFormat("     <span class=\"font9pt\">{0} : </span>", lblComment);
            html.Append("     <br />");            
            html.AppendFormat("     <input id=\"agencyNote{0}\" type=\"text\" class=\"noteInput\" />", i.ToString());
            html.Append("   </div>");
        }
        html.Append(" </div>");

        #region old
        /*
        int listCount = 0;
        float _y = 256f;
        int pageNo = 1;
        foreach (SearchResult row in offerList)
        {
            ++listCount;
            if (listCount == 1)
            {
                writeList.Add(new TvReport.Coordinate { Pic = oprLogo, x = 20f, y = 267f, H = 16f, Type = TvReport.writeType.Image, PageNo = pageNo });
                writeList.Add(new TvReport.Coordinate { Pic = agencyLogo, x = 115f, y = 267f, W = 20f, Type = TvReport.writeType.Image, PageNo = pageNo });
                writeList.Add(new TvReport.Coordinate { value = agency.FirmName, x = 190f, y = 281f, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = pageNo });
                writeList.Add(new TvReport.Coordinate { value = UserData.UserName, x = 190f, y = 277f, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = pageNo });
                writeList.Add(new TvReport.Coordinate { value = userPhone, x = 190f, y = 273f, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = pageNo });
                writeList.Add(new TvReport.Coordinate { value = UserData.EMail, x = 190f, y = 269f, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = pageNo });
                writeList.Add(new TvReport.Coordinate { value = createdBy + " " + DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString(), x = 190f, y = 266f, FontSize = 8, Align = TvReport.fontAlign.Right, PageNo = pageNo });
                writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20f, y = 264f, H = 0, W = 170f, LineWidth = 0.5f, PageNo = pageNo });
                _y = 256f;
                writeList.Add(new TvReport.Coordinate { value = fixNoteText, x = 20.0f, y = _y, FontSize = 8, PageNo = pageNo });
            }
            _y -= 1f;
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20f, y = _y, H = 0, W = 170f, LineWidth = 0.25f, PageNo = pageNo });
            _y -= 8f;
            writeList.Add(new TvReport.Coordinate { value = row.DepCityName + " - " + row.ArrCityName + " - " + row.DepCityName, x = 22.0f, y = _y, FontSize = 10, bold = true, PageNo = pageNo });
            _y -= 4.5f;
            writeList.Add(new TvReport.Coordinate { value = lblHotel + " : " + (row.HotelName + " ( " + row.HotCat + " ) (" + row.HotLocationName + ")"), x = 22.0f, y = _y, FontSize = 10, bold = true, Italic = true, PageNo = pageNo });
            _y -= 4.5f;
            writeList.Add(new TvReport.Coordinate { value = lblCheckIn + " : " + (row.CheckIn.HasValue ? row.CheckIn.Value.ToShortDateString() : ""), x = 22.0f, y = _y, FontSize = 10, Italic = true, PageNo = pageNo });
            writeList.Add(new TvReport.Coordinate { value = lblDuration + " : " + (row.Night.HasValue ? row.Night.Value.ToString() : ""), x = 90.0f, y = _y, FontSize = 10, Italic = true, PageNo = pageNo });
            _y -= 4.5f;
            writeList.Add(new TvReport.Coordinate { value = lblRoomType + " : " + row.RoomName, x = 22.0f, y = _y, FontSize = 10, Italic = true, PageNo = pageNo });
            writeList.Add(new TvReport.Coordinate { value = lblBoardType + " : " + row.BoardName, x = 90.0f, y = _y, FontSize = 10, Italic = true, PageNo = pageNo });
            _y -= 4.5f;
            writeList.Add(new TvReport.Coordinate { value = lblAccom + " : " + row.AccomFullName, x = 22.0f, y = _y, FontSize = 10, Italic = true, PageNo = pageNo });
            _y -= 4.5f;
            writeList.Add(new TvReport.Coordinate { value = lblPrice + " : " + (row.LastPrice.HasValue ? row.LastPrice.Value.ToString("#,###.00") + " " + row.SaleCur : ""), x = 22.0f, y = _y, FontSize = 10, bold = true, Italic = true, PageNo = pageNo });
            _y -= 4.5f;
            writeList.Add(new TvReport.Coordinate { value = lblComment + " :", x = 22.0f, y = _y, FontSize = 10, Italic = true, PageNo = pageNo });
            _y -= 5.0f;
            writeList.Add(new TvReport.Coordinate { FieldName = "field1" + listCount.ToString() + pageNo.ToString(), value = "", x = 22.0f, y = _y, H = 4.5f, W = 160.0f, LineWidth = 0.2f, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.Text, PageNo = pageNo });
            if (listCount == 5)
            {
                _y -= 1.0f;
                writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20f, y = _y, H = 0, W = 170f, LineWidth = 0.25f, PageNo = pageNo });
                writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20f, y = _y, H = 256.0f - _y, W = 0f, LineWidth = 0.25f, PageNo = pageNo });
                writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 190f, y = _y, H = 256.0f - _y, W = 0f, LineWidth = 0.25f, PageNo = pageNo });

                listCount = 0;
                ++pageNo;
            }
        }
        if (listCount != 5)
        {
            _y -= 1.0f;
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20f, y = _y, H = 0, W = 170f, LineWidth = 0.25f, PageNo = pageNo });
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 20f, y = _y, H = 256.0f - _y, W = 0f, LineWidth = 0.25f, PageNo = pageNo });
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 190f, y = _y, H = 256.0f - _y, W = 0f, LineWidth = 0.25f, PageNo = pageNo });
        }
        */
        #endregion

        html.Append("</div>"); //mainDiv
        html.Append("</body>");
        html.Append("</html>");
        
            StringWriter strWrt = new StringWriter();
            HtmlTextWriter htmText = new HtmlTextWriter(strWrt);
            //this.Page.RenderControl(htmText);
            StringBuilder htmlPdf = strWrt.GetStringBuilder();
            //initialize the PdfConvert object
            PdfConverter pdfConverter = new PdfConverter();

            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;

            //pdfConverter.PageWidth = 670;
            pdfConverter.PdfDocumentOptions.LeftMargin = 30;
            pdfConverter.PdfDocumentOptions.TopMargin = 10;
            // set the demo license key
            pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"];// "RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==";

            // get the base url for string conversion which is the url from where the html code was retrieved
            // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
            string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
            string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";

            // get the pdf bytes from html string
            //byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(html.ToString(), basePageUrl);
            string fileNameA = UserData.AgencyID + UserData.UserID + ".pdf";
            string fileNameB = siteFolderISS + "ACE\\" + fileNameA;
            if (File.Exists(fileNameB))
                File.Delete(fileNameB);
            //pdfConverter.BatchConversion = true;
            pdfConverter.SavePdfFromHtmlStringToFile(html.ToString(), fileNameB);
        

        return fileNameA; /*new TvReport.AllOperator().createOfferHtml(UserData.UserID, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", html.ToString(), ref errorMsg);*/
    }
}
