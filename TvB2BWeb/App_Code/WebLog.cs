﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WebLog
/// </summary>
public static class WebLog
{
    public static Guid SaveWebServiceLog(Guid? pId, string pTransactionId, string pRequestUrl, string pRequest, string pResponse, string pExceptionMessage)
    {
        try
        {

            string sqlCommand = "";
            sqlCommand = @"
                if exists(select null from WebServiceLog(nolock) where Id=@Id)
                    UPDATE WebServiceLog Set ResponseMessage=ISNULL(@ResponseMessage,ResponseMessage),ExceptionMessage=@ExceptionMessage where Id=@Id
                else
                    INSERT INTO WebServiceLog(
                [Id],[ServiceUrl],[RequestId],[RequestMessage],[ResponseMessage],[ExceptionMessage],[ElapsedTime],[CreateDate]) 
                    values(
                @Id,@RequestUrl,@TransactionId,@RequestMessage,@ResponseMessage,@ExceptionMessage,0,GETDATE()  

                )";
            if (!pId.HasValue)
                pId = Guid.NewGuid();
            Database db = (Database)DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetSqlStringCommand(sqlCommand);
            db.AddInParameter(dbCommand, "Id", DbType.Guid, pId);
            db.AddInParameter(dbCommand, "TransactionId", DbType.String, pTransactionId);
            db.AddInParameter(dbCommand, "RequestUrl", DbType.String, pRequestUrl);
            db.AddInParameter(dbCommand, "RequestMessage", DbType.String, pRequest);
            db.AddInParameter(dbCommand, "ResponseMessage", DbType.String, pResponse);
            db.AddInParameter(dbCommand, "ExceptionMessage", DbType.String, pExceptionMessage);
            db.ExecuteNonQuery(dbCommand);
            return pId.Value;
        }
        catch (Exception ex)
        {
            return Guid.Empty;
        }
    }
}