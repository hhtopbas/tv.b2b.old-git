﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using TvReport.SunReport;
using System.Text;
using TvTools;
using System.IO;
using System.Threading;

/// <summary>
/// Summary description for SunReport
/// </summary>
public class SunReport
{
    public string showInsuranceListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResCustInsuranceRecord> customerInsurances = new TvReport.SunReport.SunReport().getInsuranceCustomers(UserData.Market, ResData.ResMain.ResNo, ref errorMsg);
        if (customerInsurances == null || customerInsurances.Count < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResCustInsuranceRecord row in customerInsurances)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q in ResData.ResCon
                         where q.CustNo == row.CustNo && q.ServiceID == row.ServiceID
                         select q.DocStat;

            Int16? DocStat = Conversion.getInt16OrNull(ResData.ResCon.Find(f => f.CustNo == row.CustNo && f.ServiceID == row.ServiceID).DocStat);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == row.ServiceID);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td style=\"font-family: Tahoma; font-size: 14px;\">");
            sb.Append(i.ToString() + ". " + Name);
            sb.Append("     </td>");
            sb.Append("     <td>&nbsp;");

            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Insurance0969801('{0}', {1}, {2});\" style=\"cursor: pointer; text-decoration: underline;\">{3} ",
                                resService.ResNo,
                                resService.RecID,
                                row.CustNo,
                                resService.ServiceNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         Бронирование оплаты еще не завершено.");
                else
                {
                    sb.AppendFormat("<span onclick=\"Insurance0969801('{0}', {1}, {2});\" style=\"cursor: pointer; text-decoration: underline;\">{3} ",
                                resService.ResNo,
                                resService.RecID,
                                row.CustNo,
                                resService.ServiceNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showInsurance(User UserData, ref ResDataRecord ResData, int? ServiceID, int? CustNo)
    {
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder.Replace('/', '\\') + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("~\\");

        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        if (!Equals(resService.ServiceType, "INSURANCE")) return string.Empty;

        InsuranceRecord insurance = new Insurances().getInsurance(UserData.Market, resService.Service, ref errorMsg);
        Int16 maxTourist = insurance.MaxPaxPolicy.HasValue ? insurance.MaxPaxPolicy.Value : Convert.ToInt16(6);

        List<ResCustInsuranceRecord> customerInsurances = new TvReport.SunReport.SunReport().getInsuranceCustomers(UserData.Market, ResData.ResMain.ResNo, ref errorMsg);

        string custListString = CustNo.ToString();
        var insuranceCust = from q1 in ResData.ResService
                            join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                            where q1.RecID == ServiceID
                            select new { CustNo = q2.CustNo, recID = Convert.ToInt32(0), polNo = q2.DocNo };
        if (insuranceCust.Count() > 0)
        {
            Int16 cnt = 1;
            foreach (var row in insuranceCust.Where(w => w.CustNo != CustNo))
            {
                cnt++;
                if (cnt <= maxTourist)
                    custListString += "," + row.CustNo.ToString();
            }
        }

        List<TvReport.SunReport.CustInsuranceRecord> custInsList = new TvReport.SunReport.SunReport().getcustInsurance(ResData.ResMain.ResNo, ServiceID, custListString, ref errorMsg);

        TvReport.SunReport.InsuranceRptRecord ins = new TvReport.SunReport.SunReport().getInsurance(UserData.Market, ResData.ResMain.ResNo, ServiceID, ref errorMsg);
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        string polNo = string.Empty;

        string pdfName = ResData.ResService.Find(f => f.RecID == ServiceID).Service;
        if (pdfName == "GVA50A_AUS" || pdfName == "GVA50A_IT" || pdfName == "RENGVA15S")
        {
            #region GVA50A_AUS / GVA50A_IT / RENGVA15S
            if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
            else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;
            //grid
            float _Y = 654;
            foreach (CustInsuranceRecord custIns in custInsList)
            {
                writeList.Add(new TvReport.Coordinate { value = custIns.Surname + "," + custIns.Name, x = 16, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 168, y = _Y, FontSize = 8 });
                string nationalCode = string.Equals(custIns.NationCode.ToString(), "RU") ? "RF" : custIns.NationCode.ToString();
                writeList.Add(new TvReport.Coordinate { value = nationalCode, x = 260, y = _Y, FontSize = 8 });
                _Y -= 11;
            }
            //tablo
            writeList.Add(new TvReport.Coordinate { value = ins.INameS.ToString(), x = 235, y = 570, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.Coverage.HasValue ? ins.Coverage.Value.ToString("# ###.") : "", x = 235, y = 556, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.InsZone, x = 235, y = 542, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.BegDate.HasValue ? ins.BegDate.Value.ToShortDateString() : "", x = 235, y = 528, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.Duration.ToString(), x = 235, y = 514, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 235, y = 472, FontSize = 8 });

            #endregion
        }
        else if (pdfName == "RENGVA15")
        {
            #region RENGVA15
            if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
            else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;
            //grid
            float _Y = 621;
            foreach (CustInsuranceRecord custIns in custInsList)
            {
                writeList.Add(new TvReport.Coordinate { value = custIns.Surname + "," + custIns.Name, x = 16, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 168, y = _Y, FontSize = 8 });
                string nationalCode = string.Equals(custIns.NationCode.ToString(), "RU") ? "RF" : custIns.NationCode.ToString();
                writeList.Add(new TvReport.Coordinate { value = nationalCode, x = 260, y = _Y, FontSize = 8 });
                _Y -= 11;
            }
            //tablo
            writeList.Add(new TvReport.Coordinate { value = ins.INameS.ToString(), x = 235, y = 534, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.Coverage.HasValue ? ins.Coverage.Value.ToString("# ###.") : "", x = 235, y = 520, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.InsZone, x = 235, y = 506, FontSize = 8 });
            string ser_Beg_EndDate = (resService.BegDate.HasValue ? resService.BegDate.Value.ToShortDateString() : "") + " - " + (resService.EndDate.HasValue ? resService.EndDate.Value.ToShortDateString() : "");
            writeList.Add(new TvReport.Coordinate { value = ser_Beg_EndDate, x = 235, y = 494, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.Duration.ToString(), x = 235, y = 480, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 235, y = 438, FontSize = 8 });
            #endregion
        }
        else if (pdfName == "B2BHP" || pdfName == "B2BHPVIP")//b2b hp, b2b hp vip eng
        {
            #region B2BHP / B2BHPVIP
            if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
            else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;
            //grid
            float _Y = 635;
            foreach (CustInsuranceRecord custIns in custInsList)
            {
                writeList.Add(new TvReport.Coordinate { value = custIns.Surname + "," + custIns.Name, x = 18, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 106, y = _Y, FontSize = 8 });
                string nationalCode = string.Equals(custIns.NationCode.ToString(), "RU") ? "RF" : custIns.NationCode.ToString();
                writeList.Add(new TvReport.Coordinate { value = nationalCode, x = 172, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custIns.PassSerie.ToString() + "-" + custIns.PassNo.ToString(), x = 235, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custIns.SalePrice.HasValue ? custIns.SalePrice.Value.ToString("#,###.00") : "", x = 295, y = _Y, FontSize = 8 });
                _Y -= 11;
            }
            //tablo            
            writeList.Add(new TvReport.Coordinate { value = ins.INameS.ToString(), x = 395, y = 665, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.BegDate.HasValue ? ins.BegDate.Value.ToShortDateString() : "", x = 395, y = 632, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.Duration.ToString(), x = 395, y = 608, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = ins.InsuranceName.ToString(), x = 395, y = 585, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = custInsList[0].NetCur.ToString(), x = 495, y = 575, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 504, y = 502, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = DateTime.Now.TimeOfDay.ToString(), x = 504, y = 490, FontSize = 8 });
            #endregion
        }
        else
            if (pdfName == "RENNVA4" || pdfName == "RENCAN20K" || pdfName == "RENCAN3000" ||
                pdfName == "NVPR_T" || pdfName == "NVPRVIP_T" || pdfName == "RENNV" || pdfName == "RENNV7" ||
                pdfName == "RENNVG4" || pdfName == "RENNVI4" || pdfName == "RENNVI7" || pdfName == "RENNVPRVIP" ||
                pdfName == "RENNVPR") //b2b hp, b2b hp vip eng
            {
                #region RENNVA4 / RENCAN20K / RENCAN3000 / NVPR_T / NVPRVIP_T / RENNV / RENNV7 / RENNVG4 / RENNVI4 / RENNVI7 / RENNVPRVIP / RENNVPR
                if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                    polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
                else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;
                //grid
                float _Y = 635;
                foreach (CustInsuranceRecord custIns in custInsList)
                {
                    ResCustInsuranceRecord insurancePrice = customerInsurances.Find(f => f.CustNo == CustNo && f.ServiceID == ServiceID);
                    writeList.Add(new TvReport.Coordinate { value = custIns.Surname + "," + custIns.Name, x = 18, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 106, y = _Y, FontSize = 8 });
                    string nationalCode = string.Equals(custIns.NationCode.ToString(), "RU") ? "RT" : custIns.NationCode.ToString();
                    writeList.Add(new TvReport.Coordinate { value = nationalCode, x = 172, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = custIns.PassSerie.ToString() + "-" + custIns.PassNo.ToString(), x = 235, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = insurancePrice != null && insurancePrice.SalePrice.HasValue ? insurancePrice.SalePrice.Value.ToString("#,###.00") + " y.e." : "", x = 295, y = _Y, FontSize = 8 });
                    _Y -= 11;
                }
                //tablo            
                writeList.Add(new TvReport.Coordinate { value = resService.CrtDate.HasValue ? resService.CrtDate.Value.ToString() : "", x = 395, y = 748, FontSize = 8 });

                writeList.Add(new TvReport.Coordinate { value = ins.INameS.ToString(), x = 395, y = 665, FontSize = 8 });
                string serCrt_BegDate = (resService.CrtDate.HasValue ? resService.CrtDate.Value.ToShortDateString() : "") + " - " + (resService.BegDate.HasValue ? resService.BegDate.Value.ToShortDateString() : "");
                int duration = Math.Abs((resService.CrtDate.Value - resService.BegDate.Value).Days);
                writeList.Add(new TvReport.Coordinate { value = serCrt_BegDate, x = 395, y = 632, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = (duration + 1).ToString(), x = 395, y = 608, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.InsuranceName.ToString(), x = 395, y = 585, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custInsList[0].NetCur.ToString(), x = 495, y = 575, FontSize = 8 });

                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 504, y = 502, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Now.TimeOfDay.ToString(), x = 504, y = 490, FontSize = 8 });

                #endregion
            }
            else if (pdfName == "NA" || pdfName == "ONKEY1")
            {
                #region NA / ONKEY1
                if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                    polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
                else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;
                //grid        
                float _Y = 658;
                foreach (CustInsuranceRecord custIns in custInsList)
                {
                    writeList.Add(new TvReport.Coordinate { value = custIns.Surname + "," + custIns.Name, x = 36, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 358, y = _Y, FontSize = 8 });
                    string nationalCode = string.Equals(custIns.NationCode.ToString(), "RU") ? "RF" : custIns.NationCode.ToString();
                    writeList.Add(new TvReport.Coordinate { value = nationalCode, x = 460, y = _Y, FontSize = 8 });
                    _Y -= 11;
                }
                //tablo                        
                writeList.Add(new TvReport.Coordinate { value = ins.INameS.ToString(), x = 360, y = 580, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.Duration.ToString(), x = 360, y = 524, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = "1 y.e. = 1 " + custInsList[0].NetCur.ToString(), x = 360, y = 510, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 360, y = 495, FontSize = 8 });
                #endregion
            }
            else if (pdfName == "GVA50KIDS" || pdfName == "GVA50_T")
            {
                #region GVA50KIDS
                if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                    polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
                else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;
                //grid  
                float _Y = 662;
                foreach (CustInsuranceRecord custIns in custInsList)
                {
                    writeList.Add(new TvReport.Coordinate { value = custIns.Surname + "," + custIns.Name, x = 37, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 249, y = _Y, FontSize = 8 });
                    string nationalCode = string.Equals(custIns.NationCode.ToString(), "RU") ? "RF" : custIns.NationCode.ToString();
                    writeList.Add(new TvReport.Coordinate { value = nationalCode, x = 319, y = _Y, FontSize = 8 });
                    _Y -= 11;
                }
                //tablo
                writeList.Add(new TvReport.Coordinate { value = ins.INameS.ToString(), x = 262, y = 634, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.Coverage.HasValue ? ins.Coverage.Value.ToString("# ###.") + " y.e." : "", x = 262, y = 620, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.InsZone, x = 262, y = 606, FontSize = 8 });
                string ser_Beg_EndDate = (resService.BegDate.HasValue ? resService.BegDate.Value.ToShortDateString() : "") + " - " + (resService.EndDate.HasValue ? resService.EndDate.Value.ToShortDateString() : "");
                int duration = Math.Abs((resService.EndDate.Value - resService.BegDate.Value).Days);
                writeList.Add(new TvReport.Coordinate { value = ser_Beg_EndDate, x = 262, y = 594, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = (duration + 1).ToString(), x = 262, y = 580, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 262, y = 538, FontSize = 8 });

                #endregion
            }
            else if (pdfName == "GVA050HC" || pdfName == "VIPGVA100" || pdfName == "GVA100_T")
            {
                #region GVA050HC / VIPGVA100 / GVA100_T
                if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                    polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
                else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;
                //grid  
                float _Y = 662;
                foreach (CustInsuranceRecord custIns in custInsList)
                {
                    writeList.Add(new TvReport.Coordinate { value = custIns.Surname + "," + custIns.Name, x = 37, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 249, y = _Y, FontSize = 8 });
                    string nationalCode = string.Equals(custIns.NationCode.ToString(), "RU") ? "RF" : custIns.NationCode.ToString();
                    writeList.Add(new TvReport.Coordinate { value = nationalCode, x = 319, y = _Y, FontSize = 8 });
                    _Y -= 11;
                }
                //tablo
                writeList.Add(new TvReport.Coordinate { value = ins.INameS.ToString(), x = 262, y = 634, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.Coverage.HasValue ? ins.Coverage.Value.ToString("# ###.") + " y.e." : "", x = 262, y = 620, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.InsZone, x = 262, y = 606, FontSize = 8 });
                string ser_Beg_EndDate = (resService.BegDate.HasValue ? resService.BegDate.Value.ToShortDateString() : "") + " - " + (resService.EndDate.HasValue ? resService.EndDate.Value.ToShortDateString() : "");
                int duration = Math.Abs((resService.BegDate.Value - resService.EndDate.Value).Days);
                writeList.Add(new TvReport.Coordinate { value = ser_Beg_EndDate, x = 262, y = 594, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = (duration + 1).ToString(), x = 262, y = 580, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 262, y = 538, FontSize = 8 });

                #endregion
            }
            else
            {
                pdfName = "RENGVA15";
                #region Other
                if (string.IsNullOrEmpty(insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo))
                    polNo = new Insurances().getNewPolNo(resService.Service, ref errorMsg).ToString();
                else polNo = insuranceCust.Where(w => w.CustNo == CustNo).FirstOrDefault().polNo;
                //grid
                float _Y = 621;
                foreach (CustInsuranceRecord custIns in custInsList)
                {
                    writeList.Add(new TvReport.Coordinate { value = custIns.Surname + "," + custIns.Name, x = 16, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = custIns.Birthday.HasValue ? custIns.Birthday.Value.ToShortDateString() : "", x = 168, y = _Y, FontSize = 8 });
                    string nationalCode = string.Equals(custIns.NationCode.ToString(), "RU") ? "RF" : custIns.NationCode.ToString();
                    writeList.Add(new TvReport.Coordinate { value = nationalCode, x = 260, y = _Y, FontSize = 8 });
                    _Y -= 11;
                }
                //tablo
                writeList.Add(new TvReport.Coordinate { value = ins.INameS.ToString(), x = 235, y = 534, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.Coverage.HasValue ? ins.Coverage.Value.ToString("# ###.") : "", x = 235, y = 520, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.InsZone, x = 235, y = 506, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.BegDate.HasValue ? ins.BegDate.Value.ToShortDateString() : "", x = 235, y = 494, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = ins.Duration.ToString(), x = 235, y = 480, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 235, y = 438, FontSize = 8 });

                #endregion
            }
        string retVal = string.Empty;
        retVal = new TvReport.SunReport.SunReport().createInsurance(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), pdfName, ref errorMsg);

        if (!string.IsNullOrEmpty(retVal))
        {
            var query = from q1 in insuranceCust
                        join q2 in custListString.Split(',') on q1.CustNo equals Convert.ToInt32(q2)
                        select q1;
            foreach (var row in query)
            {
                if (new Reservation().setInsuranceDoc(ResData.ResMain.ResNo, ServiceID, row.CustNo, polNo, ref errorMsg))
                {
                    new Insurances().setInsurancePolicy(UserData, resService.Service, Conversion.getInt32OrNull(polNo), row.CustNo, resService.Supplier, resService.ResNo, resService.BegDate, resService.EndDate,
                                                        resService.Duration, ins.Coverage, ins.CoverageCur, resService.DepLocation, ins.InsZone, UserData.AgencyRec.Location, UserData.UserID, "Y", ref errorMsg);
                }
                ResConRecord resCon = ResData.ResCon.Find(f => f.CustNo == row.CustNo && f.ServiceID == ServiceID);
                resCon.DocNo = polNo;
                resCon.DocStat = 1;
                resCon.DocIsDate = DateTime.Today;
            }
            return retVal;
        }
        else return string.Empty;

    }

    public string showAggrement(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        string statusStr = string.Empty;
        switch (ResData.ResMain.ConfStat)
        {
            case 0: statusStr = "Ожидание"; break;
            case 1: statusStr = "ОК"; break;
            case 2: statusStr = "Не подтверждено"; break;
            case 3: statusStr = "Но шоу"; break;
        }
        OfficeRecord oprOffice = new TvBo.Common().getOffice(UserData.Market, agency.OprOffice, ref errorMsg);
        writeList.Add(new TvReport.Coordinate { value = "SUNRISE TOUR / " + (oprOffice != null ? oprOffice.Name : ""), x = 232, y = 750, FontSize = 11 });
        writeList.Add(new TvReport.Coordinate { value = statusStr, x = 340, y = 739, FontSize = 11 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 400, y = 700, W = 45, H = 45, Type = TvReport.writeType.QRCode });
        writeList.Add(new TvReport.Coordinate { value = agency.Name, x = 154, y = 711, FontSize = 9 });
        writeList.Add(new TvReport.Coordinate { value = agency.Fax1, x = 154, y = 694, FontSize = 9 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Now.ToString(), x = 154, y = 676, FontSize = 9 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 154, y = 657, FontSize = 9 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNote, x = 154, y = 640, FontSize = 9 });

        writeList.Add(new TvReport.Coordinate { value = "Туристы", x = 54, y = 615, FontSize = 9, bold = true });
        float _y = 615;
        foreach (ResCustRecord row in ResData.ResCust)
        {
            ResCustInfoRecord resCustInfo = ResData.ResCustInfo.Find(f => f.CustNo == row.CustNo);
            _y -= 11;
            writeList.Add(new TvReport.Coordinate { value = row.TitleStr + ". " + row.Surname + " " + row.Name, x = 54, y = _y, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "", x = 250, y = _y, FontSize = 9 });
            writeList.Add(new TvReport.Coordinate { value = resCustInfo != null ? (string.IsNullOrEmpty(resCustInfo.AddrHomeTel) ? resCustInfo.MobTel : resCustInfo.AddrHomeTel) : "", x = 300, y = _y, FontSize = 9 });
        }
        _y -= 20;
        writeList.Add(new TvReport.Coordinate { value = "Услуга", x = 54, y = _y, FontSize = 9, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "Кол-во", x = 105, y = _y, FontSize = 9, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "Описание", x = 140, y = _y, FontSize = 9, bold = true });
        var services = from srv in ResData.ResService
                       where srv.StatSer < 2
                       group srv by new { Service = srv.ServiceType, LService = srv.ServiceTypeNameL } into k
                       select new { Service = k.Key.Service, LServiceType = k.Key.LService, Count = ResData.ResService.Where(w => w.ServiceType == k.Key.Service).Count() };
        foreach (var q in services)
        {
            List<ResServiceRecord> resService = (from Srv in ResData.ResService
                                                 where Srv.ServiceType == q.Service && Srv.StatSer < 2
                                                 select Srv).ToList<ResServiceRecord>();
            bool first = true;
            foreach (ResServiceRecord row in resService)
            {
                _y -= 10;
                if (first)
                {
                    writeList.Add(new TvReport.Coordinate { value = string.IsNullOrEmpty(q.LServiceType) ? q.Service : q.LServiceType, x = 54, y = _y, FontSize = 9 });
                    first = false;
                }
                string ServiceDesc = string.Empty;
                switch (row.ServiceType)
                {
                    case "HOTEL": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "FLIGHT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "TRANSPORT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "TRANSFER": ServiceDesc += ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "RENTING": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "EXCURSION": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "INSURANCE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "VISA": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "HANDFEE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    default: ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                }
                if (!Equals(row.ServiceType, "HOTEL"))
                    ServiceDesc = row.ServiceNameL + " (" + ServiceDesc + ")";
                else ServiceDesc = row.ServiceNameL;
                writeList.Add(new TvReport.Coordinate { value = row.Unit.ToString(), x = 120, y = _y, FontSize = 9 });
                writeList.Add(new TvReport.Coordinate { value = ServiceDesc, x = 140, y = _y, FontSize = 9 });
                if (Equals(row.ServiceType, "HOTEL"))
                {
                    _y -= 10;
                    writeList.Add(new TvReport.Coordinate
                    {
                        value = "Заезд : " + (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") +
                            "   Выезд : " + (row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : ""),
                        x = 140,
                        y = _y,
                        FontSize = 9
                    });
                    _y -= 10;
                    writeList.Add(new TvReport.Coordinate
                    {
                        value = "Тип номера : " + row.RoomNameL,
                        x = 140,
                        y = _y,
                        FontSize = 9
                    });
                    _y -= 10;
                    writeList.Add(new TvReport.Coordinate
                    {
                        value = "Тип питания : " + row.BoardNameL,
                        x = 140,
                        y = _y,
                        FontSize = 9
                    });

                }
            }
        }
        if (ResData.ResServiceExt.Where(w => w.StatSer < 2).Count() > 0)
        {
            _y -= 25;
            writeList.Add(new TvReport.Coordinate { value = "Доп. услуги", x = 54, y = _y, FontSize = 9, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Кол-во", x = 165, y = _y, FontSize = 9, bold = true });
            foreach (ResServiceExtRecord row in ResData.ResServiceExt.Where(w => w.StatSer < 2).Select(s => s).ToList<ResServiceExtRecord>())
            {
                _y -= 10;
                writeList.Add(new TvReport.Coordinate { value = row.ExtServiceNameL, x = 54, y = _y, FontSize = 9 });
                writeList.Add(new TvReport.Coordinate { value = row.Unit.ToString(), x = 180, y = _y, FontSize = 9 });
            }
        }
        _y -= 25;
        writeList.Add(new TvReport.Coordinate
        {
            value = "Итого к оплате : " + (ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : ""),
            x = 54,
            y = _y,
            FontSize = 9,
            bold = true
        });
        _y -= 30;

        return new TvReport.SunReport.SunReport().createAggreement(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         Бронирование оплаты еще не завершено.");
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;
        float _Y = 771;
        float _X = 15;
        foreach (var row in custs)
        {
            _Y -= 11;
            writeList.Add(new TvReport.Coordinate { value = row.TitleStr + ". " + row.Surname + " " + row.Name, x = _X, y = _Y, FontSize = 8 });
        }
        _Y = 800;
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 400, y = _Y, FontSize = 8 });
        _Y -= 28;
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyNameL, x = 400, y = _Y, FontSize = 8 });
        _Y = 760; _X = 175;
        int? countryID = new Locations().getLocationForCountry(resService.DepLocation.Value, ref errorMsg);
        writeList.Add(new TvReport.Coordinate { value = new Locations().getLocationName(UserData.Market, countryID.Value, NameType.NormalName, ref errorMsg), x = _X, y = _Y, FontSize = 8 });
        _Y -= 11;
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName + ", " + resService.DepLocationName, x = _X, y = _Y, FontSize = 8 });
        _Y -= 28;
        writeList.Add(new TvReport.Coordinate { value = resService.RoomName, x = _X, y = _Y, FontSize = 8 });
        _X = 400; //_Y += 28;
        foreach (ResServiceExtRecord row in ResData.ResServiceExt)
        {
            writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName, x = _X, y = _Y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToShortDateString(), x = _X + 126, y = _Y, FontSize = 8 });
            _Y -= 11;
        }
        _X = 285; _Y = 683;
        writeList.Add(new TvReport.Coordinate { value = resService.BoardName, x = _X, y = _Y, FontSize = 8 });
        _X = 230; _Y -= 19;
        writeList.Add(new TvReport.Coordinate { value = resService.Adult.ToString(), x = _X, y = _Y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Child.ToString(), x = _X + 112, y = _Y, FontSize = 8 });
        _X = 285; _Y -= 19;
        writeList.Add(new TvReport.Coordinate { value = (resService.BegDate.HasValue ? resService.BegDate.Value.ToShortDateString() : ""), x = _X, y = _Y, FontSize = 8 });
        _Y -= 19;
        writeList.Add(new TvReport.Coordinate { value = (resService.EndDate.HasValue ? resService.EndDate.Value.ToShortDateString() : ""), x = _X, y = _Y, FontSize = 8 });
        _X = 13;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "TRANSFER").Select(s => s).ToList<ResServiceRecord>())
        {
            writeList.Add(new TvReport.Coordinate { value = row.ServiceName, x = _X, y = _Y, FontSize = 8 });
            _Y -= 15;
        }

        return new TvReport.SunReport.SunReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var customersTicket = from H in ResData.ResService
                              join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                              join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                              join T in ResData.Title on C.Title equals T.TitleNo
                              join F in flights on H.Service equals F.Code
                              join A in airLines on F.Airline equals A.Code
                              where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                              group Rc by new
                              {
                                  CustNo = Rc.CustNo,
                                  Name = C.Name,
                                  SurName = C.Surname,
                                  TitleName = T.Code
                              } into g
                              select new
                              {
                                  CustNo = g.Key.CustNo,
                                  Name = g.Key.Name,
                                  SurName = g.Key.SurName,
                                  TitleName = g.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;

            Int16? DocStat = Conversion.getInt16OrNull(ResData.ResCon.Find(f => f.CustNo == row.CustNo).DocStat);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");                
            }
            else
            {

                //if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                //    sb.Append("         Бронирование оплаты еще не завершено.");
                //else
                //{
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                            ResData.ResMain.ResNo,
                            row.CustNo,
                            i.ToString() + ". " + Name);
                sb.Append((DocStat.HasValue && DocStat.Value == 1) ? "(выписанный)" : "(невыписанный)" + "</span>");                
                //}
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicketRefListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var customersTicket = from H in ResData.ResService
                              join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                              join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                              join T in ResData.Title on C.Title equals T.TitleNo
                              join F in flights on H.Service equals F.Code
                              join A in airLines on F.Airline equals A.Code
                              where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                              group Rc by new
                              {
                                  CustNo = Rc.CustNo,
                                  Name = C.Name,
                                  SurName = C.Surname,
                                  TitleName = T.Code
                              } into g
                              select new
                              {
                                  CustNo = g.Key.CustNo,
                                  Name = g.Key.Name,
                                  SurName = g.Key.SurName,
                                  TitleName = g.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;

            Int16? DocStat = Conversion.getInt16OrNull(ResData.ResCon.Find(f => f.CustNo == row.CustNo).DocStat);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span>{0}</span>&nbsp;&nbsp;",
                                i.ToString() + ". " + Name);
                sb.AppendFormat("<span onclick=\"TicketRef('{0}', '{1}', '{2}', 'HTMLPDF');\" style=\"cursor: pointer; text-decoration: underline;\">{3} ",
                                filename + "TicketRef.aspx",
                                "TicketRef" + row.CustNo,
                                row.CustNo,
                                "Справка по а/б");
                sb.Append("</span>");
            }
            else
            {

                //if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                //    sb.Append("         Бронирование оплаты еще не завершено.");
                //else
                //{
                sb.AppendFormat("<span>{0}</span>&nbsp;&nbsp;",
                            i.ToString() + ". " + Name);
                sb.AppendFormat("&nbsp;&nbsp;<span onclick=\"TicketRef('{0}', '{1}', '{2}');\" style=\"cursor: pointer; text-decoration: underline;\">{3} ",
                            filename + "TicketRef.aspx",
                            "TicketRef" + row.CustNo,
                            row.CustNo,
                            "Справка по а/б");
                sb.Append("</span>");
                //}
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            writeList.Clear();
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                ++cnt;
                List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                              join q3 in flyList on q1.Service equals q3.FlightNo
                                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                              select q1).ToList<ResServiceRecord>();


                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToString("dd.MM.yyyy") + " / " + agency.LocationName, x = 368, y = 748, FontSize = 8 });

                AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);
                writeList.Add(new TvReport.Coordinate { value = airLine != null ? airLine.Name : "", x = 266, y = 635, FontSize = 8 });

                string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());

                writeList.Add(new TvReport.Coordinate { value = eTicketNo, x = 266, y = 618, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = custFlights.FirstOrDefault().ResNo, x = 266, y = 601, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name, x = 266, y = 582, FontSize = 8 });
                float _Y = 492;
                foreach (ResServiceRecord row in custAirleineFlights)
                {
                    FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    writeList.Add(new TvReport.Coordinate { value = row.DepLocationName, x = 20, y = _Y + 6, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.ArrLocationName, x = 20, y = _Y - 6, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport, x = 126, y = _Y + 6, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrAirport, x = 126, y = _Y - 6, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = airLine.CarCode, x = 176, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.Service, x = 204, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.FlgClass, x = 248, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToString("dd.MM.yyyy"), x = 266, y = _Y, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToString("HH:mm") : "", x = 320, y = _Y + 6, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.ArrTime.HasValue ? flightDay.ArrTime.Value.ToString("HH:mm") : "", x = 320, y = _Y - 6, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = airLine.NameS, x = 356, y = _Y, FontSize = 8 });

                    writeList.Add(new TvReport.Coordinate { value = row.BegDate.Value.ToString("dd.MM.yyyy"), x = 408, y = _Y + 6, FontSize = 8 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime > flightDay.ArrTime ? row.BegDate.Value.AddDays(1).ToString("dd.MM.yyyy") : row.BegDate.Value.ToString("dd.MM.yyyy"), x = 408, y = _Y - 6, FontSize = 8 });
                    if (string.Equals(row1.airline, "SU"))
                    {
                        string bagWeight = string.Empty;
                        if (string.Equals(row.FlgClass, "C"))
                            bagWeight = "3PC";
                        else bagWeight = "1PC";
                        writeList.Add(new TvReport.Coordinate { value = bagWeight, x = 480, y = _Y, FontSize = 8 });
                    }
                    else writeList.Add(new TvReport.Coordinate { value = flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString("#,###") : "", x = 480, y = _Y, FontSize = 8 });

                    string confStat = string.Empty;
                    switch (row.StatConf)
                    {
                        case 0: confStat = resCust.Title < 8 ? "Request" : "NS"; break;
                        case 1: confStat = resCust.Title < 8 ? "OK" : "NS"; break;
                        case 2: confStat = resCust.Title < 8 ? "Not Confirm" : "NS"; break;
                        case 3: confStat = resCust.Title < 8 ? "No Show" : "NS"; break;
                    }
                    writeList.Add(new TvReport.Coordinate { value = confStat, x = 505, y = _Y, FontSize = 8 });
                    _Y -= 25;
                    new Reservation().setFlightTicketPrintUser(row.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg);
                }


                string ServiceCode = "ETicket_" + row1.airline;
                eTicketList.Add(new TvReport.SunReport.SunReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ServiceCode, ref errorMsg));
            }
            string retVal = string.Empty;
            if (eTicketList.Count() == 1)
                retVal = eTicketList.FirstOrDefault();
            else retVal = new TvReport.SunReport.SunReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);

            return retVal;
        }
        else return "";

    }

    public string showProforma(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        BankRecord oprBank = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);

        writeList.Add(new TvReport.Coordinate { value = "Proforma Nr." + ResData.ResMain.ResNo, x = 252, y = 827, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = oprBank.NameL + ", " + opr.AddrCity, x = 40, y = 743, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.Bank1IBAN, x = 370, y = 744, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.Bank1AccNo, x = 370, y = 718, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.Bank2AccNo, x = 370, y = 679, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.TaxAccNo, x = 70, y = 695, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.TaxOffice, x = 210, y = 695, FontSize = 10 });
        writeList.Add(new TvReport.Coordinate { value = opr.FirmName, x = 40, y = 680, FontSize = 10 });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo + " от " + DateTime.Today.ToShortDateString() + " г.", x = 265, y = 626, FontSize = 12, bold = true });
        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("ИНН {0}, КПП {1}, {2}, {3}",
                            opr.TaxAccNo,
                            opr.TaxOffice,
                            opr.FirmName,
                            opr.AddrZip),
            x = 110,
            y = 590,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("{0}, {1}",
                            opr.InvAddrCity,
                            opr.InvAddress),
            x = 110,
            y = 578,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("ИНН {0}, КПП {1}, {2}, {3}",
                            agency.TaxAccNo,
                            agency.TaxOffice,
                            agency.FirmName,
                            agency.AddrZip),
            x = 110,
            y = 533,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("{0}, {1}",
                            agency.InvAddrCity,
                            agency.InvAddress),
            x = 110,
            y = 521,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 155, y = 462, FontSize = 10, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "", x = 470, y = 462, FontSize = 10, bold = true, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "", x = 575, y = 462, FontSize = 10, bold = true, RightToLeft = true });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "", x = 575, y = 380, FontSize = 10, bold = true, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyPayable.HasValue ? (ResData.ResMain.AgencyPayable.Value + 1).ToString("#,###.00") : "", x = 575, y = 418, FontSize = 10, bold = true, RightToLeft = true });
        TvTools.NumberToWordRU numToWordC = new NumberToWordRU();
        string numToWord = numToWordC.CurrencyToText(ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value : 0, ResData.ResMain.SaleCur, "");
        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("Всего наименований 2, на сумму {0} {1}",
                            ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "",
                            " ( " + numToWord + " )"),
            x = 40,
            y = 333,
            FontSize = 10
        });

        writeList.Add(new TvReport.Coordinate { value = opr.BossName, x = 532, y = 186, FontSize = 10, RightToLeft = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = opr.ContName, x = 532, y = 158, FontSize = 10, RightToLeft = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = "По доверенности №021110/1 от 02.11.10.", x = 420, y = 143, FontSize = 10, RightToLeft = true, Align = TvReport.fontAlign.Right });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 335, y = 77, FontSize = 10 });

        return new TvReport.SunReport.SunReport().createProforma(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showInvoice(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        BankRecord oprBank = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);
        BankRecord agencyBank = new Banks().getBank(UserData.Market, agency.Bank1, ref errorMsg);
        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("{0}, ИНН {1}, {2}, {3} г, {4},  р/с  {5}  {6}, в банке АКБ '{7}' (ОАО),",
                            opr.FirmName,
                            opr.TaxAccNo,
                            opr.InvAddrZip,
                            opr.InvAddrCity,
                            opr.InvAddress,
                            opr.Bank2AccNo,
                            opr.Bank2IBAN,
                            oprBank.NameL),
            x = 40,
            y = 544,
            FontSize = 8
        });
        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("БИК {0}, к/с {1}",
                            opr.Bank1IBAN,
                            opr.Bank1AccNo),
            x = 40,
            y = 535,
            FontSize = 8
        });

        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("{0}, ИНН {1}, тел.: {2}, факс: {3},",
                            agency.FirmName,
                            agency.TaxAccNo,
                            agency.Phone1,
                            agency.Fax1),
            x = 118,
            y = 486,
            FontSize = 8
        });
        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("р/с {0}, в банке КБ {1} ООО, БИК {2}, к/с {3}",
                            agency.Bank2AccNo,
                            agencyBank.NameL,
                            agency.Bank1IBAN,
                            agency.Bank1AccNo),
            x = 118,
            y = 477,
            FontSize = 8
        });

        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("{0}, ИНН {1}, {2}, {3} г, {4},",
                            opr.FirmName,
                            opr.TaxAccNo,
                            opr.InvAddrZip,
                            opr.InvAddrCity,
                            opr.InvAddress),
            x = 118,
            y = 455,
            FontSize = 8
        });
        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("р/с  {0}, {1}, в банке АКБ '{2}' (ОАО),БИК {3}, к/с {4}",
                            opr.Bank2AccNo,
                            opr.Bank2IBAN,
                            oprBank.NameL,
                            opr.Bank1IBAN,
                            opr.Bank1AccNo),
            x = 118,
            y = 446,
            FontSize = 8
        });

        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("{0}, ИНН {1}, {2} тел.: {3}, факс: {4},",
                            agency.FirmName,
                            agency.TaxAccNo,
                            agency.InvAddress,
                            agency.Phone1,
                            agency.Fax1),
            x = 118,
            y = 424,
            FontSize = 8
        });
        writeList.Add(new TvReport.Coordinate
        {
            value = string.Format("р/с {0}, в банке КБ \"{1}\" ООО, БИК {2}, к/с {3}",
                            agency.Bank2AccNo,
                            agency.Bank2IBAN,
                            agencyBank.NameL,
                            agency.Bank1IBAN,
                            agency.Bank1AccNo),
            x = 118,
            y = 415,
            FontSize = 8
        });

        writeList.Add(new TvReport.Coordinate { value = "Агентский договор № " + new Agency().getAgencyAgreement(UserData, ResData.ResMain.BegDate.Value, ref errorMsg), x = 118, y = 394, FontSize = 8 });

        string agencyPayable = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value.ToString("#,###.00") : "";
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 294, y = 352, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 364, y = 352, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "Туристическая путевка " + ResData.ResMain.ResNo, x = 63, y = 263, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyPayable, x = 565, y = 263, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = agencyPayable, x = 630, y = 263, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = agencyPayable, x = 798, y = 263, FontSize = 8, RightToLeft = true });

        writeList.Add(new TvReport.Coordinate { value = agencyPayable, x = 798, y = 249, FontSize = 8, RightToLeft = true });
        writeList.Add(new TvReport.Coordinate { value = agencyPayable, x = 798, y = 236, FontSize = 8, RightToLeft = true });

        CurrencyRecord currencyInfo = new Banks().getCurrency(UserData.Market, ResData.ResMain.SaleCur, ref errorMsg);
        string amountToWord = new TvTools.NumberToWordRU().CurrencyToText(ResData.ResMain.AgencyPayable.Value, currencyInfo.NameL, currencyInfo.FracName);
        writeList.Add(new TvReport.Coordinate { value = amountToWord, x = 40, y = 112, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = opr.BossName, x = 294, y = 88, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = opr.ContName, x = 294, y = 69, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "\" " + DateTime.Today.Day + " \"", x = 182, y = 22, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToString("MMMM"), x = 235, y = 22, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = DateTime.Today.Year.ToString() + " года", x = 295, y = 22, FontSize = 8 });

        return new TvReport.SunReport.SunReport().createInvoice(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    
}



