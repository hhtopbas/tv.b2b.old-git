﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using TvTools;
using TvReport.NovReport;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Text;
using System.Text.RegularExpressions;

public class NovReport
{
    /*
    public string showContract_AIP(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        bool imhHolpack = false;
        byte[] contractLogo = new TvReport.NovReport.NovReport().getContractLogo(ResData.ResMain.PLOperator, ResData.ResMain.HolPack, ref imhHolpack, ref errorMsg);

        writeList.Add(new TvReport.Coordinate { Pic = contractLogo, x = 61.275f, y = 746.7f, H = 62, Type = TvReport.writeType.Image });

        writeList.Add(new TvReport.Coordinate { value = "TURIZMO PASLAUGŲ TEIKIMO SUTARTIS", x = 230, y = 761, FontSize = 14, bold = true });
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);

        string oprAddress = opr.FirmName + " " + opr.Address + " " + opr.AddrZip + " " + opr.AddrCity + " " + opr.Phone1 + " " + opr.Email1;
        writeList.Add(new TvReport.Coordinate { value = oprAddress, x = 60, y = 727.25f, FontSize = 5 });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 726.75f, H = 0, W = 250, LineWidth = Convert.ToSingle(25 / 100.0) });
        string oprInfo = "Kelionės organizatoriaus prievolių įvykdymo užtikrinimo draudimo polisas Nr. " +
                         opr.InsPolicyNo + " " + (opr.InsExpDate.HasValue ? opr.InsExpDate.Value.ToShortDateString() : "");
        writeList.Add(new TvReport.Coordinate { value = oprInfo, x = 60, y = 722, FontSize = 5 });
        writeList.Add(new TvReport.Coordinate { value = "Kelionės sutarties Nr. " + ResData.ResMain.ResNo, x = 312, y = 727, FontSize = 11, bold = true });

        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 717, H = 0, W = 456, LineWidth = 0.5f });

        writeList.Add(new TvReport.Coordinate { value = "Sutarties sudarymo data:", x = 60, y = 708.5F, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "", x = 160.3f, y = 708.5F, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "Agentūra:", x = 60, y = 697.1f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agent.FirmName, x = 100.0f, y = 697.1f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Agentūros darbuotojas:", x = 288, y = 697.1f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyUserNameL, x = 378.45f, y = 697.1f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "Faksas:", x = 60, y = 685.7f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DFax, x = 91.35f, y = 685.7f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Telefonas:", x = 185.4f, y = 685.7f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Phone1, x = 228.15f, y = 685.7f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "El.paštas:", x = 320.9f, y = 685.7f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.EMail1, x = 360.8f, y = 685.7f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "TURISTO (-Ų) duomenys:", x = 61.5f, y = 667.175f, FontSize = 7, bold = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 666, H = 0, W = 456, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "vardas, pavardé", x = 63.4f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { value = "asmens kodas", x = 215.75f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 213.75f, y = 666, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "paso serija, numeris", x = 315.5f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 313.5f, y = 666, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "pasas galioja iki", x = 412.4f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 410.4f, y = 666, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 656, H = 0, W = 456, LineWidth = 0.1f });
        float _y = 656.0f;
        foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0))
        {
            _y -= 10;
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.25f });
            writeList.Add(new TvReport.Coordinate { value = row.Name + " " + row.Surname, x = 63.4f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.IDNo, x = 215.75f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.PassSerie + ", " + row.PassNo, x = 315.5f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.PassExpDate.HasValue ? row.PassExpDate.Value.ToShortDateString() : "", x = 412.4f, y = _y + 3, FontSize = 6 });
        }
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 666, H = _y - 666, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 516, y = 666, H = _y - 666, W = 0, LineWidth = 0.1f });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "TURISTO (-Ų) papildomi duomenys", x = 61.5f, y = _y, FontSize = 6, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "(namų adresas, telefonas, faksas arba el.paštas) :", x = 164.8f, y = _y, FontSize = 6, Italic = true });
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
        _y -= 8;
        if (_leader != null)
            writeList.Add(new TvReport.Coordinate
            {
                value = !Equals(_leader.ContactAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip + " " + _leader.AddrHomeTel + " " + _leader.AddrHomeFax + " " + _leader.AddrHomeEmail : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip + " " + _leader.AddrWorkTel + " " + _leader.AddrWorkFax + " " + _leader.AddrWorkEMail,
                x = 61.5f,
                y = _y,
                FontSize = 6
            });

        _y -= 5;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.5f });

        _y -= 11.4f;
        string CountryCity = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg) + " / " +
                           ResData.ResMain.ArrCityNameL;
        writeList.Add(new TvReport.Coordinate { value = "Kelionė lėktuvu į:", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = CountryCity, x = 131.025f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Išvykimo data:", x = 252.45f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "", x = 312.15f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Grįžimo data:", x = 466.2f, y = _y, FontSize = 8, Align = TvReport.fontAlign.Right, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "", x = 470.2f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2))
        {
            _y -= 3f;
            HotelRecord hotel = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Viešbučio pavadinimas :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = hotel.LocalName + ", (" + row.DepLocationNameL + ")", x = 158.4f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Nakvynių skaičius :", x = 466.2f, y = _y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = row.Duration.ToString(), x = 470.2f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Kategorija (šalyje) :", x = 61.5f, y = _y - 10.6875f, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = hotel != null ? hotel.OfCategory : "", x = 141.3f, y = _y - 10.6875f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Maitinimo tipas :", x = 181.2f, y = _y - 10.6875f, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = row.BoardNameL, x = 249.6f, y = _y - 10.6875f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Kambarių skaičius :", x = 466.2f, y = _y - 10.6875f, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = "1", x = 470.2f, y = _y - 10.6875f, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = "Kambario tipas :", x = 61.5f, y = _y - (2 * 10.6875f), FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = row.AccomNameL + " / " + row.RoomNameL, x = 129.9f, y = _y - (2 * 10.6875f), FontSize = 8 });
            _y = _y - (3 * 12.6875f);
        }
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.5f });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Apgyvendinimo tipas :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field1", value = "", x = 152.7f, y = _y - 3.0f, H = 12.0f, W = 342.0f, LineWidth = 0.2f, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.Text });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "(viešbutis, motelis, svečių namai, privatus sektorius, vasaros tipo nameliai, bungalai,kita)", x = 152.7f, y = _y, FontSize = 5, Italic = true });
        _y -= 9.4f;
        writeList.Add(new TvReport.Coordinate { value = "Patogumai :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field2", value = "Off", Caption = "kambariuose ", x = 109.95f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field3", value = "Off", Caption = "; bloke ", x = 167.65f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field4", value = "Off", Caption = "; koridoriuje ", x = 205.4f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field5", value = "Off", Caption = "; teritorijoje ", x = 258.825f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field6", value = "On", Caption = "; kaip nurodyta sutarties prieduose ", x = 309.4f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Kitos paslaugos, įskaičiuotos į kelionės kainą :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field7", value = "Off", Caption = "kelionių organizatoriaus atstovo paslaugos ", x = 241.0f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field8", value = "Off", Caption = "; vizų įforminimas ", x = 407.0f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { FieldName = "field9", value = "Off", Caption = "; atvežimas: oro uostas-viešbutis ", x = 61.5f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field10", value = "Off", Caption = "; viešbutis oro uostas ", x = 191.175f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field11", value = "On", Caption = "; kaip nurodyta sutarties prieduose ", x = 283.8f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Specialūs turisto pageidavimai :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field12", value = "", x = 185.45f, y = _y - 3.0f, H = 12.0f, W = 313.0f, LineWidth = 0.2f, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.Text });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "(vykdomi esant galimybei)(kaimyniniai kambariai, tam tikra kambarių vieta, kt.)", x = 185.45f, y = _y, FontSize = 5, Italic = true });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Specialūs susitarimai :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field13", value = "", x = 152f, y = _y - 3.0f, H = 12.0f, W = 356.0f, LineWidth = 0.2f, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.Text });
        _y -= 11.4f;
        List<ResServiceRecord> flights = ResData.ResService.Where(w => w.ServiceType == "FLIGHT").ToList<ResServiceRecord>();
        if (flights.Count > 0)
        {
            FlightDayRecord depFlight = new Flights().getFlightDay(flights.FirstOrDefault().Service, flights.FirstOrDefault().BegDate.Value, ref errorMsg);
            FlightDayRecord retFlight = new Flights().getFlightDay(flights.LastOrDefault().Service, flights.LastOrDefault().BegDate.Value, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Pirminis išvykimo laikas :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = depFlight.DepTime.HasValue ? depFlight.DepTime.Value.ToShortTimeString() : "", x = 162.675f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Skrydį vykdantis oro vežėjas :", x = 213.75f, y = _y, FontSize = 8, bold = true });
            AirlineRecord depAirline = new Flights().getAirline(UserData.Market, depFlight.Airline, ref errorMsg);
            AirlineRecord retAirline = new Flights().getAirline(UserData.Market, retFlight.Airline, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = (depAirline != null ? depAirline.NameL : "") + (retAirline != null ? " / " + retAirline.NameL : ""), x = 328.3f, y = _y, FontSize = 8 });
            _y -= 8f;
            writeList.Add(new TvReport.Coordinate { value = "(Čia ir kelionės dokumentuose pateikiamas išvykimo laikas yra preliminarus ir iki faktinės kelionės datos gali keistis. Nei kelionių organizatorius, nei skrydį", x = 61.5f, y = _y, FontSize = 6 });
            _y -= 8f;
            writeList.Add(new TvReport.Coordinate { value = "vykdantis oro vežėjas jo negarantuoja. Šis skrydžio laikas yra pateikiamas tik turistų informacijai, todėl ne vėliau kaip 1 dieną prieš skrydį reikėtų jį pasitikrinti", x = 61.5f, y = _y, FontSize = 6 });
            _y -= 8f;
            writeList.Add(new TvReport.Coordinate { value = "interneto svetainėje www.sofatravel.lt.)", x = 61.5f, y = _y, FontSize = 6 });
            _y -= 11.4f;
            writeList.Add(new TvReport.Coordinate { value = "Pirminė išvykimo vieta :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            AirportRecord depAirport = new Flights().getAirport(UserData.Market, depFlight.DepAirport, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = depAirport != null ? depAirport.LocalName : "", x = 155.5f, y = _y, FontSize = 8 });
            _y -= 11.4f;
            writeList.Add(new TvReport.Coordinate { value = "Pirminė grįžimo vieta :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            AirportRecord retAirport = new Flights().getAirport(UserData.Market, retFlight.ArrAirport, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = retAirport != null ? retAirport.LocalName : "", x = 155.5f, y = _y, FontSize = 8 });
        }
        _y -= 4;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.5f });
        #region Customer Price Table
        List<TvReport.tableRow> prices = new List<TvReport.tableRow>();
        List<TvReport.tableCell> priceRow = new List<TvReport.tableCell>();
        for (int i = 0; i <= ResData.ResCust.Count; i++)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = i == 0 ? "" : i.ToString();
            cell.Width = i == 0 ? 143f : 45f;
            cell.Align = TvReport.fontAlign.Center;
            priceRow.Add(cell);
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });

        //Kelialapio kaina vienam asmeniui
        var queryService = from q1 in ResData.ResService
                           join q2 in ResData.ResCustPrice on q1.RecID equals q2.ServiceID
                           where Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID == 0)
                           select new { custNo = q2.CustNo, price = q2.SalePrice };
        var queryServiceExt = from q1 in ResData.ResServiceExt
                              join q2 in ResData.ResCustPrice on q1.RecID equals q2.ExtServiceID
                              where Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID != 0)
                              select new { custNo = q2.CustNo, price = q2.SalePrice };
        var queryUnion = queryService.Union(queryServiceExt);
        var queryIncPac = from q in queryUnion
                          group q by new { custNo = q.custNo } into k
                          select new { k.Key.custNo, price = (queryUnion.Where(w => w.custNo == k.Key.custNo).Sum(s => s.price)) };

        if (queryIncPac.Count() > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "Kelialapio kaina vienam asmeniui";
            cell.Width = 143f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (var row in queryIncPac.OrderBy(o => o.custNo))
            {
                cell = new TvReport.tableCell();
                cell.Text = row.price.HasValue ? row.price.Value.ToString("#,###.00") : "";
                cell.Width = 45f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
            prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        }
        var queryServiceOutPack = from q1 in ResData.ResService
                                  join q2 in ResData.ResCustPrice on q1.RecID equals q2.ServiceID
                                  where !Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID == 0)
                                  select new { id = q2.ExtServiceID, desc = q1.ServiceNameL, custNo = q2.CustNo, price = q2.SalePrice };
        var queryServiceExtOutPack = from q1 in ResData.ResServiceExt
                                     join q2 in ResData.ResCustPrice on q1.RecID equals q2.ExtServiceID
                                     where !Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID != 0)
                                     select new { id = q2.ExtServiceID, desc = q1.ExtServiceNameL, custNo = q2.CustNo, price = q2.SalePrice };
        var queryUnionOutPack = queryServiceOutPack.Union(queryServiceExtOutPack);
        var queryOutPack = from q in queryUnionOutPack
                           select q;

        if (queryOutPack.Count() > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            var query1 = from q in queryOutPack
                         group q by new { id = q.id, desc = q.desc } into k
                         select new { id = k.Key.id, desc = k.Key.desc };
            foreach (var r1 in query1)
            {
                priceRow = new List<TvReport.tableCell>();
                TvReport.tableCell cell = new TvReport.tableCell();
                cell.Text = r1.desc;
                cell.Width = 143f;
                cell.Align = TvReport.fontAlign.Left;
                priceRow.Add(cell);
                foreach (var row in queryOutPack.Where(w => w.id == r1.id).OrderBy(o => o.custNo))
                {
                    cell = new TvReport.tableCell();
                    cell.Text = row.price.HasValue ? row.price.Value.ToString("#,###.00") : "";
                    cell.Width = 45f;
                    cell.Align = TvReport.fontAlign.Right;
                    priceRow.Add(cell);
                }
                prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
            }
        }
        priceRow = new List<TvReport.tableCell>();
        decimal priceTotal = 0;
        if (ResData.ResCust.Count > 0)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "";
            cell.Width = 143f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (ResCustRecord r1 in ResData.ResCust)
            {
                decimal? totalPrice = null;
                int? custNo = r1.CustNo;
                totalPrice = (queryIncPac.Where(w => w.custNo == custNo).Sum(s => s.price).HasValue ? queryIncPac.Where(w => w.custNo == custNo).Sum(s => s.price).Value : Convert.ToDecimal(0)) +
                             (queryOutPack.Where(w => w.custNo == custNo).Sum(s => s.price).HasValue ? queryOutPack.Where(w => w.custNo == custNo).Sum(s => s.price).Value : Convert.ToDecimal(0));
                cell = new TvReport.tableCell();
                cell.Text = totalPrice.HasValue ? totalPrice.Value.ToString("#,###.00") : "";
                priceTotal += totalPrice.HasValue ? totalPrice.Value : 0;
                cell.Width = 45f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        #endregion

        string pdfTabelStr = Newtonsoft.Json.JsonConvert.SerializeObject(prices);
        _y -= 4f;
        writeList.Add(new TvReport.Coordinate { value = pdfTabelStr, Type = TvReport.writeType.PdfTable, x = 62, y = _y, FontSize = 7 });
        _y -= ((11.0f * (prices.Count() + 2)) + 11.4f);
        writeList.Add(new TvReport.Coordinate { value = "Bendra užsakymo kaina", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = priceTotal > 0 ? priceTotal.ToString("#,###.00") : "", x = 259.95f, y = _y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = "Mokėjimai", x = 379.02f, y = _y, FontSize = 8, bold = true });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Įmokėta", x = 61.5f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { FieldName = "field20", value = "Off", Caption = "Grynais", x = 283.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field21", value = "Off", Caption = "Pavedimu", x = 333.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field22", value = "Off", Caption = "Mokėjimo kortele", x = 391.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field23", value = "Off", Caption = "Banko finansavimas", x = 470.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        _y -= 11f;
        writeList.Add(new TvReport.Coordinate { value = "Įmokėta", x = 61.5f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { FieldName = "field30", value = "Off", Caption = "Grynais", x = 283.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field31", value = "Off", Caption = "Pavedimu", x = 333.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field32", value = "Off", Caption = "Mokėjimo kortele", x = 391.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field33", value = "Off", Caption = "Banko finansavimas", x = 470.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Nuolaidos", x = 61.5f, y = _y, FontSize = 8, bold = true });
        decimal PasSupDis = ResData.ResMain.PasSupDis.HasValue ? ResData.ResMain.PasSupDis.Value : 0;
        decimal EBPas = ResData.ResMain.EBPas.HasValue ? ResData.ResMain.EBPas.Value : 0;
        decimal AgencyDisPasVal = ResData.ResMain.AgencyDisPasVal.HasValue ? ResData.ResMain.AgencyDisPasVal.Value : 0;
        decimal bonus = new TvReport.NovReport.NovReport().getBonus(ResData.ResMain.ResNo, ref errorMsg);
        decimal TotalDiscount = PasSupDis - EBPas - AgencyDisPasVal - bonus;
        writeList.Add(new TvReport.Coordinate { value = ": " + TotalDiscount.ToString("#,###.00"), x = 160f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Bendra užsakymo kaina", x = 61.5f, y = _y, FontSize = 8, bold = true });
        CurrencyRecord currency = new Banks().getCurrency(UserData.Market, ResData.ResMain.SaleCur, ref errorMsg);
        string numberToword = new LibVB.VBUtils.NumberToWordLT().SumaZod(ResData.ResMain.PasPayable.Value, currency.Name, currency.FracName);
        string totalPriceStr = ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " ( " + numberToword + " ) ";
        writeList.Add(new TvReport.Coordinate { value = ": " + totalPriceStr, x = 160f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { FieldName = "field40", value = "On", Caption = "", x = 61.5f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { value = "Esu informuota(-s) apie galimybę sudaryti medicininių išlaidų, neįvykusios kelionės rizikos, nelaimingų", x = 71.5f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "atsitikimų draudimo sutartis ir su draudimo taisyklėmis susipažinau.", x = 71.5f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "(Turisto parašas)", x = 510.3f, y = _y, FontSize = 7, Italic = true, Align = TvReport.fontAlign.Right });

        return new TvReport.NovReport.NovReport().createContract(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showContract_LT(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        bool imgHolpack = false;
        byte[] contractLogo = new TvReport.NovReport.NovReport().getContractLogo(ResData.ResMain.PLOperator, ResData.ResMain.HolPack, ref imgHolpack, ref errorMsg);

        writeList.Add(new TvReport.Coordinate { Pic = contractLogo, x = 61.275f, y = 746.7f, H = 62, Type = TvReport.writeType.Image });

        writeList.Add(new TvReport.Coordinate { value = "TURIZMO PASLAUGŲ TEIKIMO SUTARTIS", x = 230, y = 761, FontSize = 14, bold = true });
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);

        string oprAddress = opr.FirmName + " " + opr.Address + " " + opr.AddrZip + " " + opr.AddrCity + " " + opr.Phone1 + " " + opr.Email1;
        writeList.Add(new TvReport.Coordinate { value = oprAddress, x = 60, y = 727.25f, FontSize = 5 });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 726.75f, H = 0, W = 250, LineWidth = Convert.ToSingle(25 / 100.0) });
        string oprInfo = "Kelionės organizatoriaus prievolių įvykdymo užtikrinimo draudimo polisas Nr. " +
                         opr.InsPolicyNo + " " + (opr.InsExpDate.HasValue ? opr.InsExpDate.Value.ToShortDateString() : "");
        writeList.Add(new TvReport.Coordinate { value = oprInfo, x = 60, y = 722, FontSize = 5 });
        writeList.Add(new TvReport.Coordinate { value = "Kelionės sutarties Nr. " + ResData.ResMain.ResNo, x = 312, y = 727, FontSize = 11, bold = true });

        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 717, H = 0, W = 456, LineWidth = 0.5f });

        writeList.Add(new TvReport.Coordinate { value = "Sutarties sudarymo data:", x = 60, y = 708.5F, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "", x = 160.3f, y = 708.5F, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "Agentūra:", x = 60, y = 697.1f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agent.FirmName, x = 100.0f, y = 697.1f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Agentūros darbuotojas:", x = 288, y = 697.1f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyUserNameL, x = 378.45f, y = 697.1f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "Faksas:", x = 60, y = 685.7f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DFax, x = 91.35f, y = 685.7f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Telefonas:", x = 185.4f, y = 685.7f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Phone1, x = 228.15f, y = 685.7f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "El.paštas:", x = 320.9f, y = 685.7f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.EMail1, x = 360.8f, y = 685.7f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "TURISTO (-Ų) duomenys:", x = 61.5f, y = 667.175f, FontSize = 7, bold = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 666, H = 0, W = 456, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "vardas, pavardé", x = 63.4f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { value = "asmens kodas", x = 215.75f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 213.75f, y = 666, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "paso serija, numeris", x = 315.5f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 313.5f, y = 666, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "pasas galioja iki", x = 412.4f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 410.4f, y = 666, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 656, H = 0, W = 456, LineWidth = 0.1f });
        float _y = 656.0f;
        foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0))
        {
            _y -= 10;
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.25f });
            writeList.Add(new TvReport.Coordinate { value = row.Name + " " + row.Surname, x = 63.4f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.IDNo, x = 215.75f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.PassSerie + ", " + row.PassNo, x = 315.5f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.PassExpDate.HasValue ? row.PassExpDate.Value.ToShortDateString() : "", x = 412.4f, y = _y + 3, FontSize = 6 });
        }
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 666, H = _y - 666, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 516, y = 666, H = _y - 666, W = 0, LineWidth = 0.1f });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "TURISTO (-Ų) papildomi duomenys", x = 61.5f, y = _y, FontSize = 6, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "(namų adresas, telefonas, faksas arba el.paštas) :", x = 164.8f, y = _y, FontSize = 6, Italic = true });
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
        _y -= 8;
        if (_leader != null)
            writeList.Add(new TvReport.Coordinate
            {
                value = !Equals(_leader.ContactAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip + " " + _leader.AddrHomeTel + " " + _leader.AddrHomeFax + " " + _leader.AddrHomeEmail : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip + " " + _leader.AddrWorkTel + " " + _leader.AddrWorkFax + " " + _leader.AddrWorkEMail,
                x = 61.5f,
                y = _y,
                FontSize = 6
            });

        _y -= 5;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.5f });

        _y -= 11.4f;
        string CountryCity = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg) + " / " +
                           ResData.ResMain.ArrCityNameL;
        writeList.Add(new TvReport.Coordinate { value = "Kelionė lėktuvu į:", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = CountryCity, x = 131.025f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Išvykimo data:", x = 252.45f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "", x = 312.15f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Grįžimo data:", x = 466.2f, y = _y, FontSize = 8, Align = TvReport.fontAlign.Right, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "", x = 470.2f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2))
        {
            _y -= 3f;
            HotelRecord hotel = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Viešbučio pavadinimas :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = hotel.LocalName + ", (" + row.DepLocationNameL + ")", x = 158.4f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Nakvynių skaičius :", x = 466.2f, y = _y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = row.Duration.ToString(), x = 470.2f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Kategorija (šalyje) :", x = 61.5f, y = _y - 10.6875f, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = hotel != null ? hotel.OfCategory : "", x = 141.3f, y = _y - 10.6875f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Maitinimo tipas :", x = 181.2f, y = _y - 10.6875f, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = row.BoardNameL, x = 249.6f, y = _y - 10.6875f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Kambarių skaičius :", x = 466.2f, y = _y - 10.6875f, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = "1", x = 470.2f, y = _y - 10.6875f, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = "Kambario tipas :", x = 61.5f, y = _y - (2 * 10.6875f), FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = row.AccomNameL + " / " + row.RoomNameL, x = 129.9f, y = _y - (2 * 10.6875f), FontSize = 8 });
            _y = _y - (3 * 12.6875f);
        }
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.5f });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Apgyvendinimo tipas :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field1", value = "", x = 152.7f, y = _y - 3.0f, H = 12.0f, W = 342.0f, LineWidth = 0.2f, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.Text });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "(viešbutis, motelis, svečių namai, privatus sektorius, vasaros tipo nameliai, bungalai,kita)", x = 152.7f, y = _y, FontSize = 5, Italic = true });
        _y -= 9.4f;
        writeList.Add(new TvReport.Coordinate { value = "Patogumai :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field2", value = "Off", Caption = "kambariuose ", x = 109.95f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field3", value = "Off", Caption = "; bloke ", x = 167.65f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field4", value = "Off", Caption = "; koridoriuje ", x = 205.4f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field5", value = "Off", Caption = "; teritorijoje ", x = 258.825f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field6", value = "On", Caption = "; kaip nurodyta sutarties prieduose ", x = 309.4f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Kitos paslaugos, įskaičiuotos į kelionės kainą :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field7", value = "Off", Caption = "kelionių organizatoriaus atstovo paslaugos ", x = 241.0f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field8", value = "Off", Caption = "; vizų įforminimas ", x = 407.0f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { FieldName = "field9", value = "Off", Caption = "; atvežimas: oro uostas-viešbutis ", x = 61.5f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field10", value = "Off", Caption = "; viešbutis oro uostas ", x = 191.175f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field11", value = "On", Caption = "; kaip nurodyta sutarties prieduose ", x = 283.8f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Specialūs turisto pageidavimai :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field12", value = "", x = 185.45f, y = _y - 3.0f, H = 12.0f, W = 313.0f, LineWidth = 0.2f, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.Text });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "(vykdomi esant galimybei)(kaimyniniai kambariai, tam tikra kambarių vieta, kt.)", x = 185.45f, y = _y, FontSize = 5, Italic = true });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Specialūs susitarimai :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field13", value = "", x = 152f, y = _y - 3.0f, H = 12.0f, W = 356.0f, LineWidth = 0.2f, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.Text });
        _y -= 11.4f;
        List<ResServiceRecord> flights = ResData.ResService.Where(w => w.ServiceType == "FLIGHT").ToList<ResServiceRecord>();
        if (flights.Count > 0)
        {
            FlightDayRecord depFlight = new Flights().getFlightDay(flights.FirstOrDefault().Service, flights.FirstOrDefault().BegDate.Value, ref errorMsg);
            FlightDayRecord retFlight = new Flights().getFlightDay(flights.LastOrDefault().Service, flights.LastOrDefault().BegDate.Value, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Pirminis išvykimo laikas :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = depFlight.DepTime.HasValue ? depFlight.DepTime.Value.ToShortTimeString() : "", x = 162.675f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Skrydį vykdantis oro vežėjas :", x = 213.75f, y = _y, FontSize = 8, bold = true });
            AirlineRecord depAirline = new Flights().getAirline(UserData.Market, depFlight.Airline, ref errorMsg);
            AirlineRecord retAirline = new Flights().getAirline(UserData.Market, retFlight.Airline, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = (depAirline != null ? depAirline.NameL : "") + (retAirline != null ? " / " + retAirline.NameL : ""), x = 328.3f, y = _y, FontSize = 8 });
            _y -= 8f;
            writeList.Add(new TvReport.Coordinate { value = "(Čia ir kelionės dokumentuose pateikiamas išvykimo laikas yra preliminarus ir iki faktinės kelionės datos gali keistis. Nei kelionių organizatorius, nei skrydį", x = 61.5f, y = _y, FontSize = 6 });
            _y -= 8f;
            writeList.Add(new TvReport.Coordinate { value = "vykdantis oro vežėjas jo negarantuoja. Šis skrydžio laikas yra pateikiamas tik turistų informacijai, todėl ne vėliau kaip 1 dieną prieš skrydį reikėtų jį pasitikrinti", x = 61.5f, y = _y, FontSize = 6 });
            _y -= 8f;
            writeList.Add(new TvReport.Coordinate { value = "interneto svetainėje www.novaturas.lt.)", x = 61.5f, y = _y, FontSize = 6 });
            _y -= 11.4f;
            writeList.Add(new TvReport.Coordinate { value = "Pirminė išvykimo vieta :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            AirportRecord depAirport = new Flights().getAirport(UserData.Market, depFlight.DepAirport, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = depAirport != null ? depAirport.LocalName : "", x = 155.5f, y = _y, FontSize = 8 });
            _y -= 11.4f;
            writeList.Add(new TvReport.Coordinate { value = "Pirminė grįžimo vieta :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            AirportRecord retAirport = new Flights().getAirport(UserData.Market, retFlight.ArrAirport, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = retAirport != null ? retAirport.LocalName : "", x = 155.5f, y = _y, FontSize = 8 });
        }
        _y -= 4;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.5f });
        #region Customer Price Table
        List<TvReport.tableRow> prices = new List<TvReport.tableRow>();
        List<TvReport.tableCell> priceRow = new List<TvReport.tableCell>();
        for (int i = 0; i <= ResData.ResCust.Count; i++)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = i == 0 ? "" : i.ToString();
            cell.Width = i == 0 ? 143f : 45f;
            cell.Align = TvReport.fontAlign.Center;
            priceRow.Add(cell);
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });

        //Kelialapio kaina vienam asmeniui
        var queryService = from q1 in ResData.ResService
                           join q2 in ResData.ResCustPrice on q1.RecID equals q2.ServiceID
                           where Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID == 0)
                           select new { custNo = q2.CustNo, price = q2.SalePrice };
        var queryServiceExt = from q1 in ResData.ResServiceExt
                              join q2 in ResData.ResCustPrice on q1.RecID equals q2.ExtServiceID
                              where Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID != 0)
                              select new { custNo = q2.CustNo, price = q2.SalePrice };
        var queryUnion = queryService.Union(queryServiceExt);
        var queryIncPac = from q in queryUnion
                          group q by new { custNo = q.custNo } into k
                          select new { k.Key.custNo, price = (queryUnion.Where(w => w.custNo == k.Key.custNo).Sum(s => s.price)) };

        if (queryIncPac.Count() > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "Kelialapio kaina vienam asmeniui";
            cell.Width = 143f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (var row in queryIncPac.OrderBy(o => o.custNo))
            {
                cell = new TvReport.tableCell();
                cell.Text = row.price.HasValue ? row.price.Value.ToString("#,###.00") : "";
                cell.Width = 45f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
            prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        }
        var queryServiceOutPack = from q1 in ResData.ResService
                                  join q2 in ResData.ResCustPrice on q1.RecID equals q2.ServiceID
                                  where !Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID == 0)
                                  select new { id = q2.ExtServiceID, desc = q1.ServiceNameL, custNo = q2.CustNo, price = q2.SalePrice };
        var queryServiceExtOutPack = from q1 in ResData.ResServiceExt
                                     join q2 in ResData.ResCustPrice on q1.RecID equals q2.ExtServiceID
                                     where !Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID != 0)
                                     select new { id = q2.ExtServiceID, desc = q1.ExtServiceNameL, custNo = q2.CustNo, price = q2.SalePrice };
        var queryUnionOutPack = queryServiceOutPack.Union(queryServiceExtOutPack);
        var queryOutPack = from q in queryUnionOutPack
                           select q;

        if (queryOutPack.Count() > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            var query1 = from q in queryOutPack
                         group q by new { id = q.id, desc = q.desc } into k
                         select new { id = k.Key.id, desc = k.Key.desc };
            foreach (var r1 in query1)
            {
                priceRow = new List<TvReport.tableCell>();
                TvReport.tableCell cell = new TvReport.tableCell();
                cell.Text = r1.desc;
                cell.Width = 143f;
                cell.Align = TvReport.fontAlign.Left;
                priceRow.Add(cell);
                foreach (var row in queryOutPack.Where(w => w.id == r1.id).OrderBy(o => o.custNo))
                {
                    cell = new TvReport.tableCell();
                    cell.Text = row.price.HasValue ? row.price.Value.ToString("#,###.00") : "";
                    cell.Width = 45f;
                    cell.Align = TvReport.fontAlign.Right;
                    priceRow.Add(cell);
                }
                prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
            }
        }
        priceRow = new List<TvReport.tableCell>();
        decimal priceTotal = 0;
        if (ResData.ResCust.Count > 0)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "";
            cell.Width = 143f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (ResCustRecord r1 in ResData.ResCust)
            {
                decimal? totalPrice = null;
                int? custNo = r1.CustNo;
                totalPrice = (queryIncPac.Where(w => w.custNo == custNo).Sum(s => s.price).HasValue ? queryIncPac.Where(w => w.custNo == custNo).Sum(s => s.price).Value : Convert.ToDecimal(0)) +
                             (queryOutPack.Where(w => w.custNo == custNo).Sum(s => s.price).HasValue ? queryOutPack.Where(w => w.custNo == custNo).Sum(s => s.price).Value : Convert.ToDecimal(0));
                cell = new TvReport.tableCell();
                cell.Text = totalPrice.HasValue ? totalPrice.Value.ToString("#,###.00") : "";
                priceTotal += totalPrice.HasValue ? totalPrice.Value : 0;
                cell.Width = 45f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        #endregion

        string pdfTabelStr = Newtonsoft.Json.JsonConvert.SerializeObject(prices);
        _y -= 4f;
        writeList.Add(new TvReport.Coordinate { value = pdfTabelStr, Type = TvReport.writeType.PdfTable, x = 62, y = _y, FontSize = 7 });
        _y -= ((11.0f * (prices.Count() + 2)) + 11.4f);
        writeList.Add(new TvReport.Coordinate { value = "Bendra užsakymo kaina", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = priceTotal > 0 ? priceTotal.ToString("#,###.00") : "", x = 259.95f, y = _y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = "Mokėjimai", x = 379.02f, y = _y, FontSize = 8, bold = true });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Įmokėta", x = 61.5f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { FieldName = "field20", value = "Off", Caption = "Grynais", x = 283.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field21", value = "Off", Caption = "Pavedimu", x = 333.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field22", value = "Off", Caption = "Mokėjimo kortele", x = 391.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field23", value = "Off", Caption = "Banko finansavimas", x = 470.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        _y -= 11f;
        writeList.Add(new TvReport.Coordinate { value = "Įmokėta", x = 61.5f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { FieldName = "field30", value = "Off", Caption = "Grynais", x = 283.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field31", value = "Off", Caption = "Pavedimu", x = 333.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field32", value = "Off", Caption = "Mokėjimo kortele", x = 391.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field33", value = "Off", Caption = "Banko finansavimas", x = 470.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Nuolaidos", x = 61.5f, y = _y, FontSize = 8, bold = true });
        decimal PasSupDis = ResData.ResMain.PasSupDis.HasValue ? ResData.ResMain.PasSupDis.Value : 0;
        decimal EBPas = ResData.ResMain.EBPas.HasValue ? ResData.ResMain.EBPas.Value : 0;
        decimal AgencyDisPasVal = ResData.ResMain.AgencyDisPasVal.HasValue ? ResData.ResMain.AgencyDisPasVal.Value : 0;
        decimal bonus = new TvReport.NovReport.NovReport().getBonus(ResData.ResMain.ResNo, ref errorMsg);
        decimal TotalDiscount = PasSupDis - EBPas - AgencyDisPasVal - bonus;
        writeList.Add(new TvReport.Coordinate { value = ": " + TotalDiscount.ToString("#,###.00"), x = 160f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Bendra užsakymo kaina", x = 61.5f, y = _y, FontSize = 8, bold = true });
        CurrencyRecord currency = new Banks().getCurrency(UserData.Market, ResData.ResMain.SaleCur, ref errorMsg);
        string numberToword = new LibVB.VBUtils.NumberToWordLT().SumaZod(ResData.ResMain.PasPayable.Value, currency.Name, currency.FracName);
        string totalPriceStr = ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " ( " + numberToword + " ) ";
        writeList.Add(new TvReport.Coordinate { value = ": " + totalPriceStr, x = 160f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { FieldName = "field40", value = "On", Caption = "", x = 61.5f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { value = "Esu informuota(-s) apie galimybę sudaryti medicininių išlaidų, neįvykusios kelionės rizikos, nelaimingų", x = 71.5f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "atsitikimų draudimo sutartis ir su draudimo taisyklėmis susipažinau.", x = 71.5f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "(Turisto parašas)", x = 510.3f, y = _y, FontSize = 7, Italic = true, Align = TvReport.fontAlign.Right });

        return new TvReport.NovReport.NovReport().createContract(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showContract_LV(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        bool imgHolpack = false;
        byte[] contractLogo = new TvReport.NovReport.NovReport().getContractLogo(ResData.ResMain.PLOperator, ResData.ResMain.HolPack, ref imgHolpack, ref errorMsg);

        writeList.Add(new TvReport.Coordinate { Pic = contractLogo, x = 28.35f, y = 759.78f, H = 55, Type = TvReport.writeType.Image });

        writeList.Add(new TvReport.Coordinate { value = "TŪRISMA PAKALPOJUMU SNIEGŠANAS LĪGUMS No " + ResData.ResMain.ResNo, x = 158.76f, y = 782.46f, FontSize = 13 });
        writeList.Add(new TvReport.Coordinate { value = "Datums:", x = 28.35f, y = 747.0225f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "", x = 64.04f, y = 747.0225f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Rezervācijas Nr. " + ResData.ResMain.ResNo, x = 319.52f, y = 747.0225f, FontSize = 8, bold = true });
        float _y = 746f;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 28.35f, y = _y, H = 0, W = 289.17f, LineWidth = 0.5f });

        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);

        _y -= 4f;
        string areText = string.Format("Tūrisma pakalpojumu sniegšanas līgums (turpmāk tekstā – Līgums) noslēgts starp tūrisma aģentūra {0} vienotais reg Nr. (reģ.nr. tūrisma pakalpojumu sniedzēju datu bāzē {1}) , juridiskā adrese {2} , kuras vārdā šo līgumu paraksta aģentūras darbinieks no vienas puses.",
                                         agency.RegisterCode,
                                         agency.LicenseNo,
                                         agencyAddr.DAddress + " " + agencyAddr.DAddrZip + " " + agencyAddr.DAddrCity);
        writeList.Add(new TvReport.Coordinate { value = areText, x = 30f, y = _y, W = 558f, H = 28f, FontSize = 7, Type = TvReport.writeType.AreaText });
        _y -= 28f;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 28.35f, y = _y, H = 0f, W = 558.495f, LineWidth = 0.5f });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Aģentūras darbinieks:", x = 28.35f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = UserData.UserName, x = 113.225f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Fakss:", x = 28.35f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Fax1, x = 56.7f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Tālrunis:", x = 170.1f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Phone1, x = 208.12f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "E-pasts:", x = 310.1f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.EMail1, x = 345.12f, y = _y, FontSize = 8 });
        _y -= 12f;
        ///////////////
        writeList.Add(new TvReport.Coordinate { value = "INFORMACIJA PAR TŪRISTU (-IEM):", x = 30f, y = _y, FontSize = 7, bold = true });
        _y -= 2f;
        float tblY = _y;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 30, y = _y, H = 0, W = 524.475f, LineWidth = 0.1f });
        _y -= 10;
        writeList.Add(new TvReport.Coordinate { value = "vārds, uzvārds", x = 31f, y = _y + 3, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { value = "personas kods", x = 206f, y = _y + 3, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 204.3525f, y = tblY, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "pases Nr.", x = 319f, y = _y + 3, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 317.7525f, y = tblY, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "derīga līdz", x = 445f, y = _y + 3, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 433.9875f, y = tblY, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 30, y = tblY - 10f, H = 0, W = 524.475f, LineWidth = 0.1f });
        //_y -= -10;
        foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0))
        {
            _y -= 10;
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 30, y = _y, H = 0, W = 524.475f, LineWidth = 0.25f });
            writeList.Add(new TvReport.Coordinate { value = row.Name + " " + row.Surname, x = 31f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.IDNo, x = 206f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.PassSerie + (!string.IsNullOrEmpty(row.PassSerie) && !string.IsNullOrEmpty(row.PassNo) ? "," : "") + row.PassNo, x = 319f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.PassExpDate.HasValue ? row.PassExpDate.Value.ToShortDateString() : "", x = 445f, y = _y + 3, FontSize = 6 });
        }
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 30f, y = tblY, H = _y - tblY, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 554.475f, y = tblY, H = _y - tblY, W = 0, LineWidth = 0.1f });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "Papildus informācija par klientu (-iem):", x = 31f, y = _y, FontSize = 6, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "(adrese, tālrunis, e-pasts) :", x = 148.605f, y = _y, FontSize = 6, Italic = true });
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
        _y -= 8;
        if (_leader != null)
            writeList.Add(new TvReport.Coordinate
            {
                value = !Equals(_leader.ContactAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip + " " + _leader.AddrHomeTel + " " + _leader.AddrHomeFax + " " + _leader.AddrHomeEmail : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip + " " + _leader.AddrWorkTel + " " + _leader.AddrWorkFax + " " + _leader.AddrWorkEMail,
                x = 31f,
                y = _y,
                FontSize = 6
            });

        _y -= 5;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 30, y = _y, H = 0, W = 554.475f, LineWidth = 0.5f });
        //////////
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Ceļojuma galamērķis:", x = 30f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.HolPackNameL, x = 116.85f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Izbraukšanas datums:", x = 252.45f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "", x = 341.9175f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Atgriešanās datums:", x = 466.2f, y = _y, FontSize = 8, Align = TvReport.fontAlign.Right, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "", x = 470.2f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2))
        {
            _y -= 3f;
            HotelRecord hotel = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Viesnīca / Ēdināšana :", x = 30f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = hotel.LocalName + ", (" + row.DepLocationNameL + ")", x = 117.2925f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Nakšu skaits :", x = 439f, y = _y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = row.Duration.ToString(), x = 445f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Numuru skaits :", x = 530f, y = _y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = "1", x = 533f, y = _y, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = "Ēdināšana :", x = 30f, y = _y - 10.6875f, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = row.BoardNameL, x = 80.3475f, y = _y - 10.6875f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Viesnīcas kategorija (attiecīgajā valstī) :", x = 530f, y = _y - 10.6875f, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = hotel != null ? hotel.OfCategory : "", x = 533f, y = _y - 10.6875f, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = "Numuru tips :", x = 30f, y = _y - (2 * 10.6875f), FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = row.AccomNameL + " / " + row.RoomNameL, x = 85.9575f, y = _y - (2 * 10.6875f), FontSize = 8 });
            _y = _y - (3 * 10.6875f);
        }
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 30f, y = _y, H = 0, W = 554.475f, LineWidth = 0.5f });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Cenā iekļauti papildus pakalpojumi :", x = 30f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field01", value = "On", Caption = "transfērs lidosta-viesnīca ", x = 198.45f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field02", value = "On", Caption = "transfērs lidosta-viesnīca ", x = 354.75f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Klienta īpašās vēlmes :", x = 30f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field03", value = "", x = 120.0f, y = _y - 3.0f, H = 12.0f, W = 411.0f, LineWidth = 0.2f, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.Text });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "(tiek izpildītas iespēju robežās)", x = 30f, y = _y, FontSize = 5, Italic = true });
        writeList.Add(new TvReport.Coordinate { value = "(numuri blakus, precīza numura atrašanās vieta utt.)", x = 240f, y = _y, FontSize = 5, Italic = true });

        _y -= 11.4f;
        List<ResServiceRecord> flights = ResData.ResService.Where(w => w.ServiceType == "FLIGHT").ToList<ResServiceRecord>();
        if (flights.Count > 0)
        {
            FlightDayRecord depFlight = new Flights().getFlightDay(flights.FirstOrDefault().Service, flights.FirstOrDefault().BegDate.Value, ref errorMsg);
            FlightDayRecord retFlight = new Flights().getFlightDay(flights.LastOrDefault().Service, flights.LastOrDefault().BegDate.Value, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Izlidošanas laiks :", x = 30f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = (depFlight.DepTime.HasValue ? depFlight.DepTime.Value.ToShortTimeString() : "") + " OBLIGÃTI precizēt izlidošanas laiku vienu dienu pirms izlidošanas!", x = 104f, y = _y, FontSize = 8 });
            AirlineRecord depAirline = new Flights().getAirline(UserData.Market, depFlight.Airline, ref errorMsg);
            AirlineRecord retAirline = new Flights().getAirline(UserData.Market, retFlight.Airline, ref errorMsg);
            _y -= 11f;
            AirportRecord depAirport = new Flights().getAirport(UserData.Market, depFlight.DepAirport, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Izlidošanas vieta :", x = 30f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = depAirport != null ? depAirport.LocalName : "", x = 104f, y = _y, FontSize = 8 });
            _y -= 11f;
            AirportRecord retAirport = new Flights().getAirport(UserData.Market, retFlight.ArrAirport, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Ielidošanas vieta :", x = 30f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = retAirport != null ? retAirport.LocalName : "", x = 104f, y = _y, FontSize = 8 });
        }

        _y -= 4;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 30f, y = _y, H = 0, W = 554.475f, LineWidth = 0.5f });

        #region Customer Price Table
        List<TvReport.tableRow> prices = new List<TvReport.tableRow>();
        List<TvReport.tableCell> priceRow = new List<TvReport.tableCell>();
        for (int i = 0; i <= ResData.ResCust.Count; i++)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = i == 0 ? "" : i.ToString();
            cell.Width = i == 0 ? 143f : 45f;
            cell.Align = TvReport.fontAlign.Center;
            priceRow.Add(cell);
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });

        //Kelialapio kaina vienam asmeniui
        var queryService = from q1 in ResData.ResService
                           join q2 in ResData.ResCustPrice on q1.RecID equals q2.ServiceID
                           where Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID == 0)
                           select new { custNo = q2.CustNo, price = q2.SalePrice };
        var queryServiceExt = from q1 in ResData.ResServiceExt
                              join q2 in ResData.ResCustPrice on q1.RecID equals q2.ExtServiceID
                              where Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID != 0)
                              select new { custNo = q2.CustNo, price = q2.SalePrice };
        var queryUnion = queryService.Union(queryServiceExt);
        var queryIncPac = from q in queryUnion
                          group q by new { custNo = q.custNo } into k
                          select new { k.Key.custNo, price = (queryUnion.Where(w => w.custNo == k.Key.custNo).Sum(s => s.price)) };

        if (queryIncPac.Count() > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "Ceļojuma cena vienam cilvēkam LVL";
            cell.Width = 143f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (var row in queryIncPac.OrderBy(o => o.custNo))
            {
                cell = new TvReport.tableCell();
                cell.Text = row.price.HasValue ? row.price.Value.ToString("#,###.00") : "";
                cell.Width = 45f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
            prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        }
        var queryServiceOutPack = from q1 in ResData.ResService
                                  join q2 in ResData.ResCustPrice on q1.RecID equals q2.ServiceID
                                  where !Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID == 0)
                                  select new { id = q2.ExtServiceID, desc = q1.ServiceNameL, custNo = q2.CustNo, price = q2.SalePrice };
        var queryServiceExtOutPack = from q1 in ResData.ResServiceExt
                                     join q2 in ResData.ResCustPrice on q1.RecID equals q2.ExtServiceID
                                     where !Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID != 0)
                                     select new { id = q2.ExtServiceID, desc = q1.ExtServiceNameL, custNo = q2.CustNo, price = q2.SalePrice };
        var queryUnionOutPack = queryServiceOutPack.Union(queryServiceExtOutPack);
        var queryOutPack = from q in queryUnionOutPack
                           select q;

        if (queryOutPack.Count() > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            var query1 = from q in queryOutPack
                         group q by new { id = q.id, desc = q.desc } into k
                         select new { id = k.Key.id, desc = k.Key.desc };
            foreach (var r1 in query1)
            {
                priceRow = new List<TvReport.tableCell>();
                TvReport.tableCell cell = new TvReport.tableCell();
                cell.Text = r1.desc;
                cell.Width = 143f;
                cell.Align = TvReport.fontAlign.Left;
                priceRow.Add(cell);
                foreach (var row in queryOutPack.Where(w => w.id == r1.id).OrderBy(o => o.custNo))
                {
                    cell = new TvReport.tableCell();
                    cell.Text = row.price.HasValue ? row.price.Value.ToString("#,###.00") : "";
                    cell.Width = 45f;
                    cell.Align = TvReport.fontAlign.Right;
                    priceRow.Add(cell);
                }
                prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
            }
        }
        priceRow = new List<TvReport.tableCell>();
        decimal priceTotal = 0;
        if (ResData.ResCust.Count > 0)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "Kopējā cena vienam cilvēkam LVL";
            cell.Width = 143f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (ResCustRecord r1 in ResData.ResCust)
            {
                decimal? totalPrice = null;
                int? custNo = r1.CustNo;
                totalPrice = (queryIncPac.Where(w => w.custNo == custNo).Sum(s => s.price).HasValue ? queryIncPac.Where(w => w.custNo == custNo).Sum(s => s.price).Value : Convert.ToDecimal(0)) +
                             (queryOutPack.Where(w => w.custNo == custNo).Sum(s => s.price).HasValue ? queryOutPack.Where(w => w.custNo == custNo).Sum(s => s.price).Value : Convert.ToDecimal(0));
                cell = new TvReport.tableCell();
                cell.Text = totalPrice.HasValue ? totalPrice.Value.ToString("#,###.00") : "";
                priceTotal += totalPrice.HasValue ? totalPrice.Value : 0;
                cell.Width = 45f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        #endregion

        string pdfTabelStr = Newtonsoft.Json.JsonConvert.SerializeObject(prices);
        _y -= 4f;
        writeList.Add(new TvReport.Coordinate { value = pdfTabelStr, Type = TvReport.writeType.PdfTable, x = 31, y = _y, FontSize = 7 });
        _y -= ((11.0f * prices.Count()));
        prices = new List<TvReport.tableRow>();
        priceRow = new List<TvReport.tableCell>();
        for (int i = 0; i < 2; i++)
        {
            decimal? totalPrice = null;
            totalPrice = (queryIncPac.Sum(s => s.price).HasValue ? queryIncPac.Sum(s => s.price).Value : Convert.ToDecimal(0)) +
                         (queryOutPack.Sum(s => s.price).HasValue ? queryOutPack.Sum(s => s.price).Value : Convert.ToDecimal(0));
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = i == 0 ? "Kopējā pasūtījuma cena LVL" : (totalPrice.HasValue ? totalPrice.Value.ToString("#,###.00") : "");
            cell.Width = i == 0 ? 143f : (ResData.ResCust.Count * 45f);
            cell.Align = i == 0 ? TvReport.fontAlign.Left : TvReport.fontAlign.Center;
            priceRow.Add(cell);
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        priceRow = new List<TvReport.tableCell>();
        for (int i = 0; i < 2; i++)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = i == 0 ? "Samaksāta priekšapmaksa LVL" : "";
            cell.Width = i == 0 ? 143f : (ResData.ResCust.Count * 45f);
            cell.Align = i == 0 ? TvReport.fontAlign.Left : TvReport.fontAlign.Center;
            priceRow.Add(cell);
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        priceRow = new List<TvReport.tableCell>();
        for (int i = 0; i < 2; i++)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = i == 0 ? "Apmaksāts LVL" : "";
            cell.Width = i == 0 ? 143f : (ResData.ResCust.Count * 45f);
            cell.Align = i == 0 ? TvReport.fontAlign.Left : TvReport.fontAlign.Center;
            priceRow.Add(cell);
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        pdfTabelStr = Newtonsoft.Json.JsonConvert.SerializeObject(prices);
        _y -= 4f;
        writeList.Add(new TvReport.Coordinate { value = pdfTabelStr, Type = TvReport.writeType.PdfTable, x = 31, y = _y, FontSize = 7 });
        _y -= (11.0f * (prices.Count() + 2));
        writeList.Add(new TvReport.Coordinate { value = "Atlaide LVL", x = 30f, y = _y, FontSize = 8, bold = true });
        decimal PasSupDis = ResData.ResMain.PasSupDis.HasValue ? ResData.ResMain.PasSupDis.Value : 0;
        decimal EBPas = ResData.ResMain.EBPas.HasValue ? ResData.ResMain.EBPas.Value : 0;
        decimal AgencyDisPasVal = ResData.ResMain.AgencyDisPasVal.HasValue ? ResData.ResMain.AgencyDisPasVal.Value : 0;
        decimal bonus = new TvReport.NovReport.NovReport().getBonus(ResData.ResMain.ResNo, ref errorMsg);
        decimal TotalDiscount = PasSupDis - EBPas - AgencyDisPasVal - bonus;
        writeList.Add(new TvReport.Coordinate { value = ": " + (TotalDiscount > 0 ? TotalDiscount.ToString("#,###.00") : ""), x = 166.08f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Kopējā pasūtījuma cena LVL", x = 30f, y = _y, FontSize = 8, bold = true });
        CurrencyRecord currency = new Banks().getCurrency(UserData.Market, ResData.ResMain.SaleCur, ref errorMsg);
        string numberToword = new LibVB.VBUtils.NumberToWordLV().SumaZodLV(ResData.ResMain.PasPayable.Value, currency.Name, currency.FracName);
        string totalPriceStr = ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " ( " + numberToword + " ) ";
        writeList.Add(new TvReport.Coordinate { value = ": " + totalPriceStr, x = 166.08f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Visu pakalpojumu kopējā cena ar atlaidi, LVL", x = 30f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ":", x = 205f, y = _y, FontSize = 8 });
        _y -= 20f;
        writeList.Add(new TvReport.Coordinate { value = "AGENTŪRA:", x = 30f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 30f, y = _y - 2f, H = 0, W = 255f, LineWidth = 0.5f });
        writeList.Add(new TvReport.Coordinate { value = "TŪRISTS", x = 332f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 332f, y = _y - 2f, H = 0, W = 255f, LineWidth = 0.5f });
        _y -= 13.4f;
        writeList.Add(new TvReport.Coordinate { value = "vārds,uzvārds paraksts Z.V.", x = 30f, y = _y, FontSize = 6 });
        writeList.Add(new TvReport.Coordinate { value = "vārds,uzvārds paraksts", x = 332f, y = _y, FontSize = 6 });

        return new TvReport.NovReport.NovReport().createContract(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showContract_EE(User UserData, ResDataRecord ResData)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        bool imgHolpack = false;
        byte[] contractLogo = new TvReport.NovReport.NovReport().getContractLogo(ResData.ResMain.PLOperator, ResData.ResMain.HolPack, ref imgHolpack, ref errorMsg);

        writeList.Add(new TvReport.Coordinate { Pic = contractLogo, x = 61.275f, y = 746.7f, H = 62, Type = TvReport.writeType.Image });

        writeList.Add(new TvReport.Coordinate { value = "REISITEENUSE LEPING KLIENDIGA", x = 230, y = 761, FontSize = 14, bold = true });
        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);

        string oprAddress = opr.FirmName + " " + opr.Address + " " + opr.AddrZip + " " + opr.AddrCity + " " + opr.Phone1 + " " + opr.Email1;
        writeList.Add(new TvReport.Coordinate { value = oprAddress, x = 60, y = 727.25f, FontSize = 5 });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 726.75f, H = 0, W = 250, LineWidth = Convert.ToSingle(25 / 100.0) });
        writeList.Add(new TvReport.Coordinate { value = "Reisileping Nr. " + ResData.ResMain.ResNo, x = 312, y = 727, FontSize = 11, bold = true });

        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 717, H = 0, W = 456, LineWidth = 0.5f });

        writeList.Add(new TvReport.Coordinate { value = "Lepingu sõlmimise kuupäev:", x = 60, y = 708.5F, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "", x = 177.31f, y = 708.5F, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "Reisibüroo:", x = 60, y = 697.1f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agent.FirmName, x = 111.34f, y = 697.1f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Reisibüroo töötaja:", x = 288, y = 697.1f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.AgencyUserNameL, x = 361.44f, y = 697.1f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "Faks:", x = 60, y = 685.7f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DFax, x = 82.845f, y = 685.7f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Telefon:", x = 185.4f, y = 685.7f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Phone1, x = 216.81f, y = 685.7f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "E-Mail:", x = 320.9f, y = 685.7f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.EMail1, x = 349.46f, y = 685.7f, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = "Reisija ja kaasreisijate andmed:", x = 61.5f, y = 667.175f, FontSize = 7, bold = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 666, H = 0, W = 456, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "ees-, perekonnanimi", x = 63.4f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { value = "Isikukood", x = 215.75f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 213.75f, y = 666, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "Passi number", x = 315.5f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 313.5f, y = 666, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { value = "Kehtiv kuni", x = 412.4f, y = 659, FontSize = 6, Italic = true });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 410.4f, y = 666, H = -10, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 656, H = 0, W = 456, LineWidth = 0.1f });
        float _y = 656.0f;
        foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0))
        {
            _y -= 10;
            writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.25f });
            writeList.Add(new TvReport.Coordinate { value = row.Name + " " + row.Surname, x = 63.4f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.IDNo, x = 215.75f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.PassSerie + ", " + row.PassNo, x = 315.5f, y = _y + 3, FontSize = 6 });
            writeList.Add(new TvReport.Coordinate { value = row.PassExpDate.HasValue ? row.PassExpDate.Value.ToShortDateString() : "", x = 412.4f, y = _y + 3, FontSize = 6 });
        }
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = 666, H = _y - 666, W = 0, LineWidth = 0.1f });
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 516, y = 666, H = _y - 666, W = 0, LineWidth = 0.1f });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "Lisaandmed REISIJA ja kaasreisijate kohta", x = 61.5f, y = _y, FontSize = 6, bold = true });
        writeList.Add(new TvReport.Coordinate { value = "(kodune aadress,telefon, faks või e-mail) :", x = 184.645f, y = _y, FontSize = 6, Italic = true });
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
        _y -= 8;
        if (_leader != null)
            writeList.Add(new TvReport.Coordinate
            {
                value = !Equals(_leader.ContactAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip + " " + _leader.AddrHomeTel + " " + _leader.AddrHomeFax + " " + _leader.AddrHomeEmail : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip + " " + _leader.AddrWorkTel + " " + _leader.AddrWorkFax + " " + _leader.AddrWorkEMail,
                x = 61.5f,
                y = _y,
                FontSize = 6
            });

        _y -= 5;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.5f });

        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Lennureisi algpunkt:", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.DepCityNameL, x = 150f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Väljumise kuupäev:", x = 466.2f, y = _y, FontSize = 8, Align = TvReport.fontAlign.Right, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "", x = 469.2f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Lennureisi sihtpunkt:", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityNameL, x = 150f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = "Tagasituleku kuupäev:", x = 466.2f, y = _y, FontSize = 8, Align = TvReport.fontAlign.Right, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "", x = 469.2f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Tagasituleku kuupäev:", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.HolPackNameL, x = 150f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2))
        {
            _y -= 3f;
            HotelRecord hotel = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Hotelli nimetus :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = hotel.LocalName + ", (" + row.DepLocationNameL + ")", x = 127.215f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Ööde arv :", x = 433.76f, y = _y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = row.Duration.ToString(), x = 435.76f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Tubade arv :", x = 498.96f, y = _y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = "1", x = 501.96f, y = _y, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = "Toitlustamine :", x = 61.5f, y = _y - 10.6875f, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = row.BoardNameL, x = 120.04f, y = _y - 10.6875f, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Hotelli kategooria (ametlik) :", x = 433.76f, y = _y - 10.6875f, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
            writeList.Add(new TvReport.Coordinate { value = hotel != null ? hotel.OfCategory : "", x = 435.76f, y = _y - 10.6875f, FontSize = 8 });

            writeList.Add(new TvReport.Coordinate { value = "Toa tüüp :", x = 61.5f, y = _y - (2 * 10.6875f), FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = row.AccomNameL + " / " + row.RoomNameL, x = 101.55f, y = _y - (2 * 10.6875f), FontSize = 8 });
            _y = _y - (3 * 12.6875f);
        }
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.5f });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Reisitasu sisse kuuluvad :", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { FieldName = "field2", value = "Off", Caption = "viisade vormistamine ", x = 163.725f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field3", value = "Off", Caption = "; reisikorraldaja esindaja teenused ", x = 254.445f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field4", value = "Off", Caption = "; transfeer lennujaam-hotell ", x = 390.525f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { FieldName = "field5", value = "Off", Caption = "; transfeer hotell-lennujaam ", x = 61.5f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field6", value = "Off", Caption = "; ekskursioonid ", x = 177.74f, y = _y, FontSize = 8, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Reisitasu suurendamise võimalus puudub.", x = 61.5f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Reisija erisoovid : ", x = 61.5f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { FieldName = "field12", value = "", x = 126.705f, y = _y - 3.0f, H = 12.0f, W = 383.0f, LineWidth = 0.2f, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.Text });
        _y -= 8;
        writeList.Add(new TvReport.Coordinate { value = "(täidetakse võimaluse korral)", x = 61.5f, y = _y, FontSize = 5, Italic = true });
        writeList.Add(new TvReport.Coordinate { value = "(kõrvuti toad, täpne toa asukoht jne)", x = 227.5f, y = _y, FontSize = 5, Italic = true });
        List<ResServiceRecord> flights = ResData.ResService.Where(w => w.ServiceType == "FLIGHT").ToList<ResServiceRecord>();
        _y -= 11.4f;
        if (flights.Count > 0)
        {
            FlightDayRecord depFlight = new Flights().getFlightDay(flights.FirstOrDefault().Service, flights.FirstOrDefault().BegDate.Value, ref errorMsg);
            FlightDayRecord retFlight = new Flights().getFlightDay(flights.LastOrDefault().Service, flights.LastOrDefault().BegDate.Value, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = "Esialgne väljumise aeg :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            writeList.Add(new TvReport.Coordinate { value = depFlight.DepTime.HasValue ? depFlight.DepTime.Value.ToShortTimeString() : "", x = 166f, y = _y, FontSize = 8 });
            writeList.Add(new TvReport.Coordinate { value = "Palume kontrollida väljumise aega oma reisibüroost päev enne reisi!", x = 230.0f, y = _y, FontSize = 7 });
            _y -= 11.4f;
            writeList.Add(new TvReport.Coordinate { value = "Esialgne väljumise koht :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            AirportRecord depAirport = new Flights().getAirport(UserData.Market, depFlight.DepAirport, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = depAirport != null ? depAirport.LocalName : "", x = 166f, y = _y, FontSize = 8 });
            _y -= 11.4f;
            writeList.Add(new TvReport.Coordinate { value = "Esialgne tagasituleku koht :", x = 61.5f, y = _y, FontSize = 8, bold = true });
            AirportRecord retAirport = new Flights().getAirport(UserData.Market, retFlight.ArrAirport, ref errorMsg);
            writeList.Add(new TvReport.Coordinate { value = retAirport != null ? retAirport.LocalName : "", x = 166f, y = _y, FontSize = 8 });
        }
        _y -= 4;
        writeList.Add(new TvReport.Coordinate { Type = TvReport.writeType.Line, x = 60, y = _y, H = 0, W = 456, LineWidth = 0.5f });
        #region Customer Price Table
        List<TvReport.tableRow> prices = new List<TvReport.tableRow>();
        List<TvReport.tableCell> priceRow = new List<TvReport.tableCell>();
        for (int i = 0; i <= ResData.ResCust.Count; i++)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = i == 0 ? "" : i.ToString();
            cell.Width = i == 0 ? 143f : 45f;
            cell.Align = TvReport.fontAlign.Center;
            priceRow.Add(cell);
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });

        //Kelialapio kaina vienam asmeniui
        var queryService = from q1 in ResData.ResService
                           join q2 in ResData.ResCustPrice on q1.RecID equals q2.ServiceID
                           where Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID == 0)
                           select new { custNo = q2.CustNo, price = q2.SalePrice };
        var queryServiceExt = from q1 in ResData.ResServiceExt
                              join q2 in ResData.ResCustPrice on q1.RecID equals q2.ExtServiceID
                              where Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID != 0)
                              select new { custNo = q2.CustNo, price = q2.SalePrice };
        var queryUnion = queryService.Union(queryServiceExt);
        var queryIncPac = from q in queryUnion
                          group q by new { custNo = q.custNo } into k
                          select new { k.Key.custNo, price = (queryUnion.Where(w => w.custNo == k.Key.custNo).Sum(s => s.price)) };

        if (queryIncPac.Count() > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "Reisi hind inimese kohta";
            cell.Width = 143f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (var row in queryIncPac.OrderBy(o => o.custNo))
            {
                cell = new TvReport.tableCell();
                cell.Text = row.price.HasValue ? row.price.Value.ToString("#,###.00") : "";
                cell.Width = 45f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
            prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        }
        var queryServiceOutPack = from q1 in ResData.ResService
                                  join q2 in ResData.ResCustPrice on q1.RecID equals q2.ServiceID
                                  where !Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID == 0)
                                  select new { id = q2.ExtServiceID, desc = q1.ServiceNameL, custNo = q2.CustNo, price = q2.SalePrice };
        var queryServiceExtOutPack = from q1 in ResData.ResServiceExt
                                     join q2 in ResData.ResCustPrice on q1.RecID equals q2.ExtServiceID
                                     where !Equals(q1.IncPack, "Y") && (q2.ExtServiceID.HasValue && q2.ExtServiceID != 0)
                                     select new { id = q2.ExtServiceID, desc = q1.ExtServiceNameL, custNo = q2.CustNo, price = q2.SalePrice };
        var queryUnionOutPack = queryServiceOutPack.Union(queryServiceExtOutPack);
        var queryOutPack = from q in queryUnionOutPack
                           select q;

        if (queryOutPack.Count() > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            var query1 = from q in queryOutPack
                         group q by new { id = q.id, desc = q.desc } into k
                         select new { id = k.Key.id, desc = k.Key.desc };
            foreach (var r1 in query1)
            {
                priceRow = new List<TvReport.tableCell>();
                TvReport.tableCell cell = new TvReport.tableCell();
                cell.Text = r1.desc;
                cell.Width = 143f;
                cell.Align = TvReport.fontAlign.Left;
                priceRow.Add(cell);
                foreach (var row in queryOutPack.Where(w => w.id == r1.id).OrderBy(o => o.custNo))
                {
                    cell = new TvReport.tableCell();
                    cell.Text = row.price.HasValue ? row.price.Value.ToString("#,###.00") : "";
                    cell.Width = 45f;
                    cell.Align = TvReport.fontAlign.Right;
                    priceRow.Add(cell);
                }
                prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
            }
        }
        priceRow = new List<TvReport.tableCell>();
        decimal priceTotal = 0;
        if (ResData.ResCust.Count > 0)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "Hind kokku inimese kohta";
            cell.Width = 143f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (ResCustRecord r1 in ResData.ResCust)
            {
                decimal? totalPrice = null;
                int? custNo = r1.CustNo;
                totalPrice = (queryIncPac.Where(w => w.custNo == custNo).Sum(s => s.price).HasValue ? queryIncPac.Where(w => w.custNo == custNo).Sum(s => s.price).Value : Convert.ToDecimal(0)) +
                             (queryOutPack.Where(w => w.custNo == custNo).Sum(s => s.price).HasValue ? queryOutPack.Where(w => w.custNo == custNo).Sum(s => s.price).Value : Convert.ToDecimal(0));
                cell = new TvReport.tableCell();
                cell.Text = totalPrice.HasValue ? totalPrice.Value.ToString("#,###.00") : "";
                priceTotal += totalPrice.HasValue ? totalPrice.Value : 0;
                cell.Width = 45f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        #endregion

        string pdfTabelStr = Newtonsoft.Json.JsonConvert.SerializeObject(prices);
        _y -= 4f;
        writeList.Add(new TvReport.Coordinate { value = pdfTabelStr, Type = TvReport.writeType.PdfTable, x = 62, y = _y, FontSize = 7 });
        _y -= (11.0f * (prices.Count() + 1));
        writeList.Add(new TvReport.Coordinate { value = "Reisitasu sisse kuuluvad kõik kohustuslikud väljaminekud", x = 61.5f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Tellimuse hind kokku", x = 61.5f, y = _y, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = priceTotal > 0 ? priceTotal.ToString("#,###.00") : "", x = 259.95f, y = _y, FontSize = 8, bold = true, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { value = "Makseviis", x = 379.02f, y = _y, FontSize = 8, bold = true });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Makstud", x = 61.5f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { FieldName = "field21", value = "Off", Caption = "Sularahas ", x = 333.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field22", value = "Off", Caption = "Ülekandega ", x = 391.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field23", value = "Off", Caption = "Kaardimakse ", x = 470.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        _y -= 11f;
        writeList.Add(new TvReport.Coordinate { value = "Makstud", x = 61.5f, y = _y, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { FieldName = "field31", value = "Off", Caption = "Sularahas", x = 333.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field32", value = "Off", Caption = "Ülekandega", x = 391.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        writeList.Add(new TvReport.Coordinate { FieldName = "field33", value = "Off", Caption = "Kaardimakse", x = 470.19f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Left });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Hind kokku", x = 61.5f, y = _y, FontSize = 8, bold = true });
        decimal PasSupDis = ResData.ResMain.PasSupDis.HasValue ? ResData.ResMain.PasSupDis.Value : 0;
        decimal EBPas = ResData.ResMain.EBPas.HasValue ? ResData.ResMain.EBPas.Value : 0;
        decimal AgencyDisPasVal = ResData.ResMain.AgencyDisPasVal.HasValue ? ResData.ResMain.AgencyDisPasVal.Value : 0;
        decimal bonus = new TvReport.NovReport.NovReport().getBonus(ResData.ResMain.ResNo, ref errorMsg);
        decimal TotalDiscount = PasSupDis - EBPas - AgencyDisPasVal - bonus;
        writeList.Add(new TvReport.Coordinate { value = ": " + TotalDiscount.ToString("#,###.00"), x = 160f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Summa sõnadega", x = 61.5f, y = _y, FontSize = 8, bold = true });
        CurrencyRecord currency = new Banks().getCurrency(UserData.Market, ResData.ResMain.SaleCur, ref errorMsg);
        string numberToword = new LibVB.VBUtils.NumberToWordEE().SumaZodEE(ResData.ResMain.PasPayable.Value, currency.Name, currency.FracName);
        string totalPriceStr = ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " ( " + numberToword + " ) ";
        writeList.Add(new TvReport.Coordinate { value = ": " + totalPriceStr, x = 160f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Max.lubatud pagasimäär 15+5kg!", x = 61.5f, y = _y, FontSize = 8, bold = true, Underline = true });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Reisija on tutvunud OÜ Novatours kodulehel oleva olulise informatsiooniga (http://www.novatours.ee/enne-reisi/Oluline-info),", x = 61.5f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "samuti OÜ Novatours reisitingimustega (http://www.novatours.ee/enne-reisi/Reisitingimused)", x = 61.5f, y = _y, FontSize = 8 });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "ning neis sätestatud õiguste ja kohustustega lepingu rikkumise, ülesütlemise ning nõuete esitamisega seoses.", x = 61.5f, y = _y, FontSize = 8 });

        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Reisiteenuse lepinguga tutvunud:", x = 517.94f, y = _y, FontSize = 8, Align = TvReport.fontAlign.Right });
        _y -= 7.5f;
        writeList.Add(new TvReport.Coordinate { value = "(Reisija allkiri)", x = 517.94f, y = _y, FontSize = 5, Italic = true, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;

        writeList.Add(new TvReport.Coordinate { FieldName = "field50", value = "Off", Caption = "Meditsiinikindlustus", x = 61.5f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field51", value = "Off", Caption = "Reisitõrkekindlustus", x = 150.0f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        writeList.Add(new TvReport.Coordinate { FieldName = "field52", value = "Off", Caption = "Pagasikindlustus", x = 246.0f, y = _y, FontSize = 7, Type = TvReport.writeType.Field, FieldType = TvReport.fieldTypes.CheckBox, Align = TvReport.fontAlign.Right });
        _y -= 11.4f;
        writeList.Add(new TvReport.Coordinate { value = "Kindlustusetingimustega tutvunud:", x = 61.5f, y = _y, FontSize = 8 });
        _y -= 7.5f;
        writeList.Add(new TvReport.Coordinate { value = "(Reisija allkiri)", x = 61.5f, y = _y, FontSize = 5, Italic = true });

        return new TvReport.NovReport.NovReport().createContract(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showContract(User UserData, ResDataRecord ResData)
    {
        string retValue = string.Empty;
        switch (UserData.Market)
        {
            case "LTNOV": retValue = showContract_LT(UserData, ResData); break;
            case "LVNOV": retValue = showContract_LV(UserData, ResData); break;
            case "LTAIP": retValue = showContract_AIP(UserData, ResData); break;
            case "EENOV": retValue = showContract_EE(UserData, ResData); break;
        }
        return retValue;
    }
    */

    public string getHandicaps(User UserData, ResDataRecord ResData)
    {        
        string errorMsg = string.Empty;        
        StringBuilder sb = new StringBuilder();

        var hotels = from q in ResData.ResService
                     where q.ServiceType == "HOTEL"
                     group q by new
                     {
                         Hotel = q.Service,
                         ServiceName = q.ServiceName,
                         BegDate = q.BegDate,
                         EndDate = q.EndDate
                     } into k
                     select new { Hotel = k.Key.Hotel, ServiceName = k.Key.ServiceName, BegDate = k.Key.BegDate, EndDate = k.Key.EndDate };
        if (hotels != null && hotels.Count() > 0)
        {            
            string hotelHandicapStr = string.Empty;
            foreach (var row in hotels)
            {
                List<HotelHandicapRecord> hotelHandicapList = new Hotels().getHotelHandicaps(UserData.Market, ResData.ResMain.PLMarket, row.Hotel, row.BegDate, row.EndDate, ref errorMsg);
                if (hotelHandicapList != null && hotelHandicapList.Where(w=>w.PrtVoucher == true).Count() > 0)
                {
                    foreach (HotelHandicapRecord r1 in hotelHandicapList.Where(w => w.PrtVoucher == true))
                        hotelHandicapStr += string.Format("{0} ", r1.Name);
                }
            }
            if (hotelHandicapStr.Length > 0)
            {         
                sb.Append(hotelHandicapStr);                
            }
        }
                
        return sb.ToString();
    }
   
    public string showFlightTicketListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var customersTicket = from H in ResData.ResService
                              join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                              join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                              join T in ResData.Title on C.Title equals T.TitleNo
                              join F in flights on H.Service equals F.Code
                              join A in airLines on F.Airline equals A.Code
                              where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                              group Rc by new
                              {
                                  CustNo = Rc.CustNo,
                                  Name = C.Name,
                                  SurName = C.Surname,
                                  TitleName = T.Code
                              } into g
                              select new
                              {
                                  CustNo = g.Key.CustNo,
                                  Name = g.Key.Name,
                                  SurName = g.Key.SurName,
                                  TitleName = g.Key.TitleName
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            sb.Append(" <tr>");
            i++;

            Int16? DocStat = Conversion.getInt16OrNull(ResData.ResCon.Find(f => f.CustNo == row.CustNo).DocStat);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            sb.Append("     <td>&nbsp;");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";
            string issuedMsg = "{0} </span>";
            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append(string.Format(issuedMsg, DocStat.HasValue && DocStat.Value == 1 ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued").ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued").ToString()));
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append(string.Format("         {0}", HttpContext.GetGlobalResourceObject("LibraryResource", "BookingPaymentNotCompletted")));
                else
                {
                    sb.AppendFormat("<span onclick=\"Ticket0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} ",
                                ResData.ResMain.ResNo,
                                row.CustNo,
                                i.ToString() + ". " + Name);
                    sb.Append(string.Format(issuedMsg, DocStat.HasValue && DocStat.Value == 1 ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued").ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued").ToString()));
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showFlightTicket(User UserData, ResDataRecord ResData, int? CustNo)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        List<FlightRecord> flightList = new Flights().getFlights(UserData.Market, ref errorMsg);
        List<ResServiceRecord> custFlights = (from q1 in ResData.ResService
                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                              select q1).ToList<ResServiceRecord>();
        List<FlightDayRecord> flyList = new Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        var airlines = from q1 in custFlights
                       join q2 in flightList on q1.Service equals q2.Code
                       group q2 by new { airlines = q2.Airline } into k
                       select new { airline = k.Key.airlines };
        if (airlines != null && airlines.Count() > 0)
        {
            TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);

            writeList.Clear();
            List<string> eTicketList = new List<string>();
            Int16 cnt = 0;
            foreach (var row1 in airlines)
            {
                bool imgHolpack = false;
                byte[] contractLogo = new TvReport.NovReport.NovReport().getContractLogo(ResData.ResMain.PLOperator, ResData.ResMain.HolPack, ref imgHolpack, ref errorMsg);
                object airlineLogo = new TvReport.NovReport.NovReport().getAirlineLogo(row1.airline, false, ref errorMsg);
                ++cnt;
                List<ResServiceRecord> custAirleineFlights = (from q1 in ResData.ResService
                                                              join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                              join q3 in flyList on q1.Service equals q3.FlightNo
                                                              where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q3.Airline == row1.airline && q1.BegDate == q3.FlyDate
                                                              select q1).ToList<ResServiceRecord>();

                writeList.Add(new TvReport.Coordinate { Pic = contractLogo, x = 14f, y = 120f, H = 18f, Type = TvReport.writeType.Image, PageNo = 1 });
                writeList.Add(new TvReport.Coordinate { Pic = (byte[])airlineLogo, x = 160f, y = 120f, H = 18f, Type = TvReport.writeType.Image, PageNo = 1 });

                AirlineRecord airLine = new Flights().getAirline(UserData.Market, row1.airline, ref errorMsg);

                string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());
                writeList.Add(new TvReport.Coordinate { value = resCust.Surname + " / " + resCust.Name + " " + resCust.TitleStr, x = 56f, y = 240f - 147f, FontSize = 8, PageNo = 1 });
                writeList.Add(new TvReport.Coordinate { value = eTicketNo, x = 56f, y = 231.025f - 147f, FontSize = 8, PageNo = 1 });
                writeList.Add(new TvReport.Coordinate { value = custFlights.FirstOrDefault().ResNo, x = 154f, y = 231.025f - 147f, FontSize = 8, PageNo = 1 });
                string issuedBy = agencyAddr.Name + " / " + agencyAddr.FirmName + " / " + agencyAddr.Address + " / " + agencyAddr.AddrZip + " / " + agencyAddr.AddrCity;
                writeList.Add(new TvReport.Coordinate { value = issuedBy, x = 56f, y = 222f - 147f, FontSize = 8, PageNo = 1 });
                writeList.Add(new TvReport.Coordinate { value = DateTime.Today.ToShortDateString(), x = 56f, y = 212.975f - 147f, FontSize = 8, PageNo = 1 });

                float _Y = 30f;
                foreach (ResServiceRecord row in custAirleineFlights)
                {
                    FlightDayRecord flightDay = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    string confStat = string.Empty;
                    switch (row.StatConf)
                    {
                        case 0: confStat = resCust.Title < 8 ? "Request" : "NS"; break;
                        case 1: confStat = resCust.Title < 8 ? "OK" : "NS"; break;
                        case 2: confStat = resCust.Title < 8 ? "Not Confirm" : "NS"; break;
                        case 3: confStat = resCust.Title < 8 ? "No Show" : "NS"; break;
                    }

                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepAirport + " / " + flightDay.ArrAirport, x = 10.5f + 2f, y = _Y, FontSize = 8, PageNo = 1 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.Airline, x = 44.25f + 2f, y = _Y, FontSize = 8, PageNo = 1 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.FlightNo + " / " + row.FlgClass, x = 63.5f + 2f, y = _Y, FontSize = 8, PageNo = 1 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.FlyDate.ToShortDateString(), x = 90.75f + 2f, y = _Y, FontSize = 8, PageNo = 1 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.CountCloseTime.HasValue ? flightDay.CountCloseTime.Value.ToShortTimeString() : "", x = 107.25f + 2f, y = _Y, FontSize = 8, PageNo = 1 });
                    writeList.Add(new TvReport.Coordinate { value = flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToShortTimeString() : "", x = 125.25f + 2f, y = _Y, FontSize = 8, PageNo = 1 });
                    writeList.Add(new TvReport.Coordinate { value = "nefiksuojamas", x = 143.25f + 2f, y = _Y + 4.0f, FontSize = 7, PageNo = 1 });
                    writeList.Add(new TvReport.Coordinate { value = "(not stated)", x = 143.25f + 2f, y = _Y, FontSize = 7, PageNo = 1 });
                    writeList.Add(new TvReport.Coordinate { value = confStat, x = 161.25f + 2f, y = _Y, FontSize = 8, PageNo = 1 });
                    string bagWeightStr = resCust.Title.HasValue && resCust.Title.Value > 6 ? (flightDay.BagWeightForInf.HasValue ? flightDay.BagWeightForInf.Value.ToString("#,###.") : flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString() : "") : flightDay.BagWeight.HasValue ? flightDay.BagWeight.Value.ToString() : "";
                    writeList.Add(new TvReport.Coordinate { value = bagWeightStr, x = 182.5f + 2f, y = _Y, FontSize = 8, PageNo = 1 });
                    _Y -= 10.0f;
                }
                string extServiceStr = string.Empty;
                foreach (ResServiceRecord row in custAirleineFlights)
                {
                    List<ResServiceExtRecord> extServis = ResData.ResServiceExt.Where(w => w.ServiceID == row.RecID).ToList<ResServiceExtRecord>();
                    string extServiceStr_A = string.Empty;
                    foreach (ResServiceExtRecord r1 in extServis)
                    {
                        if (extServiceStr_A.Length > 0) extServiceStr_A += " / ";
                        extServiceStr_A += r1.ExtServiceNameL;
                    }
                    if (extServiceStr_A.Length > 0)
                    {
                        if (extServiceStr.Length > 0) extServiceStr += " , ";
                        extServiceStr += "(" + row.Service + ") " + extServiceStr_A;
                    }
                }
                writeList.Add(new TvReport.Coordinate { value = extServiceStr, x = 12.5f, y = _Y, FontSize = 8, PageNo = 1 });
                _Y -= 10.0f;
                FlightsText eTickeTextRecord = new Flights().getFlightTicketText(custAirleineFlights.FirstOrDefault().Service, ref errorMsg);
                string eTicketTextHtmlStr = string.Empty;
                if (!string.IsNullOrEmpty(eTickeTextRecord.HTMLText))
                    eTicketTextHtmlStr = eTickeTextRecord.HTMLText;
                else
                    if (!string.IsNullOrEmpty(eTickeTextRecord.RTFText))
                    {
                        TvTools.RTFtoHTML rtfToHtml = new RTFtoHTML();

                        rtfToHtml.rtf = eTickeTextRecord.RTFText;// Regex.Replace(eTickeTextRecord.RTFText, @"\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?", ""); 
                        eTicketTextHtmlStr = rtfToHtml.html();
                    }
                if (!string.IsNullOrEmpty(eTicketTextHtmlStr))
                    writeList.Add(new TvReport.Coordinate { value = eTicketTextHtmlStr, x = 10f, y = _Y, FontSize = 8, Type = TvReport.writeType.Html, PageNo = 2 });

                eTicketList.Add(new TvReport.NovReport.NovReport().createFlyTicket(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg));
            }

            string retVal = string.Empty;
            if (eTicketList.Count() == 1)
                retVal = eTicketList.FirstOrDefault();
            else retVal = new TvReport.NovReport.NovReport().mergeFlyTicket(siteFolderISS + "ACE\\", eTicketList);

            return retVal;
        }
        else return "";
    }

    public string showVoucherListHTML(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         where q2.Service == row.Service && q2.RecID == row.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                sb.Append(((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued")) + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"Voucher0969801('{0}', {1});\" style=\"cursor: pointer; text-decoration: underline;\">{2} (",
                                 row.ResNo,
                                row.RecID,
                                i.ToString() + ". " + row.ServiceTypeNameL);
                    sb.Append(((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued")) + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public string showHotelVoucher_LT(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        AgencyRecord agency = UserData.AgencyRec;
        string errorMsg = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        OperatorRecord opr = new TvBo.Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, false, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);

        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
        var custs = from q1 in ResData.ResService
                    join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                    join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                    where q1.RecID == ServiceID
                    select q3;
        var extraService = from q in ResData.ResServiceExt
                           where q.ServiceID == ServiceID
                           select q;
        float y1 = 99.2f;
        float y2 = 198.4f;

        bool imgHolpack = false;
        byte[] contractLogo = new TvReport.NovReport.NovReport().getContractLogo(ResData.ResMain.PLOperator, ResData.ResMain.HolPack, ref imgHolpack, ref errorMsg);

        writeList.Add(new TvReport.Coordinate { Pic = contractLogo, x = 10f, y = 273.5f, H = 15.0f, Type = TvReport.writeType.Image });
        writeList.Add(new TvReport.Coordinate { Pic = contractLogo, x = 10f, y = 273.5f - y1, H = 15.0f, Type = TvReport.writeType.Image });
        writeList.Add(new TvReport.Coordinate { Pic = contractLogo, x = 10f, y = 273.5f - y2, H = 15.0f, Type = TvReport.writeType.Image });

        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 26f, y = 267f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 26f, y = 267f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 26f, y = 267f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 86f, y = 267f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 86f, y = 267f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNo, x = 86f, y = 267f - y2, FontSize = 8 });
        string arrCountry = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.HasValue ? ResData.ResMain.ArrCity.Value : 0, ref errorMsg);
        writeList.Add(new TvReport.Coordinate { value = arrCountry, x = 154f, y = 267f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = arrCountry, x = 154f, y = 267f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = arrCountry, x = 154f, y = 267f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityNameL, x = 26f, y = 263.25f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityNameL, x = 26f, y = 263.25f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ArrCityNameL, x = 26f, y = 263.25f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.DepCityNameL, x = 86f, y = 263.25f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.DepCityNameL, x = 86f, y = 263.25f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.DepCityNameL, x = 86f, y = 263.25f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Duration.ToString(), x = 154f, y = 263.25f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Duration.ToString(), x = 154f, y = 263.25f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.Duration.ToString(), x = 154f, y = 263.25f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationNameL, x = 26f, y = 259.5f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationNameL, x = 26f, y = 259.5f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.DepLocationNameL, x = 26f, y = 259.5f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 86f, y = 259.5f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 86f, y = 259.5f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.ServiceName, x = 86f, y = 259.5f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BoardNameL, x = 154f, y = 259.5f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BoardNameL, x = 154f, y = 259.5f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = resService.BoardNameL, x = 154f, y = 259.5f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNote.ToString(), x = 86f, y = 255.75f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNote.ToString(), x = 86f, y = 255.75f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = ResData.ResMain.ResNote.ToString(), x = 86f, y = 255.75f - y2, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = resService.RoomNameL, x = 8f, y = 250f, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = resService.RoomNameL, x = 8f, y = 250f - y1, FontSize = 8, bold = true });
        writeList.Add(new TvReport.Coordinate { value = resService.RoomNameL, x = 8f, y = 250f - y2, FontSize = 8, bold = true });
        float _Y = 250f;
        string passengers = string.Empty;
        bool breakLine = custs.Count() > 1 ? true : false;
        foreach (var row in custs)
        {
            breakLine = !breakLine;
            if (passengers.Length > 0) passengers += " / ";
            passengers += row.TitleStr + ". " + row.Surname + " " + row.Name;
            if (breakLine)
            {
                _Y -= 4.0f;
                writeList.Add(new TvReport.Coordinate { value = passengers, x = 8f, y = _Y, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = passengers, x = 8f, y = _Y - y1, FontSize = 8 });
                writeList.Add(new TvReport.Coordinate { value = passengers, x = 8f, y = _Y - y2, FontSize = 8 });
                passengers = string.Empty;
            }
        }

        _Y = 250f;
        if (extraService != null && extraService.Count() > 0)
        {
            _Y -= 4.0f;
            writeList.Add(new TvReport.Coordinate { value = "Service name", x = 107f, y = _Y, FontSize = 7, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Service name", x = 107f, y = _Y - y1, FontSize = 7, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Service name", x = 107f, y = _Y - y2, FontSize = 7, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Beginning date", x = 165f, y = _Y, FontSize = 7, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Beginning date", x = 165f, y = _Y - y1, FontSize = 7, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Beginning date", x = 165f, y = _Y - y2, FontSize = 7, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Ending date", x = 185f, y = _Y, FontSize = 7, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Ending date", x = 185f, y = _Y - y1, FontSize = 7, bold = true });
            writeList.Add(new TvReport.Coordinate { value = "Ending date", x = 185f, y = _Y - y2, FontSize = 7, bold = true });
            foreach (var row in extraService)
            {
                _Y -= 4.0f;
                writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName, x = 107f, y = _Y, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName, x = 107f, y = _Y - y1, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = row.ExtServiceName, x = 107f, y = _Y - y2, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "", x = 165f, y = _Y, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "", x = 165f, y = _Y - y1, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "", x = 165f, y = _Y - y2, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : "", x = 185f, y = _Y, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : "", x = 185f, y = _Y - y1, FontSize = 7 });
                writeList.Add(new TvReport.Coordinate { value = row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : "", x = 185f, y = _Y - y2, FontSize = 7 });
            }
        }

        SupplierRecord partner = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
        if (partner == null) partner = new SupplierRecord();
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Name, x = 34f, y = 226f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Name, x = 34f, y = 226f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.Name, x = 34f, y = 226f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DAddress.ToString(), x = 8f, y = 222f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DAddrCity.ToString() + " " + agencyAddr.DAddrZip.ToString(), x = 8f, y = 218f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DAddress.ToString(), x = 8f, y = 222f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DAddrCity.ToString() + " " + agencyAddr.DAddrZip.ToString(), x = 8f, y = 218f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DAddress.ToString(), x = 8f, y = 222f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DAddrCity.ToString() + " " + agencyAddr.DAddrZip.ToString(), x = 8f, y = 218f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DPhone.ToString(), x = 17f, y = 214f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DPhone.ToString(), x = 17f, y = 214f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = agencyAddr.DPhone.ToString(), x = 17f, y = 214f - y2, FontSize = 8 });

        writeList.Add(new TvReport.Coordinate { value = partner.Name, x = 118f, y = 226f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Name, x = 118f, y = 226f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Name, x = 118f, y = 226f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Address, x = 110f, y = 222f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.AddrCity + " " + partner.AddrZip, x = 110f, y = 218f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Address, x = 110f, y = 222f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.AddrCity + " " + partner.AddrZip, x = 110f, y = 218f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Address, x = 110f, y = 222f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.AddrCity + " " + partner.AddrZip, x = 110f, y = 218f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Phone1 + " " + partner.Phone2, x = 118f, y = 214f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Phone1 + " " + partner.Phone2, x = 118f, y = 214f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Phone1 + " " + partner.Phone2, x = 118f, y = 214f - y2, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Fax1, x = 118f, y = 210f, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Fax1, x = 118f, y = 210f - y1, FontSize = 8 });
        writeList.Add(new TvReport.Coordinate { value = partner.Fax1, x = 118f, y = 210f - y2, FontSize = 8 });

        return new TvReport.NovReport.NovReport().createVoucher(ResData.ResMain.ResNo, siteFolderISS + docFolder.Replace('/', '\\'), siteFolderISS + "ACE\\", Newtonsoft.Json.JsonConvert.SerializeObject(writeList), ref errorMsg);
    }

    public string showHotelVoucher(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        string retValue = string.Empty;
        switch (UserData.Market)
        {
            case "LTNOV": retValue = showHotelVoucher_LT(UserData, ResData, ServiceID); break;
            case "LVNOV": break;
            case "LTAIP": break;
            case "EENOV": break;
        }
        return retValue;
    }

    public string showProforma_LT(User UserData, ResDataRecord ResData)
    {

        return "";
    }

    public string showProforma(User UserData, ResDataRecord ResData)
    {
        string retValue = string.Empty;
        switch (UserData.Market)
        {
            case "LTNOV": retValue = showProforma_LT(UserData, ResData); break;
            case "LVNOV": break;
            case "LTAIP": break;
            case "EENOV": break;
        }
        return retValue;
    }


}
