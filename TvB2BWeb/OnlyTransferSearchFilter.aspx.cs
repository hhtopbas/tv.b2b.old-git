﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Text;
using System.Web.Services;
using TvTools;
using System.Web.Script.Services;
using System.Collections;
using System.IO;

public partial class OnlyTransferSearchFilter : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
    }

    private static bool WriteFile(List<OnlyTransferFilterData> result)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\OnlyTransferFilter." + HttpContext.Current.Session.SessionID;
        if (File.Exists(path))
            File.Delete(path);
        FileStream f = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
        try
        {
            StreamWriter writer = new StreamWriter(f);
            string compress = TvTools.GZipCompres.Compress(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            writer.Write(compress);
            writer.Close();
            return true;
        }
        catch (Exception)
        {
            return false;
        }
        finally
        {
            f.Close();
        }
    }

    private static List<OnlyTransferFilterData> ReadFile()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string path = AppDomain.CurrentDomain.BaseDirectory + "Cache\\OnlyTransferFilter." + HttpContext.Current.Session.SessionID;
        System.IO.StreamReader reader = new System.IO.StreamReader(path);
        List<OnlyTransferFilterData> list = new List<OnlyTransferFilterData>();
        try
        {
            string uncompressed = TvTools.GZipCompres.Decompress(reader.ReadToEnd());
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OnlyTransferFilterData>>(uncompressed);
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            reader.Close();
        }
        return list;
    }

    public static bool? CreateFilter()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        OnlyTransferFilter filter = new OnlyTransferFilter
        {
            BegDate = DateTime.Today,
            EndDate = DateTime.Today,
            Direction = "F"
        };

        HttpContext.Current.Session["OnlyTransferFilter"] = filter;
        return true;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (!CreateFilter().HasValue)
        {
            HttpContext.Current.Response.StatusCode = 408;
            return null;
        }

        List<OnlyTransferFilterData> onlyTrans = new OnlyTransfers().getOnlyTransferFilters(UserData, ref errorMsg);

        if (!WriteFile(onlyTrans)) return null;

        OnlyTransferFilter criteria = (OnlyTransferFilter)HttpContext.Current.Session["OnlyTransferFilter"];
        bool? currencyConvert = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "ShowMarketCur"));
        List<ReadyCurrencyRecord> getCurrencyList = new TvBo.Common().getReadyCurrency(UserData.Market, UserData.SaleCur, ref errorMsg);

        List<Int32> fltRoomCount = new List<Int32>();
        fltRoomCount.Add(1);

        string curList = string.Empty;
        curList += "<select id=\"currentCur\">";
        foreach (ReadyCurrencyRecord row in getCurrencyList)
        {
            curList += string.Format("<option value=\"{0}\" {2} >{1}</option>",
                                        row.Code,
                                        Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "SWEMAR") ? row.Code : row.NameL,
                                        Equals(UserData.SaleCur, row.Code) ? "selected=\"selected\"" : "");
        }
        curList += "</select>";
        string marketCur = string.Format("<div {3}><input id=\"CurrencyConvert\" type=\"checkbox\" {0} /><label for=\"CurrencyConvert\">{1}</label>&nbsp;{2}</div>",
                currencyConvert.HasValue && currencyConvert.Value ? "checked='checked'" : "",
                HttpContext.GetGlobalResourceObject("LibraryResource", "lblShowInPrice2"),
                curList,
                Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "FINMAR") ? "style=\"display: none;\"" : "");

        var transferTypes = from q in onlyTrans
                            group q by new { q.TrfType, q.TrfTypeNameL, q.TrfTypeName } into k
                            select new { Code = k.Key.TrfType, Name = useLocalName ? k.Key.TrfTypeNameL : k.Key.TrfTypeName };

        bool? showTrfType = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("OnlyTransfer", "ShowTransferType"));

        return new
        {
            ShowTrfType = showTrfType.HasValue ? showTrfType.Value : false,
            TransferTypes = transferTypes,
            CheckIn = DateTime.Today,
            CheckOut = DateTime.Today,
            CustomRegID = UserData.CustomRegID,
            CurrencyConvert = currencyConvert.HasValue ? currencyConvert.Value : true,
            MarketCur = marketCur,
            DateFormat = twoLetterISOLanguageName,
            fltRoomCount = new fltComboDayRecord { maxDayList = fltRoomCount, selectedDay = 0 }
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getCountry(string Direction)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<OnlyTransferFilterData> onlyTrans = ReadFile();
        return (from q in onlyTrans
                where string.Equals(q.Direction, Direction)
                group q by new { q.Country, q.CountryName, q.CountryNameL, q.City, q.CityName, q.CityNameL } into k
                select new { Code = k.Key.City, Name = useLocalName ? k.Key.CityNameL : k.Key.CityName, Country = k.Key.Country, CountryName = useLocalName ? k.Key.CountryNameL : k.Key.CountryName });
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getResort(string Airport, string Direction)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        AirportRecord airport = new OnlyTransfers().getTransferAirport(UserData, Airport, ref errorMsg);
        List<OnlyTransferFilterData> onlyTrans = ReadFile();
        if (!string.Equals(Direction, "B"))
            return (from q in onlyTrans
                    where string.Equals(q.Direction, Direction) &&
                       q.TrfFrom == (airport.TrfLocation.HasValue ? airport.TrfLocation.Value : airport.Location)
                    group q by new { q.TrfTo, q.TrfToName, q.TrfToNameL } into k
                    select new { Code = k.Key.TrfTo, Name = useLocalName ? k.Key.TrfToNameL : k.Key.TrfToName });
        else
            return (from q in onlyTrans
                    where string.Equals(q.Direction, Direction) &&
                       q.TrfTo == (airport.TrfLocation.HasValue ? airport.TrfLocation.Value : airport.Location)
                    group q by new { q.TrfFrom, q.TrfFromName, q.TrfFromNameL } into k
                    select new { Code = k.Key.TrfFrom, Name = useLocalName ? k.Key.TrfFromNameL : k.Key.TrfFromName });

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getAirports(string Direction, int? City)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<OnlyTransferFilterData> onlyTrans = ReadFile();

        List<AirportRecord> _airports = new Flights().getCityAirportLists(UserData, null, City, ref errorMsg);
        var airports = from q in _airports
                       select new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name };
        return airports;
    }

    [WebMethod(EnableSession = true)]
    public static string getRoomInfo(string RoomCount)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        //Int16 maxChildAge = UserData.TvParams.TvParamReser.MaxChdAge.HasValue ? Convert.ToInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAge.Value)) : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? Convert.ToInt16(18) : Convert.ToInt16(12));
        Int16 maxChildAge = UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.HasValue ? Convert.ToInt16(Math.Floor(UserData.TvParams.TvParamReser.MaxChdAgeB2Bsrc.Value)) : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ? Convert.ToInt16(18) : Convert.ToInt16(12));
        StringBuilder html = new StringBuilder();
        plMaxPaxCounts maxPaxData = new plMaxPaxCounts();
        object _paxData = new TvBo.Common().getFormConfigValue("SearchPanel", "PaxData");
        if (_paxData != null)
        {
            plMaxPaxCounts paxData = Newtonsoft.Json.JsonConvert.DeserializeObject<plMaxPaxCounts>(_paxData.ToString());
            maxPaxData = paxData;
        }
        if (HttpContext.Current.Session["SearchPLData"] != null && (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal)))
            if (((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount != null)
                maxPaxData = ((SearchPLData)HttpContext.Current.Session["SearchPLData"]).PlMaxPaxCount;

        Int32 j = 0;
        int i = 1;
        if (string.Equals(UserData.CustomRegID, Common.crID_Rezeda) || string.Equals(UserData.CustomRegID, Common.crID_KidyTour))
            maxPaxData.maxAdult = 12;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.ctID_Falcon))
            maxPaxData.maxAdult = 60;
        if (string.Equals(UserData.CustomRegID, Common.crID_SunFun))
            maxPaxData.maxAdult = 15;

        html.AppendFormat("<div id=\"divRoomInfo{0}\" style=\"width:250px; clear:both;\">", i.ToString());

        html.AppendFormat("  <div id=\"divAdult{0}\" style=\"float: left; width:100px;\">", i.ToString());
        html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblAdult").ToString());
        html.AppendFormat("  <select id=\"fltAdult{0}\" style=\"width: 50px;\">", i.ToString(), i.ToString());
        for (j = 1; j <= maxPaxData.maxAdult; j++)
        {
            if (j == 2)
                html.AppendFormat("     <option value='{0}' selected=\"selected\">{1}</option>", j.ToString(), j.ToString());
            else html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        }
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");

        html.AppendFormat("  <div id=\"divChild{0}\" style=\"float: left; width:50px;\">", i.ToString());
        html.AppendFormat("  <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChild").ToString());
        html.AppendFormat("  <select id=\"fltChild{0}\" onchange=\"onChildChange('{1}')\" style=\"width: 50px;\">", i.ToString(), i.ToString());
        for (j = 0; j < 5; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat("</div>");

        html.AppendFormat("<div id=\"divRoomInfoChd{0}\" style=\"width:250px; clear:both; display:none;\">", i.ToString());
        html.AppendFormat(" <div id=\"divRoomInfoChd1{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 1");
        html.AppendFormat("   <select id=\"fltRoomInfoChd1{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat(" <div id=\"divRoomInfoChd2{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 2");
        html.AppendFormat("   <select id=\"fltRoomInfoChd2{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat(" <div id=\"divRoomInfoChd3{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 3");
        html.AppendFormat("   <select id=\"fltRoomInfoChd3{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");
        html.AppendFormat(" <div id=\"divRoomInfoChd4{0}\" style=\"width: 61px; float: left; text-align: Left; display: none;\">", i.ToString());
        html.AppendFormat("   <span>{0}</span><br />", HttpContext.GetGlobalResourceObject("LibraryResource", "lblChildAge").ToString() + " 4");
        html.AppendFormat("   <select id=\"fltRoomInfoChd4{0}\" style=\"width: 100%;\">", i.ToString());
        for (j = 0; j <= maxChildAge; j++)
            html.AppendFormat("     <option value='{0}'>{1}</option>", j.ToString(), j.ToString());
        html.AppendFormat("  </select>");
        html.AppendFormat(" </div>");

        html.AppendFormat("</div>");

        return html.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string SetCriterias(string Direction, long? CheckIn, long? CheckOut, string RoomCount, string roomsInfo,
        int? Country, string Airport, int? Resort, string CurControl, string CurrentCur, string TrfType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["TransferCriteria"] == null) CreateFilter();
        OnlyTransferFilter criteria = (OnlyTransferFilter)HttpContext.Current.Session["OnlyTransferFilter"];
        criteria.Direction = Direction;
        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(CheckIn) / 86400000) + 1);
        DateTime checkOut = new DateTime(1970, 1, 1);
        checkOut = checkOut.AddDays((Convert.ToInt64(CheckOut) / 86400000) + 1);
        criteria.BegDate = checkIn;
        criteria.EndDate = checkOut;
        criteria.Country = Country;
        criteria.TrfType = TrfType;
        AirportRecord airport = new OnlyTransfers().getTransferAirport(UserData, Airport, ref errorMsg);
        if (airport != null)
        {
            switch (Direction)
            {
                case "F":
                    criteria.TrfFrom = airport.TrfLocation.HasValue ? airport.TrfLocation.Value : airport.Location;
                    criteria.TrfTo = Resort;
                    break;
                case "B":
                    criteria.TrfFrom = Resort;
                    criteria.TrfTo = airport.TrfLocation.HasValue ? airport.TrfLocation.Value : airport.Location;
                    break;
                case "R":
                    criteria.TrfFrom = airport.TrfLocation.HasValue ? airport.TrfLocation.Value : airport.Location;
                    criteria.TrfTo = Resort;
                    break;
            }
        }
        criteria.RoomCount = Convert.ToInt16(RoomCount);
        string _room = "[" + roomsInfo.Replace("|", "\"").Replace(')', '}').Replace('(', '{') + "]";
        List<SearchCriteriaRooms> rooms = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchCriteriaRooms>>(_room);
        criteria.RoomsInfo.Clear();
        int i = 0;
        foreach (SearchCriteriaRooms row in rooms)
        {
            ++i;
            criteria.RoomsInfo.Add(new SearchCriteriaRooms
            {
                RoomNr = i,
                Adult = row.Adult,
                Child = row.Child,
                Chd1Age = row.Chd1Age >= 0 ? row.Chd1Age : null,
                Chd2Age = row.Chd2Age >= 0 ? row.Chd2Age : null,
                Chd3Age = row.Chd3Age >= 0 ? row.Chd3Age : null,
                Chd4Age = row.Chd4Age >= 0 ? row.Chd4Age : null
            });
        }
        criteria.CurControl = Equals(CurControl, "Y");
        criteria.CurrentCur = CurrentCur;

        HttpContext.Current.Session["OnlyTransferFilter"] = criteria;
        return "OK";
    }
}