﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResMonitor.aspx.cs" Inherits="ResMonitor"
    EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<%@ Register Src="~/Comment.ascx" TagName="ResComment" TagPrefix="resCom1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "ResMonitor") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.simplemodal.js" type="text/javascript"></script>
    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
    <script src="Scripts/json2.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/ResMonitor.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script type="text/javascript">

        $.blockUI.defaults.message = '<h1>' + '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>' + '<\/h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);


        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        var comboAllStr = '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>';

    function logout() {
        $('<div>' + '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>' + '</div>').dialog({
                autoOpen: true,
                position: {
                    my: 'center',
                    at: 'center'
                },
                modal: true,
                resizable: true,
                autoResize: true,
                bigframe: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                    $(this).dialog("close");
                    $(this).dialog("destroy");
                    window.location = 'Default.aspx';
                }
            }
          });
        window.setTimeout(function () { window.location = "<%=VirtualPathUtility.ToAbsolute("~/Default.aspx")%>"; }, 10000);
        }

        function showAlert(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                          $(this).dialog('close');
                      }
                  }
                });
              });
      }

      var NS = document.all;

      function maximize() {
          var maxW = screen.availWidth;
          var maxH = screen.availHeight;
          if (location.href.indexOf('pic') == -1) {
              if (window.opera) { } else {
                  top.window.moveTo(0, 0);
                  if (document.all) { top.window.resizeTo(maxW, maxH); }
                  else
                      if (document.layers || document.getElementById) {
                          if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                              top.window.outerHeight = maxH; top.window.outerWidth = maxW;
                          }
                      }
              }
          }
      }

      function setDepCity(data) {
          $("#fltDepCity").html('');
          var comboData = '';
          comboData += '<option value="">' + comboAllStr + '<\/option>';
          var depCityData = data.DepCityData;
          $.each(depCityData, function (i) {
              comboData += '<option value="' + this.Code + '">' + this.Name + '<\/option>';
          });
          $("#fltDepCity").html(comboData);
          if (data.Filter.DepCity != null && data.Filter.DepCity != '') $("#fltDepCity").val(data.Filter.DepCity);
      }

      function setArrCity(data) {
          $("#fltArrCity").html('');
          $("#fltArrCity").append("<option value=''>" + comboAllStr + "<\/option>");
          $.each(data.ArrCityData, function (i) {
              $("#fltArrCity").append("<option value='" + this.Code + "'>" + this.Name + "<\/option>");
          });
          if (data.Filter.ArrCity != null && data.Filter.ArrCity != '') $("#fltArrCity").val(data.Filter.ArrCity);
      }

      function setHolpack(data) {
          $("#fltHolPack").html('');
          $("#fltHolPack").append("<option value=''>" + comboAllStr + "<\/option>");
          $.each(data.HolPackData, function (i) {
              $("#fltHolPack").append("<option value='" + this.Code + "'>" + this.Name + "<\/option>");
          });
          if (data.Filter.HolPack != null && data.Filter.HolPack != '') $("#fltHolPack").val(data.Filter.HolPack)
      }

      function setAgencyOffice(data) {
          $("#fltAgencyOffice").html('');
          $("#fltAgencyOffice").data('UserList', data.AgencyUserData);
          $("#fltAgencyOffice").data('User', data.Filter.AgencyUser);
          $("#fltAgencyOffice").data('ShowAllRes', data.Filter.ShowAllRes);

          $("#fltAgencyOffice").append("<option value=''>" + comboAllStr + "<\/option>");
          $.each(data.AgencyOfficeData, function (i) {
              $("#fltAgencyOffice").append("<option value='" + this.Code + "'>" + this.Name + "<\/option>");
          });
          if (data.Filter.AgencyOffice != null && data.Filter.AgencyOffice != '') $("#fltAgencyOffice").val(data.Filter.AgencyOffice);
      }

      function changeAgencyOffice(agencyOffice) {
          var userList = $("#fltAgencyOffice").data('UserList');
          var user = $("#fltAgencyOffice").data('User');
          var showAllRes = $("#fltAgencyOffice").data('ShowAllRes');
          var activeUserList = [];
          if (agencyOffice != '') {
              $.each(userList, function (i) {
                  if (this.ParentCode == agencyOffice)
                      activeUserList.push(this);
              });
          }
          else {
              activeUserList = userList;
          }
          setAgencyUser(activeUserList, user, showAllRes);
      }

      function setAgencyUser(data, AgencyUser, ShowAllRes) {
          $("#fltAgencyUser").html('');
          $("#fltAgencyUser").append("<option value=''>" + comboAllStr + "<\/option>");
          $.each(data, function (i) {
              $("#fltAgencyUser").append("<option value='" + this.Code + "'>" + this.Name + "<\/option>");
          });
          if (AgencyUser != null && AgencyUser != '')
              $("#fltAgencyUser").val(AgencyUser);

          if (!ShowAllRes)
              $("#fltAgencyUser").attr("disabled", true);
          else $("#fltAgencyUser").attr("disabled", false);
      }

      function setCurrency(data) {
          $("#fltCurrency").html('');
          var comboData = '<option value="">' + comboAllStr + '<\/option>';
          $.each(data.CurrencyData, function (i) {
              comboData += '<option value="' + this.Code + '">' + this.Name + '<\/option>';
          });
          $("#fltCurrency").html(comboData);

          if (data.Filter.Currency != null && data.Filter.Currency != '')
              $("#fltCurrency").val(data.Filter.Currency);
      }

      function DateChgBegDate(_TextDay1, _PopCal) {
          if (_TextDay1.value == '') return
          var _TextDay2 = document.getElementById('<%= fltBeginDate2.ClientID %>');
                  if ((!_TextDay1) || (!_PopCal)) return
                  if (_TextDay2.value != '') {
                      var _format = _TextDay1.getAttribute("Format");
                      var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                      var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                      if (_Day1 > _Day2) {
                          showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                  _TextDay2.value = _TextDay1.value;
              }
          }
      }

      function DateChgBegDateTo(_TextDay2, _PopCal) {
          if (_TextDay2.value == '') return
          var _TextDay1 = document.getElementById('<%= fltBeginDate1.ClientID %>');
              if ((!_TextDay2) || (!_PopCal)) return
              if (_TextDay1.value != '') {
                  var _format = _TextDay2.getAttribute("Format");
                  var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                  var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                  if (_Day1 > _Day2) {
                      showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                  _TextDay2.value = _TextDay1.value;
              }
          }
      }

      function DateChgEndDate(_TextDay1, _PopCal) {
          if (_TextDay1.value == '') return
          var _TextDay2 = document.getElementById('<%= fltEndDate2.ClientID %>');
              if ((!_TextDay1) || (!_PopCal)) return
              if (_TextDay2.value != '') {
                  var _format = _TextDay1.getAttribute("Format");
                  var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                  var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                  if (_Day1 > _Day2) {
                      showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                  _TextDay2.value = _TextDay1.value;
              }
          }
      }

      function DateChgEndDateTo(_TextDay2, _PopCal) {
          if (_TextDay2.value == '') return
          var _TextDay1 = document.getElementById('<%= fltEndDate1.ClientID %>');
              if ((!_TextDay2) || (!_PopCal)) return
              if (_TextDay1.value != '') {
                  var _format = _TextDay2.getAttribute("Format");
                  var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                  var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                  if (_Day1 > _Day2) {
                      showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                  _TextDay2.value = _TextDay1.value;
              }
          }
      }

      function DateChgResDate(_TextDay1, _PopCal) {
          if (_TextDay1.value == '') return
          var _TextDay2 = document.getElementById('<%= fltResDate2.ClientID %>');
              if ((!_TextDay1) || (!_PopCal)) return
              if (_TextDay2.value != '') {
                  var _format = _TextDay1.getAttribute("Format");
                  var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                  var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                  if (_Day1 > _Day2) {
                      showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                  _TextDay2.value = _TextDay1.value;
              }
          }
      }

      function DateChgResDateTo(_TextDay2, _PopCal) {
          if (_TextDay2.value == '') return
          var _TextDay1 = document.getElementById('<%= fltResDate1.ClientID %>');
              if ((!_TextDay2) || (!_PopCal)) return
              if (_TextDay1.value != '') {
                  var _format = _TextDay2.getAttribute("Format");
                  var _Day2 = _PopCal.getDate(_TextDay2.value, _format);
                  var _Day1 = _PopCal.getDate(_TextDay1.value, _format);
                  if (_Day1 > _Day2) {
                      showAlert('<%= GetGlobalResourceObject("LibraryResource", "LastDateSmall") %>');
                  _TextDay2.value = _TextDay1.value;
              }
          }
      }

      function getPageCount() {
          var pageItemCount = $.cookies.get('pageItemCountResMonitor');
          if (pageItemCount == null) {
              pageItemCount = '10';
              $.cookies.set('pageItemCountResMonitor', '10');
          }
          return parseInt(pageItemCount);
      }

      function setOtherFields(data) {
          $('input[name=fltResStatus]').each(function (index) { this.checked = false; });
          if (data.Filter.ResStatus != null && data.Filter.ResStatus != '') {
              for (i = 0; i < data.Filter.ResStatus.split(';').length; i++) {
                  $("#fltResStatus" + data.Filter.ResStatus.split(';')[i]).attr("checked", "checked");
              }
          }
          $('input[name=fltConfirmStatus]').each(function (index) { this.checked = false; });
          if (data.Filter.ConfStatus != null && data.Filter.ConfStatus != '') {
              for (i = 0; i < data.Filter.ConfStatus.split(';').length; i++) {
                  $("#fltConfirmStatus" + data.Filter.ConfStatus.split(';')[i]).attr("checked", "checked");
              }
          }
          $('input[name=fltPaymentStatus]').each(function (index) { this.checked = false; });
          if (data.Filter.PayStatus != null && data.Filter.PayStatus != '') {
              for (i = 0; i < data.Filter.PayStatus.split(';').length; i++) {
                  $("#fltPaymentStatus" + data.Filter.PayStatus.split(';')[i]).attr("checked", "checked");
              }
          }

          $("#fltSurname").val(data.Filter.LeaderNameF != null ? data.Filter.LeaderNameF : "");
          $("#fltName").val(data.Filter.LeaderNameS != null ? data.Filter.LeaderNameS : "");
          $("#fltResNo1").val(data.Filter.ResNo1 != null ? data.Filter.ResNo1 : "");
          $("#fltResNo2").val(data.Filter.ResNo2 != null ? data.Filter.ResNo2 : "");
          var _date = '';
          if (data.Filter.BegDate1 != null && data.Filter.BegDate1 != '') {
              var _date = new Date(Date(eval(data.Filter.BegDate1.toString().substring(0, 10))).toString());
              $("#fltBeginDate1").val($.format.date(_date, data.Filter.DateFormat));
          }
          else $("#fltBeginDate1").val('');

          if (data.Filter.BegDate2 != null && data.Filter.BegDate2 != '') {
              var _date = new Date(Date(eval(data.Filter.BegDate2.toString().substring(0, 10))).toString());
              $("#fltBeginDate2").val($.format.date(_date, data.Filter.DateFormat));
          }
          else $("#fltBeginDate2").val('');


          if (data.Filter.EndDate1 != null && data.Filter.EndData1 != '') {
              var _date = new Date(Date(eval(data.Filter.EndDate1.toString().substring(0, 10))).toString());
              $("#fltEndDate1").val($.format.date(_date, data.Filter.DateFormat));
          }
          else $("#fltEndDate1").val('');

          if (data.Filter.EndDate2 != null && data.Filter.EndData2 != '') {
              var _date = new Date(Date(eval(data.Filter.EndDate2.toString().substring(0, 10))).toString());
              $("#fltEndDate2").val($.format.date(_date, data.Filter.DateFormat));
          }
          else $("#fltEndDate2").val('');

          if (data.Filter.ResDate1 != null && data.Filter.ResDate1 != '') {
              var _date = new Date(Date(eval(data.Filter.ResDate1.toString().substring(0, 10))).toString());
              $("#fltResDate1").val($.format.date(_date, data.Filter.DateFormat));
          }
          else $("#fltResDate1").val('');

          if (data.Filter.ResDate2 != null && data.Filter.ResData2 != '') {
              var _date = new Date(Date(eval(data.Filter.ResDate2.toString().substring(0, 10))).toString());
              $("#fltResDate2").val($.format.date(_date, data.Filter.DateFormat));
          }
          else $("#fltResDate2").val('');
      }

      function getBonus() {
          $.ajax({
              async: false,
              type: "POST",
              data: '{}',
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              url: 'ResMonitor.aspx/getTotalBonus',
              success: function (msg) {
                  $("#divBonus").html('');
                  $("#divBonus").html(msg.d);
              },
              error: function (xhr, msg, e) {
                  if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                      showAlert(xhr.responseText);
              },
              statusCode: {
                  408: function () {
                      logout();
                  }
              }
          });
      }

      function createFilterData(clear) {
          $.ajax({
              async: false,
              type: "POST",
              data: '{"clear":"' + clear + '"}',
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              url: 'ResMonitor.aspx/CreateFilterData',
              success: function (msg) {
                  if (msg.hasOwnProperty('d') && msg.d != '') {
                      var data = JSON.parse(msg.d);//$.json.decode(msg.d);
                      setDepCity(data);
                      setArrCity(data);
                      setHolpack(data);
                      setAgencyOffice(data);
                      setAgencyUser(data.AgencyUserData, data.Filter.AgencyUser, data.Filter.ShowAllRes);
                      setCurrency(data);
                      if (data.ShowDraft == true)
                          $("#showDraftDiv").show();
                      else $("#showDraftDiv").hide();
                      if (data.ShowOptionControl)
                          $("#showOptionControlDiv").show();
                      else $("#showOptionControlDiv").hide();

                      setOtherFields(data);
                  }
              },
              error: function (xhr, msg, e) {
                  if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                      showAlert(xhr.responseText);
              },
              statusCode: {
                  408: function () {
                      logout();
                  }
              }
          });
      }

      function openRow(rowID) {
          var ocRow = $("#ocRowId" + rowID).val();
          if (ocRow == '1') {
              $("#ocRowId" + rowID).val('0');
              $("#openDetail" + rowID).attr("src", "Images/infoOpen.gif");
              $("#rowOtherInfo" + rowID).hide();
          }
          else {
              $("#ocRowId" + rowID).val('1');
              $("#openDetail" + rowID).attr("src", "Images/infoClose.gif");
              $("#rowOtherInfo" + rowID).show();
          }
      }

      function searchData(pageNo, first) {
          var pageCount = getPageCount();
          $.ajax({
              async: false,
              type: "POST",
              data: '{"PageNr":"' + pageNo + '","PageCount":"' + pageCount + '","First":"' + first + '"}',
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              url: 'ResMonitor.aspx/showResult',
              success: function (msg) {
                  $("#resMonitorGrid").html('');
                  $("#resMonitorGrid").html(msg.d);

                  $(".leaderName").textTruncate(110);
                  $(".hotelName").textTruncate(160);
                  $(".depCity").textTruncate(95);
                  $(".arrCity").textTruncate(95);

              },
              error: function (xhr, msg, e) {
                  if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                      showAlert(xhr.responseText);
              },
              statusCode: {
                  408: function () {
                      logout();
                  }
              }
          });

      }

      function pageDataCountChange() {
          var pageNr = $("#pageList").val();
          var pageDateList = $("#pageDataList").val();
          $.cookies.set('pageItemCountResMonitor', pageDateList);
          searchData(pageNr, true);
      }

      function pageChange() {
          var pageNr = $("#pageList").val();
          var pageDateList = $("#pageDataList").val();
          searchData(pageNr, true);
      }

      function btnFilterClick() {
          var _resStatus = '';
          var _confStatus = '';
          var _payStatus = '';

          var chkBox = $("input:checked");
          for (i = 0; i < chkBox.length; i++) {
              if (chkBox[i].name == 'fltResStatus') {
                  if (_resStatus.length > 0) _resStatus += ';';
                  _resStatus += chkBox[i].value;
              }
              if (chkBox[i].name == 'fltConfirmStatus') {
                  if (_confStatus.length > 0) _confStatus += ';';
                  _confStatus += chkBox[i].value;
              }
              if (chkBox[i].name == 'fltPaymentStatus') {
                  if (_payStatus.length > 0) _payStatus += ';';
                  _payStatus += chkBox[i].value;
              }
          }

          var _showDraft = $("#cbShowDraft:checked").is(':checked') ? '1' : '0';

          var _showOptionRes = $("input[@name='rbOptionControl']:checked").val();

          var _filterData = '';
          _filterData += '<|resNo1|:|' + $("#fltResNo1").val() + '|';
          _filterData += ',|resNo2|:|' + $("#fltResNo2").val() + '|';
          _filterData += ',|begDate1|:|' + $("#fltBeginDate1").val() + '|';
          _filterData += ',|begDate2|:|' + $("#fltBeginDate2").val() + '|';
          _filterData += ',|endDate1|:|' + $("#fltEndDate1").val() + '|';
          _filterData += ',|endDate2|:|' + $("#fltEndDate2").val() + '|';
          _filterData += ',|resDate1|:|' + $("#fltResDate1").val() + '|';
          _filterData += ',|resDate2|:|' + $("#fltResDate2").val() + '|';
          _filterData += ',|payDate1|:||';
          _filterData += ',|payDate2|:||';
          _filterData += ',|arrCity|:|' + $("#fltArrCity").val() + '|';
          _filterData += ',|depCity|:|' + $("#fltDepCity").val() + '|';
          _filterData += ',|agencyUser|:|' + $("#fltAgencyUser").val() + '|';
          _filterData += ',|leaderNameF|:|' + $("#fltName").val() + '|';
          _filterData += ',|leaderNameS|:|' + $("#fltSurname").val() + '|';
          _filterData += ',|holPack|:|' + $("#fltHolPack").val() + '|';
          _filterData += ',|resStatus|:|' + _resStatus + '|';
          _filterData += ',|confStatus|:|' + _confStatus + '|';
          _filterData += ',|payStatus|:|' + _payStatus + '|';
          _filterData += ',|showDraft|:|' + _showDraft + '|';
          _filterData += ',|showOptionRes|:|' + _showOptionRes + '|';
          _filterData += ',|agencyOffice|:|' + $("#fltAgencyOffice").val() + '|';
          _filterData += ',|dateFormat|:|' + $("#fltBeginDate1").attr("Format") + '|';
          _filterData += ',|PageNo|:|' + '1' + '|';
          _filterData += ',|Currency|:|' + $("#fltCurrency").val() + '|>';
          $.ajax({
              async: false,
              type: "POST",
              data: '{"data":"' + _filterData + '"}',
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              url: 'ResMonitor.aspx/SaveFilter',
              success: function (msg) {
                  if (msg.d == "OK")
                      searchData(1, false);
                  getBonus();
              },
              error: function (xhr, msg, e) {
                  if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                      showAlert(xhr.responseText);
              },
              statusCode: {
                  408: function () {
                      logout();
                  }
              }
          });
      }

      function ShowDraftClick() {
          if ($(".divSelect").attr("display") == 'block') {
              $("#rbOptionControl").hidden();
              $("#divStatus").hidden();
          }
          else {
              $("#rbOptionControl").show();
              $("#divStatus").show();
          }
      }

      function showResNote(msg) {
          $(function () {
              $("#messages").html(msg.toString().replace(/!/g, '\"'));
              $("#dialog").dialog("destroy");
              $("#dialog-message").dialog({
                  modal: true,
                  buttons: {
                      '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        $(this).dialog('close');
                    }
                  }
                });
              });
    }

    function showCommentDiv(resNo, userID, ci) {
        $(function () {
            $("#dialog").dialog("destroy");
            $("#dialog-resComment").dialog({
                modal: true,
                height: 500,
                width: 820,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnClear") %>': function () {
                              var comment = document.getElementById('<%= (resComment.FindControl("txtMessage")).ClientID %>');
                            comment.value = "";
                            return true;
                        },
                          '<%= GetGlobalResourceObject("LibraryResource", "btnSend") %>': function () {
                              var comment = document.getElementById('<%= (resComment.FindControl("txtMessage")).ClientID %>');
                          if (comment.value != '') {
                              var paraList = '{"ResNo":"' + resNo + '","Comment":"' + comment.value + '","UserID":"' + userID + '","lcID":' + ci + '}';
                              $.ajax({
                                  type: "POST",
                                  data: paraList,
                                  dataType: "json",
                                  contentType: "application/json; charset=utf-8",
                                  url: 'ResMonitor.aspx/setCommentData',
                                  success: function (msg) {
                                      $('#lblCommentHeader').html(resNo);
                                      $('#gridComment').html('');
                                      $('#gridComment').html(msg.d);
                                      comment.value = '';
                                  },
                                  error: function (xhr, msg, e) {
                                      if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                          showAlert('<%= GetGlobalResourceObject("LibraryResource","lblNotSaved").ToString() %>');
                              },
                                statusCode: {
                                    408: function () {
                                        logout();
                                    }
                                }
                            });
                    }
                          return true;
                      },
                          '<%= GetGlobalResourceObject("LibraryResource", "btnRefresh") %>': function () {
                              var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","lcID":' + ci + '}';
                              $.ajax({
                                  type: "POST",
                                  data: paraList,
                                  dataType: "json",
                                  contentType: "application/json; charset=utf-8",
                                  url: 'ResMonitor.aspx/getCommentData',
                                  success: function (msg) {
                                      $('#lblCommentHeader').html(resNo);
                                      $('#gridComment').html('');
                                      $('#gridComment').html(msg.d);
                                  },
                                  error: function (xhr, msg, e) {
                                      if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                          showAlert(xhr.responseText);
                                  },
                                  statusCode: {
                                      408: function () {
                                          logout();
                                      }
                                  }
                              });
                              return true;
                          },
                          '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                              $(this).dialog('close');
                              return false;
                          }
                      }
                    });
                      $('#dialog-resComment').live("dialogclose", function () {
                          if ($(this).find("#commnetCount").val() == '0') {
                              $("#img_" + resNo).html('');
                              $("#img_" + resNo).html('&nbsp;');
                          }
                      });
                  });
          }

          function readAllComment(resNo, userID, ci) {
              var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","RecID":-1,"lcID":' + ci + '}';
              $.ajax({
                  type: "POST",
                  data: paraList,
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  url: 'ResMonitor.aspx/setReadCommentData',
                  success: function (msg) {
                      var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","lcID":' + ci + '}';
                      $.ajax({
                          type: "POST",
                          data: paraList,
                          dataType: "json",
                          contentType: "application/json; charset=utf-8",
                          url: 'ResMonitor.aspx/getCommentData',
                          success: function (msg) {
                              $('#lblCommentHeader').html(resNo);
                              $('#gridComment').html(msg.d);
                              showCommentDiv(resNo, userID, ci);
                          },
                          error: function (xhr, msg, e) {
                              if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                  showAlert(xhr.responseText);
                          },
                          statusCode: {
                              408: function () {
                                  logout();
                              }
                          }
                      });
                  },
                  error: function (xhr, msg, e) {
                      if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                          showAlert(xhr.responseText);
                  },
                  statusCode: {
                      408: function () {
                          logout();
                      }
                  }
              });
          }

          function showResComment(resNo, userID, ci) {
              if ($("#hfCustomRegID").val() == '0855101') {
                  readAllComment(resNo, userID, ci);
              }
              else {
                  var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","lcID":' + ci + '}';
                  $.ajax({
                      type: "POST",
                      data: paraList,
                      dataType: "json",
                      contentType: "application/json; charset=utf-8",
                      url: 'ResMonitor.aspx/getCommentData',
                      success: function (msg) {
                          $('#lblCommentHeader').html(resNo);
                          $('#gridComment').html(msg.d);
                          showCommentDiv(resNo, userID, ci);
                      },
                      error: function (xhr, msg, e) {
                          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                              showAlert(xhr.responseText);
                      },
                      statusCode: {
                          408: function () {
                              logout();
                          }
                      }
                  });
              }
          }

          function readComment(resNo, userID, recID, ci) {
              var paraList = '{"ResNo":"' + resNo + '","UserID":"' + userID + '","RecID":' + recID + ',"lcID":' + ci + '}';
              $.ajax({
                  type: "POST",
                  data: paraList,
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  url: 'ResMonitor.aspx/setReadCommentData',
                  success: function (msg) {
                      $('#lblCommentHeader').html(resNo);
                      $('#gridComment').html('');
                      $('#gridComment').html(msg.d);
                  },
                  error: function (xhr, msg, e) {
                      if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                          showAlert('<%= GetGlobalResourceObject("LibraryResource","lblNotSaved").ToString() %>');
                  },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }

                });
    }

    function showCommissionInvoiceDiv(msg) {
        $(function () {
            $('#commissionInvoice').html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-commissionInvoice").dialog({
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                      $(this).dialog('close');
                      return false;

                  }
                  }
                });
              });
  }

  function showCommissionInvoice(resNo, ci) {
      //paymentPlan dialog-resComment
      var paraList = '{"ResNo":"' + resNo + '", "lcID":' + ci + '}';
      $.ajax({
          type: "POST",
          data: paraList,
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          url: 'ResMonitor.aspx/getCommisionInvoices',
          success: function (msg) {
              showCommissionInvoiceDiv(msg.d);
          },
          error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                  showAlert(xhr.responseText);
          },
          statusCode: {
              408: function () {
                  logout();
              }
          }
      });
  }

  function showPaymentPlanDiv(msg) {
      $(function () {
          $('#paymentPlan').html(msg);
          $("#dialog").dialog("destroy");
          $("#dialog-paymentPlan").dialog({
              modal: true,
              buttons: {
                  '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                            return false;

                        }
                      }
                    });
                  });
        }

        function showPaymentPlan(resNo, ci) {
            //paymentPlan dialog-resComment
            var paraList = '{"ResNo":"' + resNo + '", "lcID":' + ci + '}';
            $.ajax({
                type: "POST",
                data: paraList,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'Services/ResMonitorServices.asmx/getPaymentPlanData',
                success: function (msg) {
                    showPaymentPlanDiv(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function showResView(ResNo) {
            $.ajax({
                type: "POST",
                data: '{"ResNo":"' + ResNo + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: 'ResMonitor.aspx/reservationIsBusy',
                success: function (msg) {
                    if (msg.d != '')
                        showAlert(msg.d);
                    else window.location = $("#basePageUrl").val() + "ResView.aspx?ResNo=" + ResNo;

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function b2cRes() {
            $("#forB2CRes").val('2');
        }

        function makeReservation() {
            $('html').css('overflow', 'hidden');
            $("#MakeReservation").attr("src", 'MakeReservation.aspx' + ($("#forB2CRes").val() == '1' ? '?forb2c=1' : ''));
            $("#dialog").dialog("destroy");
            $("#dialog-MakeReservation").dialog(
                {
                    autoOpen: true,
                    modal: true,
                    minWidth: 1000,
                    minHeight: 600,
                    resizable: true,
                    autoResize: false,
                    bigframe: true,
                    close: function (event, ui) {
                        $('html').css('overflow', 'auto');
                        createFilterData(false);
                        btnFilterClick();
                    }
                })
                .dialogExtend({
                    "maximize": true,
                    "icons": {
                        "maximize": "ui-icon-circle-plus",
                        "restore": "ui-icon-pause"
                    }
                });
            $("#dialog-MakeReservation").dialogExtend("maximize");
        }

        function cancelMakeRes() {
            $("#dialog-MakeReservation").dialog("close");
            createFilterData(false);
        }

        function gotoResViewPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            $('html').css('overflow', 'auto');
            window.location = 'ResView.aspx?ResNo=' + ResNo;
        }

        function paymentPage(ResNo) {
            $("#MakeReservation").attr("src", "");
            $("#dialog-MakeReservation").dialog("close");
            window.open('Payments/BeginPayment.aspx?ResNo=' + ResNo, '_blank');
        }

        function showListBonus(_html) {
            $("#divBonusList").html('');
            $("#divBonusList").html(_html);
            $(function () {
                $("#dialog").dialog("destroy");
                $("#dialog-bonusList").dialog({
                    modal: true,
                    height: 500,
                    width: 820,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                              $("#dialog-bonusList").dialog("close");
                              return false;
                          }
                      }
                    });
                  });
          }

          function showBonusList(list) {
              $.ajax({
                  type: "POST",
                  data: '{"listType":"' + list + '"}',
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  url: 'ResMonitor.aspx/getBonusList',
                  success: function (msg) {
                      showListBonus(msg.d);
                  },
                  error: function (xhr, msg, e) {
                      if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                          showAlert(xhr.responseText);
                  },
                  statusCode: {
                      408: function () {
                          logout();
                      }
                  }
              });
          }

          function exportExcel() {
              $.ajax({
                  type: "POST",
                  data: '{}',
                  dataType: "json",
                  contentType: "application/json; charset=utf-8",
                  url: 'ResMonitor.aspx/exportExcel',
                  success: function (msg) {
                      if (msg.d != '')
                          window.open(msg.d);
                  },
                  error: function (xhr, msg, e) {
                      if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                          showAlert(xhr.responseText);
                  },
                  statusCode: {
                      408: function () {
                          logout();
                      }
                  }
              });
          }

          function showExportHtml(_html) {
              window.scrollTo(0, 0);
              $("#exportDiv").html('');
              $("#exportDiv").html(_html);
              $("#dialog").dialog("destroy");
              $("#dialog-export").dialog({
                  modal: true,
                  buttons: {
                      '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>': function () {
                            $("#dialog-export").dialog("close");
                            return false;
                        }
                    }
                  })
            .dialogExtend({
                "maximize": true,
                "icons": {
                    "maximize": "ui-icon-circle-plus",
                    "restore": "ui-icon-pause"
                }
            });
                $("#dialog-export").dialogExtend("maximize");
            }

            function exportHtml() {
                $.ajax({
                    type: "POST",
                    data: '{}',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    url: 'ResMonitor.aspx/exportHtml',
                    success: function (msg) {
                        if (msg.d != '')
                            showExportHtml(msg.d);
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showAlert(xhr.responseText);
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }

            $(document).ready(
                function () {
                    maximize();
                    createFilterData(false);
                    $.query = $.query.load(location.href);
                    fromB2C = $.query.get('FromB2C');
                    if (fromB2C == '1') {
                        if ($("#forB2CRes").val() != '2') {
                            $("#forB2CRes").val('1');
                            setTimeout('makeReservation()', 1000);
                        }
                    }
                    else
                        btnFilterClick();
                });

    </script>

</head>
<body>
    <form id="ResMonitorForm" runat="server">
        <div class="Page">
            <tv1:Header ID="tvHeader" runat="server" />
            <tv1:MainMenu ID="tvMenu" runat="server" />
            <div class="FilterArea">
                <table cellpadding="0" cellspacing="0" width="998px">
                    <tr>
                        <td class="LeftDiv border" style="width: 495px">
                            <div class="divResNo">
                                <span class="Caption"><strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblResNo")%>:&nbsp;</strong></span>
                                <div style="width: 115px; float: left;">
                                    <input id="fltResNo1" style="width: 110px;" />
                                </div>
                                <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                                    <strong>&gt;&gt;</strong>
                                </div>
                                <div style="width: 115px; float: left;">
                                    <input id="fltResNo2" style="width: 110px;" />
                                </div>
                            </div>
                            <div class="divResBegDate">
                                <span class="Caption"><strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblBeginDate")%>:&nbsp;</strong></span>
                                <div style="width: 115px; float: left; white-space: nowrap;">
                                    <asp:TextBox ID="fltBeginDate1" runat="server" Width="90px" />
                                    <rjs:PopCalendar ID="ppcBegDate1" runat="server" Control="fltBeginDate1" ClientScriptOnDateChanged="DateChgBegDate" />
                                </div>
                                <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                                    <strong>&gt;&gt;</strong>
                                </div>
                                <div style="width: 115px; float: left; white-space: nowrap;">
                                    <asp:TextBox ID="fltBeginDate2" runat="server" Width="90px" />
                                    <rjs:PopCalendar ID="ppcBegDate2" runat="server" Control="fltBeginDate2" ClientScriptOnDateChanged="DateChgBegDateTo" />
                                </div>
                            </div>
                            <div class="divResEndDate">
                                <span class="Caption"><strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblEndDate")%>:&nbsp;</strong></span>
                                <div style="width: 115px; float: left; white-space: nowrap;">
                                    <asp:TextBox ID="fltEndDate1" runat="server" Width="90px" />
                                    <rjs:PopCalendar ID="ppcEndDate1" runat="server" Control="fltEndDate1" ClientScriptOnDateChanged="DateChgEndDate" />
                                </div>
                                <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                                    <strong>&gt;&gt;</strong>
                                </div>
                                <div style="width: 115px; float: left; white-space: nowrap;">
                                    <asp:TextBox ID="fltEndDate2" runat="server" Width="90px" />
                                    <rjs:PopCalendar ID="ppcEndDate2" runat="server" Control="fltEndDate2" ClientScriptOnDateChanged="DateChgEndDateTo" />
                                </div>
                            </div>
                            <div class="divResRegisterDate">
                                <span class="Caption"><strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblResDate")%>:&nbsp;</strong></span>
                                <div style="width: 115px; float: left; white-space: nowrap;">
                                    <asp:TextBox ID="fltResDate1" runat="server" Width="90px" />
                                    <rjs:PopCalendar ID="ppcResDate1" runat="server" Control="fltResDate1" ClientScriptOnDateChanged="DateChgResDate" />
                                </div>
                                <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                                    <strong>&gt;&gt;</strong>
                                </div>
                                <div style="width: 115px; float: left; white-space: nowrap;">
                                    <asp:TextBox ID="fltResDate2" runat="server" Width="90px" />
                                    <rjs:PopCalendar ID="ppcResDate2" runat="server" Control="fltResDate2" ClientScriptOnDateChanged="DateChgResDateTo" />
                                </div>
                            </div>
                            <div class="divHolpack">
                                <span class="Caption"><strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblHolpack")%>:&nbsp;</strong></span>
                                <div style="width: 300px; float: left; text-align: left;">
                                    <select id="fltHolPack" style="width: 95%;">
                                    </select>
                                </div>
                            </div>
                            <div class="divDepCity">
                                <span class="Caption"><strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblDepCity")%>:&nbsp;</strong></span>
                                <div style="width: 300px; float: left; text-align: left;">
                                    <select id="fltDepCity" style="width: 95%;">
                                    </select>
                                </div>
                            </div>
                            <div class="divArrCity">
                                <span class="Caption"><strong>
                                    <%= GetGlobalResourceObject("ResMonitor", "lblArrCity")%>:&nbsp;</strong></span>
                                <div style="width: 300px; float: left; text-align: left;">
                                    <select id="fltArrCity" style="width: 95%;">
                                    </select>
                                </div>
                            </div>
                            <div class="divArrCity">
                                <span class="Caption"><strong>
                                    <%= GetGlobalResourceObject("ResView", "lblCurrency")%>:&nbsp;</strong></span>
                                <div style="width: 300px; float: left; text-align: left;">
                                    <select id="fltCurrency" style="width: 95%;">
                                    </select>
                                </div>
                            </div>
                            <div class="divSelect">
                                <div id="showDraftDiv" style="margin-left: 3px;">
                                    <input id="cbShowDraft" type="checkbox" /><label for="cbShowDraft"><%= GetGlobalResourceObject("LibraryResource", "cbShowDraft")%></label>
                                </div>
                                <div id="showOptionControlDiv" style="margin-left: 3px;">
                                    <input id="rbOptionControl0" type="radio" name="rbOptionControl" value="0" checked="checked" /><label
                                        for="rbOptionControl0"><%= GetGlobalResourceObject("LibraryResource", "ReservationAll")%></label>&nbsp;&nbsp;
                                <input id="rbOptionControl1" type="radio" name="rbOptionControl" value="1" /><label
                                    for="rbOptionControl1"><%= GetGlobalResourceObject("LibraryResource", "ReservationOptional")%></label>
                                </div>
                            </div>
                        </td>
                        <td style="width: 4px;">&nbsp;
                        </td>
                        <td class="RightDiv border">
                            <div class="divAgencyOfficeAgencyUser">
                                <div class="divAgencyOffice">
                                    <strong>
                                        <%= GetGlobalResourceObject("ResMonitor", "lblAgencyOffice")%>:&nbsp;</strong>
                                    <br />
                                    <select id="fltAgencyOffice" style="width: 200px;" onchange="changeAgencyOffice(this.value);">
                                    </select>
                                </div>
                                <div class="divAgencyUser">
                                    <strong>
                                        <%= GetGlobalResourceObject("ResMonitor", "lblAgencyUser")%>:&nbsp;</strong>
                                    <br />
                                    <select id="fltAgencyUser" style="width: 200px;">
                                    </select>
                                </div>
                            </div>
                            <div class="divLeader">
                                <div class="divSurname">
                                    <strong>
                                        <%= GetGlobalResourceObject("ResMonitor", "lblSurname")%>:&nbsp;</strong>
                                    <br />
                                    <input id="fltSurname" style="width: 200px;" />
                                </div>
                                <div class="divName">
                                    <strong>
                                        <%= GetGlobalResourceObject("ResMonitor", "lblName")%>:&nbsp;</strong>
                                    <br />
                                    <input id="fltName" style="width: 200px;" />
                                </div>
                            </div>
                            <div class="divStatus">
                                <table>
                                    <tr>
                                        <td>
                                            <strong>
                                                <%= GetGlobalResourceObject("ResMonitor", "lblResStatus")%>:&nbsp;</strong>
                                        </td>
                                        <td>
                                            <strong>
                                                <%= GetGlobalResourceObject("ResMonitor", "lblConfStatus")%>:&nbsp;</strong>
                                        </td>
                                        <td>
                                            <strong>
                                                <%= GetGlobalResourceObject("ResMonitor", "lblPayStatus")%>:&nbsp;</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <input id="fltResStatus0" name="fltResStatus" type="checkbox" value="0" /><label
                                                for="fltStatus0"><%= GetGlobalResourceObject("LibraryResource", "ResStatus0")%></label><br />
                                            <input id="fltResStatus1" name="fltResStatus" type="checkbox" value="1" /><label
                                                for="fltStatus1"><%= GetGlobalResourceObject("LibraryResource", "ResStatus1")%></label><br />
                                            <input id="fltResStatus2" name="fltResStatus" type="checkbox" value="2" /><label
                                                for="fltStatus2"><%= GetGlobalResourceObject("LibraryResource", "ResStatus2")%></label><br />
                                            <input id="fltResStatus3" name="fltResStatus" type="checkbox" value="3" /><label
                                                for="fltStatus3"><%= GetGlobalResourceObject("LibraryResource", "ResStatus3")%></label>
                                        </td>
                                        <td valign="top">
                                            <input id="fltConfirmStatus0" name="fltConfirmStatus" type="checkbox" value="0" /><label
                                                for="fltConfirmStatus0"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus0")%></label><br />
                                            <input id="fltConfirmStatus1" name="fltConfirmStatus" type="checkbox" value="1" /><label
                                                for="fltConfirmStatus1"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus1")%></label><br />
                                            <input id="fltConfirmStatus2" name="fltConfirmStatus" type="checkbox" value="2" /><label
                                                for="fltConfirmStatus2"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus2")%></label><br />
                                            <input id="fltConfirmStatus3" name="fltConfirmStatus" type="checkbox" value="3" /><label
                                                for="fltConfirmStatus3"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus3")%></label>
                                        </td>
                                        <td valign="top">
                                            <input id="fltPaymentStatusU" name="fltPaymentStatus" type="checkbox" value="U" /><label
                                                for="fltPaymentStatusU"><%= GetGlobalResourceObject("LibraryResource", "PayStatusU")%></label><br />
                                            <input id="fltPaymentStatusP" name="fltPaymentStatus" type="checkbox" value="P" /><label
                                                for="fltPaymentStatusU"><%= GetGlobalResourceObject("LibraryResource", "PayStatusP")%></label><br />
                                            <input id="fltPaymentStatusO" name="fltPaymentStatus" type="checkbox" value="O" /><label
                                                for="fltPaymentStatusU"><%= GetGlobalResourceObject("LibraryResource", "PayStatusO")%></label><br />
                                            <input id="fltPaymentStatusV" name="fltPaymentStatus" type="checkbox" value="V" /><label
                                                for="fltPaymentStatusU"><%= GetGlobalResourceObject("LibraryResource", "PayStatusV")%></label><br />
                                            <input id="fltPaymentStatusN" name="fltPaymentStatus" type="checkbox" value="N" /><label
                                                for="fltPaymentStatusN"><%= GetGlobalResourceObject("LibraryResource", "PayStatusN")%></label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="clear: both;" />
                <div id="divBonus">
                </div>
                <div style="clear: both;" />
                <div class="divFilterButton">
                    <table>
                        <tr>
                            <td>
                                <input type="button" id="btnFilter" value='<%= GetGlobalResourceObject("ResMonitor", "btnFilter")%>'
                                    style="width: 140px;" onclick="btnFilterClick();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                            </td>
                            <td>
                                <input type="button" id="btnfilterclear" value='<%= GetGlobalResourceObject("ResMonitor", "btnClear")%>'
                                    style="width: 140px;" onclick="createFilterData(true);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear: both;" />
            <div class="Content">
                <div id="resMonitorGrid">
                </div>
            </div>
            <div class="Footer">
                <tv1:Footer ID="tvfooter" runat="server" />
            </div>
        </div>
        <div id="dialog-paymentPlan" title='<%= GetGlobalResourceObject("ResMonitor", "titlePaymentPlan")%>'
            style="display: none;">
            <span id="paymentPlan"></span>
        </div>
        <div id="dialog-commissionInvoice" title='<%= GetGlobalResourceObject("ResMonitor", "lblCommisionInvoiceNumber")%>'
            style="display: none;">
            <span id="commissionInvoice"></span>
        </div>
        <div id="dialog-resComment" title='<%= GetGlobalResourceObject("ResMonitor", "titleComment")%>'
            style="display: none;">
            <resCom1:ResComment ID="resComment" runat="server" />
        </div>
        <div id="dialog-bonusList" title='' style="display: none;">
            <div id="divBonusList">
            </div>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
        <div id="dialog-export" title="" style="display: none;">
            <div id="exportDiv">
            </div>
        </div>
        <div id="waitMessage" style="display: none;">
            <img alt="" src="Images/Wait.gif" />
        </div>
        <div id="dialog-MakeReservation" title='<%= GetGlobalResourceObject("MakeReservation", "lblMakeReservation") %>'
            style="display: none; text-align: center;">
            <iframe id="MakeReservation" runat="server" height="100%" width="960px" frameborder="0"
                style="clear: both; text-align: left;"></iframe>
        </div>
        <asp:HiddenField ID="hfCustomRegID" runat="server" />
        <input id="forB2CRes" type="hidden" value="0" />
    </form>
</body>
</html>
