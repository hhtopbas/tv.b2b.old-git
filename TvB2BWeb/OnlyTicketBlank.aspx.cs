﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class OnlyTicketBlank : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url); //Session["Menu"] = "OnlyTicketBlank.aspx";
        Response.Redirect("~/OnlyTicket.aspx" + Request.Url.Query);
        
    }
}
