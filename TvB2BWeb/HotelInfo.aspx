﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HotelInfo.aspx.cs" Inherits="TvSearch.HotelInfo"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle","HotelInfo") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="Scripts/simpletabs_1.3.packed.js" type="text/javascript"></script>

    <link href="CSS/HotelInfo.css" rel="stylesheet" type="text/css" />
    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        body { padding: 0 0 0 0; margin: 0 0 0; font-size: 12px; background-color: #FFFFFF; font-family: 'Open Sans', sans-serif; font-style: normal; font-weight: 400; color: #000000; line-height: 18px; }
        .wrapper { width: 700px; height: 500px; margin: 0px auto; }
        .header { width: 680px; height: 53px; float: left; padding: 10px; }
        .paximum-logo { float: left; height: 53px; width: 200px; }
        .ml-10 { margin-left: 10px; }
        .mr-10 { margin-right: 10px; }
        .border-rad { border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; }
        .content { float: left; width: auto; height: auto; padding: 10px; margin: 10px 0; border: 1px solid #E0E0E0; }
        .hotel-photo { float: left; height: 125px; width: 125px; background-color: red; }
            .hotel-photo img { width: 100% !important; height: 100% !important; display: block; }
        .hotel-name { float: left; height: auto; width: 436px; font-size: 18px; font-weight: 600; }
        .hotel-loc { float: left; height: auto; width: 400px; font-size: 14px; font-weight: 600; margin-left: 10px; margin-top: 6px; }
        .hotel-desc { float: left; height: auto; width: 540px; font-size: 12px; font-weight: 400; margin-left: 10px; margin-top: 5px; line-height: 18px; }
        .stars { float: left; height: 22px; margin-top: 2px; overflow: hidden; float: right; }
        .star0 { width: 0; }
        .star1 { width: 20px; }
        .star15 { width: 30px; }
        .star2 { width: 40px; }
        .star25 { width: 50px; }
        .star3 { width: 60px; }
        .star35 { width: 72px; }
        .star4 { width: 80px; }
        .star45 { width: 94px; }
        .star5 { width: 100px; }
        .score-con { float: right; height: 38px; }
            .score-con span { float: left; margin: 10px 0 0 10px; }
        .score-box { height: 30px; width: auto; border: 1px solid #E0E0E0; line-height: 18px; float: right; margin-top: 16px; }
            .score-box span { color: #163645; font-size: 14px; font-style: normal; margin: 6px; font-weight: 700; }
        .rew { font-size: 12px !important; font-weight: normal !important; margin: 6px 10px 6px 1px !important; }
        .tab-content { float: left; width: 100%; height: auto; background-color: #FFFFFF; }
        .desc-list { list-style: none; margin: 0px; padding: 0px; margin-top: -10px; }
            .desc-list li { margin: 10px 0; }
        h5 { font-size: 15px; line-height: 25px; margin: 0; font-weight: 700; }
        .well { background-color: #e4e7ea; width: auto; float: left; margin: 10px 10px 0px 0; padding: 8px 12px 8px 8px; border: 1px solid #ECECEC; }
            .well i { margin-right: 5px; height: 10px; width: 10px; float: left; margin-top: 2px; }
        p { margin: 0px; border-bottom: 1px solid #F3F0F0; padding-bottom: 6px; }
        div.simpleTabs { float: left; margin-top: 12px; }
        ul.simpleTabsNavigation { margin: 0px; padding: 0; text-align: left; }
            ul.simpleTabsNavigation li { list-style: none; display: inline; margin: 0; padding: 0; }
                ul.simpleTabsNavigation li a { border: 1px solid #E0E0E0; padding: 3px 6px; background: #0065A5; color: white; font-size: 14px; text-decoration: none; padding: 8px 12px 8px 8px; }
                    ul.simpleTabsNavigation li a:hover { background: #0065A5; color: white;; }
                    ul.simpleTabsNavigation li a.current { background: #fff; color: #222; border-bottom: 1px solid #fff; font-weight: 600; }
        div.simpleTabsContent { border: 1px solid #E0E0E0; padding: 5px 15px 15px; margin-top: 6px; display: none; }
            div.simpleTabsContent.currentTab { display: block; height: auto; float: left; }
    </style>
    <script language="javascript" type="text/javascript">

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }

        function showFormData() {
            $.ajax({
                type: "POST",
                url: "HotelInfo.aspx/getFormData",
                data: '{"Hotel":"' + $("#hfHotel").val() + '","Market":"' + $("#hfMarket").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    return true;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        window.close();
                    }
                }
            });
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            $("#hfHotel").val($.query.get('Hotel'));
            $("#hfCulture").val($.query.get('Culture'));
            $("#hfMarket").val($.query.get('Market'));
            getHotelImage();
        });

    </script>

</head>
<body class="ui-helper-reset ui-widget">
    <form id="HotelInfoForm" runat="server">
        <div class="wrapper">
            <!--header-->
            <div class="header">
                <div class="paximum-logo">
                </div>
                <div class="score-con">
                    <div class="score-box">
                        <span id="Raiting"></span>
                        <span class="rew">Hotel Score</span>
                    </div>
                </div>
            </div>
            <!--header-->
            <!--Hotel infos-->
            <div class="content">
                <div class="hotel-photo">
                    <img id="HotelImageUrl" alt="hotel Photo" />
                </div>
                <div id="HotelName" class="hotel-name ml-10">                    
                </div>
                <div class="stars">
                    <img id="HotelStars" src="Images/HotelStars/0Stars.png" height="22" alt="" />
                </div>
                <div id="HotelLocation" class="hotel-loc">                    
                </div>
                <div id="Description" class="hotel-desc">                    
                </div>
            </div>
            <!--Hotel infos-->

            <!--Hotel Images-->

            <!--Hotel Images-->

            <!--Hotel Desription &  Facalities-->
            <div class="simpleTabs">
                <ul class="simpleTabsNavigation">
                    <li><a href="#">Facilities</a></li>
                    <li><a href="#">Hotel Description</a></li>
                </ul>
                <!--Facilities-->
                <div id="Facilities" class="simpleTabsContent">

<%--                    {{#Facilities}}
            <div class="well">
                <i>
                    <img alt="" src="Images/well.png" width="10" height="10"></i>{{FacilityName}}
            </div>
                    {{/Facilities}}--%>

                </div>
                <!--Hotel Description-->
                <div class="simpleTabsContent">
                    <div id="HotelDescription">
                        <%--{{#HotelDescription}}
                <h5>{{Title}}</h5>
                        <p>{{Description}}</p>
                        {{/HotelDescription}}     --%>           
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
