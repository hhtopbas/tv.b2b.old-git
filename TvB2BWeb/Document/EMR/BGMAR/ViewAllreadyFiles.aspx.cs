﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvBo;
using TvTools;
using System.Web.Script.Services;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Globalization;

public partial class ViewAllreadyFiles : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        List<ResDocument> docList = new UIReservation().getReadyDocument(UserData, ResData.ResMain.ResNo, ref errorMsg);
        StringBuilder html = new StringBuilder();
        
        html.AppendLine("<table class=\"ui-widget-content\" style=\"width: 500px; text-align: left;\">");        
        html.AppendLine(" <tbody>");

        foreach (ResDocument row in docList)
        {
            html.Append("  <tr>");
            html.AppendFormat("    <td onclick=\"selectedView(" + row.RecID.ToString() + ", '" + row.FileType + "')\"><span style=\"text-decoration:underline; cursor: pointer;\">{0}</span></td>", row.DocName);
            html.AppendLine("  </tr>");

        }
        html.AppendLine("  </tbody>");
        html.AppendLine("</table>");
        return html.ToString(); 
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static object getDocument(int? recID, string fileType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null || !recID.HasValue) { HttpContext.Current.Response.StatusCode = 500; };

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResDocument doc = new UIReservation().getDocument(UserData, recID.Value, ref errorMsg);
        byte[] docFile = new UIReservation().getDocumentObject(UserData, recID.Value, ref errorMsg);
        if (File.Exists(HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute("~/ACE") + "\\" + ResData.ResMain.ResNo + "_" + doc.DocName + "." + doc.FileType.ToLower()))) {
            File.Delete(HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute("~/ACE") + "\\" + ResData.ResMain.ResNo + "_" + doc.DocName + "." + doc.FileType.ToLower()));
        }
        File.WriteAllBytes(HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute("~/ACE") + "\\" + ResData.ResMain.ResNo + "_" + doc.DocName + "." + doc.FileType.ToLower()), docFile);
        return new {
            FileName = Global.getBasePageRoot() + "ACE/" + ResData.ResMain.ResNo + "_" + doc.DocName + "." + doc.FileType.ToLower()
        };
    }    
}
