﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvBo;
using TvTools;
using System.Web.Script.Services;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

public partial class EMR_Invoice : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack) {

        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        List<ReservationInvoice> resInv = new Payments().getReservationInvoiceList(UserData, ResData.ResMain.ResNo, ref errorMsg);
        StringBuilder html = new StringBuilder();

        if (resInv == null || resInv.Count < 1)
        {
            html.AppendLine("<br />");
            html.AppendLine("<br />");
            html.AppendLine("<br />");
            html.AppendLine("По тази резервация все още няма плащане!");
            html.AppendLine("<br />");
            html.AppendLine("След извършване на плащане ще може да изтеглите фактура оттук!");
            return html.ToString();
        }

        html.AppendLine("<table class=\"ui-widget-content\" style=\"width: 500px; text-align: left;\">");
        html.AppendLine(" <thead>");
        html.AppendLine("  <tr>");
        html.AppendLine("   <th>&nbsp;</th>");
        html.AppendLine("   <th>Invoice No</th>");
        html.AppendLine("   <th>Invoice Date</th>");
        html.AppendLine("   <th>Amount</th>");
        html.AppendLine("   <th>Curr</th>");
        html.AppendLine("  </tr>");
        html.AppendLine(" </thead>");
        html.AppendLine(" <tbody>");

        foreach (ReservationInvoice row in resInv)
        {
            html.AppendLine("  <tr>");
            html.AppendFormat("    <td><input type=\"button\" value=\"{1}\" onclick=\"showInvoice({0})\"></td>", row.RecID, "Show");
            html.AppendFormat("    <td>{0}</td>", row.InvoiceNo);
            html.AppendFormat("    <td>{0}</td>", row.InvDate.HasValue ? row.InvDate.Value.ToShortDateString() : string.Empty);
            html.AppendFormat("    <td align=\"right\">{0}</td>", row.InvAmount.HasValue ? row.InvAmount.Value.ToString("#,###.00") : string.Empty);
            html.AppendFormat("    <td>{0}</td>", row.InvCurr);
            html.AppendLine("  </tr>");

        }
        html.AppendLine("  </tbody>");
        html.AppendLine("</table>");
        return html.ToString(); 
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string getInvoice(int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null || !recID.HasValue) return "";

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        object inv = new Payments().getReservationInvoice(UserData, recID.Value, ref errorMsg);
        string fileName = ResData.ResMain.ResNo + "_" + System.Guid.NewGuid() + ".PDF";
        
        try
        {
            System.IO.FileStream _FileStream = new System.IO.FileStream(AppDomain.CurrentDomain.BaseDirectory + "ACE\\" + fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            _FileStream.Write((byte[])inv, 0, ((byte[])inv).Length);
            _FileStream.Close();
            return WebRoot.BasePageRoot + "ACE/" + fileName;
        }
        catch (Exception _Exception)
        {
            return "";
        }
    }
}
