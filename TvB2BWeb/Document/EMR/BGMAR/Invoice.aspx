﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Invoice.aspx.cs" Inherits="EMR_Invoice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ETicket</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>
    <link href="../../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
            table td { height: 21px; }
        body { line-height: 1; font-family: Tahoma Times New Roman; font-size: 8pt; width: 100%; }
        .mainPage { width: 100%; }
        .clear { clear: both; height: 1px; }
        .turistdata { font-family: Tahoma Times New Roman; font-size: 10px; width: 250px; }
        .header { text-align: center; font-size: 16px; font-family: Tahoma Times New Roman; font-weight: bold; padding-top: 20px; z-index: 10; }
        .info { padding-left: 10px; padding-right: 10px; width: 682px; z-index: 20; }
        .FieldLabel { font-size: 10px; font-family: Tahoma Times New Roman; }
        .w200px { width: 230px; }
        .w123px { width: 150px; }
        .SeperatorDiv { height: 30px; }
        #noPrintDiv { width: 794px; text-align: center; }
        #custListDiv { text-align: left; font-size: 10pt; }

        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function showInvoice(recID)
        {
            var params = new Object();
            params.recID = recID;
            $.ajax({
                async: false,
                type: "POST",
                url: "Invoice.aspx/getInvoice",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {                        
                        location.href = '../../../ViewPDF.aspx' + "?url=" + encodeURIComponent(msg.d);
                    }
                },
                error: function (xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Invoice.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $("#invoiceList").html('');
                        $("#invoiceList").html(msg.d);
                    }
                },
                error: function (xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function () {
                getFormData();
            });
    </script>

</head>
<body>
    <form id="ticketForm" runat="server" class="ui-helper-reset">
        <input id="param1" type="hidden" value="" />
        <div id="noPrintDiv" class="ui-widget" style="width: 100%; text-align: center;">
            <div id="invoiceList">                
            </div>
        </div>
    </form>
</body>
</html>
