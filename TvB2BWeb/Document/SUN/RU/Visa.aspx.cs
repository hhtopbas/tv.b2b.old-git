﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using TvBo;
using System.Threading;
using System.Text;
using TvTools;
using System.Web.Script.Services;
using Newtonsoft.Json.Linq;
using System.IO;

public partial class Sunrise_Visa : BasePage
{    
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    internal static List<CodeName> getOccupation()
    {
        List<CodeName> list = new List<CodeName>();
        list.Add(new CodeName { Code = "26", Name = "ADMINIST. OFFICER" });
        list.Add(new CodeName { Code = "1", Name = "ARCHITECT" });
        list.Add(new CodeName { Code = "2", Name = "ARTISAN" });
        list.Add(new CodeName { Code = "3", Name = "ARTIST" });
        list.Add(new CodeName { Code = "4", Name = "BANKER" });
        list.Add(new CodeName { Code = "23", Name = "BLUE-COLLAR WORKER" });
        list.Add(new CodeName { Code = "33", Name = "CHEMIST" });
        list.Add(new CodeName { Code = "15", Name = "CIVIL SERVENT" });
        list.Add(new CodeName { Code = "6", Name = "CLERGYMAN. RELIGIOUS" });
        list.Add(new CodeName { Code = "10", Name = "COMPANY EXECUTIVE" });
        list.Add(new CodeName { Code = "16", Name = "COMPUTER EXPERT" });
        list.Add(new CodeName { Code = "9", Name = "DIPLOMAT" });
        list.Add(new CodeName { Code = "5", Name = "DRIVER, LORRY DRIVER" });
        list.Add(new CodeName { Code = "35", Name = "ELECTRONICS EXPERT" });
        list.Add(new CodeName { Code = "0", Name = "FARMER" });
        list.Add(new CodeName { Code = "22", Name = "FASHION, COSMETICS" });
        list.Add(new CodeName { Code = "27", Name = "JOURNALIST" });
        list.Add(new CodeName { Code = "30", Name = "LEGAL PROFESSION" });
        list.Add(new CodeName { Code = "19", Name = "MAGISTRATE" });
        list.Add(new CodeName { Code = "11", Name = "MANAGER" });
        list.Add(new CodeName { Code = "21", Name = "MEDICAL AND PARAMED." });
        list.Add(new CodeName { Code = "34", Name = "NO OCCUPATION" });
        list.Add(new CodeName { Code = "24", Name = "OTHER" });
        list.Add(new CodeName { Code = "25", Name = "OTHER TECHNICIAN" });
        list.Add(new CodeName { Code = "18", Name = "PENSIONER" });
        list.Add(new CodeName { Code = "28", Name = "POLICEMAN, SOLDIER" });
        list.Add(new CodeName { Code = "29", Name = "POLITICIAN" });
        list.Add(new CodeName { Code = "13", Name = "PRIVAT SERVENT" });
        list.Add(new CodeName { Code = "8", Name = "PROF. SPORTSMAN" });
        list.Add(new CodeName { Code = "17", Name = "SCIENTIF. RESEARCHER" });
        list.Add(new CodeName { Code = "20", Name = "SEAMAN" });
        list.Add(new CodeName { Code = "31", Name = "SELF-EMPLOYED" });
        list.Add(new CodeName { Code = "14", Name = "STUDENT, TRAINEE" });
        list.Add(new CodeName { Code = "32", Name = "TEACHER" });
        list.Add(new CodeName { Code = "7", Name = "TRADESMAN" });
        list.Add(new CodeName { Code = "12", Name = "WHITE-COLLAR WORKER" });
        return list;
    }

    public static bool checkCustVisaInfo(User UserData, ResDataRecord ResData, int? CustNo)
    {
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        return (resCust != null &&
                  !string.IsNullOrEmpty(resCust.Surname) &&
                  !string.IsNullOrEmpty(resCust.Name) &&
                  resCust.Birtday.HasValue &&
                  !string.IsNullOrEmpty(resCust.PassNo) &&
                  !string.IsNullOrEmpty(resCust.PassSerie) &&
                  !string.IsNullOrEmpty(resCust.PassGiven) &&
                  resCust.PassIssueDate.HasValue &&
                  resCust.PassExpDate.HasValue);
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var customersVisa = from H in ResData.ResService
                            join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                            join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                            join T in ResData.Title on C.Title equals T.TitleNo
                            where H.ServiceType == "VISA"
                            group Rc by new
                            {
                                ServiceID = H.RecID,
                                CustNo = Rc.CustNo,
                                Name = C.Name,
                                NameL = C.NameL,
                                SurName = C.Surname,
                                SurNameL = C.SurnameL,
                                TitleName = T.Code,
                                DocStat = Rc.DocStat
                            } into g
                            select new
                            {
                                ServiceID = g.Key.ServiceID,
                                CustNo = g.Key.CustNo,
                                Name = g.Key.Name,
                                NameL = g.Key.NameL,
                                SurName = g.Key.SurName,
                                SurNameL = g.Key.SurNameL,
                                TitleName = g.Key.TitleName,
                                DocStat = g.Key.DocStat
                            };
        if (customersVisa == null || customersVisa.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersVisa.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        object _documentFolder = new Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        string onClickBaseStr = "onclick=\"getCustForm({0}, {1});\" style=\"cursor: pointer; text-decoration: underline;\"";
        foreach (var row in customersVisa)
        {
            string onClick = string.Empty;
            i++;
            var query1 = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         where q2.ServiceType == "VISA" && q1.CustNo == row.CustNo //&& q1.DocStat != 1
                         select q1;
            Int16? DocStat = Conversion.getInt16OrNull(query1.Where(w => w.DocStat == 1).Count() > 0 ? 1 : 0);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";
            if (checkCustVisaInfo(UserData, ResData, row.CustNo))
            {
                if (row.DocStat != 1)
                {
                    onClick = string.Format(onClickBaseStr,
                                        row.CustNo,
                                        row.ServiceID);
                    sb.AppendFormat("<span {0}>{1}</span>",
                                    onClick,
                                    i.ToString() + ". " + Name);                    
                    /*
                    if (string.IsNullOrEmpty(row.SurNameL) || string.IsNullOrEmpty(row.NameL))
                    {
                        sb.AppendFormat("<br />");
                        sb.Append("<table cellpadding=\"0\" cellspacing=\"0\">");
                        sb.AppendFormat("<tr><td style=\"font-size:6pt;\">{0}</td><td style=\"font-size:6pt;\">{1}</td></tr>", "Surname (Local)", "Name (Local)");
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.AppendFormat("<span style=\"display:none;\"><input id=\"SurnameL_{0}\" type=\"text\" maxlength=\"30\" value=\"{1}\"/></span>", row.CustNo.ToString(), row.SurNameL);
                        sb.Append("</td>");
                        sb.Append("<td>");
                        sb.AppendFormat("<span style=\"display:none;\"><input id=\"NameL_{0}\" type=\"text\" maxlength=\"30\" value=\"{1}\"/></span>", row.CustNo.ToString(), row.NameL);
                        sb.Append("</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                    }
                    else
                    {
                        sb.AppendFormat("<br />{0} {1}", row.SurNameL, row.NameL);
                    }
                    */
                }
                else
                    sb.AppendFormat("<span>{0} ({1})</span>",
                                    i.ToString() + ". " + Name,
                                    HttpContext.GetGlobalResourceObject("LibraryResource", "Issued"));
            }
            else sb.AppendFormat("<span>{0} ({1})</span>",
                                    i.ToString() + ". " + Name,
                                    "Неoбхoдимo ввести дaту рoждения, серию и нoмер пaспoртa, дaты выдaчи и oкoнчaния срoкa действия пaспoртa, кем выдaн пaспoрт.");
            sb.Append(" <br />");
            sb.Append(" <br />");
        }
        sb.Append(" <br />");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<TvReport.htmlCodeData> getCustForm(int? CustNo, string SurnameL, string NameL)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return null;
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();

        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();

        ResCustRecord cust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        if (cust != null)
        {

            if (!string.IsNullOrEmpty(SurnameL) || !string.IsNullOrEmpty(NameL))
            {
                cust.SurnameL = SurnameL;
                cust.NameL = NameL;
                if (new Reservation().updateResCustLocalName(UserData, CustNo, SurnameL, NameL, ref errorMsg))
                {
                }
                else
                {
                }
            }
            ResCustVisaRecord resCustVisa = ResData.ResCustVisa.Find(f => f.CustNo == CustNo);

            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtSurname", TagName = "span", Data = cust.Surname });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtName", TagName = "span", Data = cust.Name });

            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtGender", TagName = "val", Data = cust.Title == 1 ? "M" : (cust.Title == 2 ? "F" : "") });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtBirthDate", TagName = "span", Data = cust.Birtday.HasValue ? cust.Birtday.Value.ToString("dd/MM/yyyy") : "" });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPassSerieNo", TagName = "span", Data = cust.PassSerie.ToString() + " " + cust.PassNo.ToString() });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPassIssueDate", TagName = "span", Data = cust.PassIssueDate.HasValue ? cust.PassIssueDate.Value.ToString("dd/MM/yyyy") : "" });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPassExpDate", TagName = "span", Data = cust.PassExpDate.HasValue ? cust.PassExpDate.Value.ToString("dd/MM/yyyy") : "" });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPassIssueBy", TagName = "span", Data = cust.PassGiven });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtIDNo", TagName = "span", Data = cust.IDNo });

            List<Location> countrys = new Locations().getLocationList(UserData.Market, LocationType.Country, null, null, null, null, ref errorMsg);
            var queryL = from q in countrys
                         select new { q.RecID, q.Name };
            string countryStr = Newtonsoft.Json.JsonConvert.SerializeObject(queryL);
            htmlData.Add(new TvReport.htmlCodeData { IdName = "Nationality", TagName = "combo", Data = countryStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "NationalityOriginal", TagName = "combo", Data = countryStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });
            var countryOfBirth = from q in countrys
                                 where !string.IsNullOrEmpty(q.CountryCode)
                                 select new { RecID = q.CountryCode, q.Name };
            string countryOfBirthStr = Newtonsoft.Json.JsonConvert.SerializeObject(countryOfBirth);
            htmlData.Add(new TvReport.htmlCodeData { IdName = "BirthCountry", TagName = "combo", Data = countryOfBirthStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });

            htmlData.Add(new TvReport.htmlCodeData { IdName = "VisaCountry1", TagName = "combo", Data = countryStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "VisaCountry2", TagName = "combo", Data = countryStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "VisaCountry3", TagName = "combo", Data = countryStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "VisaCountry4", TagName = "combo", Data = countryStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });

            htmlData.Add(new TvReport.htmlCodeData { IdName = "destCountry", TagName = "combo", Data = countryStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });
            //italy=249
            List<Location> italyCity = new Locations().getLocationList(UserData.Market, LocationType.City, null, 249, null, null, ref errorMsg);
            

            var queryItaly = from q in italyCity
                             select new { q.RecID, q.Name };
            string italyStr = Newtonsoft.Json.JsonConvert.SerializeObject(queryItaly);
            if ((new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg)) == 249)
                htmlData.Add(new TvReport.htmlCodeData { IdName = "firstEntryCountry", TagName = "combo", Data = italyStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });
            else
                htmlData.Add(new TvReport.htmlCodeData { IdName = "firstEntryCountry", TagName = "combo", Data = countryStr.Replace('{', '<').Replace('}', '>').Replace('"', '!') });
            //var query = from q in getOccupation()
            //            select new { RecID = q.Code, q.Name };
            //string OcuList = Newtonsoft.Json.JsonConvert.SerializeObject(query);

            //htmlData.Add(new TvReport.htmlCodeData { IdName = "OccupationIndex", TagName = "combo", Data = OcuList.Replace('{', '<').Replace('}', '>').Replace('"', '!') });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "section10Div", TagName = "display", Data = (cust.Age.HasValue && cust.Age.Value < 18) ? "block" : "none" });

            if (resCustVisa != null)
            {
                htmlData.Add(new TvReport.htmlCodeData { IdName = "BirthSurName", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.BirthSurname) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "BirthPlaced", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.PlaceOfBirth) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "BirthCountry", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.CountryOfBirth) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "Nationality", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.CitizenShip) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "NationalityOriginal", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.CitizenshipOriginal) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "LegalRepSurname", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.LegalRepresentativeSurname) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "LegalRepName", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.LegalRepresentativeName) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "LegalRepAddr", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.LegalRepresentativeAddress) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "LegalRepCitizenship", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.LegalRepresentativeCitizenship) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "MaritalStatus", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.Marital) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "MaritalStatusOther", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.MaritalRemark) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "Address", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.Address) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "Phone", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.Tel) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "MobTel", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.Mobile) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "OccupationIndex", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.Job) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "EmployerName", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.Employer) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "EmployerAddress", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.EmpAddress) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "EmployerPhone", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.EmpTel) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "PreviousShengen", TagName = "", Data = resCustVisa.VisaCount > 0 ? "1" : "0" });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "PSFrom1", TagName = "jQDateTime", Value = Conversion.ConvertToJulian(resCustVisa.ValidFrom) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "PSFrom2", TagName = "jQDateTime", Value = Conversion.ConvertToJulian(resCustVisa.ValidFrom2) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "PSFrom3", TagName = "jQDateTime", Value = Conversion.ConvertToJulian(resCustVisa.ValidFrom3) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "PSFrom4", TagName = "jQDateTime", Value = Conversion.ConvertToJulian(resCustVisa.ValidFrom4) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "PSTo1", TagName = "jQDateTime", Value = Conversion.ConvertToJulian(resCustVisa.ValidTill) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "PSTo2", TagName = "jQDateTime", Value = Conversion.ConvertToJulian(resCustVisa.ValidTill2) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "PSTo3", TagName = "jQDateTime", Value = Conversion.ConvertToJulian(resCustVisa.ValidTill3) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "PSTo4", TagName = "jQDateTime", Value = Conversion.ConvertToJulian(resCustVisa.ValidTill4) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "VisaCountry1", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.VisaCountry) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "VisaCountry2", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.VisaCountry1) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "VisaCountry3", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.VisaCountry2) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "VisaCountry4", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.VisaCountry3) });


                htmlData.Add(new TvReport.htmlCodeData { IdName = "OrgName", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.OrganizationName) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "OrgAddress", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.OrganizationAddress) });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "OrgPhone", TagName = "", Data = Conversion.getStrOrNull(resCustVisa.OrganizationPhone) });

                htmlData.Add(new TvReport.htmlCodeData { IdName = "destCountry", TagName = "", Data = resCustVisa.CountryOfDestination.ToString() });
                htmlData.Add(new TvReport.htmlCodeData { IdName = "firstEntryCountry", TagName = "", Data = resCustVisa.CountryOfFirstEntry.ToString() });
            }
        }
        return htmlData;
    }

    [WebMethod(EnableSession = true)]
    public static Sunrise_VisaReturn printVisa(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<TvReport.Coordinate> writeList = new List<TvReport.Coordinate>();
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        JObject _data = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(data.Replace('<', '{').Replace('>', '}').Replace('!', '"').Replace("\\!", "\""));

        string errorMsg = string.Empty;
        object _documentFolder = new Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("~") + "\\";

        List<VisaFormDataRecord> saveDataList = new List<VisaFormDataRecord>();
        List<JProperty> dataList = new List<JProperty>();
        JToken first = _data.First;
        JProperty token = (JProperty)first;
        dataList.Add(token);
        for (int i = 1; i < _data.Count; i++)
        {
            token = (JProperty)token.Next;
            dataList.Add(token);
        }

        List<Location> countrys = new Locations().getLocationList(UserData.Market, LocationType.Country, null, null, null, null, ref errorMsg);        

        saveDataList.Add(new VisaFormDataRecord { Name = "BirthSurname", Value = dataList.Find(f => string.Equals(f.Name, "BirthSurName")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "PlaceOfBirth", Value = dataList.Find(f => string.Equals(f.Name, "BirthPlaced")).Value.ToString() });        
        saveDataList.Add(new VisaFormDataRecord { Name = "CountryOfBirth", Value = dataList.Find(f => string.Equals(f.Name, "BirthCountry")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "CitizenShip", Value = dataList.Find(f => string.Equals(f.Name, "Nationality")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "CitizenshipOriginal", Value = dataList.Find(f => string.Equals(f.Name, "NationalityOriginal")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "LegalRepresentativeSurname", Value = dataList.Find(f => string.Equals(f.Name, "LegalRepSurname")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "LegalRepresentativeName", Value = dataList.Find(f => string.Equals(f.Name, "LegalRepName")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "LegalRepresentativeAddress", Value = dataList.Find(f => string.Equals(f.Name, "LegalRepAddr")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "LegalRepresentativeCitizenship", Value = dataList.Find(f => string.Equals(f.Name, "LegalRepCitizenship")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "GivenBy", Value = dataList.Find(f => string.Equals(f.Name, "txtPassIssueBy")).Value.ToString() });
        CodeName occupationList = getOccupation().Find(f1 => f1.Code == dataList.Find(f => string.Equals(f.Name, "OccupationIndex")).Value.ToString());
        string occupation = occupationList != null ? occupationList.Name : "";
        saveDataList.Add(new VisaFormDataRecord { Name = "Job", Value = dataList.Find(f => string.Equals(f.Name, "OccupationIndex")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "Employer", Value = dataList.Find(f => string.Equals(f.Name, "EmployerName")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "EmpAddress", Value = dataList.Find(f => string.Equals(f.Name, "EmployerAddress")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "EmpTel", Value = dataList.Find(f => string.Equals(f.Name, "EmployerPhone")).Value.ToString() });
        string maritalStatus = dataList.Find(f => string.Equals(f.Name, "MaritalStatus")).Value.ToString();
        saveDataList.Add(new VisaFormDataRecord { Name = "Marital", Value = dataList.Find(f => string.Equals(f.Name, "MaritalStatus")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "MaritalRemark", Value = dataList.Find(f => string.Equals(f.Name, "MaritalStatusOther")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "Sex", Value = dataList.Find(f => string.Equals(f.Name, "txtGender")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "Address", Value = dataList.Find(f => string.Equals(f.Name, "Address")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "Tel", Value = dataList.Find(f => string.Equals(f.Name, "Phone")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "Mobile", Value = dataList.Find(f => string.Equals(f.Name, "MobTel")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "DepCity", Value = ResData.ResMain.DepCityName });
        saveDataList.Add(new VisaFormDataRecord { Name = "ArrCity", Value = ResData.ResMain.ArrCityName });
        AgencyRecord agency = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        if (agency != null)
        {
            saveDataList.Add(new VisaFormDataRecord { Name = "AgencyName", Value = agency.Name });
            saveDataList.Add(new VisaFormDataRecord { Name = "AgencyAddress", Value = agency.Address + ", " + agency.AddrZip + ", " + agency.AddrCity });
            saveDataList.Add(new VisaFormDataRecord { Name = "AgencyTel", Value = agency.Phone1 });
            saveDataList.Add(new VisaFormDataRecord { Name = "AgencyFax", Value = agency.Fax1 });
        }
        int? visaCount = 0;
        saveDataList.Add(new VisaFormDataRecord { Name = "CountryOfDestination", Value = dataList.Find(f => string.Equals(f.Name, "destCountry")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "CountryOfFirstEntry", Value = dataList.Find(f => string.Equals(f.Name, "firstEntryCountry")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "OrganizationName", Value = dataList.Find(f => string.Equals(f.Name, "OrgName")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "OrganizationAddress", Value = dataList.Find(f => string.Equals(f.Name, "OrgAddress")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "OrganizationPhone", Value = dataList.Find(f => string.Equals(f.Name, "OrgPhone")).Value.ToString() });

        decimal? pSFrom1 = Conversion.getDecimalOrNull(dataList.Find(f => string.Equals(f.Name, "PSFrom1")).Value.ToString());
        DateTime? dtPSFrom1 = Conversion.ConvertFromJulian(pSFrom1.HasValue ? (long?)Math.Floor(pSFrom1.Value + 1) : null);
        visaCount += pSFrom1.HasValue ? 1 : 0;
        saveDataList.Add(new VisaFormDataRecord { Name = "ValidFrom", Value = dtPSFrom1 });

        decimal? pSFrom2 = Conversion.getDecimalOrNull(dataList.Find(f => string.Equals(f.Name, "PSFrom2")).Value.ToString());
        DateTime? dtPSFrom2 = Conversion.ConvertFromJulian(pSFrom2.HasValue ? (long?)Math.Floor(pSFrom2.Value + 1) : null);
        visaCount += pSFrom2.HasValue ? 1 : 0;
        saveDataList.Add(new VisaFormDataRecord { Name = "ValidFrom2", Value = dtPSFrom2 });

        decimal? pSFrom3 = Conversion.getDecimalOrNull(dataList.Find(f => string.Equals(f.Name, "PSFrom3")).Value.ToString());
        DateTime? dtPSFrom3 = Conversion.ConvertFromJulian(pSFrom3.HasValue ? (long?)Math.Floor(pSFrom3.Value + 1) : null);
        visaCount += pSFrom3.HasValue ? 1 : 0;
        saveDataList.Add(new VisaFormDataRecord { Name = "ValidFrom3", Value = dtPSFrom3 });

        decimal? pSFrom4 = Conversion.getDecimalOrNull(dataList.Find(f => string.Equals(f.Name, "PSFrom4")).Value.ToString());
        DateTime? dtPSFrom4 = Conversion.ConvertFromJulian(pSFrom4.HasValue ? (long?)Math.Floor(pSFrom4.Value + 1) : null);
        visaCount += pSFrom4.HasValue ? 1 : 0;
        saveDataList.Add(new VisaFormDataRecord { Name = "ValidFrom4", Value = dtPSFrom4 });

        decimal? pSTo1 = Conversion.getDecimalOrNull(dataList.Find(f => string.Equals(f.Name, "PSTo1")).Value.ToString());
        DateTime? dtPSTo1 = Conversion.ConvertFromJulian(pSTo1.HasValue ? (long?)Math.Floor(pSTo1.Value + 1) : null);
        visaCount += pSTo1.HasValue ? 1 : 0;
        saveDataList.Add(new VisaFormDataRecord { Name = "ValidTill", Value = dtPSTo1 });

        decimal? pSTo2 = Conversion.getDecimalOrNull(dataList.Find(f => string.Equals(f.Name, "PSTo2")).Value.ToString());
        DateTime? dtPSTo2 = Conversion.ConvertFromJulian(pSTo2.HasValue ? (long?)Math.Floor(pSTo2.Value + 1) : null);
        visaCount += pSTo2.HasValue ? 1 : 0;
        saveDataList.Add(new VisaFormDataRecord { Name = "ValidTill2", Value = dtPSTo2 });

        decimal? pSTo3 = Conversion.getDecimalOrNull(dataList.Find(f => string.Equals(f.Name, "PSTo3")).Value.ToString());
        DateTime? dtPSTo3 = Conversion.ConvertFromJulian(pSTo3.HasValue ? (long?)Math.Floor(pSTo3.Value + 1) : null);
        visaCount += pSTo3.HasValue ? 1 : 0;
        saveDataList.Add(new VisaFormDataRecord { Name = "ValidTill3", Value = dtPSTo3 });

        decimal? pSTo4 = Conversion.getDecimalOrNull(dataList.Find(f => string.Equals(f.Name, "PSTo4")).Value.ToString());
        DateTime? dtPSTo4 = Conversion.ConvertFromJulian(pSTo4.HasValue ? (long?)Math.Floor(pSTo4.Value + 1) : null);
        visaCount += pSTo4.HasValue ? 1 : 0;
        saveDataList.Add(new VisaFormDataRecord { Name = "ValidTill4", Value = dtPSTo4 });

        saveDataList.Add(new VisaFormDataRecord { Name = "VisaCountry", Value = dataList.Find(f => string.Equals(f.Name, "VisaCountry1")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "VisaCountry1", Value = dataList.Find(f => string.Equals(f.Name, "VisaCountry2")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "VisaCountry2", Value = dataList.Find(f => string.Equals(f.Name, "VisaCountry3")).Value.ToString() });
        saveDataList.Add(new VisaFormDataRecord { Name = "VisaCountry3", Value = dataList.Find(f => string.Equals(f.Name, "VisaCountry4")).Value.ToString() });
        //visaCount = saveDataList.Where(w => w.Name.Substring(0, 9) == "ValidFrom" && (!string.IsNullOrEmpty(w.Value.ToString()))).Count();
        saveDataList.Add(new VisaFormDataRecord { Name = "VisaCount", Value = visaCount.ToString() });

        int? _serviceID = Conversion.getInt32OrNull(dataList.Find(f => string.Equals(f.Name, "serviceID")).Value.ToString());
        int? _custNo = Conversion.getInt32OrNull(dataList.Find(f => string.Equals(f.Name, "custNo")).Value.ToString());

        if (new Reservation().updateVisaData(UserData, _custNo, saveDataList, ref ResData, ref errorMsg))
        {
            HttpContext.Current.Session["ResData"] = ResData;
            return new Sunrise_VisaReturn { localUrl = "OK", extrnUrl = "OK" };
        }
        else return new Sunrise_VisaReturn { localUrl = string.Empty, extrnUrl = string.Empty };
    }
}

public class Sunrise_VisaReturn
{
    public string localUrl { get; set; }
    public string extrnUrl { get; set; }
}