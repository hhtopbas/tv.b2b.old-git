﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;

public partial class Document_SUN_RU_TicketRef : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserData"] != null)
        {
            User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
            Thread.CurrentThread.CurrentCulture = UserData.Ci;
            Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(string resNo, string custNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string retVal = "\"retVal\":\"{0}\",\"retType\":\"{1}\"";
        if (HttpContext.Current.Session["ResData"] == null) return "{" + string.Format(retVal, "", "") + "}";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        int? CustNo = TvTools.Conversion.getInt32OrNull(custNo);
        OperatorRecord opr = new Common().getOperator(ResData.ResMain.Operator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, ref errorMsg);

        object _documentFolder = new Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? TvTools.Conversion.getStrOrNull(_documentFolder) : "";
        DocumentFolder += "/" + UserData.Market;
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        List<ResServiceRecord> query = (from q1 in ResData.ResCon
                                        join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                                        where q1.CustNo == CustNo && q2.ServiceType == "FLIGHT"
                                        orderby q2.BegDate, q2.SeqNo
                                        select q2).ToList<ResServiceRecord>();

        //FlightRecord firstFlight = new Flights().getFlight(UserData, query.FirstOrDefault().Service, ref errorMsg);
        decimal price = 0;
        decimal adlPrice = 0;
        decimal chdPrice = 0;
        decimal infPrice = 0;
        decimal? rate = new TvReport.SunReport.SunReport().getTicketRefRate(resNo, ResData.ResMain.Market, "RUR", ref errorMsg);
        string sClass = string.Empty;
        foreach (ResServiceRecord row in query)
        {
            sClass = row.FlgClass;
            TvReport.SunReport.TicketRefPriceRecord netPrices = new TvReport.SunReport.SunReport().getTicketRefValues(row.Supplier, row.Service, row.BegDate, row.FlgClass, row.AllotType, ref errorMsg);
            if (netPrices != null)
            {
                adlPrice = TvTools.Conversion.getDecimalOrNull(netPrices.NetPrice).HasValue ? TvTools.Conversion.getDecimalOrNull(netPrices.NetPrice).Value : 0;
                if (rate.HasValue)
                    adlPrice = adlPrice * rate.Value;
                chdPrice = TvTools.Conversion.getDecimalOrNull(netPrices.NetChd).HasValue ? TvTools.Conversion.getDecimalOrNull(netPrices.NetChd).Value : 0;
                if (rate.HasValue)
                    chdPrice = chdPrice * rate.Value;
                infPrice = TvTools.Conversion.getDecimalOrNull(netPrices.NetInf).HasValue ? TvTools.Conversion.getDecimalOrNull(netPrices.NetInf).Value : 0;
                if (rate.HasValue)
                    infPrice = infPrice * rate.Value;
            }
            if (resCust.Title > 7)
                price += infPrice;
            else if (resCust.Title > 5)
                price += chdPrice;
            else price += adlPrice;
        }
        string flgClass = string.Empty;
        List<FlightClassRecord> sClassList = new Flights().getFlightClass(UserData.Market, ref errorMsg);
        FlightClassRecord fClass = sClassList.Find(f => f.Class == sClass);
        if (fClass != null) flgClass = fClass.NameL;
        string numToWord = new TvTools.NumberToWordRU().CurrencyToText(price, "руб.", "коп.");
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtSurname", TagName = "span", Data = resCust != null ? resCust.Surname : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtName", TagName = "span", Data = resCust != null ? resCust.Name : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtSClass", TagName = "span", Data = flgClass });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDepCity1", TagName = "span", Data = ResData.ResMain.DepCityNameL });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtArrCity", TagName = "span", Data = ResData.ResMain.ArrCityNameL });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDepCity2", TagName = "span", Data = ResData.ResMain.DepCityNameL });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtBeginDate", TagName = "span", Data = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtEndDate", TagName = "span", Data = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCost", TagName = "span", Data = price.ToString("# ###.00") });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCostWord", TagName = "span", Data = numToWord });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "iIssueDate", TagName = "span", Data = DateTime.Today.ToShortDateString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "stampImg", TagName = "img", Data = WebRoot.BasePageRoot + DocumentFolder + "/stamp.png" });
        //htmlData.Add(new TvReport.htmlCodeData { IdName = "headerDiv", TagName = "css", Data = "background-image", Value="url(" + WebRoot.BasePageRoot + DocumentFolder + "/image002.gif)" });
        
        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }
}
