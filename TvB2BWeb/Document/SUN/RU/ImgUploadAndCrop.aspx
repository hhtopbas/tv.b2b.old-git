﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImgUploadAndCrop.aspx.cs"
    Inherits="ImgUploadAndCrop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Image UpLoad And Crop</title>
    <!-- no cache headers -->
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <%--<meta http-equiv="refresh" content="300" />--%>
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.imgareaselect.js" type="text/javascript"></script>

    <link href="../../../CSS/imgareaselect-default.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(document).ready(function() {
            if ($("#isFileSave").val() == "1" && $("#isImgCropted").val() == "0") {
                var ias = $('#imgCrop').imgAreaSelect({
                    instance: true,
                    onSelectEnd: storeCoords,
                    aspectRatio: "3:4"
                });
                var imgW = parseInt($('#imgCrop').width()) - 10;
                var imgH = parseInt($('#imgCrop').height()) - 10;
                var x1 = 1;
                var y1 = 1;
                var x2 = imgW;
                var y2 = imgW / 0.75;
                if (y2 > imgH) {
                    y2 = imgH;
                    x2 = imgH * 0.75;
                }
                ias.setSelection(x1, y1, x2, y2, true);
                ias.setOptions({ show: true });
                ias.update();
            }
            if ($('#isImgCropted').val() == '1') {
                self.parent.savedAndCropped(jQuery('#returnFileName').val());
            }
            else {

            }
        });

        function storeCoords(img, c) {
            $('#X').val(parseInt(c.x1));
            $('#X2').val(parseInt(c.x2));
            $('#Y').val(parseInt(c.y1));
            $('#Y2').val(parseInt(c.y2));
            $('#W').val(parseInt(c.width));
            $('#H').val(parseInt(c.height));
            $('#ImgWidth').val($('#imgCrop').width());
            $('#ImgHeight').val($('#imgCrop').height());
        };
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="pnlUpload" runat="server">
            <asp:FileUpload ID="Upload" runat="server" />
            <br />
            <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Upload" />
            <asp:Label ID="lblError" runat="server" Visible="false" />
        </asp:Panel>
        <asp:Panel ID="pnlCrop" runat="server" Visible="false">
            <div style="width: 400px; float: left;">
                <asp:Image ID="imgCrop" runat="server" Width="340px" ImageUrl="nophoto.jpg" BorderWidth="0" />
                <asp:HiddenField ID="X" runat="server" />
                <asp:HiddenField ID="X2" runat="server" />
                <asp:HiddenField ID="Y" runat="server" />
                <asp:HiddenField ID="Y2" runat="server" />
                <asp:HiddenField ID="W" runat="server" />
                <asp:HiddenField ID="OrgImgWidth" runat="server" />
                <asp:HiddenField ID="ImgWidth" runat="server" />
                <asp:HiddenField ID="H" runat="server" />
                <asp:HiddenField ID="OrgImgHeight" runat="server" />
                <asp:HiddenField ID="ImgHeight" runat="server" />
                <asp:HiddenField ID="returnFileName" runat="server" />
            </div>
            <div style="float: left; width: 150px;">
                <asp:Button ID="btnCrop" runat="server" Text="Crop and save" OnClick="btnCrop_Click" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlCropped" runat="server" Visible="false">
            <asp:Image ID="imgCropped" runat="server" />
        </asp:Panel>
    </div>
    <asp:HiddenField ID="isFileSave" runat="server" Value="0" />
    <asp:HiddenField ID="isImgCropted" runat="server" Value="0" />
    </form>
</body>
</html>
