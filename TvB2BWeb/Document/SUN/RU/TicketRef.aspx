﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TicketRef.aspx.cs" Inherits="Document_SUN_RU_TicketRef" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <title>Ticket Referance</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <style type="text/css">
        .mainPage { width: 794px; background-color: #FFF; }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData(resNo, custNo) {
            $.ajax({
                async: false,
                type: "POST",
                url: "TicketRef.aspx/getFormData",
                data: '{"resNo":"' + resNo + '","custNo":"' + custNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                case 'css':
                                    $("#" + this.IdName).css(this.Data, this.Value);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var custNo = $.query.get('docName');
                getFormData(resNo, custNo);
                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
            });
    </script>

</head>
<body>
    <form id="TicketRefForm" runat="server">
    <input id="param1" type="hidden" value="" />
        <div class="mainPage">
            <div id="headerDiv" style="width: 100%; font-family: Verdana,Arial,sans-serif;">
                <div style="text-align: right; color: #FFF; font-size: 7pt;">
                    <img alt="" src="ST.png" style="width: 794px;" />
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <p style="text-align: right; padding-right: 10px;">
                    Выдана по месту требования
                </p>
                <br />
                <br />
                <br />
                <br />
                <p style="text-align: center;">
                    СПРАВКА
                </p>
                <p style="text-align: left; padding-left: 100px; padding-right: 10px;">
                    Настоящая справка дана о том, что себестоимость авиабилета класса&nbsp;<span id="txtSClass"></span>&nbsp;для&nbsp;туриста&nbsp;
                <span id="txtSurname"></span>&nbsp;<span id="txtName"></span>&nbsp;по маршруту&nbsp;<span
                    id="txtDepCity1"> </span>–<span id="txtArrCity"></span>–<span id="txtDepCity2"></span>&nbsp;на
                период с&nbsp; <span id="txtBeginDate"></span>&nbsp;по&nbsp;<span id="txtEndDate"></span>&nbsp;составила&nbsp;<span
                    id="txtCost"> </span>&nbsp;(<span id="txtCostWord"></span>).
                </p>
                <p style="text-align: left; padding-left: 100px; padding-right: 10px;">
                    В случае если итоговая стоимость авиаперелёта превышает стоимость тура в данной
                справке, то данный тур реализован в соответствии с акционным предложением.
                </p>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <table width="100%">
                    <tr>
                        <td style="width: 50%;" align="center" valign="top">
                            <br />
                            Руководитель отдела продаж
                        </td>
                        <td align="center">&nbsp;<img id="stampImg" alt="" src="" style="width: 227px; height: 159px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; height: 40px;" align="center">&nbsp;
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 50%;" align="center">
                            <span id="iIssueDate"></span>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
