﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using SD = System.Drawing;
using System.Drawing.Drawing2D;

public partial class ImgUploadAndCrop : System.Web.UI.Page
{
    String pathLoaded = HttpContext.Current.Request.PhysicalApplicationPath + "CustPicCache\\Loaded\\";
    String path = HttpContext.Current.Request.PhysicalApplicationPath + "CustPicCache\\";
    String SaveFileName = HttpContext.Current.Request.QueryString["custNo"];
    String FileExtension = string.Empty;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            imgCrop.ImageUrl = "~/Document/ANX/ANEXRU/nophoto.jpg?ver=" + DateTime.Now.ToString("ddMMyyyyHHmmss"); 
            System.IO.DirectoryInfo dirTmp = new System.IO.DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "CustPicCache\\Loaded");
            System.IO.FileInfo[] filesTmp = dirTmp.GetFiles("CustNo_" + SaveFileName.ToString() + ".*");
            foreach (var row in filesTmp)
                if (File.Exists(row.FullName))
                    File.Delete(row.FullName);
        }
    }


    protected void btnUpload_Click(object sender, EventArgs e)
    {
        Boolean FileOK = false;
        Boolean FileSaved = false;
        if (Upload.HasFile)
        {
            ViewState["WorkingImage"] = Upload.FileName;
            FileExtension = Path.GetExtension(ViewState["WorkingImage"].ToString()).ToLower();
            String[] allowedExtensions = { ".jpeg", ".jpg" };
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (FileExtension == allowedExtensions[i])
                {
                    FileOK = true;
                }
            }
        }
        if (FileOK)
        {
            try
            {
                if (Upload.PostedFile.InputStream.Length < 153600 && Upload.PostedFile.InputStream.Length > 0)
                {                    
                    if (File.Exists(pathLoaded + "CustNo_" + SaveFileName + FileExtension))
                        File.Delete(pathLoaded + "CustNo_" + SaveFileName + FileExtension);

                    var file = Upload.PostedFile.InputStream;
                    System.Drawing.Bitmap img = new System.Drawing.Bitmap(file);
                    if (img.HorizontalResolution > 90 && img.HorizontalResolution <= 305 && img.VerticalResolution > 90 && img.VerticalResolution <= 305)
                    {
                        if (img.Width < 240 || img.Height < 320)
                        {
                            lblError.Text = "Incorrect image dimension.(min. 240x320 pixel)";
                            lblError.Visible = true;
                            FileSaved = false;
                        }
                        else
                        {
                            OrgImgHeight.Value = img.Height.ToString();
                            OrgImgWidth.Value = img.Width.ToString();                            
                            img.Save(pathLoaded + "CustNo_" + SaveFileName + FileExtension, System.Drawing.Imaging.ImageFormat.Jpeg);
                            ViewState["WorkingImage"] = "CustNo_" + SaveFileName + FileExtension;
                            FileSaved = true;
                        }
                    }
                    else
                    {
                        lblError.Text = "Incorrect image resolution.(min. 90dpi - max. 305dpi)";
                        lblError.Visible = true;
                        FileSaved = false;
                    }
                    /*
                    img.Save(pathLoaded + "CustNo_" + SaveFileName + FileExtension, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //Upload.PostedFile.SaveAs(pathLoaded + "CustNo_" + SaveFileName + FileExtension);
                    string errorMsg = string.Empty;
                    if (imgCheck("CustNo_" + SaveFileName + FileExtension, ref errorMsg))
                    {
                        ViewState["WorkingImage"] = "CustNo_" + SaveFileName + FileExtension;
                        FileSaved = true;
                    }
                    else
                    {

                        lblError.Text = errorMsg;
                        lblError.Visible = true;
                        FileSaved = false;
                    }
                    */
                }
                else
                {

                    lblError.Text = "File could not be uploaded. Incorrect image size.";
                    lblError.Visible = true;
                    FileSaved = false;
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "File could not be uploaded." + ex.Message.ToString();
                lblError.Visible = true;
                FileSaved = false;
            }
        }
        else
        {
            lblError.Text = "Cannot accept files of this type.";
            lblError.Visible = true;
        }
        if (FileSaved)
        {
            pnlUpload.Visible = false;
            pnlCrop.Visible = true;
            imgCrop.ImageUrl = "";
            imgCrop.ImageUrl = "~/CustPicCache/Loaded/" + ViewState["WorkingImage"].ToString() +"?ver=" + DateTime.Now.ToString("ddMMyyyyHHmmss"); 
            isFileSave.Value = "1";
            isImgCropted.Value = "0";
        }
        else
        {
            if (File.Exists(pathLoaded + "CustNo_" + SaveFileName + FileExtension))
                File.Delete(pathLoaded + "CustNo_" + SaveFileName + FileExtension);
        }
    }

    internal bool imgCheck(string fileName, ref string errorMsg)
    {
        System.Drawing.Image image = null;
        string fileNamePath = System.Web.HttpContext.Current.Server.MapPath("~") + "\\CustPicCache\\Loaded\\" + fileName;
        try
        {
            image = System.Drawing.Image.FromFile(fileNamePath);
            decimal scala = (decimal)image.Width / (decimal)image.Height;
            OrgImgHeight.Value = image.Height.ToString();
            OrgImgWidth.Value = image.Width.ToString();            
            if (image.RawFormat.Guid != System.Drawing.Imaging.ImageFormat.Jpeg.Guid)
            {
                errorMsg = "Please, only jpeg format.";
                return false;
            }
            if (image.HorizontalResolution > 90 && image.HorizontalResolution <= 305 && image.VerticalResolution > 90 && image.VerticalResolution <= 305)
            {
                if (image.Width >= 240 && image.Height >= 320)
                    return true;
                else
                {
                    errorMsg = "Incorrect image dimension.";
                    return false;
                }
            }
            else
            {
                errorMsg = "Incorrect image resolution.";
                return false;
            }
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
            return false;
        }
        finally
        {
            image.Dispose();
        }
    }

    protected void btnCrop_Click(object sender, EventArgs e)
    {
        string ImageName = ViewState["WorkingImage"].ToString();
        
        returnFileName.Value = TvBo.WebRoot.BasePageRoot + "CustPicCache/" + ImageName;
        float xScala = 1.0f;
        float yScala = 1.0f;
        if (string.IsNullOrEmpty(W.Value) ||
            string.IsNullOrEmpty(H.Value) ||
            string.IsNullOrEmpty(X.Value) ||
            string.IsNullOrEmpty(X2.Value) ||
            string.IsNullOrEmpty(Y.Value) ||
            string.IsNullOrEmpty(Y2.Value))
        {
            System.Drawing.Image image = null;
            string fileNamePath = System.Web.HttpContext.Current.Server.MapPath("~") + "\\CustPicCache\\Loaded\\" + ImageName;
            try
            {
                image = System.Drawing.Image.FromFile(fileNamePath);
                if (image.Width >= 240 && image.Height >= 320)
                {
                    if (image.Width > image.Height)
                    {
                        W.Value = Convert.ToInt32(image.Height * 3 / 4).ToString();
                        H.Value = image.Height.ToString();
                        X.Value = "0";
                        Y.Value = "0";
                        X2.Value = W.Value;
                        Y2.Value = image.Height.ToString();
                    }
                    else
                    {
                        W.Value = image.Width.ToString();
                        H.Value = Convert.ToInt32(image.Width * 3 / 4).ToString();
                        X.Value = "0";
                        Y.Value = "0";
                        X2.Value = image.Width.ToString();
                        Y2.Value = H.Value;
                    }
                }
                else
                {
                    W.Value = image.Width.ToString();
                    H.Value = image.Height.ToString();
                    X.Value = "0";
                    Y.Value = "0";
                    X2.Value = image.Width.ToString();
                    Y2.Value = image.Height.ToString();
                }
            }
            catch (Exception Ex)
            {
                throw (Ex);
            }
            finally
            {
                image.Dispose();
            }
        }
        else
        {
            xScala = Convert.ToSingle(OrgImgWidth.Value) / Convert.ToSingle(ImgWidth.Value);
            yScala = Convert.ToSingle(OrgImgHeight.Value) / Convert.ToSingle(ImgHeight.Value);
        }
        xScala = 1.0f;
        yScala = 1.0f;
        int w = Convert.ToInt32(Convert.ToSingle(W.Value) * xScala);
        int h = Convert.ToInt32(Convert.ToSingle(H.Value) * yScala);
        int x = Convert.ToInt32(Convert.ToSingle(X.Value) * xScala);
        int x2 = Convert.ToInt32(Convert.ToSingle(X2.Value) * xScala);
        int y = Convert.ToInt32(Convert.ToSingle(Y.Value) * yScala);
        int y2 = Convert.ToInt32(Convert.ToSingle(Y2.Value) * yScala);

        if (TvBo.ImageResize.CropImageV2(x, y, x2, y2, w, h, TvTools.Conversion.getDecimalOrNull(ImgWidth.Value), ImageName, ImageName))
        {
            isImgCropted.Value = "1";
            returnFileName.Value = TvBo.WebRoot.BasePageRoot + "CustPicCache/" + /*"CustNo_" +*/ ImageName;
        }

    }

    protected void btnCrop1_Click(object sender, EventArgs e)
    {
        string ImageName = Session["WorkingImage"].ToString();
        int w = Convert.ToInt32(W.Value);
        int h = Convert.ToInt32(H.Value);
        int x = Convert.ToInt32(X.Value);
        int y = Convert.ToInt32(Y.Value);
        byte[] CropImage = Crop(path + ImageName, w, h, x, y);
        using (MemoryStream ms = new MemoryStream(CropImage, 0, CropImage.Length))
        {
            ms.Write(CropImage, 0, CropImage.Length);
            using (SD.Image CroppedImage = SD.Image.FromStream(ms, true))
            {
                string SaveTo = path + "crop" + ImageName;
                CroppedImage.Save(SaveTo, CroppedImage.RawFormat);
                pnlCrop.Visible = false;
                pnlCropped.Visible = true;
                imgCropped.ImageUrl = "images/crop" + ImageName;
            }
        }
    }

    static byte[] Crop(string Img, int Width, int Height, int X, int Y)
    {
        try
        {
            using (SD.Image OriginalImage = SD.Image.FromFile(Img))
            {
                using (SD.Bitmap bmp = new SD.Bitmap(Width, Height))
                {
                    bmp.SetResolution(OriginalImage.HorizontalResolution, OriginalImage.VerticalResolution);
                    using (SD.Graphics Graphic = SD.Graphics.FromImage(bmp))
                    {
                        Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                        Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        Graphic.DrawImage(OriginalImage, new SD.Rectangle(0, 0, Width, Height), X, Y, Width, Height, SD.GraphicsUnit.Pixel);
                        MemoryStream ms = new MemoryStream();
                        bmp.Save(ms, OriginalImage.RawFormat);
                        return ms.GetBuffer();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw (Ex);
        }
    }

}

public class imageSize
{
    public imageSize()
    {
    }
    public int w { get; set; }
    public int h { get; set; }
    public int x { get; set; }
    public int x2 { get; set; }
    public int y { get; set; }
    public int y2 { get; set; }
}
