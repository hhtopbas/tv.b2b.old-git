﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvBo;

public partial class Safiran_Contract : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fa-IR");
        Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fa-IR");
    }

    public static string getCustomers(User UserData, ResDataRecord ResData)
    {
        Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fa-IR");
        Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fa-IR");

        string errorMsg = string.Empty;
        var query = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).FirstOrDefault();        
        HotelRecord hotel = new HotelRecord();
        if (query != null)
            hotel = new Hotels().getHotelDetail(UserData, query.Service, ref errorMsg);        
        if (ResData == null && ResData.ResCust == null) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<table width=\"100%\"  cellpadding=\"0\" cellspacing=\"0\" >");        
        foreach (ResCustRecord row in ResData.ResCust)
        {
            sb.Append(" <tr>");
            sb.Append("  <td style=\"font-family: Tahoma; font-size: small; white-space:nowrap; text-align:center;\" >");
            sb.Append(hotel != null ? hotel.Category + "/" + hotel.Name : "&nbsp;");
            sb.Append("  </td>");
            sb.Append("     <td style=\"font-family: Tahoma; font-size: small; white-space:nowrap; text-align:center; width:100px;\" >");
            sb.Append(row.ppPasPayable.HasValue ? row.ppPasPayable.Value.ToString("#.") : "&nbsp;");
            sb.Append("     </td>");
            sb.Append("     <td style=\"font-family: Tahoma; font-size: small; white-space:nowrap; text-align:center;width:80px;\" >");
            sb.Append(row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "&nbsp;");
            sb.Append("     </td>");
            sb.Append("     <td style=\"font-family: Tahoma; font-size: small; white-space:nowrap; text-align:center; width:260px;\" >");
            sb.Append(row.Surname + " " + row.Name);
            sb.Append("     </td>");            
            sb.Append(" </tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }
    
    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        //Thread.CurrentThread.CurrentCulture = UserData.Ci;
        //Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fa-IR");
        Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fa-IR");
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();        
        ResMainRecord resMain = ResData.ResMain;
        OperatorRecord opr = new Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);        
        ResCustRecord leaderCustNo = ResData.ResCust.Find(w => string.Equals(w.Leader, "Y"));
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == leaderCustNo.CustNo);
        string leaderInfo = string.Empty;
        if (_leader != null) leaderInfo = !Equals(_leader.ContactAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip;
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });        
        htmlData.Add(new TvReport.htmlCodeData { IdName = "ResNo", TagName = "span", Data = ResData.ResMain.ResNo.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "ResDate", TagName = "span", Data = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtName", TagName = "span", Data = leaderCustNo.Surname + " " + leaderCustNo.Name });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtBirtday", TagName = "span", Data = leaderCustNo.Birtday.HasValue ? leaderCustNo.Birtday.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAddr", TagName = "span", Data = leaderInfo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtMobtel", TagName = "span", Data = _leader.MobTel.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAdult", TagName = "span", Data = ResData.ResMain.Adult.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtNight", TagName = "span", Data = ResData.ResMain.Days.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResBegDate", TagName = "span", Data = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResEndDate", TagName = "span", Data = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtArrCity", TagName = "span", Data = ResData.ResMain.ArrCityName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridTourist", TagName = "div", Data = getCustomers(UserData, ResData).Replace('"', '!') });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "AmountByBaseCur", TagName = "span", Data = "واحد پول ایران" });        
        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }
}
