﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contract.aspx.cs" Inherits="Safiran_Contract" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--<meta http-equiv="Content-Type" content="text/html; charset=windows-1256" />--%>
    <title>Contrat</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Arial; font-size: 8pt; width: 750px; }
        .clearfix:after { content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0; }
        .clear { clear: both; height: 1px; }
        .mainPage { width: 748px; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
        p.MsoNormal { margin-top: 0cm; margin-right: 0cm; margin-bottom: 10.0pt; margin-left: 0cm; line-height: 115%; font-size: 11.0pt; font-family: "Calibri" , "sans-serif"; }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Contract.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    if (this.AfterInsertShow && this.Data != '')
                                        $("#" + this.IdName + "Div").show();
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function recursiveReplace(node) {
            if (node.nodeType == 3) { // text node
                node.nodeValue = node.nodeValue.replace(/1/g, '۱');
                node.nodeValue = node.nodeValue.replace(/2/g, "۲");
                node.nodeValue = node.nodeValue.replace(/3/g, "۳");
                node.nodeValue = node.nodeValue.replace(/4/g, "۴");
                node.nodeValue = node.nodeValue.replace(/5/g, "۵");
                node.nodeValue = node.nodeValue.replace(/6/g, "۶");
                node.nodeValue = node.nodeValue.replace(/7/g, "۷");
                node.nodeValue = node.nodeValue.replace(/8/g, "۸");
                node.nodeValue = node.nodeValue.replace(/9/g, "۹");
                node.nodeValue = node.nodeValue.replace(/0/g, "۰");
                //۱۲۳۴۵۶۷۸۹۰
            } else if (node.nodeType == 1) { // element
                $(node).contents().each(function() {
                    recursiveReplace(this);
                });
            }
        }

        $(document).ready(
            function() {
                getFormData();
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var docName = $.query.get('docName');
                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
                var bottomPage = document.getElementById('bottomPage');
                var topPage = document.getElementById('topPage');
                recursiveReplace(bottomPage);
                recursiveReplace(topPage);
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="mainPage">
        <div style="text-align:right;">
            <img alt="" src="mahbalLogo.jpg" />
        </div>
        <br />
        <br />
        <div id="topPage" style="width: 100%; font-size: 11pt;">           
            <p style="direction: rtl;">
                <span style="direction: rtl;">شماره ثبت: </span><span dir="rtl" style="font-family: Arial, Helvetica, sans-serif">
                    216939</span><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                &nbsp;&nbsp;&nbsp;&nbsp;بسمه تعالی&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="direction: rtl;">
                    
                شماره&nbsp; :</span><span id="ResNo" style="direction: ltr;">[ResNo]</span>
            </p>
            <p>
                &nbsp;</p>
            <p style="direction: rtl;">
                <span style="direction: rtl;">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                شرکت خدمات مسافرت هوایی و جهانگردی ماه بال پرواز&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                تاريخ :</span><span id="ResDate" style="direction: ltr;">[ResDate]</span>
            </p>
            <p>
                &nbsp;</p>
            <p style="text-align: center; direction: rtl;">
                قرارداد گشت های خارج از کشور</p>
            <p style="direction: rtl;">
                &nbsp;</p>
            <p style="direction: rtl;">
                این قرارداد بین خانم / آقای&nbsp; <span id="txtName" style="direction: ltr;">[Name]</span>
                <span style="direction: rtl;">متولد</span>&nbsp; <span id="txtBirtday" style="direction: ltr;">
                    [Birtday]</span>&nbsp; <span style="direction: rtl;">‫نشانی: </span>
            </p>
            <p>
                &nbsp;</p>
            <p style="direction: rtl;">
                <span id="txtAddr" style="direction: ltr;">[LeaderAddr]</span><span style="direction: ltr;">&nbsp;&nbsp;&nbsp;
                </span><span style="direction: rtl;">تلفن محل کار / منزل</span>&nbsp; <span id="txtMobtel"
                    style="direction: ltr;">[LeaderMobTel]</span>
            </p>
            <p>
                &nbsp;</p>
            <p style="direction: rtl;">
                منفردا یا به نمایندگی تام الختیار از جانب افراد زیر جمعا به تعداد <span id="txtAdult"
                    style="direction: ltr;">[Adult]</span>&nbsp;&nbsp; <span style="direction: rtl;">نفر
                        که از این پس مسافر</span>&nbsp; <span style="direction: rtl;">نامیده می شود از یک طرف
                            و دفتر خدمات </span>
            </p>
            <p>
                &nbsp;</p>
            <p style="direction: rtl;">
                <span style="direction: rtl">مسافرتي و گردشگري ماه بال پرواز با شماره مجوز</span>&nbsp;126-2110&nbsp;
                که ازاین پس کارگزار نامیده می شود از طرف دیگر به منظور استفاده از برنامه&nbsp;
            </p>
            <p>
                &nbsp;</p>
            <p style="direction: rtl;">
                <span style="direction: rtl">گشت</span>&nbsp;<span id="txtNight" style="direction: ltr;">[Night]</span>&nbsp;<span
                    style="direction: rtl">روزه با وسیله نقلیه </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;<span style="direction: rtl">از تاریخ </span>&nbsp;<span id="txtResBegDate"
                    style="direction: ltr;">[ResBegDate]</span>&nbsp;<span style="direction: rtl">تا تاریخ</span>&nbsp;
                <span id="txtResEndDate" style="direction: ltr;">[ResEndDate]</span>&nbsp; <span
                    style="direction: rtl">به مقصد</span>&nbsp;<span id="txtArrCity" style="direction: ltr;">
                        [ArrCity]</span>
            </p>
            <p>
                &nbsp;</p>
            <p style="direction: rtl;">
                <span style="direction: rtl;">جمعا به مبلغ هزینه ریالی</span>&nbsp; <span id="AmountByBaseCur"
                    style="direction: ltr;">[AmountByBaseCur]</span>&nbsp; <span style="direction: rtl;">
                        که نرخ و برنامه آن به </span>&nbsp; <span style="direction: rtl;">مسافر ارایه گردیده
                            است منعقد می گردد</span>
            </p>
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p>
                &nbsp;</p>
            <p style="direction: rtl; text-align: center;">
                مشخصات مسافر و قیمت گشت</p>
        </div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-weight: bold; direction: rtl; text-align: center; height:20px; ">
                   <span style="direction: rtl;">درجه هتل / نام هتل</span></td>
                <td style="font-weight: bold; direction: rtl; text-align: center; height:20px; width:100px;">
                    <span style="direction: rtl;">مبلغ</span></td>
                <td style="font-weight: bold; direction: rtl; text-align: center; height:20px; width:80px;">
                    <span style="direction: rtl;">تاریخ تولد</span></td>
                <td style="font-weight: bold; direction: rtl; text-align: center; height:20px; width:260px;">
                    <span style="direction: rtl;">نام و نام خانوادگی</span></td>
            </tr>
        </table>
        <div id="gridTourist">
        </div>
        <br />
        <br />
        <div id="bottomPage" style="direction: rtl;">
            <p style="direction: rtl;">
                :موضوع قرارداد
            </p>
            <p style="direction: rtl;">
                موضوع این قرارداد تعیین حدود و وظایف و مسئولیت های طرفین و همچنین نرخ گشت برای انجام
                مسافرت با گشت های دسته جمعی به شرح زیر می باشد
            </p>
            <p style="direction: rtl;">
                ماده 1 - چنانچه تور به حد نصاب مورد نظر(حداقل15 نفر ) نرسد کارگزار مجاز به ابطال
                آن، اعلام به مسافر یک هفته قبل از تاریخ اجرای تور و استرداد وجوه دریافتی از مسافر
                می باشد
            </p>
            <p style="direction: rtl;">
                ماده 2 - ساعت تحویل اطاقهای هتل به هنگام ورود ساعت2 بعدازظهر و تخلیه ساعت12 ظهر
                بوده که مسافر موظف به رعایت آن می باشد
            </p>
            <p style="direction: rtl;">
                ماده 3 - در اطاق های دوتخته، تخت سوم از نوع سفری بوده که موقتا در اطاق دوتخته گذاشته
                می شود
            </p>
            <p style="direction: rtl;">
                ماده 4 - مسافرین انفرادی به هنگام ثبت نام مشمول مقررات و نرخ استفاده از اتاق یک
                تخته می باشند
            </p>
            <p style="direction: rtl;">
                ماده 5 - در خصوص امور مربوط به پروازهای هوایی با توجه به مقررات حمل و نقل هوایی
                که در تاریخ12 اکتبر1929 در ورشو به امضا رسیده است و نیز اصلاحیه پیمان مزبور که در
                تاریخ28 سپتامبر1995 در لاهه تصویب گردیده و همچنین مقررات مربوط به سازمان هواپیمایی
                کشوری عمل خواهد شد
            </p>
            <p style="direction: rtl;">
                ماده 6 - در صورتیکه مسافر به هر دلیل برای سایر مسافرین یا سرپرست گروه و یا راهنمایان
                محلی و کلاکسانی که در اجرای گشت نقش داشته مزاحمتی ایجاد نماید و یا مرتکب اعمال خلاف
                شئونات جمهوری اسلامی ایران و یا کشور مربوطه گردد، سرپرست گروه موظف است بر حسب مورد
                تصمیم لازم را در خصوص این مسافر اخذ نموده و مراتب را جهت ضبط موضوع به کارگزار مجری
                گزارش نماید
            </p>
            <p style="direction: rtl;">
                ماده 7 - در صورت بروز هرگونه اختلاف بین طرفین قرارداد سازمان میراث فرهنگی، صنایع
                دستی و گردشگری، حکم مرضی الطرفین بوده و آراء صادره مورد توافق طرفین خواهد بود
            </p>
            <br />
            <p style="direction: rtl;">
                نرخ و شرایط ابطال گشت اعم از چارتر و غیرچارتر
            </p>
            <p style="direction: rtl;">
                ماده 8 - در صورت انصراف کتبی مسافر از همراهی تور تا یکماه قبل از پرواز معادل20 ٪
                قیمت کل تور و از یکماه تا سه هفته قبل از پرواز معادل 30٪ قیمت کل تور و از سه هفته
                تا72 ساعت قبل از پرواز معادل50 ٪ قیمت کل تور و کمتر از72 ساعت قبل از پرواز معادل70٪
                قیمت کل تور به عنوان جریمه از کل مبلغ گشت کسر و مابقی وجوه به مسافر پرداخت خواهد
                شد
            </p>
            <p style="direction: rtl;">
                تبصره 1- در خصوص ثبت نام گروهی فقط فردی که به نمایندگی از سوی مسافرین اقدام به ثبت
                نام و عقدقرارداد نموده مجاز به مراجعه به کارگزار جهت پیگیری ابطال و یا دریافت وجوه
                پرداختی می باشد و سایر مسافرین در این خصوص مجاز به مراجعه به کارگزار نمی باشند
            </p>
            <p style="direction: rtl;">
                تبصره2 - کارگزار موظف است تاریخ و شماره انصراف کتبی مسافر را در دفاتر رسمی خود ثبت
                نماید
            </p>
            <br />
            <p style="direction: rtl;">
                تعهدات مسافر
            </p>
            <p style="direction: rtl;">
                ماده 9- رعایت شئونات اسلامی در طول سفر از سوی مسافر الزامی است
            </p>
            <p style="direction: rtl;">
                ماده10 - حفظ اموال و مدارک شخصی بعهده مسافر بوده و در صورت مفقود شدن آن هیچ نوع
                مسئولیتی متوجه مدیر گشت و یا کارگزار نخواهد بود
            </p>
            <p style="direction: rtl;">
                ماده 11 - چنانچه صدور ویزا و تحویل گذرنامه توسط برخی سفارتخانه ها در ساعت آخر قبل
                از شروع سفر صورت بگیرد مسافر بایستی مقدمات سفر خود را قبلا آماده نموده باشد .</p>
            <p style="direction: rtl;">
                ماده 12- مسافر موظف است به هنگام ثبت نام معادل50 ٪ هزینه کل تور را در قبال اخذ رسید
                پرداخت و تا یک ماه قبل از پرواز الباقی هزینه تور را تسویه نماید تبصره - تامین هزینه
                ارزی به طرق قانونی بعهده مسافرین می باشد
            </p>
            <p style="direction: rtl;">
                ماده 13 - ارائه ضامن معتبر بر حسب نیاز گشت هایی که احتیاج به معرفی ضامن و یا ارائه
                ضمانت نامه معتبر دارد، از سوی مسافر الزامی بوده و در صورت درخواست سفارتخانه مربوطه
                جهت انجام مصاحبه با مسافر و یا انگشت نگاری به منظور صدور روادید، مسافر باید با هماهنگی
                کارگزار در کنسولگری سفارتخانه ذیربط حضور یافته و در مصاحبه شرکت نماید و در صورت
                عدم حضور مسافر در وقت تعیین شده و یا عدم ارائه مدارک کافی درخواستی از سوی سفارتخانه
                که منجر به عدم دریافت روادید می گردد، جرائم منعقده در ماده8 این قرارداد اخذ خواهد
                شد تبصره - در صورتیکه مسافر کلیه مدارک مورد نیاز را به موقع تسلیم و بر اساس صورت
                نیز در وقت مقرر در سفارتخانه مربوطه حاضر و مراحل مختلف مصاحبه را انجام دهد ولی به
                هر علت، سفارت از صدور روادید وی خودداری نماید، مسافر حق هیچگونه اعتراضی نداشته و
                کارگزار موظف به استرداد وجه دریافتی پس از کسر 10 ٪ هزینه ریالی تور بعنوان هزینه
                های انجام شده به مسافر خواهد بود
            </p>
            <p style="direction: rtl;">
                ماده 14 - مسافر موظف است در ساعات اعلام شده در برنامه های گشت شرکت نموده و در صورت
                عدم حضور بموقع به هنگام حرکت تور، شخصا بایستی هزینه های مربوطه را جهت پیوستن به
                گروه و ادامه برنامه تور پرداخت نماید در غیراینصورت کارگزار مربوطه هیچگونه مسئولیتی
                در این خصوص نخواهد داشت
            </p>
            <p style="direction: rtl;">
                ماده 15 - در صورتیکه هنگام ابطال تور توسط مسافر روادید مسافر اخذ شده باشد کارگزار
                مجاز به ابطال روادید مربوطه توسط مقامات سفارت صادرکننده روادید خواهد بود و جرائم
                ابطال تور بر اسا ماده 8 این قرارداد اخذ خواهد شد
            </p>
            <p style="direction: rtl;">
                ماده 16 - در صورت افزایش و یا تغییر نرخ حاصله در قیمت گشت اعم از افزایش نرخ بلیط
                و یا سایر خدمات با ارائه مدارک مستند از سوی کارگزار، مسافر موظف به پرداخت مابه التفات
                اعلام شده به کارگزار می باشد
            </p>
            <p style="direction: rtl;">
                تبصره 1 - چنانچه مسافر شرایط مندرج در ماده فوق را نپذیرد موظف به پرداخت هزینه ابطال
                خواهد بود
            </p>
            <p style="direction: rtl;">
                ماده 17 - چنانچه مسافر ممنوع الخروج و در بدو ورود به کشوری مقامات محلی از ورود آن
                به کشور جلوگیری نمایند مسافر ملزم به پرداخت جریمه های ماده 8 این قرارداد بوده و
                سرپرست تور فقط موظف به همکاری جهت بازگشت مسافر به کشور می باشد و هیچ نوع مسئولیتی
                در این خصوص متوجه کارگزار نخواهد بود. اقامت در سالن ترانزیت تا زمان بازگشت و هزینه
                مربوطه بعهده مسافر می باشد
            </p>
            <p style="direction: rtl;">
                ماده 18 - در صورتی که برنامه ارائه شده توسط کارگزار در طول سفر عینا به اجرا گذاشته
                نشود مسافر مجاز است موارد را با ارائه دلایل مستدل به کارگزار اعلام نموده و کارگزار
                موظف به جلب رضایت مسافر می باشد و در صورت بروز اختلاف رای سازمان میراث فرهنگی، صنایع
                دستی و گردشگری به عنوان حکم مورد پذیرش طرفین قرار می گیرد
            </p>
            <p style="direction: rtl;">
                ماده 19 - کارگزار در خصوص عدم صدور روادید از سوی سفارتخانه برای هیچیک از مسافرین
                مسئولیت نداشته و چنانچه مسافرینی بصورت گروهی ثبت نام نموده و به دلایلی سفارتخانه
                مربوطه از صدور روادید برای هر یک از آنان ممانعت به عمل آورد سایر مسافرین بایستی
                از گشت استفاده نموده و در صورت اعلام انصراف بایستی مطابق ماده 8 این قرار داد جرائم
                متعلقه را پرداخت نمایند
            </p>
            <p style="direction: rtl;">
                تبصره 2 - در خصوص بستگان درجه یک ( پدر - مادر - همسر - فرزندان مجرد ) 50% هزینه
                جرایم متعلقه در ماده 8 این قرارداد اخذ خواهد شد و در خصوص افرادی که موفق به اخذ
                ویزا نشده اند طبق تبصره ماده 13 عمل خواهد شد
            </p>
            <p style="direction: rtl;">
                ماده 20 - مسافر متعهد است در طول استفاده از خدمات قید شده در این قرارداد کلیه ضوابط
                اعلام شده از سوی کارگزار را رعایت و در صورت وارد نمودن خسارت به هتلها، رستوران ها،
                اماکن و وسایط نقلیه شخصا مسئول پرداخت خسارت می باشد و در صورت عدم تامین خسارت وارده
                کارگزار مجاز به استفاده از تضمین مسافر بر اساس مدارک مستند پس از کسب موافقت از سازمان
                میراث فرهنگی، صنایع دستی و گردشگری بعنوان حکم می باشد
            </p>
            <br />
            <p style="direction: rtl;">
                تعهدات کارگزار
            </p>
            <p style="direction: rtl;">
                ماده 21 - کارگزار موظف است برنامه تور مورد قرارداد را در هنگام عقد قرارداد به مسافران
                ارایه نماید
            </p>
            <p style="direction: rtl;">
                ماده 22 - کارگزار متعهد است که برنامه سفر و گشت مورد توافق را به طور کامل و دقیق
                انجام داده و در صورت وقوع هرگونه پیشامد غیرمنتظره از قبیل جنگ، شورش، اعتصاب، شرایط
                جوی، و مشکلاتی که از کنترل کارگزار خارج بوده و سبب ابطال و یا تغییر برنامه گشت و
                یا مسافرت شود مسئولیتی متوجه کارگزار نخواهدبود
            </p>
            <p style="direction: rtl;">
                ماده 23 - کارگزار موظف است بیمه کامل را در مورد مسافر اعمال نموده و در صورت بروز
                هرگونه حادثه ای در طول سفر، مسافر میبایستی بر اساس شرایط بیمه تحت پوشش اقدام نماید
                و در این خصوص کارگزار همکاری و مساعدت لازم را به عمل خواهد آورد
            </p>
            <p style="direction: rtl;">
                این قرارداد در 23 ماده و 6 تبصره در سه نسخه که هرکدام حکم واحد را دارد تنظیم و بر
                اساس مواد 230، 221 و 10 قانون مدنی بوده که طرفین و قائم مقام قانونی آنها لازم الاتباع
                می باشد
            </p>
            <br />
            <p style="direction: rtl;">
                یک نسخه از این قرارداد تحویل مسافر و دو نسخه دیگر نزد کارگزار خواهد بود
            </p>
            <p style="direction: rtl;">
                &nbsp;</p>
            <br />
            <br />
            <table style="direction: rtl;" width="100%">
                <tr>
                    <td align="center" valign="top">
                        نام و نام خانوادگی مسافر و یا نماینده مسافران
                        <br />
                        امضاء
                    </td>
                    <td align="center" valign="top">
                        نام و نام خانوادگی مدیر عامل / مدیر عامل دفتر یا مدیر فنی<br />
                        <br />
                        شرکت خدمات مسافرتی و گردشگری
                        <br />
                        <br />
                        مهر و امضاء
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <p style="direction: ltr;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <img alt="" src="stamp.gif" width="90px" />
            </p>
        </div>
    </div>
    </form>
</body>
</html>
