﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Voucher.aspx.cs" Inherits="Voucher_EE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">    
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Voucher</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Arial; font-size: 8pt; width: 762px; }
        .clear { clear: both; height: 1px; }
        .h22px { height: 22px; }
        .line333 { color: #333; }
        .line666 { color: #BBB; }
        .mainPage { font-family: Arial; font-size: 8pt; width: 760px; height: 1060px; display: none; border: solid 1px #BBB; }
        .mainContent { width: 100%; }
        .arial6 { font-family: Arial; font-size: 6pt; }
        .arial8 { font-family: Arial; font-size: 8pt; }
        .arial9 { font-family: Arial; font-size: 9pt; }
        .times9b { font-family: Times New Roman; font-size: 9pt; font-weight: bold; }
        .arial10 { font-family: Arial; font-size: 10pt; }
        .arial11 { font-family: Arial; font-size: 11pt; }
        .arial11b { font-family: Arial; font-size: 11pt; font-weight: bold; }
        .arial12 { font-family: Arial; font-size: 12pt; }
        .dataDiv { border: 1px solid #808080; height: 331px; position: relative; }
        .SeperatorDiv { height: 30px; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function showVoucher(ServiceID) {
            var serviceID = parseInt(ServiceID);
            $("#param1").val(serviceID.toString());
            $.ajax({
                async: false,
                type: "POST",
                url: "Voucher.aspx/showVoucher",
                data: '{"ServiceID":' + serviceID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $(".mainPage").show();
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                        $("#gridPriceTable").width();
                        getFormData();
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Voucher.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $("#voucherListDiv").html('');
                        $("#voucherListDiv").html(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $.query = $.query.load(location.href);

                if ($.query.get('param1')) {
                    var serviceID = parseInt($.query.get('param1'));
                    showVoucher(serviceID);
                }
                else
                    getFormData();

                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
                else
                    self.parent.setViewReportSrc(location.href);
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <input id="param1" type="hidden" value="" />
    <div id="noPrintDiv">
        <div id="voucherListDiv">
            &nbsp;
        </div>
    </div>
    <div class="mainPage">
        <div class="dataDiv">
            <div>
                <table style="width: 100%;">
                    <tr style="page-break-inside: avoid">
                        <td width="50%">
                            <img id="oprLogo0" alt="" src="" height="60px" width="120px" />
                        </td>
                        <td align="right" valign="top" width="50%" class="times9b">
                            OPERATOR COUPON
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-top: 8px">
                <table border="0" cellpadding="0" cellspacing="0" width="758px">
                    <tr style="page-break-inside: avoid">
                        <td style="width: 60px">
                            Booking No.
                        </td>
                        <td style="width: 186px">
                            <span id="txtResNo0"></span>
                        </td>
                        <td style="width: 60px">
                            Ref. No.
                        </td>
                        <td style="width: 210px">
                            <span id="txtRefNo0"></span>
                        </td>
                        <td style="width: 60px">
                            Country
                        </td>
                        <td style="width: 182px">
                            <span id="txtCountry0"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Arrive on
                        </td>
                        <td style="width: 186px">
                            <span id="txtArrival0"></span>
                        </td>
                        <td>
                            Departure
                        </td>
                        <td style="width: 210px">
                            <span id="txtDeparture0"></span>
                        </td>
                        <td>
                            Nights
                        </td>
                        <td style="width: 182px">
                            <span id="txtNight0"></span>
                        </td>
                    </tr>
                    <tr style="page-break-inside: avoid">
                        <td>
                            City
                        </td>
                        <td style="width: 186px">
                            <span id="txtCity0"></span>
                        </td>
                        <td>
                            Hotel
                        </td>
                        <td style="width: 210px">
                            <span id="txtHotel0"></span>
                        </td>
                        <td>
                            Meals
                        </td>
                        <td style="width: 182px">
                            <span id="txtMeals0"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            Remarks: <span id="txtRemarks0"></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-top: 3px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr style="page-break-inside: avoid">
                        <td valign="top" width="50%">
                            <div id="gridRoomPerson0">
                            </div>
                        </td>
                        <td valign="top">
                            <div id="gridExtras0">
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="position: absolute; bottom: 0px; width: 99%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td valign="top" width="100">
                                Your travel agency :
                            </td>
                            <td valign="top" style="width: 268px;">
                                <span id="txtAgencyName0"></span>
                            </td>
                            <td valign="top" style="padding-left: 10px; width: 60px">
                                Partners
                            </td>
                            <td valign="top">
                                <span id="txtPartners0"></span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2">
                                <span id="txtAgencyAddress0"></span>
                            </td>
                            <td style="padding-left: 10px" valign="top">
                                &nbsp;
                            </td>
                            <td valign="top">
                                <span id="txtPartnersAddress0"></span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Phone
                            </td>
                            <td valign="top" style="width: 268px;">
                                <span id="txtAgencyPhone0"></span>
                            </td>
                            <td style="padding-left: 10px" valign="top">
                                Phone
                            </td>
                            <td valign="top">
                                <span id="txtPartnersPhone0"></span>
                            </td>
                        </tr>
                        <tr style="page-break-inside: avoid">
                            <td colspan="2">
                                &nbsp;
                            </td>
                            <td style="padding-left: 10px" valign="top">
                                Fax
                            </td>
                            <td valign="top">
                                <span id="txtPartnersFax0"></span>
                            </td>
                        </tr>
                    </table>
                    <table border="0" style="width: 98%; height: 15px;">
                        <tr>
                            <td style="padding-left: 10px;">
                                <span id="lblOpr0">RESERVED AND PAYABLE BY "NOVATOURS" TOUR OPERATOR</span>
                            </td>
                            <td align="right" style="padding-right: 10px; width: 30%;">
                                <span id="txtDate0"></span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="SeperatorDiv">
            &nbsp;</div>
        <div class="dataDiv">
            <div>
                <table style="width: 100%;">
                    <tr style="page-break-inside: avoid">
                        <td width="50%">
                            <img id="oprLogo1" alt="" height="60px" src="" width="120px" />
                        </td>
                        <td align="right" class="times9b" valign="top" width="50%">
                            HOTEL COUPON
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-top: 8px">
                <table border="0" cellpadding="0" cellspacing="0" width="758px">
                    <tr style="page-break-inside: avoid">
                        <td style="width: 60px">
                            Booking No.
                        </td>
                        <td style="width: 186px">
                            <span id="txtResNo1"></span>
                        </td>
                        <td style="width: 60px">
                            Ref. No.
                        </td>
                        <td style="width: 210px">
                            <span id="txtRefNo1"></span>
                        </td>
                        <td style="width: 60px">
                            Country
                        </td>
                        <td style="width: 182px">
                            <span id="txtCountry1"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Arrive on
                        </td>
                        <td style="width: 186px">
                            <span id="txtArrival1"></span>
                        </td>
                        <td>
                            Departure
                        </td>
                        <td style="width: 210px">
                            <span id="txtDeparture1"></span>
                        </td>
                        <td>
                            Nights
                        </td>
                        <td style="width: 182px">
                            <span id="txtNight1"></span>
                        </td>
                    </tr>
                    <tr style="page-break-inside: avoid">
                        <td>
                            City
                        </td>
                        <td style="width: 186px">
                            <span id="txtCity1"></span>
                        </td>
                        <td>
                            Hotel
                        </td>
                        <td style="width: 210px">
                            <span id="txtHotel1"></span>
                        </td>
                        <td>
                            Meals
                        </td>
                        <td style="width: 182px">
                            <span id="txtMeals1"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            Remarks: <span id="txtRemarks1"></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-top: 3px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr style="page-break-inside: avoid">
                        <td valign="top" width="50%">
                            <div id="gridRoomPerson1">
                            </div>
                        </td>
                        <td valign="top">
                            <div id="gridExtras1">
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="position: absolute; bottom: 0px; width: 99%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td valign="top" width="100">
                                Your travel agency :
                            </td>
                            <td style="width: 268px;" valign="top">
                                <span id="txtAgencyName1"></span>
                            </td>
                            <td style="padding-left: 10px; width: 60px" valign="top">
                                Partners
                            </td>
                            <td valign="top">
                                <span id="txtPartners1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top">
                                <span id="txtAgencyAddress1"></span>
                            </td>
                            <td style="padding-left: 10px" valign="top">
                                &nbsp;
                            </td>
                            <td valign="top">
                                <span id="txtPartnersAddress1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Phone
                            </td>
                            <td style="width: 268px;" valign="top">
                                <span id="txtAgencyPhone1"></span>
                            </td>
                            <td style="padding-left: 10px" valign="top">
                                Phone
                            </td>
                            <td valign="top">
                                <span id="txtPartnersPhone1"></span>
                            </td>
                        </tr>
                        <tr style="page-break-inside: avoid">
                            <td colspan="2">
                                &nbsp;
                            </td>
                            <td style="padding-left: 10px" valign="top">
                                Fax
                            </td>
                            <td valign="top">
                                <span id="txtPartnersFax1"></span>
                            </td>
                        </tr>
                    </table>
                    <table border="0" style="width: 98%; height: 15px;">
                        <tr>
                            <td style="padding-left: 10px;">
                                <span id="lblOpr1">RESERVED AND PAYABLE BY "NOVATOURS" TOUR OPERATOR</span>
                            </td>
                            <td align="right" style="padding-right: 10px; width: 30%;">
                                <span id="txtDate1"></span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="SeperatorDiv">
            &nbsp;</div>
        <div class="dataDiv">
            <div>
                <table style="width: 100%;">
                    <tr style="page-break-inside: avoid">
                        <td width="50%">
                            <img id="oprLogo2" alt="" height="60px" src="" width="120px" />
                        </td>
                        <td align="right" class="times9b" valign="top" width="50%">
                            TOURIST COUPON
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-top: 8px">
                <table border="0" cellpadding="0" cellspacing="0" width="758px">
                    <tr style="page-break-inside: avoid">
                        <td style="width: 60px">
                            Booking No.
                        </td>
                        <td style="width: 186px">
                            <span id="txtResNo2"></span>
                        </td>
                        <td style="width: 60px">
                            Ref. No.
                        </td>
                        <td style="width: 210px">
                            <span id="txtRefNo2"></span>
                        </td>
                        <td style="width: 60px">
                            Country
                        </td>
                        <td style="width: 182px">
                            <span id="txtCountry2"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Arrive on
                        </td>
                        <td style="width: 186px">
                            <span id="txtArrival2"></span>
                        </td>
                        <td>
                            Departure
                        </td>
                        <td style="width: 210px">
                            <span id="txtDeparture2"></span>
                        </td>
                        <td>
                            Nights
                        </td>
                        <td style="width: 182px">
                            <span id="txtNight2"></span>
                        </td>
                    </tr>
                    <tr style="page-break-inside: avoid">
                        <td>
                            City
                        </td>
                        <td style="width: 186px">
                            <span id="txtCity2"></span>
                        </td>
                        <td>
                            Hotel
                        </td>
                        <td style="width: 210px">
                            <span id="txtHotel2"></span>
                        </td>
                        <td>
                            Meals
                        </td>
                        <td style="width: 182px">
                            <span id="txtMeals2"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            Remarks: <span id="txtRemarks2"></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-top: 3px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr style="page-break-inside: avoid">
                        <td valign="top" width="50%">
                            <div id="gridRoomPerson2">
                            </div>
                        </td>
                        <td valign="top">
                            <div id="gridExtras2">
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="position: absolute; bottom: 0px; width: 99%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td valign="top" width="100">
                                Your travel agency :
                            </td>
                            <td style="width: 268px;" valign="top">
                                <span id="txtAgencyName2"></span>
                            </td>
                            <td style="padding-left: 10px; width: 60px" valign="top">
                                Partners
                            </td>
                            <td valign="top">
                                <span id="txtPartners2"></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top">
                                <span id="txtAgencyAddress2"></span>
                            </td>
                            <td style="padding-left: 10px" valign="top">
                                &nbsp;
                            </td>
                            <td valign="top">
                                <span id="txtPartnersAddress2"></span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Phone
                            </td>
                            <td style="width: 268px;" valign="top">
                                <span id="txtAgencyPhone2"></span>
                            </td>
                            <td style="padding-left: 10px" valign="top">
                                Phone
                            </td>
                            <td valign="top">
                                <span id="txtPartnersPhone2"></span>
                            </td>
                        </tr>
                        <tr style="page-break-inside: avoid">
                            <td colspan="2">
                                &nbsp;
                            </td>
                            <td style="padding-left: 10px" valign="top">
                                Fax
                            </td>
                            <td valign="top">
                                <span id="txtPartnersFax2"></span>
                            </td>
                        </tr>
                    </table>
                    <table border="0" style="width: 98%; height: 15px;">
                        <tr>
                            <td style="padding-left: 10px;">
                                <span id="lblOpr2">RESERVED AND PAYABLE BY "NOVATOURS" TOUR OPERATOR</span>
                            </td>
                            <td align="right" style="padding-right: 10px; width: 30%;">
                                <span id="txtDate2"></span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
