﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Proforma.aspx.cs" Inherits="Proforma_EE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Proforma</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Tahoma Times New Roman; font-size: 9pt; width: 794px; }
        .line333 { color: #333; }
        .line666 { color: #BBB; }
        .clear { clear: both; height: 1px; }
        .mainPage { width: 794px; border: solid 1px #BBB; }
        table td { padding-right: 15px; height: 22px; }
        .boldTd { font-weight: bold; }
        .r150 { text-align: right; width: 150px; }
        .RightOpenTD { font-family: Times New Roman; font-size: 9pt; font-weight: bold; border-top-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-color: #666666; border-bottom-color: #666666; border-left-color: #666666; }
        .RightTopOpenTD { font-family: Times New Roman; font-size: 9pt; border-bottom-style: solid; border-left-style: solid; border-bottom-width: 1px; border-left-width: 1px; border-bottom-color: #666666; border-left-color: #666666; }
        .CloseTD { border: 1px solid #666666; font-family: Times New Roman; font-size: 9pt; font-weight: bold; }
        .TopOpenTD { font-family: Times New Roman; font-size: 9pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-right-color: #666666; border-bottom-color: #666666; border-left-color: #666666; }
        .style1 { height: 22px; }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Proforma.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    if (this.AfterInsertShow && this.Data != '')
                                        $("#" + this.IdName + "Div").show();
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                        $("#gridPriceTable").width();
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                getFormData();
                $.query = $.query.load(location.href);
                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
                else {
                    self.parent.setViewReportSrc(location.href);
                    try {
                        self.parent.viewReportRemoveButton('EE Version');
                    }
                    catch (err) {
                    }
                    var baseUrl = $.url(location.href);
                    self.parent.viewReportAddButton('EN Version', function() { window.location = baseUrl.data.attr.base + baseUrl.data.attr.directory + 'ProformaEn.aspx'; });
                }
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="mainPage">
        <div>
            <div style="width: 100%; text-align: center;">
                <span style="font-size: medium; font-weight: bold;">Arve nr. :</span> <span id="TxtResNoA"
                    style="font-size: medium; font-weight: bold;"></span>
                <hr class="line666" size="1" />
            </div>
            <table width="100%">
                <tr>
                    <td width="6">
                        &nbsp;
                    </td>
                    <td width="128px">
                        <span style="font-size: small; font-weight: bold;">Arve kuupäev</span>
                    </td>
                    <td align="left">
                        <span id="TxtPrintTime">Print Time</span>
                    </td>
                    <td width="128px">
                        <span style="font-size: small; font-weight: bold;">Tellija</span>
                    </td>
                    <td width="269px">
                        <span id="TxtAgencyName">Agency Name</span>
                    </td>
                </tr>
                <tr>
                    <td width="6">
                        &nbsp;
                    </td>
                    <td width="128px">
                        <span style="font-size: small; font-weight: bold;">Müüja</span>
                    </td>
                    <td width="269px">
                        <span id="TxtOperator">Operator</span>
                    </td>
                    <td colspan="2">
                        <span style="font-size: small; font-weight: bold;">Reg.Nr.</span>&nbsp; <span id="TxtRegNo">
                            Register No</span> &nbsp;&nbsp;&nbsp;<span style="font-size: small; font-weight: bold;">KMKR</span>
                        &nbsp;<span id="TxtAgTaxNo">Tax No</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <span style="font-size: small; font-weight: bold;">Reisikonsultan</span>
                    </td>
                    <td>
                        <span id="TxtAgencUser"></span>
                    </td>
                    <td>
                        <span style="font-size: small; font-weight: bold;">Aadress</span>
                    </td>
                    <td>
                        <span id="TxtAgAdres"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <span style="font-size: small; font-weight: bold;">Ostja</span>
                    </td>
                    <td>
                        <span id="TxtLeaderName"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <span style="font-size: small; font-weight: bold;">Broneering</span>
                    </td>
                    <td>
                        <span id="TxtResNo"></span>
                    </td>
                    <td>
                        <span style="font-size: small; font-weight: bold;">Reisijate arv</span>
                    </td>
                    <td>
                        <span id="TxtPax"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <span style="font-size: small; font-weight: bold;">Riik</span>
                    </td>
                    <td>
                        <span id="TxtCountry"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <span style="font-size: small; font-weight: bold;">Linn</span>
                    </td>
                    <td>
                        <span id="TxtArrCity"></span>
                    </td>
                </tr>
            </table>
            <hr class="line666" size="1" />
            <div id="gridFlightDiv" style="display: none;">
                <div id="gridFlight">
                </div>
                <br />
                <hr class="line666" size="1" />
            </div>
            <div id="gridHotelDiv" style="display: none;">
                <span style="font-weight: bold; font-size: 12pt;">Hotell - toad</span>
                <br />
                <span style="font-weight: bold;">Reisi sihtkoht </span><span id="TxtResort"></span>
                <br />
                <div id="gridHotel">
                </div>
                <hr class="line666" size="1" />
            </div>
            <div id="gridCustPriceDiv" style="display: none;">
                <span style="font-weight: bold; font-size: 12pt;">Reisipaketi hind</span>
                <br />
                <div id="gridCustPrice">
                </div>
                <hr class="line666" size="1" />
            </div>
            <div id="gridExtWithoutKomDiv" style="display: none;">
                <span style="font-weight: bold; font-size: 12pt;">Lisateenused</span>
                <br />
                <div id="gridExtWithoutKom">
                </div>
                <hr class="line666" size="1" />
            </div>
            <div id="gridExtWithKomDiv" style="display: none;">
                <span style="font-weight: bold; font-size: 12pt;">Lisateenused ilma komisjonita</span>
                <br />
                <div id="gridExtWithKom">
                </div>
                <hr class="line666" size="1" />
            </div>
            <%--<span style="font-weight: bold; font-size: 12pt;">Transfeer Lennuujaam - Hotell - Lennujaam</span>--%>
            <%--<hr class="line666" size="1" />--%>
            <div id="footherDiv">
                <table width="100%">
                    <tr>
                        <td width="350px">
                            &nbsp;
                        </td>
                        <td class="boldTd">
                            <span>Hind kokku</span>
                        </td>
                        <td class="r150">
                            <span id="lblTotalSum"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <span id="lblDiscountPer0">%</span>
                        </td>
                        <td class="boldTd">
                            <span>Soodustus (</span><span id="Currency0">Curr</span>)
                        </td>
                        <td class="r150">
                            <span id="lblTotalReservationDiscount0"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            &nbsp;<%--<span id="lblDiscountPer1">%</span>--%>
                        </td>
                        <td class="boldTd">
                            <span>Soodustus1 (</span><span id="Currency1">Curr</span>)
                        </td>
                        <td class="r150">
                            <span id="lblTotalReservationDiscount1"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td class="boldTd">
                            <span>Summa kokku (</span><span id="Currency2">Curr</span>)
                        </td>
                        <td class="r150">
                            <span id="lblTotalSumMinusDiscount"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td class="boldTd" colspan="2">
                            <span>Käibemaksu arvestus vastavalt KM seaduse § 40.8.</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td class="boldTd">
                            <span>Agendikomisjon käibemaksuga</span>
                        </td>
                        <td class="r150">
                            <span id="lblKomision"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <span>Agendikomisjon ilma käibemaksuta</span>
                        </td>
                        <td class="r150">
                            <span id="lblVATWithoutKomision"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <span>Käibemaks</span>
                        </td>
                        <td class="r150">
                            <span id="lblVAT"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td class="boldTd">
                            <span>Kokku tasuda (</span><span id="Currency3">Curr</span>)
                        </td>
                        <td class="r150">
                            <span id="lblTotal"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2" class="boldTd">
                            <span>Kurss 1 EUR = </span><span id="lblParity"></span>&nbsp;EEK
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td class="boldTd">
                            <span>Kokku tasuda (</span><span id="lblOtherCurrency"></span>)
                        </td>
                        <td class="r150">
                            <span id="lblTotalEUR"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td class="boldTd">
                            <span>Ettemaks </span><span id="lblAgencyPaymentPer"></span>
                        </td>
                        <td class="r150">
                            <span id="lblAgencyPayment"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <b>Ettemaksu tasumise tähtaeg 1 päev.</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                            <span id="lblTotalWithWord"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <span style="font-weight: bold;">Arve tasumise tahtaeg</span>
                <div id="lblPaymentLastDate">
                </div>
                <br />
                <span style="font-weight: bold;">Reisikorraldaja :</span> <span id="lblOperatorName"
                    style="font-weight: bold;"></span>
                <br />
                <span style="font-weight: bold;">Reg. nr.</span>&nbsp; <span id="lblOperatorRegisterCode"
                    style="font-weight: bold;"></span>&nbsp;&nbsp; <span style="font-weight: bold;">KMKR
                        nr.</span>&nbsp; <span id="lblOperatorTaxAcount" style="font-weight: bold;">
                </span>
                <br />
                <span style="font-weight: bold;">Aadress:</span> <span id="lblOperatorAdress" style="font-weight: bold;">
                </span>
                <br />
                <span id="lblBankInfo" style="font-weight: bold;"></span>
                <br />
                <span id="lblBankInfo0" style="font-weight: bold;"></span>
                <br />
                <span style="font-weight: bold;">Selle arve tasumisega kinnitan, et olen tutvunud ja
                    nõustun Novatours OÜ reisitingimustega. </span>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
