﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using TvBo;
using System.Threading;
using System.Text;
using TvTools;

public partial class BusContrat_LT : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public static string getGridTourist(User UserData, ResDataRecord ResData)
    {
        string supplierNote = string.Empty;
        var query = from q in ResData.ResService
                    where string.Equals(q.ServiceType, "TRANSPORT")
                    orderby q.BegDate
                    select new { supplierNote = q.SupNote };
        if (query.Count() > 0)
            supplierNote = query.FirstOrDefault().supplierNote;
        StringBuilder sb = new StringBuilder();
        sb.Append("<table style=\"font-size: 12px; width:100%;\" cellpadding=\"1px\" cellspacing=\"0px\" >");
        sb.Append("<tr>");
        sb.Append("<td style=\"border: 1px solid #808080; font-size: x-small; font-style: italic;\">&nbsp;vardas, pavardé</td>");
        sb.Append("<td style=\"border: 1px solid #808080; width: 160px; font-size: x-small; font-style: italic;\">&nbsp;asmens kodas</td>");
        sb.Append("<td style=\"border: 1px solid #808080; width: 325px; font-size: x-small; font-style: italic;\">&nbsp;vietos autobuse</td>");
        sb.Append("</tr>");
        foreach (TvBo.ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0))
        {
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 245px; border-bottom-style: solid; border-left-style: solid; border-bottom-width: 1px; border-left-width: 1px; border-bottom-color: #808080; border-left-color: #808080;\">");
            sb.Append(row.Name + " " + row.Surname);
            sb.Append("</td>");

            sb.Append("<td style=\"width: 160px; border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #808080;\">");
            sb.Append(string.IsNullOrEmpty(row.IDNo) ? "&nbsp;" : row.IDNo);
            sb.Append("</td>");

            sb.Append("<td style=\"width: 325px; border-right-color: #808080; border-bottom-color: #808080; border-right-width: 1px; border-bottom-width: 1px; border-right-style: solid; border-bottom-style: solid;\">" + (supplierNote.Length > 0 ? supplierNote : "&nbsp;") + "</td>");
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }

    public static string getGridHotel(User UserData, ResDataRecord ResData)
    {
        var ResSer = from Rs in ResData.ResService
                     where Rs.ServiceType == "HOTEL" && Rs.StatSer < 2
                     select new
                     {
                         Service = Rs.Service,
                         Night = Rs.Duration,
                         Board = Rs.Board,
                         Room = Rs.Room,
                         Accom = Rs.Accom,
                         Location = Rs.DepLocation
                     };
        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;
        foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2))
        {
            HotelRecord hotel = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
            sb.Append("<table class=\"hotelGridCss\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
            sb.Append(" <tbody>");
            sb.Append("  <tr>");
            sb.Append("    <td colspan=\"2\" align=\"left\">");
            sb.Append("      <b>Kelionės trukmė (n.)</b> : ");
            sb.AppendFormat("      <span>{0}</span>", row.Duration);
            sb.Append("    </td>");
            sb.Append("    <td align=\"left\">");
            sb.Append("        &nbsp;");
            sb.Append("    </td>");
            sb.Append("  </tr>");
            sb.Append("  <tr>");
            sb.Append("    <td style=\"width: 200px;\" align=\"left\">");
            sb.AppendFormat("      <b>Maitinimo tipas : </b><span>{0}</span>", row.BoardNameL);
            sb.Append("    </td>");
            sb.Append("    <td align=\"left\">");
            sb.Append("       &nbsp;");
            sb.Append("    </td>");
            sb.Append("    <td style=\"width: 220px;\" align=\"left\">");
            sb.Append("       &nbsp;");
            sb.Append("    </td>");
            sb.Append("  </tr>");
            sb.Append("  <tr>");
            sb.Append("    <td style=\"border-bottom: #000000 1px solid;\" colspan=\"3\">");
            sb.AppendFormat("      <b>Apgyvendinimas/Kambario tipas : </b><span>{0}</span>", row.AccomNameL + " / " + row.RoomNameL);
            sb.Append("    </td>");
            sb.Append("    </tr>");
            sb.Append(" </tbody>");
            sb.Append("</table>");
        }

        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();
        bool holPackImg = false;
        byte[] contractLogo = new TvReport.NovReport.NovReport().getContractLogo(ResData.ResMain.PLOperator, ResData.ResMain.HolPack, ref holPackImg, ref errorMsg);
        string contractLogoStr = TvBo.UICommon.WriteBinaryImage(contractLogo, holPackImg ? ResData.ResMain.HolPack : ResData.ResMain.PLOperator, "HOL_", "", "");
        OperatorRecord opr = new Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, false, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);
        string oprAddress = opr.FirmName + " " + opr.Address + " " + opr.AddrZip + " " + opr.AddrCity + " " + opr.Phone1 + " " + opr.Email1;
        string oprInfo = "Kelionės organizatoriaus prievolių įvykdymo užtikrinimo draudimo polisas Nr. " +
                         opr.InsPolicyNo + ", " + opr.InsSupplierName + ", " + (opr.InsExpDate.HasValue ? opr.InsExpDate.Value.ToShortDateString() : "");
        var leaderCustNo = ResData.ResCust.Where(w => string.Equals(w.Leader, "Y"));
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == leaderCustNo.FirstOrDefault().CustNo);
        string leaderInfo = string.Empty;
        if (_leader != null) leaderInfo = !Equals(_leader.ContactAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip + " " + _leader.AddrHomeTel + " " + _leader.AddrHomeFax + " " + _leader.AddrHomeEmail : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip + " " + _leader.AddrWorkTel + " " + _leader.AddrWorkFax + " " + _leader.AddrWorkEMail;
        string CountryCity = ResData.ResMain.HolPackNameL;

        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "OprLogo", TagName = "img", Data = contractLogoStr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "AddressInfo", TagName = "span", Data = oprAddress });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "ResNo", TagName = "span", Data = ResData.ResMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "OprInfo", TagName = "span", Data = oprInfo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "OprInfo1", TagName = "span", Data = "Kelionių organizatoriaus pažymėjimo Nr. " + opr.LicenseNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "ResDate", TagName = "span", Data = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "AgencyName", TagName = "span", Data = agent.FirmName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "AgencyUserName", TagName = "span", Data = ResData.ResMain.AgencyUserName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "Fax", TagName = "span", Data = agencyAddr.Fax1 });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "Tel", TagName = "span", Data = agencyAddr.Phone1 });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "Email", TagName = "span", Data = agencyAddr.EMail1 });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridTourist", TagName = "div", Data = getGridTourist(UserData, ResData).Replace('"', '!') });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderInfo", TagName = "span", Data = leaderInfo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "PackageLocalName", TagName = "span", Data = CountryCity });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "BeginDate", TagName = "span", Data = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "EndDate", TagName = "span", Data = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridHotel", TagName = "div", Data = getGridHotel(UserData, ResData).Replace('"', '!') });

        var query = from q in ResData.ResService
                    where string.Equals(q.ServiceType, "TRANSPORT")
                    orderby q.BegDate
                    select new { pickupPoint = q.PickupNameL };

        htmlData.Add(new TvReport.htmlCodeData { IdName = "DepAirPort", TagName = "span", Data = query.Count() > 0 ? query.FirstOrDefault().pickupPoint : "&nbsp;" });

        #region Customer Price Table
        List<TvReport.tableRow> prices = new List<TvReport.tableRow>();
        List<TvReport.tableCell> priceRow = new List<TvReport.tableCell>();

        //Kelialapio kaina vienam asmeniui
        //List<TvReport.NovReport.CustomerPrice> incPackPrices = new TvReport.NovReport.NovReport().getCustomerIncPackPrice(ResData.ResMain.ResNo, ref errorMsg);
        List<TvReport.NovReport.ContractCustomerPrice> custPrices = new TvReport.NovReport.NovReport().getContractCustomerPrices(ResData.ResMain.ResNo, UserData.Market, UserData.Ci, ref errorMsg);
        int i = 0;
        priceRow.Add(new TvReport.tableCell { Text = "", Width = 290f, Align = TvReport.fontAlign.Center });
        foreach (var row1 in ResData.ResCust.OrderBy(o => o.CustNo))
        {
            ++i;
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = i.ToString();
            cell.Width = 70f;
            cell.Align = TvReport.fontAlign.Center;
            priceRow.Add(cell);
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        List<TvReport.NovReport.ContractCustomerPrice> incPackPrices = custPrices.Where(w => w.ServiceID == 0 && w.ExtServiceID == 0).OrderBy(o => o.CustNo).ToList<TvReport.NovReport.ContractCustomerPrice>();
        if (incPackPrices != null && incPackPrices.Count > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "Kelialapio kaina vienam asmeniui";
            cell.Width = 290f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (TvReport.NovReport.ContractCustomerPrice row in incPackPrices.OrderBy(o => o.CustNo))
            {
                cell = new TvReport.tableCell();
                cell.Text = row.Price > 0 ? row.Price.ToString("#,###.00") : "";
                cell.Width = 70f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
            prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        }

        List<TvReport.NovReport.ContractCustomerPrice> queryOutPack = custPrices.Where(w => w.ServiceID != 0).OrderBy(o => o.CustNo).ToList<TvReport.NovReport.ContractCustomerPrice>();
        if (queryOutPack.Count() > 0)
        {
            priceRow = new List<TvReport.tableCell>();
            var LService = from Ls in queryOutPack
                           group Ls by new { ServiceID = Ls.ServiceID, ExtServiceID = Ls.ExtServiceID } into g
                           select new { ServiceID = g.Key.ServiceID, ExtServiceID = g.Key.ExtServiceID };
            foreach (var r in LService)
            {
                priceRow = new List<TvReport.tableCell>();
                var rowQ = from r1 in queryOutPack
                           where r1.ServiceID == r.ServiceID && r1.ExtServiceID == r.ExtServiceID
                           select new { Description = r1.ServiceDesc };
                if (rowQ.Count() > 0)
                {
                    TvReport.tableCell cell = new TvReport.tableCell();
                    cell.Text = rowQ.FirstOrDefault().Description;
                    cell.Width = 290f;
                    cell.Align = TvReport.fontAlign.Left;
                    priceRow.Add(cell);
                    foreach (var row1 in ResData.ResCust.OrderBy(o => o.CustNo))
                    {
                        var sums = from r1 in queryOutPack
                                   where r1.ServiceID == r.ServiceID && r1.ExtServiceID == r.ExtServiceID && r1.CustNo == row1.CustNo
                                   select new { Price = r1.Price };
                        decimal sumPrice = 0;
                        if (sums.Count() > 0)
                            sumPrice = sums.Sum(s => s.Price);
                        cell = new TvReport.tableCell();
                        cell.Text = sumPrice > 0 ? sumPrice.ToString("#,###.00") : "";
                        cell.Width = 70f;
                        cell.Align = TvReport.fontAlign.Right;
                        priceRow.Add(cell);
                    }

                }
                prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
            }
        }
        priceRow = new List<TvReport.tableCell>();
        decimal priceTotal = 0;
        if (ResData.ResCust.Count > 0)
        {
            TvReport.tableCell cell = new TvReport.tableCell();
            cell.Text = "";
            cell.Width = 290f;
            cell.Align = TvReport.fontAlign.Left;
            priceRow.Add(cell);
            foreach (ResCustRecord r1 in ResData.ResCust)
            {
                decimal? totalPrice = null;
                int? custNo = r1.CustNo;
                totalPrice = custPrices.Where(w => w.CustNo == custNo).Sum(s => s.Price);
                cell = new TvReport.tableCell();
                cell.Text = totalPrice.HasValue ? totalPrice.Value.ToString("#,###.00") : "";
                priceTotal += totalPrice.HasValue ? totalPrice.Value : 0;
                cell.Width = 70f;
                cell.Align = TvReport.fontAlign.Right;
                priceRow.Add(cell);
            }
        }
        prices.Add(new TvReport.tableRow { TableRow = priceRow, RowHeight = 11 });
        #endregion

        #region write Customer Price Table
        StringBuilder gridPrice = new StringBuilder();
        gridPrice.Append("<table id=\"gridPriceTable\" cellpadding=\"3px\" cellspacing=\"0px\" >");
        foreach (TvReport.tableRow row in prices)
        {
            gridPrice.Append(" <tr>");
            List<TvReport.tableCell> cells = new List<TvReport.tableCell>();
            cells = row.TableRow;
            foreach (TvReport.tableCell cell in cells)
            {
                string alignStr = string.Empty;
                string styleStr = string.Empty;
                switch (cell.Align)
                {
                    case TvReport.fontAlign.Center: styleStr += " text-align: center;"; break;
                    case TvReport.fontAlign.Right: styleStr += " text-align: right;"; break;
                    default: styleStr += " text-align: left;"; break;
                }
                if (cell.Width.HasValue && cell.Width.Value > 0)
                    styleStr += "width: " + cell.Width.Value.ToString("#.") + "px;";
                styleStr += " white-space:nowrap;";
                string cellStr = string.Format("<td style=\"{1}\">{0}</td>",
                                    cell.Text,
                                    styleStr);
                gridPrice.Append(cellStr);
            }
            gridPrice.Append(" </tr>");
        }
        gridPrice.Append("</table>");
        #endregion

        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridPriceSum", TagName = "div", Data = gridPrice.ToString().Replace('"', '!') });

        htmlData.Add(new TvReport.htmlCodeData { IdName = "TotalCost", TagName = "span", Data = priceTotal > 0 ? priceTotal.ToString("#,###.00") : "" });

        decimal PasSupDis = ResData.ResMain.PasSupDis.HasValue ? ResData.ResMain.PasSupDis.Value : 0;
        decimal EBPas = ResData.ResMain.EBPas.HasValue ? ResData.ResMain.EBPas.Value : 0;
        decimal AgencyDisPasVal = ResData.ResMain.AgencyDisPasVal.HasValue ? ResData.ResMain.AgencyDisPasVal.Value : 0;
        decimal bonus = new TvReport.NovReport.NovReport().getBonus(ResData.ResMain.ResNo, ref errorMsg);
        decimal TotalDiscount = PasSupDis - EBPas - AgencyDisPasVal - bonus;
        decimal promoQ = ResData.ResPromo.Sum(s => (s.Amount.HasValue ? s.Amount.Value : 0));
        if (promoQ > 0)
            TotalDiscount = TotalDiscount - promoQ;
        htmlData.Add(new TvReport.htmlCodeData { IdName = "IndirimToplami", TagName = "span", Data = TotalDiscount != 0 ? TotalDiscount.ToString("#,###.00") : "" });
        CurrencyRecord currency = new Banks().getCurrency(UserData.Market, ResData.ResMain.SaleCur, ref errorMsg);
        string numberToword = new LibVB.VBUtils.NumberToWordLT().SumaZod(ResData.ResMain.PasPayable.Value, currency.Name, currency.FracName);
        string totalPriceStr = ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " ( " + numberToword + " ) ";
        htmlData.Add(new TvReport.htmlCodeData { IdName = "KalanToplam", TagName = "span", Data = totalPriceStr });
        /*  
        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });        
        */
        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }

    [WebMethod(EnableSession = true)]
    public static string viewBinaryReport()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        string retVal = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        return "";
    }
}