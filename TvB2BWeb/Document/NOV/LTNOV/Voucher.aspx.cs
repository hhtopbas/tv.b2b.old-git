﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvTools;

public partial class Voucher_LT : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];        
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         where q2.Service == row.Service && q2.RecID == row.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"showVoucher({0});\" style=\"cursor: pointer; text-decoration: underline;\">{1} (",                                
                                row.RecID,
                                i.ToString() + ". " + row.ServiceNameL);
                sb.Append(((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued")) + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"showVoucher({0});\" style=\"cursor: pointer; text-decoration: underline;\">{1} (",                                 
                                row.RecID,
                                i.ToString() + ". " + row.ServiceNameL);
                    sb.Append(((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued")) + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public static StringBuilder ShowRoomPerson(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        StringBuilder sb = new StringBuilder();
        ResServiceRecord hotel = ResData.ResService.Find(f => f.RecID == ServiceID);
        sb.Append("<table >");
        sb.Append(" <tr>");
        sb.Append("     <td style=\"font-weight:bold;\">");
        sb.Append(hotel != null ? hotel.RoomName : "");
        sb.Append("     </td>");
        sb.Append(" </tr>");
        sb.Append(" <tr>");
        sb.Append("     <td>");
        string roomPerson = string.Empty;
        var customers = from q1 in ResData.ResService
                        join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                        join q3 in ResData.ResCust on q2.CustNo equals q3.CustNo
                        where q1.RecID == ServiceID &&
                              q1.StatSer < 2
                        select q3;
        foreach (var row in customers)
        {
            if (roomPerson != "")
                roomPerson += " / ";
            roomPerson += row.TitleStr + ". " + row.Surname + " " + row.Name;
        }
        sb.Append(roomPerson);
        sb.Append("     </td>");
        sb.Append(" </tr>");
        sb.Append("</table>");
        return sb;
    }

    public static StringBuilder ShowRoomExtras(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        StringBuilder sb = new StringBuilder();
        var extra = from q in ResData.ResServiceExt
                    where q.ServiceID == ServiceID &&
                        q.StatSer < 2
                    select new { ServiceName = q.ExtServiceName, q.BegDate, q.EndDate };
        var addiServis = from q in ResData.ResService
                         where (string.Equals(q.ServiceType, "EXCURSION") || string.Equals(q.ServiceType, "TRANSFER")) &&
                            string.Equals(q.IncPack, "N") && q.Compulsory == false &&
                            q.StatSer < 2
                         select new { ServiceName = q.ServiceName, q.BegDate, q.EndDate };
        if (extra.Count() > 0)
        {
            sb.Append("<table width=\"370px\">");
            sb.Append(" <tr>");
            sb.Append("     <td style=\"font-weight:bold; width: 210px\">Service name</td>");
            sb.Append("     <td style=\"font-weight:bold; width: 80px\">Beginning date</td>");
            sb.Append("     <td style=\"font-weight:bold; width: 80px\">Ending date</td>");
            sb.Append(" </tr>");
            foreach (var row in extra)
            {
                sb.Append(" <tr>");
                sb.Append("     <td>");
                sb.Append(row.ServiceName);
                sb.Append("     </td>");
                sb.Append("     <td>");
                sb.Append(row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "");
                sb.Append("     </td>");
                sb.Append("     <td>");
                sb.Append(row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : "");
                sb.Append("     </td>");
                sb.Append(" </tr>");
            }
            sb.Append("</table>");
        }
        if (addiServis.Count() > 0)
        {
            sb.Append("<table width=\"370px\">");
            sb.Append(" <tr>");
            sb.Append("     <td style=\"font-weight:bold; width: 210px\">Service name</td>");
            sb.Append("     <td style=\"font-weight:bold; width: 80px\">Beginning date</td>");
            sb.Append("     <td style=\"font-weight:bold; width: 80px\">Ending date</td>");
            sb.Append(" </tr>");
            foreach (var row in addiServis)
            {
                sb.Append(" <tr>");
                sb.Append("     <td>");
                sb.Append(row.ServiceName);
                sb.Append("     </td>");
                sb.Append("     <td>");
                sb.Append(row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "");
                sb.Append("     </td>");
                sb.Append("     <td>");
                sb.Append(row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : "");
                sb.Append("     </td>");
                sb.Append(" </tr>");
            }
            sb.Append("</table>");
        }
        return sb;
    }    

    [WebMethod(EnableSession = true)]
    public static string showVoucher(int? ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord hotelService = ResData.ResService.Find(f => f.RecID == ServiceID);
        if (hotelService == null) return "";

        StringBuilder sb = new StringBuilder();
        bool holPackImg = false;
        byte[] contractLogo = new TvReport.NovReport.NovReport().getContractLogo(ResData.ResMain.PLOperator, ResData.ResMain.HolPack, ref holPackImg, ref errorMsg);
        string contractLogoStr = TvBo.UICommon.WriteBinaryImage(contractLogo, holPackImg ? ResData.ResMain.HolPack : ResData.ResMain.PLOperator, "HOL_", "", "");
        OperatorRecord opr = new Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, false, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);
        string oprAddress = opr.FirmName + " " + opr.Address + " " + opr.AddrZip + " " + opr.AddrCity + " " + opr.Phone1 + " " + opr.Email1;
        var leaderCustNo = ResData.ResCust.Where(w => string.Equals(w.Leader, "Y"));
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == leaderCustNo.FirstOrDefault().CustNo);
        string leaderInfo = string.Empty;
        if (_leader != null) leaderInfo = !Equals(_leader.ContactAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip + " " + _leader.AddrHomeTel + " " + _leader.AddrHomeFax + " " + _leader.AddrHomeEmail : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip + " " + _leader.AddrWorkTel + " " + _leader.AddrWorkFax + " " + _leader.AddrWorkEMail;
        List<ResServiceRecord> flights = ResData.ResService.Where(w => w.ServiceType == "FLIGHT").ToList<ResServiceRecord>();

        StringBuilder RoomPerson = ShowRoomPerson(UserData, ResData, ServiceID);
        StringBuilder Extras = ShowRoomExtras(UserData, ResData, ServiceID);

        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });
        string countryName = new Locations().getLocationForCountryName(UserData, hotelService.DepLocation.Value, ref errorMsg);
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, hotelService.Service, ref errorMsg);
        SupplierRecord supplier = new TvSystem().getSupplier(UserData.Market, hotelService.Supplier, ref errorMsg);
        string handicaps = new NovReport().getHandicaps(UserData, ResData);
        bool aipPL = false;
        if (string.Equals(ResData.ResMain.PLMarket, "LTECO")) aipPL = true;
        for (int i = 0; i < 3; i++)
        {
            htmlData.Add(new TvReport.htmlCodeData { IdName = "oprLogo" + i.ToString(), TagName = "img", Data = contractLogoStr });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo" + i.ToString(), TagName = "span", Data = ResData.ResMain.ResNo });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtRefNo" + i.ToString(), TagName = "span", Data = ResData.ResMain.ResNo });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCountry" + i.ToString(), TagName = "span", Data = countryName });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtArrival" + i.ToString(), TagName = "span", Data = hotelService.BegDate.HasValue ? hotelService.BegDate.Value.ToShortDateString() : "" });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDeparture" + i.ToString(), TagName = "span", Data = hotelService.EndDate.HasValue ? hotelService.EndDate.Value.ToShortDateString() : "" });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtNight" + i.ToString(), TagName = "span", Data = hotelService.Duration.ToString() });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCity" + i.ToString(), TagName = "span", Data = hotel.LocationName });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtHotel" + i.ToString(), TagName = "span", Data = hotel.Name });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtMeals" + i.ToString(), TagName = "span", Data = hotelService.BoardName });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtRemarks" + i.ToString(), TagName = "span", Data = ResData.ResMain.ResNote + " " + handicaps });

            htmlData.Add(new TvReport.htmlCodeData { IdName = "gridRoomPerson" + i.ToString(), TagName = "div", Data = RoomPerson.ToString() });

            htmlData.Add(new TvReport.htmlCodeData { IdName = "gridExtras" + i.ToString(), TagName = "div", Data = Extras.ToString() });

            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyName" + i.ToString(), TagName = "span", Data = string.IsNullOrEmpty(agencyAddr.FirmName) ? agencyAddr.Name : agencyAddr.FirmName });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPartners" + i.ToString(), TagName = "span", Data = string.IsNullOrEmpty(supplier.FirmName) ? supplier.Name : supplier.FirmName });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyAddress" + i.ToString(), TagName = "span", Data = agencyAddr.Address + " " + agencyAddr.AddrZip + " " + agencyAddr.AddrCity + " " + agencyAddr.AddrCountry });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPartnersAddress" + i.ToString(), TagName = "span", Data = supplier.Address + " " + supplier.AddrZip + " " + supplier.AddrCity + " " + supplier.AddrCountry });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyPhone" + i.ToString(), TagName = "span", Data = agencyAddr.Phone1 });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPartnersPhone" + i.ToString(), TagName = "span", Data = supplier.Phone1 });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPartnersFax" + i.ToString(), TagName = "span", Data = supplier.Fax1 });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDate" + i.ToString(), TagName = "span", Data = DateTime.Now.ToString("yyyy/MM/dd") });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "lblOpr" + i.ToString(), TagName = "span", Data = aipPL ? " " : "RESERVED AND PAYABLE BY \"NOVATURAS\" TOUR OPERATOR" });
        }
        ResData = new Reservation().setPrintVoucherDoc(UserData, ResData, ServiceID, ref errorMsg);
        HttpContext.Current.Session["ResData"] = ResData;
        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }
}
