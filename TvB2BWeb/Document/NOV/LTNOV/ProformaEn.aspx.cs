﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvBo;

public partial class ProformaEn_LT : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public static string getGridFlight(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        if (ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).Count() > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table style=\"width:99%;\">");
            foreach (ResServiceRecord row in ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")))
            {
                string statConf = string.Empty;
                switch (row.StatConf)
                {
                    case 0: statConf = "Request"; break;
                    case 1: statConf = "OK"; break;
                    case 2: statConf = "Not Confirm"; break;
                    case 3: statConf = "No Show"; break;
                    default: statConf = "Error"; break;
                }
                FlightDetailRecord flightDay = new Flights().getFlightDetail(UserData, UserData.Market, row.Service, row.BegDate.Value, ref errorMsg);
                sb.Append("    <tr>");
                sb.Append("        <td>" + flightDay.DepAirport + "-" + flightDay.ArrAirport + "</td>");
                sb.Append("        <td style=\"width:250px;\">" + row.Service + "</td>");
                sb.Append("        <td style=\"width:100px;\">" + (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") + "</td>");
                sb.Append("        <td style=\"width:60px;\">" + (flightDay.DepTime.HasValue ? flightDay.DepTime.Value.ToShortTimeString() : "") + "</td>");
                sb.Append("        <td style=\"width:60px;\">" + statConf + "</td>");
                sb.Append("    </tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }
        else return "";
    }

    public static string getGridHotel(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        if (ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).Count() > 0)
        {
            var Query = from q in ResData.ResService
                        where string.Equals(q.ServiceType, "HOTEL")
                        select q;
            var HotLocation = from HotLoc in ResData.ResService
                              where HotLoc.ServiceType == "HOTEL"
                              select new { HLoc = HotLoc.DepLocation };
            string Country = new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg);
            string City = HotLocation.Count() > 0 ? new Locations().getLocationForCityName(UserData, HotLocation.FirstOrDefault().HLoc.Value, ref errorMsg) : new Locations().getLocationForCountryName(UserData, ResData.ResMain.ArrCity.Value, ref errorMsg);
            string Resort = HotLocation.Count() > 0 ? new Locations().getLocationName(UserData, HotLocation.FirstOrDefault().HLoc.Value, ref errorMsg) : "";
            StringBuilder sb = new StringBuilder();
            sb.Append("<br />");
            sb.AppendFormat("<span style=\"font-weight: bold;\">Country : </span><span id=\"TxtCountry\">{0}</span>", Country);
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.AppendFormat("<span style=\"font-weight: bold;\">City : </span><span id=\"TxtCity\">{0}</span>", City);
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.AppendFormat("<span style=\"font-weight: bold;\">Resort : </span><span id=\"TxtResort\">{0}</span>", Resort);
            sb.Append("<br />");
            sb.Append("<table style=\"width: 99%;\" cellpadding=\"2\" cellspacing=\"0\">");
            sb.Append(" <tr>");
            sb.Append("  <td class=\"RightOpenTD\" align=\"center\">Hotel / meals</td>");
            sb.Append("  <td class=\"RightOpenTD\" style=\"width: 90px;\" align=\"center\">Date from</td>");
            sb.Append("  <td class=\"RightOpenTD\" style=\"width: 80px;\" align=\"center\">Nights</td>");
            sb.Append("  <td class=\"RightOpenTD\" style=\"width: 60px;\" align=\"center\">Room No</td>");
            sb.Append("  <td class=\"RightOpenTD\" style=\"width: 120px;\" align=\"center\">Room Type</td>");
            sb.Append("  <td class=\"RightOpenTD\" style=\"width: 60px;\" align=\"center\">Pax</td>");
            sb.Append("  <td class=\"CloseTD\" style=\"width: 60px;\" align=\"center\">#</td>");
            sb.Append(" </tr>");
            foreach (ResServiceRecord row in ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")))
            {
                string statConf = string.Empty;
                switch (row.StatConf)
                {
                    case 0: statConf = "Request"; break;
                    case 1: statConf = "OK"; break;
                    case 2: statConf = "Not Confirm"; break;
                    case 3: statConf = "No Show"; break;
                    default: statConf = "Error"; break;
                }
                HotelRecord HotelData = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
                sb.Append(" <tr>");
                sb.Append("  <td>" + HotelData.Name + "</td>");
                sb.Append("  <td align=\"center\">" + (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") + "</td>");
                sb.Append("  <td align=\"center\">" + row.Duration.ToString() + "</td>");
                sb.Append("  <td align=\"center\">" + row.Unit.ToString() + "</td>");
                sb.Append("  <td align=\"center\">" + row.Room.ToString() + "</td>");
                sb.Append("  <td align=\"center\">" + (row.Adult.Value + row.Child.Value).ToString() + "</td>");
                sb.Append("  <td align=\"center\">" + statConf + "</td>");
                sb.Append(" </tr>");
            }

            sb.Append("</table>");

            return sb.ToString();
        }
        else return "";
    }

    public static string getGridCustPrice(User UserData, ResDataRecord ResData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<table style=\"width: 99%;\" cellpadding=\"2\" cellspacing=\"0\">");
        sb.Append("    <tr>");
        sb.Append("        <td class=\"RightOpenTD\" align=\"left\">Surname, Name</td>");
        sb.Append("        <td class=\"CloseTD\" style=\"width: 110px;\" align=\"right\">Price</td>");
        sb.Append("    </tr>");
        string errorMsg = string.Empty;
        List<TvReport.NovReport.Proforma_CustPrice> custPrice = new TvReport.NovReport.NovReport().getInvInPackServicePrice(ResData.ResMain.ResNo, ref errorMsg);
        foreach (var row in custPrice)
        {
            sb.Append("    <tr>");
            sb.Append("        <td align=\"left\">" + row.Surname + ", " + row.Name + "</td>");
            sb.Append("        <td style=\"width: 110px;\" align=\"right\">" + (row.Price.HasValue ? row.Price.Value.ToString("#,###.00") : "") + "</td>");
            sb.Append("    </tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }

    public static string getGridExtWithoutCom(User UserData, ResDataRecord ResData, List<TvReport.NovReport.InvServicePrice> invOutPackPrice)
    {
        string errorMsg = string.Empty;
        List<TvReport.NovReport.InvServicePrice> withOutCom = invOutPackPrice.Where(w => w.PayCom).Select(s => s).ToList<TvReport.NovReport.InvServicePrice>();
        if (withOutCom.Count > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table style=\"width: 99%;\" cellpadding=\"2\" cellspacing=\"0\">");
            sb.Append("    <tr>");
            sb.Append("        <td class=\"RightOpenTD\" align=\"left\">Product adds name</td>");
            sb.Append("        <td class=\"RightOpenTD\" style=\"width: 60px;\" align=\"left\">Quantity</td>");
            sb.Append("        <td class=\"CloseTD\" style=\"width: 110px;\" align=\"right\">Price</td>");
            sb.Append("    </tr>");
            foreach (TvReport.NovReport.InvServicePrice row in withOutCom)
            {
                sb.Append("    <tr>");
                sb.Append("        <td align=\"left\">" + row.ServiceDesc + "</td>");
                sb.Append("        <td style=\"width: 60px;\" align=\"right\">" + row.Unit.ToString() + "</td>");
                sb.Append("        <td style=\"width: 110px;\" align=\"right\">" + (row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "") + "</td>");
                sb.Append("    </tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }
        else return "";
    }

    public static string getGridExtWithCom(User UserData, ResDataRecord ResData, List<TvReport.NovReport.InvServicePrice> invOutPackPrice)
    {
        string errorMsg = string.Empty;
        List<TvReport.NovReport.InvServicePrice> withCom = invOutPackPrice.Where(w => w.PayCom == false).Select(s => s).ToList<TvReport.NovReport.InvServicePrice>();
        if (withCom.Count > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table style=\"width: 99%;\" cellpadding=\"2\" cellspacing=\"0\">");
            sb.Append("    <tr>");
            sb.Append("        <td class=\"RightOpenTD\" align=\"left\">Product adds name</td>");
            sb.Append("        <td class=\"RightOpenTD\" style=\"width: 60px;\" align=\"left\">Quantity</td>");
            sb.Append("        <td class=\"CloseTD\" style=\"width: 110px;\" align=\"right\">Price</td>");
            sb.Append("    </tr>");
            foreach (TvReport.NovReport.InvServicePrice row in withCom)
            {
                sb.Append("    <tr>");
                sb.Append("        <td align=\"left\">" + row.ServiceDesc + "</td>");
                sb.Append("        <td style=\"width: 60px;\" align=\"right\">" + row.Unit.ToString() + "</td>");
                sb.Append("        <td style=\"width: 110px;\" align=\"right\">" + (row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "") + "</td>");
                sb.Append("    </tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }
        else return "";
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        
        bool NovaBusTrips = false;
        bool NovaLongDistanceTrips = false;        

        OperatorRecord opr = new Common().getOperator(ResData.ResMain.Operator, ref errorMsg);
        AgencyRecord currentAgency = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(currentAgency.UseMainCont ? (string.IsNullOrEmpty(currentAgency.MainOffice) ? currentAgency.Code : currentAgency.MainOffice) : currentAgency.Code, false, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(currentAgency.Code, false, ref errorMsg);
        AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, false, ref errorMsg);        
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(currentAgency.Code, ref errorMsg);
        ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
        ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));

        List<TvReport.NovReport.InvServicePrice> invOutPackPrice = new TvReport.NovReport.NovReport().getOutPackServicePrice(ResData.ResMain.ResNo, UserData.Market, ref errorMsg);
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });
        decimal? _parity = new Common().Exchange(ResData.ResMain.PLMarket, ResData.ResMain.ResDate.Value, "EUR", ResData.ResMain.SaleCur, 1, true, ref errorMsg);
        decimal? Vat = new Common().getTax(UserData, ResData.ResMain.ResDate, ResData.ResMain.ArrCity, ref errorMsg);
        decimal EURLTL = _parity.HasValue ? _parity.Value : 1;
        decimal? totalSum = ResData.ResCustPrice.Sum(s => s.SalePrice);
        decimal PasPayable = ResData.ResMain.PasPayable.HasValue ? ResData.ResMain.PasPayable.Value : 0;
        decimal PasSupDis = ResData.ResMain.PasSupDis.HasValue ? ResData.ResMain.PasSupDis.Value : 0;
        decimal EBPas = ResData.ResMain.EBPas.HasValue ? ResData.ResMain.EBPas.Value : 0;
        decimal EBAgency = ResData.ResMain.EBAgency.HasValue ? ResData.ResMain.EBAgency.Value : 0;
        decimal bonus = new TvReport.AllOperator().getBonus(ResData.ResMain.ResNo);
        decimal ReservationDiscount = (PasSupDis - EBPas - bonus);// -EBAgency; 2010-02-15 Vidas        
        decimal promoQ = ResData.ResPromo.Sum(s => (s.Amount.HasValue ? s.Amount.Value : 0));
        if (promoQ > 0)
            ReservationDiscount = ReservationDiscount - promoQ;
        decimal AgencyDisPasVal = ResData.ResMain.AgencyDisPasVal.HasValue ? ResData.ResMain.AgencyDisPasVal.Value : 0;
        decimal TotalSumMinusDiscount = (totalSum.HasValue ? totalSum.Value : 0) + ReservationDiscount - AgencyDisPasVal;        
        decimal AgencyCom = ResData.ResMain.AgencyCom.HasValue ? ResData.ResMain.AgencyCom.Value : 0;
        decimal Commision = AgencyCom - AgencyDisPasVal;
        decimal VatWithoutKomision = (Commision / (1 + ((Vat.HasValue ? Vat.Value : 0) / 100)));
        decimal Tax = Commision - VatWithoutKomision;
        decimal agencyEB = ResData.ResMain.EBAgency.HasValue ? ResData.ResMain.EBAgency.Value : 0;
        decimal agencyEBVatWithout = agencyEB / (1 + ((Vat.HasValue ? Vat.Value : 0) / 100));
        decimal agencyEBVat = agencyEB - agencyEBVatWithout;
        decimal agencyPayable = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value : 0;
        decimal Total = agencyPayable;//(TotalSum + ReservationDiscount - AgencyDisPasVal - Kommision);
        LibVB.VBUtils.NumberToWordEN numToWord = new LibVB.VBUtils.NumberToWordEN();
        CurrencyRecord curr = new Banks().getCurrency(UserData.Market, "EUR", ref errorMsg);
        string totalWithWord = numToWord.SumaZodEng(Total / EURLTL, "EUR", curr != null ? curr.FracName : "");
        //htmlData.Add(new TvReport.htmlCodeData { IdName = "lblPaymentLastDate", TagName = "div", Data = new UICommon().getResPaymentsPlanHTML(UserData.Market, ResData.ResMain.ResNo, ResData.ResMain.SaleCur, UserData.Ci.DateTimeFormat.ShortDatePattern, ref errorMsg) });        
        BankRecord oprbank1 = new Banks().getBank(UserData.Market, opr.Bank1, ref errorMsg);
        BankRecord oprbank2 = new Banks().getBank(UserData.Market, opr.Bank2, ref errorMsg);
        BankRecord agencybank = new Banks().getBank(UserData.Market, mainAgent.Bank1, ref errorMsg);

        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtResNo", TagName = "span", Data = ResData.ResMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtResNoD", TagName = "span", Data = ResData.ResMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtPrintTime", TagName = "span", Data = DateTime.Now.ToLongDateString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtOperator", TagName = "span", Data = opr.FirmName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtRegisterCode", TagName = "span", Data = (NovaBusTrips || NovaLongDistanceTrips ? "125142371" : opr.RegisterCode) + " VAT Code:" + opr.TaxAccNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtOperatorAdress", TagName = "span", Data = NovaBusTrips || NovaLongDistanceTrips ? "Jasinskio 16,  LT-01112 Vilnius":(opr.Address + " " + opr.AddrZip + " " + opr.AddrCity) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtOfficeAddress", TagName = "span", Data = "Phone :" + (NovaBusTrips || NovaLongDistanceTrips ? "+370 5 2649482" : opr.Phone1) + " , Fax :" + (NovaBusTrips || NovaLongDistanceTrips ? "+370 52624857" : opr.Fax1) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtOperatorBank", TagName = "span", Data = oprbank1 != null ? oprbank1.Name : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtBankIbanNos_Curs", TagName = "span", Data = opr.Bank1IBAN + " , " + opr.Bank1Curr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtOperatorBank0", TagName = "span", Data = oprbank2 != null ? oprbank2.Name : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtBankIbanNos_Curs0", TagName = "span", Data = opr.Bank2IBAN + " , " + opr.Bank2Curr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtAgency", TagName = "span", Data = string.IsNullOrEmpty(agencyAddr.FirmName) ? agencyAddr.Name : agencyAddr.FirmName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtRegisterCode0", TagName = "span", Data = mainAgent.RegisterCode });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtTaxAccountNo", TagName = "span", Data = mainAgent.TaxAccNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtAgencyAdress", TagName = "span", Data = agencyAddr.Address + " " + agencyAddr.AddrZip + " " + agencyAddr.AddrCity });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtAgencyBank", TagName = "span", Data = agencybank != null ? agencybank.Name : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtAgencyIbanNos_Curs", TagName = "span", Data = mainAgent.Bank1IBAN + " , " + mainAgent.Bank1Curr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo0", TagName = "span", Data = ResData.ResMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtLeaderName", TagName = "span", Data = leader != null ? (leader.Surname + " " + leader.Name) : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtPax", TagName = "span", Data = (ResData.ResMain.Adult.Value + ResData.ResMain.Child.Value).ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridFlight", TagName = "div", Data = getGridFlight(UserData, ResData), AfterInsertShow = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridHotel", TagName = "div", Data = getGridHotel(UserData, ResData), AfterInsertShow = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridCustPrice", TagName = "div", Data = getGridCustPrice(UserData, ResData), AfterInsertShow = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridExtWithoutKom", TagName = "div", Data = getGridExtWithoutCom(UserData, ResData, invOutPackPrice), AfterInsertShow = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridExtWithKom", TagName = "div", Data = getGridExtWithCom(UserData, ResData, invOutPackPrice), AfterInsertShow = true });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblTotalSum", TagName = "span", Data = totalSum.HasValue ? totalSum.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblTotalParite", TagName = "span", Data = ResData.ResMain.SaleCur });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblParite", TagName = "span", Data = EURLTL.ToString("#,###.00") + " " + ResData.ResMain.SaleCur });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblTotalReservationDiscount", TagName = "span", Data = ReservationDiscount != 0 ? (ReservationDiscount / EURLTL).ToString("#,###.00") : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblAgencyDisPasVal", TagName = "span", Data = AgencyDisPasVal != 0 ? ((AgencyDisPasVal / EURLTL) * (-1)).ToString("#,###0.00") : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblTotalSumMinusDiscount", TagName = "span", Data = (PasPayable / EURLTL).ToString("#,###.00") });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblAgencyEB", TagName = "span", Data = agencyEB != 0 ? "-" + (agencyEB / EURLTL).ToString("#,###.00") : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblKomision", TagName = "span", Data = Commision != 0 ? (Commision / EURLTL).ToString("#,###.00") : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblTotal", TagName = "span", Data = agencyPayable != 0 ? (agencyPayable / EURLTL).ToString("#,###.00") : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblTotalWithWord", TagName = "span", Data = totalWithWord });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblPaymentLastDate", TagName = "div", Data = new UICommon().getResPaymentsPlanHTML(UserData.Market, ResData.ResMain.ResNo, "EUR", TvTools.strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' '), ref errorMsg) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "lblOperatorFax", TagName = "span", Data = NovaBusTrips || NovaLongDistanceTrips ? "+370 52624857" : opr.Fax1 });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtResNoC", TagName = "span", Data = ResData.ResMain.ResNo });
        if (NovaBusTrips || NovaLongDistanceTrips)
            htmlData.Add(new TvReport.htmlCodeData { IdName = "bank1Hide", TagName = "hidden", Data = "" });
        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }

}
