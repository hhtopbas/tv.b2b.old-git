﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contrat_EN.aspx.cs" Inherits="Contrat_LT_EN" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contrat</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <link href="../../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Arial; font-size: 8pt; width: 794px; }
        .clear { clear: both; height: 1px; }
        .h22px { height: 22px; }
        .line333 { color: #333; }
        .line666 { color: #BBB; }
        .mainPage { width: 794px; border: solid 1px #BBB; }
        .logo_headerDiv { width: 100%; height: 76px; }
        .mainContent { width: 100%; }
        .operatorAdress { width: 100%; height: 36px; }
        .arial6 { font-family: Arial; font-size: 6pt; }
        .arial8 { font-family: Arial; font-size: 8pt; }
        .arial9 { font-family: Arial; font-size: 9pt; }
        .arial10 { font-family: Arial; font-size: 10pt; }
        .arial11 { font-family: Arial; font-size: 11pt; }
        .arial11b { font-family: Arial; font-size: 11pt; font-weight: bold; }
        .arial12 { font-family: Arial; font-size: 12pt; }
        .agencyInfo { width: 100%; }
        .agencyInfo strong { font-family: Arial; font-size: 10pt; }
        .touristDetail { width: 100%; }
        .gridTourist { width: 100%; padding-left: 5px; }
        .hotelHeaderDiv { width: 100%; }
        .inputNotes { border-bottom: 1px solid #C0C0C0; height: 21px; border-left-style: none; border-right-style: none; border-top-style: none; }
        .printStyle { color: #000000; border: 1px solid #BBB; }
        .priceDiv { width: 100%; }
        .priceBottom table { border-collapse: collapse; }
        .priceBottom table, th, td { border: 1px solid #BBB; }
        .priceBottom td { height: 25px; }
        .priceBottom .colInfo { background-color: #ffffff; white-space: nowrap; }
        .headerTable { height: 100%; width: 100%; }
        .headerTable table { border-collapse: collapse; }
        .headerTable table, th, td { border: none; }
        #gridPriceSum table, th, td { border: 1px solid #BBB; }
        #gridPriceSum td { height: 22px; padding: 0px 4px 0px 3px; }
        .hotelGridCss { border-collapse: collapse; border-spacing: 0; border: none; width: 100%; }
        .hotelGridCss span { font-size: 10pt; font-weight: bold; }
        .hotelGridCss td { height: 25px; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Contrat.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                                    break;
                            }
                        });
                        //$("#gridPriceTable").width();                        
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $('input:checkbox').click(function() {
                    if ($(this).attr("checked") != "checked") {
                        $(this).removeAttr("checked");
                    }
                    else {
                        $(this).attr("checked", "checked");
                    }
                });

                getFormData();
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var docName = $.query.get('docName');
                if ($.query.get('print')) {
                    $('input:checkbox').removeAttr('checked');
                    $.each($.query.keys, function(i) {
                        if (i != 'ResNo' && i != 'docName' && i != 'print' && i != '__VIEWSTATE') {
                            var elm = $("#" + i);
                            if (this == 'on' || this == 'off') {
                                if (this == 'on')
                                    elm.attr('checked', 'checked');
                                else
                                    elm.removeAttr('checked');
                            }
                            else if (this != 'true' || this != 'false') {
                                elm.val(this);
                            }
                        }
                    });
                    window.print();
                    window.close();
                }
                else {
                    self.parent.setViewReportSrc(location.href);
                    try {
                        self.parent.viewReportRemoveButton(docName);
                    }
                    catch (err) {
                    }
                    var baseUrl = $.url(location.href);
                    self.parent.viewReportAddButton('<%= GetGlobalResourceObject("LibraryResource", "btnPrintNext") %>', function() { window.location = baseUrl.data.attr.base + baseUrl.data.attr.directory + 'AgreementNext.aspx?ResNo=' + resNo + '&docName=' + docName; });
                }
            });
    </script>

</head>
<body>
    <form id="contratForm" runat="server">
    <input id="param1" type="hidden" value="" />
    <div class="mainPage">
        <div class="logo_headerDiv">
            <table class="headerTable" style="width: 100%; height: 100%;">
                <tr>
                    <td style="width: 125px; text-align: center;" valign="middle">
                        <img id="OprLogo" alt="" title="" src="" width="110px" height="55px" />
                    </td>
                    <td style="text-align: center; font-size: 14pt; font-weight: bold;" valign="middle">
                        Tourism services’ provision agreement
                    </td>
                </tr>
            </table>
        </div>
        <div class="mainContent">
            <div class="operatorAdress">
                <div style="height: 49%; float: left; width: 50%; border-bottom: solid 1px #AAA;
                    margin-bottom: 0px; position: relative;">
                    <span id="AddressInfo" class="arial6" style="bottom: 0px; left: 2px; position: absolute;">
                        adresss </span>
                </div>
                <div class="arial11b" style="height: 49%; float: left;">
                    Number of the agreement<span id="ResNo"></span>
                </div>
                <div class="clear">
                </div>
                <div style="height: 49%; margin-left: 3px;" class="arial6">
                    <span id="OprInfo">Opr</span><br />
                    <span id="OprInfo1">Opr</span>
                </div>
            </div>
        </div>
        <hr size="1" class="line666" />
        <div class="agencyInfo">
            <div class="h22px">
                <strong>The date of the agreement’s formation:</strong>&nbsp;<span id="ResDate"></span>
            </div>
            <div class="h22px" style="width: 100%;">
                <div style="width: 49%; float: left;">
                    <strong>Travel agency: </strong><span id="AgencyName"></span>
                </div>
                <div style="width: 49%; float: left;">
                    <strong>The worker of the travel agency: </strong><span id="AgencyUserName"></span>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="h22px">
                <div style="width: 25%; float: left;">
                    <strong>Fax: </strong><span id="Fax"></span>
                </div>
                <div style="width: 30%; float: left;">
                    <strong>Теlephone: </strong><span id="Tel"></span>
                </div>
                <div style="width: 43%; float: left;">
                    <strong>E-mail: </strong><span id="Email"></span>
                </div>
            </div>
        </div>
        <hr size="1" class="line666" />
        <div class="touristDetail">
            <strong class="arial8" style="margin-left: 5px;">Data of the tourist(s)</strong>
            <br />
            <div id="gridTourist">
                gridTourist
            </div>
            <span class="arial8"><b>Additional data of the tourist(s)</b></span><i> (home address,
                telephone, fax or e-mail) </i>: <span id="txtLeaderInfo"></span>
        </div>
        <hr size="1" class="line666" />
        <div class="hotelHeaderDiv">
            <div style="width: 49%; float: left;">
                <strong>Trip by plane to: </strong><span id="PackageLocalName" class="arial12"></span>
            </div>
            <div style="width: 25%; float: left;">
                <strong>Date of departure: </strong><span id="BeginDate" class="a arial12"></span>
            </div>
            <div style="width: 25%; float: left;">
                <strong>Date of return: </strong><span id="EndDate" class="arial12"></span>
            </div>
            <div class="clear">
            </div>
            <div id="gridHotel">
                gridHotel
            </div>
            <div style="height: 40px;">
                <div style="width: 155px; height: 40px; float: left;">
                    <strong>Type of accommodation: </strong>
                </div>
                <div style="width: 605px; float: left;">
                    <input id="note1" name="note1" type="text" class="inputNotes" style="width: 600px;" /><br />
                    <i>(hotel, motel, pension, private sector, summer type houses, bungalows, other)</i>
                </div>
            </div>
            <div class="clear">
            </div>
            <div>
                <div>
                    <strong>Conveniences: </strong><span class="arial10">in the rooms
                        <input type="checkbox" id="cb1" name="cb1" class="printStyle" />; in the block
                        <input type="checkbox" id="cb2" name="cb2" class="printStyle" />; in the corridor
                        <input type="checkbox" id="cb3" name="cb3" class="printStyle" />; in the territory
                        <input type="checkbox" id="cb4" name="cb4" class="printStyle" />; how it is indicated in the
                        annexes of the agreement
                        <input type="checkbox" id="cb5" name="cb5" checked="checked" class="printStyle" />.</span>
                </div>
                <div>
                    <strong>Other services included in the price of the trip: </strong><span class="arial10">
                        trip organizer’s representative’s services
                        <input type="checkbox" id="cb6" name="cb6" class="printStyle" />; visa processing
                        <input type="checkbox" id="cb7" name="cb7" class="printStyle" />;
                        <br />
                        transportation: airport-hotel
                        <input type="checkbox" id="cb8" name="cb8" class="printStyle" />; hotel-airport
                        <input type="checkbox" id="cb9" name="cb9" class="printStyle" />; how it is indicated in the
                        annexes of the agreement
                        <input type="checkbox" id="cb10" name="cb10" checked="checked" class="printStyle" />.</span>
                </div>
            </div>
            <div class="clear">
            </div>
            <div>
                <div style="width: 230px; height: 40px; float: left;">
                    <strong>Special requests of the tourist: </strong>
                </div>
                <div style="width: 560px; float: left;">
                    <input id="note2" name="note2" type="text" class="inputNotes" style="width: 525px;" /><br />
                    <i>(they are carried out if possible)(neighbor rooms, a certain place of rooms, etc.)</i>
                </div>
            </div>
            <div class="clear">
            </div>
            <div>
                <div style="width: 160px; float: left; height: 18px;">
                    <strong>Special agreements: </strong>
                </div>
                <div style="width: 625px; float: left;">
                    <input id="note3" name="note3" type="text" class="inputNotes" style="width: 595px; height: 18px;" />
                </div>
            </div>
            <div class="clear">
            </div>
            <div>
                <div style="width: 35%; float: left;">
                    <strong>Initial time of departure: </strong><span id="DepFlightTime" class="arial12">
                    </span>
                </div>
                <div style="width: 64%; float: left;">
                    <strong>The operating air carrier: </strong><span id="AirLineName" class="arial12">
                    </span>
                </div>
            </div>
            <div class="arial8" style="margin: 3px; height: auto; line-height: 1.5;">
                (Here and in travel documents indicated time of departure is preliminary and can
                change till the actual trip date. Nor the trip organizer, nor the air carrier does
                not guarantee it. This flight time is given just for the tourists’ information so
                not later than 1 day till the flight it is required to check it in the website www.novaturas.lt.)
            </div>
            <div class="clear" style="height: 2px;">
            </div>
            <div style="width: 100%;">
                <div>
                    <div class="arial9" style="width: 220px; float: left; height: 20px;">
                        <strong>Initial place of departure: </strong>
                    </div>
                    <div style="text-align: left; width: 550px; float: left; height: 20px;" class="arial11">
                        <span id="DepAirPort" class="arial9"></span>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div>
                    <div class="arial9" style="width: 220px; float: left; height: 20px;">
                        <strong>Initial place of return: </strong>
                    </div>
                    <div style="text-align: left; width: 550px; float: left; height: 20px;" class="arial11">
                        <span id="ArrAirPort" class="arial9"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <hr size="1" class="line666" />
        <div class="clear">
        </div>
        <div class="priceDiv">
            <div id="gridPriceSum">
            </div>
            <table cellpadding="0" cellspacing="0px" border="0" width="100%">
                <tr>
                    <td align="center" class="priceBottom">
                        <table style="border-width: 1px; border-collapse: collapse; border-spacing: 0; border-color: #BBB;
                            text-align: left;" width="100%">
                            <tr>
                                <td class="arial10 colInfo" style="width: 300px;">
                                    The total price of the booking
                                </td>
                                <td style="width: 80px; text-align: center;">
                                    <span id="TotalCost"></span>
                                </td>
                                <td class="arial10" align="center">
                                    <b>Payments</b>
                                </td>
                            </tr>
                            <tr>
                                <td class="arial10">
                                    <div style="float: left;">
                                        Date of payment: _______________</div>
                                    <div style="float: right; padding-right: 4px;">
                                        Payed</div>
                                </td>
                                <td style="text-align: center;">
                                    &nbsp;
                                </td>
                                <td class="arial8" align="center">
                                    <input id="cb20" name="cb20" type="checkbox" class="printStyle" />
                                    Cash
                                    <input id="cb21" name="cb21" type="checkbox" class="printStyle" />
                                    Transfer
                                    <input id="cb22" name="cb22" type="checkbox" class="printStyle" />
                                    Credit card
                                    <input id="cb23" name="cb23" type="checkbox" class="printStyle" />
                                    Bank financing
                                </td>
                            </tr>
                            <tr>
                                <td class="arial10">
                                    <div style="float: left;">
                                        Date of payment: _______________</div>
                                    <div style="float: right; padding-right: 4px;">
                                        Payed</div>
                                </td>
                                <td align="center">
                                    &nbsp;
                                </td>
                                <td class="arial8" align="center">
                                    <input id="cb30" name="cb30" type="checkbox" class="printStyle" />
                                    Cash
                                    <input id="cb31" name="cb31" type="checkbox" class="printStyle" />
                                    Transfer
                                    <input id="cb32" name="cb32" type="checkbox" class="printStyle" />
                                    Credit card
                                    <input id="cb33" name="cb33" type="checkbox" class="printStyle" />
                                    Bank financing
                                </td>
                            </tr>
                            <tr>
                                <td class="arial10" style="text-align: left;">
                                    <b>Discounts: </b>
                                </td>
                                <td class="arial10" align="left" style="padding-left: 10px;" colspan="2">
                                    <b><span id="IndirimToplami"></span></b>
                                </td>
                            </tr>
                            <tr>
                                <td class="arial10" style="text-align: left; white-space: nowrap;">
                                    <b>The total price of the booking: </b>
                                </td>
                                <td class="arial10" align="left" style="padding-left: 10px;" colspan="2">
                                    <b><span id="KalanToplam"></span></b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" valign="bottom" align="left" class="arial9" bgcolor="#ffffff">
                        <input id="cb40" name="cb40" type="checkbox" checked="checked" />
                        I am informed about the possibility to make medical expenses, travel cancellation
                        risk, unfortunate accidents’ insurance contracts and I am familiar with the insurance
                        rules.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" valign="top" align="right" class="arial9" bgcolor="#ffffff">
                        <i>(Tourist‘s signature)</i>&nbsp;&nbsp;&nbsp;&nbsp;<br />
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
