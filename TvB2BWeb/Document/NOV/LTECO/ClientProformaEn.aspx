﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClientProformaEn.aspx.cs"
    Inherits="ClientProformaEn_LT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Agency Confirmation</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Tahoma Times New Roman; font-size: 10pt; width: 680px; }
        .line333 { color: #333; }
        .line666 { color: #BBB; }
        .clear { clear: both; height: 1px; }
        .mainPage { width: 680px; border: solid 1px #BBB; }
        table td { padding-right: 15px; height: 22px; }
        .boldTd { font-weight: bold; }
        .r150 { text-align: right; width: 150px; }
        .RightOpenTD { font-family: Times New Roman; font-size: 9pt; font-weight: bold; border-top-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-color: #666666; border-bottom-color: #666666; border-left-color: #666666; }
        .RightTopOpenTD { font-family: Times New Roman; font-size: 9pt; border-bottom-style: solid; border-left-style: solid; border-bottom-width: 1px; border-left-width: 1px; border-bottom-color: #666666; border-left-color: #666666; }
        .CloseTD { border: 1px solid #666666; font-family: Times New Roman; font-size: 9pt; font-weight: bold; }
        .TopOpenTD { font-family: Times New Roman; font-size: 9pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-right-color: #666666; border-bottom-color: #666666; border-left-color: #666666; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "ClientProforma.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    if (this.AfterInsertShow && this.Data != '')
                                        $("#" + this.IdName + "Div").show();
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $.query = $.query.load(location.href);
                getFormData();
                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
                else {
                    self.parent.setViewReportSrc(location.href);
                    try {
                        self.parent.viewReportRemoveButton('EN Version');
                    }
                    catch (err) {
                    }
                    var baseUrl = $.url(location.href);
                    self.parent.viewReportAddButton('LT Version', function() { window.location = baseUrl.data.attr.base + baseUrl.data.attr.directory + 'ClientProforma.aspx'; });
                }
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="mainPage">
        <div id="divHeader" style="width: 100%;">
            <table style="width: 100%;">
                <tr>
                    <td align="center">
                        <font size="2"><b>Information of reservation : <span id="TxtResNo"></span></b></font>
                    </td>
                </tr>
            </table>
            <table style="width: 100%; font-size: 8pt;">
                <tr>
                    <td>
                        Date:&nbsp;<span id="TxtPrintTime"></span>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="width: 50%;">
                        <b><font size="2">Operator : <span id="TxtOperator"></span>
                            <br />
                            Address : <span id="TxtOperatorAdress"></span>
                            <br />
                            Phone : <span id="txtOpePhone"></span>&nbsp;&nbsp; Fax : <span id="txtOpeFax"></span>
                        </font></b>
                    </td>
                    <td valign="top">
                        <b><font size="2">Agency : <span id="TxtAgencyName"></span>
                            <br />
                            <br />
                            Address : <span id="TxtAgencyAdress"></span></font></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <hr size="1" />
        <div style="width: 100%;">
            <span><b>Clients :</b></span>
            <br />
            <div id="gridTurist">
            </div>
            <br />
        </div>
        <hr size="1" />
        <table cellpadding="0" cellspacing="0" style="height: 450px; width: 100%; vertical-align: top;">
            <tr>
                <td valign="top" style="padding-right: 0px;">
                    <div style="clear: both;">
                        <br />
                        <div id="gridServices">
                        </div>
                        <br />
                    </div>
                    <div style="clear: both;">
                        <br />
                        <div id="gridExtServices">
                        </div>
                        <br />
                    </div>
                </td>
            </tr>
        </table>
        <hr size="2" />
        <div style="width: 100%;">
            <br />
            <table width="100%">
                <tr>
                    <td align="right">
                        <b>Catalgue price :</b>
                    </td>
                    <td align="right" width="100px">
                        <span id="txtSalePrice"></span>
                    </td>
                    <td align="left" width="40px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <b>Discount :</b>
                    </td>
                    <td align="right" width="100px">
                        <span id="txtDiscount"></span>
                    </td>
                    <td align="left" width="40px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td align="right" width="100px">
                        &nbsp;
                    </td>
                    <td align="left" width="40px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <span style="font-size: 11pt; font-weight: bold;">Client has to pay :</span>
                    </td>
                    <td align="right" width="100px">
                        <span id="txtTotal" style="font-size: 11pt; font-weight: bold;"></span>
                    </td>
                    <td align="left" width="40px">
                        <span id="txtCur" style="font-size: 11pt; font-weight: bold;"></span>
                    </td>
                </tr>
            </table>
            <br />
        </div>
        <div>
            <br />
            <div id="txtFootherText">
            </div>
        </div>
    </div>
    </form>
</body>
</html>
