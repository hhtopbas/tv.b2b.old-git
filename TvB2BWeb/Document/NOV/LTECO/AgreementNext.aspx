﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgreementNext.aspx.cs" Inherits="AgreementNext_LT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contrat next page</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Tahoma Times New Roman; font-size: 9pt; width: 790px; }
        .clear { clear: both; height: 1px; }
        .mainPage { width: 790px; border: solid 1px #BBB; }

        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        var btnPrintNext = '<%= GetGlobalResourceObject("LibraryResource", "btnPrintNext") %>';

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "AgreementNext.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d')) {
                        $("#dataDiv").html('');
                        var data = msg.d;
                        if (data.retVal != '') {
                            if (data.retType == 'H') {
                                $("#dataDiv").html(data.retVal);
                            }
                            else if (data.retType == 'R') {
                                $("#dataDiv").html(data.retVal);
                            }
                            else if (data.retType == 'W') {
                                $("#dataDiv").html(data.retVal);
                            }
                            else if (data.retType == 'P') {
                              $("#dataDiv").html(data.retVal);
                            }
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function () {
                getFormData();
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var docName = $.query.get('docName');
                var next = $.query.get('next');
                if (self.parent.length > 0) {
                    self.parent.setViewReportSrc(location.href);

                    try {
                        self.parent.viewReportRemoveButton(btnPrintNext);
                    }
                    catch (err) {
                    }

                    var baseUrl = $.url(location.href);
                    try {
                        self.parent.viewReportRemoveButton(docName);
                    }
                    catch (err) {
                    }
                    self.parent.viewReportAddButton(docName, function () {
                        window.location = next + '?ResNo=' + resNo + '&docName=' + docName;
                    });

                }
                else {
                    window.print();
                    window.close();
                }
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="noPrintDiv">
            <div id="custListDiv">
                &nbsp;
            </div>
        </div>
        <div class="mainPage">
            <div id="dataDiv">
            </div>
        </div>
    </form>
</body>
</html>
