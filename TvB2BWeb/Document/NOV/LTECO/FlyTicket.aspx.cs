﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvBo;
using TvTools;
using System.Web.Script.Services;

public partial class FlyTicket_LT : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<AirlineRecord> airLines = new Flights().getAirlines(UserData.Market, ref errorMsg);
        List<FlightRecord> flights = new Flights().getFlights(UserData.Market, ref errorMsg);
        var customersTicket = from H in ResData.ResService
                              join Rc in ResData.ResCon on H.RecID equals Rc.ServiceID
                              join C in ResData.ResCust on Rc.CustNo equals C.CustNo
                              join T in ResData.Title on C.Title equals T.TitleNo
                              join F in flights on H.Service equals F.Code
                              join A in airLines on F.Airline equals A.Code
                              where H.ServiceType == "FLIGHT" && (A.TicketIssued.HasValue && A.TicketIssued.Value == true)
                              group Rc by new
                              {
                                  CustNo = Rc.CustNo,
                                  Name = C.Name,
                                  SurName = C.Surname,
                                  TitleName = T.Code,
                              } into g
                              select new
                              {
                                  CustNo = g.Key.CustNo,
                                  Name = g.Key.Name,
                                  SurName = g.Key.SurName,
                                  TitleName = g.Key.TitleName,
                              };
        if (customersTicket == null || customersTicket.Count() < 1) return "";
        StringBuilder sb = new StringBuilder();
        sb.Append("<br />");

        if (customersTicket.Count() < 1)
        {
            sb.Append(HttpContext.GetGlobalResourceObject("LibraryResource", "sZeroRecords").ToString());
        }

        //sb.Append("<table style=\"font-family: Tahoma; font-size: 12px; text-align: left;\">");
        object _documentFolder = new Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (var row in customersTicket)
        {
            i++;
            var query1 = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         where q2.ServiceType == "FLIGHT" && q1.CustNo == row.CustNo
                         select q1;
            Int16? DocStat = Conversion.getInt16OrNull(query1.Where(w => w.DocStat == 1).Count() > 0 ? 1 : 0);
            TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == row.CustNo);
            string Name = resCust.TitleStr + ". " + resCust.Surname + " " + resCust.Name;
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";
            string issuedMsg = " ({0}) </span>";
            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"showTicket('{0}');\" style=\"cursor: pointer; text-decoration: underline;\">{1} ",
                                row.CustNo,
                                i.ToString() + ". " + Name);
                sb.Append(string.Format(issuedMsg, DocStat.HasValue && DocStat.Value == 1 ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued").ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued").ToString()));
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append(string.Format("         {0}", HttpContext.GetGlobalResourceObject("LibraryResource", "BookingPaymentNotCompletted")));
                else
                {
                    sb.AppendFormat("<span onclick=\"showTicket('{0}');\" style=\"cursor: pointer; text-decoration: underline;\">{1} ",
                                row.CustNo,
                                i.ToString() + ". " + Name);
                    sb.Append(string.Format(issuedMsg, DocStat.HasValue && DocStat.Value == 1 ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued").ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued").ToString()));
                }
            }
            sb.Append(" <br />");
        }
        sb.Append(" <br />");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static List<TvReport.htmlCodeData> showTicket(int? CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return new List<TvReport.htmlCodeData>();
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        List<TvReport.NovReport.FlightTicketRecord> TicketData = new TvReport.NovReport.NovReport().getResTicketData(ResData.ResMain.ResNo, CustNo, UserData.AgencyID, UserData.UserID, UserData.Market, ref errorMsg);
        if (TicketData == null || TicketData.Count() < 1) return new List<TvReport.htmlCodeData>();
        StringBuilder sb = new StringBuilder();

        List<TvBo.ResServiceRecord> resService = ResData.ResService;
        List<TvBo.ResConRecord> resCon = ResData.ResCon;
        List<TvBo.ResServiceRecord> custFlights = (from q1 in resService
                                                   join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                   where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT"
                                                   select q1).ToList<TvBo.ResServiceRecord>();
        List<TvBo.FlightRecord> flights = new List<FlightRecord>();
        var custFlightsQuery = from q in custFlights
                               group q by new { q.Service } into k
                               select new { k.Key.Service };
        foreach (var row in custFlightsQuery)
        {
            TvBo.FlightRecord flight = new TvBo.Flights().getFlight(UserData, row.Service /*custFlights.FirstOrDefault().Service*/, ref errorMsg);
            flights.Add(flight);
        }

        List<TvBo.FlightDayRecord> flyList = new TvBo.Flights().getFlightDay(UserData, custFlights.OrderBy(o => o.BegDate).FirstOrDefault().BegDate.Value, custFlights.OrderBy(o => o.BegDate).LastOrDefault().BegDate.Value, ref errorMsg);
        TvBo.ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == CustNo);

        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        Int16 cnt = 0;

        ++cnt;
        bool imgHolpack = false;
        object operatorLogo = new TvReport.NovReport.NovReport().getContractLogo(ResData.ResMain.PLOperator, ResData.ResMain.HolPack, ref imgHolpack, ref errorMsg);
        string operatorLogoStr = TvBo.UICommon.WriteBinaryImage(operatorLogo, imgHolpack ? ResData.ResMain.HolPack : ResData.ResMain.PLOperator, "HOL_", "", "");

        var airlines = from q in flights
                       group q by new { q.Airline } into k
                       select new { k.Key.Airline };

        object airlineLogo = new TvReport.NovReport.NovReport().getAirlineLogo(flights.FirstOrDefault().Airline, true, ref errorMsg);
        if (airlines.Count() > 1)
            airlineLogo = null;
        string airlineLogoStr = airlineLogo != null ? TvBo.UICommon.WriteBinaryImage(airlineLogo, flights.FirstOrDefault().Airline, "AIR_", "", "") : "";

        htmlData.Add(new TvReport.htmlCodeData { IdName = "imgOperator", TagName = "img", Data = operatorLogoStr, PdfPage = cnt });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "imgAirlines", TagName = "img", Data = airlineLogoStr, PdfPage = cnt });

        List<TvBo.ResServiceRecord> custAirlineFlights = (from q1 in ResData.ResService
                                                          join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                          join q3 in flyList on q1.Service equals q3.FlightNo
                                                          join q4 in flights on q3.Airline equals q4.Airline
                                                          where q2.CustNo == CustNo && q1.ServiceType == "FLIGHT" && q1.BegDate == q3.FlyDate
                                                          select q1).ToList<TvBo.ResServiceRecord>();
        string eTicketNo = custFlights.FirstOrDefault().ResNo + custFlights.FirstOrDefault().BegDate.Value.ToString("MMddyy") + resCust.SeqNo.ToString() + (cnt < 10 ? "0" + cnt.ToString() : cnt.ToString());

        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtTurist", TagName = "span", Data = resCust.Surname + " / " + resCust.Name + " " + resCust.TitleStr, PdfPage = cnt });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtETicketNo", TagName = "span", Data = eTicketNo, PdfPage = cnt });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo1", TagName = "span", Data = custFlights.FirstOrDefault().ResNo, PdfPage = cnt });
        string issuedBy = agencyAddr.Name + " / " + agencyAddr.FirmName + " / " + agencyAddr.Address + " / " + agencyAddr.AddrZip + " / " + agencyAddr.AddrCity;
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtIssuedBy", TagName = "span", Data = issuedBy, PdfPage = cnt });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDateOfIssue", TagName = "span", Data = DateTime.Today.ToShortDateString(), PdfPage = cnt });

        #region createFlightTable
        sb.Append("<table cellpadding=\"1\" cellspacing=\"0\">");
        sb.Append("  <tr>");
        sb.Append("     <td style=\"width:130px; font-size: xx-small; border: 1px solid #666666; \">");
        sb.Append("                        Išvykimo/Atvykimo vieta (Departure/Arrival place)</td>");
        sb.Append("     <td style=\"width:70px; font-size: xx-small; border: 1px solid #666666; \">");
        sb.Append("                        Vežėjas (Carrier)</td>");
        sb.Append("     <td style=\"width:90px; font-size: xx-small; border: 1px solid #666666; \">");
        sb.Append("                        Skrydžio");
        sb.Append("                        <br />");
        sb.Append("                        numeris / Klasė");
        sb.Append("                        <br />");
        sb.Append("                        (Flight/Class)</td>");
        sb.Append("     <td style=\"width:70px; font-size: xx-small; border: 1px solid #666666; \">");
        sb.Append("                        Data (Date)</td>");
        sb.Append("     <td style=\"font-size: xx-small; border: 1px solid #666666; \">");
        sb.Append("                        Registracijos pabaigos laikas&nbsp; (Latest check-in time)</td>");
        sb.Append("     <td style=\"font-size: xx-small; border: 1px solid #666666; \">");
        sb.Append("                        Išvykimo laikas (Departure time)</td>");
        sb.Append("     <td style=\"font-size: xx-small; border: 1px solid #666666; \">");
        sb.Append("                        Atvykimo laikas (Arrival time)</td>");
        sb.Append("     <td style=\"font-size: xx-small; border: 1px solid #666666; \">");
        sb.Append("                        Skrydžio rezervacijos būklė ");
        sb.Append("                        <br />");
        sb.Append("                        (Status)</td>");
        sb.Append("     <td style=\"font-size: xx-small; border: 1px solid #666666; \">");
        sb.Append("                        Leistinas vežti registruoto bagažo kiekis,kg ");
        sb.Append("                        <br />");
        sb.Append("                        (Allow, kg)</td>");
        sb.Append("  </tr>");
        string Flights = string.Empty;

        foreach (TvReport.NovReport.FlightTicketRecord row in TicketData)
        {
            if (Flights.Length > 0) Flights += ",";
            Flights += row.FlightNo;

            sb.Append(" <tr>");
            sb.Append("     <td style=\"border: 1px solid #666666; \">" + row.AirPorts + "</td>");
            sb.Append("     <td style=\"border: 1px solid #666666; \">" + row.Airline + "</td>");
            sb.Append("     <td style=\"border: 1px solid #666666; \">" + row.FlightNoStr + "</td>");
            sb.Append("     <td style=\"border: 1px solid #666666; \">" + (row.DepDate.HasValue ? row.DepDate.Value.ToShortDateString() : row.BegDate.ToShortDateString()) + "</td>");
            sb.Append("     <td style=\"border: 1px solid #666666; \">" + (row.LastCInTime.HasValue ? row.LastCInTime.Value.ToShortTimeString() : "&nbsp;") + "</td>");
            sb.Append("     <td style=\"border: 1px solid #666666; \">" + (row.DepTime.HasValue ? row.DepTime.Value.ToShortTimeString() : "&nbsp;") + "</td>");
            sb.Append("     <td style=\"border: 1px solid #666666; \">nefiksuojamas (not stated)</td>");
            sb.Append("     <td style=\"border: 1px solid #666666; \">" + row.Stat + "</td>");
            string bagWeight = row.BagWeight.HasValue ? row.BagWeight.Value.ToString("#,###") : "&nbsp;";
            sb.Append("     <td style=\"border: 1px solid #666666; \">" + bagWeight + "</td>");
            sb.Append(" </tr>");
        }

        sb.Append("<table>");
        #endregion

        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridTicket", TagName = "div", Data = sb.ToString(), PdfPage = cnt });

        #region create Flights Extra Services
        string extServiceStr = string.Empty;
        List<ServiceExtRecord> extraServiceList = new Reservation().getServiceExtDetailList(UserData, ref errorMsg);
        var serviceExtras = from q in custAirlineFlights
                            group q by new { q.RecID, q.Service } into grps
                            select new { grps.Key.RecID, grps.Key.Service };
        foreach (var row in serviceExtras)
        {
            List<TvBo.ResServiceExtRecord> extServis = (from q1 in ResData.ResServiceExt
                                                        join q2 in ResData.ResConExt on q1.RecID equals q2.ServiceID
                                                        join q3 in extraServiceList on q1.ExtService equals q3.Code
                                                        where q1.ServiceID == row.RecID && q2.CustNo == CustNo && q3.DispInFPasLst && q3.Service == "FLIGHT"
                                                        select q1).ToList<TvBo.ResServiceExtRecord>();
            string extServiceStr_A = string.Empty;
            if (extServiceStr.Length > 0) extServiceStr += "<br />";
            foreach (TvBo.ResServiceExtRecord r1 in extServis)
            {
                if (extServiceStr_A.Length > 0) extServiceStr_A += " / ";
                extServiceStr_A += r1.ExtServiceNameL;
            }
            if (extServiceStr_A.Length > 0)
            {
                if (extServiceStr.Length > 0) extServiceStr += " , ";
                extServiceStr += "(" + row.Service + ") " + extServiceStr_A;
            }
        }
        extServiceStr = "<span style=\"font-size: xx-small;\"><b>Special services : </b><br />" + extServiceStr + "</span>";
        #endregion

        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridExtService", TagName = "div", Data = extServiceStr, PdfPage = cnt });

        TvBo.FlightsText eTickeTextRecord = new TvBo.Flights().getFlightTicketText(custAirlineFlights.FirstOrDefault().Service, ref errorMsg);
        string eTicketTextHtmlStr = string.Empty;
        if (!string.IsNullOrEmpty(eTickeTextRecord.HTMLText))
            eTicketTextHtmlStr = eTickeTextRecord.HTMLText;
        else
            if (!string.IsNullOrEmpty(eTickeTextRecord.RTFText))
            {
                TvTools.RTFtoHTML rtfToHtml = new TvTools.RTFtoHTML();
                rtfToHtml.rtf = eTickeTextRecord.RTFText;
                eTicketTextHtmlStr = rtfToHtml.html().Replace("MS Sans Serif", "sans-serif"); ;
            }
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TicketDoc", TagName = "div", Data = eTicketTextHtmlStr, PdfPage = cnt });

        foreach (TvReport.NovReport.FlightTicketRecord row in TicketData)
        {
            if (new TvBo.Reservation().setFlightTicketPrintUser(ResData.ResMain.ResNo, row.RecID, CustNo, eTicketNo, ref errorMsg))
            {
                TvBo.ResConRecord record = resCon.Find(f => f.CustNo == CustNo && f.ServiceID == row.RecID);
                if (record != null)
                {
                    record.DocIsDate = DateTime.Now;
                    record.DocNo = eTicketNo;
                    record.DocStat = 1;
                }
            }
        }

        HttpContext.Current.Session["ResData"] = ResData;
        return htmlData;
    }
}
