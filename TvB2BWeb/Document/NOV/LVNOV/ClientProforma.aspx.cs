﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using TvBo;
using System.Text;
using TvTools;

public partial class ClientProforma_LV : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public static string getCustomers(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        if (ResData.ResCust.Count > 0)
        {
            sb.Append("<table>");
            sb.Append("  <tr>");
            sb.Append("      <td width=\"350px\"><b>Uzvārds, vārds</b></td>");
            sb.Append("      <td width=\"150px\"><b>Dzimšanas</b></td>");
            sb.Append("      <td width=\"150px\"><b>Pases nr.</b></td>");
            sb.Append("  </tr>");
            foreach (ResCustRecord r in ResData.ResCust)
            {
                sb.Append("  <tr>");
                sb.AppendFormat("      <td>{0}</td>", r.TitleStr + "." + r.Surname + " " + r.Name);
                sb.AppendFormat("      <td>{0}</td>", r.Birtday.HasValue ? r.Birtday.Value.ToString("dd/MM/yyyy") : "&nbsp;");
                sb.AppendFormat("      <td>{0}</td>", r.PassSerie.ToString() + " " + r.PassNo.ToString());
                sb.Append("  </tr>");
            }
            sb.Append("<table>");
        }
        return sb.ToString();
    }

    public static string getPackageServices(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        List<TvReport.NovReport.ResService> resService = new TvReport.NovReport.NovReport().getResServices(UserData.Market, ResData.ResMain.ResNo, UserData.Ci, ref errorMsg);
        if (resService != null && resService.Count > 0)
        {
            sb.Append("<table width=\"100%\">");
            sb.Append("    <tr>");
            sb.Append("        <td width=\"160px\"><b>Pakalpojumi</b></td>");
            sb.Append("        <td width=\"80px\" align=\"center\"><b>Vienība</b></td>");
            sb.Append("        <td><b>Apraksts</b></td>");
            sb.Append("        <td></td>");
            sb.Append("    </tr>");
            foreach (TvReport.NovReport.ResService r in resService)
            {
                string statusStr = HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + r.StatConf.ToString()).ToString();
                sb.Append("    <tr>");
                sb.AppendFormat("        <td valign=\"top\">{0}</td>", r.Name);
                sb.AppendFormat("        <td valign=\"top\" align=\"center\">{0}</td>", r.Unit.ToString());
                sb.AppendFormat("        <td valign=\"top\">{0}</td>", r.Description.ToString());
                sb.AppendFormat("        <td valign=\"top\">{0}</td>", statusStr);
                sb.Append("    </tr>");
                sb.Append("    <tr>");
                sb.AppendFormat("        <td hight=\"5px\">{0}</td>", "&nbsp");
                sb.AppendFormat("        <td hight=\"5px\">{0}</td>", "&nbsp");
                sb.AppendFormat("        <td hight=\"5px\">{0}</td>", "&nbsp");
                sb.AppendFormat("        <td hight=\"5px\">{0}</td>", "&nbsp");
                sb.Append("    </tr>");
            }
            sb.Append("</table>");
        }
        return sb.ToString();
    }

    public static string getExtraService(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        var Query = from q1 in ResData.ResService
                    join q2 in ResData.ResServiceExt on q1.RecID equals q2.ServiceID
                    where q1.StatSer != 2 && q2.StatSer != 2
                    select new { MainService = q1.ServiceTypeNameL, Name = q2.ExtServiceNameL, BegDate = q2.BegDate, EndDate = q2.EndDate, Unit = q2.Unit, q2.Adult, q2.Child, q2.SalePrice, q2.SaleCur, q2.StatConf };
        if (Query != null && Query.Count() > 0)
        {
            sb.Append("<div style=\"height: 1px; background-color: #000000;\">");
            sb.Append("</div>");
            sb.Append("<table width=\"100%\">");
            sb.Append("    <tr>");
            sb.Append("        <td width=\"160px\"><b>Papildus pakalpojumi</b></td>");
            sb.Append("        <td width=\"80px\">&nbsp;</td>");
            sb.Append("        <td>&nbsp;</td>");
            sb.Append("    </tr>");
            foreach (var r in Query)
            {
                string statusStr = HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + r.StatConf.ToString()).ToString();
                sb.Append("    <tr>");
                sb.AppendFormat("        <td valign=\"top\">{0}</td>", r.MainService);
                sb.AppendFormat("        <td valign=\"top\" align=\"center\">{0}</td>", r.Unit.ToString());
                sb.AppendFormat("        <td valign=\"top\">{0}</td>", r.Name.ToString());
                sb.AppendFormat("        <td valign=\"top\">{0}</td>", statusStr);
                sb.Append("    </tr>");
            }
            sb.Append("</table>");
        }
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        OperatorRecord opr = new Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord currentAgency = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(currentAgency.UseMainCont ? (string.IsNullOrEmpty(currentAgency.MainOffice) ? currentAgency.Code : currentAgency.MainOffice) : currentAgency.Code, false, ref errorMsg);
        AgencyRecord agentAccount = new Agency().getAgency(currentAgency.UseMainAcc ? (string.IsNullOrEmpty(currentAgency.MainOffice) ? currentAgency.Code : currentAgency.MainOffice) : currentAgency.Code, false, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(currentAgency.Code, ref errorMsg);
        ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == (ResData.ResCust.Find(f1 => f1.Leader == "Y").CustNo));
        ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));
        BankRecord agencybank = new Banks().getBank(UserData.Market, agentAccount != null ? agentAccount.Bank1 : agent.Bank1, ref errorMsg);
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtResNo", TagName = "span", Data = ResData.ResMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtPrintTime", TagName = "span", Data = DateTime.Now.ToString("dd MMM yyyy") });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtOperator", TagName = "span", Data = opr.FirmName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtOperatorAddress", TagName = "span", Data = opr.Address + " " + opr.AddrZip + " " + opr.AddrCity });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtOpePhone", TagName = "span", Data = opr.Phone1 });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtOpeFax", TagName = "span", Data = opr.Fax1 });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtAgencyName", TagName = "span", Data = string.IsNullOrEmpty(agencyAddr.FirmName) ? agencyAddr.Name : agencyAddr.FirmName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "TxtAgencyAdress", TagName = "span", Data = agencyAddr.Address + " " + agencyAddr.AddrZip + " " + agencyAddr.AddrCity });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridTurist", TagName = "div", Data = getCustomers(UserData, ResData) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridServices", TagName = "div", Data = getPackageServices(UserData, ResData) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridExtServices", TagName = "div", Data = getExtraService(UserData, ResData) });
        decimal TotalSum = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value : 0;
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtSalePrice", TagName = "span", Data = TotalSum > 0 ? TotalSum.ToString("#,###.00") : "" });
        decimal PasSupDis = ResData.ResMain.PasSupDis.HasValue ? ResData.ResMain.PasSupDis.Value : 0;
        decimal EBPas = ResData.ResMain.EBPas.HasValue ? ResData.ResMain.EBPas.Value : 0;
        decimal EBAgency = ResData.ResMain.EBAgency.HasValue ? ResData.ResMain.EBAgency.Value : 0;
        decimal bonus = new TvReport.AllOperator().getBonus(ResData.ResMain.ResNo);
        decimal AgencyDisPasVal = ResData.ResMain.AgencyDisPasVal.HasValue ? ResData.ResMain.AgencyDisPasVal.Value : 0;
        decimal ReservationDiscount = PasSupDis - EBPas - AgencyDisPasVal - bonus;// -EBAgency; 2010-02-15 Vidas
        decimal promoQ = ResData.ResPromo.Sum(s => (s.Amount.HasValue ? s.Amount.Value : 0));
        if (promoQ > 0)
            ReservationDiscount = ReservationDiscount - promoQ;

        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDiscount", TagName = "span", Data = ReservationDiscount != 0 ? ReservationDiscount.ToString("#,###.00") : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtTotal", TagName = "span", Data = (TotalSum + ReservationDiscount).ToString("#,###.00") });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCur", TagName = "span", Data = ResData.ResMain.SaleCur });

        FixNotesRecord fixNote = new Common().getFixNotes(UserData.Market, "Contract", ref errorMsg);
        string fixNoteText = string.Empty;
        RTFtoHTML rtfHtml = new RTFtoHTML();
        if (fixNote != null)
        {
            rtfHtml.rtf = fixNote.Note;
            fixNoteText = rtfHtml.html();
        }
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtFootherText", TagName = "div", Data = fixNoteText });

        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }
}
