﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlyTicket.aspx.cs" Inherits="FlyTicket_LV" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ETicket</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <%--<script src="../../../Scripts/jquery.dimensions.js" type="text/javascript"></script>--%>

    <link href="../../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        table td { height: 21px; }
        body { line-height: 1; font-family: Tahoma Times New Roman; font-size: 8pt; width: 794px; }
        .mainPage { width: 794px; }
        .clear { clear: both; height: 1px; }
        .turistdata { font-family: Tahoma Times New Roman; font-size: 10px; width: 250px; }
        .header { text-align: center; font-size: 16px; font-family: Tahoma Times New Roman; font-weight: bold; padding-top: 20px; z-index: 10; }
        .info { padding-left: 10px; padding-right: 10px; width: 682px; z-index: 20; }
        .FieldLabel { font-size: 10px; font-family: Tahoma Times New Roman; }
        .w200px { width: 230px; }
        .w123px { width: 150px; }
        .SeperatorDiv { height: 30px; }
        #noPrintDiv { width: 794px; text-align: center; }
        #custListDiv { text-align: left; font-size: 10pt; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function showTicket(CustNo) {
            $("#param1").val(CustNo.toString());
            $.ajax({
                async: false,
                type: "POST",
                url: "FlyTicket.aspx/showTicket",
                data: '{"CustNo":' + CustNo + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d')) {
                        var data = msg.d;
                        $(".mainPage").show();
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data);
                                    $("#" + this.IdName).height();
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    if (this.Data != '')
                                        $("#" + this.IdName).attr("src", this.Data);
                                    else $("#" + this.IdName).hide();
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                        $.query = $.query.load(location.href);
                        if (!$.query.get('print')) {
                            var width = $(".mainPage").width();
                            var height = $(".mainPage").height();
                            var param1 = $("#param1").val();
                            self.parent.viewReportPrint(location.href + "&print=1&param1=" + CustNo, width, height);
                        }
                        getFormData();
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "FlyTicket.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $("#custListDiv").html('');
                        $("#custListDiv").html(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $.query = $.query.load(location.href);

                if ($.query.get('param1')) {
                    var custNo = parseInt($.query.get('param1'));
                    showTicket(custNo);
                }
                else
                    getFormData();

                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
                else
                    self.parent.setViewReportSrc(location.href);
            });
    </script>

</head>
<body>
    <form id="ticketForm" runat="server">
    <input id="param1" type="hidden" value="" />
    <div id="noPrintDiv">
        <div id="custListDiv">
            &nbsp;
        </div>
    </div>
    <div class="mainPage" style="display: none;">
        <div style="height: auto;">
            <table style="height: 80px; width: 100%;">
                <tr>
                    <td valign="top">
                        <img alt="" title="" id="imgOperator" height="60px" src="" />
                    </td>
                    <td valign="top" width="50%">
                        <img alt="" title="" id="imgAirlines" height="60px" src="" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
        </div>
        <div>
            <div class="header">
                Ceļojuma informācija (Travel information)
            </div>
            <div class="header">
                (ELECTRONIC TICKET PASSENGER ITINERARY)
            </div>
        </div>
        <div class="info" style="z-index: 5; height: auto; width: 97%; border: solid 1px #BBB; height: 140px;">
            <br />
            <div style="clear: both; height: auto;">
                <div style="float: left; width: 240px; height: 34px;">
                    PASAŽIERA UZVĀRDS, VĀRDS:<br />
                    (NAME OF PASSENGER)
                </div>
                <div style="float: left; height: 34px;">
                    <span id="txtTurist" class="turistdata"></span>
                </div>
            </div>
            <%--<div style="clear: both;">
                <div style="float: left; width: 240px; font-weight: bold; height: 34px;">
                    REZERVACIJOS NR.:<br />
                    (RESERVATION NO.)
                </div>
                <div style="float: left; font-weight: bold; height: 34px;">
                    <span id="txtResNo1" class="turistdata"></span>
                </div>
            </div>--%>
            <div style="clear: both;">
                <div style="float: left; width: 240px; font-weight: bold; height: 34px;">
                    ELEKTRONISKĀS BIĻETES NR.:<br />
                    (ELECTRONIC TICKET NUMBER)
                </div>
                <div style="float: left; height: 34px;">
                    <span id="txtETicketNo" class="turistdata"></span>
                </div>
            </div>
            <div style="clear: both;">
                <div style="float: left; width: 240px; height: 34px;">
                    IZDOŠANAS VIETA:<br />
                    (ISSUED BY)
                </div>
                <div style="float: left; height: 34px;">
                    <span id="txtIssuedBy" class="turistdata"></span>
                </div>
            </div>
            <div style="clear: both;">
                <div style="float: left; width: 240px; height: 34px;">
                    IZDOŠANAS DATUMS:<br />
                    (DATE OF ISSUE)
                </div>
                <div style="float: left; height: 34px;">
                    <span id="txtDateOfIssue" class="turistdata"></span>
                </div>
            </div>
            <br />
        </div>
        <br />
        <div style="width: 100%;">
            <div id="gridTicket">
            </div>
        </div>
        <div style="width: 100%;">
            <div id="gridExtService">
            </div>
        </div>
        <div style="width: 100%;" lang="lv">
            <br />
            <div id="TicketDoc">
            </div>
            <br />
        </div>
    </div>
    </form>
</body>
</html>
