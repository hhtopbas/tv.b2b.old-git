﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BusContrat.aspx.cs" Inherits="BusContrat_LV" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contrat</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <link href="../../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Arial; font-size: 8pt; width: 794px; }
        .clear { clear: both; height: 1px; }
        .h22px { height: 22px; }
        .line333 { color: #333; }
        .line666 { color: #BBB; }
        .mainPage { width: 794px; border: solid 1px #BBB; }
        .logo_headerDiv { width: 100%; height: 76px; }
        .mainContent { width: 100%; }
        .operatorAdress { width: 100%; height: 36px; }
        .arial6 { font-family: Arial; font-size: 6pt; }
        .arial8 { font-family: Arial; font-size: 8pt; }
        .arial9 { font-family: Arial; font-size: 9pt; }
        .arial10 { font-family: Arial; font-size: 10pt; }
        .arial11 { font-family: Arial; font-size: 11pt; }
        .arial11b { font-family: Arial; font-size: 11pt; font-weight: bold; }
        .arial12 { font-family: Arial; font-size: 12pt; }
        .agencyInfo { width: 100%; }
        .agencyInfo strong { font-family: Arial; font-size: 11pt; }
        .touristDetail { width: 100%; }
        .gridTourist { width: 100%; padding-left: 5px; }
        .hotelHeaderDiv { width: 100%; }
        .inputNotes { border-bottom: 1px solid #C0C0C0; height: 21px; border-left-style: none; border-right-style: none; border-top-style: none; }
        .printStyle { color: #000000; border: 1px solid #BBB; }
        .priceDiv { width: 100%; }
        .priceBottom table { border-collapse: collapse; }
        .priceBottom table, th, td { border: 1px solid #BBB; }
        .priceBottom td { height: 25px; }
        .priceBottom .colInfo { background-color: #ffffff; white-space: nowrap; }
        .headerTable { height: 100%; width: 100%; }
        .headerTable table { border-collapse: collapse; }
        .headerTable table, th, td { border: none; }
        #gridPriceSum table, th, td { border: 1px solid #BBB; }
        #gridPriceSum td { height: 22px; padding: 0px 4px 0px 3px; }
        .hotelGridCss { border-collapse: collapse; border-spacing: 0; border: none; width: 100%; }
        .hotelGridCss td { height: 25px; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "BusContrat.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                        $("#gridPriceTable").width();
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                getFormData();
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var docName = $.query.get('docName');
                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
                else {
                    self.parent.setViewReportSrc(location.href);
                    try {
                        self.parent.viewReportRemoveButton(docName);
                    }
                    catch (err) {
                    }
                    var baseUrl = $.url(location.href);
                    self.parent.viewReportAddButton('<%= GetGlobalResourceObject("LibraryResource", "btnPrintNext") %>', function() { window.location = baseUrl.data.attr.base + baseUrl.data.attr.directory + 'AgreementNext.aspx?ResNo=' + resNo + '&docName=' + docName; });
                }
            });
    </script>

</head>
<body>
    <form id="contratForm" runat="server">
    <div class="mainPage">
        <div class="logo_headerDiv">
            <table class="headerTable" style="width: 100%; height: 100%;">
                <tr>
                    <td style="width: 125px; text-align: center;" valign="middle">
                        <img id="OprLogo" alt="" title="" src="" width="110px" height="55px" />
                    </td>
                    <td style="text-align: center; font-size: 13pt; font-weight: bold;" valign="middle">
                        TŪRISMA PAKALPOJUMU SNIEGŠANAS LĪGUMS No&nbsp<span id="ResNo0"></span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="mainContent">
            <div class="operatorAdress">
                <div style="margin-left: 3px; margin-bottom: 3px;">
                    Tūrisma pakalpojumu sniegšanas līgums (turpmāk tekstā – Līgums) noslēgts starp tūrisma
                    aģentūra &quot;<strong><span id="AgencyName0"></span></strong> &quot;, vienotais
                    reg Nr.<strong><span id="txtAgencyRegNo"></span></strong> &nbsp;( reģ.nr. tūrisma
                    pakalpojumu sniedzēju datu bāzē <strong><span id="TxtLisanceNo"></span></strong>
                    ), juridiskā adrese <strong><span id="TxtAgencyAddress"></span></strong>&nbsp;,
                    kuras vārdā šo līgumu paraksta aģentūras darbinieks no vienas puses
                </div>
            </div>
        </div>
        <br />
        <hr size="1" class="line666" />
        <div class="agencyInfo">
            <div class="h22px">
                <strong>Līguma datums:</strong><span id="ResDate"></span>
            </div>
            <div class="h22px" style="width: 100%;">
                <div style="width: 49%; float: left;">
                    <strong>Aģentūra: </strong><span id="AgencyName"></span>
                </div>
                <div style="width: 49%; float: left;">
                    <strong>Aģentūras darbinieks: </strong><span id="AgencyUserName"></span>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="h22px">
                <div style="width: 25%; float: left;">
                    <strong>Fakss: </strong><span id="Fax"></span>
                </div>
                <div style="width: 30%; float: left;">
                    <strong>Tālrunis: </strong><span id="Tel"></span>
                </div>
                <div style="width: 43%; float: left;">
                    <strong>E-pasts: </strong><span id="Email"></span>
                </div>
            </div>
        </div>
        <hr size="1" class="line666" />
        <div class="touristDetail">
            <strong class="arial8" style="margin-left: 5px;">INFORMACIJA PAR TŪRISTU (-IEM)</strong>
            <br />
            <div id="gridTourist">
                gridTourist
            </div>
            <span class="arial8"><b>Papildus informācija par klientu (-iem)</b></span><i> (adrese,
                tālrunis, e-pasts</i> : <span id="txtLeaderInfo"></span>
        </div>
        <hr size="1" class="line666" />
        <div class="hotelHeaderDiv">
            <div style="width: 49%; float: left;">
                <strong>Autobusu tūre: </strong><span id="PackageLocalName" class="arial12"></span>
            </div>
            <div style="width: 25%; float: left;">
                <strong>Izbraukšanas datums: </strong><span id="BeginDate" class="a arial12"></span>
            </div>
            <div style="width: 25%; float: left;">
                <strong>Atgriešanās datums: </strong><span id="EndDate" class="arial12"></span>
            </div>
            <div class="clear">
            </div>
            <div>
                <strong>Papildus ērtības : </strong><span class="arial10">kondicionieris
                    <input type="checkbox" id="Checkbox1" class="printStyle" />; WC
                    <input type="checkbox" id="Checkbox2" class="printStyle" />; Video
                    <input type="checkbox" id="Checkbox3" class="printStyle" />; karsto dzērienu automāts
                    <input type="checkbox" id="Checkbox4" class="printStyle" />; saskaņā ar līguma pielikumiem
                    <input type="checkbox" id="Checkbox5" checked="checked" class="printStyle" />.</span>
            </div>
            <div id="gridHotel">
                gridHotel
            </div>
            <div style="height: 40px;">
                <div style="width: 155px; height: 40px; float: left;">
                    <strong>Mītnes tips: </strong>
                </div>
                <div style="width: 605px; float: left;">
                    <input id="note1" type="text" class="inputNotes" style="width: 600px;" /><br />
                    <i>(viesnīca, motelis, hostelis, privāts sektors, cits)</i>
                </div>
            </div>
            <div class="clear">
            </div>
            <div>
                <div>
                    <strong>Aprīkojums : </strong><span class="arial10">numuriņā
                        <input type="checkbox" id="cb1" class="printStyle" />; korpuss
                        <input type="checkbox" id="cb2" class="printStyle" />; koridoris
                        <input type="checkbox" id="cb3" class="printStyle" />; teritorija
                        <input type="checkbox" id="cb4" class="printStyle" />; saskaņā ar līguma pielikumiem
                        <input type="checkbox" id="cb5" checked="checked" class="printStyle" />.</span>
                </div>
                <div>
                    <strong>Lūdzu pievērsiet uzmanību:</strong> tūrists, kurš ceļo viens un nav rezervējis
                    vienvietīgu numuru, tiks izmitināts ar citiem grupas tūristiem divvietīgā, dievvietīgā
                    ar papildus gultu vai trīsvietīgā numurā.
                </div>
                <div>
                    <strong>Citi pakalpojumi, kas iekļauti brauciena cenā: </strong><span class="arial10">
                        saskaņā ar līguma pielikumiem
                        <input type="checkbox" id="cb10" checked="checked" class="printStyle" />.</span>
                </div>
                <div>
                    <strong>Ekskursijas, kas iekļautas brauciena cenā : </strong><span class="arial10">saskaņā
                        ar līguma pielikumiem
                        <input type="checkbox" id="cb11" checked="checked" class="printStyle" />.</span>
                </div>
            </div>
            <div class="clear">
            </div>
            <div>
                <div style="width: 230px; height: 40px; float: left;">
                    <strong>Tūrista īpašas vēlmes: </strong>
                </div>
                <div style="width: 560px; float: left;">
                    <input id="note2" type="text" class="inputNotes" style="width: 525px;" /><br />
                    <i>(tiek nodrošināts, ja ir iespējams) (numuri blakus, precīza numura atrašanās un tml.)</i>
                </div>
            </div>
            <div class="clear">
            </div>
            <div>
                <div style="width: 160px; float: left; height: 18px;">
                    <strong>Speciāli noteikumi: </strong>
                </div>
                <div style="width: 625px; float: left;">
                    <input id="note3" type="text" class="inputNotes" style="width: 595px; height: 18px;" />
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="clear" style="height: 2px;">
            </div>
            <div style="width: 100%;">
                <div>
                    <div class="arial9" style="width: 220px; float: left; height: 20px;">
                        <strong>Sākotnējā izbraukšanas vieta:</strong>
                    </div>
                    <div style="text-align: left; width: 550px; float: left; height: 20px;" class="arial11">
                        <span id="DepAirPort" class="arial9"></span>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <hr size="1" class="line666" />
        <div class="clear">
        </div>
        <div class="priceDiv">
            <div id="gridPriceSum">
            </div>
            <table cellpadding="0" cellspacing="0px" border="0" width="100%">
                <tr>
                    <td align="center" class="priceBottom" colspan="3">
                        <table style="border-width: 1px; border-collapse: collapse; border-spacing: 0; border-color: #BBB;
                            text-align: left;" width="100%">
                            <tr>
                                <td class="arial10 colInfo" style="width: 300px;">
                                    Kopējā pasūtījuma cena LVL:
                                </td>
                                <td style="width: 80px; text-align: center;">
                                    <span id="TotalCost"></span>
                                </td>
                                <td class="arial10" align="center">
                                    <b>Maksājumi</b>
                                </td>
                            </tr>
                            <tr>
                                <td class="arial10">
                                    Maksājuma _____________________ Apmaksāts
                                </td>
                                <td style="text-align: center;">
                                    &nbsp;
                                </td>
                                <td class="arial8" align="center">
                                    <input id="cb20" type="checkbox" class="printStyle" />
                                    Skaidrā naudā
                                    <input id="cb21" type="checkbox" class="printStyle" />
                                    Karte
                                    <input id="cb22" type="checkbox" class="printStyle" />
                                    Pārskaitījums
                                </td>
                            </tr>
                            <tr>
                                <td class="arial10">
                                    Maksājuma _____________________ Apmaksāts
                                </td>
                                <td align="center">
                                    &nbsp;
                                </td>
                                <td class="arial8" align="center">
                                    <input id="cb30" type="checkbox" class="printStyle" />
                                    Skaidrā naudā
                                    <input id="cb31" type="checkbox" class="printStyle" />
                                    Karte
                                    <input id="cb32" type="checkbox" class="printStyle" />
                                    Pārskaitījums
                                </td>
                            </tr>
                            <tr>
                                <td class="arial10" style="text-align: left;">
                                    <b><span id="lblDiscountTotal">Atlaide LVL: </span></b>
                                </td>
                                <td class="arial10" align="left" style="padding-left: 10px;" colspan="2">
                                    <b><span id="IndirimToplami"></span></b>
                                </td>
                            </tr>
                            <tr>
                                <td class="arial10" style="text-align: left; white-space: nowrap;">
                                    <b><span id="lblTotal">Kopējā pasūtījuma cena LVL: </span></b>
                                </td>
                                <td class="arial10" align="left" style="padding-left: 10px;" colspan="2">
                                    <b><span id="KalanToplam"></span></b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" valign="bottom" align="left" class="arial9" bgcolor="#ffffff">
                        <input id="cb40" type="checkbox" checked="checked" />
                        Es apstiprinu, ka tiku informēts, par iespēju iegādāties apdrošināšanas polisi,
                        lai segtu iespējamos izdevumus brauciena atcelšanas gadījumā vai sakarā ar sniegto
                        palīdzību, tajā skaitā tepatriāciju, saslimšanas vai nelaimes gadījumā, kā arī ar
                        polises izmantošanas noteikumiem.
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="right" class="arial9" bgcolor="#ffffff" style="width: 33%;">
                        &nbsp;
                    </td>
                    <td valign="top" align="right" class="arial9" bgcolor="#ffffff" style="width:33%;">
                        &nbsp;
                    </td>
                    <td style="width: 33%;">
                        <b>TŪRISTS: </b>
                        <br />
                        <b>Paraksts: </b>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
