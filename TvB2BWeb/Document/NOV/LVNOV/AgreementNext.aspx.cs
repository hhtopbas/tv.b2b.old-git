﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using TvBo;
using System.Web.Script.Services;

public partial class AgreementNext_LT : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static TvReport.NovReport.formDataAgreement getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null)
            return new TvReport.NovReport.formDataAgreement();
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        string basePageUrl = TvBo.WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        //object obj = new TvReport.NovReport.NovReport().getContrat2Page(ResData.ResMain.HolPack, ref errorMsg);
        TvBo.ContractTextRecord contractText = new TvBo.Common().getContractText(ResData.ResMain.HolPack, ref errorMsg);
        if (contractText != null) {
            string contractTextStr = string.Empty;
            if (string.Equals(contractText.UseContText, "R") && !string.IsNullOrEmpty(contractText.ContractText)) {
                TvTools.RTFtoHTML rtfToHtml = new TvTools.RTFtoHTML();
                rtfToHtml.rtf = contractText.ContractText;
                contractTextStr = rtfToHtml.html();
                TvReport.NovReport.formDataAgreement rV = new TvReport.NovReport.formDataAgreement { retVal = contractTextStr, retType = contractText.UseContText };
                return rV;
            } else
                if (string.Equals(contractText.UseContText, "H") && !string.IsNullOrEmpty(contractText.ContractHtml)) {
                TvReport.NovReport.formDataAgreement rV = new TvReport.NovReport.formDataAgreement { retVal = contractText.ContractHtml, retType = contractText.UseContText };
                return rV;
            } else
                    if (contractText.ContractPict != null) {
                string wordDocument = string.Empty;
                string holPackFileName = string.Join(ResData.ResMain.HolPack, contractText.ChgDate.HasValue ? contractText.ChgDate.Value.Ticks.ToString() : DateTime.Now.Ticks.ToString());
                wordDocument = TvBo.UICommon.WriteBinaryFile(contractText.ContractPict, holPackFileName, "", !string.IsNullOrEmpty(contractText.ContractDocType) && string.Equals(contractText.ContractDocType.ToLower(), ".pdf") ? "pdf" : (string.Equals(contractText.UseContText, "R") ? "rtf" : (string.Equals(contractText.UseContText, "W") ? "doc" : "rtf")));
                if (!string.IsNullOrEmpty(wordDocument)) {
                    wordDocument = TvBo.WebRoot.BasePageRoot + "ACE/" + wordDocument;
                    if (!string.IsNullOrEmpty(contractText.ContractDocType) && string.Equals(contractText.ContractDocType.ToLower(), ".pdf"))
                        contractText.UseContText = "P";
                    else
                        contractText.UseContText = "W";
                    TvReport.NovReport.formDataAgreement rV = new TvReport.NovReport.formDataAgreement { retVal = string.Format("<br /><h2><a href=\"{0}\" onclick=\"window.open('{0}','external');return false;\"><img src=\"../../../Images/download_icon.png\" />&nbsp;Download document</a></h2><br />", wordDocument), retType = contractText.UseContText };
                    return rV;
                } else {
                    contractTextStr = "<br /><strong>document not found</strong><br />";
                    TvReport.NovReport.formDataAgreement rV = new TvReport.NovReport.formDataAgreement { retVal = "<br /><strong>document not found</strong><br />", retType = contractText.UseContText };
                    return rV;
                }
            } else {
                contractTextStr = "<br /><strong>document not found</strong><br />";
                TvReport.NovReport.formDataAgreement rV = new TvReport.NovReport.formDataAgreement { retVal = "<br /><strong>document not found</strong><br />", retType = contractText.UseContText };
                return rV;
            }
        } else {
            TvReport.NovReport.formDataAgreement rV = new TvReport.NovReport.formDataAgreement { retVal = "<br /><strong>document not found</strong><br />", retType = "H" };
            return rV;
        }
    }
}
