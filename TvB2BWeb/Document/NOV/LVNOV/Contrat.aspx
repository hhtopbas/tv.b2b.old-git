﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contrat.aspx.cs" Inherits="Contrat_LV" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contrat</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Arial; font-size: 8pt; width: 794px; }
        .clearfix:after { content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0; }
        .clear { clear: both; height: 1px; }
        .h22px { height: 22px; }
        .line333 { color: #333; }
        .line666 { color: #BBB; }
        .mainPage { width: 794px; border: solid 1px #BBB; }
        .logo_headerDiv { width: 100%; height: 76px; }
        .mainContent { width: 100%; }
        .operatorAdress { width: 100%; }
        .arial6 { font-family: Arial; font-size: 6pt; }
        .arial7 { font-family: Arial; font-size: 7pt; }
        .arial8 { font-family: Arial; font-size: 8pt; }
        .arial9 { font-family: Arial; font-size: 9pt; }
        .arial10 { font-family: Arial; font-size: 10pt; }
        .arial11 { font-family: Arial; font-size: 11pt; }
        .arial11b { font-family: Arial; font-size: 11pt; font-weight: bold; }
        .arial12 { font-family: Arial; font-size: 12pt; }
        .agencyInfo { width: 100%; }
        .agencyInfo strong { font-family: Arial; font-size: 11pt; }
        .touristDetail { width: 100%; }
        .gridTourist { width: 100%; padding-left: 5px; }
        .hotelHeaderDiv { width: 100%; }
        .inputNotes { border-bottom: 1px solid #C0C0C0; height: 21px; border-left-style: none; border-right-style: none; border-top-style: none; }
        .printStyle { color: #000000; border: 1px solid #BBB; }
        .priceDiv { width: 100%; }
        .priceBottom table { border-collapse: collapse; }
        .priceBottom table, th, td { border: 1px solid #BBB; }
        .priceBottom td { height: 25px; }
        .priceBottom .colInfo { background-color: #ffffff; white-space: nowrap; }
        .headerTable { height: 100%; width: 100%; }
        .headerTable table { border-collapse: collapse; }
        .headerTable table, th, td { border: none; }
        #gridPriceSum table, th, td { border: 1px solid #BBB; }
        #gridPriceSum td { height: 22px; padding: 0px 4px 0px 3px; }
        .hotelGridCss { border-collapse: collapse; border-spacing: 0; border: none; width: 100%; }
        .hotelGridCss span { font-size: 10pt; font-weight: bold; }
        .hotelGridCss td { height: 25px; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Contrat.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                        //$("#gridPriceTable").width();                       
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $('input:checkbox').click(function() {
                    if ($(this).attr("checked") != "checked") {
                        $(this).removeAttr("checked");
                    }
                    else {
                        $(this).attr("checked", "checked");
                    }
                });

                getFormData();
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var docName = $.query.get('docName');
                if ($.query.get('print')) {
                    $('input:checkbox').removeAttr('checked');
                    $.each($.query.keys, function(i) {
                        if (i != 'ResNo' && i != 'docName' && i != 'print' && i != '__VIEWSTATE') {
                            var elm = $("#" + i);
                            if (this == 'on' || this == 'off') {
                                if (this == 'on')
                                    elm.attr('checked', 'checked');
                                else
                                    elm.removeAttr('checked');
                            }
                            else if (!(this == true || this == false)) {
                                elm.val(this);
                            }
                        }
                    });
                    window.print();
                    window.close();
                }
                else {
                    self.parent.setViewReportSrc(location.href);
                    try {
                        self.parent.viewReportRemoveButton(docName);
                    }
                    catch (err) {
                    }
                    var baseUrl = $.url(location.href);
                    self.parent.viewReportAddButton('<%= GetGlobalResourceObject("LibraryResource", "btnPrintNext") %>', function() { window.location = baseUrl.data.attr.base + baseUrl.data.attr.directory + 'AgreementNext.aspx?ResNo=' + resNo + '&docName=' + docName; });
                }
            });
    </script>

</head>
<body>
    <form id="contratForm" runat="server">
    <input id="param1" type="hidden" value="" />
    <div class="mainPage">
        <div class="logo_headerDiv">
            <table class="headerTable" style="width: 100%; height: 100%;">
                <tr>
                    <td style="width: 125px; text-align: center;" valign="middle">
                        <img id="OprLogo" alt="" title="" src="" width="110px" height="55px" />
                    </td>
                    <td style="text-align: center; font-size: 14pt; font-weight: bold;" valign="middle">
                        TŪRISMA PAKALPOJUMU SNIEGŠANAS LĪGUMS No <span id="ResNo0"></span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="mainContent">
            <div class="operatorAdress clearfix">
                <div style="float: left; width: 50%; border-bottom: solid 1px #AAA; margin-bottom: 0px;
                    position: relative;">
                    <span id="ResDate">ResDate</span>
                </div>
                <div class="arial11b" style="width: 50%; float: left;">
                    Rezervācijas Nr.<span id="ResNo"></span>
                </div>
                <div style="float: left; width: 100%; margin-left: 3px; margin-bottom: 3px;">
                    Tūrisma pakalpojumu sniegšanas līgums (turpmāk tekstā – Līgums) noslēgts starp tūrisma
                    aģentūra &quot;<strong><span id="AgencyName"></span></strong> &quot;, vienotais
                    reg Nr.<strong><span id="txtAgencyRegNo"></span></strong> &nbsp;( reģ.nr. tūrisma
                    pakalpojumu sniedzēju datu bāzē <strong><span id="TxtLisanceNo"></span></strong>
                    ), juridiskā adrese <strong><span id="TxtAgencyAddress"></span></strong>&nbsp;,
                    kuras vārdā šo līgumu paraksta aģentūras darbinieks no vienas puses
                </div>
            </div>
        </div>
        <hr size="1" class="line666" />
        <div class="agencyInfo">
            <div class="h22px">
                <strong>Aģentūras darbinieks: </strong><span id="AgencyUserName"></span>
            </div>
            <div class="clear">
            </div>
            <div class="h22px arial8">
                <div style="width: 25%; float: left;">
                    <strong>Fakss: </strong><span id="Fax"></span>
                </div>
                <div style="width: 30%; float: left;">
                    <strong>Tālrunis: </strong><span id="Tel"></span>
                </div>
                <div style="width: 43%; float: left;">
                    <strong>E-pasts: </strong><span id="Email"></span>
                </div>
            </div>
        </div>
        <hr size="1" class="line666" />
        <div class="touristDetail">
            <strong class="arial8" style="margin-left: 5px;">INFORMACIJA PAR TŪRISTU (-IEM)</strong>
            <br />
            <div id="gridTourist">
                gridTourist
            </div>
            <span class="arial8"><b>Papildus informācija par klientu (-iem):</b></span><i> (adrese,
                tālrunis, e-pasts)</i> :
            <div class="clear">
            </div>
            <span id="txtLeaderInfo"></span>
        </div>
        <hr size="1" class="line666" />
        <div class="hotelHeaderDiv">
            <div style="width: 49%; float: left;">
                <strong>Ceļojuma galamērķis: </strong><span id="PackageLocalName" class="arial10">
                </span>
            </div>
            <div style="width: 25%; float: left;">
                <strong>Izbraukšanas datums: </strong><span id="BeginDate" class="arial10"></span>
            </div>
            <div style="width: 25%; float: left;">
                <strong>Atgriešanās datums: </strong><span id="EndDate" class="arial10"></span>
            </div>
            <div>
                <strong>Produkts: </strong><span id="Products" class="arial10"></span>
            </div>
            <div id="gridHotel">
                gridHotel
            </div>
            <div class="clear">
            </div>
            <div>
                <div>
                    <strong>Cenā iekļauti papildus pakalpojumi : </strong><span class="arial10">transfērs
                        lidosta-viesnīca
                        <input type="checkbox" id="cb1" name="cb1" class="printStyle" checked="checked" />; transfērs
                        viesnīca-lidosta
                        <input type="checkbox" id="cb2" name="cb2" class="printStyle" checked="checked" />; </span>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="arial8">
                <div style="width: 180px; height: 40px; float: left;">
                    <strong>Klienta īpašās vēlmes: </strong>
                    <br />
                    <span style="font-style: italic;" class="arial6">
                        <br />
                        (tiek izpildītas iespēju robežās)</span>
                </div>
                <div style="width: 610px; float: left;">
                    <input id="note2" name="note2" type="text" class="inputNotes" style="width: 600px;" /><br />
                    <span style="font-style: italic;" class="arial6">(numuri blakus, precīza numura atrašanās
                        vieta utt.)</span>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="arial8">
                <div style="width: 35%; float: left;">
                    <strong>Izlidošanas laiks: </strong><span id="DepFlightTime" class="arial12"></span>
                </div>
                <div style="width: 64%; float: left;">
                    <strong>Pārvadātājs: </strong><span id="depRetAirline" class="arial12"></span>
                    <%--OBLIGÃTI precizēt izlidošanas laiku vienu dienu pirms izlidošanas!:--%>
                </div>
            </div>
            <div class="clear" style="height: 2px;">
            </div>
            <div class="arial7">
                (Līgumā un ceļojumu dokumentos norādītais izlidošanas laiks ir provizorisks un līdz
                faktiskajai ceļojuma dienai var mainīties. Ne tūrisma operators, ne avikompānija,
                kas izpilda reisu, negarantē šo laiku. Norādītajiem izlidošanas laikiem ir tikai
                informatīvs raksturs un ne vēlāk kā vienu dienu pirms lidojuma tas jāprecizē mājas
                lapā www.novatours.lv)
                <br />
            </div>
            <div class="clear" style="height: 2px;">
            </div>
            <div style="width: 100%;" class="arial8">
                <div>
                    <div class="arial9" style="width: 220px; float: left; height: 20px;">
                        <strong>Izlidošanas vieta:</strong>
                    </div>
                    <div style="text-align: left; width: 550px; float: left; height: 20px;" class="arial11">
                        <span id="DepAirPort" class="arial9"></span>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div>
                    <div class="arial9" style="width: 220px; float: left; height: 20px;">
                        <strong>Ielidošanas vieta:</strong>
                    </div>
                    <div style="text-align: left; width: 550px; float: left; height: 20px;" class="arial11">
                        <span id="ArrAirPort" class="arial9"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <hr size="1" class="line666" />
        <div class="clear">
        </div>
        <div class="priceDiv" class="arial8">
            <div id="gridPriceSum">
            </div>
            <table cellpadding="0" cellspacing="0px" border="0" width="100%">
                <tr>
                    <td align="center" class="priceBottom arial8" colspan="3">
                        <table style="border-width: 1px; border-collapse: collapse; border-spacing: 0; border-color: #BBB;
                            text-align: left;" width="100%">
                            <tr>
                                <td class="colInfo" style="width: 300px;">
                                    <span id="lblOrderPrice">Kopējā pasūtījuma cena LVL</span>
                                </td>
                                <td style="width: 80px; text-align: center;">
                                    <span id="TotalCost"></span>
                                </td>
                                <td align="center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span id="lblAdvancePayment">Samaksāta priekšapmaksa LVL</span>
                                </td>
                                <td style="text-align: center;">
                                    &nbsp;
                                </td>
                                <td align="center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span id="lblPaid">Apmaksāts LVL</span>
                                </td>
                                <td align="center">
                                    &nbsp;
                                </td>
                                <td class="arial8" align="center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <b><span id="lblDiscount">Atlaide LVL: </span></b>
                                </td>
                                <td class="arial10" align="left" style="padding-left: 10px;" colspan="2">
                                    <b><span id="IndirimToplami"></span></b>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; white-space: nowrap;">
                                    <b><span id="lblTotalOrderPrice1">Kopējā pasūtījuma cena LVL: </span></b>
                                </td>
                                <td align="left" style="padding-left: 10px;" colspan="2">
                                    <b><span id="KalanToplam"></span></b>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; white-space: nowrap;">
                                    <b><span id="lblTotalOrderPrice2">Kopējā pasūtījuma cena EUR: </span></b>
                                </td>
                                <td align="left" style="padding-left: 10px;" colspan="2">
                                    <b><span id="totalEur"></span></b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--<tr>
                    <td colspan="3" valign="bottom" align="left" class="arial9" bgcolor="#ffffff">
                        Visu pakalpojumu kopējā cena ar atlaidi, LVL:
                    </td>
                </tr>--%>
                <tr>
                    <td colspan="3" style="height: 5px;">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td style="width: 360px; border-bottom: solid 1px #000000;">
                        AGENTŪRA:
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td style="border-bottom: solid 1px #000000;">
                        TŪRISTS:
                    </td>
                </tr>
                <tr>
                    <td>
                        vārds,uzvārds paraksts Z.V.
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td>
                        vārds,uzvārds paraksts
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
