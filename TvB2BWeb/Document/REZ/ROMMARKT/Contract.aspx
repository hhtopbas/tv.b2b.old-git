﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contract.aspx.cs" Inherits="Rezeda_Contract" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contrat</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <link href="../../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        body { width: 700px; }
        .mainPage { width: 700px; border: solid 1px #BBB; }
        @media print {
            #noPrint { display: none; }
        }
        body { font-family: Arial; font-size: 9pt; }
    </style>

    <script type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Contract.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                        $("#gridPriceTable").width();
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                getFormData();
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var docName = $.query.get('docName');
                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="font-family: Times New Roman;" class="mainPage">
        <table width="100%" style="font-size: 11pt;">
            <tr>
                <td align="center" style="font-weight: bold;">
                    CONTRACT<br />
                    de comercializare a pachetelor de servicii turistice
                    <br />
                    Nr. <span id="txtResNo"></span> din <span id="txtDate"></span>
                </td>
                <td align="center">
                    <img alt="" src="RezLogo.jpg" width="100px" />
                </td>
            </tr>
        </table>
        <br />
        <div>
            Părţile contractante<br />
            Societatea Comercială SC REZEDA WORLD TRAVEL SRL, cu sediul în Bucuresti, Str Tunari,
            nr 83, sector 2, cod unic de înregistrare RO 24787696, titulară a Licenţei de turism
            Tour Operatoare nr. 5226, pentru Agenţia de turism SC REZEDA WORLD TRAVEL SRL, cu
            sediul în Bucuresti, Str Tunari Nr 83, sector 2, reprezentată prin dl. Abdulhamit
            Gazez, în calitate de administrator, denumită în continuare Agenţia,<br />
            Şi Turistul/Reprezentantul turistului<br />
            Domnul/Doamna <span id="txtLeaderName"></span><span id="txtLeaderSurName"></span>
            domiciliat/domiciliată în <span id="txtLeaderAddr"></span><span id="LeaderAddrZip">
            </span><span id="txtLeaderAddrCity"></span>, telefon <span id="txtLeaderMobTel">
            </span>posesor/posesoare al/a buletinului/cărţii de identitate seria <span id="txtLeaderPassSerie">
            </span>, nr. <span id="txtLeaderPassNo"></span>eliberat/eliberată de <span id="txtResNo1">
            </span>la data <span id="txtDate1"></span>,au convenit la incheierea prezentului
            contract<br />
            I. Obiectul contractului îl constituie vanzarea de catre Agenţie a pachetului de
            servicii înscris în voucher, bilet de odihnă – tratament, bilet de excursie, anexat
            la prezentul contract şi eliberarea documentelor de plată.
            <br />
            II Pretul<br />
            1. Preţul contractului este <span id="txtPasPayable1"></span>şi cuprinde costul
            serviciilor turistice efective, comisionul Agenţiei şi T.V.A. Avansul este de <span
                id="txtPasPayable"></span>.lei iar plata finală se va face pana la data de <span
                    id="txtDueDate"></span>. .
            <br />
            III. Drepturile şi obligaţiile Agenţiei<br />
            1. În cazul în care Agenţia este nevoită să modifice una dintre prevederile esenţiale
            ale contractului, are obligaţia să informeze turistul cu cel puţin 15 zile înainte
            de data plecării.<br />
            2. Poate sa modifice pretul contractului in sensul majorarii sau micsorarii, dupa
            caz, numai daca modificarea are loc ca urmare a variatiilor costurilor de transport,
            a redeventelor si a taxelor aferente serviciilor de aterizare, debarcare/imbarcare
            in porturi si aeroporturi si ale taxelor de turist ori a cursurilor de schimb valutar
            aferente pachetului de servicii turistice contractat;
            <br />
            3. În cazul în care, după începerea călătoriei turistice, o parte importantă din
            serviciile turistice prevăzute în contract nu este realizată sau agenţia de turism
            organizatoare constată că nu le va putea realiza, Agenţia este obligată:<br />
            a) să ofere turistului alternative corespunzătoare în vederea continuării călătoriei
            turistice, fără majorarea preţului, respectiv serviciile turistice oferite să fie
            de aceeaşi calitate şi cantitate;<br />
            b) să restituie turistului sumele ce reprezintă diferenţa dintre serviciile turistice
            achitate şi cele efectiv prestate în timpul călătoriei turistice;<br />
            c) în cazul în care nu pot fi oferite turistului alternative corespunzătoare sau
            acesta nu le acceptă din motive întemeiate, să asigure fără costuri suplimentare
            transportul retur al turistului la locul de plecare ori în alt loc agreat de acesta
            şi, după caz, despăgubirea pentru serviciile neprestate.<br />
            4. Agenţia este răspunzătoare pentru buna executare a obligaţiilor asumate prin
            contract, cu excepţia următoarelor cazuri:<br />
            a) când neîndeplinirea sau îndeplinirea defectuoasă a obligaţiilor asumate prin
            contract se datorează turistului;<br />
            b) când neîndeplinirea obligaţiilor se datorează unor cauze de forţă majoră, sau
            unor imprejurari pe care nici agentia de turism, nici prestatorii de servicii nu
            le puteau prevede sau evita (modificari de orar sau itinerar, intarzieri in traficul
            mijloacelor de transport etc).<br />
            5. Agenţia are obligaţia să furnizeze în scris turistului, în termen de 3 zile înainte
            de data plecării, următoarele informaţii:<br />
            a) orarele, locurile escalelor şi legăturile, precum şi, după caz, locul ce urmează
            să fie ocupat de turist în fiecare dintre mijloacele de transport incluse în contract;<br />
            b) denumirea, sediul/adresa, numerele de telefon şi de fax ale reprezentanţei locale
            a organizatorului şi/sau a detailistului ori, în lipsa acesteia, un număr de apel
            de urgenţă care să îi permită contactarea organizatorului şi/sau a detailistului;<br />
            c) pentru călătoriile minorilor neînsoţiţi de părinţi, informaţii care să permită
            parintilor stabilirea unui contact direct cu copilul sau cu responsabilul de la
            locul cazării copilului;<br />
            IV. Drepturile şi obligaţiile turistului<br />
            1. În cazul în care turistul nu poate să participe la călătoria turistică, acesta
            poate să cesioneze contractul unei terţe persoane care îndeplineşte toate condiţiile
            aplicabile pachetului de servicii turistice contractat, cu obligaţia de a anunţa
            in scris Agenţia cu cel putin 5 zile inaintea datei de plecare. In acest caz Agentia
            reziliaza contractul cu turistul care cesioneaza contractul si incheie un alt contract
            cu noul turist. Pentru calatoriile individuale, in cazul in care mijlocul de transport
            este avionul, transferul poate fi efectuat numai daca exista posibilitatea transferarii
            locului de zbor. Turistul care cedează pachetul său de servicii, precum şi cesionarul
            sunt responsabili în mod solidar la plata preţului călătoriei şi a eventualelor
            costuri suplimentare apărute cu ocazia acestei cedări.<br />
            2. In cazul sejururilor de odihna si / sau de tratament, turistul are obligatia
            sa respecte programul de acordare a serviciilor in Romania, respectiv : in statiunile
            de pe litoral, cazarea se face la ora 18.00 a zilei de intrare si se termina la
            ora 12.00 a zilei inscrise pe voucher ori pe biletul de odihna si / sau de tratament
            ; in statiunile din tara, altele decat cele de pe litoral, cazarea se face incepand
            cu ora 12.00 a zilei de intrare si se termina cel tarziu la ora 12.00 a zilei urmatoare
            celei inscrise pe bilet.<br />
            3. În cazul în care preţurile stabilite în contract sunt majorate cu peste 10%,
            turistul poate rezilia contractul, având dreptul la rambursarea de către Agenţie
            a sumelor plătite.<br />
            4.1. Turistul este obligat să comunice Agenţiei, în termen de 5 zile calendaristice
            de la primirea înştiinţării prevăzute la cap. III pct. 1, hotărârea sa de a opta
            pentru:<br />
            a) rezilierea contractului fără plata penalităţilor; sau<br />
            b) acceptarea noilor condiţii ale contractului.<br />
            4.2. În cazul în care turistul reziliază contractul sau Agenţia anulează călătoria
            turistică înaintea datei de plecare, turistul are dreptul:<br />
            a) să accepte la acelaşi preţ un alt pachet de servicii turistice de calitate echivalentă
            sau superioară, propus de Agenţie;<br />
            b) să accepte un pachet de servicii turistice de calitate inferioară, propus de
            Agenţie, cu rambursarea imediată a diferenţei de preţ;<br />
            c) să i se ramburseze imediat toate sumele achitate în virtutea contractului.<br />
            4.3. În toate cazurile menţionate turistul are dreptul să solicite Agenţiei şi o
            despăgubire pentru neîndeplinirea prevederilor contractului inţial, cu excepţia
            cazurilor în care:<br />
            a) anularea s-a făcut datorită nerealizării numărului minim de persoane menţionat
            în contract, iar Agenţia a informat in scris turistul cu cel putin 15 zile calendaristice
            premergătoare datei plecării;<br />
            b) anularea s-a datorat unui caz de forţă majoră (circumstanţe imprevizibile, independente
            de voinţa celui care le invocă şi ale căror consecinţe nu au putut fi evitate în
            ciuda oricăror eforturi depuse, în aceasta nefiind incluse suprarezervările, caz
            in care responsabilitatea revine companiei aeriene);<br />
            c) anularea s-a făcut din vina turistului.<br />
            5. Turistul are dreptul să rezilieze în orice moment, în tot sau în parte, contractul,
            iar în cazul în care rezilierea îi este imputabilă, este obligat să despăgubească
            Agenţia pentru prejudiciul creat acesteia, cu excepţia cazurilor de forţă majoră
            definite conform legii.<br />
            Dacă turistul, solicită schimbarea hotelului, structurii camerelor sau a oricărora
            dintre servicii, aceasta echivalează cu rezilierea contractului, cu aplicarea penalităţilor
            legale la momentul respectiv, şi încheierea unui nou contract.<br />
            6. Turistul este obligat să achite la recepţia unităţii hoteliere taxa de staţiune,
            taxa de salubrizare, precum şi alte taxe locale, fără a putea pretinde despăgubiri
            sau returnarea sumelor de la Agenţie.<br />
            7. Turistul este obligat să prezinte la recepţia unităţii hoteliere actele sale
            de identitate, precum şi documentul de călătorie eliberat (voucher, bilet de odihnă
            şi/sau de tratament) în vederea acordării serviciilor turistice.<br />
            V. Renunţări, penalizări, despăgubiri<br />
            1. În cazul în care turistul renunţă din vina sa la pachetul de servicii turistice
            care face obiectul prezentului contract, el datorează Agenţiei penalizări după cum
            urmează:<br />
            a) 10.% din preţul pachetului de servicii, dacă renunţarea se face cu mai mult de
            30 de zile calendaristice înainte de data plecării;<br />
            b) 50.% din preţul pachetului de servicii, dacă renunţarea se face în intervalul
            16 - 30 de zile înainte de data plecării;<br />
            c) 100% din preţul pachetului de servicii, dacă renunţarea se face într-un interval
            mai mic de 16 zile înainte de plecare sau pentru neprezentarea la program.<br />
            2. Pentru biletele de odihnă şi/sau de tratament cumpărate prin organizaţii sindicale,
            Agenţia va face restituiri numai în baza cererilor de renunţare contrasemnate şi
            ştampilate de reprezentantul organizaţiei sindicale.<br />
            3. În cazul în care o ambasadă refuză să acorde viza de intrare pentru efectuarea
            pachetului de servicii, turistului i se vor reţine toate taxele achitate de Agenţie
            prestatorilor direcţi, precum şi cheltuielile de operare proprii acesteia.<br />
            4. În cazul în care turistul care a intrat pe teritoriul statului în care se realizează
            pachetul de servicii turistice refuză să se mai întoarcă în România şi autorităţile
            din ţara respectivă fac cheltuieli de orice natură cu acesta, turistul respectiv
            are obligaţia de a suporta toate aceste cheltuieli.<br />
            5. Penalizările echivalente cu preţul contractului se aplică şi în cazul în care
            turistul nu ajunge la timp la aeroport sau la locul de plecare, dacă nu poate pleca
            în călătorie pentru că nu are actele în regulă sau dacă este întors de la graniţă
            de către poliţia de frontieră.<br />
            6. Turistul trebuie să depună în scris cererea de renunţare la pachetul de servicii
            turistice, cu număr de înregistrare la Agenţia la care a achitat serviciile. În
            caz contrar cererea de renunţare nu este luată în considerare.<br />
            7. Agenţia va acorda despăgubiri în funcţie de gradul de nerespectare a obligaţiilor
            din contract.<br />
            VI. Reclamaţii<br />
            1. În cazul în care turistul este nemulţumit de serviciile turistice primite, acesta
            are obligaţia de a întocmi o sesizare în scris, clar şi explicit cu privire la deficienţele
            constatate la faţa locului, legate de realizarea pachetului de servicii turistice
            contractat, ce se va transmite prompt atât Agenţiei, cât şi prestatorului de servicii
            (conducerii hotelului, restaurantului).<br />
            2. Atât Agenţia, cât şi prestatorul de servicii vor acţiona imediat pentru soluţionarea
            sesizării. În cazul în care sesizarea nu este soluţionată sau este soluţionată parţial,
            turistul va depune la sediul Agenţiei o reclamaţie în scris, în termen de maximum
            3 zile calendaristice de la încheierea călătoriei, Agenţia urmând ca în termen de
            2 zile calendaristice să comunice turistului despăgubirile care i se cuvin.<br />
            VII. Asigurari - Turistul este asigurat pentru rambursarea cheltuielilor de repatriere
            si / sau a sumelor achitate de turist in cazul insolvabilitatii sau falimentului
            agentiei de turism la Societatea de asigurare <span id="txtAvanceAmount"></span>
            <span id="txtAvanceCur"></span>din localitatea <span id="txtLeaderAddr1"></span>
            <span id="txtLeaderAddrZip1"></span><span id="txtLeaderAddrCity1"></span>tel. <span
                id="txtLeaderMobTel1"></span>Facultativ, turistul are posibilitatea incheierii
            unui contract de asigurare, care sa acopere taxele de transfer sau a unui contract
            de asistenta, care sa acopere taxele de repatriere in caz de accidente, de boala
            sau deces ori a unui contract de asigurare pentru bagaje.<br />
            VIII. Documentele contractului, se constituie ca anexa la acesta si sunt urmatoarele
            :<br />
            a) voucherul, biletul de odihna-tratament, biletul de excursie, dupa caz ;<br />
            b) programul turistic, in cazul actiunilor turistice.<br />
            XI. Dispoziţii finale<br />
            1 Prezentul contract a fost încheiat în două exemplare, câte unul pentru fiecare
            parte.<br />
            2.Comercializarea pachetelor de servicii turitice se va face in conformitate cu
            prevederile prezentului contract si cu respectarea prevederilor Oordonantei Guvernului
            107/1999 privind activitatea de comercializare a pachetelor de servicii turistice
            aprobata cu modificari si completari prin Legea 631/2001 cu modificarile ulterioare.<br />
            3. Contractul poate fi prezentat şi sub forma unui catalog, pliant sau alt înscris,
            dacă turistul este informat despre aceasta şi daca documentul conţine informaţiile
            prevăzute de art 10 alin. (2) Ordonanţei Guvernului nr. 107/1999 privind comercializarea
            pachetelor de servicii turistice, aprobată cu modificări şi completări prin Legea
            nr. 631/2001, cu modificarile ulterioare. Prezentul contract a fost incheiat in
            2 (doua) exemplare, cate unul pentru fiecare parte, astazi,
            <br />
            <br />
        </div>
        <table width="100%">
            <tr>
                <td align="center" valign="top">
                    SC REZEDA WORLD TRAVEL SRL<br />
                    <span id="txtUserName"></span>
                </td>
                <td align="center" valign="top">
                    Turist
                    <br />
                    Am citit contractul si anexele si am
                    <br />
                    primit un exemplar al acestora
                    <br />
                    <span id="txtLeaderName2"></span><span id="txtLeaderSurName2"></span>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <p style="text-align: center; font-weight: bold; font-size: 11pt; text-decoration: underline;">
            Bon de comanda No : <span id="txtResNo4"></span>Data : <span id="txtDate4"></span>
        </p>
        <table width="100%" cellspacing="1" style="border: solid 2px #666; height: 250px;">
            <tr>
                <td valign="top" style="border-right: solid 2px #666;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 248px;">
                        <tr>
                            <td valign="top" style="height: 208px;">
                                <div id="custGridDiv" style="width: 100%;">                                    
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FFCF6A; color: #000; height: 20px; font-weight: bold;">
                                Structura pax :
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 20px;">
                                Adulti: <span id="txtAdult"></span>&nbsp;&nbsp; Copii: <span id="txtChild"></span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 34%;" valign="top">
                    <div style="background-color: #FFCF6A; height: 20px;">
                        <strong>Titular:</strong>
                    </div>
                    <div style="height: 20px;">
                        <span id="txtLeaderName4"></span>&nbsp;<span id="txtLeaderSurname4"></span>
                    </div>
                    <div style="background-color: #FFCF6A; height: 20px;">
                        <strong>Adresa:</strong>
                    </div>
                    <div style="height: 100px;">
                        <span id="txtLeaderAddr4"></span>
                        <br />
                        <span id="txtLeaderAddrZip4"></span>
                        <br />
                        <span id="txtLeaderAddrCity4"></span>
                        <br />
                        <br />
                        <strong>Telefon: </strong><span id="txtMobPhone"></span>
                    </div>
                    <div style="background-color: #FFCF6A; height: 20px;">
                        &nbsp;
                    </div>
                    <div>
                        <strong style="font-style: italic;">Data acceptului: </strong>
                        <br />
                        <br />
                        <strong style="font-style: italic;">Semnatura: </strong>
                    </div>
                </td>
            </tr>
        </table>
        <br />
        <div id="bottomDiv">
        </div>
        <table>
            <tr>
                <td colspan="2">
                    In cazul in care turistul nu primeste confirmarea rezervarii in cel mult 10 zile
                    de la data semnarii bonului de comanda, acesta are dreptul la rambursarea imediata
                    a sumelor platite (inclusiv comisionul agentiei). Turist reprezentant [Reservation."LeaderName"]
                    [Reservation."LeaderSurName"] declar pe proprie raspundere ca reprezint cu puteri
                    depline toti turistii inscrisi in bonul de comanda si semnez in numele meu si in
                    numele acestora. Am primit un exemplar din contract.
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" style="width: 50%;">
                    Semnatura agentului de turism<br />
                    <span id="txtUserName4" style="font-weight: bold;"></span>
                </td>
                <td align="center" valign="top">
                    Semnatura turistului
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
