﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using TvBo;
using System.Threading;
using System.Text;
using TvTools;
using System.Web.Script.Services;


public partial class Rezeda_Contract : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ResNo = string.Empty;

        if (!IsPostBack)
        {

        }
    }

    public static string getServiceGrid(User UserData, ResDataRecord ResData, decimal? avanceAmount, TvReport.RezedaReport.Payments payment, List<resPayPlanRecord> paymentPlan)
    {
        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;
        List<TvReport.RezedaReport.rezedaResService> resServices = new TvReport.RezedaReport.RezedaReport().getResServices(UserData.Market, ResData.ResMain.ResNo, ref errorMsg);
        List<TvReport.RezedaReport.rezedaResServiceExt> resServiceExt = new TvReport.RezedaReport.RezedaReport().getResServiceExt(UserData.Market, ResData.ResMain.ResNo, ref errorMsg);
        sb.Append("<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border: solid 2px #666;\">");
        sb.Append(" <tr>");
        sb.AppendFormat("<td colspan=\"5\" style=\"background-color: #FFCF6A; height: 20px;text-align:center;\">{0}</td>",
                "Servicii Turistice Solicitate :");
        sb.Append(" </tr>");

        sb.Append(" <tr>");
        sb.AppendFormat("<td>{0}</td>", "Servicii");
        sb.AppendFormat("<td style=\"width:70px;\">{0}</td>", "&nbsp;");
        sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", "Unit");
        sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", "Adulti");
        sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", "Copii");
        sb.Append(" </tr>");
        if (resServices != null && resServices.Count > 0)
        {
            foreach (TvReport.RezedaReport.rezedaResService row in resServices)
            {
                sb.Append(" <tr>");
                sb.AppendFormat("<td>{0}</td>", row.ServiceDesc);
                sb.AppendFormat("<td style=\"width:70px;\">{0}</td>", string.IsNullOrEmpty(row.AccomFullName) ? "&nbsp;" : row.AccomFullName);
                sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", row.Unit.ToString());
                sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", row.Adult.ToString());
                sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", row.Child.ToString());
                sb.Append(" </tr>");
            }
        }
        if (resServiceExt != null && resServiceExt.Count > 0)
        {
            sb.Append(" <tr>");
            sb.AppendFormat("<td colspan=\"5\" style=\"background-color: #FFCF6A; height: 20px;text-align:center;\">{0}</td>",
                    "Informatii Extra Servicii :");
            sb.Append(" </tr>");

            sb.Append(" <tr>");
            sb.AppendFormat("<td colspan=\"2\">{0}</td>", "Extra Servicii");
            sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", "Unit");
            sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", "Adulti");
            sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", "Copii");
            sb.Append(" </tr>");

            foreach (TvReport.RezedaReport.rezedaResServiceExt row in resServiceExt)
            {
                sb.Append(" <tr>");
                sb.AppendFormat("<td colspan=\"2\">{0}</td>", row.ServiceDesc);
                sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", row.Unit.ToString());
                sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", row.Adult.ToString());
                sb.AppendFormat("<td style=\"width:35px;\">{0}</td>", row.Child.ToString());
                sb.Append(" </tr>");
            }
        }

        sb.Append(" <tr>");
        string amountTable = string.Empty;
        amountTable += "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">";
        amountTable += "<tr>";
        amountTable += string.Format("<td style=\"width:150px;\"><strong>{0}</strong></td><td style=\"width:5px;\">:</td><td><strong>{1}</strong></td>",
                                "Pret servicii",
                                ResData.ResMain.PasPayable.HasValue ? (ResData.ResMain.PasPayable.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : "");
        amountTable += "</tr>";
        amountTable += "<tr>";
        amountTable += string.Format("<td style=\"width:150px;\"><strong>{0}</strong></td><td style=\"width:5px;\">:</td><td><strong>{1}</strong></td>",
                                "Avans",
                                avanceAmount.HasValue ? ("Suma " + (avanceAmount.HasValue ? avanceAmount.Value.ToString("#,###.00") + " " + (payment != null ? payment.PaidCur.ToString() : "") : "") + "/ Data " + (payment != null ? (payment.PayDate.HasValue ? payment.PayDate.Value.ToShortDateString() : "") : "") + " /Mod de plata " + (payment != null ? payment.PayTypeName.ToString() : "")) : "");
        amountTable += "</tr>";

        //amountTable += "</table>";
        //sb.AppendFormat("<td colspan=\"5\" style=\"\">{0}</td>",
        //        amountTable);
        //sb.Append(" </tr>");
        decimal? restdeplate = null;
        decimal pasPayable = ResData.ResMain.PasPayable.HasValue ? ResData.ResMain.PasPayable.Value : Convert.ToDecimal(0);
        restdeplate = pasPayable - (avanceAmount.HasValue ? avanceAmount.Value : Convert.ToDecimal(0));
        amountTable += string.Format("<td style=\"width:150px;\"><strong>{0}</strong></td><td style=\"width:5px;\">:</td><td><strong>{1}</strong></td>",
                                "Rest de plata :",
                                "Suma " + (restdeplate.HasValue ? restdeplate.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "") + "/ Data " + (paymentPlan != null ? paymentPlan.FirstOrDefault().DueDate.ToShortDateString() : ""));
        amountTable += "</tr>";
        amountTable += "</table>";
        sb.AppendFormat("<td colspan=\"5\" style=\"border-top: solid 1px #666;\">{0}</td>",
                amountTable);
        sb.Append(" </tr>");
        sb.AppendFormat("<td colspan=\"5\" style=\"border-top: solid 2px #666;\">{0}</td>",
@"Regim pasapoarte / vize:<br />
Formalitati sanatate/asigurari:                                               Asigurari facultative:<br />
Conditii:<br />
Cereri facultative(fara plata si fara confirmare):" + ResData.ResMain.ResNo +
@"<br />
Documente necesare minorilor: <b>Imputernicire notariala de accept pentru parasirea tarii, din partea parintilor care nu il insotesc si cazier judiciar al celui care insoteste minorul fara parinti</b><br />
Numar minim persoane necesar pt. efectuarea programului: <b>70% din capacitatea mijlocului de transport</b> <br />
Data limita de anuntare a turistului privind  anularea calatoriei: <b>15 zile inainte de plecare</b> ");
        sb.Append(" </tr>");
        sb.Append("</table>");
        return sb.ToString().Replace('"', '!');
    }

    public static string getCustomerGrid(User UserData, ResDataRecord ResData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
        sb.Append(" <tr style=\"background-color: #FFCF6A;\">");
        sb.AppendFormat("<td style=\"height: 20px;\">{0}</td><td style=\"height: 20px;\">{1}</td><td style=\"height: 20px;\">{2}</td><td style=\"height: 20px;\">{3}</td>",
                "Nume si prenume",
                "Data nastere",
                "Varsta",
                "Pasaport");
        sb.Append(" </tr>");
        foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0))
        {
            sb.Append(" <tr>");
            sb.AppendFormat("<td style=\"height: 20px;\">{0}</td><td style=\"height: 20px; width:70px;\">{1}</td><td style=\"height: 20px; width: 40px; text-align: center;\">{2}</td><td style=\"height: 20px;width: 100px;\">{3}</td>",
                row.Name + " " + row.Surname,
                row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "&nbsp;",
                row.Age.HasValue ? row.Age.ToString() : "&nbsp;",
                row.PassSerie.ToString() + " " + row.PassNo.ToString());
            sb.Append(" </tr>");
        }
        sb.Append("</table>");
        return sb.ToString().Replace('"', '!');
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static List<TvReport.htmlCodeData> getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return new List<TvReport.htmlCodeData>();
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        StringBuilder sb = new StringBuilder();
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, false, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);
        ResMainRecord resMain = ResData.ResMain;
        var leaderCustNo = ResData.ResCust.Where(w => string.Equals(w.Leader, "Y"));
        ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));
        if (leader == null) return htmlData;
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
        if (_leader == null) return htmlData;
        string leaderInvAddress = string.Empty;
        if (_leader != null)
            leaderInvAddress = !Equals(_leader.InvoiceAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip;

        List<resPayPlanRecord> paymentPlan = new ReservationMonitor().getPaymentPlan(resMain.ResNo, ref errorMsg);
        DateTime? dueDate = null;
        if (paymentPlan != null && paymentPlan.Count > 0)
            dueDate = paymentPlan.OrderByDescending(o => o.DueDate).Select(s => s.DueDate).FirstOrDefault();
        decimal? avanceAmount = new TvReport.RezedaReport.RezedaReport().getAvanceAmount(resMain.ResNo, ref errorMsg);
        List<TvReport.RezedaReport.Payments> payments = new TvReport.RezedaReport.RezedaReport().getPayments(resMain.ResNo, ref errorMsg);

        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo", TagName = "span", Data = resMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDate", TagName = "span", Data = DateTime.Today.ToShortDateString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderName", TagName = "span", Data = leader.Name });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderSurName", TagName = "span", Data = leader.Surname });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddr", TagName = "span", Data = !string.Equals(_leader.ContactAddr, "W") ? _leader.AddrHome.ToString() : _leader.AddrWork.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "LeaderAddrZip", TagName = "span", Data = !string.Equals(_leader.ContactAddr, "W") ? _leader.AddrHomeZip.ToString() : _leader.AddrWorkZip.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddrCity", TagName = "span", Data = !string.Equals(_leader.ContactAddr, "W") ? _leader.AddrHomeCity.ToString() : _leader.AddrWorkCity.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderMobTel", TagName = "span", Data = _leader.MobTel.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderPassSerie", TagName = "span", Data = leader.PassSerie.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderPassNo", TagName = "span", Data = leader.PassNo.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo1", TagName = "span", Data = resMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDate1", TagName = "span", Data = DateTime.Today.ToShortDateString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPasPayable1", TagName = "span", Data = resMain.PasPayable.HasValue ? resMain.PasPayable.Value.ToString("#,###.00") : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPasPayable", TagName = "span", Data = resMain.PasPayable.HasValue ? resMain.PasPayable.Value.ToString("#,###.00") : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDueDate", TagName = "span", Data = dueDate.HasValue ? dueDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAvanceAmount", TagName = "span", Data = avanceAmount.HasValue ? avanceAmount.Value.ToString("#,###.00") : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAvanceCur", TagName = "span", Data = payments != null && payments.Count > 0 ? payments.FirstOrDefault().PaidCur : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddr1", TagName = "span", Data = !string.Equals(_leader.ContactAddr, "W") ? _leader.AddrHome.ToString() : _leader.AddrWork.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddrZip1", TagName = "span", Data = !string.Equals(_leader.ContactAddr, "W") ? _leader.AddrHomeZip.ToString() : _leader.AddrWorkZip.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddrCity1", TagName = "span", Data = !string.Equals(_leader.ContactAddr, "W") ? _leader.AddrHomeCity.ToString() : _leader.AddrWorkCity.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderMobTel1", TagName = "span", Data = _leader.MobTel.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtUserName", TagName = "span", Data = UserData.UserName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderName2", TagName = "span", Data = leader.Name });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderSurName2", TagName = "span", Data = leader.Surname });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo4", TagName = "span", Data = resMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtDate4", TagName = "span", Data = DateTime.Today.ToShortDateString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAdult", TagName = "span", Data = resMain.Adult.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtChild", TagName = "span", Data = resMain.Child.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderName4", TagName = "span", Data = leader.Name });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderSurname4", TagName = "span", Data = leader.Surname });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddr4", TagName = "span", Data = !string.Equals(_leader.ContactAddr, "W") ? _leader.AddrHome.ToString() : _leader.AddrWork.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddrZip4", TagName = "span", Data = !string.Equals(_leader.ContactAddr, "W") ? _leader.AddrHomeZip.ToString() : _leader.AddrWorkZip.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddrCity4", TagName = "span", Data = !string.Equals(_leader.ContactAddr, "W") ? _leader.AddrHomeCity.ToString() : _leader.AddrWorkCity.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtMobPhone", TagName = "span", Data = _leader.MobTel.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "bottomDiv", TagName = "div", Data = getServiceGrid(UserData, ResData, avanceAmount, payments.FirstOrDefault(), paymentPlan) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtUserName4", TagName = "span", Data = UserData.UserName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "custGridDiv", TagName = "div", Data = getCustomerGrid(UserData, ResData) });
        return htmlData;
    }
}
