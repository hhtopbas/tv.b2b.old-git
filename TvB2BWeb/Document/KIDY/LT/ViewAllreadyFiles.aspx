﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewAllreadyFiles.aspx.cs" Inherits="ViewAllreadyFiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>ETicket</title>

  <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="../../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>
  <link href="../../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />

  <style type="text/css">
        
    </style>

  <script language="javascript" type="text/javascript">

    function logout() {
      self.parent.logout();
    }

    function selectedView(recID, fileType) {
      var params = new Object();
      params.recID = recID;
      params.fileType = fileType;
      $.ajax({
        async: false,
        type: "POST",
        url: "ViewAllreadyFiles.aspx/getDocument",
        data: $.json.encode(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          document.location = msg.d.FileName;
        },
        error: function (xhr, msg, e) {
          alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getFormData() {
      $.ajax({
        async: false,
        type: "POST",
        url: "ViewAllreadyFiles.aspx/getFormData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != '') {
            $("#invoiceList").html('');
            $("#invoiceList").html(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          alert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(
        function () {
          getFormData();
        });
  </script>

</head>
<body>
  <form id="ticketForm" runat="server" class="ui-helper-reset"> 
    <input id="param1" type="hidden" value="" />
    <iframe id="txtArea1" style="display: none"></iframe>
    <div id="noPrintDiv" class="ui-widget" style="width: 100%; text-align: center;">
      <div id="invoiceList" class="ui-helper-clearfix">
      </div>
    </div>

  </form>
</body>
</html>
