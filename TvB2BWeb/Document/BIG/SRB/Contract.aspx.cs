﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using TvBo;
using System.Threading;
using System.Text;
using TvTools;
using System.Web.Script.Services;


public partial class BigBlue_Contract : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    public static string getResCustDiv(User UserData, ResDataRecord ResData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
        sb.Append(" <tr>");
        sb.AppendFormat("<td style=\"height: 20px; font-style: italic;\">{0}</td><td style=\"height: 20px;font-style: italic;\">{1}</td><td style=\"height: 20px;font-style: italic;\">{2}</td>",
                "Ime prezime",
                "Datum rođenja",
                "Godine");
        sb.Append(" </tr>");
        foreach (ResCustRecord row in ResData.ResCust.Where(w => w.Status == 0))
        {
            sb.Append(" <tr>");
            sb.AppendFormat("<td style=\"height: 20px;\">{0}</td><td style=\"height: 20px; width:70px;\">{1}</td><td style=\"height: 20px; width: 40px; text-align: center;\">{2}</td>",
                row.Name + " " + row.Surname,
                row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "&nbsp;",
                row.Age.HasValue ? row.Age.ToString() : "&nbsp;");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString().Replace('"', '!');
    }

    public static string getHotelsDiv(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        if (ResData.ResService.Where(w => w.ServiceType == "HOTEL").Count() > 0)
        {
            foreach (ResServiceRecord row in ResData.ResService.Where(w => w.ServiceType == "HOTEL"))
            {
                HotelRecord hotel = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
                sb.Append("<br />");
                sb.Append("<table width=\"100%\" style=\"border: solid 1px #666;\">");
                string category = string.Empty;
                switch (hotel.Category)
                {
                    case "2STARS": category = "2*"; break;
                    case "2STARSP": category = "2*+"; break;
                    case "3STARS": category = "3*"; break;
                    case "3STARSP": category = "3*+"; break;
                    case "4STARS": category = "4*"; break;
                    case "4STARSP": category = "4*+"; break;
                    case "5STARS": category = "5*"; break;
                    case "APART": category = "APARTMAN"; break;
                    case "HV1": category = "HOLIDAY VILLAGE HV1"; break;
                    case "HV2": category = "HOLIDAY VILLAGE HV2"; break;
                    case "KRSTARENJE": category = "KRSTARENJE"; break;
                    case "VILLA": category = "VILLA"; break;                    
                }                
                sb.AppendFormat("<tr><td style=\"font-style: italic; width: 130px;\">Smeštaj:</td><td colspan=\"3\">{0}</td></tr>",
                    hotel.Name + " " + category + " , " + hotel.LocationName);
                sb.AppendFormat("<tr><td style=\"font-style: italic;\">Prijava - Odjava:</td><td>{0}</td><td style=\"width: 80px; font-style: italic;\">Broj noći:</td><td style=\"width: 80px;\">{1}</td></tr>",
                    (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "") + " - " + (row.EndDate.HasValue ? row.EndDate.Value.ToShortDateString() : ""),
                    row.Duration.ToString());
                var query = from q1 in ResData.ResCust
                            join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                            where q2.ServiceID == row.RecID
                            select q1;
                Int16 child = row.Child.HasValue ? row.Child.Value : Convert.ToInt16(0);
                Int16 inf = child > 0 ? Convert.ToInt16(query.Where(w => w.Title > 7).Count()) : Convert.ToInt16(0);
                child = Convert.ToInt16(child - inf);
                sb.AppendFormat("<tr><td style=\"font-style: italic;\">Broj osoba:</td><td colspan=\"3\">{0}</td></tr>",
                    "Odrasli:&nbsp;<b>" + row.Adult.ToString() + "</b>&nbsp;&nbsp;Deca:&nbsp;<b>" + child.ToString() + "</b>&nbsp;&nbsp;Inf:&nbsp;<b>" + inf.ToString() + "</b>");
                sb.AppendFormat("<tr><td style=\"font-style: italic;\">Tip sobe:</td><td>{0}</td><td style=\"font-style: italic;\">Broj soba:</td><td>{1}</td></tr>", 
                    row.RoomName,
                    row.Unit.ToString());                                
                sb.AppendFormat("<tr><td style=\"font-style: italic;\">Tip usluge:</td><td colspan=\"3\">{0}</td></tr></table>", 
                    row.BoardName);                
            }
        }
        return sb.ToString().Replace('"', '!');
    }

    public static string getPasPriceGrid(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        List<TvReport.BigBlueReport.pasPrice> pasPrice = new TvReport.BigBlueReport.BigBlueReport().getPasPrice(ResData.ResMain.ResNo, ref errorMsg);
        StringBuilder sb = new StringBuilder();
        if (pasPrice != null && pasPrice.Count > 0)
        {
            sb.Append("<table width=\"100%\">");
            sb.Append("<tr style=\"background-color: #eee;\">");
            sb.AppendFormat("<td>{0}</td>", "Opis usluga");
            sb.AppendFormat("<td style=\"width: 40px;\">{0}</td>", "Enota");
            sb.AppendFormat("<td style=\"width: 90px;\">{0}</td>", "cena na enoto");
            sb.AppendFormat("<td style=\"width: 90px;\">{0}</td>", "Cena");
            sb.AppendFormat("<td style=\"width: 50px;\">{0}</td>", "Valuta");
            sb.Append("</tr>");
            decimal total = 0;
            foreach (TvReport.BigBlueReport.pasPrice row in pasPrice)
            {
                sb.Append("<tr>");
                sb.AppendFormat("<td>{0}</td>", row.ServiceDesc);
                sb.AppendFormat("<td style=\"width: 40px;\">{0}</td>", row.Unit.ToString());
                sb.AppendFormat("<td style=\"width: 90px;\">{0}</td>", row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "");
                sb.AppendFormat("<td style=\"width: 90px;\">{0}</td>", row.SalePrice.HasValue && row.Unit.HasValue ? (row.Unit.Value * row.SalePrice.Value).ToString("#,###.00") : "");
                total += row.SalePrice.HasValue && row.Unit.HasValue ? (row.Unit.Value * row.SalePrice.Value) : 0;
                sb.AppendFormat("<td style=\"width: 50px;\">{0}</td>", ResData.ResMain.SaleCur);
                sb.Append("</tr>");
            }
            sb.Append("<tr>");
            sb.AppendFormat("<td style=\"border-top: solid 1px #999;\">{0}</td>", "&nbsp;");
            sb.AppendFormat("<td colspan=\"2\" style=\"text-align: right;border-top: solid 1px #999;\"><b>{0}<b></td>", "Ukupno za uplatu:");
            sb.AppendFormat("<td style=\"width: 90px;border-top: solid 1px #999;\">{0}</td>", total.ToString("#,###.00"));
            sb.AppendFormat("<td style=\"width: 50px;border-top: solid 1px #999;\">{0}</td>", ResData.ResMain.SaleCur);
            sb.Append("</tr>");
            sb.Append("</table>");
        }
        return sb.ToString().Replace('"', '!');
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static List<TvReport.htmlCodeData> getFormData() 
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return new List<TvReport.htmlCodeData>();
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        StringBuilder sb = new StringBuilder();
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, false, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, false, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);
        ResMainRecord resMain = ResData.ResMain;
        var leaderCustNo = ResData.ResCust.Where(w => string.Equals(w.Leader, "Y"));
        ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));
        if (leader == null) return htmlData;
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
        if (_leader == null) return htmlData;
        string leaderInvAddress = string.Empty;
        if (_leader != null)
            leaderInvAddress = !Equals(_leader.InvoiceAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip;

        List<resPayPlanRecord> paymentPlan = new ReservationMonitor().getPaymentPlan(resMain.ResNo, ref errorMsg);
        DateTime? dueDate = null;
        if (paymentPlan != null && paymentPlan.Count > 0)
            dueDate = paymentPlan.OrderByDescending(o => o.DueDate).Select(s => s.DueDate).FirstOrDefault();
        decimal? avanceAmount = new TvReport.RezedaReport.RezedaReport().getAvanceAmount(resMain.ResNo, ref errorMsg);
        List<TvReport.RezedaReport.Payments> payments = new TvReport.RezedaReport.RezedaReport().getPayments(resMain.ResNo, ref errorMsg);

        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });        
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo", TagName = "span", Data = resMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridCustDiv", TagName = "div", Data = getResCustDiv(UserData, ResData) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLaderName", TagName = "span", Data = leader.Name + " " + leader.Surname });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddr", TagName = "span", Data = !Equals(_leader.InvoiceAddr, "W") ? _leader.AddrHome : _leader.AddrWork });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddrZip", TagName = "span", Data = !Equals(_leader.InvoiceAddr, "W") ? _leader.AddrHomeZip : _leader.AddrWorkZip });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderAddrCity", TagName = "span", Data = !Equals(_leader.InvoiceAddr, "W") ? _leader.AddrHomeCity : _leader.AddrWorkCity });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderPhone", TagName = "span", Data = !Equals(_leader.InvoiceAddr, "W") ? _leader.AddrHomeTel : _leader.AddrWorkTel });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtLeaderMobile", TagName = "span", Data = _leader.MobTel.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridHotelsDiv", TagName = "div", Data = getHotelsDiv(UserData, ResData) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNote", TagName = "span", Data = resMain.ResNote.ToString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "gridServicesDiv", TagName = "div", Data = getPasPriceGrid(UserData, ResData) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtRegisterDate", TagName = "span", Data = resMain.ResDate.HasValue ? resMain.ResDate.Value.ToShortDateString() : "" });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtUserName", TagName = "span", Data = UserData.UserName });
        return htmlData;
    }
}
