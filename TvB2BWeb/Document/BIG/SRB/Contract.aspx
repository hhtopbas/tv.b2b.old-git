﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contract.aspx.cs" Inherits="BigBlue_Contract" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contrat</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <link href="../../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        body { width: 700px; }
        .mainPage { width: 700px; }
        @media print {
            #noPrint { display: none; }
        }
        body { font-family: Arial; font-size: 9pt; }
    </style>

    <script type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Contract.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        if (data.length > 0) {
                            $.each(data, function(i) {
                                switch (this.TagName) {
                                    case 'span':
                                        $("#" + this.IdName).text('');
                                        $("#" + this.IdName).text(this.Data);
                                        break;
                                    case 'div':
                                        $("#" + this.IdName).html('');
                                        $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                        break;
                                    case 'img':
                                        $("#" + this.IdName).removeAttr("src");
                                        $("#" + this.IdName).attr("src", this.Data);
                                        break;
                                    default: $("#" + this.IdName).val('');
                                        $("#" + this.IdName).val(this.Data);
                                }
                            });
                            $("#gridPriceTable").width();
                        }
                        else {
                            $(".mainPage").hide();
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                getFormData();
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var docName = $.query.get('docName');
                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">    
    <div style="font-family: Times New Roman;font-size: 9pt; text-align: center;" class="mainPage">
        <br />
        <table width="100%" style="border: solid 1px #666; text-align: left;">
            <tr>
                <td rowspan="5" style="width: 105px; text-align: center;">
                    <img alt="" src="BigBlueLogo.gif" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <strong>BIG BLUE GROUP DOO</strong>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 75px;">
                    Adresa :
                </td>
                <td style="width: 250px;">
                    Makedonska 4, 11000 Beograd
                </td>
                <td style="width: 75px;" align="right">
                    &nbsp; Licenca :
                </td>
                <td>
                    &nbsp; OTP 255/2010 od 12.02.2010
                </td>
            </tr>
            <tr>
                <td align="right">
                    Tel / Fax :
                </td>
                <td>
                    +381 11 222 8 330; +381 11 323 33 44
                </td>
                <td align="right">
                    PIB :
                </td>
                <td>
                    104518987
                </td>
            </tr>
            <tr>
                <td align="right">
                    Web :
                </td>
                <td>
                    www.bigblue.rs
                </td>
                <td align="right">
                    Volks Bank :
                </td>
                <td>
                    285-0364120780001-08
                </td>
            </tr>
            <tr>
                <td align="right">
                    E-Mail :
                </td>
                <td>
                    info@bigblue.rs
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <br />
        <table width="100%" style="text-align: left; font-size: 12pt;">
            <tr>
                <td style="font-size: 24pt; font-weight: bold;">
                    <strong>Ugovor o putovanju</strong>
                </td>
                <td style="font-size: 14pt; font-weight: bold; text-align: right">
                    Broj :
                </td>
                <td style="font-size: 14pt; font-weight: bold; width: 130px;">
                    <span id="txtResNo"></span>
                </td>
            </tr>
        </table>
        <br />
        <table width="100%" style="text-align: left;">
            <tr>
                <td style="font-weight: bold; width: 49%;">
                    <strong>Učesnici u putovanju:</strong>
                </td>
                <td style="width: 2%;">
                    &nbsp;
                </td>
                <td style="font-weight: bold; width: 130px;">
                    <strong>Nosilac putovanja :</strong>
                </td>
            </tr>
        </table>
        <table width="100%" style="text-align: left; min-height: 150px;">
            <tr>
                <td style="width: 49%; border: solid 1px #666;" valign="top">
                    <div id="gridCustDiv">
                    </div>
                </td>
                <td style="width: 2%; min-height: 150px;">
                </td>
                <td style="border: solid 1px #666; font-size: 10pt;">
                    <b><span id="txtLaderName">Leader Name</span></b>
                    <br />
                    <br />
                    <span id="txtLeaderAddr">Leader Address</span>
                    <br />
                    <span id="txtLeaderAddrZip">Leader Address Zip</span>
                    <br />
                    <span id="txtLeaderAddrCity">Leader Address City</span>
                    <br />
                    <span style="font-style: italic;">Telefon :</span><span id="txtLeaderPhone">Leader Phone</span>
                    <br />
                    <span style="font-style: italic;">Mobile :</span><span id="txtLeaderMobile">Leader Mobile</span>
                </td>
            </tr>
        </table>
        <br />
        <div style="width: 100%; text-align: left;">
            <span style="font-size: 13pt; font-weight: bold;">Podaci o aranžmanu:</span>
        </div>
        <div id="gridHotelsDiv" style="width: 100%; text-align: left;">
            <table width="100%">
                <tr>
                    <td style="font-style: italic;">
                        Smeštaj:
                    </td>
                    <td colspan="3">
                        [Hotel."HotelName"] [Hotel."Category"], [Hotel."Location"]
                    </td>
                </tr>
                <tr>
                    <td style="font-style: italic;">
                        Prijava - Odjava:
                    </td>
                    <td>
                        [Hotel."BegDate"] - [Hotel."EndDate"]
                    </td>
                    <td style="width: 80px; font-style: italic;">
                        Broj noći:
                    </td>
                    <td style="width: 80px;">
                        [Hotel."Duration"]
                    </td>
                </tr>
                <tr>
                    <td style="font-style: italic;">
                        Broj osoba:
                    </td>
                    <td colspan="3">
                        Odrasli:&nbsp;[Hotel."Adult"]&nbsp;&nbsp;Deca:&nbsp;[Hotel."Child"]&nbsp;&nbsp;Inf:&nbsp;[Hotel."Infant"]
                    </td>
                </tr>
                <tr>
                    <td style="font-style: italic;">
                        Tip sobe:
                    </td>
                    <td>
                        [Hotel."Room"]
                    </td>
                    <td style="font-style: italic;">
                        Broj soba:
                    </td>
                    <td>
                        [Hotel."Unit"]
                    </td>
                </tr>
                <tr>
                    <td style="font-style: italic;">
                        Tip usluge:
                    </td>
                    <td colspan="3">
                        [Hotel."Board"]
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style="width: 100%; text-align: left; border: solid 1px #666; font-size: 12pt;
            min-height: 30px;">
            <strong>Napomena:&nbsp;&nbsp;</strong><span id="txtResNote">Res Note</span>
        </div>
        <br />
        <div id="gridServicesDiv" style="width: 100%; text-align: left;">
        </div>
        <br />
        <hr style="width: 100%" />
        <div style="width: 100%; text-align: left; border: solid 1px #666; font-size: 12pt;
            min-height: 30px;">
            <strong>Napomena:</strong><br />
            Potpisom potvrđujem da sam upoznat sa Opštim uslovima putovanja i programom putovanja,
            da iste prihvatam i potvrđujem da ću ih u potpunosti poštovati. Potpisom takođe
            potvrđujem da mi je ponuđeno putno zdravstveno osiguranje kao i Opšti uslovi osiguranja.
            Cena putnog osiguranja nije obuhvaćena Ugovorom i plaća se prilikom izdavanja polise
            u iznosu koji je naznačen na polisi. Sve uplate se vrše u dinarskoj protivvrednosti
            po prodajnom kursu Volks Banke za efektivu na dan uplate.
        </div>
        <br />
        <div style="width: 100%; text-align: left; font-size: 12pt; min-height: 30px;">
            <br />
            U Beogradu, <span id="txtRegisterDate"></span>
            <br />
            <br />
            <br />
            <table width="100%">
                <tr>
                    <td style="width: 50%;" align="center">
                        POTPIS STRANKE
                    </td>
                    <td align="center">
                        Za Big Blue:<span id="txtUserName"></span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
