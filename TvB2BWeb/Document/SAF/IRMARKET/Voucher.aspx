﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Voucher.aspx.cs" Inherits="Safiran_Voucher" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Voucher</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="../../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Arial; font-size: 8pt; width: 762px; }
        .clear { clear: both; height: 1px; }
        .h22px { height: 22px; }
        .line333 { color: #333; }
        .line666 { color: #BBB; }
        .mainPage { font-family: Arial; font-size: 8pt; width: 760px; height: 1060px; display: none; border: solid 1px #BBB; }
        .mainContent { width: 100%; }
        .arial6 { font-family: Arial; font-size: 6pt; }
        .arial8 { font-family: Arial; font-size: 8pt; }
        .arial9 { font-family: Arial; font-size: 9pt; }
        .times9b { font-family: Times New Roman; font-size: 9pt; font-weight: bold; }
        .arial10 { font-family: Arial; font-size: 10pt; }
        .arial11 { font-family: Arial; font-size: 11pt; }
        .arial11b { font-family: Arial; font-size: 11pt; font-weight: bold; }
        .arial12 { font-family: Arial; font-size: 12pt; }
        .dataDiv { border: 1px solid #808080; height: 331px; position: relative; }
        .SeperatorDiv { height: 30px; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function showVoucher(ServiceID) {
            var serviceID = parseInt(ServiceID);
            $("#param1").val(serviceID.toString());
            $.ajax({
                async: false,
                type: "POST",
                url: "Voucher.aspx/showVoucher",
                data: '{"ServiceID":' + serviceID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $(".mainPage").show();
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    if (this.AfterInsertShow && this.Data != '')
                                        $("#" + this.IdName + "Div").show();
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                        $("#gridPriceTable").width();
                        getFormData();
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Voucher.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $("#voucherListDiv").html('');
                        $("#voucherListDiv").html(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $.query = $.query.load(location.href);

                if ($.query.get('param1')) {
                    var serviceID = parseInt($.query.get('param1'));
                    showVoucher(serviceID);
                }
                else
                    getFormData();

                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <input id="param1" type="hidden" value="" />
    <div id="noPrintDiv">
        <div id="voucherListDiv">
            &nbsp;
        </div>
    </div>
    <div class="mainPage">
        <div id="divOperator" style="width: 201mm; height: 92mm; border: solid 1px black;
            margin: 1px 1px 1px 1px; clear: both;">
            <div style="width: 66mm; height: 84mm; float: left;">
                <div style="width: 66mm; clear: both; text-align: center;">
                    <img alt="" src="" id="OperatorLogo" style="height: 17mm;" />
                    <br />
                    <div id="hotelsProLogoDiv" style="display: none;">
                        <span id="hotelsProLogo"></span>
                    </div>
                </div>
                <div style="width: 66; height: 55mm; clear: both;">
                    <span style="font-weight: bold; font-size: 10pt;">Guest's name : </span>
                    <br />
                    <div id="gridTurist">
                    </div>
                </div>
                <div style="width: 66; height: 11mm; clear: both;">
                    <span style="font-weight: bold; font-size: 10pt;">Pax : </span>
                    <br />
                    <div>
                        <div style="float: left; width: 16mm; height: 15px;">
                            <span style="font-weight: bold; font-size: 9pt;">Adult :</span>
                        </div>
                        <div style="float: left; width: 13mm;">
                            <span id="txtAdult"></span>
                        </div>
                        <div style="float: left; width: 16mm; height: 15px; margin-left: 2mm;">
                            <span style="font-weight: bold; font-size: 9pt;">Child :</span>
                        </div>
                        <div style="float: left; width: 13mm;">
                            <span id="txtChild"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 135mm; height: 84mm; float: left;">
                <div style="height: 18mm; clear: both;">
                    <div style="width: 88mm; height: 18mm; float: left;">
                        <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                            <tr>
                                <td valign="middle" align="center">
                                    <span id="txtAgencyName" style="font-weight: bold; font-size: 10pt;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="center">
                                    <span id="txtAgencyPhone" style="font-weight: bold; font-size: 10pt;"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float: left;">
                        <table cellpadding="0" cellspacing="0" width="100%" height="100%">
                            <tr>
                                <td align="center" valign="middle">
                                    <span style="font-size: 12pt; font-weight: bold;">VOUCHER</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    No :<span id="txtResNo"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <span id="txtResDate"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="height: 9mm; clear: both;">
                    <table cellpadding="5" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td width="80px">
                                <span style="font-weight: bold; font-size: 10pt;">Hotel :</span>
                            </td>
                            <td>
                                <span id="hotelName_CatLoc"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 12mm; width: 135mm; clear: both;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Check In :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtCheckIn"></span>
                            </td>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Nights :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtNight"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Check Out :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtCheckOut"></span>
                            </td>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Board :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtBoard"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 14mm; clear: both;">
                    <div style="width: 100%; height: 6mm;">
                        <table cellpadding="5" cellspacing="0" style="width: 100%; height: 100%;">
                            <tr>
                                <td width="80px">
                                    <span style="font-weight: bold; font-size: 10pt;">Rooms :</span>
                                </td>
                                <td>
                                    <span id="txtAccom"></span>&nbsp;/&nbsp;<span id="txtRoom"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 8mm;">
                        <table cellpadding="5" cellspacing="0" style="width: 100%; height: 100%;">
                            <tr>
                                <td width="80px" valign="top">
                                    <span style="font-weight: bold; font-size: 10pt;">Transfers :</span>
                                </td>
                                <td valign="top">
                                    <div id="txtTransfer">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="height: 20mm; clear: both;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td valign="top" style="white-space: nowrap; width: 115px;">
                                <span style="font-weight: bold; font-size: 10pt;">Excursion:</span>
                            </td>
                            <td valign="top">
                                <div id="txtExcursion">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="white-space: nowrap; width: 115px;">
                                <span style="font-weight: bold; font-size: 10pt;">Add. Servives:</span>
                            </td>
                            <td valign="top">
                                <div id="addService">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="white-space: nowrap; width: 115px;">
                                <span style="font-weight: bold; font-size: 10pt;">Extra Service(s) :</span>
                            </td>
                            <td valign="top">
                                <div id="hotelExtService">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 11mm;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td width="33%" valign="top">
                                <span style="font-weight: bold; font-size: 10pt;">Note :</span>
                                <br />
                                <span id="txtResNote"></span>
                            </td>
                            <td width="33%" valign="top">
                                <span style="font-weight: bold; font-size: 10pt;">Sell Agent :</span>
                                <br />
                                <span id="txtAgencyNameA"></span>
                            </td>
                            <td valign="top">
                                <span style="font-style: italic; font-size: 10pt;">signature </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear: both; height: 25px; width: 756px;">
                <span id="bottomNote" style="font-style: italic;">Tour Guide : +90 536 338 39 37 // Other services than
                    specified to be paid by tourist</span>
            </div>
        </div>
        <br />
        <div id="divOperator0" style="width: 201mm; height: 92mm; border: solid 1px black;
            margin: 1px 1px 1px 1px; clear: both;">
            <div style="width: 66mm; height: 84mm; float: left;">
                <div style="width: 66mm; clear: both; text-align: center;">
                    <img alt="" src="" id="OperatorLogo0" style="height: 17mm;" />
                    <br />
                    <div id="hotelsProLogo0Div" style="display: none;">
                        <span id="hotelsProLogo0"></span>
                    </div>
                </div>
                <div style="width: 66; height: 55mm; clear: both;">
                    <span style="font-weight: bold; font-size: 10pt;">Guest's name : </span>
                    <br />
                    <div id="gridTurist0">
                    </div>
                </div>
                <div style="width: 66; height: 11mm; clear: both;">
                    <span style="font-weight: bold; font-size: 10pt;">Pax : </span>
                    <br />
                    <div>
                        <div style="float: left; width: 16mm; height: 15px;">
                            <span style="font-weight: bold; font-size: 9pt;">Adult :</span>
                        </div>
                        <div style="float: left; width: 13mm;">
                            <span id="txtAdult0"></span>
                        </div>
                        <div style="float: left; width: 16mm; height: 15px; margin-left: 2mm;">
                            <span style="font-weight: bold; font-size: 9pt;">Child :</span>
                        </div>
                        <div style="float: left; width: 13mm;">
                            <span id="txtChild0"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 135mm; height: 84mm; float: left;">
                <div style="height: 18mm; clear: both;">
                    <div style="width: 88mm; height: 18mm; float: left;">
                        <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                            <tr>
                                <td valign="middle" align="center">
                                    <span id="txtAgencyName0" style="font-weight: bold; font-size: 10pt;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="center">
                                    <span id="txtAgencyPhone0" style="font-weight: bold; font-size: 10pt;"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float: left;">
                        <table cellpadding="0" cellspacing="0" width="100%" height="100%">
                            <tr>
                                <td align="center" valign="middle">
                                    <span style="font-size: 12pt; font-weight: bold;">VOUCHER</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    No :<span id="txtResNo0"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <span id="txtResDate0"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="height: 9mm; clear: both;">
                    <table cellpadding="5" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td width="80px">
                                <span style="font-weight: bold; font-size: 10pt;">Hotel :</span>
                            </td>
                            <td>
                                <span id="hotelName_CatLoc0"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 12mm; width: 135mm; clear: both;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Check In :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtCheckIn0"></span>
                            </td>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Nights :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtNight0"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Check Out :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtCheckOut0"></span>
                            </td>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Board :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtBoard0"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 14mm; clear: both;">
                    <div style="width: 100%; height: 6mm;">
                        <table cellpadding="5" cellspacing="0" style="width: 100%; height: 100%;">
                            <tr>
                                <td width="80px">
                                    <span style="font-weight: bold; font-size: 10pt;">Rooms :</span>
                                </td>
                                <td>
                                    <span id="txtAccom0"></span>&nbsp;/&nbsp;<span id="txtRoom0"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 8mm;">
                        <table cellpadding="5" cellspacing="0" style="width: 100%; height: 100%;">
                            <tr>
                                <td width="80px" valign="top">
                                    <span style="font-weight: bold; font-size: 10pt;">Transfers :</span>
                                </td>
                                <td valign="top">
                                    <div id="txtTransfer0">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="height: 20mm; clear: both;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td valign="top" style="white-space: nowrap; width: 115px;">
                                <span style="font-weight: bold; font-size: 10pt;">Excursion:</span>
                            </td>
                            <td valign="top">
                                <div id="txtExcursion0">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="white-space: nowrap; width: 115px;">
                                <span style="font-weight: bold; font-size: 10pt;">Add. Servives:</span>
                            </td>
                            <td valign="top">
                                <div id="addService0">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="white-space: nowrap; width: 115px;">
                                <span style="font-weight: bold; font-size: 10pt;">Extra Service(s) :</span>
                            </td>
                            <td valign="top">
                                <div id="hotelExtService0">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 11mm;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td width="33%" valign="top">
                                <span style="font-weight: bold; font-size: 10pt;">Note :</span>
                                <br />
                                <span id="txtResNote0"></span>
                            </td>
                            <td width="33%" valign="top">
                                <span style="font-weight: bold; font-size: 10pt;">Sell Agent :</span>
                                <br />
                                <span id="txtAgencyNameA0"></span>
                            </td>
                            <td valign="top">
                                <span style="font-style: italic; font-size: 10pt;">signature </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear: both; height: 25px; width: 756px;">
                <span id="bottomNote0" style="font-style: italic;">Tour Guide : +90 536 338 39 37 // Other services than
                    specified to be paid by tourist</span>
            </div>
        </div>
        <br />
        <div id="divOperator1" style="width: 201mm; height: 92mm; border: solid 1px black;
            margin: 1px 1px 1px 1px; clear: both;">
            <div style="width: 66mm; height: 84mm; float: left;">
                <div style="width: 66mm; clear: both; text-align: center;">
                    <img alt="" src="" id="OperatorLogo1" style="height: 17mm;" />
                    <br />
                    <div id="hotelsProLogo1Div" style="display: none;">
                        <span id="hotelsProLogo1"></span>
                    </div>
                </div>
                <div style="width: 66; height: 55mm; clear: both;">
                    <span style="font-weight: bold; font-size: 10pt;">Guest's name : </span>
                    <br />
                    <div id="gridTurist1">
                    </div>
                </div>
                <div style="width: 66; height: 11mm; clear: both;">
                    <span style="font-weight: bold; font-size: 10pt;">Pax : </span>
                    <br />
                    <div>
                        <div style="float: left; width: 16mm; height: 15px;">
                            <span style="font-weight: bold; font-size: 9pt;">Adult :</span>
                        </div>
                        <div style="float: left; width: 13mm;">
                            <span id="txtAdult1"></span>
                        </div>
                        <div style="float: left; width: 16mm; height: 15px; margin-left: 2mm;">
                            <span style="font-weight: bold; font-size: 9pt;">Child :</span>
                        </div>
                        <div style="float: left; width: 13mm;">
                            <span id="txtChild1"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 135mm; height: 84mm; float: left;">
                <div style="height: 18mm; clear: both;">
                    <div style="width: 88mm; height: 18mm; float: left;">
                        <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                            <tr>
                                <td valign="middle" align="center">
                                    <span id="txtAgencyName1" style="font-weight: bold; font-size: 10pt;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="center">
                                    <span id="txtAgencyPhone1" style="font-weight: bold; font-size: 10pt;"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float: left;">
                        <table cellpadding="0" cellspacing="0" width="100%" height="100%">
                            <tr>
                                <td align="center" valign="middle">
                                    <span style="font-size: 12pt; font-weight: bold;">VOUCHER</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    No :<span id="txtResNo1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <span id="txtResDate1"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="height: 9mm; clear: both;">
                    <table cellpadding="5" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td width="80px">
                                <span style="font-weight: bold; font-size: 10pt;">Hotel :</span>
                            </td>
                            <td>
                                <span id="hotelName_CatLoc1"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 12mm; width: 135mm; clear: both;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Check In :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtCheckIn1"></span>
                            </td>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Nights :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtNight1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Check Out :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtCheckOut1"></span>
                            </td>
                            <td style="width: 20mm;" align="right">
                                <span style="font-weight: bold; font-size: 10pt;">Board :</span>
                            </td>
                            <td style="width: 47mm;">
                                <span id="txtBoard1"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 14mm; clear: both;">
                    <div style="width: 100%; height: 6mm;">
                        <table cellpadding="5" cellspacing="0" style="width: 100%; height: 100%;">
                            <tr>
                                <td width="80px">
                                    <span style="font-weight: bold; font-size: 10pt;">Rooms :</span>
                                </td>
                                <td>
                                    <span id="txtAccom1"></span>&nbsp;/&nbsp;<span id="txtRoom1"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 8mm;">
                        <table cellpadding="5" cellspacing="0" style="width: 100%; height: 100%;">
                            <tr>
                                <td width="80px" valign="top">
                                    <span style="font-weight: bold; font-size: 10pt;">Transfers :</span>
                                </td>
                                <td valign="top">
                                    <div id="txtTransfer1">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="height: 20mm; clear: both;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td valign="top" style="white-space: nowrap; width: 115px;">
                                <span style="font-weight: bold; font-size: 10pt;">Excursion:</span>
                            </td>
                            <td valign="top">
                                <div id="txtExcursion1">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="white-space: nowrap; width: 115px;">
                                <span style="font-weight: bold; font-size: 10pt;">Add. Servives:</span>
                            </td>
                            <td valign="top">
                                <div id="addService1">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="white-space: nowrap; width: 115px;">
                                <span style="font-weight: bold; font-size: 10pt;">Extra Service(s) :</span>
                            </td>
                            <td valign="top">
                                <div id="hotelExtService1">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 11mm;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td width="33%" valign="top">
                                <span style="font-weight: bold; font-size: 10pt;">Note :</span>
                                <br />
                                <span id="txtResNote1"></span>
                            </td>
                            <td width="33%" valign="top">
                                <span style="font-weight: bold; font-size: 10pt;">Sell Agent :</span>
                                <br />
                                <span id="txtAgencyNameA1"></span>
                            </td>
                            <td valign="top">
                                <span style="font-style: italic; font-size: 10pt;">signature </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear: both; height: 25px; width: 756px;">
                <span id="bottomNote1" style="font-style: italic;">Tour Guide : +90 536 338 39 37 // Other services than
                    specified to be paid by tourist</span>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
