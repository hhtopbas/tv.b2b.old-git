﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contract.aspx.cs" Inherits="Safiran_Contract" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contrat</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Arial; font-size: 8pt; width: 710px; }
        .clearfix:after { content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0; }
        .clear { clear: both; height: 1px; }
        .mainPage { width: 708px; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Contract.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    if (this.AfterInsertShow && this.Data != '')
                                        $("#" + this.IdName + "Div").show();
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                getFormData();
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var docName = $.query.get('docName');
                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="mainPage">
        <div style="width: 100%; vertical-align: middle;">
            <img alt="" id="imgOperator" src="" style="width: 150px;" />
            <span style="font-size: 12pt; font-weight: bold;">Reservation Contract For Reseravation
                No:&nbsp;<span id="txtResNo"></span> &nbsp;Date:&nbsp;<span id="txtDate"></span>
            </span>
            <br />
            <br />
        </div>
        <div style="width: 100%; text-align: center;">
            <span style="font-size: 12pt; font-weight: bold;">Tourist Informations</span>
        </div>
        <div style="width: 708px; border: solid 1px #666; font-size: 12pt;" class="clearfix">
            <div style="width: 64%; height: 233px; float: left; border-right: solid 1px #666;">
                <div style="height: 183px;">
                    <div id="gridCust">
                    </div>
                </div>
                <div style="height: 50px;">
                    <span id="txtPax"></span>
                </div>
            </div>
            <div style="width: 35%; height: 233px; float: left;">
                <div style="height: 83px;">
                    <div style="background-color: #FF9933; height: 20px;">
                        <b>Leader Name, Surname</b>
                    </div>
                    <span id="lblLeader"></span>
                </div>
                <div style="height: 149px;">
                    <div style="background-color: #FF9933; height: 20px;">
                        <b>Address :</b>
                    </div>
                    <span id="lblAdres"></span>
                    <br />
                    <span style="background-color: #FF9933; height: 20px;"><b>Phone :</b></span> <span
                        id="lblPhone"></span>
                </div>
            </div>
        </div>
        <div style="width: 100%; border: solid 1px #666; font-family: Arial; font-size: 12pt;">
            <div style="background-color: #FF9933; height: 20px; text-align: center;">
                <b>Main Services Informations</b>
            </div>
            <div id="gridService">
            </div>
        </div>
        <div id="gridExtServiceDiv" style="display: none; width: 100%; border: solid 1px #666; font-family: Arial; font-size: 12pt;">
            <div style="background-color: #FF9933; height: 20px; text-align: center;">
                <b>Extra Services Informations</b>
            </div>
            <div id="gridExtService">
            </div>
        </div>
        <div id="divToplamlar" style="width: 100%; border: solid 1px #666; font-family: Arial;
            font-size: 12pt;">
        </div>
        <div style="width: 100%; border: solid 1px #666; font-family: Arial; font-size: 12pt;">
            • You are kindly required to send your modifications with this form
            <br />
            • Please assure your passenmngnewger that hotel C/IN is at 14.00 and C/OUT is at
            12.00
            <br />
            • If there is a modification or cancellation on your reservation, you are kindly
            required to inform us 2 days in prior, otherwise there will be issued NO SHOW invoice
            <br />
            • You are kindly required to make payment of your reservation which C/IN and C/OUT
            has finished. Otherwise the rest of your reservation which still not make C/IN or
            C/OUT automatically will be erased from our system. For your information.
            <br />
            • Your reservation is confirmed with No Show Guarantee. In case of non arrival or
            early C/OUT reservation cost will be charged to you.
            <br />
            • If there appears any claim regarding the services mentioned above, please let
            us know within 30 days from C/OUT date. Please kindly be informed that claims after
            this period will be disregarded. Thank you for your attention and kind understanding.
            <br />
            <br />
            <div id="txtExcursionText"></div>            
        </div>
    </div>
    </form>
</body>
</html>
