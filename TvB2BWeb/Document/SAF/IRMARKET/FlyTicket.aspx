﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlyTicket.aspx.cs" Inherits="Safiran_FlyTicket" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ETicket</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <%--<script src="../../../Scripts/jquery.dimensions.js" type="text/javascript"></script>--%>

    <link href="../../../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        table td { height: 21px; }
        body { line-height: 1; font-family: Tahoma Times New Roman; font-size: 8pt; width: 794px; }
        .mainPage { width: 794px; }
        .clear { clear: both; height: 1px; }
        .turistdata { font-family: Tahoma Times New Roman; font-size: 10px; width: 250px; }
        .header { text-align: center; font-size: 16px; font-family: Tahoma Times New Roman; font-weight: bold; padding-top: 20px; z-index: 10; }
        .info { padding-left: 10px; padding-right: 10px; width: 682px; z-index: 20; }
        .FieldLabel { font-size: 10px; font-family: Tahoma Times New Roman; }
        .w200px { width: 230px; }
        .w123px { width: 150px; }
        .SeperatorDiv { height: 30px; }
        #noPrintDiv { width: 794px; text-align: center; }
        #custListDiv { text-align: left; font-size: 10pt; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
        .style1 { width: 115px; }
        .style2 { width: 300px; }
        .style3 { width: 50px; }
        .style4 { width: 75px; }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function showTicket(CustNo) {
            $("#param1").val(CustNo.toString());
            $.ajax({
                async: false,
                type: "POST",
                url: "FlyTicket.aspx/showTicket",
                data: '{"CustNo":' + CustNo + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $(".mainPage").show();
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    $("#" + this.IdName).height();
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    if (this.Data != '')
                                        $("#" + this.IdName).attr("src", this.Data);
                                    else $("#" + this.IdName).hide();
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                        $.query = $.query.load(location.href);
                        if (!$.query.get('print')) {
                            var width = $(".mainPage").width();
                            var height = $(".mainPage").height();
                            var param1 = $("#param1").val();
                            self.parent.viewReportPrint(location.href + "&print=1&param1=" + CustNo, width, height);
                        }
                        getFormData();
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "FlyTicket.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $("#custListDiv").html('');
                        $("#custListDiv").html(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $.query = $.query.load(location.href);

                if ($.query.get('param1')) {
                    var custNo = parseInt($.query.get('param1'));
                    showTicket(custNo);
                }
                else
                    getFormData();

                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
            });
    </script>

</head>
<body>
    <form id="ticketForm" runat="server">
    <input id="param1" type="hidden" value="" />
    <div id="noPrintDiv">
        <div id="custListDiv">
            &nbsp;
        </div>
    </div>
    <div class="mainPage" style="display: none;">
        <div id="divZoom">
            <div style="clear: both;">
                <div id="divLogoAndHeader" style="height: 82mm; width: 170mm">
                    <div style="float: left; width: 70mm; height: 76mm; text-align: center; vertical-align: middle;">
                        <img alt="" id="imgOpe" src="" style="width: 230px;" />
                    </div>
                    <div style="float: left; width: 100mm; height: 70mm;">
                        <table style="width: 99%; height: 240px">
                            <tr>
                                <td align="center" valign="middle" height="10mm">
                                    <span style="font-family: Arial; font-size: 12pt;"><b>ELECTRONIC TICKET PASSENGER ITINERARY</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td height="195px" align="center" valign="middle">
                                    <span style="font-family: Arial; font-size: 12pt;"><b><i>We Care About You</i></b></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <span style="font-family: Arial; font-size: 10pt; margin-right: 10mm;"><b><i>SAFIRAN
                                        TRAVEL</i></b></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="divResNoAndPNR" style="clear: both; width: 170mm; height: 7mm; border: solid 1px #666;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                        <tr>
                            <td align="left" width="50%">
                                <span style="font-family: Arial; font-size: 10pt;"><b>&nbsp;Reservation Nr :</b> <span
                                    id="txtResNo"></span></span>
                            </td>
                            <td align="right" width="50%">
                                <span style="font-family: Arial; font-size: 10pt;"><b>PNR :</b></span> <span id="txtPNR">
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height:4px;width: 170mm; ">&nbsp;</div>
                <div id="divTuristInfo" style="clear: both; width: 170mm;border: solid 1px #666;">
                    <table width="100%" style="font-family: Arial; font-size: 8pt;" cellpadding="1" cellspacing="1">
                        <tr>
                            <td class="style1">
                                <span style="font-family: Arial; font-size: 8pt;">Name of passenger</span>
                            </td>
                            <td class="style2">
                                <span id="txtTurist"></span>
                            </td>
                            <td class="style4">
                                <span style="font-family: Arial; font-size: 8pt;">Date</span>
                            </td>
                            <td>
                                <span id="txtDate"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                <span style="font-family: Arial; font-size: 8pt;">Agency</span>
                            </td>
                            <td class="style2">
                                <span id="txtAgency"></span>
                            </td>
                            <td class="style4">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                <span style="font-family: Arial; font-size: 8pt;">Airline</span>
                            </td>
                            <td class="style2">
                                <span id="txtAirline"></span>
                            </td>
                            <td class="style4">
                                <span style="font-family: Arial; font-size: 8pt;">Date of Issued</span>
                            </td>
                            <td>
                                <span id="txtDateOfIssue"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                <span style="font-family: Arial; font-size: 8pt;">Not Transferable</span>
                            </td>
                            <td colspan="3">
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <div id="gridTicketDiv" style="clear: both; width: 169mm;">
                    <div id="gridTicket">
                    </div>
                </div>
                <br />
                <div id="divFooter" style="clear: both; width: 170mm;">
                    <%--<br />
                    <asp:Literal ID="TicketDoc" runat="server" Mode="PassThrough" />--%>
                    <br />                    
                    <div dir="rtl" lang="fa">
                        <span>قابل توجّه</span>
                        <br />
                        <span>ساعاتي که در زير تعيين شده آخرين فرصتي است که مسافرين محترم بايد در محل هاي تعيين
                            شده حاضر باشند. هيچ پروازي به علت عدم حضور مسافر به تاخير نخواهد افتاد. مسافريني
                            که نتوانند در ساعات معينه زير حاضر شوند جاي آنان به ديگري واگذار و بليط آنان مشمول
                            مقررات مربوط به تاخير مسافرين خواهد بود.</span>
                        <br />
                        <span>تذکّر مهّم </span>
                        <br />
                        <span>تعهد شرکت هواپيمايي کيش در قبال گم شدن، تاخير حمل و يا خسارت به بار مسافر محدود
                            به 25 کيلو و براي هر کيلو 20 دلار ميباشد. مگر اينکه ارزش بالاتر کالا قبل از پرواز
                            به شرکت اعلام و هزينه هاي اضافي مربوط پرداخت شده باشد.</span>
                    </div>
                    <br />
                    <div>
                        Important<br />
                        The check-in times shown hereunder are the limit times at which you are requested
                        to arrive at the Town Terminal or Airport. No flights will be delayed because of
                        the late arrival of a passenger, and the non-compliance with the check-in times
                        hereunder will be considered as a late cancellation.
                    </div>
                    <br />
                    <div dir="rtl" lang="fa">
                        <span>تذکر مهم</span><br />
                        <span>لزوم تایید مجدد جا جهت ادامه مسافرت یا برگشت</span><br />
                        <span>اگر در مقصد یا بین راه در نقطه ای بیش از 72 ساعت (3 روز) قصد توقف دارید لازم است
                            استفاده از جای نگاهداری شده خود را در پرواز مورد نظر حتما تایید فرمایید. برای تایید
                            استفاده از جای نگاهداری شده می بایست به دفتر هواپیمایی کیش محل توقف و یا به دفتر
                            هواپیمایی مربوط حداقل 72 ساعت (3 روز) قبل از حرکت مراجعه فرمایید. لازم است خاطر
                            نشان گردد که عدم اجرای این تذکر ممکن است منجر به ابطال جای نگاهداری شده گردد.</span>
                    </div>
                    <br />
                    <div style="text-align: center; font-weight: bold;">
                        +شماره روابط عمومی : 222 1 111 532 90+
                    </div>
                    <div>
                        <br />
                        RECONFIRMATION of RESERVATION<br />
                        When at any point on your itinerary, you make a SCHEDULED STOP of more than 72 hours,
                        please RECONFIRM your intention of using your continuing or return reservation.
                        To do so, please inform the airline office at the point where you intend to resume
                        your journey at least 72 HOURS BEFORE DEPARTURE OF YOUR FLIGHT.
                        <br />
                        FAILURE to reconfirm will result in the CANCELLATION of your reservation(s).
                    </div>
                    <div style="text-align: center; font-weight: bold;">
                    <br />
                        Guest Relation: +90 532 111 1 222</div>
                </div>
            <br />
            <table style="width: 170mm;">
                <tr>
                    <td>
                        <img alt="" src="Nayeb.png" style="width: 150px;" />
                    </td>
                    <td align="right" style="font-family: Arial; font-weight: bold; font-size: 10pt;">
                        رستوران نایب استانبول با غذاهای لذیذ ایرانی در خدمت مسافرین عزیز میباشد .<br />
                        PHONE : 0212 – 587 68 (روبروی هتل کایا)
                        <br />
                        ADDRESS: MILLET CAD . N0: 79 –FINDIKZADE- ISTANBUL
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div> </div>
    </form>
</body>
</html>
