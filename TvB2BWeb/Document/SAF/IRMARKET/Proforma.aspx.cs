﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvBo;
using TvTools;

public partial class Safiran_Proforma : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    
    public static string getSum(User UserData, ResDataRecord ResData)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            ResMainRecord RM = ResData.ResMain;
            string errorMsg = string.Empty;
            decimal SupDis = Conversion.getDecimalOrNull(RM.PasSupDis).HasValue ? Conversion.getDecimalOrNull(RM.PasSupDis).Value : Convert.ToDecimal(0);
            decimal AmountToPay = Conversion.getDecimalOrNull(RM.PasPayable).HasValue ? Conversion.getDecimalOrNull(RM.PasPayable).Value : Convert.ToDecimal(0);
            string BaseCur = UserData.SaleCur;
            string SaleCur = RM.SaleCur;
            //decimal? AmountToPayBaseCurrency = 0;
            sb.Append("<table cellpadding=\"4\" cellspacing=\"0\" width=\"225px\" style=\"font-family: Arial; font-size: 10pt;\">");
            sb.Append(string.Format("  <tr><td align=\"right\" width=\"100px\">Total :</td><td>{0}</td></tr>", RM.SalePrice.Value.ToString("#,###.") + " " + SaleCur));
            sb.Append(string.Format("  <tr><td align=\"right\">Suppl./ Disc:</td><td>{0}</td></tr>", (SupDis != 0 ? (SupDis.ToString("#,###.") + " " + SaleCur) : "")));
            sb.Append(string.Format("  <tr><td align=\"right\">Amount To Pay :</td><td>{0}</td></tr>", AmountToPay.ToString("#,###.") + " " + SaleCur));
            /*
            if (SaleCur != BaseCur)
            {
                try
                {
                    AmountToPayBaseCurrency = new Common().Exchange(UserData.Market, RM.ResDate.Value, SaleCur, BaseCur, AmountToPay, true, ref errorMsg);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        sb.AppendFormat("   <tr><td align=\"right\">By {0} :</td><td>{1}&nbsp;{2}</td></tr>", BaseCur, AmountToPayBaseCurrency.Value.ToString("#,###.0"), BaseCur);
                    }
                }
                catch { }
            }
            */
            sb.Append("</table>");
            return sb.ToString();
        }
        catch 
        {
            return "";
        }
    }

    public static string getResService(User UserData, ResDataRecord ResData)
    {
        List<ResServiceRecord> Services = ResData.ResService.Where(w => w.StatSer < 2).ToList<ResServiceRecord>();
        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;
        sb.Append("<table cellpadding=\"4\" cellspacing=\"2\" style=\"margin-left: 20px; font-family: Arial; font-size: 10pt;\">");
        sb.Append("    <tr>");
        sb.Append("        <td><b><u>Adult</u></b></td>");
        sb.Append("        <td><b><u>Child</u></b></td>");
        sb.Append("        <td>&nbsp;</td>");
        sb.Append("        <td>&nbsp;</td>");
        sb.Append("        <td>&nbsp;</td>");
        sb.Append("    </tr>");
        decimal? IncPackPriceService = ResData.ResService.Where(w => string.Equals(w.IncPack, "Y")).Sum(s => s.SalePrice);
        decimal? IncPackPriceExtService = ResData.ResServiceExt.Where(w => string.Equals(w.IncPack, "Y")).Sum(s => s.SalePrice);
        if (IncPackPriceService.HasValue)
        {
            decimal IncPackPrice = (IncPackPriceService.HasValue ? IncPackPriceService.Value : Convert.ToDecimal(0)) + (IncPackPriceExtService.HasValue ? IncPackPriceExtService.Value : Convert.ToDecimal(0));
            sb.Append("    <tr>");
            sb.Append(string.Format("        <td>{0}</td>", ResData.ResMain.Adult.Value.ToString()));
            sb.Append(string.Format("        <td>{0}</td>", ResData.ResMain.Child.Value.ToString()));
            sb.Append(string.Format("        <td align=\"right\">{0}</td>", IncPackPrice.ToString("#,###.")));
            sb.Append(string.Format("        <td>{0}</td>", ResData.ResMain.SaleCur));
            sb.Append(string.Format("        <td>{0}</td>", "Package Total"));
            sb.Append("    </tr>");
        }
        foreach (ResServiceRecord row in Services.Where(w=>string.Equals(w.IncPack, "N")).ToList<ResServiceRecord>())
        {
            if (row.SalePrice.HasValue && row.SalePrice.Value > 0)
            {
                string ServiceDesc = string.Empty;
                switch (row.ServiceType)
                {
                    case "HOTEL":
                        if (Equals(UserData.CustomRegID, Common.crID_Magellan))
                            ServiceDesc += row.RoomNameL + "," + row.Accom + "," + row.Board + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        else ServiceDesc += row.Board + "/(" + row.RoomNameL + " / " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString() + ")";
                        break;
                    case "FLIGHT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "TRANSPORT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "TRANSFER": TransferRecord trf = new Transfers().getTransfer(UserData.Market, row.Service, ref errorMsg);
                        if (trf != null && string.Equals(trf.Direction, "R"))
                            ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        else ServiceDesc += row.BegDate.Value.ToShortDateString();
                        break;
                    case "RENTING": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "EXCURSION": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "INSURANCE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "VISA": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "HANDFEE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    default: ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                }
                sb.Append("    <tr>");
                sb.Append(string.Format("        <td>{0}</td>", row.Adult.Value.ToString()));
                sb.Append(string.Format("        <td>{0}</td>", row.Child.Value.ToString()));
                sb.Append(string.Format("        <td align=\"right\">{0}</td>", row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.") : "&nbsp;"));
                sb.Append(string.Format("        <td>{0}</td>", !string.IsNullOrEmpty(row.SaleCur) ? row.SaleCur : "&nbsp;"));
                sb.Append(string.Format("        <td>{0}</td>", row.ServiceName + ServiceDesc));
                sb.Append("    </tr>");
            }
        }
        sb.Append("</table>");

        return sb.ToString();
    }

    public static string getCustomers(User UserData, ResDataRecord ResData)
    {
        List<ResCustRecord> ResCust = ResData.ResCust.Where(w => w.Status == 0).ToList<ResCustRecord>();
        StringBuilder sb = new StringBuilder();
        sb.Append("<table cellpadding=\"2\" cellspacing=\"0\" style=\"margin-left:20px; font-family:Arial; font-size:10pt;\">");
        sb.Append("    <tr>");
        sb.Append("        <td style=\"width:350px;\"><span><b><u>Passanger(s)</u></b></span></td>");
        sb.Append("        <td style=\"width:90px;\"><span><b><u>Birth Date</u></b></span></td>");
        sb.Append("    </tr>");
        foreach (ResCustRecord row in ResCust)
        {
            sb.Append("    <tr>");
            sb.Append(string.Format("        <td style=\"width:350px;\">{0}</td>", row.TitleStr + " " + row.Surname + " " + row.Name.ToString()));
            sb.Append(string.Format("        <td style=\"width:90px;\">{0}</td>", row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : ""));
            sb.Append("    </tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }

    public static string getHotelService(User UserData, ResDataRecord ResData)
    {
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        List<ResServiceRecord> resServiceList = ResData.ResService.Where(w => w.ServiceType == "HOTEL").ToList<ResServiceRecord>();
        foreach (ResServiceRecord row in resServiceList)
        {
            string hotelName = row.ServiceName.ToString();
            string begDate = row.BegDate.Value.ToShortDateString();
            string endDate = row.EndDate.Value.ToShortDateString();
            string night = row.Duration.Value.ToString();
            string board = row.Board.ToString();
            string roomType = row.RoomName.ToString();
            string accom = row.Accom.ToString();
            string hotelLocation = row.DepLocationName.ToString();
            sb.Append("<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" style=\"padding: 1px; font-family:Arial; font-size:10pt; border-top: solid 1px #000000; border-bottom:solid 1px #000000;\">");
            sb.Append("   <tr>");
            sb.Append("       <td width=\"80px\">&nbsp;</td>");
            sb.Append("       <td width=\"5px\">&nbsp;</td>");
            sb.Append("       <td>&nbsp;</td>");
            sb.Append("       <td align=\"center\" width=\"90px\"><b>From</b></td>");
            sb.Append("       <td align=\"center\" width=\"90px\"><b>To</b></td>");
            sb.Append("       <td align=\"center\" width=\"60px\"><b>Night</b></td>");
            sb.Append("       <td align=\"center\" width=\"60px\"><b>Board</b></td>");
            sb.Append("   </tr><tr>");
            sb.Append("       <td width=\"80px\"><span><b>Hotel</b></span></td>");
            sb.Append("       <td width=\"5px\"><b>:</b></td>");
            sb.Append(string.Format("       <td>{0}</td>", hotelName));
            sb.Append(string.Format("       <td align=\"center\" width=\"90px\">{0}</td>", begDate));
            sb.Append(string.Format("       <td align=\"center\" width=\"90px\">{0}</td>", endDate));
            sb.Append(string.Format("       <td align=\"center\" width=\"60px\">{0}</td>", night));
            sb.Append(string.Format("       <td align=\"center\" width=\"60px\">{0}</td>", board));
            sb.Append("   </tr><tr>");
            sb.Append("       <td width=\"80px\"><span><b>Room type</b></span></td>");
            sb.Append("       <td width=\"5px\"><b>:</b></td>");
            sb.Append(string.Format("       <td>{0}</td>", roomType + " / " + accom));
            sb.Append("       <td align=\"center\" width=\"90px\">&nbsp;</td>");
            sb.Append("       <td align=\"center\" width=\"90px\">&nbsp;</td>");
            sb.Append("       <td align=\"center\" width=\"60px\">&nbsp;</td>");
            sb.Append("       <td align=\"center\" width=\"60px\">&nbsp;</td>");
            sb.Append("   </tr><tr>");
            sb.Append("       <td width=\"80px\"><span><b>Location</b></span></td>");
            sb.Append("       <td width=\"5px\"><b>:</b></td>");
            sb.Append(string.Format("       <td>{0}</td>", hotelLocation));
            sb.Append("       <td align=\"center\" width=\"90px\">&nbsp;</td>");
            sb.Append("       <td align=\"center\" width=\"90px\">&nbsp;</td>");
            sb.Append("       <td align=\"center\" width=\"60px\">&nbsp;</td>");
            sb.Append("       <td align=\"center\" width=\"60px\">&nbsp;</td>");
            sb.Append("   </tr>");
            sb.Append("</table>");
        }
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();
        ResMainRecord resMain = ResData.ResMain;
        OperatorRecord opr = new Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);
        string oprAddress = opr.FirmName + " " + opr.Address + " " + opr.AddrZip + " " + opr.AddrCity + " " + opr.Phone1 + " " + opr.Email1;
        var leaderCustNo = ResData.ResCust.Where(w => string.Equals(w.Leader, "Y"));
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == leaderCustNo.FirstOrDefault().CustNo);
        string leaderInfo = string.Empty;
        if (_leader != null) leaderInfo = !Equals(_leader.ContactAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip + " " + _leader.AddrHomeTel + " " + _leader.AddrHomeFax + " " + _leader.AddrHomeEmail : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip + " " + _leader.AddrWorkTel + " " + _leader.AddrWorkFax + " " + _leader.AddrWorkEMail;
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });
        string contractLogoStr = CacheObjects.getOperatorLogoUrl(UserData.Operator);


        htmlData.Add(new TvReport.htmlCodeData { IdName = "imgOperator", TagName = "img", Data = contractLogoStr });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtInvoiceDate", TagName = "span", Data = DateTime.Today.ToShortDateString() });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo", TagName = "span", Data = ResData.ResMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtClientName", TagName = "span", Data = _leader.CTitleName + " " + _leader.CSurName + " " + _leader.CName });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtClientAddress", TagName = "span", Data = string.Equals(_leader.InvoiceAddr, "W") ? _leader.AddrWork : _leader.AddrHome });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtClientZip", TagName = "span", Data = string.Equals(_leader.InvoiceAddr, "W") ? _leader.AddrWorkZip : _leader.AddrHomeZip });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtClientCity", TagName = "span", Data = string.Equals(_leader.InvoiceAddr, "W") ? _leader.AddrWorkCity : _leader.AddrHomeCity });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtPhone1", TagName = "span", Data = string.Equals(_leader.InvoiceAddr, "W") ? _leader.AddrWorkTel : _leader.AddrHomeTel });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtClientCountry", TagName = "span", Data = string.Equals(_leader.InvoiceAddr, "W") ? _leader.AddrWorkCountry : _leader.AddrHomeCountry });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo1", TagName = "span", Data = ResData.ResMain.ResNo });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "hotelGrid", TagName = "div", Data = getHotelService(UserData, ResData) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "passengerGrid", TagName = "div", Data = getCustomers(UserData, ResData) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "servicePrice", TagName = "div", Data = getResService(UserData, ResData) });
        htmlData.Add(new TvReport.htmlCodeData { IdName = "sum", TagName = "div", Data = getSum(UserData, ResData) });

        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }
}
