﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvBo;

public partial class Safiran_Voucher : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public static string getCustomers(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        StringBuilder sb = new StringBuilder();
        var query = from q1 in ResData.ResCust
                    join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                    where q2.ServiceID == ServiceID
                    select q1;
        sb.Append("<table >");
        foreach (var row in query)
        {
            sb.Append(" <tr>");
            sb.Append("     <td>");
            sb.Append(row.TitleStr + ", " + row.Surname + " " + row.Name + " , " + (row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : ""));
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }

    public static string getTransfers(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        StringBuilder sb = new StringBuilder();
        //and Exists(Select * from ResCon Where ResNo=TS.ResNo and ServiceID=TS.RecID and CustNo in (Select CustNo from ResCon Where ResNo=TS.ResNo and ServiceID=@RecID) )
        var hotelCust = from q1 in ResData.ResCust
                        join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                        join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                        where q3.RecID == ServiceID
                        select q1;
        var query = from q1 in hotelCust
                    join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                    join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                    where string.Equals(q3.ServiceType, "TRANSFER")
                    orderby q3.BegDate
                    select q3;
        var transfers = from q in query
                        group q by new { q.BegDate, ServiceName = q.ServiceName } into k
                        select new
                        {
                            k.Key.BegDate,
                            ServiceName = k.Key.ServiceName
                        };
        foreach (var row in transfers)
        {
            if (sb.Length > 0) sb.Append("<br />");
            sb.Append(row.ServiceName);
        }
        return sb.ToString();
    }

    public static string getExcursion(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        StringBuilder sb = new StringBuilder();
        //and Exists(Select * from ResCon Where ResNo=TS.ResNo and ServiceID=TS.RecID and CustNo in (Select CustNo from ResCon Where ResNo=TS.ResNo and ServiceID=@RecID) )
        var hotelCust = from q1 in ResData.ResCust
                        join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                        join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                        where q3.RecID == ServiceID
                        select q1;
        var query = from q1 in hotelCust
                    join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                    join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                    where string.Equals(q3.ServiceType, "EXCURSION")
                    orderby q3.BegDate
                    select q3;
        var transfers = from q in query
                        group q by new { q.BegDate, ServiceName = q.ServiceName } into k
                        select new
                        {
                            k.Key.BegDate,
                            ServiceName = k.Key.ServiceName
                        };
        foreach (var row in transfers)
        {
            if (sb.Length > 0) sb.Append("<br />");
            sb.Append(row.ServiceName);
        }
        return sb.ToString();
    }

    public static string getAdditionalService(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        StringBuilder sb = new StringBuilder();
        List<ServiceRecord> serv = new Common().getMainServices(UserData.Market);
        //and Exists(Select * from ResCon Where ResNo=TS.ResNo and ServiceID=TS.RecID and CustNo in (Select CustNo from ResCon Where ResNo=TS.ResNo and ServiceID=@RecID) )
        var hotelCust = from q1 in ResData.ResCust
                        join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                        join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                        where q3.RecID == ServiceID
                        select q1;
        var query = from q1 in hotelCust
                    join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                    join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                    join q4 in serv on q3.ServiceType equals q4.Code
                    where q4.Type == "U" && q3.StatSer < 2
                    select q3;

        var additional = from q in query
                         group q by new { q.BegDate, q.ServiceTypeName, ServiceName = q.ServiceName } into k
                         select new
                         {
                             k.Key.BegDate,
                             k.Key.ServiceTypeName,
                             ServiceName = k.Key.ServiceName
                         };
        string addServcieStr = string.Empty;
        foreach (var row in additional)
        {
            addServcieStr += string.Format("<tr><td>{0}</td><td>&nbsp;&nbsp;&nbsp;</td><td>{1}</td></tr>",
                                            row.ServiceTypeName,
                                            row.ServiceName);
        }
        if (addServcieStr.Length > 0)
        {
            addServcieStr = "<table>" + addServcieStr;
            addServcieStr += "</table>";
        }
        return addServcieStr;
    }

    public static string getExtService(User UserData, ResDataRecord ResData, int? ServiceID)
    {
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        List<ResServiceExtRecord> resServiceExtList = ResData.ResServiceExt.Where(w => w.ServiceID == ServiceID).ToList<ResServiceExtRecord>();
        sb.Append("<table >");
        if (resServiceExtList != null)
        {
            foreach (ResServiceExtRecord row in resServiceExtList)
            {
                sb.Append(string.Format("<tr><td>{0}</td></tr>", row.ExtServiceName));
            }
        }
        sb.Append("</table>");
        return resServiceExtList.Count > 0 ? sb.ToString() : "";
    }

    [WebMethod(EnableSession = true)]
    public static string showVoucher(string ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        int? _serviceID = TvTools.Conversion.getInt32OrNull(ServiceID);
        StringBuilder sb = new StringBuilder();
        ResMainRecord resMain = ResData.ResMain;
        OperatorRecord opr = new Common().getOperator(ResData.ResMain.PLOperator, ref errorMsg);
        AgencyRecord agent = new Agency().getAgency(ResData.ResMain.Agency, ref errorMsg);
        AgencyRecord mainAgent = new Agency().getAgency(string.IsNullOrEmpty(agent.MainOffice) ? agent.Code : agent.MainOffice, ref errorMsg);
        if (agent == null) agent = new Agency().getAgency(UserData.AgencyID, ref errorMsg);
        TvReport.NovReport.AgencyDocAddressRecord agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(ResData.ResMain.Agency, ref errorMsg);
        if (agencyAddr == null) agencyAddr = new TvReport.NovReport.NovReport().getAgencyDocAdress(UserData.AgencyID, ref errorMsg);
        string oprAddress = opr.FirmName + " " + opr.Address + " " + opr.AddrZip + " " + opr.AddrCity + " " + opr.Phone1 + " " + opr.Email1;
        var leaderCustNo = ResData.ResCust.Where(w => string.Equals(w.Leader, "Y"));
        ResCustInfoRecord _leader = ResData.ResCustInfo.Find(f => f.CustNo == leaderCustNo.FirstOrDefault().CustNo);
        string leaderInfo = string.Empty;
        if (_leader != null) leaderInfo = !Equals(_leader.ContactAddr, "W") ? _leader.AddrHome + " " + _leader.AddrHomeCity + " " + _leader.AddrHomeZip + " " + _leader.AddrHomeTel + " " + _leader.AddrHomeFax + " " + _leader.AddrHomeEmail : _leader.AddrWork + " " + _leader.AddrWorkCity + " " + _leader.AddrWorkZip + " " + _leader.AddrWorkTel + " " + _leader.AddrWorkFax + " " + _leader.AddrWorkEMail;
        List<TvReport.htmlCodeData> htmlData = new List<TvReport.htmlCodeData>();
        //        htmlData.Add(new TvReport.htmlCodeData { IdName = "", TagName = "", Data = "" });
        string contractLogoStr = CacheObjects.getOperatorLogoUrl(UserData.Operator);
        string cntStr = string.Empty;
        TvReport.SafReport.VoucherRecord voucherHotel = new TvReport.SafReport.SafReport().getVoucherHotels(ResData.ResMain.ResNo, _serviceID, ref errorMsg);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == _serviceID);
        if (resService == null && voucherHotel == null)
            return string.Empty;
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, resService.Service, ref errorMsg);
        if (hotel == null)
            return string.Empty;

        for (int i = 0; i < 3; i++)
        {
            if (i != 0) cntStr = (i - 1).ToString();
            htmlData.Add(new TvReport.htmlCodeData { IdName = "OperatorLogo" + cntStr, TagName = "img", Data = contractLogoStr });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "hotelsProLogo" + cntStr, TagName = "div", Data = voucherHotel.HTLSPRONote, AfterInsertShow = true });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "gridTurist" + cntStr, TagName = "div", Data = getCustomers(UserData, ResData, _serviceID) });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAdult" + cntStr, TagName = "span", Data = resService.Adult.ToString() });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtChild" + cntStr, TagName = "span", Data = resService.Child.ToString() });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyName" + cntStr, TagName = "span", Data = agencyAddr.Name });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyPhone" + cntStr, TagName = "span", Data = agencyAddr.Phone1 });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNo" + cntStr, TagName = "span", Data = ResData.ResMain.ResNo });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResDate" + cntStr, TagName = "span", Data = ResData.ResMain.ResDate.HasValue ? ResData.ResMain.ResDate.Value.ToShortDateString() : "" });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "hotelName_CatLoc" + cntStr, TagName = "span", Data = voucherHotel.HotelName });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCheckIn" + cntStr, TagName = "span", Data = resService.BegDate.Value.ToShortDateString() });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtNight" + cntStr, TagName = "span", Data = resService.Duration.ToString() });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtCheckOut" + cntStr, TagName = "span", Data = resService.EndDate.Value.ToShortDateString() });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtBoard" + cntStr, TagName = "span", Data = voucherHotel.BoardName });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAccom" + cntStr, TagName = "span", Data = voucherHotel.AccomFullName + " " + voucherHotel.Unit.ToString() + " " + voucherHotel.RoomName });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtRoom" + cntStr, TagName = "span", Data = voucherHotel != null ? voucherHotel.RoomName : resService.RoomName });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtTransfer" + cntStr, TagName = "div", Data = getTransfers(UserData, ResData, _serviceID) });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtExcursion" + cntStr, TagName = "div", Data = getExcursion(UserData, ResData, _serviceID) });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "addService" + cntStr, TagName = "div", Data = getAdditionalService(UserData, ResData, _serviceID) });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "hotelExtService" + cntStr, TagName = "div", Data = getExtService(UserData, ResData, _serviceID) });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtResNote" + cntStr, TagName = "span", Data = ResData.ResMain.ResNote.ToString() });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "txtAgencyNameA" + cntStr, TagName = "span", Data = agencyAddr.Name });
            htmlData.Add(new TvReport.htmlCodeData { IdName = "bottomNote" + cntStr, TagName = "span", Data = string.Equals(resService.Supplier, "HTLSPRO") ? "Emergency Number 00 632 584 7926" : "Tour Guide, ISTANBUL +90 532 111 1 222 - ANKARA  +90 538 886 19 19 - ANTALYA +90 549 760 82 74 /Other services than specified to be paid by tourist" });
        }
        ResData = new TvBo.Reservation().setPrintVoucherDoc(UserData, ResData, _serviceID, ref errorMsg);
        HttpContext.Current.Session["ResData"] = ResData;
        return Newtonsoft.Json.JsonConvert.SerializeObject(htmlData);
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResServiceRecord> hotels = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer < 2).Select(s => s).ToList<ResServiceRecord>();
        if (hotels == null || hotels.Count < 1) return "";
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("<table style=\"font-family: Tahoma; font-size: 12px;\">");
        object _documentFolder = new Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? TvTools.Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        int i = 0;
        foreach (ResServiceRecord row in hotels)
        {
            sb.Append(" <tr>");
            i++;

            var DocStt = from q1 in ResData.ResCon
                         join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                         where q2.Service == row.Service && q2.RecID == row.RecID
                         group q1 by new { DocStat = q1.DocStat } into k
                         select new { k.Key.DocStat };
            Int16? DocStat = TvTools.Conversion.getInt16OrNull(DocStt.Count() > 1 ? Convert.ToInt16(0) : DocStt.FirstOrDefault().DocStat);

            sb.Append("     <td>");
            string filename = basePageUrl + DocumentFolder + "/" + UserData.Market + "/";

            if (Equals(ResData.ResMain.AllowDocPrint, "Y"))
            {
                sb.AppendFormat("<span onclick=\"showVoucher({0});\" style=\"cursor: pointer; text-decoration: underline;\">{1} (",
                                row.RecID,
                                i.ToString() + ". " + (string.Equals(row.Supplier, "HTLSPRO") ? "WWHOTELS" : row.ServiceNameL));
                sb.Append(((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued")) + ")</span>");
            }
            else
            {

                if (!UserData.AgencyRec.DocPrtNoPay && !(Equals(ResData.ResMain.PaymentStat, "O") || Equals(ResData.ResMain.PaymentStat, "V")))
                    sb.Append("         " + HttpContext.GetGlobalResourceObject("LibraryResource", "NoPaymentNoPrint"));
                else
                {
                    sb.AppendFormat("<span onclick=\"showVoucher({0});\" style=\"cursor: pointer; text-decoration: underline;\">{1} (",
                                row.RecID,
                                i.ToString() + ". " + (string.Equals(row.Supplier, "HTLSPRO") ? "WWHOTELS" : row.ServiceNameL));
                    sb.Append(((DocStat.HasValue && DocStat.Value == 1) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued") : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued")) + ")</span>");
                }
            }
            sb.Append("     </td>");
            sb.Append(" </tr>");
        }
        sb.Append("</table>");

        return sb.ToString();
    }
}
