﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Invoice.aspx.cs" Inherits="Safiran_Invoice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contrat</title>

    <script src="../../../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../../../Scripts/jquery.url.js" type="text/javascript"></script>

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        body { line-height: 1; font-family: Arial; font-size: 8pt; width: 710px; }
        .clearfix:after { content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0; }
        .clear { clear: both; height: 1px; }
        .mainPage { width: 786px; height: 1070px; }
        @media print {
            #noPrintDiv { display: none; visibility: hidden; }
        }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "Invoice.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data, function(i) {
                            switch (this.TagName) {
                                case 'span':
                                    $("#" + this.IdName).text('');
                                    $("#" + this.IdName).text(this.Data);
                                    break;
                                case 'div':
                                    $("#" + this.IdName).html('');
                                    $("#" + this.IdName).html(this.Data.replace(/!/g, '"'));
                                    if (this.AfterInsertShow && this.Data != '')
                                        $("#" + this.IdName + "Div").show();
                                    break;
                                case 'img':
                                    $("#" + this.IdName).removeAttr("src");
                                    $("#" + this.IdName).attr("src", this.Data);
                                    break;
                                default: $("#" + this.IdName).val('');
                                    $("#" + this.IdName).val(this.Data);
                            }
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                getFormData();
                $.query = $.query.load(location.href);
                var resNo = $.query.get('ResNo');
                var docName = $.query.get('docName');
                if ($.query.get('print')) {
                    window.print();
                    window.close();
                }
            });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="mainPage">
        <div id="divLogo" style="text-align: center; height: 46mm;">
            <div style="display: none; visibility: hidden;">
                <img id="imgOperator" alt="" src="" style="height: 45mm;" />
            </div>
            <img alt="" src="yeni_logo.gif" />
        </div>
        <div id="divHeader1" style="height: 30mm;">
            <table cellpadding="2" cellspacing="0" width="100%" style="font-family: Arial; font-size: 10pt;">
                <tr>
                    <td width="70%">
                        <span id="txtClientName"></span>
                    </td>
                    <td width="80px">
                        <span>Invoice Date</span>
                    </td>
                    <td width="5px">
                        :
                    </td>
                    <td>
                        <span id="txtInvoiceDate"></span>
                    </td>
                </tr>
                <tr>
                    <td width="70%">
                        <span id="txtClientAddress"></span>
                    </td>
                    <td width="80px">
                        <span>Book Nr.</span>
                    </td>
                    <td width="5px">
                        :
                    </td>
                    <td>
                        <span id="txtResNo"></span>
                    </td>
                </tr>
                <tr>
                    <td width="70%">
                        <span id="txtClientZip"></span>&nbsp;<span id="txtClientCity"></span>
                    </td>
                    <td width="80px">
                        <span>Telephone</span>
                    </td>
                    <td width="5px">
                        :
                    </td>
                    <td>
                        <span id="txtPhone1"></span>
                    </td>
                </tr>
                <tr>
                    <td width="70%" align="center">
                        <span id="txtClientCountry"></span>
                    </td>
                    <td width="80px">
                        Fax
                    </td>
                    <td width="5px">
                        :
                    </td>
                    <td>
                        <span id="txtFax"></span>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        &nbsp;
                    </td>
                    <td>
                        E-Mail
                    </td>
                    <td width="5px">
                        :
                    </td>
                    <td>
                        <span id="txtEmail"></span>
                    </td>
                </tr>
            </table>
            <br />
            <span style="font-family: Arial; font-size: 10pt; margin-left: 200px;"><b>Reservation
                Book of Number :</b></span> <span id="txtResNo1" style="font-weight: bold;">
            </span>&nbsp;&nbsp;&nbsp;&nbsp; <span id="hotelsProResNote"></span>
        </div>
        <div id="divcenter" style="height: 150mm;">
            <br />
            <div id="hotelGridDiv">
                <div id="hotelGrid">
                </div>
            </div>
            <br />
            <div id="passengerGridDiv">
                <div id="passengerGrid">
                </div>
            </div>
            <br />
            <div id="servicePriceDiv">
                <div id="servicePrice">
                </div>
            </div>
            <br />
        </div>
        <div id="sumDiv" style="text-align: right; height: 20mm;">
            <div id="sum">
            </div>
        </div>
        <div id="divfooter" style="text-align: center; height: 30mm; font-family: Arial;
            font-size: 10pt;">
            <table width="100%">
                <tr>
                    <td style="width: 110px;">
                        <img alt="" src="tursablogo.gif" />
                    </td>
                    <td style="width: 285px; font-weight: bold; padding-right: 5px; border-right: solid 1px #000;"
                        align="right">
                        HEAD OFFICE : Ergenekon Cd.<br />
                        Ahmet Bey Plaza No:10/2<br />
                        Pangalti–Sisli- ISTANBUL<br />
                        Phone : +90 444 0 977<br />
                        Fax : +90 212 235 85 95<br />
                        E-Mail : info@safirantravel.com<br />
                    </td>
                    <td style="width: 285px; font-weight: bold; text-align: left; padding-left: 5px;">
                        BRANCH OFFICE: Kavaklıdere Mh.<br />
                        Büklüm Sk. No:2/28<br />
                        Çankaya – ANKARA<br />
                        Phone : +90 312 418 31 65-69<br />
                        Fax : +90 312 418 31 05<br />
                        E-Mail : info@safirantravel.com<br />
                    </td>
                    <td style="width: 105px;">
                        <img alt="" src="tuvlogo.gif" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
