﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;

public partial class DynamicsPackageBlank : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        if (!IsPostBack)
        {
            Session["SearchPLData"] = null;
            Session["Criteria"] = null;
            Session["Menu"] = new UICommon().getCurrentPageForMenuItem(UserData, Request.Url); //Session["Menu"] = "DynamicsPackageBlank.aspx";
            Response.Redirect("~/DynamicsPackage.aspx" + Request.Url.Query);
        }
    }
}
