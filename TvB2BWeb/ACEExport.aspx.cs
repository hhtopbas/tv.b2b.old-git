﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using System.Text;

public partial class ACEExport : BasePage
{
    protected string baseUrlStr = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        baseUrlStr = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath;
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null) return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];        
        StringBuilder sb = new StringBuilder();

        sb.Append("<br />");
        sb.Append("Please download ACE report, right click &quot;Save Target As..&quot;<br />");

        string aceFileName = string.Empty;
        if (!new Aces().SendAceTo(UserData, ResData, AppDomain.CurrentDomain.BaseDirectory+"ACE\\", ref aceFileName, ref errorMsg))
        {
           sb.Append(errorMsg);
        }
        else
        {
            sb.Append("<a href=\"" + WebRoot.BasePageRoot + "ACE/" + aceFileName + "\">" + aceFileName + "</a>");
        }
        sb.Append("<br />");

        return sb.ToString();
    }

}
