﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgencyPaymentMonitorV2.aspx.cs"
  Inherits="AgencyPaymentMonitorV2" EnableEventValidation="false" %>

<%@ Register Src="~/Comment.ascx" TagName="ResComment" TagPrefix="resCom1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="Common/Header.ascx" TagName="Header" TagPrefix="tv1" %>
<%@ Register Src="Common/Footer.ascx" TagName="Footer" TagPrefix="tv1" %>
<%@ Register Src="Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="tv1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "AgencyPaymentMonitor")%></title>

  <script src="Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
  <script src="Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="Scripts/jquery.cookies.2.2.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
  <script src="Scripts/jquery.textTruncate.js" type="text/javascript"></script>
  <script src="Scripts/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="Scripts/datatable/jquery.dataTables.Pagination.ListBox.js" type="text/javascript"></script>
  <script src="Scripts/datatable/ColVis.js" type="text/javascript"></script>
  <script src="Scripts/datatable/TableTools.js" type="text/javascript"></script>
  <script src="Scripts/jquery.dialogextend.js" type="text/javascript"></script>
  <script src="Scripts/NumberFormat154.js" type="text/javascript"></script>
  <script src="Scripts/jquery.printPage.js" type="text/javascript"></script>
  <script src="Scripts/json2.js" type="text/javascript"></script>

  <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="CSS/confirm.css" rel="stylesheet" type="text/css" />
  <link href="CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="CSS/AgencyPayment.css" rel="stylesheet" type="text/css" />
  <link href="CSS/datatable/jquery.dataTables.css" rel="stylesheet" />
  <link href="CSS/datatable/jquery.dataTables_themeroller.css" rel="stylesheet" />
  <link href="CSS/datatable/ColVisAlt.css" rel="stylesheet" type="text/css" />
  <link href="CSS/datatable/ColVis.css" rel="stylesheet" type="text/css" />
  <link href="CSS/datatable/TableTools.css" rel="stylesheet" type="text/css" />
  <link href="CSS/datatable/TableTools_JUI.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
    .ColVis { width: 150px; top: 0px; }

    .ColVis_Button { text-align: right !important; }

    .dataTables_length { height: 30px !important; }
  </style>

  <script language="javascript" type="text/javascript">

    var twoLetterISOLanguageName = '<%= twoLetterISOLanguageName %>';
    var basePageRoot = '<%= Global.getBasePageRoot() %>';
    var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var btnClose = '<%= GetGlobalResourceObject("LibraryResource", "btnClose") %>';
    var ComboAll = '<%= GetGlobalResourceObject("LibraryResource", "ComboAll") %>';
    var dataTableColVisText = '<%= GetGlobalResourceObject("LibraryResource", "dataTableColVisText") %>';
    var sProcessing = '<%= GetGlobalResourceObject("LibraryResource", "sProcessing") %>';
    var sLengthMenu = '<%= GetGlobalResourceObject("LibraryResource", "sLengthMenu") %>';
    var sZeroRecords = '<%= GetGlobalResourceObject("LibraryResource", "sZeroRecords") %>';
    var sEmptyTable = '<%= GetGlobalResourceObject("LibraryResource", "sEmptyTable") %>';
    var sInfo = '<%= GetGlobalResourceObject("LibraryResource", "sInfo") %>';
    var sInfoEmpty = '<%= GetGlobalResourceObject("LibraryResource", "sInfoEmpty") %>';
    var sInfoFiltered = '<%= GetGlobalResourceObject("LibraryResource", "sInfoFiltered") %>';
    var sInfoPostFix = '<%= GetGlobalResourceObject("LibraryResource", "sInfoPostFix") %>';
    var sSearch = '<%= GetGlobalResourceObject("LibraryResource", "sSearch") %>';
    var sUrl = '<%= GetGlobalResourceObject("LibraryResource", "sUrl") %>';
    var sFirst = '<%= GetGlobalResourceObject("LibraryResource", "sFirst") %>';
    var sPrevious = '<%= GetGlobalResourceObject("LibraryResource", "sPrevious") %>';
    var sNext = '<%= GetGlobalResourceObject("LibraryResource", "sNext") %>';
    var sLast = '<%= GetGlobalResourceObject("LibraryResource", "sLast") %>';
    var sPage = '<%= GetGlobalResourceObject("LibraryResource", "sPage") %>';
    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var lblSessionEnd = '<%= GetGlobalResourceObject("HeaderResource", "lblSessionEnd") %>';
    $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    var defaultData;
    var cultureNumber;
    var cultureDate;
    var selectedPaymentList = [];
    var paymentDetail = new Object();

    if (typeof window.event != 'undefined')
      document.onkeydown = function () {
        if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
          return (event.keyCode != 8);
      }
    else
      document.onkeypress = function (e) {
        if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
          return (e.keyCode != 8);
      }

    function logout() {
      $('<div>' + lblSessionEnd + '</div>').dialog({
        autoOpen: true,
        position: {
          my: 'center',
          at: 'center'
        },
        modal: true,
        resizable: true,
        autoResize: true,
        bigframe: true,
        buttons: [{
          text: btnOK,
          click: function () {
            $(this).dialog("close");
            $(this).dialog("destroy");
            window.location = 'Default.aspx';
          }
        }]
      });
    }

    function showAlert(msg) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
            }
          }]
        });
      });
    }

    function showDialog(msg, transfer, trfUrl) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
              if (transfer == true) {
                var url = trfUrl;
                $(location).attr('href', url);
              }
            }
          }]
        });
      });
    }

    function showMessage(msg, transfer, trfUrl) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
            }
          }, {
            text: btnCancel,
            click: function () {
              $(this).dialog('close');
            }
          }]
        });
      });
    }

    function showJournalRef(resNo) {
      var params = new Object();
      params.ResNo = resNo;
      $.ajax({
        type: "POST",
        data: JSON.stringify(params),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'AgencyPaymentMonitorV2.aspx/getJournalRef',
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null && msg.d != '') {
            $('#divResPayment').html(msg.d);
          } else {
            $('#divResPayment').html('Record not found.');
          }
          $("#dialog").dialog("destroy");
          $("#dialog-journalRef").dialog({
            modal: true,
            width: 1024,
            height: 300,
            buttons: [{
              text: btnOK,
              click: function () {
                $(this).dialog('close');
                return false;
              }
            }]
          });

        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function closePaymentDiv() {
      $("#dialog-totalPayment").dialog('close');
      selectedPaymentList = [];
      paymentDetail = new Object();
      selectedPaymentList = [];
      $("#editPaymentSum").val('');
      $("#selectedPaymentSum").val('');
      $("#sumPaymentDiv").removeData("totalValue");
      createFilterData(true);
      btnFilterClick();
    }

    function tryNumberFormat(obj) {
      var currentCulture = $("body").data("CurrentCulture");
      var nf = new NumberFormat(obj);
      if (cultureNumber != null) {
        nf.PERIOD = cultureNumber.CurrencyGroupSeparator;
        nf.COMMA = cultureNumber.CurrencyDecimalSeparator;
      }
      var rsdoS = nf.PERIOD;
      var rsdoD = nf.COMMA;
      nf.setPlaces(2);
      nf.setSeparators(true, rsdoS, rsdoD);
      obj = nf.toFormatted();
      return obj;
    }

    var NS = document.all;

    function maximize() {
      var maxW = screen.availWidth;
      var maxH = screen.availHeight;
      if (location.href.indexOf('pic') == -1) {
        if (window.opera) { } else {
          top.window.moveTo(0, 0);
          if (document.all) { top.window.resizeTo(maxW, maxH); }
          else
            if (document.layers || document.getElementById) {
              if ((top.window.outerHeight < maxH) || (top.window.outerWidth < maxW)) {
                top.window.outerHeight = maxH; top.window.outerWidth = maxW;
              }
            }
        }
      }
    }

    function setDepCity(data) {
      $("#fltDepCity").html('');
      $("#fltDepCity").append("<option value=''>" + ComboAll + "</option>");
      $.each(data.DepCityData, function (i) {
        $("#fltDepCity").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
      });
      if (data.Filter.DepCity != null && data.Filter.DepCity != '') $("#fltDepCity").val(data.Filter.DepCity);
    }

    function setArrCity(data) {
      $("#fltArrCity").html('');
      $("#fltArrCity").append("<option value=''>" + ComboAll + "</option>");
      $.each(data.ArrCityData, function (i) {
        $("#fltArrCity").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
      });
      if (data.Filter.ArrCity != null && data.Filter.ArrCity != '') $("#fltArrCity").val(data.Filter.ArrCity);
    }

    function setAgencyOffice(data) {
      $("#fltAgencyOffice").html('');
      $("#fltAgencyOffice").data('UserList', data.AgencyUserData);
      $("#fltAgencyOffice").data('User', data.Filter.AgencyUser);
      $("#fltAgencyOffice").data('ShowAllRes', data.Filter.ShowAllRes);

      $("#fltAgencyOffice").append("<option value=''>" + ComboAll + "</option>");
      $.each(data.AgencyOfficeData, function (i) {
        $("#fltAgencyOffice").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
      });
      if (data.Filter.AgencyOffice != null && data.Filter.AgencyOffice != '') $("#fltAgencyOffice").val(data.Filter.AgencyOffice);
    }

    function changeAgencyOffice(agencyOffice) {
      var userList = $("#fltAgencyOffice").data('UserList');
      var user = $("#fltAgencyOffice").data('User');
      var showAllRes = $("#fltAgencyOffice").data('ShowAllRes');
      var activeUserList = [];
      if (agencyOffice != '') {
        $.each(userList, function (i) {
          if (this.ParentCode == agencyOffice)
            activeUserList.push(this);
        });
      }
      else {
        activeUserList = userList;
      }
      setAgencyUser(activeUserList, user, showAllRes);
    }

    function setAgencyUser(data, AgencyUser, ShowAllRes) {
      $("#fltAgencyUser").html('');
      $("#fltAgencyUser").append("<option value=''>" + ComboAll + "</option>");
      $.each(data, function (i) {
        $("#fltAgencyUser").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
      });
      if (!ShowAllRes) {
        $("#fltAgencyUser").attr("disabled", true);
        $("#fltAgencyOffice").attr("disabled", true);

      }
      else {
        $("#fltAgencyUser").attr("disabled", false);
        $("#fltAgencyOffice").attr("disabled", false);
      }

      if (AgencyUser != null && AgencyUser != '')
        $("#fltAgencyUser").val(AgencyUser);
    }

    function setOtherFields(data) {
      $('input[name=fltResStatus]').each(function (index) { this.checked = false; });
      if (data.Filter.ResStatus != null && data.Filter.ResStatus != '') {
        for (i = 0; i < data.Filter.ResStatus.split(';').length; i++) {
          $("#fltResStatus" + data.Filter.ResStatus.split(';')[i]).attr("checked", "checked");
        }
      }
      $('input[name=fltConfirmStatus]').each(function (index) { this.checked = false; });
      if (data.Filter.ConfStatus != null && data.Filter.ConfStatus != '') {
        for (i = 0; i < data.Filter.ConfStatus.split(';').length; i++) {
          $("#fltConfirmStatus" + data.Filter.ConfStatus.split(';')[i]).attr("checked", "checked");
        }
      }
      $('input[name=fltPaymentStatus]').each(function (index) { this.checked = false; });
      if (data.Filter.PayStatus != null && data.Filter.PayStatus != '') {
        for (i = 0; i < data.Filter.PayStatus.split(';').length; i++) {
          $("#fltPaymentStatus" + data.Filter.PayStatus.split(';')[i]).attr("checked", "checked");
        }
      }

      $("#fltSurname").val(data.Filter.LeaderNameF != null ? data.Filter.LeaderNameF : "");
      $("#fltName").val(data.Filter.LeaderNameS != null ? data.Filter.LeaderNameS : "");
      var _date = '';


      if (data.Filter.DueDate != null && data.Filter.DueDate != '') {
        var _date = new Date(Date(eval(data.Filter.DueDate)).toString());
        $("#fltDueDate").val($.format.date(_date, data.Filter.DateFormat));
      }
      else $("#fltBeginDate1").val('');

      if (data.Filter.BeginDate1 != null && data.Filter.BeginDate1 != '') {
        var _date = new Date(Date(eval(data.Filter.BeginDate1)).toString());
        $("#fltBeginDate1").val($.format.date(_date, data.Filter.DateFormat));
      }
      else $("#fltBeginDate1").val('');

      if (data.Filter.BeginDate2 != null && data.Filter.BeginDate2 != '') {
        var _date = new Date(Date(eval(data.Filter.BeginDate2)).toString());
        $("#fltBeginDate2").val($.format.date(_date, data.Filter.DateFormat));
      }
      else $("#fltBeginDate2").val('');

      if (data.Filter.ResDate1 != null && data.Filter.ResData1 != '') {
        var _date = new Date(Date(eval(data.Filter.ResDate1)).toString());
        $("#fltResDate1").val($.format.date(_date, data.Filter.DateFormat));
      }
      else $("#fltResDate1").val('');

      if (data.Filter.ResDate2 != null && data.Filter.ResData2 != '') {
        var _date = new Date(Date(eval(data.Filter.ResDate2)).toString());
        $("#fltResDate2").val($.format.date(_date, data.Filter.DateFormat));
      }
      else $("#fltResDate2").val('');

      btnFilterClick();
    }

    function showResNote(msg) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
            }
          }]
        });
      });
    }

    function showPaymentPlanDiv(msg) {
      $(function () {
        $('#paymentPlan').html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-paymentPlan").dialog({
          modal: true,

          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
              return false;
            }
          }]
        });
      });
    }

    function showPaymentPlan(resNo, ci) {
      var params = new Object();
      params.ResNo = resNo;
      params.lcID = ci;
      $.ajax({
        type: "POST",
        data: JSON.stringify(params),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'Services/ResMonitorServices.asmx/getPaymentPlanData',
        success: function (msg) {
          showPaymentPlanDiv(msg.d);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function showResView(ResNo) {
      window.location = $("#basePageUrl").val() + "ResView.aspx?ResNo=" + ResNo;
    }

    $.extend($.ui.dialog.prototype, {
      'addbutton': function (buttonName, func) {
        var buttons = this.element.dialog('option', 'buttons');
        buttons[buttonName] = func;
        this.element.dialog('option', 'buttons', buttons);
      }
    });

    // Allows simple button removal from a ui dialog
    $.extend($.ui.dialog.prototype, {
      'removebutton': function (buttonName) {
        var buttons = this.element.dialog('option', 'buttons');
        delete buttons[buttonName];
        this.element.dialog('option', 'buttons', buttons);
      }
    });

    function viewReportAddButton(btnName, func) {
      $("#dialog-viewReport").dialog('addbutton', btnName, func);
    }

    function viewReportRemoveButton(btnName) {
      $("#dialog-viewReport").dialog('removebutton', btnName);
    }

    function viewReportPrint(url, width, height) {
      if ($.browser.msie) {
        var windowSizeArray = ["width=" + width + ",height=" + height + ",scrollbars=yes"];
        var windowName = "popUp";
        var windowSize = windowSizeArray[0];
        window.open(url, windowName, windowSize);
      }
      else {
        $("#viewReport").width(width + 32);
        $("#viewReport").height(height + 32);
        $("#viewReport").contents().find(".mainPage").printPage($("#viewReport").contents().find('style'), width);
      }
    }

    function ReportShow(param1, param2) {
      if (param1 == '' && (param2 != null || param2 != ''))
        showAlert(param2);
      else window.open(param1);
    }

    function htmlPageRender(resNo, htmlUrl, DocName, docNameResource) {
      $("#viewReport").removeAttr("src");
      $("#viewReport").attr("src", htmlUrl + "?ResNo=" + resNo + "&docName=" + docNameResource);
      $("#dialog-viewReport").dialog(
      {
        autoOpen: true,
        modal: true,
        buttons: [{
          text: btnClose,
          click: function () {
            $(this).dialog('close');
            return true;
          }
        }]
      })
      .dialogExtend({
        "maximize": true,
        "icons": {
          "maximize": "ui-icon-circle-plus",
          "restore": "ui-icon-pause"
        }
      });
      $("#dialog-viewReport").dialogExtend("maximize");
    }

    function pdfFileView(htmlUrl) {
      var btnClose = btnClose;
      $("#viewPDF").removeAttr("src");
      var _pdfViewPage = "ViewPDF.aspx"
      $("#viewPDF").attr("src", htmlUrl + "?url=" + encodeURIComponent(htmlUrl));
      $('html').css('overflow', 'hidden');
      $("#dialog-viewPDF").dialog(
      {
        bgiframe: true,
        autoOpen: true,
        modal: true,
        open: function (event, ui) { },
        close: function (event, ui) { $('html').css('overflow', 'auto'); },
        buttons: [{
          text: btnClose,
          click: function () {
            $(this).dialog('close');
            return true;
          }
        }]
      }).dialogExtend({
        "maximize": true,
        "icons": {
          "maximize": "ui-icon-circle-plus",
          "restore": "ui-icon-pause"
        }
      });
      $("#dialog-viewPDF").dialogExtend("maximize");
    }

    function reportResultShow(param1, param2, ResultType) {
      if (param1 == '' && (param2 != null || param2 != '')) {
        showAlert(param2);
      }
      else {
        if (ResultType == 'PDF')
          pdfFileView(param1);
        else if (ResultType == 'HTMLPDF') {

        }
      }
    }

    function getLanguageID(resNo) {
      var multiLanguageID = null;
      var param = new Object();
      param.ResNo = resNo;
      $.ajax({
        async: false,
        type: "POST",
        data: JSON.stringify(param),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'AgencyPaymentMonitorV2.aspx/getMultiLangID',
        success: function (msg) {
          var html = '';
          if (msg.hasOwnProperty('d') && msg.d != null && msg.d.length > 0) {
            $("#langListDiv").html('');
            $.each(msg.d, function (i) {
              var clickBtn = "'" + resNo + "'," + this.Id;
              $("#langListDiv").append('<a href="#" onclick="getInvoice(' + clickBtn + ')">' + this.Name + '</a><br />');
            });
            $("#dialog").dialog("destroy");
            $("#dialog-SelectReportLang").dialog({
              position: 'center',
              modal: true
            });
          }
          else {
            multiLanguageID = null;
            showInvoice(ResNo, multiLanguageID);
          }

        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getInvoice(ResNo, langId) {
      $("#dialog-SelectReportLang").dialog('close');
      showInvoice(ResNo, langId);
    }

    function showResInv(ResNo) {
      getLanguageID(ResNo);
    }

    function showInvoice(ResNo, langId) {
      var param = new Object();
      param.ResNo = ResNo;
      param.LangId = parseInt(langId != undefined ? langId : null);
      $.ajax({
        async: false,
        type: "POST",
        url: "AgencyPaymentMonitorV2.aspx/getInvoice",
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != null && msg.d != '') {
            var retVal = $.json.decode(msg.d);
            reportResultShow(retVal.Param1, retVal.Param2, retVal.ResultType);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });

    }

    function loadFilter() {
      var fData = defaultData.Filter;

      var _resStatus = '';
      var _confStatus = '';
      var _payStatus = '';

      var chkBox = $("input:checked");
      for (i = 0; i < chkBox.length; i++) {
        if (chkBox[i].name == 'fltResStatus') {
          if (_resStatus.length > 0) _resStatus += ',';
          _resStatus += chkBox[i].value;
        }
        if (chkBox[i].name == 'fltConfirmStatus') {
          if (_confStatus.length > 0) _confStatus += ',';
          _confStatus += chkBox[i].value;
        }
        if (chkBox[i].name == 'fltPaymentStatus') {
          if (_payStatus.length > 0) _payStatus += ',';
          _payStatus += chkBox[i].value;
        }
      }
      var _includeDueDate = $("#fltIncludeDueDate:checked").is(':checked') ? '1' : '0';
      if ($("#fltDueDate").datepicker("getDate") != null) fData.DueDate = "\/Date(" + $("#fltDueDate").datepicker("getDate").getTime() + ")\/"; else fData.DueDate = null;
      if ($("#fltBeginDate1").datepicker("getDate") != null) fData.BegDate1 = "\/Date(" + $("#fltBeginDate1").datepicker("getDate").getTime() + ")\/"; else fData.BegDate1 = null;
      if ($("#fltBeginDate2").datepicker("getDate") != null) fData.BegDate2 = "\/Date(" + $("#fltBeginDate2").datepicker("getDate").getTime() + ")\/"; else fData.BegDate2 = null;
      if ($("#fltResDate1").datepicker("getDate") != null) fData.ResDate1 = "\/Date(" + $("#fltResDate1").datepicker("getDate").getTime() + ")\/"; else fData.ResDate1 = null;
      if ($("#fltResDate2").datepicker("getDate") != null) fData.ResDate2 = "\/Date(" + $("#fltResDate2").datepicker("getDate").getTime() + ")\/"; else fData.ResDate2 = null;

      fData.ResNo1 = $("#fltResNo1").val();
      fData.ResNo2 = $("#fltResNo2").val();

      if ($("#fltDepCity").val() != '') fData.DepCity = parseInt($("#fltDepCity").val()); else fData.DepCity = null;
      if ($("#fltArrCity").val() != '') fData.ArrCity = parseInt($("#fltArrCity").val()); else fData.ArrCity = null;

      fData.AgencyUser = $("#fltAgencyUser").val();
      fData.LeaderNameF = $("#fltName").val();
      fData.LeaderNameS = $("#fltSurname").val();
      fData.PayStatus = _payStatus;
      fData.ConfStatus = _confStatus;
      fData.ResStatus = _resStatus;
      fData.IncludeDueDate = $("#fltIncludeDueDate:checked").is(':checked');
      fData.AgencyOffice = $("#fltAgencyOffice").val();
      fData.PageNo = 1;
      defaultData.Filter = fData;

      $.ajax({
        async: false,
        type: "POST",
        data: '{"data":"' + $.json.encode(fData).replace(/{/g, '<').replace(/}/g, '>').replace(/"/g, '|') + '"}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'AgencyPaymentMonitorV2.aspx/GetSummary',
        success: function (msg) {
          $('#divSum').html('');
          $('#divSum').html(msg.d);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function btnFilterClick() {
      selectedPaymentList = [];
      loadFilter();
      $("#resultTableDiv").html('');
      $("#resultTableDiv").html('<table cellspacing="0" cellpadding="0" border="0" id="resTable" class="display" style="margin-left: 0pt;"></table>');
      showSummary();
    }

    function showSummary() {
      var oTable = null;
      var _aoColumns = [];
      $.ajax({
        async: false,
        type: "POST",
        data: '{}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'AgencyPaymentMonitorV2.aspx/getGridHeaders',
        success: function (msg) {
          if (msg.d != null && msg.d != '') {
            $.each($.json.decode(msg.d), function (i) {
              obj = new Object();
              if (this.sTitle != undefined && this.sTitle != null) obj.sTitle = this.sTitle;
              if (this.sWidth != undefined && this.sWidth != null) obj.sWidth = this.sWidth;
              if (this.bSortable != undefined && this.bSortable != null) obj.bSortable = this.bSortable;
              if (this.sType != undefined && this.sType != null) obj.sType = this.sType;
              if (this.sClass != undefined && this.sClass != null) obj.sClass = this.sClass;
              if (this.bSearchable != undefined && this.bSearchable != null) obj.bSearchable = this.bSearchable;
              _aoColumns.push(obj);
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });

      var oLanguage = {
        "sProcessing": sProcessing,
        "sLengthMenu": sLengthMenu,
        "sZeroRecords": sZeroRecords,
        "sEmptyTable": sEmptyTable,
        "sInfo": sInfo,
        "sInfoEmpty": sInfoEmpty,
        "sInfoFiltered": sInfoFiltered,
        "sInfoPostFix": sInfoPostFix,
        "sSearch": sSearch,
        "sUrl": sUrl,
        "oPaginate": {
          "sFirst": sFirst,
          "sPrevious": sPrevious,
          "sNext": sNext,
          "sLast": sLast,
          "sPage": sPage
        },
        "fnInfoCallback": null
      };
      var first = true;


      oTable = $('#resTable').dataTable({
        "sDom": 'C<"clear">lfrtip',
        "bDestory": false,
        "bRetrieve": true,
        "bStateSave": true,
        "sScrollX": 999,
        "bAutoWidth": false,
        "bScrollCollapse": true,
        "bPaginate": true,
        "sPaginationType": "listbox",
        "bFilter": false,
        "bJQueryUI": true,
        "oLanguage": oLanguage,
        "aoColumns": _aoColumns,
        "bProcessing": false,
        "bServerSide": true,
        //"aaSorting": [],
        "sServerMethod": "POST",
        "sAjaxSource": "AgencyPaymentMonitorV2Data.aspx",
        "fnStateSave": function (oSettings, oData) {
          localStorage.setItem('DataTables', $.json.encode(oData));
        },
          "fnStateLoad": function (oSettings) {
              if (localStorage != undefined && localStorage.length > 0 && localStorage.getItem('DataTables') != null)

            return $.json.decode(localStorage.getItem('DataTables'));
        },
        "fnStateLoadParams": function (oSettings, oData) {

          if (first) {
            oData.iStart = 0;
            oSettings._iDisplayStart = 0;
            if (oData.oFilter != null && oData.oFilter != undefined && oData.oFilter.sSearch != null && oData.oFilter.sSearch != undefined)
              oData.oFilter.sSearch = "";
          }
          first = false;
        },
        "oColVis": {
          "buttonText": dataTableColVisText,
          "sAlign": "right"
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
          var tmp = sSource;
          $.ajax({
            async: false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "success": function (data, textStatus, xmlHttpRequest) {

              var json = {
                "sEcho": aoData[0].value,
                "aaData": [],
                "iTotalRecords": data.iTotalRecords,
                "iTotalDisplayRecords": data.iTotalDisplayRecords
              };
              $.each(data.aaData, function (i) {
                var obj = [];
                if (this.SelectRes != undefined) obj.push(this.SelectRes);
                  if (this.ResNo != undefined) obj.push(this.ResNo);
                  if (this.OptionDate != undefined) obj.push(this.OptionDate);
                if (this.DepCityName != undefined) obj.push(this.DepCityName);
                if (this.ArrCityName != undefined) obj.push(this.ArrCityName);
                if (this.BegDate != undefined) obj.push(this.BegDate);
                if (this.EndDate != undefined) obj.push(this.EndDate);
                if (this.Days != undefined) obj.push(this.Days);
                if (this.statusStr != undefined) obj.push(this.statusStr);
                if (this.ResDate != undefined) obj.push(this.ResDate);
                if (this.LeaderName != undefined) obj.push(this.LeaderName);
                if (this.Adult != undefined) obj.push(this.Adult);
                if (this.Child != undefined) obj.push(this.Child);
                if (this.SaleCur != undefined) obj.push(this.SaleCur);
                if (this.SalePrice != undefined) obj.push(this.SalePrice);
                if (this.AgencyCom != undefined) obj.push(this.AgencyCom);
                if (this.DiscountFromCom != undefined) obj.push(this.DiscountFromCom);
                if (this.Discount != undefined) obj.push(this.Discount);
                if (this.BrokerCom != undefined) obj.push(this.BrokerCom);
                if (this.AgencySupDis != undefined) obj.push(this.AgencySupDis);
                if (this.AgencyComSup != undefined) obj.push(this.AgencyComSup);
                if (this.EBAgency != undefined) obj.push(this.EBAgency);
                if (this.EBPas != undefined) obj.push(this.EBPas);
                if (this.AgencyPayable != undefined) obj.push(this.AgencyPayable);
                if (this.AgencyPayable2 != undefined) obj.push(this.AgencyPayable2);
                if (this.AgencyPayment != undefined) obj.push(this.AgencyPayment);
                if (this.Balance != undefined) obj.push(this.Balance);
                  if (this.JournalRef != undefined) obj.push(this.JournalRef);
                  
                if (this.UserBonus != undefined) obj.push(this.UserBonus);
                  if (this.AgencyBonus != undefined) obj.push(this.AgencyBonus);
                  
                json.aaData.push(obj);

              });

              fnCallback(json);
            },
            error: function (xhr, msg, e) {
              if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                showAlert(xhr.responseText);
            },
            statusCode: {
              408: function () {
                logout();
              }
            }
          });
        }
      }).fnAdjustColumnSizing();
      //var table = $.fn.dataTable.fnTables(true);
      //if (table.length > 0) {
      //    $(table).dataTable().fnAdjustColumnSizing();
      //}
    }

    function getDefaultData(clear) {
      var retVal;
      $.ajax({
        async: false,
        type: "POST",
        data: '{"clear":"' + clear + '"}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'AgencyPaymentMonitorV2.aspx/CreateFilterData',
        success: function (msg) {
          if (msg.hasOwnProperty('d')) {
            retVal = msg.d;
          }
          else retVal = null;
          return retVal;
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
      return retVal;
    }

    function createFilterData(clear) {
      paymentDetail = new Object();
      selectedPaymentList = [];
      $("#editPaymentSum").val('');
      $("#selectedPaymentSum").val('');
      $("#sumPaymentDiv").removeData("totalValue");

      defaultData = getDefaultData(true);
      $("#fltDueDate").datepicker("setDate", null);
      $("#fltBeginDate1").datepicker("setDate", null);
      $("#fltBeginDate2").datepicker("setDate", null);
      $("#fltResDate1").datepicker("setDate", null);
      $("#fltResDate2").datepicker("setDate", null);
      $("#fltResNo1").val('');
      $("#fltResNo2").val('');
      setDepCity(defaultData);
      setArrCity(defaultData);
      setAgencyOffice(defaultData);
      setAgencyUser(defaultData.AgencyUserData, defaultData.Filter.AgencyUser, defaultData.Filter.ShowAllRes);
      setOtherFields(defaultData);
      $("body").data("CurrentCulture", defaultData.currentCultureNumber);
    }

    function showExportHtml(_html) {
      top.window.scrollTo(0, 0);
      $("#exportDiv").html('');
      $("#exportDiv").html(_html);
      $("#dialog").dialog("destroy");
      $("#dialog-export").dialog({
        modal: true,
        buttons: [{
          text: btnClose,
          click: function () {
            $("#dialog-export").dialog("close");
            return false;
          }
        }]
      }).dialogExtend({
        "maximize": true,
        "icons": {
          "maximize": "ui-icon-circle-plus",
          "restore": "ui-icon-pause"
        }
      });
      $("#dialog-export").dialogExtend("maximize");
    }

    function exportExcel() {
      $.ajax({
        type: "POST",
        data: '{}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'AgencyPaymentMonitorV2.aspx/exportExcel',
        success: function (msg) {
          if (msg.d != '') {
            window.open(msg.d, '_blank');
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function exportHtml() {
      $.ajax({
        type: "POST",
        data: '{}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'AgencyPaymentMonitorV2.aspx/exportHtml',
        success: function (msg) {
          if (msg.d != '') {
            showExportHtml(msg.d);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function selectedRowUpdate(resNo, selected) {
      var param = new Object();
      param.ResNo = resNo;
      param.Selected = selected;
      $.ajax({
        type: "POST",
        data: $.json.encode(param),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'AgencyPaymentMonitorV2.aspx/selectedRowUpdate',
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            if (msg.d.show) {
              $("#selectedSumPaymentDiv").show();
              $("#editPaymentDiv").hide();
              $("#selectedPaymentSum").html('');
              $("#selectedPaymentSum").html(msg.d.innerHtml);
            }
            else {
              $("#selectedSumPaymentDiv").hide();
              $("#editPaymentDiv").show();
            }
          }
          else {
            $("#selectedSumPaymentDiv").hide();
            $("#editPaymentDiv").show();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function addListSelectedRow() {

    }

    function selectRow(_this) {
      var total = 0.0;
      if ($("#sumPaymentDiv").data("totalValue") == undefined) {
        $("#sumPaymentDiv").data("totalValue", 0.0);
      }
      else {
        total = $("#sumPaymentDiv").data("totalValue");
        totalValue = total;
      }
      var selected = false;
      var selectedPaymentList = [];
      if ($(_this).attr("checked") == "checked") {
        selectedRowUpdate($(_this).val(), true);
      }
      else {
        selectedRowUpdate($(_this).val(), false);
      }
      if ($("input[name='selectedReservation']").length > 0) {
        total = 0.0;
        $.each($("input[name='selectedReservation']"), function (i) {
          var value = parseFloat($(this).attr("balance")) / 100;
          var curr = $(this).attr("curr");
          total += value;
          if ($(this).attr("checked") == "checked") {
            selected = true;
            var obj = new Object();
            obj.ResNo = $(this).val();
            obj.Balance = value;
            obj.Curr = curr;
            selectedPaymentList.push(obj);
          }
          else {
            total -= parseFloat($(this).attr("balance")) / 100;
          }
        });
      }
      if (selectedPaymentList.length > 0) {
        $("#selectedSumPaymentDiv").show();
        $("#editPaymentDiv").hide();
      }
      else {
        $("#selectedSumPaymentDiv").hide();
        $("#editPaymentDiv").show();
      }
    }

    function gotoPayment(value, paymentList) {
      var editPayment = $("#editPaymentSum");
      try {
        var value = parseFloat(editPayment.val());
        if (value == NaN || editPayment.val() == '') {
          paymentDetail.value = null;
        }
        else {
          paymentDetail.value = value * 100;
        }
      }
      catch (e) {
        paymentDetail.value = null;
      }
      $("#totalPayment").removeAttr("src");
      var htmlUrl = basePageRoot + "Payments/Novaturas/Default.aspx";
      $("#totalPayment").attr("src", htmlUrl + "?Auto=" + (paymentDetail.value == null ? "0" : "1") + "&Manuel=" + paymentDetail.value);

      window.scrollTo(0, 0);
      $('html').css('overflow', 'hidden');

      $("#dialog-totalPayment").dialog(
      {
        autoOpen: true,
        modal: true,
        close: function (event, ui) {
          $('html').css('overflow', 'auto');

          selectedPaymentList = [];
          paymentDetail = new Object();
          selectedPaymentList = [];
          $("#editPaymentSum").val('');
          $("#selectedPaymentSum").val('');
          $("#sumPaymentDiv").removeData("totalValue");
          deleteSelectedList();
        }
      }).dialogExtend({
        maximize: true,
        icons: {
          maximize: "ui-icon-circle-plus",
          restore: "ui-icon-pause"
        }
      });
      $("#dialog-totalPayment").dialogExtend("maximize");
    }

    function deleteSelectedList() {
      selectedPaymentList = [];
      $("#editPaymentSum").val('');
      $.ajax({
        type: "POST",
        data: '{}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: 'AgencyPaymentMonitorV2.aspx/deleteSelectedList',
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            if (msg.d.show) {
              $("#selectedSumPaymentDiv").show();
              $("#editPaymentDiv").hide();
              $("#selectedPaymentSum").html('');
              $("#selectedPaymentSum").html(msg.d.innerHtml);
            }
            else {
              $("#selectedSumPaymentDiv").hide();
              $("#editPaymentDiv").show();
            }
            btnFilterClick();
          }
          else {
            $("#selectedSumPaymentDiv").hide();
            $("#editPaymentDiv").show();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showAlert(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function summaryPayment() {
      if ($("input[name='selectedReservation']").length > 0) {
        var resList = [];
        if ($("#editPaymentSum").is(":visible")) {
          var editPayment = $("#editPaymentSum");
          var value = parseFloat(editPayment.val());
          gotoPayment(value, null);
        }
        else {
          var selectedPayment = $("#selectedPaymentSum");
          $.each($("input[name='selectedReservation']"), function (i) {
            if ($(this).attr("checked") == "checked") {
              var value = parseFloat($(this).attr("balance")) / 100;
              var obj = new Object();
              obj.ResNo = $(this).val();
              obj.Payment = value;
              resList.push(obj);
            }
          });
          gotoPayment(null, resList);
        }
      } else
        if ($("#editPaymentSum").is(":visible")) {
          var editPayment = $("#editPaymentSum");
          var value = parseFloat(editPayment.val());
          gotoPayment(value, null);
        }

    }

    $(document).ready(function () {
      selectedPaymentList = [];
      maximize();
      defaultData = getDefaultData(true);
      if (defaultData.showAgencyPayment == true) {

        $("#sumPaymentDiv").show();
      }
      else {

        $("#sumPaymentDiv").hide();
      }
      $.datepicker.setDefaults($.datepicker.regional[twoLetterISOLanguageName != 'en' ? twoLetterISOLanguageName : '']);

      $("#fltDueDate").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true
      });
      $("#fltBeginDate1").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            var date1 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
            if ($("#fltBeginDate2").datepicker("getDate") != null) {
              var date2 = $("#fltBeginDate2").datepicker("getDate");
              if (date2.getTime() < date1.getTime()) {
                $("#fltBeginDate2").datepicker("setDate", date1);
              }
            }
          }
        }
      });
      $("#fltBeginDate2").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
            if ($("#fltBeginDate1").datepicker("getDate") != null) {
              var date1 = $("#fltBeginDate1").datepicker("getDate");
              if (date2.getTime() < date1.getTime()) {
                $("#fltBeginDate1").datepicker("setDate", date2);
              }
            }
          }
        }
      });
      $("#fltResDate1").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            var date1 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
            if ($("#fltResDate2").datepicker("getDate") != null) {
              var date2 = $("#fltResDate2").datepicker("getDate");
              if (date2.getTime() < date1.getTime()) {
                $("#fltResDate2").datepicker("setDate", date1);
              }
            }
          }
        }
      });
      $("#fltResDate2").datepicker({
        showOn: "button",
        buttonImage: "Images/Calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onSelect: function (dateText, inst) {
          if (dateText != '') {
            var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
            if ($("#fltResDate1").datepicker("getDate") != null) {
              var date1 = $("#fltResDate1").datepicker("getDate");
              if (date2.getTime() < date1.getTime()) {
                $("#fltResDate1").datepicker("setDate", date2);
              }
            }
          }
        }
      });
      setDepCity(defaultData);
      setArrCity(defaultData);
      setAgencyOffice(defaultData);
      setAgencyUser(defaultData, defaultData.Filter.AgencyUser, defaultData.Filter.ShowAllRes);
      setOtherFields(defaultData);
    });

  </script>

</head>
<body>
  <form id="ResMonitorForm" runat="server">
    <div class="Page">
      <tv1:Header ID="tvHeader" runat="server" />
      <tv1:MainMenu ID="tvMenu" runat="server" />
      <div class="FilterArea">
        <div>
          <div class="LeftDiv border">
            <div class="divResNo">
              <span class="Caption">
                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblDueDate")%>
                            : </span>
              <div style="width: 120px; float: left;">
                <input id="fltDueDate" type="text" class="dateStyle" />
              </div>
              <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
              </div>
              <div style="width: 125px; float: left;">
                <input id="fltIncludeDueDate" type="checkbox" checked="checked" />
                <label for="fltIncludeDueDate">
                  <%= GetGlobalResourceObject("AgencyPaymentMonitor", "fltIncludeDueDate")%></label>
              </div>
            </div>
            <div class="divResBegDate">
              <span class="Caption">
                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "BegDate")%>
                            : </span>
              <div style="width: 115px; float: left; white-space: nowrap;">
                <input id="fltBeginDate1" type="text" class="dateStyle" />
              </div>
              <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                <span style="font-size: 7pt; font-weight: bold; font-family: Tahoma;">&gt;&gt;</span>
              </div>
              <div style="width: 115px; float: left; white-space: nowrap;">
                <input id="fltBeginDate2" type="text" class="dateStyle" />
              </div>
            </div>
            <div class="divResEndDate">
              <span class="Caption">
                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblSaleDate")%>
                            : </span>
              <div style="width: 115px; float: left; white-space: nowrap;">
                <input id="fltResDate1" type="text" class="dateStyle" />
              </div>
              <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                <span style="font-size: 7pt; font-weight: bold; font-family: Tahoma;">&gt;&gt;</span>
              </div>
              <div style="width: 115px; float: left; white-space: nowrap;">
                <input id="fltResDate2" type="text" class="dateStyle" />
              </div>
            </div>
            <div class="divResEndDate">
              <span class="Caption">
                <%= GetGlobalResourceObject("ResMonitor", "lblResNo")%>
                            : </span>
              <div style="width: 115px; float: left; white-space: nowrap;">
                <input id="fltResNo1" type="text" class="dateStyle" />
              </div>
              <div style="width: 20px; float: left; margin-top: 4px; text-align: center;">
                <span style="font-size: 7pt; font-weight: bold; font-family: Tahoma;">&gt;&gt;</span>
              </div>
              <div style="width: 115px; float: left; white-space: nowrap;">
                <input id="fltResNo2" type="text" class="dateStyle" />
              </div>
            </div>
            <div class="divDepCity">
              <span class="Caption">
                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblDepCity")%>
                            : </span>
              <div style="width: 300px; float: left; text-align: left;">
                <select id="fltDepCity" style="width: 95%;">
                </select>
              </div>
            </div>
            <div class="divArrCity">
              <span class="Caption">
                <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblArrCity")%>
                            : </span>
              <div style="width: 300px; float: left; text-align: left;">
                <select id="fltArrCity" style="width: 95%;">
                </select>
              </div>
            </div>
            <div id="divSum" align="center">
            </div>
          </div>
          <div class="RightDiv border">
            <div class="divAgencyOfficeAgencyUser">
              <div class="divAgencyOffice">
                <span class="Caption">
                  <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblAgencyOffice")%>
                                : </span>
                <br />
                <select id="fltAgencyOffice" style="width: 200px;" onchange="changeAgencyOffice(this.value);">
                </select>
              </div>
              <div class="divAgencyUser">
                <span class="Caption">
                  <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblAgencyUser")%>
                                : </span>
                <br />
                <select id="fltAgencyUser" style="width: 200px;">
                </select>
              </div>
            </div>
            <div class="divLeader">
              <div class="divSurname">
                <span class="Caption">
                  <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblSurname")%>
                                : </span>
                <br />
                <asp:TextBox ID="fltSurname" runat="server" Width="200px" />
              </div>
              <div class="divName">
                <span class="Caption">
                  <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblName")%>
                                : </span>
                <br />
                <asp:TextBox ID="fltName" runat="server" Width="200px" />
              </div>
            </div>
            <div class="divStatus">
              <table>
                <tr>
                  <td>
                    <span class="Caption">
                      <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblResStatus")%>
                                        :</span>
                  </td>
                  <td>
                    <span class="Caption">
                      <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblContStatus")%>
                                        :</span>
                  </td>
                  <td>
                    <span class="Caption">
                      <%= GetGlobalResourceObject("AgencyPaymentMonitor", "lblPayStatus")%>
                                        :</span>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input id="fltResStatus0" name="fltResStatus" type="checkbox" value="0" /><label
                      for="fltStatus0"><%= GetGlobalResourceObject("LibraryResource", "ResStatus0")%></label><br />
                    <input id="fltResStatus1" name="fltResStatus" type="checkbox" value="1" /><label
                      for="fltStatus1"><%= GetGlobalResourceObject("LibraryResource", "ResStatus1")%></label><br />
                    <input id="fltResStatus2" name="fltResStatus" type="checkbox" value="2" /><label
                      for="fltStatus2"><%= GetGlobalResourceObject("LibraryResource", "ResStatus2")%></label><br />
                    <input id="fltResStatus3" name="fltResStatus" type="checkbox" value="3" /><label
                      for="fltStatus3"><%= GetGlobalResourceObject("LibraryResource", "ResStatus3")%></label>
                  </td>
                  <td>
                    <input id="fltConfirmStatus0" name="fltConfirmStatus" type="checkbox" value="0" /><label
                      for="fltConfirmStatus0"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus0")%></label><br />
                    <input id="fltConfirmStatus1" name="fltConfirmStatus" type="checkbox" value="1" /><label
                      for="fltConfirmStatus1"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus1")%></label><br />
                    <input id="fltConfirmStatus2" name="fltConfirmStatus" type="checkbox" value="2" /><label
                      for="fltConfirmStatus2"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus2")%></label><br />
                    <input id="fltConfirmStatus3" name="fltConfirmStatus" type="checkbox" value="3" /><label
                      for="fltConfirmStatus3"><%= GetGlobalResourceObject("LibraryResource", "ConfStatus3")%></label>
                  </td>
                  <td>
                    <input id="fltPaymentStatus1" name="fltPaymentStatus" type="checkbox" value="1" /><label
                      for="fltPaymentStatus1"><%= GetGlobalResourceObject("LibraryResource", "fltBalance1")%></label><br />
                    <input id="fltPaymentStatus2" name="fltPaymentStatus" type="checkbox" value="2" /><label
                      for="fltPaymentStatus2"><%= GetGlobalResourceObject("LibraryResource", "fltBalance2")%></label><br />
                    <input id="fltPaymentStatus3" name="fltPaymentStatus" type="checkbox" value="3" /><label
                      for="fltPaymentStatus3"><%= GetGlobalResourceObject("LibraryResource", "fltBalance3")%></label><br />
                    <input id="fltPaymentStatus4" name="fltPaymentStatus" type="checkbox" value="4" /><label
                      for="fltPaymentStatus4"><%= GetGlobalResourceObject("LibraryResource", "fltBalance4")%></label>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="divFilterButton">
          <table>
            <tr>
              <td>
                <input type="button" id="btnFilter" value='<%= GetGlobalResourceObject("AgencyPaymentMonitor", "btnFilter")%>'
                  style="width: 140px;" onclick="btnFilterClick();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
              </td>
              <td>
                <input type="button" id="btnfilterclear" value='<%= GetGlobalResourceObject("AgencyPaymentMonitor", "btnfilterclear")%>'
                  style="width: 140px;" onclick="createFilterData(true);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="Content">
        <div class="exportDiv">
          <input type="button" value="<%= GetGlobalResourceObject("LibraryResource", "exportToExcel")%>" onclick="exportExcel();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
          &nbsp;
                <input type="button" value="<%= GetGlobalResourceObject("LibraryResource", "exportToHtml")%>" onclick="exportHtml();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
        </div>
        <div id="resultTableDiv" class="ui-helper-clearfix">
        </div>
        <div id="sumPaymentDiv" class="ui-helper-clearfix ui-helper-hidden">
          <div id="selectedSumPaymentDiv" class="ui-helper-hidden">
            <span id="selectedPaymentSumLabel" style="font-weight: bold;">Selected sum for payment</span><br />
            <span id="selectedPaymentSum" style="width: 200px; text-align: right;"></span>
          </div>
          <div id="editPaymentDiv">
            <input id="editPaymentSum" type="text" />&nbsp;
          </div>
          <div id="paymentButtonDiv">
            <input type="button" value="Pay" onclick="summaryPayment()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" style="width: 100px;" />
            <input type="button" value="Delete List" onclick="deleteSelectedList()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" style="width: 100px;" />
          </div>
        </div>
      </div>
      <div class="Footer">
        <tv1:Footer ID="tvfooter" runat="server" />
      </div>
    </div>

    <div id="confirm">
      <div class="header">
        <span>
          <%= GetGlobalResourceObject("LibraryResource", "msgConfirm")%></span>
      </div>
      <p class="message">
      </p>
      <div class="buttons">
        <div class="no simplemodal-close">
          <%= GetGlobalResourceObject("LibraryResource", "btnNo")%>
        </div>
        <div class="yes">
          <%= GetGlobalResourceObject("LibraryResource", "btnYes")%>
        </div>
      </div>
    </div>

    <div id="waitMessage" style="display: none;">
      <img alt="" title="" src="Images/Wait.gif" />
    </div>

    <asp:HiddenField ID="pdfConvSerial" runat="server" Value="RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==" />


    <div id="dialog-paymentPlan" title="Payment Plan" style="display: none;">
      <span id="paymentPlan"></span>
    </div>
    <div id="dialog-journalRef" title='<%= GetGlobalResourceObject("ResView", "lblPayments") %>' style="display: none;">
      <div id="divResPayment"></div>
    </div>
    <div id="dialog-resComment" title="Reservation Comment" style="display: none;">
      <resCom1:ResComment ID="resComment" runat="server" />
    </div>
    <div id="dialog-message" title="" style="display: none;">
      <p>
        <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message </span>
      </p>
    </div>

    <div id="dialog-export" title="" style="display: none;">
      <div id="exportDiv">
      </div>
    </div>

    <div id="dialog-viewReport" title='' style="display: none;">
      <iframe id="viewReport" runat="server" height="100%" width="100%" frameborder="0"
        style="clear: both;"></iframe>
    </div>

    <div id="dialog-totalPayment" title='' class="ui-helper-hidden">
      <iframe id="totalPayment" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
    </div>

    <div id="dialog-viewPDF" title='' style="display: none;">
      <iframe id="viewPDF" runat="server" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
    </div>

    <div id="dialog-SelectReportLang" title='<%= GetGlobalResourceObject("ResView", "headerLanguageSelect") %>' style="display: none;">
      <div id="langListDiv">
      </div>
    </div>
  </form>
</body>
</html>
