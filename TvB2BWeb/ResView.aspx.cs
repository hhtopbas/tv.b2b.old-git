﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvBo;
using System.Globalization;
using System.Threading;
using System.Text;
using TvTools;
using System.IO;
using Winnovative;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;
using System.Net;
using System.Security.Cryptography;
using BankIntegration;

public partial class ResView : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Session["Menu"] = "ResMonitor";
        if (UserData.Ci.Name.ToLower() == "lt-lt")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "yyyy.MM.dd";
        else if (UserData.Ci.Name.ToLower() == "lv-lv")
            UserData.Ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

    }

    void IsPostBackBefore(User userData)
    {
        string errorMsg = string.Empty;
        CultureInfo ci = userData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
    }

    void IsPostBackInside(object sender, EventArgs e, User UserData)
    {

    }

    void IsPosBackAfter(User UserData)
    {

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData(string resNo, string pxmResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string resCheck = string.Empty;
        string drawMenu = string.Empty;
        string resCheckMsg = string.Empty;
        if (VersionControl.CheckWebVersion(UserData.WebVersion, 44, VersionControl.Equality.gt) && !string.IsNullOrEmpty(resNo) && string.IsNullOrEmpty(pxmResNo))
        {
            Int16? saleResource = 1;
            string userName = string.Empty;
            bool? resLocked = new Reservation().lockReservation(UserData, resNo, ref saleResource, ref userName, false, ref errorMsg);
            if (resLocked.HasValue && resLocked.Value)
            {
                resCheck = "0";
                resCheckMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationIsBusy").ToString();
            }
            else
            {
                resCheck = "1";
                if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                    drawMenu = DrawButtons(resNo);
            }
        }
        else
        {
            resCheck = "2";
            if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                drawMenu = DrawButtons(resNo);
        }

        string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "Version"));
        string resMonitorUrl = WebRoot.BasePageRoot + "ResMonitor" + version + ".aspx";
        int? resViewTimeout = Conversion.getInt32OrNull(ConfigurationManager.AppSettings["ResViewTimeout"]);
        return new
        {
            ReservationMenu = drawMenu,
            ResCheck = resCheck,
            ResCheckMsg = resCheckMsg,
            resMonitorUrl = resMonitorUrl,
            showCloseResButton = string.Equals(UserData.CustomRegID, Common.crID_Detur),
            ResViewTimeOut = resViewTimeout
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string DrawButtons(string resNo)
    {

        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        TvBo.ResDataRecord ResData = new ResDataRecord();

        if (!string.IsNullOrEmpty(resNo) || (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"] == null)
        {
            ResData = new ResTables().getReservationData(UserData, resNo, string.Empty, ref errorMsg);
            HttpContext.Current.Session["ResData"] = ResData;
        }
        else
            ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }

        string mnAddingService = string.Empty;
        bool isPax = false;
        if (ResData.ResMain.SaleResource == 9)
            isPax = true;
        bool onlyTicket = ResData.ResMain.SaleResource == 2 || ResData.ResMain.SaleResource == 3 || ResData.ResMain.SaleResource == 5;
        List<SaleServiceRecord> saleRecords = new ReservationCommon().getSaleServices(UserData, ResData, ref errorMsg);
        StringBuilder MenuStr = new StringBuilder();
        MenuStr.Append("<div id=\"divAddResServiceMenu\"><div style=\"min-width: 100px; height: 60px;\">");
        List<SaleServiceRecord> saleService = new List<SaleServiceRecord>();
        Int16[] onlytickets = new Int16[3] { 2, 3, 5 };

        if (string.IsNullOrEmpty(ResData.ResMain.PackType) && !onlytickets.Contains(ResData.ResMain.SaleResource.Value))
            saleService = saleRecords.Where(w => w.B2BIndPack.HasValue && w.B2BIndPack.Value == true).ToList<SaleServiceRecord>();
        else
            saleService = saleRecords.Where(w => string.Equals(w.WebSale, "Y")).ToList<SaleServiceRecord>();

        List<ExternalServiceMenu> extService = new UICommon().getExternalServiceMenu();

        List<AgencySaleSer> agencySaleService = new Agency().getAgencySaleRestriction(UserData.AgencyID, ref errorMsg);

        foreach (SaleServiceRecord row in saleService)
        {
            if (agencySaleService.Find(f => f.ServiceType == row.Service) == null)
            {
                switch (row.Service)
                {
                    case "HOTEL":
                        int? cultureTourIndividualMaxHotelCount = Conversion.getInt32OrNull(new TvBo.Common().getFormConfigValue("General", "CultureTourIndividualMaxHotelCount"));
                        if (!cultureTourIndividualMaxHotelCount.HasValue || (cultureTourIndividualMaxHotelCount.HasValue && ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL") && string.Equals(w.IncPack, "N")).Count() < cultureTourIndividualMaxHotelCount.Value))
                            MenuStr.AppendFormat(" <div onclick=\"showAddResService('Controls/RSAdd_Hotel{0}.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/hotel.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString() + " </div>",
                                        Equals(ConfigurationManager.AppSettings["CalendarV2"], "1") ? "NC" : string.Empty);

                        break;
                    case "FLIGHT":
                        MenuStr.AppendFormat(" <div onclick=\"showAddResService('Controls/RSAdd_Flight{0}.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/flight.gif\" width=\"32\" height=\"32\"><br />" +
                                    HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString() + " </div>",
                                    Equals(ConfigurationManager.AppSettings["CalendarV2"], "1") ? "NC" : string.Empty);
                        break;
                    case "TRANSPORT":
                        MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Transport.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/transport.gif\" width=\"32\" height=\"32\"><br />" +
                                    HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransport").ToString() + " </div>");
                        break;
                    case "RENTING":
                        MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Renting.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/renting.gif\" width=\"32\" height=\"32\"><br />" +
                                    HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceRenting").ToString() + " </div>");
                        break;
                    case "EXCURSION":
                        MenuStr.AppendFormat(" <div onclick=\"showAddResService('Controls/RSAdd_Excursion{0}.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/excursion.gif\" width=\"32\" height=\"32\"><br />" +
                                                HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceExcursion").ToString() + " </div>",
                                                Equals(ConfigurationManager.AppSettings["CalendarV2"], "1") ? "NC" : string.Empty);
                        break;
                    case "INSURANCE":
                        MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Insurance.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/insurance.gif\" width=\"32\" height=\"32\"><br />" +
                                    HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceInsurance").ToString() + " </div>");
                        break;
                    case "VISA":
                        MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Visa.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/visa.gif\" width=\"32\" height=\"32\"><br />" +
                                    HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceVisa").ToString() + " </div>");
                        break;
                    case "TRANSFER":
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_YekTravel) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_ZemExpert) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Calypso) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_MbnTour_Ir) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Qasswa) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_SeaTravel) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_FilipTravel))
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_TransferV2.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/transfer.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransfer").ToString() + " </div>");
                        else
                            if (!onlyTicket || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Go2Holiday) || !(Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && Equals(UserData.Market, "SWEMAR")))
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Transfer.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/transfer.gif\" width=\"32\" height=\"32\"><br />" +
                                     HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransfer").ToString() + " </div>");
                        break;
                    case "HANDFEE":
                        MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Handfee.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/handfee.gif\" width=\"32\" height=\"32\"><br />" +
                                    HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHandfee").ToString() + " </div>");
                        break;
                    default:
                        //if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                        //{
                        //    MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Other.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/other.gif\" width=\"32\" height=\"32\"><br />" +
                        //                HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOtherDetur").ToString() + " </div>");
                        //}
                        //else
                        MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Other.aspx?serviceCode=" + row.Service + "');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/other.gif\" width=\"32\" height=\"32\"><br />" +
                            /*HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOther").ToString()*/ (string.IsNullOrEmpty(row.NameL) ? row.Name : row.NameL) + " </div>");
                        break;
                }
            }
        }


        if (extService != null && extService.Count > 0)
            foreach (ExternalServiceMenu extRow in extService)
                if (extRow.Disable)
                {
                    string nameResource = new UICommon().GetExternalServiceMenuGlobalResourceObject(UserData, extRow.ResourceName);
                    List<JSonStruc> extParams = Newtonsoft.Json.JsonConvert.DeserializeObject<List<JSonStruc>>(extRow.Params);
                    string queryString = new UICommon().createQueryString(UserData, ResData, extParams, ref errorMsg);
                    string onclick = extRow.ExternalWindow ?
                                            string.Format("window.open('{0}{1}','_blank')", extRow.Url, queryString)
                                            :
                                            string.Format("window.open('{0}{1}','_blank')", extRow.Url, queryString);
                    MenuStr.Append(" <div onclick=\"" + onclick + "\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/external.gif\" width=\"32\" height=\"32\"><br />" +
                                    (string.IsNullOrEmpty(nameResource) ? extRow.Name : nameResource) + " </div>");
                }


        MenuStr.Append(" <div onclick=\"showAddResServiceExt('Controls/ExtraService_Add.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/extraservice.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddExtraService").ToString() + " </div>");
        MenuStr.Append("</div></div>");

        if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                if (ResData.ResMain.ResLock.HasValue && ResData.ResMain.ResLock.Value > 0)
                    mnAddingService = string.Empty;
                else
                {
                    if (onlyTicket && !Equals(UserData.Market, "SWEMAR"))
                    {
                        MenuStr = new StringBuilder();
                        MenuStr.Append("<div id=\"divAddResServiceMenu\"><div style=\"min-width: 100px; height: 60px;\">");
                        if (saleService.Where(w => w.Service == "TRANSFER").Count() > 0)
                        {
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Transfer.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/transfer.gif\" width=\"32\" height=\"32\"><br />" +
                                    HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceTransfer").ToString() + " </div>");
                        }
                        if (saleService.Where(w => w.Service == "INSURANCE").Count() > 0)
                        {
                            MenuStr.Append(" <div onclick=\"showAddResService('Controls/RSAdd_Insurance.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/insurance.gif\" width=\"32\" height=\"32\"><br />" +
                                HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceInsurance").ToString() + " </div>");
                        }
                        MenuStr.Append(" <div onclick=\"showAddResServiceExt('Controls/ExtraService_Add.aspx');\" class=\"divLeft\"><img alt=\"\" src=\"Images/Services/extraservice.gif\" width=\"32\" height=\"32\"><br />" +
                                        HttpContext.GetGlobalResourceObject("LibraryResource", "AddExtraService").ToString() + " </div>");
                        MenuStr.Append("</div></div>");
                        mnAddingService = MenuStr.ToString();
                    }
                    else
                        mnAddingService = MenuStr.ToString();
                }
            }
            else
            {
                if ((onlyTicket && !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Go2Holiday)) || (ResData.ResMain.ResLock.HasValue && ResData.ResMain.ResLock.Value > 0))
                    mnAddingService = string.Empty;
                else
                    mnAddingService = MenuStr.ToString();
            }
        }
        else
            mnAddingService = MenuStr.ToString();
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel) && ResData.ResMain.PaymentStat != "U")
            mnAddingService = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
            mnAddingService = string.Empty;
        if (UserData.MainAgency)
            mnAddingService = string.Empty;

        bool serviceAdding = new Agency().getRole(UserData, 100);
        if (!serviceAdding)
            mnAddingService = string.Empty;

        return mnAddingService;
    }

    [WebMethod(EnableSession = true)]
    public static string checkResUser(string resNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (string.IsNullOrEmpty(resNo))
            return "true";

        string errorMsg = string.Empty;
        ResUserCheck checkUser = new Reservation().CheckReservationUserV2(UserData, resNo, ref errorMsg);
        bool? newUserRole = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "NewUserRole"));
        if (newUserRole.HasValue && newUserRole.Value)
        {
            if (checkUser == ResUserCheck.NotAllowed || checkUser == ResUserCheck.ResNotFound)
                return HttpContext.GetGlobalResourceObject("LibraryResource", "ReservationAgencyNotCorrect").ToString();
            else
                return "true";
        }
        else
        {
            if (new Reservation().CheckReservationUser(UserData, resNo, ref errorMsg))
                return "true";
            else
                return HttpContext.GetGlobalResourceObject("LibraryResource", "ReservationAgencyNotCorrect").ToString();
        }
    }

    public static string drawSubMenuForAnex(User UserData)
    {
        if (HttpContext.Current.Session["ResData"] == null)
            return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";

        bool isPax = false;
        if (ResData.ResMain.SaleResource == 9)
            isPax = true;
        bool AllowDocPrint = ResData.ResMain != null ? Equals(ResData.ResMain.AllowDocPrint, "Y") : false;
        bool isPayment = string.Equals(ResData.ResMain.PaymentStat, "O") || string.Equals(ResData.ResMain.PaymentStat, "V");
        bool isPartyPaid = string.Equals(ResData.ResMain.PaymentStat, "O") || string.Equals(ResData.ResMain.PaymentStat, "V") || string.Equals(ResData.ResMain.PaymentStat, "P");
        bool DocPrint = false;
        if (ResData != null && UserData.AgencyRec.DocPrtNoPay && ResData.ResMain.ResStat < 2)
            DocPrint = true;
        bool OnlyTicket = (ResData.ResMain.SaleResource.Value == 3 || ResData.ResMain.SaleResource.Value == 2 || ResData.ResMain.SaleResource.Value == 5) ? true : false;
        bool ResOK = ResData.ResMain.ConfStat == 1 ? true : false;

        bool inFlight = ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && w.StatSer != 2).Count() > 0 ? true : false;
        bool inHotel = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer != 2).Count() > 0 ? true : false;
        bool inTransfer = ResData.ResService.Where(w => w.ServiceType == "TRANSFER" && w.StatSer != 2).Count() > 0 ? true : false;
        bool inTransport = ResData.ResService.Where(w => w.ServiceType == "TRANSPORT" && w.StatSer != 2).Count() > 0 ? true : false;
        bool inInsurance = ResData.ResService.Where(w => w.ServiceType == "INSURANCE" && w.StatSer != 2).Count() > 0 ? true : false;
        bool inVisa = ResData.ResService.Where(w => w.ServiceType == "VISA" && w.StatSer != 2 && w.DepLocation == 1419).Count() > 0 ? true : false;
        bool inExcursion = ResData.ResService.Where(w => w.ServiceType == "EXCURSION" && w.StatSer != 2).Count() > 0 ? true : false;
        List<ResViewMenu> subMenuList = CacheObjects.getResViewMenuData();
        StringBuilder menuBuilder = new StringBuilder();
        string DocFol = string.Empty;
        string version = TvTools.Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "Version"));
        string page = HttpContext.Current.Session["Menu"].ToString().Replace("Blank", "");
        string classCss = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover";

        menuBuilder.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"gotoResMonitor('{1}');\" class=\"{2}\" />&nbsp;",
                   HttpContext.GetGlobalResourceObject("LibraryResource", "BackPage").ToString(),
                   WebRoot.BasePageRoot + "ResMonitor" + version + ".aspx",
                   classCss);

        bool agencyGaveGuaranty = new TvReport.AnexReport.AnexReport().getAgencyGaveGuaranty(ResData.ResMain.ResNo, ref errorMsg);
        if (agencyGaveGuaranty)
        {
            isPayment = true;
            isPartyPaid = true;
        }
        foreach (ResViewMenu row in subMenuList.Where(w => w.Disable == true).Select(s => s))
        {
            if (row.SubFolder)
                DocFol = DocumentFolder + "/" + UserData.Market + "/";
            else
                DocFol = "";

            bool menuVisible = row.Disable;
            bool menuDisable = true;
            bool noShowMenu = true;
            string[] noShowMenuStr = row.NoShowMarket.Split('|');
            if (noShowMenuStr.Contains(UserData.Market))
                noShowMenu = false;
            if (DocPrint)
            {
                menuDisable = (true && ResOK) || (string.Equals(row.Type, "_ALL") && ResData.ResMain.ConfStat.Value < 2);
                //0,1,3
            }
            else
                if (isPartyPaid && string.Equals(row.Code, "Visa"))
                menuDisable = true;
            else
                    if ((!row.PaymentControl || (ResData.ResMain.PaymentStat == "O" || ResData.ResMain.PaymentStat == "V")) && ResData.ResMain.ConfStat.Value == 1)
                menuDisable = true;
            else
                        if (AllowDocPrint)
                menuDisable = true;
            else
                            if (row.Type == "_USER")
            {
                if (UserData.MainAgency)
                    menuDisable = false;
                else
                    menuDisable = true;
            }
            else
            {
                if (row.Type.ToUpper() == "_ALL" || Equals(row.Type.ToUpper(), "INVOICEA") || Equals(row.Type.ToUpper(), "INVOICEP") || Equals(row.Type.ToUpper(), "INVOICE") || row.Type.ToUpper() == "CONTRACT" || row.Type.ToUpper() == "CONFIRM")
                    menuDisable = true;
                else
                    menuDisable = false;

            }
            if (Equals(row.Code, "TicketPriceRefound"))
            {
                if (ResData.ResMain.EndDate.Value <= DateTime.Today && inFlight && ResOK && row.afterCheckout && isPayment)
                    menuDisable = true;
                else
                    menuDisable = false;
            }
            if (Equals(row.Type.ToUpper(), "HOTEL"))
                if (!inHotel || OnlyTicket)
                    menuDisable = false;
            if (Equals(row.Service.ToUpper(), "FLIGHT") && !inFlight)
                menuDisable = false;
            if (Equals(row.Service.ToUpper(), "TRANSFER") && !inTransfer)
                menuDisable = false;
            if (Equals(row.Service.ToUpper(), "TRANSPORT") && !inTransport)
                menuDisable = false;
            if (Equals(row.Service.ToUpper(), "INSURANCE") && !inInsurance)
                menuDisable = false;
            if (Equals(row.Service.ToUpper(), "VISA") && !inVisa)
                menuDisable = false;

            if (row.PaymentControl && !AllowDocPrint && !DocPrint)
            {
                if (UserData.MasterAgency)
                    menuDisable = false;
                else
                    if (isPayment)
                    menuDisable = true;
                else
                    menuDisable = false;
                if (isPartyPaid && string.Equals(row.Code, "Visa"))
                    if (inVisa)
                        menuDisable = true;
                    else
                        menuDisable = false;
            }

            if (string.Equals(row.Code, "Payment") || string.Equals(row.Code, "Invoice") || string.Equals(row.Code, "Proforma"))
            {
                if ((ResData.ResMain.ConfStat == 1 || ResData.ResMain.ResStat == 3) && !(isPayment || ResData.ResMain.Balance == 0))
                    menuVisible = true;
                else
                    menuVisible = false;
            }

            if (menuVisible && noShowMenu && menuDisable)
            {
                string PageUrl = WebRoot.BasePageRoot + DocFol + row.Url + "?ResNo=" + ResData.ResMain.ResNo;
                bool openMenuItem = false;
                if (Equals(row.Type.ToUpper(), "INVOICEA") || Equals(row.Type.ToUpper(), "INVOICEP") || Equals(row.Type.ToUpper(), "INVOICE") || Equals(row.Type.ToUpper(), "_ALL"))
                    openMenuItem = true;
                else
                    if (menuDisable && (ResData.ResMain.ResStat < 2 || row.Type == "_USER"))
                    openMenuItem = true;

                if (Equals(row.Service, "INSURANCE"))
                {
                    if (inInsurance)
                        openMenuItem = true;
                    else
                        openMenuItem = false;
                }

                Int16 forCheckInDays = Convert.ToInt16((ResData.ResMain.BegDate.Value - DateTime.Today).Days.ToString());

                switch (row.Type.ToUpper())
                {
                    // PrtVoucher PrtInsurance PrtContrat
                    case "INSURANCE":
                        if ((UserData.TvParams.TvParamSystem.PrtInsuranceDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtInsuranceDayOptW.Value : 999) - forCheckInDays < 0)
                            openMenuItem = false;
                        break;
                    case "FLIGHT":
                        if ((UserData.TvParams.TvParamSystem.PrtTicketDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtTicketDayOptW.Value : 999) - forCheckInDays < 0)
                            openMenuItem = false;
                        break;
                    case "HOTEL":
                        if ((UserData.TvParams.TvParamSystem.PrtVoucherDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtVoucherDayOptW.Value : 999) - forCheckInDays < 0)
                            openMenuItem = false;
                        break;
                    case "CONTRACT":
                        if ((UserData.TvParams.TvParamSystem.PrtContractDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtContractDayOptW.Value : 999) - forCheckInDays < 0)
                            openMenuItem = false;
                        break;
                    case "CONFIRM":
                        openMenuItem = ResOK;
                        break;
                }

                //openMenuItem = openMenuItem && ResOK;
                switch (row.Code)
                {
                    case "Invoice":
                        openMenuItem = openMenuItem && (ResData.ResMain.ConfStat == 1 || ResData.ResMain.ResStat == 3);
                        break;
                    case "FlyTicket":
                        openMenuItem = openMenuItem && ResOK && (isPayment || AllowDocPrint);
                        break;
                    case "Voucher":
                        openMenuItem = inHotel && openMenuItem && ResOK && (isPayment || AllowDocPrint);
                        break;
                    case "Insurance":
                        openMenuItem = openMenuItem && ResOK && (isPayment || AllowDocPrint);
                        break;
                    case "AgencyConfirmation":
                        openMenuItem = openMenuItem && ResOK;
                        break;
                    case "Visa":
                        openMenuItem = /*openMenuItem && */ResOK /*&& (isPartyPaid || AllowDocPrint)*/;
                        break;
                    case "Payment":
                        openMenuItem = (openMenuItem && ResOK) || (ResData.ResMain.ResStat == 3);
                        break;
                    case "Comment":
                        openMenuItem = true;
                        break;
                    case "ACEFiles":
                        openMenuItem = UserData.AgencyRec != null && UserData.AgencyRec.AceExport && (UserData.AgencyRec.AceSendTo == 2 || UserData.AgencyRec.AceSendTo == 3);
                        break;
                }
                //Medine hanımın 13.04.2012 14:20 mailine istinaden.
                //Voucher   Reservation stastus : ok  Payment status : Paid/Over                    x
                //Bilet     Reservation stastus : ok  Payment status : Paid/Over                    x
                //Sigrota   Reservation stastus : ok  Payment status : Paid/Over                    x
                //Vize      Reservation stastus : ok  Payment status : Paid/Over/Partly Pad         x
                //Fatura    Reservation stastus : ok  Payment status : Paid/Over/Partly Pad/Unpaid  x
                //Confirm   Reservation stastus : ok  Payment status : Paid/Over/Partly Pad/Unpaid  x
                bool WDoc = true;
                if (UserData.WDoc != null)
                    switch (row.Type.ToUpper())
                    {
                        case "INVOICE":
                            WDoc = (UserData.WDoc.WDoc_InvoiceA || UserData.WDoc.WDoc_InvoiceP);
                            break;
                        case "INVOICEA":
                            WDoc = UserData.WDoc.WDoc_InvoiceA;
                            break;
                        case "INVOICEP":
                            WDoc = UserData.WDoc.WDoc_InvoiceP;
                            break;
                        case "HOTEL":
                            WDoc = UserData.WDoc.WDoc_Voucher;
                            break;
                        case "FLIGHT":
                            WDoc = UserData.WDoc.WDoc_Ticket;
                            break;
                        case "RECEIPT":
                            WDoc = UserData.WDoc.WDoc_Receipt;
                            break;
                        case "INSURANCE":
                            WDoc = UserData.WDoc.WDoc_Insurance;
                            break;
                        case "CONTRACT":
                            WDoc = UserData.WDoc.WDoc_Agree;
                            break;
                        case "CONFIRM":
                            WDoc = UserData.WDoc.WDoc_Confirmation;
                            break;
                        case "EXCURSION":
                            WDoc = UserData.WDoc.WDoc_ExcursionVoucher;
                            break;
                        case "TRANSPORT":
                            WDoc = UserData.WDoc.WDoc_TransportVoucher;
                            break;
                    }

                if (!(string.Equals(ResData.ResMain.PaymentStat, "U") || string.Equals(ResData.ResMain.PaymentStat, "P")) && string.Equals(row.Code, "Payment"))
                    openMenuItem = false;

                if (Equals(row.Type.ToUpper(), "TICKETREFOUND") && ResData.ResMain.EndDate.Value <= DateTime.Today && inFlight && ResOK && isPayment)
                    openMenuItem = true;

                bool printETicket = true;
                if (inFlight && string.Equals(row.Code, "FlyTicket"))
                {
                    List<ResServiceRecord> flightList = ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).ToList<ResServiceRecord>();
                    if (flightList != null)
                        foreach (ResServiceRecord fRow in flightList)
                        {
                            FlightDayRecord flight = new Flights().getFlightDay(UserData, fRow.Service, fRow.BegDate.Value, ref errorMsg);
                            if (flight != null && flight.AllowPrintTicB2B.HasValue && flight.AllowPrintTicB2B.Value == false)
                                printETicket = false;
                        }
                }

                if (openMenuItem && WDoc && printETicket)

                    if (openMenuItem && WDoc)
                        menuBuilder.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"{1}\" class=\"{2}\" />&nbsp;",
                            new UICommon().GetSubMenuGlobalResourceObject(UserData, row.ResourceName).ToString(),
                            "printDocument('" + row.Code + "', '" + row.Url + "','" + new UICommon().GetSubMenuGlobalResourceObject(UserData, row.ResourceName).ToString() + "'," + row.SubFolder.ToString().ToLower() + "," + row.ResBusyControl.ToString().ToLower() + ",null);",
                            classCss);
            }
        }
        return menuBuilder.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string drawSubMenu()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        //if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
        //    return drawSubMenuForAnex(UserData);

        if (HttpContext.Current.Session["ResData"] == null)
            return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        bool isPaximum = false;
        if (ResData.ResMain.SaleResource == 8 && UserData.PaxSetting != null && UserData.PaxSetting.Pxm_Use.HasValue && UserData.PaxSetting.Pxm_Use.Value)
            isPaximum = true;

        bool isPax = false;
        if (ResData.ResMain.SaleResource == 9)
            isPax = true;

        GetBookingsByUserResponse bookingInfo = null;

        bool? sendDocEmail = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("Report", "SendDocEmail"));

        if (UserData.PaxSetting != null)
            if (UserData.PaxSetting.Pxm_WorkType == PxmWorkType.All || UserData.PaxSetting.Pxm_WorkType == PxmWorkType.UsePaximumButton)
            {
                isPaximum = isPaximum && true;
            }
            else
                if (UserData.PaxSetting.Pxm_WorkType == PxmWorkType.UsePaximumInline)
            {
                isPaximum = isPaximum && (UserData.PaxSetting.Pxm_VoucherOpt == PxmVoucherOption.Paximum);
            };

        if (isPaximum)
            bookingInfo = new MixSearchPaximum().getBookingsByUser(UserData, ResData.ResMain.PxmResNo.ToString(), ref errorMsg);

        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";

        bool? _exportAceFile = UserData.AgencyRec.AceExport;
        bool exportAceFile = _exportAceFile.HasValue ? _exportAceFile.Value : true;
        bool AllowDocPrint = ResData.ResMain != null ? Equals(ResData.ResMain.AllowDocPrint, "Y") : false;
        bool isPayment = (string.Equals(ResData.ResMain.PaymentStat, "O") || string.Equals(ResData.ResMain.PaymentStat, "V")) || UserData.AgencyRec.DocPrtNoPay || AllowDocPrint;
        bool isPartyPaid = string.Equals(ResData.ResMain.PaymentStat, "O") || string.Equals(ResData.ResMain.PaymentStat, "V") || string.Equals(ResData.ResMain.PaymentStat, "P") || UserData.AgencyRec.DocPrtNoPay || AllowDocPrint;
        bool docPrintIsPayed = false;
        #region For Novaturas
        bool NovaBusTrips = false;
        #endregion
        if (ResData != null && isPayment && ResData.ResMain.ResStat < 2)
            docPrintIsPayed = true;

        if (ResData != null && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            if (isPayment && ResData.ResMain.ConfStat.Value == 1 && ResData.ResMain.ResStat < 2)
                docPrintIsPayed = true;
            HolPackRecord holPack = new TvBo.Common().getHolPack(UserData.Market, ResData.ResMain.HolPack, ref errorMsg);
            if (holPack != null)
            {
                if (holPack.Category == 1)
                    NovaBusTrips = true;
            }
        }
        bool withOptionRes = true;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && ResData.ResMain.OptDate.HasValue)
            withOptionRes = false;
        bool OnlyTicket = (ResData.ResMain.SaleResource.Value == 3 || ResData.ResMain.SaleResource.Value == 2 || ResData.ResMain.SaleResource.Value == 5) ? true : false;
        bool ResOK = ResData.ResMain.ConfStat == 1 ? true : false;
        bool inFlight = ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && w.StatSer != 2).Count() > 0 && withOptionRes ? true : false;
        bool inHotel = ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.StatSer != 2).Count() > 0 && withOptionRes ? true : false;
        bool inTransfer = ResData.ResService.Where(w => w.ServiceType == "TRANSFER" && w.StatSer != 2).Count() > 0 ? true : false;
        bool inTransport = ResData.ResService.Where(w => w.ServiceType == "TRANSPORT" && w.StatSer != 2).Count() > 0 ? true : false;
        bool inInsurance = ResData.ResService.Where(w => w.ServiceType == "INSURANCE" && w.StatSer != 2).Count() > 0 ? true : false;
        bool inExcursion = ResData.ResService.Where(w => w.ServiceType == "EXCURSION" && w.StatSer != 2).Count() > 0 ? true : false;
        bool inVisa = (ResData.ResService.Where(w => w.ServiceType == "VISA" && w.StatSer != 2).Count() > 0 ? true : false);

        List<ResViewMenu> subMenuList = CacheObjects.getResViewMenuData();
        StringBuilder menuBuilder = new StringBuilder();
        string DocFol = string.Empty;
        string page = HttpContext.Current.Session["Menu"].ToString().Replace("Blank", "");
        string classCss = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover";
        string version = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "Version"));

        string backBtnClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover";

        string firstPage = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "FirstPage"));
        bool resMonitorRole = new Agency().getRole(UserData, 1000);
        string backPage = !resMonitorRole ? firstPage : "ResMonitor" + version;
        if (string.Equals(UserData.CustomRegID, Common.crID_Detur))
            menuBuilder.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"gotoResMonitor('{1}');\" class=\"{2}\" />&nbsp;",
               HttpContext.GetGlobalResourceObject("ResView", "CloseReservation").ToString(),
               WebRoot.BasePageRoot + ("ResMonitor" + version + ".aspx"),
               backBtnClass);
        //if (version == "V2")

        //{
        //    menuBuilder.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"window.location='{1}';\" class=\"{2}\" />&nbsp;",
        //    HttpContext.GetGlobalResourceObject("LibraryResource", "BackPage").ToString(),
        //    WebRoot.BasePageRoot + "ResMonitorV2Blank.aspx",
        //    classCss);
        //}
        menuBuilder.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"gotoResMonitor('{1}');\" class=\"{2}\" />&nbsp;",
        HttpContext.GetGlobalResourceObject("LibraryResource", "BackPage").ToString(),
        WebRoot.BasePageRoot + backPage + ".aspx",
        classCss);


        foreach (ResViewMenu row in subMenuList.Where(w => w.Disable == true).Select(s => s))
        {
            bool sendEmail = false;
            sendEmail = sendDocEmail.HasValue && sendDocEmail.Value == true;

            if (row.SubFolder)
                DocFol = DocumentFolder + "/" + UserData.Market + "/";
            else
                DocFol = "";

            bool menuVisible = row.Disable;
            bool menuDisable = true;
            bool noShowMenu = true;
            string[] noShowMenuStr = row.NoShowMarket.Split('|');
            if (noShowMenuStr.Contains(UserData.Market))
                noShowMenu = false;

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            {
                if (!UserData.MainAgency)
                    if (docPrintIsPayed)
                        menuDisable = ResOK || !row.ResStatusControl;
                    else
                        if ((!row.PaymentControl || isPayment) && (ResOK || !row.ResStatusControl))
                        menuDisable = true;
                    else
                        menuDisable = false;
            }
            else
            {
                if (docPrintIsPayed)
                    if (row.Code == "Comment")
                        menuDisable = (true && ResOK) || ((string.Equals(row.Type, "_ALL") || !row.ResStatusControl) && ResData.ResMain.ConfStat.Value <= 2);
                    else
                        menuDisable = (true && ResOK) || ((string.Equals(row.Type, "_ALL") || !row.ResStatusControl) && ResData.ResMain.ConfStat.Value < 2);
                else
                    if ((!row.PaymentControl || (ResData.ResMain.PaymentStat == "O" || ResData.ResMain.PaymentStat == "V")) && ResData.ResMain.ConfStat.Value == 1)
                    menuDisable = true;
                else
                        if (row.Type == "_USER")
                {
                    if (UserData.MainAgency)
                        menuDisable = false;
                    else
                        menuDisable = true;
                }
                else
                {
                    if ((row.Type == "_ALL" || row.Type == "INVOICE" || row.Type == "PROFORMA" || row.Type == "CONTRACT") && !row.PaymentControl && (ResOK || !row.ResStatusControl))
                        menuDisable = true;
                    else
                        menuDisable = false;
                }
            }

            if (Equals(row.Type.ToUpper(), "HOTEL"))
            {
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                {
                    if (!inHotel)
                        menuDisable = false;
                }
                else
                {
                    if (!inHotel || OnlyTicket)
                        menuDisable = false;
                }
            }
            if (Equals(row.Service.ToUpper(), "FLIGHT") && !inFlight)
                menuDisable = false;
            if (Equals(row.Service.ToUpper(), "TRANSFER") && !inTransfer)
                menuDisable = false;
            if (Equals(row.Service.ToUpper(), "TRANSPORT") && !inTransport)
                menuDisable = false;
            if (Equals(row.Service.ToUpper(), "INSURANCE") && !inInsurance)
                menuDisable = false;
            if (Equals(row.Service.ToUpper(), "EXCURSION") && !inExcursion)
                menuDisable = false;
            if (Equals(row.Service.ToUpper(), "VISA") && !inVisa)
                menuDisable = false;
            #region For Novaturas
            /* 2013-05-24 According to ticket number 198 has been removed.
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && (NovaBusTrips || NovaLongDistanceTrips))
            {
                if ((Equals(row.Service.ToUpper(), "FLIGHT") && inFlight) || (Equals(row.Type.ToUpper(), "HOTEL") && inHotel))
                    menuDisable = false;
            }
            */
            #endregion
            if (Equals(row.Type.ToUpper(), "ACE"))
            {
                menuDisable = true;
                if (exportAceFile)
                    menuVisible = true;
                else
                    menuVisible = false;
                sendEmail = false;
            }


            bool WDoc = true;
            if (UserData.WDoc != null)
                switch (row.Type.ToUpper())
                {
                    case "INVOICE":
                        WDoc = (UserData.WDoc.WDoc_InvoiceA);
                        break;
                    case "PROFORMA":
                        WDoc = (UserData.WDoc.WDoc_InvoiceA);
                        break;
                    case "INVOICEA":
                        WDoc = UserData.WDoc.WDoc_InvoiceA;
                        break;
                    case "INVOICEP":
                        WDoc = UserData.WDoc.WDoc_InvoiceP;
                        break;
                    case "HOTEL":
                        WDoc = UserData.WDoc.WDoc_Voucher;
                        break;
                    case "FLIGHT":
                        WDoc = UserData.WDoc.WDoc_Ticket;
                        break;
                    case "RECEIPT":
                        WDoc = UserData.WDoc.WDoc_Receipt;
                        break;
                    case "INSURANCE":
                        WDoc = UserData.WDoc.WDoc_Insurance;
                        break;
                    case "CONTRACT":
                        WDoc = UserData.WDoc.WDoc_Agree;
                        break;
                }

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            {
                #region
                if (menuVisible && noShowMenu && menuDisable && WDoc)
                {
                    string PageUrl = WebRoot.BasePageRoot + DocFol + row.Url + "?ResNo=" + ResData.ResMain.ResNo;
                    Int16 forCheckInDays = Convert.ToInt16((ResData.ResMain.BegDate.Value - DateTime.Today).Days.ToString());
                    bool prtOptionOK = true;
                    switch (row.Type.ToUpper())
                    {
                        // PrtVoucher PrtInsurance PrtContrat
                        case "INSURANCE":
                            if ((UserData.TvParams.TvParamSystem.PrtInsuranceDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtInsuranceDayOptW.Value : 999) - forCheckInDays < 0)
                                prtOptionOK = false;
                            break;
                        case "FLIGHT":
                            if ((UserData.TvParams.TvParamSystem.PrtTicketDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtTicketDayOptW.Value : 999) - forCheckInDays < 0)
                                prtOptionOK = false;
                            break;
                        case "HOTEL":
                            if ((UserData.TvParams.TvParamSystem.PrtVoucherDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtVoucherDayOptW.Value : 999) - forCheckInDays < 0)
                                prtOptionOK = false;
                            break;
                        case "CONTRACT":
                            if ((UserData.TvParams.TvParamSystem.PrtContractDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtContractDayOptW.Value : 999) - forCheckInDays < 0)
                                prtOptionOK = false;
                            if (OnlyTicket && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                                prtOptionOK = false;
                            break;
                    }

                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && string.Equals(row.Code, "FlyTicketRef"))
                        if (ResData.ResMain.Balance > 0)
                            prtOptionOK = false;

                    if (prtOptionOK)
                        menuBuilder.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"{1}\" class=\"{2}\" />&nbsp;",
                            new UICommon().GetSubMenuGlobalResourceObject(UserData, row.ResourceName).ToString(),
                            "printDocument('" + row.Code + "', '" + (string.Equals(row.Code, "Agreement") ? (NovaBusTrips ? "Bus" + row.Url : row.Url) : row.Url) + "','" + new UICommon().GetSubMenuGlobalResourceObject(UserData, row.ResourceName).ToString() + "'," + row.SubFolder.ToString().ToLower() + "," + row.ResBusyControl.ToString().ToLower() + ", " + (row.FormID.HasValue ? row.FormID.ToString() : "null") + ", null, null, false);", classCss);
                }
                #endregion
            }
            else
                if (menuVisible && noShowMenu && menuDisable && WDoc)
            {
                string PageUrl = WebRoot.BasePageRoot + DocFol + row.Url + "?ResNo=" + ResData.ResMain.ResNo;
                bool openMenuItem = false;
                if (Equals(row.Type.ToUpper(), "INVOICEA") || Equals(row.Type.ToUpper(), "INVOICEP") || Equals(row.Type.ToUpper(), "INVOICE") || Equals(row.Type.ToUpper(), "PROFORMA"))
                    openMenuItem = true;
                else
                    if (menuDisable && (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) ||
                                        ResData.ResMain.ResStat < 2 || row.Type == "_USER"))
                    openMenuItem = true;

                if (Equals(row.Service, "INSURANCE"))
                {
                    if (inInsurance)
                        openMenuItem = true;
                    else
                        openMenuItem = false;
                }

                Int16 forCheckInDays = Convert.ToInt16((ResData.ResMain.BegDate.Value - DateTime.Today).Days.ToString());

                switch (row.Type.ToUpper())
                {
                    // PrtVoucher PrtInsurance PrtContrat
                    case "INSURANCE":
                        if ((UserData.TvParams.TvParamSystem.PrtInsuranceDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtInsuranceDayOptW.Value : 999) - forCheckInDays < 0)
                            openMenuItem = false;
                        else
                            openMenuItem = ResOK;
                        if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) && ResData.ResMain.OptDate.HasValue)
                            openMenuItem = false;
                        break;
                    case "FLIGHT":
                        if ((UserData.TvParams.TvParamSystem.PrtTicketDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtTicketDayOptW.Value : 999) - forCheckInDays < 0)
                            openMenuItem = false;
                        else
                            openMenuItem = ResOK;
                        if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) && ResData.ResMain.OptDate.HasValue)
                            openMenuItem = false;
                        break;
                    case "HOTEL":
                        if ((UserData.TvParams.TvParamSystem.PrtVoucherDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtVoucherDayOptW.Value : 999) - forCheckInDays < 0)
                            openMenuItem = false;
                        else
                            openMenuItem = ResOK;
                        if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) && ResData.ResMain.OptDate.HasValue)
                            openMenuItem = false;
                        break;
                    case "CONTRACT":
                        if ((UserData.TvParams.TvParamSystem.PrtContractDayOptW.HasValue ? UserData.TvParams.TvParamSystem.PrtContractDayOptW.Value : 999) - forCheckInDays < 0)
                            openMenuItem = false;
                        if (OnlyTicket && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                            openMenuItem = false;
                        if (string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) && ResData.ResMain.OptDate.HasValue)
                            openMenuItem = false;
                        break;
                    case "CONFIRM":
                        openMenuItem = ResOK;
                        break;
                }

                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran))
                    openMenuItem = (ResData.ResMain.ConfStat.Value == 1) && openMenuItem;

                #region if Anex
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) && !string.Equals(ResData.ResMain.PaymentStat, "U") && string.Equals(row.Code, "Payment"))
                    openMenuItem = false;
                #endregion

                bool printETicket = true;
                if (inFlight && string.Equals(row.Code, "FlyTicket"))
                {
                    List<ResServiceRecord> flightList = ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).ToList<ResServiceRecord>();
                    if (flightList != null)
                        foreach (ResServiceRecord fRow in flightList)
                        {
                            FlightDayRecord flight = new Flights().getFlightDay(UserData, fRow.Service, fRow.BegDate.Value, ref errorMsg);
                            if (flight != null && flight.AllowPrintTicB2B.HasValue && flight.AllowPrintTicB2B.Value == false)
                                printETicket = false;
                        }
                }

                if (string.Equals(row.Code, "Payment") && (ResData.ResMain.PaymentStat == "O" || ResData.ResMain.PaymentStat == "V"))
                {
                    openMenuItem = false;
                    sendEmail = false;
                }
                else
                {
                    if (string.Equals(row.Code, "Payment") && row.Url.IndexOf("QuickPayment.aspx") > -1)
                    {
                        sendEmail = false;
                        if (string.Equals(UserData.CustomRegID, Common.crID_Dallas) && VersionControl.getTableField("AgencyUser", "CanPayment"))
                        {
                            if (UserData.AgencyRec.CanPayment.HasValue && UserData.AgencyRec.CanPayment.Value == true && UserData.CanPayment.HasValue && UserData.CanPayment.Value == true)
                            {
                                openMenuItem = true;
                            }
                            else
                            {
                                openMenuItem = false;
                            }
                        }
                        else
                        {
                            if (UserData.AgencyRec.CanPayment.HasValue && UserData.AgencyRec.CanPayment.Value == true)
                            {
                                openMenuItem = true;
                            }
                            else
                            {
                                openMenuItem = false;
                            }
                        }
                    }
                }
                if (string.Equals(row.Code, "Payment") && row.Url.IndexOf("QuickPayment.aspx") > -1 && ResData.ResMain.SaleResource == 8)
                {
                    sendEmail = false;
                    openMenuItem = false;
                }
                if (string.Equals(UserData.CustomRegID, Common.crID_Dallas) && (string.Equals(row.Code, "Invoice") || string.Equals(row.Code, "Proforma")))
                {
                    openMenuItem = UserData.AgencyRec.CanPayment == true && UserData.CanPayment == true;
                }
                if (row.Code == "Comment" || row.Code == "Complaint")
                {
                    openMenuItem = true;
                    WDoc = true;
                    printETicket = true;
                    sendEmail = false;
                }

                if (row.ShowType.HasValue)
                {
                    if (row.ShowType.Value == (short)1 && ResData.ResService.Where(w => w.IncPack == "N" && string.Equals(w.ServiceType, row.Type)).Count() > 0)
                        openMenuItem = openMenuItem && true;
                    else if (row.ShowType.Value == (short)0 && ResData.ResService.Where(w => w.IncPack == "Y" && string.Equals(w.ServiceType, row.Type)).Count() > 0)
                        openMenuItem = openMenuItem && true;
                    else
                        openMenuItem = openMenuItem && false;
                }


                if (openMenuItem && WDoc && printETicket)
                {
                    if (isPaximum && row.Type.ToUpper() == "HOTEL")
                    {
                        if (bookingInfo != null && bookingInfo.Bookings.Length > 0 && bookingInfo.Bookings[0].Confirmation && !string.IsNullOrEmpty(bookingInfo.Bookings[0].VoucherPnrUrl))
                            menuBuilder.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"{1}\" class=\"{2}\" />&nbsp;",
                                new UICommon().GetSubMenuGlobalResourceObject(UserData, row.ResourceName).ToString(),
                                "printPaximumDocument('" + bookingInfo.Bookings[0].VoucherPnrUrl + "')",
                                classCss);
                    }
                    else
                    {
                        int? FormID = null;
                        if (!string.IsNullOrEmpty(row.MarketFormID))
                        {
                            List<DocReportFormID> formIDList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocReportFormID>>(row.MarketFormID);
                            DocReportFormID formID = formIDList.Find(f => f.Market == UserData.Market);
                            if (formID != null && formID.FormID.HasValue)
                            {
                                FormID = formID.FormID.Value;
                            }
                        }
                        else
                        {
                            FormID = row.FormID;
                        }
                        if (isPax && (row.Code == "Proforma" || row.Code == "AgencyConfirmation" || row.Code == "Cancel"))
                        {
                            menuBuilder.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"{1}\" class=\"{2}\" />&nbsp;",
                            new UICommon().GetSubMenuGlobalResourceObject(UserData, row.ResourceName).ToString(),
                            "showAlertWithLink('" + GetJSAlert(row.Code) + "','" + UserData.PaxSetting.Pxm_RedictionUrl + "');",
                            classCss);
                        }
                        else
                        {
                            menuBuilder.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"{1}\" class=\"{2}\" />&nbsp;",
                            new UICommon().GetSubMenuGlobalResourceObject(UserData, row.ResourceName).ToString(),
                            "printDocument('" + row.Code + "', '" + (string.Equals(row.Code, "Agreement") ? (NovaBusTrips ? "Bus" + row.Url : row.Url) : row.Url) + "','" + new UICommon().GetSubMenuGlobalResourceObject(UserData, row.ResourceName).ToString() + "'," + row.SubFolder.ToString().ToLower() + "," + row.ResBusyControl.ToString().ToLower() + "," + row.MultiLang.ToString().ToLower() + "," + (FormID.HasValue ? FormID.ToString() : "null") + ",'" + "0" + "', " + (sendEmail ? "true" : "false") + ");", classCss);
                        }
                    }
                }
            }
        }
        return menuBuilder.ToString();
    }
    private static string GetJSAlert(string code)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        string pxmUrl = UserData.PaxSetting.Pxm_RedictionUrl;
        switch (code)
        {
            case "Confirm": return HttpContext.GetGlobalResourceObject("ResView", "msgPaxConfirmBooking").ToString();
            case "Cancel": return HttpContext.GetGlobalResourceObject("ResView", "msgPaxCancelBooking").ToString();
            case "Proforma": return HttpContext.GetGlobalResourceObject("ResView", "msgPaxInvoiceBooking").ToString();
            case "AgencyConfirmation": return HttpContext.GetGlobalResourceObject("ResView", "msgPaxConfirmationAgency").ToString();
            default: return "";
        }
    }
    [WebMethod(EnableSession = true)]
    public static object getMultiLangID(string DocCode)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null)
            return null;
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        if (VersionControl.CheckWebVersion(UserData.WebVersion, 44, VersionControl.Equality.le))
            return null;

        if (DocCode.ToUpper(CultureInfo.InvariantCulture) == "CONTRACT")
        {
            List<int?> multiLangID = new TvReport.AllOperator().getDocumentLang(ResData.ResMain.Market, 5, ref errorMsg);

            var query = (from q in CultureInfo.GetCultures(CultureTypes.AllCultures)
                         join qMl in multiLangID on q.LCID.ToString() equals qMl.ToString()
                         select new { Id = q.LCID, Name = q.DisplayName }).ToList();
            var otherItems = query.Where(w => w.Id != UserData.Ci.LCID);
            var lcidItem = query.Where(w => w.Id == UserData.Ci.LCID).ToList();
            if (query.Count > 0)
            {
                if (lcidItem.Any())
                {
                    lcidItem.AddRange(otherItems);
                    return lcidItem;
                }
                else
                    return otherItems;
            }
            else
                return null;

        }
        else
            if (DocCode.ToUpper(CultureInfo.InvariantCulture) == "AGENCYCONFIRMATION")
        {
            List<int?> multiLangID = new TvReport.AllOperator().getDocumentLang(ResData.ResMain.Market, 6, ref errorMsg);

            var query = (from q in CultureInfo.GetCultures(CultureTypes.AllCultures)
                         join qMl in multiLangID on q.LCID.ToString() equals qMl.ToString()
                         select new { Id = q.LCID, Name = q.DisplayName }).ToList();
            var otherItems = query.Where(w => w.Id != UserData.Ci.LCID);
            var lcidItem = query.Where(w => w.Id == UserData.Ci.LCID).ToList();
            if (query.Count > 0)
            {
                if (lcidItem.Any())
                {
                    lcidItem.AddRange(otherItems);
                    return lcidItem;
                }
                else
                    return otherItems;
            }
            else
                return null;
        }
        else
                if (DocCode.ToUpper(CultureInfo.InvariantCulture) == "INVOICE" || DocCode.ToUpper() == "PROFORMA")
        {
            List<int?> multiLangID = new TvReport.AllOperator().getDocumentLang(ResData.ResMain.Market, 1, ref errorMsg);

            var query = (from q in CultureInfo.GetCultures(CultureTypes.AllCultures)
                         join qMl in multiLangID on q.LCID.ToString() equals qMl.ToString()
                         select new { Id = q.LCID, Name = q.DisplayName }).ToList();
            var otherItems = query.Where(w => w.Id != UserData.Ci.LCID);
            var lcidItem = query.Where(w => w.Id == UserData.Ci.LCID).ToList();
            if (query.Count > 0)
            {
                if (lcidItem.Any())
                {
                    lcidItem.AddRange(otherItems);
                    return lcidItem;
                }
                else
                    return otherItems;
            }
            else
                return null;
        }
        else
        {
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string custChgFee(string data)
    {
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        TvBo.ResDataRecord oldResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string resCustjSonValue = data.Replace('|', '"').Replace('<', '{').Replace('>', '}');
        try
        {
            TvBo.ResCustjSonResView custInfoFormArray = Newtonsoft.Json.JsonConvert.DeserializeObject<TvBo.ResCustjSonResView>(resCustjSonValue);
            TvBo.ResCustRecord currentResCust = new ResCustRecord();
            currentResCust = new TvBo.CustomerInfos().jSonToResCust(UserData, custInfoFormArray, currentResCust, oldResData.ResMain.BegDate, ref errorMsg);
            if (currentResCust == null)
                return errorMsg;
            ResChgFeePrice chgPrice = new Reservation().whatHasChangedResCust(UserData, oldResData, currentResCust.CustNo, currentResCust, ref errorMsg);
            if (chgPrice == null)
                return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
            else
            {
                //string chgFeeMsgStr = string.Format("The changes you are about to make costs {0} {1} as change penalty. Do you want to continue?",
                //                                 chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
                string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                                 chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
                return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
            }
        }
        catch (Exception Ex)
        {
            return string.Format(retVal, -1, Ex.Message).Replace('[', '{').Replace(']', '}');
        }
    }

    [WebMethod(EnableSession = true)]
    public static string returnResCustInfo(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResCustRecord> resCust = ResData.ResCust;
        List<ResCustInfoRecord> resCustInfo = ResData.ResCustInfo;
        string resCustjSonValue = data.Replace('|', '"').Replace('<', '{').Replace('>', '}');
        try
        {
            TvBo.ResCustjSonResView custInfoFormArray = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<TvBo.ResCustjSonResView>(resCustjSonValue); //Newtonsoft.Json.JsonConvert.DeserializeObject<TvBo.ResCustjSonResView>(resCustjSonValue);

            TvBo.ResCustInfoRecord currentRec = resCustInfo.Find(f => f.CustNo == Convert.ToInt32(custInfoFormArray.CustNo));
            if (currentRec == null)
            {
                currentRec = new ResCustInfoRecord();
                resCustInfo.Add(currentRec);
            }
            TvBo.ResCustInfoRecord custInfo = new TvBo.CustomerInfos().jSonToResCustInfo(custInfoFormArray, currentRec);

            TvBo.ResCustRecord currentResCust = resCust.Find(f => f.CustNo == Convert.ToInt32(custInfoFormArray.CustNo));

            currentResCust = new TvBo.CustomerInfos().jSonToResCust(UserData, custInfoFormArray, currentResCust, ResData.ResMain.BegDate, ref errorMsg);
            if (currentResCust == null)
                return errorMsg;
            Int16 retVal = new Reservation().EditTuristPrepareServices(UserData, ref ResData, currentResCust.CustNo, PrepareServiceType.UpdateService, ref errorMsg);
            if (retVal != 0)
            {
                if (retVal > 0)
                {
                    switch (retVal)
                    {
                        case 11:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomNotFound").ToString();
                            break;
                        case 12:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();
                            break;
                        case 13:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();
                            break;
                        case 14:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();
                            break;
                        case 15:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();
                            break;
                    }
                }
                return errorMsg;
            }
            else
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().UpdateReservation(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    return "";
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "\n";
                    }
                    return Msg;
                }
            }
        }
        catch (Exception Ex)
        {
            string error = Ex.Message;
            return ""; // Ex.Message;
        }
    }

    public static string getResPaymentInfo(User UserData, TvBo.ResMainRecord resMain)
    {
        bool? _showAgencyComView = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyComView"));
        bool showAgencyComView = !_showAgencyComView.HasValue || (UserData.ShowAllRes || _showAgencyComView.Value);
        bool showAgencyCom = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyCom")).Value : true;
        bool showAgencyEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showAgencyEB")).Value : true;
        bool showPassengerEB = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).HasValue ? Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showPassengerEB")).Value : true;

        bool localName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        if (_showAgencyComView.HasValue && _showAgencyComView.Value)
        {
            showAgencyCom = true;
            showAgencyEB = true;
            showPassengerEB = true;
        }
        if (Users.checkCurrentUserOperator(TvBo.Common.crID_FamilyTour))
        {
            showAgencyCom = true;
            showAgencyEB = true;
            showPassengerEB = true;
        }
        StringBuilder sb = new StringBuilder();



        sb.Append("<table id=\"tablePayment\">");
        sb.AppendFormat("<tr><td class=\"td\"><b>{0}&nbsp;</b></td><td class=\"td\" align=\"left\"><b>{1}</b></td><td class=\"td\">&nbsp;</td><td class=\"td\">&nbsp;</td><td>&nbsp;</td><td class=\"td\">&nbsp;</td><td class=\"td\"><b>{2}</b> </td><td class=\"td\">{3}</td></tr> ",
            HttpContext.GetGlobalResourceObject("ResView", "lblCurrency") + ":",
            resMain.SaleCur,
            !string.Equals(UserData.CustomRegID, Common.crID_SunFun) && UserData.AgencyRec.AgencyType == 0 ? HttpContext.GetGlobalResourceObject("AgencyPaymentMonitor", "BrokerCom").ToString() + ":" : "&nbsp;",
            !string.Equals(UserData.CustomRegID, Common.crID_SunFun) && UserData.AgencyRec.AgencyType == 0 ? (resMain.BrokerCom.HasValue ? resMain.BrokerCom.Value.ToString("#,###.00") : "&nbsp;") : "&nbsp;");
        sb.AppendFormat("<tr><td><b>{0}&nbsp;</b></td><td>{1}</td>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentSalePrice") + ":",
            resMain.SalePrice.HasValue ? resMain.SalePrice.Value.ToString("#,###.00") : "&nbsp;");

        sb.AppendFormat("<td><b>{0}&nbsp;</b></td><td>{1}</td>",
            showPassengerEB ? HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentPassEB") + ":" : "&nbsp;",
            showPassengerEB && resMain.EBPas.HasValue ? resMain.EBPas.Value.ToString("#,###.00") : "&nbsp;");

        sb.AppendFormat("<td><b>{0}&nbsp;</b></td><td>{1}</td>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentPassAmountToPay") + ":",
            resMain.PasPayable.HasValue ? resMain.PasPayable.Value.ToString("#,###.00") : "&nbsp;");
        decimal agencyCom = (resMain.AgencyCom.HasValue ? resMain.AgencyCom.Value : 0) + (resMain.AgencyComSup.HasValue ? resMain.AgencyComSup.Value : 0) - (resMain.AgencyDisPasVal.HasValue ? resMain.AgencyDisPasVal.Value : 0);
        // (isnull(RM.AgencyCom,0) + isnull(RM.AgencyComSup, 0) - isnull(RM.AgencyDisPasVal, 0)),
        sb.AppendFormat("<td><b>{0}&nbsp;</b></td><td>{1}</td></tr>",
            showAgencyCom ? HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentAgencyCom") + ":" : "&nbsp;",
            showAgencyCom && agencyCom > 0 ? agencyCom.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<tr><td><b>{0}&nbsp;</b></td><td>{1}</td>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentSupDis") + ":",
            resMain.Discount.HasValue ? resMain.Discount.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td><b>{0}&nbsp;</b></td><td>{1}</td>",
            showAgencyEB ? HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentAgencyEB") + ":" : "&nbsp;",
            showAgencyEB && resMain.EBAgency.HasValue ? resMain.EBAgency.Value.ToString("#,###.00") : "&nbsp;");

        decimal? agencyAmountToPay = resMain.AgencyPayable;
        if (UserData.AgencyRec.AgencyType == 2)
            agencyAmountToPay = resMain.AgencyPayable.HasValue ? resMain.AgencyPayable.Value + (resMain.BrokerCom.HasValue ? resMain.BrokerCom.Value : (decimal?)0) : null;

        if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && !Equals(UserData.Market, "SWEMAR"))
            sb.AppendFormat("<td><b>{0}&nbsp;</b></td><td>{1}</td>",
                (!Users.checkCurrentUserOperator(TvBo.Common.crID_Azur) || showAgencyCom) ? HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentAgencyAmountToPay") + ":" : "&nbsp;",
                (!Users.checkCurrentUserOperator(TvBo.Common.crID_Azur) || showAgencyCom) && agencyAmountToPay.HasValue ? agencyAmountToPay.Value.ToString("#,###.00") : "&nbsp;");
        else
            sb.Append("<td></td><td>&nbsp;</td>");

        decimal? balance = resMain.Balance;
        if (UserData.AgencyRec.AgencyType == 2)
            balance = resMain.Balance.HasValue ? resMain.Balance.Value + (resMain.BrokerCom.HasValue ? resMain.BrokerCom.Value : (decimal?)0) : null;

        if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && !Equals(UserData.Market, "SWEMAR"))
            sb.AppendFormat("<td><b>{0}&nbsp;</b></td><td>{1}</td></tr>",
                (!Users.checkCurrentUserOperator(TvBo.Common.crID_Azur) || showAgencyCom) ? HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentBalance") + ":" : "&nbsp;",
                (!Users.checkCurrentUserOperator(TvBo.Common.crID_Azur) || showAgencyCom) && balance.HasValue ? balance.Value.ToString("#,###.00") : "&nbsp;");
        else
            sb.Append("<td></td><td>&nbsp;</td></tr>");
        sb.AppendFormat("<tr><td><b>{0}&nbsp;</b></td><td>{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblPromotion") + ":",
            resMain.PromoAmount.HasValue ? resMain.PromoAmount.Value.ToString("#,###.00") : "&nbsp;");
        sb.AppendFormat("<td><b>{0}</b></td><td>{1}</td>", "&nbsp;", "&nbsp;");
        sb.AppendFormat("<td><b>{0}</b></td><td>{1}</td>", "&nbsp;", "&nbsp;");
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            sb.AppendFormat("<td><b>{0}</b></td><td>{1}</td></tr>", HttpContext.GetGlobalResourceObject("MakeReservation", "resPaymentBalance") + ":", resMain.PasBalance.HasValue ? resMain.PasBalance.Value.ToString("#,###.00") : "&nbsp;");
        else
            sb.AppendFormat("<td><b>{0}</b></td><td>{1}</td>", "&nbsp;", "&nbsp;");
        sb.Append("</table>");

        return sb.ToString();
    }

    public static object getObject(TvBo.ResMainRecord row, string fieldName)
    {
        System.Reflection.PropertyInfo[] oProps = null;
        oProps = row.GetType().GetProperties();
        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                    where q.Name == fieldName
                                                    select q).ToList<System.Reflection.PropertyInfo>();
        object value = null;
        if (_pi != null && _pi.Count() > 0)
        {
            System.Reflection.PropertyInfo pi = _pi[0];
            value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
        }
        return value;
    }

    public static string getResPaymentInfoTemplate(User UserData, TvBo.ResMainRecord row, string tmpl)
    {
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int lastPosition = 0;
        if (string.IsNullOrEmpty(tmpl))
            return getResPaymentInfo(UserData, row);
        string retVal = string.Empty;
        string tmpTemplate = tmpl;
        bool exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{[");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("]}");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 1)
                {
                    string classKey = local[0].Trim('"');
                    string resourceKey = local[1].Trim('"');
                    string localStr = HttpContext.GetGlobalResourceObject(classKey, resourceKey).ToString();
                    tmpl = tmpl.Replace("{[" + valueTemp + "]}", localStr);
                }
            }
            else
                exit = false;
        }
        tmpTemplate = tmpl;
        exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("{");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 1);
                int last = tmpTemplate.IndexOf("}");
                valueTemp = tmpTemplate.Substring(0, last);
                valueTemp = valueTemp.Trim('"');
                tmpTemplate = tmpTemplate.Remove(0, last + 1);

                string valueStr = string.Empty;
                if (valueTemp.IndexOf('.') > -1)
                {
                    if (Equals(valueTemp, "UserData.Ci.LCID"))
                        valueStr = UserData.Ci.LCID.ToString();
                }
                else
                    if (valueTemp.IndexOf('-') > -1)
                {
                    string[] valuelist = valueTemp.Split('-');
                    List<TvTools.objectList> objectValues = new List<objectList>();
                    Type _type = typeof(System.String);
                    for (int i = 0; i < valuelist.Length; i++)
                    {
                        object value = getObject(row, valuelist[i]);
                        objectValues.Add(new TvTools.objectList
                        {
                            TypeName = value.GetType().Name,
                            Value = value
                        });
                    }
                    object obj = mathLib.returnFormulaMinus(objectValues, ref _type);
                    valueStr = Conversion.getObjectToString(obj, _type);
                }
                else
                        if (valueTemp.IndexOf('+') > -1)
                {
                    string[] valuelist = valueTemp.Split('+');
                    List<TvTools.objectList> objectValues = new List<objectList>();
                    Type _type = typeof(System.String);
                    for (int i = 0; i < valuelist.Length; i++)
                    {
                        object value = getObject(row, valuelist[i]);
                        objectValues.Add(new TvTools.objectList
                        {
                            TypeName = value.GetType().Name,
                            Value = value
                        });
                    }
                    object obj = mathLib.returnFormulaPlus(objectValues, ref _type);
                    valueStr = Conversion.getObjectToString(obj, _type);
                }
                else
                {
                    object value = getObject(row, valueTemp);
                    valueStr = Conversion.getObjectToString(value, value.GetType()); //value.ToString();
                }
                tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", valueStr);
            }
            else
                exit = false;
        }
        return tmpl;
    }

    public static string getDetailTemplate(User UserData)
    {
        string sb = string.Empty;
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\ResViewPaymentInfoTemplate.tmpl";
        if (System.IO.File.Exists(filePath))
        {
            System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
            sb = Tex.ReadToEnd();
            Tex.Close();
        }
        return sb;
    }

    [WebMethod(EnableSession = true)]
    public static string getResMainRecord(string reLoad, string resNo, string paxResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool localName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        bool reservationCancel = new Agency().getRole(UserData, 1);
        bool reservationDateChange = new Agency().getRole(UserData, 2);


        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"] != null ? (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"] : null;
        if ((Equals(reLoad, "Y") && (!string.IsNullOrEmpty(resNo)) || !string.IsNullOrEmpty(paxResNo)) || (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"] == null)
            ResData = new ResTables().getReservationData(UserData, resNo, paxResNo, ref errorMsg);
        ResMainRecord resMain = ResData.ResMain;

        bool isPax = false;
        if (ResData.ResMain.SaleResource == 9)
            isPax = true;

        bool isPaximumRes = false;
        if (!Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel) && resMain.SaleResource.HasValue && resMain.SaleResource.Value == 9)
            isPaximumRes = true;

        bool OnlyTicket = (ResData.ResMain.SaleResource.Value == 3 || ResData.ResMain.SaleResource.Value == 2 || ResData.ResMain.SaleResource.Value == 5) ? true : false;

        bool? resCancel = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ReservationCancel"));
        if (isPaximumRes)
            resCancel = false;

        if (resMain.SaleResource == 6)
            resMain.ResLock = 1;
        HttpContext.Current.Session["ResData"] = ResData;
        bool? resDateChange = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ResDateChange"));
        bool? showPaymentPlan = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("ResMonitor", "ShowPaymentPlan"));
        if (isPaximumRes)
            resDateChange = false;

        bool? _serviceEdit = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ServiceEdit"));
        if (!_serviceEdit.HasValue)
            _serviceEdit = false;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || /*Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||*/ Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
            _serviceEdit = false;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel) && ResData.ResMain.PaymentStat != "U")
            _serviceEdit = false;
        StringBuilder sb = new StringBuilder();
        sb.Append("<div>");
        sb.Append("<table id=\"tableResMain\">");
        sb.Append("<tr>");
        sb.Append("<td style=\"border: 1px solid #AEAEAE\">");
        sb.Append("<table width=\"100%\" style=\"text-align: left;\">");
        sb.Append("<tr>");
        sb.AppendFormat("<td align=\"right\" style=\"width: 100px;\"><b>{0}:&nbsp;</b></td><td style=\"width: 325px;\"><span style=\"font-size: 12pt;\">{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblResNo"),
            resMain.ResNo);
        string changeBegDateStr = string.Format("<input type=\"button\" onclick=\"changeResBegDate('{1}','');\" style=\"height:100%;\" value=\"{0}\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
                                            HttpContext.GetGlobalResourceObject("LibraryResource", "btnDateChange"),
                                            resNo);
        if (Equals(ConfigurationManager.AppSettings["CalendarV2"], "1"))
            changeBegDateStr = string.Format("<input type=\"button\" onclick=\"changeResBegDate('{1}','1');\" style=\"height:100%;\" value=\"{0}\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
                                            HttpContext.GetGlobalResourceObject("LibraryResource", "btnDateChange"),
                                            resNo);
        sb.AppendFormat("<td align=\"right\"><b>{0}:&nbsp;</td><td>{1}</td><td rowspan=\"3\">{2}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblBeginDate"),
            resMain.BegDate.HasValue ? resMain.BegDate.Value.ToShortDateString() : "&nbsp;",
            reservationDateChange && (resDateChange.HasValue ? resDateChange.Value : (_serviceEdit.Value)) && ResData.ResMain.ResStat < 2 && ResData.ResMain.ResLock == 0 && !OnlyTicket ? changeBegDateStr : "");

        sb.Append("<td align=\"right\" rowspan=\"3\">");
        if (ResData.ResMain.ResLock.HasValue && ResData.ResMain.ResLock.Value > 0)
            sb.Append("<img alt=\"\" src=\"Images/Lock.png\" height=\"25px\" width=\"25px\" />");

        string clearOptionTime = string.Empty;
        if ((string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_SunFun) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka)) && ResData.ResMain.OptDate.HasValue)
        {
            if (isPax)
            {
                sb.AppendFormat("<input id=\"btnCancel\" type=\"button\" value=\"{0}\" style=\"width:150px\" onclick=\"showAlertWithLink('" + GetJSAlert("Confirm") + "','" + UserData.PaxSetting.Pxm_RedictionUrl + "');\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
                                    HttpContext.GetGlobalResourceObject("ResView", "btnClearOption"));
            }
            else
            {
                sb.AppendFormat("<input id=\"btnCancel\" type=\"button\" value=\"{0}\" style=\"width:150px\" onclick=\"clearOptionTime()\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
                                    HttpContext.GetGlobalResourceObject("ResView", "btnClearOption"));
            }

        }
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Qasswa) && (ResData.ResMain.ResStat.HasValue && ResData.ResMain.ResStat.Value == 9))
        {
            sb.AppendFormat("<input id=\"btnCancel\" type=\"button\" value=\"{0}\" style=\"width:150px\" onclick=\"setResStatus()\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
                                    HttpContext.GetGlobalResourceObject("ResView", "gridSrvConfirm"));
        }

        if ((!resCancel.HasValue || (resCancel.HasValue && resCancel.Value)) && reservationCancel && string.Equals(ResData.ResMain.PLMarket, ResData.ResMain.Market))
        {
            if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            {
                if (ResData.ResMain.ResStat < 2 && ResData.ResMain.ResLock == 0 && _serviceEdit.Value && !(string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran) && ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).Count() > 0))
                    sb.AppendFormat("<input id=\"btnCancel\" type=\"button\" value=\"{0}\" style=\"width:150px\" onclick=\"showCancelReservation('Controls/CancelReservation.aspx')\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
                                        HttpContext.GetGlobalResourceObject("ResView", "btnCancelReservation"));
                else
                    sb.Append("&nbsp;" + clearOptionTime);
            }
            else
            {

                if (ResData.ResMain.ResStat < 2 && ResData.ResMain.ResLock == 0 && resMain.OptDate.HasValue && resMain.OptDate.Value > DateTime.Today)
                {
                    if (isPax)
                    {
                        sb.AppendFormat("<input id=\"btnCancel\" type=\"button\" value=\"{0}\" style=\"width:150px\" onclick=\"showAlertWithLink('" + GetJSAlert("Cancel") + "','" + UserData.PaxSetting.Pxm_RedictionUrl + "');\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
                                        HttpContext.GetGlobalResourceObject("ResView", "btnCancelReservation"));
                    }
                    else
                        sb.AppendFormat("<input id=\"btnCancel\" type=\"button\" value=\"{0}\" style=\"width:150px\" onclick=\"showCancelReservation('Controls/CancelReservation.aspx')\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
                                            HttpContext.GetGlobalResourceObject("ResView", "btnCancelReservation"));
                }

                sb.Append("&nbsp;" + clearOptionTime);
            }
        }
        else
            sb.Append("&nbsp;" + clearOptionTime);
        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.AppendFormat("<td align=\"right\"><b>{0}:&nbsp;</td><td>{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblAgency"),
            localName ? resMain.AgencyNameL : resMain.AgencyName);
        sb.AppendFormat("<td align=\"right\"><b>{0}:&nbsp;</td><td>{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblEndDate"),
            resMain.EndDate.HasValue ? resMain.EndDate.Value.ToShortDateString() : "&nbsp;");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.AppendFormat("<td align=\"right\" style=\"width: 100px;\"><b>{0}:&nbsp;</b></td><td style=\"width: 325px;\">{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblAgencyUser"),
            localName ? resMain.AgencyUserNameL : resMain.AgencyUserName);
        sb.AppendFormat("<td align=\"right\"><b>{0}:&nbsp;</td><td>{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblRegisterDate"),
            resMain.ReceiveDate.HasValue ? resMain.ReceiveDate.Value.ToShortDateString() : "&nbsp;");
        sb.Append("</tr>");
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
        {
            sb.Append("<tr>");
            sb.AppendFormat("<td align=\"right\" style=\"width: 100px;\"><b>{0}</b></td><td style=\"width: 325px;\">{1}</td>",
                "&nbsp;",
                "&nbsp;");
            sb.AppendFormat("<td align=\"right\"><b>{0}:&nbsp;</td><td>{1}</td>",
                HttpContext.GetGlobalResourceObject("ResMonitor", "lblAgencyConfirmDate"),
                string.Equals(resMain.ConfToAgency, "Y") ? (resMain.ConfToAgencyDate.HasValue ? resMain.ConfToAgencyDate.Value.ToShortDateString() : "&nbsp;") : "&nbsp;");
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        sb.Append("</td></tr>");
        sb.Append("<tr><td>");

        string detailTemplate = getDetailTemplate(UserData);
        if (string.IsNullOrEmpty(detailTemplate))
            sb.Append(getResPaymentInfo(UserData, resMain));
        else
            sb.Append(getResPaymentInfoTemplate(UserData, resMain, detailTemplate));

        sb.Append("</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table id=\"tableStatus\">");
        sb.AppendFormat("<tr><td><b>{0}:&nbsp;</b>&nbsp;{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblReservationStatus"),
            resMain.ResStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + resMain.ResStat.Value.ToString()).ToString() : "&nbsp;");
        sb.AppendFormat("<td><b>{0}:&nbsp;</b>&nbsp;{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblConfirmation"),
            resMain.ConfStat.HasValue ? HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + resMain.ConfStat.Value.ToString()).ToString() : "&nbsp;");
        sb.AppendFormat("<td><b>{0}:&nbsp;</b>&nbsp;{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblPaymentStatus"),
            !string.IsNullOrEmpty(resMain.PaymentStat) ? HttpContext.GetGlobalResourceObject("LibraryResource", "PayStatus" + resMain.PaymentStat).ToString() : "&nbsp;");
        sb.Append("<td align=\"right\">");
        if (ResData.ResMain.ResStat < 2 && (!showPaymentPlan.HasValue || showPaymentPlan.Value))
            sb.AppendFormat("<input type=\"button\" value=\"{0}\" style=\"width:150px;\" onclick=\"paymentPlan('{1}');\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
                HttpContext.GetGlobalResourceObject("ResView", "lblPaymentPlan"),
                resMain.ResNo);
        else
            sb.Append("&nbsp;");
        sb.Append("</td></tr>");
        sb.Append("</table>");
        sb.Append("</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("</td></tr>");
        sb.Append("</table>");
        sb.Append("</div>");
        if (string.Equals(UserData.CustomRegID, Common.crID_FilipTravel))
        {
            sb.Append("<div id=\"divSpecialCodes\" style=\"text-align: left;\">");
            sb.AppendLine("<table cellpadding=\"0\" cellspacing=\"0\" style=\"width: auto;\">");
            sb.AppendLine("    <tr>");
            sb.AppendLine("        <td style=\"width: 200px;\">");
            sb.AppendLine("            BR. FISKALNOG RACUNA");
            sb.AppendLine("        </td>");
            sb.AppendLine("        <td style=\"width: 100px;\">");
            sb.AppendFormat("            <input id=\"iCode1\" type=\"text\" maxlength=\"10\" style=\"width: 90px; border: solid 1px #333;\" disabled=\"disabled\" value=\"{0}\" />", resMain.Code1);
            sb.AppendLine("        </td>");
            sb.AppendLine("        <td style=\"width: 200px;\">");
            sb.AppendLine("            DATUM IZDAVANJA FISK. RAC.");
            sb.AppendLine("        </td>");
            sb.AppendLine("        <td style=\"width: 100px;\">");
            sb.AppendFormat("            <input id=\"iCode2\" type=\"text\" maxlength=\"10\" style=\"width: 90px; border: solid 1px #333;\" disabled=\"disabled\" value=\"{0}\" />", resMain.Code2);
            sb.AppendLine("        </td>");
            sb.AppendLine("    </tr>");
            sb.AppendLine("    <tr>");
            sb.AppendLine("        <td style=\"width: 200px;\">");
            sb.AppendLine("            UPLACEN IZNOS U DINARIMA");
            sb.AppendLine("        </td>");
            sb.AppendLine("        <td style=\"width: 100px;\">");
            sb.AppendFormat("            <input id=\"iCode3\" type=\"text\" maxlength=\"10\" style=\"width: 90px; border: solid 1px #333;\" disabled=\"disabled\" value=\"{0}\" />", resMain.Code3);
            sb.AppendLine("        </td>");
            sb.AppendLine("        <td style=\"width: 200px;\">");
            sb.AppendLine("            NAPOMENA");
            sb.AppendLine("        </td>");
            sb.AppendLine("        <td style=\"width: 100px;\">");
            sb.AppendFormat("            <input id=\"iCode4\" type=\"text\" maxlength=\"10\" style=\"width: 90px; border: solid 1px #333;\" disabled=\"disabled\" value=\"{0}\" />", resMain.Code4);
            sb.AppendLine("        </td>");
            sb.AppendLine("    </tr>");
            sb.AppendLine("    <tr>");
            sb.AppendLine("        <td colspan=\"4\">");
            sb.AppendLine("            <input id=\"btnEditSpecialCode\" type=\"button\" value=\"Edit Special Code\" onclick=\"editSpecialCode();\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />&nbsp;&nbsp;|&nbsp;&nbsp;");
            sb.AppendLine("            <input id=\"btnSaveSpecialCode\" type=\"button\" value=\"Save Special Code\" onclick=\"saveSpecialCode();\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" disabled=\"disabled\" />");
            sb.AppendLine("        </td>");
            sb.AppendLine("    </tr>");
            sb.AppendLine("</table>");
            sb.Append("</div>");
        }
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getResCustRecords()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (HttpContext.Current.Session["ResData"] == null)
            return "";
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();

        bool editing = (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel) && ResData.ResMain.PaymentStat != "U");
        bool? editCustomerInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "EditCustomerInfo"));
        if (!Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel) && (!editCustomerInfo.HasValue || editCustomerInfo.Value))
            editing = true;
        else
            editing = false;

        List<VisaFollowUpRecord> visaStatusList = new List<VisaFollowUpRecord>();
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            visaStatusList = new Reservation().getVisaStatus(ResData.ResMain.ResNo, ref errorMsg);
        bool? showVisaInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showVisaInfo"));

        List<string> SrrcServiceTypeList = new Ssrcs().getSsrcServiceTypes(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);

        bool? deleteCust = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "deleteCust"));
        bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
        bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));

        bool custEdit = new Agency().getRole(UserData, 501);
        bool custDelete = new Agency().getRole(UserData, 502);
        bool custAdding = new Agency().getRole(UserData, 500);
        deleteCust = deleteCust.HasValue && deleteCust.Value && custDelete;
        editing = editing && custEdit;

        sb.Append("<table id=\"gridResCustTable\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%\">");
        sb.Append("<tr id=\"gridResCustHeader2\">");
        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustTitle"));
        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustSurname"));
        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustName"));
        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustBirthDate"));
        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustAge"));
        if (!showPIN.HasValue || showPIN.Value)
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustPIN"));
        if (!showPassportInfo.HasValue || showPassportInfo.Value)
        {
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustSerie"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustNo"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustP"));
        }
        if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustVoucher"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustTicket"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustInsurance"));
        }
        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustLeader"));
        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridCustInfo"));
        sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "gridPpPrice"));
        sb.Append("</tr>");

        bool alternatingRow = true;
        bool resOptionTime = ResData.ResMain.OptDate.HasValue;

        foreach (TvBo.ResCustRecord row in ResData.ResCust)
        {
            List<ResServiceRecord> custResService = (from q1 in ResData.ResService
                                                     join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                     where q2.CustNo == row.CustNo
                                                     select q1).ToList<ResServiceRecord>();
            bool notDeleted = !(deleteCust.HasValue && deleteCust.Value || !deleteCust.HasValue);

            alternatingRow = alternatingRow ? false : true;
            if (alternatingRow)
                sb.Append("<tr class=\"gridResCustItemAlternating\">");
            else
                sb.Append("<tr class=\"gridResCustItem\">");

            //sb.AppendFormat(" <td style=\"width: 20px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.CustNo);
            sb.AppendFormat(" <td style=\"width: 30px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.TitleStr);
            sb.AppendFormat(" <td style=\"{0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.Surname);
            sb.AppendFormat(" <td style=\"width: {2}; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.Name, !Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ? "60px" : "100px");
            sb.AppendFormat(" <td style=\"width: 70px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.Birtday.HasValue ? row.Birtday.Value.ToShortDateString() : "&nbsp;");
            sb.AppendFormat(" <td style=\"width: 30px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.Age.HasValue ? row.Age.Value.ToString() : "&nbsp;");
            //
            if (!showPIN.HasValue || showPIN.Value)
                sb.AppendFormat(" <td style=\"width: 90px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.IDNo);
            if (!showPassportInfo.HasValue || showPassportInfo.Value)
            {
                sb.AppendFormat(" <td style=\"width: 40px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.PassSerie);
                sb.AppendFormat(" <td style=\"width: 90px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.PassNo);
                sb.AppendFormat(" <td style=\"width: 25px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", row.HasPassport.HasValue && row.HasPassport.Value ? "<img title=\"\" src=\"Images/accept.gif\" />" : "&nbsp;");
            }
            if (!Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                sb.AppendFormat(" <td style=\"width: 50px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", (row.DocVoucher.HasValue && row.DocVoucher.Value > 0) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued").ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued").ToString());
                sb.AppendFormat(" <td style=\"width: 50px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", (row.DocTicket.HasValue && row.DocTicket.Value > 0) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued").ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued").ToString());
                sb.AppendFormat(" <td style=\"width: 50px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", (row.DocInsur.HasValue && row.DocInsur.Value > 0) ? HttpContext.GetGlobalResourceObject("LibraryResource", "Issued").ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "NotIssued").ToString());
            }
            sb.AppendFormat(" <td style=\"width: 50px; text-align: center; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", Equals(row.Leader, "Y") ? "<img title=\"\" src=\"Images/accept.gif\" />" : "&nbsp;");
            //editing = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Azur);
            string onClickView = string.Empty;
            if ((string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || editing) &&
                (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || ResData.ResMain.ResLock == 0) &&
                 ResData.ResMain.ResStat < 2 && (row.Status.HasValue && row.Status.Value == 0))
                onClickView = string.Format("onclick=\"showResCustInfo('{0}', 'false');\"", row.CustNo.ToString());
            else
                onClickView = string.Format("onclick=\"showResCustInfo('{0}', 'true');\"", row.CustNo.ToString());

            string viewImg = string.Format("<img title=\"{0}\" src=\"Images/view.png\" {1} style=\"cursor:pointer\" />",
                HttpContext.GetGlobalResourceObject("ResView", "gridCustEditView"),
                onClickView);

            if ((string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt)) && resOptionTime)
                viewImg = string.Empty;

            string viewEdit = (ResData.ResMain.ResStat < 2 || ResData.ResMain.ResStat == 9) && (row.Status.HasValue && row.Status.Value == 0) ? viewImg : HttpContext.GetGlobalResourceObject("ResView", "lblCancelled").ToString();

            string onClickDelete = !(Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || Equals(UserData.CustomRegID, TvBo.Common.crID_Anex)) && ResData.ResMain.ResLock == 0 && ResData.ResMain.ResStat < 2 && (row.Status.HasValue && row.Status.Value == 0) ? (string.Format("onclick=\"showDeleteCust('{0}', '{1}');\"", row.CustNo.ToString(), row.ResNo)) : "";
            string deleteImg = string.Format("<img title=\"{0}\" src=\"Images/cancel.png\" {1} style=\"cursor:pointer\" />",
                HttpContext.GetGlobalResourceObject("ResView", "gridCustDelete"),
                onClickDelete);
            string delete = custDelete && !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Azur) && !notDeleted && ResData.ResMain.ResLock == 0 && ResData.ResMain.ResStat < 2 && (row.Status.HasValue && row.Status.Value == 0) ? (!Equals(row.Leader, "Y") ? deleteImg : "") : "";
            string ssrcStr = string.Format("<img title=\"{0}\" src=\"Images/SsrCode.png\" onclick=\"showSSRC({1})\" height=\"18px\" />",
                                HttpContext.GetGlobalResourceObject("ResView", "lblSpecialServiceReqCode"),
                                row.CustNo);
            string visaStr = string.Format("<img title=\"{0}\" src=\"Images/visa.gif\" onclick=\"showResCustVisa({1})\" />",
                                "Visa",
                                row.CustNo);
            if (string.Equals(UserData.CustomRegID, Common.crID_Qasswa))
            {
                visaStr = string.Format("<img title=\"{0}\" src=\"Images/passport_visa.png\" onclick=\"onclickPassportScanBtn({1})\" />",
                                "Visa",
                                row.SeqNo);
                showVisaInfo = true;
            }
            string editViewTd = string.Format(" <td style=\"width: 75px;\">{0}&nbsp;{1}&nbsp;{2}{3}</td>",
                viewEdit,
                editing && !(Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || Equals(UserData.CustomRegID, TvBo.Common.crID_Anex)) && (ResData.ResMain.ResLock == 0 && ResData.ResMain.ResStat < 2 && (row.Status.HasValue && row.Status.Value == 0)) && string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt) ? ssrcStr : "",
                deleteCust.HasValue && deleteCust.Value && !(Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || Equals(UserData.CustomRegID, TvBo.Common.crID_Anex)) ? delete : "",
                showVisaInfo.HasValue && showVisaInfo.Value ? "&nbsp;" + visaStr : "");

            sb.Append(editViewTd);
            decimal? PPPrice;
            if (row.ppPasPayable.HasValue)
            {
                PPPrice = row.ppPasPayable;
            }
            else
            {
                List<ResCustPriceRecord> ppPrice = ResData.ResCustPrice.Where(w => w.CustNo == row.CustNo).Select(s => s).ToList<ResCustPriceRecord>();
                PPPrice = ppPrice.Sum(s => s.SalePrice);
            }
            //decimal? ppPrice = ResData.ResCust.Find(f => f.CustNo == row.CustNo).ppSalePrice;
            sb.AppendFormat(" <td style=\"text-align: right; width: 75px; {0}\" >{1}</td>", (row.Status.HasValue && row.Status.Value > 0) ? "font-style:italic;" : "", PPPrice.HasValue ? PPPrice.Value.ToString("#,###.00") : "&nbsp;");
            sb.Append("</tr>");
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            {
                #region Sunrise, Anex
                //16 columns, if deturs 15 columns
                if (visaStatusList != null && visaStatusList.Count > 0)
                {
                    VisaFollowUpRecord visaStatus = visaStatusList.Find(f => f.CustNo == row.CustNo);
                    Int16 columns = 16;
                    if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                        columns = 15;
                    if (visaStatus != null)
                    {
                        sb.Append("<tr>");
                        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                        {
                            string comment = string.Empty;
                            if (visaStatus.LastPosition == 6)
                            {
                                if (string.IsNullOrEmpty(visaStatus.Comment))
                                    comment = HttpContext.GetGlobalResourceObject("ResView", "lblVisaStat").ToString() + " : " + HttpContext.GetGlobalResourceObject("VisaStatuAnex", "visaStatus" + visaStatus.LastPosition).ToString() + "(Виза получена)";
                                else
                                    comment = HttpContext.GetGlobalResourceObject("ResView", "lblVisaStat").ToString() + " : " + HttpContext.GetGlobalResourceObject("VisaStatuAnex", "visaStatus" + visaStatus.LastPosition).ToString() + (!string.IsNullOrEmpty(visaStatus.Comment) ? "(" + visaStatus.Comment.ToString() + ")" : "");
                            }
                            else
                            {
                                comment = HttpContext.GetGlobalResourceObject("ResView", "lblVisaStat").ToString() + " : " + HttpContext.GetGlobalResourceObject("VisaStatuAnex", "visaStatus" + visaStatus.LastPosition).ToString();
                                if (!string.IsNullOrEmpty(ResData.ResMain.Code3))
                                    comment = "Предполагаемая дата получения : " + ResData.ResMain.Code3.ToString() + " - " + comment;
                            }
                            sb.AppendFormat("<td colspan=\"{0}\" style=\"text-align: right; color: #FF0000; font-weight: bold;\">{1}</td>",
                            columns,
                            comment);
                        }
                        else
                            sb.AppendFormat("<td colspan=\"{0}\" style=\"text-align: right; color: #FF0000; font-weight: bold;\">{2} : {1}</td>",
                                columns,
                                HttpContext.GetGlobalResourceObject("LibraryResource", "visaStatus" + visaStatus.LastPosition),
                                HttpContext.GetGlobalResourceObject("ResView", "lblVisaStat"));
                        sb.Append("</tr>");
                    }
                }
                #endregion
            }
        }

        if (!(Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || Equals(UserData.CustomRegID, TvBo.Common.crID_Anex)))
            #region not (Sunrise, Anex)
            if ((editing || custAdding) && ResData.ResMain.ResLock == 0 && ResData.ResMain.ResStat != 2 && ResData.ResMain.ResStat != 3 && (ResData.ResMain.SaleResource != 2 && ResData.ResMain.SaleResource != 3) && !string.Equals(ResData.ResMain.PackType, "T"))
            {
                bool? addingCust = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "addingCust"));
                bool? changeLeader = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "changeLeader"));

                sb.Append("<tr>");
                sb.AppendFormat("<td valign=\"middle\" style=\"text-align:left; border: solid 1px #999;\" colspan=\"{0}\">", Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ? "15" : "16");
                if ((!addingCust.HasValue || (addingCust.HasValue && addingCust.Value)) && custAdding)
                    sb.AppendFormat("<input id=\"btnAddTourist\" type=\"button\" value='{0}' onclick=\"showAddTurist('Controls/ReservationAddTourist.aspx')\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only\" />",
                                    HttpContext.GetGlobalResourceObject("ResView", "lblAddNewTourist"));

                sb.Append("&nbsp;&nbsp;");
                if (!changeLeader.HasValue || (changeLeader.HasValue && changeLeader.Value))
                    sb.AppendFormat("<input id=\"btnChangeLeader\" type=\"button\" value='{0}' onclick=\"changeLeader();\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only\" />",
                                    HttpContext.GetGlobalResourceObject("ResView", "btnChangeLeader"));

                sb.Append("</td>");
                sb.Append("</tr>");

            }
        #endregion
        sb.Append("</table>");
        string custTable = string.Empty;
        bool viewBtn = sb.ToString().IndexOf("view.png") > 0; //HttpContext.GetGlobalResourceObject("ResView", "gridCustEditView"),
        bool cancelBtn = sb.ToString().IndexOf("cancel.png") > 0; //HttpContext.GetGlobalResourceObject("ResView", "gridCustDelete")
        bool ssrcBtn = sb.ToString().IndexOf("SsrCode.png") > 0; //HttpContext.GetGlobalResourceObject("ResView", "lblSpecialServiceReqCode")
        if (viewBtn || cancelBtn || ssrcBtn)
        {
            custTable += "<div style=\"white-space: nowrap;\">";
            if (viewBtn)
            {
                custTable += "<img alt=\"\" src=\"Images/view.png\" height=\"18px\" />";
                custTable += string.Format("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("ResView", "gridCustEditView"));
            }
            if (cancelBtn)
            {
                if (viewBtn)
                    custTable += "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                custTable += "<img alt=\"\" src=\"Images/cancel.png\" height=\"18px\" />";
                custTable += string.Format("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("ResView", "gridCustDelete"));
            }
            if (ssrcBtn)
            {
                if (viewBtn || cancelBtn)
                    custTable += "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                custTable += "<img alt=\"\" src=\"Images/SsrCode.png\" height=\"18px\" />";
                custTable += string.Format("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("ResView", "lblSpecialServiceReqCode"));
            }
            custTable += "</div>";
        }

        return custTable + sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getResServiceRecords()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null)
            return "";
        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;

        bool localName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        bool serviceEdit = new Agency().getRole(UserData, 101);
        bool serviceDelete = new Agency().getRole(UserData, 102);

        bool? showCompField = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ShowCompField"));
        bool? showIncPackField = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ShowIncPackField"));
        bool? useFlightSeat = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "UseFlightSeat"));
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        bool resLock = !(!ResData.ResMain.ResLock.HasValue || ResData.ResMain.ResLock == 0);

        bool isPaximumRes = false;
        if (ResData.ResMain.SaleResource.HasValue && ResData.ResMain.SaleResource.Value == 8)
            isPaximumRes = true;
        if (isPaximumRes)
            serviceEdit = false;
        bool? _showCancelService = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showCancelService"));
        bool showCancelService = _showCancelService.HasValue ? _showCancelService.Value : true;

        bool? _serviceDelete = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ServiceDelete"));
        bool deleteServices = (_serviceDelete.HasValue ? _serviceDelete.Value : false) && serviceDelete;

        deleteServices = _serviceDelete.HasValue ? _serviceDelete.Value : false;
        if (ResData.ResService.Where(w => w.StatSer < 2).Count() < 2)
            deleteServices = false;
        bool? _serviceEdit = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ServiceEdit"));
        bool editServices = (_serviceEdit.HasValue ? _serviceEdit.Value : true) && serviceEdit;

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            deleteServices = false;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) /*|| Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt)*/)
            editServices = false;
        else
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel) && ResData.ResMain.PaymentStat != "U")
            editServices = false;
        List<ResViewMenu> subMenuList = CacheObjects.getResViewMenuData();
        sb.Append("<table id=\"gridResServiceTable\" cellpadding=\"2\" cellspacing=\"0\">");
        sb.Append("<tr id=\"gridResServiceHeader2\">");
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            sb.AppendFormat("<td>{0}</td><td>{1}</td>{2}{3}<td>{4}</td><td>{5}</td><td>{6}</td><td>&nbsp;</td></tr>",
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvServiceType"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvServiceDesc"),
                !showIncPackField.HasValue || (showIncPackField.HasValue && showIncPackField.Value) ? "<td>" + HttpContext.GetGlobalResourceObject("ResView", "gridSrvInclude") + "</td>" : "",
                !showCompField.HasValue || (showCompField.HasValue && showCompField.Value) ? "<td>" + HttpContext.GetGlobalResourceObject("ResView", "gridSrvCompulsory") + "</td>" : "",
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvStatus"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvUnit"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvSalePrice"));
        else
        {
            string showCompFieldStr = string.Empty;
            string showIncPackFieldStr = string.Empty;

            sb.AppendFormat("<td>{0}</td><td>{1}</td>{2}{3}<td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>&nbsp;</td></tr>",
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvServiceType"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvServiceDesc"),
                !showIncPackField.HasValue || (showIncPackField.HasValue && showIncPackField.Value) ? "<td>" + HttpContext.GetGlobalResourceObject("ResView", "gridSrvInclude") + "</td>" : "",
                !showCompField.HasValue || (showCompField.HasValue && showCompField.Value) ? "<td>" + HttpContext.GetGlobalResourceObject("ResView", "gridSrvCompulsory") + "</td>" : "",
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvConfirm"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvStatus"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvUnit"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvSalePrice"));
        }
        bool alternatingRow = true;

        string serviceEditVersion = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "serviceEditVersion"));

        List<SpecSerRQCodeRecord> ssrcList = new Ssrcs().getSpecialRQCode(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);

        bool showPickupPoint = (!ResData.ResMain.ResLock.HasValue || ResData.ResMain.ResLock == 0) && (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) ||
                                                                                                       string.Equals(UserData.CustomRegID, TvBo.Common.crID_Doris));

        List<VehicleCategoryRecord> vehicleCatList = new ReservationRequestForm().getVehicleCategory(UserData, ref errorMsg);

        foreach (TvBo.ResServiceRecord row in ResData.ResService.OrderBy(o => o.BegDate))
        {
            bool busSeatSelect = (!ResData.ResMain.ResLock.HasValue || ResData.ResMain.ResLock == 0) && string.Equals(row.ServiceType, "TRANSPORT") && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && ((ResData.ResMain.BegDate.Value - DateTime.Today).TotalDays > 3);
            bool flightSeatSelect = (!ResData.ResMain.ResLock.HasValue || ResData.ResMain.ResLock == 0) && string.Equals(row.ServiceType, "FLIGHT") && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt);
            flightSeatSelect = flightSeatSelect && useFlightSeat.HasValue && useFlightSeat.Value;
            bool showService = true;
            if (!showCancelService && row.StatSer == 2)
                showService = false;

            string ServiceDesc = string.Empty;
            switch (row.ServiceType)
            {
                #region Description
                case "HOTEL":
                    HotelRecord hR = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
                    string hotelLocation = hR != null ? (localName ? hR.LocationLocalName : hR.LocationName) : "";
                    if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                        ServiceDesc += (string.IsNullOrEmpty(hotelLocation) ? "" : " (" + hotelLocation + ") ") + (localName ? row.RoomNameL : row.RoomName) + "," + row.Board + " , " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Magellan))
                        ServiceDesc += (localName ? row.RoomNameL : row.RoomName) + "," + row.Accom + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                        ServiceDesc += (localName ? row.RoomNameL : row.RoomName) + ",(" + (localName ? row.AccomNameL : row.AccomName) + "), " + row.Board + " , " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    else
                        ServiceDesc += (localName ? row.RoomNameL : row.RoomName) + "," + row.Board + " , " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "FLIGHT":
                    FlightDayRecord flg = new Flights().getFlightDay(UserData, row.Service, row.BegDate.Value, ref errorMsg);
                    ServiceDesc += flg != null && flg.TDepDate.HasValue ? flg.TDepDate.Value.ToShortDateString() : row.BegDate.Value.ToShortDateString();
                    break;
                case "TRANSPORT":
                    ServiceDesc += row.BegDate.Value.ToShortDateString();
                    TransportRecord transport = new Transports().getTransport(UserData.Market, ResData.ResMain.PLMarket, row.Service, ref errorMsg);
                    if ((transport != null && transport.SerArea.HasValue && transport.SerArea.Value == 1))
                        busSeatSelect = false;
                    break;
                case "TRANSFER":
                    TransferRecord trf = new Transfers().getTransfer(UserData.Market, row.Service, ref errorMsg);
                    if (trf != null && string.Equals(trf.Direction, "R"))
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    else
                        ServiceDesc += row.BegDate.Value.ToShortDateString();
                    break;
                case "RENTING":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "EXCURSION":
                    ServiceDesc += row.BegDate.Value.ToShortDateString();
                    if (row.GrpPackID.HasValue)
                    {
                        ExcursionPack package = new Excursions().getExcursionPack(UserData, row.GrpPackID.Value, ref errorMsg);
                        if (package != null)
                            ServiceDesc += " " + (localName ? package.NameL : package.Name);
                    }
                    else
                        if (row.ExcTimeID.HasValue)
                    {
                        ExcursionTimeTable timeTable = new Excursions().getExcursionTimes(UserData, row.ExcTimeID, ref errorMsg);
                        if (timeTable != null)
                        {
                            if (timeTable.TransFromTime.HasValue && timeTable.TransToTime.HasValue)
                            {
                                ServiceDesc += " (" + timeTable.PriceCode + " [" + timeTable.TransFromTime.Value.ToString("HH:mm") + " < - > " + timeTable.TransToTime.Value.ToString("HH:mm") + "])";
                            }
                            if (timeTable.FreeFromTime.HasValue && timeTable.FreeToTime.HasValue)
                            {
                                ServiceDesc += "<br />";
                                ServiceDesc += "<span style='font-size: 7pt;'>Free time : " + timeTable.FreeFromTime.ToString() + "-" + timeTable.FreeToTime.ToString() + "</span>";
                            }
                        }
                    }
                    break;
                case "INSURANCE":
                    if (string.Equals(UserData.CustomRegID, Common.crID_SunFun))
                        ServiceDesc += string.Empty;
                    else
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "VISA":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                case "HANDFEE":
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    break;
                default:
                    ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                    if (!(new Reservation().showAdditionalService(UserData, ResData.ResMain, row.ServiceType, row.Service, ref errorMsg)))
                        showService = false;
                    break;
                    #endregion
            }
            ServiceDesc = (localName ? row.ServiceNameL : row.ServiceName) + " / " + ServiceDesc + "";
            if (showService)
            {
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran))
                {
                    deleteServices = _serviceDelete.HasValue ? _serviceDelete.Value : false;
                    if (ResData.ResService.Where(w => w.StatSer < 2).Count() < 2)
                        deleteServices = false;
                    if (string.Equals(row.ServiceType, "FLIGHT"))
                        deleteServices = false;
                }
                alternatingRow = alternatingRow ? false : true;
                if (row.StatSer == 2 || row.StatSer == 3)
                    sb.Append("<tr class=\"gridResServiceItemDisable\">");
                else
                    if (alternatingRow)
                    sb.Append("<tr class=\"gridResServiceItemAlternating\">");
                else
                    sb.Append("<tr class=\"gridResServiceItem\">");

                //sb.AppendFormat("<td style=\"width: 30px; text-align: left;\">{0}</td>", row.RecID.ToString());
                sb.AppendFormat("<td style=\"width: 130px; text-align: left;\">{0}</td>", localName ? row.ServiceTypeNameL : row.ServiceTypeName);

                List<ResCustRecord> ServiceCustomers = (from q1 in ResData.ResCust
                                                        join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                                                        where q2.ServiceID == row.RecID
                                                        select q1).ToList<ResCustRecord>();
                string serviceCusts = string.Empty;
                foreach (ResCustRecord r1 in ServiceCustomers)
                {
                    if (serviceCusts.Length > 0)
                        serviceCusts += ", ";
                    serviceCusts += string.Format("{0}. {1} {2}", r1.TitleStr, r1.Surname, r1.Name);
                }

                if ((!ResData.ResMain.ResLock.HasValue || ResData.ResMain.ResLock == 0) && string.Equals(UserData.CustomRegID, Common.crID_Qasswa) && (/*string.Equals(row.ServiceType, "EXCURSION") ||*/ string.Equals(row.ServiceType, "TRANSFER")))
                {
                    //vehicleCatList
                    string vehicleCat = string.Empty;
                    vehicleCat += string.Format("<option value=\"\">{0}</option>", HttpContext.GetGlobalResourceObject("LibraryResource", "ComboSelect"));
                    foreach (VehicleCategoryRecord r1 in vehicleCatList)
                        vehicleCat += string.Format("<option value=\"{0}\" {2}>{1}</option>",
                                        r1.RecID.ToString(),
                                        r1.Name,
                                        string.Equals(r1.RecID.ToString(), row.VehicleCatID.ToString()) ? "selected=\"selected\"" : "");
                    vehicleCat = "<select style=\"width:99%;\" onchange=\"changeVehicleCat(this.value, '" + row.RecID.ToString() + "')\">" + vehicleCat + "</select>";

                    sb.AppendFormat("<td style=\"text-align: left;\"><div style=\"float: left;\">{0}</div><div style=\"float:right; width: 180px; text-align: left;\"><div style=\"width:100px; float: left;\">{1}</div><div style=\"width:80px; float: left;\">{2}</div></div></td>",
                            ServiceDesc + "<br /><span style=\"font-size: 7pt;\">(" + serviceCusts + ")</span>",
                            HttpContext.GetGlobalResourceObject("Controls", "viewVehicleCat") + "<br />" + vehicleCat,
                            HttpContext.GetGlobalResourceObject("Controls", "viewVehicleUnit") + "<br />" + "<input style=\"width: 90%;\" onblur=\"changeVehicleUnit(this.value, '" + row.RecID.ToString() + "')\" value=\"" + row.VehicleUnit.ToString() + "\"/>");

                }
                else
                    //Kayako id = 2098 && 7310
                    if ((!ResData.ResMain.ResLock.HasValue || ResData.ResMain.ResLock == 0) && showPickupPoint && string.Equals(row.ServiceType, "TRANSPORT") && ((ResData.ResMain.BegDate.Value - DateTime.Today).TotalDays > 3))
                {
                    List<TransportStatRecord> serviceList = new Transports().getPickups(UserData.Market, row.Service, row.DepLocation, ref errorMsg);
                    string pickupPoint = string.Empty;
                    foreach (TransportStatRecord r1 in serviceList)
                        pickupPoint += string.Format("<option value=\"{0}\" {2}>{1}</option>",
                                        r1.Location,
                                        localName ? r1.LocationLocalName : r1.LocationName,
                                        string.Equals(r1.Location, row.Pickup) ? "selected=\"selected\"" : "");
                    pickupPoint = "<select style=\"width:99%;\" onchange=\"changePickup(this.value, '" + row.RecID.ToString() + "')\">" + pickupPoint + "</select>";

                    sb.AppendFormat("<td style=\"text-align: left;\"><div style=\"float: left;\">{0}</div><div style=\"float:right; width:125px; text-align: left;\">{1}</div></td>",
                            ServiceDesc + "<br /><span style=\"font-size: 7pt;\">(" + serviceCusts + ")</span>",
                            HttpContext.GetGlobalResourceObject("Controls", "viewPickupPoint") + " :<br />" + pickupPoint);
                }
                else
                    sb.AppendFormat("<td style=\"text-align: left;\">{0}", ServiceDesc + "<br /><span style=\"font-size: 7pt;\">(" + serviceCusts + ")</span>");

                var serviceResConList = from q1 in ResData.ResCon
                                        where (!string.IsNullOrEmpty(q1.SpecSerRQCode1) || !string.IsNullOrEmpty(q1.SpecSerRQCode2))
                                          && q1.ServiceID == row.RecID
                                          && (ssrcList.Select(s => s.Code).Contains(q1.SpecSerRQCode1) || ssrcList.Select(s => s.Code).Contains(q1.SpecSerRQCode2))
                                        select q1;

                string ssrCodeDesc = string.Empty;
                foreach (var ssrRow in serviceResConList)
                {
                    SpecSerRQCodeRecord ssrCode1 = ssrcList.Find(f => f.Code == ssrRow.SpecSerRQCode1);
                    SpecSerRQCodeRecord ssrCode2 = ssrcList.Find(f => f.Code == ssrRow.SpecSerRQCode2);
                    if (ssrCode1 != null)
                    {
                        if (ssrCodeDesc.Length > 0)
                            ssrCodeDesc += "<br />";

                        List<ResCustRecord> ssrServiceCustomers = (from q1 in ResData.ResCust
                                                                   join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                                                                   where ssrRow.RecID == q2.RecID
                                                                   select q1).ToList<ResCustRecord>();
                        string ssrServiceCusts = string.Empty;
                        foreach (ResCustRecord r1 in ssrServiceCustomers)
                        {
                            if (ssrServiceCusts.Length > 0)
                                ssrServiceCusts += ", ";
                            ssrServiceCusts += string.Format("{0}. {1} {2}", r1.TitleStr, r1.Surname, r1.Name);
                        }
                        ssrCodeDesc += "<b>(" + (localName ? ssrCode1.NameL : ssrCode1.Name) + ")</b>(" + ssrServiceCusts + ")";

                    }
                    if (ssrCode2 != null)
                    {
                        if (ssrCodeDesc.Length > 0)
                            ssrCodeDesc += "<br />";

                        List<ResCustRecord> ssrServiceCustomers = (from q1 in ResData.ResCust
                                                                   join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                                                                   where ssrRow.RecID == q2.RecID
                                                                   select q1).ToList<ResCustRecord>();
                        string ssrServiceCusts = string.Empty;
                        foreach (ResCustRecord r1 in ssrServiceCustomers)
                        {
                            if (ssrServiceCusts.Length > 0)
                                ssrServiceCusts += ", ";
                            ssrServiceCusts += string.Format("{0}. {1} {2}", r1.TitleStr, r1.Surname, r1.Name);
                        }
                        ssrCodeDesc += "<b>(" + (localName ? ssrCode1.NameL : ssrCode1.Name) + ")</b>(" + ssrServiceCusts + ")";
                    }
                }
                if (ssrCodeDesc.Length > 0)
                    sb.AppendFormat("<br /><span style=\"font-size: 7pt;\">{0}</span>", ssrCodeDesc);
                sb.Append("</td>");
                if (!showIncPackField.HasValue || (showIncPackField.HasValue && showIncPackField.Value == true))
                    sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", Equals(row.IncPack, "Y") ? "<img title=\"\" src=\"Images/accept.gif\" />" : "&nbsp;");

                if (!showCompField.HasValue || (showCompField.HasValue && showCompField.Value == true))
                    sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", (row.Compulsory.HasValue && row.Compulsory.Value) ? "<img title=\"\" src=\"Images/accept.gif\" />" : "&nbsp;");

                if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                    sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.StatConf.ToString()).ToString());

                sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.StatSer.ToString()).ToString());
                sb.AppendFormat("<td style=\"width: 40px; text-align: center;\">{0}</td>", row.Unit.ToString());
                if (row.StatSer == 2 || row.StatSer == 3)
                    sb.AppendFormat("<td style=\"width: 90px; text-align: right;\">{0}</td>", "&nbsp;");
                else
                    sb.AppendFormat("<td style=\"width: 90px; text-align: right;\">{0}</td>", Equals(row.IncPack, "Y") ? HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString() : (row.SalePrice.HasValue ? row.SalePrice.Value.ToString() : "&nbsp;"));

                string viewPageUrl = string.Empty;
                string editPageUrl = string.Empty;
                bool document = false;

                #region url
                switch (row.ServiceType)
                {
                    case "HOTEL":
                        viewPageUrl = "RSView_Hotel.aspx";
                        editPageUrl = "RSEdit_Hotel" + serviceEditVersion + ".aspx";
                        document = subMenuList.Where(w => w.Service == "HOTEL").Count() > 0;
                        break;
                    case "FLIGHT":
                        viewPageUrl = "RSView_Flight.aspx";
                        editPageUrl = "RSEdit_Flight" + serviceEditVersion + ".aspx";
                        document = subMenuList.Where(w => w.Service == "FLIGHT").Count() > 0;
                        break;
                    case "TRANSPORT":
                        viewPageUrl = "RSView_Transport.aspx";
                        editPageUrl = "RSEdit_Transport" + serviceEditVersion + ".aspx";
                        document = subMenuList.Where(w => w.Service == "TRANSPORT").Count() > 0;
                        break;
                    case "TRANSFER":
                        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_Qasswa) ||
                            string.Equals(UserData.CustomRegID, TvBo.Common.crID_ZemExpert))
                            viewPageUrl = "RSView_TransferV2.aspx";
                        else
                            viewPageUrl = "RSView_Transfer.aspx";
                        editPageUrl = "RSEdit_Transfer" + serviceEditVersion + ".aspx";
                        document = subMenuList.Where(w => w.Service == "TRANSFER").Count() > 0;
                        break;
                    case "HANDFEE":
                        viewPageUrl = "RSView_Handfee.aspx";
                        editPageUrl = "RSEdit_Handfee.aspx";
                        document = subMenuList.Where(w => w.Service == "HANDFEE").Count() > 0;
                        break;
                    case "EXCURSION":
                        viewPageUrl = "RSView_Excursion.aspx";
                        editPageUrl = "RSEdit_Excursion" + serviceEditVersion + ".aspx";
                        document = subMenuList.Where(w => w.Service == "EXCURSION").Count() > 0;
                        break;
                    case "INSURANCE":
                        viewPageUrl = "RSView_Insurance.aspx";
                        editPageUrl = "RSEdit_Insurance" + serviceEditVersion + ".aspx";
                        document = subMenuList.Where(w => w.Service == "INSURANCE").Count() > 0;
                        break;
                    case "RENTING":
                        viewPageUrl = "RSView_Renting.aspx";
                        editPageUrl = "RSEdit_Renting" + serviceEditVersion + ".aspx";
                        document = subMenuList.Where(w => w.Service == "RENTING").Count() > 0;
                        break;
                    case "VISA":
                        viewPageUrl = "RSView_Visa.aspx";
                        editPageUrl = "RSEdit_Visa" + serviceEditVersion + ".aspx";
                        document = subMenuList.Where(w => w.Service == "VISA").Count() > 0;
                        break;
                    default:
                        viewPageUrl = "RSView_Other.aspx";
                        editPageUrl = "RSEdit_Other" + serviceEditVersion + ".aspx";
                        break;
                }
                string resServiceUrl = string.Format("Controls/{0}?ResNo={1}&RecID={2}",
                        viewPageUrl,
                        row.ResNo,
                        row.RecID.ToString());
                string editServiceUrl = string.Format("Controls/{0}?RecID={1}",
                        editPageUrl,
                        row.RecID.ToString());
                #endregion

                string printDoc = string.Empty;
                bool editIncPackCompService = !(string.Equals(row.IncPack, "Y") || (row.Compulsory.HasValue && row.Compulsory.Value == true));
                bool? incPackOrCompServiceEditing = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "IncPackOrCompServiceEdit"));

                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || (incPackOrCompServiceEditing.HasValue && incPackOrCompServiceEditing.Value == true))
                    editIncPackCompService = true;

                printDoc = string.Format("<img title='{0}' src=\"Images/view.png\" onclick=\"showResService('{1}');\" />&nbsp;", HttpContext.GetGlobalResourceObject("ResView", "lblView"), resServiceUrl);
                if ((editIncPackCompService && editServices) && row.StatSer != 2 && row.StatSer != 3 && ResData.ResMain.ResLock == 0 && (row.Compulsory.HasValue && !row.Compulsory.Value))
                    if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel) || row.ServiceType == "TRANSFER")
                        printDoc += string.Format("<img title='{0}' src=\"Images/edit.png\" onclick=\"editResService('{1}');\" />&nbsp;", HttpContext.GetGlobalResourceObject("ResView", "lblEdit"), editServiceUrl);

                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                {
                    if (!string.Equals(row.ServiceType, "FLIGHT") && Equals(row.IncPack, "N") && row.Compulsory.HasValue && !row.Compulsory.Value && deleteServices && row.StatSer != 2 && row.StatSer != 3 && ResData.ResMain.ResLock == 0)
                        printDoc += string.Format("<img title=\"\" src=\"Images/cancel.png\" onclick=\"deleteServiceMsg({0});\" />", row.RecID);
                }
                else
                {
                    if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) && string.Equals(row.IncPack, "N") && row.Compulsory.HasValue && !row.Compulsory.Value && deleteServices && row.StatSer != 2 && row.StatSer != 3 && ResData.ResMain.ResLock == 0)
                        printDoc += string.Format("<img title=\"\" src=\"Images/cancel.png\" onclick=\"deleteServiceMsg({0});\" />", row.RecID);
                }
                if (busSeatSelect)
                    printDoc += string.Format("<img title=\"\" src=\"Images/BusSeat.png\" onclick=\"busSeatSelect({0});\" />", row.RecID);
                if (flightSeatSelect)
                {
                    bool useSeatCheckIn = true;
                    List<ResServiceExtRecord> extServiceList = ResData.ResServiceExt.Where(w => w.ServiceID == row.RecID).ToList<ResServiceExtRecord>();
                    foreach (var rExt in extServiceList)
                    {
                        ExtSerAllotDef alltDef = new Reservation().getExtraServiceAllotmentDefination(UserData, ResData.ResMain.PLMarket, row.Service, rExt.ExtService, rExt.Supplier, rExt.BegDate, rExt.EndDate, ref errorMsg);
                        if (alltDef != null && (ResData.ResMain.BegDate.Value - DateTime.Today).Days >= alltDef.RelDay)
                        {
                            useSeatCheckIn = false;
                        }
                    }
                    if (ResData.ResMain.OptDate.HasValue || ResData.ResMain.ConfStat != 1)
                        useSeatCheckIn = false;
                    if (useSeatCheckIn)
                        printDoc += string.Format("<img title=\"\" src=\"Images/flightSeat.png?v=2\" onclick=\"flightSeatSelect({0});\" />", row.RecID);
                }
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran))
                {
                    List<string> notdelete = new List<string>();
                    notdelete.Add("HTLSPRO");
                    notdelete.Add("BILETBANK");
                    notdelete.Add("ATLASJET");
                    if (notdelete.Contains(row.Supplier))
                        printDoc = string.Format("<img title='{0}' src=\"Images/view.png\" onclick=\"showResService('{1}');\" />&nbsp;", HttpContext.GetGlobalResourceObject("ResView", "lblView"), resServiceUrl);
                }
                sb.AppendFormat("<td style=\"width: 75px; text-align: left;\" class=\"handMouse\">{0}</td>", printDoc);
                sb.Append("</tr>");
            }
        }
        sb.Append("</table>");

        StringBuilder sbHeader = new StringBuilder();
        bool cancelBtnShow = sb.ToString().ToLower().IndexOf("cancel.png") > 0;
        bool editBtnShow = sb.ToString().ToLower().IndexOf("edit.png") > 0;
        bool viewBtnShow = sb.ToString().ToLower().IndexOf("view.png") > 0;
        if (cancelBtnShow || editBtnShow || viewBtnShow)
        {
            sbHeader.Append("<div style=\"white-space: nowrap;\">");
            if (cancelBtnShow)
            {
                sbHeader.Append("<img alt=\"\" src=\"Images/cancel.png\" height=\"18px\" />");
                sbHeader.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("ResView", "lblCancelService"));
            }
            if (editBtnShow)
            {
                if (cancelBtnShow)
                    sbHeader.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                sbHeader.Append("<img alt=\"\" src=\"Images/edit.png\" height=\"18px\" />");
                sbHeader.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("ResView", "lblEditService"));
            }
            if (viewBtnShow)
            {
                if (editBtnShow || cancelBtnShow)
                    sbHeader.Append("<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>");
                sbHeader.Append("<img alt=\"\" src=\"Images/view.png\" height=\"18px\" />");
                sbHeader.AppendFormat("&nbsp;-&nbsp;{0}", HttpContext.GetGlobalResourceObject("ResView", "lblView"));
            }
            sbHeader.Append("</div>");
        }

        return sbHeader.ToString() + sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getResServiceExtRecords()
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null)
            return "";
        StringBuilder sb = new StringBuilder();

        bool localName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        bool? showCompField = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ShowCompField"));
        bool? showIncPackField = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ShowIncPackField"));

        bool? _showCancelService = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showCancelService"));
        bool showCancelService = _showCancelService.HasValue ? _showCancelService.Value : true;

        bool? _serviceEdit = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ServiceEdit"));
        bool editServices = _serviceEdit.HasValue ? _serviceEdit.Value : true;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
            editServices = false;

        bool? _serviceDelete = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "ServiceDelete"));
        bool deleteServices = _serviceDelete.HasValue ? _serviceDelete.Value : false;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            deleteServices = false;

        List<ServiceExtMarOpt> showExtraService = new Reservation().getServiceExtMarOpt(UserData, UserData.Market, ref errorMsg);

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        sb.Append("<table id=\"gridResServiceExtTable\" cellpadding=\"2\" cellspacing=\"0\">");
        sb.Append("<tr id=\"gridResServiceExtHeader\">");
        sb.AppendFormat("<td>{0}</td><td>{1}</td>",
            HttpContext.GetGlobalResourceObject("ResView", "lblMainService"),
            HttpContext.GetGlobalResourceObject("ResView", "lblExtSrvDesc"));
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            sb.AppendFormat(deleteServices ? "{0}{1}<td>{2}</td><td>{3}</td><td>{4}</td><td>&nbsp;</td></tr>" : "{0}{1}<td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                !showIncPackField.HasValue || (showIncPackField.HasValue && showIncPackField.Value) ? "<td>" + HttpContext.GetGlobalResourceObject("ResView", "gridSrvInclude") + "</td>" : "",
                !showCompField.HasValue || (showCompField.HasValue && showCompField.Value) ? "<td>" + HttpContext.GetGlobalResourceObject("ResView", "gridSrvCompulsory") + "</td>" : "",
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvStatus"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvUnit"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvSalePrice"));
        else
            sb.AppendFormat(deleteServices ? "{0}{1}<td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>&nbsp;</td></tr>" : "{0}{1}<td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>",
                !showIncPackField.HasValue || (showIncPackField.HasValue && showIncPackField.Value) ? "<td>" + HttpContext.GetGlobalResourceObject("ResView", "gridSrvInclude") + "</td>" : "",
                !showCompField.HasValue || (showCompField.HasValue && showCompField.Value) ? "<td>" + HttpContext.GetGlobalResourceObject("ResView", "gridSrvCompulsory") + "</td>" : "",
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvConfirm"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvStatus"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvUnit"),
                HttpContext.GetGlobalResourceObject("ResView", "gridSrvSalePrice"));

        bool alternatingRow = true;
        foreach (TvBo.ResServiceExtRecord row in ResData.ResServiceExt.Where(w => (showCancelService || w.StatSer != 2)))
        {
            ServiceExtMarOpt showing = showExtraService.Find(f => f.ServiceCode == row.ExtService && f.ShowInResDetB2B.Value == false);
            if (showing == null)
            {
                FlightDayRecord flg = null;
                if (string.Equals(row.ServiceType, "FLIGHT"))
                {
                    ResServiceRecord serv = ResData.ResService.Find(f => f.RecID == row.ServiceID);
                    if (serv != null)
                        flg = new Flights().getFlightDay(UserData, serv.Service, serv.BegDate.Value, ref errorMsg);
                }
                alternatingRow = alternatingRow ? false : true;
                if (row.StatSer == 2 || row.StatSer == 3)
                {
                    sb.Append("<tr class=\"gridResServiceExtItemDisable\">");
                }
                else
                    if (alternatingRow)
                    sb.Append("<tr class=\"gridResServiceExtItemAlternating\">");
                else
                    sb.Append("<tr class=\"gridResServiceExtItem\">");

                //sb.AppendFormat("<td style=\"width: 30px; text-align: left;\">{0}</td>", row.RecID.ToString());
                sb.AppendFormat("<td style=\"width: 100px; text-align: left;\">{0}</td>", localName ? (string.IsNullOrEmpty(row.ServiceTypeNameL) ? row.ServiceType : row.ServiceTypeNameL) : (string.IsNullOrEmpty(row.ServiceTypeName) ? row.ServiceType : row.ServiceTypeName));
                string description = string.Empty;
                if (string.Equals(row.ServiceType, "FLIGHT"))
                {
                    description += flg != null && flg.TDepDate.HasValue ? flg.TDepDate.Value.ToShortDateString() : (row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "");
                }
                else
                {
                    description += row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "";
                    description += row.EndDate.HasValue ? (description.Length > 0 ? " - " : "") + row.EndDate.Value.ToShortDateString() : "";
                }
                List<ResCustRecord> ServiceCustomers = (from q1 in ResData.ResCust
                                                        join q2 in ResData.ResConExt on q1.CustNo equals q2.CustNo
                                                        where q2.ServiceID == row.RecID
                                                        select q1).ToList<ResCustRecord>();
                string serviceCusts = string.Empty;
                foreach (ResCustRecord r1 in ServiceCustomers)
                {
                    if (serviceCusts.Length > 0)
                        serviceCusts += ", ";
                    serviceCusts += string.Format("{0}. {1} {2}", r1.TitleStr, r1.Surname, r1.Name);
                }
                sb.AppendFormat("<td style=\"text-align: left;\">{0}<br /><span style=\"font-size: 7pt;\">({1})</span></td>",
                    (localName ? row.ExtServiceNameL : row.ExtServiceName) + (description.Length > 0 ? " (" + description + ")" : "&nbsp"),
                    serviceCusts);

                if (!showIncPackField.HasValue || (showIncPackField.HasValue && showIncPackField.Value == true))
                    sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", Equals(row.IncPack, "Y") ? "<img title=\"\" src=\"Images/accept.gif\" />" : "&nbsp;");

                if (!showCompField.HasValue || (showCompField.HasValue && showCompField.Value == true))
                    sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", Equals(row.Compulsory, "Y") ? "<img title=\"\" src=\"Images/accept.gif\" />" : "&nbsp;");

                if (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                    sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "ConfStatus" + row.StatConf.ToString()).ToString());
                sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + row.StatSer.ToString()).ToString());
                sb.AppendFormat("<td style=\"width: 40px; text-align: center;\">{0}</td>", row.Unit.ToString());
                if ((row.StatSer == 2 || row.StatSer == 3) && ResData.ResMain.ResLock != 0)
                    sb.AppendFormat("<td style=\"width: 90px; text-align: right;\">{0}</td>", "&nbsp;");
                else
                    sb.AppendFormat("<td style=\"width: 90px; text-align: right;\">{0}</td>", Equals(row.IncPack, "Y") ? HttpContext.GetGlobalResourceObject("ResView", "lblInPackage").ToString() : (row.SalePrice.HasValue ? row.SalePrice.Value.ToString() : "&nbsp;"));
                if (deleteServices && (row.StatSer.HasValue && row.StatSer < 2) && ResData.ResMain.ResLock == 0 && !Equals(row.IncPack, "Y") && !Equals(row.Compulsory, "Y"))
                {
                    string deleteStr = string.Format("<img title=\"\" src=\"Images/cancel.png\" onclick=\"deleteServiceExtMsg({0});\" />", row.RecID);
                    sb.AppendFormat("<td style=\"width: 40px; text-align: center;\">{0}</td>", row.StatSer != 2 && row.StatSer != 3 && ResData.ResMain.ResLock != 0 && !Equals(row.IncPack, "N") && !Equals(row.Compulsory, "N") ? "" : deleteStr);
                }
                sb.Append("</tr>");
            }
        }
        sb.Append("</table>");
        string retVal = string.Empty;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            editServices = false;
        else
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel) && ResData.ResMain.PaymentStat == "U")
            editServices = false;

        if (editServices && !Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && !Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && (ResData.ResMain.ResStat != 2 && ResData.ResMain.ResStat != 3) && ResData.ResMain.ResLock == 0)
        {
            retVal += "<div style=\"text-align: center;\">";
            retVal += string.Format("<input id=\"btnAddExtraService\" type=\"button\" value=\"{0}\" onclick=\"showAddResServiceExt('Controls/ExtraService_Add.aspx')\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only\" />",
                HttpContext.GetGlobalResourceObject("ResView", "lblAddExtraService"));
            retVal += "</div>";
        }
        return ResData.ResServiceExt.Count > 0 ? sb.ToString() + retVal : retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string getResSupDis()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null)
            return "";
        bool localName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        if (ResData.ResSupDis.Count > 0)
        {

            sb.Append("<br />");
            sb.Append("<fieldset><legend><label>»</label>");
            sb.AppendFormat("<span style=\"font-weight: bold;\" id=\"lblGridResSupDis\">{0}</span></legend>", "Supplement / Discount");
            sb.Append("<table id=\"gridResSupDisTable\" cellSpacing=\"0\" cellPadding=\"2\">");
            sb.Append("<tr class=\"gridResSupDisHeader\">");
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblDescription"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblAmount"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblCurr"));
            sb.AppendFormat("<td>{0}</td>", "%");
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblAppType"));
            sb.Append("</tr>");
            bool alternatingRow = true;
            foreach (TvBo.ResSupDisRecord row in ResData.ResSupDis)
            {
                alternatingRow = alternatingRow ? false : true;
                sb.AppendFormat("<tr class=\"{0}\">", !alternatingRow ? "gridResSupDisRow" : "gridResSupDisRowAlternating");
                sb.AppendFormat("<td class=\"resSupDisDesc\">{0}</td>", localName ? row.SupDisNameL : row.SupDisName);
                sb.AppendFormat("<td class=\"resSupDisAmount\">{0}</td>", row.Amount.HasValue && row.Amount.Value != 0 ? row.Amount.Value.ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"resSupDisCur\">{0}</td>", row.Cur);
                sb.AppendFormat("<td class=\"resSupDisPerVal\">{0}</td>", row.PerVal.HasValue && row.PerVal.Value > 0 ? row.PerVal.Value.ToString("#.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"resSupDisApplyType\">{0}</td>", Equals(row.ApplyType, "P") ? HttpContext.GetGlobalResourceObject("LibraryResource", "ApplyTypeName_P").ToString() : HttpContext.GetGlobalResourceObject("LibraryResource", "ApplyTypeName_A").ToString());
                sb.Append("</tr>");
            }

            sb.Append("</table>");
            sb.Append("</fieldset>");
            return sb.ToString();
        }
        else
            return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string getResPayment()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null)
            return "";
        bool localName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResPaymentsRecord> resPaymentList = new Reservation().getResPayment(UserData.Market, ResData.ResMain.ResNo, ref errorMsg);
        if (resPaymentList.Count > 0)
        {
            sb.Append("<br />");
            sb.Append("<fieldset><legend><label>»</label>");
            sb.AppendFormat("<span style=\"font-weight: bold;\" id=\"lblGridResPayment\">{0}</span></legend>",
                HttpContext.GetGlobalResourceObject("ResView", "lblPayments"));
            sb.Append("<table id=\"gridResPaymentTable\" cellSpacing=\"0\" cellPadding=\"2\">");
            sb.Append("<tr class=\"gridResPaymentHeader\">");
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblReceiptType"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblPaymentType"));

            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblClientName"));

            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblReference"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblPaymentDate"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblAmount"));
            sb.AppendFormat("<td>{0}</td>", HttpContext.GetGlobalResourceObject("ResView", "lblCurr"));
            sb.AppendFormat("<td>{0}</td>", "&nbsp;");
            sb.Append("</tr>");
            bool alternatingRow = true;
            bool? dontPrintReceipt = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("ResView", "DontPrintReceipt"));

            foreach (TvBo.ResPaymentsRecord row in resPaymentList)
            {
                Int16 recType = row.RecType.HasValue ? row.RecType.Value : Convert.ToInt16(-1);
                alternatingRow = alternatingRow ? false : true;
                sb.AppendFormat("<tr class=\"{0}\">", !alternatingRow ? "gridResPaymentRow" : "gridResPaymentRowAlternating");
                sb.AppendFormat("<td class=\"gridResPaymentReceipType\">{0}</td>", recType == 0 ? HttpContext.GetGlobalResourceObject("LibraryResource", "Payment").ToString() : (recType == 1 ? HttpContext.GetGlobalResourceObject("LibraryResource", "Refund").ToString() : "&nbsp;"));
                sb.AppendFormat("<td class=\"gridResPaymentPaymentType\">{0}</td>", localName ? row.PayTypeNameL : row.PayTypeName);
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                    sb.AppendFormat("<td class=\"gridResPaymentClientName\">{0}</td>", row.ClientName);
                sb.AppendFormat("<td class=\"gridResPaymentPaymentType\">{0}</td>", row.Reference);
                sb.AppendFormat("<td class=\"gridResPaymentPaymentDate\">{0}</td>", row.PayDate.HasValue ? row.PayDate.Value.ToShortDateString() : "&nbsp;");
                sb.AppendFormat("<td class=\"gridResPaymentAmount\">{0}</td>", row.PaidAmount.HasValue ? row.PaidAmount.Value.ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("<td class=\"gridResPaymentCur\">{0}</td>", row.PaidCur);
                if (dontPrintReceipt.HasValue && dontPrintReceipt.Value)
                    sb.AppendFormat("<td class=\"gridResPaymentCur\"></td>", row.JournalId);
                else
                    sb.AppendFormat("<td class=\"gridResPaymentCur\"><img src=\"Images/print.png\" onclick=\"showReceipt({0})\"/></td>", row.JournalId);
                sb.Append("</tr>");
            }

            sb.Append("</table>");
            sb.Append("</fieldset>");
            return sb.ToString();
        }
        else
            return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string getPaymentPlan(string ResNo)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<resPayPlanRecord> payPlan = new ReservationMonitor().getPaymentPlan(ResNo, ref errorMsg);
        if (payPlan != null && payPlan.Count > 0)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<table cellpadding=\"8\" cellspacing=\"8\" style=\"font-size: 9pt; width: 100%;\">");
            sb.Append(" <tr>");
            sb.Append("     <td align=\"left\"><b>" + HttpContext.GetGlobalResourceObject("LibraryResource", "DueDate_Date", UserData.Ci).ToString() + "</b></td>");
            sb.Append("     <td style=\"width:100px; text-align:right;\"><b>" + HttpContext.GetGlobalResourceObject("LibraryResource", "DueDate_Amount").ToString() + "</b></td>");
            sb.Append("     <td style=\"width:50px; text-align:right;\"><b>" + HttpContext.GetGlobalResourceObject("LibraryResource", "DueDate_Cur").ToString() + "</b></td>");
            sb.Append(" </tr>");
            foreach (resPayPlanRecord row in payPlan)
            {
                sb.Append(" <tr>");
                sb.AppendFormat("  <td>{0}</td>", row.DueDate.ToShortDateString());
                sb.AppendFormat("  <td align=\"right\">{0}</td>", row.Amount.HasValue ? row.Amount.Value.ToString("#,###.00") : "&nbsp;");
                sb.AppendFormat("  <td style=\"width: 50px; text-align:right;\">&nbsp;{0}</td>", row.Cur);
                sb.Append(" </tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }
        else
            return "";
    }

    [WebMethod(EnableSession = true)]
    public static string deleteCustomers(string ResNo, string CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<ReservastionSaveErrorRecord> saveReturn = new List<ReservastionSaveErrorRecord>();

        int _custNo = Conversion.getInt32OrNull(CustNo).HasValue ? Conversion.getInt32OrNull(CustNo).Value : Convert.ToInt32(-1);

        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResNo = ResData.ResMain.ResNo;

        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == _custNo);
        if (resCust == null)
        {
            saveReturn.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = HttpContext.GetGlobalResourceObject("SpCalcErrors", "CalcEXtService4").ToString() });
            return Newtonsoft.Json.JsonConvert.SerializeObject(saveReturn);
        }
        List<ResServiceRecord> resServiceDeleteCust = (from q1 in ResData.ResService
                                                       join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                       where q2.CustNo == _custNo
                                                       select q1).ToList<ResServiceRecord>();
        foreach (TvBo.ResServiceRecord row in resServiceDeleteCust)
        {
            Int16 retVal = new Reservation().DeleteTuristPrepareService(UserData, ResData, _custNo, row.RecID, PrepareServiceType.DeleteCustomers, ref errorMsg);
            if (retVal != 0)
            {
                if (retVal > 0)
                {
                    switch (retVal)
                    {
                        case 11:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomNotFound").ToString();
                            break;
                        case 12:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();
                            //"Odada kalacak minimum yetişkin sayısı, seçili yetişkin sayısından büyük."; 
                            break;
                        case 13:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();
                            //"Odada kalacak minimum yetişkin sayısı, seçili yetişkin sayısından küçük."; 
                            break;
                        case 14:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();
                            //"Odada kalacak minimum çocuk sayısı, seçili çocuk sayısından büyük."; 
                            break;
                        case 15:
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();
                            //"Odada kalacak minimum pax sayısı, seçili pax sayısından büyük."; 
                            break;
                    }
                    //var _data = '{"ServiceID":"' + serviceID + '","Adult":"' + adult + '","Child":"' + child + '"}';
                    TvBo.ResServiceRecord tmpService = ResData.ResService.Find(f => f.RecID == row.RecID);
                    string otherReturnValue = "<!ServiceID!:!" + row.RecID + "!,!Adult!:!" + tmpService.Adult + "!, !Child!:!" + row.Child + "!}";
                    saveReturn.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = retVal.ToString(), OtherReturnValue = otherReturnValue });
                    return Newtonsoft.Json.JsonConvert.SerializeObject(saveReturn);
                }
                else
                {
                    saveReturn.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = errorMsg });
                    return Newtonsoft.Json.JsonConvert.SerializeObject(saveReturn);
                }
            }
        }

        ResDataRecord ResDataOld = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        resCust.Status = 0;
        saveReturn = new Reservation().DeleteTourist(UserData, ref ResData, ResDataOld, _custNo, ref errorMsg);
        if (saveReturn.Count == 0 && string.IsNullOrEmpty(errorMsg))
            saveReturn.Add(new ReservastionSaveErrorRecord { ControlOK = true, Message = "OK" });
        else
            saveReturn.Add(new ReservastionSaveErrorRecord { ControlOK = false, Message = errorMsg });
        return Newtonsoft.Json.JsonConvert.SerializeObject(saveReturn);
    }

    [WebMethod(EnableSession = true)]
    public static string printDocument(string DocName, string DocUrl, bool? SubFolder, int? MultiLangID, int? FormID, string param1, string External)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string retValM = "\"NewWindow\":\"{0}\",\"JavaScriptName\":\"{1}\",\"Param1\":\"{2}\",\"Param2\":\"{3}\",\"ResultType\":\"{4}\"";
        string retVal = string.Empty;
        string errorMsg = string.Empty;

        bool AllowDocPrint = ResData.ResMain != null ? Equals(ResData.ResMain.AllowDocPrint, "Y") : false;
        bool isPayment = (string.Equals(ResData.ResMain.PaymentStat, "O") || string.Equals(ResData.ResMain.PaymentStat, "V")) || UserData.AgencyRec.DocPrtNoPay || AllowDocPrint;
        bool isPartyPaid = string.Equals(ResData.ResMain.PaymentStat, "O") || string.Equals(ResData.ResMain.PaymentStat, "V") || string.Equals(ResData.ResMain.PaymentStat, "P") || UserData.AgencyRec.DocPrtNoPay || AllowDocPrint;
        bool docPrintIsPayed = false;

        if (ResData != null && isPayment && ResData.ResMain.ResStat < 2)
            docPrintIsPayed = true;

        if (ResData != null && string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            if (isPayment && ResData.ResMain.ConfStat.Value == 1 && ResData.ResMain.ResStat < 2)
                docPrintIsPayed = true;
        }

        if (docPrintIsPayed == false && isPartyPaid == false && isPayment == false)
        {
            if (DocName == "FlyTicket" || DocName == "Voucher" || DocName == "Excursion" || DocName == "Transfer")
                return "{" + string.Format(retValM, "0", "deturReport", "", "Unauthorized printing of documents", "") + "}";
        }

        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string basePageUrl = WebRoot.BasePageRoot;
        string docFolder = basePageUrl + (SubFolder.HasValue && SubFolder.Value ? (DocumentFolder + "/" + UserData.Market + "/") : "");
        string docFile = docFolder + DocUrl;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        if (string.Equals(DocUrl, "TVPDF") || string.Equals(DocUrl, "TVHTML"))
        {
            #region Report TourVisio

            if (VersionControl.CheckWebVersion(UserData.WebVersion, 21, VersionControl.Equality.gt))
            {
                Int16? docOutputType = null;
                switch (DocUrl)
                {
                    case "TVPDF":
                        docOutputType = 0;
                        break;
                    case "TVHTML":
                        docOutputType = 1;
                        break;
                }
                bool isExternal = string.Equals(External, "1");
                string reportResult = new TvReports().getReportV2PDF(UserData, ResData, DocName, HttpContext.Current.Server.MapPath("~") + "\\ACE\\", MultiLangID, docOutputType, FormID, param1, docFolder, isExternal, ref errorMsg);
                if (!string.IsNullOrEmpty(errorMsg))
                    retVal = string.Format(retValM, "0", "deturReport", "", errorMsg, "");
                else
                {
                    if (!string.IsNullOrEmpty(reportResult))
                    {
                        if (docOutputType == 0)
                        {
                            if (isExternal)
                            {
                                //"\"NewWindow\":\"{0}\",\"JavaScriptName\":\"{1}\",\"Param1\":\"{2}\",\"Param2\":\"{3}\",\"ResultType\":\"{4}\"";
                                retVal = string.Format(retValM, "1", "paximum", reportResult, string.IsNullOrEmpty(reportResult) ? "Please contact your operator" : string.Empty, string.Empty);
                            }
                            else
                            {
                                retVal = string.Format(retValM, "0", "deturReport", WebRoot.BasePageRoot + "ACE/" + reportResult, "", "PDF");
                            }
                        }
                        else
                            if (docOutputType == 1)
                        {
                            retVal = string.Format("\"NewWindow\":\"{0}\",\"JavaScriptName\":\"{1}\",\"Param1\":\"{2}\",\"Param2\":\"{3}\",\"ResultType\":\"{4}\",\"nextHtmlUrl\":\"{5}\"",
                                                    "0",
                                                    "htmlPageRenderForNextPage",
                                                    WebRoot.BasePageRoot + "ACE/" + reportResult, "",
                                                    "HTML",
                                                    docFolder + "AgreementNext.aspx");
                        }
                    }
                    else
                    {
                        retVal = string.Format(retValM, "0", "deturReport", "", errorMsg, "");
                    }
                }
            }
            else
            {
                tvReportPDFRecord report = null;
                report = new TvReports().generateReportPDF(UserData, ResData, DocName, ref errorMsg);
                if (report != null)
                {
                    Int16 status = report.Status.HasValue ? report.Status.Value : (Int16)(-1);
                    switch (status)
                    {
                        case 0:
                            errorMsg = "Unable to create invoices. Please contact your Operator.(0)";
                            retVal = string.Format(retValM, "0", "deturReport", "", errorMsg, "");
                            break;
                        case 1:
                            if (((byte[])report.RptImage).Length > 0)
                            {
                                try
                                {
                                    string fileName = ResData.ResMain.ResNo + "_" + System.Guid.NewGuid() + ".PDF";
                                    if (File.Exists(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName))
                                        File.Delete(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName);
                                    FileStream fileStream = new FileStream(HttpContext.Current.Server.MapPath("~") + "\\ACE\\" + fileName, FileMode.Create, FileAccess.ReadWrite);
                                    fileStream.Write((byte[])report.RptImage, 0, ((byte[])report.RptImage).Length);
                                    fileStream.Close();
                                    retVal = string.Format(retValM, "0", "deturReport", WebRoot.BasePageRoot + "ACE/" + fileName, "", "PDF");
                                }
                                catch (Exception Ex)
                                {
                                    retVal = string.Format(retValM, "0", "deturReport", "", Ex.Message, "");
                                }
                            }
                            break;
                        case 2:
                            errorMsg = "Reservation not found.";
                            retVal = string.Format(retValM, "0", "deturReport", "", errorMsg, "");
                            break;
                        case 3:
                            errorMsg = "Unable to create invoices. Please contact your Operator.(0)";
                            retVal = string.Format(retValM, "0", "deturReport", "", errorMsg, "");
                            break;
                        default:
                            errorMsg = "Unable to create invoices. Please contact your Operator.(0)";
                            retVal = string.Format(retValM, "0", "deturReport", "", errorMsg, "");
                            break;
                    }
                }
                else
                {
                    retVal = string.Format(retValM, "0", "deturReport", "", errorMsg, "");
                }
            }
            #endregion
        }
        else if (Equals(DocName, "Comment"))
        {
            #region Comment
            retVal = string.Format(retValM,
                                        "2",
                                        "showResComment",
                                        "",
                                        "",
                                        "");
            #endregion
        }
        else if (Equals(DocName, "Complaint"))
        {
            #region Complaint
            retVal = string.Format(retValM,
                                        "2",
                                        "showComplaint",
                                        "",
                                        "",
                                        "");
            #endregion
        }
        else if (Equals(DocName, "Payment"))
        {
            #region Payment
            retVal = string.Format(retValM,
                                "0",
                                "getPayments",
                                basePageUrl + DocUrl,
                                "",
                                "");
            #endregion
        }
        else if (string.Equals(DocUrl, "STD"))
        {
            #region Standart Report
            if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "STD",
                                "PDF");
            }
            else if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "STD",
                                "PDF");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            #region Detur
            if (Equals(DocName, "Payment"))
            {
                retVal = string.Format(retValM,
                                    "2",
                                    "getPaymentRes",
                                    "Payments/BeginPayment.aspx",
                                    "",
                                    "");
            }
            else if (Equals(DocName, "PrintDocument"))
            {
                #region Print Document
                if (UserData.Market == "FINMAR")
                {
                    ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                    ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
                    if (leaderInfo == null)
                    {
                        retVal = string.Format(retValM,
                                                "0",
                                                "deturReport",
                                                "",
                                                HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError"),
                                                "");
                        return "{" + retVal + "}";
                    }
                    string filePath = HttpContext.Current.Server.MapPath("~") + "\\Document\\DET\\" + UserData.Market + "\\invoice.pdf";
                    string tempFilePath = HttpContext.Current.Server.MapPath("~") + "\\ACE\\";
                    string filename = new TvReport.DetReport.DetReport().getPDF_FinMar(filePath, tempFilePath, ResData.ResMain.ResNo, UserData.Market);

                    retVal = string.Format(retValM,
                                            "1",
                                            "deturReport",
                                            WebRoot.BasePageRoot + "ACE/" + filename,
                                            "",
                                            "PDF");
                    if (!string.IsNullOrEmpty(filename))
                    {
                        if (!Equals(ResData.ResMain.InvoiceIssued, "Y"))
                        {
                            FileStream st = new FileStream(tempFilePath + filename, FileMode.Open);
                            if (st.Length > 0)
                            {
                                byte[] buffer = new byte[st.Length];
                                st.Read(buffer, 0, (int)st.Length);
                                st.Close();
                                new TvReport.DetReport.DetReport().createInvoice(ResData.ResMain.ResNo, UserData.Operator, UserData.OprOffice, leader.CustNo.ToString(),
                                                    leader.Surname + " " + leader.Name,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHome : leaderInfo.AddrWork,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeZip : leaderInfo.AddrWorkZip,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCity : leaderInfo.AddrWorkCity,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry,
                                                    ResData.ResMain.PasAmount, ResData.ResMain.SaleCur, buffer, UserData.UserID, UserData.AgencyID,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeEmail : leaderInfo.AddrWorkEMail,
                                                    "");
                            }
                            else
                                st.Close();
                        }
                        if (new TvReport.DetReport.DetReport().invoiceIssued(ResData.ResMain.ResNo))
                        {
                            ResData.ResMain.InvoiceIssued = "Y";
                            HttpContext.Current.Session["ResData"] = ResData;
                        }
                    }
                }
                else if (UserData.Market == "SWEMAR")
                {
                    ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                    ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
                    if (leaderInfo == null)
                    {
                        retVal = string.Format(retValM,
                                                "0",
                                                "deturReport",
                                                "",
                                                HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError"),
                                                "");
                        return "{" + retVal + "}";
                    }
                    string filePath = HttpContext.Current.Server.MapPath("") + "\\Document\\DET\\" + UserData.Market + "\\invoice.pdf";
                    string tempFilePath = HttpContext.Current.Server.MapPath("") + "\\ACE\\";
                    string filename = new TvReport.DetReport.DetReport().getPDF_SweMar(filePath, tempFilePath, ResData.ResMain.ResNo, UserData.Market);
                    retVal = string.Format(retValM,
                                        "1",
                                        "deturReport",
                                        WebRoot.BasePageRoot + "ACE/" + filename,
                                        "",
                                        "PDF");
                    if (!string.IsNullOrEmpty(filename))
                    {
                        if (!Equals(ResData.ResMain.InvoiceIssued, "Y"))
                        {
                            FileStream st = new FileStream(tempFilePath + filename, FileMode.Open);
                            if (st.Length > 0)
                            {
                                byte[] buffer = new byte[st.Length];
                                st.Read(buffer, 0, (int)st.Length);
                                st.Close();
                                new TvReport.DetReport.DetReport().createInvoice(ResData.ResMain.ResNo, UserData.Operator, UserData.OprOffice, leader.CustNo.ToString(),
                                                    leader.Surname + " " + leader.Name,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHome : leaderInfo.AddrWork,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeZip : leaderInfo.AddrWorkZip,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCity : leaderInfo.AddrWorkCity,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry,
                                                    ResData.ResMain.PasAmount, ResData.ResMain.SaleCur, buffer, UserData.UserID, UserData.AgencyID,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeEmail : leaderInfo.AddrWorkEMail,
                                                    "");
                            }
                            else
                                st.Close();
                        }
                        if (new TvReport.DetReport.DetReport().invoiceIssued(ResData.ResMain.ResNo))
                        {
                            ResData.ResMain.InvoiceIssued = "Y";
                            HttpContext.Current.Session["ResData"] = ResData;
                        }
                    }
                }
                else if (UserData.Market == "DENMAR")
                {
                    ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                    ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
                    if (leaderInfo == null)
                    {
                        retVal = string.Format(retValM,
                                                "0",
                                                "deturReport",
                                                "",
                                                HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError"),
                                                "");
                        return "{" + retVal + "}";
                    }

                    string filePath = HttpContext.Current.Server.MapPath("") + "\\Document\\DET\\" + UserData.Market + "\\invoice.pdf";
                    string tempFilePath = HttpContext.Current.Server.MapPath("") + "\\ACE\\";
                    string filename = new TvReport.DetReport.DetReport().getPDF_DenMar(filePath, tempFilePath, ResData.ResMain.ResNo, UserData.Market);
                    retVal = string.Format(retValM,
                                        "1",
                                        "deturReport",
                                        WebRoot.BasePageRoot + "ACE/" + filename,
                                        "",
                                        "PDF");
                    if (!string.IsNullOrEmpty(filename))
                    {
                        if (!Equals(ResData.ResMain.InvoiceIssued, "Y"))
                        {
                            FileStream st = new FileStream(tempFilePath + filename, FileMode.Open);
                            if (st.Length > 0)
                            {
                                byte[] buffer = new byte[st.Length];
                                st.Read(buffer, 0, (int)st.Length);
                                st.Close();
                                new TvReport.DetReport.DetReport().createInvoice(ResData.ResMain.ResNo, UserData.Operator, UserData.OprOffice, leader.CustNo.ToString(),
                                                    leader.Surname + " " + leader.Name,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHome : leaderInfo.AddrWork,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeZip : leaderInfo.AddrWorkZip,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCity : leaderInfo.AddrWorkCity,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry,
                                                    ResData.ResMain.PasAmount, ResData.ResMain.SaleCur, buffer, UserData.UserID, UserData.AgencyID,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeEmail : leaderInfo.AddrWorkEMail,
                                                    "");
                            }
                            else
                                st.Close();
                        }
                        if (new TvReport.DetReport.DetReport().invoiceIssued(ResData.ResMain.ResNo))
                        {
                            ResData.ResMain.InvoiceIssued = "Y";
                            HttpContext.Current.Session["ResData"] = ResData;
                        }
                    }
                }
                else
                    if (UserData.Market == "NORMAR")
                {
                    ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                    ResCustInfoRecord leaderInfo = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
                    if (leaderInfo == null)
                    {
                        retVal = string.Format(retValM,
                                                "0",
                                                "deturReport",
                                                "",
                                                HttpContext.GetGlobalResourceObject("MakeReservation", "CustomerDataError"),
                                                "");
                        return "{" + retVal + "}";
                    }

                    string filePath = HttpContext.Current.Server.MapPath("") + "\\Document\\DET\\" + UserData.Market + "\\invoice.pdf";
                    string tempFilePath = HttpContext.Current.Server.MapPath("") + "\\ACE\\";
                    string filename = new TvReport.DetReport.DetReport().getPDF_NorMar(filePath, tempFilePath, ResData.ResMain.ResNo, UserData.Market);
                    retVal = string.Format(retValM,
                                        "1",
                                        "deturReport",
                                        WebRoot.BasePageRoot + "ACE/" + filename,
                                        "",
                                        "PDF");
                    if (!string.IsNullOrEmpty(filename))
                    {
                        if (!Equals(ResData.ResMain.InvoiceIssued, "Y"))
                        {
                            FileStream st = new FileStream(tempFilePath + filename, FileMode.Open);
                            if (st.Length > 0)
                            {
                                byte[] buffer = new byte[st.Length];
                                st.Read(buffer, 0, (int)st.Length);
                                st.Close();
                                new TvReport.DetReport.DetReport().createInvoice(ResData.ResMain.ResNo, UserData.Operator, UserData.OprOffice, leader.CustNo.ToString(),
                                                    leader.Surname + " " + leader.Name,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHome : leaderInfo.AddrWork,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeZip : leaderInfo.AddrWorkZip,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCity : leaderInfo.AddrWorkCity,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeCountry : leaderInfo.AddrWorkCountry,
                                                    ResData.ResMain.PasAmount, ResData.ResMain.SaleCur, buffer, UserData.UserID, UserData.AgencyID,
                                                    leaderInfo.InvoiceAddr != "W" ? leaderInfo.AddrHomeEmail : leaderInfo.AddrWorkEMail,
                                                    "");
                            }
                            else
                                st.Close();
                        }
                        if (new TvReport.DetReport.DetReport().invoiceIssued(ResData.ResMain.ResNo))
                        {
                            ResData.ResMain.InvoiceIssued = "Y";
                            HttpContext.Current.Session["ResData"] = ResData;
                        }
                    }
                }
                #endregion
            }
            else if (Equals(DocName, "EmailDocument"))
            {
                string url = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "b2cEmailUrl"));
                url = url.Trim().Trim('\n').Trim('\r');
                if (string.IsNullOrEmpty(url))
                {
                    retVal = string.Format(retValM, "0", "EmailSended", "Email service not found.", "", "");
                    return "{" + retVal + "}";
                }
                ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                ResCustInfoRecord leaderInfo = leader != null ? ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo) : null;
                if (leaderInfo != null)
                {
                    if ((Equals(leaderInfo.ContactAddr, "H") ? (string.IsNullOrEmpty(leaderInfo.AddrHomeEmail) ? true : false) : (string.IsNullOrEmpty(leaderInfo.AddrWorkEMail) ? true : false)) == false)
                    {

                        fi.detur.SendMail sm = new fi.detur.SendMail();
                        sm.Url = url;
                        sm.MailSend(ResData.ResMain.ResNo, UserData.Ci.Name, ResData.ResMain.Agency);
                        retVal = string.Format(retValM, "0", "EmailSended", "", "", "");
                        return "{" + retVal + "}";
                    }
                    else
                    { //not sended.
                        retVal = string.Format(retValM, "0", "EmailSended", HttpContext.GetGlobalResourceObject("LibraryResource", "LeaderEmailEmpty"), "", "");
                        return "{" + retVal + "}";
                    }
                }
                else
                { //not sended.
                    retVal = string.Format(retValM, "0", "EmailSended", HttpContext.GetGlobalResourceObject("LibraryResource", "LeaderIsNo"), "", "");
                    return "{" + retVal + "}";
                }
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
        {
            #region Sunrise
            if (Equals(DocName, "Insurance"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                    if (Equals(DocName, "FlyTicketRef"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "HTMLPDF");
            }
            else
                        if (Equals(DocName, "Aggreement"))
            {
                //showAggrement sunriseReportShow
                string filename = new SunReport().showAggrement(UserData, ResData);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                WebRoot.BasePageRoot + "ACE/" + filename,
                                "",
                                "PDF");
            }
            else
                            if (Equals(DocName, "Proforma"))
            {
                //showAggrement sunriseReportShow
                string filename = new SunReport().showProforma(UserData, ResData);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                WebRoot.BasePageRoot + "ACE/" + filename,
                                "",
                                "PDF");
            }
            else
                                if (Equals(DocName, "Invoice"))
            {
                //showAggrement sunriseReportShow
                string filename = new SunReport().showInvoice(UserData, ResData);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                WebRoot.BasePageRoot + "ACE/" + filename,
                                "",
                                "PDF");
            }
            else
                                    if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                                        if (Equals(DocName, "Visa"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
        {
            #region Anex Ru
            if (Equals(DocName, "TicketPriceRefound"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                if (Equals(DocName, "Payment"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                    if (Equals(DocName, "Insurance"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                        if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                            if (Equals(DocName, "AgencyConfirmation"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                                if (Equals(DocName, "Aggreement"))
            {
                string filename = new AnexReport().showAggrement(UserData, ResData);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                WebRoot.BasePageRoot + "ACE/" + filename,
                                "",
                                "PDF");
            }
            else
                                    if (Equals(DocName, "Proforma"))
            {
                //showAggrement sunriseReportShow
                string filename = new AnexReport().showProforma(UserData, ResData);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                WebRoot.BasePageRoot + "ACE/" + filename,
                                "",
                                "PDF");
            }
            else
                                        if (Equals(DocName, "Invoice"))
            {
                //showAggrement sunriseReportShow
                string filename = new AnexReport().showInvoice(UserData, ResData);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                WebRoot.BasePageRoot + "ACE/" + filename,
                                "",
                                "PDF");
            }
            else
                                            if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                                                if (Equals(DocName, "Visa"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel))
        {
            #region NTravel
            if (Equals(DocName, "Insurance"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "",
                                "PDF");
            }
            else
                if (Equals(DocName, "Contract"))
            {
                string filename = new AzrReport().showContract(UserData, ResData);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                WebRoot.BasePageRoot + "ACE/" + filename,
                                "",
                                "PDF");
            }
            else
                    if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                        if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                            if (Equals(DocName, "Invoice"))
            {
                //showAggrement sunriseReportShow
                string filename = new AzrReport().showInvoice(UserData, ResData, ref errorMsg);
                if (string.IsNullOrEmpty(filename))
                    retVal = string.Format(retValM,
                                    "0",
                                    "sunriseReportShow",
                                    "",
                                    errorMsg,
                                    "");
                else
                    retVal = string.Format(retValM,
                                    "1",
                                    "sunriseReportShow",
                                    WebRoot.BasePageRoot + "ACE/" + filename,
                                    "",
                                    "PDF");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            #region Novatouras
            if (Equals(DocName, "Agreement"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else if (Equals(DocName, "Invoice"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else if (Equals(DocName, "ClientInvoice"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else if (Equals(DocName, "ACEFiles"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            #endregion
        }

        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
        {
            #region Fit Tourism
            if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                    if (Equals(DocName, "Invoice"))
            {
                //showAggrement sunriseReportShow
                string filename = new FitReport().showInvoice(UserData, ResData, ref errorMsg);
                if (string.IsNullOrEmpty(filename))
                    retVal = string.Format(retValM,
                                    "0",
                                    "sunriseReportShow",
                                    "",
                                    errorMsg,
                                    "");
                else
                    retVal = string.Format(retValM,
                                    "1",
                                    "sunriseReportShow",
                                    WebRoot.BasePageRoot + "ACE/" + filename,
                                    "",
                                    "PDF");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Magellan))
        {
            #region Magellan
            if (Equals(DocName, "Insurance"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                if (Equals(DocName, "Contract"))
            {
                //string filename = new MagReport().showContract(UserData, ResData);
                //retVal = string.Format(retVal, "1", "sunriseReportShow", HttpContext.Current.Session["BasePageUrl"].ToString() + "ACE/" + filename, "");
            }
            else
                    if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                        if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                            if (Equals(DocName, "Invoice"))
            {
                string filename = new MagReport().showInvoice(UserData, ResData, ref errorMsg);
                if (string.IsNullOrEmpty(filename))
                    retVal = string.Format(retValM,
                                    "0",
                                    "sunriseReportShow",
                                    "",
                                    errorMsg,
                                    "");
                else
                    retVal = string.Format(retValM,
                                    "1",
                                    "sunriseReportShow",
                                    WebRoot.BasePageRoot + "ACE/" + filename,
                                    "",
                                    "PDF");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran))
        {
            #region Safiran
            if (Equals(DocName, "Payment"))
            {
                retVal = string.Format(retValM,
                                    "0",
                                    "getPayments",
                                    basePageUrl + DocUrl,
                                    "",
                                    "");
            }
            else
                if (Equals(DocName, "Agreement"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                    if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                        if (Equals(DocName, "Proforma"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                            if (Equals(DocName, "Invoice"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                                if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Azur))
        {
            #region Azur Reizen
            if (Equals(DocName, "Invoice"))
            {
                //showAggrement sunriseReportShow
                string filename = new AzurReport().showInvoice(UserData, ResData, ref errorMsg);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                string.IsNullOrEmpty(filename) ? "" : WebRoot.BasePageRoot + "ACE/" + filename,
                                string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(errorMsg) ? errorMsg : "",
                                "PDF");
            }
            else
                if (Equals(DocName, "ClientInvoice"))
            {
                //showAggrement sunriseReportShow
                string filename = new AzurReport().showInvoiceClient(UserData, ResData, ref errorMsg);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                string.IsNullOrEmpty(filename) ? "" : WebRoot.BasePageRoot + "ACE/" + filename,
                                string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(errorMsg) ? errorMsg : "",
                                "PDF");
            }
            else
                    if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                        if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                            if (Equals(DocName, "Contract"))
            {
                string filename = new AzurReport().showContract(UserData, ResData, ref errorMsg);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                string.IsNullOrEmpty(filename) ? "" : WebRoot.BasePageRoot + "ACE/" + filename,
                                string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(errorMsg) ? errorMsg : "",
                                "PDF");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda))
        {
            #region Rezeda
            if (Equals(DocName, "Invoice"))
            {
                //showAggrement sunriseReportShow
                string filename = new RezedaReport().showInvoice(UserData, ResData, ref errorMsg);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                string.IsNullOrEmpty(filename) ? "" : WebRoot.BasePageRoot + "ACE/" + filename,
                                string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(errorMsg) ? errorMsg : "",
                                "PDF");
            }
            else
                if (Equals(DocName, "ClientInvoice"))
            {
                //showAggrement sunriseReportShow
                string filename = new RezedaReport().showInvoiceClient(UserData, ResData, ref errorMsg);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                string.IsNullOrEmpty(filename) ? "" : WebRoot.BasePageRoot + "ACE/" + filename,
                                string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(errorMsg) ? errorMsg : "",
                                "PDF");
            }
            else
                    if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                        if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                            if (Equals(DocName, "Contract"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Qualitum))
        {
            #region Qualitum
            if (Equals(DocName, "Invoice"))
            {
                string filename = new QuaReport().showInvoice(UserData, ResData, ref errorMsg);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                string.IsNullOrEmpty(filename) ? "" : WebRoot.BasePageRoot + "ACE/" + filename,
                                string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(errorMsg) ? errorMsg : "",
                                "PDF");
            }
            else
                if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                    if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Quheilan))
        {
            #region Quheilan
            if (Equals(DocName, "Payment"))
            {
                retVal = string.Format(retValM,
                                    "0",
                                    "getPayments",
                                    basePageUrl + DocUrl,
                                    "",
                                    "");
            }
            else
                if (Equals(DocName, "Agreement"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                    if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                        if (Equals(DocName, "Proforma"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                            if (Equals(DocName, "Invoice"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                                if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_BigBlue))
        {
            #region Big Blue
            if (Equals(DocName, "Invoice"))
            {
                string filename = new QuaReport().showInvoice(UserData, ResData, ref errorMsg);
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReportShow",
                                string.IsNullOrEmpty(filename) ? "" : WebRoot.BasePageRoot + "ACE/" + filename,
                                string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(errorMsg) ? errorMsg : "",
                                "PDF");
            }
            else
                if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                    if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                        if (Equals(DocName, "Contract"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mahbal))
        {
            #region Mahbal
            if (Equals(DocName, "Payment"))
            {
                retVal = string.Format(retValM,
                                    "0",
                                    "getPayments",
                                    basePageUrl + DocUrl,
                                    "",
                                    "");
            }
            else
                if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                    if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            else
                        if (Equals(DocName, "Contract"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                            if (Equals(DocName, "Receipt"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "sunriseReport",
                                DocName,
                                "",
                                "PDF");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
        {
            #region MNG
            if (Equals(DocName, "Payment"))
            {
                retVal = string.Format(retValM,
                                    "0",
                                    "getPayments",
                                    basePageUrl + DocUrl,
                                    "",
                                    "");
            }
            else
                if (Equals(DocName, "Agreement"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                    if (Equals(DocName, "Voucher"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                        if (Equals(DocName, "Proforma"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                            if (Equals(DocName, "Invoice"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
                                if (Equals(DocName, "FlyTicket"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Kenba))
        {
            #region Kenba
            if (Equals(DocName, "EmailDocument"))
            {
                string url = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "b2cEmailUrl"));
                url = url.Trim().Trim('\n').Trim('\r');
                if (string.IsNullOrEmpty(url))
                {
                    retVal = string.Format(retValM, "0", "EmailSended", "Email service not found.", "", "");
                    return "{" + retVal + "}";
                }
                ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                ResCustInfoRecord leaderInfo = leader != null ? ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo) : null;
                if (leaderInfo != null)
                {
                    if ((Equals(leaderInfo.ContactAddr, "H") ? (string.IsNullOrEmpty(leaderInfo.AddrHomeEmail) ? true : false) : (string.IsNullOrEmpty(leaderInfo.AddrWorkEMail) ? true : false)) == false)
                    {

                        fi.detur.SendMail sm = new fi.detur.SendMail();
                        sm.Url = url;
                        sm.MailSend(ResData.ResMain.ResNo, UserData.Ci.Name, ResData.ResMain.Agency);
                        retVal = string.Format(retValM, "0", "EmailSended", "", "", "");
                        return "{" + retVal + "}";
                    }
                    else
                    { //not sended.
                        retVal = string.Format(retValM, "0", "EmailSended", HttpContext.GetGlobalResourceObject("LibraryResource", "LeaderEmailEmpty"), "", "");
                        return "{" + retVal + "}";
                    }
                }
                else
                { //not sended.
                    retVal = string.Format(retValM, "0", "EmailSended", HttpContext.GetGlobalResourceObject("LibraryResource", "LeaderIsNo"), "", "");
                    return "{" + retVal + "}";
                }
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Emerald))
        {
            #region Emerald
            if (Equals(DocName, "FileDocument"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "getPayments",
                                docFile,
                                DocName,
                                "");
            }
            else if (Equals(DocName, "Invoice") || Equals(DocName, "Proforma"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
            {
                retVal = string.Empty;
            }
            #endregion
        }
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_KidyTour))
        {
            #region Kidy Tour
            if (Equals(DocName, "FileDocument"))
            {
                retVal = string.Format(retValM,
                                "1",
                                "getPayments",
                                docFile,
                                DocName,
                                "");
            }
            else if (Equals(DocName, "Invoice") || Equals(DocName, "Proforma"))
            {
                retVal = string.Format(retValM, "1", "htmlPageRender", docFile, DocName, "");
            }
            else
            {
                retVal = string.Empty;
            }
            #endregion
        }


        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getReportList(string Param1, string Param2)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        if (Equals(Param1, "Insurance"))
        {
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                return new SunReport().showInsuranceListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel))
                return new AzrReport().showInsuranceListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                return new AnexReport().showInsuranceListHTML(UserData, ResData);
        }
        else
            if (Equals(Param1, "Voucher"))
        {
            if (string.Equals(Param2, "STD"))
                return new StandartReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                return new SunReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel))
                return new AzrReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
                return new FitReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Magellan))
                return new MagReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                return new NovReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Azur))
                return new AzurReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Qualitum))
                return new QuaReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_BigBlue))
                return new BigBlueReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mahbal))
                return new MahbalReport().showVoucherListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                return new AnexReport().showVoucherListHTML(UserData, ResData);
        }
        else
                if (Equals(Param1, "FlyTicket"))
        {
            if (string.Equals(Param2, "STD"))
                return new StandartReport().showFlightTicketListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                return new SunReport().showFlightTicketListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel))
                return new AzrReport().showFlightTicketListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
                return new FitReport().showFlightTicketListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                return new NovReport().showFlightTicketListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Azur))
                return new AzurReport().showFlightTicketListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda))
                return new RezedaReport().showFlightTicketListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Qualitum))
                return new QuaReport().showFlightTicketListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mahbal))
                return new MahbalReport().showFlightTicketListHTML(UserData, ResData);
            else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                return new AnexReport().showFlightTicketListHTML(UserData, ResData);
        }
        else
                    if (Equals(Param1, "FlyTicketRef"))
        {
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
                return new SunReport().showFlightTicketRefListHTML(UserData, ResData);
        }
        else
                        if (Equals(Param1, "Receipt"))
        {
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mahbal))
                return new MahbalReport().showReceiptListHTML(UserData, ResData);
        }
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string getReportRun(string ResNo, int? Service, int? CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == Service);
        string retValM = "\"NewWindow\":\"{0}\", \"JavaScriptName\":\"{1}\", \"Param1\":\"{2}\", \"Param2\":\"{3}\"";
        string retVal = string.Empty;
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("~\\");
        string returnUrl = string.Empty;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            returnUrl = new SunReport().showInsurance(UserData, ref ResData, Service, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            returnUrl = new AnexReport().showInsurance(UserData, ref ResData, Service, CustNo);
        else
            returnUrl = new AzrReport().showInsurance(UserData, ref ResData, Service, CustNo);

        HttpContext.Current.Session["ResData"] = ResData;
        /*
        try
        { 
         
        string fileName = siteFolderISS + "ACE\\" + returnUrl;
        FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        byte[] pdfdata = new byte[fileStream.Length];
        fileStream.Read(pdfdata, 0, Convert.ToInt32(fileStream.Length));
        fileStream.Close();
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Length", pdfdata.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AddHeader("Accept-Ranges", "bytes");
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.AddHeader("Expires", "0");
            HttpContext.Current.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            HttpContext.Current.Response.AddHeader("Pragma", "public");
            HttpContext.Current.Response.AddHeader("content-Transfer-Encoding", "binary");
            HttpContext.Current.Response.AddHeader("Content-Type", "binary/octet-stream");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + returnUrl + "\"");
            HttpContext.Current.Response.BinaryWrite(pdfdata);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End(); 
        }
        catch
        {
        }
        */
        string returnInsuranceListHtml = string.Empty;
        if (!string.IsNullOrEmpty(returnUrl))
            returnInsuranceListHtml = getReportList("Insurance", "");

        string errorMsg = string.Empty;
        if (string.IsNullOrEmpty(returnUrl))
            return string.Format(retValM,
                "-99",
                "",
                "Report error.",
                "");
        returnUrl = basePageUrl + "ACE/" + returnUrl;
        retVal = string.Format(retValM,
                                    "0",
                                    "sunriseReportShow",
                                    returnUrl,
                                    string.IsNullOrEmpty(returnInsuranceListHtml) ? string.Empty : returnInsuranceListHtml.Replace('"', '!'));
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getReportVoucher(string ResNo, int? Service)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        List<ResViewMenu> subMenuList = CacheObjects.getResViewMenuData();

        ResViewMenu voucher = subMenuList.Where(w => w.Code == "Voucher").FirstOrDefault();

        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == Service);
        string retVal = "\"NewWindow\":\"{0}\", \"JavaScriptName\":\"{1}\", \"Param1\":\"{2}\", \"Param2\":\"{3}\"";
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        string returnUrl = string.Empty;
        if (voucher != null && string.Equals(voucher.Url, "STD"))
            returnUrl = new StandartReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            returnUrl = new SunReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel))
            returnUrl = new AzrReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
            returnUrl = new FitReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Magellan))
            returnUrl = new MagReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            returnUrl = new NovReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Azur))
            returnUrl = new AzurReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda))
            returnUrl = new RezedaReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Qualitum))
            returnUrl = new QuaReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_BigBlue))
            returnUrl = new BigBlueReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            returnUrl = new AnexReport().showHotelVoucher(UserData, ResData, Service);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mahbal))
            returnUrl = new MahbalReport().showHotelVoucher(UserData, ResData, Service);

        string errorMsg = string.Empty;

        if (string.IsNullOrEmpty(returnUrl))
            return string.Format(retVal,
                "-99",
                "",
                "Report error.",
                "");
        ResData = new Reservation().setPrintVoucherDoc(UserData, ResData, Service, ref errorMsg);
        HttpContext.Current.Session["ResData"] = ResData;
        string _html = getReportList("Voucher", "");
        returnUrl = basePageUrl + "ACE/" + returnUrl;
        retVal = string.Format(retVal,
                                    "0",
                                    "sunriseReportShow",
                                    returnUrl,
                                    _html.Replace('"', '!'));
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getReportTicket(string ResNo, int? CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ///
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == CustNo);
        List<ResViewMenu> subMenuList = CacheObjects.getResViewMenuData();
        ResViewMenu voucher = subMenuList.Where(w => w.Code == "FlyTicket").FirstOrDefault();
        string retVal = "\"NewWindow\":\"{0}\", \"JavaScriptName\":\"{1}\", \"Param1\":\"{2}\", \"Param2\":\"{3}\"";
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        string returnUrl = string.Empty;
        if (voucher != null && string.Equals(voucher.Url, "STD"))
            returnUrl = new StandartReport().showFlightTicket(UserData, ResData, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            returnUrl = new SunReport().showFlightTicket(UserData, ResData, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_NTravel))
            returnUrl = new AzrReport().showFlightTicket(UserData, ResData, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
            returnUrl = new FitReport().showFlightTicket(UserData, ResData, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            returnUrl = new NovReport().showFlightTicket(UserData, ResData, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Azur))
            returnUrl = new AzurReport().showFlightTicket(UserData, ResData, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Rezeda))
            returnUrl = new RezedaReport().showFlightTicket(UserData, ResData, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Qualitum))
            returnUrl = new QuaReport().showFlightTicket(UserData, ResData, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mahbal))
            returnUrl = new MahbalReport().showFlightTicket(UserData, ResData, CustNo);
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
            returnUrl = new AnexReport().showFlightTicket(UserData, ResData, CustNo);

        string _html = getReportList("FlyTicket", "");
        string errorMsg = string.Empty;
        if (string.IsNullOrEmpty(returnUrl))
            return string.Format(retVal,
                "-99",
                "",
                "Report error.",
                "");
        returnUrl = basePageUrl + "ACE/" + returnUrl;

        retVal = string.Format(retVal,
                                    "0",
                                    "sunriseReportShow",
                                    returnUrl,
                                    _html.Replace('"', '!'));
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getReportReceipt(string ResNo, int? ReceiptNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        string retVal = "\"NewWindow\":\"{0}\", \"JavaScriptName\":\"{1}\", \"Param1\":\"{2}\", \"Param2\":\"{3}\"";
        object _documentFolder = new TvBo.Common().getFormConfigValue("Report", "DocumentFolder");
        string DocumentFolder = _documentFolder != null ? Conversion.getStrOrNull(_documentFolder) : "";
        string docFolder = DocumentFolder + "\\" + UserData.Market + "\\";
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";

        string returnUrl = string.Empty;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Mahbal))
            returnUrl = new MahbalReport().showReceipt(UserData, ref ResData, ReceiptNo);
        string _html = getReportList("Receipt", "");
        string errorMsg = string.Empty;
        if (string.IsNullOrEmpty(returnUrl))
            return string.Format(retVal,
                "-99",
                "",
                "Report error.",
                "");
        returnUrl = basePageUrl + "ACE/" + returnUrl;

        retVal = string.Format(retVal,
                                    "0",
                                    "sunriseReportShow",
                                    returnUrl,
                                    _html.Replace('"', '!'));

        HttpContext.Current.Session["ResData"] = ResData;
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getCommentData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            return new TvBo.ReservationMonitor().setReadComment(UserData.UserID, ResData.ResMain.ResNo, -1, UserData.Ci.LCID);
        else
            return new TvBo.ReservationMonitor().getCommentDataString(ResData.ResMain.ResNo, UserData.UserID, UserData.Ci.LCID);
    }

    [WebMethod(EnableSession = true)]
    public static string setCommentData(string Comment)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;

        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        retVal = new TvBo.ReservationMonitor().setComment(UserData, ResData.ResMain.ResNo, Comment, UserData.Ci.LCID);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string getChangeLeaderData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        List<ResCustRecord> resCust = ResData.ResCust.Where(w => w.Title < 6 && w.Status == 0).Select(s => s).ToList<ResCustRecord>();

        StringBuilder sb = new StringBuilder();

        foreach (ResCustRecord row in resCust)
        {
            sb.AppendFormat("<tr><td class=\"radieTd\"><input type=\"radio\" name=\"leader\" value=\"{0}\" {2} /></td><td class=\"nameTd\">{1}</td></tr>",
                row.CustNo,
                row.TitleStr + " " + row.Surname + " " + row.Name,
                Equals(row.Leader, "Y") ? "checked=\"checked\"" : "");
        }
        if (sb.Length > 0)
        {
            sb.Insert(0, "<table id=\"resChangeLeader\">");
            sb.Append("</table>");
        }
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string saveChangeLeaderData(int? CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResCustRecord> resCust = ResData.ResCust.Where(w => w.Title < 6).Select(s => s).ToList<ResCustRecord>();

        ResCustRecord oldResCust = resCust.Find(f => f.Leader == "Y");
        oldResCust.Leader = "N";
        ResCustRecord newResCust = resCust.Find(f => f.CustNo == CustNo);
        newResCust.Leader = "Y";
        string retStr = "[\"retVal\":\"{0}\",\"retMsg\":\"{1}\"]";
        List<CustControlErrorRecord> custCont = new Reservation().CustControl(UserData, ResData, ref errorMsg);
        if (custCont == null || (custCont.Count == 1 && custCont.FirstOrDefault().ErrorCode == 0))
        {
            bool retVal = new Reservation().changeReservationLeader(UserData, CustNo, ref errorMsg);
            if (retVal)
                return string.Format(retStr, "0", "").Replace('[', '{').Replace(']', '}');
            else
                return string.Format(retStr, "-1", errorMsg).Replace('[', '{').Replace(']', '}');
        }
        else
            return string.Format(retStr, "1", errorMsg).Replace('[', '{').Replace(']', '}');
    }

    [WebMethod(EnableSession = true)]
    public static string getResChgFee(Int16? ChgType, int? CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        ResChgFeePrice resChgFee = new Reservation().getResChgFee(ResData, ChgType, true, ref errorMsg);
        if (resChgFee == null)
            return "";
        else
        {
            if (ChgType == 44 && resChgFee.ChgPer.HasValue && resChgFee.ChgPer.Value > 0 && (!resChgFee.ChgAmount.HasValue || resChgFee.ChgAmount.Value == 0))
            {
                ResCustRecord cust = ResData.ResCust.Find(f => f.CustNo == CustNo);
                if (cust != null)
                {
                    decimal? ppPrice = null;
                    if (cust.Status == 0)
                        ppPrice = (cust.ppSalePrice.HasValue ? cust.ppSalePrice.Value : 0) + (cust.ppPromoAmount.HasValue ? cust.ppPromoAmount.Value : 0) - (cust.PpPasEB.HasValue ? cust.PpPasEB.Value : 0);
                    else
                        ppPrice = cust.ppBaseCanAmount;
                    resChgFee.ChgAmount = ppPrice.HasValue ? ppPrice.Value * (resChgFee.ChgPer.Value / 100) : 0;
                    resChgFee.ChgCur = ResData.ResMain.SaleCur;
                }
            }
            return string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                            resChgFee.ChgAmount.Value.ToString("#,###.0"), resChgFee.ChgCur);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string setReadCommentData(string ResNo, string UserID, int? RecID, int? lcID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;
        retVal = new TvBo.ReservationMonitor().setReadComment(UserID, ResNo, RecID.HasValue ? RecID.Value : -1, lcID.HasValue ? lcID.Value : -1);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string cancelResService(int? RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null)
            return "";
        if (!RecID.HasValue)
            return "";
        string errorMsg = string.Empty;
        string retVal = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        List<ReservastionSaveErrorRecord> returnData = new Reservation().deleteResServiceRealData(UserData, ref ResData, RecID, ref errorMsg);
        if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
        {
            //if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            //{
            //    string Msg = string.Empty;
            //    foreach (ReservastionSaveErrorRecord row in returnData)
            //    {
            //        if (!row.ControlOK)
            //            Msg += row.Message + "\n";
            //    }
            //    retVal = Msg;
            //}
            //else
            //{
            HttpContext.Current.Session["ResData"] = ResData;
            retVal = "";
            //}
        }
        else
        {
            string Msg = string.Empty;
            foreach (ReservastionSaveErrorRecord row in returnData)
            {
                if (!row.ControlOK)
                    Msg += row.Message + "\n";
            }
            retVal = Msg;
        }

        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string cancelResServiceExt(int? RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null)
            return "";
        if (!RecID.HasValue)
            return "";
        string errorMsg = string.Empty;
        string retVal = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        List<ReservastionSaveErrorRecord> returnData = new Reservation().deleteResServiceExtRealData(UserData, ref ResData, RecID, ref errorMsg);
        if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
        {
            //if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            //{
            //    string Msg = string.Empty;
            //    foreach (ReservastionSaveErrorRecord row in returnData)
            //    {
            //        if (!row.ControlOK)
            //            Msg += row.Message + "\n";
            //    }
            //    retVal = Msg;
            //}
            //else
            //{
            HttpContext.Current.Session["ResData"] = ResData;
            retVal = "";
            //}
        }
        else
        {
            string Msg = string.Empty;
            foreach (ReservastionSaveErrorRecord row in returnData)
            {
                if (!row.ControlOK)
                    Msg += row.Message + "\n";
            }
            retVal = Msg;
        }

        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string viewPDFReport(string reportType, string _html, string pageWidth, string ResNo, string urlBase, string pdfConvSerial)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        _html = _html.Replace('|', '"');
        _html = _html.Replace("¨", @"\");
        _html = _html.Replace("<div id=\"noPrintDiv\">", "<div id=\"noPrintDiv\" style=\"display: none; visibility: hidden;\">");
        string errorMsg = string.Empty;
        string basePageUrl = WebRoot.BasePageRoot;
        string siteFolderISS = HttpContext.Current.Server.MapPath("") + "\\";
        string pdfFileName = string.Empty;
        pdfFileName = reportType + "_" + ResNo + ".pdf";
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
            pdfFileName = reportType + "_" + ResNo + "_" + System.Guid.NewGuid() + ".pdf";
        StringBuilder html = new StringBuilder(_html);
        int PageWidth = Conversion.getInt32OrNull(pageWidth).HasValue ? Conversion.getInt32OrNull(pageWidth).Value : 700;

        //initialize the PdfConvert object
        PdfConverter pdfConverter = new PdfConverter();

        pdfConverter.HtmlViewerWidth = PageWidth + 20;

        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
        pdfConverter.PdfDocumentOptions.FitWidth = true;
        pdfConverter.PdfDocumentOptions.JpegCompressionEnabled = true;
        pdfConverter.PdfDocumentOptions.JpegCompressionLevel = 50;
        pdfConverter.PdfDocumentOptions.EmbedFonts = true;

        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
        pdfConverter.PdfDocumentOptions.LeftMargin = 10;
        pdfConverter.PdfDocumentOptions.TopMargin = 5;

        // set the demo license key
        //pdfConverter.LicenseKey = pdfConvSerial; // "RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==";
        pdfConverter.LicenseKey = ConfigurationManager.AppSettings["Winnovative"];// "RG92ZHVkdXF0cGRxanRkd3VqdXZqfX19fQ==";

        // get the base url for string conversion which is the url from where the html code was retrieved
        // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
        string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
        string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";

        try
        {
            pdfConverter.SavePdfFromHtmlStringToFile(html.ToString(), siteFolderISS + "ACE\\" + pdfFileName, urlBase);
            string returnUrl = basePageUrl + "ACE/" + pdfFileName;
            return returnUrl;
        }
        catch
        {
            return "";
        }

    }

    [WebMethod(EnableSession = true)]
    public static string setPickupPoint(string PickupPoint, string ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; };
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResServiceRecord> resServiceList = ResData.ResService;
        ResServiceRecord resService = resServiceList.Find(f => f.RecID == Conversion.getInt32OrNull(ServiceID));
        if (resService != null && Conversion.getInt32OrNull(PickupPoint).HasValue)
        {
            Location loc = new Locations().getLocation(UserData.Market, Conversion.getInt32OrNull(PickupPoint).Value, ref errorMsg);
            if (loc != null)
            {
                resService.Pickup = Conversion.getInt32OrNull(PickupPoint);
                resService.PickupName = loc.Name;
                resService.PickupNameL = loc.NameL;
                if (new Reservation().changePickup(UserData, ResData.ResMain.ResNo, resService.RecID, resService.Pickup, ref errorMsg))
                    HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string setVehicleCategory(string VehicleCatID, string ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; };
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResServiceRecord> resServiceList = ResData.ResService;
        ResServiceRecord resService = resServiceList.Find(f => f.RecID == Conversion.getInt32OrNull(ServiceID));
        resService.VehicleCatID = Conversion.getInt32OrNull(VehicleCatID);

        List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);

        if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
        {
            HttpContext.Current.Session["ResData"] = ResData;
            string Msg = string.Empty;
            foreach (ReservastionSaveErrorRecord r in returnData)
            {
                Msg += r.Message + "<br />";
            }

            return string.Empty;
        }
        else
        {
            string Msg = string.Empty;
            foreach (ReservastionSaveErrorRecord r in returnData)
            {
                if (!r.ControlOK)
                    Msg += r.Message + "\n";
            }

            return Msg;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string setVehicleUnit(string VehicleUnit, string ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; };
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResServiceRecord> resServiceList = ResData.ResService;
        ResServiceRecord resService = resServiceList.Find(f => f.RecID == Conversion.getInt32OrNull(ServiceID));
        resService.VehicleUnit = Conversion.getInt16OrNull(VehicleUnit);

        List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);

        if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
        {
            HttpContext.Current.Session["ResData"] = ResData;
            string Msg = string.Empty;
            foreach (ReservastionSaveErrorRecord r in returnData)
            {
                Msg += r.Message + "<br />";
            }

            return string.Empty;
        }
        else
        {
            string Msg = string.Empty;
            foreach (ReservastionSaveErrorRecord r in returnData)
            {
                if (!r.ControlOK)
                    Msg += r.Message + "\n";
            }

            return Msg;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string reservationIsBusy()
    {
        //return string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; };
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        //bool? _resIsBusy = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "reservationIsBusyControl"));
        //if (!_resIsBusy.HasValue || (_resIsBusy.HasValue && _resIsBusy.Value))
        //{
        bool resIsBusy = new Reservation().reservationIsBusy(ResData.ResMain.ResNo);
        if (resIsBusy)
            return HttpContext.GetGlobalResourceObject("LibraryResource", "msgReservationIsBusy").ToString();
        else
            return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string setLock()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; };
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        new Reservation().setLock(UserData, ResData.ResMain.ResNo, ref errorMsg);

        return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string unLockReservation()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; };
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        new Reservation().unLockReservation(UserData, ResData.ResMain.ResNo, ref errorMsg);

        return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string saveResCustVisa(int? custNo, string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResCustRecord> resCust = ResData.ResCust;
        List<ResCustVisaRecord> resCustVisaList = ResData.ResCustVisa;
        ResCustVisaRecord resCustVisa = resCustVisaList.Find(f => f.CustNo == custNo);
        if (resCustVisa == null)
        {
            resCustVisa = new ResCustVisaRecord();
            resCustVisa.CustNo = custNo;
        }
        string resCustVisajSonValue = data.Replace('|', '"').Replace('<', '{').Replace('>', '}');

        try
        {
            ResCustVisaRecord resCustVisaConverted = new CustomerInfos().jSonToResCustVisa(UserData, Newtonsoft.Json.JsonConvert.DeserializeObject(resCustVisajSonValue), resCustVisa, ref errorMsg);
            if (new Reservation().SaveResCustVisaRecord(UserData, ref ResData, ref resCustVisaConverted, custNo.HasValue ? custNo.Value : -1, ref errorMsg))
            {
                HttpContext.Current.Session["ResData"] = ResData;
                return "";
            }
            else
                return "No";
        }
        catch (Exception Ex)
        {
            string error = Ex.Message;
            return "No"; // Ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getCancelReason()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retVal = string.Empty;
        List<CanReasonRecord> cancelReason = new Reservation().getCanReason(UserData.Market, ref errorMsg);
        List<CanReasonRecord> _cancelReason = new List<CanReasonRecord>();
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            _cancelReason = (from q in cancelReason
                             where q.ValidForB2B && Equals(q.Penalized, "Y")
                             select q).ToList<CanReasonRecord>();
        else
            _cancelReason = (from q in cancelReason
                             where q.ValidForB2B
                             select q).ToList<CanReasonRecord>();
        if (cancelReason == null || cancelReason.Count < 1)
            return string.Empty;
        retVal = Newtonsoft.Json.JsonConvert.SerializeObject(_cancelReason);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string AddingCustomersServices(List<int?> Custs)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        bool? _showCancelService = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "showCancelService"));
        bool showCancelService = _showCancelService.HasValue ? _showCancelService.Value : true;

        bool localName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();

        var custQuery = from q1 in ResData.ResCust
                        join q2 in Custs on q1.CustNo equals q2
                        group q1 by new { q1.CustNo, q1.TitleStr, q1.Name, q1.Surname } into g
                        select new { g.Key.CustNo, g.Key.TitleStr, g.Key.Surname, g.Key.Name };
        foreach (var custRow in custQuery)
        {
            sb.Append("<br />");
            sb.AppendFormat("<h3>{0}. {1} {2}</h3>", custRow.TitleStr, custRow.Surname, custRow.Name);
            sb.Append("<br />");
            sb.AppendFormat("<b>{0}</b><br />", HttpContext.GetGlobalResourceObject("ResView", "lblMainService"));
            List<TvBo.ResServiceRecord> serList = (from q1 in ResData.ResService
                                                   join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                   where q2.CustNo == custRow.CustNo
                                                   select q1).ToList<TvBo.ResServiceRecord>();
            int serviceNo = 0;
            foreach (TvBo.ResServiceRecord row in serList.OrderBy(o => o.BegDate))
            {
                bool showService = true;
                if (!showCancelService && row.StatSer == 2)
                    showService = false;

                string ServiceDesc = string.Empty;
                switch (row.ServiceType)
                {
                    case "HOTEL":
                        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Magellan))
                            ServiceDesc += (localName ? row.RoomNameL : row.RoomName) + "," + row.Accom + " " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        else
                            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Anex))
                            ServiceDesc += (localName ? row.RoomNameL : row.RoomName) + ",(" + row.AccomName + "), " + row.Board + " , " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        else
                            ServiceDesc += (localName ? row.RoomNameL : row.RoomName) + "," + row.Board + " , " + row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "FLIGHT":
                        ServiceDesc += row.BegDate.Value.ToShortDateString();
                        break;
                    case "TRANSPORT":
                        ServiceDesc += row.BegDate.Value.ToShortDateString();
                        break;
                    case "TRANSFER":
                        TransferRecord trf = new Transfers().getTransfer(UserData.Market, row.Service, ref errorMsg);
                        if (trf != null && string.Equals(trf.Direction, "R"))
                            ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        else
                            ServiceDesc += row.BegDate.Value.ToShortDateString();
                        break;
                    case "RENTING":
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "EXCURSION":
                        ServiceDesc += row.BegDate.Value.ToShortDateString();
                        break;
                    case "INSURANCE":
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "VISA":
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    case "HANDFEE":
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        break;
                    default:
                        ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString();
                        if (!(new Reservation().showAdditionalService(UserData, ResData.ResMain, row.ServiceType, row.Service, ref errorMsg)))
                            showService = false;
                        break;
                }
                ServiceDesc = (localName ? row.ServiceNameL : row.ServiceName) + " / " + ServiceDesc + "";
                if (showService)
                {
                    ++serviceNo;
                    sb.AppendFormat("{1}. {0} <br />", ServiceDesc, serviceNo);
                }
            }
            sb.AppendFormat("<b>{0}</b><br />", HttpContext.GetGlobalResourceObject("ResView", "lblGridResServiceExt"));
            List<TvBo.ResServiceExtRecord> serExtList = (from q1 in ResData.ResServiceExt
                                                         join q2 in ResData.ResConExt on q1.RecID equals q2.ServiceID
                                                         where q2.CustNo == custRow.CustNo
                                                         select q1).ToList<TvBo.ResServiceExtRecord>();
            serviceNo = 0;
            List<ServiceExtMarOpt> showExtraService = new Reservation().getServiceExtMarOpt(UserData, UserData.Market, ref errorMsg);
            foreach (TvBo.ResServiceExtRecord row in serExtList.OrderBy(o => o.BegDate))
            {
                ServiceExtMarOpt showing = showExtraService.Find(f => f.ServiceCode == row.ExtService && f.ShowInResDetB2B.Value == false);
                if (showing == null)
                {
                    string description = string.Empty;
                    description += row.BegDate.HasValue ? row.BegDate.Value.ToShortDateString() : "";
                    description += row.EndDate.HasValue ? (description.Length > 0 ? " - " : "") + row.EndDate.Value.ToShortDateString() : "";

                    ++serviceNo;
                    sb.AppendFormat("{1}. {0} <br />", (localName ? row.ExtServiceNameL : row.ExtServiceName) + (description.Length > 0 ? " (" + description + ")" : "&nbsp"), serviceNo);
                }
            }
        }

        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string clearOption()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        if (new Reservation().clearOptionDate(UserData, ResData.ResMain.ResNo, ref errorMsg))
            return "";
        else
            return errorMsg;
    }

    [WebMethod(EnableSession = true)]
    public static string setResStatus()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResMainRecord resMain = ResData.ResMain;
        resMain.ResStat = 0;
        resMain.Complete = 1;

        List<ReservastionSaveErrorRecord> returnData = new Reservation().SetConfirmReservation(UserData, ref ResData);
        if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
        {
            HttpContext.Current.Session["ResData"] = ResData;
            return "";
        }
        else
        {
            string Msg = string.Empty;
            foreach (ReservastionSaveErrorRecord row in returnData)
            {
                if (!row.ControlOK)
                    Msg += row.Message + "\n";
            }
            return Msg;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object saveSpecialCode(string Code1, string Code2, string Code3, string Code4)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;

        ResMainRecord resMain = ResData.ResMain;
        resMain.Code1 = Code1;
        resMain.Code2 = Code2;
        resMain.Code3 = Code3;
        resMain.Code4 = Code4;

        if (new Reservation().SaveResMainSpecialCodes(UserData, resMain, ref errorMsg))
        {
            return new
            {
                Saved = true,
                Message = ""
            };
        }
        else
        {
            return new
            {
                Saved = false,
                Message = errorMsg
            };
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object sendDocumentEmail(string DocUrl, string DocType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        List<DocumentEmailSender> senderOptionList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentEmailSender>>(Conversion.getStrOrNull(new Common().getFormConfigValue("Report", "DocEmailSenderOptions")));

        if (senderOptionList == null)
        {
            return false;
        }

        string errorMsg = string.Empty;

        try
        {
            string fromEmails = string.Empty;

            List<DocumentEmailSender> senderOptions = senderOptionList.Where(w => w.Enabled == true).OrderBy(o => (o.Priority.HasValue ? o.Priority : 99)).ToList();
            foreach (DocumentEmailSender row in senderOptions)
            {
                if (string.Equals(row.Sender, "UserEmail") && !string.IsNullOrEmpty(UserData.EMail))
                {
                    fromEmails = UserData.EMail;
                    break;
                }
                if (string.Equals(row.Sender, "AgencyEmail") && !string.IsNullOrEmpty(UserData.AgencyRec.Email1))
                {
                    fromEmails = UserData.AgencyRec.Email1;
                    break;
                }
                if (string.Equals(row.Sender, "OfficeEmail"))
                {
                    OfficeRecord OprOffice = new Common().getOffice(UserData.Market, UserData.OprOffice, ref errorMsg);
                    if (OprOffice != null && !string.IsNullOrEmpty(OprOffice.email1))
                    {
                        fromEmails = OprOffice.email1;
                        break;
                    }
                }
                if (string.Equals(row.Sender, "OperatorEmail"))
                {
                    OperatorRecord Opr = new Common().getOperator(UserData.Operator, ref errorMsg);
                    if (Opr != null && !string.IsNullOrEmpty(Opr.Email1))
                    {
                        fromEmails = Opr.Email1;
                        break;
                    }
                }
            }

            if (string.IsNullOrEmpty(fromEmails))
            {
                return false;
            }

            ResCustRecord leader = ResData.ResCust.Find(f => string.Equals(f.Leader, "Y"));
            if (leader == null)
            {
                return false;
            }

            ResCustInfoRecord leaderAddr = ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo);
            if (leaderAddr == null || string.IsNullOrEmpty(leaderAddr.AddrHomeEmail))
            {
                return false;
            }

            string attachFileName = DocUrl.Substring(DocUrl.LastIndexOf("/") + 1);
            attachFileName = HttpContext.Current.Server.MapPath("ACE") + "\\" + attachFileName;

            if (!File.Exists(attachFileName))
            {
                return false;
            }
            byte[] attachFile = File.ReadAllBytes(attachFileName);
            string fileName = "Attach";
            AsyncMailSenderParams mailParam = new AsyncMailSenderParams();
            mailParam.SenderMail = UserData.TvParams.TvParamSystem.SMTPAccount;
            mailParam.DisplayName = fromEmails;
            mailParam.SenderPassword = UserData.TvParams.TvParamSystem.SMTPPass;

            mailParam.ToAddress = leaderAddr.AddrHomeEmail;

            string body = string.Empty;

            mailParam.Port = UserData.TvParams.TvParamSystem.SMTPPort;
            mailParam.SMTPServer = UserData.TvParams.TvParamSystem.SMTPServer;
            mailParam.EnableSSL = false;
            switch (DocType)
            {
                case "AgencyConfirmation":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnAgencyConfirmation").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnAgencyConfirmation").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "Contract":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnContract").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnContract").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "FlyTicket":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnFlightTicket").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnFlightTicket").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "Invoice":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnInvoice").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnInvoice").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "Proforma":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnProforma").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnProforma").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "Voucher":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnVoucher").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnVoucher").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "Excursion":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnExcursion").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnExcursion").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "Transfer":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnTransfer").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnTransfer").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "Insurance":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnInsurance").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnInsurance").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "TicketPriceRefound":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnTicketPriceRefound").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnTicketPriceRefound").ToString() + " ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                case "Receipt":
                    fileName = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnReceipt").ToString() + ".PDF";
                    mailParam.Subject = new UICommon().GetSubMenuGlobalResourceObject(UserData, "mnReceipt").ToString() + ", ";
                    mailParam.Subject += HttpContext.GetGlobalResourceObject("ResView", "lblResNo").ToString() + " : " + ResData.ResMain.ResNo;
                    break;
                default:
                    body = string.Empty;
                    break;
            }

            mailParam.FileBinary = attachFile;
            mailParam.FileName = fileName;
            body += leader.TitleStr + ". " + leader.Surname + " " + leader.Name;
            body += "<br/>" + mailParam.Subject;
            body += "<br/>" + UserData.AgencyName;
            mailParam.Body = body;

            new SendMail().MailSender(mailParam);
            return true;
        }
        catch
        {
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object updateCustomerData(int? SeqNo, PassportReaderData_ComboSmart PassportData)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null)
        {
            return false;
        }

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        string Passport_Type = string.Empty;
        string Passport_StateCode = string.Empty;
        string Passport_SurName = string.Empty;
        string Passport_Name = string.Empty;
        string Passport_Serie = string.Empty;
        string Passport_No = string.Empty;
        string Passport_Nationality = string.Empty;
        string Passport_DateOfBirth = string.Empty;
        string Passport_Sex = string.Empty;
        string Passport_ExpireDate = string.Empty;
        string Passport_IDNo = string.Empty;

        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        try
        {
            if (PassportData != null &&
                !string.IsNullOrEmpty(PassportData.Field1) && PassportData.Field1.Length == 44 &&
                !string.IsNullOrEmpty(PassportData.Field2) && PassportData.Field2.Length == 44)
            {

                Passport_Type = PassportData.Field1.Substring(0, 5).Split('<')[0];
                Passport_StateCode = PassportData.Field1.Substring(0, 5).Split('<')[1];
                Passport_SurName = PassportData.Field1.Substring(5).Substring(0, PassportData.Field1.Substring(6).IndexOf("<<") + 1);
                Passport_Name = PassportData.Field1.Substring(PassportData.Field1.IndexOf("<<") + 1).Trim('<');
                Passport_Serie = PassportData.Field2.Substring(0, 1);
                Passport_No = PassportData.Field2.Substring(1, 8);
                Passport_Nationality = PassportData.Field2.Substring(10, 3);
                Passport_DateOfBirth = PassportData.Field2.Substring(13, 6);
                Passport_Sex = PassportData.Field2.Substring(20, 1);
                Passport_ExpireDate = PassportData.Field2.Substring(21, 6);
                Passport_IDNo = PassportData.Field2.Substring(28, 14).Trim('<');

            }
            else
            {
                return false;
            }
        }
        catch (Exception Ex)
        {
            errorMsg = Ex.Message;
            return false;
        }
        List<ResCustRecord> resCustList = ResData.ResCust;
        ResCustRecord resCust = resCustList.Find(f => f.SeqNo == SeqNo);

        if (resCust != null)
        {
            resCust.Surname = Passport_SurName;
            resCust.Name = Passport_Name;
            resCust.PassSerie = Passport_Serie;
            resCust.PassNo = Passport_No;
            resCust.Nationality = Passport_Nationality;
            resCust.Birtday = new DateTime(Convert.ToInt32(Passport_DateOfBirth.Substring(0, 2)) > 20 ? Convert.ToInt32("19" + Passport_DateOfBirth.Substring(0, 2)) : Convert.ToInt32("20" + Passport_DateOfBirth.Substring(0, 2)), Convert.ToInt32(Passport_DateOfBirth.Substring(2, 2)), Convert.ToInt32(Passport_DateOfBirth.Substring(4, 2)));
            resCust.PassExpDate = new DateTime(Convert.ToInt32("20" + Passport_ExpireDate.Substring(0, 2)), Convert.ToInt32(Passport_ExpireDate.Substring(2, 2)), Convert.ToInt32(Passport_ExpireDate.Substring(4, 2)));
            resCust.IDNo = Passport_IDNo;

            List<ReservastionSaveErrorRecord> returnData = new Reservation().UpdateReservation(UserData, ref ResData);
            if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
            {
                HttpContext.Current.Session["ResData"] = ResData;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}

public class drawSubMenu
{
    public drawSubMenu()
    {
    }
    public Int16? retVal { get; set; }
    public string Message { get; set; }
}

