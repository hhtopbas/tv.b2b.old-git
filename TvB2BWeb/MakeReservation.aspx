﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MakeReservation.aspx.cs"
    Inherits="MakeReservation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "MakeReservation") %></title>

    <script src="Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>

    <script src="Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="Scripts/checkDate.js" type="text/javascript"></script>

    <script src="Scripts/linq.js" type="text/javascript"></script>

    <script src="Scripts/jquery.linq.js" type="text/javascript"></script>

    <script src="Scripts/Tv.Utils.js" type="text/javascript"></script>

    <link href="CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="CSS/MakeReservation.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script type="text/javascript">

        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var InvalidBirthDate = '<%= GetGlobalResourceObject("LibraryResource", "InvalidBirthDate") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
        var btnGotoResCard = '<%= GetGlobalResourceObject("LibraryResource", "btnGotoResCard") %>';
        var btnPaymentPage = '<%= GetGlobalResourceObject("LibraryResource", "btnPaymentPage") %>';
        var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
        var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';
        var WhereInvoiceTo = '<%= GetGlobalResourceObject("LibraryResource", "WhereInvoiceTo") %>';
        var lblAlreadyRes = '<%= GetGlobalResourceObject("BookTicket", "lblAlreadyRes") %>';
        var PickupPointNotSelected = '<%= GetGlobalResourceObject("MakeReservation", "PickupPointNotSelected") %>';
        var ComboSelect = '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
        var ForAgency = '<%= GetGlobalResourceObject("LibraryResource", "ForAgency") %>';
        var ForPassenger = '<%= GetGlobalResourceObject("LibraryResource", "ForPassenger") %>';
        var EnterChildOrInfantBirtDay = '<%= GetGlobalResourceObject("LibraryResource", "EnterChildOrInfantBirtDay") %>';
        var lblNoResult = '<%= GetGlobalResourceObject("BookTicket", "lblNoResult") %>';
        var viewOldResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewOldResSalePrice") %>';
        var viewNewResSalePrice = '<%= GetGlobalResourceObject("Controls", "viewNewResSalePrice") %>';
        var cancelService = '<%= GetGlobalResourceObject("LibraryResource", "cancelService") %>';

        var reservationSaved = false;

        if (typeof window.event != 'undefined')
            document.onkeydown = function () {
                if (event.srcElement.tagName.toUpperCase() != 'INPUT' && event.srcElement.tagName.toUpperCase() != 'TEXTAREA')
                    return (event.keyCode != 8);
            }
        else
            document.onkeypress = function (e) {
                if (e.target.nodeName.toUpperCase() != 'INPUT' && e.target.nodeName.toUpperCase() != 'TEXTAREA')
                    return (e.keyCode != 8);
            }


        function logout() {
            self.parent.logout();
        }


        $(document).ajaxStart(function () {
            $.blockUI({
                message: '<h1>' + lblPleaseWait + '</h1>'
            });
        }).ajaxStop(function () {
            $.unblockUI();
        });

        var _dateMask = '';
        var _dateFormat = '';

        function SetAge(Id, cinDate, _formatDate) {
            try {
                var birthDate = dateValue(_formatDate, $("#iBirthDay" + Id).val());
                if (birthDate == null) {
                    $("#iBirthDay" + Id).val('');
                    $("#iAge" + Id).val('');
                    if ($("#customRegID").val() == '0969801') {
                        $("#cCardBtn_" + Id).hide();
                    }
                    return;
                }
                var checkIN = dateValue(_formatDate, cinDate);
                if (checkIN == null) return;
                var _minBirthDate = new Date(1, 1, 1910);
                var _birthDate = birthDate;
                var _cinDate = checkIN;
                if (_birthDate < _minBirthDate) {
                    $("#iAge" + Id).val('');
                    birthDate = '';
                    if ($("#customRegID").val() == '0969801') {
                        $("#cCardBtn_" + Id).hide();
                    }
                    showMsg(InvalidBirthDate);
                    return;
                }
                var age = Age(_birthDate, _cinDate);
                $("#iAge" + Id).val(age);
                if ($("#customRegID").val() == '0969801') { //for Sunrise
                    var realAge = Age(_birthDate, new Date());
                    if (((parseInt($("#iTitle" + Id).val()) == 1 || parseInt($("#iTitle" + Id).val()) == 2) && parseInt(realAge) >= 20) ||
                        ((parseInt($("#iTitle" + Id).val()) == 3 || parseInt($("#iTitle" + Id).val()) == 4) && parseInt(realAge) >= 18))
                        $("#cCardBtn_" + Id).show();
                    else $("#cCardBtn_" + Id).hide();
                }
            }
            catch (err) {
                $("#iAge" + Id).val('');
                if ($("#customRegID").val() == '0969801') {
                    $("#cCardBtn_" + Id).hide();
                }
            }
        }

        function showDialogYesNo(msg) {
            $("#messages").html(msg);
            var _maxWidth = 880;
            var _maxHeight = 500;
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                maxWidth: _maxWidth,
                maxHeight: _maxHeight,
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        return true;
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        $(this).dialog('close');
                        return false;
                    }
                }]
            });
        }

        function showMsg(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                autoResize: true,
                resizable: true,
                resizeStop: function (event, ui) {
                    $(this).dialog({ position: 'center' });
                },
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        }

        function showDialog(msg, transfer, trfUrl) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                maxWidth: 880,
                maxHeight: 500,
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        if (transfer == true) {
                            var url = trfUrl;
                            $(location).attr('href', url);
                        }
                    }
                }]
            });
        }

        function showDialogEndGotoPaymentPage(msg, ResNo) {
            $('<div>' + msg + '</div>').dialog({
                buttons: [{
                    text: btnGotoResCard,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        self.parent.gotoResViewPage(ResNo);
                    }
                }, {
                    text: btnPaymentPage,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        self.parent.paymentPage(ResNo);
                    }
                }]
            });
        }

        function gotoResViewPageAndOpenPayment(ResNo) {
            window.close;
            self.parent.gotoResViewPageAndOpenPayment(ResNo);
        }

        function showDialogEndExit(msg, ResNo) {
            $("#messages").html('');
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                resizable: true,
                autoResize: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        window.close;
                        if (ResNo != '')
                            self.parent.gotoResViewPage(ResNo, false);
                        else self.parent.cancelMakeRes();
                    }
                }]
            });
        }

        function showMessage(msg, transfer, trfUrl) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    maxWidth: 880,
                    maxHeight: 500,
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }, {
                        text: btnCancel,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }]
                });
            });
        }

        function editResService(serviceUrl) {
            setCustomers();
            $("#dialog-resServiceEdit").dialog("open");
            $("#resServiceEdit").attr("src", serviceUrl);
            return false;
        }

        function returnEditResServices(save) {
            if (save == true) {
                $("#dialog-resServiceEdit").dialog("close");
                getResMainDiv();
            }
            else {
                $("#dialog-resServiceEdit").dialog("close");
            }
        }

        function getCustomers() {
            var cust = '';
            var hfCustCount = parseInt($("#hfCustCount").val());
            var custCount = parseInt(hfCustCount);
            var custList = [];
            for (var i = 1; i <= custCount; i++) {
                var cust = new Object();
                cust.SeqNo = $("#iSeqNo" + i).text();
                cust.Title = $("#iTitle" + i).length > 0 ? $("#iTitle" + i).val() : '';
                cust.Surname = $("#iSurname" + i).length > 0 ? $("#iSurname" + i).val() : '';
                cust.SurnameL = $("#iSurnameL" + i).length > 0 ? $("#iSurnameL" + i).val() : '';
                cust.Name = $("#iName" + i).length > 0 ? $("#iName" + i).val() : '';
                cust.NameL = $("#iNameL" + i).length > 0 ? $("#iNameL" + i).val() : '';
                cust.BirtDay = $("#iBirthDay" + i).length > 0 ? $("#iBirthDay" + i).val() : '';
                cust.Age = $("#iAge" + i).length > 0 ? $("#iAge" + i).val() : '';
                cust.IDNo = $("#iIDNo" + i).length > 0 ? $("#iIDNo" + i).val() : '';
                cust.PassSerie = $("#iPassSerie" + i).length > 0 ? $("#iPassSerie" + i).val() : '';
                cust.PassNo = $("#iPassNo" + i).length > 0 ? $("#iPassNo" + i).val() : '';
                cust.PassIssueDate = $("#iPassIssueDate" + i).length > 0 ? $("#iPassIssueDate" + i).val() : '';
                cust.PassExpDate = $("#iPassExpDate" + i).length > 0 ? $("#iPassExpDate" + i).val() : '';
                cust.PassGiven = $("#iPassGiven" + i).length > 0 ? $("#iPassGiven" + i).val() : '';
                cust.Phone = $("#iPhone" + i).length > 0 ? $("#iPhone" + i).val() : '';
                cust.Nation = $("#iNation" + i).length > 0 ? parseInt($("#iNation" + i).val()) : null;
                cust.Nationality = $("#iNationality" + i).length > 0 ? $("#iNationality" + i).val() : null;
                cust.Passport = $("#iHasPassport" + i).length > 0 ? $("#iHasPassport" + i)[0].checked : true;
                cust.Leader = $('input[name=Leader]:checked').val() == cust.SeqNo;
                cust.ResCustInfo = null;
                var addressInfo = $("#resCustInfo" + i)
                if ($("#resCustInfo" + i).length > 0) {
                    if (cust.Leader) {
                        var custInfo = new Object();
                        custInfo.CTitle = $("#CTitle" + i).length > 0 ? $("#CTitle" + i).val() : '';
                        custInfo.CName = $("#CName" + i).length > 0 ? $("#CName" + i).val() : '';
                        custInfo.CSurName = $("#CSurName" + i).length > 0 ? $("#CSurName" + i).val() : '';
                        custInfo.MobTel = ($("#MobTelCountryCode" + i).length > 0 ? $("#MobTelCountryCode" + i).text() : '') + ($("#MobTel" + i).length > 0 ? $("#MobTel" + i).val() : '');
                        custInfo.ContactAddr = 'H';
                        custInfo.InvoiceAddr = 'H';
                        custInfo.AddrHome = $("#AddrHome" + i).length > 0 ? $("#AddrHome" + i).val() : '';
                        custInfo.AddrHomeCity = $("#AddrHomeCity" + i).length > 0 ? $("#AddrHomeCity" + i).val() : '';
                        custInfo.AddrHomeCountry = $("#AddrHomeCountry" + i).length > 0 ? $("#AddrHomeCountry" + i).val() : '';

                        custInfo.AddrHomeZip = $("#AddrHomeZip" + i).length > 0 ? $("#AddrHomeZip" + i).val() : '';
                        custInfo.AddrHomeTel = ($("#AddrHomeTelCountryCode" + i).length > 0 ? $("#AddrHomeTelCountryCode" + i).text() : '') + ($("#AddrHomeTel" + i).length > 0 ? $("#AddrHomeTel" + i).val() : '');
                        custInfo.AddrHomeFax = ($("#AddrHomeFaxCountryCode" + i).length > 0 ? $("#AddrHomeFaxCountryCode" + i).text() : '') + ($("#AddrHomeFax" + i).length > 0 ? $("#AddrHomeFax" + i).val() : '');
                        custInfo.AddrHomeEmail = $("#AddrHomeEmail" + i).length > 0 ? $("#AddrHomeEmail" + i).val() : '';
                        custInfo.HomeTaxOffice = $("#HomeTaxOffice" + i).length > 0 ? $("#HomeTaxOffice" + i).val() : '';
                        custInfo.HomeTaxAccNo = $("#HomeTaxAccNo" + i).length > 0 ? $("#HomeTaxAccNo" + i).val() : '';
                        custInfo.WorkFirmName = $("#WorkFirmName" + i).length > 0 ? $("#WorkFirmName" + i).val() : '';
                        custInfo.AddrWork = $("#AddrWork" + i).length > 0 ? $("#AddrWork" + i).val() : '';
                        custInfo.AddrWorkCity = $("#AddrWorkCity" + i).length > 0 ? $("#AddrWorkCity" + i).val() : '';
                        custInfo.AddrWorkCountry = $("#AddrWorkCountry" + i).length > 0 ? $("#AddrWorkCountry" + i).val() : '';
                        custInfo.AddrWorkZip = $("#AddrWorkZip" + i).length > 0 ? $("#AddrWorkZip" + i).val() : '';
                        custInfo.AddrWorkTel = ($("#AddrWorkTelCountryCode" + i).length > 0 ? $("#AddrWorkTelCountryCode" + i).text() : '') + ($("#AddrWorkTel" + i).length > 0 ? $("#AddrWorkTel" + i).val() : '');
                        custInfo.AddrWorkFax = ($("#AddrWorkFaxCountryCode" + i).length > 0 ? $("#AddrWorkFaxCountryCode" + i).text() : '') + ($("#AddrWorkFax" + i).length > 0 ? $("#AddrWorkFax" + i).val() : '');
                        custInfo.AddrWorkEMail = $("#AddrWorkEMail" + i).length > 0 ? $("#AddrWorkEMail" + i).val() : '';
                        custInfo.WorkTaxOffice = $("#WorkTaxOffice" + i).length > 0 ? $("#WorkTaxOffice" + i).val() : '';
                        custInfo.WorkTaxAccNo = $("#WorkTaxAccNo" + i).length > 0 ? $("#WorkTaxAccNo" + i).val() : '';
                        custInfo.Bank = $("#Bank" + i).length > 0 ? $("#Bank" + i).val() : '';
                        custInfo.BankAccNo = $("#BankAccNo" + i).length > 0 ? $("#BankAccNo" + i).val() : '';
                        custInfo.BankIBAN = $("#BankIBAN" + i).length > 0 ? $("#BankIBAN" + i).val() : '';

                        cust.ResCustInfo = custInfo;
                    }
                }
                custList.push(cust);
            }
            return $.json.encode(custList);
        }

        function saveReservation(selectedPromo) {
            if ($("#divWhereInvoice").css('display') == 'block') {
                if ($("#iInvoiceTo").val() == '' || $("#iInvoiceTo").val() == undefined) {
                    showMsg(WhereInvoiceTo);
                    return;
                }
            }

            setCustomers();
            if (reservationSaved) {
                showMsg(lblAlreadyRes);
                return;
            } else {
                reservationSaved = true;
            }

            var agencyBonus = $("#agencyBonus").length > 0 ? $("#agencyBonus").val() : "";
            var userBonus = $("#userBonus").length > 0 ? $("#userBonus").val() : "";
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            data.SelectedPromo = selectedPromo;
            data.AgencyBonus = agencyBonus;
            data.UserBonus = userBonus;
            data.invoiceTo = $("#iInvoiceTo").val();
            data.saveOption = ($("#iSaveOption").attr('checked') != undefined) ? ($("#iSaveOption").attr('checked') == 'checked' ? true : false) : false;
            data.bonusID = $("#iBonusID").val();
            data.optionTime = $("#withOption").length > 0 && $("#withOption").attr("checked") == 'checked' ? '1' : '0';
            data.goAhead = $("#goAhead").val() == '1' ? true : false;
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/saveReservation",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var retVal = $.json.decode(msg.d)[0];
                    if (retVal.ControlOK == true) {
                        if (retVal.GotoPaymentPage == true)
                            showDialogEndGotoPaymentPage(retVal.Message, retVal.ResNo);
                        else if (retVal.PaymentPageUrl == "Custom")
                            gotoResViewPageAndOpenPayment(retVal.ResNo);
                        else if (retVal.GotoReservation)
                            showDialogEndExit(retVal.Message, retVal.ResNo);
                        else showDialogEndExit(retVal.Message, '');
                    }
                    else {
                        if (retVal.OtherReturnValue == '32') {
                            showMsg(retVal.Message);
                            $("#goAhead").val('1');
                            reservationSaved = false;
                        }
                        else {
                            showMsg(retVal.Message);
                            reservationSaved = false;
                            $("#goAhead").val('0');
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                    reservationSaved = false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

        }

        function selectPromomotion() {
            setCustomers();
            var url = 'Promotion.aspx';
            $("#dialog-promoSelect").dialog("open");
            $("#promoSelect").attr("src", url);
        }

        function returnSelectedPromotion(data) {
            if (data == '') {
                $("#dialog-promoSelect").dialog("close");
                saveReservation(data);
            }
            else {
                $("#dialog-promoSelect").dialog("close");
                saveReservation(data);
            }
        }

        function setCustomers() {
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/setCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    return true;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMsg(xhr.responseText);
                    }
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function saveMakeRes(selectedPromo) {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/serviceControl",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d == false) {
                        $("#messages").html(PickupPointNotSelected);
                        $("#dialog").dialog("destroy");
                        $("#dialog-message").dialog({
                            maxWidth: 500,
                            maxHeight: 300,
                            modal: true,
                            buttons: [{
                                text: btnOK,
                                click: function () {
                                    $(this).dialog('close');
                                    saveMakeResSecond(selectedPromo);
                                }
                            }, {
                                text: btnCancel,
                                click: function () {
                                    $(this).dialog('close');
                                    return false;
                                }
                            }]
                        });
                    } else {
                        saveMakeResSecond(selectedPromo);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }


        function saveMakeResSecond(selectedPromo) {
            if ($("#divWhereInvoice").css('display') == 'block') {
                if ($("#iInvoiceTo").val() == '' || $("#iInvoiceTo").val() == undefined) {
                    showMsg(WhereInvoiceTo);
                    return;
                }
            }

            setCustomers();
            var data = new Object();
            var customerCode = $("#iCustomerCode").length > 0 ? $("#iCustomerCode").val() : '';
            var dosier = $("#iDosier").length > 0 ? $("#iDosier").val() : '';
            data.Honeymoon = $("#cbHoneymoon")[0].checked;
            data.PromoCode = $("#iPromoCode").val();
            data.Customers = getCustomers();
            data.Discount = $("#iAgencyDiscount").val();
            data.ResNote = $("#iNote").length > 0 ? $("#iNote").val().replace(/\"/g, "|") : '';
            data.aceCustomerCode = customerCode;
            data.aceDosier = dosier;
            data.Code1 = $("#iCode1").length > 0 ? $("#iCode1").val() : '';
            data.Code2 = $("#iCode1").length > 0 ? $("#iCode2").val() : '';
            data.Code3 = $("#iCode1").length > 0 ? $("#iCode3").val() : '';
            data.Code4 = $("#iCode1").length > 0 ? $("#iCode4").val() : '';
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/getPromoList",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var retVal = $.json.decode(msg.d);
                    //MakeReservation.cs Anex için yapılan bölüm kaldırıldı.

                    if (retVal.retVal == "OK" && retVal.data == "|!|") {
                        saveReservation('');
                    }
                    else {
                        if (retVal.retVal == "OK")
                            selectPromomotion();
                        else showMsg(retVal.retVal);
                    }

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMsg(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function cancelMakeRes() {
            window.close;
            self.parent.cancelMakeRes();
        }

        function clickMsgAccept() {
            if ($('#msgAccept').attr('checked'))
                $("#btnSave").show();
            else $("#btnSave").hide();
        }

        function getHandicaps() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/getHandicaps",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divHandicap").html('');
                    $("#divHandicap").html(msg.d);
                    if ($('#msgAccept').attr('checked')) {
                        $("#btnSave").show();
                    }
                    else {
                        $("#btnSave").hide();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getResServiceDiv() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/getResServiceDiv",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResService").html('');
                    $("#divResService").html(msg.d);
                    getHandicaps();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function homeCountryChanged(idNo) {
            var country = $("#AddrHomeCountry" + idNo).val();
            var countryList = $("#countryList").val();

            var queryResult = $.Enumerable.From($.json.decode(countryList))
                .Where(function (x) { return x.CountryName == country })
                .Select("$.CountryPhoneCode")
                .First();
            if (queryResult != '') {
                $("#MobTelCountryCode" + idNo).text('+' + queryResult);
                $("#AddrHomeTelCountryCode" + idNo).text('+' + queryResult);
                $("#AddrHomeFaxCountryCode" + idNo).text('+' + queryResult);
            }
        }

        function leaderChange(idNo) {
            var leader = $("#iLeader" + idNo);
            var hfCustCount = parseInt($("#hfCustCount").val());
            var custCount = parseInt(hfCustCount);
            var custList = [];
            for (var i = 1; i <= custCount; i++) {
                var addressInfo = $("#resCustInfo" + i);
                if (i == idNo)
                    addressInfo.show();
                else addressInfo.hide();
            }
        }

        function onSurnameExit(elmID) {
            var surnameList = $(".nameSurnameCss");
            var elmValue = $("#" + elmID).val();
            var custNo = elmID.replace('iSurname', '');
            surnameList.each(function (index) {
                $("#iTitle" + custNo + " option:selected").text();
                var custNoIdx = $(this).attr('custNo');
                if (custNoIdx == custNo) {
                    var newValue = $("#iTitle" + $(this).attr('custNo') + " option:selected").text() + ". ";
                    newValue += $("#" + elmID).val() + " ";
                    newValue += $("#iName" + custNo).val();
                    $(this).html(newValue);
                }
            });
        }

        function onNameExit(elmID) {
            var surnameList = $(".nameSurnameCss");
            var elmValue = $(elmID).val();
            var custNo = elmID.replace('iName', '');
            surnameList.each(function (index) {
                $("#iTitle" + custNo + " option:selected").text();
                var custNoIdx = $(this).attr('custNo');
                if (custNoIdx == custNo) {
                    var newValue = $("#iTitle" + $(this).attr('custNo') + " option:selected").text() + ". ";
                    newValue += $("#iSurname" + custNo).val() + " ";
                    newValue += $("#" + elmID).val();
                    $(this).html(newValue);
                }
            });
        }


        function getResCustDiv() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/getResCustdiv",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResCust").html('');
                    $("#divResCust").html(msg.d);
                    var _dateMask = $("#dateMask").val();
                    if ($(".formatDate").length > 0) {
                        $(".formatDate").mask(_dateMask);
                    }
                    if ($("#customRegID").val() == '0969801') {
                        $('img[name="cCardBtn"]').hide();
                    }
                    if ($(".mobPhone").length > 0 && $("#mobPhoneMask").val() != '') {
                        $(".mobPhone").mask($("#mobPhoneMask").val());
                    }
                    if ($(".phone").length > 0 && $("#phoneMask").val() != '') {
                        $(".phone").mask($("#phoneMask").val());
                    }

                    getResServiceDiv();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function promotionSection(promotionStatus, promoCode) {
            if (promotionStatus != "1") {
                $("#divPromotion").hide();
                $("#iPromoCode").val(promoCode);
            }
            else {
                $("#divPromotion").show();
                $("#iPromoCode").val(promoCode);
            }
        }

        function resNoteOptChange() {
            $("#iNote").val('');
            var valueHtml = '';
            $("input[type='checkbox'][name='resNoteOpt']").each(function () {
                var id = $(this).attr('id');
                var value = $(this).attr('value');
                if ($(this).attr('checked') == 'checked') {
                    valueHtml += (valueHtml.length > 0 ? ', ' : '') + value;
                }
            });
            $("#iNote").val(valueHtml);
        }

        function getResMainDiv() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/getResMainDiv",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d')) {
                        var retVal = msg.d;
                        $("#divResMain").html('');
                        $("#divResMain").html(retVal.data);
                        $("#customRegID").val(retVal.CustomRegID);

                        if (retVal.mobPhoneMask != '') {
                            $("#mobPhoneMask").val(retVal.mobPhoneMask);
                        }
                        if (retVal.phoneMask != '') {
                            $("#phoneMask").val(retVal.phoneMask);
                        }

                        getResCustDiv();

                        promotionSection(retVal.statusPromotion, retVal.PromoCode);

                        if (retVal.divACE) { $("#divACE").show(); }
                        if (retVal.divDiscountAgencyCom) { $("#divDiscountAgencyCom").show(); }
                        if (retVal.divWhereInvoice) { $("#divWhereInvoice").show(); }
                        $("#divSaveOption").html('');
                        $("#divSaveOption").html(retVal.divSaveOptionDiv);
                        if (retVal.divSaveOption) { $("#divSaveOption").show(); }
                        if (retVal.divResNote) {
                            $("#divNote").show();
                            $("#iNote").val(retVal.ResNote);
                        }
                        if (retVal.divBonusID) { $("#bonusID").show(); }
                        if (retVal.ResNoteOpt != null && retVal.ResNoteOpt != '') {
                            $("#divResNoteOpt").show();
                            $("#divResNoteOpt").html(retVal.ResNoteOpt)
                        }
                        if (retVal.showSpecialCode == true) {
                            $("#divSpecialCodes").show();
                        } else {
                            $("#divSpecialCodes").hide();
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnCustInfoEdit(data, source) {
            if (source == "save") {
                $.ajax({
                    type: "POST",
                    url: "MakeReservation.aspx/setResCustInfo",
                    data: '{"data":"' + data + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (source == "save")
                            getResCustDiv();
                        $("#dialog-resCustomerAddress").dialog("close");
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showMsg(xhr.responseText);
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }
            else {
                $("#dialog-resCustomerAddress").dialog("close");
            }
        }

        function showCustAddress(CustNo) {
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/setResCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var custInfoUrl = 'CustomerAddress.aspx?CustNo=';

                    $("#dialog-resCustomerAddress").dialog("open");
                    $("#resCustomerAddress").attr("src", custInfoUrl + CustNo);
                    return false;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnCustDetailEdit(dataS, source) {
            if (source == "save") {
                var data = new Object();
                data.data = dataS;
                $.ajax({
                    type: "POST",
                    url: "MakeReservation.aspx/setResCustomer",
                    data: $.json.encode(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (source == "save")
                            getResCustDiv();
                        $("#dialog-resCustOtherInfo").dialog("close");
                    },
                    error: function (xhr, msg, e) {
                        if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                            showMsg(xhr.responseText);
                    },
                    statusCode: {
                        408: function () {
                            logout();
                        }
                    }
                });
            }
            else {
                $("#dialog-resCustOtherInfo").dialog("close");
            }
        }

        function showResCustInfo(CustNo) {
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/setResCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var custInfoUrl = 'CustomerDetail.aspx?CustNo=';

                    $("#dialog-resCustOtherInfo").dialog("open");
                    $("#resCustOtherInfo").attr("src", custInfoUrl + CustNo);
                    return false;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }


        function returnSsrc(cancel, refresh) {
            if (cancel == true) {
                $("#dialog-Ssrc").dialog("close");
            }
            else {
                $("#dialog-Ssrc").dialog("close");
                if (refresh) {
                    getResMainDiv(); promotionDiv();
                }
                else {
                    showMsg(msg.d);
                }
            }
        }

        function showSSRC(CustNo) {
            setCustomers();
            var serviceUrl = 'Controls/Ssrc.aspx?CustNo=' + CustNo + '&Save=false';
            $("#dialog-Ssrc").dialog("open");
            $("#Ssrc").attr("src", serviceUrl);
        }

        function deleteCust(CustNo) {

        }

        function addCCard() {
            var custNo = $("#cCardCustNo").val();
            var phone = $("#iCCardPhone").val();
            var name = $("#iName" + custNo).length > 0 ? $("#iName" + custNo).val() : ''
            var surName = $("#iSurname" + custNo).length > 0 ? $("#iSurname" + custNo).val() : ''
            var data = new Object();
            data.custNo = custNo;
            data.name = name;
            data.surName = surName;
            data.phone = phone;
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/sendCCardApplyInfo",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#dialog-cCardApply").dialog("close");
                    $("#cCardCustNo").val('');
                    if (msg.d == "OK") {
                        showMsg("Спасибо. Заявка отправлена в Банк. Банк свяжется с клиентом для дальнейших действий");
                    } else if (msg.d != "") {
                        showMsg("Возникли проблемы при отправке данных в банк. Пожалуйста, обратитесь к оператору");
                    } else {
                    }
                },
                error: function (xhr, msg, e) {
                    $("#cCardCustNo").val('');
                    $("#dialog-cCardApply").dialog("close");
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        $("#cCardCustNo").val('');
                        $("#dialog-cCardApply").dialog("close");
                        logout();
                    }
                }
            });
        }

        function showCCard(custNo) {
            $("#cCardCustNo").val(custNo);
            $("#dialog").dialog("destroy");
            $("#dialog-cCardApply").dialog({
                autoResize: true,
                resizable: true,
                modal: true
            });
        }

        function showExtServiceDetail(idNo) {
            $("#extraservice" + idNo).hide();
            $("#extraserviceDetail" + idNo).show();
        }

        function hideExtServiceDetail(idNo) {
            $("#extraserviceDetail" + idNo).hide();
            $("#extraservice" + idNo).show();
        }

        function drawButtons() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/drawButtons",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divServiceMenu").html('');
                    $("#divServiceMenu").html(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnAddResServices(reLoad) {
            $("#dialog-resServiceAdd").dialog("close");
            if (reLoad == true)
                getResMainDiv();
            drawButtons();
        }

        function checkUsers(custs) {
            var retVal = true;
            var CustList = $.json.decode(getCustomers());
            try {
                var i = 0;
                for (i = 0; i < CustList.length; i++) {
                    if (CustList[i].Title > 5 && CustList[i].BirtDay == '') {
                        retVal = false;
                    }
                }
            }
            catch (e) {
                retVal = false;
            }
            return retVal;
        }

        function showAddResService(serviceUrl) {
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/setCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var chkUsers = checkUsers(getCustomers());
                    if (chkUsers) {
                        $("#dialog-resServiceAdd").dialog("open");
                        if (serviceUrl.toString().indexOf('?') > -1)
                            $("#resServiceAdd").attr("src", serviceUrl + '&recordType=temp');
                        else $("#resServiceAdd").attr("src", serviceUrl + '?recordType=temp');
                    } else {
                        showMsg(EnterChildOrInfantBirtDay);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function returnAddResServicesExt(save) {
            if (save == true) {
                $("#dialog-resServiceExtAdd").dialog("close");
                getResMainDiv();
            }
            else {
                $("#dialog-resServiceExtAdd").dialog("close");
            }
        }

        function searchSelectCust(CustNo) {
            $("#dialog-searchCust").dialog("close");
            var Custs = $.json.decode(getCustomers());
            var data = new Object();
            data.CustNo = CustNo;
            data.Custs = Custs;
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/copyCustomer",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        getResMainDiv();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function searchCustListShow(_html) {
            $(function () {
                $("#searchCust").html('');
                $("#searchCust").html(_html);
                $("#dialog").dialog("destroy");
                $("#dialog-searchCust").dialog({
                    modal: true,
                    width: 650,
                    height: 450,
                    resizable: true,
                    autoResize: true
                });
            });
        }

        function searchCust(i) {
            $("#dialog").dialog("destroy");
            $("#searchCust").attr("src", "CustomerSearch.aspx?refNo=" + i + "&SeqNo=" + $("#iSeqNo" + i).text());
            $("#dialog-searchCust").dialog({
                modal: true,
                width: 750,
                height: 500
            });
        }

        function searchCustGetList(i) {
            var Surname = $("#iSurname" + i).val();
            var Name = $("#iName" + i).val();
            var BirthDate = $("#iBirthDay" + i).val();
            var SeqNo = $("#iSeqNo" + i).text();

            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/searchCustomer",
                data: '{"refNo":"' + i + '","SeqNo":"' + SeqNo + '","Surname":"' + Surname + '","Name":"' + Name + '","BirthDate":"' + BirthDate + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        if (msg.d.toString().substring(0, 4) == "Err:") {
                            var err = msg.d.toString().substring(4);
                            showMsg(err.length > 0 ? err : lblNoResult);
                        }
                        else {
                            searchCustListShow(msg.d);
                        }
                    } else {
                        showMsg(lblNoResult);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function showAddResServiceExt(serviceUrl) {
            setCustomers();
            var data = new Object();
            data.data = $.json.decode(getCustomers());
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/setCustomers",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var chkUsers = checkUsers(getCustomers());
                    if (chkUsers) {
                        $("#dialog-resServiceExtAdd").dialog("open");
                        $("#resServiceExtAdd").attr("src", serviceUrl + '?recordType=temp');
                    } else {
                        showMsg(EnterChildOrInfantBirtDay);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function promotionDiv() {
            $.ajax({
                async: false,
                type: "POST",
                url: "MakeReservation.aspx/getPromotionDiv",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d.split(';')[0] == "0") {
                        $("#divPromotion").hide();
                    }
                    else {
                        $("#divPromotion").show();
                        if (msg.d.split(';')[1] == "1") {
                            $("#divPromotionHoneymoon").show();
                        }
                        else {
                            $("#divPromotionHoneymoon").hide();
                        }
                        if (msg.d.split(';')[2] == "1") {
                            $("#divPromotionPromotionCode").show();
                        }
                        else {
                            $("#divPromotionPromotionCode").hide();
                        }
                    }

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function removeResService(recID) {
            setCustomers();
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/removeService",
                data: '{"RecID":' + recID + ',"forPrice":false}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        getResMainDiv();
                        promotionDiv();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function removeResServiceDialog(_html, recID) {
            $("#messages").html(_html);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                maxWidth: 880,
                maxHeight: 500,
                modal: true,
                buttons: [{
                    text: btnYes,
                    click: function () {
                        $(this).dialog('close');
                        removeResService(recID);
                        return true;
                    }
                }, {
                    text: btnNo,
                    click: function () {
                        $(this).dialog('close');
                        return false;
                    }
                }]
            });
        }

        function removeResServiceMsg(recID) {
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/removeService",
                data: '{"RecID":' + recID + ',"forPrice":true}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var data = $.json.decode(msg.d);
                    var _html = '';
                    _html += viewOldResSalePrice;
                    _html += '<br />';
                    _html += '<strong>' + data.OldPrice + '</strong>';
                    _html += '<br />';
                    _html += viewNewResSalePrice;
                    _html += '<br />'
                    _html += '<strong>' + data.NewPrice + '</strong>';
                    _html += '<br />';
                    _html += cancelService;

                    removeResServiceDialog(_html, recID);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function removeResServiceExt(recID) {
            setCustomers();
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/removeServiceExt",
                data: '{"RecID":' + recID + ',"forPrice":false}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        getResMainDiv();
                        promotionDiv();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function removeResServiceExtDialog(_html, recID) {
            $("#messages").html(_html);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                maxWidth: 880,
                maxHeight: 500,
                modal: true,
                buttons: [{
                    text: btnYes,
                    click: function () {
                        $(this).dialog('close');
                        removeResServiceExt(recID);
                        return true;
                    }
                }, {
                    text: btnNo,
                    click: function () {
                        $(this).dialog('close');
                        return false;
                    }
                }]
            });
        }

        function removeResServiceExtMsg(recID) {
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/removeServiceExt",
                data: '{"RecID":' + recID + ',"forPrice":true}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var data = $.json.decode(msg.d);
                    var _html = '';
                    _html += viewOldResSalePrice;
                    _html += '<br />';
                    _html += '<strong>' + data.OldPrice + '</strong>';
                    _html += '<br />';
                    _html += viewNewResSalePrice;
                    _html += '<br />'
                    _html += '<strong>' + data.NewPrice + '</strong>';
                    _html += '<br />';
                    _html += cancelService;

                    removeResServiceExtDialog(_html, recID);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changePickup(pickupPoint, serviceID) {
            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/setPickupPoint",
                data: '{"PickupPoint":"' + pickupPoint + '","ServiceID":"' + serviceID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == '') { }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeFlight() {
            setCustomers();
            var serviceUrl = 'ChangePLFlight.aspx';
            $("#dialog-changeFlight").dialog("open");
            $("#changeFlight").attr("src", serviceUrl);
        }

        function retutnChangeFlight() {
            $("#dialog-changeFlight").dialog("close");
            getResMainDiv();
            promotionDiv();
        }

        function showPaymentDetail() {
            if ($("#tablePaymentDiv").length > 0) {
                if ($("#tablePayment").css('display') == 'none') {
                    $("#tablePayment").show();
                    $("#passAmountDiv").hide();
                    $("#onOffImg").attr('src', 'Images/infoClose.gif');
                }
                else {
                    $("#tablePayment").hide();
                    $("#passAmountDiv").show();
                    $("#onOffImg").attr('src', 'Images/infoOpen.gif');
                }
            }
        }

        function dialogInitialize() {
            $("#dialog-Ssrc").dialog({ autoOpen: false, modal: true, width: 640, height: 550, resizable: true, autoResize: true });
            $("#dialog-resServiceEdit").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
            $("#dialog-promoSelect").dialog({ autoOpen: false, modal: true, width: 820, height: 440, resizable: true, autoResize: true });
            $("#dialog-resCustOtherInfo").dialog({ autoOpen: false, modal: true, width: 440, height: 470, resizable: true, autoResize: true });
            $("#dialog-resCustomerAddress").dialog({ autoOpen: false, modal: true, width: 600, height: 535, resizable: false, autoResize: true });
            $("#dialog-resServiceAdd").dialog({ autoOpen: false, modal: true, width: 675, height: 600, resizable: true, autoResize: true });
            $("#dialog-resServiceExtAdd").dialog({ autoOpen: false, modal: true, width: 870, height: 525, resizable: true, autoResize: true });
            $("#dialog-changeFlight").dialog({ autoOpen: false, modal: true, width: 900, height: 525, resizable: true, autoResize: true });
        }

        function onClickOptionTime() {
            if ($("#withOption").length > 0) {
                if ($("#withOption").attr("checked") == "checked") {
                    $("#withOptionMsg").show();
                } else {
                    $("#withOptionMsg").hide();
                }
            }
        }

        function arrivalInformationElmChange() {
            if ($("#arrivalInformation").show()) {

                if (($("#airPort").val() != undefined && $("#airPort").val() != '' &&
                    $("#flightNo").val() != '' &&
                    $("#arrivalTimeHH").val() != '' &&
                    $("#arrivalTimeMM").val() != '') ||
                    $("#noTransfer").attr("checked")) {
                    $("#btnSave").show();
                } else {
                    $("#btnSave").hide();
                }

            }
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            if ($.query.get('forb2c') == '1') {
                self.parent.b2cRes();
            }
            var copyPasteAvailable =<%=CopyPasteIsActive%>;
            if (!copyPasteAvailable) {
                $(document).on("cut copy paste", "input", function (e) {
                    e.preventDefault();
                });
                $(document).on("contextmenu", "input", function (e) { e.preventDefault(); });
            }

            $("#onlyTransfer").val($.query.get('onlytransfer') == '1' ? '1' : '0');

            $("#iInvoiceTo").append("<option value='' >" + ComboSelect + "</option>");
            $("#iInvoiceTo").append("<option value='0' >" + ForAgency + "</option>");
            $("#iInvoiceTo").append("<option value='1' >" + ForPassenger + "</option>");

            $.ajax({
                type: "POST",
                url: "MakeReservation.aspx/userBlackList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == '') {
                        getResMainDiv();
                        promotionDiv();
                    }
                    else { showMsg(msg.d); }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMsg(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            dialogInitialize();
            $('#iBonusID').blur(function () {
                var value = $('#iBonusID').val();
                if (value.length != 0) {
                    value = value.replace(/[^0-9]+/g, '');
                    if (value.length != 9) {
                        alert('Бонус ID должно быть 9 символов');
                        $('#iBonusID').val(value);
                        $('#iBonusID').focus();
                    }
                }
            });
        });

    </script>

</head>
<body>
    <form id="MakeReservationForm" runat="server">
        <input id="customRegID" type="hidden" />
        <input id="onlyTransfer" type="hidden" />
        <input id="phoneMask" type="hidden" />
        <input id="mobPhoneMask" type="hidden" />
        <input id="goAhead" type="hidden" value="0" />
        <div style="width: 900px" class="ui-widget">
            <div id="divResMain" class="ui-helper-clearfix ui-widget">
            </div>
            <div style="text-align: center; clear: both;">
                <div id="divResCust" class="resCustGridCss">
                </div>
            </div>
            <br />
            <div style="text-align: center;">
                <div id="divResService">
                </div>
            </div>
            <div id="divSpecialCodes" class="divPromotionCss ui-helper-hidden">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 25%;">
                            <strong>BR. FISKALNOG RACUNA </strong>
                        </td>
                        <td style="width: 75%;">
                            <input id="iCode1" type="text" maxlength="10" style="width: 75px; border: solid 1px #333;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            <strong>DATUM IZDAVANJA FISK. RAC.</strong>
                        </td>
                        <td style="width: 75%;">
                            <input id="iCode2" type="text" maxlength="10" style="width: 75px; border: solid 1px #333;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            <strong>UPLACEN IZNOS U DINARIMA</strong>
                        </td>
                        <td style="width: 75%;">
                            <input id="iCode3" type="text" maxlength="10" style="width: 75px; border: solid 1px #333;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">
                            <strong>NAPOMENA</strong>
                        </td>
                        <td style="width: 75%;">
                            <input id="iCode4" type="text" maxlength="10" style="width: 75px; border: solid 1px #333;" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divPromotion" class="divPromotionCss" style="display: none;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td id="divPromotionHoneymoon" style="width: 50%;">
                            <strong>
                                <%= GetGlobalResourceObject("MakeReservation", "lblHoneymoon") %>: </strong>
                            <input id="cbHoneymoon" type="checkbox" />
                        </td>
                        <td id="divPromotionPromotionCode" style="width: 50%;">
                            <strong>
                                <%= GetGlobalResourceObject("MakeReservation", "lblPromotionCode") %>: </strong>
                            <input id="iPromoCode" type="text" style="width: 75px; border: solid 1px #333;" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="text-align: center;">
                <div id="divHandicap">
                </div>
            </div>
            <div style="text-align: center;">
                <div id="divNote" style="display: none; text-align: left;">
                    <strong>
                        <%= GetGlobalResourceObject("BookTicket", "saveDivNote") %>: </strong>
                    <br />
                    <input id="iNote" type="text" style="width: 95%; height: 30px;" />
                </div>
                <div id="divResNoteOpt" style="display: none; text-align: left;">
                </div>
                <div id="divACE" style="display: none;">
                    <div style="width: 100%; height: 5px;">
                        &nbsp;
                    </div>
                    <div style="float: left; width: 150px; text-align: right; padding-left: 100px; margin-top: 3px;">
                        <strong>
                            <%= GetGlobalResourceObject("BookTicket", "saveDivCustomerCode")%>: </strong>
                    </div>
                    <div style="float: left; width: 150px; text-align: left;">
                        <input id="iCustomerCode" type="text" />
                    </div>
                    <div style="float: left; width: 150px; text-align: right; margin-top: 3px;">
                        <strong>
                            <%= GetGlobalResourceObject("BookTicket", "saveDivDosier")%>: </strong>
                    </div>
                    <div style="float: left; width: 150px; text-align: left;">
                        <input id="iDosier" type="text" />
                    </div>
                </div>
                <div id="divDiscountAgencyCom" style="clear: both; display: none;">
                    <div style="width: 100%; height: 5px;">
                        &nbsp;
                    </div>
                    <div style="float: left; width: 150px; text-align: right; padding-left: 100px; margin-top: 3px;">
                        <strong><%= GetGlobalResourceObject("MakeReservation", "agencyDiscount")%> (%): </strong>
                    </div>
                    <div style="float: left; width: 150px; text-align: left;">
                        <input id="iAgencyDiscount" type="text" />
                    </div>
                </div>
                <div id="divWhereInvoice" style="clear: both; display: none;">
                    <strong>
                        <%= GetGlobalResourceObject("MakeReservation", "whereInvoice")%>:</strong>
                    <select id="iInvoiceTo" style="width: 200px;">
                    </select>
                </div>
                <div id="divSaveOption" style="clear: both; display: none;">
                </div>
            </div>
            <div id="bonusID" style="width: 400px; display: none;">
                <fieldset>
                    <legend><strong>Бонусная карта (ID-номер)</strong> </legend>
                    <div style="text-align: left;">
                        <span style="color: #F00; font-size: 7pt;">Напоминаем вам, что в случае не заполнения
                        поля ID номера, повторный ввод или добавление ID через администрацию НЕВОЗМОЖЕН.</span>
                        <br />
                        <br />
                        <input id="iBonusID" type="text" maxlength="9" onkeypress="return isNumericChar(event);" />
                    </div>
                    <br />
                </fieldset>
            </div>
            <div class="divButtons">
                <div style="width: 100%; height: 5px;">
                    &nbsp;
                </div>
                <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnSaveReservation").ToString() %>'
                    onclick="saveMakeRes('');" style="min-width: 100px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                &nbsp;&nbsp;
                <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource","btnCancel").ToString() %>'
                    onclick="cancelMakeRes();" style="min-width: 100px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
            </div>
            <br />
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <span id="messages">Message</span>
        </div>
        <div id="dialog-resCustomerAddress" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerAddress") %>'
            style="display: none;">
            <iframe id="resCustomerAddress" runat="server" height="490px" width="570px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resCustOtherInfo" title='<%= GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo") %>'
            style="display: none;">
            <iframe id="resCustOtherInfo" runat="server" height="430px" width="410px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-promoSelect" title='<%= GetGlobalResourceObject("MakeReservation", "lblSelectPromotion") %>'
            style="display: none;">
            <iframe id="promoSelect" runat="server" height="400px" width="795px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resServiceAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblService") %>'
            style="display: none;">
            <iframe id="resServiceAdd" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-resServiceExtAdd" title='<%= GetGlobalResourceObject("MakeReservation", "lblExtService") %>'
            style="display: none;">
            <iframe id="resServiceExtAdd" runat="server" height="100%" width="835px" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-Ssrc" title='<%= GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode") %>'
            style="display: none;">
            <iframe id="Ssrc" runat="server" height="100%" width="100%" frameborder="0" style="clear: both;"></iframe>
        </div>
        <div id="dialog-searchCust" title='<%= GetGlobalResourceObject("MakeReservation", "searchCustLabel") %>'
            style="display: none;">
            <iframe id="searchCust" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
            <%--<div id="searchCust">
            </div--%>>
        </div>
        <div id="dialog-resServiceEdit" title='<%= GetGlobalResourceObject("ResView", "lblEditService") %>'
            style="display: none;">
            <iframe id="resServiceEdit" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-changeFlight" title='<%= GetGlobalResourceObject("ResView", "lblEditService") %>'
            style="display: none;">
            <iframe id="changeFlight" runat="server" height="100%" width="100%" frameborder="0"
                style="clear: both;"></iframe>
        </div>
        <div id="dialog-cCardApply" style="display: none; width: 350px; height: 150px; text-align: center;">
            <br />
            <br />
            <p>
                Для оформления заявки на кредитную карту, пожалуйста, введите телефон туриста и
            нажмите «отправить»
            </p>
            <br />
            <div style="vertical-align: middle;">
                <span style="font-weight: bold; font-size: 1.5em;">8</span>&nbsp;<input id="iCCardPhone"
                    type="text" maxlength="10" value="0" onkeypress="return isPhoneChar(event);" />
                <br />
                <br />
                <input type="hidden" id="cCardCustNo" value="" />
                <input type="button" value="Отправить" onclick="addCCard()" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
            </div>
        </div>
    </form>
</body>
</html>
