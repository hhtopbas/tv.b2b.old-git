﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Text;
using System.Threading;
using TvTools;

public partial class BookTicket : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
    }

    [WebMethod(EnableSession = true)]
    public static string getBookFlightInfo(string BookNr)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        int? bookNr = Conversion.getInt32OrNull(BookNr);
        FlightData flightData = (FlightData)HttpContext.Current.Session["FlightData"];
        OnlyFlight_FlightInfo bookRow = flightData.FlightInfo.Find(f => f.RecID == bookNr);
        if (bookRow == null) return "";
        StringBuilder sb = new StringBuilder();
        List<ServiceExtMarOpt> showExtraService = new Reservation().getServiceExtMarOpt(UserData, UserData.Market, ref errorMsg);

        sb.Append("<div id=\"selectedFlightDiv\">");
        sb.Append("<table id=\"selectFlightInfoTable\" style=\"width: 100%;\" cellpadding=\"4\" cellspacing=\"4\">");
        sb.AppendFormat("<tr><td align=\"center\" colspan=\"2\" id=\"header\">{0}</td></tr>", HttpContext.GetGlobalResourceObject("BookTicket", "flightInfoSelectedFlight").ToString());
        sb.Append("<tr>");
        sb.Append("<td id=\"selectFlightInfoTableLeftTd\">");
        sb.Append("<div id=\"departureDiv\">");
        sb.Append("<table cellpadding=\"2\" cellspacing=\"2\">");
        sb.AppendFormat("<tr><td colspan=\"2\">{0}</td></tr>", bookRow.RouteFromName + " => " + bookRow.RouteToName);
        sb.AppendFormat("<tr><td colspan=\"2\">{0}</td></tr>", bookRow.DepDate.HasValue ? bookRow.DepDate.Value.ToShortDateString() : "&nbsp;");

        FlightDetailRecord flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, bookRow.DepFlight, bookRow.DepDate.Value, ref errorMsg);

        sb.AppendFormat("<tr><td class=\"fieldCaption\">{0}</td><td>{1}</td></tr>",
                                        HttpContext.GetGlobalResourceObject("BookTicket", "flightInfoDeparture").ToString(),
                                        (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToShortTimeString() : "&nbsp;") + ", " +
                                        bookRow.RouteFromName + ", " +
                                        new Locations().getLocationForCountryName(UserData, bookRow.RouteFrom.Value, ref errorMsg) + " - " +
                                        flightDetail.DepAirportNameL);
        sb.AppendFormat("<tr><td class=\"fieldCaption\">{0}</td><td>{1}</td></tr>",
                                        HttpContext.GetGlobalResourceObject("BookTicket", "flightInfoArrival").ToString(),
                                        (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToShortTimeString() : "&nbsp;") + ", " +
                                        bookRow.RouteToName + ", " +
                                        new Locations().getLocationForCountryName(UserData, bookRow.RouteTo.Value, ref errorMsg) + " - " +
                                        flightDetail.ArrAirportNameL);
        sb.AppendFormat("<tr><td class=\"fieldCaption\">{0}</td><td>{1}</td></tr>",
                                        HttpContext.GetGlobalResourceObject("BookTicket", "flightInfoAirCraft").ToString(),
                                        flightDetail.FlightNo + " (" + flightDetail.AirlineNameL + ")");
        sb.AppendFormat("<tr><td class=\"fieldCaption\">{0}</td><td>{1}</td></tr>",
                                        HttpContext.GetGlobalResourceObject("BookTicket", "flightInfoClass").ToString(),
                                        bookRow.DepClass);
        sb.Append("</table>");
        List<OnlyFlight_ServiceExtra> extraService = flightData.ServiceExtra.Where(w => w.RecID == bookRow.RecID && w.MainService == bookRow.DepFlight && w.ArrDep != "Arrival").ToList<OnlyFlight_ServiceExtra>();
        StringBuilder eSb = new StringBuilder();
        bool showEXtService = false;
        if (extraService != null && extraService.Count > 0)
        {
            eSb.Append("<table id=\"gridDepExtra\">");
            eSb.Append("<tr class=\"header\">");
            eSb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraExtraName").ToString());
            eSb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraCurrency").ToString());
            eSb.AppendFormat("<td align=\"center\"><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraAdult").ToString());
            eSb.AppendFormat("<td align=\"center\"><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraChild").ToString());
            eSb.AppendFormat("<td align=\"center\"><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraInfant").ToString());
            eSb.Append("</tr>");
            foreach (OnlyFlight_ServiceExtra row in extraService)
            {
                ServiceExtMarOpt showing = showExtraService.Find(f => f.ServiceCode == row.Extra && f.ShowInResDetB2B.Value == false);
                if (showing == null)
                {
                    showEXtService = true;
                    eSb.Append("<tr>");
                    eSb.AppendFormat("<td>{0}</td>", row.ExtraName);
                    eSb.AppendFormat("<td style=\"40px\">{0}</td>", row.SaleCur);
                    eSb.AppendFormat("<td align=\"right\" style=\"60px\">{0}</td>", row.ExtraSale);
                    eSb.AppendFormat("<td align=\"right\" style=\"60px\">{0}</td>", row.ExtraChdSale);
                    eSb.AppendFormat("<td align=\"right\" style=\"60px\">{0}</td>", row.ExtraInfSale);
                    eSb.Append("</tr>");
                }
            }
            eSb.Append("</table>");
        }
        if (showEXtService)
            sb.Append(eSb.ToString());
        sb.Append("</div>");
        sb.Append("</td>");

        sb.Append("<td id=\"selectFlightInfoTableRightTd\">");
        if (string.IsNullOrEmpty(bookRow.ArrFlight))
        {
            sb.Append("&nbsp;");
        }
        else
        {
            sb.Append("<div id=\"arrivalDiv\">");
            sb.Append("<table cellpadding=\"2\" cellspacing=\"2\">");
            sb.AppendFormat("<tr><td colspan=\"2\">{0}</td></tr>", bookRow.RouteToName + " => " + bookRow.RouteFromName);
            sb.AppendFormat("<tr><td colspan=\"2\">{0}</td></tr>", bookRow.ArrDate.HasValue ? bookRow.ArrDate.Value.ToShortDateString() : "&nbsp;");

            flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, bookRow.ArrFlight, bookRow.ArrDate.Value, ref errorMsg);

            sb.AppendFormat("<tr><td class=\"fieldCaption\">{0}</td><td>{1}</td></tr>",
                                HttpContext.GetGlobalResourceObject("BookTicket", "flightInfoDeparture").ToString(),
                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToShortTimeString() : "&nbsp;") + ", " +
                                bookRow.RouteToName + ", " +
                                new Locations().getLocationForCountryName(UserData, bookRow.RouteTo.Value, ref errorMsg) + " - " +
                                flightDetail.DepAirportNameL);
            sb.AppendFormat("<tr><td class=\"fieldCaption\">{0}</td><td>{1}</td></tr>",
                                HttpContext.GetGlobalResourceObject("BookTicket", "flightInfoArrival").ToString(),
                                (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToShortTimeString() : "&nbsp;") + ", " +
                                bookRow.RouteFromName + ", " +
                                new Locations().getLocationForCountryName(UserData, bookRow.RouteFrom.Value, ref errorMsg) + " - " +
                                flightDetail.ArrAirportNameL);
            sb.AppendFormat("<tr><td class=\"fieldCaption\">{0}</td><td>{1}</td></tr>",
                                HttpContext.GetGlobalResourceObject("BookTicket", "flightInfoAirCraft").ToString(),
                                flightDetail.FlightNo + " (" + flightDetail.AirlineNameL + ")");
            sb.AppendFormat("<tr><td class=\"fieldCaption\">{0}</td><td>{1}</td></tr>",
                                HttpContext.GetGlobalResourceObject("BookTicket", "flightInfoClass").ToString(),
                                bookRow.ArrClass);
            sb.Append("</table>");
            //extraService = flightData.ServiceExtra.Where(w => w.RecID == bookRow.RecID && w.ArrDep == "Arrival").Select(s => s).ToList<OnlyFlight_ServiceExtra>();
            extraService = flightData.ServiceExtra.Where(w => w.RecID == bookRow.RecID && w.MainService == bookRow.ArrFlight && w.ArrDep == "Arrival").ToList<OnlyFlight_ServiceExtra>();
            eSb = new StringBuilder();
            showEXtService = false;
            if (extraService != null && extraService.Count > 0)
            {
                eSb.Append("<table id=\"gridArrExtra\">");
                eSb.Append("<tr class=\"header\">");
                eSb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraExtraName").ToString());
                eSb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraCurrency").ToString());
                eSb.AppendFormat("<td align=\"center\"><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraAdult").ToString());
                eSb.AppendFormat("<td align=\"center\"><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraChild").ToString());
                eSb.AppendFormat("<td align=\"center\"><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridExtraInfant").ToString());
                eSb.Append("</tr>");
                foreach (OnlyFlight_ServiceExtra row in extraService)
                {
                    ServiceExtMarOpt showing = showExtraService.Find(f => f.ServiceCode == row.Extra && f.ShowInResDetB2B.Value == false);
                    if (showing == null)
                    {
                        showEXtService = true;
                        eSb.Append("<tr>");
                        eSb.AppendFormat("<td>{0}</td>", row.ExtraName);
                        eSb.AppendFormat("<td style=\"40px\">{0}</td>", row.SaleCur);
                        eSb.AppendFormat("<td align=\"right\" style=\"60px\">{0}</td>", row.ExtraSale);
                        eSb.AppendFormat("<td align=\"right\" style=\"60px\">{0}</td>", row.ExtraChdSale);
                        eSb.AppendFormat("<td align=\"right\" style=\"60px\">{0}</td>", row.ExtraInfSale);
                        eSb.Append("</tr>");
                    }
                }
                eSb.Append("</table>");
            }
            if (showEXtService)
                sb.Append(eSb.ToString());
            eSb.Append("</div>");
        }
        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("</div>");

        sb.Append("<br />");

        sb.Append("<div id=\"passengerSelectDiv\">");
        sb.Append("<table><tr>");
        sb.AppendFormat("<td><span id=\"lblAdult\">{0} : </span></td>", HttpContext.GetGlobalResourceObject("BookTicket", "passSelectAdult").ToString());
        sb.Append("<td><input id=\"iAdult\" type=\"text\" value=\"1\" style=\"width:30px;\" /></td>");
        sb.AppendFormat("<td><span id=\"lblChild\">{0} : </span></td>", HttpContext.GetGlobalResourceObject("BookTicket", "passSelectChild").ToString());
        sb.Append("<td><input id=\"iChild\" type=\"text\" value=\"0\" style=\"width:30px;\" /></td>");
        sb.AppendFormat("<td><span id=\"lblInfant\">{0} : </span></td>", HttpContext.GetGlobalResourceObject("BookTicket", "passSelectInfant").ToString());
        sb.Append("<td><input id=\"iInfant\" type=\"text\" value=\"0\" style=\"width:30px;\" /></td>");
        sb.Append("<td>&nbsp;</td>");
        sb.AppendFormat("<td><input id=\"btnCreatePass\" type=\"button\" value=\"{0}\" style=\"height:25px;\" onclick=\"makeTicket({1});\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" /></td>",
                HttpContext.GetGlobalResourceObject("BookTicket", "createPass").ToString(),
                bookRow.RecID);
        sb.Append("</tr></table>");
        sb.Append("</div>");

        return sb.ToString();
    }

    public static string createSaveSection(User UserData, ResDataRecord ResData)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        sb.Append("<div id=\"saveResDiv\">");
        sb.Append("<table width=\"98%\"><tr><td style=\"width: 170px;\">&nbsp;</td><td>&nbsp;</td></tr>");
        sb.Append("<tr>");
        sb.AppendFormat("<td class=\"lblTotalPriceTd\">{0} : </td>", HttpContext.GetGlobalResourceObject("BookTicket", "saveDivTotalPrice").ToString());
        decimal? salePrice = 0;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_KidyTour) || Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || Equals(UserData.CustomRegID, TvBo.Common.crID_Mahbal) || Equals(UserData.CustomRegID, TvBo.Common.crID_Go2Holiday))
            salePrice = ResData.ResMain.PasPayable.HasValue ? ResData.ResMain.PasPayable.Value : 0;
        else salePrice = ResData.ResMain.AgencyPayable.HasValue ? ResData.ResMain.AgencyPayable.Value : 0;
        sb.AppendFormat("<td class=\"txtTotalPrice\">&nbsp;&nbsp;{0}</td>", salePrice.HasValue && salePrice.Value > 0 ? salePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "&nbsp;");
        sb.Append("</tr>");

        sb.Append("<tr>");
        sb.Append("<td>&nbsp;</td>");
        sb.Append("<td>&nbsp;</td>");
        sb.Append("</tr>");

        sb.AppendFormat("<tr {0}>", (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt)) ? "style=\"display: none;\"" : "");
        sb.AppendFormat("<td class=\"lblTotalPriceTd\">{0} : </td>", HttpContext.GetGlobalResourceObject("BookTicket", "saveDivNote").ToString());
        sb.Append("<td><input id=\"iNote\" type=\"text\" /></td>");
        sb.Append("</tr>");

        if (UserData.AgencyRec != null && UserData.AgencyRec.AceExport)
        {
            sb.Append("<tr>");
            sb.AppendFormat("<td class=\"lblTotalPriceTd\">{0} : </td>", HttpContext.GetGlobalResourceObject("BookTicket", "saveDivCustomerCode").ToString());
            sb.Append("<td><input id=\"iCustomerCode\" type=\"text\" class=\"customerCode\" /></td>");
            sb.Append("</tr>");

            sb.Append("<tr>");
            sb.AppendFormat("<td class=\"lblTotalPriceTd\">{0} : </td>", HttpContext.GetGlobalResourceObject("BookTicket", "saveDivDosier").ToString());
            sb.Append("<td><input id=\"iDosier\" type=\"text\" class=\"doiser\" /></td>");
            sb.Append("</tr>");
        }

        //if (Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        //{
        //    sb.Append("<tr>");
        //    sb.AppendFormat("<td class=\"lblTotalPriceTd\">{0} : </td>", "Agency discount (%)");
        //    sb.Append("<td><input id=\"iAgencyDiscount\" type=\"text\" class=\"customerCode\" /></td>");
        //    sb.Append("</tr>");
        //}

        string handicaps = new OnlyTickets().getTicketHandicapsStr(UserData, UserData.Market, ResData, ref errorMsg);
        if (!string.IsNullOrEmpty(handicaps))
        {
            sb.Append("<tr>");
            sb.AppendFormat("<td class=\"lblTotalPriceTd\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("BookTicket", "saveDivHandicaps").ToString());
            sb.Append("<td>");
            sb.AppendFormat("<span style=\"font-family: tahoma; font-size: medium; font-weight: bold; color: #FF0000;\">{0}</span><br />",
                    HttpContext.GetGlobalResourceObject("BookTicket", "saveDivImportantFlightInformation").ToString());
            sb.Append(handicaps);
            sb.AppendFormat("<input id=\"cbHandicap\" type=\"checkbox\" onclick=\"cbHandicapChanged();\"><label for=\"cbHandicap\">{0}</label>",
                    HttpContext.GetGlobalResourceObject("BookTicket", "saveDivIHaveReadAndAccept").ToString());
            sb.Append("</td>");
            sb.Append("</tr>");
        }
        else sb.Append("<tr style=\"display:none;\"><td><input id=\"cbHandicap\" type=\"checkbox\" checked=\"checked\" style=\"display:none;\" /></td></tr>");
        sb.Append("</table>");

        sb.AppendFormat("<div id=\"divWhereInvoice\" style=\"text-align:center; display: {0};\">", string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) ? "block" : "none");
        sb.AppendFormat("<strong>{0}</strong>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "whereInvoice"));
        sb.Append("<select id=\"iInvoiceTo\" style=\"width: 200px;\">");
        sb.AppendFormat("<option value=\"\">{0}</option>",
                    HttpContext.GetGlobalResourceObject("LibraryResource", "ComboSelect").ToString());
        sb.AppendFormat("<option value=\"0\">{0}</option>",
                HttpContext.GetGlobalResourceObject("LibraryResource", "ForAgency").ToString());
        sb.AppendFormat("<option value=\"1\">{0}</option>",
                HttpContext.GetGlobalResourceObject("LibraryResource", "ForPassenger").ToString());
        sb.Append("</select>");
        sb.Append("</div>");
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && ResData.ResMain.OptDate.HasValue)
        {
            sb.Append("<div id=\"divSaveOption\" style=\"text-align:center;\">");

            string optionTimeMsg = string.Format(HttpContext.GetGlobalResourceObject("MakeReservation", "optionTimeWithOptionNT").ToString(),
                                                    ResData.ResMain.OptDate.Value.ToShortDateString() + " " + ResData.ResMain.OptDate.Value.ToShortTimeString());
            string saveOptionStr = string.Format("<label for=\"withOption\">{0}</label><input id=\"withOption\" type=\"checkbox\" name=\"optionTime\" onclick=\"onClickOptionTime()\" /><br /><br /><span id=\"withOptionMsg\" style=\"font-color: Red; display: none;\"><b>{1}</b></span>",
                                    optionTimeMsg,
                                    "Booking will be cancelled automatically if the agency user will not re-confirm it until the specified time above!");
            sb.Append(saveOptionStr);
            sb.Append("<br /><br /></div>");
        }
        else
        {
            sb.AppendFormat("<div id=\"divSaveOption\" style=\"text-align:center; display: {0};\">", string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) ? (UserData.OwnAgency ? "block" : "none") : "none");

            sb.AppendFormat("<input id=\"iSaveOption\" type=\"checkbox\" {1} /><label for=\"iSaveOption\"><b>{0}</b></label>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "saveOption"),
                    !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) || (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr) && UserData.OwnAgency) ? "" : "checked=\"checked\"");
            sb.Append("</div>");
        }


        sb.Append("<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
        sb.Append("<tr><td align=\"center\">");
        sb.AppendFormat("<input id=\"btnSave\" type=\"button\" value=\"{0}\" style=\"width:100px; height:30px;\" onclick=\"btnSaveClick({1});\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only\" />",
                HttpContext.GetGlobalResourceObject("LibraryResource", "btnSaveReservation").ToString(),
                ResData.ResCust.Count);
        sb.Append("</td><td align=\"center\">");
        sb.AppendFormat("<input id=\"btnCancel\" type=\"button\" value=\"{0}\" style=\"width:100px; height:30px;\" onclick=\"btnCancelClick();\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only\" />",
                HttpContext.GetGlobalResourceObject("LibraryResource", "btnCancel").ToString());
        sb.Append("</td></tr></table>");
        sb.Append("</div>");

        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string[] dateMaskB = dateMaskA.Split(UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d": _dateMask += "/99"; _dateFormat += "/dd"; break;
                    case "m": _dateMask += "/99"; _dateFormat += "/MM"; break;
                    case "y": _dateMask += "/9999"; _dateFormat += "/yyyy"; break;
                    default: break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateFormat = _dateFormat.Remove(0, 1);

        return "{" + string.Format("\"dateMask\":\"{0}\",\"dateFormat\":\"{1}\",\"maxPax\":{2}",
                                _dateMask.Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0]),
                                _dateFormat.Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0]),
                                UserData.TvParams.TvParamFlight.MaxPaxonTicket.HasValue ? UserData.TvParams.TvParamFlight.MaxPaxonTicket.Value : 99)
               + "}";
    }

    public static string getResCustDivTmp(User UserData, ResDataRecord ResData, string tmpCustEdit, ref string errorMsg)
    {
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int lastPosition = 0;
        string retVal = string.Empty;
        string tmpTemplate = tmpCustEdit;

        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string[] dateMaskB = dateMaskA.Split(UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d": _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99"; _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "dd"; break;
                    case "m": _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "99"; _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "MM"; break;
                    case "y": _dateMask += UserData.Ci.DateTimeFormat.DateSeparator[0] + "9999"; _dateFormat += UserData.Ci.DateTimeFormat.DateSeparator[0] + "yyyy"; break;
                    default: break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateFormat = _dateFormat.Remove(0, 1);
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci).Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _phoneMask = string.Empty;
        if (UserData.PhoneMask != null)
            _phoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.MobilPhoneMask.Replace('#', '9') : "";

        List<string> SrrcServiceTypeList = new Ssrcs().getSsrcServiceTypes(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);

        string _dateFormatRegion = new TvBo.Common().getDateFormatRegion(UserData.Ci);
        tmpCustEdit = tmpCustEdit.Replace("{\"dateFormatRegion\"}", _dateFormatRegion);

        bool seachCust = new Agency().getRole(UserData, 504);

        bool exit = true;
        while (exit)
        {
            string valueTemp = string.Empty;
            int first = tmpTemplate.IndexOf("[{");
            if (first > -1)
            {
                lastPosition = first;
                tmpTemplate = tmpTemplate.Remove(0, first + 2);
                int last = tmpTemplate.IndexOf("}]");
                valueTemp = tmpTemplate.Substring(0, last);
                tmpTemplate = tmpTemplate.Remove(0, last + 2);
                string[] local = valueTemp.Trim().Split(',');
                if (local.Length > 1)
                {
                    string classKey = local[0].Trim('\"');
                    string resourceKey = local[1].Trim('\"');
                    string localStr = HttpContext.GetGlobalResourceObject(classKey, resourceKey).ToString();
                    tmpCustEdit = tmpCustEdit.Replace("[{" + valueTemp + "}]", localStr);
                }
            }
            else exit = false;
        }

        tmpTemplate = tmpCustEdit;

        string tmpLoopSection = tmpCustEdit.Substring(tmpCustEdit.IndexOf("{loop"));
        tmpLoopSection = tmpLoopSection.Substring(5, tmpLoopSection.IndexOf("loop}") - 5);
        string loopSection = string.Empty;
        Int16 cnt = 0;
        List<DDLData> nations = CacheObjects.getNations(UserData.Market);
        List<Nationality> nationalityList = CacheObjects.getNationality(UserData);
        bool? defaultCountrySelected = Conversion.getBoolOrNull(new Common().getFormConfigValue("MakeRes", "ResCustDefaultCountrySelected"));
        List<TitleAgeRecord> title = ResData.TitleCust;

        foreach (ResCustRecord row in ResData.ResCust)
        {
            string nationList = string.Empty;
            if (defaultCountrySelected.HasValue && defaultCountrySelected.Value)
                nationList = string.Format("<option value=\"\" selected=\"selected\">{0}</option>", HttpContext.GetGlobalResourceObject("LibraryResource", "ComboSelect").ToString());
            else
                nationList = "";

            //if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
            //    foreach (Nationality nRow in nationalityList)
            //        nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.Code3,(notSelectDefaultNationality.HasValue && !notSelectDefaultNationality.Value) && Equals(nRow.Code3, row.Nationality) ? "selected=\"selected\"" : "", nRow.Name);
            //else
            //    foreach (DDLData nRow in nations)
            //        nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.FieldData, Equals(nRow.FieldData, row.Nation.ToString()) ? "selected=\"selected\"" : "", nRow.TextData);

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                foreach (Nationality nRow in nationalityList.OrderBy(o => o.OrderNo).ThenBy(t => t.Name))
                    if (defaultCountrySelected.HasValue && defaultCountrySelected.Value && row.IDNo == null)
                        nationList += string.Format("<option value=\"{0}\">{1}</option>", nRow.Code3, nRow.Name);
                    else
                        nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.Code3, Equals(nRow.Code3, row.Nationality) ? "selected=\"selected\"" : "", nRow.Name);
            else
                foreach (DDLData nRow in nations)
                    nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.FieldData, Equals(nRow.FieldData, row.Nation.ToString()) ? "selected=\"selected\"" : "", nRow.TextData);


            string titleList = string.Empty;
            if (row.Title < 6)
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo < 6).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }
            else
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo > 5).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }

            cnt += 1;
            string _tmpl = tmpLoopSection;
            string tmpl = tmpLoopSection;

            exit = true;

            while (exit)
            {
                string valueTemp = string.Empty;

                int first = _tmpl.IndexOf("{\"");
                if (first > -1)
                {
                    lastPosition = first;
                    _tmpl = _tmpl.Remove(0, first + 2);
                    int last = _tmpl.IndexOf("\"}");
                    valueTemp = _tmpl.Substring(0, last);
                    _tmpl = _tmpl.Remove(0, last + 2);


                    string valueStr = string.Empty;
                    if (valueTemp.IndexOf('.') > -1)
                    {
                        if (Equals(valueTemp, "UserData.Ci.LCID"))
                            valueStr = UserData.Ci.LCID.ToString();
                    }
                    else
                    {
                        System.Reflection.PropertyInfo[] oProps = null;
                        oProps = row.GetType().GetProperties();
                        List<System.Reflection.PropertyInfo> _pi = (from q in oProps
                                                                    where q.Name == valueTemp
                                                                    select q).ToList<System.Reflection.PropertyInfo>();
                        if (_pi != null && _pi.Count() > 0)
                        {
                            System.Reflection.PropertyInfo pi = _pi[0];
                            object value = pi.GetValue(row, null) == null ? DBNull.Value : pi.GetValue(row, null);
                            valueStr = value.ToString();
                        }
                    }

                    if (Equals(valueTemp, "PhoneCountryCode"))
                    {
                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", UserData.PhoneMask != null ? UserData.PhoneMask.CountryCode : "");
                    }
                    else
                        if (Equals(valueTemp, "dateFormat"))
                        {
                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", dateFormat);
                        }
                        else
                            if (Equals(valueTemp, "BirtDay"))
                            {
                                string birthDate = row.Birtday.HasValue ? row.Birtday.Value.ToString(_dateFormat) : "";
                                tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", birthDate);
                            }
                            else
                                if (Equals(valueTemp, "ResDate"))
                                {
                                    tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(dateFormat) : "");
                                }
                                else
                                    if (Equals(valueTemp, "Title"))
                                    {
                                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", titleList);
                                    }
                                    else
                                        if (Equals(valueTemp, "Nation"))
                                        {
                                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", nationList);
                                        }
                                        else
                                            if (Equals(valueTemp, "Nationality"))
                                            {
                                                tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", nationList);
                                            }
                                            else
                                                if (Equals(valueTemp, "LeaderCheck"))
                                                {
                                                    if (Equals(row.Leader, "Y"))
                                                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "checked='checked'");
                                                    else tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", " ");
                                                }
                                                else
                                                    if (Equals(valueTemp, "searchCustVisible"))
                                                    {
                                                        if (!seachCust)
                                                            tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", "display: none;");
                                                        else tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", " ");
                                                    }
                                                    else
                                                        tmpl = tmpl.Replace("{\"" + valueTemp + "\"}", valueStr);

                    tmpl = tmpl.Replace("{[]}", row.CustNo.ToString());
                    tmpl = tmpl.Replace("[]", cnt.ToString());
                }
                else exit = false;
            }

            loopSection += tmpl;
        }

        tmpTemplate = tmpTemplate.Replace("{loop" + tmpLoopSection + "loop}", loopSection);
        StringBuilder sb = new StringBuilder();

        sb.Append("<div id=\"resCustGrid\">");
        sb.AppendFormat("<input id=\"hfCustCount\" type=\"hidden\" value=\"{0}\" />", ResData.ResCust.Count);
        sb.AppendFormat("<input id=\"dateMask\" type=\"hidden\" value=\"{0}\" />", _dateMask);
        sb.AppendFormat("<input id=\"phone_Mask\" type=\"hidden\" value=\"{0}\" />", _phoneMask);
        sb.Append(tmpTemplate);
        sb.Append("</div>");

        return sb.ToString();
    }

    public static string getTemplate(User UserData)
    {
        string sb = string.Empty;
        string filePath = AppDomain.CurrentDomain.BaseDirectory + "Data\\" + new UICommon().getWebID() + "\\" + UserData.Market + "\\TicketResCustEdit.tmpl";
        if (System.IO.File.Exists(filePath))
        {
            System.IO.StreamReader Tex = new System.IO.StreamReader(filePath);
            sb = Tex.ReadToEnd();
            Tex.Close();
        }
        return sb;
    }

    [WebMethod(EnableSession = true)]
    public static string createTicketCustStr()
    {
        #region old
        /*
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        List<ResCustRecord> resCust = ResData.ResCust;
        string _dateFormatStr = new TvBo.Common().getDateFormatRegion(UserData.Ci);
        sb.Append("<table id=\"gridCust\">");
        sb.Append("<tr class=\"header\">");
        sb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridCustPassID").ToString());
        sb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridCustTitle").ToString());
        sb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridCustSurname").ToString());
        sb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridCustName").ToString());
        sb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridCustDateOfBirth").ToString()
                + (!string.IsNullOrEmpty(_dateFormat) ? "<br />" + _dateFormatStr : string.Empty));
        sb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridCustAge").ToString());
        sb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridCustPhone").ToString());
        sb.AppendFormat("<td><b>{0}</b></td>", HttpContext.GetGlobalResourceObject("BookTicket", "gridCustParentID").ToString());
        //sb.AppendFormat("<td><b>{0}</b></td>", "#");
        sb.Append("</tr>");
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci);
        foreach (ResCustRecord row in resCust)
        {
            sb.Append("<tr>");
            sb.AppendFormat("<td class=\"passid\">{0}</td>", row.CustNo);
            string titleStr = string.Empty;
            titleStr += string.Format("<select id=\"title{0}\" style=\"width: 50px;\">", row.SeqNo);
            foreach (TitleAgeRecord r in ResData.TitleCust)
                titleStr += string.Format("<option value=\"{0}\" {1}>{2}</option>", r.TitleNo, (row.Title == r.TitleNo ? "selected=\"selected\"" : string.Empty), r.Code);
            titleStr += "</select>";
            sb.AppendFormat("<td class=\"title\">{0}</td>", titleStr);
            sb.AppendFormat("<td><input id=\"iSurname{0}\" type=\"text\" style=\"width: 98%; {1} \" /></td>", 
                row.SeqNo,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules));
            sb.AppendFormat("<td><input id=\"iName{0}\" type=\"text\" style=\"width: 98%; {1} \" /></td>", 
                row.SeqNo,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules));
            sb.AppendFormat("<td class=\"birthdate\"><input id=\"iBirthDate{0}\" type=\"text\" style=\"width: 98%;\" class=\"formatDate\" onblur=\"return SetAge({1},'{2}', '{3}')\" /></td>",
                    row.SeqNo,
                    row.SeqNo,
                    ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(dateFormat).Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '/') : "",
                    dateFormat.Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], '/'));
            sb.AppendFormat("<td class=\"age\"><input id=\"iAge{0}\" type=\"text\" style=\"width: 98%;\" /></td>", row.SeqNo);
            sb.AppendFormat("<td class=\"phone\"><input id=\"iPhone{0}\" type=\"text\" style=\"width: 98%;\" /></td>", row.SeqNo);
            sb.AppendFormat("<td class=\"parentid\"><input id=\"iParent{0}\" type=\"text\" style=\"width: 98%;\" /></td>", row.SeqNo);
            //sb.AppendFormat("<td class=\"viewedit\">{0}</td>", "View");
            sb.Append("</tr>");
        }
        sb.Append("</table>");
        sb.Append("<br />");

        return sb.ToString();
        */
        #endregion

        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        string tmplResCustEdit = getTemplate(UserData);

        if (!string.IsNullOrEmpty(tmplResCustEdit))
        {
            return getResCustDivTmp(UserData, ResData, tmplResCustEdit, ref errorMsg);
        }

        string dateMaskA = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ');
        string[] dateMaskB = dateMaskA.Split(UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string _dateMask = string.Empty;
        string _dateFormat = string.Empty;
        foreach (string row in dateMaskB)
            if (!string.IsNullOrEmpty(row.ToString()))
                switch (row[0].ToString().ToLower())
                {
                    case "d": _dateMask += "/99"; _dateFormat += "/dd"; break;
                    case "m": _dateMask += "/99"; _dateFormat += "/MM"; break;
                    case "y": _dateMask += "/9999"; _dateFormat += "/yyyy"; break;
                    default: break;
                }
        _dateMask = _dateMask.Remove(0, 1);
        _dateMask = _dateMask.Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0]);
        _dateFormat = _dateFormat.Remove(0, 1);
        _dateFormat = _dateFormat.Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0]);
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci);
        string _phoneMask = string.Empty;
        if (UserData.PhoneMask != null)
            _phoneMask = UserData.PhoneMask.MobilPhoneMask != null ? UserData.PhoneMask.MobilPhoneMask.Replace('#', '9') : "";

        List<string> SrrcServiceTypeList = new Ssrcs().getSsrcServiceTypes(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);

        string _dateFormatRegion = new TvBo.Common().getDateFormatRegion(UserData.Ci);

        bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
        if (UserData.TvParams.TvParamReser.NeedPIN.HasValue && UserData.TvParams.TvParamReser.NeedPIN.Value)
            showPIN = true;
        bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));
        StringBuilder sb = new StringBuilder();

        //sb.Append("<fieldset><legend><label>»</label>");
        //sb.AppendFormat("<span class=\"lblGridResCust\">{0}</span></legend>",
        //        HttpContext.GetGlobalResourceObject("MakeReservation", "lblGridResCust"));
        sb.Append("<div id=\"resCustGrid\">");
        sb.AppendFormat("<input id=\"hfCustCount\" type=\"hidden\" value=\"{0}\" />", ResData.ResCust.Count);
        sb.AppendFormat("<input id=\"dateMask\" type=\"hidden\" value=\"{0}\" />", _dateMask);
        sb.AppendFormat("<input id=\"phone_Mask\" type=\"hidden\" value=\"{0}\" />", _phoneMask);
        #region ResCustGrid
        sb.Append("<div class=\"resCustGridCss\">");

        bool seachCust = new Agency().getRole(UserData, 504);

        #region ResCustGrid Header
        sb.Append("<div class=\"header\">");
        sb.AppendFormat("<div class=\"SeqNoH\"><strong>{0}</strong></div>", "&nbsp;");
        sb.AppendFormat("<div class=\"TitleH\"><strong>{0}</strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustTitle"));
        sb.AppendFormat("<div class=\"Surname{1}H\"><strong>{0}</strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustSurname"),
                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
        sb.AppendFormat("<div class=\"Name{1}H\"><strong>{0}</strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustName"),
                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
        sb.AppendFormat("<div class=\"BirthDayH\"><strong>{0}</strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustBirthDate").ToString() + "<br />" + _dateFormatRegion);
        sb.AppendFormat("<div class=\"AgeH\"><strong>{0}</strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustAge"));
        if (showPIN == null || (showPIN.HasValue && showPIN.Value))
            sb.AppendFormat("<div class=\"IdNoH\"><strong>{0}</strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPIN"));
        if (showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value))
        {
            sb.AppendFormat("<div class=\"PassSerieH\"><strong>{0}</strong></div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassSerie"));
            sb.AppendFormat("<div class=\"PassNoH\"><strong>{0}</strong></div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassNo"));
            sb.AppendFormat("<div class=\"HasPassportH\"><strong>{0}</strong></div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPass"));
        }
        if (showPIN == null || (showPIN.HasValue && showPIN.Value))
        {
            sb.AppendFormat("<div class=\"PhoneH\"><strong>{0}</strong></div>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPhone"));
        }
        else
        {
            string CountryCodeStr = string.Empty;
            if (UserData.PhoneMask != null)
                CountryCodeStr = !string.IsNullOrEmpty(UserData.PhoneMask.CountryCode) ? " (" + UserData.PhoneMask.CountryCode + ")" : "";
            sb.AppendFormat("<div class=\"PhoneLH\"><strong>{0}</strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPhone").ToString() + CountryCodeStr);
        }
        sb.AppendFormat("<div class=\"Nation{1}H\"><strong>{0}</strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustCitizenship"),
                !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
        sb.AppendFormat("<div class=\"LeaderH\"><strong>{0}</strong></div>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustLeader"));
        sb.AppendFormat("<div class=\"ViewEditH\"><strong>{0}</strong></div>", "#");
        sb.Append("</div>");

        #endregion

        List<DDLData> nations = CacheObjects.getNations(UserData.Market);
        List<Nationality> nationalityList = CacheObjects.getNationality(UserData);
        bool? defaultCountrySelected = Conversion.getBoolOrNull(new Common().getFormConfigValue("MakeRes", "ResCustDefaultCountrySelected"));
        List<TitleAgeRecord> title = ResData.TitleCust;
        Int16 cnt = 0;
        foreach (TvBo.ResCustRecord row in ResData.ResCust)
        {
            #region ResData.ResCust
            bool Ssrcode = (from q1 in ResData.ResCon
                            join q2 in ResData.ResService on q1.ServiceID equals q2.RecID
                            join q3 in SrrcServiceTypeList on q2.ServiceType equals q3
                            where q1.CustNo == row.CustNo
                            select new { q2.RecID }).Count() > 0;

            string nationList = string.Empty;
            if (defaultCountrySelected.HasValue && defaultCountrySelected.Value)
                nationList = string.Format("<option value=\"\" selected=\"selected\">{0}</option>", HttpContext.GetGlobalResourceObject("LibraryResource", "ComboSelect").ToString());
            else
                nationList = "";

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                foreach (Nationality nRow in nationalityList.OrderBy(o => o.OrderNo).ThenBy(t => t.Name))
                    if (defaultCountrySelected.HasValue && defaultCountrySelected.Value && row.IDNo == null)
                        nationList += string.Format("<option value=\"{0}\">{1}</option>", nRow.Code3, nRow.Name);
                    else
                        nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.Code3, Equals(nRow.Code3, row.Nationality) ? "selected=\"selected\"" : "", nRow.Name);
            else
                foreach (DDLData nRow in nations)
                    nationList += string.Format("<option value=\"{0}\" {1}>{2}</option>", nRow.FieldData, Equals(nRow.FieldData, row.Nation.ToString()) ? "selected=\"selected\"" : "", nRow.TextData);

            string titleList = string.Empty;
            if (row.Title < 6)
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo < 6).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }
            else
            {
                foreach (TitleAgeRecord trow in title.Where(w => w.TitleNo > 5).Select(s => s).ToList<TitleAgeRecord>())
                    titleList += string.Format("<option value=\"{0}\" {1}>{2}</option>", trow.TitleNo, Equals(trow.TitleNo.ToString(), row.Title.ToString()) ? "selected=\"selected\"" : "", trow.Code);
            }
            cnt++;
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<div class=\"row\" style=\"background-color: #E2E2E2;\">");
            else sb.Append("<div class=\"row\">");
            sb.AppendFormat("<div class=\"SeqNo\"><span id=\"iSeqNo{0}\">{1}</span></div>", cnt, cnt);
            sb.AppendFormat("<div class=\"Title\"><select id=\"iTitle{0}\">{1}</select></div>", cnt, titleList);
            sb.AppendFormat("<div class=\"Surname{0}\">", !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            sb.AppendFormat("<input id=\"iSurname{0}\" type=\"text\" value=\"{1}\" maxlength=\"30\" style=\"width:96%; {2}\" onkeypress=\"return isNonUniCodeChar(event);\" onChange=\"isNameSurnameString(this.id);\"/>",
                cnt,
                row.Surname,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules));
            sb.AppendFormat("<input id=\"iSurnameL{0}\" type=\"hidden\" value=\"{1}\" maxlength=\"30\" /></div>",
                cnt,
                row.SurnameL);
            sb.AppendFormat("<div class=\"Name{0}\">", !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            sb.AppendFormat("<input id=\"iName{0}\" type=\"text\" value=\"{1}\" maxlength=\"30\" style=\"width:96%; {2}\" onkeypress=\"return isNonUniCodeChar(event);\" onChange=\"isNameSurnameString(this.id);\" />",
                cnt,
                row.Name,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules));
            sb.AppendFormat("<input id=\"iNameL{0}\" type=\"hidden\" value=\"{1}\" maxlength=\"30\" /></div>",
                cnt,
                row.NameL);

            string birthDate = row.Birtday.HasValue ? row.Birtday.Value.ToString(_dateFormat.Replace('/', UserData.Ci.DateTimeFormat.DateSeparator[0])) : "";

            sb.AppendFormat("<div class=\"BirthDay\"><input id=\"iBirthDay{0}\" type=\"text\" class=\"formatDate\" style=\"width:95%;\" onblur=\"return SetAge({1},'{2}', '{3}')\" value=\"{4}\"/></div>",
                                cnt,
                                cnt,
                                ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.ToString(_dateFormat) : "",
                                _dateFormat,
                                birthDate);
            sb.AppendFormat("<div class=\"Age\"><input id=\"iAge{0}\" type=\"text\" value=\"{1}\" style=\"width:90%;\" disabled=\"disabled\" /></div>", cnt, row.Age);
            if (showPIN == null || (showPIN.HasValue && showPIN.Value))
                sb.AppendFormat("<div class=\"IdNo\"><input id=\"iIDNo{0}\" type=\"text\" value=\"{1}\" style=\"width:93%;\" maxlength=\"20\" /></div>", cnt, row.IDNo);

            if (!showPassportInfo.HasValue || showPassportInfo.Value)
            {
                sb.AppendFormat("<div class=\"PassSerie\"><input id=\"iPassSerie{0}\" type=\"text\" value=\"{1}\" style=\"width:90%;\" maxlength=\"5\" /></div>", cnt, row.PassSerie);
                sb.AppendFormat("<div class=\"PassNo\"><input id=\"iPassNo{0}\" type=\"text\" value=\"{1}\" style=\"width:93%;\" maxlength=\"10\" /></div>", cnt, row.PassNo);
                sb.AppendFormat("<div class=\"HasPassport\"><input id=\"iHasPassport{0}\" type=\"checkbox\" {1} {2} />", cnt, (row.HasPassport.HasValue ? row.HasPassport.Value : true) ? "checked=\"checked\"" : "", row.Title < 6 ? "disabled=\"disabled\"" : "");
                sb.AppendFormat("<input id=\"iPassIssueDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt,
                                row.PassIssueDate.HasValue ? row.PassIssueDate.Value.ToString(_dateFormat) : "");
                sb.AppendFormat("<input id=\"iPassExpDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt,
                                row.PassExpDate.HasValue ? row.PassExpDate.Value.ToString(_dateFormat) : "");
                sb.AppendFormat("<input id=\"iPassGiven{0}\" type=\"hidden\" value=\"{1}\" maxlength=\"30\" />", cnt,
                                row.PassGiven);
                sb.Append("</div>");
            }
            else
            {
                sb.Append("<div style=\"display: none; visibility: hidden;\">");
                sb.AppendFormat("<input id=\"iPassSerie{0}\" type=\"hidden\" value=\"{1}\"  />", cnt, row.PassSerie);
                sb.AppendFormat("<input id=\"iPassNo{0}\" type=\"hidden\" value=\"{1}\" />", cnt, row.PassNo);
                sb.AppendFormat("<input id=\"iHasPassport{0}\" type=\"checkbox\" {1} />", cnt, cnt, (row.HasPassport.HasValue ? row.HasPassport.Value : true) ? "checked=\"checked\"" : "");
                sb.AppendFormat("<input id=\"iPassIssueDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt, "");
                sb.AppendFormat("<input id=\"iPassExpDate{0}\" type=\"hidden\" value=\"{1}\"/>", cnt, "");
                sb.AppendFormat("<input id=\"iPassGiven{0}\" type=\"hidden\" value=\"{1}\" maxlength=\"30\" />", cnt, row.PassGiven);
                sb.Append("</div>");
            }

            if (showPIN == null || (showPIN.HasValue && showPIN.Value))
                sb.AppendFormat("<div class=\"Phone\"><input id=\"iPhone{0}\" type=\"text\" value=\"{1}\" style=\"width:90%;\" maxlength=\"15\" onkeypress=\"return isPhoneChar(event);\" /></div>",
                    cnt,
                    string.IsNullOrEmpty(row.Phone) ? row.Phone : row.Phone.Replace(" ", ""));
            else sb.AppendFormat("<div class=\"PhoneL\"><input id=\"iPhone{0}\" type=\"text\" value=\"{1}\" style=\"width:98%;\" maxlength=\"15\" onkeypress=\"return isPhoneChar(event);\" /></div>",
                        cnt,
                        string.IsNullOrEmpty(row.Phone) ? row.Phone : row.Phone.Replace(" ", ""));

            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                sb.AppendFormat("<div class=\"Nation{2} floatLeft\"><select id=\"iNationality{0}\">{1}</select></div>", cnt, nationList, !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            else
                sb.AppendFormat("<div class=\"Nation{2} floatLeft\"><select id=\"iNation{0}\">{1}</select></div>", cnt, nationList, !showPassportInfo.HasValue || showPassportInfo.Value ? "" : "L");
            if (row.Title < 6)
                sb.AppendFormat("<div class=\"Leader\"><input id=\"iLeader{0}\" type=\"radio\" name=\"Leader\" value=\"{1}\" {2} /></div>", cnt, cnt, Equals(row.Leader, "Y") ? "checked=\"checked\"" : "");
            else sb.Append("<div class=\"Leader\">&nbsp;</div>");
            string searchStr = ""; // string.Format("<img alt=\"{0}\" src=\"Images/search16.png\" onclick=\"searchCust({1})\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustLabel"), cnt);
            string infoStr = string.Format("<img alt=\"{0}\" src=\"Images/info.gif\" onclick=\"showResCustInfo({1})\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerOtherInfo"), row.CustNo);
            string addrInfoStr = string.Format("<img alt=\"{0}\" src=\"Images/address.gif\" onclick=\"showCustAddress({1})\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "lblCustomerAddress"), row.CustNo);
            string ssrcStr = string.Format("<img alt=\"{0}\" src=\"Images/depFlight.gif\" onclick=\"showSSRC({1})\" width=\"16px\" height=\"16px\" />", HttpContext.GetGlobalResourceObject("MakeReservation", "lblSpecialServiceReqCode"), row.CustNo);

            sb.AppendFormat("<div class=\"ViewEdit\">{0}&nbsp;{1}&nbsp;{2}&nbsp;{3}</div>", searchStr, infoStr, addrInfoStr, Ssrcode ? ssrcStr : "");

            //List<ResCustPriceRecord> ppPrice = ResData.ResCust.Where(w => w.CustNo == row.CustNo).Select(s => s).ToList<ResCustPriceRecord>();
            decimal? PPPrice = row.ppPasPayable;
            string ppPriceStr = PPPrice.HasValue ? PPPrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur : "&nbsp;";
            sb.AppendFormat("<div class=\"Price\"><h3>{0} : {1}</h3></div>", HttpContext.GetGlobalResourceObject("MakeReservation", "lblPerPerson"), ppPriceStr);
            sb.Append("</div>");

            #endregion
        }
        sb.Append("</div>");
        #endregion
        sb.Append("</div>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string getResService()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();

        bool? _serviceEdit = false;

        List<ServiceExtMarOpt> showExtraService = new Reservation().getServiceExtMarOpt(UserData, UserData.Market, ref errorMsg);

        sb.Append("<br /><fieldset><legend><label>»</label>");
        sb.AppendFormat("<span id=\"lblGridService\">{0}</span></legend>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "lblGridService"));
        sb.Append("<div id=\"gridResServiceDiv\" style=\"padding-left: 2px; padding-right: 2px; text-align: left;\">");
        sb.Append("<table style=\"width: 100%; border-collapse: collapse; font-family: Arial; color: #333333\" id=\"gridResService\" border=\"0\" cellSpacing=\"0\" cellPadding=\"3\">");
        sb.Append("<tr style=\"background-color:#444; color:#FFF;\">");
        sb.AppendFormat("<td style=\"width: 150px;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerServiceType"));
        sb.AppendFormat("<td><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerServiceDesc"));
        sb.AppendFormat("<td style=\"width: 60px; text-align: center;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerComp"));
        sb.AppendFormat("<td style=\"width: 60px; text-align: center;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerIncPack"));
        sb.AppendFormat("<td style=\"width: 50px; text-align: center;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerUnit"));
        sb.AppendFormat("<td style=\"width: 90px; text-align:right;\"><strong>{0}</strong></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resSerPrice"));
        if (_serviceEdit.HasValue && _serviceEdit.Value)
            sb.AppendFormat("<td style=\"width: 50px; text-align:right;\"><strong>{0}</strong></td>", "&nbsp;");
        sb.Append("</tr>");
        ResServiceRecord lastFlight = ResData.ResService.Where(w => w.ServiceType == "FLIGHT").OrderBy(o => o.BegDate).LastOrDefault();
        string lastFlightNo = lastFlight != null ? lastFlight.Service : string.Empty;
        int cnt = 0;
        foreach (TvBo.ResServiceRecord row in ResData.ResService)
        {
            #region ResData.ResService
            cnt++;
            if (Convert.ToInt32(cnt / 2.0) == (cnt / 2.0))
                sb.Append("<tr style=\"background-color:#DDD;\">");
            else sb.Append("<tr>");

            sb.AppendFormat("<td style=\"width: 150px;\">{0}</td>", string.IsNullOrEmpty(row.ServiceTypeNameL) ? row.ServiceType : (UserData.EqMarketLang ? row.ServiceTypeNameL : row.ServiceTypeName));

            string ServiceDesc = string.Empty;
            switch (row.ServiceType)
            {
                case "HOTEL": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                case "FLIGHT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                case "TRANSPORT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                case "TRANSFER": ServiceDesc += ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                case "RENTING": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                case "EXCURSION": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                case "INSURANCE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                case "VISA": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                case "HANDFEE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                default: ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
            }
            sb.AppendFormat("<td>{0}</td>", (UserData.EqMarketLang ? row.ServiceNameL : row.ServiceName) + " (" + ServiceDesc + ")");

            string compulsoryStr = string.Empty;
            if (row.Compulsory.HasValue && row.Compulsory.Value)
                compulsoryStr = "<img alt=\"\" src=\"Images/accept.gif\" />";
            else compulsoryStr = "&nbsp;";
            sb.AppendFormat("<td style=\"width: 60px; text-align: center;\">{0}</td>", compulsoryStr);

            string IncludePackStr = string.Empty;
            if (Equals(row.IncPack, "Y"))
                IncludePackStr = "<img alt=\"\" src=\"Images/accept.gif\" />";
            else IncludePackStr = "&nbsp;";
            sb.AppendFormat("<td style=\"width: 60px; text-align: center;\">{0}</td>", IncludePackStr);

            sb.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", row.Unit);

            object _showIncSrvPrice = new TvBo.Common().getFormConfigValue("MakeRes", "ShowIncSrvPrice");
            bool? ShowIncSrvPrice = Conversion.getBoolOrNull(_showIncSrvPrice);
            string salePrice = string.Empty;

            if (!Equals(row.IncPack, "Y") || (ShowIncSrvPrice.HasValue ? ShowIncSrvPrice.Value : false))
                salePrice = row.SalePrice.HasValue ? row.SalePrice.Value.ToString("#,###.00") : "&nbsp;";
            else
                salePrice = HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
            sb.AppendFormat("<td style=\"width: 90px; text-align:right;\"><b>{0}</b></td>", salePrice);
            sb.Append("</tr>");

            List<ResServiceExtRecord> showResServiceExt = ResData.ResServiceExt.Where(w => w.ServiceID == row.RecID).Select(s => s).ToList<ResServiceExtRecord>();

            //ServiceExtMarOpt showing = showExtraService.Find(f => f.ShowInResDetB2B.Value == false);
            //ServiceExtMarOpt showing = showExtraService.Find(f => f.ServiceCode == row.ExtService && f.ShowInResDetB2B.Value == false);
            bool showExtService = false;
            StringBuilder sb1 = new StringBuilder();
            if (showResServiceExt != null && showResServiceExt.Count > 0)
            {
                sb1.Append("<tr>");
                sb1.Append("<td>&nbsp;</td>");
                if (_serviceEdit.HasValue && _serviceEdit.Value)
                    sb1.Append("<td colspan=\"6\" style=\"vertical-align: top;\">");
                else sb1.Append("<td colspan=\"5\" style=\"vertical-align: top;\">");

                sb1.AppendFormat("<table id=\"extraservice{0}\" class=\"extratotal\" cellpadding=\"0\" cellspacing=\"0\">", row.RecID.ToString() + "_" + row.RecID.ToString());
                sb1.AppendFormat("<tr><td><strong>{0}</strong></td>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "lblExtraService"));
                sb1.AppendFormat("<td style=\"text-align:right; width: 125px;\"><input type=\"button\" value=\"{0}\" class=\"down\" onClick=\"showExtServiceDetail('{1}');\" /></td>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtShowdetail"),
                        row.RecID.ToString() + "_" + row.RecID.ToString());
                sb1.Append("<td style=\"width: 60px;\">&nbsp;</td>");
                sb1.Append("<td style=\"width: 60px;\">&nbsp;</td>");
                sb1.Append("<td style=\"width: 50px;\">&nbsp;</td>");
                decimal? totalExt = (from q in showResServiceExt
                                     where q.IncPack != "Y" || (ShowIncSrvPrice.HasValue ? ShowIncSrvPrice.Value : false)
                                     select new { salePrice = q.SalePrice }).Sum(s => s.salePrice);
                sb1.AppendFormat("<td style=\"width: 90px; text-align: right;\"><b>{0}</b></td>", totalExt.HasValue && (ShowIncSrvPrice.HasValue ? ShowIncSrvPrice.Value : false) ? totalExt.Value.ToString("#,###.00") : "&nbsp;");
                if (_serviceEdit.HasValue && _serviceEdit.Value)
                    sb1.Append("<td style=\"width: 50px;\">&nbsp;</td>");
                sb1.Append("</tr></table>");

                sb1.AppendFormat("<table id=\"extraserviceDetail{0}\" class=\"extradetail\" cellpadding=\"0\" cellspacing=\"0\">", row.RecID.ToString() + "_" + row.RecID.ToString());
                sb1.Append("<tr style=\"background:#888; color:#FFF;\"><td>");
                sb1.AppendFormat("<div class=\"showdetaildiv\"><strong>{0}</strong>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtDesc"));
                sb1.AppendFormat("<input type=\"button\" value=\"{0}\" class=\"up\" onClick=\"hideExtServiceDetail('{1}');\" /></div></td>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtHidedetail"),
                        row.RecID.ToString() + "_" + row.RecID.ToString());
                sb1.AppendFormat("<td style=\"width: 60px; text-align: center;\"><strong>{0}</strong></td>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtComp"));
                sb1.AppendFormat("<td style=\"width: 60px; text-align: center;\"><strong>{0}</strong></td>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtIncPack"));
                sb1.AppendFormat("<td style=\"width: 50px; text-align: center;\"><strong>{0}</strong></td>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtUnit"));
                sb1.AppendFormat("<td style=\"width: 90px; text-align: right;\"><strong>{0}</strong></td>",
                        HttpContext.GetGlobalResourceObject("MakeReservation", "resSerExtPrice"));
                if (_serviceEdit.HasValue && _serviceEdit.Value)
                    sb1.Append("<td style=\"width: 50px;\">");
                sb1.Append("</tr>");

                foreach (TvBo.ResServiceExtRecord r in showResServiceExt)
                {
                    ServiceExtMarOpt showing = showExtraService.Find(f => f.ServiceCode == r.ExtService && f.ShowInResDetB2B.Value == false);
                    if (showing == null)
                    {
                        showExtService = true;
                        sb1.AppendFormat("<tr><td>{0}</td>", UserData.EqMarketLang ? r.ExtServiceNameL : r.ExtServiceName);
                        compulsoryStr = Equals(r.Compulsory, "Y") ? "<img alt=\"\" src=\"Images/accept.gif\" />" : "&nbsp;";
                        sb1.AppendFormat("<td style=\"width: 60px; text-align: center;\">{0}</td>", compulsoryStr);
                        IncludePackStr = Equals(r.IncPack, "Y") ? "<img alt=\"\" src=\"Images/accept.gif\" />" : "&nbsp;";
                        sb1.AppendFormat("<td style=\"width: 60px; text-align: center;\">{0}</td>", IncludePackStr);
                        sb1.AppendFormat("<td style=\"width: 50px; text-align: center;\">{0}</td>", r.Unit);
                        if (!Equals(r.IncPack, "Y") || (ShowIncSrvPrice.HasValue ? ShowIncSrvPrice.Value : false))
                            salePrice = r.SalePrice.HasValue ? r.SalePrice.Value.ToString("#,###.00") : "&nbsp;";
                        else
                            salePrice = "&nbsp;";
                        sb1.AppendFormat("<td style=\"width: 90px; text-align: right;\">{0}</td>", salePrice);
                        sb1.Append("</tr>");
                    }
                }
                sb1.Append("</table>");
                sb1.Append("<td>");
                sb1.Append("</tr>");
            }
            if (showExtService)
                sb.Append(sb1.ToString());
            #endregion
        }
        sb.Append("</table>");
        sb.Append("</div><br /></fieldset>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string setResCustomer(resCustjSonData data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResCustRecord> resCust = ResData.ResCust;

        resCustjSonData cust = new resCustjSonData();
        //data = data.Replace('|', '"').Replace('[', '{').Replace(']', '}').Replace('<', '{').Replace('>', '}');
        //string[] _data = data.Split('@');
        //for (int i = 0; i < _data.Length; i++)
        //{
        cust = data;
        ResCustRecord _cust = resCust.Find(f => f.SeqNo == cust.SeqNo);
        TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
        _cust.Title = cust.Title;
        _cust.TitleStr = _title != null ? _title.Code : "";
        _cust.Surname = cust.Surname;
        _cust.SurnameL = cust.SurnameL;
        _cust.Name = cust.Name;
        _cust.NameL = cust.NameL;
        _cust.Birtday = Conversion.convertDateTime(cust.Birtday.Replace("/", UserData.Ci.DateTimeFormat.DateSeparator).Replace(".", UserData.Ci.DateTimeFormat.DateSeparator).Replace("-", UserData.Ci.DateTimeFormat.DateSeparator),
                                                    strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' '));
        _cust.Age = Conversion.getInt16OrNull(cust.Age);
        _cust.IDNo = cust.IDNo;
        _cust.PassSerie = cust.PassSerie;
        _cust.PassNo = cust.PassNo;
        _cust.PassIssueDate = Conversion.getDateTimeOrNull(cust.PassIssueDate);
        _cust.PassExpDate = Conversion.getDateTimeOrNull(cust.PassExpDate);
        _cust.PassGiven = cust.PassGiven;
        _cust.Phone = cust.Phone;
        if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
        {
            if (!string.IsNullOrEmpty(cust.Nationality))
                _cust.Nationality = cust.Nationality;
        }
        else
        {
            if (cust.Nation.HasValue)
                _cust.Nation = cust.Nation;
        }
        _cust.HasPassport = cust.Passport;
        _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";
        //}

        HttpContext.Current.Session["ResData"] = ResData;

        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string setResCustomers(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        List<ResCustRecord> resCust = ResData.ResCust;

        resCustjSonDataF cust = new resCustjSonDataF();
        data = data.Replace('|', '"').Replace('[', '{').Replace(']', '}').Replace('<', '{').Replace('>', '}');
        string[] _data = data.Split('@');
        for (int i = 0; i < _data.Length; i++)
        {
            cust = Newtonsoft.Json.JsonConvert.DeserializeObject<resCustjSonDataF>(_data[i]);
            ResCustRecord _cust = resCust.Find(f => f.SeqNo == cust.SeqNo);
            TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
            _cust.Title = cust.Title;
            _cust.TitleStr = _title != null ? _title.Code : "";
            _cust.Surname = cust.Surname;
            _cust.SurnameL = cust.SurnameL;
            _cust.Name = cust.Name;
            _cust.NameL = cust.NameL;
            _cust.Birtday = Conversion.convertDateTime(cust.Birtday.Replace("/", UserData.Ci.DateTimeFormat.DateSeparator).Replace(".", UserData.Ci.DateTimeFormat.DateSeparator).Replace("-", UserData.Ci.DateTimeFormat.DateSeparator),
                                                        strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' '));
            _cust.Age = Conversion.getInt16OrNull(cust.Age);
            _cust.IDNo = cust.IDNo;
            _cust.PassSerie = cust.PassSerie;
            _cust.PassNo = cust.PassNo;
            _cust.PassIssueDate = Conversion.getDateTimeOrNull(cust.PassIssueDate);
            _cust.PassExpDate = Conversion.getDateTimeOrNull(cust.PassExpDate);
            _cust.PassGiven = cust.PassGiven;
            _cust.Phone = cust.Phone;
            if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
            {
                if (!string.IsNullOrEmpty(cust.Nationality))
                    _cust.Nationality = cust.Nationality;
            }
            else
            {
                if (cust.Nation.HasValue)
                    _cust.Nation = cust.Nation;
            }
            _cust.HasPassport = cust.Passport;
            _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";
        }

        HttpContext.Current.Session["ResData"] = ResData;

        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string setResCustInfo(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        string _data = data.Replace('<', '{').Replace('>', '}').Replace('|', '"');
        TvBo.ResCustInfojSon resCustInfojSon = Newtonsoft.Json.JsonConvert.DeserializeObject<TvBo.ResCustInfojSon>(_data);

        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == Conversion.getInt32OrNull(resCustInfojSon.CustNo));
        List<ResCustInfoRecord> _ResCustInfo = ResData.ResCustInfo;

        ResCustInfoRecord resCustInfoRec = new ResCustInfoRecord();
        if (ResData.ResCustInfo.Count > 0 && _ResCustInfo.Find(f => f.CustNo == resCust.CustNo) != null)
        {
            resCustInfoRec = _ResCustInfo.Find(f => f.CustNo == resCust.CustNo);
            resCustInfoRec.MobTel = strFunc.Trim(resCust.Phone, ' ');
        }
        else
        {
            resCustInfoRec.CustNo = resCust.CustNo;
            resCustInfoRec.CTitle = resCust.Title;
            resCustInfoRec.RecID = _ResCustInfo.Count > 0 ? _ResCustInfo.LastOrDefault().RecID + 1 : 1;
            resCustInfoRec.RecordID = resCustInfoRec.RecID;
            resCustInfoRec.MobTel = strFunc.Trim(resCust.Phone, ' ');
            IntCountryListRecord country = new TvBo.Common().getCountryIntCode(UserData.Market, UserData.Country, ref errorMsg);
            resCustInfoRec.AddrHomeCountryCode = country != null ? country.IntCode : string.Empty;
            resCustInfoRec.AddrWorkCountryCode = country != null ? country.IntCode : string.Empty;
            resCustInfoRec.MemTable = false;

            _ResCustInfo.Add(resCustInfoRec);
        }
        resCustInfoRec.CTitle = Conversion.getInt16OrNull(resCustInfojSon.CTitle);
        resCustInfoRec.CName = resCustInfojSon.CName;
        resCustInfoRec.CSurName = resCustInfojSon.CSurName;
        resCustInfoRec.MobTel = strFunc.Trim(resCustInfojSon.MobTel, ' ');
        resCust.Phone = string.IsNullOrEmpty(resCustInfojSon.MobTel) ? resCust.Phone : resCustInfojSon.MobTel;
        resCustInfoRec.ContactAddr = resCustInfojSon.ContactAddr;
        resCustInfoRec.InvoiceAddr = resCustInfojSon.InvoiceAddr;
        resCustInfoRec.AddrHome = resCustInfojSon.AddrHome;
        resCustInfoRec.AddrHomeCity = resCustInfojSon.AddrHomeCity;
        resCustInfoRec.AddrHomeCountry = resCustInfojSon.AddrHomeCountry;
        resCustInfoRec.AddrHomeCountryCode = resCustInfojSon.AddrHomeCountryCode;
        resCustInfoRec.AddrHomeZip = resCustInfojSon.AddrHomeZip;
        resCustInfoRec.AddrHomeTel = strFunc.Trim(resCustInfojSon.AddrHomeTel, ' ');
        resCustInfoRec.AddrHomeFax = strFunc.Trim(resCustInfojSon.AddrHomeFax, ' ');
        resCustInfoRec.AddrHomeEmail = resCustInfojSon.AddrHomeEmail;
        resCustInfoRec.HomeTaxOffice = resCustInfojSon.HomeTaxOffice;
        resCustInfoRec.HomeTaxAccNo = resCustInfojSon.HomeTaxAccNo;
        resCustInfoRec.WorkFirmName = resCustInfojSon.WorkFirmName;
        resCustInfoRec.AddrWork = resCustInfojSon.AddrWork;
        resCustInfoRec.AddrWorkCity = resCustInfojSon.AddrWorkCity;
        resCustInfoRec.AddrWorkCountry = resCustInfojSon.AddrWorkCountry;
        resCustInfoRec.AddrWorkCountryCode = resCustInfojSon.AddrWorkCountryCode;
        resCustInfoRec.AddrWorkZip = resCustInfojSon.AddrWorkZip;
        resCustInfoRec.AddrWorkTel = strFunc.Trim(resCustInfojSon.AddrWorkTel, ' ');
        resCustInfoRec.AddrWorkFax = strFunc.Trim(resCustInfojSon.AddrWorkFax, ' ');
        resCustInfoRec.AddrWorkEMail = resCustInfojSon.AddrWorkEMail;
        resCustInfoRec.WorkTaxOffice = resCustInfojSon.WorkTaxOffice;
        resCustInfoRec.WorkTaxAccNo = resCustInfojSon.WorkTaxAccNo;
        resCustInfoRec.Bank = resCustInfojSon.Bank;
        resCustInfoRec.BankAccNo = resCustInfojSon.BankAccNo;
        resCustInfoRec.BankIBAN = resCustInfojSon.BankIBAN;

        HttpContext.Current.Session["ResData"] = ResData;

        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string setCustomer(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);

        try
        {
            resCustjSonDataF cust = new resCustjSonDataF();
            data = data.Replace('|', '"').Replace('<', '{').Replace('>', '}');
            string[] _data = data.Split('@');
            for (int i = 0; i < _data.Length; i++)
            {
                cust = Newtonsoft.Json.JsonConvert.DeserializeObject<resCustjSonDataF>(_data[i]);
                ResCustRecord _cust = ResData.ResCust.Find(f => f.SeqNo == cust.SeqNo);
                TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
                _cust.Title = cust.Title;
                _cust.TitleStr = _title != null ? _title.Code : "";
                _cust.Surname = cust.Surname;
                _cust.Name = cust.Name;
                _cust.Birtday = Conversion.getDateTimeOrNull(cust.Birtday);
                _cust.Age = Conversion.getInt16OrNull(cust.Age);
                _cust.IDNo = cust.IDNo;
                _cust.PassSerie = cust.PassSerie;
                _cust.PassNo = cust.PassNo;
                _cust.Phone = cust.Phone;
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                {
                    if (!string.IsNullOrEmpty(cust.Nationality))
                        _cust.Nationality = cust.Nationality;
                }
                else
                {
                    if (cust.Nation.HasValue)
                        _cust.Nation = cust.Nation;
                }
                _cust.HasPassport = cust.Passport;
                _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";
            }

            HttpContext.Current.Session["ResData"] = ResData;
            return "OK";
        }
        catch
        {
            return "NO";
        }

    }

    [WebMethod(EnableSession = true)]
    public static string setCustomers(string data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);

        try
        {
            resCustjSonDataF cust = new resCustjSonDataF();
            data = data.Replace('|', '"').Replace('<', '{').Replace('>', '}');
            string[] _data = data.Split('@');
            for (int i = 0; i < _data.Length; i++)
            {
                cust = Newtonsoft.Json.JsonConvert.DeserializeObject<resCustjSonDataF>(_data[i]);
                ResCustRecord _cust = ResData.ResCust.Find(f => f.SeqNo == cust.SeqNo);
                TitleRecord _title = ResData.Title.Find(f => f.TitleNo == cust.Title);
                _cust.Title = cust.Title;
                _cust.TitleStr = _title != null ? _title.Code : "";
                _cust.Surname = cust.Surname;
                _cust.Name = cust.Name;
                _cust.Birtday = Conversion.getDateTimeOrNull(cust.Birtday);
                _cust.Age = Conversion.getInt16OrNull(cust.Age);
                _cust.IDNo = cust.IDNo;
                _cust.PassSerie = cust.PassSerie;
                _cust.PassNo = cust.PassNo;
                _cust.Phone = cust.Phone;
                if (Convert.ToDecimal(UserData.TvVersion) > Convert.ToDecimal("040071120"))
                {
                    if (!string.IsNullOrEmpty(cust.Nationality))
                        _cust.Nationality = cust.Nationality;
                }
                else
                {
                    if (cust.Nation.HasValue)
                        _cust.Nation = cust.Nation;
                }
                _cust.HasPassport = cust.Passport;
                _cust.Leader = cust.Leader.HasValue ? (cust.Leader.Value ? "Y" : "N") : "N";
            }

            HttpContext.Current.Session["ResData"] = ResData;
            return "OK";
        }
        catch
        {
            return "NO";
        }

    }

    [WebMethod(EnableSession = true)]
    public static string searchCustomer(string refNo, string SeqNo, string Surname, string Name, string BirthDate)
    {
        /*
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        DateTime? birthDate = Conversion.getDateTimeOrNull(BirthDate);

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        string dateFormat = new TvBo.Common().getDateFormat(UserData.Ci);

        SearchCustomerOption searchOptions = SearchCustomerOption.AllData;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            searchOptions = SearchCustomerOption.OnlyAgency;
        else
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
                searchOptions = SearchCustomerOption.OnlyMainAgency;
            else
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                    searchOptions = SearchCustomerOption.OnlyMarket;
                else
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Dallas))
                        searchOptions = SearchCustomerOption.AllData;
                    else
                        searchOptions = SearchCustomerOption.OnlyAgency;

        List<ResCustRecord> list = new Reservation().searchCustomers(UserData, searchOptions, Surname, Name, birthDate, ref errorMsg);
        if (list != null && list.Count > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div id=\"divSearcCustList\">");
            sb.Append("<table>");
            sb.Append("<tr class=\"sercahCustListHeader\">");
            sb.Append("<td class=\"sselectH\">&nbsp;</td>");
            sb.AppendFormat("<td class=\"sSurnameH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustSurname"));
            sb.AppendFormat("<td class=\"sNameH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustName"));
            sb.AppendFormat("<td class=\"sBirthDateH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustBirthDate"));
            sb.AppendFormat("<td class=\"sPassSerieH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustPassSerie"));
            sb.AppendFormat("<td class=\"sPassNoH\">{0}</td>",
                    HttpContext.GetGlobalResourceObject("MakeReservation", "searchCustPassNo"));
            sb.Append("</tr>");

            foreach (ResCustRecord row in list)
            {
                sb.Append("<tr class=\"sercahCustListRow\">");
                sb.AppendFormat("<td class=\"sselect\"><input type=\"button\" value=\"{0}\" onclick=\"searchSelectCust('{1}')\" /></td>",
                        HttpContext.GetGlobalResourceObject("LibraryResource", "btnSelect"),
                        row.CustNo.ToString() + ";" + SeqNo + ";" + refNo);
                sb.AppendFormat("<td class=\"sSurname\">{0}</td>", row.Surname);
                sb.AppendFormat("<td class=\"sName\">{0}</td>", row.Name);
                sb.AppendFormat("<td class=\"sBirthDate\">{0}</td>", row.Birtday.HasValue ? row.Birtday.Value.ToString(dateFormat) : "&nbsp;");
                sb.AppendFormat("<td class=\"sPassSerie\">{0}</td>", row.PassSerie);
                sb.AppendFormat("<td class=\"sPassNo\">{0}</td>", row.PassNo);
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }
        else 
        */
        return "";
    }

    [WebMethod(EnableSession = true)]
    public static string copyCustomer(string CustNo, string Custs)
    {
        int? oldCustNo = Conversion.getInt32OrNull(CustNo.Split(';')[0]);
        int? newCustNo = Conversion.getInt32OrNull(CustNo.Split(';')[1]);

        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (setCustomers(Custs) != "OK") return "";

        string errorMsg = string.Empty;
        ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResCustRecord> resCust = ResData.ResCust;
        List<ResCustInfoRecord> resCustInfo = ResData.ResCustInfo;

        ResCustRecord oldCust = new ResTables().getResCustRecord(UserData, oldCustNo, ref errorMsg);
        ResCustInfoRecord oldCustInfo = new ResTables().getResCustInfoRecord(oldCustNo, ref errorMsg);

        ResCustRecord newCust = resCust.Find(f => f.SeqNo == newCustNo);
        ResCustInfoRecord newCustInfo = resCustInfo.Find(f => f.CustNo == (newCust != null ? newCust.CustNo : -1));
        if (newCust != null)
        {
            Int16? seqNo = newCust.SeqNo;
            int custNo = newCust.CustNo;
            int? custNoT = newCust.CustNoT;

            newCust.Title = oldCust.Title;
            newCust.TitleStr = oldCust.TitleStr;
            newCust.Surname = oldCust.Surname;
            newCust.SurnameL = oldCust.SurnameL;
            newCust.Name = oldCust.Name;
            newCust.NameL = oldCust.NameL;

            newCust.Birtday = oldCust.Birtday;
            newCust.Age = oldCust.Age;

            newCust.HasPassport = oldCust.HasPassport;
            newCust.IDNo = oldCust.IDNo;
            newCust.Nation = oldCust.Nation;
            newCust.NationName = oldCust.NationName;
            newCust.NationNameL = oldCust.NationNameL;
            newCust.Nationality = oldCust.Nationality;
            newCust.NationalityName = oldCust.NationalityName;
            newCust.PassExpDate = oldCust.PassExpDate;
            newCust.PassGiven = oldCust.PassGiven;
            newCust.PassIssueDate = oldCust.PassIssueDate;
            newCust.PassNo = oldCust.PassNo;
            newCust.PassSerie = oldCust.PassSerie;
            newCust.Phone = strFunc.Trim(oldCust.Phone, ' ');

            if (newCustInfo != null && oldCustInfo != null)
            {
                newCustInfo.AddrHome = oldCustInfo.AddrHome;
                newCustInfo.AddrHomeCity = oldCustInfo.AddrHomeCity;
                newCustInfo.AddrHomeCountry = oldCustInfo.AddrHomeCountry;
                newCustInfo.AddrWorkCountryCode = oldCustInfo.AddrWorkCountryCode;
                newCustInfo.AddrHomeEmail = oldCustInfo.AddrHomeEmail;
                newCustInfo.AddrHomeFax = strFunc.Trim(oldCustInfo.AddrHomeFax, ' ');
                newCustInfo.AddrHomeTel = strFunc.Trim(oldCustInfo.AddrHomeTel, ' ');
                newCustInfo.AddrHomeZip = oldCustInfo.AddrHomeZip;
                newCustInfo.AddrWork = oldCustInfo.AddrWork;
                newCustInfo.AddrWorkCity = oldCustInfo.AddrWorkCity;
                newCustInfo.AddrWorkCountry = oldCustInfo.AddrWorkCountry;
                newCustInfo.AddrWorkCountryCode = oldCustInfo.AddrWorkCountryCode;
                newCustInfo.AddrWorkEMail = oldCustInfo.AddrWorkEMail;
                newCustInfo.AddrWorkFax = strFunc.Trim(oldCustInfo.AddrWorkFax, ' ');
                newCustInfo.AddrWorkTel = strFunc.Trim(oldCustInfo.AddrWorkTel, ' ');
                newCustInfo.AddrWorkZip = oldCustInfo.AddrWorkZip;
                newCustInfo.Bank = oldCustInfo.Bank;
                newCustInfo.BankAccNo = oldCustInfo.BankAccNo;
                newCustInfo.BankIBAN = oldCustInfo.BankIBAN;
                newCustInfo.CName = oldCustInfo.CName;
                newCustInfo.ContactAddr = oldCustInfo.ContactAddr;
                newCustInfo.CSurName = oldCustInfo.CSurName;
                newCustInfo.CTitle = oldCustInfo.CTitle;
                newCustInfo.CTitleName = oldCustInfo.CTitleName;
                newCustInfo.HomeTaxAccNo = oldCustInfo.HomeTaxAccNo;
                newCustInfo.HomeTaxOffice = oldCustInfo.HomeTaxOffice;
                newCustInfo.InvoiceAddr = oldCustInfo.InvoiceAddr;
                newCustInfo.Jobs = oldCustInfo.Jobs;
                newCustInfo.MobTel = oldCustInfo.MobTel.Trim(' ');
                newCust.Phone = newCustInfo.MobTel;
                newCustInfo.Note = oldCustInfo.Note;
                newCustInfo.WorkFirmName = oldCustInfo.WorkFirmName;
                newCustInfo.WorkTaxAccNo = oldCustInfo.WorkTaxAccNo;
                newCustInfo.WorkTaxOffice = oldCustInfo.WorkTaxOffice;
            }
            else
            {
                if (oldCustInfo != null)
                {
                    newCustInfo = new ResCustInfoRecord();
                    newCustInfo = oldCustInfo;
                    newCustInfo.CustNo = custNo;
                    ResData.ResCustInfo.Add(newCustInfo);
                }
            }
            HttpContext.Current.Session["ResData"] = ResData;
        }
        return "OK";
    }

    [WebMethod(EnableSession = true)]
    public static string makeTicket(string BookNr, string Adult, string Child, string Infant, string _dateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["OnlyTicketCriteria"] == null)
        { HttpContext.Current.Response.StatusCode = 408; return null; }
        OnlyTicketCriteria criteria = (OnlyTicketCriteria)HttpContext.Current.Session["OnlyTicketCriteria"];

        StringBuilder sb = new StringBuilder();
        string errorMsg = string.Empty;
        Int16 adult = Conversion.getInt16OrNull(Adult).HasValue ? Conversion.getInt16OrNull(Adult).Value : Convert.ToInt16(1);
        Int16 child = Conversion.getInt16OrNull(Child).HasValue ? Conversion.getInt16OrNull(Child).Value : Convert.ToInt16(0);
        Int16? infant = Conversion.getInt16OrNull(Infant);
        int? bookNr = Conversion.getInt32OrNull(BookNr);
        FlightData flightData = (FlightData)HttpContext.Current.Session["FlightData"];
        OnlyFlight_FlightInfo bookRow = flightData.FlightInfo.Find(f => f.RecID == bookNr);
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            if (((bookRow.DepFreeSeat.HasValue ? bookRow.DepFreeSeat.Value : 0) < (adult + child)) || ((bookRow.ArrFreeSeat.HasValue ? bookRow.ArrFreeSeat.Value : 0) < (adult + child)))
            {
                errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "AllotNotEnough").ToString();
                return "<div style=\"font-size: 12pt; color: #FF0000; padding-bottom: 4px;\"><b>" + errorMsg + "</b></div>";
            }

        if (new Reservation().CreditControl(UserData.AgencyID, UserData.Market, ref errorMsg) == 2)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "NoCreditLimit").ToString();
            return "<div style=\"font-size: 12pt; color: #FF0000; padding-bottom: 4px;\"><b>" + errorMsg + "</b></div>";
        }

        if (UserData.BlackList || UserData.MasterAgency)
        {
            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "NoAuthResForBlacklist").ToString();
            return "<div style=\"font-size: 12pt; color: #FF0000; padding-bottom: 4px;\"><b>" + errorMsg + "</b></div>";
        }
        bool CreateChildAge = true;
        if (UserData.WebService)
            CreateChildAge = true;
        else
        {
            object _createChildAge = new TvBo.Common().getFormConfigValue("MakeReservation", "CreateChildAge");
            CreateChildAge = _createChildAge != null ? (bool)_createChildAge : false;
        }

        ResDataRecord ResData = new OnlyTickets().getFlightTicket(UserData, criteria, bookRow, adult, child, infant, CreateChildAge, ref errorMsg);

        if (!string.IsNullOrEmpty(errorMsg))
            return "<div style=\"font-size: 12pt; color: #FF0000; padding-bottom: 4px;\"><b>" + errorMsg + "</b></div>";

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Enka))
        {
            ResMainRecord resMain = ResData.ResMain;
            resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
        }

        HttpContext.Current.Session["ResData"] = ResData;

        string retVal = string.Empty;
        retVal += "<fieldset><legend><label>»</label>";
        retVal += string.Format("<span class=\"lblGridResCust\">{0}</span></legend>",
                                HttpContext.GetGlobalResourceObject("MakeReservation", "lblGridResCust"));
        retVal += createTicketCustStr();
        retVal += "</fieldset>";
        retVal += getResService();
        retVal += createSaveSection(UserData, ResData);
        Guid? logID = null;
        String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
        if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
        {
            if (flightData.LogID.HasValue)
                logID = new WEBLog().saveWEBBookLog(flightData.LogID.Value, DateTime.Now, null, ref errorMsg);
        }
        retVal += string.Format("<input id=\"logID\" type=\"hidden\" value=\"{0}\" />", logID.HasValue ? logID.ToString() : string.Empty, string.Empty);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string saveReservation(string data, string Note, string customerCode, string dosier, string agencyDiscount, string invoiceTo, string saveOption, string optionTime, Guid? logID,
                        string Code1, string Code2, string Code3, string Code4)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        setCustomers(data);

        string errorMsg = string.Empty;
        List<ReservastionSaveErrorRecord> returnData = new List<ReservastionSaveErrorRecord>();
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        ResMainRecord resMain = ResData.ResMain;
        bool saveAsDraft = false;
        bool? setOptionTime = null;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            if (string.Equals(optionTime, "1"))
                setOptionTime = true;
            else setOptionTime = false;
        }

        if (Conversion.getBoolOrNull(saveOption).HasValue)
            saveAsDraft = Conversion.getBoolOrNull(saveOption).Value;

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Mng_Tr))
        {
            if (Conversion.getByteOrNull(invoiceTo).HasValue)
                resMain.InvoiceTo = Conversion.getByteOrNull(invoiceTo).Value;
        }

        resMain.ResNote = Note.Replace('|', '"');
        decimal? agencyDisPasPer = Conversion.getDecimalOrNull(agencyDiscount);
        resMain.AgencyDisPasPer = agencyDisPasPer;
        resMain.AceCustomerCode = customerCode;
        resMain.AceTfNumber = dosier;

        resMain.Code1 = Code1;
        resMain.Code2 = Code2;
        resMain.Code3 = Code3;
        resMain.Code4 = Code4;

        Int16 CreditCont = new TvBo.Reservation().CreditControl(UserData.AgencyID, UserData.Market, ref errorMsg);

        if (CreditCont == 2)
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoCreditLimit").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }

        if (ResData.ResService.Count < 1)
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "NoResServiceMsg").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }

        if (!(new Flights().CheckFlightTime(UserData, ResData.ResService, ref errorMsg)))
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = HttpContext.GetGlobalResourceObject("LibraryResource", "CheckInTimeOver").ToString()
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }

        List<CustControlErrorRecord> custControl = new Reservation().CustControl(UserData, ResData, ref errorMsg);
        if (custControl == null || custControl.Count == 1)
        {
            resMain.OptDate = new Reservation().getOptionDate(UserData, ResData, true, ref errorMsg);
            if (setOptionTime.HasValue && setOptionTime.Value == false)
                resMain.OptDate = null;

            returnData = new Reservation().SaveReservation(UserData, ref ResData, saveAsDraft);

            if (returnData == null || returnData.Count < 1)
            {

                returnData.Add(new ReservastionSaveErrorRecord
                {
                    ControlOK = false,
                    Message = HttpContext.GetGlobalResourceObject("LibraryResource", "ResNotSaved").ToString()
                });
                return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
            }
            else
            {
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    //new Reservation().setResConfirmation(ResData, ResData.ResMain.ResNo, ref errorMsg);
                    if (UserData.AgencyRec != null && UserData.AgencyRec.AceExport)
                    {
                        string AceFileName = string.Empty;
                        if (!(new Aces().SendAceTo(UserData, ResData, AppDomain.CurrentDomain.BaseDirectory + "ACE//", ref AceFileName, ref errorMsg)))
                        {
                        }
                    }

                    DateTime OptionDate = DateTime.Now;

                    List<resPayPlanRecord> resPayPlan = new ReservationMonitor().getPaymentPlan(ResData.ResMain.ResNo, ref errorMsg);
                    string paymentPlanStr = new TvBo.ReservationMonitor().getPaymentPlanHTML(ResData.ResMain.ResNo, UserData.Ci.LCID);
                    string _Message = string.Empty;
                    resPayPlanRecord firstPayment = resPayPlan.OrderBy(o => o.DueDate).FirstOrDefault();
                    if (resPayPlan != null && resPayPlan.Count > 0)
                    {
                        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                            _Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />" + paymentPlanStr,
                                                                ResData.ResMain.ResNo);
                        else
                        {
                            string paidMessage = string.Format(HttpContext.GetGlobalResourceObject("MakeReservation", "lblAmount").ToString(),
                                                                firstPayment.Amount.Value.ToString("#,###.00") + " " + firstPayment.Cur,
                                                                firstPayment.DueDate.ToShortDateString());
                            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Orex))
                                paidMessage = string.Empty;
                            _Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString() + "<br />{1}",
                                                                ResData.ResMain.ResNo,
                                                                paidMessage);
                        }

                    }
                    else
                        _Message = string.Format(HttpContext.GetGlobalResourceObject("LibraryResource", "ResIsSave").ToString(), ResData.ResMain.ResNo);

                    returnData[0].ControlOK = true;
                    returnData[0].Message = _Message;
                    returnData[0].SendEmailB2C = getEmailUrl(UserData, ResData);
                    returnData[0].GotoPaymentPage = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran);
                    returnData[0].PaymentPageUrl = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ? "Payments/BeginPayment.aspx" : (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran) ? "Payments/Safiran/SafiranPayment.aspx" : "");
                    returnData[0].DirectPaymentPage = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran);
                    returnData[0].ResNo = ResData.ResMain.ResNo;

                    String useLog = System.Configuration.ConfigurationManager.AppSettings["useLog"];
                    if (!string.IsNullOrEmpty(useLog) && string.Equals(useLog, "1"))
                    {
                        if (logID.HasValue)
                            new WEBLog().updateWEBBookLog(logID.Value, DateTime.Now, ResData.ResMain.ResNo, ref errorMsg);
                    }

                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
                }
            }
        }
        else
        {
            returnData.Add(new ReservastionSaveErrorRecord
            {
                ControlOK = false,
                Message = new Reservation().getCustControlErrorMessage(custControl)
            });
            return Newtonsoft.Json.JsonConvert.SerializeObject(returnData);
        }
    }

    public static string getEmailUrl(User UserData, ResDataRecord ResData)
    {
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        try
        {
            if (string.Equals(UserData.CustomRegID, Common.crID_CelexTravel))
            {
                ResServiceRecord hotel = ResData.ResService.Where(w => w.ServiceType == "HOTEL").FirstOrDefault();
                if (hotel != null)
                {
                    AsyncMailSenderParams mailParam = new AsyncMailSenderParams();
                    mailParam.SenderMail = UserData.TvParams.TvParamSystem.SMTPAccount;
                    mailParam.DisplayName = UserData.TvParams.TvParamSystem.SMTPAccount;
                    mailParam.SenderPassword = UserData.TvParams.TvParamSystem.SMTPPass;
                    string fromEmails = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "resSendEmails"));
                    mailParam.ToAddress = string.IsNullOrEmpty(fromEmails) ? "celex@celextravel.com;mehmet@celextravel.com;meral@celextravel.com" : fromEmails;
                    mailParam.Subject = "New reservation.Res no: " + ResData.ResMain.ResNo;
                    mailParam.Port = UserData.TvParams.TvParamSystem.SMTPPort;
                    mailParam.SMTPServer = UserData.TvParams.TvParamSystem.SMTPServer;
                    mailParam.EnableSSL = false;
                    string body = ResData.ResMain.ResNo + UserData.AgencyName + hotel.ServiceName + hotel.BegDate.Value.ToString("dd/MM/yyyy") + hotel.EndDate.Value.ToString("dd/MM/yyyy") + hotel.RoomName + hotel.AccomName + hotel.BoardName;
                    mailParam.Body = body;

                    new SendMail().MailSender(mailParam);
                }
                return "";
            }
            else
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                {
                    string url = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "b2cEmailUrl"));
                    url = url.Trim().Trim('\n').Trim('\r');

                    ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                    ResCustInfoRecord leaderInfo = leader != null ? ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo) : null;
                    if (!string.IsNullOrEmpty(url) && leaderInfo != null)
                    {
                        if ((Equals(leaderInfo.ContactAddr, "H") ? (string.IsNullOrEmpty(leaderInfo.AddrHomeEmail) ? true : false) : (string.IsNullOrEmpty(leaderInfo.AddrWorkEMail) ? true : false)) == false)
                        {
                            //url += string.Format("?resNumber={0}&cultureCode={1}&agencyCode={2}",
                            //        ResData.ResMain.ResNo,
                            //        UserData.Ci.Name,
                            //        ResData.ResMain.Agency).ToString();
                            //return string.Format("'serviceUrl':'{0}','ResNo':'{1}','CultureID':'{2}','AgencyID':'{3}'",
                            //                        url,
                            //                        ResData.ResMain.ResNo,
                            //                        UserData.Ci.Name,
                            //                        ResData.ResMain.Agency);



                            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                            {
                                fi.detur.SendMail sm = new fi.detur.SendMail();
                                sm.Url = url;
                                sm.MailSend(ResData.ResMain.ResNo, UserData.Ci.Name, ResData.ResMain.Agency);
                            }
                            return string.Format("'serviceUrl':'{0}','ResNo':'{1}','CultureID':'{2}','AgencyID':'{3}'",
                                                    url,
                                                    ResData.ResMain.ResNo,
                                                    UserData.Ci.Name,
                                                    ResData.ResMain.Agency);
                        }
                        else return string.Empty;

                    }
                    else return string.Empty;
                }
                else return string.Empty;
        }
        catch
        {
            return string.Empty;
        }
    }

}
