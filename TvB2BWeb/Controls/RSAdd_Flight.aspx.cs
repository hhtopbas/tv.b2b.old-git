﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using System.Text;
using TvTools;

public partial class Controls_RSAdd_Flight : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ppcFlyDate.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcFlyDate.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcFlyDate.From.Date = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;

        if (!IsPostBack) {
            ppcFlyDate.DateValue = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
            Int64 days = (ppcFlyDate.DateValue - new DateTime(1970, 1, 1)).Days - 1;
            iDate.Value = (days * 86400000).ToString();
        }
    }

    [WebMethod]
    public static string getRouteFrom(string RecordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        List<FlightAndDaysRecord> depFlights = new Flights().getDepartureFlightAndLocation(UserData.Operator, UserData.Market, null, ref errorMsg);
        if (string.IsNullOrEmpty(ResData.ResMain.PackType) || string.Equals(UserData.CustomRegID, Common.crID_Elsenal)) {
            var query = from q in depFlights
                        where q.DepCity == ResData.ResMain.DepCity || q.ArrCity == ResData.ResMain.ArrCity ||
                              q.ArrCity == ResData.ResMain.DepCity || q.DepCity == ResData.ResMain.ArrCity
                        group q by new { RecID = q.DepCity, Name = q.DepCityNameL + " (" + q.DepAirportNameL + ")" } into k
                        select new { RecID = k.Key.RecID, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        } else if (string.Equals(ResData.ResMain.PackType, "O")) {
            var query = from q in depFlights
                        where q.DepCity == ResData.ResMain.ArrCity || q.ArrCity == ResData.ResMain.ArrCity
                        group q by new { RecID = q.DepCity, Name = q.DepCityNameL + " (" + q.DepAirportNameL + ")" } into k
                        select new { RecID = k.Key.RecID, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        } else {
            var query = from q in depFlights
                        where q.DepCity == ResData.ResMain.DepCity || q.DepCity == ResData.ResMain.ArrCity
                        group q by new { RecID = q.DepCity, Name = q.DepCityNameL + " (" + q.DepAirportNameL + ")" } into k
                        select new { RecID = k.Key.RecID, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod]
    public static string getRouteTo(string RouteFrom)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        Int32? routeFrom = Conversion.getInt32OrNull(RouteFrom);
        if (routeFrom.HasValue) {
            List<FlightAndDaysRecord> depFlights = new Flights().getDepartureFlightAndLocation(UserData.Operator, UserData.Market, routeFrom, ref errorMsg);
            if (string.IsNullOrEmpty(ResData.ResMain.PackType)) {
                var query = from q in depFlights
                            group q by new { RecID = q.ArrCity, Name = q.ArrCityNameL + " (" + q.ArrAirportNameL + ")" } into k
                            select new { RecID = k.Key.RecID, Name = k.Key.Name };
                return Newtonsoft.Json.JsonConvert.SerializeObject(query);
            } else {
                var query = from q in depFlights
                                //where q.DepCity == ResData.ResMain.DepCity || q.ArrCity == ResData.ResMain.ArrCity
                            group q by new { RecID = q.ArrCity, Name = q.ArrCityNameL + " (" + q.ArrAirportNameL + ")" } into k
                            select new { RecID = k.Key.RecID, Name = k.Key.Name };
                return Newtonsoft.Json.JsonConvert.SerializeObject(query);
            }
        } else
            return string.Empty;
    }

    [WebMethod]
    public static string getFlights(string RouteFrom, string RouteTo, string FlyDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32? routeFrom = Conversion.getInt32OrNull(RouteFrom);
        Int32? routeTo = Conversion.getInt32OrNull(RouteTo);
        Int32 _dateA = Convert.ToInt32((Convert.ToInt64(FlyDate) / 86400000).ToString());
        DateTime _dateB = new DateTime(1970, 1, 1);
        DateTime? flyDate = _dateB.AddDays(_dateA + 1);
        List<FlightDayRecord> flights = new Flights().getRouteFlights(UserData, UserData.Market, flyDate, routeFrom, routeTo, null, ref errorMsg);
        var query = from q in flights
                    group q by new { FlightNo = q.FlightNo, Name = q.FlightNo + " (" + q.DepAirport + "=>" + q.ArrAirport + ")" } into k
                    select new { FlightNo = k.Key.FlightNo, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod]
    public static string getClass()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        List<FlightClassRecord> flightClass = new Flights().getFlightClass(UserData.Market, ref errorMsg);
        var query = from q in flightClass
                    where q.B2B
                    select q;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    where s.Status == 0
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod]
    public static string CalcService(string selectedCusts, string Departure, string Arrival, string Flight, string SClass, string FlyDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? routeFrom = Conversion.getInt32OrNull(Departure);
        Int32? routeTo = Conversion.getInt32OrNull(Arrival);
        Int32 _dateA = Convert.ToInt32((Convert.ToInt64(FlyDate) / 86400000).ToString());
        DateTime _dateB = new DateTime(1970, 1, 1);
        DateTime? flyDate = _dateB.AddDays(_dateA + 1);
        if (!flyDate.HasValue)
            return retVal;

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] selectedCust = selectedCusts.Split('|');
        TvBo.FlightDayRecord FlightDay = new Flights().getFlightDay(UserData, Flight, flyDate.Value, ref errorMsg);

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0) {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        } else {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((flyDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, flyDate.Value, flyDate.Value, "FLIGHT", Flight,
                                string.Empty, string.Empty, string.Empty, SClass, Night, StartDay, Night, routeFrom, routeTo, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg)) {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        } else {
            int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
            ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
            DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "FLIGHT", ServiceID, ref errorMsg);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"Adult\":\"{2}\",\"Child\":\"{3}\",\"Unit\":\"{4}\"",
                    returnData.Rows[0]["SalePrice"].ToString() + " " + returnData.Rows[0]["SaleCur"].ToString(),
                    supplierRec.NameL,
                    AdlCnt,
                    ChdCnt,
                    service.Unit);
        }
        return "{" + retVal + "}";
    }

    [WebMethod]
    public static string SaveService(string selectedCusts, string Departure, string Arrival, string Flight, string SClass, string FlyDate, string recordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        Int32? routeFrom = Conversion.getInt32OrNull(Departure);
        Int32? routeTo = Conversion.getInt32OrNull(Arrival);
        Int32 _dateA = Convert.ToInt32((Convert.ToInt64(FlyDate) / 86400000).ToString());
        DateTime _dateB = new DateTime(1970, 1, 1);
        DateTime? flyDate = _dateB.AddDays(_dateA + 1);
        if (!flyDate.HasValue)
            return retVal;

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] selectedCust = selectedCusts.Split('|');
        TvBo.FlightDayRecord FlightDay = new Flights().getFlightDay(UserData, Flight, flyDate.Value, ref errorMsg);

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0) {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        } else {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((flyDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, flyDate.Value, flyDate.Value, "FLIGHT", Flight,
                                string.Empty, string.Empty, string.Empty, SClass, Night, StartDay, Night, routeFrom, routeTo, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg)) {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        } else {
            if (_recordType == true) {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg)) {
                    List<ResServiceRecord> resServiceList = ResData.ResService;
                    ResServiceRecord resService = resServiceList.LastOrDefault();
                    resService.ExcludeService = false;
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                } else {
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
            } else {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK) {
                    HttpContext.Current.Session["ResData"] = ResData;
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData) {
                        if (!row.ControlOK)
                            Msg += row.Message + "<br />";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                } else {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData) {
                        if (!row.ControlOK)
                            Msg += row.Message + "\n";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
            }
        }
        return "{" + retVal + "}";
    }
}
