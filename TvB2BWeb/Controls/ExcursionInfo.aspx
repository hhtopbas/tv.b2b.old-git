﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExcursionInfo.aspx.cs" Inherits="Controls_ExcursionInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= GetGlobalResourceObject("Controls", "viewExcursion")%></title>

    <style type="text/css">
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td { margin: 0; padding: 0; border: 0; outline: 0; font-size: 100%; text-decoration: none; }
        body { line-height: 1; }
        html { overflow: auto; }
        ol, ul { list-style: none; }
        blockquote, q { quotes: none; }
        :focus, a:focus, a:active { outline: 0; }
        input { outline: 0; }
        ins { text-decoration: none; }
        del { text-decoration: line-through; }
        table { border-collapse: collapse; border-spacing: 0; }
        /*Sifirlama */
        html, body { height: 100%; min-height: 550px; font: normal 8pt/10pt tahoma, verdana, sans-serif; }
        .Page { height: 100%; width: 1002px; margin: 0 auto 0 auto; position: relative; }
        .Content { clear: both; width: 1000px; min-height: 580px; display: inline-block; /*position: relative; */ padding-bottom: 50px; margin-bottom: 0; }
        .Footer { margin: 0 auto 0 auto; width: 1000px; position: relative; margin-bottom: -50px; height: 50px; }
        .border { border-top: 1px solid #fff; border-left: 1px solid #fff; border-bottom: 1px solid #a6cbe5; border-right: 1px solid #a6cbe5; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    </form>
</body>
</html>
