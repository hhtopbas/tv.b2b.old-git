﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvTools;

public partial class Controls_Ssrc : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
    }

    [WebMethod(EnableSession = true)]
    public static string getSsrc(string CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        int? custNo = Conversion.getInt32OrNull(CustNo);
        StringBuilder sb = new StringBuilder();
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResCustRecord resCust = ResData.ResCust.Find(f => f.CustNo == Conversion.getInt32OrNull(CustNo));
        if (resCust == null)
            return HttpContext.GetGlobalResourceObject("SpCalcErrors", "CalcService4").ToString();
        List<ResServiceRecord> resServiceCust = (from q1 in ResData.ResService
                                                 join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                 where q2.CustNo == custNo
                                                 select q1).ToList<ResServiceRecord>();
        List<string> ssrcServiceType = new Ssrcs().getSsrcServiceTypes(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);
        List<SpecSerRQCodeRecord> SrrcList = new Ssrcs().getFreeSrrCodes(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);
        var query = from q1 in resServiceCust
                    join q2 in ssrcServiceType on q1.ServiceType equals q2
                    select q1;
        if (query.Count() > 0)
        {
            List<string> roundTrip = new List<string>();
            roundTrip.Add("FLIGHT"); roundTrip.Add("TRANSFER"); roundTrip.Add("TRANSPORT");
            int cnt = -1;
            foreach (var row in query)
            {
                ResConRecord resConRec = ResData.ResCon.Find(f => f.ServiceID == row.RecID && f.CustNo == custNo);
                cnt++;
                sb.Append("<div class=\"ServiceSsrc\">");
                sb.AppendFormat("<input id=\"serviceID{0}\" type=\"hidden\" value=\"{1}\">", cnt, row.RecID);
                string ServiceDesc = string.Empty;
                switch (row.ServiceType)
                {
                    case "HOTEL": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "FLIGHT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "TRANSPORT": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "TRANSFER": ServiceDesc += ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "RENTING": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "EXCURSION": ServiceDesc += row.BegDate.Value.ToShortDateString(); break;
                    case "INSURANCE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "VISA": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    case "HANDFEE": ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                    default: ServiceDesc += row.BegDate.Value.ToShortDateString() + "-" + row.EndDate.Value.ToShortDateString(); break;
                }
                sb.AppendFormat("<h3>{0}</h3>", row.ServiceNameL + " (" + ServiceDesc + ")");
                sb.Append("<div class=\"SsrcDiv\">");

                sb.AppendFormat("<strong>{0}</strong><br />",
                    HttpContext.GetGlobalResourceObject("Controls", "lblSpecialServiceRequestCode"));
                
                string roundTripServices = string.Empty;
                if (roundTrip.Contains(row.ServiceType))
                {
                    roundTripServices = string.Format("roundTrip=\"{0}\" first=\"{1}\"",
                                                    row.ServiceType,
                                                    query.Where(w => string.Equals(w.ServiceType, row.ServiceType)).FirstOrDefault().Service == row.Service ? "1" : "0");
                }
                string ssrcListStr1 = string.Format("<option value=\"\">{0}</option>", "");
                foreach (SpecSerRQCodeRecord ssrcRec in SrrcList.Where(w => w.ServiceType == row.ServiceType && (string.IsNullOrEmpty(w.ServiceItem) || w.ServiceItem == row.Service)).Distinct())
                {
                    string ssrPrice = string.Empty;
                    if (ssrcRec.Priced.HasValue && ssrcRec.Priced.Value)
                    {
                        if (resCust.Title > 7 && ssrcRec.SaleInfPrice.HasValue)
                            ssrPrice = ssrcRec.SaleInfPrice.Value.ToString("#,###.00") + " " + ssrcRec.SaleCur;
                        else
                            if (resCust.Title > 5 && ssrcRec.SaleChdPrice.HasValue)
                                ssrPrice = ssrcRec.SaleChdPrice.Value.ToString("#,###.00") + " " + ssrcRec.SaleCur;
                            else
                                ssrPrice = ssrcRec.SaleAdlPrice.Value.ToString("#,###.00") + " " + ssrcRec.SaleCur;
                    }

                    string pricedStr = " (" + (ssrcRec.Priced.HasValue && ssrcRec.Priced.Value ? "Priced" : "Free") + ") " + ssrPrice;
                    if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Kusadasi_Ro))
                        pricedStr = string.Empty;

                    if (ssrcRec.Code == resConRec.SpecSerRQCode1)
                        ssrcListStr1 += string.Format("<option value=\"{0}\" selected=\"selected\">{1}</option>",
                                            ssrcRec.Code,
                                            ssrcRec.NameL + pricedStr);
                    else ssrcListStr1 += string.Format("<option value=\"{0}\">{1}</option>",
                                            ssrcRec.Code,
                                            ssrcRec.NameL + pricedStr);
                }
                sb.AppendFormat("<select id=\"iSsrc1_{0}\" {2} onchange=\"changeiSsrc1(this.value, {0})\" >{1}</select><br />", cnt, ssrcListStr1, roundTripServices);
                sb.Append("</div></div><br />");
            }
            sb.AppendFormat("<input id=\"custNo\" type=\"hidden\" value=\"{0}\">", custNo);
            sb.AppendFormat("<input id=\"recordCnt\" type=\"hidden\" value=\"{0}\">", cnt + 1);
            return sb.ToString();
        }
        else return HttpContext.GetGlobalResourceObject("Controls", "lblSpecialServiceRequestCodeNotFound").ToString();
    }

    [WebMethod(EnableSession = true)]
    public static string saveSsrc(string Data)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        string _data = "[" + Data.Replace('!', '"').Replace("*L", "{").Replace("*R", "}") + "]";
        List<SsrcJson> ssrcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SsrcJson>>(_data);
        List<ResConRecord> resCon = ResData.ResCon;
        foreach (SsrcJson row in ssrcList)
        {
            ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == row.ServiceID);
            ResConRecord resConRec = resCon.Find(f => f.CustNo == row.CustNo && f.ServiceID == row.ServiceID);                        

            SpecSerRQCodeRecord ssr1 = new Ssrcs().getSpecialRequest(UserData.Market, ResData.ResMain.PLMarket, resService.Service, row.SpecSerRQCode1, ref errorMsg);
            if (ssr1 != null && ssr1.Priced.HasValue && ssr1.Priced.Value)
            {
                ResData = new Reservation().setSsrcExtraService(UserData, ResData, row.CustNo, row.ServiceID, ssr1.ExtServicePriceID, ref errorMsg);
                
                SpecSerRQCodeRecord ssr2 = new Ssrcs().getSpecialRequest(UserData.Market, ResData.ResMain.PLMarket, resService.Service, row.SpecSerRQCode2, ref errorMsg);
                if (ssr2 != null && ssr2.Priced.HasValue && ssr2.Priced.Value)
                    ResData = new Reservation().setSsrcExtraService(UserData, ResData, row.CustNo, row.ServiceID, ssr2.ExtServicePriceID, ref errorMsg);

                if (string.IsNullOrEmpty(errorMsg))
                {
                    if (!row.Real)
                    {
                        if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                        {
                            resConRec.Remark = row.Remark;
                            resConRec.SpecSerRQCode1 = row.SpecSerRQCode1;
                            resConRec.SpecSerRQCode2 = row.SpecSerRQCode2;
                            if (row.Real)
                            {
                                new Reservation().updateResConRecord(UserData, resConRec, ref errorMsg);
                            }
                            HttpContext.Current.Session["ResData"] = ResData;
                        }
                        //return errorMsg;
                    }
                    else
                    {
                        List<TvBo.ReservastionSaveErrorRecord> returnData = new Reservation().UpdateReservation(UserData, ref ResData);

                        if (returnData.Count == 0 || (returnData.Count == 1 && (bool)returnData[0].ControlOK))
                        {
                            HttpContext.Current.Session["ResData"] = ResData;
                            //return errorMsg;
                        }
                        else
                        {
                            string Msg = string.Empty;
                            foreach (ReservastionSaveErrorRecord row1 in returnData)
                            {
                                if (!row1.ControlOK)
                                    Msg += row1.Message + "\n";
                            }
                            return Msg;
                        }
                    }
                }
            }
            else
            {
                resConRec.Remark = row.Remark;
                resConRec.SpecSerRQCode1 = row.SpecSerRQCode1;
                resConRec.SpecSerRQCode2 = row.SpecSerRQCode2;
                if (row.Real)
                {
                    new Reservation().updateResConRecord(UserData, resConRec, ref errorMsg);
                }
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        return "OK";
    }
}

public class SsrcJson
{
    public SsrcJson()
    {
    }

    int? _CustNo;
    public int? CustNo
    {
        get { return _CustNo; }
        set { _CustNo = value; }
    }

    int? _ServiceID;
    public int? ServiceID
    {
        get { return _ServiceID; }
        set { _ServiceID = value; }
    }

    string _Remark;
    public string Remark
    {
        get { return _Remark; }
        set { _Remark = value; }
    }

    string _SpecSerRQCode1;
    public string SpecSerRQCode1
    {
        get { return _SpecSerRQCode1; }
        set { _SpecSerRQCode1 = value; }
    }

    string _SpecSerRQCode2;
    public string SpecSerRQCode2
    {
        get { return _SpecSerRQCode2; }
        set { _SpecSerRQCode2 = value; }
    }

    bool _Real;
    public bool Real
    {
        get { return _Real; }
        set { _Real = value; }
    }
}