﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSAdd_Visa.aspx.cs" Inherits="Controls_RSAdd_Visa"
    EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <title><%= GetGlobalResourceObject("PageTitle", "RSAddVisa") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    
    <link href="../CSS/RSAddStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function CheckInDateChange(_TextBox, _PopCal) {
            var _TextBoxWeek = $("#iCheckOut");
            _TextBoxWeek.value = _TextBox.value;
            if ((!_TextBox) || (!_PopCal)) return
            var _CheckIn2 = $("#ppcCheckOut");
            var _format = _TextBox.getAttribute("Format");
            var _Date = _PopCal.getDate(_TextBox.value, _format);
            if (_Date) {
                var _CIn2 = eval(_TextBoxWeek.attr("Calendar"));
                _CIn2.value = _Date;
                _TextBoxWeek.val(_TextBox.value);
                $("#iNight").text('0');
            }
        }

        function CheckOutDateChange(_TextBox, _PopCal) {
            var _TextBoxDay2 = $("#iCheckIn");
            var _format = _TextBox.getAttribute("Format");
            var _Day1 = _PopCal.getDate(_TextBoxDay2.val(), _format);
            var _Day2 = _PopCal.getDate(_TextBox.value, _format);
            if (_Day1 > _Day2) {
                showMessage('<%= GetGlobalResourceObject("LibraryResource","LastDateSmall") %>');
                _TextBox.value = _TextBoxDay2.val();
            }
            var days = (_Day2 - _Day1) / 86400000;
            $("#iNight").text(days.toString());
        }

        function getFormData() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Visa.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        $("#iNight").text(msg.d);
                    }
                    getCountry();
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getCountry() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Visa.aspx/getCountryList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sService").html("");
                    $("#sCountry").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sCountry").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getService() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Visa.aspx/getVisa",
                data: '{"Country":"' + $("#sCountry").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sService").html("");
                    $("#sService").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sService").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function changeCountry() {
            getService();
        }

        function createTourist() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Visa.aspx/getTourist",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#box1View").html("");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#box1View").append("<option value='" + this.CustNo + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });

            $(function() {
                $.configureBoxes();
            });
        }

        function preCalc(save) {
            if ($("#sCountry").val() == null || $("#sCountry").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectCountry") %>');
                return;
            }
            if ($("#sService").val() == null || $("#sService").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectVisa") %>');
                return;
            }

            var selectCust = '';
            $("#box1View option").each(function() {
                if (selectCust.length > 0) selectCust += "|";
                selectCust += $(this).val();
            });
            if (selectCust.length < 1) {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>');
                return;
            }
            var begDate = $("#iCheckIn").val();
            var _begDateFormat = $("#iCheckIn").attr("Format");
            var endDate = $("#iCheckOut").val();
            var _endDateFormat = $("#iCheckOut").attr("Format");
            if (begDate == '' || endDate == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseCorrectDate") %>');
                return;
            }

            var _data = '{"selectedCusts":"' + selectCust + '"}';
            $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/RSAdd_Visa.aspx/PaxControl",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var paxs = $.json.decode(msg.d);
                        if (paxs.Adult > 0) {
                            $("#iAdult").text(paxs.Adult.toString());
                            $("#iChild").text(paxs.Child.toString());
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });

            var _data = '';
            var _url = "";
            if (save) {
                _url = "../Controls/RSAdd_Visa.aspx/SaveService";
                _data = '{"selectedCusts":"' + selectCust + '"' +
                         ',"Country":"' + $("#sCountry").val() + '"' +
                         ',"Service":"' + $("#sService").val() + '"' +
                         ',"BegDate":"' + begDate + '"' +
                         ',"BegDateFormat":"' + _begDateFormat + '"' +
                         ',"EndDate":"' + endDate + '"' +
                         ',"EndDateFormat":"' + _endDateFormat + '"' +
                         ',"recordType":"' + $("#recordType").val() + '"' + '}';
            }
            else {
                _url = "../Controls/RSAdd_Visa.aspx/CalcService";
                _data = '{"selectedCusts":"' + selectCust + '"' +
                         ',"Country":"' + $("#sCountry").val() + '"' +
                         ',"Service":"' + $("#sService").val() + '"' +
                         ',"BegDate":"' + begDate + '"' +
                         ',"BegDateFormat":"' + _begDateFormat + '"' +
                         ',"EndDate":"' + endDate + '"' +
                         ',"EndDateFormat":"' + _endDateFormat + '"' + '}';
            }
            $.ajax({
                type: "POST",
                url: _url,
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    var result = $.json.decode(msg.d);
                    if (save) {
                        window.close;
                        self.parent.returnAddResServices(true, result.Supplier);
                    }
                    if (msg.d != '') {

                        if (result.Price != '') {
                            $("#iSalePrice").text(result.Price);
                        } else showMessage(result.Supplier);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            if (source == 'save') {
                preCalc(true);
            }
            else
                if (source == 'cancel') {
                window.close;
                self.parent.returnAddResServices(false, '');
            }
        }

        function onLoad() {
            $.query = $.query.load(location.href);
            $("#recordType").val($.query.get('recordType'));
            getFormData();
            createTourist();
        }                
    </script>

</head>
<body onload="onLoad();">
    <form id="formRsHotel" runat="server">
    <input id="recordType" type="hidden" value="" />
    <br />
    <div id="divRs">
        <div>
            <table>
                <tr>
                    <td class="leftCell">
                        <div id="divCountry" class="divs">
                            <div class="LeftDiv">
                              <span class="compulsoryField">* </span>
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewCountry") %>
                                    :</span>
                            </div>
                            <div id="divSCountry" class="inputDiv">
                                <select id="sCountry" class="combo" onchange="changeCountry()">
                                </select>
                            </div>
                        </div>
                        <div id="divService" class="divs">
                            <div class="LeftDiv">
                              <span class="compulsoryField">* </span>
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewVisa") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sService" class="combo">
                                </select>
                            </div>
                        </div>
                        <div id="divChekInOut" class="divs">
                            <div class="LeftDiv">
                              <span class="compulsoryField">* </span>
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewBeginEndDate") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <asp:TextBox ID="iCheckIn" runat="server" Width="100px" />
                                <rjs:PopCalendar ID="ppcCheckIn" runat="server" Control="iCheckIn" ClientScriptOnDateChanged="CheckInDateChange" />
                                &nbsp;-&nbsp;
                                <asp:TextBox ID="iCheckOut" runat="server" Width="100px" />
                                <rjs:PopCalendar ID="ppcCheckOut" runat="server" Control="iCheckOut" ClientScriptOnDateChanged="CheckOutDateChange" />
                            </div>
                        </div>
                        <div id="divNight" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewNight") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iNight"></span></b>
                            </div>
                        </div>
                        <div id="divAdultChild" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                    :</span>
                            </div>
                            <div class="divAdultChild">
                                <b><span id="iAdult"></span></b>
                            </div>
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                    :</span>
                            </div>
                            <div class="divAdultChild">
                                <b><span id="iChild"></span></b>
                            </div>
                        </div>
                        <%--<div id="divSupplier" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplier") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplier"></span></b>
                            </div>
                        </div>
                        <div id="divSupplierNote" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplierNote"></span></b>
                            </div>
                        </div>--%>
                    </td>
                    <td class="rightCell">
                        <br />
                        <div id="div1">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewSalePrice") %>
                                :</span> <span id="iSalePrice" class="salePrice"></span>
                        </div>
                        <br />
                        <div id="div2">
                            <input id="Button1" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                                onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                style="width: 150px;" />
                        </div>
                        <asp:HiddenField ID="hfDate" runat="server" />
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="2" cellspacing="0">
                <tr>
                    <td align="center">
                        <b>
                            <%= GetGlobalResourceObject("Controls", "lblServiceTourist")%></b>
                    </td>
                    <td>
                    </td>
                    <td align="center">
                        <b>
                            <%= GetGlobalResourceObject("Controls", "lblOtherTourist")%></b>
                    </td>
                </tr>
                <tr>
                    <td style="width: 290px;" align="left">
                        <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box1Filter" />
                        <button type="button" id="box1Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            X</button><br />
                        <select id="box1View" multiple="multiple" style="width: 100%; height: 125px;">
                        </select><br />
                        <%--<span id="box1Counter" class="countLabel"></span>--%>
                        <select id="box1Storage">
                        </select>
                    </td>
                    <td style="width: 40px;" align="center">
                        <button id="to2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            >
                        </button>
                        <br />
                        <button id="allTo2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            >>
                        </button>
                        <br />
                        <button id="allTo1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            <<
                        </button>
                        <br />
                        <button id="to1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            <
                        </button>
                    </td>
                    <td style="width: 290px;" align="left">
                        <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box2Filter" />
                        <button type="button" id="box2Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            X</button><br />
                        <select id="box2View" multiple="multiple" style="width: 100%; height: 125px;">
                        </select><br />
                        <%--<span id="box2Counter" class="countLabel"></span>--%>
                        <select id="box2Storage">
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <table id="divBtn">
            <tr>
                <td style="width: 50%;" align="center">
                    <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
                        onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
                <td align="center">
                    <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                        onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
            </tr>
        </table>
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
            </span><span id="messages">Message</span>
        </p>
    </div>
    </form>
</body>
</html>
