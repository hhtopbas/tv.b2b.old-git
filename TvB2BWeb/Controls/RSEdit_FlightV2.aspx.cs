﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using System.Text;
using TvTools;

public partial class Controls_RSEdit_FlightV2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {

        }
    }

    [WebMethod(EnableSession = true)]
    public static string getServiceDetail(string RecID)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        Int32? recID = Conversion.getInt32OrNull(RecID);
        if (!recID.HasValue) return string.Empty;

        var query = from q in ResData.ResService
                    where q.RecID == recID.Value
                    select new
                    {
                        Departure = q.DepLocationNameL,
                        Arrival = q.ArrLocationNameL,
                        FlyDate = q.BegDate.Value.ToShortDateString(),
                        Service = q.Service,
                        FlgClass = q.FlgClass,
                        Adult = q.Adult,
                        Child = q.Child,
                        Unit = q.Unit,
                        Supplier = q.SupplierName,
                        SupNote = q.SupNote,
                        StatConf = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ? q.StatConfNameL : string.Empty,
                        StatSer = q.StatSerNameL,
                        PnrNo = q.PnrNo,
                        SalePrice = !Equals(q.IncPack, "Y") ? q.SalePrice.Value.ToString("#,###.00") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        SaleCur = !Equals(q.IncPack, "Y") ? q.SaleCur : "",
                        ChangePax = Equals(q.IncPack, "Y") || (q.Compulsory.HasValue && q.Compulsory.Value) ? "0" : "1"
                    };
        if (query == null || query.Count() < 1) return string.Empty;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query.FirstOrDefault());
    }

    [WebMethod(EnableSession = true)]
    public static string getFlights(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord service = ResData.ResService.Find(f => f.RecID == recID);

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
        {
            List<getPLFlightsRecord> getPLFlights = new Flights().getPListFlights(UserData, ResData, service.Unit, ref errorMsg);
            List<FlightRecord> getFlights = new Flights().getFlights(UserData.Market, ref errorMsg);
            var query = from q in getPLFlights
                        join q1 in getFlights on q.DepFlightNo equals q1.Code
                        group q by new { FlightNo = q.DepFlightNo, Name = q.DepFlightNo + " (" + q1.DepAir + "=>" + q1.ArrAir + ") (" + q.DepFlyTime.Value.ToShortTimeString() + "-" + q1.ArrTime.Value.ToShortTimeString() + ")" } into k
                        select new { FlightNo = k.Key.FlightNo, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        else
        {
            List<FlightDayRecord> flights = new Flights().getRouteFlights(UserData, UserData.Market, service.BegDate, service.DepLocation, service.ArrLocation, ResData.ResMain.HolPack, ref errorMsg);
            var query = from q in flights
                        where q.DepCity == service.DepLocation && q.ArrCity == service.ArrLocation
                        group q by new { FlightNo =q.FlightNo, Name = (!string.IsNullOrEmpty(q.PNLName) ? q.PNLName : q.FlightNo) + " (" + q.DepAirport + "=>" + q.ArrAirport + ") (" + q.DepTime.Value.ToShortTimeString() + "-" + q.ArrTime.Value.ToShortTimeString() + ")" } into k
                        select new { FlightNo = k.Key.FlightNo, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getClass(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord service = ResData.ResService.Find(f => f.RecID == recID);

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
        {
            List<getPLFlightsRecord> getPLFlights = new Flights().getPListFlights(UserData, ResData, service.Unit, ref errorMsg);
            var query = from q in getPLFlights
                        group q by new { SClass = q.DepFlgClass } into k
                        select new { Code = k.Key.SClass, Name = k.Key.SClass };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        else
        {
            List<string> flightClass = new Flights().getFlightClass(UserData.Market, service.Service, service.BegDate.Value, ref errorMsg);
            var query = from q in flightClass
                        select new { Code = q, Name = q };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        Int32? recID = Conversion.getInt32OrNull(RecID);

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var querySelected = from s in ResData.ResCust
                            join q1 in ResData.ResCon on s.CustNo equals q1.CustNo
                            where s.Status == 0 && q1.ServiceID == recID
                            select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name, Selected = true };
        var queryUnSelected = from s in ResData.ResCust
                              //join q1 in ResData.ResCon on s.CustNo equals q1.CustNo
                              where s.Status == 0 && (querySelected.Where(w => w.CustNo == s.CustNo).Count() < 1)
                              group s by new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name } into k
                              select new { CustNo = k.Key.CustNo, Name = k.Key.Name, Selected = false };
        var query = querySelected.Union(queryUnSelected);
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static editServiceReturnData CalcService(string RecID, string Flight, string SClass, bool? newRes, string SelectedCust)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResDataRecord tmpResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        decimal? oldResSalePrice = ResData.ResMain.SalePrice;
        ResServiceRecord resService = (TvBo.ResServiceRecord)TvBo.Common.DeepClone(ResData.ResService.Find(f => f.RecID == recID));
        Int16? oldUnit = Conversion.getInt16OrNull(resService.Unit.ToString());
        decimal? oldServicePrice = resService.SalePrice;

        List<getPLFlightsRecord> getPLFlights = new List<getPLFlightsRecord>();
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            getPLFlights = new Flights().getPListFlights(UserData, ResData, resService.Unit, ref errorMsg);
        string currentFlight = resService.Service;
        string currentClass = resService.FlgClass;
        FlightDetailRecord flightDetail = new FlightDetailRecord();

        List<ResConRecord> serviceResCon = (List<ResConRecord>)TvBo.Common.DeepClone(ResData.ResCon.Where(w => w.ServiceID == resService.RecID).ToList<ResConRecord>());

        string[] selectedCust = SelectedCust.Split('|');

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        int InfCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5 && w.Title < 8).Count();
            InfCnt = tmpPax.Where(w => w.Title > 7).Count();
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;

        bool serviceChanged = false;
        if (resService.Service != Flight)
        {
            serviceChanged = true;
            oldUnit = 0;
        }
        resService.PriceSource = !Equals(resService.Service, Flight) || !Equals(resService.FlgClass, SClass) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Flight;
        resService.FlgClass = SClass;
        resService.Supplier = string.Empty;
        resService.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                            (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
        resService.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                            (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
        resService.Adult = (Int16)AdlCnt;
        resService.Child = (Int16)(ChdCnt + InfCnt);
        resService.Unit = (Int16)(AdlCnt + ChdCnt);

        if (resService.Unit > oldUnit || serviceChanged)
        {
            int status = -1;
            int freeSeat = 0;
            if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, resService.Service, resService.BegDate.Value, resService.FlgClass, resService.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", resService.Supplier))
                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
        }

        ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
        ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
        {
            #region Sunrise
            if (ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0)).Count() == 2)
            {
                if (resService.BegDate == ResData.ResMain.BegDate)
                {
                    ResServiceRecord otherFlight = tmpResData.ResService.Find(f => f.Service != currentFlight && f.ServiceType == "FLIGHT");
                    getPLFlightsRecord seconfFlight = getPLFlights.Find(f => f.DepFlightNo == Flight && f.DepFlgClass == SClass);
                    if (otherFlight == null || seconfFlight == null)
                    {
                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "ServiceCannotBeChanged").ToString();
                        return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                    }
                    if (seconfFlight != null)
                    {
                        if (resService.Service != seconfFlight.RetFlightNo)
                        {
                            serviceChanged = true;
                            oldUnit = 0;
                        }
                        resService = ResData.ResService.Find(f => f.RecID == otherFlight.RecID);
                        resService.Service = seconfFlight.RetFlightNo;
                        resService.FlgClass = seconfFlight.RetFlgClass;
                        resService.Supplier = string.Empty;
                        if (resService.Unit > oldUnit || serviceChanged)
                        {
                            int status = -1;
                            int freeSeat = 0;
                            if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, resService.Service, resService.BegDate.Value, resService.FlgClass, resService.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", resService.Supplier))
                                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                        }
                        ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
                        ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);
                    }
                }
                else
                {
                    ResServiceRecord otherFlight = tmpResData.ResService.Find(f => f.Service != currentFlight && f.ServiceType == "FLIGHT");
                    getPLFlightsRecord seconfFlight = getPLFlights.Find(f => f.RetFlightNo == Flight && f.RetFlgClass == SClass);
                    if (otherFlight == null || seconfFlight == null)
                    {
                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "ServiceCannotBeChanged").ToString();
                        return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                    }
                    if (seconfFlight != null)
                    {
                        if (resService.Service != seconfFlight.RetFlightNo)
                        {
                            serviceChanged = true;
                            oldUnit = 0;
                        }
                        resService = ResData.ResService.Find(f => f.RecID == otherFlight.RecID);
                        resService.Service = seconfFlight.DepFlightNo;
                        resService.FlgClass = seconfFlight.DepFlgClass;
                        resService.Supplier = string.Empty;
                        if (resService.Unit > oldUnit || serviceChanged)
                        {
                            int status = -1;
                            int freeSeat = 0;
                            if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, resService.Service, resService.BegDate.Value, resService.FlgClass, resService.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", resService.Supplier))
                                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                        }
                        ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
                        ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);
                    }
                }
            }
            #endregion Sunrise
        }
        else
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
            {
                #region Detur
                getPLFlightsRecord returnFlight = new Flights().getReturnFlight(UserData.Market, resService.Service, resService.BegDate, resService.FlgClass, ResData.ResMain.Days, resService.Unit, ref errorMsg);
                if (returnFlight != null)
                {
                    List<ResServiceRecord> retFlightList = (from q1 in ResData.ResService
                                                            join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                            where q1.ServiceType == "FLIGHT" && q1.RecID != resService.RecID
                                                            select q1).ToList<ResServiceRecord>();
                    if (retFlightList.Count > 0)
                    {
                        ResServiceRecord retFlight = ResData.ResService.Find(f => f.RecID == retFlightList.FirstOrDefault().RecID);
                        if (retFlight != null)
                        {
                            if (retFlight.Service != returnFlight.RetFlightNo)
                            {
                                serviceChanged = true;
                                oldUnit = 0;
                            }
                            retFlight.IncPack = resService.PriceSource == 0 ? "N" : "Y";
                            retFlight.Service = returnFlight.RetFlightNo;
                            retFlight.FlgClass = SClass;
                            retFlight.Supplier = string.Empty;
                            flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, retFlight.Service, retFlight.BegDate.Value, ref errorMsg);
                            retFlight.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                            retFlight.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;

                            if (retFlight.Unit > oldUnit || serviceChanged)
                            {
                                int status = -1;
                                int freeSeat = 0;
                                if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, retFlight.Service, retFlight.BegDate.Value, retFlight.FlgClass, retFlight.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", retFlight.Supplier))
                                    return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                            }
                            ResData = new Reservation().modifyServiceCustomers(UserData, ResData, retFlight.RecID, SelectCust, ref errorMsg);
                            ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), retFlight, ResData.ResCon, ref errorMsg);
                        }
                    }
                }
                #endregion
            }

        if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
        }
        else
        {
            resService = ResData.ResService.Find(f => f.RecID == recID.Value);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
            return new editServiceReturnData
            {
                Calc = true,
                CalcPrice = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                CalcCur = resService.SalePrice.HasValue && !Equals(resService.IncPack, "Y") ? resService.SaleCur : string.Empty,
                Adult = resService.Adult,
                Child = resService.Child,
                Unit = resService.Unit,
                OldResPrice = oldResSalePrice.HasValue ? oldResSalePrice.Value.ToString("#,###.00") : string.Empty,
                OldServicePrice = oldServicePrice.HasValue ? oldServicePrice.Value.ToString("#,###.00") : string.Empty,
                Supplier = supplierRec.NameL,
                Msg = string.Empty
            };
        }
    }

    [WebMethod(EnableSession = true)]
    public static editServiceReturnData SaveService(string RecID, string Flight, string SClass, bool? newRes, string SelectedCust)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResDataRecord tmpResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        decimal? oldResSalePrice = ResData.ResMain.SalePrice;
        ResServiceRecord resService = (TvBo.ResServiceRecord)TvBo.Common.DeepClone(ResData.ResService.Find(f => f.RecID == recID));
        Int16? oldUnit = Conversion.getInt16OrNull(resService.Unit.ToString());
        decimal? oldServicePrice = resService.SalePrice;
        List<int?> recIDList = new List<int?>();
        recIDList.Add(resService.RecID);

        List<getPLFlightsRecord> getPLFlights = new List<getPLFlightsRecord>();        

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            getPLFlights = new Flights().getPListFlights(UserData, ResData, resService.Unit, ref errorMsg);

        string currentFlight = resService.Service;
        string currentClass = resService.FlgClass;

        List<ResConRecord> serviceResCon = (List<ResConRecord>)TvBo.Common.DeepClone(ResData.ResCon.Where(w => w.ServiceID == resService.RecID).ToList<ResConRecord>());
        bool customerChanged = false;

        string[] selectedCust = SelectedCust.Split('|');

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        int InfCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5 && w.Title < 8).Count();
            InfCnt = tmpPax.Where(w => w.Title > 7).Count();
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;

        if (SelectCust.Where(w => w.Selected == true).Count() != ResData.ResCon.Where(w => w.ServiceID == resService.RecID).Count())
            customerChanged = true;

        bool serviceChanged = false;
        if (resService.Service != Flight)
        {
            serviceChanged = true;
            oldUnit = 0;
        }

        resService.PriceSource = !Equals(resService.Service, Flight) || !Equals(resService.FlgClass, SClass) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Flight;
        resService.FlgClass = SClass;
        resService.Supplier = string.Empty;
        resService.Adult = (Int16)AdlCnt;
        resService.Child = (Int16)(ChdCnt + InfCnt);
        resService.Unit = (Int16)(AdlCnt + ChdCnt);

        FlightDetailRecord flightDetail = new FlightDetailRecord();
        flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, resService.Service, resService.BegDate.Value, ref errorMsg);
        resService.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                            (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
        resService.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                            (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;

        if (resService.Unit > oldUnit || serviceChanged)
        {
            int status = -1;
            int freeSeat = 0;
            if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, resService.Service, resService.BegDate.Value, resService.FlgClass, resService.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", resService.Supplier))
                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
        }

        ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
        List<ResConRecord> resCon = ResData.ResCon;
        foreach (ResConRecord row in resCon.Where(w => w.ServiceID == resService.RecID))
        {
            ResConRecord resC = resCon.Find(f => f.RecID == row.RecID);
            if (resC != null)
                resC.MemTable = false;
        }
        ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);

        if (newRes.HasValue && newRes.Value)
        {
            #region new Service
            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
            {
                #region Sunrise
                if (ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0)).Count() == 2)
                {
                    if (resService.BegDate == ResData.ResMain.BegDate)
                    {
                        ResServiceRecord otherFlight = tmpResData.ResService.Find(f => f.Service != currentFlight && f.ServiceType == "FLIGHT");
                        getPLFlightsRecord seconfFlight = getPLFlights.Find(f => f.DepFlightNo == Flight && f.DepFlgClass == SClass);
                        if (otherFlight == null || seconfFlight == null)
                        {
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "ServiceCannotBeChanged").ToString();
                            return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                        }
                        if (seconfFlight != null)
                        {
                            TvBo.ResServiceRecord resServiceSecond = ResData.ResService.Find(f => f.RecID == otherFlight.RecID);
                            if (resServiceSecond.Service != seconfFlight.RetFlightNo)
                            {
                                serviceChanged = true;
                                oldUnit = 0;
                            }
                            resServiceSecond.Service = seconfFlight.RetFlightNo;
                            resServiceSecond.FlgClass = seconfFlight.RetFlgClass;
                            resServiceSecond.Supplier = string.Empty;
                            flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, resServiceSecond.Service, resServiceSecond.BegDate.Value, ref errorMsg);
                            resServiceSecond.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                            resServiceSecond.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;
                            ResData = new Reservation().extraServiceControl(UserData, ResData, resServiceSecond, ref errorMsg);
                            if (resService.Unit > oldUnit || serviceChanged)
                            {
                                int status = -1;
                                int freeSeat = 0;
                                if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, resServiceSecond.Service, resServiceSecond.BegDate.Value, resServiceSecond.FlgClass, resServiceSecond.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", resServiceSecond.Supplier))
                                    return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                            }
                            ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resServiceSecond.RecID, SelectCust, ref errorMsg);
                            List<ResConRecord> resConL = ResData.ResCon;
                            foreach (ResConRecord row in resConL.Where(w => w.ServiceID == resServiceSecond.RecID))
                            {
                                ResConRecord resC = resCon.Find(f => f.RecID == row.RecID);
                                if (resC != null)
                                    resC.MemTable = false;
                            }
                            ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resServiceSecond, ResData.ResCon, ref errorMsg);
                            recIDList.Add(resService.RecID);
                        }
                    }
                    else
                    {
                        ResServiceRecord otherFlight = tmpResData.ResService.Find(f => f.Service != currentFlight && f.ServiceType == "FLIGHT");
                        getPLFlightsRecord seconfFlight = getPLFlights.Find(f => f.RetFlightNo == Flight && f.RetFlgClass == SClass);
                        if (otherFlight == null || seconfFlight == null)
                        {
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "ServiceCannotBeChanged").ToString();
                            return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                        }
                        if (seconfFlight != null)
                        {
                            TvBo.ResServiceRecord resServiceSecond = ResData.ResService.Find(f => f.RecID == otherFlight.RecID);
                            if (resServiceSecond.Service != seconfFlight.RetFlightNo)
                            {
                                serviceChanged = true;
                                oldUnit = 0;
                            }
                            resServiceSecond.Service = seconfFlight.DepFlightNo;
                            resServiceSecond.FlgClass = seconfFlight.DepFlgClass;
                            resServiceSecond.Supplier = string.Empty;
                            flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, resServiceSecond.Service, resServiceSecond.BegDate.Value, ref errorMsg);
                            resServiceSecond.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                            resServiceSecond.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;
                            if (resService.Unit > oldUnit || serviceChanged)
                            {
                                int status = -1;
                                int freeSeat = 0;
                                if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, resServiceSecond.Service, resServiceSecond.BegDate.Value, resServiceSecond.FlgClass, resServiceSecond.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", resServiceSecond.Supplier))
                                    return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                            }
                            ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resServiceSecond.RecID, SelectCust, ref errorMsg);
                            List<ResConRecord> resConL = ResData.ResCon;
                            foreach (ResConRecord row in resConL.Where(w => w.ServiceID == resServiceSecond.RecID))
                            {
                                ResConRecord resC = resCon.Find(f => f.RecID == row.RecID);
                                if (resC != null)
                                    resC.MemTable = false;
                            }
                            ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resServiceSecond, ResData.ResCon, ref errorMsg);
                            recIDList.Add(resService.RecID);
                        }
                    }
                }
                #endregion
            }
            else
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
                {
                    #region Detur
                    getPLFlightsRecord returnFlight = new Flights().getReturnFlight(UserData.Market, resService.Service, resService.BegDate, resService.FlgClass, ResData.ResMain.Days, resService.Unit, ref errorMsg);
                    if (returnFlight != null)
                    {
                        List<ResServiceRecord> retFlightList = (from q1 in ResData.ResService
                                                                join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                                where q1.ServiceType == "FLIGHT" && q1.RecID != resService.RecID
                                                                select q1).ToList<ResServiceRecord>();
                        if (retFlightList.Count > 0)
                        {
                            ResServiceRecord retFlight = ResData.ResService.Find(f => f.RecID == retFlightList.FirstOrDefault().RecID);
                            if (retFlight != null)
                            {
                                if (retFlight.Service != returnFlight.RetFlightNo)
                                {
                                    serviceChanged = true;
                                    oldUnit = 0;
                                }
                                retFlight.IncPack = resService.PriceSource == 0 ? "N" : "Y";
                                retFlight.Service = returnFlight.RetFlightNo;
                                retFlight.FlgClass = SClass;
                                retFlight.Supplier = string.Empty;
                                flightDetail = new FlightDetailRecord();
                                flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, retFlight.Service, retFlight.BegDate.Value, ref errorMsg);
                                retFlight.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                    (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                                retFlight.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                    (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;

                                if (resService.Unit > oldUnit || serviceChanged)
                                {
                                    int status = -1;
                                    int freeSeat = 0;
                                    if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, retFlight.Service, retFlight.BegDate.Value, retFlight.FlgClass, retFlight.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", retFlight.Supplier))
                                        return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                                }

                                ResData = new Reservation().modifyServiceCustomers(UserData, ResData, retFlight.RecID, SelectCust, ref errorMsg);
                                List<ResConRecord> resConL = ResData.ResCon;
                                foreach (ResConRecord row in resConL.Where(w => w.ServiceID == retFlight.RecID))
                                {
                                    ResConRecord resC = resCon.Find(f => f.RecID == row.RecID);
                                    if (resC != null)
                                        resC.MemTable = false;
                                }
                                ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), retFlight, ResData.ResCon, ref errorMsg);
                                recIDList.Add(retFlight.RecID);
                            }
                        }
                    }
                    #endregion
                }

            if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            {
                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
            }
            else
            {
                #region re calculated
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                HttpContext.Current.Session["ResData"] = ResData;
                return new editServiceReturnData
                {
                    Calc = true,
                    CalcPrice = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                    CalcCur = resService.SalePrice.HasValue && !Equals(resService.IncPack, "Y") ? resService.SaleCur : string.Empty,
                    Adult = resService.Adult,
                    Child = resService.Child,
                    Unit = resService.Unit,
                    OldResPrice = oldResSalePrice.HasValue ? oldResSalePrice.Value.ToString("#,###.00") : string.Empty,
                    OldServicePrice = oldServicePrice.HasValue ? oldServicePrice.Value.ToString("#,###.00") : string.Empty,
                    Supplier = supplierRec.NameL,
                    Msg = string.Empty
                };
                #endregion
            }
            #endregion
        }
        else
        {
            #region old service
            Int32 ErrCode = 0;
            #region Detur
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                if (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0)
                {
                    #region price list Flight
                    getPLFlightsRecord returnFlight = new Flights().getReturnFlight(UserData.Market, resService.Service, resService.BegDate, resService.FlgClass, ResData.ResMain.Days, resService.Unit, ref errorMsg);
                    if (returnFlight != null)
                    {
                        List<ResServiceRecord> retFlightList = (from q1 in ResData.ResService
                                                                join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                                where q1.ServiceType == "FLIGHT" && q1.RecID != resService.RecID
                                                                select q1).ToList<ResServiceRecord>();
                        if (retFlightList.Count > 0)
                        {
                            ResServiceRecord retFlight = ResData.ResService.Find(f => f.RecID == retFlightList.FirstOrDefault().RecID);
                            if (retFlight != null)
                            {
                                if (retFlight.Service != returnFlight.RetFlightNo)
                                {
                                    serviceChanged = true;
                                    oldUnit = 0;
                                }

                                retFlight.PriceSource = resService.PriceSource;
                                retFlight.IncPack = resService.PriceSource == 0 ? "N" : "Y";
                                retFlight.Service = returnFlight.RetFlightNo;
                                retFlight.FlgClass = SClass;
                                retFlight.Supplier = string.Empty;
                                flightDetail = new FlightDetailRecord();
                                flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, retFlight.Service, retFlight.BegDate.Value, ref errorMsg);
                                retFlight.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                    (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                                retFlight.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                    (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;

                                if (resService.Unit > oldUnit || serviceChanged)
                                {
                                    int status = -1;
                                    int freeSeat = 0;
                                    if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, retFlight.Service, retFlight.BegDate.Value, retFlight.FlgClass, retFlight.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", retFlight.Supplier))
                                        return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                                }

                                ResData = new Reservation().modifyServiceCustomers(UserData, ResData, retFlight.RecID, SelectCust, ref errorMsg);
                                List<ResConRecord> resConL = ResData.ResCon;
                                foreach (ResConRecord row in resConL.Where(w => w.ServiceID == retFlight.RecID))
                                {
                                    ResConRecord resC = resCon.Find(f => f.RecID == row.RecID);
                                    if (resC != null)
                                        resC.MemTable = false;
                                }
                                ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData(ResData), retFlight, ref errorMsg);
                                recIDList.Add(retFlight.RecID);
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region onlyTicket
                    if (customerChanged)
                    {
                        ResDataRecord ResDatatmp = new ResTables().copyData(ResData);
                        ResServiceRecord retFlight = ResData.ResService.OrderBy(o => o.BegDate).Where(w => string.Equals(w.ServiceType, "FLIGHT")).LastOrDefault();
                        if (retFlight != null)
                        {
                            retFlight.Adult = (Int16)AdlCnt;
                            retFlight.Child = (Int16)(ChdCnt + InfCnt);
                            retFlight.Unit = (Int16)(AdlCnt + ChdCnt);
                            if (retFlight.Unit > oldUnit || serviceChanged)
                            {
                                int status = -1;
                                int freeSeat = 0;
                                if (!new Flights().CheckFlightAllot(UserData, ResData.ResMain.PLOperator, ResData.ResMain.ResDate, retFlight.Service, retFlight.BegDate.Value, retFlight.FlgClass, retFlight.Unit.Value - oldUnit.Value, ref status, ref freeSeat, ref errorMsg, "H", retFlight.Supplier))
                                    return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                            }

                            ResData = new Reservation().modifyServiceCustomers(UserData, ResData, retFlight.RecID, SelectCust, ref errorMsg);
                            List<ResConRecord> resConL = ResData.ResCon;
                            foreach (ResConRecord row in resConL.Where(w => w.ServiceID == retFlight.RecID))
                            {
                                ResConRecord resC = resCon.Find(f => f.RecID == row.RecID);
                                if (resC != null)
                                    resC.MemTable = false;
                            }
                            ResData = new Reservation().extraServiceControlV2(UserData, ResDatatmp, retFlight, ResData.ResCon, ref errorMsg);
                            recIDList.Add(retFlight.RecID);
                        }
                    }
                    #endregion
                }
            }
            #endregion

            ResData = new TvBo.Reservation().editService(UserData, ResData, recIDList, ref ErrCode, ref errorMsg, false);

            if (!string.IsNullOrEmpty(errorMsg))
            {
                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
            }
            else
                if (ErrCode != 0)
                {
                    if (ErrCode < 0)
                        errorMsg = string.IsNullOrEmpty(errorMsg) ? "unknown error" : errorMsg;
                    else errorMsg = new ResCalcError().calcError(ErrCode, ResData).ToString();
                    return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                }
                else
                {
                    #region re calculated
                    resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                    SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                    HttpContext.Current.Session["ResData"] = ResData;
                    return new editServiceReturnData
                    {
                        Calc = true,
                        CalcPrice = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        CalcCur = resService.SalePrice.HasValue && !Equals(resService.IncPack, "Y") ? resService.SaleCur : string.Empty,
                        Adult = resService.Adult,
                        Child = resService.Child,
                        Unit = resService.Unit,
                        OldResPrice = !Equals(resService.IncPack, "Y") && oldResSalePrice.HasValue ? oldResSalePrice.Value.ToString("#,###.00") : string.Empty,
                        OldServicePrice = !Equals(resService.IncPack, "Y") && oldServicePrice.HasValue ? oldServicePrice.Value.ToString("#,###.00") : string.Empty,
                        Supplier = supplierRec.NameL,
                        Msg = string.Empty
                    };
                    #endregion
                }
            #endregion
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getChgFee(string RecID, string Flight, string SClass)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        resService.Service = Flight;
        resService.FlgClass = SClass;
        ResChgFeePrice chgPrice = new Reservation().whatHasChangedResService(UserData, ResData, recID, resService, ref errorMsg);
        if (chgPrice == null)
            return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        else
        {
            string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                             chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
            return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
        }
    }
}
