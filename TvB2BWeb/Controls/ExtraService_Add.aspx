﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExtraService_Add.aspx.cs"
    Inherits="Controls_ExtraService_Add" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "ExtraServiceAdd") %></title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/ExtraService_Add.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        var btnOk = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
        var addPleaseSelectTourist = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>';
        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
        var serviceNote = '';
        var selectedExtServiceTourist = '';

        function logout() {
            self.parent.logout();
        }

        function addExtraServiceNextStep(mainRecID, extraServiceID, priceType) {

            var params = new Object();
            params.SelectedCusts = selectedExtServiceTourist;
            params.MainServiceID = mainRecID;
            params.ExtServiceID = extraServiceID;
            params.recordType = $("#recordType").val();
            params.dayDuration = $("#durationCombo_" + mainRecID + "_" + extraServiceID).length > 0 ? $("#durationCombo_" + mainRecID + "_" + extraServiceID).val() : null;
            params.ExtServiceNote = serviceNote;
            $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/ExtraService_Add.aspx/calcExtraService",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == '') {
                        window.close;
                        self.parent.returnAddResServicesExt(true, '');
                    }
                    else
                        alert(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function durationComboChanged(mainRecID, extraServiceID, value) {

        }

        function addExtraServiceForSrvNote(mainRecID, extraServiceID, priceType) {
            serviceNote = '';
            $("#dialog-ExtSrvNote").dialog(
            {
                autoOpen: true,
                modal: true,
                width: 630,
                height: 275,
                buttons: [{
                    text: btnOk,
                    click: function () {
                        serviceNote = $("#iExtServiceNote").val();
                        addExtraService(mainRecID, extraServiceID, priceType);
                        return true;
                    }
                }, {
                    text: btnCancel,
                    click: function () {
                        serviceNote = '';
                        addExtraService(mainRecID, extraServiceID, priceType);
                        return false;
                    }
                }]
            });
        }

        function addExtraService(mainRecID, extraServiceID, priceType) {
            if (priceType == 1) {
                var retval = AddTourist(mainRecID, extraServiceID, priceType);

            }
            else {
                selectedExtServiceTourist = '';
                addExtraServiceNextStep(mainRecID, extraServiceID, priceType);
            }
        }

        function getExtraService() {
            $.ajax({
                type: "POST",
                url: "../Controls/ExtraService_Add.aspx/getExtraServices",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#gridExtraServices").html("");
                    $("#gridExtraServices").html(msg.d);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function createTourist(serviceID, extraServiceID) {
            $.ajax({
                type: "POST",
                url: "../Controls/ExtraService_Add.aspx/getTourist",
                data: '{"serviceID":"' + serviceID + '","ExtraServiceID":"' + extraServiceID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#box1View").html('');
                    $("#box2View").html('');
                    if (msg.d != '') {
                        var data = $.json.decode(msg.d);
                        $.each(data.CustList, function (i) {
                            $("#box1View").append("<option value='" + this.CustNo + "'>" + this.Name + "</option>");
                        });
                        if (data.LockCustList == true) {
                            $("#buttons").attr('disabled', 'disabled');
                            $("#box1View").attr('disabled', 'disabled');
                            $("#box1Storage").attr('disabled', 'disabled');
                        }
                        else {
                            $("#buttons").removeAttr('disabled');
                            $("#box1View").removeAttr('disabled');
                            $("#box1Storage").removeAttr('disabled');
                        }
                    }
                    return true;
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                    return false;
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            $(function () {
                $.configureBoxes();
            });
        }

        function AddTourist(serviceID, extraServiceID, priceType) {

            selectedExtServiceTourist = '';
            createTourist(serviceID, extraServiceID);
            $("#dialog-AddTourist").dialog(
            {
                autoOpen: true,
                modal: true,
                width: 630,
                height: 375,
                resizable: true,
                autoResize: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                        var selectCust = '';
                        $("#box1View option").each(function () {
                            if (selectCust.length > 0) selectCust += "|";
                            selectCust += $(this).val();
                        });
                        if (selectCust.length < 1) {
                            alert('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>');
                            return true;
                        }
                        selectedExtServiceTourist = selectCust
                        $(this).dialog('close');

                        addExtraServiceNextStep(serviceID, extraServiceID, priceType);
                        return true;
                    },
                    '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>': function () {
                        $(this).dialog('close');
                        selectedExtServiceTourist = '';
                        return false;
                    }
                }
            });
            return false;
        }

        function pageLoad() {
            $.query = $.query.load(location.href);
            $("#recordType").val($.query.get('recordType'));
            getExtraService();
        }
    </script>

</head>
<body onload="pageLoad();">
    <form id="ExtraServiceAddForm" runat="server">
        <input id="recordType" type="hidden" value="" />
        <div style="width: 100%; text-align: center;">
            <div id="gridExtraServices" style="text-align: left;">
            </div>
        </div>
        <div style="clear: both; font-size: 8pt;">
            <div id="dialog-AddTourist" title="" style="clear: both; display: none; font-size: 8pt;">
                <div>
                    <table cellpadding="2" cellspacing="0">
                        <tr>
                            <td align="center">
                                <b>
                                    <%= GetGlobalResourceObject("Controls", "lblServiceTourist")%></b>
                            </td>
                            <td></td>
                            <td align="center">
                                <b>
                                    <%= GetGlobalResourceObject("Controls", "lblOtherTourist")%></b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 290px;" align="left">
                                <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                            <input type="text" id="box1Filter" />
                                <button type="button" id="box1Clear">
                                    X</button><br />
                                <select id="box1View" multiple="multiple" style="width: 100%; height: 125px;">
                                </select><br />
                                <%--<span id="box1Counter" class="countLabel"></span>--%>
                                <select id="box1Storage">
                                </select>
                            </td>
                            <td id="buttons" style="width: 40px;" align="center">
                                <button id="to2" type="button" style="width: 34px;">
                                    >
                                </button>
                                <br />
                                <button id="allTo2" type="button" style="width: 34px;">
                                    >>
                                </button>
                                <br />
                                <button id="allTo1" type="button" style="width: 34px;">
                                    <<
                                </button>
                                <br />
                                <button id="to1" type="button" style="width: 34px;">
                                    <
                                </button>
                            </td>
                            <td style="width: 290px;" align="left">
                                <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                            <input type="text" id="box2Filter" />
                                <button type="button" id="box2Clear">
                                    X</button><br />
                                <select id="box2View" multiple="multiple" style="width: 100%; height: 125px;">
                                </select><br />
                                <%--<span id="box2Counter" class="countLabel"></span>--%>
                                <select id="box2Storage">
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div id="dialog-ExtSrvNote" title='' style="display: none;">
            <span>Note :</span><br />
            <input id="iExtServiceNote" type="text" style="width: 90%" />
        </div>
    </form>
</body>
</html>
