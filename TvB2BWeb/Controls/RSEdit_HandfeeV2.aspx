﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSEdit_HandfeeV2.aspx.cs" Inherits="Controls_RSEdit_HandfeeV2"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><%= GetGlobalResourceObject("PageTitle", "RSEditHandfee") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-migrate-1.2.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <link id="jqUiThemes" href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link id="jqUiT" href="../CSS/themes/Default/jquery.ui.theme.css" rel="stylesheet" type="text/css" />

    <link href="../CSS/baseCss.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/RSEditStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        var comboSelect = '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';

        var newRes = $.query.get('NewRes') == '1' ? true : false;

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {

            $("#messages").html(msg);

            var buttons = { '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () { $(this).dialog('close'); } };

            $("#dialog-message").dialog({
                modal: true,
                buttons: buttons
            });

        }

        function getHandfee(RecID, Handfee) {

            var params = new Object();
            params.RecID = recID;

            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Handfee.aspx/getHandfee",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    $("#sHandFee").html('');
                    $("#sHandFee").append("<option value=''>" + comboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != null) {

                        $.each($.json.decode(msg.d), function (i) {
                            $("#sHandFee").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });

                        $("#sHandFee").val(Handfee);

                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMessage(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function disabledTuristEdit() {
            if ($.browser.msie == true) {
                $("#serviceCustomers").prop('disabled', true);
            }
            else {
                $("#box1View").prop('disabled', true);
                $("#box2View").prop('disabled', true);
                $("#serviceCustomers input,button").prop('disabled', true);
            }
        }

        function createTourist(recID) {
            var params = new Object();
            params.RecID = recID;

            $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/RSEdit_ExcursionV2.aspx/getTourist",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    $("#box1View").html('');
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $.each(msg.d, function (i) {
                            if (this.Selected == true)
                                $("#box1View").append("<option value='" + this.CustNo + "'>" + this.Name + "</option>");
                            else $("#box2View").append("<option value='" + this.CustNo + "'>" + this.Name + "</option>");
                        });
                    }

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMessage(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            $.configureBoxes();

            if ($("#hfChangePax").val() == '0') {
                setTimeout(disabledTuristEdit, 200);
            }
        }

        function preCalc(save) {

            if ($("#sHandFee").val() == null || $("#sHandFee").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectHandFee") %>');
                return;
            }

            var selectCust = '';

            $("#box1View option").each(function () {
                if (selectCust.length > 0) selectCust += "|";
                selectCust += $(this).val();
            });

            if (selectCust.length < 1) {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>');
                return;
            }

            var params = new Object();
            params.RecID = $("#serviceID").val();
            params.Handfee = $("#sHandFee").val();
            params.newRes = newRes;
            params.SelectedCust = selectCust;
            
            var _url = '';
            if (save)
                _url = '../Controls/RSEdit_Handfee.aspx/SaveService';
            else _url = '../Controls/RSEdit_Handfee.aspx/CalcService';

            $.ajax({
                type: "POST",
                url: _url,
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        if (save) {
                            if (msg.d.Calc == true) {
                                window.close;
                                self.parent.returnEditResServices(true);
                            } else {
                                showMessage(msg.d.Msg);
                            }
                        }
                        else {
                            if (msg.d.Calc == true) {
                                $("#iSalePrice").text(msg.d.CalcPrice + ' ' + msg.d.CalcCur);
                                $("#iOldServicePrice").css("text-decoration", "line-through");
                                $("#iAdult").text(msg.d.Adult != null ? msg.d.Adult : '');
                                $("#iChild").text(msg.d.Child != null ? msg.d.Child : '');
                                $("#iUnit").text(msg.d.Unit != null ? msg.d.Unit : '');
                                $("#iResNewPrice").text(msg.d.NewResPrice)

                            } else {
                                showMessage(msg.d.Msg);
                            }
                        }
                    } else {
                        showMessage('unknown error.');
                    }

                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMessage(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function exit(source) {

            if (source == 'save') {

                if ($("#sHandFee").val() == null || $("#sHandFee").val() == '') {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectHandFee") %>');
                    return;
                }

                var selectCust = '';
                $("#box1View option").each(function () {
                    if (selectCust.length > 0) selectCust += "|";
                    selectCust += $(this).val();
                });

                if (selectCust.length < 1) {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>');
                    return;
                }

                var params = new Object();
                params.RecID = $("#serviceID").val();
                params.HandFee = $("#sHandFee").val();

                if (newRes != true) {
                    $.ajax({
                        type: "POST",
                        url: "../Controls/RSEdit_Handfee.aspx/getChgFee",
                        data: $.json.encode(params),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {

                            if (msg.hasOwnProperty('d') && msg.d != null) {
                                if (msg.d.changeFee == true) {
                                    $(function () {
                                        $("#messages").html(msg.d.Msg);
                                        var buttons = {
                                            '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>': function () {
                                                $(this).dialog('close');
                                                preCalc(true);
                                            },
                                            '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>': function () {
                                                $(this).dialog('close');
                                            }
                                        };
                                        $("#dialog-message").dialog({
                                            modal: true,
                                            buttons: buttons
                                        });
                                    });
                                } else {
                                    preCalc(true);
                                }
                            }

                        },
                        error: function (xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                                showMessage(xhr.responseText);
                            }
                        },
                        statusCode: {
                            408: function () {
                                logout();
                            }
                        }
                    });
                } else preCalc(true);
            } else {
                if (source == 'cancel') {
                    window.close;
                    self.parent.returnEditResServices(false);
                }
            }
        }

        function clickIAgree() {
            if ($("#iAgree").is(':checked')) {
                $("#btnSave").removeAttr('disabled');
            } else {
                $("#btnSave").attr('disabled', 'disabled');
            }
        }

        function firstPrice() {
            $("#iSalePrice").html('');
            $("#iResNewPrice").html('');
            $("#iOldServicePrice").css("text-decoration", "none");
            $("#iAgree").removeAttr('checked');
            $("#btnSave").attr('disabled', 'disabled');
        }

        function getServiceDetail(recID) {

            var params = new Object();
            params.RecID = recID;

            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Handfee.aspx/getServiceDetail",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var resService = msg.d;
                        $("#hfChangePax").val(resService.ChangePax);
                        $("#iBegDate").text(resService.BegDate);
                        $("#iEndDate").text(resService.EndDate);
                        $("#sCountry").text(resService.DepLocation);
                        $("#sLocation").text(resService.ArrLocation);
                        $("#iAdult").text(resService.Adult);
                        $("#iChild").text(resService.Child);
                        $("#iNight").text(resService.Duration);
                        $("#iUnit").text(resService.Unit);

                        if (resService.StatConf != '')
                            $("#sConfirmation").text(resService.StatConf);
                        else $("#divConfirmation").hide();

                        $("#sStatus").text(resService.StatSer);

                        $("#iOldServicePrice").text(resService.oldServicePrice);
                        $("#iResOldPrice").text(resService.oldResPrice);

                        getHandfee(recID, resService.Service);

                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMessage(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(function () {

            $("#jqUiThemes").attr('href', '<%= Global.getBasePageRoot() %>' + 'CSS/' + '<%= Global.themesPath %>' + 'jquery-ui.css');
            $("#jqUiT").attr('href', '<%= Global.getBasePageRoot() %>' + 'CSS/' + '<%= Global.themesPath %>' + 'jquery.ui.theme.css');

            $.query = $.query.load(location.href);

            var recID = $.query.get('RecID');
            $("#serviceID").val(recID);

            getServiceDetail(recID);
            createTourist(recID);

            firstPrice();

            $('.selectionButtons').on('click', 'button', function () {
                firstPrice();
            });
            $('body').on('dblclick', '#box1View', function () {
                firstPrice();
            });
            $('body').on('dblclick', '#box2View', function () {
                firstPrice();
            });
        });

    </script>

</head>
<body class="ui-widget ui-helper-reset">
    <form id="formRsHandFee" runat="server">
        <input id="hfChangePax" type="hidden" />
        <input id="serviceID" type="hidden" />
        <br />
        <div class="editResService">
            <div>
                <table class="ui-widget-content">
                    <tr>
                        <td class="ui-widget-content leftCell">
                            <div id="divCountry" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewCountry") %> :</span>
                                </div>
                                <div id="divSCountry" class="inputDiv">
                                    <b><span id="sDepLocation"></span></b>
                                </div>
                            </div>
                            <div id="divLocation" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewLocation") %> :</span>
                                </div>
                                <div id="divSLocation" class="inputDiv">
                                    <b><span id="sArrLocation"></span></b>
                                </div>
                            </div>
                            <div id="divHandfee" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewHandFee") %> :</span>
                                </div>
                                <div class="inputDiv">
                                    <select id="sHandFee" onchange="firstPrice()">
                                    </select>
                                </div>
                            </div>
                            <div id="divChekInOut" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewBeginEndDate") %> :</span>
                                </div>
                                <div class="inputDiv">
                                    <b><span id="iBegDate"></span>&nbsp;-&nbsp; <span id="iEndDate"></span></b>
                                </div>
                            </div>
                            <div id="divNight" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewNight") %> :</span>
                                </div>
                                <div class="inputDiv">
                                    <b><span id="iNight"></span></b>
                                </div>
                            </div>
                            <div id="divAdultChild" class="ui-helper-clearfix divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewAdult") %> :</span>
                                </div>
                                <div class="adultDiv">
                                    <b><span id="iAdult"></span></b>
                                </div>
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewChild") %> :</span>
                                </div>
                                <div class="childDiv">
                                    <b><span id="iChild"></span></b>
                                </div>
                            </div>
                            <div id="divUnit" class="ui-helper-clearfix divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewUnit") %> :</span>
                                </div>
                                <div class="inputDiv">
                                    <b><span id="iUnit"></span></b>
                                </div>
                            </div>
                        </td>
                        <td class="ui-widget-content rightCell">
                            <span class="status">
                                <%= GetGlobalResourceObject("Controls", "viewStatus") %> :<strong id="sStatus"></strong></span>

                            <br />

                            <span class="confirmation">
                                <%= GetGlobalResourceObject("Controls", "viewConfirmation") %> :<strong id="sConfirmation"></strong></span>
                            <br />
                            <div class="ui-widget-content divSalePrice">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSalePrice") %></span>
                                <br />
                                <span id="iOldServicePrice" class="oldSalePrice"></span>
                                <br />
                                <span id="iSalePrice" class="salePrice"></span>
                            </div>
                            <br />
                            <div class="reCalcBtnDiv">
                                <input type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                                    onclick="preCalc(false);" class="ui-button ui-state-default ui-corner-all" />
                            </div>
                            <br />
                            <div class="divPriceDetail">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewOldSalePrice") %> :</span>
                                <span id="iResOldPrice" class="salePrice"></span>
                                <br />
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewNewSalePrice") %> :</span>
                                <span id="iResNewPrice" class="salePrice"></span>
                                <br />
                                <span class="iAgree">
                                    <input id="iAgree" type="checkbox" onclick="clickIAgree()" />
                                    <label for="iAgree"><%= GetGlobalResourceObject("Controls", "iAgree") %></label>
                                </span>
                            </div>
                            <br />
                            <div class="ui-widget-content buttons">
                                <table>
                                    <tr>
                                        <td>
                                            <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>' disabled="disabled"
                                                onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                        </td>
                                        <td>
                                            <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                                                onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <table id="serviceCustomers" class="ui-widget-content customers">
                    <thead>
                        <tr class="ui-widget-header">
                            <th>
                                <strong>
                                    <%= GetGlobalResourceObject("Controls", "lblServiceTourist")%></strong>
                            </th>
                            <th>&nbsp;</th>
                            <th>
                                <strong>
                                    <%= GetGlobalResourceObject("Controls", "lblOtherTourist")%></strong>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="selectedCustomers">
                                <span>Filter:</span>
                                <input id="box1Filter" type="text" class="filterInput" />
                                <button type="button" id="box1Clear" class="ui-button ui-state-default ui-corner-all filterClearBtn">
                                    X</button><br />
                                <select id="box1View" multiple="multiple" class="view">
                                </select><br />
                                <select id="box1Storage">
                                </select>
                            </td>
                            <td class="selectionButtons">
                                <button id="to2" type="button" class="ui-button ui-state-default ui-corner-all">
                                    >
                                </button>
                                <br />
                                <button id="allTo2" type="button" class="ui-button ui-state-default ui-corner-all">
                                    >>
                                </button>
                                <br />
                                <button id="allTo1" type="button" class="ui-button ui-state-default ui-corner-all">
                                    <<
                                </button>
                                <br />
                                <button id="to1" type="button" class="ui-button ui-state-default ui-corner-all">
                                    <
                                </button>
                            </td>
                            <td class="unSelectedCustomers">
                                <span>Filter:</span>
                                <input id="box2Filter" type="text" class="filterInput" />
                                <button type="button" id="box2Clear" class="ui-button ui-state-default ui-corner-all filterClearBtn">
                                    X</button><br />
                                <select id="box2View" multiple="multiple" class="view">
                                </select><br />
                                <select id="box2Storage">
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="dialog-message" title="" class="ui-helper-hidden">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
            </p>
        </div>
    </form>
</body>
</html>
