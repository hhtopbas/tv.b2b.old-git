﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSEdit_Hotel.aspx.cs" Inherits="Controls_RSEdit_Hotel"
    EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "RSEditHotel") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    
    <link href="../CSS/RSEditStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        var newRes = $.query.get('NewRes') == '1' ? true : false;

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function getLocations(recID, location, hotel, room, board, accom) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Hotel.aspx/getLocationList",
                data: '{"recID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sBoard").html("");
                    $("#sAccom").html("");
                    $("#sRoom").html("");
                    $("#sHotel").html("");
                    $("#sLocation").html("");
                    $("#sLocation").append("<option value=''><div>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</div></option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sLocation").append("<option value='" + this.Code + "'><div class=\"tranCss\">" + this.Name + "</div></option>");
                        });
                        if (location != '') { $("#sLocation").val(location); }
                        getHotels(hotel, room, board, accom, recID);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getHotels(hotel, room, board, accom, recID) {
            var location = $("#sLocation").val();
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Hotel.aspx/getHotels",
                data: '{"Location":"' + location + '", "recID":' + recID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sBoard").html("");
                    $("#sAccom").html("");
                    $("#sRoom").html("");
                    $("#sHotel").html("");
                    $("#sHotel").append("<option value=''>" + '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sHotel").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        if (hotel != '') { $("#sHotel").val(hotel); }
                        getRooms(room, accom, recID);
                        getBoards(board, recID);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getRooms(room, accom, recID) {
            var sHotel = $("#sHotel");
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Hotel.aspx/getRooms",
                data: '{"Hotel":"' + sHotel.val() + '","recID":' + recID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sRoom").html("");
                    $("#sRoom").append("<option value='' >" + '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sRoom").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        if (room != '') { $("#sRoom").val(room); }
                        getAccoms(accom, room, recID);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getAccoms(accom, room, recID) {
            var sHotel = $("#sHotel");
            var sRoom = '';
            if (room != '')
                sRoom = room;
            else sRoom = $("#sRoom").val();
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Hotel.aspx/getAccoms",
                data: '{"Hotel":"' + sHotel.val() + '","Room":"' + sRoom + '","recID":' + recID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sAccom").html("");
                    $("#sAccom").append("<option value='' >" + '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sAccom").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        if (accom != '') { $("#sAccom").val(accom); }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getBoards(board, recID) {
            var sHotel = $("#sHotel");
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Hotel.aspx/getBoards",
                data: '{"Hotel":"' + sHotel.val() + '","recID":' + recID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sBoard").html("");
                    $("#sBoard").append("<option value='' >" + '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sBoard").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        if (board != '') { $("#sBoard").val(board); }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function changeHotels() {
            var recID = $("#hfrecID").val();
            getRooms('', '', recID);
            //getAccoms();
            getBoards('', recID);
        }

        function changeLocations() {
            var recID = $("#hfrecID").val();
            getHotels('', '', '', '', recID);
        }


        function preCalc(save) {
            if ($("#sHotel").val() == null || $("#sHotel").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectHotel") %>');
                return;
            }
            if ($("#sRoom").val() == null || $("#sRoom").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectRoom") %>');
                return;
            }
            if ($("#sAccom").val() == null || $("#sAccom").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectAccom") %>');
                return;
            }
            if ($("#sBoard").val() == null || $("#sBoard").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectBoard") %>');
                return;
            }

            var hfrecID = $("#hfrecID").val();
            var _data = '{"RecID":"' + hfrecID + '"' +
                         ',"Hotel":"' + $("#sHotel").val() + '"' +
                         ',"Room":"' + $("#sRoom").val() + '"' +
                         ',"Accom":"' + $("#sAccom").val() + '"' +
                         ',"Board":"' + $("#sBoard").val() + '"' +
                         ',"newRes":' + newRes + '}';
            var _url = "";
            if (save)
                _url = "../Controls/RSEdit_Hotel.aspx/SaveService";
            else _url = "../Controls/RSEdit_Hotel.aspx/CalcService"

            $.ajax({
                type: "POST",
                url: _url,
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var result = $.json.decode(msg.d);
                        if (result.Price != '') {
                            $("#iSalePrice").text(result.Price);
                            $("#iSupplier").text(result.Supplier);
                        } else {
                            showMessage(result.Supplier);
                            return;
                        }
                        if (save) {
                            window.close;
                            self.parent.returnEditResServices(true);
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            if (source == 'save') {
                if ($("#sHotel").val() == null || $("#sHotel").val() == '') {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectHotel") %>');
                    return;
                }
                if ($("#sRoom").val() == null || $("#sRoom").val() == '') {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectRoom") %>');
                    return;
                }
                if ($("#sAccom").val() == null || $("#sAccom").val() == '') {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectAccom") %>');
                    return;
                }
                if ($("#sBoard").val() == null || $("#sBoard").val() == '') {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectBoard") %>');
                    return;
                }

                var hfrecID = $("#hfrecID").val();
                var _data = '{"RecID":"' + hfrecID + '"' +
                         ',"Hotel":"' + $("#sHotel").val() + '"' +
                         ',"Room":"' + $("#sRoom").val() + '"' +
                         ',"Accom":"' + $("#sAccom").val() + '"' +
                         ',"Board":"' + $("#sBoard").val() + '"}';
                if (newRes != true) {
                    $.ajax({
                        type: "POST",
                        url: "../Controls/RSEdit_Hotel.aspx/getChgFee",
                        data: _data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(msg) {
                            if (msg.d != '') {
                                var retVal = $.json.decode(msg.d);
                                if (retVal.retVal == 1) {
                                    $(function() {
                                        $("#messages").html(retVal.Message);
                                        $("#dialog").dialog("destroy");
                                        $("#dialog-message").dialog({
                                            modal: true,
                                            buttons: {
                                                '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>': function() {
                                                    $(this).dialog('close');
                                                    preCalc(true);
                                                },
                                                '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>': function() {
                                                    $(this).dialog('close');
                                                }
                                            }
                                        });
                                    });
                                }
                                else preCalc(true);
                            }
                        },
                        error: function(xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showMessage(xhr.responseText);
                        },
                        statusCode: {
                            408: function() {
                                logout();
                            }
                        }
                    });
                } else preCalc(true);
            }
            else {
                window.close;
                self.parent.returnEditResServices(false);
            }
        }

        function getServiceDetail(recID) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Hotel.aspx/getServiceDetail",
                data: '{"RecID":' + recID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var resService = $.json.decode(msg.d);
                        $("#iCheckIn").text(resService.BegDate);
                        $("#iCheckOut").text(resService.EndDate);
                        $("#iAdult").text(resService.Adult);
                        $("#iChild").text(resService.Child);
                        $("#iNight").text(resService.Night);
                        if (resService.StatConf != '')
                            $("#iConfirmation").text(resService.StatConf);
                        else $("#divConfirmation").hide();
                        $("#iStatus").text(resService.StatSer);
                        var salePrice = resService.SalePrice + ' ' + resService.SaleCur;
                        $("#iSalePrice").text(salePrice);
                        getLocations(recID, resService.Location, resService.Service, resService.Room, resService.Board, resService.Accom);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function onLoad() {
            $.query = $.query.load(location.href);
            var recID = $.query.get('RecID');
            $("#hfrecID").val(recID);
            getServiceDetail(recID);

            $("#sLocation").mousedown(function() { if ($.browser.msie) { $(this).css("width", "auto"); } });
            $("#sLocation").change(function() { if ($.browser.msie) { $(this).css("width", "290px"); } });
            $("#sLocation").blur(function() { if ($.browser.msie) { $(this).css("width", "290px"); } });
        }                
    </script>

</head>
<body onload="onLoad();">
    <form id="formRsHotel" runat="server">
    <br />
    <input id="hfrecID" type="hidden" />
    <div id="divRs">
        <div>
            <table>
                <tr>
                    <td class="leftCell">
                        <div id="divLocation" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewLocation") %>
                                    :</span>
                            </div>
                            <div id="divSLocation" class="inputDiv">
                                <select id="sLocation" onchange="changeLocations()">
                                </select>
                            </div>
                        </div>
                        <div id="divHotel" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewHotel") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sHotel" onchange="changeHotels()">
                                </select>
                            </div>
                        </div>
                        <div id="divChekInOut" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewCheckInOut") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iCheckIn"></span>&nbsp;-&nbsp; <span id="iCheckOut"></span></b>
                            </div>
                        </div>
                        <div id="divNight" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewNight") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iNight"></span></b>
                            </div>
                        </div>
                        <div id="divRoom" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewRoom") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sRoom">
                                </select>
                            </div>
                        </div>
                        <div id="divAccom" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAccom") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sAccom">
                                </select>
                            </div>
                        </div>
                        <div id="divBoard" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewBoard") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sBoard">
                                </select>
                            </div>
                        </div>
                        <div id="divAdultChild" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                    :</span>
                            </div>
                            <div class="divAdultChild" style="text-align: left;">
                                <b><span id="iAdult"></span></b>
                            </div>
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                    :</span>
                            </div>
                            <div class="divAdultChild" style="text-align: left;">
                                <b><span id="iChild"></span></b>
                            </div>
                        </div>
                        <%--<div id="divSupplier" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplier") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplier"></span></b>
                            </div>
                        </div>
                        <div id="divSupplierNote" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplierNote"></span></b>
                            </div>
                        </div>--%>
                    </td>
                    <td valign="top" style="border-left-style: solid; border-width: 1px; border-color: #000000">
                        <div id="divStatus">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewStatus") %></span>
                            <br />
                            <b><span id="iStatus"></span></b>
                        </div>
                        <br />
                        <div id="divConfirmation">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewConfirmation") %></span>
                            <br />
                            <b><span id="iConfirmation"></span></b>
                        </div>
                        <br />
                        <div id="divSalePrice">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewSalePrice") %></span>
                            <br />
                            <span id="iSalePrice" class="salePrice"></span>
                        </div>
                        <br />
                        <div id="divCalcBtn">
                            <input id="btnRecalc" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                                onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                style="width: 150px;" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <table id="divBtn">
            <tr>
                <td style="width: 50%;" align="center">
                    <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
                        onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
                <td align="center">
                    <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                        onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
            </tr>
        </table>
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
            </span><span id="messages">Message</span>
        </p>
    </div>
    </form>
</body>
</html>
