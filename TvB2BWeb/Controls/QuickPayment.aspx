﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QuickPayment.aspx.cs" Inherits="Controls_QuickPayment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Quick Payment</title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/QuickPayment.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        function showMessage(msg) {
            var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: [
                    {
                        text: btnOK,
                        click: function () { $(this).dialog('close'); }
                    }]
            });
        }

        function goPayment() {
            var pQ = localStorage.getItem("PayQueue");
            if (pQ != null)
                return false;
            $("#btnPayment").prop("disabled", true);
            localStorage.setItem("PayQueue", "1");
            var params = new Object();
            params.AccountType = $("#iAccountType").val();
            params.Account = $("#iAccount").val();
            params.PaymentType = $("#iPaymentType").val();
            params.PayAmount = $("#iPayAmount").val();
            params.PayAmountCur = $("#iPayAmountCur").val();
            params.Explanation = '';//$("#iExplanation").val();
            params.Reference = $("#iReference").val();
            if (params.PaymentType == '') {
                showMessage('Please select payment type.');
                $("#btnPayment").prop("disabled", false);
                localStorage.removeItem("PayQueue");
                return;
            }
            $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/QuickPayment.aspx/setPayment",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        if (msg.d.isPayment) {
                            $("#paymentDiv").hide();
                            $("#paymentMessageDiv").show();
                            $("#message").html(msg.d.Message);
                        }
                        else {
                            $("#message").html(msg.d.Message);
                            $("#paymentMessageDiv").show();
                        }
                    }
                    else {
                        $("#paymentMessageDiv").show();
                        $("#message").html('Payment could not be received.');
                    }
                    localStorage.removeItem("PayQueue");
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        $("#btnPayment").prop("disabled", false);
                        localStorage.removeItem("PayQueue");
                        showMessage(xhr.responseText);
                    }

                },
                statusCode: {
                    408: function () {
                        localStorage.removeItem("PayQueue");
                        logout();
                    }
                }
            });
        }

        function onSelect() {
            accountList = $("#iAccountType").data('iAcounts');
            $("#iAccount").val('');
            if (accountList != null) {
                $.each(accountList, function (i) {
                    if ($("#iAccountType").val() == this.Code)
                        $("#iAccount").val(this.Name);
                });

            }
        }

        function getFormData() {
            var params = new Object();
            $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/QuickPayment.aspx/getFormData",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $("#iAccountType").html('');
                        if (msg.d.iAccountTypeList != null) {
                            $.each(msg.d.iAccountTypeList, function (i) {
                                $("#iAccountType").append('<option value="' + this.Code + '">' + this.Name + '</option>');
                            });
                        }
                        $("#iAccountType").val(msg.d.iAccountType);
                        $("#iAccount").val('');
                        if (msg.d.iAccountList != null) {
                            $.each(msg.d.iAccountList, function (i) {
                                if ($("#iAccountType").val() == this.Code) {
                                    $("#iAccount").val(this.Name);
                                    if ($("#iAccountType").val() == '0') {
                                        $("#iAccount").prop('disabled', 'disabled');
                                        $("#iAccountType").prop('disabled', 'disabled');
                                    }
                                    else {
                                        $("#iAccount").removeAttr('disabled');
                                        $("#iAccountType").prop('disabled', 'disabled');
                                    }
                                }
                            });
                        }
                        $("#iAccountType").data('iAcounts', msg.d.iAccountList);
                        $("#iPaymentType").html('');
                        $("#iPaymentType").append('<option value="">---</option>');
                        if (msg.d.iPaymentTypeList != null) {
                            $.each(msg.d.iPaymentTypeList, function (i) {
                                $("#iPaymentType").append('<option value="' + this.Code + '">' + this.Name + '</option>');
                            });
                        }
                        $("#iPayAmount").val('');
                        $("#iPayAmount").val(msg.d.iAmount);

                        $("#iPayAmountCur").html('');
                        if (msg.d.iPayAmountCurList != null) {
                            $.each(msg.d.iPayAmountCurList, function (i) {
                                $("#iPayAmountCur").append('<option value="' + this.Curr + '">' + this.Name + '</option>');
                            });
                        }
                        $("#iPayAmountCur").val(msg.d.iSaleCurr);

                        $("#iPaidDate").html(msg.d.iPaidDate);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getAmountByPayType() {
            var payType = $("#iPaymentType").val();
            var payCur = $("#iPayAmountCur").val();
            if (payType != '' && payCur != '') {
                var params = new Object();
                params.pPayTypeCode = payType;
                params.pPayCurrency = payCur;
                 $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/QuickPayment.aspx/changeAmountByPaymentType",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        if (msg.d.Amount) {
                            $("#iPayAmount").val(msg.d.Amount);
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        $("#btnPayment").prop("disabled", false);
                        localStorage.removeItem("PayQueue");
                        showMessage(xhr.responseText);
                    }

                },
                statusCode: {
                    408: function () {
                        localStorage.removeItem("PayQueue");
                        logout();
                    }
                }
            });
            }
        }

        $(document).ready(function () {
            getFormData();
        });
    </script>
</head>
<body>
    <form id="paymentForm" runat="server">
        <div id="paymentDiv" class="ui-widget">
            <div class="header">Payment</div>
            <div class="inputrow ui-helper-clearfix">
                <span class="label">Account type</span>
                <div class="editinput">
                    <select id="iAccountType" onchange="onSelect()"></select>
                </div>
            </div>
            <div class="inputrow ui-helper-clearfix">
                <span class="label">Account</span>
                <div class="editinput">
                    <input id="iAccount" type="text" />
                </div>
            </div>
            <div class="inputrow ui-helper-clearfix">
                <span class="label">Paid date</span>
                <div class="editinputdate">
                    <span id="iPaidDate"></span>
                </div>
            </div>
            <div class="inputrow ui-helper-clearfix">
                <span class="label">Payment type</span>
                <div class="editinput">
                    <select id="iPaymentType" onchange="getAmountByPayType()"></select>
                </div>
            </div>
            <div class="inputrow ui-helper-clearfix">
                <span class="label">Pay Amount</span>
                <div class="editinput">
                    <input id="iPayAmount" type="text" class="amount" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <select id="iPayAmountCur" class="currency" onchange="getAmountByPayType()"></select>
                </div>
            </div>
            <%--<div class="inputrow ui-helper-clearfix">
                <span class="label">Explanation</span>
                <div class="editinput">
                    <input id="iExplanation" type="text" />
                </div>
            </div>--%>
            <div class="inputrow ui-helper-clearfix">
                <span class="label">Referance</span>
                <div class="editinput">
                    <input id="iReference" type="text" />
                </div>
            </div>
            <br />
            <br />
            <br />
            <input type="button" id="btnPayment" value="Payment" onclick="goPayment()" />
        </div>
        <div id="paymentMessageDiv" class="ui-widget ui-helper-hidden">
            <span id="message" class="paymentMessage"></span>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
            </p>
        </div>
    </form>
</body>
</html>
