﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSEdit_Transport.aspx.cs"
    Inherits="Controls_RSEdit_Transport" EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <title><%= GetGlobalResourceObject("PageTitle", "RSEditTransport") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    
    <link href="../CSS/RSEditStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        var newRes = $.query.get('NewRes') == '1' ? true : false;

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function depLocationChanged() {
            getPickups();
        }

        function getDeparture(recID, departure) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Transport.aspx/getDeparture",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {                                                            
                    if (msg.d != '') {
                        $("#sDepLocation").html("");
                        $.each($.json.decode(msg.d), function (i) {
                            $("#sDepLocation").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        $("#sDepLocation").val(departure);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function arrLocationChanged() {
            getPickups();
        }

        function getArrival(recID, arrival) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Transport.aspx/getArrival",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {                                                           
                    if (msg.d != '') {
                        $("#sArrLocation").html("");
                        $.each($.json.decode(msg.d), function (i) {
                            $("#sArrLocation").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        $("#sArrLocation").val(arrival);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getService(recID, service) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Transport.aspx/getService",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sPickup").html("");
                    $("#sBus").html("");
                    $("#sService").html("");
                    $("#sService").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sService").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        $("#sService").val(service);
                        changeService();
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getBus() {
            var hfRecID = document.getElementById("<%= hfRecID.ClientID %>");
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Transport.aspx/getBus",
                data: '{"RecID":"' + hfRecID.value + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sBus").html("");
                    $("#sBus").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sBus").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        $("#sBus").val($("#hfBus").val());
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getPickups() {
            var data = new Object();
            data.RecID = $("#hfRecID").val();
            data.Departure = $("#sDepLocation").val();
            data.Arrival = $("#sArrLocation").val();
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Transport.aspx/getPickups",
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sPickup").html("");
                    $("#sPickup").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sPickup").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                        $("#sPickup").val($("#hfPickup").val());
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function changeService() {
            getBus();
            getPickups();
        }

        function preCalc(save) {
            if ($("#sService").val() == null || $("#sService").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTransport") %>');
                return;
            }

            var hfRecID = document.getElementById("<%= hfRecID.ClientID %>");
            var data = new Object();
            
            data.RecID = hfRecID.value;
            data.Pickup = $("#sPickup").val();
            data.Service = $("#sService").val();
            data.Bus = $("#sBus").val();
            data.newRes = newRes;
            data.Departure = $("#sDepLocation").val();
            data.Arrival = $("#sArrLocation").val();
            var _url = "";
            if (save)
                _url = "../Controls/RSEdit_Transport.aspx/SaveService";
            else _url = "../Controls/RSEdit_Transport.aspx/CalcService";
            $.ajax({
                type: "POST",
                url: _url,
                data: $.json.encode(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (save) {
                        window.close;
                        self.parent.returnEditResServices(true);
                    }
                    if (msg.d != '') {
                        var result = $.json.decode(msg.d);
                        if (result.Price != '') {
                            $("#iSalePrice").text(result.Price);
                            $("#iSupplier").text(result.Supplier);
                        } else showMessage(result.Supplier);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            if (source == 'save') {
                if ($("#sService").val() == null || $("#sService").val() == '') {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTransport") %>');
                    return;
                }

                var hfRecID = document.getElementById("<%= hfRecID.ClientID %>");
                var _data = '{"RecID":"' + hfRecID.value + '"' +
                         ',"Pickup":"' + $("#sPickup").val() + '"' +
                         ',"Service":"' + $("#sService").val() + '"' +
                         ',"Bus":"' + $("#sBus").val() + '"}';
                if (newRes != true) {
                    $.ajax({
                        type: "POST",
                        url: "../Controls/RSEdit_Transport.aspx/getChgFee",
                        data: _data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(msg) {
                            if (msg.d != '') {
                                var retVal = $.json.decode(msg.d);
                                if (retVal.retVal == 1) {
                                    $(function() {
                                        $("#messages").html(retVal.Message);
                                        $("#dialog").dialog("destroy");
                                        $("#dialog-message").dialog({
                                            modal: true,
                                            buttons: {
                                                '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>': function() {
                                                    $(this).dialog('close');
                                                    preCalc(true);
                                                },
                                                '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>': function() {
                                                    $(this).dialog('close');
                                                }
                                            }
                                        });
                                    });
                                }
                                else preCalc(true);
                            }
                        },
                        error: function(xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showMessage(xhr.responseText);
                        },
                        statusCode: {
                            408: function() {
                                logout();
                            }
                        }
                    });
                } else preCalc(true);
            }
            else
                if (source == 'cancel') {
                window.close;
                self.parent.returnEditResServices(false);
            }
        }

        function getServiceDetail(recID) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Transport.aspx/getServiceDetail",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var resService = $.json.decode(msg.d);
                        $("#iDate").text(resService.BegDate);
                        $("#iPickupDate").text(resService.EndDate);                        
                        $("#iAdult").text(resService.Adult);
                        $("#iChild").text(resService.Child);
                        $("#iNight").text(resService.Duration);
                        $("#iUnit").text(resService.Unit);                        
                        if (resService.StatConf != '')
                            $("#sConfirmation").text(resService.StatConf);
                        else $("#divConfirmation").hide();
                        $("#sStatus").text(resService.StatSer);
                        var salePrice = resService.SalePrice + ' ' + resService.SaleCur;
                        $("#iSalePrice").text(salePrice);
                        $("#hfBus").val(resService.Bus);
                        $("#hfPickup").val(resService.Pickup);
                        getDeparture(recID, resService.DepLocation);
                        getArrival(recID, resService.ArrLocation);
                        getService(recID, resService.Service);
                        getPickups();
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function onLoad() {
            $.query = $.query.load(location.href);
            var recID = $.query.get('RecID');
            var hfRecID = document.getElementById("<%= hfRecID.ClientID %>");
            hfRecID.value = recID;
            getServiceDetail(recID);
        }                
    </script>

</head>
<body onload="onLoad();">
    <form id="formRsHotel" runat="server">
    <br />
    <div id="divRs">
        <div>
            <table>
                <tr>
                    <td class="leftCell">
                        <div id="divDepLocation" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewDeparture") %>
                                    :</span>
                            </div>
                            <div id="divSDepLocation" class="inputDiv">
                                <select id="sDepLocation" onchange="depLocationChanged()"></select>
                            </div>
                        </div>
                        <div id="divArrLocation" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewArrival") %>
                                    :</span>
                            </div>
                            <div id="divSArrLocation" class="inputDiv">
                                <select id="sArrLocation" onchange="arrLocationChanged()"></select>
                            </div>
                        </div>
                        <div id="divTransportDate" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewDate") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iDate"></span></b>
                            </div>
                        </div>
                        <div id="divService" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewTransport") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sService" onchange="changeService();">
                                </select>
                            </div>
                        </div>
                        <div id="divPickupDate" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewPickupDate") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iPickupDate"></span></b>
                            </div>
                        </div>
                        <div id="divPickupPoint" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewPickupPoint") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sPickup">
                                </select>
                            </div>
                        </div>
                        <div id="divBus" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewBus") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sBus">
                                </select>
                            </div>
                        </div>
                        <div id="divAdultChild" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                    :</span>
                            </div>
                            <div style="float: left; width: 100px; height: 100%; text-align: left;">
                                <b><span id="iAdult"></span></b>
                            </div>
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                    :</span>
                            </div>
                            <div style="float: left; width: 100px; height: 100%; text-align: left;">
                                <b><span id="iChild"></span></b>
                            </div>
                        </div>
                        <%--<div id="divSupplier" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplier") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplier"></span></b>
                            </div>
                        </div>
                        <div id="divSupplierNote" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplierNote"></span></b>
                            </div>
                        </div>--%>
                    </td>
                    <td class="rightCell">
                        <div id="divStatus">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewStatus") %></span>
                            <br />
                            <b><span id="sStatus"></span></b>
                        </div>
                        <br />
                        <div id="divConfirmation">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewConfirmation") %></span>
                            <br />
                            <b><span id="sConfirmation"></span></b>
                        </div>
                        <br />
                        <div id="divSalePrice">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewSalePrice") %></span>
                            <br />
                            <span id="iSalePrice" class="salePrice"></span>
                        </div>
                        <br />
                        <div id="divCalcBtn">
                            <input id="btnRecalc" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                                onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                style="width: 150px;" />
                        </div>
                        <asp:HiddenField ID="hfRecID" runat="server" />
                        <asp:HiddenField ID="hfBus" runat="server" />
                        <asp:HiddenField ID="hfPickup" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <table id="divBtn">
            <tr>
                <td style="width: 50%;" align="center">
                    <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
                        onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
                <td align="center">
                    <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                        onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
            </tr>
        </table>
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
            </span><span id="messages">Message</span>
        </p>
    </div>
    </form>
</body>
</html>
