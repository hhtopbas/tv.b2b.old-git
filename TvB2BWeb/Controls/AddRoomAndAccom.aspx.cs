﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using TvTools;
using System.Text;

public partial class Controls_AddRoomAndAccom : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(string PaxCount)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string errorMsg = string.Empty;
        StringBuilder sb = new StringBuilder();

        Int16? pax = Conversion.getInt16OrNull(PaxCount);

        if (ResData.ResService.Where(w => w.ServiceType == "HOTEL").Count() < 1) return "";

        var hotelServices = from q in ResData.ResService
                            where q.ServiceType == "HOTEL"
                            group q by new { q.Service } into k
                            select new
                            {
                                RecID = (ResData.ResService.Where(w => w.ServiceType == "HOTEL" && w.Service == k.Key.Service).OrderBy(o=>o.SeqNo).FirstOrDefault().RecID),
                                HotelCode = k.Key.Service
                            };
        
        foreach (var row in hotelServices)
        {
            string Hotel = string.Empty;
            List<TvBo.HotelAccomPaxRecord> accomPax = new Hotels().getHotelAccomPax(UserData.Market, row.HotelCode, ref errorMsg);
            HotelRecord hotel = new Hotels().getHotelDetail(UserData, row.HotelCode, ref errorMsg);
            var query = from q in accomPax
                        where
                            (q.Adult + q.ChdAgeG1 + q.ChdAgeG2 + q.ChdAgeG3 + q.ChdAgeG4) >= pax &&
                            (q.Adult + q.ChdAgeG1 + q.ChdAgeG2 + q.ChdAgeG3 + q.ChdAgeG4) <= pax
                        group q by new { Room = q.Room, RoomName = q.RoomName, Accom = q.Accom, AccomName = q.AccomName, q.Adult, Childs = (q.ChdAgeG1 + q.ChdAgeG2 + q.ChdAgeG3 + q.ChdAgeG4) } into k
                        select new { Room = k.Key.Room, RoomName = k.Key.RoomName, Accom = k.Key.Accom, AccomName = k.Key.AccomName, Adult = k.Key.Adult, Childs = k.Key.Childs };
            foreach (var r in query)
            {
                sb.Append("<tr>");
                sb.AppendFormat("<td class=\"leftTD\"><input type=\"radio\" value=\"{0}\" name=\"rooms\" /></td>", row.HotelCode + ";" + r.Room + ";" + r.Accom + ";" + r.Adult + ";" + r.Childs + ";" + row.RecID);
                string HotelName = hotel.LocalName + ", " + hotel.Category + " (" + hotel.LocationLocalName + ")";
                sb.AppendFormat("<td>{0}</td>", "(" + HotelName + ")," + r.RoomName + " " + r.AccomName + "(" + r.Adult.ToString() + " " +
                        HttpContext.GetGlobalResourceObject("LibraryResource", "lblAdult").ToString() + "," + r.Childs + " " +
                        HttpContext.GetGlobalResourceObject("LibraryResource", "lblChild").ToString() + ")");
                sb.Append("</tr>");
            }
        }
        if (sb.Length > 0)
        {
            return "<table>" + sb.ToString() + "</table>";
        }
        else return sb.ToString();
    }
}
