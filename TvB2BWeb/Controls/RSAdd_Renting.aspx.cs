﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSAdd_Renting : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ppcCheckIn.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckIn.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcCheckIn.From.Date = DateTime.Today;
        ppcCheckOut.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckOut.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcCheckOut.From.Date = DateTime.Today;

        if (!IsPostBack)
        {            
            //sLocation.Attributes.Add("onchange", "changeLocation();");            

            ppcCheckIn.DateValue = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value: DateTime.Today;
            ppcCheckOut.DateValue = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
            iNight.Text = (ppcCheckOut.DateValue - ppcCheckIn.DateValue).Days.ToString();
        }
    }    

    [WebMethod(EnableSession=true)]
    public static string getLocationList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];        
        string errorMsg = string.Empty;
        List<RentingPriceRecord> rentingPrices = new Rentings().getRentingPrices(UserData.Market, ResData.ResMain.PLMarket, string.Empty, null, ResData.ResMain.BegDate, ResData.ResMain.EndDate, ref errorMsg);
        var query = from q in rentingPrices
                    group q by new { RecID = q.Location, Name = q.LocationLocalName } into k
                    select new { RecID = k.Key.RecID, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod(EnableSession = true)]
    public static string getRenting(string Location)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? location = Conversion.getInt32OrNull(Location);        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        List<RentingRecord> rentingList = new Rentings().getRentingList(UserData.Market, ResData.ResMain.PLMarket, location, ResData.ResMain.BegDate, ResData.ResMain.EndDate, ref errorMsg);
        var query = from q in rentingList
                    select new { Code = q.Code , Name = q.LocalName };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    where s.Status == 0
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string CalcService(string selectedCusts, string Location, string Renting, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;        
        int? location = Conversion.getInt32OrNull(Location);        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);        
        string[] selectedCust = selectedCusts.Split('|');        
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);
        if (!begDate.HasValue && !endDate.HasValue) return retVal;

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)        
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;        
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "RENTING", Renting, string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, location, location, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
            DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "RENTING", ServiceID, ref errorMsg);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", returnData.Rows[0]["SalePrice"].ToString() + " " + returnData.Rows[0]["SaleCur"].ToString(), supplierRec.NameL);
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string SaveService(string selectedCusts, string Location, string Renting, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat, string recordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        int? location = Conversion.getInt32OrNull(Location);        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];        
        string[] selectedCust = selectedCusts.Split('|');        
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo                    
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);
        if (!begDate.HasValue && !endDate.HasValue) return retVal;

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).TotalDays);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).TotalDays);
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "RENTING", Renting,
            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, location, location, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            if (_recordType == true)
            {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                {
                    List<ResServiceRecord> resServiceList = ResData.ResService;
                    ResServiceRecord resService = resServiceList.LastOrDefault();
                    resService.ExcludeService = false;
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
                else
                {
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
            }
            else
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "<br />";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "\n";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
            }
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string PaxControl(string selectedCusts)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();            
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;            
        }        
        retVal = string.Format("\"Adult\":{0},\"Child\":{1}", AdlCnt.ToString(), ChdCnt.ToString());
        return "{" + retVal + "}";
    }

}
