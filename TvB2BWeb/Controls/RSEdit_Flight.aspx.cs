﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using System.Text;
using TvTools;

public partial class Controls_RSEdit_Flight : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {

        }
    }

    [WebMethod(EnableSession = true)]
    public static string getServiceDetail(string RecID)
    {
        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        Int32? recID = Conversion.getInt32OrNull(RecID);
        if (!recID.HasValue) return string.Empty;

        var query = from q in ResData.ResService
                    where q.RecID == recID.Value
                    select new
                    {
                        Departure = q.DepLocationNameL,
                        Arrival = q.ArrLocationNameL,
                        FlyDate = q.BegDate.Value.ToShortDateString(),
                        Service = q.Service,
                        FlgClass = q.FlgClass,
                        Adult = q.Adult,
                        Child = q.Child,
                        Unit = q.Unit,
                        Supplier = q.SupplierName,
                        SupNote = q.SupNote,
                        StatConf = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ? q.StatConfNameL : string.Empty,
                        StatSer = q.StatSerNameL,
                        PnrNo = q.PnrNo,
                        SalePrice = !Equals(q.IncPack, "Y") ? q.SalePrice.Value.ToString("#,###.00") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        SaleCur = !Equals(q.IncPack, "Y") ? q.SaleCur : ""
                    };
        if (query == null || query.Count() < 1) return string.Empty;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query.FirstOrDefault());
    }

    [WebMethod(EnableSession = true)]
    public static string getFlights(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord service = ResData.ResService.Find(f => f.RecID == recID);

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
        {
            List<getPLFlightsRecord> getPLFlights = new Flights().getPListFlights(UserData, ResData, service.Unit, ref errorMsg);
            List<FlightRecord> getFlights = new Flights().getFlights(UserData.Market, ref errorMsg);
            var query = from q in getPLFlights
                        join q1 in getFlights on q.DepFlightNo equals q1.Code
                        group q by new { FlightNo = q.DepFlightNo, Name = q.DepFlightNo + " (" + q1.DepAir + "=>" + q1.ArrAir + ") (" + q.DepFlyTime.Value.ToShortTimeString() + "-" + q1.ArrTime.Value.ToShortTimeString() + ")" } into k
                        select new { FlightNo = k.Key.FlightNo, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        else
        {
            List<FlightDayRecord> flights = new Flights().getRouteFlights(UserData, UserData.Market, service.BegDate, service.DepLocation, service.ArrLocation, ResData.ResMain.HolPack, ref errorMsg);
            var query = from q in flights
                        where q.DepCity == service.DepLocation && q.ArrCity == service.ArrLocation
                        group q by new { FlightNo = q.FlightNo, Name =(!string.IsNullOrEmpty(q.PNLName)?q.PNLName:q.FlightNo) + " (" + q.DepAirport + "=>" + q.ArrAirport + ") (" + q.DepTime.Value.ToShortTimeString() + "-" + q.ArrTime.Value.ToShortTimeString() + ")" } into k
                        select new { FlightNo = k.Key.FlightNo, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getClass(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord service = ResData.ResService.Find(f => f.RecID == recID);

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
        {
            List<getPLFlightsRecord> getPLFlights = new Flights().getPListFlights(UserData, ResData, service.Unit, ref errorMsg);
            var query = from q in getPLFlights
                        group q by new { SClass = q.DepFlgClass } into k
                        select new { Code = k.Key.SClass, Name = k.Key.SClass };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        else
        {
            List<string> flightClass = new Flights().getFlightClass(UserData.Market, service.Service, service.BegDate.Value, ref errorMsg);
            var query = from q in flightClass
                        select new { Code = q, Name = q };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string CalcService(string RecID, string Flight, string SClass, bool? newRes)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResDataRecord tmpResData = new ResDataRecord();
        tmpResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID.Value);
        List<getPLFlightsRecord> getPLFlights = new List<getPLFlightsRecord>();
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            getPLFlights = new Flights().getPListFlights(UserData, ResData, resService.Unit, ref errorMsg);
        string currentFlight = resService.Service;
        string currentClass = resService.FlgClass;
        FlightDetailRecord flightDetail = new FlightDetailRecord();

        resService.PriceSource = !Equals(resService.Service, Flight) || !Equals(resService.FlgClass, SClass) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Flight;
        resService.FlgClass = SClass;
        resService.Supplier = string.Empty;

        resService.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                            (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
        resService.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                            (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";

        ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
        {
            #region Sunrise
            if (ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0)).Count() == 2)
            {
                if (resService.BegDate == ResData.ResMain.BegDate)
                {
                    ResServiceRecord otherFlight = tmpResData.ResService.Find(f => f.Service != currentFlight && f.ServiceType == "FLIGHT");
                    getPLFlightsRecord seconfFlight = getPLFlights.Find(f => f.DepFlightNo == Flight && f.DepFlgClass == SClass);
                    if (otherFlight == null || seconfFlight == null)
                    {
                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "ServiceCannotBeChanged").ToString();
                        retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                        return "{" + retVal + "}";
                    }
                    if (seconfFlight != null)
                    {
                        resService = ResData.ResService.Find(f => f.RecID == otherFlight.RecID);
                        resService.Service = seconfFlight.RetFlightNo;
                        resService.FlgClass = seconfFlight.RetFlgClass;
                        resService.Supplier = string.Empty;
                        ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
                    }
                }
                else
                {
                    ResServiceRecord otherFlight = tmpResData.ResService.Find(f => f.Service != currentFlight && f.ServiceType == "FLIGHT");
                    getPLFlightsRecord seconfFlight = getPLFlights.Find(f => f.RetFlightNo == Flight && f.RetFlgClass == SClass);
                    if (otherFlight == null || seconfFlight == null)
                    {
                        errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "ServiceCannotBeChanged").ToString();
                        retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                        return "{" + retVal + "}";
                    }
                    if (seconfFlight != null)
                    {
                        resService = ResData.ResService.Find(f => f.RecID == otherFlight.RecID);
                        resService.Service = seconfFlight.DepFlightNo;
                        resService.FlgClass = seconfFlight.DepFlgClass;
                        resService.Supplier = string.Empty;
                        ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
                    }
                }
            }
            #endregion Sunrise
        }
        else
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
            {
                #region Detur
                getPLFlightsRecord returnFlight = new Flights().getReturnFlight(UserData.Market, resService.Service, resService.BegDate, resService.FlgClass, ResData.ResMain.Days, resService.Unit, ref errorMsg);
                if (returnFlight != null)
                {
                    List<ResServiceRecord> retFlightList = (from q1 in ResData.ResService
                                                            join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                            where q1.ServiceType == "FLIGHT" && q1.RecID != resService.RecID
                                                            select q1).ToList<ResServiceRecord>();
                    if (retFlightList.Count > 0)
                    {
                        ResServiceRecord retFlight = ResData.ResService.Find(f => f.RecID == retFlightList.FirstOrDefault().RecID);
                        if (retFlight != null)
                        {
                            retFlight.IncPack = resService.PriceSource == 0 ? "N" : "Y";
                            retFlight.Service = returnFlight.RetFlightNo;
                            retFlight.FlgClass = SClass;
                            retFlight.Supplier = string.Empty;
                            flightDetail = new FlightDetailRecord();
                            flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, retFlight.Service, retFlight.BegDate.Value, ref errorMsg);
                            retFlight.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                            retFlight.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;

                            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
                        }
                    }
                }
                #endregion
            }

        if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            resService = ResData.ResService.Find(f => f.RecID == recID.Value);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
            string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                        SalePriceStr,
                        supplierRec.NameL);
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string SaveService(string RecID, string Flight, string SClass, bool? newRes)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResDataRecord tmpResData = new ResDataRecord();
        tmpResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID.Value);
        List<int?> recIDList = new List<int?>();
        recIDList.Add(resService.RecID);

        List<getPLFlightsRecord> getPLFlights = new List<getPLFlightsRecord>();
        getPLFlightsRecord orjFlight = null;
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            orjFlight = new Flights().getReturnFlight(UserData.Market, resService.Service, resService.BegDate, resService.FlgClass, ResData.ResMain.Days, resService.Unit, ref errorMsg);

        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
            getPLFlights = new Flights().getPListFlights(UserData, ResData, resService.Unit, ref errorMsg);

        string currentFlight = resService.Service;
        string currentClass = resService.FlgClass;

        resService.PriceSource = !Equals(resService.Service, Flight) || !Equals(resService.FlgClass, SClass) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Flight;
        resService.FlgClass = SClass;
        resService.Supplier = string.Empty;

        FlightDetailRecord flightDetail = new FlightDetailRecord();
        flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, resService.Service, resService.BegDate.Value, ref errorMsg);
        resService.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                            (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
        resService.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                            (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;

        if (newRes.HasValue && newRes.Value)
        {
            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise) && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
            {
                #region Sunrise
                if (ResData.ResService.Where(w => w.ServiceType == "FLIGHT" && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0)).Count() == 2)
                {
                    if (resService.BegDate == ResData.ResMain.BegDate)
                    {
                        ResServiceRecord otherFlight = tmpResData.ResService.Find(f => f.Service != currentFlight && f.ServiceType == "FLIGHT");
                        getPLFlightsRecord seconfFlight = getPLFlights.Find(f => f.DepFlightNo == Flight && f.DepFlgClass == SClass);
                        if (otherFlight == null || seconfFlight == null)
                        {
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "ServiceCannotBeChanged").ToString();
                            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                            return "{" + retVal + "}";
                        }
                        if (seconfFlight != null)
                        {
                            TvBo.ResServiceRecord resServiceSecond = ResData.ResService.Find(f => f.RecID == otherFlight.RecID);
                            resServiceSecond.Service = seconfFlight.RetFlightNo;
                            resServiceSecond.FlgClass = seconfFlight.RetFlgClass;
                            resServiceSecond.Supplier = string.Empty;
                            flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, resServiceSecond.Service, resServiceSecond.BegDate.Value, ref errorMsg);
                            resServiceSecond.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                            resServiceSecond.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;
                            ResData = new Reservation().extraServiceControl(UserData, ResData, resServiceSecond, ref errorMsg);
                            recIDList.Add(resServiceSecond.RecID);
                        }
                    }
                    else
                    {
                        ResServiceRecord otherFlight = tmpResData.ResService.Find(f => f.Service != currentFlight && f.ServiceType == "FLIGHT");
                        getPLFlightsRecord seconfFlight = getPLFlights.Find(f => f.RetFlightNo == Flight && f.RetFlgClass == SClass);
                        if (otherFlight == null || seconfFlight == null)
                        {
                            errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "ServiceCannotBeChanged").ToString();
                            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                            return "{" + retVal + "}";
                        }
                        if (seconfFlight != null)
                        {
                            TvBo.ResServiceRecord resServiceSecond = ResData.ResService.Find(f => f.RecID == otherFlight.RecID);
                            resServiceSecond.Service = seconfFlight.DepFlightNo;
                            resServiceSecond.FlgClass = seconfFlight.DepFlgClass;
                            resServiceSecond.Supplier = string.Empty;
                            flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, resServiceSecond.Service, resServiceSecond.BegDate.Value, ref errorMsg);
                            resServiceSecond.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                            resServiceSecond.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;
                            ResData = new Reservation().extraServiceControl(UserData, ResData, resServiceSecond, ref errorMsg);
                            recIDList.Add(resServiceSecond.RecID);
                        }
                    }
                }
                #endregion
            }
            else
                if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
                {
                    #region Detur
                    getPLFlightsRecord returnFlight = new Flights().getReturnFlight(UserData.Market, resService.Service, resService.BegDate, resService.FlgClass, ResData.ResMain.Days, resService.Unit, ref errorMsg);
                    if (returnFlight != null)
                    {
                        List<ResServiceRecord> retFlightList = (from q1 in ResData.ResService
                                                                join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                                where q1.ServiceType == "FLIGHT" && q1.RecID != resService.RecID
                                                                select q1).ToList<ResServiceRecord>();
                        if (retFlightList.Count > 0)
                        {
                            ResServiceRecord retFlight = ResData.ResService.Find(f => f.RecID == retFlightList.FirstOrDefault().RecID);
                            if (retFlight != null)
                            {
                                retFlight.IncPack = resService.PriceSource == 0 ? "N" : "Y";
                                retFlight.Service = returnFlight.RetFlightNo;
                                retFlight.FlgClass = SClass;
                                retFlight.Supplier = string.Empty;
                                flightDetail = new FlightDetailRecord();
                                flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, retFlight.Service, retFlight.BegDate.Value, ref errorMsg);
                                retFlight.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                    (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                                retFlight.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                    (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;

                                ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
                                recIDList.Add(retFlight.RecID);
                            }
                        }
                    }
                    #endregion
                }

            if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);

                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                            SalePriceStr,
                            supplierRec.NameL);
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        else
        {
            Int32 ErrCode = 0;
            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && (resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0))
            {
                #region Detur
                getPLFlightsRecord returnFlight = new Flights().getReturnFlight(UserData.Market, resService.Service, resService.BegDate, resService.FlgClass, ResData.ResMain.Days, resService.Unit, ref errorMsg);
                if (returnFlight != null)
                {
                    List<ResServiceRecord> retFlightList = (from q1 in ResData.ResService
                                                            join q2 in ResData.ResCon on q1.RecID equals q2.ServiceID
                                                            where q1.ServiceType == "FLIGHT" && q1.RecID != resService.RecID
                                                            select q1).ToList<ResServiceRecord>();
                    if (retFlightList.Count > 0)
                    {
                        ResServiceRecord retFlight = ResData.ResService.Find(f => f.RecID == retFlightList.FirstOrDefault().RecID);
                        if (retFlight != null)
                        {
                            retFlight.PriceSource = resService.PriceSource;
                            retFlight.IncPack = resService.PriceSource == 0 ? "N" : "Y";
                            retFlight.Service = returnFlight.RetFlightNo;
                            retFlight.FlgClass = SClass;
                            retFlight.Supplier = string.Empty;
                            flightDetail = new FlightDetailRecord();
                            flightDetail = new Flights().getFlightDetail(UserData, UserData.Market, retFlight.Service, retFlight.BegDate.Value, ref errorMsg);
                            retFlight.ServiceName = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")";
                            retFlight.ServiceNameL = flightDetail.FlightNo + " (" + flightDetail.DepAirport + "->" + flightDetail.ArrAirport + "), " + resService.FlgClass + ", (" +
                                                                (flightDetail.DepTime.HasValue ? flightDetail.DepTime.Value.ToString("HH:mm") : "") + "-" + (flightDetail.ArrTime.HasValue ? flightDetail.ArrTime.Value.ToString("HH:mm") : "") + ")"; ;

                            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData(ResData), retFlight, ref errorMsg);
                            recIDList.Add(retFlight.RecID);
                        }
                    }
                }
                #endregion
            }            
            recIDList.Add(resService.RecID);
            ResData = new TvBo.Reservation().editService(UserData, ResData, recIDList, ref ErrCode, ref errorMsg, false);

            if (!string.IsNullOrEmpty(errorMsg))
            {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            }
            else
                if (ErrCode != 0)
                {
                    if (ErrCode < 0)
                        errorMsg = string.IsNullOrEmpty(errorMsg) ? "Bilinmeyen hata" : errorMsg;
                    else errorMsg = new ResCalcError().calcError(ErrCode, ResData).ToString();
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                }
                else
                {
                    resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                    SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                    string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                                SalePriceStr,
                                supplierRec.NameL);
                    HttpContext.Current.Session["ResData"] = ResData;
                }
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getChgFee(string RecID, string Flight, string SClass)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        resService.Service = Flight;
        resService.FlgClass = SClass;
        ResChgFeePrice chgPrice = new Reservation().whatHasChangedResService(UserData, ResData, recID, resService, ref errorMsg);
        if (chgPrice == null)
            return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        else
        {
            string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                             chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
            return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
        }
    }
}
