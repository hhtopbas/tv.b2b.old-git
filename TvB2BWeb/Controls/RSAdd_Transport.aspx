﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSAdd_Transport.aspx.cs"
    Inherits="Controls_RSAdd_Transport" EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "RSAddTransport") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/RSAddStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script type="text/javascript">

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function CheckInDateChange(_TextBox, _PopCal) {
            var hfDate = document.getElementById("<%= hfDate.ClientID %>");
            if ((!_TextBox) || (!_PopCal)) return
            var _format = _TextBox.getAttribute("Format");
            var _Date = _PopCal.getDate(_TextBox.value, _format);
            if (_Date) {
                hfDate.value = Date.parse(_Date);
            }
            getDeparture();
        }

        function getTransportDays() {
            var _data = '{"DepCity":"' + $("#sDepLocation").val() + '","ArrCity":"' + $("#sArrLocation").val() + '"}';
            $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/RSAdd_Transport.aspx/getTransportDays",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#ppcPickupDate").attr('disabled');
                    var objPopCalId = PopCalGetCalendarIndex('ppcPickupDate');
                    objPopCal = objPopCalList[objPopCalId];
                    objPopCal.Holidays = [];
                    objPopCal.HolidaysCounter = 0;
                    if (msg.hasOwnProperty('d') && msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            PopCalAddSpecialDay(this.Day, this.Month, this.Year, '', 0);
                        });
                    }
                    $("#ppcPickupDate").removeAttr('disabled');
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getDeparture() {
            var objPopCalId = PopCalGetCalendarIndex('ppcPickupDate');
            objPopCal = objPopCalList[objPopCalId];
            var _TextBox = $("#iPickupDate");
            var _format = _TextBox.attr("Format");
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Transport.aspx/getDeparture",
                data: '{"selectedDate":"' + _TextBox.val() + '","dateFormat":"' + _format + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sDepLocation").html("");
                    $("#sArrLocation").html("");
                    $("#sPickup").html("");
                    $("#sBus").html("");
                    $("#sService").html("");
                    $("#sDepLocation").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sDepLocation").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getArrival() {
            var objPopCalId = PopCalGetCalendarIndex('ppcPickupDate');
            objPopCal = objPopCalList[objPopCalId];
            var _TextBox = $("#iPickupDate");
            var _format = _TextBox.attr("Format");
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Transport.aspx/getArrival",
                data: '{"Departure":"' + $("#sDepLocation").val() + '","selectedDate":"' + _TextBox.val() + '","dateFormat":"' + _format + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sArrLocation").html("");
                    $("#sPickup").html("");
                    $("#sBus").html("");
                    $("#sService").html("");
                    $("#sArrLocation").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sArrLocation").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getService() {
            var objPopCalId = PopCalGetCalendarIndex('ppcPickupDate');
            objPopCal = objPopCalList[objPopCalId];
            var _TextBox = $("#iPickupDate");
            var _format = _TextBox.attr("Format");
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Transport.aspx/getService",
                data: '{"Departure":"' + $("#sDepLocation").val() + '","Arrival":"' + $("#sArrLocation").val() + '","selectedDate":"' + _TextBox.val() + '","dateFormat":"' + _format + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sPickup").html("");
                    $("#sBus").html("");
                    $("#sService").html("");
                    $("#sService").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sService").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getBus() {
            var objPopCalId = PopCalGetCalendarIndex('ppcPickupDate');
            objPopCal = objPopCalList[objPopCalId];
            var _TextBox = $("#iPickupDate");
            var _format = _TextBox.attr("Format");
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Transport.aspx/getBus",
                data: '{"Departure":"' + $("#sDepLocation").val() + '","Arrival":"' + $("#sArrLocation").val() + '","selectedDate":"' + _TextBox.val() + '","dateFormat":"' + _format + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sBus").html("");
                    $("#sBus").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sBus").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getPickups() {

            var objPopCalId = PopCalGetCalendarIndex('ppcPickupDate');
            objPopCal = objPopCalList[objPopCalId];
            var _TextBox = $("#iPickupDate");
            var _format = _TextBox.attr("Format");

            var params = new Object();
            params.Service = $("#sService").val();
            params.Location = $("#sDepLocation").val();
            params.selectedDate = _TextBox.val();
            params.dateFormat = _format;

            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Transport.aspx/getPickups",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sPickup").html("");
                    $("#sPickup").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sPickup").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function changeDeparture() {
            getArrival();
            getTransportDays();
        }

        function changeArrival() {
            getService();
        }

        function changeService() {
            getBus();
            getPickups();
        }

        function createTourist() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Transport.aspx/getTourist",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#box1View").html("");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#box1View").append("<option value='" + this.CustNo + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });

            $.configureBoxes();
        }

        function preCalc(save) {
            if ($("#sDepLocation").val() == null || $("#sDepLocation").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectDeparture") %>');
                return;
            }
            if ($("#sArrLocation").val() == null || $("#sArrLocation").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectArrival") %>');
                return;
            }
            if ($("#sService").val() == null || $("#sService").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTransport") %>');
                return;
            }

            var selectCust = '';
            $("#box1View option").each(function() {
                if (selectCust.length > 0) selectCust += "|";
                selectCust += $(this).val();
            });
            if (selectCust.length < 1) {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>');
                return;
            }
            var begDate = $("#iPickupDate").val();
            var _begDateFormat = $("#iPickupDate").attr("Format");
            if (begDate == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseCorrectDate") %>');
                return;
            }

            var _data = '{"selectedCusts":"' + selectCust + '"}';
            $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/RSAdd_Transport.aspx/PaxControl",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var paxs = $.json.decode(msg.d);
                        if (paxs.Adult > 0) {
                            $("#iAdult").text(paxs.Adult.toString());
                            $("#iChild").text(paxs.Child.toString());
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });

            var _data = '';
            var _url = "";
            if (save) {
                _url = "../Controls/RSAdd_Transport.aspx/SaveService";
                _data = '{"selectedCusts":"' + selectCust + '"' +
                         ',"Departure":"' + $("#sDepLocation").val() + '"' +
                         ',"Arrival":"' + $("#sArrLocation").val() + '"' +
                         ',"Pickup":"' + $("#sPickup").val() + '"' +
                         ',"Service":"' + $("#sService").val() + '"' +
                         ',"Bus":"' + $("#sBus").val() + '"' +
                         ',"Date":"' + begDate + '"' +
                         ',"DateFormat":"' + _begDateFormat + '"' +
                         ',"recordType":"' + $("#recordType").val() + '"' + '}';
            }
            else {
                _url = "../Controls/RSAdd_Transport.aspx/CalcService";
                _data = '{"selectedCusts":"' + selectCust + '"' +
                         ',"Departure":"' + $("#sDepLocation").val() + '"' +
                         ',"Arrival":"' + $("#sArrLocation").val() + '"' +
                         ',"Pickup":"' + $("#sPickup").val() + '"' +
                         ',"Service":"' + $("#sService").val() + '"' +
                         ',"Bus":"' + $("#sBus").val() + '"' +
                         ',"Date":"' + begDate + '"' +
                         ',"DateFormat":"' + _begDateFormat + '"' + '}';
            }
            $.ajax({
                type: "POST",
                url: _url,
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    var result = $.json.decode(msg.d);
                    if (save) {
                        window.close;
                        self.parent.returnAddResServices(true, result.Supplier);
                    }
                    else {
                        if (result.Price != '')
                            $("#iSalePrice").text(result.Price);
                        else showMessage(result.Supplier);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            if (source == 'save') {
                preCalc(true);
            }
            else
                if (source == 'cancel') {
                window.close;
                self.parent.returnAddResServices(false, '');
            }
        }

        function getFormData() {
            var retVal = null;
            $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/RSAdd_Transport.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        retVal = msg.d;
                        return retVal;
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
            return retVal;
        }

        $(document).ready(function() {
            $.query = $.query.load(location.href);
            $("#recordType").val($.query.get('recordType'));
            var data = getFormData();
            //            $.datepicker.setDefaults($.datepicker.regional[data.DateLang != 'en' ? defaultData.Filter.DateLang : '']);
            //            $("#iTransDate").datepicker({
            //                showOn: "button",
            //                buttonImage: "../Images/Calendar.gif",
            //                buttonImageOnly: true,
            //                onSelect: function(dateText, inst) {
            //                    if (dateText != '') {
            //                        var date1 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));

            //                    }
            //                }
            //                //                setDate: data.CurrentDate,
            //                //                minDate: new Date(data.FromDate.replace('/Date()','').replace(')/','')), 
            //                //                maxDate: new Date(data.ToDate.replace('/Date()','').replace(')/',''))
            //            });
            //            $('#iTransDate').datepicker("disable"); //sonst bekommt er den focus und der picker erscheint
            //            $('#iTransDate').datepicker({
            //                setDate: new Date(data.CurrentDate.replace('/Date(', '').replace(')/', '')),
            //                minDate: new Date(data.CurrentDate.replace('/Date(', '').replace(')/', ''))
            //            }); // vorsicht januar ist 0 
            //            $('#iTransDate').datepicker("enable");
            getDeparture();
            getTransportDays();
            createTourist();
        });           
                
    </script>

</head>
<body>
    <form id="formRsHotel" runat="server">
    <br />
    <input id="recordType" type="hidden" value="" />
    <div id="divRs">
        <div>
            <table>
                <tr>
                    <td class="leftCell">
                        <div id="divPickupDate" class="divs">
                            <div class="LeftDiv">
                              <span class="compulsoryField">* </span>
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewPickupDate") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <%--<input id="iTransDate" type="text" />
                                <br />--%>
                                <asp:TextBox ID="iPickupDate" runat="server" Width="100px" />
                                <rjs:PopCalendar ID="ppcPickupDate" runat="server" Control="iPickupDate" ClientScriptOnDateChanged="CheckInDateChange" />
                            </div>
                        </div>
                        <div id="divDepLocation" class="divs">
                            <div class="LeftDiv">
                              <span class="compulsoryField">* </span>
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewDeparture") %>
                                    :</span>
                            </div>
                            <div id="divSDepLocation" class="inputDiv">
                                <select id="sDepLocation" class="combo" onchange="changeDeparture()">
                                </select>
                            </div>
                        </div>
                        <div id="divArrLocation" class="divs">
                            <div class="LeftDiv">
                              <span class="compulsoryField">* </span>
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewArrival") %>
                                    :</span>
                            </div>
                            <div id="divSArrLocation" class="inputDiv">
                                <select id="sArrLocation" class="combo" onchange="changeArrival()">
                                </select>
                            </div>
                        </div>
                        <div id="divService" class="divs">
                            <div class="LeftDiv">
                              <span class="compulsoryField">* </span>
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewTransport") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sService" class="combo" onchange="changeService()">
                                </select>
                            </div>
                        </div>
                        <div id="divPickupPoint" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewPickupPoint") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sPickup" class="combo">
                                </select>
                            </div>
                        </div>
                        <div id="divBus" class="divs">
                            <div class="LeftDiv">
                              <span class="compulsoryField">* </span>
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewBus") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sBus" class="combo">
                                </select>
                            </div>
                        </div>
                        <div id="divAdultChild" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                    :</span>
                            </div>
                            <div class="divAdultChild">
                                <b><span id="iAdult"></span></b>
                            </div>
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                    :</span>
                            </div>
                            <div class="divAdultChild">
                                <b><span id="iChild"></span></b>
                            </div>
                        </div>
                        <%--<div id="divSupplier" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplier") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplier"></span></b>
                            </div>
                        </div>
                        <div id="divSupplierNote" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplierNote"></span></b>
                            </div>
                        </div>--%>
                    </td>
                    <td class="rightCell">
                        <br />
                        <div id="div1">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewSalePrice") %>
                                :</span> <span id="iSalePrice" class="salePrice"></span>
                        </div>
                        <br />
                        <div id="div2">
                            <input id="Button1" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                                onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                style="width: 150px;" />
                        </div>
                        <asp:HiddenField ID="hfDate" runat="server" />
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="2" cellspacing="0">
                <tr>
                    <td align="center">
                        <b>
                            <%= GetGlobalResourceObject("Controls", "lblServiceTourist")%></b>
                    </td>
                    <td>
                    </td>
                    <td align="center">
                        <b>
                            <%= GetGlobalResourceObject("Controls", "lblOtherTourist")%></b>
                    </td>
                </tr>
                <tr>
                    <td style="width: 290px;" align="left">
                        <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box1Filter" />
                        <button type="button" id="box1Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            X</button><br />
                        <select id="box1View" multiple="multiple" style="width: 100%; height: 125px;">
                        </select><br />
                        <%--<span id="box1Counter" class="countLabel"></span>--%>
                        <select id="box1Storage">
                        </select>
                    </td>
                    <td style="width: 40px;" align="center">
                        <button id="to2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            >
                        </button>
                        <br />
                        <button id="allTo2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            >>
                        </button>
                        <br />
                        <button id="allTo1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            <<
                        </button>
                        <br />
                        <button id="to1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            <
                        </button>
                    </td>
                    <td style="width: 290px;" align="left">
                        <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box2Filter" />
                        <button type="button" id="box2Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            X</button><br />
                        <select id="box2View" multiple="multiple" style="width: 100%; height: 125px;">
                        </select><br />
                        <%--<span id="box2Counter" class="countLabel"></span>--%>
                        <select id="box2Storage">
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <table id="divBtn">
            <tr>
                <td style="width: 50%;" align="center">
                    <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
                        onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
                <td align="center">
                    <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                        onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
            </tr>
        </table>
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
            </span><span id="messages">Message</span>
        </p>
    </div>
    </form>
</body>
</html>
