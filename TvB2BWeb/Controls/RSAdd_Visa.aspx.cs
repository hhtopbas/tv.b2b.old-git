﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSAdd_Visa : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ppcCheckIn.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckIn.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcCheckIn.From.Date = DateTime.Today;
        ppcCheckOut.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckOut.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcCheckOut.From.Date = DateTime.Today;

        if (!IsPostBack)
        {
           // sCountry.Attributes.Add("onchange", "changeCountry();");            

            ppcCheckIn.DateValue = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value: DateTime.Today;
            ppcCheckOut.DateValue = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
            //iNight.Text = (ppcCheckOut.DateValue - ppcCheckIn.DateValue).Days.ToString();
        }
    }

    [WebMethod]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        DateTime ppcCheckIn = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
        DateTime ppcCheckOut = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
        string iNight = (ppcCheckOut - ppcCheckIn).Days.ToString();
        return iNight;
    }

    [WebMethod]
    public static string getCountryList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];        
        string errorMsg = string.Empty;
        List<Location> visaCountry = new Visas().getVisaLocations(UserData.Market, ResData.ResMain.PLMarket, ref errorMsg);
        string retVal = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            Int32? arrCountry = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.HasValue ? ResData.ResMain.ArrCity.Value : -1, ref errorMsg);
            var query = from q in visaCountry
                        where q.Country == arrCountry
                        group q by new { RecID = q.RecID, Name = q.NameL } into k
                        select new { RecID = k.Key.RecID, Name = k.Key.Name };
            retVal = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        else
        {
            var query = from q in visaCountry
                        group q by new { RecID = q.RecID, Name = q.NameL } into k
                        select new { RecID = k.Key.RecID, Name = k.Key.Name };
            retVal = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        return retVal;
    }
   
    [WebMethod]
    public static string getVisa(string Country)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? country = Conversion.getInt32OrNull(Country);        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        List<VisaRecord> visaList = new Visas().getVisaCountry(UserData.Market, ResData.ResMain.PLMarket, country, ResData.ResMain.BegDate, ResData.ResMain.EndDate, ref errorMsg);
        var query = from q in visaList
                    where !(q.RestSingleSale.HasValue && q.RestSingleSale.Value && ResData.ResService.Count == 0)
                    select new { Code = q.Code, Name = q.LocalName };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }
    
    [WebMethod]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            var query = from s in ResData.ResCust
                        where s.Status == 0 && s.HasPassport.HasValue && s.HasPassport.Value
                        select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
            retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        else
        {
            var query = from s in ResData.ResCust
                        where s.Status == 0 //&& (!s.HasPassport.HasValue || s.HasPassport.Value)
                        select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
            retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        return retval;
    }

    [WebMethod]
    public static string CalcService(string selectedCusts, string Country, string Service, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        int? country = Conversion.getInt32OrNull(Country);                
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] selectedCust = selectedCusts.Split('|');        
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);
        if (!begDate.HasValue && !endDate.HasValue) return retVal;

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)        
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;        
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "VISA", Service, string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, country, country, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
            DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "VISA", ServiceID, ref errorMsg);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", returnData.Rows[0]["SalePrice"].ToString() + " " + returnData.Rows[0]["SaleCur"].ToString(), supplierRec.NameL);
        }
        return "{" + retVal + "}";
    }

    [WebMethod]
    public static string SaveService(string selectedCusts, string Country, string Service, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat, string recordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        int? country = Conversion.getInt32OrNull(Country);                
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string[] selectedCust = selectedCusts.Split('|');        
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);
        if (!begDate.HasValue && !endDate.HasValue) return retVal;

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "VISA", Service,
            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, country, country, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            if (_recordType == true)
            {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                {
                    List<ResServiceRecord> resServiceList = ResData.ResService;
                    ResServiceRecord resService = resServiceList.LastOrDefault();
                    resService.ExcludeService = false;
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
                else
                {
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
            }
            else
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "<br />";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "\n";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
            }
        }
        return "{" + retVal + "}";
    }

    [WebMethod]
    public static string PaxControl(string selectedCusts)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();            
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;            
        }        
        retVal = string.Format("\"Adult\":{0},\"Child\":{1}", AdlCnt.ToString(), ChdCnt.ToString());
        return "{" + retVal + "}";
    }

}
