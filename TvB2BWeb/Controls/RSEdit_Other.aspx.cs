﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSEdit_Other : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {
        }
    }

    [WebMethod(EnableSession=true)]
    public static string getServiceDetail(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        Int32? recID = Conversion.getInt32OrNull(RecID);
        if (!recID.HasValue) return string.Empty;
        var query = from q in ResData.ResService
                    where q.RecID == recID.Value
                    select new
                    {
                        BegDate = q.BegDate.Value.ToShortDateString(),
                        EndDate = q.EndDate.Value.ToShortDateString(),
                        DepLocation = q.DepLocationNameL,
                        ArrLocation = q.ArrLocationNameL,
                        Service = q.Service + "|" + q.ServiceType,
                        Adult = q.Adult,
                        Child = q.Child,
                        Unit = q.Unit,
                        Duration = q.Duration,
                        Supplier = q.SupplierName,
                        SupNote = q.SupNote,
                        StatConf = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ? q.StatConfNameL : string.Empty,
                        StatSer = q.StatSerNameL,
                        SalePrice = !Equals(q.IncPack, "Y") ? q.SalePrice.Value.ToString("#,###.00") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        SaleCur = !Equals(q.IncPack, "Y") ? q.SaleCur : "",
                        ResPrice = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value.ToString("#.00") + " " + ResData.ResMain.SaleCur : ""
                    };

        if (query == null || query.Count() < 1) return string.Empty;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query.FirstOrDefault());
    }

    [WebMethod(EnableSession = true)]
    public static string getAdService(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? recID = Conversion.getInt32OrNull(RecID);        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        string errorMsg = string.Empty;
        List<AdServiceRecord> adServiceList = new AdServices().getAdServiceLocations(UserData.Market, ResData.ResMain.PLMarket, ResData.ResMain.BegDate, ResData.ResMain.EndDate, resService.DepLocation, resService.ArrLocation, ref errorMsg);
        var query = from q in adServiceList
                    select new { Code = q.Code + "|" + q.Service, Name = q.LocalName };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod(EnableSession = true)]
    public static string CalcService(string RecID, string AdService, bool? newRes)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        int? recID = Conversion.getInt32OrNull(RecID);        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        string serviceType = string.Empty;
        string service = string.Empty;
        if (AdService.Length > 0)
        {
            serviceType = AdService.Split('|')[1];
            service = AdService.Split('|')[0];
        }

        resService.PriceSource = !Equals(resService.Service, service) || Equals(resService.ServiceType, serviceType) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.ServiceType = serviceType;
        resService.Service = service;
        resService.Supplier = string.Empty;

        ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
        if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"NewPrice\":\"{2}\"", string.Empty, errorMsg, string.Empty);
        }
        else
        {
            resService = ResData.ResService.Find(f => f.RecID == recID.Value);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
            string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"NewPrice\":\"{2}\"",
                        SalePriceStr,
                        supplierRec.NameL,
                        ResData.ResMain.SalePrice.HasValue ? (ResData.ResMain.SalePrice.Value.ToString("#.00") + " " + ResData.ResMain.SaleCur) : "");
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string SaveService(string RecID, string AdService, bool? newRes)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string serviceType = string.Empty;
        string service = string.Empty;
        if (AdService.Length > 0)
        {
            serviceType = AdService.Split('|')[1];
            service = AdService.Split('|')[0];
        }
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID.Value);
        TvBo.AdServiceRecord otherService = new AdServices().getAdService(UserData.Market, serviceType, service, ref errorMsg);
        resService.PriceSource = !Equals(resService.Service, service) || Equals(resService.ServiceType, serviceType) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.ServiceType = serviceType;
        resService.Service = service;
        resService.ServiceName = otherService.Name;
        resService.ServiceNameL = otherService.LocalName;
        
        resService.Supplier = string.Empty;

        if (newRes.HasValue && newRes.Value)
        {
            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
            if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"NewPrice\":\"{2}\"", string.Empty, errorMsg, string.Empty);
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"NewPrice\":\"{2}\"",
                            SalePriceStr,
                            supplierRec.NameL,
                            string.Empty);
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        else
        {
            Int32 ErrCode = 0;
            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
            List<int?> recIDList = new List<int?>();
            recIDList.Add(resService.RecID);
            ResData = new TvBo.Reservation().editService(UserData, ResData, recIDList, ref ErrCode, ref errorMsg, false);
            if (ErrCode != 0)
            {
                if (ErrCode < 0)
                    errorMsg = string.IsNullOrEmpty(errorMsg) ? "Bilinmeyen hata" : errorMsg;
                else errorMsg = new ResCalcError().calcError(ErrCode, ResData).ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"NewPrice\":\"{2}\"", string.Empty, errorMsg, string.Empty);
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"NewPrice\":\"{2}\"",
                            SalePriceStr,
                            supplierRec.NameL,
                            string.Empty);
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getChgFee(string RecID, string AdService)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        resService.Service = AdService;        
        ResChgFeePrice chgPrice = new Reservation().whatHasChangedResService(UserData, ResData, recID, resService, ref errorMsg);
        if (chgPrice == null)
            return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        else
        {
            string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                             chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
            return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
        }
    }
}
