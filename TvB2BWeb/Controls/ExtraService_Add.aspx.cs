﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using TvBo;
using System.Text;
using System.Threading;
using TvTools;
using System.Web.Script.Services;

public partial class Controls_ExtraService_Add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string getExtraServices()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool localName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<TvBo.ExtraServiceListRecord> extras = new Reservation().getExtraServiceList(UserData, ResData, null, false, ref errorMsg);
        var extSerGroup = from q in extras
                          group q by new { grp = q.MainService, grpName = localName ? q.MainServiceNameL : q.MainServiceName } into k
                          select new { grp = k.Key.grp, grpName = k.Key.grpName };
        StringBuilder sb = new StringBuilder();
        if (extSerGroup != null && extSerGroup.Count() > 0)
        {
            sb.Append("<table id=\"mainServiceExtraService\" cellpadding=\"2\" cellspacing=\"1\" width=\"100%\">");
            int i = 0;
            foreach (var q in extSerGroup)
            {
                List<TvBo.ExtraServiceListRecord> extrasGrp = extras.Where(w => w.MainService == q.grp /*&& w.SaleAdlPrice.HasValue && w.SaleAdlPrice.Value > 0*/).Select(s => s).ToList<ExtraServiceListRecord>();
                List<ResCustRecord> ServiceCustomers = (from q1 in ResData.ResCust
                                                        join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                                                        where q2.ServiceID == q.grp
                                                        select q1).ToList<ResCustRecord>();
                string serviceCusts = string.Empty;
                foreach (ResCustRecord r1 in ServiceCustomers)
                {
                    if (serviceCusts.Length > 0) serviceCusts += ", ";
                    serviceCusts += string.Format("{0}. {1} {2}", r1.TitleStr, r1.Surname, r1.Name);
                }
                sb.AppendFormat("<tr><td><b>{0}</b><br /><span style=\"font-size: 7pt;\">({1})</span><br />",
                    q.grpName,
                    serviceCusts);

                sb.Append("<table class=\"MainServiceExtra\">");
                sb.Append("<tr class=\"header\">");
                sb.AppendFormat("<td align=\"center\">&nbsp;</td><td align=\"center\"><b>{0}</b></td><td>{4}</td><td align=\"center\"><b>{1}</b></td><td align=\"center\"><b>{2}</b></td><td align=\"center\"><b>{3}</b></td></tr>",
                                    HttpContext.GetGlobalResourceObject("Controls", "lblExtSrvDescription"),
                                    HttpContext.GetGlobalResourceObject("Controls", "lblExtSrvPrice"),
                                    HttpContext.GetGlobalResourceObject("Controls", "lblExtSrvCalcType"),
                                    HttpContext.GetGlobalResourceObject("Controls", "lblExtSrvPriceType"),
                                    "&nbsp;");
                foreach (ExtraServiceListRecord row in extrasGrp.OrderBy(o => localName ? o.DescriptionL : o.Description))
                {
                    ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == row.MainService);
                    i++;
                    sb.Append("<tr>");
                    string addExtBtn = string.Format("<span id=\"addExtraServiceBtn{4}\" onclick=\"addExtraService{5}({0}, {1}, {2});\">{3}</span>",
                                                row.MainService,
                                                row.RecID,
                                                row.PriceType,
                                                HttpContext.GetGlobalResourceObject("Controls", "lblExtSrvAddExtra"),
                                                i,
                                                string.Equals(UserData.CustomRegID, Common.crID_RoyalPersia) ? "ForSrvNote" : "");
                    string date = "<select id=\"durationCombo_{1}_{2}\">{0}</select>";
                    if (row.CalcType != 0)
                        date = string.Empty;
                    else
                    {
                        if ((resService.Duration.HasValue && resService.Duration.Value > 1) && (string.IsNullOrEmpty(row.ChkType) || string.Equals(row.ChkType, "A")))
                        {
                            string option = string.Empty;
                            DateTime _date = resService.BegDate.Value;
                            for (int duration = 0; duration <= resService.Duration.Value; duration++)
                            {
                                option += string.Format("<option value=\"{0}\">{1}</option>", duration, _date.AddDays(duration).ToString("dd MMM yyyy"));
                            }
                            date = string.Format(date, option, resService.RecID, row.RecID);
                            date += string.Format("<input id=\"addExtraServiceDayValue{0}\" type=\"hidden\" value=\"{1}\" />",
                                    i, "0");
                        }
                        else
                        {
                            if (string.Equals(row.ChkType, "C"))
                            {
                                string option = string.Empty;
                                DateTime _date = resService.BegDate.Value;
                                int duration = 0;
                                option += string.Format("<option value=\"{0}\">{1}</option>", duration, _date.AddDays(duration).ToString("dd MMM yyyy"));
                                date = string.Format(date, option, resService.RecID, row.RecID);
                                date += string.Format("<input id=\"addExtraServiceDayValue{0}\" type=\"hidden\" value=\"{1}\" />",
                                        i, "0");
                            }
                            else
                                if (string.Equals(row.ChkType, "O"))
                                {
                                    string option = string.Empty;
                                    DateTime _date = resService.BegDate.Value;
                                    int duration = resService.Duration.Value;
                                    option += string.Format("<option value=\"{0}\">{1}</option>", duration, _date.AddDays(duration).ToString("dd MMM yyyy"));
                                    date = string.Format(date, option, resService.RecID, row.RecID);
                                    date += string.Format("<input id=\"addExtraServiceDayValue{0}\" type=\"hidden\" value=\"{1}\" />",
                                            i, "0");
                                }
                                else
                                    date = string.Empty;
                        }
                    }
                    sb.AppendFormat("<td class=\"addbtn\">{0}</td>", addExtBtn);
                    sb.AppendFormat("<td class=\"desc\">{0}</td>", localName ? row.DescriptionL : row.Description);
                    sb.AppendFormat("<td class=\"date\">{0}</td>", date);
                    sb.AppendFormat("<td class=\"price\">{0}</td>", row.SaleAdlPrice.HasValue ? row.SaleAdlPrice.Value.ToString("#,###.00") + " " + row.SaleCur : "&nbsp;");
                    sb.AppendFormat("<td class=\"pricetype\">{0}</td>",
                            HttpContext.GetGlobalResourceObject("LibraryResource", "CalcType" + row.CalcType));
                    sb.AppendFormat("<td class=\"calctype\">{0}</td>",
                            HttpContext.GetGlobalResourceObject("LibraryResource", "PriceType" + row.PriceType));
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                sb.Append("</td></tr>");
            }
            sb.Append("</table>");
        }
        else
            sb.Append(HttpContext.GetGlobalResourceObject("Controls", "msgThereIsNoMatchingRecords"));
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string getTourist(string serviceID, string ExtraServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        int? _serviceID = Conversion.getInt32OrNull(serviceID);
        int? _extServiceID = Conversion.getInt32OrNull(ExtraServiceID);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<TvBo.ExtraServiceListRecord> extras = new Reservation().getExtraServiceList(UserData, ResData, _extServiceID, false, ref errorMsg);

        TvBo.ExtraServiceListRecord extraSrv = extras.Find(f => f.RecID == _extServiceID);
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    join c in ResData.ResCon on s.CustNo equals c.CustNo
                    where c.ServiceID == _serviceID && s.Status == 0
                    group s by new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name } into k
                    select new { CustNo = k.Key.CustNo, Name = k.Key.Name };
        var retValJSon = new { LockCustList = extraSrv != null && extraSrv.TakeAllUser.HasValue && extraSrv.TakeAllUser.Value == true ? true : false, CustList = query };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(retValJSon);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string calcExtraService(string SelectedCusts, string MainServiceID, string ExtServiceID, string recordType, int? dayDuration, string ExtServiceNote)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;

        bool _recordType = Equals(recordType, "temp");

        int? mainServiceID = Conversion.getInt32OrNull(MainServiceID);
        int? extServiceID = Conversion.getInt32OrNull(ExtServiceID);

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        string[] selectedCust = SelectedCusts.Split('|');
        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        if (SelectedCusts.Length > 0)
        {
            foreach (string s in selectedCust)
                SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        }
        else
        {
            var mainServiceCusts = from q1 in ResData.ResCon
                                   join q2 in SelectCust on q1.CustNo equals q2.CustNo
                                   select new { custNo = q1.CustNo };
            foreach (var s in mainServiceCusts)
                SelectCust.Find(f => f.CustNo == s.custNo).Selected = true;
        }

        ResData = new Reservation().setExtraService(UserData, ResData, SelectCust, mainServiceID, extServiceID, dayDuration, dayDuration.HasValue ? true : false, ExtServiceNote, false, ref errorMsg);
        if (string.IsNullOrEmpty(errorMsg))
        {
            if (_recordType == true)
            {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                    HttpContext.Current.Session["ResData"] = ResData;

                return errorMsg;

            }
            else
            {
                List<TvBo.ReservastionSaveErrorRecord> returnData = new Reservation().UpdateReservation(UserData, ref ResData);

                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    return errorMsg;
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "\n";
                    }
                    return Msg;
                }
            }
        }
        else return errorMsg;
    }
}
