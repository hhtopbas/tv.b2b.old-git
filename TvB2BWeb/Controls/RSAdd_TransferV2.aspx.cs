﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Threading;
using System.Web.Services;
using System.Data;
using TvTools;
using System.Web.Script.Services;

public partial class Controls_RSAdd_TransferV2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        ppcCheckIn.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckIn.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) && (ResData.ResService.Where(w => w.ServiceType == "EXCURSION").Count() == ResData.ResService.Count()))
            ppcCheckIn.From.Date = ResData.ResMain.BegDate.HasValue ? (ResData.ResMain.BegDate.Value.AddMonths(-1) > DateTime.Today ? DateTime.Today : ResData.ResMain.BegDate.Value.AddMonths(-1)) : DateTime.Today;
        else
            ppcCheckIn.From.Date = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
        ppcCheckInR.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckInR.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) && (ResData.ResService.Where(w => w.ServiceType == "EXCURSION").Count() == ResData.ResService.Count()))
            ppcCheckInR.From.Date = ResData.ResMain.BegDate.HasValue ? (ResData.ResMain.BegDate.Value.AddMonths(-1) > DateTime.Today ? DateTime.Today : ResData.ResMain.BegDate.Value.AddMonths(-1)) : DateTime.Today;
        else
            ppcCheckIn.From.Date = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
        if (!IsPostBack) {
            ResServiceRecord hotelService = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).OrderBy(o => o.BegDate).FirstOrDefault();
            ppcCheckIn.DateValue = hotelService != null ? hotelService.BegDate.Value : (ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today);
            ppcCheckInR.DateValue = hotelService != null ? hotelService.EndDate.Value : (ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today);
            Int64 days = (ppcCheckIn.DateValue - new DateTime(1970, 1, 1)).Days - 1;
            Int64 daysR = (ppcCheckInR.DateValue - new DateTime(1970, 1, 1)).Days - 1;
            iDate.Value = (days * 86400000).ToString();
            iDateR.Value = (daysR * 86400000).ToString();            
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        string hotel = string.Empty;
        int? hotelTrfLocation = null;
        int? airportTrfLocation = null;
        if (ResData.ResService.Where(w => w.ServiceType == "HOTEL").Count() > 0) {
            hotel = ResData.ResService.Where(w => w.ServiceType == "HOTEL").FirstOrDefault().Service;
            hotelTrfLocation = new Hotels().getHotelTrfLocation(UserData, hotel, ref errorMsg);
            if (ResData.ResService.Where(w => w.ServiceType == "FLIGHT").Count() > 0) {
                airportTrfLocation = ResData.ResService.Where(w => w.ServiceType == "FLIGHT").OrderBy(o => o.BegDate).First().ArrLocation;
            } else {
                List<AirportRecord> airPorts = new Hotels().getHotelAirports(UserData, hotel, ref errorMsg);
                if (airPorts != null && airPorts.Count > 0) {
                    airportTrfLocation = airPorts.First().TrfLocation.HasValue ? airPorts.First().TrfLocation.Value : airPorts.First().Location;
                }
            }
        }

        #region Default Type
        string defaultType = string.Empty;
        if (Equals(UserData.CustomRegID, Common.crID_Qasswa) || Equals(UserData.CustomRegID, Common.crID_CelexTravel) || Equals(UserData.CustomRegID, Common.crID_Calypso))
            defaultType = "";
        else if (Equals(UserData.CustomRegID, TvBo.Common.crID_FitTurizm))
            defaultType = "OH";
        else
            defaultType = "RT";
        #endregion

        #region
        List<TrfTypeListRecord> transferTypeParam = new List<TrfTypeListRecord>();
        List<TransferListParam> transferParameters = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TransferListParam>>(Conversion.getStrOrNull(new Common().getFormConfigValue("General", "transferTypes")));
        if (transferParameters != null && transferParameters.Count > 0) {
            TransferListParam transferParams = transferParameters.Find(f => f.Market == UserData.Market);
            if (transferParams != null) {
                foreach (TrfTypeListRecord row in transferParams.TrfTypeList)
                    row.Desc = HttpContext.GetGlobalResourceObject("Controls", row.Desc).ToString();
                transferTypeParam = transferParams.TrfTypeList;
            }
        }
        #endregion

        List<VehicleCategoryRecord> vehicleCatList = new ReservationRequestForm().getVehicleCategory(UserData, ref errorMsg);
        return new {
            hotel = hotel,
            DefaultType = defaultType,
            TransferTypeList = transferTypeParam,
            TransferExt = string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) ||
                          string.Equals(UserData.CustomRegID, TvBo.Common.crID_Elsenal) ||
                          string.Equals(UserData.CustomRegID, TvBo.Common.crID_YekTravel) ||
                          string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel) ||
                          string.Equals(UserData.CustomRegID, TvBo.Common.crID_Calypso) ||
                          string.Equals(UserData.CustomRegID, TvBo.Common.crID_MbnTour_Ir) ||
                          //string.Equals(UserData.CustomRegID, TvBo.Common.crID_MbnTour_Tr) ||
                          string.Equals(UserData.CustomRegID, TvBo.Common.crID_Qasswa) ||
                          string.Equals(UserData.CustomRegID, TvBo.Common.crID_SeaTravel) ||
                          string.Equals(UserData.CustomRegID, TvBo.Common.crID_ZemExpert),
            HotelTrfLocation = hotelTrfLocation,
            AirportLocation = airportTrfLocation,
            showVehicleCat = string.Equals(UserData.CustomRegID, Common.crID_Qasswa),
            vehicleCatList = vehicleCatList
        };
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    where s.Status == 0
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getTransferRouteFrom(string trfType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        bool onlyTicket = ResData.ResMain.SaleResource.HasValue && (new List<short> { 2, 3, 5 }).Contains(ResData.ResMain.SaleResource.Value);
        string errorMsg = string.Empty;
        string retval = string.Empty;
        List<int?> country = new List<int?>();
        List<int?> city = new List<int?>();
        int? countryDep = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
        if (countryDep.HasValue)
            country.Add(countryDep);
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (ResData.ResService.Where(w => w.ServiceType == "HOTEL").Count() > 0) {
            if (string.Equals(UserData.CustomRegID, Common.crID_CelexTravel)) {
                List<ResServiceRecord> hotelSerList = ResData.ResService.Where(w => w.ServiceType == "HOTEL").ToList<ResServiceRecord>();
                foreach (ResServiceRecord row in hotelSerList) {
                    HotelRecord hotelRec = new Hotels().getHotelDetail(UserData, row.Service, ref errorMsg);
                    if (hotelRec != null) {
                        int? countryArr = new Locations().getLocationForCountry(hotelRec.TrfLocation, ref errorMsg);
                        city.Add(new Locations().getLocationForCity(hotelRec.TrfLocation, ref errorMsg));
                        country.Add(countryArr);
                        List<AirportRecord> hotelAirports = new Hotels().getHotelAirports(UserData, hotelRec.Code, ref errorMsg);
                        if (hotelAirports != null && hotelAirports.Count > 0) {
                            foreach (var r in hotelAirports)
                                city.Add(r.TrfLocation.HasValue ? r.TrfLocation.Value : r.Location);
                        }
                    }
                }
            } else {
                ResServiceRecord hotelSer = ResData.ResService.Where(w => w.ServiceType == "HOTEL").FirstOrDefault();
                HotelRecord hotelRec = new Hotels().getHotelDetail(UserData, hotelSer.Service, ref errorMsg);
                if (hotelRec != null) {
                    int? countryArr = new Locations().getLocationForCountry(hotelRec.TrfLocation, ref errorMsg);
                    city.Add(new Locations().getLocationForCity(hotelRec.TrfLocation, ref errorMsg));
                    country.Add(countryArr);
                    List<AirportRecord> hotelAirports = new Hotels().getHotelAirports(UserData, hotelRec.Code, ref errorMsg);
                    if (hotelAirports != null && hotelAirports.Count > 0) {
                        foreach (var r in hotelAirports)
                            city.Add(r.TrfLocation.HasValue ? r.TrfLocation.Value : r.Location);
                    }
                }
            }
        }
        List<TransferPriceRecord> transferList = new Transfers().getTransferLocations(UserData.Market, ResData.ResMain.PLMarket, country, city, ResData.ResMain.BegDate, ResData.ResMain.EndDate, null, ref errorMsg);
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet))
            transferList = transferList.Where(w => w.TrfFrom.HasValue && w.TrfTo.HasValue).ToList<TransferPriceRecord>();
        switch (trfType) {
            case "AHA":
                #region Airport to Hotel to Airport
                if (string.Equals(ResData.ResMain.PackType, "O")) {
                    var queryH = ResData.ResService.Where(w => w.ServiceType == "HOTEL").ToList();
                    if (queryH.GroupBy(g => g.Service).Count() == 1) {
                        List<AirportRecord> hotelAirports = new Hotels().getHotelAirports(UserData, queryH.FirstOrDefault().Service, ref errorMsg);
                        var hotelAPList = from q in hotelAirports
                                          group q by new { RecID = q.Location, Name = useLocalName ? q.LocationNameL : q.LocationName } into k
                                          select new { RecID = k.Key.RecID, Name = k.Key.Name };
                        retval = Newtonsoft.Json.JsonConvert.SerializeObject(hotelAPList);
                    }
                } else {
                    var queryFlight = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").ToList();
                    var queryH = ResData.ResService.Where(w => w.ServiceType == "HOTEL").ToList();
                    if (queryFlight.Count > 0 && queryH.Count > 0) {
                        if (queryH.GroupBy(g => g.Service).Count() == 1) {
                            HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH.FirstOrDefault().Service, ref errorMsg);
                            if (hotel != null &&
                                transferList.Where(w => w.TrfFrom == queryFlight.FirstOrDefault().ArrLocation).Count() > 0 &&
                                transferList.Where(w => w.TrfTo == queryFlight.LastOrDefault().DepLocation).Count() > 0) {
                                if (transferList.Where(w => w.TrfTo == hotel.TrfLocation).Count() > 0 &&
                                    transferList.Where(w => w.TrfFrom == hotel.TrfLocation).Count() > 0) {
                                    var Q1 = from q in transferList
                                             where q.TrfFrom == queryFlight.FirstOrDefault().ArrLocation && q.TrfTo == hotel.TrfLocation
                                             group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                                             select new { RecID = k.Key.RecID, Name = k.Key.Name };
                                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                                } else {
                                    var Q1 = from q in transferList
                                             where q.TrfFrom == queryFlight.FirstOrDefault().ArrLocation
                                             group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                                             select new { RecID = k.Key.RecID, Name = k.Key.Name };
                                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                                }
                            }
                        }
                    }
                }
                #endregion
                break;
            case "RT":
                #region Airport to Hotel to Airport
                transferList = new Transfers().getTransferLocations(UserData.Market, ResData.ResMain.PLMarket, country, null, ResData.ResMain.BegDate, ResData.ResMain.EndDate, true, ref errorMsg);
                ResServiceRecord flightServiceRT = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").FirstOrDefault();
                int? country_RT = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
                int? city_RT = new Locations().getLocationForCity(ResData.ResMain.ArrCity.Value, ref errorMsg);
                var Q1_RT = from q in transferList //new Locations().getLocationList(UserData.Market, LocationType.None, null, country_RT, city_RT, null, ref errorMsg)
                            where q.Country == country_RT && (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || (q.TrfFrom == city_RT || q.TrfTo == city_RT))
                            group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                            select new { RecID = k.Key.RecID, Name = k.Key.Name };
                retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1_RT);
                #endregion
                break;
            case "AH":
                #region Airport to Hotel
                if (string.Equals(ResData.ResMain.PackType, "O")) {
                    var queryH_AH = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                    if (queryH_AH.Count > 0) {
                        string hotel = queryH_AH.FirstOrDefault().Service;
                        List<AirportRecord> hotelAirports = new Hotels().getHotelAirports(UserData, hotel, ref errorMsg);
                        var hotelAPList = from q in hotelAirports
                                          group q by new { RecID = q.Location, Name = useLocalName ? q.LocationNameL : q.LocationName } into k
                                          select new { RecID = k.Key.RecID, Name = k.Key.Name };
                        retval = Newtonsoft.Json.JsonConvert.SerializeObject(hotelAPList);
                    }
                } else {
                    var queryF_AH = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").ToList();
                    if (queryF_AH.Count > 0) {
                        FlightDayRecord flight = new Flights().getFlightDay(UserData, queryF_AH.FirstOrDefault().Service, queryF_AH.FirstOrDefault().BegDate.Value, ref errorMsg);
                        if (flight != null) {
                            AirportRecord airport = new Flights().getAirport(UserData.Market, flight.ArrAirport, ref errorMsg);
                            if (transferList.Where(w => w.TrfTo == airport.Location).Count() > 0) {
                                var Q1 = from q in transferList
                                         where q.TrfFrom == airport.Location
                                         group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                                         select new { RecID = k.Key.RecID, Name = k.Key.Name };
                                retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                            }
                        }
                    }
                }
                #endregion
                break;
            case "HA":
                #region Hotel to Airport
                var queryH_HA = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                if (queryH_HA.Count > 0) {
                    HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH_HA.FirstOrDefault().Service, ref errorMsg);
                    if (hotel != null) {
                        if (transferList.Where(w => w.TrfTo == hotel.TrfLocation).Count() > 0) {
                            var Q1 = from q in transferList
                                     where q.TrfFrom == hotel.TrfLocation
                                     group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                                     select new { RecID = k.Key.RecID, Name = k.Key.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        } else {
                            var Q1 = from q in new Locations().getLocationList(UserData.Market, LocationType.None, hotel.TrfLocation, null, null, null, ref errorMsg)
                                     select new { RecID = q.RecID, Name = useLocalName ? q.NameL : q.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        }
                    }
                } else
                    if (onlyTicket) {
                    var Q1 = from q in transferList
                             group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                             select new { RecID = k.Key.RecID, Name = k.Key.Name };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                }
                #endregion
                break;
            case "HH":
                #region Hotel to Hotel
                var queryH_HH = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                if (queryH_HH.Count > 1) {
                    HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH_HH.FirstOrDefault().Service, ref errorMsg);
                    if (hotel != null) {
                        if (transferList.Where(w => w.TrfTo == hotel.TrfLocation).Count() > 0) {
                            var Q1 = from q in transferList
                                     where q.TrfFrom == hotel.TrfLocation
                                     group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                                     select new { RecID = k.Key.RecID, Name = k.Key.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        } else {
                            var Q1 = from q in new Locations().getLocationList(UserData.Market, LocationType.None, hotel.TrfLocation, null, null, null, ref errorMsg)
                                     select new { RecID = q.RecID, Name = useLocalName ? q.NameL : q.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        }
                    }
                }
                #endregion
                break;
            case "AA":
                #region Airport to Airport
                var queryF_AA = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").ToList();
                if (queryF_AA.Count > 0) {
                    List<AirportRecord> airportList = new List<AirportRecord>();
                    foreach (var row in queryF_AA) {
                        List<AirportRecord> APListDep = new Flights().getCountryAirportList(UserData.Market, row.DepLocation, ref errorMsg);
                        List<AirportRecord> APListArr = new Flights().getCountryAirportList(UserData.Market, row.ArrLocation, ref errorMsg);
                        airportList.AddRange(APListDep);
                        airportList.AddRange(APListArr);
                    }
                    if (airportList.Count > 0) {
                        var airPL = from q in airportList
                                    group q by new { q.Location } into k
                                    select new { Location = k.Key.Location };
                        var Q1 = from q in transferList
                                 join q2 in airPL on q.TrfFrom equals q2.Location
                                 group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                                 select new { RecID = k.Key.RecID, Name = k.Key.Name };
                        retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                    }
                }
                #endregion
                break;
            case "OH":
                #region Free
                var queryH_OH = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                if (queryH_OH.Count > 0) {
                    HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH_OH.FirstOrDefault().Service, ref errorMsg);
                    if (hotel != null) {
                        int? hotelTrfLocationCity = new Locations().getLocationForCity(hotel.TrfLocation, ref errorMsg);
                        if (transferList.Where(w => w.TrfTo == hotelTrfLocationCity).Count() > 0) {
                            var Q1 = from q in transferList
                                     where q.TrfFrom == hotelTrfLocationCity
                                     group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                                     select new { RecID = k.Key.RecID, Name = k.Key.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        } else {
                            var Q1 = from q in new Locations().getLocationList(UserData.Market, LocationType.None, hotelTrfLocationCity, null, null, null, ref errorMsg)
                                     select new { RecID = q.RecID, Name = useLocalName ? q.NameL : q.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        }
                    }
                }
                #endregion
                break;
            case "HO":
                #region Free
                var queryH_HO = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                if (queryH_HO.Count > 0) {
                    HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH_HO.FirstOrDefault().Service, ref errorMsg);
                    if (hotel != null) {
                        if (transferList.Where(w => w.TrfFrom == hotel.TrfLocation).Count() > 0) {
                            var Q1 = from q in transferList
                                     where q.TrfFrom == hotel.TrfLocation
                                     group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfFromNameL : q.TrfFromName } into k
                                     select new { RecID = k.Key.RecID, Name = k.Key.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        } else {
                            var Q1 = from q in new Locations().getLocationList(UserData.Market, LocationType.None, hotel.TrfLocation, null, null, null, ref errorMsg)
                                     select new { RecID = q.RecID, Name = useLocalName ? q.NameL : q.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        }
                    }
                }
                #endregion
                break;
            default:
                #region Full Free
                var RouteF = from R in transferList
                             group R by new { RecID = R.TrfFrom, Name = useLocalName ? R.TrfFromNameL : R.TrfFromName } into g
                             select new { g.Key.RecID, g.Key.Name };
                retval = Newtonsoft.Json.JsonConvert.SerializeObject(RouteF);
                #endregion
                break;
        }
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getTransferRouteTo(string DepLocation, string trfType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? depLocation = Conversion.getInt32OrNull(DepLocation);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        bool onlyTicket = ResData.ResMain.SaleResource.HasValue && (new List<short> { 2, 3, 5 }).Contains(ResData.ResMain.SaleResource.Value);
        string errorMsg = string.Empty;
        string retval = string.Empty;
        List<int?> country = new List<int?>();
        int? countryDep = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
        if (countryDep.HasValue)
            country.Add(countryDep);
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        if (ResData.ResService.Where(w => w.ServiceType == "HOTEL").Count() > 0) {
            ResServiceRecord hotelSer = ResData.ResService.Where(w => w.ServiceType == "HOTEL").FirstOrDefault();
            int? countryArr = new Locations().getLocationForCountry(hotelSer.DepLocation.Value, ref errorMsg);
            country.Add(countryArr);
        }
        List<TransferPriceRecord> transferList = new Transfers().getTransferLocations(UserData.Market, ResData.ResMain.PLMarket, country, null, ResData.ResMain.BegDate, ResData.ResMain.EndDate, (trfType == "RT"), ref errorMsg);
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet))
            transferList = transferList.Where(w => w.TrfFrom.HasValue && w.TrfTo.HasValue).ToList<TransferPriceRecord>();

        switch (trfType) {
            case "AHA":
                #region Airport to Hotel to Airport
                var queryH_AHA = ResData.ResService.Where(w => w.ServiceType == "HOTEL").ToList();
                if (queryH_AHA.Count > 0) {
                    if (queryH_AHA.GroupBy(g => g.Service).Count() == 1) {
                        HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH_AHA.FirstOrDefault().Service, ref errorMsg);
                        if (hotel != null) {
                            if (transferList.Where(w => Equals(w.TrfTo, hotel.TrfLocation)).Count() > 0) {
                                var Q1 = from q in transferList
                                         where q.TrfFrom == depLocation && q.TrfTo == hotel.TrfLocation
                                         group q by new { RecID = q.TrfTo, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                                         select new { RecID = k.Key.RecID, Name = k.Key.Name };
                                retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                            } else {
                                var Q2 = from q in new Locations().getLocationList(UserData.Market, LocationType.None, hotel.TrfLocation, null, null, null, ref errorMsg)
                                         select new { RecID = q.RecID, Name = useLocalName ? q.NameL : q.Name };
                                retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q2);
                            }
                        }
                    }
                }
                #endregion
                break;
            case "RT":
                #region Airport to Hotel to Airport
                transferList = new Transfers().getTransferLocations(UserData.Market, ResData.ResMain.PLMarket, country, null, ResData.ResMain.BegDate, ResData.ResMain.EndDate, true, ref errorMsg);
                int? country_RT = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
                int? city_RT = new Locations().getLocationForCity(ResData.ResMain.ArrCity.Value, ref errorMsg);
                if (onlyTicket) {
                    var Q2_RT = from q in transferList //Locations().getLocationList(UserData.Market, LocationType.None, null, country_RT, city_RT, null, ref errorMsg)                            
                                where q.TrfFrom == depLocation
                                group q by new { RecID = q.TrfTo, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                                select new { RecID = k.Key.RecID, Name = k.Key.Name };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q2_RT);
                } else {
                    HotelRecord hotel_RT = new Hotels().getHotelDetail(UserData, ResData.ResService.Where(w => w.ServiceType == "HOTEL").FirstOrDefault().Service, ref errorMsg);
                    var Q1_RT = from q in transferList //Locations().getLocationList(UserData.Market, LocationType.None, null, country_RT, city_RT, null, ref errorMsg)                            
                                where q.TrfFrom == depLocation && (hotel_RT == null || hotel_RT.TrfLocation == q.TrfTo)
                                group q by new { RecID = q.TrfTo, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                                select new { RecID = k.Key.RecID, Name = k.Key.Name };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1_RT);
                }
                #endregion
                break;
            case "AH":
                #region Airport to Hotel
                var queryH_AH = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                if (queryH_AH.Count > 0) {
                    HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH_AH.FirstOrDefault().Service, ref errorMsg);
                    if (hotel != null) {
                        if (transferList.Where(w => w.TrfTo == hotel.TrfLocation).Count() > 0) {
                            var Q1 = from q in transferList
                                     where q.TrfFrom == depLocation && q.TrfTo == hotel.TrfLocation
                                     group q by new { RecID = q.TrfTo, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                                     select new { RecID = k.Key.RecID, Name = k.Key.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        } else {
                            var Q1 = from q in new Locations().getLocationList(UserData.Market, LocationType.None, hotel.TrfLocation, null, null, null, ref errorMsg)
                                     select new { RecID = q.RecID, Name = useLocalName ? q.NameL : q.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        }
                    }

                } else
                    if (onlyTicket) {
                    var Q1 = from q in transferList
                             where q.TrfFrom == depLocation
                             group q by new { RecID = q.TrfTo, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                             select new { RecID = k.Key.RecID, Name = k.Key.Name };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                }
                #endregion
                break;
            case "HA":
                #region Hotel to Airport
                if (string.Equals(ResData.ResMain.PackType, "O")) {
                    var queryH_HA = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                    if (queryH_HA.Count > 0) {
                        string hotel = queryH_HA.FirstOrDefault().Service;
                        List<AirportRecord> hotelAirports = new Hotels().getHotelAirports(UserData, hotel, ref errorMsg);
                        var hotelAPList = from q in hotelAirports
                                          group q by new { RecID = q.Location, Name = useLocalName ? q.LocationNameL : q.LocationName } into k
                                          select new { RecID = k.Key.RecID, Name = k.Key.Name };
                        retval = Newtonsoft.Json.JsonConvert.SerializeObject(hotelAPList);
                    }
                } else {
                    var queryF_HA = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").ToList();
                    if (queryF_HA.Count > 0) {
                        FlightDayRecord flight = new Flights().getFlightDay(UserData, queryF_HA.LastOrDefault().Service, queryF_HA.LastOrDefault().BegDate.Value, ref errorMsg);
                        if (flight != null) {
                            AirportRecord airport = new Flights().getAirport(UserData.Market, flight.DepAirport, ref errorMsg);
                            if (transferList.Where(w => w.TrfTo == airport.Location).Count() > 0) {
                                var Q1 = from q in transferList
                                         where q.TrfFrom == airport.Location
                                         group q by new { RecID = q.TrfTo, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                                         select new { RecID = k.Key.RecID, Name = k.Key.Name };
                                retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                            }
                        }
                    }
                }
                #endregion
                break;
            case "HH":
                #region Hotel to Hotel
                var queryH_HH = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                if (queryH_HH.Count > 1) {
                    HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH_HH.LastOrDefault().Service, ref errorMsg);
                    if (hotel != null) {
                        if (transferList.Where(w => w.TrfTo == hotel.TrfLocation).Count() > 0) {
                            var Q1 = from q in transferList
                                     where q.TrfFrom == depLocation && q.TrfTo == hotel.TrfLocation
                                     group q by new { RecID = q.TrfTo, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                                     select new { RecID = k.Key.RecID, Name = k.Key.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        } else {
                            var Q1 = from q in new Locations().getLocationList(UserData.Market, LocationType.None, hotel.TrfLocation, null, null, null, ref errorMsg)
                                     select new { RecID = q.RecID, Name = useLocalName ? q.NameL : q.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        }
                    }
                }
                #endregion
                break;
            case "AA":
                #region Airport to Airport
                var queryF_AA = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").ToList();
                if (queryF_AA.Count > 0) {
                    List<AirportRecord> airportList = new Flights().getCountryAirportList(UserData.Market, depLocation, ref errorMsg);
                    var Q1 = from q in transferList
                             join q2 in airportList on q.TrfTo equals q2.Location
                             group q by new { RecID = q.TrfTo, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                             select new { RecID = k.Key.RecID, Name = k.Key.Name };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                }
                #endregion
                break;
            case "OH":
                #region Free
                var queryH_HO = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                if (queryH_HO.Count > 0) {
                    HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH_HO.FirstOrDefault().Service, ref errorMsg);
                    if (hotel != null) {
                        if (transferList.Where(w => w.TrfTo == hotel.TrfLocation).Count() > 0) {
                            var Q1 = from q in transferList
                                     where q.TrfFrom == depLocation && q.TrfTo == hotel.TrfLocation
                                     group q by new { RecID = q.TrfTo, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                                     select new { RecID = k.Key.RecID, Name = k.Key.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        } else {
                            var Q1 = from q in new Locations().getLocationList(UserData.Market, LocationType.None, hotel.TrfLocation, null, null, null, ref errorMsg)
                                     select new { RecID = q.RecID, Name = useLocalName ? q.NameL : q.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        }
                    }
                }
                #endregion
                break;
            case "HO":
                #region Free
                var queryH_OH = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "HOTEL").ToList();
                if (queryH_OH.Count > 0) {
                    HotelRecord hotel = new Hotels().getHotelDetail(UserData, queryH_OH.FirstOrDefault().Service, ref errorMsg);
                    if (hotel != null) {
                        int? hotelTrfLocationCity = new Locations().getLocationForCity(hotel.TrfLocation, ref errorMsg);
                        if (transferList.Where(w => w.TrfTo == hotelTrfLocationCity).Count() > 0) {
                            var Q1 = from q in transferList
                                     where q.TrfFrom == hotelTrfLocationCity
                                     group q by new { RecID = q.TrfFrom, Name = useLocalName ? q.TrfToNameL : q.TrfToName } into k
                                     select new { RecID = k.Key.RecID, Name = k.Key.Name };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        } else {
                            var Q1 = from q in new Locations().getLocationList(UserData.Market, LocationType.None, hotelTrfLocationCity, null, null, null, ref errorMsg)
                                     select new { RecID = q.RecID, Name = q.NameL };
                            retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q1);
                        }
                    }
                }
                #endregion
                break;
            default:
                #region Full Free
                var RouteB = from R in transferList
                             where R.TrfFrom == depLocation
                             group R by new { RecID = R.TrfTo, Name = useLocalName ? R.TrfToNameL : R.TrfToName } into g
                             select new { g.Key.RecID, g.Key.Name };
                retval = Newtonsoft.Json.JsonConvert.SerializeObject(RouteB);
                #endregion
                break;
        }
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getTransfers(string DepLocation, string ArrLocation, string trfType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? depLocation = Conversion.getInt32OrNull(DepLocation);
        int? arrLocation = Conversion.getInt32OrNull(ArrLocation != "undefined" ? ArrLocation : "");
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;
        string retval = string.Empty;
        List<TransferRecord> transferList = new Transfers().getTransfers(UserData, ResData, depLocation, arrLocation, true, null, ref errorMsg);
        List<TransferRecord> trfL1 = new List<TransferRecord>();
        if (Equals(trfType, "AHA") || Equals(trfType, "RT"))
            trfL1 = new Transfers().getTransfers(UserData, ResData, Equals(trfType, "AHA") ? arrLocation : depLocation, Equals(trfType, "AHA") ? depLocation : arrLocation, true, null, ref errorMsg);
        switch (trfType) {
            case "AHA":
                #region Airport to Hotel to Airport
                if (transferList.Count > 0 && trfL1.Count > 0) {
                    var Q1 = from q in transferList
                             group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name, Departure = depLocation, Arrival = arrLocation } into k
                             select new { Code = k.Key.Code, Name = k.Key.Name, Departure = k.Key.Departure, Arrival = k.Key.Arrival, transType = "F" };
                    var Q2 = from q in trfL1
                             group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name, Departure = arrLocation, Arrival = depLocation } into k
                             select new { Code = k.Key.Code, Name = k.Key.Name, Departure = k.Key.Departure, Arrival = k.Key.Arrival, transType = "B" };
                    var Q = from q1 in Q1
                            join q2 in Q2 on q1.Arrival equals q2.Departure
                            select new { Code = q1.Code + "|" + q2.Code, Name = q1.Name + ", " + q2.Name, transType = q1.transType + "|" + q2.transType };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                } else {
                    transferList = new Transfers().getTransfers(UserData, ResData, depLocation, null, true, null, ref errorMsg);
                    trfL1 = new Transfers().getTransfers(UserData, ResData, null, depLocation, true, null, ref errorMsg);
                    var Q1 = from q in transferList
                             group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name, Departure = depLocation, Arrival = arrLocation, Direction = q.Direction } into k
                             select new { Code = k.Key.Code, Name = k.Key.Name, Departure = k.Key.Departure, Arrival = k.Key.Arrival, Direction = k.Key.Direction };
                    var Q2 = from q in trfL1
                             group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name, Departure = arrLocation, Arrival = depLocation, Direction = q.Direction } into k
                             select new { Code = k.Key.Code, Name = k.Key.Name, Departure = k.Key.Departure, Arrival = k.Key.Arrival, Direction = k.Key.Direction };
                    var Q = from q1 in Q1
                            join q2 in Q2 on q1.Arrival equals q2.Departure
                            select new { Code = q1.Code + "|" + q2.Code + "|" + q1.Direction, Name = q1.Name + ", " + q2.Name, transType = "M" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                }
                #endregion
                break;
            case "RT":
                #region Airport to Hotel to Airport
                if (transferList.Where(w => w.Direction == "R").Count() > 0 && trfL1.Where(w => w.Direction == "R").Count() > 0) {
                    var Q1 = from q in transferList.Where(w => w.Direction == "R")
                             group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name, Departure = depLocation, Arrival = arrLocation } into k
                             select new { Code = k.Key.Code, Name = k.Key.Name, Departure = k.Key.Departure, Arrival = k.Key.Arrival };
                    var Q2 = from q in trfL1.Where(w => w.Direction == "R")
                             group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name, Departure = arrLocation, Arrival = depLocation } into k
                             select new { Code = k.Key.Code, Name = k.Key.Name, Departure = k.Key.Departure, Arrival = k.Key.Arrival };
                    var Q = from q in Q1.Union(Q2)
                            group q by new { q.Code, q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "R" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                } else {
                    transferList = new Transfers().getTransfers(UserData, ResData, depLocation, null, true, null, ref errorMsg);
                    trfL1 = new Transfers().getTransfers(UserData, ResData, null, depLocation, true, null, ref errorMsg);
                    var Q1 = from q in transferList.Where(w => w.Direction == "R")
                             group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name, Departure = depLocation, Arrival = arrLocation } into k
                             select new { Code = k.Key.Code, Name = k.Key.Name, Departure = k.Key.Departure, Arrival = k.Key.Arrival };
                    var Q2 = from q in trfL1.Where(w => w.Direction == "R")
                             group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name, Departure = arrLocation, Arrival = depLocation } into k
                             select new { Code = k.Key.Code, Name = k.Key.Name, Departure = k.Key.Departure, Arrival = k.Key.Arrival };
                    var Q = from q in Q1.Union(Q2)
                            group q by new { q.Code, q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "R" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                }
                #endregion
                break;
            case "AH":
                #region Airport to Hotel
                if (transferList.Count > 0) {
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                } else {
                    transferList = new Transfers().getTransfers(UserData, ResData, depLocation, null, true, null, ref errorMsg);
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                }
                #endregion
                break;
            case "HA":
                #region Hotel to Airport
                if (transferList.Count > 0) {
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                } else {
                    transferList = new Transfers().getTransfers(UserData, ResData, null, arrLocation, true, null, ref errorMsg);
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                }
                #endregion
                break;
            case "HH":
                #region Hotel to Hotel
                if (transferList.Count > 0) {
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                }
                #endregion
                break;
            case "AA":
                #region Airport to Airport
                if (transferList.Count > 0) {
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                }
                #endregion
                break;
            case "OH":
                #region Other to Hotel
                if (transferList.Count > 0) {
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                } else {
                    transferList = new Transfers().getTransfers(UserData, ResData, depLocation, null, true, null, ref errorMsg);
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                }
                #endregion
                break;
            case "HO":
                #region Hotel to Other
                if (transferList.Count > 0) {
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                } else {
                    transferList = new Transfers().getTransfers(UserData, ResData, null, arrLocation, true, null, ref errorMsg);
                    var Q = from q in transferList
                            group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name } into k
                            select new { Code = k.Key.Code, Name = k.Key.Name, transType = "" };
                    retval = Newtonsoft.Json.JsonConvert.SerializeObject(Q);
                }
                #endregion
                break;
            default:
                transferList = new Transfers().getTransfers(UserData, ResData, depLocation, arrLocation, true, null, ref errorMsg);
                var trfQ = from q in transferList
                           group q by new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name, q.Direction } into k
                           select new { Code = k.Key.Code, Name = k.Key.Name, transType = k.Key.Direction };
                retval = Newtonsoft.Json.JsonConvert.SerializeObject(trfQ);
                break;
        }
        return retval;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getTransferDates(int? DepLocation, int? ArrLocation, string Transfer)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        List<calendarColor> retVal = new Transfers().getTransferDates(UserData, ResData, DepLocation, ArrLocation, Transfer, ref errorMsg);

        return new {
            transferDate = retVal
        };
    }

    [WebMethod(EnableSession = true)]
    public static string CalcService(string selectedCusts, string Departure, string Arrival, string Transfer,
                string _Date, string _DateR, string trfType, int? VehicleCatID, Int16? VehicleUnit)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? routeFrom = Conversion.getInt32OrNull(Departure);
        Int32? routeTo = Conversion.getInt32OrNull(Arrival);
        Int32 _dateA = Convert.ToInt32((Convert.ToInt64(_Date) / 86400000).ToString());
        DateTime _dateB = new DateTime(1970, 1, 1);
        DateTime? Date = _dateB.AddDays(_dateA + 1);
        if (!Date.HasValue)
            return retVal;

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        List<calendarColor> transferDays = new Transfers().getTransferDates(UserData, ResData, routeFrom, routeTo, Transfer, ref errorMsg);
        if (transferDays.Count > 0) {
            var query = from q in transferDays
                        select new {
                            transferDay = new DateTime(q.Year.HasValue ? q.Year.Value : DateTime.Today.Year,
                                                        q.Month.HasValue ? q.Month.Value : DateTime.Today.Month,
                                                        q.Day.HasValue ? q.Day.Value : DateTime.Today.Day)
                        };
            if (query.Where(w => w.transferDay == Date.Value).Count() < 1) {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                                    string.Empty,
                                    HttpContext.GetGlobalResourceObject("Controls", "addPleaseCorrectDate"));
                return "{" + retVal + "}";
            }
        }

        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0) {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        } else {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        if (Equals(trfType, "AHA")) {
            if (string.Equals(ResData.ResMain.PackType, "O"))
                Date = ResData.ResMain.BegDate;
            else
                Date = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").FirstOrDefault().BegDate;
        }
        Int16 StartDay = Convert.ToInt16((Date.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;

        string[] _trf = Transfer.Split('|');
        bool isRT = false;
        for (int i = 0; i < _trf.Length; i++) {
            TransferRecord transfer = new Transfers().getTransfer(UserData.Market, _trf[i], ref errorMsg);
            if (transfer != null && string.Equals(transfer.Direction, "R"))
                isRT = true;
        }

        TransferRecord transfer0 = new Transfers().getTransfer(UserData.Market, _trf[0], ref errorMsg);

        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.Value, Date.Value, "TRANSFER", _trf[0],
            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, routeFrom, routeTo, transfer0 != null ? transfer0.Direction : "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, VehicleCatID, VehicleUnit);
        int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
        int ServiceID1 = -1;
        if (!isRT) {
            if (!(_trf.Length > 2 && string.Equals(_trf[2], "R")) && string.Equals(trfType, "AHA")) {
                if (string.Equals(ResData.ResMain.PackType, "O"))
                    Date = ResData.ResMain.EndDate;
                else
                    Date = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").LastOrDefault().BegDate;
                StartDay = Convert.ToInt16((Date.Value - ResData.ResMain.BegDate.Value).Days);
                Night = 1;
                ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.Value, Date.Value, "TRANSFER", _trf[1],
                                                string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, routeTo, routeFrom, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, VehicleCatID, VehicleUnit);
                ServiceID1 = ResData.ResService[ResData.ResService.Count - 1].RecID;
            }
        }
        if (!string.IsNullOrEmpty(errorMsg)) {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        } else {
            ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
            ResServiceRecord service1 = new ResServiceRecord();
            if (!(_trf.Length > 2 && string.Equals(_trf[2], "R")) && string.Equals(trfType, "AHA") && !isRT)
                service1 = ResData.ResService.Find(f => f.RecID == ServiceID1);

            DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "TRANSFER", ServiceID, ref errorMsg);
            if (!string.IsNullOrEmpty(errorMsg)) {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                return "{" + retVal + "}";
            }
            decimal? _salePrice = Conversion.getDecimalOrNull(returnData.Rows[0]["SalePrice"]);
            DataTable returnData1;
            if (!(_trf.Length > 2 && string.Equals(_trf[2], "R")) && string.Equals(trfType, "AHA") && !isRT) {
                returnData1 = new Reservation().CalcServicePrice(UserData, ResData, "TRANSFER", ServiceID1, ref errorMsg);
                _salePrice += Conversion.getDecimalOrNull(returnData1.Rows[0]["SalePrice"]).HasValue ? Conversion.getDecimalOrNull(returnData1.Rows[0]["SalePrice"]).Value : Convert.ToDecimal(0);
            }
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"Adult\":\"{2}\",\"Child\":\"{3}\",\"Unit\":\"{4}\"",
                (_salePrice.HasValue ? _salePrice.Value.ToString("#,###.00") : "") + " " + returnData.Rows[0]["SaleCur"].ToString(),
                    supplierRec.NameL,
                    AdlCnt,
                    ChdCnt,
                    service.Unit);
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string SaveService(string selectedCusts, string Departure, string Arrival, string Transfer,
                string _Date, string _DateR, string PickupTimeHH, string PickupTimeSS, string PickupNote,
                string PickupTimeHHR, string PickupTimeSSR, string PickupNoteR, string recordType,
                string trfType, string SuppNote, int? RegionFrom, int? RegionTo, string RegionFromLocation, string RegionToLocation,
                int? RegionFromR, int? RegionToR, string RegionFromLocationR, string RegionToLocationR, int? VehicleCatID, Int16? VehicleUnit)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        Int32? routeFrom = Conversion.getInt32OrNull(Departure);
        Int32? routeTo = Conversion.getInt32OrNull(Arrival);
        Int32 _dateA = Convert.ToInt32((Convert.ToInt64(_Date) / 86400000).ToString());
        DateTime _dateB = new DateTime(1970, 1, 1);
        DateTime? Date = _dateB.AddDays(_dateA + 1);
        if (!Date.HasValue)
            return retVal;
        _dateA = Convert.ToInt32((Convert.ToInt64(_DateR) / 86400000).ToString());
        _dateB = new DateTime(1970, 1, 1);
        DateTime? DateR = _dateB.AddDays(_dateA + 1);

        int? pickupTimeHH = Conversion.getInt32OrNull(PickupTimeHH != "undefined" ? PickupTimeHH : "");
        int? pickupTimeSS = Conversion.getInt32OrNull(PickupTimeSS != "undefined" ? PickupTimeSS : "");
        int? pickupTimeHHR = Conversion.getInt32OrNull(PickupTimeHHR != "undefined" ? PickupTimeHHR : "");
        int? pickupTimeSSR = Conversion.getInt32OrNull(PickupTimeSSR != "undefined" ? PickupTimeSSR : "");
        DateTime? pickupTime = null;
        DateTime? pickupTimeR = null;
        if (pickupTimeHH.HasValue && pickupTimeSS.HasValue) {
            pickupTime = Date.Value.AddHours(pickupTimeHH.Value).AddMinutes(pickupTimeSS.Value);
        }
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        List<calendarColor> transferDays = new Transfers().getTransferDates(UserData, ResData, routeFrom, routeTo, Transfer, ref errorMsg);
        if (transferDays.Count > 0) {
            var query = from q in transferDays
                        select new {
                            transferDay = new DateTime(q.Year.HasValue ? q.Year.Value : DateTime.Today.Year,
                                                        q.Month.HasValue ? q.Month.Value : DateTime.Today.Month,
                                                        q.Day.HasValue ? q.Day.Value : DateTime.Today.Day)
                        };
            if (query.Where(w => w.transferDay == Date.Value).Count() < 1) {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                                    string.Empty,
                                    HttpContext.GetGlobalResourceObject("Controls", "addPleaseCorrectDate"));
                return "{" + retVal + "}";
            }
        }

        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0) {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        } else {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        if (Equals(trfType, "AHA")) {
            if (ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").Count() > 0)
                Date = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").FirstOrDefault().BegDate;
            else
                Date = ResData.ResMain.BegDate;
        }
        Int16 StartDay = Convert.ToInt16((Date.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;
        string[] _trf = Transfer.Split('|');
        bool isRT = false;
        for (int i = 0; i < _trf.Length; i++) {
            TransferRecord transfer = new Transfers().getTransfer(UserData.Market, _trf[i], ref errorMsg);
            if (transfer != null && string.Equals(transfer.Direction, "R"))
                isRT = true;
        }

        if (isRT) {
            if (!DateR.HasValue)
                return retVal;
            if (pickupTimeHHR.HasValue && pickupTimeSSR.HasValue)
            {
                pickupTimeR = DateR.Value.AddHours(pickupTimeHHR.Value).AddMinutes(pickupTimeSSR.Value);
            }
            else
                pickupTimeR = DateR.Value;
        }
        TransferRecord transfer0 = new Transfers().getTransfer(UserData.Market, _trf[0], ref errorMsg);

        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.Value, transfer0.Direction == "R" ? DateR.Value : Date.Value, "TRANSFER", _trf[0],
            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, routeFrom, routeTo, transfer0 != null ? transfer0.Direction : "", "", 0, 0, null, pickupTime, PickupNote,
            string.Empty, Conversion.getByteOrNull(RegionFrom.ToString()), Conversion.getByteOrNull(RegionTo.ToString()), Conversion.getByteOrNull(RegionFromR.ToString()), Conversion.getByteOrNull(RegionToR.ToString()), RegionFromLocation, RegionToLocation, RegionFromLocationR, RegionToLocationR, null, false, null, string.Empty, null, ref errorMsg, pickupTimeR, PickupNoteR, null, null, null, VehicleCatID, VehicleUnit);
        int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
        int ServiceID1 = -1;
        if (!isRT) {
            if (ServiceID > 0) {
                List<ResServiceRecord> resService = ResData.ResService;
                ResServiceRecord trfService = resService.Find(f => f.RecID == ServiceID);
                if (trfService != null)
                    trfService.SupNote = Conversion.getStrOrNull(SuppNote);
            }
        }
        if (!(_trf.Length > 2 && string.Equals(_trf[2], "R")) && Equals(trfType, "AHA") && !isRT) {
            if (ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").Count() > 0)
                Date = ResData.ResService.OrderBy(o => o.BegDate).Where(w => w.ServiceType == "FLIGHT").LastOrDefault().BegDate;
            else
                Date = ResData.ResMain.EndDate;

            StartDay = Convert.ToInt16((Date.Value - ResData.ResMain.BegDate.Value).Days);
            Night = 1;
            ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.Value, Date.Value, "TRANSFER", _trf[1],
                                            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, routeTo, routeFrom, "", "", 0, 0, null, pickupTime, PickupNote, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, VehicleCatID, VehicleUnit);
            ServiceID1 = ResData.ResService[ResData.ResService.Count - 1].RecID;
            if (ServiceID1 > 0) {
                List<ResServiceRecord> resService = ResData.ResService;
                ResServiceRecord trfService = resService.Find(f => f.RecID == ServiceID1);
                if (trfService != null)
                    trfService.SupNote = Conversion.getStrOrNull(SuppNote);
            }
        }
        if (!string.IsNullOrEmpty(errorMsg)) {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        } else {
            if (_recordType == true) {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg)) {
                    List<ResServiceRecord> resServiceList = ResData.ResService;
                    ResServiceRecord resService = resServiceList.LastOrDefault();
                    resService.ExcludeService = false;
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                } else {
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
            } else {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK) {
                    HttpContext.Current.Session["ResData"] = ResData;
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData) {
                        if (!row.ControlOK)
                            Msg += row.Message + "<br />";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                } else {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData) {
                        if (!row.ControlOK)
                            Msg += row.Message + "<br />";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
            }
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> getRegionDeparture(int? DepLocation, int? ArrLocation, string Transfer, long? flyDate, int? RegionFrom, bool Return)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;

        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(flyDate) / 86400000) + 1);
        TransferRecord transfer = new Transfers().getTransfer(UserData.Market, Transfer, ref errorMsg);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CodeName> retVal = new List<CodeName>();
        switch (RegionFrom) {
            case 0:
                if (transfer != null && transfer.Direction == "B")
                    retVal = new OnlyTransfers().getLocationFlights(UserData, ArrLocation, checkIn, true, ref errorMsg);
                else if (transfer != null && transfer.Direction == "R")
                    retVal = new OnlyTransfers().getLocationFlights(UserData, DepLocation, checkIn, Return ? true : false, ref errorMsg);
                else
                    retVal = new OnlyTransfers().getLocationFlights(UserData, Return ? ArrLocation : DepLocation, checkIn, false, ref errorMsg);
                break;
            case 1:
                if (transfer != null && transfer.Direction == "B")
                    retVal = new OnlyTransfers().getLocationHotels(UserData, DepLocation, useLocalName, ref errorMsg);
                else if (transfer != null && transfer.Direction == "R")
                    retVal = new OnlyTransfers().getLocationHotels(UserData, ArrLocation, useLocalName, ref errorMsg);
                else
                    retVal = new OnlyTransfers().getLocationHotels(UserData, Return ? ArrLocation : DepLocation, useLocalName, ref errorMsg);
                break;
            case 2:
                retVal = new OnlyTransfers().getLocations(UserData, new Locations().getLocationForCity(ArrLocation.HasValue ? ArrLocation.Value : -1, ref errorMsg), useLocalName, ref errorMsg);
                break;
            default:
                retVal = new List<CodeName>();
                break;
        }
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> getRegionArrival(int? ArrLocation, int? DepLocation, string Transfer, long? flyDate, int? RegionTo, bool Return)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(flyDate) / 86400000) + 1);

        string errorMsg = string.Empty;

        TransferRecord transfer = new Transfers().getTransfer(UserData.Market, Transfer, ref errorMsg);

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CodeName> retVal = new List<CodeName>();
        switch (RegionTo) {
            case 0:
                if (transfer != null && transfer.Direction == "B")
                    retVal = new OnlyTransfers().getLocationFlights(UserData, ArrLocation, checkIn, true, ref errorMsg);
                else if (transfer != null && transfer.Direction == "R")
                    retVal = new OnlyTransfers().getLocationFlights(UserData, DepLocation, checkIn, Return ? true : false, ref errorMsg);
                else
                    retVal = new OnlyTransfers().getLocationFlights(UserData, Return ? DepLocation : ArrLocation, checkIn, true, ref errorMsg);
                break;
            case 1:
                if (transfer != null && transfer.Direction == "B")
                    retVal = new OnlyTransfers().getLocationHotels(UserData, DepLocation, useLocalName, ref errorMsg);
                else if (transfer != null && transfer.Direction == "R")
                    retVal = new OnlyTransfers().getLocationHotels(UserData, ArrLocation, useLocalName, ref errorMsg);
                else
                    retVal = new OnlyTransfers().getLocationHotels(UserData, Return ? DepLocation : ArrLocation, useLocalName, ref errorMsg);
                break;
            case 2:
                retVal = new OnlyTransfers().getLocations(UserData, new Locations().getLocationForCity(ArrLocation.HasValue ? ArrLocation.Value : -1, ref errorMsg), useLocalName, ref errorMsg);
                break;
            default:
                retVal = new List<CodeName>();
                break;
        }
        return retVal;
    }
}

