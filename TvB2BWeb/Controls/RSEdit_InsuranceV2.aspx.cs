﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSEdit_InsuranceV2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        Int32? recID = Conversion.getInt32OrNull(RecID);

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var querySelected = from s in ResData.ResCust
                            join q1 in ResData.ResCon on s.CustNo equals q1.CustNo
                            where s.Status == 0 && q1.ServiceID == recID
                            select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name, Selected = true };
        var queryUnSelected = from s in ResData.ResCust
                              join q1 in ResData.ResCon on s.CustNo equals q1.CustNo
                              where s.Status == 0 && q1.ServiceID != recID && (querySelected.Where(w => w.CustNo == s.CustNo).Count() < 1)
                              group s by new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name } into k
                              select new { CustNo = k.Key.CustNo, Name = k.Key.Name, Selected = false };
        var query = querySelected.Union(queryUnSelected);
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getServiceDetail(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        Int32? recID = Conversion.getInt32OrNull(RecID);
        if (!recID.HasValue) return string.Empty;
        var query = from q in ResData.ResService
                    where q.RecID == recID.Value
                    select new
                    {
                        BegDate = q.BegDate.Value.ToShortDateString(),
                        EndDate = q.EndDate.Value.ToShortDateString(),
                        DepLocation = q.DepLocationNameL,
                        Service = q.Service,
                        Adult = q.Adult,
                        Child = q.Child,
                        Unit = q.Unit,
                        Duration = q.Duration,
                        Supplier = q.SupplierName,
                        SupNote = q.SupNote,
                        StatConf = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ? q.StatConfNameL : string.Empty,
                        StatSer = q.StatSerNameL,
                        SalePrice = !Equals(q.IncPack, "Y") ? q.SalePrice.Value.ToString("#,###.00") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        SaleCur = !Equals(q.IncPack, "Y") ? q.SaleCur : "",
                        ChangePax = Equals(q.IncPack, "Y") || (q.Compulsory.HasValue && q.Compulsory.Value) ? "0" : "1"
                    };

        if (query == null || query.Count() < 1) return string.Empty;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query.FirstOrDefault());
    }

    [WebMethod(EnableSession = true)]
    public static string getInsurance(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        string errorMsg = string.Empty;
        decimal? Amount = null; // ResData.ResMain.PasPayable - ResData.ResService.SalePrice;
        Int16? RemainDay = Convert.ToInt16((ResData.ResMain.BegDate.Value - ResData.ResMain.ResDate.Value).TotalDays);
        List<InsuranceRecord> insuranceList = new Insurances().getInsuranceForCountry(UserData, ResData.ResMain.PLMarket, resService.DepLocation, RemainDay, Amount, ref errorMsg);
        int? country = new Locations().getLocationForCountry(resService.DepLocation.Value, ref errorMsg);
        List<InsurancePriceRecord> _insPrice = new Insurances().getInsurancePrice(UserData.Market, ResData.ResMain.PLMarket, country.HasValue ? country.Value : -1, ref errorMsg);        
        List<InsurancePriceRecord> insrPrice = new List<InsurancePriceRecord>();
        if (string.Equals(resService.IncPack, "Y"))
        {
            List<CatPackPlanSerRecord> catPackPlanSer = new Services().getCatPackPlanSer(ResData.ResMain.PriceListNo, ref errorMsg);

            insrPrice = (from q1 in _insPrice
                         join q2 in catPackPlanSer on q1.Insurance equals q2.ServiceItem
                         where string.Equals(q2.Service, "INSURANCE")
                         select q1).ToList<InsurancePriceRecord>();
        }
        else
            insrPrice = (from q1 in _insPrice                         
                         select q1).ToList<InsurancePriceRecord>();
        List<InsuranceRecord> insList = new List<InsuranceRecord>();
        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Sunrise))
        {
            if (resService.Compulsory.HasValue && resService.Compulsory.Value)
                insList = (from q1 in insuranceList
                           join q2 in insrPrice on q1.Code equals q2.Insurance
                           where q2.Compulsory.HasValue && q2.Compulsory.Value
                           select q1).ToList<InsuranceRecord>();
            else
                insList = (from q1 in insuranceList
                           join q2 in insrPrice on q1.Code equals q2.Insurance
                           where !q2.Compulsory.HasValue || (q2.Compulsory.HasValue && !q2.Compulsory.Value)
                           select q1).ToList<InsuranceRecord>();
        }
        else
        {
            if (resService.Compulsory.HasValue && resService.Compulsory.Value)
                insList = (from q1 in insuranceList
                           join q2 in insrPrice on q1.Code equals q2.Insurance
                           where q2.Compulsory.HasValue && q2.Compulsory.Value
                           select q1).ToList<InsuranceRecord>();
            else
                insList = (from q1 in insuranceList
                           join q2 in insrPrice on q1.Code equals q2.Insurance
                           where !q2.Compulsory.HasValue || (q2.Compulsory.HasValue && !q2.Compulsory.Value)
                           select q1).ToList<InsuranceRecord>();
        }
        var query = from q in insList
                    group q by new {q.Code, q.LocalName} into k
                    select new { Code = k.Key.Code, Name = k.Key.LocalName };
        var query1 = from q in insList
                     where q.InsType != 1
                     group q by new { q.Code, q.LocalName } into k
                     select new { Code = k.Key.Code, Name = k.Key.LocalName };

        return Newtonsoft.Json.JsonConvert.SerializeObject(Equals(ResData.ResMain.PaymentStat, "U") ? query : query1);
    }

    [WebMethod(EnableSession = true)]
    public static editServiceReturnData CalcService(string RecID, string Insurance, bool? newRes, string SelectedCust)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        int? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        decimal? oldResSalePrice = ResData.ResMain.SalePrice;
        ResServiceRecord resService = (TvBo.ResServiceRecord)TvBo.Common.DeepClone(ResData.ResService.Find(f => f.RecID == recID));
        decimal? oldServicePrice = resService.SalePrice;

        List<ResConRecord> serviceResCon = (List<ResConRecord>)TvBo.Common.DeepClone(ResData.ResCon.Where(w => w.ServiceID == resService.RecID).ToList<ResConRecord>());

        string[] selectedCust = SelectedCust.Split('|');

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        int InfCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5 && w.Title < 8).Count();
            InfCnt = tmpPax.Where(w => w.Title > 7).Count();
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;

        resService.PriceSource = !Equals(resService.Service, Insurance) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? (!Equals(resService.IncPack, "Y") ? "Y" : "N") : "Y";
        resService.Service = Insurance;
        resService.Supplier = string.Empty;
        resService.Adult = (Int16)AdlCnt;
        resService.Child = (Int16)(ChdCnt + InfCnt);
        resService.Unit = resService.Unit;

        ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
        ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);

        if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
        }
        else
        {
            resService = ResData.ResService.Find(f => f.RecID == recID.Value);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
            string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
            return new editServiceReturnData
            {
                Calc = true,
                CalcPrice = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                CalcCur = resService.SalePrice.HasValue && !Equals(resService.IncPack, "Y") ? resService.SaleCur : string.Empty,
                Adult = resService.Adult,
                Child = resService.Child,
                Unit = resService.Unit,
                OldResPrice = oldResSalePrice.HasValue ? oldResSalePrice.Value.ToString("#,###.00") : string.Empty,
                OldServicePrice = oldServicePrice.HasValue ? oldServicePrice.Value.ToString("#,###.00") : string.Empty,
                Supplier = supplierRec.NameL,
                Msg = string.Empty
            };
        }        
    }

    [WebMethod(EnableSession = true)]
    public static editServiceReturnData SaveService(string RecID, string Insurance, bool? newRes, string SelectedCust)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID.Value);
        TvBo.InsuranceRecord insurance = new Insurances().getInsurance(UserData.Market, Insurance, ref errorMsg);              

        string[] selectedCust = SelectedCust.Split('|');

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        int InfCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5 && w.Title < 8).Count();
            InfCnt = tmpPax.Where(w => w.Title > 7).Count();
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;

        resService.PriceSource = !Equals(resService.Service, Insurance) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? (Equals(resService.IncPack, "Y") ? "Y" : "N") : "Y";
        resService.Service = Insurance;
        resService.ServiceName = insurance.Name;
        resService.ServiceNameL = insurance.LocalName;
        resService.Supplier = string.Empty;
        InsuranceRecord insuranceDetail = new InsuranceRecord();
        insuranceDetail = new Insurances().getInsurance(UserData.Market, resService.Service, ref errorMsg);
        resService.ServiceName = insuranceDetail.Name;
        resService.ServiceNameL = insuranceDetail.LocalName;
        resService.Adult = (Int16)AdlCnt;
        resService.Child = (Int16)(ChdCnt + InfCnt);

        if (newRes.HasValue && newRes.Value)
        {
            ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
            ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);
            if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                HttpContext.Current.Session["ResData"] = ResData;
                return new editServiceReturnData { Calc = true, CalcPrice = resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : string.Empty, CalcCur = resService.SalePrice.HasValue ? resService.SaleCur : string.Empty, Msg = string.Empty };
            }            
        }
        else
        {
            Int32 ErrCode = 0;
            ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
            ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);
            List<int?> recIDList = new List<int?>();
            recIDList.Add(resService.RecID);
            ResData = new TvBo.Reservation().editService(UserData, ResData, recIDList, ref ErrCode, ref errorMsg, false);
            if (ErrCode != 0)
            {
                if (ErrCode < 0)
                    errorMsg = string.IsNullOrEmpty(errorMsg) ? "unknown error." : errorMsg;
                else errorMsg = new ResCalcError().calcError(ErrCode, ResData).ToString();
                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();

                HttpContext.Current.Session["ResData"] = ResData;
                return new editServiceReturnData { Calc = true, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = string.Empty };
            }
        }        
    }

    [WebMethod(EnableSession = true)]
    public static string getChgFee(string RecID, string Insurance)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        resService.Service = Insurance;
        ResChgFeePrice chgPrice = new Reservation().whatHasChangedResService(UserData, ResData, recID, resService, ref errorMsg);
        if (chgPrice == null)
            return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        else
        {
            string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                             chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
            return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
        }
    }
}
