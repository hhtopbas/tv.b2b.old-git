﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSEdit_Excursion.aspx.cs"
    Inherits="Controls_RSEdit_Excursion" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "RSEditExcursion") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/RSEditStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        var newRes = $.query.get('NewRes') == '1' ? true : false;

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function getExcursion(RecID, service) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Excursion.aspx/getExcursion",
                data: '{"RecID":"' + RecID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sExcursion").html('');
                    $("#sExcursion").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sExcursion").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        $("#sExcursion").val(service);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                    return (false);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }        

        function preCalc(save) {
            if ($("#sExcursion").val() == null || $("#sExcursion").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectExcursion") %>');
                return;
            }

            var hfRecID = document.getElementById("<%= hfRecID.ClientID %>");

            var _data = '{"RecID":"' + hfRecID.value + '"' + ',"Excursion":"' + $("#sExcursion").val() + '"' + ',"newRes":' + newRes + '}';
            var _url = "";
            if (save)
                _url = "../Controls/RSEdit_Excursion.aspx/SaveService";
            else _url = "../Controls/RSEdit_Excursion.aspx/CalcService"
            $.ajax({
                type: "POST",
                url: _url,
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        if (save) {
                            if (msg.d.Calc == true) {
                                window.close;
                                self.parent.returnEditResServices(true);
                            } else showMessage(msg.d.Msg);
                        }
                        else {
                            if (msg.d.Calc == true) {
                                $("#iSalePrice").text(msg.d.CalcPrice + ' ' + msg.d.CalcCur);
                                $("#iAdult").text(msg.d.Adult != null ? msg.d.Adult : '');
                                $("#iChild").text(msg.d.Child != null ? msg.d.Child : '');
                                $("#iUnit").text(msg.d.Unit != null ? msg.d.Unit : '');
                            } else showMessage(msg.d.Msg);
                        }
                    } else showMessage('unknown error.');
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            if (source == 'save') {
                if ($("#sExcursion").val() == null || $("#sExcursion").val() == '') {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectExcursion") %>');
                    return;
                }
                if (newRes != true) {
                    $.ajax({
                        type: "POST",
                        url: "../Controls/RSEdit_Excursion.aspx/getChgFee",
                        data: '{"RecID":"' + recID + '","Excursion":"' + $("#sExcursion").val() + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(msg) {
                            if (msg.d != '') {
                                var retVal = $.json.decode(msg.d);
                                if (retVal.retVal == 1) {
                                    $(function() {
                                        $("#messages").html(retVal.Message);
                                        $("#dialog").dialog("destroy");
                                        $("#dialog-message").dialog({
                                            modal: true,
                                            buttons: {
                                                '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>': function() {
                                                    $(this).dialog('close');
                                                    preCalc(true);
                                                },
                                                '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>': function() {
                                                    $(this).dialog('close');
                                                }
                                            }
                                        });
                                    });
                                }
                                else preCalc(true);
                            }
                        },
                        error: function(xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showMessage(xhr.responseText);
                        },
                        statusCode: {
                            408: function() {
                                logout();
                            }
                        }
                    });
                }
                else preCalc(true);
            }
            else
                if (source == 'cancel') {
                window.close;
                self.parent.returnEditResServices(false);
            }
        }

        function getServiceDetail(recID) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Excursion.aspx/getServiceDetail",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var resService = $.json.decode(msg.d);
                        $("#iDate").text(resService.Date);
                        $("#sLocation").text(resService.Location);
                        $("#iAdult").text(resService.Adult);
                        $("#iChild").text(resService.Child);
                        $("#iUnit").text(resService.Unit);
                        if (resService.StatConf != '')
                            $("#sConfirmation").text(resService.StatConf);
                        else $("#divConfirmation").hide();
                        $("#sStatus").text(resService.StatSer);
                        var salePrice = resService.SalePrice + ' ' + resService.SaleCur;
                        $("#iSalePrice").text(salePrice);
                        getExcursion(recID, resService.Service);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        $(document).ready(
            function() {
                $.query = $.query.load(location.href);
                var recID = $.query.get('RecID');
                var hfRecID = document.getElementById("<%= hfRecID.ClientID %>");
                hfRecID.value = recID;
                getServiceDetail(recID);
                
            });
    </script>

</head>
<body>
    <form id="formRsFlight" runat="server">
    <br />
    <div id="divRs">
        <div>
            <table>
                <tr>
                    <td class="leftCell">
                        <div id="divRouteFrom" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewLocation") %>
                                    :</span>
                            </div>
                            <div id="divSLocation" class="inputDiv">
                                <b><span id="sLocation"></span></b>
                            </div>
                        </div>
                        <div id="divDate" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewDate") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iDate"></span></b>
                            </div>
                        </div>
                        <div id="divFlightNo" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewExcursion") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sExcursion">
                                </select>
                            </div>
                        </div>
                        <div id="divAdultChild" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                    :</span>
                            </div>
                            <div style="float: left; width: 100px; height: 100%; text-align: left;">
                                <b><span id="iAdult"></span></b>
                            </div>
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                    :</span>
                            </div>
                            <div style="float: left; width: 100px; height: 100%; text-align: left;">
                                <b><span id="iChild"></span></b>
                            </div>
                        </div>
                        <div id="divUnit" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewUnit") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iUnit"></span></b>
                            </div>
                        </div>
                    </td>
                    <td class="rightCell">
                        <div id="divStatus">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewStatus") %></span>
                            <br />
                            <b><span id="sStatus"></span></b>
                        </div>
                        <br />
                        <div id="divConfirmation">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewConfirmation") %></span>
                            <br />
                            <b><span id="sConfirmation"></span></b>
                        </div>
                        <br />
                        <div id="divSalePrice">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewSalePrice") %></span>
                            <br />
                            <span id="iSalePrice" class="salePrice"></span>
                        </div>
                        <br />
                        <div id="divCalcBtn">
                            <input id="btnRecalc" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                                onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                style="width: 150px;" />
                        </div>
                        <asp:HiddenField ID="hfRecID" runat="server" />
                    </td>
                </tr>
            </table>            
        </div>
        <table id="divBtn">
            <tr>
                <td style="width: 50%;" align="center">
                    <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
                        onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
                <td align="center">
                    <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                        onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
            </tr>
        </table>
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
            </span><span id="messages">Message</span>
        </p>
    </div>
    </form>
</body>
</html>
