﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using System.Text;
using TvTools;
using System.Web.Script.Services;

public partial class FlightSeatCheckInForm : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            HttpContext.Current.Session["SelectedSeat"] = null;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getSeatPlan(int? ServiceID, int? CustNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();

        ResServiceRecord flightService = ResData.ResService.Find(f => f.RecID == ServiceID);
        if (flightService == null)
        {
            return new
            {
                Success = false,
                Message = "Service not found."
            };
        }

        try
        {
            var serviceCust = from c in ResData.ResCust
                              join s in ResData.ResCon on c.CustNo equals s.CustNo
                              where s.ServiceID == ServiceID
                              select c;
            var custSSR = from s in ResData.ResCon
                          where s.ServiceID == ServiceID
                          select s;
            bool notExists = serviceCust.Where(w => w.Title > 5).Count() > 0 ||
                             serviceCust.Where(w => !w.Birtday.HasValue).Count() > 0;

            ResCustRecord selectCust = ResData.ResCust.Find(f => f.CustNo == CustNo);
            bool isChild = selectCust != null && selectCust.Title > 5;
            PlaneTemp planeT = new FlightSeatCheckIn().getPlaneTemp(UserData, flightService.ResNo, flightService.Service, flightService.BegDate, ref errorMsg);
            List<PlaneTempZone> planeTZ = new FlightSeatCheckIn().getPlaneTempZone(UserData, planeT.RecID, ref errorMsg);
            List<PlaneTempZoneItems> planeTZI = new FlightSeatCheckIn().getPlaneTempZoneItems(UserData, planeT.RecID, ref errorMsg);
            List<FlightCIn> seatCInList = new FlightSeatCheckIn().getFlightSeatCIn(UserData, flightService.Service, flightService.BegDate, ref errorMsg);

            List<FlightCIn> reservedSeat = new List<FlightCIn>();
            if (HttpContext.Current.Session["SelectedSeat"] == null)
            {
                HttpContext.Current.Session["SelectedSeat"] = reservedSeat;
            }
            reservedSeat = (List<FlightCIn>)HttpContext.Current.Session["SelectedSeat"];

            List<FlightCIn> childParents = seatCInList.Union(reservedSeat).Where(w => w.ServiceID == ServiceID).ToList<FlightCIn>();
            List<CodeName> extraPriceList = new List<CodeName>();
            List<ServiceExtRecord> extraServiceList = new List<ServiceExtRecord>();
            List<novaSeatPlan> seatExtra = (from q in planeTZI
                                            where !string.IsNullOrEmpty(q.ExtraService)
                                            group q by new { q.ExtraService } into k
                                            select new novaSeatPlan { extraService = k.Key.ExtraService, allotIsFull = false }).ToList<novaSeatPlan>();

            foreach (var extSer in seatExtra)
            {
                bool onlyTicket = ResData.ResMain.SaleResource.HasValue && (new List<short> { 2, 3, 5 }).Contains(ResData.ResMain.SaleResource.Value);
                List<ExtraServiceListRecord> extPrices = new Reservation().getExtraServicesForFlight(UserData, ResData.ResMain.PLMarket, ResData.ResMain.ResDate, ResData.ResMain.SaleCur, flightService.FlgClass, flightService.BegDate, flightService.Duration, false, flightService.Service, onlyTicket, string.Empty, true, ref errorMsg);
                if (extPrices != null)
                {
                    ExtraServiceListRecord extPrice = extPrices.Find(f => (string.IsNullOrEmpty(f.ServiceItem) || (f.Service == "FLIGHT" && f.ServiceItem == flightService.Service)) && f.Code == extSer.extraService);
                    if (extPrice != null && extPrice.SaleAdlPrice.HasValue)
                    {
                        string priceStr = string.Format("[{2}], price (Adult): {0} {1} ",
                            extPrice.SaleAdlPrice.Value.ToString("#,###.00"),
                            ResData.ResMain.SaleCur,
                            extPrice.DescriptionL);
                        extraPriceList.Add(new CodeName
                        {
                            Code = extSer.extraService,
                            Name = priceStr
                        });
                    }
                    if (!new Reservation().getExtSerAllotUnit(UserData, ResData.ResMain, ResData.ResMain.PLMarket, /*Supplier*/"", "FLIGHT", flightService.Service, flightService.BegDate, flightService.EndDate, flightService.Duration, extSer.extraService, ResData.ResMain.ResDate, flightService.FlgClass, extPrice != null ? extPrice.CalcType : 0, 1, Convert.ToInt32("0"), ref errorMsg))
                    {
                        extSer.allotIsFull = true;
                    }
                }
                ServiceExtRecord extraService = new Reservation().getExtraServisDetail(UserData, "FLIGHT", extSer.extraService, ref errorMsg);
                if (extraService != null)
                {
                    extraServiceList.Add(extraService);
                }
            }
            var thisReservationSeats = from q in seatCInList
                                       join q1 in serviceCust on q.CustNo equals q1.CustNo
                                       select q;

            var thisReserveSeats = from q in reservedSeat
                                   join q1 in serviceCust on q.CustNo equals q1.CustNo
                                   select q;
            var seatPlan = from q1 in planeTZI
                           select new
                           {
                               q1.ZoneId,
                               q1.ItemRow,
                               q1.ItemCol,
                               q1.IsExit,
                               q1.ExtraLegroom,
                               q1.ItemType,
                               q1.SeatLetter,
                               q1.SeatNo,
                               isUse = seatCInList.Find(f => f.SeatLetter == q1.SeatLetter && f.SeatNo == q1.SeatNo) != null,
                               inUsethisReservation = thisReservationSeats.Where(w => w.SeatLetter == q1.SeatLetter && w.SeatNo == q1.SeatNo).Count() > 0,
                               inUsethisReserve = thisReserveSeats.Where(w => w.SeatLetter == q1.SeatLetter && w.SeatNo == q1.SeatNo).Count() > 0
                           };
            int? seatWidth = (from q in planeTZ
                              select new { maxcol = q.ItemLength })
                             .OrderBy(o => o.maxcol)
                             .LastOrDefault().maxcol;

            List<SSRQCodeRecord> ssrqCodes = new List<SSRQCodeRecord>();
            foreach (var rSSR in ResData.ResCon.Where(w => w.ServiceID == ServiceID && (!string.IsNullOrEmpty(w.SpecSerRQCode1) || !string.IsNullOrEmpty(w.SpecSerRQCode2))))
            {
                if (!string.IsNullOrEmpty(rSSR.SpecSerRQCode1))
                {
                    SSRQCodeRecord ssrq = new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, rSSR.SpecSerRQCode1, ref errorMsg);
                    if (ssrq != null)
                        ssrqCodes.Add(ssrq);
                }
                if (!string.IsNullOrEmpty(rSSR.SpecSerRQCode2))
                {
                    SSRQCodeRecord ssrq = new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, rSSR.SpecSerRQCode2, ref errorMsg);
                    if (ssrq != null)
                        ssrqCodes.Add(ssrq);
                }
            }

            foreach (ServiceExtRecord row in extraServiceList)
            {
                if (!string.IsNullOrEmpty(row.SpecSerRQCode))
                {
                    SSRQCodeRecord ssrq = new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, row.SpecSerRQCode, ref errorMsg);
                    if (ssrq != null)
                        ssrqCodes.Add(ssrq);
                }
            }

            foreach (PlaneTempZone row in planeTZ)
            {
                List<PlaneTempZoneItems> zoneSeatList = new List<PlaneTempZoneItems>();
                var planetzi = from q in planeTZI
                               orderby q.ItemCol, q.ItemRow
                               where q.ZoneId == row.RecID
                               select q;
                foreach (PlaneTempZoneItems item in planetzi)
                {
                    //RecID,PlaneID,ZoneId,SeatLetter,ItemRow,ItemCol,ItemType,ForB2B,ForB2C,IsExit,SeatNo,ForBackOffice,ExtraLegroom,ExtraService
                    PlaneTempZoneItems zoneSeat = new PlaneTempZoneItems();
                    zoneSeat.RecID = item.RecID;
                    zoneSeat.PlaneID = item.PlaneID;
                    zoneSeat.ZoneId = item.ZoneId;
                    zoneSeat.SeatLetter = item.SeatLetter;
                    zoneSeat.SeatNo = item.SeatNo;
                    zoneSeat.ItemRow = item.ItemRow;
                    zoneSeat.ItemCol = item.ItemCol;
                    zoneSeat.ItemType = item.ItemType;
                    zoneSeat.ForBackOffice = item.ForBackOffice;
                    zoneSeat.ForB2B = item.ForB2B;
                    zoneSeat.ForB2C = item.ForB2C;
                    zoneSeat.ExtraLegroom = item.ExtraLegroom;
                    zoneSeat.ExtraService = item.ExtraService;
                    zoneSeat.ExtraServicePrice = extraPriceList.Find(f => f.Code == item.ExtraService) != null ? extraPriceList.Find(f => f.Code == item.ExtraService).Name : string.Empty;
                    zoneSeat.IsExit = item.IsExit;
                    zoneSeat.thisReservation = thisReservationSeats.Where(w => w.SeatLetter == item.SeatLetter && w.SeatNo == item.SeatNo).Count() > 0 || thisReserveSeats.Where(w => w.SeatLetter == item.SeatLetter && w.SeatNo == item.SeatNo).Count() > 0;

                    if (isChild)
                    {
                        if (childParents.Where(w => w.SeatNo == item.SeatNo).Count() > 0 && !(item.IsExit.HasValue && item.IsExit.Value))
                        {
                            zoneSeat.isFull = (seatCInList.Find(f => f.SeatLetter == item.SeatLetter && f.SeatNo == item.SeatNo) != null) || (notExists && item.IsExit == true);
                        }
                        else
                        {
                            zoneSeat.isFull = true;
                        }
                    }
                    else
                    {
                        zoneSeat.isFull = (seatCInList.Find(f => f.SeatLetter == item.SeatLetter && f.SeatNo == item.SeatNo) != null) || (notExists && item.IsExit == true);
                    }

                    if (thisReserveSeats.Where(w => w.SeatLetter == item.SeatLetter && w.SeatNo == item.SeatNo).Count() > 0)
                    {
                        zoneSeat.isFull = true;
                    }

                    ServiceExtRecord extraService = extraServiceList.Find(f => f.Service == "FLIGHT" && f.Code == item.ExtraService);

                    foreach (var rSSR in ResData.ResCon.Where(w => w.CustNo == CustNo && w.ServiceID == ServiceID && (!string.IsNullOrEmpty(w.SpecSerRQCode1) || !string.IsNullOrEmpty(w.SpecSerRQCode2))))
                    {
                        if (!string.IsNullOrEmpty(rSSR.SpecSerRQCode1))
                        {
                            SSRQCodeRecord ssrq = ssrqCodes.Find(f => f.Code == rSSR.SpecSerRQCode1); //new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, rSSR.SpecSerRQCode1, ref errorMsg);
                            if (ssrq != null && ssrq.NotAllowPlaneExitSeat.HasValue && ssrq.NotAllowPlaneExitSeat.Value == true && zoneSeat.IsExit.HasValue && zoneSeat.IsExit.Value)
                                zoneSeat.isFull = true;
                        }
                        if (!string.IsNullOrEmpty(rSSR.SpecSerRQCode2))
                        {
                            SSRQCodeRecord ssrq = ssrqCodes.Find(f => f.Code == rSSR.SpecSerRQCode2); //new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, rSSR.SpecSerRQCode2, ref errorMsg);
                            if (ssrq != null && ssrq.NotAllowPlaneExitSeat.HasValue && ssrq.NotAllowPlaneExitSeat.Value == true && zoneSeat.IsExit.HasValue && zoneSeat.IsExit.Value)
                                zoneSeat.isFull = true;
                        }
                    }

                    if (extraService != null)
                    {
                        if (!string.IsNullOrEmpty(extraService.SpecSerRQCode))
                        {
                            SSRQCodeRecord ssrq = ssrqCodes.Find(f => f.Code == extraService.SpecSerRQCode); //new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, extraService.SpecSerRQCode, ref errorMsg);
                            if (ssrq != null && ssrq.NotAllowPlaneExitSeat.HasValue && ssrq.NotAllowPlaneExitSeat.Value == true)
                                zoneSeat.isFull = true;
                        }
                        if (CustNo.HasValue)
                        {
                            ResConRecord custResCon = ResData.ResCon.Find(f => f.ServiceID == ServiceID && f.CustNo == CustNo);
                            if (custResCon != null && (string.IsNullOrEmpty(custResCon.SpecSerRQCode1) || string.IsNullOrEmpty(custResCon.SpecSerRQCode2)))
                            {
                                SSRQCodeRecord ssrq = ssrqCodes.Find(f => f.Code == custResCon.SpecSerRQCode1);  //new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, custResCon.SpecSerRQCode1, ref errorMsg);
                                if (ssrq != null && ssrq.NotAllowPlaneExitSeat.HasValue && ssrq.NotAllowPlaneExitSeat.Value == true)
                                    zoneSeat.isFull = true;
                                ssrq = ssrqCodes.Find(f => f.Code == custResCon.SpecSerRQCode2);  //new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, custResCon.SpecSerRQCode2, ref errorMsg);
                                if (ssrq != null && ssrq.NotAllowPlaneExitSeat.HasValue && ssrq.NotAllowPlaneExitSeat.Value == true)
                                    zoneSeat.isFull = true;
                            }
                        }
                        else
                            if (custSSR.Where(w => !string.IsNullOrEmpty(w.SpecSerRQCode1) || !string.IsNullOrEmpty(w.SpecSerRQCode2)).Count() > 0)
                            {
                                foreach (var r in custSSR)
                                {
                                    ResConRecord custResCon = ResData.ResCon.Find(f => f.ServiceID == ServiceID && f.CustNo == r.CustNo);
                                    if (custResCon != null && (string.IsNullOrEmpty(custResCon.SpecSerRQCode1) || string.IsNullOrEmpty(custResCon.SpecSerRQCode2)))
                                    {
                                        SSRQCodeRecord ssrq = ssrqCodes.Find(f => f.Code == custResCon.SpecSerRQCode1);  //new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, custResCon.SpecSerRQCode1, ref errorMsg);
                                        if (ssrq != null && ssrq.NotAllowPlaneExitSeat.HasValue && ssrq.NotAllowPlaneExitSeat.Value == true)
                                            zoneSeat.isFull = true;
                                        ssrq = ssrqCodes.Find(f => f.Code == custResCon.SpecSerRQCode2);  //new Ssrcs().getSSRQCode(UserData.Market, ResData.ResMain.PLMarket, custResCon.SpecSerRQCode2, ref errorMsg);
                                        if (ssrq != null && ssrq.NotAllowPlaneExitSeat.HasValue && ssrq.NotAllowPlaneExitSeat.Value == true)
                                            zoneSeat.isFull = true;
                                    }
                                }
                            }

                        if (seatExtra.Where(w => w.extraService == extraService.Code && w.allotIsFull == true).Count() > 0)
                        {
                            zoneSeat.isFull = true;
                        }
                    }

                    zoneSeatList.Add(zoneSeat);
                }
                row.MaxRow = Conversion.getInt32OrNull(planeTZI.Where(w => w.ZoneId == row.RecID).LastOrDefault().ItemRow + 1);
                row.MaxCol = Conversion.getInt32OrNull(planeTZI.Where(w => w.ZoneId == row.RecID).LastOrDefault().ItemCol + 1);
                var zoneseatlist = (from q in zoneSeatList
                                    orderby q.ItemCol, q.ItemRow
                                    select q).ToList<PlaneTempZoneItems>();
                row.ZoneItems = zoneseatlist;
                row.SeatWidth = (seatWidth.HasValue ? (seatWidth.Value * 26) / (row.ItemLength.HasValue ? row.ItemLength.Value : seatWidth.Value) : 26);
            }

            object selectedSeat = null;

            if (CustNo.HasValue)
            {
                FlightCIn seatCheckIn = seatCInList.Find(f => f.CustNo == CustNo && f.ServiceID == ServiceID);
                if (seatCheckIn != null)
                {
                    selectedSeat = getSeatCust(seatCheckIn.SeatLetter + "_" + seatCheckIn.SeatNo.ToString(), CustNo, ServiceID, string.Empty);
                }
            }
            return new
            {
                Success = true,
                Message = "",
                Plane = planeT,
                PlaneZone = planeTZ,
                ColCount = planeTZI.OrderBy(o => o.ItemCol).LastOrDefault().ItemCol + 1,
                RowCount = planeTZI.OrderBy(o => o.ItemRow).LastOrDefault().ItemRow + 1,
                SeatPlan = seatPlan,
                SelectedSeat = selectedSeat
            };
        }
        catch (Exception Ex)
        {
            return new
            {
                Success = false,
                Message = Ex.Message
            };
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getCustomers(int? ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();

        List<ResServiceRecord> resServiceList = ResData.ResService;

        ResServiceRecord resFlight = resServiceList.Find(f => string.Equals(f.ServiceType, "FLIGHT") && f.RecID == ServiceID);

        if (resFlight != null)
        {
            FlightDayRecord flightDetail = new Flights().getFlightDay(UserData, resFlight.Service, resFlight.BegDate.Value, ref errorMsg);
            if (resFlight != null)
            {
                List<FlightCIn> seatUse = new FlightSeatCheckIn().getFlightSeatCIn(UserData, resFlight.Service, resFlight.BegDate.Value, ref errorMsg);
                var flightCustomers = from C in ResData.ResCust
                                      join rC in ResData.ResCon on C.CustNo equals rC.CustNo
                                      where rC.ServiceID == resFlight.RecID
                                      select new { C.CustNo, C.TitleStr, C.Surname, C.Name, C.Title };

                if (flightCustomers.Count() > 0)
                {
                    sb.AppendFormat("<span id=\"{1}\" name=\"buses\" class=\"transport\">{0}</span><br />",
                        flightDetail.FlightNo + ", " + (resFlight.BegDate.HasValue ? resFlight.BegDate.Value.ToShortDateString() : "") + ", (" + flightDetail.DepAirport + " -> " + flightDetail.ArrAirport + ")",
                        resFlight.Service + resFlight.BegDate.Value.Ticks.ToString());
                    List<FlightSeatCustomerList> flightCustList = new List<FlightSeatCustomerList>();
                    foreach (var r1 in flightCustomers)
                    {
                        FlightCIn seat = seatUse.Find(f => f.Flight == resFlight.Service && f.FlightDate == resFlight.BegDate && f.CustNo == r1.CustNo);
                        flightCustList.Add(new FlightSeatCustomerList()
                        {
                            CustNo = r1.CustNo,
                            Title = r1.Title,
                            TitleStr = r1.TitleStr,
                            Name = r1.Name,
                            Surname = r1.Surname,
                            Seat = seat
                        });
                        /*    
                        if (seat == null)
                        {
                            if (r1.Title < 8)
                                sb.AppendFormat("<span class=\"custSelect\" name=\"custs\" onclick=\"selectCustumer({0},{1},this);\"><img alt=\"\" src=\"" + WebRoot.BasePageRoot + "/Images/{3}\" /> {2}</span><br />",
                                    r1.CustNo,
                                    resFlight.RecID,
                                    r1.TitleStr + ". " + r1.Surname + " " + r1.Name,
                                    "nochecked_16.gif");
                            else
                                sb.AppendFormat("<span class=\"cust\"><img alt=\"\" src=\"" + WebRoot.BasePageRoot + "/Images/{1}\" /> {0}</span><br />",
                                    r1.TitleStr + ". " + r1.Surname + " " + r1.Name,
                                    "nochecked_16.gif");
                        }
                        else
                        {
                            string seatNo = string.Empty;
                            //FlightCIn seats = seatUse.Find(w => w.Flight == resFlight.Service && w.FlightDate == resFlight.BegDate && w.CustNo == r1.CustNo);
                            if (seats != null)
                                seatNo += seats.SeatNo.ToString() + seats.SeatLetter;
                            sb.AppendFormat("<span class=\"cust\">{0}</span><br />",
                               "(" + seatNo + ") " + r1.TitleStr + ". " + r1.Surname + " " + r1.Name);
                        }
                        */
                    }
                    return new
                    {
                        ServiceID = resFlight.RecID,
                        BasePageRoot = WebRoot.BasePageRoot,
                        CustList = flightCustList
                    };
                }
            }
        }

        return null;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getSeatCust(string SeatNo, int? CustNo, int? ServiceID, string OldImg)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        List<FlightCIn> selectedSeat = new List<FlightCIn>();
        if (HttpContext.Current.Session["SelectedSeat"] == null)
        {
            HttpContext.Current.Session["SelectedSeat"] = selectedSeat;
        }

        selectedSeat = (List<FlightCIn>)HttpContext.Current.Session["SelectedSeat"];
        FlightCIn resevSeat = null;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        StringBuilder sb = new StringBuilder();
        ResCustRecord cust = ResData.ResCust.Find(f => f.CustNo == CustNo);
        if (cust != null && !string.IsNullOrEmpty(SeatNo))
        {
            string seatLetter = SeatNo.Split('_')[0];
            Int16? seatNo = Conversion.getInt16OrNull(SeatNo.Split('_')[1]);
            List<PlaneTempZoneItems> seatPlan = new FlightSeatCheckIn().getPlaneTempZoneItemsForService(UserData, ServiceID, ref errorMsg);
            PlaneTempZoneItems seat = seatPlan.Find(f => f.SeatLetter == seatLetter && f.SeatNo == seatNo);
            ResServiceRecord flightService = ResData.ResService.Find(f => f.RecID == ServiceID);
            FlightDayRecord flyDay = new Flights().getFlightDay(UserData, flightService.Service, flightService.BegDate.Value, ref errorMsg);
            List<FlightCIn> seatUse = new FlightSeatCheckIn().getFlightSeatCIn(UserData, flightService.Service, flightService.BegDate, ref errorMsg);
            if (seatUse.Where(w => w.CustNo == CustNo).Count() > 0 || seat == null || flightService == null)
            {
                return string.Empty;
            }
            //sb.AppendFormat("<h1>Passenger : {0}</h1><br /><br />", cust.TitleStr + ". " + cust.Surname + " " + cust.Name);
            sb.AppendFormat("<h1>{0}</h1>", seat.SeatNo.ToString() + seat.SeatLetter);
            int? extraServiceID = null;
            if (!string.IsNullOrEmpty(seat.ExtraService))
            {
                decimal price = -1;
                bool onlyTicket = ResData.ResMain.SaleResource.HasValue && (new List<short> { 2, 3, 5 }).Contains(ResData.ResMain.SaleResource.Value);
                List<ExtraServiceListRecord> extPrices = new Reservation().getExtraServicesForFlight(UserData, ResData.ResMain.PLMarket, DateTime.Today, ResData.ResMain.SaleCur, flightService.FlgClass, flightService.BegDate, flightService.Duration, false, flightService.Service, onlyTicket, string.Empty, true, ref errorMsg);
                if (extPrices != null)
                {
                    ExtraServiceListRecord extPrice = extPrices.Find(f => (string.IsNullOrEmpty(f.ServiceItem) || f.ServiceItem == flightService.Service) && f.Code == seat.ExtraService);
                    if (extPrice != null)
                    {
                        if (cust.Title < 6)
                        {
                            price = extPrice.SaleAdlPrice.HasValue ? extPrice.SaleAdlPrice.Value : -1;
                        }
                        else
                            if (cust.Title > 5 && cust.Title < 8)
                            {
                                price = extPrice.SaleChdPrice.HasValue ? extPrice.SaleChdPrice.Value : (extPrice.SaleAdlPrice.HasValue ? extPrice.SaleAdlPrice.Value : -1);
                            }
                            else
                            {
                                price = extPrice.SaleInfPrice.HasValue ? extPrice.SaleInfPrice.Value : (extPrice.SaleAdlPrice.HasValue ? extPrice.SaleAdlPrice.Value : -1);
                            }
                        if (price > -1)
                        {
                            sb.AppendFormat("{0} {1}", extPrice.Description, price.ToString("#,###.00") + " " + ResData.ResMain.SaleCur);
                        }
                        extraServiceID = extPrice.RecID;
                    }
                }
            }

            resevSeat = selectedSeat.Find(f => f.CustNo == CustNo);
            if (resevSeat != null)
            {
                selectedSeat.Remove(resevSeat);
            }

            selectedSeat.Add(new FlightCIn()
            {
                CustNo = CustNo,
                Flight = flightService.Service,
                FlightDate = flightService.BegDate,
                FlightDay = flyDay.RecID,
                FlightTemp = seat.PlaneID,
                SeatLetter = seatLetter,
                SeatNo = seatNo,
                ServiceExtID = extraServiceID,
                ServiceID = flightService.RecID,
                OldSeatImg = OldImg
            });

            HttpContext.Current.Session["SelectedSeat"] = selectedSeat;

            //sb.AppendFormat("<input type=\"button\" value=\"{0}\" onclick=\"saveSeat('{1}',{2},{3});\" />",
            //    HttpContext.GetGlobalResourceObject("LibraryResource", "btnSave"),
            //    seat.SeatLetter + "_" + seat.SeatNo.ToString(),
            //    CustNo,
            //    flightService.RecID);
        }
        return new
        {
            OldSeat = resevSeat,
            NewSeat = sb.ToString()
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string saveSeat(int? ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        if (HttpContext.Current.Session["SelectedSeat"] == null)
        {
            return "NO";
        }
        List<FlightCIn> selectedSeat = (List<FlightCIn>)HttpContext.Current.Session["SelectedSeat"];
        if (selectedSeat.Count == 0)
        {
            return "Please select seat.";
        }

        ResServiceRecord flightService = ResData.ResService.Find(f => f.RecID == ServiceID);
        if (flightService == null)
        {
            return "Service not found.";
        }

        DateTime flyDate = flightService.BegDate.Value;
        FlightDayRecord flyDay = new Flights().getFlightDay(UserData, flightService.Service, flightService.BegDate.Value, ref errorMsg);
        if (flyDay == null)
        {
            return "Flight day record not found.";
        }

        List<ReservastionSaveErrorRecord> returnData = new List<ReservastionSaveErrorRecord>();

        returnData = new FlightSeatCheckIn().saveUserSeats(UserData, ref ResData, selectedSeat, flightService, ref errorMsg);
        if(string.IsNullOrEmpty(errorMsg))
        {
            int seat = 0;
            foreach (var s in selectedSeat)
            {
                //tax zımbırtısı da flighata ait bir servis olmasından dolayı burada sıkıntı var
                s.ServiceExtID = ResData.ResServiceExt.Where(w => w.ServiceID == s.ServiceID && w.ServiceType == "FLIGHT" && w.RecStatus== RecordStatus.New).Skip(seat).FirstOrDefault().RecordID;
               new FlightSeatCheckIn().UpdateSeatCheckIn(UserData, s, ref errorMsg);
               seat++;
            }
        }

        if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
        {
            HttpContext.Current.Session["ResData"] = ResData;
            return "OK";
        }
        else
        {
            string Msg = string.Empty;
            var errorMsgGrp = from q in returnData
                              where !q.ControlOK
                              group q by new { q.Message } into g
                              select new { Message = g.Key.Message };
            foreach (var row in errorMsgGrp)
            {
                Msg += row.Message + "\n";
            }
            return Msg;
        }
    }
}

public class novaSeatPlan
{
    public string extraService { get; set; }
    public bool allotIsFull { get; set; }
    public novaSeatPlan()
    {
        this.allotIsFull = false;
    }
}
