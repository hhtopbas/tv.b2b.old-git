﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSView_Hotel.aspx.cs" Inherits="Controls_RSView_Hotel"
    EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <title><%= GetGlobalResourceObject("PageTitle", "RSViewHotel") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <link href="../CSS/ServiceEditView.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function onLoad() {
            $.query = $.query.load(location.href);
            var resNo = $.query.get('ResNo');
            var recID = $.query.get('RecID');
            $.ajax({
                type: "POST",
                url: "../Services/ReservationServices.asmx/getResService",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    var resService = $.json.decode(msg.d);                    
                    $("#iCheckIn").html(resService.BegDate);
                    $("#iCheckOut").html(resService.EndDate)
                    $("#sLocation").html(resService.DepLocationNameL);
                    $("#sHotel").html(resService.ServiceNameL);
                    $("#iNight").html(resService.Duration);
                    $("#sRoom").html(resService.RoomNameL);
                    $("#sAccom").html(resService.AccomNameL);
                    $("#sBoard").html(resService.BoardNameL);
                    $("#iAdult").html(resService.Adult);
                    $("#iChild").html(resService.Child);
                    if (resService.StatConfNameL != '')
                        $("#sConfirmation").html(resService.StatConfNameL);
                    else $("#divConfirmation").hide();
                    $("#sStatus").html(resService.StatSerNameL);
                    var salePrice = resService.IncPack == 'Y' ? '<%= GetGlobalResourceObject("LibraryResource", "InPackage").ToString() %>' : resService.SalePrice + ' ' + resService.SaleCur;
                    $("#iSalePrice").html(salePrice);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }
    </script>

</head>
<body onload="onLoad()">
    <form id="formRsHotel" runat="server">
    <div id="divRsHotel">
        <div>
            <table>
                <tr>
                    <td class="leftCell">
                        <div id="divLocation" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewLocation") %></span>
                            </div>
                            <div id="divSLocation">
                                <b><span id="sLocation"></span></b>
                            </div>
                        </div>
                        <div id="divHotel" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewHotel") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="sHotel"></span></b>
                            </div>
                        </div>
                        <div id="divChekInOut" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewCheckInOut") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iCheckIn"></span>&nbsp;-&nbsp; <span id="iCheckOut"></span></b>
                            </div>
                        </div>
                        <div id="divNight" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewNight") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iNight"></span></b>
                            </div>
                        </div>
                        <div id="divRoom" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewRoom") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="sRoom"></span></b>
                            </div>
                        </div>
                        <div id="divAccom" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAccom") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="sAccom"></span></b>
                            </div>
                        </div>
                        <div id="divBoard" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewBoard") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="sBoard"></span></b>
                            </div>
                        </div>
                        <div id="divAdultChild" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAdult") %></span>
                            </div>
                            <div class="divAdultChild">
                                <b><span id="iAdult"></span></b>
                            </div>
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewChild") %></span>
                            </div>
                            <div class="divAdultChild">
                                <b><span id="iChild"></span></b>
                            </div>
                        </div>
                        <%--<div id="divSupplier" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplier") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iSupplier"></span></b>
                            </div>
                        </div>
                        <div id="divSupplierNote" class="divNotes">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iSupplierNote"></span></b>
                            </div>
                        </div>--%>
                    </td>
                    <td class="rightCell">
                        <div id="divStatus">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewStatus") %></span>
                            <br />
                            <b><span id="sStatus"></span></b>
                        </div>
                        <br />
                        <div id="divConfirmation">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewConfirmation") %></span>
                            <br />
                            <b><span id="sConfirmation"></span></b>
                        </div>
                        <br />
                        <div id="divSalePrice">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewSalePrice") %></span>
                            <br />
                            <span class="salePrice" id="iSalePrice"></span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
