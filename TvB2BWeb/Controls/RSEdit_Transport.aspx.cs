﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSEdit_Transport : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {
            //sService.Attributes.Add("onclick", "changeService();");
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getService(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? recID = Conversion.getInt32OrNull(RecID);        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        List<TransportRecord> serviceList = new Transports().getTransport(UserData.Market, UserData.Market, string.Empty, resService.DepLocation, resService.ArrLocation, ref errorMsg);
        var query = from q in serviceList
                    select new { Code = q.Code, Name = q.NameL };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod(EnableSession = true)]
    public static string getDeparture(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        if (resService == null) return string.Empty;
        TransportRecord transport = new Transports().getTransport(UserData.Market, ResData.ResMain.PLMarket, resService.Service, ref errorMsg);
        if (transport == null) return string.Empty;
        List<TransportRouteRecord> routes = new Transports().getTransportRoute(resService.Service, ref errorMsg);
        List<TvBo.Location> locations = new Locations().getLocationList(UserData.Market, LocationType.None, null, null, null, null, ref errorMsg);
        var query = from q in routes
                    join q1 in locations on q.Location equals q1.RecID
                    select new { Code = q1.RecID, Name = q1.NameL };
        
        if (query.Count() > 0 && transport.DepRet == 0)
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        else return "[{\"Code\":\"" + resService.DepLocation + "\",\"Name\":\"" + resService.DepLocationNameL + "\"}]";
        
        
    }

    [WebMethod(EnableSession = true)]
    public static string getArrival(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        if (resService == null) return string.Empty;
        TransportRecord transport = new Transports().getTransport(UserData.Market, ResData.ResMain.PLMarket, resService.Service, ref errorMsg);
        if (transport == null) return string.Empty;
        List<TransportRouteRecord> routes = new Transports().getTransportRoute(resService.Service, ref errorMsg);
        List<TvBo.Location> locations = new Locations().getLocationList(UserData.Market, LocationType.None, null, null, null, null, ref errorMsg);
        var query = from q in routes
                    join q1 in locations on q.Location equals q1.RecID
                    select new { Code = q1.RecID, Name = q1.NameL };

        if (query.Count() > 0 && transport.DepRet == 1)
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        else return "[{\"Code\":\"" + resService.ArrLocation + "\",\"Name\":\"" + resService.ArrLocationNameL + "\"}]";
    }

    [WebMethod(EnableSession = true)]
    public static string getBus(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? recID = Conversion.getInt32OrNull(RecID);        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        List<BusRecord> serviceList = new Transports().getBusList(UserData.Market, resService.DepLocation, resService.ArrLocation, null, ref errorMsg);
        var query = from q in serviceList
                    select new { Code = q.Code, Name = q.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod(EnableSession = true)]
    public static string getPickups(string RecID, string Departure, string Arrival)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        int? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord service = ResData.ResService.Find(f => f.RecID == recID);
        string errorMsg = string.Empty;
        
        int? departure = Conversion.getInt32OrNull(Departure);
        int? arrival = Conversion.getInt32OrNull(Arrival);
        TvBo.TransportRecord transport = new Transports().getTransport(UserData.Market, ResData.ResMain.PLMarket, service.Service, ref errorMsg);

        List<TransportStatRecord> serviceList = new Transports().getPickups(UserData.Market, service.Service, transport.DepRet == 0 ? departure : arrival, ref errorMsg);
        var query = from q in serviceList
                    select new { RecID = q.Location, Name = q.LocationLocalName };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod(EnableSession = true)]
    public static string getServiceDetail(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        Int32? recID = Conversion.getInt32OrNull(RecID);
        if (!recID.HasValue) return string.Empty;
        var query = from q in ResData.ResService
                    where q.RecID == recID.Value
                    select new
                    {
                        BegDate = q.BegDate.Value.ToShortDateString(),
                        EndDate = q.EndDate.Value.ToShortDateString(),
                        DepLocation = q.DepLocation,
                        ArrLocation = q.ArrLocation,
                        Service = q.Service,
                        Bus = q.Bus,
                        BusName = q.BusName,
                        Pickup = q.Pickup,
                        PickupName = q.PickupName,
                        PickupNameL = q.PickupNameL,
                        Adult = q.Adult,
                        Child = q.Child,
                        Unit = q.Unit,
                        Duration = q.Duration,
                        Supplier = q.SupplierName,
                        SupNote = q.SupNote,
                        StatConf = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ? q.StatConfNameL : string.Empty,
                        StatSer = q.StatSerNameL,
                        SalePrice = !Equals(q.IncPack, "Y") ? q.SalePrice.Value.ToString("#,###.00") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        SaleCur = !Equals(q.IncPack, "Y") ? q.SaleCur : ""
                    };

        if (query == null || query.Count() < 1) return string.Empty;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query.FirstOrDefault());
    }

    [WebMethod(EnableSession = true)]
    public static string CalcService(string RecID, string Pickup, string Service, string Bus, bool? newRes, string Departure, string Arrival)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        int? recID = Conversion.getInt32OrNull(RecID);
        int? pickup = Conversion.getInt32OrNull(Pickup);        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        int? departure = Conversion.getInt32OrNull(Departure);
        int? arrival = Conversion.getInt32OrNull(Arrival);
        List<TvBo.Location> locations = new Locations().getLocationList(UserData.Market, LocationType.None, null, null, null, null, ref errorMsg);
        TvBo.Location pickupLoc = new Locations().getLocation(UserData.Market, pickup, ref errorMsg);
        TvBo.BusRecord bus = new Transports().getBusRecord(UserData.Market, Bus, ref errorMsg);
        resService.PriceSource = !Equals(resService.Service, Service) || !Equals(resService.Bus, Bus) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Service;
        resService.Bus = Bus;
        resService.BusName = bus != null ? bus.Name : "";
        resService.BusName = "";
        resService.Pickup = pickup;
        resService.PickupName = pickupLoc != null ? pickupLoc.Name : "";
        resService.PickupNameL = pickupLoc != null ? pickupLoc.NameL : "";
        if (departure.HasValue)
        {
            resService.DepLocation = departure.Value;
            resService.DepLocationName = locations.Find(f => f.RecID == departure.Value).Name;
            resService.DepLocationNameL = locations.Find(f => f.RecID == departure.Value).NameL;
        }
        if (arrival.HasValue)
        {
            resService.ArrLocation = arrival.Value;
            resService.ArrLocationName = locations.Find(f => f.RecID == arrival.Value).Name;
            resService.ArrLocationNameL = locations.Find(f => f.RecID == arrival.Value).NameL;
        }
        resService.Supplier = string.Empty;

        ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
        if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            resService = ResData.ResService.Find(f => f.RecID == recID.Value);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
            string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                        SalePriceStr,
                        supplierRec.NameL);
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string SaveService(string RecID, string Pickup, string Service, string Bus, bool? newRes, string Departure, string Arrival)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        int? pickup = Conversion.getInt32OrNull(Pickup);        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        int? departure = Conversion.getInt32OrNull(Departure);
        int? arrival = Conversion.getInt32OrNull(Arrival);
        List<TvBo.Location> locations = new Locations().getLocationList(UserData.Market, LocationType.None, null, null, null, null, ref errorMsg);
        TvBo.Location pickupLoc = new Locations().getLocation(UserData.Market, pickup, ref errorMsg);
        TvBo.BusRecord bus = new Transports().getBusRecord(UserData.Market, Bus, ref errorMsg);
        TvBo.TransportDetailRecord transport = new Transports().getTransportDetail(UserData.Market, ResData.ResMain.PLMarket, Service, Bus, resService.BegDate.Value, ref errorMsg);
        resService.PriceSource = !Equals(resService.Service, Service) || !Equals(resService.Bus, Bus) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Service;
        resService.Bus = Bus;        
        resService.Pickup = pickup;        
        resService.Supplier = string.Empty;
        resService.ServiceName = transport.Name;
        resService.ServiceNameL = transport.NameL;
        if (departure.HasValue)
        {
            resService.DepLocation = departure.Value;
            resService.DepLocationName = locations.Find(f => f.RecID == departure.Value).Name;
            resService.DepLocationNameL = locations.Find(f => f.RecID == departure.Value).NameL;
        }
        if (arrival.HasValue)
        {
            resService.ArrLocation = arrival.Value;
            resService.ArrLocationName = locations.Find(f => f.RecID == arrival.Value).Name;
            resService.ArrLocationNameL = locations.Find(f => f.RecID == arrival.Value).NameL;
        }

        if (newRes.HasValue && newRes.Value)
        {
            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
            if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                            SalePriceStr,
                            supplierRec.NameL);
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        else
        {
            Int32 ErrCode = 0;
            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
            List<int?> recIDList = new List<int?>();
            recIDList.Add(resService.RecID);
            ResData = new TvBo.Reservation().editService(UserData, ResData, recIDList, ref ErrCode, ref errorMsg, false);
            if (ErrCode != 0)
            {
                if (ErrCode < 0)
                    errorMsg = string.IsNullOrEmpty(errorMsg) ? "Bilinmeyen hata" : errorMsg;
                else errorMsg = new ResCalcError().calcError(ErrCode, ResData).ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                            SalePriceStr,
                            supplierRec.NameL);
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getChgFee(string RecID, string Pickup, string Service, string Bus)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        resService.Service = Service;        
        ResChgFeePrice chgPrice = new Reservation().whatHasChangedResService(UserData, ResData, recID, resService, ref errorMsg);
        if (chgPrice == null)
            return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        else
        {
            string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                             chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
            return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
        }
    }
}
