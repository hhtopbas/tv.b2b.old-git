﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using TvBo;
using System.Web.Services;
using System.Text;

public partial class Controls_ExcursionInfo : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.Params["Code"]) && !string.IsNullOrEmpty(Request["Date"]))
            {
                DateTime Date = new DateTime(Convert.ToInt64(Request["Date"]));
                string Code = (string)Request.Params["Code"];

                Response.Write(getExcursionInfo(Code, Date));
            }
        }
    }

    protected string getExcursionInfo(string Code, DateTime? Date)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        return "<div style=\"font-size: 10pt;\">" + new Excursions().getExcursionMedia(UserData, Code, Date.Value, ref errorMsg) + "</div>";
    }
}
