﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using TvBo;
using System.Web.Services;
using System.Text;

public partial class Controls_BoardConsept : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; 
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.Params["Hotel"]) && !string.IsNullOrEmpty(Request.Params["Board"])&& !string.IsNullOrEmpty(Request["Date"]))
            {
                DateTime Date = new DateTime(Convert.ToInt64(Request["Date"]));
                string Hotel = (string)Request.Params["Hotel"];
                string Board = (string)Request.Params["Board"];
                Response.Write(getBoardConsept(Hotel, Board, Date));
            }
        }
    }
    
    protected string getBoardConsept(string Hotel, string Board, DateTime Date)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retval = string.Empty;
        retval = new TvBo.Hotels().getBoardConseptInfo(UserData.Market, Hotel, Board, Date, ref errorMsg);
        return retval;           
    }
}
