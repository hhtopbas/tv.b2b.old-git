﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSEdit_Hotel : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; 
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
    }

    [WebMethod(EnableSession=true)]
    public static string getServiceDetail(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);        
        if (!recID.HasValue) return string.Empty;
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, resService.Service, ref errorMsg);
        var query = from q in ResData.ResService                    
                    where q.RecID == recID.Value
                    select new
                    {
                        Location = hotel.Location,
                        LocationName = hotel.LocationLocalName,
                        BegDate = q.BegDate.Value.ToShortDateString(),
                        EndDate = q.EndDate.Value.ToShortDateString(),
                        Service = q.Service,
                        Room = q.Room,
                        Board = q.Board,
                        Accom = q.Accom,
                        Adult = q.Adult,
                        Child = q.Child,
                        Unit = q.Unit,
                        Night = q.Duration,
                        Supplier = q.SupplierName,
                        SupNote = q.SupNote,
                        StatConf = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ? q.StatConfNameL : string.Empty,
                        StatSer = q.StatSerNameL,
                        PnrNo = q.PnrNo,
                        SalePrice = !Equals(q.IncPack, "Y") ? q.SalePrice.Value.ToString("#,###.00") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        SaleCur = !Equals(q.IncPack, "Y") ? q.SaleCur : ""
                    };
        if (query == null || query.Count() < 1) return string.Empty;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query.FirstOrDefault());
    }

    [WebMethod(EnableSession = true)]
    public static string getLocationList(int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        bool forPackage = string.Equals(ResData.ResMain.PackType, "H");
        string errorMsg = string.Empty;        
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        int? CatPackID = resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;
        List<TvBo.CodeName> retval = new TvBo.Hotels().getHotelLocationCodeName(UserData, ResData.ResMain.ArrCity, null, CatPackID, resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0 && string.Equals(resService.IncPack, "Y") && forPackage ? new Locations().getLocationForCity(resService.DepLocation.Value, ref errorMsg) : null, ResData.ResMain.PackType, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(retval);
    }

    [WebMethod(EnableSession = true)]
    public static string getLocations(string ResNo, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        retval = new TvBo.Hotels().getHotelLocationJSon(UserData.Market, ResData, recID, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getHotels(int? Location, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; 
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? hotelLocation = Location;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);

        string errorMsg = string.Empty;
        string retval = string.Empty;
        int? CatPackID = resService != null && resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;
        List<TvBo.CodeName> hotelList = new TvBo.Hotels().getLocationHotelCodeName(UserData.Market, hotelLocation.HasValue ? hotelLocation.Value : 0, CatPackID, ref errorMsg);
        var retVal = from q in hotelList                     
                     group q by new { q.Code, q.Name } into k
                     select new { k.Key.Code, k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    [WebMethod(EnableSession = true)]
    public static string getRooms(string Hotel, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);

        string errorMsg = string.Empty;
        string retval = string.Empty;
        int? CatPackID = resService != null && resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;

        retval = new TvBo.Hotels().getRoomJSon(UserData.Market, Hotel, CatPackID, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getAccoms(string Hotel, string Room, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);

        string errorMsg = string.Empty;
        string retval = string.Empty;
        int? CatPackID = resService != null && resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;

        retval = new TvBo.Hotels().getAccomJSon(UserData.Market, Hotel, Room, CatPackID, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getBoards(string Hotel, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);

        string errorMsg = string.Empty;
        string retval = string.Empty;
        int? CatPackID = resService != null && resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;

        retval = new TvBo.Hotels().getBoardJSon(UserData.Market, Hotel, CatPackID, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string CalcService(string RecID, string Hotel, string Room, string Accom, string Board, bool? newRes)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        TvBo.HotelRecord HotelRec = new Hotels().getHotelDetail(UserData, Hotel, ref errorMsg);        
        TvBo.HotelRoomRecord HotelRoom = new Hotels().getHotelRoom(UserData.Market, Hotel, Room, ref errorMsg);
        TvBo.HotelAccomRecord HotelAccom = new Hotels().getHotelAccom(UserData.Market, Hotel, Room, Accom, ref errorMsg);
        TvBo.HotelBoardRecord HotelBoard = new Hotels().getHotelBoard(UserData.Market, Hotel, Board, ref errorMsg);

        Int16 ErrorCode = 0;
        if (HotelAccom == null) ErrorCode = 1; // Hotel accommodation not found.        
        if (HotelAccom.MinAdl > resService.Adult) ErrorCode = 2; // Servisi alacak olan yetişkin sayısı, odada kalacak en az yetişkin sayısından büyük.        
        if (HotelAccom.MaxAdl < resService.Adult) ErrorCode = 3; // Servisi alacak olan yetişkin sayısı, odada kalacak yetişkin sayısından fazla.        
        if (HotelAccom.MaxChd < resService.Child) ErrorCode = 4; // Servisi alacak olan çocuk sayısı, odada kalacak çocuk sayısından fazla.
        if (HotelAccom.MaxPax < (resService.Adult.Value + resService.Child.Value)) ErrorCode = 5; // Servisi alacak olan pax sayısı, odada kalacak pax sayısından fazla.
        if (ErrorCode > 0)
        {
            if (ErrorCode == 1)
                errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomNotFound").ToString();
            else errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();

            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            return "{" + retVal + "}";
        }
        resService.PriceSource = resService.StepNo.HasValue || resService.CatPRecNo.HasValue ? 1 : (!Equals(resService.Service, Hotel) || !Equals(resService.Room, Room) || !Equals(resService.Accom, Accom) || !Equals(resService.Board, Board) ? 0 : resService.PriceSource);
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Hotel;
        resService.Room = Room;
        resService.Accom = Accom;
        resService.Board = Board;
        resService.Supplier = string.Empty;        

        resService.ServiceName = HotelRec.Name + "(" + HotelRec.Category + ")";
        resService.ServiceNameL = HotelRec.LocalName + "(" + HotelRec.Category + ")";
        resService.RoomName = HotelRoom.Name;
        resService.RoomNameL = HotelRoom.NameL;
        resService.AccomName = HotelAccom.Name;
        resService.AccomNameL = HotelAccom.LocalName;
        resService.BoardName = HotelBoard.Name;
        resService.BoardNameL = HotelBoard.NameL;
        resService.Supplier = string.Empty;
        resService.DepLocation = HotelRec.Location;
        resService.DepLocationName = HotelRec.LocationName;
        resService.DepLocationNameL = HotelRec.LocationLocalName;

        ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
        if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            resService = ResData.ResService.Find(f => f.RecID == recID.Value);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
            string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                        SalePriceStr,
                        supplierRec.NameL);
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string SaveService(string RecID, string Hotel, string Room, string Accom, string Board, bool? newRes)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);        
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<ResServiceRecord> resServiceList = ResData.ResService;
        TvBo.ResServiceRecord resService = resServiceList.Find(f => f.RecID == recID);
        TvBo.HotelRecord HotelRec = new Hotels().getHotelDetail(UserData, Hotel, ref errorMsg);
        TvBo.HotelRoomRecord HotelRoom = new Hotels().getHotelRoom(UserData.Market, Hotel, Room, ref errorMsg);
        TvBo.HotelAccomRecord HotelAccom = new Hotels().getHotelAccom(UserData.Market, Hotel, Room, Accom, ref errorMsg);
        TvBo.HotelBoardRecord HotelBoard = new Hotels().getHotelBoard(UserData.Market, Hotel, Board, ref errorMsg);

        Int16 ErrorCode = 0;
        if (HotelAccom == null) ErrorCode = 1; // Hotel accommodation not found.        
        if (HotelAccom.MinAdl > resService.Adult) ErrorCode = 2; // Servisi alacak olan yetişkin sayısı, odada kalacak en az yetişkin sayısından büyük.        
        if (HotelAccom.MaxAdl < resService.Adult) ErrorCode = 3; // Servisi alacak olan yetişkin sayısı, odada kalacak yetişkin sayısından fazla.        
        if (HotelAccom.MaxChd < resService.Child) ErrorCode = 4; // Servisi alacak olan çocuk sayısı, odada kalacak çocuk sayısından fazla.
        if (HotelAccom.MaxPax < (resService.Adult.Value + resService.Child.Value)) ErrorCode = 5; // Servisi alacak olan pax sayısı, odada kalacak pax sayısından fazla.
        if (ErrorCode > 0)
        {
            if (ErrorCode == 1)
                errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomNotFound").ToString();
            else errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();

            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            return "{" + retVal + "}";
        }
        resService.PriceSource = resService.StepNo.HasValue || resService.CatPRecNo.HasValue ? 1 : (!Equals(resService.Service, Hotel) || !Equals(resService.Room, Room) || !Equals(resService.Accom, Accom) || !Equals(resService.Board, Board) ? 0 : resService.PriceSource);
            //!Equals(resService.Service, Hotel) || !Equals(resService.Room, Room) || !Equals(resService.Accom, Accom) || !Equals(resService.Board, Board) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Hotel;        
        resService.Room = Room;
        resService.Accom = Accom;
        resService.Board = Board;

        resService.ServiceName = HotelRec.Name + "(" + HotelRec.Category + ")";
        resService.ServiceNameL = HotelRec.LocalName + "(" + HotelRec.Category + ")";
        resService.RoomName = HotelRoom.Name;
        resService.RoomNameL = HotelRoom.NameL;
        resService.AccomName = HotelAccom.Name;
        resService.AccomNameL = HotelAccom.LocalName;
        resService.BoardName = HotelBoard.Name;
        resService.BoardNameL = HotelBoard.NameL;
        resService.Supplier = string.Empty;
        resService.DepLocation = HotelRec.Location;
        resService.DepLocationName = HotelRec.LocationName;
        resService.DepLocationNameL = HotelRec.LocationLocalName;

        if (newRes.HasValue && newRes.Value)
        {
            ResData = new Reservation().extraServiceControl(UserData, ResData, resService, ref errorMsg);
            if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                            SalePriceStr,
                            supplierRec.NameL);
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        else
        {
            Int32 ErrCode = 0;
            ResData = new Reservation().extraServiceControl(UserData, ResData, resService, ref errorMsg);
            List<int?> recIDList = new List<int?>();
            recIDList.Add(resService.RecID);
            ResData = new TvBo.Reservation().editService(UserData, ResData, recIDList, ref ErrCode, ref errorMsg, false);
            if (ErrCode != 0)
            {
                if (ErrCode < 0)
                    errorMsg = string.IsNullOrEmpty(errorMsg) ? "Bilinmeyen hata" : errorMsg;
                else errorMsg = new ResCalcError().calcError(ErrCode, ResData).ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                            SalePriceStr,
                            supplierRec.NameL);
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getChgFee(string RecID, string Hotel, string Room, string Accom, string Board)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        resService.Service = Hotel;
        resService.Room = Room;
        resService.Accom = Accom;
        resService.Board = Board;
        ResChgFeePrice chgPrice = new Reservation().whatHasChangedResService(UserData, ResData, recID, resService, ref errorMsg);
        if (chgPrice == null)
            return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        else
        {
            string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                             chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
            return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
        }
    }
}
