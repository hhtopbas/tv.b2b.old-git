﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddRoomAndAccom.aspx.cs"
    Inherits="Controls_AddRoomAndAccom" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "AddRoomAndAccom")%></title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/AddRoomAndAccom.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .showTourist { display: block; visibility: visible; }
        .hideTourist { display: none; visibility: hidden; }
    </style>

    <script language="javascript" type="text/javascript">

        function getFormData(Pax) {
            $.ajax({
                type: "POST",
                url: "../Controls/AddRoomAndAccom.aspx/getFormData",
                data: '{"PaxCount":"' + Pax + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#selectRoomAccom").html('');
                    if (msg.d == '') {
                        $("#btnNext").attr("disabled", "true");
                        $("#selectRoomAccom").html('<%= GetGlobalResourceObject("LibraryResource","NoPaxNoRoom").ToString() %>');
                    }
                    else $("#selectRoomAccom").html(msg.d);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function nextStep() {
            var roomAccom = $('input[name=rooms]:checked').val();
            var hotel = roomAccom.split(';')[0];
            var room = roomAccom.split(';')[1];
            var accom = roomAccom.split(';')[2];
            var adult = roomAccom.split(';')[3];
            var child = roomAccom.split(';')[4];
            var recID = roomAccom.split(';')[5];
            window.close;
            self.parent.returnAddRoomAccom(hotel, room, accom, adult, child, recID);
        }

        function logout() {
            self.parent.logout();
        }

        function canceled() {
            window.close;
            self.parent.cancelAddARoom();
        }

        function pageLoad() {
            $.query = $.query.load(location.href);
            var Pax = $.query.get('Pax');
            getFormData(Pax);
        }
        
    </script>

</head>
<body onload="javascript:pageLoad();">
    <form id="selectRoomForm" runat="server">
    <div id="gridRoomAccom">
        <div id="selectRoomAccom">
        </div>
        <br />
        <div style="text-align: center;">
            <input id="btnNext" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "sNext")%>'
                onclick="nextStep();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />&nbsp;&nbsp;
            <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel")%>'
                onclick="canceled();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
        </div>
    </div>
    </form>
</body>
</html>
