﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSAdd_Excursion.aspx.cs"
  Inherits="Controls_RSAdd_Excursion" EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
  TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "RSAddExcursion") %></title>
  <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

  <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

  <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

  <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

  <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

  <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

  <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

  <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

  <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="../CSS/RSAddStyle.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
        </style>

  <script language="javascript" type="text/javascript">

    var hfDateID = '<%= hfDate.ClientID%>';

    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
    var addPleaseSelectLocation = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectLocation") %>';
    var addPleaseSelectExcursion = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectExcursion") %>';
    var addPleaseSelectTourist = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>';
    var addPleaseCorrectDate = '<%= GetGlobalResourceObject("Controls", "addPleaseCorrectDate") %>';

    $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    function logout() {
      self.parent.logout();
    }

    function showMessage(msg) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
            }
          }]
        });
      });
    }

    function DateChange(_TextBox, _PopCal) {
      var iDate = document.getElementById(hfDateID);
      var _TextBoxWeek = $("#iDate");
      _TextBoxWeek.value = _TextBox.value;
      if ((!_TextBox) || (!_PopCal)) return
      var _format = _TextBox.getAttribute("Format");
      var _Date = _PopCal.getDate(_TextBox.value, _format);
      if (_Date) {
        iDate.value = Date.parse(_Date);
        getLocation();
      }
    }

    function getLocation() {
      var iDate = document.getElementById(hfDateID);
      var data = new Object();
      data.Type = parseInt($("#sType").val());
      data.ExcurDate = iDate.value;
      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_Excursion.aspx/getLocation",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#sLocation").html('');
          $("#sLocation").append("<option value=''>" + ComboSelect + "</option>");
          if (msg.d != '') {
            var getExc = false;
            $.each($.json.decode(msg.d), function (i) {
              if (this.DefaultLocation == this.RecID) {
                getExc = true;
                $("#sLocation").append("<option value='" + this.RecID + "' selected='selected'>" + this.Name + "</option>");
              }
              else $("#sLocation").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
            });
            if (getExc) getExcursion();
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeLocation() {
      getExcursion();
    }

    function getExcursion() {
      var iDate = document.getElementById(hfDateID);
      var data = new Object();
      data.Location = $("#sLocation").val();
      data.Type = parseInt($("#sType").val());
      data.ExcurDate = iDate.value;
      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_Excursion.aspx/getExcursion",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#sExcursion").html('');
          $("#sExcursion").append("<option value=''>" + ComboSelect + "</option>");
          if (msg.d != '') {
            $.each($.json.decode(msg.d), function (i) {
              $("#sExcursion").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function selectedExcursion(t) {
      var minSaleUnit = $("#minSaleUnit").length > 0 ? parseInt($("#minSaleUnit").val()) : 9999;
      var elm = $(t);
      var checkedCount = $("#ExcPackDetList :checkbox:checked").length;
      if (minSaleUnit < checkedCount) {
        elm.removeAttr("checked");
      }
    }

    function getExcursionDetail() {
      var excur = $("#sExcursion").length > 0 && $("#sExcursion").val() != undefined ? $("#sExcursion").val() : '';
      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_Excursion.aspx/getExcursionDetail",
        data: '{"excursionCode":"' + excur + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#packageDetail").html('');
          if (msg.d != '') {
            $("#packageDetail").html(msg.d);
            $("input[name=ExcPackDet]").change(function () {
              selectedExcursion(this);
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeExcursion() {
      if ($("#divExcPackDetails").show() && $("#divSearchType").show()) {
        getExcursionDetail();
      }
      createTourist();
    }

    function createTourist() {
      var excur = $("#sExcursion").length > 0 && $("#sExcursion").val() != undefined ? $("#sExcursion").val() : '';
      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_Excursion.aspx/getTourist",
        data: '{"excursionCode":"' + excur + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#box1View").html("");
          if (msg.d != '') {
            $.each($.json.decode(msg.d), function (i) {
              $("#box1View").append("<option value='" + this.CustNo + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });

      $(function () {
        $.configureBoxes();
      });
    }

    function preCalc(save) {

      if ($("#minSaleUnit").length > 0) {
        var minSaleUnit = parseInt($("#minSaleUnit").val());
        var checkedCount = $("#ExcPackDetList :checkbox:checked").length;
        if (checkedCount < minSaleUnit) {
          showMessage('You must select ' + minSaleUnit + ' excursions.');
          return;
        }
      }

      if ($("#sLocation").val() == null || $("#sLocation").val() == '') {
        showMessage(addPleaseSelectLocation);
        return;
      }
      if ($("#sExcursion").val() == null || $("#sExcursion").val() == '') {
        showMessage(addPleaseSelectExcursion);
        return;
      }

      var selectCust = '';
      $("#box1View option").each(function () {
        if (selectCust.length > 0) selectCust += "|";
        selectCust += $(this).val();
      });
      if (selectCust.length < 1) {
        showMessage(addPleaseSelectTourist);
        return;
      }
      var begDate = $("#iDate").val();
      var _begDateFormat = $("#iDate").attr("Format");
      if (begDate == '') {
        showMessage(addPleaseCorrectDate);
        return;
      }
      var iDate = document.getElementById(hfDateID);

      // Excursion pakete servislerin seçilerek eklenmesi
      var manuelPackageList = '';
      $.each($("#ExcPackDetList :checkbox:checked"), function (i) {
        var elm = $(this);
        var elmID = elm[0].id.toString().split('_');
        if (manuelPackageList.length > 0) manuelPackageList += ";";
        manuelPackageList += elmID[1];
      });
      // Excursion pakete servislerin seçilerek eklenmesi

      var _data = new Object();
      var _url = "";
      if (save) {
        _url = "../Controls/RSAdd_Excursion.aspx/SaveService";

        _data.selectedCusts = selectCust;
        _data.Location = $("#sLocation").val();
        _data.Excursion = $("#sExcursion").val();
        _data.ExcurDate = iDate.value;
        _data.recordType = $("#recordType").val();
        _data.Type = $("#divSearchType").show() ? parseInt($("#sType").val()) : null;
        _data.ExcList = manuelPackageList;
        _data.VehicleCatID = $("#sVehicleCatID").length > 0 ? $("#sVehicleCatID").val() : null;
        _data.VehicleUnit = $("#sVehicleUnit").length > 0 ? $("#sVehicleUnit").val() : null;        
      }
      else {
        _url = "../Controls/RSAdd_Excursion.aspx/CalcService";

        _data.selectedCusts = selectCust;
        _data.Location = $("#sLocation").val();
        _data.Excursion = $("#sExcursion").val();
        _data.ExcurDate = iDate.value;
        _data.Type = $("#divSearchType").show() ? parseInt($("#sType").val()) : null;
        _data.ExcList = manuelPackageList;
        _data.VehicleCatID = $("#sVehicleCatID").length > 0 ? $("#sVehicleCatID").val() : null;
        _data.VehicleUnit = $("#sVehicleUnit").length > 0 ? $("#sVehicleUnit").val() : null;
      }
      $.ajax({
        type: "POST",
        url: _url,
        data: $.json.encode(_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var result = msg.d;
          if (save) {
            if (result.Calc == true) {
              window.close;
              self.parent.returnAddResServices(true, result.Msg);
            } else {
              showMessage(result.Msg);
            }
          }
          if (msg.d != null) {
            if (result.Calc == true) {
              $("#iSalePrice").text(result.CalcPrice + " " + result.CalcCur);
              $("#iAdult").text(result.Adult);
              $("#iChild").text(result.Child);
              $("#iUnit").text(result.Unit);
            } else showMessage(result.Msg);
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function changeType() {
      getLocation();
      $("#sExcursion").html('');
      if ($("#sType").val() == "3") {
        $("#divExcPackDetails").show();
      }
      else {
        $("#divExcPackDetails").hide();
      }

    }

    function exit(source) {
      if (source == 'save') {
        preCalc(true);
      }
      else
        if (source == 'cancel') {
          window.close;
          self.parent.returnAddResServices(false, '');
        }
    }

    function getFormData() {
      $.ajax({
        async: false,
        type: "POST",
        url: '../Controls/RSAdd_Excursion.aspx/getFormData',
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.d != null) {
            if (msg.d.showSearchType == true) {
              $("#divSearchType").show();
              $("#sType").html('');
              $("#sType").append("<option value=''>" + ComboSelect + "</option>");

              $.each(msg.d.searchType, function (i) {
                $("#sType").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
              });              
            } else {
              $("#divSearchType").hide();
            }

            if (msg.d.showVehicleCat == true) {
              $("#divVehicleCatID").show();
              $("#divVehicleUnit").show();
            }
            else {
              $("#divVehicleCatID").hide();
              $("#divVehicleUnit").hide();
            }

            $("#sVehicleCatID").html('');
            $("#sVehicleCatID").append("<option value=''>" + ComboSelect + "</option>");
            $.each(msg.d.vehicleCatList, function (i) {
              $("#sVehicleCatID").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(function () {
      $.query = $.query.load(location.href);
      $("#recordType").val($.query.get('recordType'));
      getFormData();
      getLocation();
      createTourist();
    });
  </script>

</head>
<body>
  <form id="formRsFlight" runat="server">
    <input id="recordType" type="hidden" value="" />
    <div id="divRs">
      <table>
        <tr>
          <td class="leftCell">
            <div id="divSearchType" class="divs" style="display: none;">
              <div class="LeftDiv">
                <span class="compulsoryField">* </span>
                <span class="label">                    
                  <%= GetGlobalResourceObject("OnlyExcursion", "ExcursionType") %>
                                :</span>
              </div>
              <div id="divSType" class="inputDiv">
                <select id="sType" onchange="changeType();" style="width: 95%;">
                  <option value="0"><%= GetGlobalResourceObject("OnlyExcursion", "ExcursionType0") %></option>
                  <option value="3"><%= GetGlobalResourceObject("OnlyExcursion", "ExcursionType3") %></option>
                </select>
              </div>
            </div>
            <div id="divDate" class="divs">
              <div class="LeftDiv">
                <span class="compulsoryField">* </span>
                <span class="label">                    
                  <%= GetGlobalResourceObject("Controls", "viewDate") %>
                                :</span>
              </div>
              <div class="inputDiv">
                <asp:TextBox ID="iDate" runat="server" Width="100px" />
                <rjs:PopCalendar ID="ppcDate" runat="server" Control="iDate" ClientScriptOnDateChanged="DateChange" />
              </div>
            </div>
            <div id="divRouteFrom" class="divs">
              <div class="LeftDiv">
                <span class="label">
                    <span class="compulsoryField">* </span>
                  <%= GetGlobalResourceObject("Controls", "viewLocation") %>
                                :</span>
              </div>
              <div id="divSLocation" class="inputDiv">
                <select id="sLocation" onchange="changeLocation();" style="width: 95%;">
                </select>
              </div>
            </div>

            <div id="divFlightNo" class="divs">
              <div class="LeftDiv">
                <span class="compulsoryField">* </span>
                <span class="label">                    
                  <%= GetGlobalResourceObject("Controls", "viewExcursion") %>
                                :</span>
              </div>
              <div class="inputDiv">
                <select id="sExcursion" style="width: 95%;" onchange="changeExcursion()">
                </select>
              </div>
            </div>
            <div id="divVehicleCatID" class="divs">
              <div class="LeftDiv">
                <span class="label">
                  <%= GetGlobalResourceObject("Controls", "viewVehicleCat") %>
                                :</span>
              </div>
              <div class="inputDiv">
                <select id="sVehicleCatID" style="width: 95%;">
                </select>
              </div>
            </div>
            <div id="divVehicleUnit" class="divs">
              <div class="LeftDiv">
                <span class="label">
                  <%= GetGlobalResourceObject("Controls", "viewVehicleUnit") %>
                                :</span>
              </div>
              <div class="inputDiv">
                <input id="sVehicleUnit" style="width: 30%;" />
              </div>
            </div>
            <div id="divExcPackDetails" class="excDetails">
              <table>
                <tr>
                  <td align="right" valign="top" style="width: 100px;"></td>
                  <td id="packageDetail" align="left" valign="top" style="white-space: nowrap;"></td>
                </tr>
              </table>
            </div>
            <div id="divAdultChild" class="divs ui-helper-clearfix">
              <div class="LeftDiv">
                <span class="label">
                  <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                :</span>
              </div>
              <div style="float: left; width: 100px; height: 100%; text-align: left;">
                <b><span id="iAdult"></span></b>
              </div>
              <div class="LeftDiv">
                <span class="label">
                  <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                :</span>
              </div>
              <div style="float: left; width: 100px; height: 100%; text-align: left;">
                <b><span id="iChild"></span></b>
              </div>
            </div>
            <div id="divUnit" class="divs">
              <div class="LeftDiv">
                <span class="label">
                  <%= GetGlobalResourceObject("Controls", "viewUnit") %>
                                :</span>
              </div>
              <div class="inputDiv">
                <b><span id="iUnit"></span></b>
              </div>
            </div>
          </td>
          <td class="rightCell">
            <br />
            <div id="divSalePrice">
              <span class="label">
                <%= GetGlobalResourceObject("Controls", "viewSalePrice") %>
                            :</span> <span id="iSalePrice" class="salePrice"></span>
            </div>
            <br />
            <div id="divCalcBtn">
              <input id="btnRecalc" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                style="width: 150px;" />
            </div>
            <br />
            <div class="divPriceDetail" style="display: none;">
              <span class="label">
                <%= GetGlobalResourceObject("Controls", "viewOldSalePrice") %>
                            :</span> <span id="iResOldPrice" class="salePrice"></span>
              <br />
              <span class="label">
                <%= GetGlobalResourceObject("Controls", "viewNewSalePrice") %>
                            :</span> <span id="iResNewPrice" class="salePrice"></span>
              <br />
              <span class="iAgree">
                <input id="iAggre" type="checkbox" checked="checked" /><label for="iAggre"><%= GetGlobalResourceObject("Controls", "iAgree") %></label>
              </span>
            </div>
            <asp:HiddenField ID="hfDate" runat="server" />
          </td>
        </tr>
      </table>
      <br />
      <table cellpadding="2" cellspacing="0">
        <tr>
          <td align="center">
            <b>
              <%= GetGlobalResourceObject("Controls", "lblServiceTourist")%></b>
          </td>
          <td></td>
          <td align="center">
            <b>
              <%= GetGlobalResourceObject("Controls", "lblOtherTourist")%></b>
          </td>
        </tr>
        <tr>
          <td style="width: 290px;" align="left">
            <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                    <input type="text" id="box1Filter" />
            <button type="button" id="box1Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
              X</button><br />
            <select id="box1View" multiple="multiple" style="width: 100%; height: 125px;">
            </select><br />
            <%--<span id="box1Counter" class="countLabel"></span>--%>
            <select id="box1Storage">
            </select>
          </td>
          <td style="width: 40px;" align="center">
            <button id="to2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
              >
            </button>
            <br />
            <button id="allTo2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
              >>
            </button>
            <br />
            <button id="allTo1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
              <<
            </button>
            <br />
            <button id="to1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
              <
            </button>
          </td>
          <td style="width: 290px;" align="left">
            <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                    <input type="text" id="box2Filter" />
            <button type="button" id="box2Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
              X</button><br />
            <select id="box2View" multiple="multiple" style="width: 100%; height: 125px;">
            </select><br />
            <%--<span id="box2Counter" class="countLabel"></span>--%>
            <select id="box2Storage">
            </select>
          </td>
        </tr>
      </table>
      <table id="divBtn">
        <tr>
          <td style="width: 50%;" align="center">
            <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
              onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
              style="width: 100px;" />
          </td>
          <td align="center">
            <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
              onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
              style="width: 100px;" />
          </td>
        </tr>
      </table>
    </div>
    <div id="dialog-message" title="" style="display: none;">
      <p>
        <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
      </p>
    </div>
  </form>
</body>
</html>
