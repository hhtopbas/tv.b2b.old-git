﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangeResDate.aspx.cs" Inherits="Controls_ChangeResDate" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "ChangeResDate")%></title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/ChangeResDate.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        function showAlert(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                            return true;
                        }
                    }
                });
            });
        }

        function getFormData(resNo) {
            $.ajax({
                type: "POST",
                url: "../Controls/ChangeResDate.aspx/getFormData",
                data: '{"ResNo":"' + resNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    PopCalAddSpecialDayClear(0);
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            PopCalAddSpecialDay(this.Day, this.Month, this.Year, '', 0);
                        });
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function resDateChanged() {
            var begDate = $("#txtBegDate").val();
            var _begDateFormat = $("#txtBegDate").attr("Format");
            $.ajax({
                type: "POST",
                url: "../Controls/ChangeResDate.aspx/changeDate",
                data: '{"newBeginDate":"' + begDate + '","dateFormat":"' + _begDateFormat + '","save":true}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var returnVal = $.json.decode(msg.d);
                        if (returnVal.retVal >= 0) {
                            window.close;
                            self.parent.returnCancelChangeDate(true);
                        }
                        else if (returnVal.retVal < 0) {
                            showAlert(returnVal.Message);
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function confirmResDateChange(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>': function() {
                            $(this).dialog('close');
                            resDateChanged();
                        },
                        '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>': function() {
                            $(this).dialog('close');
                            window.close;
                            self.parent.cancelChangeResDate();
                        }
                    }
                });
            });
        }

        function nextStep() {
            var begDate = $("#txtBegDate").val();
            var _begDateFormat = $("#txtBegDate").attr("Format");
            $.ajax({
                type: "POST",
                url: "../Controls/ChangeResDate.aspx/changeDate",
                data: '{"newBeginDate":"' + begDate + '","dateFormat":"' + _begDateFormat + '","save":false}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var returnVal = $.json.decode(msg.d);
                        if (returnVal.retVal == 0) {
                            resDateChanged();
                        }
                        else if (returnVal.retVal < 0) {
                            showAlert(returnVal.Message);
                        }
                        else {
                            confirmResDateChange(returnVal.Message);
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function logout() {
            self.parent.logout();
        }

        function canceled() {
            window.close;
            self.parent.cancelChangeResDate();
        }

        function pageLoad() {
            $.query = $.query.load(location.href);
            var resNo = $.query.get('ResNo');
            getFormData(resNo);
        }
        
    </script>

</head>
<body onload="javascript:pageLoad();">
    <form id="changeResDate" runat="server">
    <div id="divChangeDate">
        <div class="calendarDiv">
            <br />
            <%= GetGlobalResourceObject("Controls", "lblResBeginDate")%>
            :
            <br />
            <asp:TextBox ID="txtBegDate" runat="server" Width="100px" />
            <rjs:PopCalendar ID="ppcBegDate" runat="server" Control="txtBegDate" />
            <br />
        </div>
        <br />
        <div style="text-align: center; font-size: 8pt;">
            <input type="button" value='<%= GetGlobalResourceObject("LibraryResource", "sNext")%>'
                onclick="nextStep();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />&nbsp;&nbsp;
            <input type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel")%>'
                onclick="canceled();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
        </div>
    </div>
    <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
        style="display: none;">
        <p>
            <span id="messages">Message</span>
        </p>
    </div>
    </form>
</body>
</html>
