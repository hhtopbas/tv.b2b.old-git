﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Threading;
using System.Web.Services;
using System.Data;
using TvTools;
using System.Web.Script.Services;

public partial class Controls_RSAdd_OnlyTransfer : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static RSAdd_OnlyTransfer_getForm getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<int> pickupTimeHour = new List<int>();
        for (int i = 0; i < 24; i++) pickupTimeHour.Add(i);
        List<int> pickupTimeMinute = new List<int>();
        for (int i = 0; i < 60; i += 5) pickupTimeMinute.Add(i);
        List<Location> countryList = new OnlyTransfers().getTransferCountry(UserData, ref errorMsg);
        List<CodeName> countrys = (from q in countryList
                                   select new CodeName { Code = q.RecID.ToString(), Name = useLocalName ? q.NameL : q.Name }).ToList<CodeName>();
        List<CodeName> sRegion = new List<CodeName>();
        sRegion.Add(new CodeName { Code = "0", Name = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceFlight").ToString() });
        sRegion.Add(new CodeName { Code = "1", Name = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceHotel").ToString() });
        sRegion.Add(new CodeName { Code = "2", Name = HttpContext.GetGlobalResourceObject("LibraryResource", "AddServiceOther").ToString() });
        RSAdd_OnlyTransfer_getForm retVal = new RSAdd_OnlyTransfer_getForm();
        retVal.DateFormat = UserData.Ci.TwoLetterISOLanguageName;
        retVal.TransferDate = ResData.ResMain.BegDate.Value.ToShortDateString();
        retVal.PickupTimeHour = pickupTimeHour;
        retVal.PickupTimeMinute = pickupTimeMinute;
        retVal.CountryList = countrys;
        retVal.sRegion = sRegion;
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> getTransferFrom(int? country, long? flyDate, int? RegionFrom)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(flyDate) / 86400000) + 1);

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CodeName> retVal = new List<CodeName>();
        switch (RegionFrom)
        {
            case 0:
                retVal = new OnlyTransfers().getCountryFlights(UserData, country, checkIn, true, ref errorMsg);
                break;
            case 1:
                retVal = new OnlyTransfers().getCountryHotels(UserData, country, useLocalName, ref errorMsg);
                break;
            case 2:
                retVal = new OnlyTransfers().getCountryLocations(UserData, country, useLocalName, ref errorMsg);
                break;
            default:
                retVal = new List<CodeName>();
                break;
        }
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> getTransferTo(int? country, long? flyDate, int? RegionTo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(flyDate) / 86400000) + 1);

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CodeName> retVal = new List<CodeName>();
        switch (RegionTo)
        {
            case 0:
                retVal = new OnlyTransfers().getCountryFlights(UserData, country, checkIn, false, ref errorMsg);
                break;
            case 1:
                retVal = new OnlyTransfers().getCountryHotels(UserData, country, useLocalName, ref errorMsg);
                break;
            case 2:
                retVal = new OnlyTransfers().getCountryLocations(UserData, country, useLocalName, ref errorMsg);
                break;
            default:
                retVal = new List<CodeName>();
                break;
        }
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> getTransfers(int? TransferFrom, int? TransferTo, long? Date)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(Date) / 86400000) + 1);

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        List<CodeName> retVal = new List<CodeName>();

        retVal = new OnlyTransfers().getCountryTransfers(UserData, null, TransferFrom, TransferTo, checkIn, ResData.ResMain.Adult + ResData.ResMain.Child, useLocalName, ref errorMsg);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> getLocations(int? country)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CodeName> locations = new OnlyTransfers().getCountryLocations(UserData, country, useLocalName, ref errorMsg);

        return locations;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<CodeName> getArrival(int? Departure, long? Date)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(Date) / 86400000) + 1);

        string errorMsg = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<CodeName> locations = new OnlyTransfers().getTransferArrival(UserData, Departure, checkIn, useLocalName, ref errorMsg);

        return locations;
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    where s.Status == 0
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<calendarColor> getTransferDates(int? trfFrom, int? trfTo, string transfer)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<calendarColor> retVal = new Transfers().getTransferDates(UserData, ResData, trfFrom, trfTo, transfer, ref errorMsg);
        return retVal;
    }

    [WebMethod(EnableSession = true)]
    public static string CalcService(string selectedCusts, string Departure, int? DepCity, string Arrival, int? ArrCity, string Transfer,
                long? _Date, string PickupTimeHH, string PickupTimeSS, string PickupNote, string recordType, int? RegionFrom, int? RegionTo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;

        DateTime? Date = new DateTime(1970, 1, 1);
        Date = Date.Value.AddDays((Convert.ToInt64(_Date) / 86400000) + 1);
        if (!_Date.HasValue) return retVal;

        Int32? routeFrom = null;
        Int32? routeTo = null;

        switch (RegionFrom)
        {
            #region From
            case 0:
                FlightDayRecord flight = new Flights().getFlightDay(UserData, Departure, Date.Value, ref errorMsg);
                if (flight != null)
                    routeFrom = flight.ArrCity;
                break;
            case 1:
                HotelRecord hotel = new Hotels().getHotelDetail(UserData, Departure, ref errorMsg);
                if (hotel != null)
                    routeFrom = hotel.TrfLocation;
                break;
            case 2:
                routeFrom = Conversion.getInt32OrNull(Departure);
                break;
            default:
                routeFrom = null;
                break;
            #endregion
        }
        switch (RegionTo)
        {
            #region To
            case 0:
                FlightDayRecord flight = new Flights().getFlightDay(UserData, Arrival, Date.Value, ref errorMsg);
                if (flight != null)
                    routeTo = flight.ArrCity;
                break;
            case 1:
                HotelRecord hotel = new Hotels().getHotelDetail(UserData, Arrival, ref errorMsg);
                if (hotel != null)
                    routeTo = hotel.TrfLocation;
                break;
            case 2:
                routeTo = Conversion.getInt32OrNull(Arrival);
                break;
            default:
                routeTo = null;
                break;
            #endregion
        }

        int? pickupTimeHH = Conversion.getInt32OrNull(PickupTimeHH != "undefined" ? PickupTimeHH : "");
        int? pickupTimeSS = Conversion.getInt32OrNull(PickupTimeSS != "undefined" ? PickupTimeSS : "");
        DateTime? pickupTime = null;
        if (pickupTimeHH.HasValue && pickupTimeSS.HasValue)
        {
            pickupTime = Date.Value.AddHours(pickupTimeHH.Value).AddMinutes(pickupTimeSS.Value);
        }
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        List<calendarColor> transferDays = new Transfers().getTransferDates(UserData, ResData, DepCity, ArrCity, Transfer, ref errorMsg);
        if (transferDays.Count > 0)
        {
            var query = from q in transferDays
                        select new
                        {
                            transferDay = new DateTime(q.Year.HasValue ? q.Year.Value : DateTime.Today.Year,
                                                        q.Month.HasValue ? q.Month.Value : DateTime.Today.Month,
                                                        q.Day.HasValue ? q.Day.Value : DateTime.Today.Day)
                        };
            if (query.Where(w => w.transferDay == Date).Count() < 1)
            {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                                    string.Empty,
                                    HttpContext.GetGlobalResourceObject("Controls", "addPleaseCorrectDate"));
                return "{" + retVal + "}";
            }
        }

        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;

        Int16 StartDay = Convert.ToInt16((Date.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;

        string[] _trf = Transfer.Split(';');
        bool isRT = false;
        TransferRecord transfer = new Transfers().getTransfer(UserData.Market, _trf[0], ref errorMsg);
        if (transfer != null && string.Equals(transfer.Direction, "R")) isRT = true;


        TransferRecord transfer0 = new Transfers().getTransfer(UserData.Market, _trf[0], ref errorMsg);

        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.Value, Date.Value, "TRANSFER", _trf[0],
            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, DepCity, ArrCity, transfer0 != null ? transfer0.Direction : "", "", 0, 0, null, pickupTime, PickupNote, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;

        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
            ResServiceRecord service1 = new ResServiceRecord();

            DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "TRANSFER", ServiceID, ref errorMsg);
            decimal? _salePrice = Conversion.getDecimalOrNull(returnData.Rows[0]["SalePrice"]);

            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"Adult\":\"{2}\",\"Child\":\"{3}\",\"Unit\":\"{4}\"",
                (_salePrice.HasValue ? _salePrice.Value.ToString("#,###.00") : "") + " " + returnData.Rows[0]["SaleCur"].ToString(),
                    supplierRec.NameL,
                    AdlCnt,
                    ChdCnt,
                    service.Unit);
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string SaveService(string selectedCusts, string Departure, int? DepCity, string Arrival, int? ArrCity, string Transfer,
                long? _Date, string PickupTimeHH, string PickupTimeSS, string PickupNote, string recordType,
                string SuppNote, int? RegionFrom, int? RegionTo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        DateTime? Date = new DateTime(1970, 1, 1);
        Date = Date.Value.AddDays((Convert.ToInt64(_Date) / 86400000) + 1);
        if (!_Date.HasValue) return retVal;

        Int32? routeFrom = null;
        Int32? routeTo = null;

        switch (RegionFrom)
        {
            #region From
            case 0:
                FlightDayRecord flight = new Flights().getFlightDay(UserData, Departure, Date.Value, ref errorMsg);
                if (flight != null)
                    routeFrom = flight.ArrCity;
                break;
            case 1:
                HotelRecord hotel = new Hotels().getHotelDetail(UserData, Departure, ref errorMsg);
                if (hotel != null)
                    routeFrom = hotel.TrfLocation;
                break;
            case 2:
                routeFrom = Conversion.getInt32OrNull(Departure);
                break;
            default:
                routeFrom = null;
                break;
            #endregion
        }
        switch (RegionTo)
        {
            #region To
            case 0:
                FlightDayRecord flight = new Flights().getFlightDay(UserData, Arrival, Date.Value, ref errorMsg);
                if (flight != null)
                    routeTo = flight.ArrCity;
                break;
            case 1:
                HotelRecord hotel = new Hotels().getHotelDetail(UserData, Arrival, ref errorMsg);
                if (hotel != null)
                    routeTo = hotel.TrfLocation;
                break;
            case 2:
                routeTo = Conversion.getInt32OrNull(Arrival);
                break;
            default:
                routeTo = null;
                break;
            #endregion
        }

        int? pickupTimeHH = Conversion.getInt32OrNull(PickupTimeHH != "undefined" ? PickupTimeHH : "");
        int? pickupTimeSS = Conversion.getInt32OrNull(PickupTimeSS != "undefined" ? PickupTimeSS : "");
        DateTime? pickupTime = null;
        if (pickupTimeHH.HasValue && pickupTimeSS.HasValue)
        {
            pickupTime = Date.Value.AddHours(pickupTimeHH.Value).AddMinutes(pickupTimeSS.Value);
        }
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        List<calendarColor> transferDays = new Transfers().getTransferDates(UserData, ResData, DepCity, ArrCity, Transfer, ref errorMsg);
        if (transferDays.Count > 0)
        {
            var query = from q in transferDays
                        select new
                        {
                            transferDay = new DateTime(q.Year.HasValue ? q.Year.Value : DateTime.Today.Year,
                                                        q.Month.HasValue ? q.Month.Value : DateTime.Today.Month,
                                                        q.Day.HasValue ? q.Day.Value : DateTime.Today.Day)
                        };
            if (query.Where(w => w.transferDay == Date.Value).Count() < 1)
            {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                                    string.Empty,
                                    HttpContext.GetGlobalResourceObject("Controls", "addPleaseCorrectDate"));
                return "{" + retVal + "}";
            }
        }

        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;

        Int16 StartDay = Convert.ToInt16((Date.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;
        string[] _trf = Transfer.Split(';');
        bool isRT = false;

        TransferRecord transfer = new Transfers().getTransfer(UserData.Market, _trf[0], ref errorMsg);
        if (transfer != null && string.Equals(transfer.Direction, "R")) isRT = true;

        TransferRecord transfer0 = new Transfers().getTransfer(UserData.Market, _trf[0], ref errorMsg);

        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.Value, Date.Value, "TRANSFER", _trf[0],
            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, DepCity, ArrCity, transfer0 != null ? transfer0.Direction : "", "", 0, 0, null, pickupTime, PickupNote, string.Empty,
            Conversion.getByteOrNull(RegionFrom.ToString()), Conversion.getByteOrNull(RegionTo.ToString()), null, null, Departure, Arrival, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;

        if (!isRT)
        {
            if (ServiceID > 0)
            {
                List<ResServiceRecord> resService = ResData.ResService;
                ResServiceRecord trfService = resService.Find(f => f.RecID == ServiceID);
                if (trfService != null) trfService.SupNote = Conversion.getStrOrNull(SuppNote);
            }
        }

        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            if (_recordType == true)
            {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                {
                    List<ResServiceRecord> resServiceList = ResData.ResService;
                    ResServiceRecord resService = resServiceList.LastOrDefault();
                    resService.ExcludeService = false;
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
                else
                {
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
            }
            else
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "<br />";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "<br />";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
            }
        }
        return "{" + retVal + "}";
    }

}

public class RSAdd_OnlyTransfer_getForm
{
    public string DateFormat { get; set; }
    public string TransferDate { get; set; }
    public List<int> PickupTimeHour { get; set; }
    public List<int> PickupTimeMinute { get; set; }
    public List<CodeName> CountryList { get; set; }
    public List<CodeName> sRegion { get; set; }

    public RSAdd_OnlyTransfer_getForm()
    {
        this.PickupTimeHour = new List<int>();
        this.PickupTimeMinute = new List<int>();
        this.CountryList = new List<CodeName>();
        this.sRegion = new List<CodeName>();        
    }
}