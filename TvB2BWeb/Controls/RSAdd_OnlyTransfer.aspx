﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSAdd_OnlyTransfer.aspx.cs" Inherits="Controls_RSAdd_OnlyTransfer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "RSAddTransfer") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/RSOnlyTransfer.css" rel="stylesheet" />

    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document)
            .ajaxStart($.blockUI)
            .ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function getRouteFrom() {
            var obj = new Object();
            obj.country = parseInt($("#sCountry").val());
            obj.flyDate = $("#iTrfDate").datepicker('getDate').getTime();
            obj.RegionFrom = parseInt($("#sRegionFrom").val());

            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_OnlyTransfer.aspx/getTransferFrom",
                data: $.json.encode(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {                    
                    $("#sArrLocation").html('');
                    $("#sDepLocation").html('');
                    $("#sDepLocation").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#sDepLocation").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }

                    if (obj.RegionFrom == '0') {
                        $("#sDepLocation").hide();
                        $("#iDepLocation").show();
                    }
                    else {
                        $("#sDepLocation").show();
                        $("#iDepLocation").hide();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });            
        }

        function getRouteTo() {
            var obj = new Object();
            obj.country = parseInt($("#sCountry").val());
            obj.flyDate = $("#iTrfDate").datepicker('getDate').getTime();
            obj.RegionTo = parseInt($("#sRegionTo").val());
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_OnlyTransfer.aspx/getTransferTo",
                data: $.json.encode(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {                    
                    $("#sArrLocation").html('');
                    if (msg.d == '') return;
                    $("#sArrLocation").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#sArrLocation").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }

                    if (obj.RegionTo == '0') {
                        $("#sArrLocation").hide();
                        $("#iArrLocation").show();
                    }
                    else {
                        $("#sArrLocation").show();
                        $("#iArrLocation").hide();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });           
        }

        function getTransfers() {
            var obj = new Object();
            obj.TransferFrom = $("#sDeparture").val();
            obj.TransferTo = $("#sArrival").val();
            obj.Date = $("#iTrfDate").datepicker('getDate').getTime();            
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_OnlyTransfer.aspx/getTransfers",
                data: $.json.encode(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {                    
                    $("#sTransfer").html('');
                    
                    $("#sTransfer").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#sTransfer").append("<option value='" + this.Code + "'><div>" + this.Name + "</div></option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });            
        }

        function changeCountrys() {
            $("#sDepparture").html('');
            $("#sArrival").html('');
            $("#sRegionFrom").val('');
            $("#sRegionTo").val('');
            $("#sDepLocation").html('');
            $("#sArrLocation").html('');
            $("#sTransfer").html('');
            $("#iPickupTimeHH").val('');
            $("#iPickupTimeSS").val('');
            $("#iPickupNote").val('');
            $("#iSupplierNote").val('');
            var obj = new Object();
            obj.country = $("#sCountry").val();
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_OnlyTransfer.aspx/getLocations",
                data: $.json.encode(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        $("#sDeparture").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                        $("#sArrival").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                        $.each(msg.d, function (i) {
                            $("#sDeparture").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                            $("#sArrival").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeDeparture() {
            $("#sTransfer").html('');            
            $("#sArrLocation").html('');
            $("#sDepLocation").html('');

            var obj = new Object();
            obj.Departure = $("#sDeparture").val();                            
            obj.Date = $("#iTrfDate").datepicker('getDate').getTime();            

            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_OnlyTransfer.aspx/getArrival",
                data: $.json.encode(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {                    
                    $("#sArrival").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#sArrival").append("<option value='" + this.Code + "'><div>" + this.Name + "</div></option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeArrival() {
            getTransfers();
        }        

        function changeTransfer() {

        }

        function createTourist() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_OnlyTransfer.aspx/getTourist",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#box1View").html("");
                    $.each($.json.decode(msg.d), function (i) {
                        $("#box1View").append("<option value='" + this.CustNo + "'>" + this.Name + "</option>");
                    });
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            $(function () {
                $.configureBoxes();
            });
        }


        function preCalc(save) {
            if ($("#sDeparture").val() == null || $("#sDeparture").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectDeparture") %>');
                return;
            }
            if ($("#sArrival").val() == null || $("#sArrival").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectArrival") %>');
                return;
            }
            if ($("#sTransfer").val() == null || $("#sTransfer").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTransfer") %>');
                return;
            }

            var selectCust = '';
            $("#box1View option").each(function () {
                if (selectCust.length > 0) selectCust += "|";
                selectCust += $(this).val();
            });
            if (selectCust.length < 1) {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>');
                    return;
                }
                var begDate = $("#iTrfDate").val();
                var _begDateFormat = $("#TrfDate").attr("Format");
                if (begDate == '') {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseCorrectDate") %>');
                return;
            }
            var iDate = $("#iTrfDate").datepicker('getDate') != null ? $("#iTrfDate").datepicker('getDate').getTime() : new Date().getTime();
            var _data = new Object();
            var _url = "";
            //selectedCusts, Departure, Arrival, Transfer, _Date, PickupTimeHH, PickupTimeSS, PickupNote, string trfType
            if (save) {
                _url = "../Controls/RSAdd_OnlyTransfer.aspx/SaveService";
                _data.selectedCusts = selectCust;
                _data.Departure = $("#sDepLocation").val();
                _data.DepCity = $("#sDeparture").val();
                _data.Arrival = $("#sArrLocation").val();
                _data.ArrCity = $("#sArrival").val();
                _data.Transfer = $("#sTransfer").val();
                _data._Date = iDate;
                _data.PickupTimeHH = $("#iPickupTimeHH").val();
                _data.PickupTimeSS = $("#iPickupTimeSS").val();
                _data.PickupNote = $("#iPickupNote").val();
                _data.recordType = $("#recordType").val();
                _data.SuppNote = $("#iSupplierNote").val();
                _data.RegionFrom = parseInt($("#sRegionFrom").val());
                _data.RegionTo = parseInt($("#sRegionTo").val());
            }
            else {
                _url = "../Controls/RSAdd_OnlyTransfer.aspx/CalcService";
                _data.selectedCusts = selectCust;
                _data.Departure = $("#sDepLocation").val();
                _data.DepCity = $("#sDeparture").val();
                _data.Arrival = $("#sArrLocation").val();
                _data.ArrCity = $("#sArrival").val();
                _data.Transfer = $("#sTransfer").val();
                _data._Date = iDate;
                _data.PickupTimeHH = $("#iPickupTimeHH").val();
                _data.PickupTimeSS = $("#iPickupTimeSS").val();
                _data.PickupNote = $("#iPickupNote").val();
                _data.recordType = $("#recordType").val();
                _data.RegionFrom = parseInt($("#sRegionFrom").val());
                _data.RegionTo = parseInt($("#sRegionTo").val());
            }
            $.ajax({
                type: "POST",
                url: _url,
                data: $.json.encode(_data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var result = $.json.decode(msg.d);
                    if (save) {
                        window.close;
                        self.parent.returnAddResServices(true, result.Supplier);
                    }
                    if (result.Price != '') {
                        $("#iSalePrice").text(result.Price);
                        //$("#iSupplier").text(result.Supplier);
                        $("#iAdult").text(result.Adult);
                        $("#iChild").text(result.Child);
                        $("#iUnit").text(result.Unit);
                    } else showMessage(result.Supplier);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            if (source == 'save') {
                preCalc(true);
            }
            else if (source == 'cancel') {
                window.close;
                self.parent.returnAddResServices(false, '');
            }
        }

        function getFormData() {
            $.ajax({
                async: false,
                type: "POST",
                url: "RSAdd_OnlyTransfer.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        var result = msg.d;
                        if (result.CountryList != null) {
                            $("#sCountry").html('');
                            $("#sCountry").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                            $.each(result.CountryList, function (i) {
                                $("#sCountry").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                            });
                        }
                        if (result.CountryList != null) {
                            $("#sRegionFrom").html('');
                            $("#sRegionFrom").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                            $("#sRegionTo").html('');
                            $("#sRegionTo").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                            $.each(result.sRegion, function (i) {
                                $("#sRegionFrom").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                                $("#sRegionTo").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                            });
                        }
                        $("#iPickupTimeHH").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                        $.each(result.PickupTimeHour, function (i) {
                            $("#iPickupTimeHH").append("<option value='" + this + "'>" + this + "</option>");                            
                        });

                        $("#iPickupTimeSS").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                        $.each(result.PickupTimeMinute, function (i) {
                            $("#iPickupTimeSS").append("<option value='" + this + "'>" + this + "</option>");
                        });
                        $.datepicker.setDefaults($.datepicker.regional[result.DateFormat != 'en' ? result.DateFormat : '']);

                        //var _date1 = new Date(Date(eval(eval(result.TransferDate))).toString());
                                     
                        $("#iTrfDate").datepicker('setDate', result.TransferDate);

                        $("#sRegionFrom").val('');
                        $("#sRegionTo").val('');
                        $("#iDepLocation").hide();
                        $("#iArrLocation").hide();
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            $("#recordType").val($.query.get('recordType'));
            $("#iTrfDate").datepicker({
                showOn: "button",
                buttonImage: "../Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                minDate: new Date()
            });

            getFormData();
            createTourist();
        });

    </script>

</head>
<body>
    <form id="formRsTransfer" runat="server">
        <input id="recordType" type="hidden" value="" />
        <input id="iDate" type="hidden" />
        <input id="hfTypeDefault" type="hidden" value="AHA" />
        <input id="fltTrfTypeParam" type="hidden" value="" />
        <div class="divBody">
            <div class="divServiceArea">
                <div id="divCountry" class="divs">
                    <div class="LeftDiv">
                      <span class="compulsoryField">* </span>
                        <span id="labelCountry" class="label">
                            <%= GetGlobalResourceObject("Controls", "viewCountry") %>
                        </span>:
                    </div>
                    <div id="divCountrys" class="inputDiv">
                        <select id="sCountry" class="combo width_95" onchange="changeCountrys()">
                        </select>
                    </div>
                </div>
                <div id="divDate" class="divs">
                    <div class="LeftDiv">
                      <span class="compulsoryField">* </span>
                        <span class="label">
                            <%= GetGlobalResourceObject("Controls", "viewTransferDate") %>
                                    :</span>
                    </div>
                    <div class="inputDiv">
                        <input id="iTrfDate" runat="server" width="100px" />
                    </div>
                </div>

                <div id="divRouteFrom" class="divs">
                    <div class="LeftDiv">
                      <span class="compulsoryField">* </span>
                        <span id="labelDepLocation" class="label">
                            <%= GetGlobalResourceObject("Controls", "viewFrom") %>
                        </span>:
                    </div>
                    <div id="divDepLocation" class="inputDiv">
                        <div>
                            <select id="sDeparture" class="combo width_95" onchange="changeDeparture()">
                            </select>
                        </div>
                        <div>
                            <select id="sRegionFrom" class="combo width_20" onchange="getRouteFrom()">
                            </select>
                            <select id="sDepLocation" class="combo width_70" >
                            </select>
                            <input id="iDepLocation" type="text" style="display: none;" />
                        </div>
                    </div>
                </div>
                <div id="divRouteTo" class="divs">
                    <div class="LeftDiv">
                      <span class="compulsoryField">* </span>
                        <span id="labelArrLocation" class="label">
                            <%= GetGlobalResourceObject("Controls", "viewTo") %>
                        </span>:
                    </div>
                    <div id="divArrLocation" class="inputDiv">
                        <div>
                            <select id="sArrival" class="combo width_95" onchange="changeArrival()">
                            </select>
                        </div>
                        <div>
                            <select id="sRegionTo" class="combo width_20" onchange="getRouteTo()">
                            </select>
                            <select id="sArrLocation" class="combo width_70" >
                            </select>
                            <input id="iArrLocation" type="text" style="display: none;" />
                        </div>
                    </div>
                </div>
                <div id="divTransfer" class="divs">
                    <div class="LeftDiv">
                      <span class="compulsoryField">* </span>
                        <span class="label">
                            <%= GetGlobalResourceObject("Controls", "viewTransfer") %>
                                    :</span>
                    </div>
                    <div class="inputDiv">
                        <select id="sTransfer" class="combo width_95" onchange="changeTransfer">
                        </select>
                    </div>
                </div>
                <div id="divPickupTime" class="divs">
                    <div class="LeftDiv">                      
                        <span class="label">
                            <%= GetGlobalResourceObject("Controls", "viewPickupTime") %>
                                    :</span>
                    </div>
                    <div class="inputDiv">
                        <select id="iPickupTimeHH" style="width: 60px;"></select>&nbsp;:&nbsp;<select id="iPickupTimeSS" style="width: 60px;"></select>
                    </div>
                </div>
                <div id="divPickupNote" class="divs">
                    <div class="LeftDiv">                      
                        <span class="label">
                            <%= GetGlobalResourceObject("Controls", "viewPickupNote") %>
                                    :</span>
                    </div>
                    <div class="inputDiv">
                        <input id="iPickupNote" class="width_95" />
                    </div>
                </div>

                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td align="center">
                            <b>
                                <%= GetGlobalResourceObject("Controls", "lblServiceTourist")%></b>
                        </td>
                        <td></td>
                        <td align="center">
                            <b>
                                <%= GetGlobalResourceObject("Controls", "lblOtherTourist")%></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 290px;" align="left">
                            <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box1Filter" />
                            <button type="button" id="box1Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                X</button><br />
                            <select id="box1View" multiple="multiple" style="width: 100%; height: 100px;">
                            </select><br />
                            <select id="box1Storage">
                            </select>
                        </td>
                        <td style="width: 40px;" align="center">
                            <button id="to2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                >
                            </button>
                            <br />
                            <button id="allTo2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                >>
                            </button>
                            <br />
                            <button id="allTo1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                <<
                            </button>
                            <br />
                            <button id="to1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                <
                            </button>
                        </td>
                        <td style="width: 290px;" align="left">
                            <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box2Filter" />
                            <button type="button" id="box2Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                X</button><br />
                            <select id="box2View" multiple="multiple" style="width: 100%; height: 100px;">
                            </select><br />
                            <select id="box2Storage">
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div class="divHeader">
                <div class="recCalcDiv">
                    <div class="divSalePrice">
                        <span class="label">
                            <%= GetGlobalResourceObject("Controls", "viewSalePrice") %>
                                :</span> <span id="iSalePrice" class="salePrice"></span>
                    </div>
                    <div class="divCalcBtn">
                        <input id="btnRecalc" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                            onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                            style="width: 150px;" />
                    </div>
                </div>
            </div>
            <div class="divBtn">
                <div class="divSave">
                    <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
                        onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </div>
                <div class="divCancel">
                    <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                        onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </div>
            </div>
        </div>
        <div id="divRs" style="display: none;">
            <div style="display: none;">
                <table>
                    <tr>
                        <td class="leftCell">


                            <div id="divAdultChild" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                    :</span>
                                </div>
                                <div class="divAdultChild">
                                    <b><span id="iAdult"></span></b>
                                </div>
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                    :</span>
                                </div>
                                <div class="divAdultChild">
                                    <b><span id="iChild"></span></b>
                                </div>
                            </div>
                            <div id="divUnit" class="divs" style="display: none;">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewUnit") %>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <b>
                                        <asp:Label ID="iUnit" runat="server" /></b>
                                </div>
                            </div>


                            <div id="divSupplierNote" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <input id="iSupplierNote" type="text" style="width: 99%;" />
                                </div>
                            </div>
                        </td>
                        <td class="rightCell">
                            <br />

                        </td>
                    </tr>
                </table>

            </div>

        </div>
        <div id="dialog-message" title="" style="display: none;">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
            </p>
        </div>
    </form>
</body>
</html>
