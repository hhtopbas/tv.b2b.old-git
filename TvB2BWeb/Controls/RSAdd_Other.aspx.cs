﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSAdd_Other : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ppcCheckIn.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckIn.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcCheckIn.From.Date = DateTime.Today;
        ppcCheckOut.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckOut.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcCheckOut.From.Date = DateTime.Today;

        if (!IsPostBack)
        {
            //sCountry.Attributes.Add("onchange", "changeCountry();");
            //sLocation.Attributes.Add("onchange", "changeLocation();");            

            ppcCheckIn.DateValue = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
            ppcCheckOut.DateValue = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
            //iNight.Text = (ppcCheckOut.DateValue - ppcCheckIn.DateValue).Days.ToString();
        }
    }

    [WebMethod]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        DateTime ppcCheckIn = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
        DateTime ppcCheckOut = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
        string iNight = (ppcCheckOut - ppcCheckIn).Days.ToString();
        return "{\"iNight\":\"" + iNight + "\",\"OldResPrice\":\"" + ResData.ResMain.SalePrice + " " + ResData.ResMain.SaleCur + "\",\"NewSalePrice\":\"\"}";
    }

    [WebMethod]
    public static string getCountryList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        List<AdServicePriceRecord> adServicePrices = new AdServices().getAdServicePrices(UserData, ResData, string.Empty, string.Empty,
                                                                                    new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value,
                                                                                    ref errorMsg), null, null, null, false, false, ref errorMsg);
        var query = from q in adServicePrices
                    group q by new { RecID = q.Country, Name = q.CountryLocalName } into k
                    select new { RecID = k.Key.RecID, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod]
    public static string getLocationList(string Country)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        int? country = Conversion.getInt32OrNull(Country);
        string errorMsg = string.Empty;
        List<AdServicePriceRecord> adServicePrices = new AdServices().getAdServicePrices(UserData, ResData, string.Empty, string.Empty, country, null, null, null, false, false, ref errorMsg);
        var query = from q in adServicePrices
                    group q by new { RecID = q.Location, Name = q.LocationLocalName } into k
                    select new { RecID = k.Key.RecID, Name = k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod]
    public static string getAdService(string Country, string Location, string ServiceCode)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? country = Conversion.getInt32OrNull(Country);
        int? location = Conversion.getInt32OrNull(Location);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        List<AdServiceRecord> adServiceList = new AdServices().getAdServiceLocations(UserData.Market, ResData.ResMain.PLMarket, ResData.ResMain.BegDate, ResData.ResMain.EndDate, country, location, ref errorMsg);
        var query = from q in adServiceList
                    where (string.IsNullOrEmpty(ServiceCode) || q.Service == ServiceCode)
                    select new { Code = q.Code + "|" + q.Service, Name = q.LocalName };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    where s.Status == 0
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod]
    public static addServiceReturnData CalcService(string selectedCusts, string Country, string Location, string AdService, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        int? country = Conversion.getInt32OrNull(Country);
        int? location = Conversion.getInt32OrNull(Location);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] otherSrv = AdService.Split('|');

        if (otherSrv.Length < 2) return new addServiceReturnData
        {
            Calc = false,
            Msg = "Additional servis not found"
        };
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);
        if (!begDate.HasValue && !endDate.HasValue) return new addServiceReturnData
        {
            Calc = false,
            Msg = "Dates is null"
        };// retVal;

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, otherSrv[1], otherSrv[0], string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, country, location == -999 ? null : location, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            return new addServiceReturnData
            {
                Calc = false,
                Msg = errorMsg
            };
        }
        else
        {
            int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
            if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            {
                ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == ServiceID);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService != null ? resService.Supplier : "", ref errorMsg);
                //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"NewPrice\":\"{2}\"", (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") + " " + (resService.SalePrice.HasValue ? resService.SaleCur : ""), supplierRec.NameL, ResData.ResMain.SalePrice.ToString() + " " + ResData.ResMain.SaleCur);
                return new addServiceReturnData
                {
                    Calc = true,
                    CalcCur = resService.SaleCur,
                    CalcPrice = resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "",
                    Supplier = supplierRec.NameL,
                    OldResPrice = ResData.ResMain.SalePrice.HasValue ? (ResData.ResMain.SalePrice.Value.ToString("#,###.00") + " " + ResData.ResMain.SaleCur) : ""

                };
            }
            else
                return new addServiceReturnData
                {
                    Calc = false,
                    Msg = errorMsg
                };
        }
        //return "{" + retVal + "}";
    }

    [WebMethod]
    public static addServiceReturnData SaveService(string selectedCusts, string Country, string Location, string AdService, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat, string recordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        int? country = Conversion.getInt32OrNull(Country);
        int? location = Conversion.getInt32OrNull(Location);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string[] otherSrv = AdService.Split('|');
        if (otherSrv.Length < 2)
            return new addServiceReturnData
        {
            Calc = false,
            Msg = "Additional servis not found"
        };
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);
        if (!begDate.HasValue && !endDate.HasValue) return new addServiceReturnData
        {
            Calc = false,
            Msg = "Dates is null"
        };

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, otherSrv[1], otherSrv[0],
            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, country, location, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            return new addServiceReturnData
            {
                Calc = false,
                Msg = errorMsg
            };
        }
        else
        {
            if (_recordType == true)
            {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"NewPrice\":\"{2}\"",
                                            string.Empty,
                                            errorMsg,
                                            ResData.ResMain.SalePrice + " " + ResData.ResMain.SaleCur);
                    return new addServiceReturnData
                    {
                        Calc = true,
                    };
                }
                else
                {
                    return new addServiceReturnData
                    {
                        Calc = false,
                        Msg = errorMsg
                    };
                }
            }
            else
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    List<ResServiceRecord> resServiceList = ResData.ResService;
                    ResServiceRecord resService = resServiceList.LastOrDefault();
                    resService.ExcludeService = false;
                    HttpContext.Current.Session["ResData"] = ResData;
                    string Msg = string.Empty;
                    /*foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (row.ControlOK)
                            Msg += row.Message + "<br />";
                    }*/
                    return new addServiceReturnData
                    {
                        Calc = true,
                        CalcPrice = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value.ToString("#,###.00") : "",
                        CalcCur = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SaleCur : "",
                        Msg = returnData[0].Message
                    };
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "\n";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"NewPrice\":\"{2}\"", string.Empty, Msg, "");
                    return new addServiceReturnData
                    {
                        Calc = false,
                        Msg = Msg
                    };
                }
            }
        }
    }

    [WebMethod]
    public static string PaxControl(string selectedCusts)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }
        retVal = string.Format("\"Adult\":{0},\"Child\":{1}", AdlCnt.ToString(), ChdCnt.ToString());
        return "{" + retVal + "}";
    }

}
