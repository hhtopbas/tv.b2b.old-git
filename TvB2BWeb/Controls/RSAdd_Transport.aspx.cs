﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;
using System.Collections;
using Newtonsoft.Json.Linq;

public partial class Controls_RSAdd_Transport : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ppcPickupDate.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcPickupDate.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcPickupDate.From.Date = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.AddDays(-1) : DateTime.Today.AddDays(-1);
        ppcPickupDate.To.Date = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.AddDays(1) : DateTime.Today.AddDays(7);
        if (!IsPostBack)
        {
            ppcPickupDate.DateValue = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
            ppcPickupDate.SetDateValue(ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today);
            Int64 days = (ppcPickupDate.DateValue - new DateTime(1970, 1, 1)).Days - 1;
            hfDate.Value = (days * 86400000).ToString();
        }
    }

    [WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod(UseHttpGet = false, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static addTransportFormData getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        addTransportFormData data = new addTransportFormData();
        data.FromDate = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value.AddDays(-1) : DateTime.Today.AddDays(-1);
        data.ToDate = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.AddDays(1) : DateTime.Today.AddDays(1);
        data.DateLang = UserData.Ci.Parent.ToString();
        data.CurrentDate = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
        return data;
    }

    [WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod(UseHttpGet = false, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static string getTransportDays(string DepCity, string ArrCity)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        int? depCity = Conversion.getInt32OrNull(DepCity);
        int? arrCity = Conversion.getInt32OrNull(ArrCity);

        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            List<TransportDayRouteV> transportRoutes = new Transports().getTransportDayRoute(UserData.Market, ResData.ResMain.PLOperator, ref errorMsg);
            List<TransportDayRecord> transportDay = new Transports().getTransportDays(UserData.Market, ResData.ResMain.PLMarket, null, string.Empty, null, null, string.Empty, ref errorMsg);
            var queryTd = from q in transportRoutes
                          join q1 in transportDay.Where(w => w.TransDate >= ResData.ResMain.BegDate.Value && w.TransDate <= ResData.ResMain.EndDate.Value) on q.Transport equals q1.Transport
                          where !q.SerArea.HasValue || (q.SerArea.HasValue && q.SerArea == 1)
                          group q by new { Date = q1.TransDate } into k
                          select new { k.Key.Date.Day, k.Key.Date.Month, k.Key.Date.Year };
            return Newtonsoft.Json.JsonConvert.SerializeObject(queryTd);
        }
        else
        {
            List<TransportDayRecord> transportDay = new Transports().getTransportDays(UserData.Market, ResData.ResMain.PLMarket, null, string.Empty, null, null, string.Empty, ref errorMsg);
            List<TransportRecord> transportList = new Transports().getTransport(UserData.Market, UserData.Market, string.Empty, null, null, ref errorMsg);
            var query = from q in transportDay
                        join qj in transportList on q.Transport equals qj.Code
                        where (q.TransDate >= ResData.ResMain.BegDate.Value.AddDays(-1) && q.TransDate <= ResData.ResMain.EndDate.Value.AddDays(2))
                        group q by new { Date = q.TransDate } into k
                        select new { k.Key.Date.Day, k.Key.Date.Month, k.Key.Date.Year };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getDeparture(string selectedDate, string dateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        DateTime? date = Conversion.convertDateTime(selectedDate, dateFormat);
        string errorMsg = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            List<TransportDayRouteV> transportRoutes = new Transports().getTransportDayRoute(UserData.Market, ResData.ResMain.PLOperator, ref errorMsg);
            var queryTd = from q in transportRoutes
                          where !q.SerArea.HasValue || (q.SerArea.HasValue && q.SerArea == 1) &&
                            (!date.HasValue || q.TransDate == date.Value)
                          group q by new { RecID = q.Departure, Name = q.DepartureName } into k
                          select new { RecID = k.Key.RecID, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(queryTd);
        }
        else
        {
            List<TransportDayRecord> transportDay = new Transports().getTransportDays(UserData.Market, ResData.ResMain.PLMarket, null, string.Empty, null, null, string.Empty, ref errorMsg);
            List<TransportRecord> transportList = new Transports().getTransport(UserData.Market, UserData.Market, string.Empty, null, null, ref errorMsg);
            var query = from q in transportDay
                        join qj in transportList on q.Transport equals qj.Code
                        where (q.TransDate >= ResData.ResMain.BegDate && q.TransDate <= ResData.ResMain.EndDate)
                        //                           && (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && qj.SerArea.HasValue && qj.SerArea.Value == 1))
                        group q by new { RecID = q.RouteFrom, Name = q.RouteFromNameL } into k
                        select new { RecID = k.Key.RecID, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getArrival(string Departure, string selectedDate, string dateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        DateTime? date = Conversion.convertDateTime(selectedDate, dateFormat);
        int? departure = Conversion.getInt32OrNull(Departure);
        string errorMsg = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            List<TransportDayRouteV> transportRoutes = new Transports().getTransportDayRoute(UserData.Market, ResData.ResMain.PLOperator, ref errorMsg);
            var queryTd = from q in transportRoutes
                          where !q.SerArea.HasValue || (q.SerArea.HasValue && q.SerArea == 1)
                            && q.Departure == departure &&
                            (!date.HasValue || q.TransDate == date.Value)
                          group q by new { RecID = q.Arrival, Name = q.ArrivalName } into k
                          select new { RecID = k.Key.RecID, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(queryTd);
        }
        else
        {
            List<TransportDayRecord> transportDay = new Transports().getTransportDays(UserData.Market, ResData.ResMain.PLMarket, null, string.Empty, departure, null, string.Empty, ref errorMsg);
            List<TransportRecord> transportList = new Transports().getTransport(UserData.Market, UserData.Market, string.Empty, null, null, ref errorMsg);
            var query = from q in transportDay
                        join qj in transportList on q.Transport equals qj.Code
                        where (q.TransDate >= ResData.ResMain.BegDate && q.TransDate <= ResData.ResMain.EndDate)
                           && (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) || (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt) && qj.SerArea.HasValue && qj.SerArea.Value == 1))
                        group q by new { RecID = q.RouteTo, Name = q.RouteToNameL } into k
                        select new { RecID = k.Key.RecID, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getService(string Departure, string Arrival, string selectedDate, string dateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? departure = Conversion.getInt32OrNull(Departure);
        int? arrival = Conversion.getInt32OrNull(Arrival);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        DateTime? date = Conversion.convertDateTime(selectedDate, dateFormat);
        string errorMsg = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            List<TransportRecord> serviceList = new Transports().getTransportForRoutes(UserData.Market, UserData.Market, departure, arrival, date, ref errorMsg);
            var query = from q in serviceList
                        where q.SerArea.HasValue && q.SerArea.Value == 1
                        group q by new { Code = q.Code, Name = q.NameL } into k
                        select new { Code = k.Key.Code, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        else
        {
            List<TransportRecord> serviceList = new Transports().getTransport(UserData.Market, UserData.Market, string.Empty, departure, arrival, ref errorMsg);
            var query = from q in serviceList                        
                        select new { Code = q.Code, Name = q.NameL };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getBus(string Departure, string Arrival, string selectedDate, string dateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? departure = Conversion.getInt32OrNull(Departure);
        int? arrival = Conversion.getInt32OrNull(Arrival);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        DateTime? date = Conversion.convertDateTime(selectedDate, dateFormat);
        string errorMsg = string.Empty;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Novaturas_Lt))
        {
            List<TransportDayRouteV> transportRoutes = new Transports().getTransportDayRoute(UserData.Market, ResData.ResMain.PLOperator, ref errorMsg);
            var query = from q in transportRoutes
                        where !q.SerArea.HasValue || (q.SerArea.HasValue && q.SerArea == 1)
                          && q.Departure == departure && q.Arrival == arrival
                        group q by new { Code = q.Bus, Name = q.Bus } into k
                        select new { Code = k.Key.Code, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        else
        {
            List<BusRecord> serviceList = new Transports().getBusList(UserData.Market, departure, arrival, null, ref errorMsg);
            var query = from q in serviceList
                        select new { Code = q.Code, Name = q.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getPickups(string Service, string Location, string selectedDate, string dateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        int? location = Conversion.getInt32OrNull(Location);
        DateTime? date = Conversion.convertDateTime(selectedDate, dateFormat);
        string errorMsg = string.Empty;
        List<TransportStatRecord> serviceList = new Transports().getPickups(UserData.Market, Service, location, ref errorMsg);
        var query = from q in serviceList
                    select new { RecID = q.Location, Name = q.LocationLocalName };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    where s.Status == 0
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string CalcService(string selectedCusts, string Departure, string Arrival, string Pickup, string Service, string Bus, string Date, string DateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        int? departure = Conversion.getInt32OrNull(Departure);
        int? arrival = Conversion.getInt32OrNull(Arrival);
        int? pickup = Conversion.getInt32OrNull(Pickup);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? endDate = Conversion.convertDateTime(Date, DateFormat);
        DateTime? begDate = null;
        if (!endDate.HasValue) return retVal;
        List<TransportRouteRecord> routes = new Transports().getTransportRoute(Service, ref errorMsg);
        var query = from q in routes
                    where q.Location == (pickup.HasValue ? pickup.Value : 0)
                    select new { Duration = q.Duration };
        if (query.Count() > 0)
        {
            begDate = endDate.Value.AddMinutes((-1 * query.First().Duration.Value));
        }
        else
            begDate = endDate;
        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);

        TransportRecord transfer = new Transports().getTransport(UserData.Market, ResData.ResMain.PLMarket, Service, ref errorMsg);

        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "TRANSPORT", Service, string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, departure, arrival, string.Empty, Bus, 0, 0, pickup, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);

        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
            DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "TRANSPORT", ServiceID, ref errorMsg);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", returnData.Rows[0]["SalePrice"].ToString() + " " + returnData.Rows[0]["SaleCur"].ToString(), supplierRec.NameL);
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string SaveService(string selectedCusts, string Departure, string Arrival, string Pickup, string Service, string Bus, string Date, string DateFormat, string recordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        int? departure = Conversion.getInt32OrNull(Departure);
        int? arrival = Conversion.getInt32OrNull(Arrival);
        int? pickup = Conversion.getInt32OrNull(Pickup);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? endDate = Conversion.convertDateTime(Date, DateFormat);
        DateTime? begDate = null;
        if (!endDate.HasValue) return retVal;
        List<TransportRouteRecord> routes = new Transports().getTransportRoute(Service, ref errorMsg);
        var query = from q in routes
                    where q.Location == (pickup.HasValue ? pickup.Value : 0)
                    select new { Duration = q.Duration };
        if (query.Count() > 0)
        {
            begDate = endDate.Value.AddMinutes((-1 * query.First().Duration.Value));
        }
        else
            begDate = endDate;

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
        TransportRecord transfer = new Transports().getTransport(UserData.Market, ResData.ResMain.PLMarket, Service, ref errorMsg);
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "TRANSPORT", Service,
            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, departure, arrival, string.Empty, Bus, 0, 0, pickup, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            if (_recordType == true)
            {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                {
                    List<ResServiceRecord> resServiceList = ResData.ResService;
                    ResServiceRecord resService = resServiceList.LastOrDefault();
                    resService.ExcludeService = false;
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
                else
                {
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
            }
            else
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "<br />";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "\n";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
            }
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string PaxControl(string selectedCusts)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }
        retVal = string.Format("\"Adult\":{0},\"Child\":{1}", AdlCnt.ToString(), ChdCnt.ToString());
        return "{" + retVal + "}";
    }

}

public class addTransportFormData
{
    public string DateLang { get; set; }
    public DateTime? CurrentDate { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
}