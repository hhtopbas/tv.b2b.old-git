﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using TvTools;
using System.Text;

public partial class Controls_ChangeResDate : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ppcBegDate.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcBegDate.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        if (!IsPostBack)
            ppcBegDate.DateValue = ResData.ResMain.BegDate.Value;
    }

    [WebMethod(EnableSession = true)]
    public static string getFormData(string ResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);                
        
        List<FlightDays> flightDays = new Flights().getFlightDays(UserData, ResData.ResMain.DepCity, ResData.ResMain.ArrCity, true, ResData.ResMain.PackType, null, string.Empty, ref errorMsg);

        if (ResData.ResService.Where(w => w.ServiceType == "FLIGHT").Count() > 0)
        {
            var query = from q in flightDays
                        select new { Year = q.FlyDate.Year, Month = q.FlyDate.Month, Day = q.FlyDate.Day };
            return query.Count() > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(query) : string.Empty;
        }
        else return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string getFlightDays(string DepCity, string ArrCity)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);                
        string errorMsg = string.Empty;
        int? depCity = Conversion.getInt32OrNull(DepCity);
        int? arrCity = Conversion.getInt32OrNull(ArrCity);
        List<FlightDays> flightDays = new Flights().getFlightDays(UserData, depCity, arrCity, true, ResData.ResMain.PackType, string.Empty, string.Empty, ref errorMsg);
        var query = from q in flightDays
                    select new { Year = q.FlyDate.Year, Month = q.FlyDate.Month, Day = q.FlyDate.Day };
        return query.Count() > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(query) : string.Empty;
    }

    [WebMethod(EnableSession = true)]
    public static string changeDate(string newBeginDate, string dateFormat, bool? save)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        DateTime? begDate = Conversion.convertDateTime(newBeginDate, dateFormat);
        if (!begDate.HasValue) return string.Format(retVal, -10, HttpContext.GetGlobalResourceObject("Controls", "lblPleaseSelectReservationNewDate")).Replace('[', '{').Replace(']', '}');
        if (Equals(begDate, ResData.ResMain.BegDate)) return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        if (!new Reservation().changeReservationDate(UserData, ResData, begDate.Value, save.HasValue ? save.Value : false, ref errorMsg))
        {
            return string.Format(retVal, -1, errorMsg).Replace('[', '{').Replace(']', '}');
        }
        else
        {
            if (save.HasValue && save.Value)
            {
                new Reservation().reoFlightAllotResChangeData(ResData.ResMain.ResNo, ref errorMsg);
                return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
            }
            else
            {
                ResChgFeePrice resChgFee = new Reservation().getResChgFee(ResData, Convert.ToInt16(UserData.WebVersion) > 10 ? Convert.ToInt16(60) : Convert.ToInt16(30), true, ref errorMsg);
                if (resChgFee == null)
                    return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
                else
                {
                    decimal amounnt = resChgFee.ChgAmount.HasValue ? resChgFee.ChgAmount.Value : 0;
                    int pax = ResData.ResCust.Where(w => w.Title < 8 && w.Status == 0).Count();
                    string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                                    (amounnt * pax).ToString("#,###.0"), resChgFee.ChgCur);
                    return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
                }
            }
        }

    }
}
