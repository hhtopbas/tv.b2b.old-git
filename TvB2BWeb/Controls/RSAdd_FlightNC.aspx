﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSAdd_FlightNC.aspx.cs" Inherits="Controls_RSAdd_FlightNC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "RSAddFlight") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.datepick/jquery-ui-i18n.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />

    <link href="../CSS/RSAddStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        var region = '<%= twoLetterISOLanguageName %>' != 'en' ? '<%= twoLetterISOLanguageName %>' : '';
        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
        var addPleaseSelectDeparture = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectDeparture") %>';
        var addPleaseSelectArrival = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectArrival") %>';
        var addPleaseSelectFlight = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectFlight") %>';
        var addPleaseSelectClass = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectClass") %>';
        var addPleaseSelectTourist = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>';
        var addPleaseCorrectDate = '<%= GetGlobalResourceObject("Controls", "addPleaseCorrectDate") %>';

        $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }]
                });
            });
        }

        function flyDateChange(_TextBox, _PopCal) {
            var iDate = ''; //document.getElementById("< % = iDate.ClientID%>");
            var _TextBoxWeek = $("#iCheckOut");
            _TextBoxWeek.value = _TextBox.value;
            if ((!_TextBox) || (!_PopCal)) return
            var _format = _TextBox.getAttribute("Format");
            var _Date = _PopCal.getDate(_TextBox.value, _format);
            if (_Date) {
                iDate.value = Date.parse(_Date);
            }
        }

        function getDeparture() {
            var params = new Object();
            params.RecordType = $("#recordType").val();
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_FlightNC.aspx/getRouteFrom",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#sRouteFrom").append("<option value=''>" + ComboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#sRouteFrom").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMessage(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeRouteFrom() {
            $("#sRouteTo").html("");
            $("#sFlightNo").html("");
            $("#sClass").html("");
            getArrival();
        }

        function getArrival() {
            var params = new Object();
            params.RouteFrom = $("#sRouteFrom").val();
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_FlightNC.aspx/getRouteTo",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#sRouteTo").append("<option value=''>" + ComboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#sRouteTo").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMessage(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeRouteTo() {
            $("#sFlightNo").html("");
            $("#sClass").html("");
            getFlight();
            getClass();
        }

        function getFlight() {
            var params = new Object();
            params.RouteFrom = parseInt($("#sRouteFrom").val());
            params.RouteTo = parseInt($("#sRouteTo").val());
            params.FlyDate = $("#iFlyDate").datepicker('getDate') != null ? $("#iFlyDate").datepicker('getDate').getTime() : null;            
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_FlightNC.aspx/getFlights",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#sFlightNo").append("<option value=''>" + ComboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#sFlightNo").append("<option value='" + this.FlightNo + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getClass() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_FlightNC.aspx/getClass",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#sClass").append("<option value=''>" + ComboSelect + "</option>");
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        $.each(msg.d, function (i) {
                            $("#sClass").append("<option value='" + this.Class + "'>(" + this.Class + ") " + this.NameL + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function createTourist() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_FlightNC.aspx/getTourist",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#box1View").html("");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#box1View").append("<option value='" + this.CustNo + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            $(function () {
                $.configureBoxes();
            });
        }

        function preCalc(save) {
            if ($("#sRouteFrom").val() == null || $("#sRouteFrom").val() == '') {
                showMessage(addPleaseSelectDeparture);
                return;
            }
            if ($("#sRouteTo").val() == null || $("#sRouteTo").val() == '') {
                showMessage(addPleaseSelectArrival);
                return;
            }
            if ($("#sFlightNo").val() == null || $("#sFlightNo").val() == '') {
                showMessage(addPleaseSelectFlight);
                return;
            }
            if ($("#sClass").val() == null || $("#sClass").val() == '') {
                showMessage(addPleaseSelectClass);
                return;
            }
            var selectCust = '';
            $("#box1View option").each(function () {
                if (selectCust.length > 0) selectCust += "|";
                selectCust += $(this).val();
            });
            if (selectCust.length < 1) {
                showMessage(addPleaseSelectTourist);
                return;
            }            
            if ($("#iFlyDate").datepicker('getDate') == null) {
                showMessage(addPleaseCorrectDate);
                return;
            }            

            var _url = '';
            var params = new Object();
            if (save) {
                _url = '../Controls/RSAdd_FlightNC.aspx/SaveService';
                params.selectedCusts = selectCust;
                params.Departure = parseInt($("#sRouteFrom").val());
                params.Arrival = parseInt($("#sRouteTo").val());
                params.Flight = parseInt($("#sFlightNo").val());
                params.SClass = $("#sClass").val();
                params.FlyDate = $("#iFlyDate").datepicker('getDate') != null ? $("#iFlyDate").datepicker('getDate').getTime() : null;
                params.recordType = $("#recordType").val();                
            }
            else {
                _url = '../Controls/RSAdd_FlightNC.aspx/CalcService';                
                params.selectedCusts = selectCust;
                params.Departure = parseInt($("#sRouteFrom").val());
                params.Arrival = parseInt($("#sRouteTo").val());
                params.Flight = parseInt($("#sFlightNo").val());
                params.SClass = $("#sClass").val();
                params.FlyDate = $("#iFlyDate").datepicker('getDate') != null ? $("#iFlyDate").datepicker('getDate').getTime() : null;
                
            }
            $.ajax({
                type: "POST",
                url: _url,
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var result = $.json.decode(msg.d);
                    if (save) {
                        window.close;
                        self.parent.returnAddResServices(true, result.Supplier);
                    }
                    if (msg.d != '') {

                        if (result.Price != '') {
                            $("#iSalePrice").text(result.Price);
                            //$("#iSupplier").text(result.Supplier);
                            $("#iAdult").text(result.Adult);
                            $("#iChild").text(result.Child);
                            $("#iUnit").text(result.Unit);
                        } else showMessage(result.Supplier);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            if (source == 'save') {
                preCalc(true);
            }
            else
                if (source == 'cancel') {
                    window.close;
                    self.parent.returnAddResServices(false, '');
                }
        }

        function getFormData() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_FlightNC.aspx/getFormData",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        $("#iFlyDate").datepicker('setDate', eval(msg.d.flyDate.replace(/\/Date\((\d+)\)\//gi, "new Date($1)")));
                        if (msg.d.fromDate != null && msg.d.fromDate != '') {
                            $("#iFlyDate").datepicker("option", "minDate", eval(msg.d.fromDate.replace(/\/Date\((\d+)\)\//gi, "new Date($1)")));
                        }
                    }
                    getDeparture();
                    createTourist();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
                        showMessage(xhr.responseText);
                    }
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            $("#recordType").val($.query.get('recordType'));

            $.datepicker.setDefaults($.datepicker.regional[region]);

            $("#iFlyDate").datepicker({
                showOn: "button",
                buttonImage: "../Images/Calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true//,
                //beforeShowDay: excursionDays
            });
            getFormData();
        });
    </script>

</head>
<body>
    <form id="formRsFlight" runat="server">
        <input id="recordType" type="hidden" value="" />
        <br />
        <div id="divRs">
            <div>
                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="width: 405px;">
                            <div id="divRouteFrom" class="divs">
                                <div class="LeftDiv">
                                  <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "lblRouteFr")%>
                                    :</span>
                                </div>
                                <div id="divSLocation" class="inputDiv">
                                    <select id="sRouteFrom" class="combo" onchange="changeRouteFrom()">
                                    </select>
                                </div>
                            </div>
                            <div id="divRouteTo" class="divs">
                                <div class="LeftDiv">
                                  <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "lblRouteTo")%>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <select id="sRouteTo" class="combo" onchange="changeRouteTo()">
                                    </select>
                                </div>
                            </div>
                            <div id="divFlyDate" class="divs">
                                <div class="LeftDiv">
                                  <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "lblFlyDate")%>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <input id="iFlyDate" type="text" style="width: 100px;" />
                                    <%--<asp:TextBox ID="iFlyDate" runat="server" Width="100px" />
                                    <rjs:popcalendar id="ppcFlyDate" runat="server" control="iFlyDate" clientscriptondatechanged="flyDateChange" />--%>
                                </div>
                            </div>
                            <div id="divFlightNo" class="divs">
                                <div class="LeftDiv">
                                  <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewFlight")%>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <select id="sFlightNo" class="combo">
                                    </select>
                                </div>
                            </div>
                            <div id="divClass" class="divs">
                                <div class="LeftDiv">
                                  <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewClass")%>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <select id="sClass" class="combo">
                                    </select>
                                </div>
                            </div>
                            <div id="divAdultChild" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewAdult")%>
                                    :</span>
                                </div>
                                <div style="float: left; width: 100px; height: 100%; text-align: left;">
                                    <b>
                                        <asp:Label ID="iAdult" runat="server" /></b>
                                </div>
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewChild")%>
                                    :</span>
                                </div>
                                <div style="float: left; width: 100px; height: 100%; text-align: left;">
                                    <b>
                                        <asp:Label ID="iChild" runat="server" /></b>
                                </div>
                            </div>
                            <div id="divUnit" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewUnit")%>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <b>
                                        <asp:Label ID="iUnit" runat="server" /></b>
                                </div>
                            </div>
                        </td>
                        <td valign="top" style="border-left-style: solid; border-width: 1px; border-color: #000000">
                            <br />
                            <div id="divSalePrice">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSalePrice")%>
                                :</span>
                                <br />
                                <span style="font-size: 12pt;"><b>
                                    <asp:Label ID="iSalePrice" runat="server" /></b> </span>
                            </div>
                            <br />
                            <div id="divCalcBtn">
                                <input id="btnRecalc" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc")%>'
                                    onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                    style="width: 150px;" />
                            </div>
                            <asp:HiddenField ID="iDate" runat="server" />
                        </td>
                    </tr>
                </table>
                <br />
                <table>
                    <tr>
                        <td align="center">
                            <b>
                                <%= GetGlobalResourceObject("Controls", "lblSerTourist")%></b>
                        </td>
                        <td></td>
                        <td align="center">
                            <b>
                                <%= GetGlobalResourceObject("Controls", "lblOthTourist")%></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 290px; text-align: left;">
                            <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box1Filter" />
                            <button type="button" id="box1Clear">
                                X</button><br />
                            <select id="box1View" multiple="multiple" style="width: 100%; height: 125px;">
                            </select><br />
                            <%--<span id="box1Counter" class="countLabel"></span>--%>
                            <select id="box1Storage">
                            </select>
                        </td>
                        <td>
                            <button id="to2" type="button" style="width: 34px;">
                                >
                            </button>
                            <br />
                            <button id="allTo2" type="button" style="width: 34px;">
                                >>
                            </button>
                            <br />
                            <button id="allTo1" type="button" style="width: 34px;">
                                <<
                            </button>
                            <br />
                            <button id="to1" type="button" style="width: 34px;">
                                <
                            </button>
                        </td>
                        <td style="width: 290px; text-align: left;">
                            <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box2Filter" />
                            <button type="button" id="box2Clear">
                                X</button><br />
                            <select id="box2View" multiple="multiple" style="width: 100%; height: 125px;">
                            </select><br />
                            <%--<span id="box2Counter" class="countLabel"></span>--%>
                            <select id="box2Storage">
                            </select>
                        </td>
                    </tr>
                </table>
                <br />
                <table id="divBtn">
                    <tr>
                        <td>
                            <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave")%>'
                                onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                style="width: 100px;" />
                        </td>
                        <td>
                            <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel")%>'
                                onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                style="width: 100px;" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
            </p>
        </div>
    </form>
</body>
</html>
