﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using TvTools;

public partial class Controls_QuickPayment : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        
        Int16? AccountType = ResData.ResMain.PaymentFrom.HasValue ? ResData.ResMain.PaymentFrom.Value : ResData.ResMain.InvoiceTo;
        if (AccountType.HasValue && AccountType.Value == 1)
            AccountType = 3;
        string account = string.Empty;        
        account = ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Surname + " " + ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Name;
        List<CodeName> accountList = new List<CodeName>();
        accountList.Add(new CodeName { Code = "0", Name = UserData.AgencyRec.Name });
        accountList.Add(new CodeName { Code = "3", Name = account });
        accountList.Add(new CodeName { Code = "4", Name = "" });
        List<CodeName> accountTypeList = new List<CodeName>();
        accountTypeList.Add(new CodeName { Code = "0", Name = HttpContext.GetGlobalResourceObject("Payments", "AccountType0").ToString() });       
        accountTypeList.Add(new CodeName { Code = "3", Name = HttpContext.GetGlobalResourceObject("Payments", "AccountType3").ToString() });
        accountTypeList.Add(new CodeName { Code = "4", Name = HttpContext.GetGlobalResourceObject("Payments", "AccountType4").ToString() });
        List<PayTypeRecord> payTypeList = new Common().getPayType(UserData, UserData.Market, ref errorMsg);
        List<CurrencyRecord> currencyList = new Banks().getCurrencys(UserData.Market, ref errorMsg);
        var currList = from q in currencyList
                       select new { Curr = q.Code, q.Name };
        string defaultCurrency = ResData.ResMain.SaleCur;
        decimal? amount = ResData.ResMain.Balance;
        if (string.Equals(UserData.CustomRegID, Common.crID_FilipTravel))
        {
            defaultCurrency = "RSD";
        }
        if (!string.Equals(defaultCurrency, ResData.ResMain.SaleCur))
        {
            amount = new Common().Exchange(UserData.Market, DateTime.Today, ResData.ResMain.SaleCur, defaultCurrency, amount, true, ref errorMsg);
        }
        return new
        {
            iAccountType = AccountType,
            iAccountTypeList = accountTypeList,
            iAccountList = accountList,
            iPaymentTypeList = payTypeList,
            iPayAmountCurList = currList,
            iSaleCurr = defaultCurrency,
            iAmount = amount,
            iPaidDate = DateTime.Today.ToShortDateString()
        };
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object changeAmountByPaymentType(string pPayTypeCode,string pPayCurrency)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        List<PayTypeRecord> payTypeList = new Common().getPayType(UserData, UserData.Market, ref errorMsg);
        var payType = payTypeList.FirstOrDefault(w => w.Code == pPayTypeCode);
        decimal? amount = ResData.ResMain.Balance;
        if (payType != null)
        {
           
            if (!string.Equals(pPayCurrency, ResData.ResMain.SaleCur))
            {
                amount = new Common().Exchange(UserData.Market, DateTime.Today, ResData.ResMain.SaleCur, pPayCurrency, amount, true, ref errorMsg,payType.RateType);
            }
        }
        return new { Amount = amount };
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object setPayment(string AccountType, string Account, string PaymentType, string PayAmount, string PayAmountCur, string Explanation, string Reference)
    {
         if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string decimalSeperator = PayAmount.IndexOf(",") > -1 ? "," : (PayAmount.IndexOf(".") > -1 ? "." : "");
        string payAmount = string.Empty;
        if (decimalSeperator != "")
            payAmount = PayAmount.Substring(0, PayAmount.IndexOf(decimalSeperator, 0));
        else
            payAmount = PayAmount;
        string payAmountDecimal = PayAmount.Substring(PayAmount.IndexOf(decimalSeperator, 0) + 1);
        string payAmountStr = payAmount + (decimalSeperator != "" ? UserData.Ci.NumberFormat.CurrencyDecimalSeparator + payAmountDecimal : "");

        decimal? amount = Conversion.getDecimalOrNull(payAmountStr);
        decimal? paymentAmount= Conversion.getDecimalOrNull(payAmountStr);
        decimal? amount_ = amount;
        List<PayTypeRecord> payTypeList = new Common().getPayType(UserData, UserData.Market, ref errorMsg);
        var payType = payTypeList.FirstOrDefault(w => w.Code == PaymentType);

        if (ResData.ResMain.SaleCur != PayAmountCur)
        {
            amount_ = new Common().Exchange(UserData.Market, DateTime.Today,PayAmountCur, ResData.ResMain.SaleCur, paymentAmount, true, ref errorMsg,payType.RateType);
        }
        int? journalID = null;
        string xml = string.Empty;
        xml += "<root>";        
        xml += string.Format("<ResPayment ResNo=\"{0}\" Payment=\"{1}\" />", ResData.ResMain.ResNo, paymentAmount.HasValue ? (paymentAmount.Value * 100).ToString("#.") : "");        
        xml += "</root>";
        string referances = Reference;
        if (new Payments().createPaymentList(UserData, paymentAmount, PayAmountCur, PaymentType, xml, ref journalID, referances, Conversion.getInt16OrNull(AccountType)))
        {            
            new Reservation().clearOptionDate(UserData, ResData.ResMain.ResNo, ref errorMsg);
            bool? sendSMS = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "SendSMS"));
            if (sendSMS.HasValue && sendSMS.Value)
            {
                Sms sms = new Sms();
                ResData.ResMain.PasAmount = amount_;
                sms.AddSmsQueue(UserData.Market, ResData, SmsBo.SmsType.PaymentReceived, UserData.UserName, ResData.ResCust.Where(w => w.Leader == "Y").FirstOrDefault().Phone, ref errorMsg);
            }
            return new 
            {
                isPayment = true,
                Message = "Payment successful."
            };
        }
        else
        {
            return new
            {
                isPayment = false,
                Message = "Payment could not be received."
            };
        }        
    }

}