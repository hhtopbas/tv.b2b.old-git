﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSEdit_Transfer.aspx.cs"
    Inherits="Controls_RSEdit_Transfer" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "RSEditTransfer") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    
    <link href="../CSS/RSEditStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        var newRes = $.query.get('NewRes') == '1' ? true : false;

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {
            $(function() {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function() {
                            $(this).dialog('close');
                        }
                    }
                });
            });
        }

        function getService(recID, service) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Transfer.aspx/getTransfers",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#sTransfer").html('');
                    $("#sTransfer").append("<option value=''>" + '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>' + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function(i) {
                            $("#sTransfer").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                        $("#sTransfer").val(service);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function preCalc(save) {
            if ($("#sTransfer").val() == null || $("#sTransfer").val() == '') {
                showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTransfer") %>');
                return;
            }
            var hfRecID = document.getElementById("<%= hfRecID.ClientID %>");
            var _data = '{"RecID":"' + hfRecID.value + '"' +
                         ',"Transfer":"' + $("#sTransfer").val() + '"' +
                         ',"PickupTimeHH":"' + $("#iPickupTimeHH").val() + '"' +
                         ',"PickupTimeSS":"' + $("#iPickupTimeSS").val() + '"' +
                         ',"PickupNote":"' + $("#iPickupNote").val() + '"' +
                         ',"SupNote":"' + $("#iSupplierNote").val() + '"' +
                         ',"newRes":' + newRes + '}';
            var _url = "";
            if (save)
                _url = "../Controls/RSEdit_Transfer.aspx/SaveService";
            else _url = "../Controls/RSEdit_Transfer.aspx/CalcService"
            $.ajax({
                type: "POST",
                url: _url,
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (save) {
                        window.close;
                        self.parent.returnEditResServices(true);
                    }
                    if (msg.d != '') {
                        var result = $.json.decode(msg.d);
                        if (result.Price != '') {
                            $("#iSalePrice").text(result.Price);
                            $("#iSupplier").text(result.Supplier);
                            $("#iAdult").text(result.Adult);
                            $("#iChild").text(result.Child);
                            $("#iUnit").text(result.Unit);
                        } else showMessage(result.Supplier);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            if (source == 'save') {
                if ($("#sTransfer").val() == null || $("#sTransfer").val() == '') {
                    showMessage('<%= GetGlobalResourceObject("Controls", "addPleaseSelectTransfer") %>');
                    return;
                }
                var hfRecID = document.getElementById("<%= hfRecID.ClientID %>");
                var _data = '{"RecID":"' + hfRecID.value + '"' +
                         ',"Transfer":"' + $("#sTransfer").val() + '"' +
                         ',"PickupTimeHH":"' + $("#iPickupTimeHH").val() + '"' +
                         ',"PickupTimeSS":"' + $("#iPickupTimeSS").val() + '"' +
                         ',"PickupNote":"' + $("#iPickupNote").val() + '"}';
                if (newRes != true) {
                    $.ajax({
                        type: "POST",
                        url: "../Controls/RSEdit_Transfer.aspx/getChgFee",
                        data: _data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(msg) {
                            if (msg.d != '') {
                                var retVal = $.json.decode(msg.d);
                                if (retVal.retVal == 1) {
                                    $(function() {
                                        $("#messages").html(retVal.Message);
                                        $("#dialog").dialog("destroy");
                                        $("#dialog-message").dialog({
                                            modal: true,
                                            buttons: {
                                                '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>': function() {
                                                    $(this).dialog('close');
                                                    preCalc(true);
                                                },
                                                '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>': function() {
                                                    $(this).dialog('close');
                                                }
                                            }
                                        });
                                    });
                                }
                                else preCalc(true);
                            }
                        },
                        error: function(xhr, msg, e) {
                            if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                                showMessage(xhr.responseText);
                        },
                        statusCode: {
                            408: function() {
                                logout();
                            }
                        }
                    });
                } else preCalc(true);
            }
            else
                if (source == 'cancel') {
                window.close;
                self.parent.returnEditResServices(false);
            }
        }

        function getServiceDetail(recID) {
            $.ajax({
                type: "POST",
                url: "../Controls/RSEdit_Transfer.aspx/getServiceDetail",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var resService = $.json.decode(msg.d);
                        $("#iBegDate").text(resService.BegDate);
                        $("#sDepLocation").text(resService.DepLocation);
                        $("#sArrLocation").text(resService.ArrLocation);
                        $("#iAdult").text(resService.Adult);
                        $("#iChild").text(resService.Child);
                        $("#iNight").text(resService.Duration);
                        $("#iUnit").text(resService.Unit);
                        $("#iPickupNote").val(resService.PickupNote);
                        $("#iPickupTimeHH").val(resService.PickupTimeHH);
                        $("#iPickupTimeSS").val(resService.PickupTimeSS);
                        if (resService.StatConf != '')
                            $("#iConfirmation").text(resService.StatConf);
                        else $("#divConfirmation").hide();
                        $("#sStatus").text(resService.StatSer);
                        var salePrice = resService.SalePrice + ' ' + resService.SaleCur;
                        $("#iSalePrice").text(salePrice);
                        if (resService.SupNoteShow == true) {
                            $("#divSupplierNote").show();
                            $("#iSupplierNote").val(resService.SupNote);
                        }
                        else $("#divSupplierNote").hide();
                        getService(recID, resService.Service);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function onLoad() {
            $.query = $.query.load(location.href);
            var recID = $.query.get('RecID');
            var hfRecID = document.getElementById("<%= hfRecID.ClientID %>");
            hfRecID.value = recID;
            getServiceDetail(recID);
        }
        
    </script>

</head>
<body onload="onLoad();">
    <form id="formRsHotel" runat="server">
    <br />
    <div id="divRs">
        <div>
            <table>
                <tr>
                    <td class="leftCell">
                        <div id="divRouteFrom" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewFrom") %>
                                    :</span>
                            </div>
                            <div id="divDepLocation" class="inputDiv">
                                <b><span id="sDepLocation"></span></b>
                            </div>
                        </div>
                        <div id="divRouteTo" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewTo") %>
                                    :</span>
                            </div>
                            <div id="divArrLocation" class="inputDiv">
                                <b><span id="sArrLocation"></span></b>
                            </div>
                        </div>
                        <div id="divDate" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewTransferDate") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iBegDate"></span></b>
                            </div>
                        </div>
                        <div id="divTransfer" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewTransfer") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <select id="sTransfer">
                                </select>
                            </div>
                        </div>
                        <div id="divAdultChild" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                    :</span>
                            </div>
                            <div class="divAdultChild" style="text-align: left;">
                                <b><span id="iAdult"></span></b>
                            </div>
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                    :</span>
                            </div>
                            <div class="divAdultChild" style="text-align: left;">
                                <b><span id="iChild"></span></b>
                            </div>
                        </div>
                        <div id="divUnit" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewUnit") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iUnit"></span></b>
                            </div>
                        </div>
                        <div id="divPickupTime" class="divs" style="display: none;">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewPickupTime") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <asp:TextBox ID="iPickupTimeHH" runat="server" Width="30px" />:
                                <asp:TextBox ID="iPickupTimeSS" runat="server" Width="30px" />
                            </div>
                        </div>
                        <div id="divPickupNote" class="divs" style="display: none;">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewPickupNote") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <asp:TextBox ID="iPickupNote" runat="server" Width="275px" />
                            </div>
                        </div>
                        <%--<div id="divSupplier" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplier") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplier"></span></b>
                            </div>
                        </div>--%>
                        <div id="divSupplierNote" class="divs" style="display:none;">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <input id="iSupplierNote" type="text" style="width: 99%;" />
                            </div>
                        </div>
                    </td>
                    <td class="rightCell">
                        <div id="divStatus">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewStatus") %></span>
                            <br />
                            <b><span id="sStatus"></span></b>
                        </div>
                        <br />
                        <div id="divConfirmation">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewConfirmation") %></span>
                            <br />
                            <b><span id="sConfirmation"></span></b>
                        </div>
                        <br />
                        <div id="divSalePrice">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewSalePrice") %></span>
                            <br />
                            <span id="iSalePrice" class="salePrice"></span>
                        </div>
                        <br />
                        <div id="divCalcBtn">
                            <input id="btnRecalc" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                                onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                style="width: 150px;" />
                        </div>
                        <asp:HiddenField ID="hfRecID" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <table id="divBtn">
            <tr>
                <td style="width: 50%;" align="center">
                    <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
                        onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
                <td align="center">
                    <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                        onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                        style="width: 100px;" />
                </td>
            </tr>
        </table>
    </div>
    <div id="dialog-message" title="" style="display: none;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;">
            </span><span id="messages">Message</span>
        </p>
    </div>
    </form>
</body>
</html>
