﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ServiceChangeRoomAndAccom.aspx.cs"
    Inherits="Controls_ServiceChangeRoomAndAccom" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "ServiceChangeRoomAndAccom")%></title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .showTourist { display: block; visibility: visible; }
        .hideTourist { display: none; visibility: hidden; }
    </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function getRoomAccomList(serviceID, adult, child, custNo) {
            var _data = '{"ServiceID":"' + serviceID + '","Adult":"' + adult + '","Child":"' + child + '"}';
            $.ajax({
                type: "POST",
                url: "../Controls/ServiceChangeRoomAndAccom.aspx/getAvailableRoomAndAccom",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#gridRoomAccom").html('');
                    var roomList = $.json.decode(msg.d);
                    var _html = '';
                    _html += '<div id="selectRoomAccom">';
                    if (roomList.length > 0) {
                        $.each(roomList, function(i) {
                            _html += '<label><input type="radio" value="' + this.Room + '|' + this.Accom + '" name="rooms" />' + this.RoomName + ' ' + this.AccomName + '</label><br />';
                        });
                    }
                    else {
                        _html += '<%= GetGlobalResourceObject("LibraryResource","NoPaxNoRoom").ToString() %>';
                    }
                    _html += '</div>';
                    _html += '<div style="text-align:center;">';
                    if (roomList.length > 0)
                        _html += '<input type="button" value=">> <%= GetGlobalResourceObject("Controls", "lblNext") %> >>" onclick="nextStep();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />&nbsp;&nbsp;';
                    _html += '<input type="button" value=<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %> onclick="canceled();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />';
                    _html += '</div>';
                    $("#gridRoomAccom").html(_html);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function pageLoad() {
            $.query = $.query.load(location.href);
            var serviceID = $.query.get('ServiceID');
            var adult = $.query.get('Adult');
            var child = $.query.get('Child');
            var custNo = $.query.get('CustNo');
            getRoomAccomList(serviceID, adult, child, custNo);
        }

        function nextStep() {
            var roomAccom = $('input[name=rooms]:checked').val();
            if (roomAccom == '') return false;
            $.query = $.query.load(location.href);
            var serviceID = $.query.get('ServiceID');
            var custNo = $.query.get('CustNo');
            var room = roomAccom.split('|')[0];
            var accom = roomAccom.split('|')[1];

            var _data = '{"ServiceID":"' + serviceID + '","Room":"' + room + '","Accom":"' + accom + '"}';
            $.ajax({
                type: "POST",
                url: "../Controls/ServiceChangeRoomAndAccom.aspx/changeRoomsAccom",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d == '') {
                        window.close;
                        self.parent.changedRoomAccom('', serviceID, custNo);
                    }
                    else {
                        alert(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });

        }

        function canceled() {
            window.close;
            self.parent.cancelAddARoom();
        }
        
    </script>

</head>
<body onload="javascript:pageLoad();">
    <form id="selectRoomForm" runat="server">
    <div style="font-family: Arial; font-size: 9pt; width: 380px;">
        <div id="gridRoomAccom">
        </div>
    </div>
    </form>
</body>
</html>
