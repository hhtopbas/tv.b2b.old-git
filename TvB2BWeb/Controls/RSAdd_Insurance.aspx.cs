﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSAdd_Insurance : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ppcCheckIn.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckIn.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcCheckIn.From.Date = DateTime.Today;
        ppcCheckOut.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckOut.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        ppcCheckOut.From.Date = DateTime.Today;

        if (!IsPostBack)
        {
            ppcCheckIn.DateValue = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
            ppcCheckOut.DateValue = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
            // iNight.Text = (ppcCheckOut.DateValue - ppcCheckIn.DateValue).Days.ToString();
        }
    }

    [WebMethod]
    public static string getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        DateTime ppcCheckIn = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
        DateTime ppcCheckOut = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
        string iNight = (ppcCheckOut - ppcCheckIn).Days.ToString();
        return iNight;
    }

    [WebMethod]
    public static object getCountryList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        int? pCountry = new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg);
        if (string.Equals(UserData.CustomRegID, Common.crID_FilipTravel))
            pCountry = null;
        List<InsurancePriceRecord> insurancePrice = new Insurances().getInsurancePrice(UserData.Market, ResData.ResMain.PLMarket, pCountry, ref errorMsg);
        var query = from q in insurancePrice
                    group q by new { RecID = q.Country.HasValue ? q.Country.Value : -1, Name = q.Country.HasValue ? q.CountryNameL : HttpContext.GetGlobalResourceObject("LibraryResource", "AllLocation").ToString() } into k
                    select new { RecID = k.Key.RecID, Name = k.Key.Name };
        return query;
    }

    [WebMethod]
    public static object getInsurance(string Country)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? country = Conversion.getInt32OrNull(Country);
        if (country < 0)
            country = null;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;

        decimal? Amount = null; // ResData.ResMain.PasPayable - ResData.ResService.SalePrice;
        Int16? RemainDay = Convert.ToInt16((DateTime.Today - ResData.ResMain.ResDate.Value).TotalDays);
        if (RemainDay.HasValue && RemainDay.Value < 0)
            RemainDay = null;
        List<InsuranceRecord> insuranceList = new Insurances().getInsuranceForCountry(UserData, ResData.ResMain.PLMarket, country, RemainDay, Amount, ref errorMsg);
        
        var query = from q in insuranceList
                    where q.B2BPub.HasValue && q.B2BPub.Value == true 
                    select new { Code = q.Code, Name = q.LocalName };

        var query1 = from q in insuranceList
                     where q.InsType != 1 && q.B2BPub.HasValue && q.B2BPub.Value == true 
                     select new { Code = q.Code, Name = q.LocalName };

        return Equals(ResData.ResMain.PaymentStat, "U") ? query : query1;
    }

    [WebMethod]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    where s.Status == 0
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod]
    public static string CalcService(string selectedCusts, string Country, string Insurance, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        int? country = Conversion.getInt32OrNull(Country);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);
        if (!begDate.HasValue && !endDate.HasValue) return retVal;

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "INSURANCE", Insurance, string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, country, country, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
            if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            {
                ResServiceRecord resServ = ResData.ResService.Find(f => f.RecID == ServiceID);
                if (resServ != null && resServ.SalePrice.HasValue && resServ.SalePrice.Value > 0)
                {
                    SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resServ.Supplier != null ? resServ.Supplier : "", ref errorMsg);
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", resServ.SalePrice.Value.ToString("#,###.00") + " " + resServ.SaleCur, supplierRec.NameL);
                }
                else                 
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, HttpContext.GetGlobalResourceObject("SpCalcErrors", "CalcService5"));                
            }
            else
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            //DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "INSURANCE", ServiceID, ref errorMsg);
            //SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
            //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", returnData.Rows[0]["SalePrice"].ToString() + " " + returnData.Rows[0]["SaleCur"].ToString(), supplierRec.NameL);
        }
        return "{" + retVal + "}";
    }

    [WebMethod]
    public static string SaveService(string selectedCusts, string Country, string Insurance, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat, string recordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        int? country = Conversion.getInt32OrNull(Country);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);
        if (!begDate.HasValue && !endDate.HasValue) return retVal;

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "INSURANCE", Insurance,
            string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, country, country, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            if (_recordType == true)
            {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                {
                    List<ResServiceRecord> resServiceList = ResData.ResService;
                    ResServiceRecord resService = resServiceList.LastOrDefault();
                    resService.ExcludeService = false;
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
                else
                {
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
            }
            else
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message;
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
            }
        }
        return "{" + retVal + "}";
    }

    [WebMethod]
    public static string PaxControl(string selectedCusts)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }
        retVal = string.Format("\"Adult\":{0},\"Child\":{1}", AdlCnt.ToString(), ChdCnt.ToString());
        return "{" + retVal + "}";
    }

}
