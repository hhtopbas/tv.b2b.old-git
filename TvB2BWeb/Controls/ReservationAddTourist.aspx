﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReservationAddTourist.aspx.cs"
    Inherits="ReservationAddTourist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "ReservationAddTourist")%></title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>

    <script src="../Scripts/Tv.Utils.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/ReservationAddTourist.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .showTourist { display: block; visibility: visible; }
        .hideTourist { display: none; visibility: hidden; }
    </style>

    <script language="javascript" type="text/javascript">

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        $(document).ready(function() {
            $("#dialog-changeRoomAccom").dialog(
            {
                autoOpen: false,
                modal: true,
                width: 420,
                height: 390,
                resizable: true,
                autoResize: true
            });
        });

        $(document).ready(function() {
            $("#dialog-addRoomAccom").dialog(
            {
                autoOpen: false,
                modal: true,
                width: 450,
                height: 390,
                resizable: true,
                autoResize: true
            });
        });

        function RoomAddPax(RoomRecID, _mask) {
            $.ajax({
                type: "POST",
                url: "../Controls/ReservationAddTourist.aspx/getTourist",
                data: '{"ServiceID":"' + RoomRecID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#selectRoom").html('');
                    $("#selectRoom").html(msg.d);
                    $("#txtBirthDate").mask(_mask);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function getRoomList() {
            $.ajax({
                type: "POST",
                url: "../Controls/ReservationAddTourist.aspx/getHotelRooms",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d != '') {
                        var retVal = $.json.decode(msg.d.replace('<[', '{').replace(']>', '}'));
                        $("#hfDate").val(retVal.dateMask);
                        if (retVal.Continue == "0") {
                            $("#selectRoom").html('');
                            $("#selectRoom").html(retVal.data.replace(/~/g, '"'));
                        }
                        else if (parseInt(retVal.Continue) < 0) {
                            RoomAddPax(-1, $("#hfDate").val());
                        }
                        else if (parseInt(retVal.Continue) > 0) {
                            alert(retVal.data);
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function cancelAddRoomAccom() {
            $("#dialog-addRoomAccom").dialog("close");
            window.close;
            self.parent.returnAddTourist(false, null);
        }

        function returnAddRoomAccom(hotel, room, accom, adult, child, recID) {
            $("#dialog-addRoomAccom").dialog("close");
            var datemask = $("#hfDate").val();
            RoomAddPaxs(adult, child, hotel, room, accom, datemask, recID);
        }

        function showAddRoomAccom(Pax) {
            $("#dialog-addRoomAccom").dialog("open");
            $("#addRoomAccom").attr("src", "../Controls/AddRoomAndAccom.aspx?Pax=" + Pax);
            return false;
        }

        function ChangeRoomAndAccomType(ServiceID, Adult, Child) {
            $("#dialog-changeRoomAccom").dialog("open");
            $("#changeRoomAccom").attr("src", "../Controls/ServiceChangeRoomAndAccom.aspx?ServiceID=" + ServiceID + "&Adult=" + Adult + "&Child=" + Child + "&CustNo=-1");
            return false;
        }

        function cancelAddARoom() {
            $("#dialog-changeRoomAccom").dialog("close");
            window.close;
            self.parent.returnAddTourist(false, null);
        }

        function changedRoomAccom(retValues, serviceID, custNo) {
            $("#dialog-changeRoomAccom").dialog("close");
            if (retValues != '') {
                alert(retValues);
            }
            else
                AddPax(serviceID);
        }

        function AddPax(ServiceID) {
            if ($("#txtSurname") == null || $("#txtSurname").val() == '') {
                alert('<%= HttpContext.GetGlobalResourceObject("LibraryResource", "EnterPasNameSurname") %>');
                return;
            }

            if ($("#txtName") == null || $("#txtName").val() == '') {
                alert('<%= HttpContext.GetGlobalResourceObject("LibraryResource", "EnterPasNameSurname") %>');
                return;
            }
            if ((parseInt($("#cbTitle").val()) > 5) && $("#txtBirthDate").val() == '') {
                alert('<%= HttpContext.GetGlobalResourceObject("LibraryResource", "EnterChildOrInfantBirtDay") %>');
                return;
            }

            //            var _data = '';
            //            _data += '"Title":"' + $("#cbTitle").val() + '"';
            //            _data += ',"Surname":"' + $("#txtSurname").val() + '"';
            //            _data += ',"Name":"' + $("#txtName").val() + '"';
            //            _data += ',"BirthDate":"' + $("#txtBirthDate").val() + '"';
            //            _data += ',"IDNo":"' + $("#txtIDNo").val() + '"';
            //            _data += ',"PassSerie":"' + $("#txtPassSerie").val() + '"';
            //            _data += ',"PassNo":"' + $("#txtPassNo").val() + '"';
            //            _data += ',"ServiceID":"' + ServiceID + '"';

            var inData = new Object();
            inData.Title = parseInt($("#cbTitle").val());
            inData.Surname = $("#txtSurname").val();
            inData.Name = $("#txtName").val();
            inData.BirthDate = $("#txtBirthDate").val();
            inData.IDNo = $("#txtIDNo").val();
            inData.PassSerie = $("#txtPassSerie").val();
            inData.PassNo = $("#txtPassNo").val();
            inData.ServiceID = parseInt(ServiceID);

            $.ajax({
                type: "POST",
                url: "../Controls/ReservationAddTourist.aspx/AddPax",
                data: $.json.encode(inData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var retVal = msg.d;
                        if (retVal.reCalc == null) {
                            window.close;
                            self.parent.returnAddTourist(true, retVal.custNo);
                        }
                        else {
                            if (retVal.reCalc == true) {
                                var adult = retVal.Pax;
                                var child = retVal.Child;
                                ChangeRoomAndAccomType(ServiceID, adult, child);
                            }
                            else alert(retVal.Msg);
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function RoomAddPaxs(adult, child, hotel, room, accom, _mask, recID) {
            $.ajax({
                type: "POST",
                url: "../Controls/ReservationAddTourist.aspx/getTourists",
                data: '{"Adult":"' + adult + '","Child":"' + child + '","Hotel":"' + hotel + '","Room":"' + room + '","Accom":"' + accom + '","RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#selectRoom").html('');
                    $("#selectRoom").html(msg.d);
                    var paxCnt = parseInt(adult) + parseInt(child);
                    var msk = $("#hfDate").val();
                    for (var i = 1; i <= paxCnt; i++) {
                        $("#txtBirthDate" + i).mask(msk);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function AddPaxRoom(paxCount, Hotel, Room, Accom, RecID) {
            var paxCnt = parseInt(paxCount);
            var _data = '';
            var paxs = [];
            for (var i = 1; i <= paxCnt; i++) {
                if ($("#txtSurname" + i) == null || $("#txtSurname" + i).val() == '') {
                    alert('<%= HttpContext.GetGlobalResourceObject("LibraryResource", "EnterPasNameSurname") %>');
                    return;
                }

                if ($("#txtName" + i) == null || $("#txtName" + i).val() == '') {
                    alert('<%= HttpContext.GetGlobalResourceObject("LibraryResource", "EnterPasNameSurname") %>');
                    return;
                }
                if ((parseInt($("#cbTitle" + i).val()) > 5) && $("#txtBirthDate" + i).val() == '') {
                    alert('<%= HttpContext.GetGlobalResourceObject("LibraryResource", "EnterChildOrInfantBirtDay") %>');
                    return;
                }
                var paxsObj = new Object();
                paxsObj.Title = parseInt($("#cbTitle" + i).val());
                paxsObj.Surname = $("#txtSurname" + i).val();
                paxsObj.Name = $("#txtName" + i).val();
                paxsObj.BirthDate = $("#txtBirthDate" + i).val();
                paxsObj.IDNo = $("#txtIDNo" + i).val();
                paxsObj.PassSerie = $("#txtPassSerie" + i).val();
                paxsObj.PassNo = $("#txtPassNo" + i).val();
                paxs.push(paxsObj);                
//                if (_data.length > 0) _data += ',';
//                _data += '<!Title!:!' + $("#cbTitle" + i).val() + '!';
//                _data += ',!Surname!:!' + $("#txtSurname" + i).val() + '!';
//                _data += ',!Name!:!' + $("#txtName" + i).val() + '!';
//                _data += ',!BirthDate!:!' + $("#txtBirthDate" + i).val() + '!';
//                _data += ',!IDNo!:!' + $("#txtIDNo" + i).val() + '!';
//                _data += ',!PassSerie!:!' + $("#txtPassSerie" + i).val() + '!';
//                _data += ',!PassNo!:!' + $("#txtPassNo" + i).val() + '!>';
            }
            var data = new Object();
            data.Hotel = Hotel;
            data.Room = Room;
            data.Accom = Accom;
            data.RecID = RecID;
            data.paxs = paxs;
            
//            _data += "@";
//            _data += '<!Hotel!:!' + Hotel + '!';
//            _data += ',!Room!:!' + Room + '!';
//            _data += ',!Accom!:!' + Accom + '!';
//            _data += ',!RecID!:!' + RecID + '!>';

            $.ajax({
                type: "POST",
                url: "../Controls/ReservationAddTourist.aspx/AddPaxs",
                data: $.json.encode(data),//'{"data":"' + _data + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var retVal = msg.d;
                        if (retVal.reCalc == null) {
                            window.close;
                            self.parent.returnAddTourist(true, retVal.custNo);
                        }
                        else {
                            if (retVal.reCalc == true) {

                            }
                            else alert(retVal.Msg);
                        }
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function addRoomAndPax() {
            var addPaxCount = $("#addPaxCount").val();
            showAddRoomAccom(addPaxCount);
        }

        function pageLoad() {
            getRoomList();
        }
        
    </script>

</head>
<body onload="javascript:pageLoad();">
    <form id="ReservationAddTouristForm" runat="server">
    <div id="divSelectRoom">
        <div id="selectRoom">
        </div>
        <input id="hfDate" type="hidden" />
    </div>
    <div id="dialog-changeRoomAccom" title='<%= GetGlobalResourceObject("ResView", "lblChangeRoomOrAccommodation")%>'
        style="display: none;">
        <iframe id="changeRoomAccom" runat="server" height="100%" width="100%" frameborder="0"
            style="clear: both;"></iframe>
    </div>
    <div id="dialog-addRoomAccom" title='<%= GetGlobalResourceObject("LibraryResource", "AddRoom")%>'
        style="display: none;">
        <iframe id="addRoomAccom" runat="server" height="100%" width="100%" frameborder="0"
            style="clear: both;"></iframe>
    </div>
    </form>
</body>
</html>
