﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSView_Transfer.aspx.cs"
    Inherits="Controls_RSView_Transfer" EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <title><%= GetGlobalResourceObject("PageTitle", "RSViewTransfer") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
   
    
    <link href="../CSS/ServiceEditView.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function onLoad() {
            $.query = $.query.load(location.href);
            var resNo = $.query.get('ResNo');
            var recID = $.query.get('RecID');
            $.ajax({
                type: "POST",
                url: "../Services/ReservationServices.asmx/getResService",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    var resService = $.json.decode(msg.d);                    
                    $("#iDate").html(resService.BegDate);
                    $("#iTransfer").html(resService.ServiceNameL);
                    $("#sDepLocation").html(resService.DepLocationNameL);
                    $("#sArrLocation").html(resService.ArrLocationNameL);
                    $("#iAdult").html(resService.Adult);
                    $("#iChild").html(resService.Child);
                    $("#iUnit").html(resService.Unit);
                    if (resService.PickupTime != null)
                        $("#iPickupTime").html(resService.PickupTime);
                    else $("#iPickupTime").html("");
                    $("#iPickupNote").html(resService.PickupNote);
                    $("#iDepartureCode").html(resService.TrfDTName);
                    $("#iDepartureDescription").html(resService.TrfDepNameL);
                    $("#iArrivalCode").html(resService.TrfATName);
                    $("#iArrivalDescription").html(resService.TrfArrNameL);
                    if (resService.StatConfNameL != '')
                        $("#sConfirmation").html(resService.StatConfNameL);
                    else $("#divConfirmation").hide();
                    $("#sStatus").html(resService.StatSerNameL);
                    var salePrice = resService.IncPack == 'Y' ? '<%= GetGlobalResourceObject("LibraryResource", "InPackage").ToString() %>' : resService.SalePrice + ' ' + resService.SaleCur;
                    $("#iSalePrice").html(salePrice);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }
    </script>

</head>
<body onload="onLoad()">
    <form id="formRsTransfer" runat="server">
    <div id="divRsTransfer">
        <div>
            <table>
                <tr>
                    <td class="leftCell">
                        <div id="divRoute" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewTransferRoute") %></span>
                            </div>
                            <div id="divLocations">
                                <b><span id="sDepLocation"></span>&nbsp;-&gt;&nbsp; <span id="sArrLocation"></span>
                                </b>
                            </div>
                        </div>
                        <div id="divDate" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewTransferDate") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iDate"></span></b>
                            </div>
                        </div>
                        <div id="divFlightClass" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewTransfer") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iTransfer"></span></b>
                            </div>
                        </div>
                        <div id="divAdultChild" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewAdult") %></span>
                            </div>
                            <div class="divAdultChild">
                                <b><span id="iAdult"></span></b>
                            </div>
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewChild") %></span>
                            </div>
                            <div class="divAdultChild">
                                <b><span id="iChild"></span></b>
                            </div>
                        </div>
                        <div id="divUnit" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewUnit") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iUnit"></span></b>
                            </div>
                        </div>
                        <div id="divPickupTime" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewPickupTime") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iPickupTime"></span></b>
                            </div>
                        </div>
                        <div id="divPickupNote" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewPickupNote") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iPickupNote"></span></b>
                            </div>
                        </div>
                        <div id="divDeparture" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewDeparture") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iDepartureCode"></span>&nbsp;-&nbsp; <span id="iDepartureDescription"></span>
                                </b>
                            </div>
                        </div>
                        <div id="divArrival" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewArrival") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iArrivalCode"></span>&nbsp;-&nbsp; <span id="iArrivalDescription"></span>
                                </b>
                            </div>
                        </div>
                        <%--<div id="divSupplier" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplier") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iSupplier"></span></b>
                            </div>
                        </div>
                        <div id="divSupplierNote" class="divNotes">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %></span>
                            </div>
                            <div class="RightDiv">
                                <b><span id="iSupplierNote"></span></b>
                            </div>
                        </div>--%>
                    </td>
                    <td class="rightCell">
                        <div id="divStatus">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewStatus") %></span>
                            <br />
                            <b><span id="sStatus"></span></b>
                        </div>
                        <br />
                        <div id="divConfirmation">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewConfirmation") %></span>
                            <br />
                            <b><span id="sConfirmation"></span></b>
                        </div>
                        <br />
                        <div id="divSalePrice">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewSalePrice") %></span>
                            <br />
                            <span id="iSalePrice" class="salePrice"></span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
