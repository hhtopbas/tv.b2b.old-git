﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSAdd_Hotel.aspx.cs" Inherits="Controls_RSAdd_Hotel"
    EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
    <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "RSAddHotel") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/RSAddStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var LastDateSmall = '<%= GetGlobalResourceObject("LibraryResource","LastDateSmall") %>';
        var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
        var addPleaseSelectHotel = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectHotel") %>';
        var addPleaseSelectRoom = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectRoom") %>';
        var addPleaseSelectAccom = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectAccom") %>';
        var addPleaseSelectBoard = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectBoard") %>';
        var addPleaseSelectTourist = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>';
        var addPleaseCorrectDate = '<%= GetGlobalResourceObject("Controls", "addPleaseCorrectDate") %>';
        var HotelAccomNotFound = '<%= GetGlobalResourceObject("LibraryResource","HotelAccomNotFound") %>';
        var HotelAccomPaxError = '<%= GetGlobalResourceObject("LibraryResource","HotelAccomPaxError") %>';


        $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>';
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        function logout() {
            self.parent.logout();
        }

        function showMessage(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: [{
                        text: btnOK,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }]
                });
            });
        }

        function CheckInDateChange(_TextBox, _PopCal) {
            var _TextBoxWeek = $("#iCheckOut");
            _TextBoxWeek.value = _TextBox.value;
            if ((!_TextBox) || (!_PopCal)) return
            var _CheckIn2 = $("#ppcCheckOut");
            var _format = _TextBox.getAttribute("Format");
            var _Date = _PopCal.getDate(_TextBox.value, _format);
            if (_Date) {
                var _CIn2 = eval(_TextBoxWeek.attr("Calendar"));
                _CIn2.value = _Date;
                _TextBoxWeek.val(_TextBox.value);
                $("#iNight").text('0');
            }
        }

        function CheckOutDateChange(_TextBox, _PopCal) {
            var _TextBoxDay2 = $("#iCheckIn");
            var _format = _TextBox.getAttribute("Format");
            var _Day1 = _PopCal.getDate(_TextBoxDay2.val(), _format);
            var _Day2 = _PopCal.getDate(_TextBox.value, _format);
            if (_Day1 > _Day2) {
                showMessage(LastDateSmall);
                _TextBox.value = _TextBoxDay2.val();
            }
            var days = Math.round((_Day2 - _Day1) / 86400000);
            $("#iNight").text(days.toString());
        }

        function getLocations() {
            var sLocations = $("#sLocation");
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Hotel.aspx/getLocationList",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#sBoard").html("");
                    $("#sAccom").html("");
                    $("#sRoom").html("");
                    $("#sHotel").html("");
                    $("#sLocation").html("");
                    $("#sLocation").append("<option value=''>" + ComboSelect + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#sLocation").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getHotels() {
            var sLocations = $("#sLocation");
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Hotel.aspx/getHotels",
                data: '{"Location":"' + sLocations.val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#sBoard").html("");
                    $("#sAccom").html("");
                    $("#sRoom").html("");
                    $("#sHotel").html("");
                    $("#sHotel").append("<option value=''>" + ComboSelect + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#sHotel").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getRooms() {
            var sHotel = $("#sHotel");
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Hotel.aspx/getRooms",
                data: '{"Hotel":"' + sHotel.val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#sRoom").html("");
                    $("#sRoom").append("<option value='' >" + ComboSelect + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#sRoom").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getAccoms() {
            var sHotel = $("#sHotel");
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Hotel.aspx/getAccoms",
                data: '{"Hotel":"' + sHotel.val() + '","Room":"' + $("#sRoom").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#sAccom").html("");
                    $("#sAccom").append("<option value='' >" + ComboSelect + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#sAccom").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getBoards() {
            var sHotel = $("#sHotel");
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Hotel.aspx/getBoards",
                data: '{"Hotel":"' + sHotel.val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#sBoard").html("");
                    $("#sBoard").append("<option value='' >" + ComboSelect + "</option>");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            $("#sBoard").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function changeHotels() {
            getRooms();
            getAccoms();
            getBoards();
        }

        function changeLocations() {
            getHotels();
        }

        function createTourist() {
            $.ajax({
                type: "POST",
                url: "../Controls/RSAdd_Hotel.aspx/getTourist",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#box1View").html("");
                    $("#box2View").html("");
                    if (msg.d != '') {
                        $.each($.json.decode(msg.d), function (i) {
                            if ($("#defaultUnSelect").val() == '1')
                                $("#box2View").append("<option value='" + this.CustNo + "'>" + this.TitleStr + ". " + this.Name + "</option>");
                            else
                                $("#box1View").append("<option value='" + this.CustNo + "'>" + this.TitleStr + ". " + this.Name + "</option>");
                        });
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

            $(function () {
                $.configureBoxes();
            });
        }

        function preCalc2(save) {

            if ($("#sHotel").val() == null || $("#sHotel").val() == '') {
                showMessage(addPleaseSelectHotel);
                return;
            }
            if ($("#sRoom").val() == null || $("#sRoom").val() == '') {
                showMessage(addPleaseSelectRoom);
                return;
            }
            if ($("#sAccom").val() == null || $("#sAccom").val() == '') {
                showMessage(addPleaseSelectAccom);
                return;
            }
            if ($("#sBoard").val() == null || $("#sBoard").val() == '') {
                showMessage(addPleaseSelectBoard);
                return;
            }
            var selectCust = '';
            $("#box1View option").each(function () {
                if (selectCust.length > 0) selectCust += "|";
                selectCust += $(this).val();
            });
            if (selectCust.length < 1) {
                showMessage(addPleaseSelectTourist);
                return;
            }
            var begDate = $("#iCheckIn").val();
            var _begDateFormat = $("#iCheckIn").attr("Format");
            var endDate = $("#iCheckOut").val();
            var _endDateFormat = $("#iCheckOut").attr("Format");
            if (begDate == '' || endDate == '') {
                showMessage(addPleaseCorrectDate);
                return;
            }

            var _data = '';
            var _url = "";
            if (save) {
                _url = "../Controls/RSAdd_Hotel.aspx/SaveService";
                _data = '{"selectedCusts":"' + selectCust + '"' +
                         ',"Hotel":"' + $("#sHotel").val() + '"' +
                         ',"Room":"' + $("#sRoom").val() + '"' +
                         ',"Accom":"' + $("#sAccom").val() + '"' +
                         ',"Board":"' + $("#sBoard").val() + '"' +
                         ',"BegDate":"' + begDate + '"' +
                         ',"BegDateFormat":"' + _begDateFormat + '"' +
                         ',"EndDate":"' + endDate + '"' +
                         ',"EndDateFormat":"' + _endDateFormat + '"' +
                         ',"recordType":"' + $("#recordType").val() + '"' + '}';
            }
            else {
                _url = "../Controls/RSAdd_Hotel.aspx/CalcService";
                _data = '{"selectedCusts":"' + selectCust + '"' +
                         ',"Hotel":"' + $("#sHotel").val() + '"' +
                         ',"Room":"' + $("#sRoom").val() + '"' +
                         ',"Accom":"' + $("#sAccom").val() + '"' +
                         ',"Board":"' + $("#sBoard").val() + '"' +
                         ',"BegDate":"' + begDate + '"' +
                         ',"BegDateFormat":"' + _begDateFormat + '"' +
                         ',"EndDate":"' + endDate + '"' +
                         ',"EndDateFormat":"' + _endDateFormat + '"' + '}';
            }
            $.ajax({
                type: "POST",
                url: _url,
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var result = msg.d;
                    if (save) {
                        if (result.Calc == false) {
                            showMessage(result.Msg);
                        } else {
                            window.close;
                            self.parent.returnAddResServices(true, "");
                        }
                    }
                    if (msg.d != null) {
                        if (result.Calc == true) {
                            $("#iSalePrice").text(result.CalcPrice + ' ' + result.CalcCur);
                        } else showMessage(result.Msg);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function preCalc(save) {
            if ($("#sHotel").val() == null || $("#sHotel").val() == '') {
                showMessage(addPleaseSelectHotel);
                return;
            }
            if ($("#sRoom").val() == null || $("#sRoom").val() == '') {
                showMessage(addPleaseSelectRoom);
                return;
            }
            if ($("#sAccom").val() == null || $("#sAccom").val() == '') {
                showMessage(addPleaseSelectAccom);
                return;
            }
            if ($("#sBoard").val() == null || $("#sBoard").val() == '') {
                showMessage(addPleaseSelectBoard);
                return;
            }
            var selectCust = '';
            $("#box1View option").each(function () {
                if (selectCust.length > 0) selectCust += "|";
                selectCust += $(this).val();
            });
            if (selectCust.length < 1) {
                showMessage(addPleaseSelectTourist);
                return;
            }
            var begDate = $("#iCheckIn").val();
            var _begDateFormat = $("#iCheckIn").attr("Format");
            var endDate = $("#iCheckOut").val();
            var _endDateFormat = $("#iCheckOut").attr("Format");
            if (begDate == '' || endDate == '') {
                showMessage(addPleaseCorrectDate);
                return;
            }

            var _data = '{"selectedCusts":"' + selectCust + '"' +
                         ',"Hotel":"' + $("#sHotel").val() + '"' +
                         ',"Room":"' + $("#sRoom").val() + '"' +
                         ',"Accom":"' + $("#sAccom").val() + '"}';
            $.ajax({
                async: false,
                type: "POST",
                url: "../Controls/RSAdd_Hotel.aspx/PaxControl",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        var paxs = $.json.decode(msg.d);
                        if (paxs.Adult > 0) {
                            $("#iAdult").text(paxs.Adult.toString());
                            $("#iChild").text(paxs.Child.toString());
                            preCalc2(save);
                        }
                        else {
                            if (paxs.Adult == -1) {
                                showMessage(HotelAccomNotFound);
                                return;
                            }
                            else
                                if (paxs.Adult < -1) {
                                    showMessage(HotelAccomPaxError);
                                    return;
                                }
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showMessage(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function exit(source) {
            if (source == 'save') {
                preCalc(true);
            }
            else
                if (source == 'cancel') {
                    window.close;
                    self.parent.returnAddResServices(false, '');
                }
        }

        function onLoad() {
            $.query = $.query.load(location.href);
            $("#recordType").val($.query.get('recordType'));
            if ($.query.get('defaultUnSelect') != '')
                $("#defaultUnSelect").val($.query.get('defaultUnSelect'));
            getLocations();
            createTourist();
        }
    </script>

</head>
<body onload="onLoad();">
    <form id="formRsHotel" runat="server">
        <input id="recordType" type="hidden" value="" />
        <input id="defaultUnSelect" type="hidden" value="0" />
        <br />
        <div id="divRs">
            <div>
                <table>
                    <tr>
                        <td class="leftCell">
                            <div id="divLocation" class="divs">
                                <div class="LeftDiv">
                                    <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewLocation") %>
                                    :</span>
                                </div>
                                <div id="divSLocation" class="inputDiv">
                                    <select id="sLocation" style="width: 90%;" onchange="changeLocations()">
                                    </select>
                                </div>
                            </div>
                            <div id="divHotel" class="divs">
                                <div class="LeftDiv">
                                    <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewHotel") %>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <select id="sHotel" style="width: 90%;" onchange="changeHotels()">
                                    </select>
                                </div>
                            </div>
                            <div id="divChekInOut" class="divs">
                                <div class="LeftDiv">
                                    <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewCheckInOut") %>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <asp:TextBox ID="iCheckIn" runat="server" Width="100px" />
                                    <rjs:PopCalendar ID="ppcCheckIn" runat="server" Control="iCheckIn" ClientScriptOnDateChanged="CheckInDateChange" />
                                    &nbsp;-&nbsp;
                                <asp:TextBox ID="iCheckOut" runat="server" Width="100px" />
                                    <rjs:PopCalendar ID="ppcCheckOut" runat="server" Control="iCheckOut" ClientScriptOnDateChanged="CheckOutDateChange" />
                                </div>
                            </div>
                            <div id="divNight" class="divs">
                                <div class="LeftDiv">
                                    <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewNight") %>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <b>
                                        <asp:Label ID="iNight" runat="server" /></b>
                                </div>
                            </div>
                            <div id="divRoom" class="divs">
                                <div class="LeftDiv">
                                    <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewRoom") %>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <select id="sRoom" style="width: 90%;">
                                    </select>
                                </div>
                            </div>
                            <div id="divAccom" class="divs">
                                <div class="LeftDiv">
                                    <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewAccom") %>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <select id="sAccom" style="width: 90%;">
                                    </select>
                                </div>
                            </div>
                            <div id="divBoard" class="divs">
                                <div class="LeftDiv">
                                    <span class="compulsoryField">* </span>
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewBoard") %>
                                    :</span>
                                </div>
                                <div class="inputDiv">
                                    <select id="sBoard" style="width: 90%;">
                                    </select>
                                </div>
                            </div>
                            <div id="divAdultChild" class="divs">
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                    :</span>
                                </div>
                                <div class="divAdultChild">
                                    <b><span id="iAdult"></span></b>
                                </div>
                                <div class="LeftDiv">
                                    <span class="label">
                                        <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                    :</span>
                                </div>
                                <div class="divAdultChild">
                                    <b><span id="iChild"></span></b>
                                </div>
                            </div>
                            <%--<div id="divSupplier" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplier") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplier"></span></b>
                            </div>
                        </div>
                        <div id="divSupplierNote" class="divs">
                            <div class="LeftDiv">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %>
                                    :</span>
                            </div>
                            <div class="inputDiv">
                                <b><span id="iSupplierNote"></span></b>
                            </div>
                        </div>--%>
                        </td>
                        <td class="rightCell">
                            <br />
                            <div id="divSalePrice">
                                <span class="label">
                                    <%= GetGlobalResourceObject("Controls", "viewSalePrice") %>
                                :</span> <span id="iSalePrice" class="salePrice"></span>
                            </div>
                            <br />
                            <div id="divCalcBtn">
                                <input id="btnRecalc" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
                                    onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                                    style="width: 150px;" />
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td align="center">
                            <b>
                                <%= GetGlobalResourceObject("Controls", "lblServiceTourist")%></b>
                        </td>
                        <td></td>
                        <td align="center">
                            <b>
                                <%= GetGlobalResourceObject("Controls", "lblOtherTourist")%></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 290px;" align="left">
                            <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box1Filter" />
                            <button type="button" id="box1Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                X</button><br />
                            <select id="box1View" multiple="multiple" style="width: 100%; height: 125px;">
                            </select><br />
                            <%--<span id="box1Counter" class="countLabel"></span>--%>
                            <select id="box1Storage">
                            </select>
                        </td>
                        <td style="width: 40px;" align="center">
                            <button id="to2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                >
                            </button>
                            <br />
                            <button id="allTo2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                >>
                            </button>
                            <br />
                            <button id="allTo1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                <<
                            </button>
                            <br />
                            <button id="to1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                <
                            </button>
                        </td>
                        <td style="width: 290px;" align="left">
                            <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box2Filter" />
                            <button type="button" id="box2Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                                X</button><br />
                            <select id="box2View" multiple="multiple" style="width: 100%; height: 125px;">
                            </select><br />
                            <%--<span id="box2Counter" class="countLabel"></span>--%>
                            <select id="box2Storage">
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <table id="divBtn">
                <tr>
                    <td style="width: 50%;" align="center">
                        <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
                            onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                            style="width: 100px;" />
                    </td>
                    <td align="center">
                        <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                            onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                            style="width: 100px;" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="dialog-message" title="" style="display: none;">
            <p>
                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
            </p>
        </div>
    </form>
</body>
</html>
