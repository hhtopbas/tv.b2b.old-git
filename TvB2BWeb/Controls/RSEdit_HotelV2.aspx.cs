﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSEdit_HotelV2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
    }

    [WebMethod(EnableSession = true)]
    public static string getServiceDetail(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        if (!recID.HasValue) return string.Empty;
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        HotelRecord hotel = new Hotels().getHotelDetail(UserData, resService.Service, ref errorMsg);
        var query = from q in ResData.ResService
                    where q.RecID == recID.Value
                    select new
                    {
                        Location = hotel.Location,
                        LocationName = hotel.LocationLocalName,
                        BegDate = q.BegDate.Value.ToShortDateString(),
                        EndDate = q.EndDate.Value.ToShortDateString(),
                        Service = q.Service,
                        Room = q.Room,
                        Board = q.Board,
                        Accom = q.Accom,
                        Adult = q.Adult,
                        Child = q.Child,
                        Unit = q.Unit,
                        Night = q.Duration,
                        Supplier = q.SupplierName,
                        SupNote = q.SupNote,
                        StatConf = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ? q.StatConfNameL : string.Empty,
                        StatSer = q.StatSerNameL,
                        PnrNo = q.PnrNo,
                        SalePrice = !Equals(q.IncPack, "Y") ? q.SalePrice.Value.ToString("#,###.00") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        SaleCur = !Equals(q.IncPack, "Y") ? q.SaleCur : "",
                        ChangePax = Equals(q.IncPack, "Y") || (q.Compulsory.HasValue && q.Compulsory.Value) ? "0" : "1"
                    };
        if (query == null || query.Count() < 1) return string.Empty;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query.FirstOrDefault());
    }

    [WebMethod(EnableSession = true)]
    public static string getLocationList(int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        bool forPackage = string.Equals(ResData.ResMain.PackType, "H");
        string errorMsg = string.Empty;
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        int? CatPackID = resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;
        List<TvBo.CodeName> retval = new TvBo.Hotels().getHotelLocationCodeName(UserData, ResData.ResMain.ArrCity,null, CatPackID, resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0 && string.Equals(resService.IncPack, "Y") && forPackage ? new Locations().getLocationForCity(resService.DepLocation.Value, ref errorMsg) : null, ResData.ResMain.PackType, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(retval);
    }

    [WebMethod(EnableSession = true)]
    public static string getLocations(string ResNo, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        retval = new TvBo.Hotels().getHotelLocationJSon(UserData.Market, ResData, recID, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getHotels(int? Location, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? hotelLocation = Location;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);

        string errorMsg = string.Empty;
        string retval = string.Empty;
        int? CatPackID = resService != null && resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;
        List<TvBo.CodeName> hotelList = new TvBo.Hotels().getLocationHotelCodeName(UserData.Market, hotelLocation.HasValue ? hotelLocation.Value : 0, CatPackID, ref errorMsg);
        var retVal = from q in hotelList
                     group q by new { q.Code, q.Name } into k
                     select new { k.Key.Code, k.Key.Name };
        return Newtonsoft.Json.JsonConvert.SerializeObject(retVal);
    }

    [WebMethod(EnableSession = true)]
    public static string getRooms(string Hotel, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);

        string errorMsg = string.Empty;
        string retval = string.Empty;
        int? CatPackID = resService != null && resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;

        retval = new TvBo.Hotels().getRoomJSon(UserData.Market, Hotel, CatPackID, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getAccoms(string Hotel, string Room, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);

        string errorMsg = string.Empty;
        string retval = string.Empty;
        int? CatPackID = resService != null && resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;

        retval = new TvBo.Hotels().getAccomJSon(UserData.Market, Hotel, Room, CatPackID, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getBoards(string Hotel, int? recID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);

        string errorMsg = string.Empty;
        string retval = string.Empty;
        int? CatPackID = resService != null && resService.CatPRecNo.HasValue && (ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0) ? ResData.ResMain.PriceListNo : null;

        retval = new TvBo.Hotels().getBoardJSon(UserData.Market, Hotel, CatPackID, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        Int32? recID = Conversion.getInt32OrNull(RecID);

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var querySelected = from s in ResData.ResCust
                            join q1 in ResData.ResCon on s.CustNo equals q1.CustNo
                            where s.Status == 0 && q1.ServiceID == recID
                            select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name, Selected = true };
        var queryUnSelected = from s in ResData.ResCust
                              //join q1 in ResData.ResCon on s.CustNo equals q1.CustNo
                              where s.Status == 0 && (querySelected.Where(w => w.CustNo == s.CustNo).Count() < 1)
                              group s by new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name } into k
                              select new { CustNo = k.Key.CustNo, Name = k.Key.Name, Selected = false };
        var query = querySelected.Union(queryUnSelected);
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static editServiceReturnData CalcService(string RecID, string Hotel, string Room, string Accom, string Board, bool? newRes, string SelectedCust)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResDataRecord tmpResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        Int16? oldUnit = Conversion.getInt16OrNull(resService.Unit.ToString());
        decimal? oldServicePrice = resService.SalePrice;
        decimal? oldResSalePrice = ResData.ResMain.SalePrice;

        List<ResConRecord> serviceResCon = (List<ResConRecord>)TvBo.Common.DeepClone(ResData.ResCon.Where(w => w.ServiceID == resService.RecID).ToList<ResConRecord>());
        string[] selectedCust = SelectedCust.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;

        TvBo.HotelRecord HotelRec = new Hotels().getHotelDetail(UserData, Hotel, ref errorMsg);
        TvBo.HotelRoomRecord HotelRoom = new Hotels().getHotelRoom(UserData.Market, Hotel, Room, ref errorMsg);
        TvBo.HotelAccomRecord HotelAccom = new Hotels().getHotelAccom(UserData.Market, Hotel, Room, Accom, ref errorMsg);
        TvBo.HotelBoardRecord HotelBoard = new Hotels().getHotelBoard(UserData.Market, Hotel, Board, ref errorMsg);

        Int16 ErrorCode = 0;
        if (HotelAccom == null) ErrorCode = 1; // Hotel accommodation not found.        
        if (HotelAccom.MinAdl > AdlCnt) ErrorCode = 2; // Servisi alacak olan yetişkin sayısı, odada kalacak en az yetişkin sayısından büyük.        
        if (HotelAccom.MaxAdl < AdlCnt) ErrorCode = 3; // Servisi alacak olan yetişkin sayısı, odada kalacak yetişkin sayısından fazla.        
        if (HotelAccom.MaxChd < ChdCnt) ErrorCode = 4; // Servisi alacak olan çocuk sayısı, odada kalacak çocuk sayısından fazla.
        if (HotelAccom.MaxPax < (AdlCnt + ChdCnt)) ErrorCode = 5; // Servisi alacak olan pax sayısı, odada kalacak pax sayısından fazla.
        if (ErrorCode > 0)
        {
            if (ErrorCode == 1)
                errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomNotFound").ToString();
            else errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();
            return new editServiceReturnData { Calc = false, Msg = errorMsg };//"{" + retVal + "}";
        }
        resService.PriceSource = resService.StepNo.HasValue || resService.CatPRecNo.HasValue ? 1 : (!Equals(resService.Service, Hotel) || !Equals(resService.Room, Room) || !Equals(resService.Accom, Accom) || !Equals(resService.Board, Board) ? 0 : resService.PriceSource);
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Hotel;
        resService.Room = Room;
        resService.Accom = Accom;
        resService.Board = Board;
        resService.Supplier = string.Empty;
        resService.Adult = Conversion.getInt16OrNull(AdlCnt);
        resService.Child = Conversion.getInt16OrNull(ChdCnt);

        resService.ServiceName = HotelRec.Name + "(" + HotelRec.Category + ")";
        resService.ServiceNameL = HotelRec.LocalName + "(" + HotelRec.Category + ")";
        resService.RoomName = HotelRoom.Name;
        resService.RoomNameL = HotelRoom.NameL;
        resService.AccomName = HotelAccom.Name;
        resService.AccomNameL = HotelAccom.LocalName;
        resService.BoardName = HotelBoard.Name;
        resService.BoardNameL = HotelBoard.NameL;
        resService.Supplier = string.Empty;
        resService.DepLocation = HotelRec.Location;
        resService.DepLocationName = HotelRec.LocationName;
        resService.DepLocationNameL = HotelRec.LocationLocalName;

        bool serviceChanged = false;
        if (resService.Service != Hotel && resService.Room != Room && resService.Accom != Accom && resService.Board != Board)
        {
            serviceChanged = true;
            oldUnit = 1;
        }

        if (serviceChanged)
        {
            string StopSale = string.Empty;
            string OverRelease = string.Empty;
            string OverAllot = string.Empty;
            string htAllot = string.Empty;
            string DailyHotelAllot = string.Empty;

            bool checkAllot = new Hotels().HotelAllotKontrol(UserData, resService.Service,
                                        ResData.ResMain.PLMarket,
                                        ResData.ResMain.PLOperator,
                                        Room, string.Empty, resService.BegDate.Value, resService.EndDate.Value, (resService.EndDate.Value - resService.BegDate.Value).Days,
                                        ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0 ? ResData.ResMain.PriceListNo.Value : 0,
                                        0,
                                        resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0 ? resService.CatPRecNo.Value : 0,
                                        ref OverRelease, ref OverAllot, ref StopSale, ref htAllot, ref DailyHotelAllot,
                                        ResData.ResMain.DepCity.Value, ResData.ResMain.ArrCity.Value, ResData.ResMain.PackType, ref errorMsg, 1);

            if (!checkAllot)
                return new editServiceReturnData { Calc = false, Msg = errorMsg };
        }

        //ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
        ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
        ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);

        if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            return new editServiceReturnData { Calc = false, Msg = errorMsg };
        }
        else
        {
            resService = ResData.ResService.Find(f => f.RecID == recID.Value);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
            string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
            return new editServiceReturnData
            {
                Calc = true,
                CalcPrice = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                CalcCur = resService.SalePrice.HasValue && !Equals(resService.IncPack, "Y") ? resService.SaleCur : string.Empty,
                Adult = resService.Adult,
                Child = resService.Child,
                Unit = resService.Unit,
                OldResPrice = !Equals(resService.IncPack, "Y") && oldResSalePrice.HasValue ? oldResSalePrice.Value.ToString("#,###.00") : string.Empty,
                OldServicePrice = !Equals(resService.IncPack, "Y") && oldServicePrice.HasValue ? oldServicePrice.Value.ToString("#,###.00") : string.Empty,
                Supplier = supplierRec.NameL,
                Msg = string.Empty
            };
        }
    }

    [WebMethod(EnableSession = true)]
    public static editServiceReturnData SaveService(string RecID, string Hotel, string Room, string Accom, string Board, bool? newRes, string SelectedCust)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        TvBo.ResDataRecord tmpResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        decimal? oldResSalePrice = ResData.ResMain.SalePrice;
        List<ResServiceRecord> resServiceList = ResData.ResService;

        ResServiceRecord resService = (TvBo.ResServiceRecord)TvBo.Common.DeepClone(ResData.ResService.Find(f => f.RecID == recID));
        Int16? oldUnit = Conversion.getInt16OrNull(resService.Unit.ToString());
        decimal? oldServicePrice = resService.SalePrice;

        TvBo.HotelRecord HotelRec = new Hotels().getHotelDetail(UserData, Hotel, ref errorMsg);
        TvBo.HotelRoomRecord HotelRoom = new Hotels().getHotelRoom(UserData.Market, Hotel, Room, ref errorMsg);
        TvBo.HotelAccomRecord HotelAccom = new Hotels().getHotelAccom(UserData.Market, Hotel, Room, Accom, ref errorMsg);
        TvBo.HotelBoardRecord HotelBoard = new Hotels().getHotelBoard(UserData.Market, Hotel, Board, ref errorMsg);

        List<ResConRecord> serviceResCon = (List<ResConRecord>)TvBo.Common.DeepClone(ResData.ResCon.Where(w => w.ServiceID == resService.RecID).ToList<ResConRecord>());
        string[] selectedCust = SelectedCust.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;


        Int16 ErrorCode = 0;
        if (HotelAccom == null) ErrorCode = 1; // Hotel accommodation not found.        
        if (HotelAccom.MinAdl > AdlCnt) ErrorCode = 2; // Servisi alacak olan yetişkin sayısı, odada kalacak en az yetişkin sayısından büyük.        
        if (HotelAccom.MaxAdl < AdlCnt) ErrorCode = 3; // Servisi alacak olan yetişkin sayısı, odada kalacak yetişkin sayısından fazla.        
        if (HotelAccom.MaxChd < ChdCnt) ErrorCode = 4; // Servisi alacak olan çocuk sayısı, odada kalacak çocuk sayısından fazla.
        if (HotelAccom.MaxPax < (AdlCnt + ChdCnt)) ErrorCode = 5; // Servisi alacak olan pax sayısı, odada kalacak pax sayısından fazla.

        if (ErrorCode > 0)
        {
            if (ErrorCode == 1)
                errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomNotFound").ToString();
            else errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString();

            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            return new editServiceReturnData { Calc = false, Msg = errorMsg };
        }
        resService.PriceSource = resService.StepNo.HasValue || resService.CatPRecNo.HasValue ? 1 : (!Equals(resService.Service, Hotel) || !Equals(resService.Room, Room) || !Equals(resService.Accom, Accom) || !Equals(resService.Board, Board) ? 0 : resService.PriceSource);
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Hotel;
        resService.Room = Room;
        resService.Accom = Accom;
        resService.Board = Board;
        resService.Adult = Conversion.getInt16OrNull(AdlCnt);
        resService.Child = Conversion.getInt16OrNull(ChdCnt);

        resService.ServiceName = HotelRec.Name + "(" + HotelRec.Category + ")";
        resService.ServiceNameL = HotelRec.LocalName + "(" + HotelRec.Category + ")";
        resService.RoomName = HotelRoom.Name;
        resService.RoomNameL = HotelRoom.NameL;
        resService.AccomName = HotelAccom.Name;
        resService.AccomNameL = HotelAccom.LocalName;
        resService.BoardName = HotelBoard.Name;
        resService.BoardNameL = HotelBoard.NameL;
        resService.Supplier = string.Empty;
        resService.DepLocation = HotelRec.Location;
        resService.DepLocationName = HotelRec.LocationName;
        resService.DepLocationNameL = HotelRec.LocationLocalName;
        bool serviceChanged = false;
        if (resService.Service != Hotel && resService.Room != Room && resService.Accom != Accom && resService.Board != Board)
        {
            serviceChanged = true;
            oldUnit = 1;
        }

        if (serviceChanged)
        {
            string StopSale = string.Empty;
            string OverRelease = string.Empty;
            string OverAllot = string.Empty;
            string htAllot = string.Empty;
            string DailyHotelAllot = string.Empty;

            bool checkAllot = new Hotels().HotelAllotKontrol(UserData, resService.Service,
                                        ResData.ResMain.PLMarket,
                                        ResData.ResMain.PLOperator,
                                        Room, string.Empty, resService.BegDate.Value, resService.EndDate.Value, (resService.EndDate.Value - resService.BegDate.Value).Days,
                                        ResData.ResMain.PriceListNo.HasValue && ResData.ResMain.PriceListNo.Value > 0 ? ResData.ResMain.PriceListNo.Value : 0,
                                        0,
                                        resService.CatPRecNo.HasValue && resService.CatPRecNo.Value > 0 ? resService.CatPRecNo.Value : 0,
                                        ref OverRelease, ref OverAllot, ref StopSale, ref htAllot, ref DailyHotelAllot,
                                        ResData.ResMain.DepCity.Value, ResData.ResMain.ArrCity.Value, ResData.ResMain.PackType, ref errorMsg, 1);

            if (!checkAllot)
                return new editServiceReturnData { Calc = false, Msg = errorMsg };
        }

        ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
        List<ResConRecord> resCon = ResData.ResCon;
        foreach (ResConRecord row in resCon.Where(w => w.ServiceID == resService.RecID))
        {
            ResConRecord resC = resCon.Find(f => f.RecID == row.RecID);
            if (resC != null)
                resC.MemTable = false;
        }
        ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);

        if (newRes.HasValue && newRes.Value)
        {
            ResData = new Reservation().extraServiceControl(UserData, ResData, resService, ref errorMsg);
            if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                return new editServiceReturnData { Calc = false, Msg = errorMsg };
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                HttpContext.Current.Session["ResData"] = ResData;
                return new editServiceReturnData
                {
                    Calc = true,
                    CalcPrice = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                    CalcCur = resService.SalePrice.HasValue && !Equals(resService.IncPack, "Y") ? resService.SaleCur : string.Empty,
                    Adult = resService.Adult,
                    Child = resService.Child,
                    Unit = resService.Unit,
                    OldResPrice = oldResSalePrice.HasValue ? oldResSalePrice.Value.ToString("#,###.00") : string.Empty,
                    OldServicePrice = oldServicePrice.HasValue ? oldServicePrice.Value.ToString("#,###.00") : string.Empty,
                    Supplier = supplierRec.NameL,
                    Msg = string.Empty
                };
            }
        }
        else
        {
            Int32 ErrCode = 0;
            //ResData = new Reservation().extraServiceControl(UserData, ResData, resService, ref errorMsg);
            List<int?> recIDList = new List<int?>();
            recIDList.Add(resService.RecID);
            ResData = new TvBo.Reservation().editService(UserData, ResData, recIDList, ref ErrCode, ref errorMsg, false);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
            }
            else
                if (ErrCode != 0)
                {
                    if (ErrCode < 0)
                        errorMsg = string.IsNullOrEmpty(errorMsg) ? "unknown error" : errorMsg;
                    else errorMsg = new ResCalcError().calcError(ErrCode, ResData).ToString();
                    return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
                }
                else
                {
                    #region re calculated
                    resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                    SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                    HttpContext.Current.Session["ResData"] = ResData;
                    return new editServiceReturnData
                    {
                        Calc = true,
                        CalcPrice = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        CalcCur = resService.SalePrice.HasValue && !Equals(resService.IncPack, "Y") ? resService.SaleCur : string.Empty,
                        Adult = resService.Adult,
                        Child = resService.Child,
                        Unit = resService.Unit,
                        OldResPrice = oldResSalePrice.HasValue ? oldResSalePrice.Value.ToString("#,###.00") : string.Empty,
                        OldServicePrice = oldServicePrice.HasValue ? oldServicePrice.Value.ToString("#,###.00") : string.Empty,
                        Supplier = supplierRec.NameL,
                        Msg = string.Empty
                    };
                    #endregion
                }
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getChgFee(string RecID, string Hotel, string Room, string Accom, string Board)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        resService.Service = Hotel;
        resService.Room = Room;
        resService.Accom = Accom;
        resService.Board = Board;
        ResChgFeePrice chgPrice = new Reservation().whatHasChangedResService(UserData, ResData, recID, resService, ref errorMsg);
        if (chgPrice == null)
            return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        else
        {
            string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                             chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
            return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
        }
    }
}
