﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ssrc.aspx.cs" Inherits="Controls_Ssrc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "Ssrc") %></title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/Ssrc.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        function logout() {
            self.parent.logout();
        }

        function changeiSsrc1(value, i) {
            var iSsrc = $("#iSsrc1_" + i.toString());
            if (iSsrc.attr("roundTrip") != undefined && iSsrc.attr("first") != undefined) {
                if (iSsrc.attr("first") == "1") {
                    var roundTrip = iSsrc.attr("roundTrip");
                    var iSsrcList = $('select');
                    for (var cnt = 0; cnt < iSsrcList.length; cnt++) {
                        var iSsrcTmp = $("#" + iSsrcList[cnt].id);
                        if (iSsrcTmp.attr("roundTrip") == roundTrip)
                            iSsrcTmp.val(value);
                    }
                }
            }
        }

        function SaveSsrc() {
            var save = false;
            $.query = $.query.load(location.href);
            var CustNo = $.query.get('CustNo');
            var saved = $.query.get('Save');
            if (saved) save = saved == 'true' ? true : false;
            var recordCnt = parseInt($("#recordCnt").val());
            var data = '';
            for (var i = 0; i < recordCnt; i++) {
                if (data.length > 0) data += ',';
                data += '*L!CustNo!:' + CustNo + ',';
                data += '!ServiceID!:' + $("#serviceID" + i).val() + ',';
                data += '!Remark!:!!,';
                data += '!SpecSerRQCode1!:!' + $("#iSsrc1_" + i).val() + '!,';
                data += '!SpecSerRQCode2!:!!,';
                data += '!Real!:' + save + '*R';
            }
            $.ajax({
                type: "POST",
                url: "../Controls/Ssrc.aspx/saveSsrc",
                data: '{"Data":"' + data + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    if (msg.d == "OK") {
                        window.close;
                        self.parent.returnSsrc(false, true);
                    } else {
                        alert(msg.d);
                    }
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });

        }

        function CancelSsrc() {
            window.close;
            self.parent.returnSsrc(true, false);
        }

        function showSsrc(CustNo) {
            $.ajax({
                type: "POST",
                url: "../Controls/Ssrc.aspx/getSsrc",
                data: '{"CustNo":"' + CustNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#gridSsrc").html('');
                    $("#gridSsrc").html(msg.d);
                },
                error: function(xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function() {
                        logout();
                    }
                }
            });
        }

        function pageLoad() {
            $.query = $.query.load(location.href);
            var CustNo = $.query.get('CustNo');
            showSsrc(CustNo);
        }        
        
    </script>

</head>
<body onload="javascript:pageLoad();">
    <form id="SsrcForm" runat="server">
    <div class="SsrcForm">
        <div>
            <div id="gridSsrc">
            </div>
            <div class="SsrcBtnDiv">
                <input id="btnOK" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnOK")%>'
                    onclick="SaveSsrc();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />&nbsp;&nbsp;
                <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel")%>'
                    onclick="CancelSsrc();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
