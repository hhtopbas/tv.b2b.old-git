﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSView_TransferV2.aspx.cs"
    Inherits="Controls_RSView_TransferV2" EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><%= GetGlobalResourceObject("PageTitle", "RSViewTransfer") %></title>
    <link rel="shortcut icon" href="http://www.sanbilgisayar.com/img/tv/favicon.ico" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />


    <link href="../CSS/ServiceEditViewV2.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        $.blockUI.defaults.message = '<h1><%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %></h1>';
        $(document)
            .ajaxStart($.blockUI)
            .ajaxStop($.unblockUI);

        function onLoad() {
            $.query = $.query.load(location.href);
            var resNo = $.query.get('ResNo');
            var recID = $.query.get('RecID');
            $.ajax({
                type: "POST",
                url: "../Services/ReservationServices.asmx/getResService",
                data: '{"RecID":"' + recID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var resService = $.json.decode(msg.d);
                    $("#iDate").html(resService.BegDate);
                    if (resService.BegDate != resService.EndDate) {
                        $("#iDateR").html(resService.EndDate);
                        $("#iDateR").show();
                        $("#endDateSp").show();
                    }
                    $("#iTransfer").html(resService.ServiceNameL);
                    $("#sDepLocation").html(resService.DepLocationNameL);
                    $("#sArrLocation").html(resService.ArrLocationNameL);
                    $("#iAdult").html(resService.Adult);
                    $("#iChild").html(resService.Child);
                    $("#iUnit").html(resService.Unit);
                    if (resService.PickupTime != null)
                        $("#iPickupTime").html(resService.PickupTime);
                    else $("#iPickupTime").html('');
                    $("#iPickupNote").html(resService.PickupNote);

                    if (resService.RPickupTime != null)
                        $("#iPickupTimeR").html(resService.RPickupTime);
                    else $("#iPickupTimeR").html('');
                    $("#iPickupNoteR").html(resService.RPickupNote);

                    $("#iDepartureCode").html(resService.TrfDTName);
                    $("#iDepartureDescription").html(resService.TrfDepNameL);
                    $("#iArrivalCode").html(resService.TrfATName);
                    $("#iArrivalDescription").html(resService.TrfArrNameL);

                    $("#iDepartureCodeR").html(resService.RTrfDTName);
                    $("#iDepartureDescriptionR").html(resService.RTrfDepNameL);
                    $("#iArrivalCodeR").html(resService.RTrfATName);
                    $("#iArrivalDescriptionR").html(resService.RTrfArrNameL);
                    if (resService.RTrfDepNameL != '' || resService.RTrfArrNameL != '') {
                        $("#returnPickupDiv").show();
                    }
                    else {
                        $("#returnPickupDiv").hide();
                    }

                    if (resService.StatConfNameL != '')
                        $("#sConfirmation").html(resService.StatConfNameL);
                    else $("#divConfirmation").hide();
                    $("#sStatus").html(resService.StatSerNameL);
                    var salePrice = resService.IncPack == 'Y' ? '<%= GetGlobalResourceObject("LibraryResource", "InPackage").ToString() %>' : resService.SalePrice + ' ' + resService.SaleCur;
                    $("#iSalePrice").html(salePrice);
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }
    </script>

</head>
<body onload="onLoad()">
    <form id="formRsTransfer" runat="server">
        <div id="Div1" class="ui-widget ui-widget-content">
            <div class="ui-helper-clearfix">
                <div class="LeftDiv">
                    <span class="label">
                        <%= GetGlobalResourceObject("Controls", "viewTransferRoute") %></span>
                </div>
                <div class="RightDiv">
                    <b><span id="sDepLocation"></span>&nbsp;-&gt;&nbsp; <span id="sArrLocation"></span>
                    </b>
                </div>
            </div>
            <div class="ui-helper-clearfix">
                <div class="LeftDiv">
                    <span class="label">
                        <%= GetGlobalResourceObject("Controls", "viewTransferDate") %></span>
                </div>
                <div class="RightDiv">
                    <b><span id="iDate"></span><span id="endDateSp" class="ui-helper-hidden">- </span><span id="iDateR" class="ui-helper-hidden"></span></b>
                </div>
            </div>
            <div class="ui-helper-clearfix">
                <div class="LeftDiv">
                    <span class="label">
                        <%= GetGlobalResourceObject("Controls", "viewTransfer") %></span>
                </div>
                <div class="RightDiv">
                    <b><span id="iTransfer"></span></b>
                </div>
            </div>
            <div class="ui-helper-clearfix">
                <div class="LeftDiv">
                    <span class="label">
                        <%= GetGlobalResourceObject("Controls", "viewAdult") %></span>
                </div>
                <div class="RightDiv">
                    <div class="ui-helper-clearfix">
                        <div class="divAdultChild">
                            <b><span id="iAdult"></span></b>
                        </div>
                        <div class="LeftDiv">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewChild") %>&nbsp;</span>
                        </div>
                        <div class="divAdultChild">
                            <b><span id="iChild"></span></b>
                        </div>
                        <div class="LeftDiv">
                            <span class="label">
                                <%= GetGlobalResourceObject("Controls", "viewUnit") %>&nbsp;</span>
                        </div>
                        <div class="divAdultChild">
                            <b><span id="iUnit"></span></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="ui-widget ui-widget-content">
            <div class="ui-helper-clearfix">
                <div class="LeftDiv">
                    <span class="label">
                        <%= GetGlobalResourceObject("Controls", "viewDeparture") %></span>
                </div>
                <div class="RightDiv">
                    <span id="iDepartureCode" class="bold"></span>&nbsp;:&nbsp;<span id="iDepartureDescription"></span>&nbsp;->&nbsp;
                <span id="iArrivalCode" class="bold"></span>&nbsp;:&nbsp;<span id="iArrivalDescription"></span>
                </div>
            </div>
            <div class="ui-helper-clearfix">
                <div class="LeftDiv">
                    <span class="label">
                        <%= GetGlobalResourceObject("Controls", "viewPickupTimeNote") %></span>
                </div>
                <div class="RightDiv">
                    <b><span id="iPickupTime"></span></b>&nbsp;<span id="iPickupNote"></span>
                </div>
            </div>
        </div>
        <br />
        <div id="returnPickupDiv" class="ui-widget ui-widget-content">
            <div class="ui-helper-clearfix">
                <div class="LeftDiv">
                    <span class="label">
                        <%= GetGlobalResourceObject("Controls", "viewReturn") %></span>
                </div>
                <div class="RightDiv">
                    <span id="iDepartureCodeR" class="bold"></span>&nbsp;:&nbsp;<span id="iDepartureDescriptionR"></span>&nbsp;->&nbsp;
                <span id="iArrivalCodeR" class="bold"></span>&nbsp;:&nbsp;<span id="iArrivalDescriptionR"></span>
                </div>
            </div>
            <div class="ui-helper-clearfix">
                <div class="LeftDiv">
                    <span class="label">
                        <%= GetGlobalResourceObject("Controls", "viewPickupTimeNote") %></span>
                </div>
                <div class="RightDiv">
                    <b><span id="iPickupTimeR"></span></b>&nbsp;<span id="iPickupNoteR"></span>
                </div>
            </div>
        </div>
        <br />
        <div class="ui-helper-clearfix ui-widget ui-widget-content divStatusConfirmationPrice">
            <div class="divStatus">
                <span class="label">
                    <%= GetGlobalResourceObject("Controls", "viewStatus") %></span>
                <br />
                <b><span id="sStatus"></span></b>
            </div>
            <div class="divConfirmation">
                <span class="label">
                    <%= GetGlobalResourceObject("Controls", "viewConfirmation") %></span>
                <br />
                <b><span id="sConfirmation"></span></b>
            </div>
            <div class="divSalePrice">
                <span class="label">
                    <%= GetGlobalResourceObject("Controls", "viewSalePrice") %></span>
                <br />
                <span id="iSalePrice" class="salePrice"></span>
            </div>
        </div>

    </form>
</body>
</html>
