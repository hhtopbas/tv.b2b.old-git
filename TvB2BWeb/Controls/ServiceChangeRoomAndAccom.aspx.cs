﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using TvTools;

public partial class Controls_ServiceChangeRoomAndAccom : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string getAvailableRoomAndAccom(string ServiceID, string Adult, string Child)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData  = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        int? serviceID = Conversion.getInt32OrNull(ServiceID);
        ResServiceRecord hotelRec = ResData.ResService.Find(f => f.RecID == serviceID);
        string Hotel = hotelRec.Service;
        int? adult = Conversion.getInt16OrNull(Adult);
        int? child = Conversion.getInt16OrNull(Child);
        string errorMsg = string.Empty;
        List<TvBo.HotelAccomPaxRecord> accomPax = new Hotels().getHotelAccomPax(UserData.Market, Hotel, ref errorMsg);
        var query = from q in accomPax
                    where q.Adult >= adult && 
                        q.Adult <= adult &&
                        (q.ChdAgeG1 + q.ChdAgeG2 + q.ChdAgeG3 + q.ChdAgeG4) >= child &&
                        (q.ChdAgeG1 + q.ChdAgeG2 + q.ChdAgeG3 + q.ChdAgeG4) <= child
                    group q by new { Room = q.Room, RoomName = q.RoomName, Accom = q.Accom, AccomName = q.AccomName } into k
                    select new { Room = k.Key.Room, RoomName = k.Key.RoomName, Accom = k.Key.Accom, AccomName = k.Key.AccomName };
        query = from q in query
                orderby q.RoomName, q.AccomName
                select q;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod]
    public static string changeRoomsAccom(string ServiceID, string Room, string Accom)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (string.IsNullOrEmpty(ServiceID) && string.IsNullOrEmpty(Room) && string.IsNullOrEmpty(Accom))
            return "Error";
        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        int? serviceID = Conversion.getInt32OrNull(ServiceID);
        ResServiceRecord service = ResData.ResService.Find(f => f.RecID == serviceID);
        string OverRelease = string.Empty;
        string OverAllot = string.Empty;
        string StopSale = string.Empty;
        string gtAllot = string.Empty;
        string DailyHotelAllot = string.Empty;
        string errorMsg = string.Empty;
        if (new Hotels().HotelAllotKontrol(UserData, service.Service, ResData.ResMain.PLMarket, ResData.ResMain.PLOperator,
                            Room, Accom, service.BegDate.Value, service.EndDate.Value, service.Duration.Value,
                            ResData.ResMain.PriceListNo.HasValue ? ResData.ResMain.PriceListNo.Value : 0, 0, 0,
                            ref OverRelease, ref OverAllot, ref StopSale, ref gtAllot, ref DailyHotelAllot,
                            ResData.ResMain.DepCity.Value, ResData.ResMain.ArrCity.Value, ResData.ResMain.PackType, ref errorMsg, 1))
        {
            service.Room = Room;
            service.Accom = Accom;
            HttpContext.Current.Session["ResData"] = ResData;
            return "";
        }
        else
            return errorMsg;
    }
}
