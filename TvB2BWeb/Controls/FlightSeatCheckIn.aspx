﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlightSeatCheckIn.aspx.cs" Inherits="FlightSeatCheckInForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "SeatSelect") %></title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.linq.js" type="text/javascript"></script>
    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/BusSelectSeat.css?V=1" rel="stylesheet" type="text/css" />
    <style type="text/css">
        </style>

    <script language="javascript" type="text/javascript">

        var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
        var btnCancel = '<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>';
        var btnYes = '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>';
        var btnNo = '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>';
        var basePage = '<%= Global.getBasePageRoot().ToString() %>';
        var msglblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';

        var rx = /INPUT|TEXTAREA/i;
        var rxT = /RADIO|CHECKBOX|SUBMIT/i;

        $(document).ajaxStart(function () {
            $.blockUI({
                message: '<h1>' + msglblPleaseWait + '</h1>'
            });
        }).ajaxStop(function () {
            $.unblockUI();
            return false;
        });

        $(document).bind("keydown keypress", function (e) {
            var preventKeyPress;
            if (e.keyCode == 8) {
                var d = e.srcElement || e.target;
                if (rx.test(e.target.tagName)) {
                    var preventPressBasedOnType = false;
                    if (d.attributes["type"]) {
                        preventPressBasedOnType = rxT.test(d.attributes["type"].value);
                    }
                    preventKeyPress = d.readOnly || d.disabled || preventPressBasedOnType;
                } else { preventKeyPress = true; }
            } else { preventKeyPress = false; }

            if (preventKeyPress) e.preventDefault();
        });

        function showAlert(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                position: 'center',
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');
                        return true;
                    }
                }]
            });
        }

        function logout() {
            self.parent.logout();
        }

        function saveSeatError(msg) {
            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                position: 'center',
                modal: true,
                buttons: [{
                    text: btnOK,
                    click: function () {
                        $(this).dialog('close');

                        location.reload();                       

                        return true;
                    }
                }]
            });
        }

        function saveSeat(serviceID) {
            var params = new Object();
            params.ServiceID = serviceID;
            $.ajax({
                type: "POST",
                url: "FlightSeatCheckIn.aspx/saveSeat",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 'OK') {
                        var recID = $("#hfServiceID").val();

                        $("#busSelectSeatDiv").html('');

                        getCustomers(recID);
                        getSeatPlan(recID, null);                        
                    }
                    else if (msg.d != 'NO') {
                        saveSeatError(msg.d);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });

        }

        function selectSeat(seatNo, serviceID, custNo) {

            if (custNo == null) {
                return;
            }

            var params = new Object();
            params.SeatNo = seatNo;
            params.CustNo = custNo;
            params.ServiceID = serviceID;
            params.OldImg = $("#" + seatNo).attr("src");
            $.ajax({
                async: false,
                type: "POST",
                url: "FlightSeatCheckIn.aspx/getSeatCust",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {

                        $("#custSeatReserv_" + custNo).html('');
                        $("#custSeatReserv_" + custNo).html(msg.d.NewSeat);
                        $("#" + seatNo).attr("src", basePage + "Images/SeatFullThisRes.png?v=3");
                        if (msg.d.hasOwnProperty('OldSeat') && msg.d.OldSeat != null) {
                            $("#" + msg.d.OldSeat.SeatLetter + '_' + msg.d.OldSeat.SeatNo).attr("src", msg.d.OldSeat.OldSeatImg);
                        }

                        var custReservSeatNumber = $(".custReservSeatNumber");
                        var selectCount = 0;
                        for (var i = 0; i < custReservSeatNumber.length; i++) {
                            if ($(custReservSeatNumber[i]).html() != '')
                                selectCount++;
                        }
                        var showSaveButton = selectCount == custReservSeatNumber.length;
                        if (showSaveButton) {
                            $("#saveButton").show();
                        }
                        else {
                            $("#saveButton").hide();
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getSeatPlan(serviceId, custNo) {
            var params = new Object();
            params.ServiceID = serviceId;
            params.CustNo = custNo;
            $.ajax({
                async: false,
                type: "POST",
                url: "FlightSeatCheckIn.aspx/getSeatPlan",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        var data = msg.d;
                        var sPlan = $("#flightSeatPlan");
                        if (data.Success) {
                            sPlan.html('');
                            var sPlanHtml = '';
                            sPlanHtml += '<div class="seatView">';
                            sPlanHtml += '<div style="text-align: left;">';
                            sPlanHtml += '<img src="' + basePage + 'Images/SeatFull.png?v=3" /> Not available &nbsp;|&nbsp;';
                            sPlanHtml += '<img src="' + basePage + 'Images/SeatEmpty.png?v=3" /> Available <br />';
                            sPlanHtml += '<img src="' + basePage + 'Images/SeatLEmpty.png?v=3" /> Available with Legroom';
                            sPlanHtml += '</div>';
                            for (var z = 0; z < data.PlaneZone.length; z++) {
                                sPlanHtml += '<div>';
                                sPlanHtml += '<h4>' + data.PlaneZone[z].ZoneName + '</h4>';
                                sPlanHtml += '<table>';
                                var rowNumber = '';
                                for (var col = 0; col < data.PlaneZone[z].MaxCol; col++) {
                                    sPlanHtml += '<tr>';
                                    rowNumber = '';
                                    for (var row = data.PlaneZone[z].MaxRow - 1; row >= 0; row--) {
                                        sPlanHtml += '<td style="width: ' + data.PlaneZone[z].SeatWidth + 'px;">';
                                        var seat = $.Enumerable.From(data.PlaneZone[z].ZoneItems)
                                                    .OrderBy(function (x) {
                                                        return x.ItemCol, x.ItemRow
                                                    })
                                                    .Where(function (x) {
                                                        return x.ItemCol == col && x.ItemRow == row;
                                                    })
                                                    .FirstOrDefault();
                                        if (seat != undefined && seat != null) {
                                            if (seat.ItemType == 0) {
                                                sPlanHtml += '<span class="seatLetter">' + seat.SeatLetter + '</span>';
                                                var extraLegroom = seat.ExtraLegroom == true ? 'L' : '';
                                                var extraService = seat.ExtraService != '' ? 'Ext' : '';
                                                if (seat.IsExit) {
                                                    if (seat.ForB2B && !seat.isFull) //seatNo, serviceID, custNo
                                                        sPlanHtml += '<img title="' + seat.ExtraServicePrice + '" alt="' + seat.ExtraServicePrice + '" id="' + seat.SeatLetter + '_' + seat.SeatNo + '" src="' + basePage + 'Images/Seat' + extraLegroom + 'ExitEmpty' + extraService + '.png?v=3" alt="' + seat.SeatNo + seat.SeatLetter + '" style="width: ' + (data.PlaneZone[z].SeatWidth - 1) + 'px; height: 32px;" onclick="selectSeat(\'' + seat.SeatLetter + '_' + seat.SeatNo + '\',' + serviceId + ',' + custNo + ')" class="selectSeat" />';
                                                    else
                                                        if (seat.thisReservation)
                                                            sPlanHtml += '<img id="' + seat.SeatLetter + '_' + seat.SeatNo + '" src="' + basePage + 'Images/SeatFullThisRes.png?v=3" alt="' + seat.SeatNo + seat.SeatLetter + '" style="width: ' + (data.PlaneZone[z].SeatWidth - 1) + 'px; height: 32px;' + (seat.thisReservation ? 'background-color: #c0e40a; border: 1px solid #808080;' : '') + '" />';
                                                        else
                                                            sPlanHtml += '<img id="' + seat.SeatLetter + '_' + seat.SeatNo + '" src="' + basePage + 'Images/SeatFull.png?v=3" alt="' + seat.SeatNo + seat.SeatLetter + '" style="width: ' + (data.PlaneZone[z].SeatWidth - 1) + 'px; height: 32px;' + (seat.thisReservation ? 'background-color: #c0e40a; border: 1px solid #808080;' : '') + '" />';
                                                }
                                                else {
                                                    if (seat.ForB2B && !seat.isFull)
                                                        sPlanHtml += '<img title="' + seat.ExtraServicePrice + '" alt="' + seat.ExtraServicePrice + '" id="' + seat.SeatLetter + '_' + seat.SeatNo + '" src="' + basePage + 'Images/Seat' + extraLegroom + 'Empty' + extraService + '.png?v=3" alt="' + seat.SeatNo + seat.SeatLetter + '" style="width: ' + (data.PlaneZone[z].SeatWidth - 1) + 'px; height: 32px;" onclick="selectSeat(\'' + seat.SeatLetter + '_' + seat.SeatNo + '\',' + serviceId + ',' + custNo + ')" class="selectSeat" />';
                                                    else
                                                        if (seat.thisReservation)
                                                            sPlanHtml += '<img id="' + seat.SeatLetter + '_' + seat.SeatNo + '" src="' + basePage + 'Images/SeatFullThisRes.png?v=3" alt="' + seat.SeatNo + seat.SeatLetter + '" style="width: ' + (data.PlaneZone[z].SeatWidth - 1) + 'px; height: 32px;' + (seat.thisReservation ? 'background-color: #c0e40a; border: 1px solid #808080;' : '') + '" />';
                                                        else
                                                            sPlanHtml += '<img id="' + seat.SeatLetter + '_' + seat.SeatNo + '" src="' + basePage + 'Images/SeatFull.png?v=3" alt="' + seat.SeatNo + seat.SeatLetter + '" style="width: ' + (data.PlaneZone[z].SeatWidth - 1) + 'px; height: 32px;' + (seat.thisReservation ? 'background-color: #c0e40a; border: 1px solid #808080;' : '') + '" />';
                                                }
                                            }
                                            else {
                                                sPlanHtml += '&nbsp;';
                                            }
                                        }
                                        else {
                                            sPlanHtml += '&nbsp;';
                                        }
                                        rowNumber = seat.SeatNo;
                                    }
                                    sPlanHtml += '</td>';
                                    sPlanHtml += '<td>&nbsp;';
                                    sPlanHtml += rowNumber;
                                    sPlanHtml += '</td>';

                                    sPlanHtml += '</tr>';
                                }
                                sPlanHtml += '</table>';
                                sPlanHtml += '</div>';
                            }
                            sPlanHtml += '</div>';
                            sPlan.append(sPlanHtml);
                            if (msg.d.SelectedSeat != '') {
                                /*
                                $("#custSeatReserv_" + custNo).html('');
                                $("#custSeatReserv_" + custNo).html(msg.d.SelectedSeat.NewSeat);
                                $("#" + seatNo).attr("src", "Images/SeatFull.png?v=3");
                                $("#" + msg.d.OldSeat.SeatNo + msg.d.OldSeat.SeatLetter).attr("src", OldSeatImg);
                                */
                                //$("#busSelectSeatDiv").html('');
                                //$("#busSelectSeatDiv").html(msg.d.SelectedSeat);
                            }
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getCustomers(serviceId) {
            var params = new Object();
            params.ServiceID = serviceId;
            $.ajax({
                async: false,
                type: "POST",
                url: "FlightSeatCheckIn.aspx/getCustomers",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {

                        $("#customerList").html('');
                        $("#customerList").append('<tr><th>&nbsp;</th><th class="custH">Passengers</th><th class="custSeatNumberH">Seat number</th><th class="custReservSeatNumberH">Selected seat</th></tr>');
                        $.each(msg.d.CustList, function (i) {
                            if (this.Seat != null || this.ReservedSeat != null) {
                                $("#customerList").append('<tr><td class="custSlct">&nbsp;</td><td class="cust">' + this.TitleStr + ' ' + this.Surname + ' ' + this.Name + '</td><td class="custSeatNumber" id="custSeat_' + this.CustNo + '">' + (this.Seat != null ? this.Seat.SeatNo + this.Seat.SeatLetter : '') + '</td><td class="custReservSeatNumber" id="custSeatReserv_' + this.CustNo + '">' + (this.ReservedSeat != null ? this.ReservedSeat.SeatNo + this.ReservedSeat.SeatLetter : '') + '</td></tr>');
                            }
                            else {
                                if (this.Title < 8) {
                                    $("#customerList").append('<tr><td class="custSlct"><img name="custImg"  id="custSeatSelect_' + this.CustNo + '" alt="" src="' + msg.d.BasePageRoot + '/Images/nochecked_16.gif" /> ' + '</td><td class="custSelect" onclick="selectCustumer(' + this.CustNo + ',' + msg.d.ServiceID + ',this);">' + this.TitleStr + '. ' + this.Surname + ' ' + this.Name + '</td><td class="custSeatNumber" id="custSeat_' + this.CustNo + '"></td><td class="custReservSeatNumber" id="custSeatReserv_' + this.CustNo + '"></td></tr>');
                                } else {
                                    $("#customerList").append('<tr><td class="custSlct">&nbsp;</td><td class="cust">' + this.TitleStr + '. ' + this.Surname + ' ' + this.Name + '</td><td class="custSeatNumber" id="custSeat_' + this.CustNo + '"></td><td class="custReservSeatNumberInfant" id="custSeatReserv_' + this.CustNo + '"></td></tr>');
                                }
                            }
                        });
                        if (msg.d.CustList.length > 0) {
                            $("#customerList").append('<tr id="saveButton" class="ui-helper-hidden"><td colspan="4" style=\"text-align: center;\" ><input type="button" value="<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>" onclick="saveSeat(' + msg.d.ServiceID + ');" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" style=\"width: 150px;\"/></td></tr>');
                    }
                }
                else {
                    showAlert("Service or Customers not found.");
                }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
    }

    function selectCustumer(custNo, serviceID, elm) {
        $("#busSelectSeatDiv").html('');
        var basePage = '<%= Global.getBasePageRoot().ToString() %>';
        $('img[name="custImg"]').attr('src', basePage + 'Images/nochecked_16.gif');
        $("#custSeatSelect_" + custNo).attr('src', basePage + 'Images/checked_16.gif');
        getSeatPlan(serviceID, custNo);
    }

    $(document).ready(function () {
        $.query = $.query.load(location.href);
        var recID = $.query.get('ServiceID');
        $("#hfServiceID").val(recID);
        getCustomers(recID);
        getSeatPlan(recID, null);
    });
    </script>

</head>
<body>
    <form id="seatCheckInForm" runat="server">
        <input id="hfServiceID" type="hidden" value="" />
        <div>
            <div class="seatCheckInDiv">
                <table class="custList" id="customerList">
                </table>
                <div id="flightSeatPlan" style="width: 300px;">
                </div>
            </div>

            <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
                style="display: none;">
                <span id="messages"></span>
            </div>
        </div>
    </form>
</body>
</html>
