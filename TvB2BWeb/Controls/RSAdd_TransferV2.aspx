﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RSAdd_TransferV2.aspx.cs" Inherits="Controls_RSAdd_TransferV2"
  EnableEventValidation="false" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
  TagPrefix="rjs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <!-- no cache headers -->
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="-1" />
  <meta http-equiv="Cache-Control" content="no-cache" />
  <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>
    <%= GetGlobalResourceObject("PageTitle", "RSAddTransfer") %></title>

  <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
  <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.json.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
  <script src="../Scripts/jQuery.dualListBox-1.2.js" type="text/javascript"></script>

  <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="../CSS/main.css" rel="stylesheet" type="text/css" />
  <link href="../CSS/RSAddStyleV2.css" rel="stylesheet" type="text/css" />

  <style type="text/css">
        </style>

  <script language="javascript" type="text/javascript">

    var iDateID = '<%= iDate.ClientID%>';
    var iDateRID = '<%= iDateR.ClientID%>';

    var lblPleaseWait = '<%= GetGlobalResourceObject("LibraryResource", "lblPleaseWait") %>';
    var btnOK = '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>';
    var ComboSelect = '<%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
    var addPleaseSelectDeparture = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectDeparture") %>';
    var addPleaseSelectArrival = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectArrival") %>';
    var addPleaseSelectTransfe = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectTransfer") %>';
    var addPleaseSelectTourist = '<%= GetGlobalResourceObject("Controls", "addPleaseSelectTourist") %>';
    var addPleaseCorrectDate = '<%= GetGlobalResourceObject("Controls", "addPleaseCorrectDate") %>';

    var trfViewAirportLocation = '<%= GetGlobalResourceObject("Controls", "trfViewAirportLocation") %>';
    var trfViewHotelLocation = '<%= GetGlobalResourceObject("Controls", "trfViewHotelLocation") %>';
    var trfViewFromHotelLocation = '<%= GetGlobalResourceObject("Controls", "trfViewFromHotelLocation") %>';
    var trfViewToHotelLocation = '<%= GetGlobalResourceObject("Controls", "trfViewToHotelLocation") %>';
    var trfFromAirportLocation = '<%= GetGlobalResourceObject("Controls", "trfFromAirportLocation") %>';
    var trfToAirportLocation = '<%= GetGlobalResourceObject("Controls", "trfToAirportLocation") %>';
    var lblRouteFr = '<%= GetGlobalResourceObject("Controls", "lblRouteFr") %>';
    var lblRouteTo = '<%= GetGlobalResourceObject("Controls", "lblRouteTo") %>';

    $.blockUI.defaults.message = '<h1>' + lblPleaseWait + '</h1>'; $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    function logout() {
      self.parent.logout();
    }

    function showMessage(msg) {
      $(function () {
        $("#messages").html(msg);
        $("#dialog").dialog("destroy");
        $("#dialog-message").dialog({
          modal: true,
          buttons: [{
            text: btnOK,
            click: function () {
              $(this).dialog('close');
            }
          }]
        });
      });
    }

    function DateChange(_TextBox, _PopCal) {
      var iDate = document.getElementById(iDateID);
      var _TextBoxWeek = $("#iTrfDate");
      _TextBoxWeek.value = _TextBox.value;
      if ((!_TextBox) || (!_PopCal)) return
      var _format = _TextBox.getAttribute("Format");
      var _Date = _PopCal.getDate(_TextBox.value, _format);
      if (_Date) {
        iDate.value = Date.parse(_Date);
      }
    }

    function DateChangeR(_TextBox, _PopCal) {
      var iDate = document.getElementById(iDateRID);
      var _TextBoxWeek = $("#iTrfDateR");
      _TextBoxWeek.value = _TextBox.value;
      if ((!_TextBox) || (!_PopCal)) return
      var _format = _TextBox.getAttribute("Format");
      var _Date = _PopCal.getDate(_TextBox.value, _format);
      if (_Date) {
        iDate.value = Date.parse(_Date);
      }
    }

    function getRouteFrom() {
      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_TransferV2.aspx/getTransferRouteFrom",
        data: '{"trfType":"' + $("#fltTrfType").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#iPickupTimeHH").val('');
          $("#iPickupTimeSS").val('');
          $("#iPickupNote").val('');
          $("#iSupplierNote").val('');
          $("#sTransfer").html('');
          $("#sArrLocation").html('');
          $("#sDepLocation").html('');
          $("#sDepLocation").append("<option value=''>" + ComboSelect + "</option>");
          if (msg.d != '') {
            $.each($.json.decode(msg.d), function (i) {
              $("#sDepLocation").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
            });

            if ($("#hfAirportLocation").val() != '') {
              $("#sDepLocation").val($("#hfAirportLocation").val());
              changeDepLocation();
            }
          }
          $("#sRegionFrom").val('');
          $("#sRegionDeparture").html('');
          $("#iDepLocation").val('');

          $("#sRegionTo").val('');
          $("#sRegionArrival").html('');
          $("#iArrLocation").val('');
          changeRegionFrom();
          changeRegionTo();
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });

      $(function () {
        $.configureBoxes();
      });
    }

    function getRouteTo() {
      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_TransferV2.aspx/getTransferRouteTo",
        data: '{"DepLocation":"' + $("#sDepLocation").val() + '","trfType":"' + $("#fltTrfType").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#iPickupTimeHH").val("");
          $("#iPickupTimeSS").val("");
          $("#iPickupNote").val('');
          $("#iSupplierNote").val('');
          $("#sTransfer").html('');
          $("#sArrLocation").html('');
          if (msg.d == '') return;
          $("#sArrLocation").append("<option value=''>" + ComboSelect + "</option>");
          if (msg.d != '') {
            var data = $.json.decode(msg.d);
            for (i = 0; i < data.length; i++) {
              $("#sArrLocation").append('<option value="' + data[i].RecID + '">' + data[i].Name + '</option>');
            }

            if ($("#hfHotelTrfLocation").val() != '') {
              $("#sArrLocation").val($("#hfHotelTrfLocation").val());
              changeArrLocation();
            }
          }
          $("#sRegionTo").val('');
          $("#sRegionArrival").html('');
          $("#iArrLocation").val('');
          changeRegionTo();
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}') {
            showMessage(xhr.responseText);
          }
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });

      $(function () {
        $.configureBoxes();
      });
    }

    function getTransfers() {
      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_TransferV2.aspx/getTransfers",
        data: '{"DepLocation":"' + $("#sDepLocation").val() + '","ArrLocation":"' + $("#sArrLocation").val() + '","trfType":"' + $("#fltTrfType").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

          $("#iPickupTimeHH").val('');
          $("#iPickupTimeSS").val('');
          $("#iPickupNote").val('');
          $("#iSupplierNote").val('');
          $("#sTransfer").html('');
          $("#sTransfer").html('');
          $("#sTransfer").append("<option value='' transType=''>" + ComboSelect + "</option>");
          if (msg.d != '') {
            $.each($.json.decode(msg.d), function (i) {
              $("#sTransfer").append("<option value='" + this.Code + "' transType='" + this.transType + "'><div>" + this.Name + "</div></option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });

      $(function () {
        $.configureBoxes();
      });
    }

    function changeDepLocation() {
      getRouteTo();
      $("#sRegionFrom").val('');
      $("#sRegionDeparture").html('');
      $("#sRegionFromR").val('');
      $("#sRegionDepartureR").html('');
      changeRegionFrom();
      changeRegionTo();
      changeRegionFromR();
      changeRegionToR();
    }

    function changeArrLocation() {
      getTransfers();
      $("#sRegionTo").val('');
      $("#sRegionArrival").html('');
      $("#sRegionToR").val('');
      $("#sRegionArrivalR").html('');
    }

    function changeTransfer() {
      $("#ppcCheckIn").attr('disabled', 'disabled');
      var data = new Object();
      data.DepLocation = parseInt($("#sDepLocation").val());
      data.ArrLocation = parseInt($("#sArrLocation").val());
      data.Transfer = $("#sTransfer").val();

      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_TransferV2.aspx/getTransferDates",
        data: $.json.encode(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          objPopCal = objPopCalList[0];
          objPopCal.Holidays = [];
          objPopCal.HolidaysCounter = 0;
          if (msg.hasOwnProperty('d') && msg.d != '') {

            var data = msg.d;

            if (data != null && data.transferDate != null) {
              $.each(data.transferDate, function (i) {
                PopCalAddSpecialDay(this.Day, this.Month, this.Year, this.Text, 0);
              });
            }
          }
          $("#ppcCheckIn").removeAttr('disabled', 'disabled');
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
          $("#ppcCheckIn").attr('disabled', 'disabled');
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function createTourist() {
      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_Hotel.aspx/getTourist",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#box1View").html("");
          $.each($.json.decode(msg.d), function (i) {
            $("#box1View").append("<option value='" + this.CustNo + "'>" + this.Name + "</option>");
          });
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });

      $(function () {
        $.configureBoxes();
      });
    }

    function preCalc(save) {
      if ($("#sDepLocation").val() == null || $("#sDepLocation").val() == '') {
        showMessage(addPleaseSelectDeparture);
        return;
      }
      if ($("#sArrLocation").val() == null || $("#sArrLocation").val() == '') {
        showMessage(addPleaseSelectArrival);
        return;
      }
      if ($("#sTransfer").val() == null || $("#sTransfer").val() == '') {
        showMessage(addPleaseSelectTransfer);
        return;
      }

      var selectCust = '';
      $("#box1View option").each(function () {
        if (selectCust.length > 0) selectCust += "|";
        selectCust += $(this).val();
      });
      if (selectCust.length < 1) {
        showMessage(addPleaseSelectTourist);
        return;
      }
      var begDate = $("#iTrfDate").val();
      var _begDateFormat = $("#TrfDate").attr("Format");
      if (begDate == '') {
        showMessage(addPleaseCorrectDate);
        return;
      }
      var iDate = document.getElementById(iDateID);
      var iDateR = document.getElementById(iDateRID);
      var _data = new Object();
      var _url = "";
      if (save) {
        _url = "../Controls/RSAdd_TransferV2.aspx/SaveService";
        _data.selectedCusts = selectCust;
        _data.Departure = $("#sDepLocation").val();
        _data.Arrival = $("#sArrLocation").val();
        _data.Transfer = $("#sTransfer").val();
        _data._Date = iDate.value;
        _data._DateR = iDateR.value;
        _data.PickupTimeHH = $("#iPickupTimeHH").val();
        _data.PickupTimeSS = $("#iPickupTimeSS").val();
        _data.PickupNote = $("#iPickupNote").val();
        _data.PickupTimeHHR = $("#iPickupTimeHHR").val();
        _data.PickupTimeSSR = $("#iPickupTimeSSR").val();
        _data.PickupNoteR = $("#iPickupNoteR").val();
        _data.recordType = $("#recordType").val();
        _data.trfType = $("#fltTrfType").val();
        _data.SuppNote = $("#iSupplierNote").val();

        _data.RegionFrom = parseInt($("#sRegionFrom").val());
        _data.RegionTo = parseInt($("#sRegionTo").val());
        _data.RegionFromLocation = $("#sRegionFrom").val() == '0' && $("#regionDepartureManual").attr("checked") == "checked" ? $("#iDepLocation").val() : $("#sRegionDeparture").val();
        _data.RegionToLocation = $("#sRegionTo").val() == '0' && $("#regionArrivalManual").attr("checked") == "checked" ? $("#iArrLocation").val() : $("#sRegionArrival").val();
        _data.RegionFromR = parseInt($("#sRegionFromR").val());
        _data.RegionToR = parseInt($("#sRegionToR").val());
        _data.RegionFromLocationR = $("#sRegionFromR").val() == '0' && $("#regionDepartureRManual").attr("checked") == "checked" ? $("#iDepLocationR").val() : $("#sRegionDepartureR").val();
        _data.RegionToLocationR = $("#sRegionToR").val() == '0' && $("#regionArrivalRManual").attr("checked") == "checked" ? $("#iArrLocationR").val() : $("#sRegionArrivalR").val();
        _data.VehicleCatID = $("#sVehicleCatID").length > 0 ? $("#sVehicleCatID").val() : null;
        _data.VehicleUnit = $("#sVehicleUnit").length > 0 ? $("#sVehicleUnit").val() : null;
      }
      else {
        _url = "../Controls/RSAdd_TransferV2.aspx/CalcService";
        _data.selectedCusts = selectCust;
        _data.Departure = $("#sDepLocation").val();
        _data.Arrival = $("#sArrLocation").val();
        _data.Transfer = $("#sTransfer").val();
        _data._Date = iDate.value;
        _data._DateR = iDateR.value;
        _data.trfType = $("#fltTrfType").val();
        _data.VehicleCatID = $("#sVehicleCatID").length > 0 ? $("#sVehicleCatID").val() : null;
        _data.VehicleUnit = $("#sVehicleUnit").length > 0 ? $("#sVehicleUnit").val() : null;
      }

      $.ajax({
        type: "POST",
        url: _url,
        data: $.json.encode(_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          var result = $.json.decode(msg.d);
          if (save) {
            window.close;
            self.parent.returnAddResServices(true, result.Supplier);
          }
          if (result.Price != '') {
            $("#iSalePrice").text(result.Price);
            //$("#iSupplier").text(result.Supplier);
            $("#iAdult").text(result.Adult);
            $("#iChild").text(result.Child);
            $("#iUnit").text(result.Unit);
          } else showMessage(result.Supplier);
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function exit(source) {
      if (source == 'save') {
        preCalc(true);
      }
      else if (source == 'cancel') {
        window.close;
        self.parent.returnAddResServices(false, '');
      }
    }

    function changeTrfType() {
      var trfType = $("#fltTrfType").val();
      switch (trfType) {
        case 'AHA':
          $("#labelDepLocation").html(trfViewAirportLocation);
          $("#labelArrLocation").html(trfViewHotelLocation);
          break;
        case 'AH':
          $("#labelDepLocation").html(trfViewAirportLocation);
          $("#labelArrLocation").html(trfViewHotelLocation);
          break;
        case 'HA':
          $("#labelDepLocation").html(trfViewHotelLocation);
          $("#labelArrLocation").html(trfViewAirportLocation);
          break;
        case 'AA':
          $("#labelDepLocation").html(trfFromAirportLocation);
          $("#labelArrLocation").html(trfToAirportLocation);
          break;
        case 'HH':
          $("#labelDepLocation").html(trfViewFromHotelLocation);
          $("#labelArrLocation").html(trfViewToHotelLocation);
          break;
        case 'OH':
          $("#labelDepLocation").html(trfViewAirportLocation);
          $("#labelArrLocation").html(trfViewHotelLocation);
          break;
        case 'HO':
          $("#labelDepLocation").html(trfViewHotelLocation);
          $("#labelArrLocation").html(trfViewAirportLocation);
          break;
        case 'RT':
          $("#labelDepLocation").html(trfViewAirportLocation);
          $("#labelArrLocation").html(trfViewHotelLocation);

          break;
        default:
          $("#labelDepLocation").html(lblRouteFr);
          $("#labelArrLocation").html(lblRouteTo);
          break;
      }

      getRouteFrom();
    }

    function getRegionFrom() {
      var iDate = document.getElementById(iDateID);
      var obj = new Object();
      obj.DepLocation = parseInt($("#sDepLocation").val());
      obj.ArrLocation = parseInt($("#sArrLocation").val());
      obj.Transfer = $("#sTransfer").val();
      obj.flyDate = iDate.value;
      obj.RegionFrom = parseInt($("#sRegionFrom").val());
      obj.Return = false;

      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_TransferV2.aspx/getRegionDeparture",
        data: $.json.encode(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#sRegionDeparture").html('');
          $("#iDepLocation").val('');

          $("#sRegionTo").val('');
          $("#sRegionArrival").html('');
          $("#iArrLocation").val('');

          $("#sRegionDeparture").append("<option value=''>" + ComboSelect + "</option>");
          if (msg.d != null) {
            $.each(msg.d, function (i) {
              $("#sRegionDeparture").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
            if (obj.RegionFrom == '1')
              $("#sRegionDeparture").val($("#hfHotel").val());

          } else
            if (obj.RegionFrom == 0) {
              $("#sRegionDeparture").hide();
              $("#iDepLocation").show();
            }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getRegionFromR() {
      var iDate = document.getElementById(iDateID);
      var obj = new Object();
      obj.DepLocation = parseInt($("#sDepLocation").val());
      obj.ArrLocation = parseInt($("#sArrLocation").val());
      obj.Transfer = $("#sTransfer").val();
      obj.flyDate = iDate.value;
      obj.RegionFrom = parseInt($("#sRegionFromR").val());
      obj.Return = true;

      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_TransferV2.aspx/getRegionDeparture",
        data: $.json.encode(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#sRegionDepartureR").html('');
          $("#iDepLocationR").val('');

          $("#sRegionToR").val('');
          $("#sRegionArrivalR").html('');
          $("#iArrLocationR").val('');

          $("#sRegionDepartureR").append("<option value=''>" + ComboSelect + "</option>");
          if (msg.d != null) {
            $.each(msg.d, function (i) {
              $("#sRegionDepartureR").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
            if (obj.RegionFrom == '1')
              $("#sRegionDepartureR").val($("#hfHotel").val());
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function regionDepartureManualClick() {
      if ($("#regionDepartureManual").attr("checked") == "checked") {
        $("#sRegionDeparture").hide();
        $("#iDepLocation").show();
      }
      else {
        $("#sRegionDeparture").show();
        $("#iDepLocation").hide();
      }
    }

    function changeRegionFrom() {
      $("#sRegionDeparture").html('');
      $("#iDepLocation").val('');
      if ($("#sRegionFrom").val() == '0') {
        $("#regionDepartureManualspan").show();
      }
      else {
        $("#regionDepartureManualspan").hide();
      }
      getRegionFrom();
    }

    function regionDepartureRManualClick() {
      if ($("#regionDepartureRManual").attr("checked") == "checked") {
        $("#sRegionDepartureR").hide();
        $("#iDepLocationR").show();
      }
      else {
        $("#sRegionDepartureR").show();
        $("#iDepLocationR").hide();
      }
    }

    function changeRegionFromR() {
      $("#sRegionDepartureR").html('');
      $("#iDepLocationR").val('');
      if ($("#sRegionFromR").val() == '0') {
        $("#regionDepartureRManualspan").show();
      }
      else {
        $("#regionDepartureRManualspan").hide();
      }
      getRegionFromR();
    }

    function getRegionTo() {
      var iDate = document.getElementById(iDateID);
      var obj = new Object();
      obj.ArrLocation = parseInt($("#sArrLocation").val());
      obj.DepLocation = parseInt($("#sDepLocation").val());
      obj.Transfer = $("#sTransfer").val();
      obj.flyDate = iDate.value;
      obj.RegionTo = parseInt($("#sRegionTo").val());
      obj.Return = false;

      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_TransferV2.aspx/getRegionArrival",
        data: $.json.encode(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          $("#sRegionArrival").html('');
          $("#iArrLocation").val('');
          $("#sRegionArrival").append("<option value=''>" + ComboSelect + "</option>");
          if (msg.d != null) {
            $.each(msg.d, function (i) {
              $("#sRegionArrival").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
            if (obj.RegionTo == '1')
              $("#sRegionArrival").val($("#hfHotel").val());
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function getRegionToR() {
      var iDate = document.getElementById(iDateRID);
      var obj = new Object();
      obj.ArrLocation = parseInt($("#sArrLocationR").val());
      obj.DepLocation = parseInt($("#sDepLocation").val());
      obj.Transfer = $("#sTransfer").val();
      obj.flyDate = iDate.value;
      obj.RegionTo = parseInt($("#sRegionToR").val());
      obj.Return = true;

      $.ajax({
        type: "POST",
        url: "../Controls/RSAdd_TransferV2.aspx/getRegionArrival",
        data: $.json.encode(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

          $("#sRegionArrivalR").html('');
          $("#iArrLocationR").val('');

          $("#sRegionArrivalR").append("<option value=''>" + ComboSelect + "</option>");
          if (msg.d != null) {
            $.each(msg.d, function (i) {
              $("#sRegionArrivalR").append("<option value='" + this.Code + "'>" + this.Name + "</option>");
            });
            if (obj.RegionTo == '1')
              $("#sRegionArrival").val($("#hfHotel").val());
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    function regionArrivalManualClick() {
      if ($("#regionArrivalManual").attr("checked") == "checked") {
        $("#sRegionArrival").hide();
        $("#iArrLocation").show();
      }
      else {
        $("#sRegionArrival").show();
        $("#iArrLocation").hide();
      }
    }

    function changeRegionTo() {
      $("#sRegionArrival").html('');
      $("#iArrLocation").val('');
      if ($("#sRegionTo").val() == '0') {
        $("#regionArrivalManualspan").show();
      }
      else {
        $("#regionArrivalManualspan").hide();
      }
      getRegionTo();
    }

    function regionArrivalRManualClick() {
      if ($("#regionArrivalRManual").attr("checked") == "checked") {
        $("#sRegionArrivalR").hide();
        $("#iArrLocationR").show();
      }
      else {
        $("#sRegionArrivalR").show();
        $("#iArrLocationR").hide();
      }
    }

    function changeRegionToR() {
      $("#sRegionArrivalR").html('');
      $("#iArrLocationR").val('');
      if ($("#sRegionToR").val() == '0') {
        $("#regionArrivalRManualspan").show();
      }
      else {
        $("#regionArrivalRManualspan").hide();
      }
      getRegionToR();
    }

    $(function () {
      $("#sTransfer").change(function () {
        var transType = $('option:selected', this).attr('transType');
        if (transType == "R") {
          $("#returnPickup").show();
          $("#divPickupTimeNoteR").show();
          $("#iTrfDateR").show();
          $("#returnDateDivSp").show();
          $("#returnDateDiv").show();
        }
        else {
          $("#returnPickup").hide();
          $("#divPickupTimeNoteR").hide();
          $("#iTrfDateR").hide();
          $("#returnDateDivSp").hide();
          $("#returnDateDiv").hide();
        }
        changeTransfer();
      });
    });

    function getFormData() {
      $("#sRegionDeparture").html('');
      $("#sRegionArrival").html('');
      $("#iDepLocation").hide();
      $("#iArrLocation").hide();
      $.ajax({
        async: false,
        type: "POST",
        url: "../Controls/RSAdd_TransferV2.aspx/getFormData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
          if (msg.hasOwnProperty('d') && msg.d != null) {
            var data = msg.d;

            $("#divOWRTSelect").data("TrfTypeList", data.TransferTypeList);

            if (data.TransferTypeList != null && data.TransferTypeList.length > 0) {
              $("#fltTrfType").html('');
              $.each(data.TransferTypeList, function (i) {
                $("#fltTrfType").append("<option value='" + this.Code + "'>" + this.Desc + "</option>");
              });
            }
            else {
              $("#fltTrfType").html('');
              $("#fltTrfType").append("<option value='" + data.DefaultType + "'></option>");
              $("#divOWRT").hide();
              $("#labelDepLocation").html(lblRouteFr);
              $("#labelArrLocation").html(lblRouteTo);
            }

            if (data.TransferExt == '1') {
              $("#divRegionFrom").show();
              $("#divRegionTo").show();
              $("#divPickupTime").show();
              $("#divPickupNote").show();
            } else {
              $("#divRegionFrom").hide();
              $("#divRegionTo").hide();
              $("#divPickupTime").hide();
              $("#divPickupNote").hide();
            }


            if (data.hotel != '') {
              $("#hfHotel").val(data.hotel);
            }

            if (data.HotelTrfLocation != null) {
              $("#hfHotelTrfLocation").val(data.HotelTrfLocation);
            }

            if (data.AirportLocation != null) {
              $("#hfAirportLocation").val(data.AirportLocation);
            }

            if (msg.d.showVehicleCat == true) {
              $("#divVehicleCatID").show();
            }
            else {
              $("#divVehicleCatID").hide();
            }

            $("#sVehicleCatID").html('');
            $("#sVehicleCatID").append("<option value=''>" + ComboSelect + "</option>");
            $.each(msg.d.vehicleCatList, function (i) {
              $("#sVehicleCatID").append("<option value='" + this.RecID + "'>" + this.Name + "</option>");
            });
          }
        },
        error: function (xhr, msg, e) {
          if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
            showMessage(xhr.responseText);
        },
        statusCode: {
          408: function () {
            logout();
          }
        }
      });
    }

    $(document).ready(function () {
      $.query = $.query.load(location.href);
      $("#recordType").val($.query.get('recordType'));
      /*
      $("#fltTrfType").val($("#hfTypeDefault").val());
      if ($("#fltTrfTypeParam").val() != '') {
        $("#fltTrfType").html('');
        $.each($.json.decode($("#fltTrfTypeParam").val()), function (i) {
          $("#fltTrfType").append("<option value='" + this.Code + "'>" + this.Desc + "</option>");
        });
      }
      else {
        $("#fltTrfType").html('');
        $("#fltTrfType").append("<option value='" + this.Code + "'></option>");
        $("#divOWRT").hide();
        $("#labelDepLocation").html(lblRouteFr);
        $("#labelArrLocation").html(lblRouteTo);
      }
      if ($("#transferExt").val() == '1') {
        $("#divRegionFrom").show();
        $("#divRegionTo").show();
        $("#divPickupTime").show();
        $("#divPickupNote").show();
      } else {
        $("#divRegionFrom").hide();
        $("#divRegionTo").hide();
        $("#divPickupTime").hide();
        $("#divPickupNote").hide();
      }
      */
      getFormData();

      changeTrfType();
      createTourist();
    });
  </script>

</head>
<body>
  <form id="formRsTransfer" runat="server">
    <div id="divRs">
      <div class="serviceInfoInputArea">
        <div id="divOWRT" class="ui-helper-clearfix">
          <div class="labelDiv">
            <span class="compulsoryField">* </span>
            <span class="label">
              <%= GetGlobalResourceObject("OnlyTicket", "lblTicketType")%>
                                    :</span>
          </div>
          <div id="divOWRTSelect" class="dataDiv">
            <select id="fltTrfType" onchange="changeTrfType();"></select>
          </div>
        </div>
        <div id="divRouteFrom" class="ui-helper-clearfix">
          <div class="labelDiv">
            <span class="compulsoryField">* </span>
            <span id="labelDepLocation" class="label"></span>:&nbsp;
          </div>
          <div id="divDepLocation" class="dataDiv">
            <select id="sDepLocation" class="combo50" onchange="changeDepLocation()">
            </select>
            <span class="ui-icon  ui-icon-arrowthick-1-e" style="float: left;"></span>
            <select id="sArrLocation" class="combo50" onchange="changeArrLocation()">
            </select>
          </div>
        </div>
        <div id="divTransfer" class="ui-helper-clearfix">
          <div class="labelDiv">
            <span class="label">
              <span class="compulsoryField">* </span>
              <%= GetGlobalResourceObject("Controls", "viewTransfer") %>
                                    :</span>
          </div>
          <div class="dataDiv">
            <select id="sTransfer" class="combo" onchange="changeTransfer()">
            </select>
          </div>
        </div>
        <div id="divVehicleCatID" class="ui-helper-clearfix">
          <div class="labelDiv">
            <span class="label">
              <%= GetGlobalResourceObject("Controls", "viewVehicleCat") %>
                                :</span>
          </div>
          <div class="dataDiv">
            <select id="sVehicleCatID" style="width: 95%;">
            </select>
          </div>
        </div>
        <div id="divVehicleUnit" class="ui-helper-clearfix">
          <div class="labelDiv">
            <span class="label">
              <%= GetGlobalResourceObject("Controls", "viewVehicleUnit") %>
                                :</span>
          </div>
          <div class="dataDiv">
            <input id="sVehicleUnit" style="width: 30%;" />
          </div>
        </div>
        <div id="divDate" class="ui-helper-clearfix">
          <div class="labelDiv">
            <span class="label">
              <span class="compulsoryField">* </span>
              <%= GetGlobalResourceObject("Controls", "viewTransferDate") %>
                                    :</span>
          </div>
          <div class="dataDiv">
            <div style="float: left;">
              <asp:TextBox ID="iTrfDate" ReadOnly="true" runat="server" Style="width: 120px;" />
              <rjs:PopCalendar ID="ppcCheckIn" runat="server" Control="iTrfDate" ClientScriptOnDateChanged="DateChange" />
            </div>
            <span id="returnDateDivSp" class="ui-icon  ui-icon-arrowthick-1-e" style="float: left; display: none;"></span>
            <div id="returnDateDiv" style="float: left; display: none;">
              <asp:TextBox ID="iTrfDateR" ReadOnly="true" runat="server" Style="width: 120px;" />
              <rjs:PopCalendar ID="ppcCheckInR" runat="server" Control="iTrfDateR" ClientScriptOnDateChanged="DateChangeR" />
            </div>
          </div>
        </div>
        <div id="divAdultChild" class="ui-helper-clearfix">
          <div class="labelDiv">
            <span class="label">
              <%= GetGlobalResourceObject("Controls", "viewAdult") %>
                                    :</span>
          </div>
          <div class="divAdultChild">
            <b><span id="iAdult"></span></b>
          </div>
          <div class="labelDiv">
            <span class="label">
              <%= GetGlobalResourceObject("Controls", "viewChild") %>
                                    :</span>
          </div>
          <div class="divAdultChild">
            <b><span id="iChild"></span></b>
          </div>
          <div class="labelDiv">
            <span class="label">
              <%= GetGlobalResourceObject("Controls", "viewUnit") %>
                                    :</span>
          </div>
          <div class="divAdultChild">
            <span id="iUnit"></span>
          </div>

        </div>
        <div class="ui-helper-clearfix">
          <div class="labelDiv">
            &nbsp;
          </div>
          <div class="pickupDeparture">
            <strong><%= GetGlobalResourceObject("Controls", "viewTrfFrom") %></strong>
          </div>
          <div class="pickupArrival">
            <strong><%= GetGlobalResourceObject("Controls", "viewTrfTo") %></strong>
          </div>
        </div>
        <div id="departurePickup" class="ui-helper-clearfix">
          <div class="labelDiv">
            <%= GetGlobalResourceObject("Controls", "viewDeparture") %> :
          </div>
          <div class="pickupDeparture">
            <div class="ui-helper-clearfix">
              <div id="divRegionFrom" style="display: block;">
                <div class="labelDiv">
                  <select id="sRegionFrom" class="combo" onchange="changeRegionFrom()">
                    <option value=""><%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %></option>
                    <option value="0"><%=GetGlobalResourceObject("LibraryResource", "AddServiceFlight") %></option>
                    <option value="1"><%=GetGlobalResourceObject("LibraryResource", "AddServiceHotel") %></option>
                    <option value="2"><%=GetGlobalResourceObject("LibraryResource", "AddServiceOther") %></option>
                  </select>
                </div>
                <div class="dataDiv">
                  <select id="sRegionDeparture" class="combo">
                  </select>
                  <input id="iDepLocation" type="text" class="ui-helper-hidden" />
                  <span id="regionDepartureManualspan" class="ui-helper-hidden">
                    <br />
                    <input id="regionDepartureManual" type="checkbox" onclick="regionDepartureManualClick()" /><label for="regionDepartureManual"><%=GetGlobalResourceObject("Controls", "lblManuelInput") %></label>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="pickupArrival">
            <div class="ui-helper-clearfix">
              <div id="divRegionTo" style="display: block;">
                <div class="labelDiv">
                  <select id="sRegionTo" class="combo" onchange="changeRegionTo()">
                    <option value=""><%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %></option>
                    <option value="0"><%=GetGlobalResourceObject("LibraryResource", "AddServiceFlight") %></option>
                    <option value="1"><%=GetGlobalResourceObject("LibraryResource", "AddServiceHotel") %></option>
                    <option value="2"><%=GetGlobalResourceObject("LibraryResource", "AddServiceOther") %></option>
                  </select>
                </div>
                <div class="dataDiv">
                  <select id="sRegionArrival" class="combo">
                  </select>
                  <input id="iArrLocation" type="text" class="ui-helper-hidden" />
                  <span id="regionArrivalManualspan" class="ui-helper-hidden">
                    <br />
                    <input id="regionArrivalManual" type="checkbox" onclick="regionArrivalManualClick()" /><label for="regionArrivalManual"><%=GetGlobalResourceObject("Controls", "lblManuelInput") %></label>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="divPickupTimeNote" class="ui-helper-clearfix" style="display: block;">
          <div class="labelDiv">
            <%= GetGlobalResourceObject("Controls", "viewPickupTimeNote") %> :                  
          </div>
          <div class="dataDiv">
            <select id="iPickupTimeHH">
              <option value=""></option>
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
            </select>
            &nbsp;
                        <select id="iPickupTimeSS">
                          <option value=""></option>
                          <option value="00">00</option>
                          <option value="05">05</option>
                          <option value="10">10</option>
                          <option value="15">15</option>
                          <option value="20">20</option>
                          <option value="25">25</option>
                          <option value="30">30</option>
                          <option value="35">35</option>
                          <option value="40">40</option>
                          <option value="45">45</option>
                          <option value="50">50</option>
                          <option value="55">55</option>
                        </select>
            &nbsp;&nbsp;
                        <input id="iPickupNote" maxlength="50" />
          </div>
        </div>
        <div id="returnPickup" class="ui-helper-clearfix ui-helper-hidden">
          <div class="labelDiv">
            <%= GetGlobalResourceObject("Controls", "viewReturn") %> :
          </div>
          <div class="pickupDeparture">
            <div class="ui-helper-clearfix">
              <div id="divRegionFromR" style="display: block;">
                <div class="labelDiv">
                  <select id="sRegionFromR" class="combo" onchange="changeRegionFromR()">
                    <option value=""><%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %></option>
                    <option value="0"><%=GetGlobalResourceObject("LibraryResource", "AddServiceFlight") %></option>
                    <option value="1"><%=GetGlobalResourceObject("LibraryResource", "AddServiceHotel") %></option>
                    <option value="2"><%=GetGlobalResourceObject("LibraryResource", "AddServiceOther") %></option>
                  </select>
                </div>
                <div class="dataDiv">
                  <select id="sRegionDepartureR" class="combo">
                  </select>
                  <input id="iDepLocationR" type="text" class="ui-helper-hidden" />
                  <span id="regionDepartureRManualspan" class="ui-helper-hidden">
                    <br />
                    <input id="regionDepartureRManual" type="checkbox" onclick="regionDepartureRManualClick()" /><label for="regionDepartureRManual">Manuel input</label>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="pickupArrival">
            <div class="ui-helper-clearfix">
              <div id="divRegionToR" style="display: block;">
                <div class="labelDiv">
                  <select id="sRegionToR" class="combo" onchange="changeRegionToR()">
                    <option value=""><%=GetGlobalResourceObject("LibraryResource", "ComboSelect") %></option>
                    <option value="0"><%=GetGlobalResourceObject("LibraryResource", "AddServiceFlight") %></option>
                    <option value="1"><%=GetGlobalResourceObject("LibraryResource", "AddServiceHotel") %></option>
                    <option value="2"><%=GetGlobalResourceObject("LibraryResource", "AddServiceOther") %></option>
                  </select>
                </div>
                <div class="dataDiv">
                  <select id="sRegionArrivalR" class="combo">
                  </select>
                  <input id="iArrLocationR" type="text" style="display: none;" />
                  <span id="regionArrivalRManualspan" class="ui-helper-hidden">
                    <br />
                    <input id="regionArrivalRManual" type="checkbox" onclick="regionArrivalRManualClick()" /><label for="regionArrivalRManual">Manuel input</label>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="divPickupTimeNoteR" class="ui-helper-clearfix ui-helper-hidden">
          <div class="labelDiv">
            <%= GetGlobalResourceObject("Controls", "viewPickupTimeNote") %> :                                    
          </div>
          <div class="dataDiv">
            <select id="iPickupTimeHHR">
              <option value=""></option>
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
            </select>
            &nbsp;
                        <select id="iPickupTimeSSR">
                          <option value=""></option>
                          <option value="00">00</option>
                          <option value="05">05</option>
                          <option value="10">10</option>
                          <option value="15">15</option>
                          <option value="20">20</option>
                          <option value="25">25</option>
                          <option value="30">30</option>
                          <option value="35">35</option>
                          <option value="40">40</option>
                          <option value="45">45</option>
                          <option value="50">50</option>
                          <option value="55">55</option>
                        </select>
            &nbsp;&nbsp;
                        <input id="iPickupNoteR" maxlength="50" />
          </div>
        </div>

        <div id="divSupplierNote" class="ui-helper-clearfix ui-helper-hidden">
          <div class="labelDiv">
            <span class="label">
              <%= GetGlobalResourceObject("Controls", "viewSupplierNote") %>
                                    :</span>
          </div>
          <div class="dataDiv">
            <input id="iSupplierNote" type="text" class="input" maxlength="150" />
          </div>
        </div>
      </div>
      <div>
        <div>
          <div id="divSalePrice">
            <span class="label">
              <%= GetGlobalResourceObject("Controls", "viewSalePrice") %>
                                :</span> <span id="iSalePrice" class="salePrice"></span>
          </div>
          <div id="divCalcBtn">
            <input id="btnRecalc" type="button" value='<%= GetGlobalResourceObject("Controls", "btnReCalc") %>'
              onclick="preCalc(false);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
              style="width: 150px;" />
          </div>
        </div>
        <table cellpadding="2" cellspacing="0">
          <tr>
            <td align="center">
              <b>
                <%= GetGlobalResourceObject("Controls", "lblServiceTourist")%></b>
            </td>
            <td></td>
            <td align="center">
              <b>
                <%= GetGlobalResourceObject("Controls", "lblOtherTourist")%></b>
            </td>
          </tr>
          <tr>
            <td style="width: 290px;" align="left">
              <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box1Filter" />
              <button type="button" id="box1Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                X</button><br />
              <select id="box1View" multiple="multiple" style="width: 100%; height: 125px;">
              </select><br />
              <%--<span id="box1Counter" class="countLabel"></span>--%>
              <select id="box1Storage">
              </select>
            </td>
            <td style="width: 40px;" align="center">
              <button id="to2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                >
              </button>
              <br />
              <button id="allTo2" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                >>
              </button>
              <br />
              <button id="allTo1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                <<
              </button>
              <br />
              <button id="to1" type="button" style="width: 34px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                <
              </button>
            </td>
            <td style="width: 290px;" align="left">
              <%= GetGlobalResourceObject("Controls", "lblFilter")%>:
                        <input type="text" id="box2Filter" />
              <button type="button" id="box2Clear" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                X</button><br />
              <select id="box2View" multiple="multiple" style="width: 100%; height: 125px;">
              </select><br />
              <%--<span id="box2Counter" class="countLabel"></span>--%>
              <select id="box2Storage">
              </select>
            </td>
          </tr>
        </table>
      </div>
      <table id="divBtn">
        <tr>
          <td style="width: 50%;" align="center">
            <input id="btnSave" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnSave") %>'
              onclick="exit('save');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
              style="width: 100px;" />
          </td>
          <td align="center">
            <input id="btnCalcel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
              onclick="exit('cancel');" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
              style="width: 100px;" />
          </td>
        </tr>
      </table>
    </div>
    <div id="dialog-message" title="" style="display: none;">
      <p>
        <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span><span id="messages">Message</span>
      </p>
    </div>
    <%-- Hiiden fields --%>
    <div class="ui-helper-hidden">
      <input id="recordType" type="hidden" value="" />
      <asp:HiddenField ID="transferExt" runat="server" Value="0" />
      <asp:HiddenField ID="iDate" runat="server" />
      <asp:HiddenField ID="iDateR" runat="server" />
      <asp:HiddenField ID="hfTypeDefault" runat="server" Value="AHA" />
      <asp:HiddenField ID="fltTrfTypeParam" runat="server" Value="" />
      <asp:HiddenField ID="hfHotel" runat="server" Value="" />
      <asp:HiddenField ID="hfHotelTrfLocation" runat="server" Value="" />
      <asp:HiddenField ID="hfAirportLocation" runat="server" Value="" />
    </div>
    <%-- Hiiden fields --%>
  </form>
  <%--<div id="divSupplier" class="ui-helper-clearfix">
                    <div class="labelDiv">
                        <span class="label">
                            <%= GetGlobalResourceObject("Controls", "viewSupplier") %>
                                    :</span>
                    </div>
                    <div class="dataDiv">
                        <b><span id="iSupplier"></span></b>
                    </div>
                </div>--%>
</body>
</html>
