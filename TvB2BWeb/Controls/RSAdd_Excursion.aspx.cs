﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using System.Text;
using TvTools;
using System.Web.Script.Services;

public partial class Controls_RSAdd_Excursion : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = new ResDataRecord();
        if (!ResData.ResMain.MemTable)
        {
            if (((ResDataRecord)Session["ResData"]).ExtrasData != null)
                ResData = ((ResDataRecord)Session["ResData"]).ExtrasData;
            else
                ResData = ((ResDataRecord)Session["ResData"]);
        }
        else
        {
            ResData = (ResDataRecord)Session["ResData"];
        }
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ppcDate.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcDate.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');

        if (!IsPostBack)
        {
            ppcDate.DateValue = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
            Int64 days = (ppcDate.DateValue - new DateTime(1970, 1, 1)).Days - 1;
            hfDate.Value = (days * 86400000).ToString();
            if ((string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel)) && string.IsNullOrEmpty(ResData.ResMain.PackType))
            {
                ppcDate.From.Date = DateTime.Today;
            }
            else
            {
                if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel))
                {
                    ppcDate.From.Date = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
                    ppcDate.DateValue = ppcDate.From.Date;
                    days = (ppcDate.DateValue - new DateTime(1970, 1, 1)).Days - 1;
                    hfDate.Value = (days * 86400000).ToString();
                }
                else
                {
                    ppcDate.From.Date = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
                }
                if (ResData.ResMain.EndDate.HasValue)
                    ppcDate.To.Date = ResData.ResMain.EndDate.Value;
            }
            if (string.Equals(UserData.CustomRegID, Common.crID_ZemExpert) && string.Equals(ResData.ResMain.PackType, "T"))
            {
                if (ResData.ResMain.BegDate.HasValue)
                    ppcDate.From.Date = ResData.ResMain.BegDate.Value.AddDays(-3);
                if (ResData.ResMain.EndDate.HasValue)
                    ppcDate.To.Date = ResData.ResMain.EndDate.Value.AddDays(3);
            }
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<CodeName> excurType = new List<CodeName>();
        excurType.Add(new CodeName { Code = "0", Name = HttpContext.GetGlobalResourceObject("OnlyExcursion", "ExcursionType0").ToString() });
        excurType.Add(new CodeName { Code = "2", Name = HttpContext.GetGlobalResourceObject("OnlyExcursion", "ExcursionType2").ToString() });
        if (ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).Count() > 0)
            excurType.Add(new CodeName { Code = "3", Name = HttpContext.GetGlobalResourceObject("OnlyExcursion", "ExcursionType3").ToString() });
        List<VehicleCategoryRecord> vehicleCatList = new ReservationRequestForm().getVehicleCategory(UserData, ref errorMsg);
        return new
        {
            showSearchType = string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel),
            searchType = excurType,
            showVehicleCat = false/*string.Equals(UserData.CustomRegID, Common.crID_Qasswa)*/,
            vehicleCatList = vehicleCatList
        };
    }

    [WebMethod(EnableSession = true)]
    public static string getLocation(int? Type, string ExcurDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        Int32 _dateA = Convert.ToInt32((Convert.ToInt64(ExcurDate) / 86400000).ToString());
        DateTime _dateB = new DateTime(1970, 1, 1);
        DateTime? Date = _dateB.AddDays(_dateA + 1);
        if (!Date.HasValue) Date = DateTime.Today;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        List<Location> excurLoc = new List<Location>();
        if (Type == 3)
        {
            List<ExcursionSearchFilterData> filterData = new ExcursionSearch().getExcursionPackFilterData(UserData, Date.Value, ref errorMsg);
            List<Location> loc = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);
            excurLoc = (from q1 in loc
                        join q2 in filterData on q1.RecID equals q2.Location
                        select q1).ToList<Location>();
        }
        else
            if (Type == 0)
            {
                List<ExcursionSearchFilterData> filterData = new ExcursionSearch().getExcursionFilterData(UserData, Date, ref errorMsg);
            //List<Location> loc = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);
            List<Location> loc = new Locations().getLocationList(UserData, LocationType.None, null, new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg), null, null, ref errorMsg);
            excurLoc = (from q1 in loc
                            join q2 in filterData on q1.RecID equals q2.Location
                            where q2.Type == ExcursionType.Regular
                            select q1).ToList<Location>();
            }
            else
                if (Type == 2)
                {
                    List<ExcursionSearchFilterData> filterData = new ExcursionSearch().getExcursionFilterData(UserData, Date, ref errorMsg);
                    List<Location> loc = new Locations().getLocationList(UserData, LocationType.None, null, null, null, null, ref errorMsg);
                    excurLoc = (from q1 in loc
                                join q2 in filterData on q1.RecID equals q2.Location
                                where q2.Type == ExcursionType.Private
                                select q1).ToList<Location>();
                }
                else
                    excurLoc = new Excursions().getExcursionLocation(UserData, ResData.ResMain.PLMarket, new Locations().getLocationForCountry(ResData.ResMain.ArrCity.Value, ref errorMsg), ResData.ResMain.ArrCity, ref errorMsg);

        ResServiceRecord hotel = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).FirstOrDefault();
        int? defLoc = null;
        if (hotel != null)
            defLoc = hotel.DepLocation;
        var query = from q in excurLoc
                    orderby q.RecID
                    group q by new { RecID = q.RecID, Name = q.NameL } into k
                    select new { RecID = k.Key.RecID, Name = k.Key.Name, DefaultLocation = defLoc };

        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod(EnableSession = true)]
    public static string getExcursion(string Location, int? Type, string ExcurDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        string errorMsg = string.Empty;

        Int32 _dateA = Convert.ToInt32((Convert.ToInt64(ExcurDate) / 86400000).ToString());
        DateTime _dateB = new DateTime(1970, 1, 1);
        DateTime? Date = _dateB.AddDays(_dateA + 1);
        if (!Date.HasValue) Date = DateTime.Today;

        Int32? excurLoc = Conversion.getInt32OrNull(Location);
        if (Type == 3)
        {
            bool hasHotel = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).Count() > 0;
            bool hasFlight = ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).Count() > 0;
            ExcursionSearchCriteria criteria = new ExcursionSearchCriteria();
            criteria.ArrCity = excurLoc;
            criteria.Category = string.Empty;
            criteria.CheckIn = Date.Value;
            criteria.RoomCount = 1;
            List<SearchCriteriaRooms> rooms = new List<SearchCriteriaRooms>();
            rooms.Add(new SearchCriteriaRooms { Adult = ResData.ResMain.Adult.Value, Child = ResData.ResMain.Child.Value });
            criteria.RoomsInfo = rooms;
            criteria.SType = SearchType.OnlyExcursion;
            criteria.Type = ExcursionType.Tour;
            if (string.Equals(UserData.CustomRegID, Common.crID_UpJet))
            {
                List<ExcursionSearchResult> excurPack = new ExcursionSearch().searchExcursionPackage(UserData, ResData, criteria, hasHotel, hasFlight, ref errorMsg);

                foreach (ExcursionSearchResult row in excurPack)
                {
                    var queryDet = row.PackageDetails.OrderBy(o => o.CheckIn).FirstOrDefault();
                    if (queryDet != null)
                        row.CheckIn = queryDet.CheckIn;

                }
                var query = from q in excurPack
                            where q.CheckIn == Date.Value
                            //orderby q.CheckIn
                            select new { Code = q.RefNo, Name = (useLocalName ? q.PackageNameL : q.PackageName) };
                return Newtonsoft.Json.JsonConvert.SerializeObject(query);
            }
            else
            {
                List<ExcursionPack> excPackList = new Excursions().getExcursionPackList(UserData, ref errorMsg);
                var query = from q in excPackList
                            select new { Code = q.RecID, Name = useLocalName ? q.NameL : q.NameL };
                return Newtonsoft.Json.JsonConvert.SerializeObject(query);
            }
        }
        else
            if (Type.HasValue && Type != 1)
            {
                List<ExcursionRecord> excursion = new Excursions().getExcursionListForLocationAndDate(UserData, ResData.ResMain.PLMarket, excurLoc, Date, Date, false, ref errorMsg);
                var query = from q in excursion
                            where q.Type == (ExcursionType)Type
                            select new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name };
                return Newtonsoft.Json.JsonConvert.SerializeObject(query);
            }
            else
                if (Type.HasValue && Type == 1)
                {
                    List<ExcursionRecord> excursion = new Excursions().getExcursionListForLocationAndDate(UserData, ResData.ResMain.PLMarket, excurLoc, Date, Date, false, ref errorMsg);
                    var query = from q in excursion
                                select new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(query);
                }
                else
                {
                    List<ExcursionRecord> excursion = new Excursions().getExcursionListForLocationAndDate(UserData, ResData.ResMain.PLMarket, excurLoc, ResData.ResMain.BegDate, ResData.ResMain.EndDate, false, ref errorMsg);
                    var query = from q in excursion
                                select new { Code = q.Code, Name = useLocalName ? q.LocalName : q.Name };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(query);
                }
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist(string excursionCode)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retval = string.Empty;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        Int16? minAge = null;
        if (!string.IsNullOrEmpty(excursionCode))
        {
            ExcursionRecord exc = new Excursions().getExcursion(UserData, excursionCode, ref errorMsg);
            if (exc != null && exc.MinAgeRest.HasValue)
                minAge = exc.MinAgeRest.Value;
        }
        var query = from s in ResData.ResCust
                    where s.Status == 0 &&
                    (!minAge.HasValue || (minAge.Value <= (s.Age.HasValue ? s.Age.Value : 999)))
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getExcursionDetail(string excursionCode)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retval = string.Empty;
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];        
        if (!string.IsNullOrEmpty(excursionCode))
        {
            ExcursionPack excPack = new Excursions().getExcursionPack(UserData, Conversion.getInt32OrNull(excursionCode), ref errorMsg);
            if (excPack == null) return string.Empty;
            List<ExcursionRecord> excPackDetailList = new Excursions().getExcursionPackDetail(UserData, ResData.ResMain.PLMarket, Conversion.getInt32OrNull(excursionCode), ref errorMsg);
            retval += string.Format("<input id=\"minSaleUnit\" type=\"hidden\" value=\"{0}\" />", excPack.MinSaleUnit.HasValue ? excPack.MinSaleUnit.Value.ToString() : "");
            foreach (ExcursionRecord row in excPackDetailList)
            {
                retval += string.Format("<input type=\"checkbox\" id=\"Excur_{0}\" name=\"ExcPackDet\" /><label for=\"Excur_{0}\">{1}</label><br />", row.Code, useLocalName ? row.LocalName : row.Name);
            }
            retval = "<div id=\"ExcPackDetList\">" + retval + "</div>";
        }        
        return retval;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static List<calendarColor> getExcursionDates(int? Location, string Excursion, int? Type)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        if (Type == 3)
        {
            List<calendarColor> retVal = new ExcursionSearch().getExcursionPackDates(UserData, ResData, Location, Conversion.getInt32OrNull(Excursion), ref errorMsg);
            return retVal;
        }
        else
        {
            List<calendarColor> retVal = new Excursions().getExcursionDates(UserData, ResData.ResMain.PLMarket,
                                                                string.IsNullOrEmpty(ResData.ResMain.PackType) ? DateTime.Today : ResData.ResMain.BegDate,
                                                                string.IsNullOrEmpty(ResData.ResMain.PackType) ? DateTime.Today.AddYears(1) : ResData.ResMain.EndDate,
                                                                Location, Excursion, ref errorMsg);
            return retVal;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static addServiceReturnData CalcService(string selectedCusts, string Location, string Excursion, string ExcurDate, int? Type, string ExcList, int? VehicleCatID, Int16? VehicleUnit)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? location = Conversion.getInt32OrNull(Location);
        Int32 _dateA = Convert.ToInt32((Convert.ToInt64(ExcurDate) / 86400000).ToString());
        DateTime _dateB = new DateTime(1970, 1, 1);
        DateTime? Date = _dateB.AddDays(_dateA + 1);
        if (!Date.HasValue) return new addServiceReturnData { Calc = false, Msg = "Date error.." };
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        List<calendarColor> excursionDays = new Excursions().getExcursionDates(UserData, ResData.ResMain.PLMarket,
                                                                string.IsNullOrEmpty(ResData.ResMain.PackType) ? Date.Value : ResData.ResMain.BegDate,
                                                                string.IsNullOrEmpty(ResData.ResMain.PackType) ? DateTime.Today.AddYears(1) : ResData.ResMain.EndDate,
                                                                location, Excursion, ref errorMsg);
        if (excursionDays.Count > 0)
        {
            var query = from q in excursionDays
                        select new
                        {
                            excursionDay = new DateTime(q.Year.HasValue ? q.Year.Value : DateTime.Today.Year,
                                                        q.Month.HasValue ? q.Month.Value : DateTime.Today.Month,
                                                        q.Day.HasValue ? q.Day.Value : DateTime.Today.Day)
                        };
            if (query.Where(w => w.excursionDay == Date.Value).Count() < 1)
            {
                return new addServiceReturnData
                {
                    Calc = false,
                    Msg = HttpContext.GetGlobalResourceObject("Controls", "addPleaseCorrectDate").ToString()
                };
            }
        }
        string[] selectedCust = selectedCusts.Split('|');

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((Date.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;
        if (Type == 3)
        {
            bool hasHotel = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).Count() > 0;
            bool hasFlight = ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).Count() > 0;

            ExcursionSearchCriteria criteria = new ExcursionSearchCriteria();
            criteria.ArrCity = location;
            criteria.Category = string.Empty;
            criteria.CheckIn = Date.Value;
            criteria.RoomCount = 1;

            if (string.IsNullOrEmpty(ExcList))
            {
                List<SearchCriteriaRooms> rooms = new List<SearchCriteriaRooms>();
                rooms.Add(new SearchCriteriaRooms { Adult = Convert.ToInt16(AdlCnt.ToString()), Child = Convert.ToInt16(ChdCnt.ToString()) });
                criteria.RoomsInfo = rooms;
                criteria.SType = SearchType.OnlyExcursion;
                criteria.Type = ExcursionType.Tour;
                List<ExcursionSearchResult> pckDetail = new ExcursionSearch().getPackageDetail(UserData, criteria, Conversion.getInt32OrNull(Excursion), hasHotel, hasFlight, ref errorMsg);
                List<int> serviceIDList = new List<int>();
                decimal? totalPrice = 0;
                foreach (ExcursionSearchResult row in pckDetail)
                {
                    StartDay = Convert.ToInt16(((row.CheckIn.HasValue ? row.CheckIn.Value : Date.Value) - ResData.ResMain.BegDate.Value).Days);
                    Night = 0;
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, row.CheckIn.HasValue ? row.CheckIn.Value : Date.Value, row.CheckIn.HasValue ? row.CheckIn.Value : Date.Value, "EXCURSION", row.Code,
                                                       string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, location, null, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, Conversion.getInt32OrNull(Excursion), ref errorMsg, null, string.Empty, null, null, null, null, VehicleUnit);
                    if (!string.IsNullOrEmpty(errorMsg))
                    {
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                        return new addServiceReturnData
                        {
                            Calc = false,
                            Msg = errorMsg
                        };
                    }
                    else
                    {
                        int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                        ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
                        DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "EXCURSION", ServiceID, ref errorMsg);
                        if (!string.IsNullOrEmpty(errorMsg))
                        {                            
                            return new addServiceReturnData
                            {
                                Calc = false,
                                Msg = errorMsg
                            };
                        }
                        else
                        {
                            totalPrice += Conversion.getDecimalOrNull(returnData.Rows[0]["SalePrice"]).HasValue ? Conversion.getDecimalOrNull(returnData.Rows[0]["SalePrice"]) : (decimal)0;
                            serviceIDList.Add(ServiceID);
                        }
                    }
                }


                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"Adult\":\"{2}\",\"Child\":\"{3}\",\"Unit\":\"{4}\"",
                                totalPrice.ToString() + " " + ResData.ResMain.SaleCur.ToString(),
                                "",
                                AdlCnt,
                                ChdCnt,
                                1);
                return new addServiceReturnData
                {
                    Calc = true,
                    Adult = AdlCnt,
                    Child = ChdCnt,
                    Unit = 1,
                    CalcPrice = totalPrice.HasValue ? totalPrice.Value.ToString("#,###.00") : "",
                    CalcCur = ResData.ResMain.SaleCur
                };
            }
            else
            {
                List<int> serviceIDList = new List<int>();
                decimal? totalPrice = 0;
                List<string> excursionList = ExcList.Split(';').ToList();
                foreach (string row in excursionList)
                {
                    StartDay = Convert.ToInt16(((Date.HasValue ? Date.Value : Date.Value) - ResData.ResMain.BegDate.Value).Days);
                    Night = 0;
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.HasValue ? Date.Value : Date.Value, Date.HasValue ? Date.Value : Date.Value, "EXCURSION", row,
                                                       string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, location, null, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, Conversion.getInt32OrNull(Excursion), ref errorMsg, null, string.Empty, null, null, null, null, VehicleUnit);
                    if (!string.IsNullOrEmpty(errorMsg))
                    {
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                        return new addServiceReturnData
                        {
                            Calc = false,
                            Msg = errorMsg
                        };
                    }
                    else
                    {
                        int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                        ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
                        DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "EXCURSION", ServiceID, ref errorMsg);
                        if (!string.IsNullOrEmpty(errorMsg))
                        {
                            //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                            return new addServiceReturnData
                            {
                                Calc = false,
                                Msg = errorMsg
                            };
                        }
                        else
                        {
                            totalPrice += Conversion.getDecimalOrNull(returnData.Rows[0]["SalePrice"]).HasValue ? Conversion.getDecimalOrNull(returnData.Rows[0]["SalePrice"]) : (decimal)0;
                            serviceIDList.Add(ServiceID);
                        }
                    }
                }
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"Adult\":\"{2}\",\"Child\":\"{3}\",\"Unit\":\"{4}\"",
                                totalPrice.ToString() + " " + ResData.ResMain.SaleCur.ToString(),
                                "",
                                AdlCnt,
                                ChdCnt,
                                1);
                return new addServiceReturnData
                {
                    Calc = true,
                    Adult = AdlCnt,
                    Child = ChdCnt,
                    Unit = 1,
                    CalcPrice = totalPrice.HasValue ? totalPrice.Value.ToString("#,###.00") : "",
                    CalcCur = ResData.ResMain.SaleCur
                };
            }
        }
        else
        {
            ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.Value, Date.Value, "EXCURSION", Excursion,
                                    string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, location, null, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, VehicleUnit);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                return new addServiceReturnData
                {
                    Calc = false,
                    Msg = errorMsg
                };
            }
            else
            {
                int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
                DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "EXCURSION", ServiceID, ref errorMsg);
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    return new addServiceReturnData
                    {
                        Calc = false,
                        Msg = errorMsg
                    };
                }
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
                //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"Adult\":\"{2}\",\"Child\":\"{3}\",\"Unit\":\"{4}\"",
                //        returnData.Rows[0]["SalePrice"].ToString() + " " + returnData.Rows[0]["SaleCur"].ToString(),
                //        supplierRec.NameL,
                //        AdlCnt,
                //        ChdCnt,
                //        service.Unit);
                return new addServiceReturnData
                {
                    Calc = true,
                    Adult = AdlCnt,
                    Child = ChdCnt,
                    Unit = service.Unit,
                    CalcPrice = returnData.Rows[0]["SalePrice"].ToString(),
                    CalcCur = returnData.Rows[0]["SaleCur"].ToString()
                };
            }

        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static addServiceReturnData SaveService(string selectedCusts, string Location, string Excursion, string ExcurDate, string recordType, int? Type, string ExcList, int? VehicleCatID, Int16? VehicleUnit)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        Int32? location = Conversion.getInt32OrNull(Location);
        Int32 _dateA = Convert.ToInt32((Convert.ToInt64(ExcurDate) / 86400000).ToString());
        DateTime _dateB = new DateTime(1970, 1, 1);
        DateTime? Date = _dateB.AddDays(_dateA + 1);
        if (!Date.HasValue) return new addServiceReturnData { Calc = false, Msg = "Date error.." };
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        List<calendarColor> excursionDays = new Excursions().getExcursionDates(UserData, ResData.ResMain.PLMarket,
                                                                string.IsNullOrEmpty(ResData.ResMain.PackType) ? Date.Value : ResData.ResMain.BegDate,
                                                                string.IsNullOrEmpty(ResData.ResMain.PackType) ? DateTime.Today.AddYears(1) : ResData.ResMain.EndDate,
                                                                location, Excursion, ref errorMsg);
        if (excursionDays.Count > 0)
        {
            var query = from q in excursionDays
                        select new
                        {
                            excursionDay = new DateTime(q.Year.HasValue ? q.Year.Value : DateTime.Today.Year,
                                                        q.Month.HasValue ? q.Month.Value : DateTime.Today.Month,
                                                        q.Day.HasValue ? q.Day.Value : DateTime.Today.Day)
                        };
            if (query.Where(w => w.excursionDay == Date.Value).Count() < 1)
            {
                return new addServiceReturnData
                {
                    Calc = false,
                    Msg = HttpContext.GetGlobalResourceObject("Controls", "addPleaseCorrectDate").ToString()
                };
            }
        }

        string[] selectedCust = selectedCusts.Split('|');
        //TvBo.ExcursionPriceRecord excurPrice = new Excursions().(UserData.Market, Excursion, Date.Value, ref errorMsg);

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((Date.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;
        if (Type == 3)
        {
            bool hasHotel = ResData.ResService.Where(w => string.Equals(w.ServiceType, "HOTEL")).Count() > 0;
            bool hasFlight = ResData.ResService.Where(w => string.Equals(w.ServiceType, "FLIGHT")).Count() > 0;
            ExcursionSearchCriteria criteria = new ExcursionSearchCriteria();
            criteria.ArrCity = location;
            criteria.Category = string.Empty;
            criteria.CheckIn = Date.Value;
            criteria.RoomCount = 1;
            if (string.IsNullOrEmpty(ExcList))
            {
                List<SearchCriteriaRooms> rooms = new List<SearchCriteriaRooms>();
                rooms.Add(new SearchCriteriaRooms { Adult = Convert.ToInt16(AdlCnt.ToString()), Child = Convert.ToInt16(ChdCnt.ToString()) });
                criteria.RoomsInfo = rooms;
                criteria.SType = SearchType.OnlyExcursion;
                criteria.Type = ExcursionType.Tour;
                List<ExcursionSearchResult> pckDetail = new ExcursionSearch().getPackageDetail(UserData, criteria, Conversion.getInt32OrNull(Excursion), hasHotel, hasFlight, ref errorMsg);
                List<int> serviceIDList = new List<int>();
                List<addServiceReturnData> retValList = new List<addServiceReturnData>();
                List<ExcursionSearchResult> excurPack = new ExcursionSearch().searchExcursionPackage(UserData, ResData, criteria, hasHotel, hasFlight, ref errorMsg);
                ExcursionSearchResult pack = excurPack.Find(f => f.RefNo == Conversion.getInt32OrNull(Excursion));

                foreach (ExcursionSearchResult row in pckDetail)
                {
                    StartDay = Convert.ToInt16(((row.CheckIn.HasValue ? row.CheckIn.Value : Date.Value) - ResData.ResMain.BegDate.Value).Days);
                    Night = 0;
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, row.CheckIn.HasValue ? row.CheckIn.Value : Date.Value, row.CheckIn.HasValue ? row.CheckIn.Value : Date.Value, "EXCURSION", row.Code,
                                        string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, location, null, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, Conversion.getInt32OrNull(Excursion), ref errorMsg, null, string.Empty, null, null, null, VehicleCatID, VehicleUnit);
                    if (!string.IsNullOrEmpty(errorMsg))
                    {
                        retValList.Add(new addServiceReturnData
                        {
                            Calc = false,
                            Msg = errorMsg
                        });
                    }
                    else
                    {
                        if (_recordType == true)
                        {
                            if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                            {
                                List<ResServiceRecord> resServiceList = ResData.ResService;
                                ResServiceRecord resService = resServiceList.LastOrDefault();
                                resService.ExcludeService = false;
                                HttpContext.Current.Session["ResData"] = ResData;
                                retValList.Add(new addServiceReturnData
                                {
                                    Calc = true,
                                    Msg = errorMsg
                                });
                            }
                            else
                            {
                                //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                                retValList.Add(new addServiceReturnData
                                {
                                    Calc = false,
                                    Msg = errorMsg
                                });
                            }
                        }
                        else
                        {
                            List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);

                            if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                            {
                                HttpContext.Current.Session["ResData"] = ResData;
                                string Msg = string.Empty;
                                foreach (ReservastionSaveErrorRecord r in returnData)
                                {
                                    Msg += r.Message + "<br />";
                                }

                                retValList.Add(new addServiceReturnData
                                {
                                    Calc = true,
                                    Msg = Msg
                                });
                            }
                            else
                            {
                                string Msg = string.Empty;
                                foreach (ReservastionSaveErrorRecord r in returnData)
                                {
                                    if (!r.ControlOK)
                                        Msg += r.Message + "\n";
                                }

                                retValList.Add(new addServiceReturnData
                                {
                                    Calc = false,
                                    Msg = Msg
                                });
                            }
                        }
                    }
                }
                if (retValList.Where(w => w.Calc == false).Count() > 0)
                {
                    return retValList.Where(w => w.Calc == false).FirstOrDefault();
                }
                if (retValList.Where(w => w.Calc == true).Count() > 0)
                {
                    return retValList.Where(w => w.Calc == true).FirstOrDefault();
                }
                return new addServiceReturnData { Calc = false, Msg = "unknown error" };
            }
            else
            {
                List<addServiceReturnData> retValList = new List<addServiceReturnData>();
                List<int> serviceIDList = new List<int>();
                decimal? totalPrice = 0;
                List<string> excursionList = ExcList.Split(';').ToList();
                foreach (string row in excursionList)
                {
                    StartDay = Convert.ToInt16(((Date.HasValue ? Date.Value : Date.Value) - ResData.ResMain.BegDate.Value).Days);
                    Night = 0;
                    ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.HasValue ? Date.Value : Date.Value, Date.HasValue ? Date.Value : Date.Value, "EXCURSION", row,
                                                       string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, location, null, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, Conversion.getInt32OrNull(Excursion), ref errorMsg, null, string.Empty, null, null, null, VehicleCatID, VehicleUnit);
                    if (_recordType == true)
                    {
                        if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                        {
                            List<ResServiceRecord> resServiceList = ResData.ResService;
                            ResServiceRecord resService = resServiceList.LastOrDefault();
                            resService.ExcludeService = false;
                            HttpContext.Current.Session["ResData"] = ResData;
                            retValList.Add(new addServiceReturnData
                            {
                                Calc = true,
                                Msg = errorMsg
                            });
                        }
                        else
                        {
                            //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                            retValList.Add(new addServiceReturnData
                            {
                                Calc = false,
                                Msg = errorMsg
                            });
                        }
                    }
                    else
                    {
                        List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);

                        if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                        {

                            HttpContext.Current.Session["ResData"] = ResData;
                            string Msg = string.Empty;
                            foreach (ReservastionSaveErrorRecord r in returnData)
                            {
                                Msg += r.Message + "<br />";
                            }

                            retValList.Add(new addServiceReturnData
                            {
                                Calc = true,
                                Msg = Msg
                            });
                        }
                        else
                        {
                            string Msg = string.Empty;
                            foreach (ReservastionSaveErrorRecord r in returnData)
                            {
                                if (!r.ControlOK)
                                    Msg += r.Message + "\n";
                            }

                            retValList.Add(new addServiceReturnData
                            {
                                Calc = false,
                                Msg = Msg
                            });
                        }
                    }
                }
                if (retValList.Where(w => w.Calc == false).Count() > 0)
                {
                    return retValList.Where(w => w.Calc == false).FirstOrDefault();
                }
                if (retValList.Where(w => w.Calc == true).Count() > 0)
                {
                    return retValList.Where(w => w.Calc == true).FirstOrDefault();
                }
                return new addServiceReturnData { Calc = false, Msg = "unknown error" };
            }
        }
        else
        {
            ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, Date.Value, Date.Value, "EXCURSION", Excursion,
                                    string.Empty, string.Empty, string.Empty, string.Empty, Night, StartDay, Night, location, null, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, VehicleCatID, VehicleUnit);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                return new addServiceReturnData
                {
                    Calc = false,
                    Msg = errorMsg
                };
            }
            else
            {
                if (_recordType == true)
                {
                    if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                    {
                        List<ResServiceRecord> resServiceList = ResData.ResService;
                        ResServiceRecord resService = resServiceList.LastOrDefault();
                        resService.ExcludeService = false;
                        HttpContext.Current.Session["ResData"] = ResData;
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                        return new addServiceReturnData
                        {
                            Calc = true,
                            Msg = errorMsg
                        };
                    }
                    else
                    {
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                        return new addServiceReturnData
                        {
                            Calc = false,
                            Msg = errorMsg
                        };
                    }
                }
                else
                {
                    List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);

                    if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                    {
                        HttpContext.Current.Session["ResData"] = ResData;
                        string Msg = string.Empty;
                        foreach (ReservastionSaveErrorRecord row in returnData)
                        {
                            Msg += row.Message + "<br />";
                        }
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                        return new addServiceReturnData
                        {
                            Calc = true,
                            Msg = Msg
                        };
                    }
                    else
                    {
                        string Msg = string.Empty;
                        foreach (ReservastionSaveErrorRecord row in returnData)
                        {
                            if (!row.ControlOK)
                                Msg += row.Message + "\n";
                        }
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                        return new addServiceReturnData
                        {
                            Calc = false,
                            Msg = Msg
                        };
                    }
                }
            }
        }
    }
}
