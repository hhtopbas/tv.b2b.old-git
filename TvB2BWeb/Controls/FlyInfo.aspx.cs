﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using TvBo;
using System.Web.Services;
using System.Text;

public partial class Controls_FlyInfo : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; 
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.Params["FlightNo"]) && !string.IsNullOrEmpty(Request["FlyDate"]))
            {
                DateTime flyDate = new DateTime(Convert.ToInt64(Request["FlyDate"]));
                string flightNo = (string)Request.Params["FlightNo"];
                Response.Write(getFlightInfo(flightNo, flyDate));
            }
        }
    }
    
    protected string getFlightInfo(string FlightNo, DateTime? FlyDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        FlightDetailRecord flight = new Flights().getFlightDetail(UserData, UserData.Market, FlightNo, FlyDate.Value, ref errorMsg);
        StringBuilder sb = new StringBuilder();        
        sb.AppendFormat("<table class=\"flyInfoCss\"><tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFlightNo"),
            flight.FlightNo);
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblFlyDate"),
             flight.FlyDate.Value.ToShortDateString());
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblDepartureTime"),
            flight.DepTime.HasValue ? flight.DepTime.Value.ToString("HH:mm") : "&nbsp;");
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblDepartureAirport"),
            "(" + flight.DepAirport + ")-" + flight.DepAirportName);
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblArrivalTime"),
            flight.ArrTime.HasValue ? flight.ArrTime.Value.ToString("HH:mm") : "&nbsp;");
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblArrivalAirport"),
            "(" + flight.ArrAirport + ")-" + flight.ArrAirportName);
        sb.AppendFormat("<tr><td class=\"Caption\">{0}</td><td>{1}</td></tr></table>",
            HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAirline"),
            flight.AirlineName);                
        return sb.ToString();
    }
}
