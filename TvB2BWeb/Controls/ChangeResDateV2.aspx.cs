﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using TvTools;
using System.Text;
using System.Collections;

public partial class Controls_ChangeResDateV2 : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";
        /*
        ppcBegDate.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcBegDate.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        if (!IsPostBack)
            ppcBegDate.DateValue = ResData.ResMain.BegDate.Value;
        */
    }

    [WebMethod(EnableSession = true)]
    public static IEnumerable getFormData(string ResNo)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);                
        
        List<FlightDays> flightDays = new Flights().getFlightDays(UserData, ResData.ResMain.DepCity, ResData.ResMain.ArrCity, true, ResData.ResMain.PackType, null, string.Empty, ref errorMsg);

        if (ResData.ResService.Where(w => w.ServiceType == "FLIGHT").Count() > 0)
        {
            var query = from q in flightDays
                        select new { Year = q.FlyDate.Year, Month = q.FlyDate.Month, Day = q.FlyDate.Day, Text="" };
            return query.Count() > 0 ? query : null;
        }
        else
        {
            return null;
        }
    }    

    [WebMethod(EnableSession = true)]
    public static string changeDate(long? newBeginDate, bool? save)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";

        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        DateTime checkIn = new DateTime(1970, 1, 1);
        checkIn = checkIn.AddDays((Convert.ToInt64(newBeginDate) / 86400000) + 1);
        DateTime? begDate = checkIn;
        if (!begDate.HasValue) return string.Format(retVal, -10, HttpContext.GetGlobalResourceObject("Controls", "lblPleaseSelectReservationNewDate")).Replace('[', '{').Replace(']', '}');
        if (Equals(begDate, ResData.ResMain.BegDate)) return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        if (!new Reservation().changeReservationDate(UserData, ResData, begDate.Value, save.HasValue ? save.Value : false, ref errorMsg))
        {
            return string.Format(retVal, -1, errorMsg).Replace('[', '{').Replace(']', '}');
        }
        else
        {
            if (save.HasValue && save.Value)
            {
                new Reservation().reoFlightAllotResChangeData(ResData.ResMain.ResNo, ref errorMsg);
                return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
            }
            else
            {
                ResChgFeePrice resChgFee = new Reservation().getResChgFee(ResData, Convert.ToInt16(UserData.WebVersion) > 10 ? Convert.ToInt16(60) : Convert.ToInt16(30), true, ref errorMsg);
                if (resChgFee == null)
                    return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
                else
                {
                    decimal amounnt = resChgFee.ChgAmount.HasValue ? resChgFee.ChgAmount.Value : 0;
                    int pax = ResData.ResCust.Where(w => w.Title < 8 && w.Status == 0).Count();
                    string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                                    (amounnt * pax).ToString("#,###.0"), resChgFee.ChgCur);
                    return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
                }
            }
        }

    }
}
