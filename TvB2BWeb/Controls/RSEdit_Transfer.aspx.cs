﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Threading;
using System.Web.Services;
using System.Data;
using TvTools;

public partial class Controls_RSEdit_Transfer : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; 
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        if (!IsPostBack)
        {
        }
    }

    [WebMethod(EnableSession=true)]
    public static string getTransfers(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? recID = Conversion.getInt32OrNull(RecID);        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        string errorMsg = string.Empty;
        string retval = string.Empty;

        List<TransferRecord> transferList = new Transfers().getTransfers(UserData, ResData, resService.DepLocation, resService.ArrLocation, true, null, ref errorMsg);
        if (string.Equals(resService.IncPack, "Y"))
        {
            List<CatPackPlanSerRecord> catPackPlanSer = new Services().getCatPackPlanSer(ResData.ResMain.PriceListNo, ref errorMsg);
            var query = from q in transferList
                        join q2 in catPackPlanSer on q.Code equals q2.ServiceItem
                        where string.Equals(q2.Service, "TRANSFER")
                        group q by new { Code = q.Code, Name = q.LocalName } into k
                        select new { Code = k.Key.Code, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
        else
        {
            var query = from q in transferList
                        group q by new { Code = q.Code, Name = q.LocalName } into k
                        select new { Code = k.Key.Code, Name = k.Key.Name };
            return Newtonsoft.Json.JsonConvert.SerializeObject(query);
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getServiceDetail(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        Int32? recID = Conversion.getInt32OrNull(RecID);
        if (!recID.HasValue) return string.Empty;
        var query = from q in ResData.ResService
                    where q.RecID == recID.Value
                    select new
                    {
                        BegDate = q.BegDate.Value.ToShortDateString(),
                        EndDate = q.EndDate.Value.ToShortDateString(),
                        DepLocation = q.DepLocationNameL,
                        ArrLocation = q.ArrLocationNameL,
                        Service = q.Service,
                        PickupTimeHH = q.PickupTime.HasValue ? q.PickupTime.Value.Hour.ToString() : "",
                        PickupTimeSS = q.PickupTime.HasValue ? q.PickupTime.Value.Minute.ToString() : "",
                        PickupNote = q.PickupNote,
                        Adult = q.Adult,
                        Child = q.Child,
                        Unit = q.Unit,
                        Duration = q.Duration,
                        Supplier = q.SupplierName,
                        SupNote = q.SupNote,
                        StatConf = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ? q.StatConfNameL : string.Empty,
                        StatSer = q.StatSerNameL,
                        SalePrice = !Equals(q.IncPack, "Y") ? q.SalePrice.Value.ToString("#,###.00") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        SaleCur = !Equals(q.IncPack, "Y") ? q.SaleCur : "",
                        SupNoteShow = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Safiran)
                    };

        if (query == null || query.Count() < 1) return string.Empty;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query.FirstOrDefault());
    }

    [WebMethod(EnableSession = true)]
    public static string CalcService(string RecID, string Transfer, string PickupTimeHH, string PickupTimeSS, string PickupNote, string SupNote, bool? newRes)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        int? pickupTimeHH = Conversion.getInt32OrNull(PickupTimeHH != "undefined" ? PickupTimeHH : "");
        int? pickupTimeSS = Conversion.getInt32OrNull(PickupTimeSS != "undefined" ? PickupTimeSS : "");
        DateTime? pickupTime = null;
        if (pickupTimeHH.HasValue && pickupTimeSS.HasValue)
        {
            pickupTime = resService.BegDate.Value.AddHours(pickupTimeHH.Value).AddMinutes(pickupTimeSS.Value);
        }
        resService.PriceSource = !Equals(resService.Service, Transfer) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Transfer;
        resService.PickupTime = pickupTime;
        resService.Supplier = string.Empty;
        resService.SupNote = SupNote;
        resService.PickupNote = PickupNote;

        ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
        if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            resService = ResData.ResService.Find(f => f.RecID == recID.Value);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
            string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                        SalePriceStr,
                        supplierRec.NameL);
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string SaveService(string RecID, string Transfer, string PickupTimeHH, string PickupTimeSS, string PickupNote, string SupNote, bool? newRes)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        TvBo.TransferRecord transfer = new Transfers().getTransfer(UserData.Market, Transfer, ref errorMsg);
        int? pickupTimeHH = Conversion.getInt32OrNull(PickupTimeHH != "undefined" ? PickupTimeHH : "");
        int? pickupTimeSS = Conversion.getInt32OrNull(PickupTimeSS != "undefined" ? PickupTimeSS : "");
        DateTime? pickupTime = null;
        if (pickupTimeHH.HasValue && pickupTimeSS.HasValue)
        {
            pickupTime = resService.BegDate.Value.AddHours(pickupTimeHH.Value).AddMinutes(pickupTimeSS.Value);
        }
        resService.PriceSource = !Equals(resService.Service, Transfer) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.Service = Transfer;
        resService.ServiceName = transfer.Name;
        resService.ServiceNameL = transfer.LocalName;
        resService.PickupTime = pickupTime;
        resService.Supplier = string.Empty;
        resService.SupNote = SupNote;
        resService.PickupNote = PickupNote;

        if (newRes.HasValue && newRes.Value)
        {
            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
            if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
            {
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                            SalePriceStr,
                            supplierRec.NameL);
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        else
        {
            Int32 ErrCode = 0;
            ResData = new Reservation().extraServiceControl(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ref errorMsg);
            List<int?> recIDList = new List<int?>();
            recIDList.Add(resService.RecID);
            ResData = new TvBo.Reservation().editService(UserData, ResData, recIDList, ref ErrCode, ref errorMsg, false);
            if (ErrCode != 0)
            {
                if (ErrCode < 0)
                    errorMsg = string.IsNullOrEmpty(errorMsg) ? "Bilinmeyen hata" : errorMsg;
                else errorMsg = new ResCalcError().calcError(ErrCode, ResData).ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"",
                            SalePriceStr,
                            supplierRec.NameL);
                HttpContext.Current.Session["ResData"] = ResData;
            }
        }
        return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string getChgFee(string RecID, string Transfer, string PickupTimeHH, string PickupTimeSS, string PickupNote)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        resService.Service = Transfer;        
        ResChgFeePrice chgPrice = new Reservation().whatHasChangedResService(UserData, ResData, recID, resService, ref errorMsg);
        if (chgPrice == null)
            return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        else
        {
            string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                             chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
            return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
        }
    }

}

