﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Threading;
using TvTools;
using System.Collections;

public partial class Controls_CancelReservation : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlCancelReason.Attributes.Add("onchange", "changeReason();");
            //cbDrReport.Attributes.Add("onclick", "changeReason();");
        }
    }

    [WebMethod]
    public static object getCancelReason()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);

        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        List<CanReasonRecord> cancelReason = new Reservation().getCanReason(UserData.Market, ref errorMsg);
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
        {
            var _cancelReason = from q in cancelReason
                                where q.ValidForB2B && (Equals(ResData.ResMain.PaymentStat, "U") ? Equals(q.Penalized, "N") : Equals(q.Penalized, "Y"))
                                select new { RecID = q.RecID, NameL = useLocalName ? q.NameL : q.Name, Penalized = q.Penalized };
            if (_cancelReason == null || _cancelReason.Count() < 1)
                return null;
            return new
            {
                ShowDrReport = !string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt),
                CancelReason = _cancelReason
            };
        }
        else
        {
            var _cancelReason = from q in cancelReason
                                where q.ValidForB2B && (ResData.ResMain.OptDate.HasValue ? Equals(q.Penalized, "N") : Equals(q.Penalized, "Y"))
                                select new { RecID = q.RecID, NameL = useLocalName ? q.NameL : q.Name, Penalized = q.Penalized };
            if (_cancelReason == null || _cancelReason.Count() < 1)
                return null;
            return new
            {
                ShowDrReport = !string.Equals(UserData.CustomRegID, Common.crID_Novaturas_Lt),
                CancelReason = _cancelReason
            };
        }
    }

    [WebMethod]
    public static string getPrices()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string retVal = string.Empty;
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        ResMainRecord resMain = ResData.ResMain;
        decimal? salePrice = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ? resMain.PasPayable : resMain.AgencyPayable;
        decimal? payment = string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) ? resMain.PasPayment : resMain.AgencyPayment;
        retVal = "\"ResStat\":\"" + HttpContext.GetGlobalResourceObject("LibraryResource", "ResStatus" + resMain.ResStat.Value.ToString()).ToString() + "\"";
        retVal += "," + "\"SalePrice\":\"" + salePrice.Value.ToString("#,###.00") + " " + resMain.SaleCur + "\"";
        decimal? agencyToPay = (salePrice.HasValue ? salePrice.Value : 0) - (payment.HasValue ? payment.Value : 0);
        retVal += "," + "\"AmountToPay\":\"" + agencyToPay.Value.ToString("#,###.00") + " " + resMain.SaleCur + "\"";
        return "{" + retVal + "}";
    }

    [WebMethod]
    public static string getReason(string ReasonID, string DrReport)
    {
        string retVal = string.Empty;
        string errorMsg = string.Empty;
        User UserData = (User)HttpContext.Current.Session["UserData"];
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];
        Int32? recID = Conversion.getInt32OrNull(ReasonID);
        PenaltyPriceRecord penaltyPrice = new PenaltyPriceRecord();
        penaltyPrice.Price = 0;
        penaltyPrice.Cur = string.Empty;
        CanReasonRecord CanReason = new Reservation().getCanReasonRec(UserData.Market, recID.HasValue ? recID.Value : -1, ref errorMsg);
        if (Equals(CanReason.Penalized, "Y"))
            penaltyPrice.Price = new Reservation().getPenaltyPrice(UserData, ResData.ResMain.ResNo, CanReason.RecID, Equals(DrReport, "Y"), UserData.UserID, false, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(penaltyPrice);
    }

    [WebMethod]
    public static string setCancelReservation(string CancelCode, string DrReport)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        Int32 ErrCode = 0;
        int? cancelCode = Conversion.getInt32OrNull(CancelCode);
        if (!cancelCode.HasValue)
        {
            return "{\"ErrCode\":\"" + HttpContext.GetGlobalResourceObject("Controls", "lblPleaseSelectCancelReason").ToString() + "\"}";
        }
        bool drReport = Equals(DrReport, "Y");
        ResDataRecord ResData = (ResDataRecord)HttpContext.Current.Session["ResData"];

        #region reservation Check
        int? ALMode = null;
        int ALDay = 0;
        DateTime? CurDate;
        string userName = string.Empty;

        ResDataRecord OldResData = new ResTables().getReservationData(UserData, ResData.ResMain.ResNo, string.Empty, ref errorMsg);
        ResMainRecord resMain = OldResData.ResMain;
        TvParameters tvParams = new TvBo.Common().getTvParameters(UserData.Market, Conversion.getDecimalOrNull(UserData.TvVersion), UserData.WebVersion, ref errorMsg);
        ParamReser paramReser = tvParams.TvParamReser;
        if (resMain.ResLock.HasValue && resMain.ResLock.Value != 1)
        {
            if (paramReser.AutoLockChk)
            {
                ALMode = paramReser.AutoLockMode;
                ALDay = paramReser.AutoLockDay.HasValue ? paramReser.AutoLockDay.Value : Convert.ToInt16(0);
                if (Equals(paramReser.AutoLockDayAfBef, "B"))
                    ALDay = ALDay * Convert.ToInt16(-1);
            }
            if (ALMode.HasValue)
            {
                if (ALMode == 0)
                    CurDate = ResData.ResMain.BegDate.Value;
                else
                    if (ALMode == 1)
                        CurDate = ResData.ResMain.EndDate.Value;
                    else
                    {
                        if (ResData.ResMain.Complete == 0)
                            CurDate = DateTime.Today.AddDays(1);
                        else
                            if (ResData.ResMain.ConfStat == 0)
                                CurDate = DateTime.Today.AddDays(1);
                            else
                                if (ResData.ResMain.ConfStatDate.HasValue)
                                    CurDate = ResData.ResMain.ResDate.Value;
                                else CurDate = ResData.ResMain.ConfStatDate.HasValue ? ResData.ResMain.ConfStatDate.Value : DateTime.Today.AddDays(1);
                    }

                if (DateTime.Today >= CurDate.Value.AddDays(ALDay))
                    resMain.ResLock = 2;
            }
            if (resMain.ResLock > 0)
            {
                return "{\"ErrCode\":\"Reservation is locked. Can not be canceled.\"}";
            }
        }
        #endregion

        CanReasonRecord CanReason = new Reservation().getCanReasonRec(UserData.Market, cancelCode.HasValue ? cancelCode.Value : -1, ref errorMsg);
        bool cancelRes = false;
        if (Equals(CanReason.Penalized, "Y") && (!string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) || (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Detur) && ResData.ResMain.AgencyPayment != 0))) //ResMain.AgencyPayment <> 0 ödeme var
        {
            decimal? retVal = new Reservation().getPenaltyPrice(UserData, ResData.ResMain.ResNo, CanReason.RecID, Equals(DrReport, "Y"), UserData.UserID, true, ref errorMsg);
            if (retVal.HasValue)
            {
                cancelRes = true;
                ResData.ResMain.ResStat = 3;
                new Reservation().reCalcResDataReal(UserData, ref ResData, ref errorMsg);
                string retV = string.Empty;
                if (UserData.TvParams.TvParamReser.SendNotMailResCancelB2B.HasValue && UserData.TvParams.TvParamReser.SendNotMailResCancelB2B.Value)
                    retV = getEmailUrl(UserData, ResData);
                HttpContext.Current.Session["ResData"] = ResData;
            }
            else ErrCode = -1;
        }
        else
        {
            cancelRes = new Reservation().cancelReservationWithoutPenatly(ResData.ResMain.ResNo, cancelCode.Value, UserData.UserID, ref ErrCode, ref errorMsg);
            new Reservation().reCalcResDataReal(UserData, ref ResData, ref errorMsg);
            string retV = string.Empty;
            if (UserData.TvParams.TvParamReser.SendNotMailResCancelB2B.HasValue && UserData.TvParams.TvParamReser.SendNotMailResCancelB2B.Value)
                retV = getEmailUrl(UserData, ResData);
        }
        return "{\"ErrCode\":" + ErrCode.ToString() + "}";
    }

    public static string getEmailUrl(User UserData, ResDataRecord ResData)
    {
        try
        {
            if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
            {
                string url = Conversion.getStrOrNull(new TvBo.Common().getFormConfigValue("General", "b2cEmailUrl"));
                url = url.Trim().Trim('\n').Trim('\r');

                ResCustRecord leader = ResData.ResCust.Find(f => f.Leader == "Y");
                ResCustInfoRecord leaderInfo = leader != null ? ResData.ResCustInfo.Find(f => f.CustNo == leader.CustNo) : null;
                if (!string.IsNullOrEmpty(url) && leaderInfo != null)
                {
                    if ((Equals(leaderInfo.ContactAddr, "H") ? (string.IsNullOrEmpty(leaderInfo.AddrHomeEmail) ? true : false) : (string.IsNullOrEmpty(leaderInfo.AddrWorkEMail) ? true : false)) == false)
                    {
                        if (Equals(UserData.CustomRegID, TvBo.Common.crID_Detur))
                        {
                            fi.detur.SendMail sm = new fi.detur.SendMail();
                            sm.Url = url;
                            sm.SendCancelReservationMail(ResData.ResMain.ResNo);
                        }
                        return string.Format("'serviceUrl':'{0}','ResNo':'{1}','CultureID':'{2}','AgencyID':'{3}'",
                                                url,
                                                ResData.ResMain.ResNo,
                                                UserData.Ci.Name,
                                                ResData.ResMain.Agency);
                    }
                    else return string.Empty;

                }
                else return string.Empty;
            }
            else return string.Empty;
        }
        catch
        {
            return string.Empty;
        }
    }
}
