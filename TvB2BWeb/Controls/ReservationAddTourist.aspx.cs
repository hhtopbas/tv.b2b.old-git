﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TvBo;
using System.Web.Services;
using System.Text;
//using AjaxControlToolkit;
using System.Globalization;
using System.Threading;
using TvTools;

public partial class ReservationAddTourist : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
    }

    [WebMethod(EnableSession = true)]
    public static string getHotelRooms()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        if (ResData.ResMain.ConfStat == 2 || ResData.ResMain.ConfStat == 3)
        {
            return string.Format("<[\"Continue\":\"{0}\",\"data\":\"{1}\",\"dateMask\":\"{2}\"]>",
                        "1",
                        HttpContext.GetGlobalResourceObject("Controls", "lblReservationIsCancelNoAddingTourist"),// "Reservation is cancel.No adding tourist.",
                        "");
        }
        string errorMsg = string.Empty;
        string dateMask = string.Empty;
        string datePatern = new TvBo.Common().getDateFormat(UserData.Ci);// UserData.Ci.DateTimeFormat.ShortDatePattern;
        string[] datePaternDim = datePatern.Split('/');
        foreach (string row in datePaternDim)
        {
            switch (row.Substring(0, 1).ToLower())
            {
                case "m": if (dateMask.Length > 0) dateMask += "/"; dateMask += "99"; break;
                case "M": if (dateMask.Length > 0) dateMask += "/"; dateMask += "99"; break;
                case "d": if (dateMask.Length > 0) dateMask += "/"; dateMask += "99"; break;
                case "y": if (dateMask.Length > 0) dateMask += "/"; dateMask += "9999"; break;
            }
        }

        StringBuilder sb = new StringBuilder();
        List<TvBo.ResServiceRecord> hotelService = ResData.ResService.Where(w => w.ServiceType == "HOTEL").Select(s => s).ToList<TvBo.ResServiceRecord>();
        if (hotelService == null || hotelService.Count < 1)
            return string.Format("<[\"Continue\":\"{0}\",\"data\":\"{1}\",\"dateMask\":\"{2}\"]>",
                            "-1",
                            HttpContext.GetGlobalResourceObject("Controls", "lblHotelServiceNotFound"),
                            dateMask);
        Int16 maxPaxCount = UserData.TvParams.TvParamReser.MaxPaxCnt.HasValue ? UserData.TvParams.TvParamReser.MaxPaxCnt.Value : Convert.ToInt16(9);
        Int16 addPaxCount = Convert.ToInt16(maxPaxCount - ResData.ResCust.Where(w => w.Status == 0).Count());
        int paxCount = addPaxCount;
        if (paxCount <= 0) return string.Format("<[\"Continue\":\"{0}\",\"data\":\"{1}\",\"dateMask\":\"{2}\"]>",
                            "2",
                            HttpContext.GetGlobalResourceObject("Controls", "lblTheNumberOfPassengersExceeded"),
                            dateMask);

        sb.Append("<table class=\"selectRoomTable\">");
        sb.AppendFormat("<tr class=\"header\"><td>{0}</td>", HttpContext.GetGlobalResourceObject("Controls", "lblRoom"));
        sb.AppendFormat("<td class=\"addCellHeader\">{0}</td></tr>", HttpContext.GetGlobalResourceObject("Controls", "lblAddTourist"));

        foreach (TvBo.ResServiceRecord row in hotelService)
        {
            sb.Append(" <tr class=\"dataRow\">");
            sb.AppendFormat("<td class=\"hotelCell\">{0}<br />", row.ServiceNameL + ", " + row.RoomNameL + ", " + row.BoardNameL + ", " + row.AccomNameL);
            List<TvBo.ResCustRecord> resCust = (from q1 in ResData.ResCust
                                                join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                                                where q2.ServiceID == row.RecID
                                                select q1).ToList<TvBo.ResCustRecord>();
            sb.Append("   <table class=\"roomPersonTable\">");
            foreach (TvBo.ResCustRecord r in resCust)
            {
                sb.AppendFormat("<tr><td>{0}</td></tr>", r.TitleStr + ", " + r.Surname + " " + r.Name);
            }
            sb.Append("   </table>");
            sb.Append("  </td>");
            sb.Append(" <td class=\"addCell\">");
            sb.AppendFormat(" <img title=\"\" src=\"../Images/default/plus.png\" onclick=\"RoomAddPax({0},'{1}')\" />",
                                row.RecID,
                                dateMask);
            sb.Append(" </td></tr>");
        }
        sb.Append("<tr><td colspan=\"2\">");
        sb.Append(HttpContext.GetGlobalResourceObject("Controls", "lblOr").ToString());
        sb.Append("</td></tr>");
        sb.Append("<tr><td>");
        sb.AppendFormat("<input type=\"button\" value='{0}' onclick=\"addRoomAndPax();\" style=\"width: 100%;\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only\" />",
            HttpContext.GetGlobalResourceObject("Controls", "addNewRoomAndTourist"));
        sb.Append("</td>");
        sb.Append("<td>");
        sb.Append("<select id=\"addPaxCount\">");
        for (int i = 0; i < addPaxCount; i++)
        {
            sb.AppendFormat("<option value='{0}'>{1} {2}.</option>", i + 1, i + 1, HttpContext.GetGlobalResourceObject("Controls", "lblPax"));
        }
        sb.Append("</select>");
        sb.Append("</td>");
        sb.Append("</tr></table>");
        string retVal = sb.ToString().Replace('"', '~');
        return string.Format("<[\"Continue\":\"{0}\",\"data\":\"{1}\",\"dateMask\":\"{2}\"]>",
                        "0",
                        retVal,
                        dateMask);
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist(string ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        int? serviceID = Conversion.getInt32OrNull(ServiceID);
        StringBuilder sb = new StringBuilder();
        sb.Append("<table style=\"width: 500px; font-family: Arial; font-size: 9pt; border: solid 1px #000000;\" cellpadding=\"1\" cellspacing=\"0\">");
        sb.AppendFormat("<tr><td align=\"right\" style=\"width: 110px;\"><b>{0} : </b></td>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resCustTitle"));
        string titleStr = string.Empty;
        foreach (var row in ResData.Title.Where(w => w.Enable == true).Select(s => s).ToList<TvBo.TitleRecord>())
        {
            titleStr += string.Format("<option value=\"{0}\">{1}</option>", row.TitleNo, row.Code);
        }
        sb.AppendFormat("<td><select id=\"cbTitle\">{0}</select></td></tr>", titleStr);
        sb.AppendFormat("<tr><td align=\"right\"><b>{0} : </b></td><td><input type=\"text\" id=\"txtSurname\" maxlength=\"30\" style=\"width:350px; {1}\" onkeypress=\"return isNonUniCodeChar(event);\" /></td></tr>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resCustSurname"),
            new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules));
        sb.AppendFormat("<tr><td align=\"right\"><b>{0} : </b></td><td><input type=\"text\" id=\"txtName\" maxlength=\"30\" style=\"width:300px; {1} \" onkeypress=\"return isNonUniCodeChar(event);\" /></td></tr>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resCustName"),
            new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules));
        sb.AppendFormat("<tr><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtBirthDate\" style=\"width:100px;\" />&nbsp;{1}</td></tr>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resCustBirthDate"),
            "(" + new TvBo.Common().getDateFormatRegion(UserData.Ci) + ")");
        bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
        bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));
        //if (showPIN == null || (showPIN.HasValue && showPIN.Value))
        sb.AppendFormat("<tr {1}><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtIDNo\" maxlength=\"20\" style=\"width:200px;\" /></td></tr>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPIN"),
            showPIN == null || (showPIN.HasValue && showPIN.Value) ? "" : "style=\"display: none;\"");
        //if (showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value))
        //{
        sb.AppendFormat("<tr {1}><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtPassSerie\" maxlength=\"5\" style=\"width:50px;\" /></td></tr>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassSerie"),
            showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value) ? "" : "style=\"display: none;\"");
        sb.AppendFormat("<tr {1}><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtPassNo\" maxlength=\"10\" style=\"width:150px;\" /></td></tr>",
            HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassNo"),
            showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value) ? "" : "style=\"display: none;\"");
        //}
        sb.Append("</table>");
        sb.Append("<div style=\"text-align: center;\">");
        sb.AppendFormat("<input id=\"btnNext\" type=\"button\" value=\">> {0} >>\" onclick=\"AddPax({1});\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
            HttpContext.GetGlobalResourceObject("Controls", "lblNext"),
            serviceID.ToString());
        sb.Append("</div>");
        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    public static AddPaxReturnRecJson AddPax(Int16? Title, string Surname, string Name, String BirthDate, string IDNo, string PassSerie, string PassNo, int? ServiceID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        
        TvBo.ResDataRecord oldResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        TvBo.ResDataRecord ResData = new ResTables().copyData(oldResData);

        int? serviceID = Conversion.getInt32OrNull(ServiceID);
        DateTime? birthDate = null;
        if (!string.IsNullOrEmpty(BirthDate))
            birthDate = Convert.ToDateTime(BirthDate);
        if (birthDate.HasValue)
        {
            double Age = Math.Floor((ResData.ResMain.BegDate.Value - birthDate.Value).TotalDays / 365.25);
            if (Age < 18)
            { if (Age >= 2) Title = 6; else Title = 8; }
            else { Title = 1; }
        }
        string errorMsg = string.Empty;
        int CustCount = ResData.ResCust.Count;
        Int16 seqNo = 0;
        ResData = new Reservation().AddTourist(UserData, ResData, Conversion.getInt16OrNull(Title), Surname, Name, birthDate, Equals(IDNo.ToLower(), "undefined") ? "" : IDNo, PassSerie, PassNo, serviceID, null, ref seqNo, ref errorMsg);
        if (!string.IsNullOrEmpty(errorMsg))
            return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = errorMsg, custNo = null }; // Customer not adding
        if (ResData.ResCust.Count != CustCount + 1)
            return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = HttpContext.GetGlobalResourceObject("Controls", "lblCustomerNotAdding").ToString(), custNo = null }; // Customer not adding
        List<Integer> query = (from q1 in ResData.ResCust
                               join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                               join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                               orderby q1.SeqNo
                               where q2.ServiceID == serviceID.Value && (q3.ServiceType == "FLIGHT" || q3.ServiceType == "HOTEL")
                               select new Integer { Value = q2.CustNo }).ToList<Integer>();

        if (query.Count() > 0)
        {
            int custNo = ResData.ResCust.Where(w => w.SeqNo == seqNo).Last().CustNo;
            List<Integer> addingService = (from q in ResData.ResCon
                                           where q.CustNo == query.FirstOrDefault().Value
                                           select new Integer { Value = q.ServiceID }).ToList<Integer>();
            List<TvBo.ResServiceRecord> serviceList = (from q1 in ResData.ResService
                                                       join q2 in addingService on q1.RecID equals q2.Value
                                                       where string.Equals(q1.IncPack, "Y") || (q1.Compulsory.HasValue && q1.Compulsory.Value == true)
                                                       orderby q1.BegDate, q1.SeqNo
                                                       select q1).ToList<TvBo.ResServiceRecord>();
            if (serviceList == null || serviceList.Count < 1)
                return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = HttpContext.GetGlobalResourceObject("LibraryResource", "NoResServiceMsg").ToString(), custNo = null };                     
            foreach (TvBo.ResServiceRecord row in serviceList)
            {
                Int16 retVal = new Reservation().AddTuristPrepareService(UserData, ref ResData, oldResData, custNo, row.RecID, PrepareServiceType.AddCustomer, ref errorMsg);

                int newTotAdultCount = ResData.ResCust.Where(w => w.Title < 6 && w.Status == 0).Count();
                int newTotChildCount = ResData.ResCust.Where(w => w.Title > 5 && w.Status == 0).Count();

                ResData.ResMain.Adult = Convert.ToInt16(newTotAdultCount);
                ResData.ResMain.Child = Convert.ToInt16(newTotChildCount);

                int newAdultCount = ResData.ResService.Find(f => f.RecID == row.RecID).Adult.Value;
                int newChildCount = ResData.ResService.Find(f => f.RecID == row.RecID).Child.Value;
                if (retVal != 0)
                {
                    if (retVal > 0)
                    {
                        switch (retVal)
                        {
                            case 11: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomNotFound").ToString(); break;
                            case 12: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString(); break;
                            case 13: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString(); break;
                            case 14: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString(); break;
                            case 15: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString(); break;
                        }
                        return new AddPaxReturnRecJson { reCalc = true, Pax = newAdultCount, Child = newChildCount, Msg = errorMsg, custNo = null };                           
                    }
                    else
                        return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = errorMsg, custNo = null }; //Error Inserting Tourist
                }
            }

            bool CalcOK = new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg);
            if (CalcOK)
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().UpdateReservation(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    List<int?> custs = new List<int?>();
                    custs.Add(custNo);
                    return new AddPaxReturnRecJson { reCalc = null, Pax = 0, Msg = string.Empty, custNo = custs };
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + " ";
                    }
                    return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = Msg, custNo = null };                    
                }                
            }
            else
                return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = errorMsg, custNo = null };                
        }
        else
            return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = HttpContext.GetGlobalResourceObject("LibraryResource", "NoResServiceMsg").ToString(), custNo = null };            
    }

    [WebMethod(EnableSession = true)]
    public static string getTourists(string Adult, string Child, string Hotel, string Room, string Accom, string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        int adult = Conversion.getInt32OrNull(Adult).HasValue ? Conversion.getInt32OrNull(Adult).Value : 1;
        int child = Conversion.getInt32OrNull(Child).HasValue ? Conversion.getInt32OrNull(Child).Value : 0;
        StringBuilder sb = new StringBuilder();
        int cnt = 0;
        for (int i = 0; i < adult; i++)
        {
            ++cnt;
            sb.Append("<table class=\"selectRoomTable\">");
            sb.AppendFormat("<tr><td align=\"right\" style=\"width: 110px;\"><b>{0} : </b></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustTitle"));
            string titleStr = string.Empty;
            foreach (var row in ResData.Title.Where(w => w.Enable == true && w.TitleNo < 6).Select(s => s).ToList<TvBo.TitleRecord>())
            {
                titleStr += string.Format("<option value=\"{0}\">{1}</option>", row.TitleNo, row.Code);
            }
            sb.AppendFormat("<td><select id=\"cbTitle{1}\">{0}</select></td></tr>", titleStr, cnt);
            sb.AppendFormat("<tr><td align=\"right\"><b>{0} : </b></td><td><input type=\"text\" id=\"txtSurname{1}\" maxlength=\"30\" style=\"width:350px; {2} \" onkeypress=\"return isNonUniCodeChar(event);\" /></td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustSurname"),
                cnt,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.surnameWrittingRules));
            sb.AppendFormat("<tr><td align=\"right\"><b>{0} : </b></td><td><input type=\"text\" id=\"txtName{1}\" maxlength=\"30\" style=\"width:300px; {2} \" onkeypress=\"return isNonUniCodeChar(event);\" /></td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustName"),
                cnt,
                new UICommon().nameWrittingRule(UserData, NameWrittingRuleTypes.nameWrittingRules));
            sb.AppendFormat("<tr><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtBirthDate{2}\" style=\"width:100px;\" />&nbsp;{1}</td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustBirthDate"),
                "(" + new TvBo.Common().getDateFormatRegion(UserData.Ci) + ")", cnt);
            bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
            bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));
            //if (showPIN == null || (showPIN.HasValue && showPIN.Value))
            sb.AppendFormat("<tr {2}><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtIDNo{1}\" maxlength=\"20\" style=\"width:200px;\" /></td></tr><tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPIN"),
                cnt,
                showPIN == null || (showPIN.HasValue && showPIN.Value) ? "" : "style=\"display: none;\"");
            //if (showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value))
            //{
            sb.AppendFormat("<tr {2}><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtPassSerie{1}\" maxlength=\"5\" style=\"width:50px;\" /></td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassSerie"),
                cnt,
                showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value) ? "" : "style=\"display: none;\"");
            sb.AppendFormat("<tr {2}><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtPassNo{1}\" maxlength=\"10\" style=\"width:150px;\" /></td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassNo"),
                cnt,
                showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value) ? "" : "style=\"display: none;\"");
            //}
            sb.Append("</table>");
        }
        for (int i = 0; i < child; i++)
        {
            ++cnt;
            sb.Append("<table class=\"selectRoomTable\">");
            sb.AppendFormat("<tr><td align=\"right\" style=\"width: 110px;\"><b>{0} : </b></td>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustTitle"));
            string titleStr = string.Empty;
            foreach (var row in ResData.Title.Where(w => w.Enable == true && w.TitleNo > 5).Select(s => s).ToList<TvBo.TitleRecord>())
            {
                titleStr += string.Format("<option value=\"{0}\">{1}</option>", row.TitleNo, row.Code);
            }
            sb.AppendFormat("<td><select id=\"cbTitle{1}\">{0}</select></td></tr>", titleStr, cnt);
            sb.AppendFormat("<tr><td align=\"right\"><b>{0} : </b></td><td><input type=\"text\" id=\"txtSurname{1}\" maxlength=\"30\" style=\"width:350px;\" /></td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustSurname"), cnt);
            sb.AppendFormat("<tr><td align=\"right\"><b>{0} : </b></td><td><input type=\"text\" id=\"txtName{1}\" maxlength=\"30\" style=\"width:300px;\" /></td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustName"), cnt);
            sb.AppendFormat("<tr><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtBirthDate{2}\" style=\"width:100px;\" />&nbsp;{1}</td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustBirthDate"),
                "(" + strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ') + ")", cnt);


            bool? showPIN = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("MakeRes", "showPIN"));
            bool? showPassportInfo = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("General", "ShowPassportInfo"));
            //if (showPIN == null || (showPIN.HasValue && showPIN.Value))
            sb.AppendFormat("<tr {2}><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtIDNo{1}\" maxlength=\"20\" style=\"width:200px;\" /></td></tr><tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPIN"),
                cnt,
                showPIN == null || (showPIN.HasValue && showPIN.Value) ? "" : "style=\"display: none;\"");
            //if (showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value))
            //{
            sb.AppendFormat("<tr {2}><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtPassSerie{1}\" maxlength=\"5\" style=\"width:50px;\" /></td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassSerie"),
                cnt,
                showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value) ? "" : "style=\"display: none;\"");
            sb.AppendFormat("<tr {2}><td align=\"right\"><b>{0} : </b></td><td><input id=\"txtPassNo{1}\" maxlength=\"10\" style=\"width:150px;\" /></td></tr>",
                HttpContext.GetGlobalResourceObject("MakeReservation", "resCustPassNo"),
                cnt,
                showPassportInfo == null || (showPassportInfo.HasValue && showPassportInfo.Value) ? "" : "style=\"display: none;\"");

            sb.Append("</table>");
        }

        sb.Append("<div style=\"text-align: center;\">");
        sb.AppendFormat("<input id=\"btnNext\" type=\"button\" value=\">> {0} >>\" onclick=\"AddPaxRoom('{1}','{2}','{3}','{4}','{5}');\" class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover\" />",
            HttpContext.GetGlobalResourceObject("Controls", "lblNext"),
            cnt,
            Hotel,
            Room,
            Accom,
            RecID);
        sb.Append("</div>");

        return sb.ToString();
    }

    [WebMethod(EnableSession = true)]
    //public static string AddPaxs(string data)
    public static AddPaxReturnRecJson AddPaxs(string Hotel, string Room, string Accom, int? RecID, List<custRecJson> paxs)
    {
        //string Title, string Surname, string Name, String BirthDate, string IDNo, string PassSerie, string PassNo;
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        string errorMsg = string.Empty;
        //string[] datas = { "", "" };// data.Split('@');
        //string data1 = "[" + datas[0].Replace('<', '{').Replace('>', '}').Replace('!', '"') + "]";
        //string data2 = datas[1].Replace('<', '{').Replace('>', '}').Replace('!', '"');
        List<custRecJson> addCustList = paxs;
        AddRoomRecJson list = new AddRoomRecJson
        {
            RecID = RecID.ToString(),
            Hotel = Hotel,
            Room = Room,
            Accom = Accom
        };
        // Newtonsoft.Json.JsonConvert.DeserializeObject<AddRoomRecJson>(data2);
        ResServiceRecord hotelService = ResData.ResService.Find(f => f.RecID == RecID);
        int CustCount = ResData.ResCust.Count;
        List<int?> custs = new List<int?>();                    
        foreach (custRecJson row in addCustList)
        {
            DateTime? birthDate = null;
            if (!string.IsNullOrEmpty(row.BirthDate))
                birthDate = Convert.ToDateTime(row.BirthDate);
            if (birthDate.HasValue)
            {
                double Age = Math.Floor((ResData.ResMain.BegDate.Value - birthDate.Value).TotalDays / 365.25);
                if (Age < 18)
                { if (Age >= 2) row.Title = "6"; else row.Title = "8"; }
                else { row.Title = "1"; }
            }
            Int16 seqNo = 0;
            ResData = new Reservation().AddTourist(UserData, ResData, Conversion.getInt16OrNull(row.Title), row.Surname, row.Name, birthDate, row.IDNo != null ? (Equals(row.IDNo.ToLower(), "undefined") ? "" : row.IDNo) : "", row.PassSerie, row.PassNo, -1, null, ref seqNo, ref errorMsg);

            if (!string.IsNullOrEmpty(errorMsg))
                return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = errorMsg, custNo = null };
                //return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + errorMsg + "\"}";
            custs.Add(ResData.ResCust.Where(w => w.SeqNo == seqNo).LastOrDefault().CustNo);
        }

        if (ResData.ResCust.Count != CustCount + addCustList.Count())
            return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = HttpContext.GetGlobalResourceObject("Controls", "lblCustomerNotAdding").ToString(), custNo = null };
            //return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + HttpContext.GetGlobalResourceObject("Controls", "lblCustomerNotAdding").ToString() + "\"}";

        List<ResServiceRecord> resService = ResData.ResService;
        ResServiceRecord hotel = resService.Find(f => f.Service == list.Hotel);
        DateTime? begDate = ResData.ResMain.BegDate;
        DateTime? endDate = ResData.ResMain.EndDate;

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (ResCustRecord cust in ResData.ResCust.Where(w => w.BookID == 99))
            SelectCust.Find(f => f.CustNo == cust.CustNo).Selected = true;
        Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
        TvBo.HotelRecord HotelRec = new Hotels().getHotelDetail(UserData, list.Hotel, ref errorMsg);

        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 99, begDate.Value, endDate.Value, "HOTEL", list.Hotel,
            list.Room, list.Accom, hotel.Board, string.Empty, Night, StartDay, Night, HotelRec.TrfLocation, null, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, hotelService != null ? Equals(hotelService.IncPack, "Y") : false, hotel.StepNo, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);

        if (!string.IsNullOrEmpty(errorMsg))
            return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = errorMsg, custNo = null };
            //return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + errorMsg + "\"}";

        SearchType sType = new SearchType();
        if (ResData.ResMain.PackType == "H") sType = SearchType.PackageSearch;
        else if (ResData.ResMain.PackType == "O") sType = SearchType.OnlyHotelSearch;
        else if (ResData.ResMain.SaleResource == 2 || ResData.ResMain.SaleResource == 3 || ResData.ResMain.SaleResource == 5) sType = SearchType.OnlyFlightSearch;

        int? CatPRecNo = ResData.ResService.Where(w => w.Service == HotelRec.Code).Select(s => s.CatPRecNo).FirstOrDefault();

        ResData = new Reservation().GeneratePriceListServiceAndExtraServicesWithoutHotel(UserData, ResData, 99, sType, ResData.ResMain.PriceListNo, CatPRecNo, hotelService != null ? Equals(hotelService.IncPack, "Y") : false, ref errorMsg);

        int newTotAdultCount = ResData.ResCust.Where(w => w.Title < 6 && w.Status == 0).Count();
        int newTotChildCount = ResData.ResCust.Where(w => w.Title > 5 && w.Status == 0).Count();

        ResData.ResMain.Adult = Convert.ToInt16(newTotAdultCount);
        ResData.ResMain.Child = Convert.ToInt16(newTotChildCount);

        string retVal = string.Empty;
        if (!string.IsNullOrEmpty(errorMsg))
        {
            return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = errorMsg, custNo = null };
            //return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + errorMsg + "\"}";
        }
        else
        {
            bool CalcOK = new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg);
            if (CalcOK)
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().UpdateReservation(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;                    
                    return new AddPaxReturnRecJson { reCalc = null, Pax = 0, Msg = string.Empty, custNo = custs };
                    //return "{\"reCalc\":\"\",\"Pax\":\"0\",\"Msg\":\"\"}";
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                        {
                            if (Msg.Length > 0) Msg += "<br />";
                            Msg += row.Message;
                        }
                    }
                    return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = Msg, custNo = null };
                    //return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + Msg + "\"}";
                }
                //HttpContext.Current.Session["ResData"] = ResData;
                //return "{\"reCalc\":\"\",\"Pax\":\"0\",\"Msg\":\"\"}"; ;
            }
            else
                return new AddPaxReturnRecJson { reCalc = false, Pax = 0, Msg = errorMsg, custNo = null };
                //return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + errorMsg + "\"}";
        }

        #region Changed
        /*

        List<Integer> query = (from q1 in ResData.ResCust
                               join q2 in ResData.ResCon on q1.CustNo equals q2.CustNo
                               join q3 in ResData.ResService on q2.ServiceID equals q3.RecID
                               orderby q1.SeqNo
                               where q2.ServiceID == serviceID.Value && (q3.ServiceType == "FLIGHT" || q3.ServiceType == "HOTEL")
                               select new Integer { Value = q2.CustNo }).ToList<Integer>();

        if (query.Count() > 0)
        {
            int custNo = ResData.ResCust.OrderBy(o => o.SeqNo).Last().CustNo;
            List<Integer> addingService = (from q in ResData.ResCon
                                           where q.CustNo == query.FirstOrDefault().Value
                                           select new Integer { Value = q.ServiceID }).ToList<Integer>();
            List<TvBo.ResServiceRecord> serviceList = (from q1 in ResData.ResService
                                                       join q2 in addingService on q1.RecID equals q2.Value
                                                       orderby q1.BegDate, q1.SeqNo
                                                       select q1).ToList<TvBo.ResServiceRecord>();
            if (serviceList == null || serviceList.Count < 1)
                return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + HttpContext.GetGlobalResourceObject("LibraryResource", "NoResServiceMsg") + "\"}";
            foreach (TvBo.ResServiceRecord row in serviceList)
            {
                Int16 retVal = new Reservation().AddTuristPrepareService(UserData, ref ResData, custNo, row.RecID, PrepareServiceType.AddCustomer, ref errorMsg);

                int newTotAdultCount = ResData.ResCust.Where(w => w.Title < 6).Count();
                int newTotChildCount = ResData.ResCust.Where(w => w.Title > 5).Count();

                ResData.ResMain.Adult = Convert.ToInt16(newTotAdultCount);
                ResData.ResMain.Child = Convert.ToInt16(newTotChildCount);

                int newAdultCount = ResData.ResService.Find(f => f.RecID == row.RecID).Adult.Value;
                int newChildCount = ResData.ResService.Find(f => f.RecID == row.RecID).Child.Value;
                if (retVal != 0)
                {
                    if (retVal > 0)
                    {
                        switch (retVal)
                        {
                            case 11: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomNotFound").ToString(); break;
                            case 12: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString(); break;
                            case 13: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString(); break;
                            case 14: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString(); break;
                            case 15: errorMsg = HttpContext.GetGlobalResourceObject("LibraryResource", "HotelAccomPaxError").ToString(); break;
                        }
                        return "{\"reCalc\":\"1\",\"Pax\":\"" + newAdultCount.ToString() + "|" + newChildCount.ToString() + "\",\"Msg\":\"" + errorMsg + "\"}";
                    }
                    else
                        return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + errorMsg + "\"}"; //Error Inserting Tourist
                }
            }

            bool CalcOK = new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg);
            if (CalcOK)
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().UpdateReservation(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    return "{\"reCalc\":\"\",\"Pax\":\"0\",\"Msg\":\"\"}";
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "\n";
                    }
                    return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + Msg + "\"}";
                }
                //HttpContext.Current.Session["ResData"] = ResData;
                //return "{\"reCalc\":\"\",\"Pax\":\"0\",\"Msg\":\"\"}"; ;
            }
            else
                return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + errorMsg + "\"}";
        }
        else
            return "{\"reCalc\":\"0\",\"Pax\":\"0\",\"Msg\":\"" + HttpContext.GetGlobalResourceObject("LibraryResource", "NoResServiceMsg") + "\"}";
       */
        #endregion

    }
}

public class custRecJson
{
    public custRecJson()
    {
    }

    string _Title;
    public string Title
    {
        get { return _Title; }
        set { _Title = value; }
    }

    string _Surname;
    public string Surname
    {
        get { return _Surname; }
        set { _Surname = value; }
    }

    string _Name;
    public string Name
    {
        get { return _Name; }
        set { _Name = value; }
    }

    string _BirthDate;
    public string BirthDate
    {
        get { return _BirthDate; }
        set { _BirthDate = value; }
    }

    string _IDNo;
    public string IDNo
    {
        get { return _IDNo; }
        set { _IDNo = value; }
    }

    string _PassSerie;
    public string PassSerie
    {
        get { return _PassSerie; }
        set { _PassSerie = value; }
    }

    string _PassNo;
    public string PassNo
    {
        get { return _PassNo; }
        set { _PassNo = value; }
    }
}

public class AddRoomRecJson
{
    public AddRoomRecJson()
    {
    }

    string _Hotel;
    public string Hotel
    {
        get { return _Hotel; }
        set { _Hotel = value; }
    }

    string _Room;
    public string Room
    {
        get { return _Room; }
        set { _Room = value; }
    }

    string _Accom;
    public string Accom
    {
        get { return _Accom; }
        set { _Accom = value; }
    }

    string _RecID;
    public string RecID
    {
        get { return _RecID; }
        set { _RecID = value; }
    }
}

public class AddPaxReturnRecJson
{
    public AddPaxReturnRecJson()
    {
        _reCalc = false;
        _custNo = new List<int?>();
    }

    bool? _reCalc;
    public bool? reCalc
    {
        get { return _reCalc; }
        set { _reCalc = value; }
    }

    Int32? _Pax;
    public Int32? Pax
    {
        get { return _Pax; }
        set { _Pax = value; }
    }

    Int32? _Child;
    public Int32? Child
    {
        get { return _Child; }
        set { _Child = value; }
    }

    string _Msg;
    public string Msg
    {
        get { return _Msg; }
        set { _Msg = value; }
    }

    List<int?> _custNo;
    public List<int?> custNo
    {
        get { return _custNo; }
        set { _custNo = value; }
    }

}