﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using System.Text;
using TvTools;
using System.Web.Script.Services;

public partial class Controls_RSAdd_FlightNC : BasePage
{
    public static string twoLetterISOLanguageName = "en";

    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        List<string> lang = new List<string>() { "ar-DZ", "ar-DZ", "ar", "en-AU", "en-GB", "en-NZ", "fr-CH", "fr", "pt-BR", "pt", "sr-SR", "sr-CS", "sr", "zh-CN", "zh-HK", "zh-TW" };
        if (lang.Contains(UserData.Ci.Name))
            twoLetterISOLanguageName = UserData.Ci.Name;
        else
            twoLetterISOLanguageName = UserData.Ci.TwoLetterISOLanguageName != "en" ? UserData.Ci.TwoLetterISOLanguageName : "";                               
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFormData()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];

        return new
        {
            flyDate = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today,
            fromDate = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getRouteFrom(string RecordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;        
        List<FlightAndDaysRecord> depFlights = new Flights().getDepartureFlightAndLocation(UserData.Operator, UserData.Market, null, ref errorMsg);
        if (string.IsNullOrEmpty(ResData.ResMain.PackType) || string.Equals(UserData.CustomRegID, Common.crID_Elsenal))
        {
            var query = from q in depFlights
                        where q.DepCity == ResData.ResMain.DepCity || q.ArrCity == ResData.ResMain.ArrCity ||
                              q.ArrCity == ResData.ResMain.DepCity || q.DepCity == ResData.ResMain.ArrCity
                        group q by new { RecID = q.DepCity, Name = q.DepCityNameL + " (" + q.DepAirportNameL + ")" } into k
                        select new { RecID = k.Key.RecID, Name = k.Key.Name };
            return query;
        }
        else
        {
            var query = from q in depFlights
                        where q.DepCity == ResData.ResMain.DepCity || q.DepCity == ResData.ResMain.ArrCity
                        group q by new { RecID = q.DepCity, Name = q.DepCityNameL + " (" + q.DepAirportNameL + ")" } into k
                        select new { RecID = k.Key.RecID, Name = k.Key.Name };
            return query;
        }
        
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getRouteTo(string RouteFrom)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;        
        Int32? routeFrom = Conversion.getInt32OrNull(RouteFrom);
        if (routeFrom.HasValue)
        {
            List<FlightAndDaysRecord> depFlights = new Flights().getDepartureFlightAndLocation(UserData.Operator, UserData.Market, routeFrom, ref errorMsg);
            if (string.IsNullOrEmpty(ResData.ResMain.PackType))
            {
                var query = from q in depFlights
                            group q by new { RecID = q.ArrCity, Name = q.ArrCityNameL + " (" + q.ArrAirportNameL + ")" } into k
                            select new { RecID = k.Key.RecID, Name = k.Key.Name };
                return query;
            }
            else
            {
                var query = from q in depFlights                            
                            group q by new { RecID = q.ArrCity, Name = q.ArrCityNameL + " (" + q.ArrAirportNameL + ")" } into k
                            select new { RecID = k.Key.RecID, Name = k.Key.Name };
                return query;
            }
        }
        else
        {
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getFlights(int? RouteFrom, int? RouteTo, long? FlyDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;        
        Int32? routeFrom = Conversion.getInt32OrNull(RouteFrom);
        Int32? routeTo = Conversion.getInt32OrNull(RouteTo);             

        DateTime flyDate = new DateTime(1970, 1, 1);
        if (!FlyDate.HasValue) flyDate = DateTime.Today;
        flyDate =  flyDate.AddDays((Convert.ToInt64(FlyDate) / 86400000) + 1);        

        List<FlightDayRecord> flights = new Flights().getRouteFlights(UserData, UserData.Market, flyDate, routeFrom, routeTo, null, ref errorMsg);
        var query = from q in flights
                    group q by new { FlightNo = q.FlightNo, Name = q.FlightNo + " (" + q.DepAirport + "=>" + q.ArrAirport + ")" } into k
                    select new { FlightNo = k.Key.FlightNo, Name = k.Key.Name };
        return query;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getClass()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;        
        List<FlightClassRecord> flightClass = new Flights().getFlightClass(UserData.Market, ref errorMsg);
        var query = from q in flightClass
                    where q.B2B
                    select q;
        return query; 
    }

    [WebMethod]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    where s.Status == 0
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }    

    [WebMethod]
    public static string CalcService(string selectedCusts, int? Departure, int? Arrival, string Flight, string SClass, long? FlyDate)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;        
        Int32? routeFrom = Conversion.getInt32OrNull(Departure);
        Int32? routeTo = Conversion.getInt32OrNull(Arrival);

        DateTime flyDate = new DateTime(1970, 1, 1);
        if (!FlyDate.HasValue) flyDate = DateTime.Today;
        flyDate = flyDate.AddDays((Convert.ToInt64(FlyDate) / 86400000) + 1); 
        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] selectedCust = selectedCusts.Split('|');
        TvBo.FlightDayRecord FlightDay = new Flights().getFlightDay(UserData, Flight, flyDate, ref errorMsg);
        
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }
               
        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((flyDate - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, flyDate, flyDate, "FLIGHT", Flight,
                                string.Empty, string.Empty, string.Empty, SClass, Night, StartDay, Night, routeFrom, routeTo, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
            ResServiceRecord service = ResData.ResService.Find(f => f.RecID == ServiceID);
            DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "FLIGHT", ServiceID, ref errorMsg);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\",\"Adult\":\"{2}\",\"Child\":\"{3}\",\"Unit\":\"{4}\"", 
                    returnData.Rows[0]["SalePrice"].ToString() + " " + returnData.Rows[0]["SaleCur"].ToString(), 
                    supplierRec.NameL,
                    AdlCnt, 
                    ChdCnt,
                    service.Unit);            
        }
        return "{" + retVal + "}";
    }

    [WebMethod]
    public static string SaveService(string selectedCusts, int? Departure, int? Arrival, string Flight, string SClass, long? FlyDate, string recordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");
        Int32? routeFrom = Conversion.getInt32OrNull(Departure);
        Int32? routeTo = Conversion.getInt32OrNull(Arrival);

        DateTime flyDate = new DateTime(1970, 1, 1);
        if (!FlyDate.HasValue) flyDate = DateTime.Today;
        flyDate = flyDate.AddDays((Convert.ToInt64(FlyDate) / 86400000) + 1); 
        
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] selectedCust = selectedCusts.Split('|');
        TvBo.FlightDayRecord FlightDay = new Flights().getFlightDay(UserData, Flight, flyDate, ref errorMsg);

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
        Int16 StartDay = Convert.ToInt16((flyDate - ResData.ResMain.BegDate.Value).Days);
        Int16 Night = 1;
        ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, flyDate, flyDate, "FLIGHT", Flight,
                                string.Empty, string.Empty, string.Empty, SClass, Night, StartDay, Night, routeFrom, routeTo, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
        if (!string.IsNullOrEmpty(errorMsg))
        {
            retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
        }
        else
        {
            if (_recordType == true)
            {
                if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                {
                    List<ResServiceRecord> resServiceList = ResData.ResService;
                    ResServiceRecord resService = resServiceList.LastOrDefault();
                    resService.ExcludeService = false;
                    HttpContext.Current.Session["ResData"] = ResData;
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
                else
                {
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                    return "{" + retVal + "}";
                }
            }
            else
            {
                List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                {
                    HttpContext.Current.Session["ResData"] = ResData;
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "<br />";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
                else
                {
                    string Msg = string.Empty;
                    foreach (ReservastionSaveErrorRecord row in returnData)
                    {
                        if (!row.ControlOK)
                            Msg += row.Message + "\n";
                    }
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                }
            }
        }
        return "{" + retVal + "}";
    }    
}
