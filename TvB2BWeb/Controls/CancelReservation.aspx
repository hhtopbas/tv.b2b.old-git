﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CancelReservation.aspx.cs"
    Inherits="Controls_CancelReservation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "CancelReservation")%></title>

    <link href="../CSS/CancelReservation.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/main.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        var ComboSelect = '<%= GetGlobalResourceObject("LibraryResource", "ComboSelect") %>';
        var plsCancelRes = '<%= GetGlobalResourceObject("Controls", "plsCancelRes") %>';
        var errorCode = '<%= GetGlobalResourceObject("Controls", "errorCode") %>';

        function logout() {
            self.parent.logout();
        }

        function changeReason() {
            if ($("#ddlCancelReason").val() == '') {
                return;
            }
            $.ajax({
                type: "POST",
                url: "../Controls/CancelReservation.aspx/getReason",
                data: '{"ReasonID":"' + $("#ddlCancelReason").val() + '","DrReport":"' + ($('#cbDrReport').attr('checked') ? "Y" : "N") + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        var retVal = $.json.decode(msg.d);
                        $("#txtPenaltySalePrice").html(retVal.Price + " " + retVal.Cur);
                        $("#txtPenaltySalePricePer").html(retVal.Per);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function drawPrices() {
            $.ajax({
                type: "POST",
                url: "../Controls/CancelReservation.aspx/getPrices",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        var retVal = $.json.decode(msg.d);
                        $("#txtResStat").html(retVal.ResStat);
                        $("#txtSalePrice").html(retVal.SalePrice);
                        $("#txtAmountToPay").html(retVal.AmountToPay);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function getCancelReason() {
            $.ajax({
                type: "POST",
                url: "../Controls/CancelReservation.aspx/getCancelReason",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#ddlCancelReason").append('<option value="">' + ComboSelect + '</option>');
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        if (msg.d.ShowDrReport == true) {
                            $("#cbDrReport").show();
                            $("#cbDrReport").next('label').show();
                        }
                        else {
                            $("#cbDrReport").hide();
                            $("#cbDrReport").next('label').hide();
                        }
                        $.each(msg.d.CancelReason, function (i) {
                            if (this.Penalized == 'Y')
                                $("#ddlCancelReason").append("<option value='" + this.RecID + "'>" + this.NameL + " (Penalized)" + "</option>");
                            else $("#ddlCancelReason").append("<option value='" + this.RecID + "'>" + this.NameL + "</option>");
                        });
                    }
                    drawPrices();
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
            $("#ddlCancelReason").on("onchange", function () {
                changeReason();
            });
        }

        function btnOKClick() {
            if ($("#ddlCancelReason").val() == '') {
                alert(plsCancelRes);
                return;
            }
            var _data = '';
            var _data = '{"CancelCode":"' + $("#ddlCancelReason").val() + '","DrReport":"' + ($('#cbDrReport').attr('checked') ? "Y" : "N") + '"}';
            $.ajax({
                type: "POST",
                url: "../Controls/CancelReservation.aspx/setCancelReservation",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        var retVal = $.json.decode(msg.d);
                        if (retVal.ErrCode == 0) {
                            window.close;
                            self.parent.returnCancelRes_Save();
                        }
                        else
                            alert(errorCode + retVal.ErrCode);
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        alert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function btnCancelClick() {
            window.close;
            self.parent.returnCancelRes_Cancel();
        }

        $(document).ready(function () {
            getCancelReason();
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="divPenaltyForm">
            <br />
            <table>
                <tr>
                    <td class="firstColumn">
                        <%= GetGlobalResourceObject("Controls", "lblReason") %>&nbsp;:&nbsp;
                    </td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlCancelReason" runat="server" Width="98%" />
                    </td>
                </tr>
                <tr>
                    <td class="seperator">&nbsp;
                    </td>
                    <td class="seperator">&nbsp;
                    </td>
                    <td class="seperator">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="firstColumn">
                        <%= GetGlobalResourceObject("Controls", "lblResStat") %>&nbsp;:&nbsp;
                    </td>
                    <td style="width: 110px;">
                        <span id="txtResStat"></span>
                    </td>
                    <td>
                        <input id="cbDrReport" type="checkbox" onclick="changeReason()" /><label for="cbDrReport"><%= GetGlobalResourceObject("Controls", "cbDrReport") %></label>
                    </td>
                </tr>
                <tr>
                    <td class="seperator">&nbsp;
                    </td>
                    <td class="seperator">&nbsp;
                    </td>
                    <td class="seperator">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="firstColumn">
                        <%= GetGlobalResourceObject("Controls", "lblSalePrice") %>&nbsp;:&nbsp;
                    </td>
                    <td align="right">
                        <span id="txtSalePrice"></span>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="seperator">&nbsp;
                    </td>
                    <td class="seperator">&nbsp;
                    </td>
                    <td class="seperator">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="firstColumn">
                        <%= GetGlobalResourceObject("Controls", "lblAmountToPay") %>&nbsp;:&nbsp;
                    </td>
                    <td align="right">
                        <span id="txtAmountToPay"></span>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="seperator">&nbsp;
                    </td>
                    <td class="seperator">&nbsp;
                    </td>
                    <td class="seperator">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="firstColumn">
                        <%= GetGlobalResourceObject("Controls", "lblPenaltySalePrice") %>&nbsp;:&nbsp;
                    </td>
                    <td align="right">
                        <b><span id="txtPenaltySalePrice"></span></b>
                    </td>
                    <td>
                        <span id="txtPenaltySalePricePer"></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <br />
                        <input id="btnOK" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>'
                            onclick="btnOKClick();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />&nbsp;&nbsp;
                    <input id="btnCancel" type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel") %>'
                        onclick="btnCancelClick();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
