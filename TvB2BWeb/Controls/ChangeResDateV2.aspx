﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangeResDateV2.aspx.cs" Inherits="Controls_ChangeResDateV2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- no cache headers -->
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <!-- end no cache headers -->
  <!--[if gt IE 10]>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <%= GetGlobalResourceObject("PageTitle", "ChangeResDate")%></title>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery-ui.custom.min.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.json.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.simplemodal.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.blockUI.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.query-2.1.7.js" type="text/javascript"></script>

    <link href="../CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/ChangeResDate.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .special_day a { background: #ABABAB url('../Images/FlightDay.gif') no-repeat !important; color: #222222 !important; }
    </style>

    <script language="javascript" type="text/javascript">

        var dayColor = [];

        function showAlert(msg) {
            $(function () {
                $("#messages").html(msg);
                $("#dialog").dialog("destroy");
                $("#dialog-message").dialog({
                    modal: true,
                    buttons: {
                        '<%= GetGlobalResourceObject("LibraryResource", "btnOK") %>': function () {
                            $(this).dialog('close');
                            return true;
                        }
                    }
                });
            });
        }

        function flightDays(date) {
            var noWeekend = $.datepicker.noWeekends(date);
            if (dayColor != null && dayColor.length > 0) {
                for (i = 0; i < dayColor.length; i++) {
                    var year = date.getFullYear();
                    var month = date.getMonth();
                    var day = date.getDate();
                    if (month == (dayColor[i].Month - 1) && day == dayColor[i].Day && year == dayColor[i].Year) {
                        return [true, 'special_day', dayColor[i].Text];
                    }
                }
                return [true, ''];
            }
            return [true, ''];
        }

        function getFormData(resNo) {
            $.ajax({
                type: "POST",
                url: "../Controls/ChangeResDateV2.aspx/getFormData",
                data: '{"ResNo":"' + resNo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.hasOwnProperty('d') && msg.d != null) {
                        var data = msg.d;
                        dayColor = [];
                        dayColor = data;
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function resDateChanged() {

            var params = new Object();
            params.newBeginDate = $("#iCheckIn").datepicker('getDate').getTime();
            params.save = true;

            $.ajax({
                type: "POST",
                url: "../Controls/ChangeResDateV2.aspx/changeDate",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        var returnVal = $.json.decode(msg.d);
                        if (returnVal.retVal >= 0) {
                            window.close;
                            self.parent.returnCancelChangeDate(true);
                        }
                        else if (returnVal.retVal < 0) {
                            showAlert(returnVal.Message);
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function confirmResDateChange(msg) {

            $("#messages").html(msg);
            $("#dialog").dialog("destroy");
            $("#dialog-message").dialog({
                modal: true,
                buttons: {
                    '<%= GetGlobalResourceObject("LibraryResource", "btnYes") %>': function () {
                        $(this).dialog('close');
                        resDateChanged();
                    },
                    '<%= GetGlobalResourceObject("LibraryResource", "btnNo") %>': function () {
                        $(this).dialog('close');
                        window.close;
                        self.parent.cancelChangeResDate();
                    }
                }
            });

        }

        function nextStep() {

            var params = new Object();
            params.newBeginDate = $("#iCheckIn").datepicker('getDate').getTime();

            params.save = false;
            $.ajax({
                type: "POST",
                url: "../Controls/ChangeResDateV2.aspx/changeDate",
                data: $.json.encode(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        var returnVal = $.json.decode(msg.d);
                        if (returnVal.retVal == 0) {
                            resDateChanged();
                        }
                        else if (returnVal.retVal < 0) {
                            showAlert(returnVal.Message);
                        }
                        else {
                            confirmResDateChange(returnVal.Message);
                        }
                    }
                },
                error: function (xhr, msg, e) {
                    if (xhr.responseText != '' && xhr.responseText != '{"d":null}')
                        showAlert(xhr.responseText);
                },
                statusCode: {
                    408: function () {
                        logout();
                    }
                }
            });
        }

        function logout() {
            self.parent.logout();
        }

        function canceled() {
            window.close;
            self.parent.cancelChangeResDate();
        }

        $(document).ready(function () {
            $.query = $.query.load(location.href);
            var resNo = $.query.get('ResNo');

            $.datepicker.setDefaults($.datepicker.regional['<%= twoLetterISOLanguageName %>' != 'en' ? '<%= twoLetterISOLanguageName %>' : '']);

                $("#iCheckIn").datepicker({
                    showOn: "button",
                    buttonImage: "../Images/Calendar.gif",
                    buttonImageOnly: true,
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    onSelect: function (dateText, inst) {
                        if (dateText != '') {
                            var date2 = new Date(parseInt(inst.selectedYear), parseInt(inst.selectedMonth), parseInt(inst.selectedDay));
                        }
                    },
                    minDate: new Date(),
                    beforeShowDay: flightDays
                });

                getFormData(resNo);
            });

    </script>

</head>
<body>
    <form id="changeResDate" runat="server">
        <div id="divChangeDate">
            <div class="calendarDiv">
                <br />
                <%= GetGlobalResourceObject("Controls", "lblResBeginDate")%>
            :
            <br />
                <input id="iCheckIn" type="text" style="width: 100px;" />
                <%--<asp:TextBox ID="txtBegDate" runat="server" Width="100px" />
            <rjs:PopCalendar ID="ppcBegDate" runat="server" Control="txtBegDate" />--%>
                <br />
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <div style="text-align: center; font-size: 8pt;">
                <input type="button" value='<%= GetGlobalResourceObject("LibraryResource", "sNext")%>'
                    onclick="nextStep();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" value='<%= GetGlobalResourceObject("LibraryResource", "btnCancel")%>'
                onclick="canceled();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
            </div>
        </div>
        <div id="dialog-message" title='<%= GetGlobalResourceObject("ResView", "lblMessage") %>'
            style="display: none;">
            <p>
                <span id="messages">Message</span>
            </p>
        </div>
    </form>
</body>
</html>
