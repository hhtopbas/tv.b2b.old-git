﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSEdit_OtherV2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; 
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;        
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        Int32? recID = Conversion.getInt32OrNull(RecID);

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var querySelected = from s in ResData.ResCust
                            join q1 in ResData.ResCon on s.CustNo equals q1.CustNo
                            where s.Status == 0 && q1.ServiceID == recID
                            select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name, Selected = true };
        var queryUnSelected = from s in ResData.ResCust
                              join q1 in ResData.ResCon on s.CustNo equals q1.CustNo
                              where s.Status == 0 && q1.ServiceID != recID && (querySelected.Where(w => w.CustNo == s.CustNo).Count() < 1)
                              group s by new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name } into k
                              select new { CustNo = k.Key.CustNo, Name = k.Key.Name, Selected = false };
        var query = querySelected.Union(queryUnSelected);
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession=true)]
    public static string getServiceDetail(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        Int32? recID = Conversion.getInt32OrNull(RecID);
        if (!recID.HasValue) return string.Empty;
        var query = from q in ResData.ResService
                    where q.RecID == recID.Value
                    select new
                    {
                        BegDate = q.BegDate.Value.ToShortDateString(),
                        EndDate = q.EndDate.Value.ToShortDateString(),
                        DepLocation = q.DepLocationNameL,
                        ArrLocation = q.ArrLocationNameL,
                        Service = q.Service + "|" + q.ServiceType,
                        Adult = q.Adult,
                        Child = q.Child,
                        Unit = q.Unit,
                        Duration = q.Duration,
                        Supplier = q.SupplierName,
                        SupNote = q.SupNote,
                        StatConf = !string.Equals(UserData.CustomRegID, TvBo.Common.crID_Anex) ? q.StatConfNameL : string.Empty,
                        StatSer = q.StatSerNameL,
                        SalePrice = !Equals(q.IncPack, "Y") ? q.SalePrice.Value.ToString("#,###.00") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                        SaleCur = !Equals(q.IncPack, "Y") ? q.SaleCur : "",
                        ResPrice = ResData.ResMain.SalePrice.HasValue ? ResData.ResMain.SalePrice.Value.ToString("#.00") + " " + ResData.ResMain.SaleCur : "",
                        ChangePax = Equals(q.IncPack, "Y") || (q.Compulsory.HasValue && q.Compulsory.Value) ? "0" : "1"
                    };

        if (query == null || query.Count() < 1) return string.Empty;
        return Newtonsoft.Json.JsonConvert.SerializeObject(query.FirstOrDefault());
    }

    [WebMethod(EnableSession = true)]
    public static string getAdService(string RecID)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        int? recID = Conversion.getInt32OrNull(RecID);        
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        TvBo.ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        string errorMsg = string.Empty;
        List<AdServiceRecord> adServiceList = new AdServices().getAdServiceLocations(UserData.Market, ResData.ResMain.PLMarket, ResData.ResMain.BegDate, ResData.ResMain.EndDate, resService.DepLocation, resService.ArrLocation, ref errorMsg);
        var query = from q in adServiceList
                    select new { Code = q.Code + "|" + q.Service, Name = q.LocalName };
        return Newtonsoft.Json.JsonConvert.SerializeObject(query);
    }

    [WebMethod(EnableSession = true)]
    public static editServiceReturnData CalcService(string RecID, string AdService, bool? newRes, string SelectedCust)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        int? recID = Conversion.getInt32OrNull(RecID);

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        decimal? oldResSalePrice = ResData.ResMain.SalePrice;
        ResServiceRecord resService = (TvBo.ResServiceRecord)TvBo.Common.DeepClone(ResData.ResService.Find(f => f.RecID == recID));
        decimal? oldServicePrice = resService.SalePrice;

        List<ResConRecord> serviceResCon = (List<ResConRecord>)TvBo.Common.DeepClone(ResData.ResCon.Where(w => w.ServiceID == resService.RecID).ToList<ResConRecord>());

        string[] selectedCust = SelectedCust.Split('|');

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        int InfCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5 && w.Title < 8).Count();
            InfCnt = tmpPax.Where(w => w.Title > 7).Count();
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;

        string serviceType = string.Empty;
        string service = string.Empty;
        if (AdService.Length > 0)
        {
            serviceType = AdService.Split('|')[1];
            service = AdService.Split('|')[0];
        }

        resService.PriceSource = !Equals(resService.Service, service) || Equals(resService.ServiceType, serviceType) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.ServiceType = serviceType;
        resService.Service = service;
        resService.Supplier = string.Empty;
        resService.Adult = (Int16)AdlCnt;
        resService.Child = (Int16)(ChdCnt + InfCnt);
        resService.Unit = resService.Unit;

        ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
        ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);

        if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
        {
            return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
        }
        else
        {
            resService = ResData.ResService.Find(f => f.RecID == recID.Value);
            SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
            string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
            return new editServiceReturnData
            {
                Calc = true,
                CalcPrice = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString(),
                CalcCur = resService.SalePrice.HasValue && !Equals(resService.IncPack, "Y") ? resService.SaleCur : string.Empty,
                Adult = resService.Adult,
                Child = resService.Child,
                Unit = resService.Unit,
                OldResPrice = oldResSalePrice.HasValue ? oldResSalePrice.Value.ToString("#,###.00") : string.Empty,
                OldServicePrice = oldServicePrice.HasValue ? oldServicePrice.Value.ToString("#,###.00") : string.Empty,
                Supplier = supplierRec.NameL,
                Msg = string.Empty
            };
        }        
    }

    [WebMethod(EnableSession = true)]
    public static editServiceReturnData SaveService(string RecID, string AdService, bool? newRes, string SelectedCust)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        Int32? recID = Conversion.getInt32OrNull(RecID);

        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = (ResServiceRecord)TvBo.Common.DeepClone(ResData.ResService.Find(f => f.RecID == recID));
        string serviceType = string.Empty;
        string service = string.Empty;
        if (AdService.Length > 0)
        {
            serviceType = AdService.Split('|')[1];
            service = AdService.Split('|')[0];
        }        

        string[] selectedCust = SelectedCust.Split('|');

        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        int InfCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5 && w.Title < 8).Count();
            InfCnt = tmpPax.Where(w => w.Title > 7).Count();
        }

        List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
        foreach (string s in selectedCust)
            SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;        
        
        TvBo.AdServiceRecord otherService = new AdServices().getAdService(UserData.Market, serviceType, service, ref errorMsg);
        resService.PriceSource = !Equals(resService.Service, service) || Equals(resService.ServiceType, serviceType) ? 0 : resService.PriceSource;
        resService.IncPack = resService.PriceSource == 0 ? "N" : "Y";
        resService.ServiceType = serviceType;
        resService.Service = service;
        resService.ServiceName = otherService.Name;
        resService.ServiceNameL = otherService.LocalName;        
        resService.Supplier = string.Empty;
        resService.Adult = (Int16)AdlCnt;
        resService.Child = (Int16)(ChdCnt + InfCnt);

        if (newRes.HasValue && newRes.Value)
        {
            ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
            ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);
            if (!new TvBo.Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();
                HttpContext.Current.Session["ResData"] = ResData;
                return new editServiceReturnData { Calc = true, CalcPrice = resService.SalePrice.HasValue ? resService.SalePrice.Value.ToString("#,###.00") : string.Empty, CalcCur = resService.SalePrice.HasValue ? resService.SaleCur : string.Empty, Msg = string.Empty };
            }
        }
        else
        {
            Int32 ErrCode = 0;
            ResData = new Reservation().modifyServiceCustomers(UserData, ResData, resService.RecID, SelectCust, ref errorMsg);
            ResData = new Reservation().extraServiceControlV2(UserData, new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]), resService, ResData.ResCon, ref errorMsg);
            List<int?> recIDList = new List<int?>();
            recIDList.Add(resService.RecID);
            ResData = new TvBo.Reservation().editService(UserData, ResData, recIDList, ref ErrCode, ref errorMsg, false);
            if (ErrCode != 0)
            {
                if (ErrCode < 0)
                    errorMsg = string.IsNullOrEmpty(errorMsg) ? "unknown error." : errorMsg;
                else errorMsg = new ResCalcError().calcError(ErrCode, ResData).ToString();
                return new editServiceReturnData { Calc = false, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = errorMsg };
            }
            else
            {
                resService = ResData.ResService.Find(f => f.RecID == recID.Value);
                SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, resService.Supplier, ref errorMsg);
                string SalePriceStr = !Equals(resService.IncPack, "Y") ? (resService.SalePrice.HasValue ? (resService.SalePrice.Value.ToString("#,###.00") + " " + resService.SaleCur) : "") : HttpContext.GetGlobalResourceObject("LibraryResource", "InPackage").ToString();

                HttpContext.Current.Session["ResData"] = ResData;
                return new editServiceReturnData { Calc = true, CalcPrice = string.Empty, CalcCur = string.Empty, Msg = string.Empty };
            }
        }        
    }

    [WebMethod(EnableSession = true)]
    public static string getChgFee(string RecID, string AdService)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        string errorMsg = string.Empty;
        string retVal = "[\"retVal\":\"{0}\",\"Message\":\"{1}\"]";
        Int32? recID = Conversion.getInt32OrNull(RecID);
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        ResServiceRecord resService = ResData.ResService.Find(f => f.RecID == recID);
        resService.Service = AdService;        
        ResChgFeePrice chgPrice = new Reservation().whatHasChangedResService(UserData, ResData, recID, resService, ref errorMsg);
        if (chgPrice == null)
            return string.Format(retVal, 0, "").Replace('[', '{').Replace(']', '}');
        else
        {
            string chgFeeMsgStr = string.Format(HttpContext.GetGlobalResourceObject("ResView", "msgChangeFree").ToString(),
                                             chgPrice.ChgAmount.Value.ToString("#,###.0"), chgPrice.ChgCur);
            return string.Format(retVal, 1, chgFeeMsgStr).Replace('[', '{').Replace(']', '}');
        }
    }
}
