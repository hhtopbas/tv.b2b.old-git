﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Web.Services;
using TvBo;
using System.Data;
using TvTools;

public partial class Controls_RSAdd_Hotel : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ResDataRecord ResData = (ResDataRecord)Session["ResData"];
        User UserData = (User)Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        ppcCheckIn.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckIn.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');
        
        int beforeDay = 0;
        int afterDay = 0;
        if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel) || string.Equals(UserData.CustomRegID, TvBo.Common.ctID_Falcon))
        {
            beforeDay = -21;
            afterDay = 21;
        }
        else
            if (string.Equals(UserData.CustomRegID, TvBo.Common.crID_Karavan) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_ZemExpert))
            {
                beforeDay = -3;
                afterDay = 3;
            }

        if ((beforeDay != 0 || afterDay != 0) && string.Equals(ResData.ResMain.PackType, "T"))
        {
            ppcCheckIn.From.Date = ResData.ResMain.BegDate.HasValue ? (ResData.ResMain.BegDate.Value.AddDays(beforeDay) > DateTime.Today ? ResData.ResMain.BegDate.Value.AddDays(beforeDay) : DateTime.Today) : DateTime.Today;
            ppcCheckIn.To.Date = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.AddDays(afterDay) : DateTime.Today;
        }
        else
            if (!string.IsNullOrEmpty(ResData.ResMain.PackType))
            {
                ppcCheckIn.From.Date = ResData.ResMain.BegDate.HasValue ? (ResData.ResMain.BegDate.Value > DateTime.Today ? ResData.ResMain.BegDate.Value : DateTime.Today) : DateTime.Today;
                ppcCheckIn.To.Date = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
            }
            else
                ppcCheckIn.From.Date = DateTime.Today;

        if ((beforeDay != 0 || afterDay != 0) && (string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel) || string.Equals(UserData.CustomRegID, TvBo.Common.ctID_Falcon)))
        {
            ppcCheckIn.From.Date = ResData.ResMain.BegDate.HasValue ? (ResData.ResMain.BegDate.Value.AddDays(beforeDay) > DateTime.Today ? ResData.ResMain.BegDate.Value.AddDays(beforeDay) : DateTime.Today) : DateTime.Today;
            ppcCheckIn.To.Date = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.AddDays(afterDay) : DateTime.Today;
        }

        ppcCheckOut.Culture = UserData.Ci.Name + " " + UserData.Ci.EnglishName;
        ppcCheckOut.Format = strFunc.Trim(UserData.Ci.DateTimeFormat.ShortDatePattern, ' ').Replace(UserData.Ci.DateTimeFormat.DateSeparator[0], ' ');

        if ((beforeDay != 0 || afterDay != 0) && string.Equals(ResData.ResMain.PackType, "T"))
        {
            ppcCheckOut.From.Date = ResData.ResMain.BegDate.HasValue ? (ResData.ResMain.BegDate.Value.AddDays(beforeDay) > DateTime.Today ? ResData.ResMain.BegDate.Value.AddDays(beforeDay) : DateTime.Today) : DateTime.Today;
            ppcCheckOut.To.Date = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.AddDays(afterDay) : DateTime.Today;
        }
        else
            if (!string.IsNullOrEmpty(ResData.ResMain.PackType))
            {
                ppcCheckOut.From.Date = ResData.ResMain.BegDate.HasValue ? (ResData.ResMain.BegDate.Value > DateTime.Today ? ResData.ResMain.BegDate.Value : DateTime.Today) : DateTime.Today;
                ppcCheckOut.To.Date = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
            }
            else
                ppcCheckOut.From.Date = DateTime.Today;

        if ((beforeDay != 0 || afterDay != 0) && (string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel) || string.Equals(UserData.CustomRegID, TvBo.Common.ctID_Falcon)))
        {
            ppcCheckOut.From.Date = ResData.ResMain.BegDate.HasValue ? (ResData.ResMain.BegDate.Value.AddDays(beforeDay) > DateTime.Today ? ResData.ResMain.BegDate.Value.AddDays(beforeDay) : DateTime.Today) : DateTime.Today;
            ppcCheckOut.To.Date = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value.AddDays(afterDay) : DateTime.Today;
        }


        if (!IsPostBack)
        {
            ppcCheckIn.DateValue = ResData.ResMain.BegDate.HasValue ? ResData.ResMain.BegDate.Value : DateTime.Today;
            ppcCheckOut.DateValue = ResData.ResMain.EndDate.HasValue ? ResData.ResMain.EndDate.Value : DateTime.Today;
            iNight.Text = (ppcCheckOut.DateValue - ppcCheckIn.DateValue).Days.ToString();
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getLocationList()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        int? city = ResData.ResMain.ArrCity;
        string errorMsg = string.Empty;
        List<TvBo.CodeName> retval = new List<CodeName>();
        if (string.Equals(UserData.CustomRegID, Common.crID_CelexTravel))
            retval = new TvBo.Hotels().getHotelLocationCodeName(UserData, null, new Locations().getLocationForCountry(city.Value, ref errorMsg), null, null, ResData.ResMain.PackType, ref errorMsg);
        else
            retval = new TvBo.Hotels().getHotelLocationCodeName(UserData, new Locations().getLocationForCity(city.Value, ref errorMsg), null, null, null, ResData.ResMain.PackType, ref errorMsg);
        return Newtonsoft.Json.JsonConvert.SerializeObject(retval);
    }

    [WebMethod(EnableSession = true)]
    public static string getHotels(string Location)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (Equals(Location, "undefined")) Location = string.Empty;
        int? hotelLocation = (string.IsNullOrEmpty(Location) || Location == "-1" || Location == "null") ? null : Conversion.getInt32OrNull(Location);
        string errorMsg = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        List<TvBo.CodeName> retval = new List<CodeName>();
        if (string.Equals(ResData.ResMain.PackType, "T"))
            retval = new TvBo.Hotels().getLocationHotelCodeNameForCultureTourAddHotel(UserData, hotelLocation.HasValue ? hotelLocation.Value : 0, null, ref errorMsg);
        else
            retval = new TvBo.Hotels().getLocationHotelCodeName(UserData.Market, hotelLocation.HasValue ? hotelLocation.Value : 0, null, ref errorMsg);

        return Newtonsoft.Json.JsonConvert.SerializeObject(retval);
    }

    [WebMethod(EnableSession = true)]
    public static string getRooms(string Hotel)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retval = string.Empty;
        retval = new TvBo.Hotels().getRoomJSon(UserData.Market, Hotel, null, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getAccoms(string Hotel, string Room)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retval = string.Empty;
        retval = new TvBo.Hotels().getAccomJSon(UserData.Market, Hotel, Room, null, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getBoards(string Hotel)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retval = string.Empty;
        retval = new TvBo.Hotels().getBoardJSon(UserData.Market, Hotel, null, ref errorMsg);
        retval = retval.Replace("'", " ");
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static string getTourist()
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        string errorMsg = string.Empty;
        string retval = string.Empty;
        var query = from s in ResData.ResCust
                    where s.Status == 0
                    select new { CustNo = s.CustNo, Name = s.Surname + " " + s.Name, TitleStr = s.TitleStr };
        retval = Newtonsoft.Json.JsonConvert.SerializeObject(query);
        return retval;
    }

    [WebMethod(EnableSession = true)]
    public static addServiceReturnData CalcService(string selectedCusts, string Hotel, string Room, string Accom, string Board, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        TvBo.ResDataRecord ResData = new ResDataRecord();
        ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);
        string[] selectedCust = selectedCusts.Split('|');
        TvBo.HotelRecord HotelRec = new Hotels().getHotelDetail(UserData, Hotel, ref errorMsg);
        TvBo.HotelAccomRecord HotelAccom = new Hotels().getHotelAccom(UserData.Market, Hotel, Room, Accom, ref errorMsg);
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }
        
        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);

        if (!begDate.HasValue && !endDate.HasValue)
            return new addServiceReturnData
            {
                Calc = false,
                Msg = "Dates is null"
            };

        #region StopSale Kontrolü için eklendi HHTOPBAS
        bool isStopSale = false;
        if (HttpContext.Current.Session["MixCriteria"] != null)
        {
            MixSearchCriteria criteriaOld = (MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];

            MixSearchCriteria criteria = new MixSearchCriteria();
            criteria.Hotel = Hotel;
            criteria.Room = Room;
            criteria.Board = Board;

            criteria.CurrentCur = criteriaOld.CurrentCur;
            criteria.RoomInfoList = new List<MixSearchCriteriaRoom>();
            criteria.RoomInfoList.Add(new MixSearchCriteriaRoom { AdultCount = (short)AdlCnt, ChildCount = (short)ChdCnt });
            criteria.CheckIn = begDate.Value < DateTime.Today ? begDate.Value.AddDays(1) : begDate.Value;
            criteria.ExpandDay = 0;
            criteria.NightFrom = Convert.ToInt16((endDate.Value - begDate.Value).TotalDays);
            criteria.NightTo = criteria.NightFrom;
            List<SearchResultOH> priceList = new OnlyHotelSearch().getHotelSaleSearch(UserData, criteria, null, ref errorMsg);
            if (priceList == null || priceList.Count == 0)
                isStopSale = true;
        }

        #endregion
        if (!isStopSale)
        {
            if ((string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Karavan)) && string.Equals(ResData.ResMain.PackType, "T"))
            {
                if (ResData.ResMain.PBegDate.HasValue && ResData.ResMain.PNight.HasValue)
                {
                    if (endDate.Value > ResData.ResMain.PBegDate.Value && endDate.Value < ResData.ResMain.PBegDate.Value.AddDays(ResData.ResMain.PNight.Value))
                    {
                        return new addServiceReturnData
                        {
                            Calc = false,
                            Msg = "Please select date different from tour date.(" + ResData.ResMain.PBegDate.Value.ToShortDateString() + " - " + ResData.ResMain.PBegDate.Value.AddDays(ResData.ResMain.PNight.Value).ToShortDateString() + ")"
                        };
                    }
                    if (begDate.Value > ResData.ResMain.PBegDate.Value && begDate.Value < ResData.ResMain.PBegDate.Value.AddDays(ResData.ResMain.PNight.Value))
                    {
                        return new addServiceReturnData
                        {
                            Calc = false,
                            Msg = "Please select date different from tour date.(" + ResData.ResMain.PBegDate.Value.ToShortDateString() + " - " + ResData.ResMain.PBegDate.Value.AddDays(ResData.ResMain.PNight.Value).ToShortDateString() + ")"
                        };
                    }
                }
                if ((endDate.Value - begDate.Value).Days > 21)
                {
                    return new addServiceReturnData
                    {
                        Calc = false,
                        Msg = string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) ? "Change date please, max 21 night." : "Change date please, max 3 night."
                    };
                }
            }

            List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
            foreach (string s in selectedCust)
                SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
            Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
            Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
            ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "HOTEL", Hotel, Room, Accom, Board, string.Empty, Night, StartDay, Night, HotelRec.TrfLocation, null, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty, string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                return new addServiceReturnData
                {
                    Calc = false,
                    Msg = errorMsg
                };
            }
            else
            {
                int ServiceID = ResData.ResService[ResData.ResService.Count - 1].RecID;
                DataTable returnData = new Reservation().CalcServicePrice(UserData, ResData, "HOTEL", ServiceID, ref errorMsg);
                if (returnData != null)
                {
                    SupplierRecord supplierRec = new TvSystem().getSupplier(UserData.Market, returnData.Rows[0]["Supplier"] != null ? returnData.Rows[0]["Supplier"].ToString() : "", ref errorMsg);
                    retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", returnData.Rows[0]["SalePrice"].ToString() + " " + returnData.Rows[0]["SaleCur"].ToString(), supplierRec.NameL);
                    return new addServiceReturnData
                    {
                        Calc = true,
                        CalcPrice = returnData.Rows[0]["SalePrice"].ToString(),
                        CalcCur = returnData.Rows[0]["SaleCur"].ToString(),
                        Supplier = supplierRec.NameL
                    };
                }
                else
                {
                    return new addServiceReturnData
                    {
                        Calc = false,
                        Msg = string.IsNullOrEmpty(errorMsg) ? "unknown error" : errorMsg
                    };
                }
            }
        }
        else
        {
            return new addServiceReturnData
            {
                Calc = false,
                Msg = HttpContext.GetGlobalResourceObject("SpCalcErrors", "changeError1").ToString()
            };
        }
      

        

      
        //return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static addServiceReturnData SaveService(string selectedCusts, string Hotel, string Room, string Accom, string Board, string BegDate, string BegDateFormat, string EndDate, string EndDateFormat, string recordType)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; 
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        bool _recordType = Equals(recordType, "temp");

        if (HttpContext.Current.Session["ResData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }

        TvBo.ResDataRecord ResData = new ResTables().copyData((TvBo.ResDataRecord)HttpContext.Current.Session["ResData"]);

        string[] selectedCust = selectedCusts.Split('|');
        TvBo.HotelRecord HotelRec = new Hotels().getHotelDetail(UserData, Hotel, ref errorMsg);
        TvBo.HotelAccomRecord HotelAccom = new Hotels().getHotelAccom(UserData.Market, Hotel, Room, Accom, ref errorMsg);
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }

        DateTime? begDate = Conversion.convertDateTime(BegDate, BegDateFormat);
        DateTime? endDate = Conversion.convertDateTime(EndDate, EndDateFormat);
        if (!begDate.HasValue && !endDate.HasValue)
            return new addServiceReturnData
            {
                Calc = false,
                Msg = "Dates is null"
            };

        #region StopSale Kontrolü için eklendi HHTOPBAS
        bool isStopSale = false;
        if (HttpContext.Current.Session["MixCriteria"] != null)
        {
            MixSearchCriteria criteriaOld = (MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];

            MixSearchCriteria criteria = new MixSearchCriteria();
            criteria.Hotel = Hotel;
            criteria.Room = Room;
            criteria.Board = Board;

            criteria.CurrentCur = criteriaOld.CurrentCur;
            criteria.RoomInfoList = new List<MixSearchCriteriaRoom>();
            criteria.RoomInfoList.Add(new MixSearchCriteriaRoom { AdultCount = (short)AdlCnt, ChildCount = (short)ChdCnt });
            criteria.CheckIn =begDate.Value<DateTime.Today?begDate.Value.AddDays(1):begDate.Value;
            criteria.ExpandDay = 0;
            criteria.NightFrom = Convert.ToInt16((endDate.Value - begDate.Value).TotalDays);
            criteria.NightTo = criteria.NightFrom;
            List<SearchResultOH> priceList = new OnlyHotelSearch().getHotelSaleSearch(UserData, criteria, null, ref errorMsg);

            if (priceList == null || priceList.Count == 0)
                isStopSale = true;
        }

        #endregion
        if (!isStopSale)
        {
            if ((string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_Karavan)) && string.Equals(ResData.ResMain.PackType, "T"))
            {

                if (((endDate.Value - begDate.Value).Days > 21 && string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet)) || ((endDate.Value - begDate.Value).Days > 3 && !string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet)))
                {
                    return new addServiceReturnData
                    {
                        Calc = false,
                        Msg = (string.Equals(UserData.CustomRegID, TvBo.Common.crID_UpJet) || string.Equals(UserData.CustomRegID, TvBo.Common.crID_CelexTravel)) ? "Change date please, max 21 night." : "Change date please, max 3 night."
                    };
                }
            }

            List<SelectCustRecord> SelectCust = new ReservationCommon().CreateSelectCustTableList(ResData);
            foreach (string s in selectedCust)
                SelectCust.Find(f => f.CustNo == Convert.ToInt32(s)).Selected = true;
            Int16 StartDay = Convert.ToInt16((begDate.Value - ResData.ResMain.BegDate.Value).Days);
            Int16 Night = Convert.ToInt16((endDate.Value - begDate.Value).Days);
            ResData = new TvBo.Reservation().AddService(UserData, ResData, SelectCust, 1, begDate.Value, endDate.Value, "HOTEL", Hotel, Room, Accom, Board, string.Empty, Night, StartDay, Night,
                                                        HotelRec.TrfLocation, null, "", "", 0, 0, null, null, string.Empty, string.Empty, null, null, null, null, string.Empty, string.Empty, string.Empty,
                                                        string.Empty, null, false, null, string.Empty, null, ref errorMsg, null, string.Empty, null, null, null, null, null);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                return new addServiceReturnData
                {
                    Calc = false,
                    Msg = errorMsg
                };
            }
            else
            {
                if (_recordType == true)
                {
                    if (new Reservation().reCalcResData(UserData, ref ResData, ref errorMsg))
                    {
                        List<ResServiceRecord> resServiceList = ResData.ResService;
                        ResServiceRecord resService = resServiceList.LastOrDefault();
                        resService.ExcludeService = false;
                        HttpContext.Current.Session["ResData"] = ResData;
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                        //return "{" + retVal + "}";
                        return new addServiceReturnData
                        {
                            Calc = true,
                        };
                    }
                    else
                    {
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, errorMsg);
                        //return "{" + retVal + "}";
                        return new addServiceReturnData
                        {
                            Calc = false,
                            Msg = errorMsg
                        };
                    }
                }
                else
                {
                    List<ReservastionSaveErrorRecord> returnData = new Reservation().SavePartialResServiceAndReCalc(UserData, ref ResData);
                    if (returnData.Count == 1 && (bool)returnData[0].ControlOK)
                    {
                        HttpContext.Current.Session["ResData"] = ResData;
                        string Msg = string.Empty;
                        foreach (ReservastionSaveErrorRecord row in returnData)
                            Msg += row.Message + "<br />";
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                        return new addServiceReturnData
                        {
                            Calc = true,
                            Msg = Msg
                        };
                    }
                    else
                    {
                        string Msg = string.Empty;
                        foreach (ReservastionSaveErrorRecord row in returnData)
                        {
                            //if (!row.ControlOK)
                            Msg += row.Message + "\n";
                        }
                        //retVal = string.Format("\"Price\":\"{0}\",\"Supplier\":\"{1}\"", string.Empty, Msg);
                        return new addServiceReturnData
                        {
                            Calc = false,
                            Msg = Msg
                        };
                    }
                }
            }
        }
        else
        {
            return new addServiceReturnData
            {
                Calc = false,
                Msg = HttpContext.GetGlobalResourceObject("SpCalcErrors", "changeError1").ToString()
            };
        }

        
        //return "{" + retVal + "}";
    }

    [WebMethod(EnableSession = true)]
    public static string PaxControl(string selectedCusts, string Hotel, string Room, string Accom)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci; Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        string errorMsg = string.Empty;
        string retVal = string.Empty;
        TvBo.ResDataRecord ResData = (TvBo.ResDataRecord)HttpContext.Current.Session["ResData"];
        TvBo.HotelAccomRecord HotelAccom = new Hotels().getHotelAccom(UserData.Market, Hotel, Room, Accom, ref errorMsg);
        string[] selectedCust = selectedCusts.Split('|');
        var tmpPax = from q1 in selectedCust.AsEnumerable()
                     join q2 in ResData.ResCust on Convert.ToInt32(q1) equals q2.CustNo
                     select new { CustNo = q2.CustNo, Title = q2.Title };
        int AdlCnt = 0;
        int ChdCnt = 0;
        if (tmpPax.Count() > 0)
        {
            AdlCnt = tmpPax.Where(w => w.Title < 6).Count();
            ChdCnt = tmpPax.Where(w => w.Title > 5).Count();
        }
        else
        {
            AdlCnt = 0;
            ChdCnt = 0;
        }
        Int16 ErrorCode = 0;
        if (HotelAccom == null) ErrorCode = 1; // Hotel accommodation not found.        
        if (HotelAccom.MinAdl > AdlCnt) ErrorCode = 2; // Servisi alacak olan yetişkin sayısı, odada kalacak en az yetişkin sayısından büyük.        
        if (HotelAccom.MaxAdl < AdlCnt) ErrorCode = 3; // Servisi alacak olan yetişkin sayısı, odada kalacak yetişkin sayısından fazla.        
        if (HotelAccom.MaxChd < ChdCnt) ErrorCode = 4; // Servisi alacak olan çocuk sayısı, odada kalacak çocuk sayısından fazla.
        if (HotelAccom.MaxPax < (AdlCnt + ChdCnt)) ErrorCode = 5; // Servisi alacak olan pax sayısı, odada kalacak pax sayısından fazla.
        if (ErrorCode > 0)
            retVal = string.Format("\"Adult\":-{0},\"Child\":{1}", ErrorCode, 0);
        else retVal = string.Format("\"Adult\":{0},\"Child\":{1}", AdlCnt.ToString(), ChdCnt.ToString());
        return "{" + retVal + "}";
    }

}
