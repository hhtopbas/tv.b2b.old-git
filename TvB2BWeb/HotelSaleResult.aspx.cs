﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using TvBo;
using TvTools;

public partial class HotelSaleResult : BasePage
{
    public static string templatePath = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        User UserData = (User)Session["UserData"];
        CultureInfo ci = UserData.Ci;
        Thread.CurrentThread.CurrentCulture = ci;
        Thread.CurrentThread.CurrentUICulture = ci;
        templatePath = WebRoot.BasePageRoot + "Data/";
        if (!IsPostBack)
        {
            HttpContext.Current.Session["SearchResultOH"] = null;
        }
    }

    public static HotelSaleResultFilter createResultFilter(User UserData, List<SearchResultOH> result, HotelSaleResultFilter filteredData)
    {
        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        List<Location> locations = TvBo.CacheObjects.getLocationList(UserData.Market);

        var Resorts = from q in result
                      join l in locations on q.HotLocation equals l.RecID
                      group q by new { ID = q.HotLocation, Name = useLocalName ? l.NameL : l.Name } into k
                      select new
                          {
                              ID = k.Key.ID,
                              Name = k.Key.Name
                          };

        if ((Resorts != null && Resorts.Count() > 1) || filteredData.FilterResort != null)
        {
            filteredData.FilterResort.Clear();

            filteredData.FilterResort.Add(new HotelSaleResultFilterList()
            {
                ID = string.Empty,
                Name = HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll").ToString(),
            });
            foreach (var row in Resorts.OrderBy(o => o.Name))
            {
                filteredData.FilterResort.Add(new HotelSaleResultFilterList()
                {
                    ID = row.ID.ToString(),
                    Name = row.Name,
                    Select = row.ID.ToString() == filteredData.FilterResortSelect
                });
            }
        }

        var Category = from q in result
                       group q by new { ID = q.HotCat, Name = q.HotCat } into k
                       select new { k.Key.ID, k.Key.Name };
        if ((Category != null && Category.Count() > 1) || filteredData.FilterCategory != null)
        {
            filteredData.FilterCategory.Clear();
            filteredData.FilterCategory.Add(new HotelSaleResultFilterList()
            {
                ID = string.Empty,
                Name = HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll").ToString(),
            });
            foreach (var row in Category.OrderBy(o => o.Name))
            {
                filteredData.FilterCategory.Add(new HotelSaleResultFilterList()
                {
                    ID = row.ID.ToString(),
                    Name = row.Name,
                    Select = row.ID.ToString() == filteredData.FilterCategorySelect
                });
            }
        }

        var Hotel = from q in result
                    group q by new { ID = q.Hotel, Name = useLocalName ? q.HotelNameL : q.HotelName } into k
                    select new { k.Key.ID, k.Key.Name };
        if ((Hotel != null && Hotel.Count() > 1) || filteredData.FilterHotel != null)
        {
            filteredData.FilterHotel.Clear();
            filteredData.FilterHotel.Add(new HotelSaleResultFilterList()
            {
                ID = string.Empty,
                Name = HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll").ToString(),
            });
            foreach (var row in Hotel.OrderBy(o => o.Name))
            {
                filteredData.FilterHotel.Add(new HotelSaleResultFilterList()
                {
                    ID = row.ID.ToString(),
                    Name = row.Name,
                    Select = row.ID.ToString() == filteredData.FilterHotelSelect
                });
            }
        }

        var Room = from q in result
                   group q by new { ID = q.Room, Name = useLocalName ? q.RoomNameL : q.RoomName } into k
                   select new { k.Key.ID, k.Key.Name };
        if ((Room != null && Room.Count() > 1) || filteredData.FilterRoom != null)
        {
            filteredData.FilterRoom.Clear();
            filteredData.FilterRoom.Add(new HotelSaleResultFilterList()
            {
                ID = string.Empty,
                Name = HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll").ToString(),
            });
            foreach (var row in Room.OrderBy(o => o.Name))
            {
                filteredData.FilterRoom.Add(new HotelSaleResultFilterList()
                {
                    ID = row.ID.ToString(),
                    Name = row.Name,
                    Select = row.ID.ToString() == filteredData.FilterRoomSelect
                });
            }
        }

        var Board = from q in result
                    group q by new { ID = q.Board, Name = useLocalName ? q.BoardNameL : q.BoardName } into k
                    select new { k.Key.ID, k.Key.Name };
        if ((Board != null && Board.Count() > 1) || filteredData.FilterBoard != null)
        {
            filteredData.FilterBoard.Clear();
            filteredData.FilterBoard.Add(new HotelSaleResultFilterList()
            {
                ID = string.Empty,
                Name = HttpContext.GetGlobalResourceObject("LibraryResource", "ComboAll").ToString(),
            });
            foreach (var row in Board.OrderBy(o => o.Name))
            {
                filteredData.FilterBoard.Add(new HotelSaleResultFilterList()
                {
                    ID = row.ID.ToString(),
                    Name = row.Name,
                    Select = row.ID.ToString() == filteredData.FilterRoomSelect
                });
            }
        }

        return filteredData;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static object getResultGrid(int? Page, int? PageItemCount, bool? Filtered, bool? NewSearch, string Sorting, List<mixFilterSelectedList> FilterData)
    {
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;

        if (HttpContext.Current.Session["MixCriteria"] == null) return null;
        MixSearchCriteria criteria = (MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];

        string errorMsg = string.Empty;

        bool useLocalName = !UserData.CheckMarketLang || (UserData.CheckMarketLang && UserData.EqMarketLang);
        bool? showOffers = Conversion.getBoolOrNull(new TvBo.Common().getFormConfigValue("SearchPanel", "showOffers"));
        bool ExtAllotCont = false;
        object extAllotControl = new TvBo.Common().getFormConfigValue("SearchPanel", "ExtAllotControl");
        if (extAllotControl != null) ExtAllotCont = (bool)extAllotControl;

        List<SearchResultOH> priceList = new List<SearchResultOH>();
        List<HotelGroup> result = new List<HotelGroup>();

        string defaultSorting = Conversion.getStrOrNull(new Common().getFormConfigValue("SearchPanel", "defaultSearchOrder"));
        string defaultSortingValue = Conversion.getStrOrNull(new Common().getFormConfigValue("SearchPanel", "defaultSearchOrderValue"));
        defaultSorting = string.IsNullOrEmpty(defaultSorting) ? "price_down" : defaultSorting;
        
        Sorting = string.IsNullOrEmpty(Sorting) ? defaultSorting : Sorting;

        string sortValue = string.Empty;
        string sortDirection = "down";
        if (!string.IsNullOrEmpty(Sorting) && Sorting.Split('_').Length > 1)
        {
            sortValue = Sorting.Split('_')[0];
            sortDirection = Sorting.Split('_')[1];
            switch (sortValue)
            {
                case "popular":
                    sortValue = "OrderHotel";
                    break;
                case "name":
                    sortValue = "HotelName";
                    break;
                case "price":
                    sortValue = "MinPrice";
                    break;
                default:
                    sortValue = string.IsNullOrEmpty(defaultSortingValue) ? "MinPrice" : defaultSortingValue;
                    break;
            }
        }

        HotelSaleResultFilter filter = new HotelSaleResultFilter();
        foreach (mixFilterSelectedList row in FilterData)
        {
            switch (row.FieldName)
            {
                case "rfPaxLocation": filter.FilterResortSelect = Conversion.getStrOrNull(row.Value); break;
                case "rfCategory": filter.FilterCategorySelect = Conversion.getStrOrNull(row.Value); break;
                case "rfHotel": filter.FilterHotelSelect = Conversion.getStrOrNull(row.Value); break;
                case "rfRoom": filter.FilterRoomSelect = Conversion.getStrOrNull(row.Value); break;
                case "rfBoard": filter.FilterBoardSelect = Conversion.getStrOrNull(row.Value); break;
            }
        }
        filter.Filtered = !string.IsNullOrEmpty(filter.FilterResortSelect) ||
                          !string.IsNullOrEmpty(filter.FilterBoardSelect) ||
                          !string.IsNullOrEmpty(filter.FilterCategorySelect) ||
                          !string.IsNullOrEmpty(filter.FilterHotelSelect) ||
                          !string.IsNullOrEmpty(filter.FilterRoomSelect);

        int totalResult = 0;

        if ((NewSearch.HasValue && NewSearch.Value) || !NewSearch.HasValue || HttpContext.Current.Session["SearchResultOH"] == null)
        {
            priceList = new OnlyHotelSearch().getHotelSaleSearch(UserData, criteria, null, ref errorMsg);
            if (priceList == null || !priceList.Any() || !string.IsNullOrEmpty(errorMsg))
                return HttpContext.GetGlobalResourceObject("Controls", "addPleaseCorrectDate");
            List<HotelDispOrderRecord> hotelOrderList = new Hotels().getHotelOrders(UserData, UserData.Operator, UserData.Market, ref errorMsg);
            foreach (SearchResultOH row in priceList) {
                HotelDispOrderRecord hotelOrder = hotelOrderList.Find(f => f.Hotel == row.Hotel);
                row.OrderHotel = hotelOrder != null ? (hotelOrder.DispNo.HasValue ? hotelOrder.DispNo.Value : (short?)9999) : (short?)9999;
            }
            result = new OnlyHotelSearch().getHotelSalePriceGroup(UserData, criteria, priceList, useLocalName, false, ref errorMsg);
            var tmp = string.Equals(sortDirection, "down") ? result.AsQueryable().OrderBy(sortValue) : result.AsQueryable().OrderByDescending(sortValue);
            List<HotelGroup> filteredRslt = new List<HotelGroup>();
            int grpID = 0;
            foreach (HotelGroup row in tmp)
            {
                grpID++;
                row.HotelGroupID = grpID;
                row.Parameter1 = grpID % 2 != 0;
                filteredRslt.Add(row);
            }
            result = filteredRslt;

            totalResult = result.Count;
            HttpContext.Current.Session["SearchResultOH"] = priceList;
        }
        else
        {
            priceList = (List<SearchResultOH>)HttpContext.Current.Session["SearchResultOH"];

            if (filter.Filtered)
            {
                List<SearchResultOH> filteredPrices = (from q in priceList
                                                       where (string.IsNullOrEmpty(filter.FilterResortSelect) || q.HotLocation.ToString() == filter.FilterResortSelect) &&
                                                             (string.IsNullOrEmpty(filter.FilterBoardSelect) || q.Board == filter.FilterBoardSelect) &&
                                                             (string.IsNullOrEmpty(filter.FilterCategorySelect) || q.HotCat == filter.FilterCategorySelect) &&
                                                             (string.IsNullOrEmpty(filter.FilterHotelSelect) || q.Hotel == filter.FilterHotelSelect) &&
                                                             (string.IsNullOrEmpty(filter.FilterRoomSelect) || q.Room == filter.FilterRoomSelect)
                                                       select q).ToList<SearchResultOH>();
                result = new OnlyHotelSearch().getHotelSalePriceGroup(UserData, criteria, filteredPrices, useLocalName, false, ref errorMsg);
            }
            else
            {
                result = new OnlyHotelSearch().getHotelSalePriceGroup(UserData, criteria, priceList, useLocalName, false, ref errorMsg);
            }
            var tmp = string.Equals(sortDirection, "down") ? result.AsQueryable().OrderBy(sortValue) : result.AsQueryable().OrderByDescending(sortValue);            
            List<HotelGroup> filteredRslt = new List<HotelGroup>();
            int grpID = 0;
            foreach (HotelGroup row in tmp)
            {
                grpID++;
                row.HotelGroupID = grpID;
                row.Parameter1 = grpID % 2 != 0;
                filteredRslt.Add(row);
            }
            result = filteredRslt;

            totalResult = result.Count;
        }

        var filteredResult = result.Where(w => w.HotelGroupID >= ((Page) * PageItemCount - PageItemCount) && w.HotelGroupID < (Page) * PageItemCount).ToArray();

        return new
        {
            //FilterValue =resultFilterMix
            lblBookButton = HttpContext.GetGlobalResourceObject("PackageSearchResult", "book"),
            lblStopSale= HttpContext.GetGlobalResourceObject("PackageSearchResult", "stopSale"),
            lblSelected = "&nbsp;",
            lblAccomName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblAccom"),
            lblBoardName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblBoardName"),
            lblRoomName = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblRoomName"),
            lblPrice = HttpContext.GetGlobalResourceObject("PackageSearchResult", "lblPrice"),
            lblTotalResult = string.Format(HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblTotalResultCount").ToString(), totalResult),
            lblSortBy = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortBy"),
            lblSortByPopular = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortPopular"),
            lblSortByNameF = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByNameF"),
            lblSortByNameL = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByNameL"),
            lblSortByPriceF = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByPriceF"),
            lblSortByPriceL = HttpContext.GetGlobalResourceObject("OnlyHotelMix", "lblSortByPriceL"),
            titleResortList = HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblResort"),
            titleCategoryList = HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblCategory"),
            titleHotelList = HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblHotel"),
            titleRoomList = HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblRoom"),
            titleBoardList = HttpContext.GetGlobalResourceObject("PackageSearchFilter", "lblBoard"),
            lblOffer = "&nbsp;",
            NumberFormat = UserData.Ci.NumberFormat,
            HotelGroup = filteredResult,
            totalResultCount = result.Count,
            PageItemCount = PageItemCount,
            CurrentPage = Page,
            FilterDetails = createResultFilter(UserData, priceList, filter),
            Sorting = string.IsNullOrEmpty(Sorting) ? defaultSorting : Sorting
        };
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json, XmlSerializeString = false)]
    public static string getAllotmentControl(string RefNoList)
    {
        return string.Empty;
        /*
        if (HttpContext.Current.Session["UserData"] == null) { HttpContext.Current.Response.StatusCode = 408; return null; }
        TvBo.User UserData = (TvBo.User)HttpContext.Current.Session["UserData"];
        Thread.CurrentThread.CurrentCulture = UserData.Ci;
        Thread.CurrentThread.CurrentUICulture = UserData.Ci;
        TvBo.MultiRoomResult searchResult = (TvBo.MultiRoomResult)HttpContext.Current.Session["SearchResult"];
        List<SearchResultOH> _bookList = new List<SearchResultOH>();
        MixSearchCriteria _criteria = (MixSearchCriteria)HttpContext.Current.Session["MixCriteria"];
        if (_criteria.SType == SearchType.TourPackageSearch) return string.Empty;
        for (int i = 0; i < RefNoList.Split(',').Length; i++)
        {
            int refNo = Convert.ToInt32(RefNoList.Split(',')[i]);
            SearchResultOH bookRow = searchResult.Find(f => f.RefNo == refNo);
            _bookList.Add(bookRow);
        }

        string errorMsg = string.Empty;

        //if (new Reservation().priceSearchAllotControl(UserData, _bookList, _criteria.SType == SearchType.TourPackageSearch, ref errorMsg))
            return string.Empty;
        //else return errorMsg;
         */
    }
}

